﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IPuestosGridBuilder : IGridViewBuilder {
        IPuestosTempletesGridBuilder Templetes();
    }

    public interface IPuestosTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IPuestosTempletesGridBuilder Master();
    }

    public interface IPuestosColumnsGridBuilder : IGridViewColumnsBuild {
        IPuestosColumnsGridBuilder IdPuesto();

        IPuestosColumnsGridBuilder IsActivo();

        IPuestosColumnsGridBuilder IdDepartamento();

        IPuestosColumnsGridBuilder Descripcion();

        IPuestosColumnsGridBuilder Sueldo();

        IPuestosColumnsGridBuilder SueldoMaximo();

        IPuestosColumnsGridBuilder RiesgoPuesto();

        IPuestosColumnsGridBuilder Creo();

        IPuestosColumnsGridBuilder FechaNuevo();
    }
}
