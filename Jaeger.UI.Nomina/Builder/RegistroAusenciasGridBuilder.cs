﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.Aplication.Nomina.Services;

namespace Jaeger.UI.Nomina.Builder {
    public class RegistroAusenciasGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IRegistroAusenciasGridBuilder,
         IRegistroAusenciasTempletesGridBuilder, IRegistroAusenciasColumnsGridBuilder {

        public RegistroAusenciasGridBuilder() : base() { }

        public IRegistroAusenciasTempletesGridBuilder Templetes() {
            return this;
        }

        public IRegistroAusenciasTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.IdEmpleado().IdNomina().IdConcepto().Fecha().Creo().FechaNuevo();
            return this;
        }

        public IRegistroAusenciasTempletesGridBuilder Simple() {
            this._Columns.Clear();
            this.Fecha().Tipo();
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder IdEmpleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdEmpleado",
                HeaderText = "IdEmpleado",
                Name = "IdEmpleado",
                IsVisible = true
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder Tipo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                IsVisible = true,
                DataSource = RegistroAusenciasService.TipoDias(),
                DisplayMember = "Descriptor",
                ValueMember = "Id",
                Width = 90,
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder IdNomina() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdNomina",
                HeaderText = "IdNomina",
                Name = "IdNomina",
                IsVisible = false
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder IdConcepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdConcepto",
                HeaderText = "IdConcepto",
                Name = "IdConcepto",
                IsVisible = true
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder Fecha() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "Fecha",
                HeaderText = "Fecha",
                Name = "Fecha",
                FormatString = this.FormatStringDate,
                Width = 85,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                IsVisible = true
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                IsVisible = true
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                IsVisible = true
            });
            return this;
        }

        public IRegistroAusenciasColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaNuevo",
                HeaderText = "FechaNuevo",
                Name = "FechaNuevo",
                IsVisible = true
            });
            return this;
        }
    }
}
