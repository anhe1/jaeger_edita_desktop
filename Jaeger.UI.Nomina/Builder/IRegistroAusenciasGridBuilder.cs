﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IRegistroAusenciasGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IRegistroAusenciasTempletesGridBuilder Templetes();
    }

    public interface IRegistroAusenciasTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IRegistroAusenciasTempletesGridBuilder Templetes();
        IRegistroAusenciasTempletesGridBuilder Master();
        IRegistroAusenciasTempletesGridBuilder Simple();
    }

    public interface IRegistroAusenciasColumnsGridBuilder : IGridViewColumnsBuild {
        IRegistroAusenciasColumnsGridBuilder IdEmpleado();
        IRegistroAusenciasColumnsGridBuilder Tipo();
        IRegistroAusenciasColumnsGridBuilder IdNomina();
        IRegistroAusenciasColumnsGridBuilder IdConcepto();
        IRegistroAusenciasColumnsGridBuilder Fecha();
        IRegistroAusenciasColumnsGridBuilder Nota();
        IRegistroAusenciasColumnsGridBuilder Creo();
        IRegistroAusenciasColumnsGridBuilder FechaNuevo();
    }
}
