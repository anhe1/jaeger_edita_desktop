﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IAreaGridBuilder : IGridViewBuilder {
        IAreaTempletesGridBuilder Templetes();
    }

    public interface IAreaTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IAreaTempletesGridBuilder Master();
    }

    public interface IAreaColumnsGridBuilder : IGridViewColumnsBuild {
        IAreaColumnsGridBuilder IdArea();

        IAreaColumnsGridBuilder IdCategoria();

        IAreaColumnsGridBuilder CtaContable();

        IAreaColumnsGridBuilder Descripcion();

        IAreaColumnsGridBuilder Responsable();
    }
}
