﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class PeriodoGridViewBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IPeriodoGridViewBuilder, IPeriodoTempletesGridViewBuilder, IPeriodoColumnsGridViewBuilder {
        public PeriodoGridViewBuilder() : base() { }

        public IPeriodoTempletesGridViewBuilder Templetes() {
            return this;
        }

        public IPeriodoTempletesGridViewBuilder Master() {
            this._Columns.Clear();
            this.Empleado().JornadasTrabajo().Ausencia().Incapacidad().Puntualidad().HorasReloj().TotalHorasExtra().HorasAutorizadas().TotalVacaciones().SalarioDiario().SalarioDiarioIntegrado().ISPT()
                .SubSidioAlEmpleo().IMSSTotalE().IMSSTotalP().PercepionGravado().PercepcionExento().TotalPercepciones().TotalDeducciones().SueldoNeto().Sueldo();
            return this;
        }

        public IPeriodoTempletesGridViewBuilder Percepciones() {
            this._Columns.Clear();
            this.Clave().Tipo().Concepto().ImporteGravado().ImporteExento();
            return this;
        }
        public IPeriodoTempletesGridViewBuilder Deducciones() {
            this._Columns.Clear();
            this.Clave().Tipo().Concepto().ImporteGravado();
            return this;
        }

        #region general
        public IPeriodoColumnsGridViewBuilder Empleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Empleado",
                HeaderText = "Empleado",
                IsPinned = true,
                Name = "Empleado",
                PinPosition = PinnedColumnPosition.Left,
                Width = 200
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder JornadasTrabajo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "JornadasTrabajo",
                HeaderText = "J. T.",
                IsVisible = true,
                Name = "JornadasTrabajo",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 35
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder Ausencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Ausencia",
                HeaderText = "Aus.",
                Name = "Ausencia",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 35
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder Incapacidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Incapacidad",
                HeaderText = "Inc.",
                Name = "Incapacidad",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 35
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder Puntualidad() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Standard,
                FieldName = "Puntualidad",
                HeaderText = "P.",
                Name = "Puntualidad",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 35
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder HorasReloj() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "HorasReloj",
                HeaderText = "Horas Reloj",
                IsVisible = true,
                Name = "HorasReloj",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                WrapText = true
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder TotalHorasExtra() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TotalHorasExtra",
                HeaderText = "Hrs. Extra",
                Name = "TotalHorasExtra",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                WrapText = true,
                ReadOnly = true
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder HorasAutorizadas() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "HorasAutorizadas",
                HeaderText = "Horas Aut.",
                Name = "HorasAutorizadas",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                WrapText = true,
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder TotalVacaciones() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TotalVacaciones",
                HeaderText = "Dias Vac.",
                Name = "TotalVacaciones",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                WrapText = true,
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder SalarioDiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "SalarioDiario",
                FormatString = "{0:n}",
                HeaderText = "S. D.",
                Name = "SalarioDiario",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 60
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder SalarioDiarioIntegrado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "SDI",
                FormatString = "{0:n}",
                HeaderText = "S. D. I.",
                Name = "SalarioDiarioIntegrado",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 60
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder SueldoSalarios() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "SueldoSalarios",
                FormatString = "{0:n}",
                HeaderText = "Sueldo",
                Name = "SueldoSalarios",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder ISPT() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "ISPT",
                FormatString = "{0:n}",
                HeaderText = "ISR Causado",
                Name = "ISR",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder SubSidioAlEmpleo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "SubSidioAlEmpleo",
                FormatString = "{0:n}",
                HeaderText = "SubSidio",
                Name = "SubSidioAlEmpleo",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder IMSSTotalE() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "IMSSTotalE",
                FormatString = "{0:n}",
                HeaderText = "IMSS \r\nObrero",
                Name = "IMSSTotalE",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder IMSSTotalP() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "IMSSTotalP",
                FormatString = "{0:n}",
                HeaderText = "IMSS \r\nPatronal",
                Name = "IMSSTotalP",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder PercepionGravado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "PercepcionGravado",
                FormatString = "{0:n}",
                HeaderText = "T. Percepciones\r\nGravado",
                Name = "PercepcionGravado",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder PercepcionExento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "Percepcionexento",
                FormatString = "{0:n}",
                HeaderText = "T. Percepciones\r\nExento",
                Name = "Percepcionexento",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder TotalPercepciones() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "TotalPercepciones",
                FormatString = "{0:n}",
                HeaderText = "T.\r\nPercepciones",
                Name = "TotalPercepciones",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder TotalDeducciones() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "Deducciones",
                FormatString = "{0:n}",
                HeaderText = "T.\r\nDeducciones",
                Name = "Deducciones",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder SueldoNeto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "SueldoNeto",
                FormatString = "{0:n}",
                HeaderText = "Sueldo Neto",
                Name = "SueldoNeto",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder Sueldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed,
                FieldName = "Sueldo",
                FormatString = "{0:n}",
                HeaderText = "Sueldo",
                IsPinned = true,
                Name = "Sueldo",
                PinPosition = PinnedColumnPosition.Right,
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }
        #endregion

        #region conceptos
        public IPeriodoColumnsGridViewBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder Tipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 75,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder Concepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Concepto",
                HeaderText = "Concepto",
                Name = "Concepto",
                Width = 300
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder ImporteGravado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImporteGravado",
                FormatString = "{0:n}",
                HeaderText = "Gravado",
                Name = "ImporteGravado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPeriodoColumnsGridViewBuilder ImporteExento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImporteExento",
                FormatString = "{0:n}",
                HeaderText = "Exento",
                Name = "ImporteExento",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }
        #endregion
    }
}
