﻿using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Builder {
    public class DepartamentoGridBuilder : GridViewBuilder, IGridViewBuilder, IDepartamentoGridBuilder, IDepartamentoTempletesGridBuilder, IDepartamentoColumnsGridBuilder {

        public DepartamentoGridBuilder() : base() { }

        public IDepartamentoTempletesGridBuilder Templetes() {
            return this;
        }

        public IDepartamentoTempletesGridBuilder Master() {
            this.IdDepartamento().IsActive().IdClasificacion().Area().Nombre().CtaContable().Clasificacion().CetroCostos();
            return this;
        }

        #region columnas
        public IDepartamentoColumnsGridBuilder IdDepartamento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "IdDepartamento",
                FormatString = "{0:0000#}",
                HeaderText = "Núm",
                Name = "ID",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IDepartamentoColumnsGridBuilder IsActive() {
            return this;
        }

        public IDepartamentoColumnsGridBuilder Area() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Standard,
                FieldName = "IdArea",
                HeaderText = "Área",
                Name = "IdArea",
                ReadOnly = true,
                Width = 200,
                TextAlignment = System.Drawing.ContentAlignment.MiddleLeft,
                DisplayMember = "Descripcion",
                ValueMember = "Id"
            });
            return this;
        }

        public IDepartamentoColumnsGridBuilder IdClasificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "IdClasificacion",
                FormatString = "{0:0000#}",
                HeaderText = "Clasificación",
                Name = "IdClasificacion",
                ReadOnly = true,
                Width = 200,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IDepartamentoColumnsGridBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripcion",
                Name = "Descripcion",
                ReadOnly = true,
                Width = 220
            });
            return this;
        }

        public IDepartamentoColumnsGridBuilder CtaContable() {
            return this;
        }

        public IDepartamentoColumnsGridBuilder Clasificacion() {
            return this;
        }

        public IDepartamentoColumnsGridBuilder CetroCostos() {
            return this;
        }
        #endregion
    }
}
