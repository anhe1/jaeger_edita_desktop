﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IPeriodoGridViewBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IPeriodoTempletesGridViewBuilder Templetes();
    }

    public interface IPeriodoTempletesGridViewBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IPeriodoTempletesGridViewBuilder Master();
        IPeriodoTempletesGridViewBuilder Percepciones();
        IPeriodoTempletesGridViewBuilder Deducciones();
    }

    public interface IPeriodoColumnsGridViewBuilder : IGridViewColumnsBuild {
        #region general
        IPeriodoColumnsGridViewBuilder Empleado();
        IPeriodoColumnsGridViewBuilder JornadasTrabajo();
        IPeriodoColumnsGridViewBuilder Ausencia();
        IPeriodoColumnsGridViewBuilder Incapacidad();
        IPeriodoColumnsGridViewBuilder Puntualidad();
        IPeriodoColumnsGridViewBuilder HorasReloj();
        IPeriodoColumnsGridViewBuilder TotalHorasExtra();
        IPeriodoColumnsGridViewBuilder HorasAutorizadas();
        IPeriodoColumnsGridViewBuilder TotalVacaciones();
        IPeriodoColumnsGridViewBuilder SalarioDiario();
        IPeriodoColumnsGridViewBuilder SalarioDiarioIntegrado();
        IPeriodoColumnsGridViewBuilder SueldoSalarios();
        IPeriodoColumnsGridViewBuilder ISPT();
        IPeriodoColumnsGridViewBuilder SubSidioAlEmpleo();
        IPeriodoColumnsGridViewBuilder IMSSTotalE();
        IPeriodoColumnsGridViewBuilder IMSSTotalP();
        IPeriodoColumnsGridViewBuilder PercepionGravado();
        IPeriodoColumnsGridViewBuilder PercepcionExento();
        IPeriodoColumnsGridViewBuilder TotalPercepciones();
        IPeriodoColumnsGridViewBuilder TotalDeducciones();
        IPeriodoColumnsGridViewBuilder SueldoNeto();
        IPeriodoColumnsGridViewBuilder Sueldo();
        #endregion

        #region conceptos
        IPeriodoColumnsGridViewBuilder Clave();
        IPeriodoColumnsGridViewBuilder Tipo();
        IPeriodoColumnsGridViewBuilder Concepto();
        IPeriodoColumnsGridViewBuilder ImporteGravado();
        IPeriodoColumnsGridViewBuilder ImporteExento();
        #endregion
    }
}
