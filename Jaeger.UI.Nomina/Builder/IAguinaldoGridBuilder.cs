﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IAguinaldoGridBuilder : IGridViewBuilder {
        IAguinaldoTempletesGridBuilder Templetes();
    }

    public interface IAguinaldoTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IAguinaldoTempletesGridBuilder Master();
    }

    public interface IAguinaldoColumnsGridBuilder : IGridViewColumnsBuild {
        #region columnas
        IAguinaldoColumnsGridBuilder IdEmpleado();

        IAguinaldoColumnsGridBuilder Clave();

        IAguinaldoColumnsGridBuilder Nombre();

        IAguinaldoColumnsGridBuilder FechaInicioLaboral();

        IAguinaldoColumnsGridBuilder DiasLaborados();

        IAguinaldoColumnsGridBuilder Ausencias();

        IAguinaldoColumnsGridBuilder SalarioDiario();

        IAguinaldoColumnsGridBuilder SalarioMensual();

        IAguinaldoColumnsGridBuilder DiasAguinaldo();

        IAguinaldoColumnsGridBuilder Importe();

        IAguinaldoColumnsGridBuilder BaseGravable();

        IAguinaldoColumnsGridBuilder BaseExento();

        IAguinaldoColumnsGridBuilder BaseGravada();

        IAguinaldoColumnsGridBuilder ISR();

        IAguinaldoColumnsGridBuilder Subsidio();

        IAguinaldoColumnsGridBuilder ISRSalarioMasAguinaldo();

        IAguinaldoColumnsGridBuilder ISRSueldo();

        IAguinaldoColumnsGridBuilder ISRAguinaldo();

        IAguinaldoColumnsGridBuilder AguinaldoPagar();
        #endregion
    }
}
