﻿using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class ComprobanteGridBuilder : GridViewBuilder, IComprobanteGridBuilder, IComprobanteTempletesGridBuilder, IComprobanteColumnsGridBuilder {
        public ComprobanteGridBuilder() : base() { }

        public IComprobanteTempletesGridBuilder Templetes() {
            return this;
        }

        public IComprobanteTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.IdNomina().IdDirectorio().IdComprobante().NoEmpleado().ReceptorNombre().ReceptorRFC().Departamento().Puesto().ClaveRiesgoPuesto().ClaveTipoRegimen().ClaveTipoContrato().ClaveTipoJornada().ClavePeriricidadPago()
                .FechaPago().FechaInicialPago().FechaFinalPago().NumDiasPagados().FechaInicioRelLaboral().Antiguedad().SalarioBaseCotApor().SalarioDiarioIntegrado().PercepcionTotalGravado().PercepcionTotalExento()
            .DeduccionTotalGravado().DeduccionTotalExento().DescuentoIncapacidad().HorasExtra().Total().ClaveTipoNomina().IdDocumento().Estado().FechaEstado().FechaTimbre().Correo().UrlFileXML().UrlFilePDF();
            return this;
        }

        public IComprobanteColumnsGridBuilder IdNomina() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdNomina",
                HeaderText = "Id",
                IsVisible = false,
                Name = "IdNomina",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder IdDirectorio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdDirectorio",
                HeaderText = "IdDirectorio",
                IsVisible = false,
                Name = "IdDirectorio",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder IdComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdComprobante",
                HeaderText = "IdComprobante",
                IsVisible = false,
                Name = "IdComprobante",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder NoEmpleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoEmpleado",
                HeaderText = "Núm.",
                Name = "NoEmpleado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ReceptorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Empleado",
                Name = "ReceptorNombre",
                Width = 250
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "RFC",
                IsVisible = false,
                Name = "ReceptorRFC",
                Width = 85
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder Departamento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Departamento",
                HeaderText = "Departamento",
                Name = "Departamento",
                Width = 150
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder Puesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Puesto",
                HeaderText = "Puesto",
                Name = "Puesto",
                Width = 150
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ClaveRiesgoPuesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveRiesgoPuesto",
                HeaderText = "Riesgo \r\nde Puesto",
                Name = "ClaveRiesgoPuesto",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ClaveTipoRegimen() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveTipoRegimen",
                HeaderText = "Tipo \r\nReg.",
                Name = "ClaveTipoRegimen",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ClaveTipoContrato() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveTipoContrato",
                HeaderText = "Tipo de \r\nContrato",
                Name = "ClaveTipoContrato",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ClaveTipoJornada() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveTipoJornada",
                HeaderText = "Tipo \r\nJornada",
                Name = "ClaveTipoJornada",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ClavePeriricidadPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClavePeriricidadPago",
                HeaderText = "Perioricidad \r\nde Pago",
                Name = "ClavePeriricidadPago",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder FechaPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaPago",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. \r\nPago",
                Name = "FechaPago",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder FechaInicialPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaInicialPago",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Ini. \r\nPago",
                Name = "FechaInicialPago",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder FechaFinalPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaFinalPago",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Final \r\nPago",
                Name = "FechaFinalPago",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder NumDiasPagados() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumDiasPagados",
                HeaderText = "Días \r\nPagados",
                Name = "NumDiasPagados",
                FormatString = this.FormatStringNumber,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder FechaInicioRelLaboral() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaInicioRelLaboral",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. \r\nRelacion",
                Name = "FechaInicioRelLaboral",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder Antiguedad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Antiguedad",
                HeaderText = "Antiguedad",
                Name = "Antiguedad"
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder SalarioBaseCotApor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioBaseCotApor",
                FormatString = "{0:n}",
                HeaderText = "Salario Base \r\nde Cotización",
                Name = "SalarioBaseCotApor",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder SalarioDiarioIntegrado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioDiarioIntegrado",
                FormatString = "{0:n}",
                HeaderText = "Salario Diario \r\nIntegrado",
                Name = "SalarioDiarioIntegrado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder PercepcionTotalGravado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "PercepcionTotalGravado",
                FormatString = "{0:n}",
                HeaderText = "P. Total \r\nGravado",
                Name = "PercepcionTotalGravado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder PercepcionTotalExento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "PercepcionTotalExento",
                FormatString = "{0:n}",
                HeaderText = "P. Total \r\nExento",
                Name = "PercepcionTotalExento",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder DeduccionTotalGravado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "DeduccionTotalGravado",
                FormatString = "{0:n}",
                HeaderText = "D. Total \r\nGravado",
                Name = "DeduccionTotalGravado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65,
                IsVisible = false,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder DeduccionTotalExento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "DeduccionTotalExento",
                FormatString = "{0:n}",
                HeaderText = "D. Total \r\nDeducciones",
                Name = "DeduccionTotalExento",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder DescuentoIncapacidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "DescuentoIncapacidad",
                FormatString = "{0:n}",
                HeaderText = "Descuento por\r\n Incapacidad",
                Name = "DescuentoIncapacidad",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder HorasExtra() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "HorasExtra",
                FormatString = "{0:n}",
                HeaderText = "Horas \r\nExtra",
                Name = "HorasExtra",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder Total() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder ClaveTipoNomina() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveTipoNomina",
                HeaderText = "Tipo de \r\nNómina",
                Name = "ClaveTipoNomina",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "UUID",
                Name = "IdDocumento",
                Width = 240
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder Estado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado \r\nSAT",
                Name = "Estado"
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder FechaEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaEstado",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. \r\nEstado",
                Name = "FechaEstado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder FechaTimbre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaTimbre",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. \r\nCertificación",
                Name = "FechaTimbre",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder Correo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "_ctlemp_mail",
                HeaderText = "Correo",
                Name = "Correo",
                IsVisible = false
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder UrlFileXML() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFileXML",
                HeaderText = "XML",
                Name = "UrlFileXML",
                Width = 40
            });
            return this;
        }

        public IComprobanteColumnsGridBuilder UrlFilePDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFilePDF",
                HeaderText = "PDF",
                Name = "UrlFilePDF",
                Width = 40
            });
            return this;
        }
    }
}
