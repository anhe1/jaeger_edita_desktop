﻿using Telerik.WinControls.UI.Export;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class AreaGridBuilder : GridViewBuilder, IGridViewBuilder, IAreaGridBuilder, IAreaTempletesGridBuilder, IAreaColumnsGridBuilder {
        public AreaGridBuilder() : base() { }

        public IAreaTempletesGridBuilder Templetes() {
            return this;
        }

        public IAreaTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.IdArea().IdCategoria().CtaContable().Descripcion().Responsable();
            return this;
        }

        public IAreaColumnsGridBuilder IdArea() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "Id",
                FormatString = "{0:0000#}",
                HeaderText = "Núm",
                Name = "ID",
                ReadOnly = true,
                Width = 50,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IAreaColumnsGridBuilder IdCategoria() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "IdCategoria",
                FormatString = "{0:0000#}",
                HeaderText = "IdCategoria",
                Name = "IdCategoria",
                ReadOnly = true,
                Width = 90,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IAreaColumnsGridBuilder CtaContable() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Standard,
                FieldName = "CtaContable",
                HeaderText = "CtaContable",
                Name = "CtaContable",
                ReadOnly = true,
                Width = 90,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IAreaColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Standard,
                FieldName = "Descripcion",
                HeaderText = "Descripcion",
                Name = "Descripcion",
                ReadOnly = true,
                Width = 200,
                TextAlignment = System.Drawing.ContentAlignment.MiddleLeft,
            });
            return this;
        }

        public IAreaColumnsGridBuilder Responsable() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Standard,
                FieldName = "Responsable",
                HeaderText = "Responsable",
                Name = "Responsable",
                ReadOnly = true,
                Width = 200,
                TextAlignment = System.Drawing.ContentAlignment.MiddleLeft,
            });
            return this;
        }
    }
}
