﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IHorasExtraGridViewBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IHorasExtraTempletesGridViewBuilder Templetes();
    }

    public interface IHorasExtraTempletesGridViewBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IHorasExtraTempletesGridViewBuilder Master();
        IHorasExtraTempletesGridViewBuilder Simple();
    }

    public interface IHorasExtraColumnsGridViewBuilder : IGridViewColumnsBuild {
        IHorasExtraColumnsGridViewBuilder Id();
        IHorasExtraColumnsGridViewBuilder Activo();
        IHorasExtraColumnsGridViewBuilder IdConcepto();
        IHorasExtraColumnsGridViewBuilder IdNomina();
        IHorasExtraColumnsGridViewBuilder IdEmpleado();
        IHorasExtraColumnsGridViewBuilder IdTipo();
        IHorasExtraColumnsGridViewBuilder Fecha();
        IHorasExtraColumnsGridViewBuilder Cantidad();
        IHorasExtraColumnsGridViewBuilder Creo();
        IHorasExtraColumnsGridViewBuilder FechaNuevo();
    }
}
