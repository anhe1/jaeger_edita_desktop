﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface ICuentaBancoGridViewBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        ICuentaBancariaTempleteGridViewBuilder Templetes();
    }

    public interface ICuentaBancariaTempleteGridViewBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        ICuentaBancariaTempleteGridViewBuilder Master();
    }

    public interface ICuentaBancariaColumnsGridViewBuilder : IGridViewColumnsBuild {
        ICuentaBancariaColumnsGridViewBuilder Activo();
        ICuentaBancariaColumnsGridViewBuilder Verificado();
        ICuentaBancariaColumnsGridViewBuilder TipoCuenta();
        ICuentaBancariaColumnsGridViewBuilder Banco();
        ICuentaBancariaColumnsGridViewBuilder Clave();
        ICuentaBancariaColumnsGridViewBuilder InstitucionBancaria();
        ICuentaBancariaColumnsGridViewBuilder NumeroCuenta();
        ICuentaBancariaColumnsGridViewBuilder Creo();
        ICuentaBancariaColumnsGridViewBuilder FechaNuevo();
    }
}
