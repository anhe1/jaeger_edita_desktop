﻿using System;
using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Builder {
    public interface IRegistroVacacionesGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IRegistroVacacionesTempletesGridBuilder Templetes();
    }

    public interface IRegistroVacacionesTempletesGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IRegistroVacacionesTempletesGridBuilder Master();
    }

    public interface IRegistroVacacionesColumnsGridBuilder : IGridViewColumnsBuild {
        IRegistroVacacionesColumnsGridBuilder Numero();
        IRegistroVacacionesColumnsGridBuilder Empleado();
        IRegistroVacacionesColumnsGridBuilder FechaIniRelLaboral();
        IRegistroVacacionesColumnsGridBuilder FechaAniversario();
        IRegistroVacacionesColumnsGridBuilder AnioServicio();
        IRegistroVacacionesColumnsGridBuilder Dias();
        IRegistroVacacionesColumnsGridBuilder Disfrutados();
        IRegistroVacacionesColumnsGridBuilder Restan();
    }

    public class RegistroVacacionesGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IRegistroVacacionesGridBuilder, IRegistroVacacionesTempletesGridBuilder, 
        IRegistroVacacionesColumnsGridBuilder {
        public RegistroVacacionesGridBuilder() : base() { }

        public IRegistroVacacionesTempletesGridBuilder Templetes() {
            return this;
        }

        public IRegistroVacacionesTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.Numero().Empleado().FechaIniRelLaboral().FechaAniversario().AnioServicio().Dias().Disfrutados().Restan();
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder Empleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Empleado",
                HeaderText = "Empleado",
                IsPinned = true,
                Name = "Empleado",
                PinPosition = PinnedColumnPosition.Left,
                Width = 220
            });
            return this;
        }
        
        public IRegistroVacacionesColumnsGridBuilder Numero() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdEmpleado",
                HeaderText = "Núm.",
                IsPinned = true,
                Name = "IdEmpleado",
                FormatString = this.FormarStringFolio,
                PinPosition = PinnedColumnPosition.Left,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder AnioServicio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "AniosServicio",
                HeaderText = "Años de \r\nServicio",
                Name = "AniosServicio",
                Width = 75,
                FormatString = "{0:N0}",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder FechaIniRelLaboral() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FecInicioRelLaboral",
                HeaderText = "Fec. Rel.\r\nLaboral",
                Name = "FecInicioRelLaboral",
                Width = 75,
                FormatString = this.FormatStringDate,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder FechaAniversario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FecAniversario",
                HeaderText = "Aniversario",
                Name = "FecAniversario",
                Width = 75,
                FormatString = this.FormatStringDate,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder Dias() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Dias",
                HeaderText = "Dias",
                Name = "Dias",
                Width = 75,
                FormatString = "{0:N0}",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder Disfrutados() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Disfrutados",
                HeaderText = "Días\r\nDisfrutados",
                Name = "Disfrutados",
                Width = 75,
                FormatString = "{0:N0}",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IRegistroVacacionesColumnsGridBuilder Restan() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Restan",
                HeaderText = "Restan",
                Name = "Restan",
                Width = 75,
                FormatString = "{0:N0}",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }
    }
}
