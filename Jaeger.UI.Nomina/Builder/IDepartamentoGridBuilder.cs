﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IDepartamentoGridBuilder : IGridViewBuilder {
        IDepartamentoTempletesGridBuilder Templetes();
    }

    public interface IDepartamentoTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IDepartamentoTempletesGridBuilder Master();
    }

    public interface IDepartamentoColumnsGridBuilder : IGridViewColumnsBuild {
        IDepartamentoColumnsGridBuilder IdDepartamento();

        IDepartamentoColumnsGridBuilder IsActive();

        IDepartamentoColumnsGridBuilder Area();

        IDepartamentoColumnsGridBuilder IdClasificacion();

        IDepartamentoColumnsGridBuilder Nombre();

        IDepartamentoColumnsGridBuilder CtaContable();

        IDepartamentoColumnsGridBuilder Clasificacion();

        IDepartamentoColumnsGridBuilder CetroCostos();
    }
}
