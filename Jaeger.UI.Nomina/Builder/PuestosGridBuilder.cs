﻿using Telerik.WinControls.UI.Export;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class PuestosGridBuilder : GridViewBuilder, IGridViewBuilder, IPuestosGridBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IPuestosTempletesGridBuilder, IPuestosColumnsGridBuilder {

        public PuestosGridBuilder() : base() { }

        public IPuestosTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public IPuestosTempletesGridBuilder Master() {
            this.IdPuesto().IsActivo().IdDepartamento().Descripcion().Sueldo().RiesgoPuesto().Creo().FechaNuevo();
            return this;
        }

        #region columnas
        public IPuestosColumnsGridBuilder IdPuesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "IdPuesto",
                FormatString = "{0:0000#}",
                HeaderText = "Núm",
                Name = "IdPuesto",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IPuestosColumnsGridBuilder IdDepartamento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "IdDepartamento",
                FormatString = "{0:0000#}",
                HeaderText = "Núm",
                Name = "IdDepartamento",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IPuestosColumnsGridBuilder IsActivo() {
            var activo = new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Status",
                Name = "Activo",
            };
            var conditionalFormattingObject1 = new ConditionalFormattingObject {
                ApplyToRow = true,
                CellBackColor = System.Drawing.Color.Empty,
                CellForeColor = System.Drawing.Color.Empty,
                Name = "Activo",
                RowBackColor = System.Drawing.Color.Empty,
                RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = System.Drawing.Color.Gray,
                TValue1 = "False",
                TValue2 = "False"
            };
            activo.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            this._Columns.Add(activo);
            return this;
        }

        public IPuestosColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripcion",
                Name = "Descripcion",
                ReadOnly = true,
                Width = 220
            });
            return this;
        }

        public IPuestosColumnsGridBuilder Sueldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "Sueldo",
                FormatString = "{0:n}",
                HeaderText = "Sueldo",
                IsPinned = true,
                Name = "Sueldo",
                PinPosition = PinnedColumnPosition.Right,
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPuestosColumnsGridBuilder SueldoMaximo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "SueldoMaximo",
                FormatString = "{0:n}",
                HeaderText = "SueldoMaximo",
                IsPinned = true,
                Name = "Sueldo Máximo",
                PinPosition = PinnedColumnPosition.Right,
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPuestosColumnsGridBuilder RiesgoPuesto() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "RiesgoPuesto",
                HeaderText = "Cv. Riesgo \r\nPuesto",
                Name = "RiesgoPuesto",
                Width = 85
            });
            return this;
        }

        public IPuestosColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                Width = 75
            });
            return this;
        }

        public IPuestosColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaNuevo",
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                Width = 75
            });
            return this;
        }
        #endregion
    }
}
