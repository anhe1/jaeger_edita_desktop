﻿
namespace Jaeger.UI.Nomina.Forms {
    partial class ConsultaRecibosControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor5 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor6 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GrupoBusqueda = new Telerik.WinControls.UI.RadGroupBox();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.Empleados = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PorEmpleado = new Telerik.WinControls.UI.RadCheckBox();
            this.Departamentos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PorDepartamento = new Telerik.WinControls.UI.RadCheckBox();
            this.AlLabel = new Telerik.WinControls.UI.RadLabel();
            this.DelLabel = new Telerik.WinControls.UI.RadLabel();
            this.FechaFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaInicial = new Telerik.WinControls.UI.RadDateTimePicker();
            this.PorFechaLabel = new Telerik.WinControls.UI.RadLabel();
            this.PorRangoFechas = new Telerik.WinControls.UI.RadRadioButton();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.PorIdDocumento = new Telerik.WinControls.UI.RadRadioButton();
            this.Nominas = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PorNomina = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).BeginInit();
            this.GrupoBusqueda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DelLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorFechaLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorRangoFechas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorIdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorNomina)).BeginInit();
            this.SuspendLayout();
            // 
            // GrupoBusqueda
            // 
            this.GrupoBusqueda.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GrupoBusqueda.Controls.Add(this.Buscar);
            this.GrupoBusqueda.Controls.Add(this.Empleados);
            this.GrupoBusqueda.Controls.Add(this.PorEmpleado);
            this.GrupoBusqueda.Controls.Add(this.Departamentos);
            this.GrupoBusqueda.Controls.Add(this.PorDepartamento);
            this.GrupoBusqueda.Controls.Add(this.AlLabel);
            this.GrupoBusqueda.Controls.Add(this.DelLabel);
            this.GrupoBusqueda.Controls.Add(this.FechaFinal);
            this.GrupoBusqueda.Controls.Add(this.FechaInicial);
            this.GrupoBusqueda.Controls.Add(this.PorFechaLabel);
            this.GrupoBusqueda.Controls.Add(this.PorRangoFechas);
            this.GrupoBusqueda.Controls.Add(this.IdDocumento);
            this.GrupoBusqueda.Controls.Add(this.PorIdDocumento);
            this.GrupoBusqueda.Controls.Add(this.Nominas);
            this.GrupoBusqueda.Controls.Add(this.PorNomina);
            this.GrupoBusqueda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrupoBusqueda.HeaderText = "Búsqueda";
            this.GrupoBusqueda.Location = new System.Drawing.Point(0, 0);
            this.GrupoBusqueda.Name = "GrupoBusqueda";
            this.GrupoBusqueda.Size = new System.Drawing.Size(331, 407);
            this.GrupoBusqueda.TabIndex = 1;
            this.GrupoBusqueda.Text = "Búsqueda";
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.Location = new System.Drawing.Point(210, 371);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(110, 24);
            this.Buscar.TabIndex = 15;
            this.Buscar.Text = "Buscar";
            // 
            // Empleados
            // 
            this.Empleados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Empleados.AutoFilter = true;
            // 
            // Empleados.NestedRadGridView
            // 
            this.Empleados.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Empleados.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Empleados.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Empleados.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Empleados.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Empleados.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Empleados.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Empleados.EditorControl.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn13.FieldName = "Nombre";
            filterDescriptor5.IsFilterEditor = true;
            filterDescriptor5.PropertyName = "Nombre";
            filterDescriptor5.Value = "Nombre";
            gridViewTextBoxColumn13.FilterDescriptor = filterDescriptor5;
            gridViewTextBoxColumn13.HeaderText = "Nombre";
            gridViewTextBoxColumn13.Name = "Nombre";
            gridViewTextBoxColumn13.Width = 239;
            this.Empleados.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13});
            this.Empleados.EditorControl.MasterTemplate.EnableFiltering = true;
            this.Empleados.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Empleados.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor5});
            this.Empleados.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Empleados.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Empleados.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.Empleados.EditorControl.Name = "NestedRadGridView";
            this.Empleados.EditorControl.ReadOnly = true;
            this.Empleados.EditorControl.ShowGroupPanel = false;
            this.Empleados.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Empleados.EditorControl.TabIndex = 0;
            this.Empleados.Enabled = false;
            this.Empleados.Location = new System.Drawing.Point(14, 340);
            this.Empleados.Name = "Empleados";
            this.Empleados.NullText = "Empleado";
            this.Empleados.Size = new System.Drawing.Size(306, 20);
            this.Empleados.TabIndex = 14;
            this.Empleados.TabStop = false;
            // 
            // PorEmpleado
            // 
            this.PorEmpleado.Location = new System.Drawing.Point(14, 316);
            this.PorEmpleado.Name = "PorEmpleado";
            this.PorEmpleado.Size = new System.Drawing.Size(90, 18);
            this.PorEmpleado.TabIndex = 13;
            this.PorEmpleado.Text = "Por Empleado";
            this.PorEmpleado.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.CheckPorEmpleado_ToggleStateChanged);
            // 
            // Departamentos
            // 
            this.Departamentos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Departamentos.AutoFilter = true;
            this.Departamentos.DisplayMember = "Descripcion";
            // 
            // Departamentos.NestedRadGridView
            // 
            this.Departamentos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamentos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamentos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamentos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamentos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamentos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamentos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Departamentos.EditorControl.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn14.FieldName = "Descripcion";
            filterDescriptor6.IsFilterEditor = true;
            filterDescriptor6.PropertyName = "Descripcion";
            gridViewTextBoxColumn14.FilterDescriptor = filterDescriptor6;
            gridViewTextBoxColumn14.HeaderText = "Descripción";
            gridViewTextBoxColumn14.Name = "Descripcion";
            gridViewTextBoxColumn14.Width = 239;
            this.Departamentos.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn14});
            this.Departamentos.EditorControl.MasterTemplate.EnableFiltering = true;
            this.Departamentos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamentos.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor6});
            this.Departamentos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamentos.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Departamentos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.Departamentos.EditorControl.Name = "NestedRadGridView";
            this.Departamentos.EditorControl.ReadOnly = true;
            this.Departamentos.EditorControl.ShowGroupPanel = false;
            this.Departamentos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamentos.EditorControl.TabIndex = 0;
            this.Departamentos.Enabled = false;
            this.Departamentos.Location = new System.Drawing.Point(14, 290);
            this.Departamentos.Name = "Departamentos";
            this.Departamentos.NullText = "Departamento";
            this.Departamentos.Size = new System.Drawing.Size(306, 20);
            this.Departamentos.TabIndex = 12;
            this.Departamentos.TabStop = false;
            this.Departamentos.ValueMember = "Descripcion";
            // 
            // PorDepartamento
            // 
            this.PorDepartamento.Location = new System.Drawing.Point(14, 266);
            this.PorDepartamento.Name = "PorDepartamento";
            this.PorDepartamento.Size = new System.Drawing.Size(113, 18);
            this.PorDepartamento.TabIndex = 11;
            this.PorDepartamento.Text = "Por Departamento";
            this.PorDepartamento.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.CheckPorDepartamento_ToggleStateChanged);
            // 
            // AlLabel
            // 
            this.AlLabel.Location = new System.Drawing.Point(14, 232);
            this.AlLabel.Name = "AlLabel";
            this.AlLabel.Size = new System.Drawing.Size(19, 18);
            this.AlLabel.TabIndex = 10;
            this.AlLabel.Text = "Al:";
            // 
            // DelLabel
            // 
            this.DelLabel.Location = new System.Drawing.Point(14, 204);
            this.DelLabel.Name = "DelLabel";
            this.DelLabel.Size = new System.Drawing.Size(25, 18);
            this.DelLabel.TabIndex = 9;
            this.DelLabel.Text = "Del:";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Enabled = false;
            this.FechaFinal.Location = new System.Drawing.Point(45, 230);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(208, 20);
            this.FechaFinal.TabIndex = 8;
            this.FechaFinal.TabStop = false;
            this.FechaFinal.Text = "jueves, 1 de noviembre de 2018";
            this.FechaFinal.Value = new System.DateTime(2018, 11, 1, 22, 10, 26, 820);
            // 
            // FechaInicial
            // 
            this.FechaInicial.Enabled = false;
            this.FechaInicial.Location = new System.Drawing.Point(45, 204);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(208, 20);
            this.FechaInicial.TabIndex = 7;
            this.FechaInicial.TabStop = false;
            this.FechaInicial.Text = "jueves, 1 de noviembre de 2018";
            this.FechaInicial.Value = new System.DateTime(2018, 11, 1, 22, 10, 26, 820);
            // 
            // PorFechaLabel
            // 
            this.PorFechaLabel.Location = new System.Drawing.Point(45, 180);
            this.PorFechaLabel.Name = "PorFechaLabel";
            this.PorFechaLabel.Size = new System.Drawing.Size(115, 18);
            this.PorFechaLabel.TabIndex = 6;
            this.PorFechaLabel.Text = "Por Fecha de emisión:";
            // 
            // PorRangoFechas
            // 
            this.PorRangoFechas.Location = new System.Drawing.Point(14, 150);
            this.PorRangoFechas.Name = "PorRangoFechas";
            this.PorRangoFechas.Size = new System.Drawing.Size(120, 18);
            this.PorRangoFechas.TabIndex = 4;
            this.PorRangoFechas.Text = "Por rango de fechas";
            this.PorRangoFechas.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.PorRangoFechas_ToggleStateChanged);
            // 
            // IdDocumento
            // 
            this.IdDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDocumento.Enabled = false;
            this.IdDocumento.Location = new System.Drawing.Point(14, 114);
            this.IdDocumento.MaxLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.NullText = "UUID";
            this.IdDocumento.Size = new System.Drawing.Size(306, 20);
            this.IdDocumento.TabIndex = 3;
            this.IdDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PorIdDocumento
            // 
            this.PorIdDocumento.Location = new System.Drawing.Point(14, 90);
            this.PorIdDocumento.Name = "PorIdDocumento";
            this.PorIdDocumento.Size = new System.Drawing.Size(131, 18);
            this.PorIdDocumento.TabIndex = 2;
            this.PorIdDocumento.Text = "Por Folio Fiscal (UUID)";
            this.PorIdDocumento.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.PorIdDocumento_ToggleStateChanged);
            // 
            // Nominas
            // 
            this.Nominas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nominas.AutoSizeDropDownHeight = true;
            this.Nominas.AutoSizeDropDownToBestFit = true;
            this.Nominas.DisplayMember = "Descripcion";
            this.Nominas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Nominas.NestedRadGridView
            // 
            this.Nominas.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nominas.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nominas.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nominas.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nominas.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nominas.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nominas.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Nominas.EditorControl.MasterTemplate.AllowRowResize = false;
            this.Nominas.EditorControl.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn15.FieldName = "IdControl";
            gridViewTextBoxColumn15.HeaderText = "Núm.";
            gridViewTextBoxColumn15.Name = "IdControl";
            gridViewTextBoxColumn15.Width = 27;
            gridViewTextBoxColumn16.FieldName = "Descripcion";
            gridViewTextBoxColumn16.HeaderText = "Descripción";
            gridViewTextBoxColumn16.Name = "Descripcion";
            gridViewTextBoxColumn16.Width = 141;
            gridViewTextBoxColumn17.FieldName = "TipoNomina";
            gridViewTextBoxColumn17.HeaderText = "Tipo";
            gridViewTextBoxColumn17.Name = "TipoNomina";
            gridViewTextBoxColumn17.Width = 27;
            gridViewTextBoxColumn18.FieldName = "FechaSubida";
            gridViewTextBoxColumn18.FormatString = "{0:d}";
            gridViewTextBoxColumn18.HeaderText = "Fecha";
            gridViewTextBoxColumn18.Name = "FechaSubida";
            gridViewTextBoxColumn18.Width = 47;
            this.Nominas.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.Nominas.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nominas.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nominas.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nominas.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.Nominas.EditorControl.Name = "NestedRadGridView";
            this.Nominas.EditorControl.ReadOnly = true;
            this.Nominas.EditorControl.ShowGroupPanel = false;
            this.Nominas.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Nominas.EditorControl.TabIndex = 0;
            this.Nominas.Enabled = false;
            this.Nominas.Location = new System.Drawing.Point(14, 53);
            this.Nominas.Name = "Nominas";
            this.Nominas.NullText = "Selecciona";
            this.Nominas.Size = new System.Drawing.Size(306, 20);
            this.Nominas.TabIndex = 1;
            this.Nominas.TabStop = false;
            this.Nominas.ValueMember = "IdControl";
            // 
            // PorNomina
            // 
            this.PorNomina.Location = new System.Drawing.Point(14, 29);
            this.PorNomina.Name = "PorNomina";
            this.PorNomina.Size = new System.Drawing.Size(80, 18);
            this.PorNomina.TabIndex = 0;
            this.PorNomina.Text = "Por Nómina";
            this.PorNomina.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.CheckPorNomina_ToggleStateChanged);
            // 
            // ConsultaRecibosControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GrupoBusqueda);
            this.Name = "ConsultaRecibosControl";
            this.Size = new System.Drawing.Size(331, 407);
            this.Load += new System.EventHandler(this.ConsultaRecibosControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).EndInit();
            this.GrupoBusqueda.ResumeLayout(false);
            this.GrupoBusqueda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DelLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorFechaLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorRangoFechas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorIdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorNomina)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox GrupoBusqueda;
        protected internal Telerik.WinControls.UI.RadButton Buscar;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Empleados;
        private Telerik.WinControls.UI.RadCheckBox PorEmpleado;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamentos;
        private Telerik.WinControls.UI.RadCheckBox PorDepartamento;
        private Telerik.WinControls.UI.RadLabel AlLabel;
        private Telerik.WinControls.UI.RadLabel DelLabel;
        protected internal Telerik.WinControls.UI.RadDateTimePicker FechaFinal;
        protected internal Telerik.WinControls.UI.RadDateTimePicker FechaInicial;
        private Telerik.WinControls.UI.RadLabel PorFechaLabel;
        private Telerik.WinControls.UI.RadRadioButton PorRangoFechas;
        protected internal Telerik.WinControls.UI.RadTextBox IdDocumento;
        private Telerik.WinControls.UI.RadRadioButton PorIdDocumento;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Nominas;
        private Telerik.WinControls.UI.RadRadioButton PorNomina;
    }
}
