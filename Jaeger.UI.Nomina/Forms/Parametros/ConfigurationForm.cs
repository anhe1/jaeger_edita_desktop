﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    /// <summary>
    /// formulario de configuracion de la aplicacion
    /// </summary>
    public partial class ConfigurationForm : RadForm {
        #region declaracion de controles
        protected Aplication.Nomina.Contracts.IConfigurationService Service;
        protected Domain.Nomina.Contracts.IConfiguration Configuration;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ConfigurationForm() {
            InitializeComponent();
        }

        private void ConfigurationForm_Load(object sender, EventArgs e) {
            using(var espera = new UI.Common.Forms.Waiting1Form(this.Preset)) {
                espera.Text = "Cargando configuración";
                espera.ShowDialog(this);
            }
        }

        #region botones
        private void TConfigurar_Guardar_Click(object sender, EventArgs e) {
            this.Configuration.Folder = this.TConfigurar.Folder.Text;
            this.Service.Set(this.Configuration);
        }

        private void TConfigurar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Preset() {
            this.Configuration = this.Service.Get();
            if (this.Configuration.DataBase == null) {
                this.Configuration.DataBase = new Domain.DataBase.Entities.DataBaseConfiguracion();
            }
            this.TConfigurar.DataBaseGrid.SelectedObject = this.Configuration.DataBase;
            this.TConfigurar.Folder.Text = this.Configuration.Folder;
        }
        #endregion
    }
}
