﻿namespace Jaeger.UI.Nomina.Forms.Parametros {
    partial class ConfigurationForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
            this.TConfigura_Guardar = new Telerik.WinControls.UI.RadButton();
            this.TConfigura_Cerrar = new Telerik.WinControls.UI.RadButton();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.TConfigurar = new Jaeger.UI.Nomina.Forms.Parametros.ConfigurationControl();
            ((System.ComponentModel.ISupportInitialize)(this.TConfigura_Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TConfigura_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TConfigura_Guardar
            // 
            this.TConfigura_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TConfigura_Guardar.Location = new System.Drawing.Point(187, 484);
            this.TConfigura_Guardar.Name = "TConfigura_Guardar";
            this.TConfigura_Guardar.Size = new System.Drawing.Size(110, 24);
            this.TConfigura_Guardar.TabIndex = 7;
            this.TConfigura_Guardar.Text = "Guardar";
            this.TConfigura_Guardar.Click += new System.EventHandler(this.TConfigurar_Guardar_Click);
            // 
            // TConfigura_Cerrar
            // 
            this.TConfigura_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TConfigura_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TConfigura_Cerrar.Location = new System.Drawing.Point(303, 484);
            this.TConfigura_Cerrar.Name = "TConfigura_Cerrar";
            this.TConfigura_Cerrar.Size = new System.Drawing.Size(110, 24);
            this.TConfigura_Cerrar.TabIndex = 8;
            this.TConfigura_Cerrar.Text = "Cerrar";
            this.TConfigura_Cerrar.Click += new System.EventHandler(this.TConfigurar_Cerrar_Click);
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(425, 45);
            this.Encabezado.TabIndex = 120;
            this.Encabezado.TabStop = false;
            // 
            // TConfigurar
            // 
            this.TConfigurar.Location = new System.Drawing.Point(12, 51);
            this.TConfigurar.MinimumSize = new System.Drawing.Size(400, 416);
            this.TConfigurar.Name = "TConfigurar";
            this.TConfigurar.Size = new System.Drawing.Size(400, 416);
            this.TConfigurar.TabIndex = 121;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TConfigura_Cerrar;
            this.ClientSize = new System.Drawing.Size(425, 520);
            this.Controls.Add(this.TConfigurar);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.TConfigura_Guardar);
            this.Controls.Add(this.TConfigura_Cerrar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(433, 550);
            this.Name = "ConfigurationForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TConfigura_Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TConfigura_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton TConfigura_Guardar;
        private Telerik.WinControls.UI.RadButton TConfigura_Cerrar;
        private System.Windows.Forms.PictureBox Encabezado;
        private ConfigurationControl TConfigurar;
    }
}