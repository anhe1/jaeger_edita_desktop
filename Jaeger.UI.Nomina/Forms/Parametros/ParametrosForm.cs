﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    public partial class ParametrosForm : RadForm {
        protected IParametrosService service;
        protected ITablaISRService serviceISR;
        protected ISubsidioAlEmpleoService serviceSubsidio;
        private IConfiguracionDetailModel _CurrentConf;
        private BindingList<TablaImpuestoSobreRentaModel> _tablaISR;
        private BindingList<TablaSubsidioAlEmpleoModel> _tablaSubSidio;

        public ParametrosForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ParametrosForm_Load(object sender, EventArgs e) {

            this.PeriodicidadClave.DataSource = CommonService.GetPeriodos();
            this.PeriodicidadClave.DisplayMember = "Descripcion";
            this.PeriodicidadClave.ValueMember = "Id";

            TParametro_Actualizar_Click(sender, e);
        }

        private void TParametro_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog();
            }
            this.CreateBinding();
        }

        private void TParametro_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog();
            }
            this.CreateBinding();
        }

        private void TParametro_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.PeriodicidadClave.DataBindings.Clear();
            this.PeriodicidadClave.DataBindings.Add("SelectedValue", this._CurrentConf, "IdPeriodo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DiasPago.DataBindings.Clear();
            this.DiasPago.DataBindings.Add("Value", this._CurrentConf, "DiasPeriodo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DiasXAnio.DataBindings.Clear();
            this.DiasXAnio.DataBindings.Add("Value", this._CurrentConf, "DiasXAnio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SalarioMinimo.DataBindings.Clear();
            this.SalarioMinimo.DataBindings.Add("Value", this._CurrentConf, "SalarioMinimo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.UMA.DataBindings.Clear();
            this.UMA.DataBindings.Add("Value", this._CurrentConf, "UMA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.HorasXDia.DataBindings.Clear();
            this.HorasXDia.DataBindings.Add("Value", this._CurrentConf, "HorasDia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TablaISR.DataSource = this._tablaISR;
            this.TablaISR.ValueMember = "Id";
            this.TablaISR.DisplayMember = "Descripcion";
            this.TablaISR.DataBindings.Clear();
            this.TablaISR.DataBindings.Add("SelectedValue", this._CurrentConf, "IdTablaISR", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TablaSubsidio.DataSource = this._tablaSubSidio;
            this.TablaSubsidio.DisplayMember = "Descripcion";
            this.TablaSubsidio.ValueMember = "Id";
            this.TablaSubsidio.DataBindings.Clear();
            this.TablaSubsidio.DataBindings.Add("SelectedValue", this._CurrentConf, "IdTablaSubsidio", true, DataSourceUpdateMode.OnPropertyChanged);

            #region cuotas imss patron

            this.CuotaFijaP.DataBindings.Clear();
            this.CuotaFijaP.DataBindings.Add("Value", this._CurrentConf, "CuotaFijaP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ExcedenteP.DataBindings.Clear();
            this.ExcedenteP.DataBindings.Add("Value", this._CurrentConf, "ExcedenteP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PrestacionDineroP.DataBindings.Clear();
            this.PrestacionDineroP.DataBindings.Add("Value", this._CurrentConf, "PrestacionDineroP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PensionadosYBeneficiariosP.DataBindings.Clear();
            this.PensionadosYBeneficiariosP.DataBindings.Add("Value", this._CurrentConf, "PensionadosYBeneficiariosP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.InvalidezYVidaP.DataBindings.Clear();
            this.InvalidezYVidaP.DataBindings.Add("Value", this._CurrentConf, "InvalidezYVidaP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RiesgoTrabajo.DataBindings.Clear();
            this.RiesgoTrabajo.DataBindings.Add("Value", this._CurrentConf, "RiesgoTrabajo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PrestacionesSociales.DataBindings.Clear();
            this.PrestacionesSociales.DataBindings.Add("Value", this._CurrentConf, "PrestacionesSociales", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SeguroDeRetiro.DataBindings.Clear();
            this.SeguroDeRetiro.DataBindings.Add("Value", this._CurrentConf, "SeguroDeRetiro", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CesantiaYVejezP.DataBindings.Clear();
            this.CesantiaYVejezP.DataBindings.Add("Value", this._CurrentConf, "CesantiaYVejezP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Infonavit.DataBindings.Clear();
            this.Infonavit.DataBindings.Add("Value", this._CurrentConf, "Infonavit", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion
            #region cuotas imss trabajador
            this.ExcedenteE.DataBindings.Clear();
            this.ExcedenteE.DataBindings.Add("Value", this._CurrentConf, "ExcedenteE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PrestacionDineroE.DataBindings.Clear();
            this.PrestacionDineroE.DataBindings.Add("Value", this._CurrentConf, "PrestacionDineroE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PensionadosYBeneficiariosE.DataBindings.Clear();
            this.PensionadosYBeneficiariosE.DataBindings.Add("Value", this._CurrentConf, "PensionadosYBeneficiariosE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.InvalidezYVidaE.DataBindings.Clear();
            this.InvalidezYVidaE.DataBindings.Add("Value", this._CurrentConf, "InvalidezYVidaE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CesantiaYVejezE.DataBindings.Clear();
            this.CesantiaYVejezE.DataBindings.Add("Value", this._CurrentConf, "CesantiaYVejezE", true, DataSourceUpdateMode.OnPropertyChanged);
            #endregion
        }

        private void Actualizar() {
            this._CurrentConf = this.service.GetConfiguracion();
            this._tablaISR = new BindingList<TablaImpuestoSobreRentaModel>(this.serviceISR.GetList());
            this._tablaSubSidio = new BindingList<TablaSubsidioAlEmpleoModel>(this.serviceSubsidio.GetList());
        }

        private void Guardar() {
            this._CurrentConf = this.service.Save(this._CurrentConf);
        }
    }
}
