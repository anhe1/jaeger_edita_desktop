﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    public partial class TablaUMAForm : RadForm {
        protected IUMAService service;
        private BindingList<UMAModel> models;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;
        public TablaUMAForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void TablaUMAForm_Load(object sender, EventArgs e) {

            this.TEmpleado.Guardar.Click += this.TEmpleado_ButtonGuardar_Click;
            this.TEmpleado.Actualizar.Click += this.TEmpleado_ButtonActualizar_Click;
            this.TEmpleado.Cerrar.Click += this.TEmpleado_ButtonCerrar_Click;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            //this.service = new UMAService();
        }

        private void TEmpleado_ButtonGuardar_Click(object sender, EventArgs e) {
            this.models = this.service.Save(this.models);
        }

        private void TEmpleado_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.radGridView1.DataSource = this.models;
        }

        private void TEmpleado_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            this.models = this.service.GetList();
        }
    }
}
