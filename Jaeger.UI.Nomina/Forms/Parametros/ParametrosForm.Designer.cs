﻿namespace Jaeger.UI.Nomina.Forms.Parametros {
    partial class ParametrosForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParametrosForm));
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.TablaSubsidio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.TablaISR = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.DiasPago = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.HorasXDia = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.UMA = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.SalarioMinimo = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.DiasXAnio = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.PeriodicidadClave = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.tabControl = new Telerik.WinControls.UI.RadPageView();
            this.pageGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.pagePago = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.RiesgoTrabajo = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.Infonavit = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.PensionadosYBeneficiariosE = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.PensionadosYBeneficiariosP = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.PrestacionesSociales = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.CesantiaYVejezE = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.CesantiaYVejezP = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.SeguroDeRetiro = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.InvalidezYVidaE = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.PrestacionDineroE = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.ExcedenteE = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.InvalidezYVidaP = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.PrestacionDineroP = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.ExcedenteP = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.CuotaFijaP = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.button2 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TablaSubsidio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaSubsidio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaSubsidio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaISR.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaISR.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HorasXDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UMA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalarioMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasXAnio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodicidadClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodicidadClave.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodicidadClave.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            this.pagePago.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RiesgoTrabajo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Infonavit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PensionadosYBeneficiariosE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PensionadosYBeneficiariosP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrestacionesSociales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CesantiaYVejezE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CesantiaYVejezP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeguroDeRetiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvalidezYVidaE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrestacionDineroE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExcedenteE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvalidezYVidaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrestacionDineroP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExcedenteP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuotaFijaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.TablaSubsidio);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.TablaISR);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.DiasPago);
            this.radGroupBox1.Controls.Add(this.HorasXDia);
            this.radGroupBox1.Controls.Add(this.UMA);
            this.radGroupBox1.Controls.Add(this.SalarioMinimo);
            this.radGroupBox1.Controls.Add(this.DiasXAnio);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.PeriodicidadClave);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "Nómina";
            this.radGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(379, 322);
            this.radGroupBox1.TabIndex = 120;
            this.radGroupBox1.Text = "Nómina";
            // 
            // TablaSubsidio
            // 
            this.TablaSubsidio.AutoSizeDropDownToBestFit = true;
            this.TablaSubsidio.DisplayMember = "Anio";
            this.TablaSubsidio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // TablaSubsidio.NestedRadGridView
            // 
            this.TablaSubsidio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.TablaSubsidio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TablaSubsidio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TablaSubsidio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.TablaSubsidio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.TablaSubsidio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.TablaSubsidio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.TablaSubsidio.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "ID";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "ID";
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "Anio";
            gridViewTextBoxColumn2.HeaderText = "Año";
            gridViewTextBoxColumn2.Name = "Anio";
            gridViewTextBoxColumn3.FieldName = "Periodo";
            gridViewTextBoxColumn3.HeaderText = "Período";
            gridViewTextBoxColumn3.Name = "Periodo";
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripcion";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "Descripcion";
            this.TablaSubsidio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.TablaSubsidio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.TablaSubsidio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.TablaSubsidio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.TablaSubsidio.EditorControl.Name = "NestedRadGridView";
            this.TablaSubsidio.EditorControl.ReadOnly = true;
            this.TablaSubsidio.EditorControl.ShowGroupPanel = false;
            this.TablaSubsidio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.TablaSubsidio.EditorControl.TabIndex = 0;
            this.TablaSubsidio.Location = new System.Drawing.Point(125, 208);
            this.TablaSubsidio.Name = "TablaSubsidio";
            this.TablaSubsidio.NullText = "Clave";
            this.TablaSubsidio.Size = new System.Drawing.Size(111, 20);
            this.TablaSubsidio.TabIndex = 382;
            this.TablaSubsidio.TabStop = false;
            this.TablaSubsidio.ValueMember = "Id";
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(14, 210);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(51, 18);
            this.radLabel13.TabIndex = 381;
            this.radLabel13.Text = "Subsidio:";
            // 
            // TablaISR
            // 
            this.TablaISR.AutoSizeDropDownToBestFit = true;
            this.TablaISR.DisplayMember = "Descripcion";
            this.TablaISR.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // TablaISR.NestedRadGridView
            // 
            this.TablaISR.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.TablaISR.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TablaISR.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TablaISR.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.TablaISR.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.TablaISR.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.TablaISR.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.TablaISR.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.FieldName = "ID";
            gridViewTextBoxColumn5.HeaderText = "Id";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "ID";
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "Anio";
            gridViewTextBoxColumn6.HeaderText = "Año";
            gridViewTextBoxColumn6.Name = "Anio";
            gridViewTextBoxColumn7.FieldName = "Periodo";
            gridViewTextBoxColumn7.HeaderText = "Período";
            gridViewTextBoxColumn7.Name = "Periodo";
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripcion";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Descripcion";
            this.TablaISR.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.TablaISR.EditorControl.MasterTemplate.EnableGrouping = false;
            this.TablaISR.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.TablaISR.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.TablaISR.EditorControl.Name = "NestedRadGridView";
            this.TablaISR.EditorControl.ReadOnly = true;
            this.TablaISR.EditorControl.ShowGroupPanel = false;
            this.TablaISR.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.TablaISR.EditorControl.TabIndex = 0;
            this.TablaISR.Location = new System.Drawing.Point(125, 183);
            this.TablaISR.Name = "TablaISR";
            this.TablaISR.NullText = "Clave";
            this.TablaISR.Size = new System.Drawing.Size(111, 20);
            this.TablaISR.TabIndex = 380;
            this.TablaISR.TabStop = false;
            this.TablaISR.ValueMember = "Id";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(14, 184);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(54, 18);
            this.radLabel12.TabIndex = 379;
            this.radLabel12.Text = "Tabla ISR:";
            // 
            // DiasPago
            // 
            this.DiasPago.Location = new System.Drawing.Point(125, 27);
            this.DiasPago.Name = "DiasPago";
            this.DiasPago.Size = new System.Drawing.Size(111, 20);
            this.DiasPago.TabIndex = 378;
            this.DiasPago.TabStop = false;
            this.DiasPago.Value = "0";
            // 
            // HorasXDia
            // 
            this.HorasXDia.Location = new System.Drawing.Point(125, 157);
            this.HorasXDia.Name = "HorasXDia";
            this.HorasXDia.Size = new System.Drawing.Size(111, 20);
            this.HorasXDia.TabIndex = 377;
            this.HorasXDia.TabStop = false;
            this.HorasXDia.Value = "0";
            // 
            // UMA
            // 
            this.UMA.Location = new System.Drawing.Point(125, 131);
            this.UMA.Name = "UMA";
            this.UMA.Size = new System.Drawing.Size(111, 20);
            this.UMA.TabIndex = 376;
            this.UMA.TabStop = false;
            this.UMA.Value = "0";
            // 
            // SalarioMinimo
            // 
            this.SalarioMinimo.Location = new System.Drawing.Point(125, 105);
            this.SalarioMinimo.Name = "SalarioMinimo";
            this.SalarioMinimo.Size = new System.Drawing.Size(111, 20);
            this.SalarioMinimo.TabIndex = 375;
            this.SalarioMinimo.TabStop = false;
            this.SalarioMinimo.Value = "0";
            // 
            // DiasXAnio
            // 
            this.DiasXAnio.Location = new System.Drawing.Point(125, 79);
            this.DiasXAnio.Name = "DiasXAnio";
            this.DiasXAnio.Size = new System.Drawing.Size(111, 20);
            this.DiasXAnio.TabIndex = 374;
            this.DiasXAnio.TabStop = false;
            this.DiasXAnio.Value = "0";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(14, 158);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(100, 18);
            this.radLabel11.TabIndex = 353;
            this.radLabel11.Text = "Horas por Jornada:";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(14, 132);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(34, 18);
            this.radLabel10.TabIndex = 351;
            this.radLabel10.Text = "UMA:";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(14, 106);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(83, 18);
            this.radLabel9.TabIndex = 349;
            this.radLabel9.Text = "Salario mínimo:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(14, 80);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(72, 18);
            this.radLabel5.TabIndex = 347;
            this.radLabel5.Text = "Días por año:";
            // 
            // PeriodicidadClave
            // 
            this.PeriodicidadClave.AutoSizeDropDownToBestFit = true;
            this.PeriodicidadClave.DisplayMember = "Clave";
            this.PeriodicidadClave.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // PeriodicidadClave.NestedRadGridView
            // 
            this.PeriodicidadClave.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.PeriodicidadClave.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PeriodicidadClave.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PeriodicidadClave.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.PeriodicidadClave.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.PeriodicidadClave.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.PeriodicidadClave.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.PeriodicidadClave.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn9.FieldName = "Id";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Id";
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Descripcion";
            gridViewTextBoxColumn10.Width = 100;
            this.PeriodicidadClave.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.PeriodicidadClave.EditorControl.MasterTemplate.EnableGrouping = false;
            this.PeriodicidadClave.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.PeriodicidadClave.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.PeriodicidadClave.EditorControl.Name = "NestedRadGridView";
            this.PeriodicidadClave.EditorControl.ReadOnly = true;
            this.PeriodicidadClave.EditorControl.ShowGroupPanel = false;
            this.PeriodicidadClave.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.PeriodicidadClave.EditorControl.TabIndex = 0;
            this.PeriodicidadClave.Location = new System.Drawing.Point(125, 53);
            this.PeriodicidadClave.Name = "PeriodicidadClave";
            this.PeriodicidadClave.NullText = "Clave";
            this.PeriodicidadClave.Size = new System.Drawing.Size(111, 20);
            this.PeriodicidadClave.TabIndex = 120;
            this.PeriodicidadClave.TabStop = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(14, 54);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(71, 18);
            this.radLabel3.TabIndex = 106;
            this.radLabel3.Text = "Periodicidad:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(14, 28);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(74, 18);
            this.radLabel2.TabIndex = 105;
            this.radLabel2.Text = "Días de pago:";
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(406, 33);
            this.Encabezado.TabIndex = 119;
            this.Encabezado.TabStop = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.pageGeneral);
            this.tabControl.Controls.Add(this.pagePago);
            this.tabControl.DefaultPage = this.pageGeneral;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl.Location = new System.Drawing.Point(0, 33);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedPage = this.pageGeneral;
            this.tabControl.Size = new System.Drawing.Size(406, 376);
            this.tabControl.TabIndex = 124;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pageGeneral
            // 
            this.pageGeneral.Controls.Add(this.radGroupBox1);
            this.pageGeneral.ItemSize = new System.Drawing.SizeF(65F, 28F);
            this.pageGeneral.Location = new System.Drawing.Point(10, 37);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(385, 328);
            this.pageGeneral.Text = "Generales";
            // 
            // pagePago
            // 
            this.pagePago.Controls.Add(this.radGroupBox2);
            this.pagePago.ItemSize = new System.Drawing.SizeF(79F, 28F);
            this.pagePago.Location = new System.Drawing.Point(10, 37);
            this.pagePago.Name = "pagePago";
            this.pagePago.Size = new System.Drawing.Size(385, 328);
            this.pagePago.Text = "Cuotas IMSS";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.RiesgoTrabajo);
            this.radGroupBox2.Controls.Add(this.radLabel21);
            this.radGroupBox2.Controls.Add(this.Infonavit);
            this.radGroupBox2.Controls.Add(this.radLabel20);
            this.radGroupBox2.Controls.Add(this.PensionadosYBeneficiariosE);
            this.radGroupBox2.Controls.Add(this.PensionadosYBeneficiariosP);
            this.radGroupBox2.Controls.Add(this.radLabel19);
            this.radGroupBox2.Controls.Add(this.PrestacionesSociales);
            this.radGroupBox2.Controls.Add(this.radLabel18);
            this.radGroupBox2.Controls.Add(this.CesantiaYVejezE);
            this.radGroupBox2.Controls.Add(this.CesantiaYVejezP);
            this.radGroupBox2.Controls.Add(this.radLabel17);
            this.radGroupBox2.Controls.Add(this.SeguroDeRetiro);
            this.radGroupBox2.Controls.Add(this.radLabel16);
            this.radGroupBox2.Controls.Add(this.InvalidezYVidaE);
            this.radGroupBox2.Controls.Add(this.PrestacionDineroE);
            this.radGroupBox2.Controls.Add(this.ExcedenteE);
            this.radGroupBox2.Controls.Add(this.InvalidezYVidaP);
            this.radGroupBox2.Controls.Add(this.PrestacionDineroP);
            this.radGroupBox2.Controls.Add(this.ExcedenteP);
            this.radGroupBox2.Controls.Add(this.CuotaFijaP);
            this.radGroupBox2.Controls.Add(this.radLabel6);
            this.radGroupBox2.Controls.Add(this.radLabel7);
            this.radGroupBox2.Controls.Add(this.radLabel8);
            this.radGroupBox2.Controls.Add(this.radLabel15);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.radLabel14);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox2.HeaderText = "Nómina";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(385, 328);
            this.radGroupBox2.TabIndex = 121;
            this.radGroupBox2.Text = "Nómina";
            // 
            // RiesgoTrabajo
            // 
            this.RiesgoTrabajo.Location = new System.Drawing.Point(202, 176);
            this.RiesgoTrabajo.Name = "RiesgoTrabajo";
            this.RiesgoTrabajo.Size = new System.Drawing.Size(75, 20);
            this.RiesgoTrabajo.TabIndex = 408;
            this.RiesgoTrabajo.TabStop = false;
            this.RiesgoTrabajo.Value = "0";
            // 
            // radLabel21
            // 
            this.radLabel21.Location = new System.Drawing.Point(14, 176);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(97, 18);
            this.radLabel21.TabIndex = 406;
            this.radLabel21.Text = "Riesgo del trabajo";
            // 
            // Infonavit
            // 
            this.Infonavit.Location = new System.Drawing.Point(202, 280);
            this.Infonavit.Name = "Infonavit";
            this.Infonavit.Size = new System.Drawing.Size(75, 20);
            this.Infonavit.TabIndex = 404;
            this.Infonavit.TabStop = false;
            this.Infonavit.Value = "0";
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(14, 280);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(63, 18);
            this.radLabel20.TabIndex = 402;
            this.radLabel20.Text = "INFONAVIT";
            // 
            // PensionadosYBeneficiariosE
            // 
            this.PensionadosYBeneficiariosE.Location = new System.Drawing.Point(283, 125);
            this.PensionadosYBeneficiariosE.Name = "PensionadosYBeneficiariosE";
            this.PensionadosYBeneficiariosE.Size = new System.Drawing.Size(75, 20);
            this.PensionadosYBeneficiariosE.TabIndex = 401;
            this.PensionadosYBeneficiariosE.TabStop = false;
            this.PensionadosYBeneficiariosE.Value = "0";
            // 
            // PensionadosYBeneficiariosP
            // 
            this.PensionadosYBeneficiariosP.Location = new System.Drawing.Point(202, 124);
            this.PensionadosYBeneficiariosP.Name = "PensionadosYBeneficiariosP";
            this.PensionadosYBeneficiariosP.Size = new System.Drawing.Size(75, 20);
            this.PensionadosYBeneficiariosP.TabIndex = 400;
            this.PensionadosYBeneficiariosP.TabStop = false;
            this.PensionadosYBeneficiariosP.Value = "0";
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(14, 124);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(122, 18);
            this.radLabel19.TabIndex = 398;
            this.radLabel19.Text = "Gastos méd. para pens.";
            // 
            // PrestacionesSociales
            // 
            this.PrestacionesSociales.Location = new System.Drawing.Point(202, 202);
            this.PrestacionesSociales.Name = "PrestacionesSociales";
            this.PrestacionesSociales.Size = new System.Drawing.Size(75, 20);
            this.PrestacionesSociales.TabIndex = 396;
            this.PrestacionesSociales.TabStop = false;
            this.PrestacionesSociales.Value = "0";
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(14, 202);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(177, 18);
            this.radLabel18.TabIndex = 394;
            this.radLabel18.Text = "Guarderías y Prestaciones Sociales";
            // 
            // CesantiaYVejezE
            // 
            this.CesantiaYVejezE.Location = new System.Drawing.Point(283, 255);
            this.CesantiaYVejezE.Name = "CesantiaYVejezE";
            this.CesantiaYVejezE.Size = new System.Drawing.Size(75, 20);
            this.CesantiaYVejezE.TabIndex = 393;
            this.CesantiaYVejezE.TabStop = false;
            this.CesantiaYVejezE.Value = "0";
            // 
            // CesantiaYVejezP
            // 
            this.CesantiaYVejezP.Location = new System.Drawing.Point(202, 254);
            this.CesantiaYVejezP.Name = "CesantiaYVejezP";
            this.CesantiaYVejezP.Size = new System.Drawing.Size(75, 20);
            this.CesantiaYVejezP.TabIndex = 392;
            this.CesantiaYVejezP.TabStop = false;
            this.CesantiaYVejezP.Value = "0";
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(14, 254);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(142, 18);
            this.radLabel17.TabIndex = 390;
            this.radLabel17.Text = "Cesantía edad avan. y vejez";
            // 
            // SeguroDeRetiro
            // 
            this.SeguroDeRetiro.Location = new System.Drawing.Point(202, 228);
            this.SeguroDeRetiro.Name = "SeguroDeRetiro";
            this.SeguroDeRetiro.Size = new System.Drawing.Size(75, 20);
            this.SeguroDeRetiro.TabIndex = 388;
            this.SeguroDeRetiro.TabStop = false;
            this.SeguroDeRetiro.Value = "0";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(14, 228);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(36, 18);
            this.radLabel16.TabIndex = 386;
            this.radLabel16.Text = "Retiro";
            // 
            // InvalidezYVidaE
            // 
            this.InvalidezYVidaE.Location = new System.Drawing.Point(283, 151);
            this.InvalidezYVidaE.Name = "InvalidezYVidaE";
            this.InvalidezYVidaE.Size = new System.Drawing.Size(75, 20);
            this.InvalidezYVidaE.TabIndex = 385;
            this.InvalidezYVidaE.TabStop = false;
            this.InvalidezYVidaE.Value = "0";
            // 
            // PrestacionDineroE
            // 
            this.PrestacionDineroE.Location = new System.Drawing.Point(283, 98);
            this.PrestacionDineroE.Name = "PrestacionDineroE";
            this.PrestacionDineroE.Size = new System.Drawing.Size(75, 20);
            this.PrestacionDineroE.TabIndex = 384;
            this.PrestacionDineroE.TabStop = false;
            this.PrestacionDineroE.Value = "0";
            // 
            // ExcedenteE
            // 
            this.ExcedenteE.Location = new System.Drawing.Point(283, 72);
            this.ExcedenteE.Name = "ExcedenteE";
            this.ExcedenteE.Size = new System.Drawing.Size(75, 20);
            this.ExcedenteE.TabIndex = 383;
            this.ExcedenteE.TabStop = false;
            this.ExcedenteE.Value = "0";
            // 
            // InvalidezYVidaP
            // 
            this.InvalidezYVidaP.Location = new System.Drawing.Point(202, 150);
            this.InvalidezYVidaP.Name = "InvalidezYVidaP";
            this.InvalidezYVidaP.Size = new System.Drawing.Size(75, 20);
            this.InvalidezYVidaP.TabIndex = 381;
            this.InvalidezYVidaP.TabStop = false;
            this.InvalidezYVidaP.Value = "0";
            // 
            // PrestacionDineroP
            // 
            this.PrestacionDineroP.Location = new System.Drawing.Point(202, 98);
            this.PrestacionDineroP.Name = "PrestacionDineroP";
            this.PrestacionDineroP.Size = new System.Drawing.Size(75, 20);
            this.PrestacionDineroP.TabIndex = 380;
            this.PrestacionDineroP.TabStop = false;
            this.PrestacionDineroP.Value = "0";
            // 
            // ExcedenteP
            // 
            this.ExcedenteP.Location = new System.Drawing.Point(202, 72);
            this.ExcedenteP.Name = "ExcedenteP";
            this.ExcedenteP.Size = new System.Drawing.Size(75, 20);
            this.ExcedenteP.TabIndex = 379;
            this.ExcedenteP.TabStop = false;
            this.ExcedenteP.Value = "0";
            // 
            // CuotaFijaP
            // 
            this.CuotaFijaP.Location = new System.Drawing.Point(202, 46);
            this.CuotaFijaP.Name = "CuotaFijaP";
            this.CuotaFijaP.Size = new System.Drawing.Size(75, 20);
            this.CuotaFijaP.TabIndex = 378;
            this.CuotaFijaP.TabStop = false;
            this.CuotaFijaP.Value = "0";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(14, 150);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(82, 18);
            this.radLabel6.TabIndex = 353;
            this.radLabel6.Text = "Invalidez y vida";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(14, 99);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(111, 18);
            this.radLabel7.TabIndex = 351;
            this.radLabel7.Text = "Prestación en dinero:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(14, 73);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(105, 18);
            this.radLabel8.TabIndex = 349;
            this.radLabel8.Text = "Especie - Excedente";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(283, 22);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(72, 18);
            this.radLabel15.TabIndex = 347;
            this.radLabel15.Text = "% Trabajador";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(202, 22);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(51, 18);
            this.radLabel4.TabIndex = 347;
            this.radLabel4.Text = "% Patrón";
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(14, 46);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(103, 18);
            this.radLabel14.TabIndex = 347;
            this.radLabel14.Text = "Especie - Cuota Fija";
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cerrar.Location = new System.Drawing.Point(317, 417);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 193;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.TParametro_Cerrar_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(236, 417);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 194;
            this.button2.Text = "Guardar";
            this.button2.Click += new System.EventHandler(this.TParametro_Guardar_Click);
            // 
            // ParametrosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cerrar;
            this.ClientSize = new System.Drawing.Size(406, 452);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ParametrosForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Parametros";
            this.Load += new System.EventHandler(this.ParametrosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TablaSubsidio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaSubsidio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaSubsidio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaISR.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaISR.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HorasXDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UMA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalarioMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasXAnio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodicidadClave.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodicidadClave.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodicidadClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.pagePago.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RiesgoTrabajo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Infonavit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PensionadosYBeneficiariosE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PensionadosYBeneficiariosP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrestacionesSociales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CesantiaYVejezE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CesantiaYVejezP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeguroDeRetiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvalidezYVidaE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrestacionDineroE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExcedenteE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvalidezYVidaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrestacionDineroP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExcedenteP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuotaFijaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadMultiColumnComboBox PeriodicidadClave;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadPageView tabControl;
        private Telerik.WinControls.UI.RadPageViewPage pageGeneral;
        private Telerik.WinControls.UI.RadPageViewPage pagePago;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadCalculatorDropDown HorasXDia;
        private Telerik.WinControls.UI.RadCalculatorDropDown UMA;
        private Telerik.WinControls.UI.RadCalculatorDropDown SalarioMinimo;
        private Telerik.WinControls.UI.RadCalculatorDropDown DiasXAnio;
        private Telerik.WinControls.UI.RadCalculatorDropDown DiasPago;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadMultiColumnComboBox TablaISR;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadCalculatorDropDown RiesgoTrabajo;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadCalculatorDropDown Infonavit;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadCalculatorDropDown PensionadosYBeneficiariosE;
        private Telerik.WinControls.UI.RadCalculatorDropDown PensionadosYBeneficiariosP;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadCalculatorDropDown PrestacionesSociales;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadCalculatorDropDown CesantiaYVejezE;
        private Telerik.WinControls.UI.RadCalculatorDropDown CesantiaYVejezP;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadCalculatorDropDown SeguroDeRetiro;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadCalculatorDropDown InvalidezYVidaE;
        private Telerik.WinControls.UI.RadCalculatorDropDown PrestacionDineroE;
        private Telerik.WinControls.UI.RadCalculatorDropDown ExcedenteE;
        private Telerik.WinControls.UI.RadCalculatorDropDown InvalidezYVidaP;
        private Telerik.WinControls.UI.RadCalculatorDropDown PrestacionDineroP;
        private Telerik.WinControls.UI.RadCalculatorDropDown ExcedenteP;
        private Telerik.WinControls.UI.RadCalculatorDropDown CuotaFijaP;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadMultiColumnComboBox TablaSubsidio;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadButton button2;
    }
}