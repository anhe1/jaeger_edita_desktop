﻿namespace Jaeger.UI.Nomina.Forms.Parametros
{
    partial class TablaISRForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridPeriodo = new Telerik.WinControls.UI.RadGridView();
            this.gridRangos = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.panelPeriodos = new Telerik.WinControls.UI.SplitPanel();
            this.panelRangos = new Telerik.WinControls.UI.SplitPanel();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.TTabla_Guardar = new Telerik.WinControls.UI.RadButton();
            this.TTabla_Cerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPeriodo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRangos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRangos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelPeriodos)).BeginInit();
            this.panelPeriodos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelRangos)).BeginInit();
            this.panelRangos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TTabla_Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TTabla_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPeriodo
            // 
            this.gridPeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPeriodo.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridPeriodo.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.gridPeriodo.MasterTemplate.AllowDragToGroup = false;
            this.gridPeriodo.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "Anio";
            gridViewTextBoxColumn2.FormatString = "{0:N0}";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Etiqueta";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 100;
            gridViewComboBoxColumn1.DataType = typeof(int);
            gridViewComboBoxColumn1.DisplayMember = "Descripcion";
            gridViewComboBoxColumn1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            gridViewComboBoxColumn1.FieldName = "Periodo";
            gridViewComboBoxColumn1.HeaderText = "Periodo";
            gridViewComboBoxColumn1.Name = "Periodo";
            gridViewComboBoxColumn1.ValueMember = "Id";
            gridViewComboBoxColumn1.Width = 100;
            this.gridPeriodo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewComboBoxColumn1});
            this.gridPeriodo.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridPeriodo.Name = "gridPeriodo";
            this.gridPeriodo.ShowGroupPanel = false;
            this.gridPeriodo.Size = new System.Drawing.Size(233, 321);
            this.gridPeriodo.TabIndex = 1;
            // 
            // gridRangos
            // 
            this.gridRangos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRangos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridRangos.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.gridRangos.MasterTemplate.AllowDragToGroup = false;
            this.gridRangos.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn3.DataType = typeof(decimal);
            gridViewTextBoxColumn3.FieldName = "LimiteInferior";
            gridViewTextBoxColumn3.FormatString = "{0:N2}";
            gridViewTextBoxColumn3.HeaderText = "Limite Inferior";
            gridViewTextBoxColumn3.Name = "LimiteInferior";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn3.Width = 70;
            gridViewTextBoxColumn3.WrapText = true;
            gridViewTextBoxColumn4.DataType = typeof(decimal);
            gridViewTextBoxColumn4.FieldName = "LimiteSuperior";
            gridViewTextBoxColumn4.FormatString = "{0:N2}";
            gridViewTextBoxColumn4.HeaderText = "Limite Superior";
            gridViewTextBoxColumn4.Name = "LimiteSuperior";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn4.Width = 70;
            gridViewTextBoxColumn4.WrapText = true;
            gridViewTextBoxColumn5.DataType = typeof(decimal);
            gridViewTextBoxColumn5.FieldName = "CuotaFija";
            gridViewTextBoxColumn5.HeaderText = "Cuota Fija";
            gridViewTextBoxColumn5.Name = "CuotaFija";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn5.Width = 70;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "PorcentajeExcedente";
            gridViewTextBoxColumn6.HeaderText = "% Excedente";
            gridViewTextBoxColumn6.Name = "PorcentajeExcedente";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 70;
            this.gridRangos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.gridRangos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridRangos.Name = "gridRangos";
            this.gridRangos.ShowGroupPanel = false;
            this.gridRangos.Size = new System.Drawing.Size(318, 321);
            this.gridRangos.TabIndex = 2;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.panelPeriodos);
            this.radSplitContainer1.Controls.Add(this.panelRangos);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 33);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(555, 321);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            // 
            // panelPeriodos
            // 
            this.panelPeriodos.Controls.Add(this.gridPeriodo);
            this.panelPeriodos.Location = new System.Drawing.Point(0, 0);
            this.panelPeriodos.Name = "panelPeriodos";
            // 
            // 
            // 
            this.panelPeriodos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.panelPeriodos.Size = new System.Drawing.Size(233, 321);
            this.panelPeriodos.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.07685664F, 0F);
            this.panelPeriodos.SizeInfo.SplitterCorrection = new System.Drawing.Size(-45, 0);
            this.panelPeriodos.TabIndex = 0;
            this.panelPeriodos.TabStop = false;
            this.panelPeriodos.Text = "splitPanel1";
            // 
            // panelRangos
            // 
            this.panelRangos.Controls.Add(this.gridRangos);
            this.panelRangos.Location = new System.Drawing.Point(237, 0);
            this.panelRangos.Name = "panelRangos";
            // 
            // 
            // 
            this.panelRangos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.panelRangos.Size = new System.Drawing.Size(318, 321);
            this.panelRangos.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.07685667F, 0F);
            this.panelRangos.SizeInfo.SplitterCorrection = new System.Drawing.Size(45, 0);
            this.panelRangos.TabIndex = 1;
            this.panelRangos.TabStop = false;
            this.panelRangos.Text = "splitPanel2";
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(555, 33);
            this.Encabezado.TabIndex = 190;
            this.Encabezado.TabStop = false;
            // 
            // TTabla_Guardar
            // 
            this.TTabla_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TTabla_Guardar.Location = new System.Drawing.Point(317, 366);
            this.TTabla_Guardar.Name = "TTabla_Guardar";
            this.TTabla_Guardar.Size = new System.Drawing.Size(110, 24);
            this.TTabla_Guardar.TabIndex = 191;
            this.TTabla_Guardar.Text = "Guardar";
            // 
            // TTabla_Cerrar
            // 
            this.TTabla_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TTabla_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TTabla_Cerrar.Location = new System.Drawing.Point(433, 366);
            this.TTabla_Cerrar.Name = "TTabla_Cerrar";
            this.TTabla_Cerrar.Size = new System.Drawing.Size(110, 24);
            this.TTabla_Cerrar.TabIndex = 192;
            this.TTabla_Cerrar.Text = "Cerrar";
            // 
            // TablaISRForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TTabla_Cerrar;
            this.ClientSize = new System.Drawing.Size(555, 402);
            this.Controls.Add(this.TTabla_Guardar);
            this.Controls.Add(this.TTabla_Cerrar);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.Encabezado);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TablaISRForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tabla Impuesto Sobre Renta (ISR)";
            this.Load += new System.EventHandler(this.TablaISR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPeriodo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRangos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRangos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelPeriodos)).EndInit();
            this.panelPeriodos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelRangos)).EndInit();
            this.panelRangos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TTabla_Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TTabla_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView gridPeriodo;
        private Telerik.WinControls.UI.RadGridView gridRangos;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel panelPeriodos;
        private Telerik.WinControls.UI.SplitPanel panelRangos;
        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadButton TTabla_Guardar;
        private Telerik.WinControls.UI.RadButton TTabla_Cerrar;
    }
}
