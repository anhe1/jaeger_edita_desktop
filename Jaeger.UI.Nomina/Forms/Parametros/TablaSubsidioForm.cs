﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    public partial class TablaSubsidioForm : RadForm {
        protected ISubsidioAlEmpleoService service;
        private BindingList<TablaSubsidioAlEmpleoModel> tablas;

        public TablaSubsidioForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void TablaSubsidio_Load(object sender, EventArgs e) {
            

            this.ToolBar.Actualizar.Click += this.ToolBar_Actualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_Cerrar_Click;

            this.ToolBar.Actualizar.PerformClick();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            //this.service = new SubsidioAlEmpleoService();
        }

        private void ToolBar_Agregar_Click(object sender, EventArgs e) {

        }

        private void ToolBar_Eliminar_Click(object sender, EventArgs e) {

        }

        private void ToolBar_Actualizar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            var _periodos = this.gridPeriodo.Columns["Periodo"] as GridViewComboBoxColumn;
            _periodos.DataSource = CommonService.GetPeriodos();
            this.CreateBinding();
        }

        private void ToolBar_Guardar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void ToolBar_Crear_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, "¿Esta seguro de crear la tabla? Esto acción eliminara toda la información almacenada,", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == System.Windows.Forms.DialogResult.No)
                return;
            using (var espera = new Waiting2Form(this.Crear)) {
                espera.Text = "Creando tablas, espere ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.gridPeriodo.DataSource = this.tablas;
            this.gridRangos.DataSource = this.tablas;
            this.gridRangos.DataMember = "Rangos";
        }

        private void Actualizar() {
            this.tablas = new BindingList<TablaSubsidioAlEmpleoModel>(this.service.GetList());
            if (this.tablas == null)
                this.tablas = new BindingList<TablaSubsidioAlEmpleoModel>();
        }

        private void Importar() {
            MessageBox.Show("No Implementado!");
            //var c = new Layout.Nomina.LayoutTablaSubsidio();
            //this.tablas = c.Import((string)this.ToolBarButtonImportar.Tag);
        }

        private void Guardar() {
            this.tablas = this.service.Save(this.tablas);
        }

        private void Crear() {
            //TODO falta implementar metodo de creacion de tablas
        }
    }
}
