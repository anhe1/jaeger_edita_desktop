﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    public partial class TablaSalarioMinimoForm : Telerik.WinControls.UI.RadForm {
        protected ISalarioMinimoService service;
        private BindingList<SalarioMinimoModel> tabla;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;
        public TablaSalarioMinimoForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void TablaSalarioMinimo_Load(object sender, EventArgs e) {
            

            this.ToolBar.Guardar.Click += this.ToolBar_Guardar_Click;
            this.ToolBar.Actualizar.Click += this.ToolBar_Actualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_Cerrar_Click;

            this.ToolBar.Actualizar.PerformClick();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            //this.service = new SalarioMinimoService();
        }

        private void ToolBar_Actualizar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void ToolBar_Guardar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.ToolBar.Actualizar.PerformClick();
        }

        private void ToolBar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Actualizar() {
            this.tabla = new BindingList<SalarioMinimoModel>(this.service.GetList());
        }

        private void Guardar() {
            this.tabla = this.service.Save(this.tabla);
        }

        private void CreateBinding() {
            this.gridData.DataSource = this.tabla;
        }
    }
}
