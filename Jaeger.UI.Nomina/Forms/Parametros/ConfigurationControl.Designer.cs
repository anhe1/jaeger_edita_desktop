﻿namespace Jaeger.UI.Nomina.Forms.Parametros {
    partial class ConfigurationControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.GBase = new Telerik.WinControls.UI.RadPageView();
            this.General = new Telerik.WinControls.UI.RadPageViewPage();
            this.OrtosPagos = new Telerik.WinControls.UI.RadTextBox();
            this.OtrosPagosALabel = new Telerik.WinControls.UI.RadLabel();
            this.OtrosPagosLabel = new Telerik.WinControls.UI.RadLabel();
            this.Deducciones = new Telerik.WinControls.UI.RadTextBox();
            this.DecuccionesALabel = new Telerik.WinControls.UI.RadLabel();
            this.DeduccionLabel = new Telerik.WinControls.UI.RadLabel();
            this.Percepciones = new Telerik.WinControls.UI.RadTextBox();
            this.PercepcionesALabel = new Telerik.WinControls.UI.RadLabel();
            this.PercepcionesLabel = new Telerik.WinControls.UI.RadLabel();
            this.Folder = new Telerik.WinControls.UI.RadTextBox();
            this.FolderALabel = new Telerik.WinControls.UI.RadLabel();
            this.FolderLabel = new Telerik.WinControls.UI.RadLabel();
            this.ModoProductivo = new Telerik.WinControls.UI.RadCheckBox();
            this.DataBase = new Telerik.WinControls.UI.RadPageViewPage();
            this.DataBaseGrid = new Telerik.WinControls.UI.RadPropertyGrid();
            ((System.ComponentModel.ISupportInitialize)(this.GBase)).BeginInit();
            this.GBase.SuspendLayout();
            this.General.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrtosPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtrosPagosALabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtrosPagosLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Deducciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DecuccionesALabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeduccionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Percepciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercepcionesALabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercepcionesLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FolderALabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FolderLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModoProductivo)).BeginInit();
            this.DataBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataBaseGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // GBase
            // 
            this.GBase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GBase.Controls.Add(this.General);
            this.GBase.Controls.Add(this.DataBase);
            this.GBase.DefaultPage = this.General;
            this.GBase.Location = new System.Drawing.Point(0, 0);
            this.GBase.Name = "GBase";
            this.GBase.SelectedPage = this.General;
            this.GBase.Size = new System.Drawing.Size(400, 416);
            this.GBase.TabIndex = 10;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.GBase.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // General
            // 
            this.General.Controls.Add(this.OrtosPagos);
            this.General.Controls.Add(this.OtrosPagosALabel);
            this.General.Controls.Add(this.OtrosPagosLabel);
            this.General.Controls.Add(this.Deducciones);
            this.General.Controls.Add(this.DecuccionesALabel);
            this.General.Controls.Add(this.DeduccionLabel);
            this.General.Controls.Add(this.Percepciones);
            this.General.Controls.Add(this.PercepcionesALabel);
            this.General.Controls.Add(this.PercepcionesLabel);
            this.General.Controls.Add(this.Folder);
            this.General.Controls.Add(this.FolderALabel);
            this.General.Controls.Add(this.FolderLabel);
            this.General.Controls.Add(this.ModoProductivo);
            this.General.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.General.Location = new System.Drawing.Point(10, 37);
            this.General.Name = "General";
            this.General.Size = new System.Drawing.Size(379, 368);
            this.General.Text = "General";
            // 
            // OrtosPagos
            // 
            this.OrtosPagos.Location = new System.Drawing.Point(139, 179);
            this.OrtosPagos.Name = "OrtosPagos";
            this.OrtosPagos.Size = new System.Drawing.Size(221, 20);
            this.OrtosPagos.TabIndex = 11;
            // 
            // OtrosPagosALabel
            // 
            this.OtrosPagosALabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic);
            this.OtrosPagosALabel.Location = new System.Drawing.Point(282, 205);
            this.OtrosPagosALabel.Name = "OtrosPagosALabel";
            this.OtrosPagosALabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.OtrosPagosALabel.Size = new System.Drawing.Size(76, 18);
            this.OtrosPagosALabel.TabIndex = 9;
            this.OtrosPagosALabel.Text = "Lista de claves";
            this.OtrosPagosALabel.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // OtrosPagosLabel
            // 
            this.OtrosPagosLabel.Location = new System.Drawing.Point(19, 180);
            this.OtrosPagosLabel.Name = "OtrosPagosLabel";
            this.OtrosPagosLabel.Size = new System.Drawing.Size(108, 18);
            this.OtrosPagosLabel.TabIndex = 10;
            this.OtrosPagosLabel.Text = "Ocultar otros pagos:";
            // 
            // Deducciones
            // 
            this.Deducciones.Location = new System.Drawing.Point(138, 131);
            this.Deducciones.Name = "Deducciones";
            this.Deducciones.Size = new System.Drawing.Size(221, 20);
            this.Deducciones.TabIndex = 8;
            // 
            // DecuccionesALabel
            // 
            this.DecuccionesALabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic);
            this.DecuccionesALabel.Location = new System.Drawing.Point(281, 157);
            this.DecuccionesALabel.Name = "DecuccionesALabel";
            this.DecuccionesALabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DecuccionesALabel.Size = new System.Drawing.Size(76, 18);
            this.DecuccionesALabel.TabIndex = 6;
            this.DecuccionesALabel.Text = "Lista de claves";
            this.DecuccionesALabel.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // DeduccionLabel
            // 
            this.DeduccionLabel.Location = new System.Drawing.Point(18, 132);
            this.DeduccionLabel.Name = "DeduccionLabel";
            this.DeduccionLabel.Size = new System.Drawing.Size(111, 18);
            this.DeduccionLabel.TabIndex = 7;
            this.DeduccionLabel.Text = "Ocultar deducciones:";
            // 
            // Percepciones
            // 
            this.Percepciones.Location = new System.Drawing.Point(138, 81);
            this.Percepciones.Name = "Percepciones";
            this.Percepciones.Size = new System.Drawing.Size(221, 20);
            this.Percepciones.TabIndex = 5;
            // 
            // PercepcionesALabel
            // 
            this.PercepcionesALabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic);
            this.PercepcionesALabel.Location = new System.Drawing.Point(281, 107);
            this.PercepcionesALabel.Name = "PercepcionesALabel";
            this.PercepcionesALabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PercepcionesALabel.Size = new System.Drawing.Size(76, 18);
            this.PercepcionesALabel.TabIndex = 3;
            this.PercepcionesALabel.Text = "Lista de claves";
            this.PercepcionesALabel.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // PercepcionesLabel
            // 
            this.PercepcionesLabel.Location = new System.Drawing.Point(18, 82);
            this.PercepcionesLabel.Name = "PercepcionesLabel";
            this.PercepcionesLabel.Size = new System.Drawing.Size(114, 18);
            this.PercepcionesLabel.TabIndex = 4;
            this.PercepcionesLabel.Text = "Ocultar percepciones:";
            // 
            // Folder
            // 
            this.Folder.Location = new System.Drawing.Point(67, 31);
            this.Folder.Name = "Folder";
            this.Folder.Size = new System.Drawing.Size(292, 20);
            this.Folder.TabIndex = 2;
            // 
            // FolderALabel
            // 
            this.FolderALabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic);
            this.FolderALabel.Location = new System.Drawing.Point(153, 57);
            this.FolderALabel.Name = "FolderALabel";
            this.FolderALabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FolderALabel.Size = new System.Drawing.Size(206, 18);
            this.FolderALabel.TabIndex = 1;
            this.FolderALabel.Text = "Ruta del folder utilizado en bucket/folder";
            this.FolderALabel.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // FolderLabel
            // 
            this.FolderLabel.Location = new System.Drawing.Point(18, 32);
            this.FolderLabel.Name = "FolderLabel";
            this.FolderLabel.Size = new System.Drawing.Size(43, 18);
            this.FolderLabel.TabIndex = 1;
            this.FolderLabel.Text = "Folder: ";
            // 
            // ModoProductivo
            // 
            this.ModoProductivo.Location = new System.Drawing.Point(19, 248);
            this.ModoProductivo.Name = "ModoProductivo";
            this.ModoProductivo.Size = new System.Drawing.Size(108, 18);
            this.ModoProductivo.TabIndex = 0;
            this.ModoProductivo.Text = "Modo productivo";
            // 
            // DataBase
            // 
            this.DataBase.Controls.Add(this.DataBaseGrid);
            this.DataBase.ItemSize = new System.Drawing.SizeF(86F, 28F);
            this.DataBase.Location = new System.Drawing.Point(10, 37);
            this.DataBase.Name = "DataBase";
            this.DataBase.Size = new System.Drawing.Size(379, 353);
            this.DataBase.Text = "Base de datos";
            // 
            // DataBaseGrid
            // 
            this.DataBaseGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataBaseGrid.Location = new System.Drawing.Point(0, 0);
            this.DataBaseGrid.Name = "DataBaseGrid";
            this.DataBaseGrid.Size = new System.Drawing.Size(379, 353);
            this.DataBaseGrid.TabIndex = 6;
            // 
            // ConfigurationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GBase);
            this.MinimumSize = new System.Drawing.Size(400, 416);
            this.Name = "ConfigurationControl";
            this.Size = new System.Drawing.Size(400, 416);
            ((System.ComponentModel.ISupportInitialize)(this.GBase)).EndInit();
            this.GBase.ResumeLayout(false);
            this.General.ResumeLayout(false);
            this.General.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrtosPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtrosPagosALabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtrosPagosLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Deducciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DecuccionesALabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeduccionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Percepciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercepcionesALabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercepcionesLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FolderALabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FolderLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModoProductivo)).EndInit();
            this.DataBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataBaseGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView GBase;
        private Telerik.WinControls.UI.RadPageViewPage General;
        protected internal Telerik.WinControls.UI.RadTextBox OrtosPagos;
        private Telerik.WinControls.UI.RadLabel OtrosPagosALabel;
        private Telerik.WinControls.UI.RadLabel OtrosPagosLabel;
        protected internal Telerik.WinControls.UI.RadTextBox Deducciones;
        private Telerik.WinControls.UI.RadLabel DecuccionesALabel;
        private Telerik.WinControls.UI.RadLabel DeduccionLabel;
        protected internal Telerik.WinControls.UI.RadTextBox Percepciones;
        private Telerik.WinControls.UI.RadLabel PercepcionesALabel;
        private Telerik.WinControls.UI.RadLabel PercepcionesLabel;
        protected internal Telerik.WinControls.UI.RadTextBox Folder;
        private Telerik.WinControls.UI.RadLabel FolderALabel;
        private Telerik.WinControls.UI.RadLabel FolderLabel;
        protected internal Telerik.WinControls.UI.RadCheckBox ModoProductivo;
        private Telerik.WinControls.UI.RadPageViewPage DataBase;
        protected internal Telerik.WinControls.UI.RadPropertyGrid DataBaseGrid;
    }
}
