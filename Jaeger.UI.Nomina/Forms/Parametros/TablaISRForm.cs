﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    public partial class TablaISRForm : RadForm {
        #region declaraciones
        protected ITablaISRService service;
        private BindingList<TablaImpuestoSobreRentaModel> tabla;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;
        #endregion

        public TablaISRForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void TablaISR_Load(object sender, EventArgs e) {
            this.TTabla_Guardar.Click += this.TTabla_Guardar_Click;
            this.TTabla_Cerrar.Click += this.TTabla_Cerrar_Click;
            this.TTabla_Actualizar_Click(sender, e);
        }

        private void TTabla_Actualizar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            var _periodos = this.gridPeriodo.Columns["Periodo"] as GridViewComboBoxColumn;
            _periodos.DataSource = CommonService.GetPeriodos();
            this.CreateBinding();
        }

        private void TTabla_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void TTabla_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.gridPeriodo.DataSource = this.tabla;
            this.gridRangos.DataSource = this.tabla;
            this.gridRangos.DataMember = "Rangos";
        }

        private void Actualizar() {
            this.tabla = new BindingList<TablaImpuestoSobreRentaModel>(this.service.GetList());
            if (this.tabla == null)
                this.tabla = new BindingList<TablaImpuestoSobreRentaModel>();
        }

        private void Guardar() {
            this.tabla = this.service.Save(this.tabla);
        }
    }
}
