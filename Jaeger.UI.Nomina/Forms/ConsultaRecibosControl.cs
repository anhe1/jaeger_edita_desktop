﻿using Jaeger.Domain.Nomina.Entities;
using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Forms {
    public partial class ConsultaRecibosControl : UserControl {
        public ConsultaRecibosControl() {
            InitializeComponent();
        }

        private void ConsultaRecibosControl_Load(object sender, EventArgs e) {
            this.Empleados.Enabled = this.PorEmpleado.Checked;
            this.Departamentos.Enabled = this.PorDepartamento.Checked;

            this.Nominas.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nominas.EditorControl.MasterTemplate.AllowRowResize = false;
            this.Nominas.EditorControl.TableElement.Size = new Size(300, 200);
            this.Nominas.EditorControl.AllowRowResize = false;
            this.Nominas.EditorControl.AllowSearchRow = true;

            this.Nominas.AutoSizeDropDownToBestFit = true;
            this.Nominas.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.Nominas.MultiColumnComboBoxElement.DropDownAnimationEnabled = true;
        }

        private void CheckPorNomina_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.Nominas.Enabled = this.PorNomina.IsChecked;
            this.PorDepartamento.Enabled = this.PorNomina.IsChecked;
            this.PorEmpleado.Enabled = this.PorNomina.IsChecked;
        }

        private void CheckPorEmpleado_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.Empleados.Enabled = this.PorEmpleado.Checked;
        }

        private void CheckPorDepartamento_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.Departamentos.Enabled = this.PorDepartamento.Checked;
        }

        private void PorRangoFechas_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.FechaInicial.Enabled = this.PorRangoFechas.IsChecked;
            this.FechaFinal.Enabled = this.PorRangoFechas.IsChecked;
        }

        private void PorIdDocumento_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.IdDocumento.Enabled = this.PorIdDocumento.IsChecked;
            this.PorDepartamento.Enabled = !this.PorIdDocumento.IsChecked;
            this.PorEmpleado.Enabled = !this.PorIdDocumento.IsChecked;
            this.PorDepartamento.Checked = false;
            this.PorEmpleado.Checked = false;
        }

        public int GetIdNomina() {
            if (this.Nominas.SelectedValue != null) {
                var seleccionado = ((GridViewDataRowInfo)this.Nominas.SelectedItem).DataBoundItem as ControlNominaModel;
                if (seleccionado != null) {
                    return seleccionado.IdControl;
                }
            }
            return -1;
        }
    }
}
