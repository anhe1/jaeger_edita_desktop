﻿
namespace Jaeger.UI.Nomina.Forms {
    partial class ResumenNominaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GridData = new Telerik.WinControls.UI.RadPivotGrid();
            this.PanelIzquierdo = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.GrupoBusqueda = new Telerik.WinControls.UI.RadGroupBox();
            this.buttonExportar = new Telerik.WinControls.UI.RadButton();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.ButtonBuscar = new Telerik.WinControls.UI.RadButton();
            this.CboEmpleados = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CheckPorEmpleado = new Telerik.WinControls.UI.RadCheckBox();
            this.CboDepartamentos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CheckPorDepartamento = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ToolBarButtonPeriodo = new Telerik.WinControls.UI.RadDropDownList();
            this.radioPorNomina = new Telerik.WinControls.UI.RadRadioButton();
            this.radioPorPeriodo = new Telerik.WinControls.UI.RadRadioButton();
            this.cboNominas = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).BeginInit();
            this.PanelIzquierdo.PanelContainer.SuspendLayout();
            this.PanelIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).BeginInit();
            this.GrupoBusqueda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonExportar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToolBarButtonPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioPorNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioPorPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNominas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNominas.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNominas.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(375, 0);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(815, 681);
            this.GridData.TabIndex = 1;
            this.GridData.DataProviderChanged += new Telerik.WinControls.UI.DataProviderChangedEventHandler(this.GridData_DataProviderChanged);
            // 
            // PanelIzquierdo
            // 
            this.PanelIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelIzquierdo.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.PanelIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.PanelIzquierdo.Name = "PanelIzquierdo";
            this.PanelIzquierdo.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 150, 648);
            // 
            // PanelIzquierdo.PanelContainer
            // 
            this.PanelIzquierdo.PanelContainer.Controls.Add(this.GrupoBusqueda);
            this.PanelIzquierdo.PanelContainer.Size = new System.Drawing.Size(347, 679);
            this.PanelIzquierdo.Size = new System.Drawing.Size(375, 681);
            this.PanelIzquierdo.TabIndex = 2;
            // 
            // GrupoBusqueda
            // 
            this.GrupoBusqueda.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GrupoBusqueda.Controls.Add(this.buttonExportar);
            this.GrupoBusqueda.Controls.Add(this.Ejercicio);
            this.GrupoBusqueda.Controls.Add(this.ButtonBuscar);
            this.GrupoBusqueda.Controls.Add(this.CboEmpleados);
            this.GrupoBusqueda.Controls.Add(this.CheckPorEmpleado);
            this.GrupoBusqueda.Controls.Add(this.CboDepartamentos);
            this.GrupoBusqueda.Controls.Add(this.CheckPorDepartamento);
            this.GrupoBusqueda.Controls.Add(this.radLabel3);
            this.GrupoBusqueda.Controls.Add(this.radLabel1);
            this.GrupoBusqueda.Controls.Add(this.ToolBarButtonPeriodo);
            this.GrupoBusqueda.Controls.Add(this.radioPorNomina);
            this.GrupoBusqueda.Controls.Add(this.radioPorPeriodo);
            this.GrupoBusqueda.Controls.Add(this.cboNominas);
            this.GrupoBusqueda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrupoBusqueda.HeaderText = "Resumen por:";
            this.GrupoBusqueda.Location = new System.Drawing.Point(0, 0);
            this.GrupoBusqueda.Name = "GrupoBusqueda";
            this.GrupoBusqueda.Size = new System.Drawing.Size(347, 679);
            this.GrupoBusqueda.TabIndex = 0;
            this.GrupoBusqueda.Text = "Resumen por:";
            // 
            // buttonExportar
            // 
            this.buttonExportar.Enabled = false;
            this.buttonExportar.Location = new System.Drawing.Point(210, 270);
            this.buttonExportar.Name = "buttonExportar";
            this.buttonExportar.Size = new System.Drawing.Size(110, 24);
            this.buttonExportar.TabIndex = 16;
            this.buttonExportar.Text = "Exportar";
            this.buttonExportar.Click += new System.EventHandler(this.BExportar_Click);
            // 
            // Ejercicio
            // 
            this.Ejercicio.Enabled = false;
            this.Ejercicio.Location = new System.Drawing.Point(256, 104);
            this.Ejercicio.Maximum = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.Ejercicio.Minimum = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.NullableValue = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.Ejercicio.Size = new System.Drawing.Size(64, 20);
            this.Ejercicio.TabIndex = 0;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ejercicio.Value = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            // 
            // ButtonBuscar
            // 
            this.ButtonBuscar.Location = new System.Drawing.Point(210, 240);
            this.ButtonBuscar.Name = "ButtonBuscar";
            this.ButtonBuscar.Size = new System.Drawing.Size(110, 24);
            this.ButtonBuscar.TabIndex = 15;
            this.ButtonBuscar.Text = "Buscar";
            this.ButtonBuscar.Click += new System.EventHandler(this.BActualizar_Click);
            // 
            // CboEmpleados
            // 
            // 
            // CboEmpleados.NestedRadGridView
            // 
            this.CboEmpleados.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboEmpleados.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmpleados.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboEmpleados.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboEmpleados.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboEmpleados.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboEmpleados.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboEmpleados.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboEmpleados.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboEmpleados.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboEmpleados.EditorControl.Name = "NestedRadGridView";
            this.CboEmpleados.EditorControl.ReadOnly = true;
            this.CboEmpleados.EditorControl.ShowGroupPanel = false;
            this.CboEmpleados.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboEmpleados.EditorControl.TabIndex = 0;
            this.CboEmpleados.Enabled = false;
            this.CboEmpleados.Location = new System.Drawing.Point(14, 206);
            this.CboEmpleados.Name = "CboEmpleados";
            this.CboEmpleados.NullText = "Empleado";
            this.CboEmpleados.Size = new System.Drawing.Size(306, 20);
            this.CboEmpleados.TabIndex = 14;
            this.CboEmpleados.TabStop = false;
            // 
            // CheckPorEmpleado
            // 
            this.CheckPorEmpleado.Enabled = false;
            this.CheckPorEmpleado.Location = new System.Drawing.Point(14, 182);
            this.CheckPorEmpleado.Name = "CheckPorEmpleado";
            this.CheckPorEmpleado.Size = new System.Drawing.Size(90, 18);
            this.CheckPorEmpleado.TabIndex = 13;
            this.CheckPorEmpleado.Text = "Por Empleado";
            // 
            // CboDepartamentos
            // 
            this.CboDepartamentos.DisplayMember = "Descripcion";
            // 
            // CboDepartamentos.NestedRadGridView
            // 
            this.CboDepartamentos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboDepartamentos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboDepartamentos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboDepartamentos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboDepartamentos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboDepartamentos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CboDepartamentos.EditorControl.Name = "NestedRadGridView";
            this.CboDepartamentos.EditorControl.ReadOnly = true;
            this.CboDepartamentos.EditorControl.ShowGroupPanel = false;
            this.CboDepartamentos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboDepartamentos.EditorControl.TabIndex = 0;
            this.CboDepartamentos.Enabled = false;
            this.CboDepartamentos.Location = new System.Drawing.Point(14, 156);
            this.CboDepartamentos.Name = "CboDepartamentos";
            this.CboDepartamentos.NullText = "Departamento";
            this.CboDepartamentos.Size = new System.Drawing.Size(306, 20);
            this.CboDepartamentos.TabIndex = 12;
            this.CboDepartamentos.TabStop = false;
            this.CboDepartamentos.ValueMember = "IdControl";
            // 
            // CheckPorDepartamento
            // 
            this.CheckPorDepartamento.Enabled = false;
            this.CheckPorDepartamento.Location = new System.Drawing.Point(14, 132);
            this.CheckPorDepartamento.Name = "CheckPorDepartamento";
            this.CheckPorDepartamento.Size = new System.Drawing.Size(113, 18);
            this.CheckPorDepartamento.TabIndex = 11;
            this.CheckPorDepartamento.Text = "Por Departamento";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(200, 105);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(50, 18);
            this.radLabel3.TabIndex = 10;
            this.radLabel3.Text = "Ejercicio:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(14, 105);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(48, 18);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "Periodo:";
            // 
            // ToolBarButtonPeriodo
            // 
            this.ToolBarButtonPeriodo.Enabled = false;
            this.ToolBarButtonPeriodo.Location = new System.Drawing.Point(68, 104);
            this.ToolBarButtonPeriodo.Name = "ToolBarButtonPeriodo";
            this.ToolBarButtonPeriodo.Size = new System.Drawing.Size(110, 20);
            this.ToolBarButtonPeriodo.TabIndex = 5;
            this.ToolBarButtonPeriodo.Text = "Selecciona";
            // 
            // radioPorNomina
            // 
            this.radioPorNomina.Location = new System.Drawing.Point(14, 29);
            this.radioPorNomina.Name = "radioPorNomina";
            this.radioPorNomina.Size = new System.Drawing.Size(80, 18);
            this.radioPorNomina.TabIndex = 4;
            this.radioPorNomina.Text = "Por Nómina";
            this.radioPorNomina.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Options_ToggleStateChanged);
            // 
            // radioPorPeriodo
            // 
            this.radioPorPeriodo.Location = new System.Drawing.Point(14, 80);
            this.radioPorPeriodo.Name = "radioPorPeriodo";
            this.radioPorPeriodo.Size = new System.Drawing.Size(84, 18);
            this.radioPorPeriodo.TabIndex = 4;
            this.radioPorPeriodo.Text = "Por periodos";
            this.radioPorPeriodo.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Options_ToggleStateChanged);
            // 
            // cboNominas
            // 
            this.cboNominas.DisplayMember = "Descripcion";
            // 
            // cboNominas.NestedRadGridView
            // 
            this.cboNominas.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboNominas.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNominas.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboNominas.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboNominas.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboNominas.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboNominas.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "IdControl";
            gridViewTextBoxColumn1.HeaderText = "Núm.";
            gridViewTextBoxColumn1.Name = "NoControl";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 260;
            gridViewTextBoxColumn3.FieldName = "TipoNomina";
            gridViewTextBoxColumn3.HeaderText = "Tipo";
            gridViewTextBoxColumn3.Name = "ClaveTipoNomina";
            gridViewTextBoxColumn4.FieldName = "FechaSubida";
            gridViewTextBoxColumn4.FormatString = "{0:d}";
            gridViewTextBoxColumn4.HeaderText = "Fecha";
            gridViewTextBoxColumn4.Name = "FechaNomina";
            gridViewTextBoxColumn4.Width = 85;
            this.cboNominas.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.cboNominas.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboNominas.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboNominas.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.cboNominas.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.cboNominas.EditorControl.Name = "NestedRadGridView";
            this.cboNominas.EditorControl.ReadOnly = true;
            this.cboNominas.EditorControl.ShowGroupPanel = false;
            this.cboNominas.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboNominas.EditorControl.TabIndex = 0;
            this.cboNominas.Enabled = false;
            this.cboNominas.Location = new System.Drawing.Point(14, 53);
            this.cboNominas.Name = "cboNominas";
            this.cboNominas.NullText = "Selecciona";
            this.cboNominas.Size = new System.Drawing.Size(306, 20);
            this.cboNominas.TabIndex = 1;
            this.cboNominas.TabStop = false;
            this.cboNominas.ValueMember = "IdControl";
            // 
            // ResumenNominaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 681);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.PanelIzquierdo);
            this.Name = "ResumenNominaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Nomina: Resumen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ResumenNominaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.PanelIzquierdo.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).EndInit();
            this.PanelIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).EndInit();
            this.GrupoBusqueda.ResumeLayout(false);
            this.GrupoBusqueda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonExportar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboDepartamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPorDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToolBarButtonPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioPorNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioPorPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNominas.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNominas.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboNominas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPivotGrid GridData;
        private Telerik.WinControls.UI.RadCollapsiblePanel PanelIzquierdo;
        private Telerik.WinControls.UI.RadGroupBox GrupoBusqueda;
        private Telerik.WinControls.UI.RadButton ButtonBuscar;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboEmpleados;
        private Telerik.WinControls.UI.RadCheckBox CheckPorEmpleado;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboDepartamentos;
        private Telerik.WinControls.UI.RadCheckBox CheckPorDepartamento;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList ToolBarButtonPeriodo;
        private Telerik.WinControls.UI.RadRadioButton radioPorPeriodo;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cboNominas;
        private Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton buttonExportar;
        private Telerik.WinControls.UI.RadRadioButton radioPorNomina;
    }
}