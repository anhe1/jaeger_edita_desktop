﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class PuestoForm : RadForm {
        protected Aplication.Nomina.Contracts.IPuestoService Service;
        protected PuestoModel Current;

        public PuestoForm(Aplication.Nomina.Contracts.IPuestoService service, PuestoModel model) {
            InitializeComponent();
            Service = service;
            this.Current = model;
        }

        private void PuestoForm_Load(object sender, EventArgs e) {
            if (this.Current == null) {
                this.Current = new PuestoModel();
            }
            this.CreateBinding();
        }

        private void CreateBinding() {
            this.Descripcion.DataBindings.Add("Text", this.Current, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.IsActivo.DataBindings.Add("Checked", this.Current, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Departamento.DataBindings.Add("SelectedValue", this.Current, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CuentaContable.DataBindings.Add("Text", this.Current, "CtaContable", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void TPuesto_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) { return; }
            using(var espera = new UI.Common.Forms.Waiting2Form(this.Guardar)) {
                espera.ShowDialog(this);
            }
        }

        private void TPuesto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private bool Validar() {
            this.errorProvider1.Clear();
            if (string.IsNullOrEmpty(this.Current.Descripcion)) {
                this.errorProvider1.SetError(this.Descripcion, "Es necesaria una descripción");
                return false;
            }
            return false;
        }
        public void Guardar() {
            this.Service.Save(this.Current);
        }
    }
}
