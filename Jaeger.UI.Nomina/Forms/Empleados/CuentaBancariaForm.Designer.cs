﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class CuentaBancariaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuentaBancariaForm));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.NumCuenta = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Clabe = new Telerik.WinControls.UI.RadTextBox();
            this.CtaContable = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.NumCliente = new Telerik.WinControls.UI.RadTextBox();
            this.ClaveBanco = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.Sucursal = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.BeneficiarioRFC = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.Activo = new Telerik.WinControls.UI.RadCheckBox();
            this.Beneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.FechaApertura = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.BancoExtranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.errorCuentaBancaria = new System.Windows.Forms.ErrorProvider(this.components);
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Moneda = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.TCuenta = new Telerik.WinControls.UI.RadDropDownList();
            this.Verificado = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.bCerrar = new Telerik.WinControls.UI.RadButton();
            this.bGuardar = new Telerik.WinControls.UI.RadButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clabe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CtaContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeneficiarioRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaApertura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoExtranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorCuentaBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Verificado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bGuardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(16, 78);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(74, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Núm. Cuenta:";
            // 
            // NumCuenta
            // 
            this.NumCuenta.Location = new System.Drawing.Point(110, 77);
            this.NumCuenta.MaxLength = 20;
            this.NumCuenta.Name = "NumCuenta";
            this.NumCuenta.Size = new System.Drawing.Size(149, 20);
            this.NumCuenta.TabIndex = 3;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(273, 77);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(41, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "CLABE:";
            // 
            // Clabe
            // 
            this.Clabe.Location = new System.Drawing.Point(320, 75);
            this.Clabe.MaxLength = 18;
            this.Clabe.Name = "Clabe";
            this.Clabe.Size = new System.Drawing.Size(149, 20);
            this.Clabe.TabIndex = 5;
            this.Clabe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbFolio_KeyPress);
            // 
            // CtaContable
            // 
            this.CtaContable.Location = new System.Drawing.Point(110, 157);
            this.CtaContable.MaxLength = 40;
            this.CtaContable.Name = "CtaContable";
            this.CtaContable.Size = new System.Drawing.Size(149, 20);
            this.CtaContable.TabIndex = 7;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(16, 104);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(73, 18);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.Text = "Núm. Cliente:";
            // 
            // NumCliente
            // 
            this.NumCliente.Location = new System.Drawing.Point(110, 103);
            this.NumCliente.MaxLength = 14;
            this.NumCliente.Name = "NumCliente";
            this.NumCliente.Size = new System.Drawing.Size(149, 20);
            this.NumCliente.TabIndex = 9;
            // 
            // ClaveBanco
            // 
            this.ClaveBanco.AutoSizeDropDownToBestFit = true;
            this.ClaveBanco.DisplayMember = "Descripcion";
            this.ClaveBanco.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ClaveBanco.NestedRadGridView
            // 
            this.ClaveBanco.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ClaveBanco.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClaveBanco.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClaveBanco.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ClaveBanco.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ClaveBanco.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ClaveBanco.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 200;
            this.ClaveBanco.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.ClaveBanco.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ClaveBanco.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ClaveBanco.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.ClaveBanco.EditorControl.Name = "NestedRadGridView";
            this.ClaveBanco.EditorControl.ReadOnly = true;
            this.ClaveBanco.EditorControl.ShowGroupPanel = false;
            this.ClaveBanco.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ClaveBanco.EditorControl.TabIndex = 0;
            this.ClaveBanco.Location = new System.Drawing.Point(110, 129);
            this.ClaveBanco.Name = "ClaveBanco";
            this.ClaveBanco.NullText = "Selecciona";
            this.ClaveBanco.Size = new System.Drawing.Size(395, 20);
            this.ClaveBanco.TabIndex = 3;
            this.ClaveBanco.TabStop = false;
            this.ClaveBanco.ValueMember = "Clave";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(270, 104);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(50, 18);
            this.radLabel8.TabIndex = 1;
            this.radLabel8.Text = "Sucursal:";
            // 
            // Sucursal
            // 
            this.Sucursal.Location = new System.Drawing.Point(326, 103);
            this.Sucursal.MaxLength = 20;
            this.Sucursal.Name = "Sucursal";
            this.Sucursal.Size = new System.Drawing.Size(87, 20);
            this.Sucursal.TabIndex = 2;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(491, 159);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(63, 18);
            this.radLabel11.TabIndex = 7;
            this.radLabel11.Text = "Autorizada:";
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(452, 104);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(50, 18);
            this.radLabel13.TabIndex = 9;
            this.radLabel13.Text = "Moneda:";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(16, 130);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(39, 18);
            this.radLabel15.TabIndex = 9;
            this.radLabel15.Text = "Banco:";
            // 
            // BeneficiarioRFC
            // 
            this.BeneficiarioRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BeneficiarioRFC.Location = new System.Drawing.Point(499, 52);
            this.BeneficiarioRFC.MaxLength = 14;
            this.BeneficiarioRFC.Name = "BeneficiarioRFC";
            this.BeneficiarioRFC.NullText = "RFC";
            this.BeneficiarioRFC.ShowClearButton = true;
            this.BeneficiarioRFC.Size = new System.Drawing.Size(112, 20);
            this.BeneficiarioRFC.TabIndex = 2;
            this.BeneficiarioRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(465, 52);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(28, 18);
            this.radLabel17.TabIndex = 9;
            this.radLabel17.Text = "RFC:";
            // 
            // Activo
            // 
            this.Activo.Location = new System.Drawing.Point(499, 28);
            this.Activo.Name = "Activo";
            this.Activo.Size = new System.Drawing.Size(51, 18);
            this.Activo.TabIndex = 11;
            this.Activo.Text = "Activo";
            // 
            // Beneficiario
            // 
            this.Beneficiario.Location = new System.Drawing.Point(110, 51);
            this.Beneficiario.MaxLength = 256;
            this.Beneficiario.Name = "Beneficiario";
            this.Beneficiario.Size = new System.Drawing.Size(342, 20);
            this.Beneficiario.TabIndex = 0;
            // 
            // FechaApertura
            // 
            this.FechaApertura.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaApertura.Location = new System.Drawing.Point(393, 158);
            this.FechaApertura.Name = "FechaApertura";
            this.FechaApertura.Size = new System.Drawing.Size(92, 20);
            this.FechaApertura.TabIndex = 4;
            this.FechaApertura.TabStop = false;
            this.FechaApertura.Text = "09/12/2020";
            this.FechaApertura.Value = new System.DateTime(2020, 12, 9, 15, 3, 16, 921);
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(16, 52);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(67, 18);
            this.radLabel16.TabIndex = 9;
            this.radLabel16.Text = "Beneficiario:";
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(303, 159);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(82, 18);
            this.radLabel14.TabIndex = 1;
            this.radLabel14.Text = "Fecha Registro:";
            // 
            // BancoExtranjero
            // 
            this.BancoExtranjero.Location = new System.Drawing.Point(511, 129);
            this.BancoExtranjero.Name = "BancoExtranjero";
            this.BancoExtranjero.Size = new System.Drawing.Size(104, 18);
            this.BancoExtranjero.TabIndex = 15;
            this.BancoExtranjero.Text = "Banco Extranjero";
            // 
            // errorCuentaBancaria
            // 
            this.errorCuentaBancaria.ContainerControl = this;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.Moneda);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.BancoExtranjero);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.TCuenta);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.Verificado);
            this.radGroupBox1.Controls.Add(this.Sucursal);
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.BeneficiarioRFC);
            this.radGroupBox1.Controls.Add(this.ClaveBanco);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.NumCuenta);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.Clabe);
            this.radGroupBox1.Controls.Add(this.Activo);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.NumCliente);
            this.radGroupBox1.Controls.Add(this.CtaContable);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.FechaApertura);
            this.radGroupBox1.Controls.Add(this.Beneficiario);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Datos de la cuenta";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 44);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(642, 197);
            this.radGroupBox1.TabIndex = 16;
            this.radGroupBox1.Text = "Datos de la cuenta";
            // 
            // Moneda
            // 
            this.Moneda.DisplayMember = "Descripcion";
            this.Moneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Moneda.Location = new System.Drawing.Point(508, 103);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(103, 20);
            this.Moneda.TabIndex = 112;
            this.Moneda.ValueMember = "Id";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(16, 26);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(55, 18);
            this.radLabel4.TabIndex = 11;
            this.radLabel4.Text = "T. Cuenta:";
            // 
            // TCuenta
            // 
            this.TCuenta.DisplayMember = "Descripcion";
            this.TCuenta.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TCuenta.Location = new System.Drawing.Point(110, 25);
            this.TCuenta.Name = "TCuenta";
            this.TCuenta.Size = new System.Drawing.Size(175, 20);
            this.TCuenta.TabIndex = 12;
            this.TCuenta.ValueMember = "Id";
            // 
            // Verificado
            // 
            this.Verificado.Location = new System.Drawing.Point(561, 158);
            this.Verificado.Name = "Verificado";
            this.Verificado.Size = new System.Drawing.Size(50, 20);
            this.Verificado.TabIndex = 111;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 158);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(92, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "Cuenta Contable:";
            // 
            // bCerrar
            // 
            this.bCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCerrar.Location = new System.Drawing.Point(520, 252);
            this.bCerrar.Name = "bCerrar";
            this.bCerrar.Size = new System.Drawing.Size(110, 24);
            this.bCerrar.TabIndex = 17;
            this.bCerrar.Text = "&Cerrar";
            this.bCerrar.Click += new System.EventHandler(this.bCerrar_Click);
            // 
            // bGuardar
            // 
            this.bGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGuardar.Location = new System.Drawing.Point(404, 252);
            this.bGuardar.Name = "bGuardar";
            this.bGuardar.Size = new System.Drawing.Size(110, 24);
            this.bGuardar.TabIndex = 17;
            this.bGuardar.Text = "Guardar";
            this.bGuardar.Click += new System.EventHandler(this.bGuardar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(642, 44);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // CuentaBancariaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bCerrar;
            this.ClientSize = new System.Drawing.Size(642, 288);
            this.Controls.Add(this.bGuardar);
            this.Controls.Add(this.bCerrar);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CuentaBancariaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cuenta Bancaria";
            this.Load += new System.EventHandler(this.CuentaBancariaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clabe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CtaContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeneficiarioRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaApertura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoExtranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorCuentaBancaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Verificado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bGuardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox NumCuenta;
        private Telerik.WinControls.UI.RadTextBox Clabe;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox CtaContable;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox NumCliente;
        private Telerik.WinControls.UI.RadMultiColumnComboBox ClaveBanco;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox Sucursal;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadCheckBox Activo;
        private Telerik.WinControls.UI.RadTextBox Beneficiario;
        private Telerik.WinControls.UI.RadDateTimePicker FechaApertura;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadCheckBox BancoExtranjero;
        private System.Windows.Forms.ErrorProvider errorCuentaBancaria;
        private Telerik.WinControls.UI.RadTextBox BeneficiarioRFC;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadToggleSwitch Verificado;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList TCuenta;
        private Telerik.WinControls.UI.RadDropDownList Moneda;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton bGuardar;
        private Telerik.WinControls.UI.RadButton bCerrar;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
