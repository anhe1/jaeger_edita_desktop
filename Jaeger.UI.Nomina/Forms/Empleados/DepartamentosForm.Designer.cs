﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class DepartamentosForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TDepartamento = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TDepartamento
            // 
            this.TDepartamento.Caption = "";
            this.TDepartamento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TDepartamento.Location = new System.Drawing.Point(0, 0);
            this.TDepartamento.Name = "TDepartamento";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TDepartamento.Permisos = uiAction1;
            this.TDepartamento.ReadOnly = false;
            this.TDepartamento.ShowActualizar = true;
            this.TDepartamento.ShowAutorizar = false;
            this.TDepartamento.ShowAutosuma = false;
            this.TDepartamento.ShowCerrar = true;
            this.TDepartamento.ShowEditar = true;
            this.TDepartamento.ShowExportarExcel = false;
            this.TDepartamento.ShowFiltro = true;
            this.TDepartamento.ShowHerramientas = false;
            this.TDepartamento.ShowImagen = false;
            this.TDepartamento.ShowImprimir = false;
            this.TDepartamento.ShowItem = false;
            this.TDepartamento.ShowNuevo = true;
            this.TDepartamento.ShowRemover = true;
            this.TDepartamento.ShowSeleccionMultiple = true;
            this.TDepartamento.Size = new System.Drawing.Size(800, 450);
            this.TDepartamento.TabIndex = 0;
            // 
            // DepartamentosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TDepartamento);
            this.Name = "DepartamentosForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Departamentos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DepartamentosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Common.Forms.GridStandarControl TDepartamento;
    }
}