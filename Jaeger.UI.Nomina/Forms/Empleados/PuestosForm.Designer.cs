﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class PuestosForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TPuesto = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TPuesto
            // 
            this.TPuesto.Caption = "";
            this.TPuesto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TPuesto.Location = new System.Drawing.Point(0, 0);
            this.TPuesto.Name = "TPuesto";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TPuesto.Permisos = uiAction1;
            this.TPuesto.ReadOnly = false;
            this.TPuesto.ShowActualizar = true;
            this.TPuesto.ShowAutorizar = false;
            this.TPuesto.ShowAutosuma = false;
            this.TPuesto.ShowCerrar = true;
            this.TPuesto.ShowEditar = true;
            this.TPuesto.ShowExportarExcel = false;
            this.TPuesto.ShowFiltro = true;
            this.TPuesto.ShowHerramientas = false;
            this.TPuesto.ShowImagen = false;
            this.TPuesto.ShowImprimir = false;
            this.TPuesto.ShowItem = false;
            this.TPuesto.ShowNuevo = true;
            this.TPuesto.ShowRemover = true;
            this.TPuesto.ShowSeleccionMultiple = true;
            this.TPuesto.Size = new System.Drawing.Size(800, 450);
            this.TPuesto.TabIndex = 0;
            // 
            // PuestosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TPuesto);
            this.Name = "PuestosForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "PuestosForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PuestosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.GridStandarControl TPuesto;
    }
}