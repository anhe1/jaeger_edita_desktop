﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class EmpleadoContratoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.checkBoxSDI = new Telerik.WinControls.UI.RadCheckBox();
            this.TxbSalarioBase = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.switchActivo = new Telerik.WinControls.UI.RadToggleSwitch();
            this.CboPerioricidadPago = new Telerik.WinControls.UI.RadDropDownList();
            this.label26 = new Telerik.WinControls.UI.RadLabel();
            this.textBoxSalarioDiario = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.label31 = new Telerik.WinControls.UI.RadLabel();
            this.label32 = new Telerik.WinControls.UI.RadLabel();
            this.label27 = new Telerik.WinControls.UI.RadLabel();
            this.textBoxSalarioDiarioIntegrado = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.CboTipoContrato = new Telerik.WinControls.UI.RadDropDownList();
            this.comboBox3 = new Telerik.WinControls.UI.RadDropDownList();
            this.DtFechaInicioRel = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label18 = new Telerik.WinControls.UI.RadLabel();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.label17 = new Telerik.WinControls.UI.RadLabel();
            this.SpinHorasJornada = new Telerik.WinControls.UI.RadSpinEditor();
            this.SpinJornadasTrabajo = new Telerik.WinControls.UI.RadSpinEditor();
            this.CboTipoEmpleado = new Telerik.WinControls.UI.RadDropDownList();
            this.label43 = new Telerik.WinControls.UI.RadLabel();
            this.label13 = new Telerik.WinControls.UI.RadLabel();
            this.Departamento = new Telerik.WinControls.UI.RadDropDownList();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.label22 = new Telerik.WinControls.UI.RadLabel();
            this.CboPuesto = new Telerik.WinControls.UI.RadDropDownList();
            this.CboRegimenFiscal = new Telerik.WinControls.UI.RadDropDownList();
            this.dtFechaTerminoRelacion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label20 = new Telerik.WinControls.UI.RadLabel();
            this.CboRiesgoPuesto = new Telerik.WinControls.UI.RadDropDownList();
            this.label16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.label19 = new Telerik.WinControls.UI.RadLabel();
            this.CboTipoJornada = new Telerik.WinControls.UI.RadDropDownList();
            this.TEmpleado = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.wPrepare = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxSDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSalarioBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPerioricidadPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSalarioDiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSalarioDiarioIntegrado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoContrato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaInicioRel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinHorasJornada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinJornadasTrabajo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPuesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaTerminoRelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRiesgoPuesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoJornada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxSDI
            // 
            this.checkBoxSDI.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.checkBoxSDI.Location = new System.Drawing.Point(283, 99);
            this.checkBoxSDI.Name = "checkBoxSDI";
            this.checkBoxSDI.Size = new System.Drawing.Size(15, 15);
            this.checkBoxSDI.TabIndex = 138;
            this.checkBoxSDI.Text = "SDI";
            // 
            // TxbSalarioBase
            // 
            this.TxbSalarioBase.Location = new System.Drawing.Point(390, 140);
            this.TxbSalarioBase.Name = "TxbSalarioBase";
            this.TxbSalarioBase.Size = new System.Drawing.Size(107, 20);
            this.TxbSalarioBase.TabIndex = 137;
            this.TxbSalarioBase.TabStop = false;
            this.TxbSalarioBase.Value = "0";
            // 
            // switchActivo
            // 
            this.switchActivo.Location = new System.Drawing.Point(447, 318);
            this.switchActivo.Name = "switchActivo";
            this.switchActivo.Size = new System.Drawing.Size(50, 20);
            this.switchActivo.TabIndex = 136;
            // 
            // CboPerioricidadPago
            // 
            this.CboPerioricidadPago.Location = new System.Drawing.Point(12, 96);
            this.CboPerioricidadPago.Name = "CboPerioricidadPago";
            this.CboPerioricidadPago.Size = new System.Drawing.Size(157, 20);
            this.CboPerioricidadPago.TabIndex = 135;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(390, 122);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 18);
            this.label26.TabIndex = 129;
            this.label26.Text = "Salario Base:";
            // 
            // textBoxSalarioDiario
            // 
            this.textBoxSalarioDiario.Location = new System.Drawing.Point(175, 96);
            this.textBoxSalarioDiario.Name = "textBoxSalarioDiario";
            this.textBoxSalarioDiario.Size = new System.Drawing.Size(100, 20);
            this.textBoxSalarioDiario.TabIndex = 128;
            this.textBoxSalarioDiario.TabStop = false;
            this.textBoxSalarioDiario.Value = "0";
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(278, 78);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(98, 18);
            this.label31.TabIndex = 133;
            this.label31.Text = "S. Base Cotización:";
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(384, 78);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 18);
            this.label32.TabIndex = 134;
            this.label32.Text = "Base de Cotización";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(175, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 18);
            this.label27.TabIndex = 130;
            this.label27.Text = "Salario Diario:";
            // 
            // textBoxSalarioDiarioIntegrado
            // 
            this.textBoxSalarioDiarioIntegrado.Location = new System.Drawing.Point(304, 96);
            this.textBoxSalarioDiarioIntegrado.Name = "textBoxSalarioDiarioIntegrado";
            this.textBoxSalarioDiarioIntegrado.Size = new System.Drawing.Size(77, 20);
            this.textBoxSalarioDiarioIntegrado.TabIndex = 131;
            this.textBoxSalarioDiarioIntegrado.TabStop = false;
            this.textBoxSalarioDiarioIntegrado.Value = "0";
            // 
            // CboTipoContrato
            // 
            this.CboTipoContrato.Location = new System.Drawing.Point(12, 140);
            this.CboTipoContrato.Name = "CboTipoContrato";
            this.CboTipoContrato.Size = new System.Drawing.Size(369, 20);
            this.CboTipoContrato.TabIndex = 126;
            // 
            // comboBox3
            // 
            this.comboBox3.Location = new System.Drawing.Point(384, 96);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(113, 20);
            this.comboBox3.TabIndex = 132;
            // 
            // DtFechaInicioRel
            // 
            this.DtFechaInicioRel.Location = new System.Drawing.Point(221, 274);
            this.DtFechaInicioRel.Name = "DtFechaInicioRel";
            this.DtFechaInicioRel.Size = new System.Drawing.Size(184, 20);
            this.DtFechaInicioRel.TabIndex = 123;
            this.DtFechaInicioRel.TabStop = false;
            this.DtFechaInicioRel.Text = "lunes, 21 de enero de 2019";
            this.DtFechaInicioRel.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 167);
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(12, 122);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 18);
            this.label18.TabIndex = 127;
            this.label18.Text = "Tipo de Contrato:";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(221, 252);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(166, 18);
            this.label15.TabIndex = 124;
            this.label15.Text = "Fecha Inicio de Relación Laboral";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(12, 78);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 18);
            this.label17.TabIndex = 125;
            this.label17.Text = "Perioricidad de Pago:";
            // 
            // SpinHorasJornada
            // 
            this.SpinHorasJornada.Location = new System.Drawing.Point(96, 318);
            this.SpinHorasJornada.Name = "SpinHorasJornada";
            this.SpinHorasJornada.Size = new System.Drawing.Size(78, 20);
            this.SpinHorasJornada.TabIndex = 185;
            this.SpinHorasJornada.TabStop = false;
            this.SpinHorasJornada.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SpinJornadasTrabajo
            // 
            this.SpinJornadasTrabajo.Location = new System.Drawing.Point(12, 318);
            this.SpinJornadasTrabajo.Name = "SpinJornadasTrabajo";
            this.SpinJornadasTrabajo.Size = new System.Drawing.Size(78, 20);
            this.SpinJornadasTrabajo.TabIndex = 184;
            this.SpinJornadasTrabajo.TabStop = false;
            this.SpinJornadasTrabajo.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CboTipoEmpleado
            // 
            this.CboTipoEmpleado.Location = new System.Drawing.Point(12, 274);
            this.CboTipoEmpleado.Name = "CboTipoEmpleado";
            this.CboTipoEmpleado.Size = new System.Drawing.Size(168, 20);
            this.CboTipoEmpleado.TabIndex = 174;
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(12, 256);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(99, 18);
            this.label43.TabIndex = 176;
            this.label43.Text = "Tipo de empleado:";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(12, 166);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 18);
            this.label13.TabIndex = 155;
            this.label13.Text = "Departamento:";
            // 
            // Departamento
            // 
            this.Departamento.DisplayMember = "Descripcion";
            this.Departamento.Location = new System.Drawing.Point(12, 184);
            this.Departamento.Name = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(200, 20);
            this.Departamento.TabIndex = 153;
            this.Departamento.ValueMember = "IdDepartamento";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(221, 166);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 18);
            this.label14.TabIndex = 156;
            this.label14.Text = "Puesto:";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(221, 300);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(171, 18);
            this.label22.TabIndex = 168;
            this.label22.Text = "Fec. Termino de Relación Laboral";
            // 
            // CboPuesto
            // 
            this.CboPuesto.DisplayMember = "Descripcion";
            this.CboPuesto.Location = new System.Drawing.Point(221, 184);
            this.CboPuesto.Name = "CboPuesto";
            this.CboPuesto.Size = new System.Drawing.Size(276, 20);
            this.CboPuesto.TabIndex = 154;
            this.CboPuesto.ValueMember = "IdPuesto";
            // 
            // CboRegimenFiscal
            // 
            this.CboRegimenFiscal.Location = new System.Drawing.Point(12, 226);
            this.CboRegimenFiscal.Name = "CboRegimenFiscal";
            this.CboRegimenFiscal.Size = new System.Drawing.Size(201, 20);
            this.CboRegimenFiscal.TabIndex = 163;
            // 
            // dtFechaTerminoRelacion
            // 
            this.dtFechaTerminoRelacion.Location = new System.Drawing.Point(221, 318);
            this.dtFechaTerminoRelacion.Name = "dtFechaTerminoRelacion";
            this.dtFechaTerminoRelacion.Size = new System.Drawing.Size(184, 20);
            this.dtFechaTerminoRelacion.TabIndex = 167;
            this.dtFechaTerminoRelacion.TabStop = false;
            this.dtFechaTerminoRelacion.Text = "lunes, 21 de enero de 2019";
            this.dtFechaTerminoRelacion.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 93);
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(12, 208);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 18);
            this.label20.TabIndex = 164;
            this.label20.Text = "Tipo de Regimen:";
            // 
            // CboRiesgoPuesto
            // 
            this.CboRiesgoPuesto.Location = new System.Drawing.Point(221, 226);
            this.CboRiesgoPuesto.Name = "CboRiesgoPuesto";
            this.CboRiesgoPuesto.Size = new System.Drawing.Size(145, 20);
            this.CboRiesgoPuesto.TabIndex = 157;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(221, 208);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(95, 18);
            this.label16.TabIndex = 158;
            this.label16.Text = "Riesgo de Puesto:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(96, 300);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(78, 18);
            this.radLabel3.TabIndex = 162;
            this.radLabel3.Text = "Horas Jornada";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(12, 300);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 161;
            this.radLabel2.Text = "Día Jornada";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(380, 208);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(86, 18);
            this.label19.TabIndex = 160;
            this.label19.Text = "Tipo de Jornada";
            // 
            // CboTipoJornada
            // 
            this.CboTipoJornada.Location = new System.Drawing.Point(380, 226);
            this.CboTipoJornada.Name = "CboTipoJornada";
            this.CboTipoJornada.Size = new System.Drawing.Size(117, 20);
            this.CboTipoJornada.TabIndex = 159;
            // 
            // TEmpleado
            // 
            this.TEmpleado.Dock = System.Windows.Forms.DockStyle.Top;
            this.TEmpleado.Etiqueta = "";
            this.TEmpleado.Location = new System.Drawing.Point(0, 33);
            this.TEmpleado.Name = "TEmpleado";
            this.TEmpleado.ShowActualizar = true;
            this.TEmpleado.ShowAutorizar = false;
            this.TEmpleado.ShowCerrar = true;
            this.TEmpleado.ShowEditar = false;
            this.TEmpleado.ShowExportarExcel = false;
            this.TEmpleado.ShowFiltro = false;
            this.TEmpleado.ShowGuardar = true;
            this.TEmpleado.ShowHerramientas = false;
            this.TEmpleado.ShowImagen = false;
            this.TEmpleado.ShowImprimir = false;
            this.TEmpleado.ShowNuevo = false;
            this.TEmpleado.ShowRemover = false;
            this.TEmpleado.Size = new System.Drawing.Size(517, 30);
            this.TEmpleado.TabIndex = 187;
            
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(517, 33);
            this.Encabezado.TabIndex = 186;
            this.Encabezado.TabStop = false;
            // 
            // wPrepare
            // 
            this.wPrepare.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WPrepare_DoWork);
            this.wPrepare.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.wPrepare_RunWorkerCompleted);
            // 
            // EmpleadoContratoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 354);
            this.Controls.Add(this.TEmpleado);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.SpinHorasJornada);
            this.Controls.Add(this.SpinJornadasTrabajo);
            this.Controls.Add(this.CboTipoEmpleado);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Departamento);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.CboPuesto);
            this.Controls.Add(this.CboRegimenFiscal);
            this.Controls.Add(this.dtFechaTerminoRelacion);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.CboRiesgoPuesto);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.CboTipoJornada);
            this.Controls.Add(this.checkBoxSDI);
            this.Controls.Add(this.TxbSalarioBase);
            this.Controls.Add(this.switchActivo);
            this.Controls.Add(this.CboPerioricidadPago);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.textBoxSalarioDiario);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.textBoxSalarioDiarioIntegrado);
            this.Controls.Add(this.CboTipoContrato);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.DtFechaInicioRel);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label17);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmpleadoContratoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contrato";
            this.Load += new System.EventHandler(this.EmpleadoContratoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxSDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSalarioBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPerioricidadPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSalarioDiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxSalarioDiarioIntegrado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoContrato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtFechaInicioRel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinHorasJornada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinJornadasTrabajo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboPuesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaTerminoRelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRiesgoPuesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboTipoJornada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCheckBox checkBoxSDI;
        private Telerik.WinControls.UI.RadCalculatorDropDown TxbSalarioBase;
        private Telerik.WinControls.UI.RadToggleSwitch switchActivo;
        private Telerik.WinControls.UI.RadDropDownList CboPerioricidadPago;
        private Telerik.WinControls.UI.RadLabel label26;
        private Telerik.WinControls.UI.RadCalculatorDropDown textBoxSalarioDiario;
        private Telerik.WinControls.UI.RadLabel label31;
        private Telerik.WinControls.UI.RadLabel label32;
        private Telerik.WinControls.UI.RadLabel label27;
        private Telerik.WinControls.UI.RadCalculatorDropDown textBoxSalarioDiarioIntegrado;
        private Telerik.WinControls.UI.RadDropDownList CboTipoContrato;
        private Telerik.WinControls.UI.RadDropDownList comboBox3;
        private Telerik.WinControls.UI.RadDateTimePicker DtFechaInicioRel;
        private Telerik.WinControls.UI.RadLabel label18;
        private Telerik.WinControls.UI.RadLabel label15;
        private Telerik.WinControls.UI.RadLabel label17;
        private Telerik.WinControls.UI.RadSpinEditor SpinHorasJornada;
        private Telerik.WinControls.UI.RadSpinEditor SpinJornadasTrabajo;
        private Telerik.WinControls.UI.RadDropDownList CboTipoEmpleado;
        private Telerik.WinControls.UI.RadLabel label43;
        private Telerik.WinControls.UI.RadLabel label13;
        private Telerik.WinControls.UI.RadDropDownList Departamento;
        private Telerik.WinControls.UI.RadLabel label14;
        private Telerik.WinControls.UI.RadLabel label22;
        private Telerik.WinControls.UI.RadDropDownList CboPuesto;
        private Telerik.WinControls.UI.RadDropDownList CboRegimenFiscal;
        private Telerik.WinControls.UI.RadDateTimePicker dtFechaTerminoRelacion;
        private Telerik.WinControls.UI.RadLabel label20;
        private Telerik.WinControls.UI.RadDropDownList CboRiesgoPuesto;
        private Telerik.WinControls.UI.RadLabel label16;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel label19;
        private Telerik.WinControls.UI.RadDropDownList CboTipoJornada;
        private Common.Forms.ToolBarStandarControl TEmpleado;
        private System.Windows.Forms.PictureBox Encabezado;
        private System.ComponentModel.BackgroundWorker wPrepare;
    }
}