﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class EmpleadoContratoForm : RadForm {
        #region declaraciones
        protected IContratoService service;
        protected IDepartamentoService departamentoService;
        protected IPuestoService puestoService;
        private IContratoModel contrato;
        private PeriodicidadPagoCatalogo perioricidadPago;
        private TipoContratoCatalogo tipoContrato;
        private RiesgoPuestoCatalogo riesgoPuesto;
        private TipoJornadaCatalogo tipoJornada;
        private ITipoRegimenCatalogo regimenesFiscales;
        #endregion

        public EmpleadoContratoForm(ContratoModel contrato, IContratoService service, IDepartamentoService departamentoService, IPuestoService puestoService) {
            InitializeComponent();
            this.contrato = contrato;
            this.service = service;
            this.departamentoService = departamentoService;
            this.puestoService = puestoService;
        }

        private void EmpleadoContratoForm_Load(object sender, EventArgs e) {
            this.regimenesFiscales = new CatalogoTipoRegimen();

            this.TEmpleado.Guardar.Click += this.TEmpleado_Guardar_Click;
            this.TEmpleado.Actualizar.Click += this.TEmpleado_Actualizar_Click;
            this.TEmpleado.Cerrar.Click += this.TEmpleado_Cerrar_Click;

            this.wPrepare.RunWorkerAsync();
        }

        private void TEmpleado_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void TEmpleado_Actualizar_Click(object sender, EventArgs e) {
            if (this.contrato == null) {
                this.contrato = new ContratoModel();
            } else {
                using(var espera = new Waiting1Form(this.Actualizar)) {
                    espera.Text = "Espera un momento...";
                    espera.ShowDialog(this);
                }
            }
            this.CreateBinding();
        }

        private void TEmpleado_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.CboPerioricidadPago.DataBindings.Clear();
            this.CboPerioricidadPago.DataBindings.Add("SelectedValue", this.contrato, "ClavePeriodicidadPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbSalarioBase.DataBindings.Clear();
            this.TxbSalarioBase.DataBindings.Add("Value", this.contrato, "SalarioBase", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxSalarioDiario.DataBindings.Clear();
            this.textBoxSalarioDiario.DataBindings.Add("Value", this.contrato, "SalarioDiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DtFechaInicioRel.DataBindings.Clear();
            this.DtFechaInicioRel.DataBindings.Add("Value", this.contrato, "FecInicioRelLaboral", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboTipoContrato.DataBindings.Clear();
            this.CboTipoContrato.DataBindings.Add("SelectedValue", this.contrato, "ClaveTipoContrato", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboRiesgoPuesto.DataBindings.Clear();
            this.CboRiesgoPuesto.DataBindings.Add("SelectedValue", this.contrato, "ClaveRiesgoPuesto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Departamento.DataBindings.Clear();
            this.Departamento.DataBindings.Add("SelectedValue", this.contrato, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboPuesto.DataBindings.Clear();
            this.CboPuesto.DataBindings.Add("Text", this.contrato, "Puesto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SpinJornadasTrabajo.DataBindings.Clear();
            this.SpinJornadasTrabajo.DataBindings.Add("Value", this.contrato, "Jornadas", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SpinHorasJornada.DataBindings.Clear();
            this.SpinHorasJornada.DataBindings.Add("Value", this.contrato, "Horas", true, DataSourceUpdateMode.OnPropertyChanged);

            this.textBoxSalarioDiarioIntegrado.DataBindings.Clear();
            this.textBoxSalarioDiarioIntegrado.DataBindings.Add("Value", this.contrato, "SalarioDiarioIntegrado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboTipoJornada.DataBindings.Clear();
            this.CboTipoJornada.DataBindings.Add("SelectedValue", this.contrato, "ClaveTipoJornada", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboRegimenFiscal.DataBindings.Clear();
            this.CboRegimenFiscal.DataBindings.Add("SelectedValue", this.contrato, "ClaveTipoRegimen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.switchActivo.DataBindings.Clear();
            this.switchActivo.DataBindings.Add("Value", this.contrato, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtFechaTerminoRelacion.Enabled = !this.contrato.Activo;
        }

        private void Actualizar() {
            this.contrato = this.service.GetById(this.contrato.IdContrato);
        }

        private void Guardar() {
            this.contrato = this.service.Save(this.contrato);
        }

        private void WPrepare_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e) {
            this.regimenesFiscales.Load();
            this.tipoContrato = new TipoContratoCatalogo();
            this.tipoContrato.Load();
            this.perioricidadPago = new PeriodicidadPagoCatalogo();
            this.perioricidadPago.Load();
            this.regimenesFiscales = new CatalogoTipoRegimen();
            this.regimenesFiscales.Load();
            this.riesgoPuesto = new RiesgoPuestoCatalogo();
            this.riesgoPuesto.Load();
            this.tipoJornada = new TipoJornadaCatalogo();
            this.tipoJornada.Load();

            this.CboTipoContrato.DisplayMember = "Descripcion";
            this.CboTipoContrato.ValueMember = "Clave";

            this.CboPerioricidadPago.DisplayMember = "Descripcion";
            this.CboPerioricidadPago.ValueMember = "Clave";

            this.CboRegimenFiscal.DisplayMember = "Descripcion";
            this.CboRegimenFiscal.ValueMember = "Clave";

            this.CboRiesgoPuesto.DisplayMember = "Descripcion";
            this.CboRiesgoPuesto.ValueMember = "Clave";

            this.CboTipoJornada.DisplayMember = "Descripcion";
            this.CboTipoJornada.ValueMember = "Clave";

            this.Departamento.DataSource = this.departamentoService.GetList<DepartamentoModel>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>() { new Jaeger.Domain.Base.Entities.Conditional("CTDPT_A", "1") });
            this.CboPuesto.DataSource = this.puestoService.GetList<PuestoModel>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>());
        }

        private void wPrepare_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e) {
            this.CboTipoContrato.DataSource = this.tipoContrato.Items;
            this.CboPerioricidadPago.DataSource = this.perioricidadPago.Items;
            this.CboRegimenFiscal.DataSource = this.regimenesFiscales.Items;
            this.CboRiesgoPuesto.DataSource = this.riesgoPuesto.Items;
            this.CboTipoJornada.DataSource = this.tipoJornada.Items;
            this.TEmpleado.Actualizar.PerformClick();
        }
    }
}
