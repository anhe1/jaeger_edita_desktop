﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class RegistrosAusenciasCalendarioForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.SchedulerDailyPrintStyle schedulerDailyPrintStyle1 = new Telerik.WinControls.UI.SchedulerDailyPrintStyle();
            this.Calendario = new Telerik.WinControls.UI.RadCalendar();
            this.Empleados = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Navigator = new Telerik.WinControls.UI.RadSchedulerNavigator();
            this.Scheduler = new Telerik.WinControls.UI.RadScheduler();
            this.GeneralPanel = new Telerik.WinControls.UI.RadPanel();
            this.TRegistro = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.Calendario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Navigator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Scheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneralPanel)).BeginInit();
            this.GeneralPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Calendario
            // 
            this.Calendario.AllowMultipleView = true;
            this.Calendario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Calendario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Calendario.Location = new System.Drawing.Point(0, 0);
            this.Calendario.Margin = new System.Windows.Forms.Padding(0);
            this.Calendario.MultiViewRows = 3;
            this.Calendario.Name = "Calendario";
            this.Calendario.SelectedDates.AddRange(new System.DateTime[] {
            new System.DateTime(1900, 1, 1, 0, 0, 0, 0)});
            this.Calendario.Size = new System.Drawing.Size(266, 640);
            this.Calendario.TabIndex = 1;
            this.Calendario.ZoomFactor = 1.2F;
            // 
            // Empleados
            // 
            this.Empleados.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.Empleados.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Empleados.NestedRadGridView
            // 
            this.Empleados.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Empleados.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Empleados.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Empleados.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Empleados.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Empleados.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Empleados.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "IdEmpleado";
            gridViewTextBoxColumn1.HeaderText = "IdEmpleado";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdEmpleado";
            gridViewTextBoxColumn2.FieldName = "NombreCompleto";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "NombreCompleto";
            this.Empleados.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Empleados.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Empleados.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Empleados.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Empleados.EditorControl.Name = "NestedRadGridView";
            this.Empleados.EditorControl.ReadOnly = true;
            this.Empleados.EditorControl.ShowGroupPanel = false;
            this.Empleados.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Empleados.EditorControl.TabIndex = 0;
            this.Empleados.Location = new System.Drawing.Point(523, 4);
            this.Empleados.Name = "Empleados";
            this.Empleados.Size = new System.Drawing.Size(257, 20);
            this.Empleados.TabIndex = 0;
            this.Empleados.TabStop = false;
            // 
            // Navigator
            // 
            this.Navigator.AssociatedScheduler = this.Scheduler;
            this.Navigator.DateFormat = "MM/dd/yyyy";
            this.Navigator.Dock = System.Windows.Forms.DockStyle.Top;
            this.Navigator.Location = new System.Drawing.Point(266, 30);
            this.Navigator.MinimumSize = new System.Drawing.Size(400, 74);
            this.Navigator.Name = "Navigator";
            this.Navigator.NavigationStepType = Telerik.WinControls.UI.NavigationStepTypes.Month;
            // 
            // 
            // 
            this.Navigator.RootElement.MinSize = new System.Drawing.Size(400, 74);
            this.Navigator.RootElement.StretchVertically = false;
            this.Navigator.Size = new System.Drawing.Size(780, 77);
            this.Navigator.TabIndex = 8;
            ((Telerik.WinControls.UI.RadToggleButtonElement)(this.Navigator.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(2).GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadLabelElement)(this.Navigator.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(2).GetChildAt(2))).Text = "11/02/2009 - 11/04/2009";
            // 
            // Scheduler
            // 
            this.Scheduler.AllowAppointmentCreateInline = false;
            this.Scheduler.AllowAppointmentMove = false;
            this.Scheduler.AllowAppointmentResize = false;
            this.Scheduler.AllowCopyPaste = Telerik.WinControls.UI.CopyPasteMode.Disallow;
            this.Scheduler.AppointmentTitleFormat = null;
            this.Scheduler.Culture = new System.Globalization.CultureInfo("en-US");
            this.Scheduler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Scheduler.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Scheduler.HeaderFormat = "dd (ddd)";
            this.Scheduler.Location = new System.Drawing.Point(266, 107);
            this.Scheduler.Name = "Scheduler";
            schedulerDailyPrintStyle1.AppointmentFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            schedulerDailyPrintStyle1.DateEndRange = new System.DateTime(2013, 3, 17, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.DateHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            schedulerDailyPrintStyle1.DateStartRange = new System.DateTime(2013, 3, 12, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.PageHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.Scheduler.PrintStyle = schedulerDailyPrintStyle1;
            this.Scheduler.Size = new System.Drawing.Size(780, 563);
            this.Scheduler.TabIndex = 0;
            // 
            // GeneralPanel
            // 
            this.GeneralPanel.Controls.Add(this.Calendario);
            this.GeneralPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.GeneralPanel.Location = new System.Drawing.Point(0, 30);
            this.GeneralPanel.Name = "GeneralPanel";
            this.GeneralPanel.Size = new System.Drawing.Size(266, 640);
            this.GeneralPanel.TabIndex = 9;
            this.GeneralPanel.Text = "radPanel1";
            // 
            // TRegistro
            // 
            this.TRegistro.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRegistro.Location = new System.Drawing.Point(0, 0);
            this.TRegistro.Name = "TRegistro";
            this.TRegistro.ShowActualizar = true;
            this.TRegistro.ShowAutosuma = false;
            this.TRegistro.ShowCancelar = false;
            this.TRegistro.ShowCerrar = true;
            this.TRegistro.ShowEditar = false;
            this.TRegistro.ShowEjercicio = true;
            this.TRegistro.ShowExportarExcel = false;
            this.TRegistro.ShowFiltro = false;
            this.TRegistro.ShowHerramientas = false;
            this.TRegistro.ShowImprimir = false;
            this.TRegistro.ShowItem = true;
            this.TRegistro.ShowNuevo = false;
            this.TRegistro.ShowPeriodo = false;
            this.TRegistro.Size = new System.Drawing.Size(1046, 30);
            this.TRegistro.TabIndex = 10;
            // 
            // RegistrosAusenciasCalendarioForm
            // 
            this.ClientSize = new System.Drawing.Size(1046, 680);
            this.Controls.Add(this.Empleados);
            this.Controls.Add(this.Scheduler);
            this.Controls.Add(this.Navigator);
            this.Controls.Add(this.GeneralPanel);
            this.Controls.Add(this.TRegistro);
            this.Name = "RegistrosAusenciasCalendarioForm";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RegistroAusenciasCalendario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Calendario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Navigator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Scheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneralPanel)).EndInit();
            this.GeneralPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadScheduler Scheduler;
        private Telerik.WinControls.UI.RadCalendar Calendario;
        private Telerik.WinControls.UI.RadSchedulerNavigator Navigator;
        private Telerik.WinControls.UI.RadPanel GeneralPanel;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Empleados;
        private Common.Forms.ToolBarCommonControl TRegistro;
    }
}