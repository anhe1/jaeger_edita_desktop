﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Localization;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class RegistrosAusenciasCalendarioForm : RadForm {
        protected internal IRegistroAusenciasService _Service;
        protected internal List<IRegistroAusenciasModel> _DataSource;
        protected internal List<IEmpleadoModel> _Empleados;
        protected internal BackgroundWorker _EmpleadosWorker = new BackgroundWorker();

        public RegistrosAusenciasCalendarioForm() {
            InitializeComponent();
        }

        private void RegistroAusenciasCalendario_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            SchedulerNavigatorLocalizationProvider.CurrentProvider = new LocalizationProviderCustomSchedulerNavigator();
            RadSchedulerLocalizationProvider.CurrentProvider = new LocalizationProviderCustomScheduler();

            this._EmpleadosWorker.DoWork += Empleados_Worker_DoWork;
            this._EmpleadosWorker.RunWorkerCompleted += Empleados_Worker_RunWorkerCompleted;

            this.TRegistro.HostCaption.Text = "Empleado: ";
            this.TRegistro.HostItem.HostedItem = this.Empleados.MultiColumnComboBoxElement;
            this.TRegistro.HostItem.MinSize = new Size(255, 20);

            this.Navigator.WeekViewButtonVisible = false;
            this.Navigator.DayViewButtonVisible = false;
            this.Scheduler.GetDayView().DayCount = 1;
            this.Navigator.ShowWeekendStateChanged += new StateChangedEventHandler(SchedulerNavigator_ShowWeekendStateChanged);
            this.Calendario.ShowOtherMonthsDays = false;
            this.Calendario.AllowMultipleSelect = false;
            this.Calendario.TitleFormat = "MMMM";

            this.Calendario.SelectedDate = DateTime.Today;
            this.Calendario.SelectionChanged += new EventHandler(Calendario_SelectionChanged);

            this.Scheduler.PropertyChanged += new PropertyChangedEventHandler(Scheduler_PropertyChanged);
            this.Scheduler.Appointments.CollectionChanged += new Telerik.WinControls.Data.NotifyCollectionChangedEventHandler(Appointments_CollectionChanged);
            this.Scheduler.AppointmentTitleFormat = "{2} ({3})";
            this.Navigator.TimelineViewButtonVisible = false;
            this.Calendario.InvalidateCalendar();
            
            this.Empleados.DisplayMember = "NombreCompleto";
            this.Empleados.ValueMember = "IdEmpleado";
            this.Empleados.AutoSize = true;

            this.TRegistro.Actualizar.Click += this.TRegistro_Actualizar_Click;
            this.TRegistro.Cerrar.Click += TRegistro_Cerrar_Click;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._EmpleadosWorker.RunWorkerAsync();
        }

        private void TRegistro_Actualizar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting1Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
        }

        private void TRegistro_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consulta() { 
            var rowinfo = this.Empleados.SelectedItem as GridViewRowInfo;
            var seleccionado = rowinfo.DataBoundItem as IEmpleadoModel;
            this._DataSource = this._Service.GetList<RegistroAusenciasModel>(Aplication.Nomina.Services.RegistroAusenciasService.Query().Year(this.TRegistro.GetEjercicio()).IdEmpleado(seleccionado.IdEmpleado).Tipo(Domain.Nomina.ValueObjects.DiasTipoEnum.Vacaciones).Build()).ToList<IRegistroAusenciasModel>();
            this.Scheduler.Appointments.Clear();
            Appointment appointment = null;
            for (int i = 0; i < this._DataSource.Count; i++) {
                appointment = new Appointment(this._DataSource[i].Fecha, this._DataSource[i].Fecha, this._DataSource[i].Tipo.ToString(), "", "");
                this.Scheduler.Appointments.Add(appointment);
            }
            this.Scheduler.ActiveViewChanged -= new EventHandler<SchedulerViewChangedEventArgs>(Scheduler_ActiveViewChanged);
            this.InitializeCalendar();
            try {
                this.Scheduler.ActiveViewType = SchedulerViewType.Month;
            } catch (Exception) {
                
            }
            this.Scheduler.ActiveViewChanged += new EventHandler<SchedulerViewChangedEventArgs>(Scheduler_ActiveViewChanged);
        }

        private void InitializeCalendar() {
            MultiMonthViewElement viewElement = this.Calendario.CalendarElement.CalendarVisualElement as MultiMonthViewElement;
            this.Calendario.CalendarElement.Margin = new Padding(0, 0, 0, 14);

            if (viewElement != null) {
                CalendarMultiMonthViewTableElement table = viewElement.GetMultiTableElement();

                foreach (MonthViewElement monthView in table.Children) {
                    monthView.TitleElement.Margin = new Padding(-4, -2, -2, -2);
                    monthView.TitleElement.Padding = new Padding(3);

                    foreach (CalendarCellElement cell in monthView.TableElement.Children) {
                        bool headerCell = (bool)cell.GetValue(CalendarCellElement.IsHeaderCellProperty);
                        if (headerCell)
                            continue;

                        SchedulerDayView view = new SchedulerDayView {
                            DayCount = 1,
                            StartDate = cell.Date
                        };
                        view.GetViewContainingDate(cell.Date);

                        view.UpdateAppointments(this.Scheduler.Appointments);

                        if (view.Appointments.Count > 0) {
                            cell.Font = new Font(cell.Font, FontStyle.Bold);
                        } else {
                            cell.Font = this.Calendario.Font;
                        }
                    }
                }
            }
        }

        private void Appointments_CollectionChanged(object sender, Telerik.WinControls.Data.NotifyCollectionChangedEventArgs e) {
            InitializeCalendar();
        }
        
        private void Calendario_SelectionChanged(object sender, EventArgs e) {
            if (this.Calendario.SelectedDates.Count > 0) {
                this.Scheduler.ActiveView.StartDate = this.Calendario.SelectedDate;
            }
        }

        private void SchedulerNavigator_ShowWeekendStateChanged(object sender, StateChangedEventArgs args) {
            if (this.Scheduler.ActiveView as SchedulerMonthView == null) {
                (this.Scheduler.ActiveView as SchedulerDayViewBase).RulerWidth = 45;
                (this.Scheduler.SchedulerElement.ViewElement as SchedulerDayViewElement).DataAreaElement.ScrollView.Value = Point.Empty;
                (this.Scheduler.SchedulerElement.ViewElement as SchedulerDayViewElement).DataAreaElement.Table.ScrollToWorkHours();
            }
        }

        private void Scheduler_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            InitializeCalendar();
        }

        private void Scheduler_ActiveViewChanged(object sender, SchedulerViewChangedEventArgs e) {
            if (e.NewView as SchedulerMonthView == null) {
                SchedulerDayViewBase dayBase = (this.Scheduler.ActiveView as SchedulerDayViewBase);

                if (e.OldView != null && e.NewView != null) {
                    if (e.NewView.ViewType != e.OldView.ViewType) {
                        dayBase.RangeFactor = ScaleRange.HalfHour;
                    }
                }

                SchedulerDayViewElement dayView = this.Scheduler.SchedulerElement.ViewElement as SchedulerDayViewElement;
                dayView.DataAreaElement.Table.ScrollToWorkHours();
            }

            if (e.NewView.ViewType != e.OldView.ViewType && e.NewView.ViewType == SchedulerViewType.Month) {
                this.Scheduler.GetMonthView().WeekCount = 5;
            }
        }

        private void Empleados_Worker_DoWork(object sender, DoWorkEventArgs e) {
            this._Empleados = this._Service.GetList<EmpleadoModel>(Aplication.Nomina.Services.EmpleadoService.Query().OnlyActive().Build()).ToList<IEmpleadoModel>();
        }

        private void Empleados_Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Empleados.DataSource = this._Empleados;
        }
    }
}
