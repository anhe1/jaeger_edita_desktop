﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class DepartamentoForm : RadForm {
        protected IDepartamentoService Service;
        protected DepartamentoModel Current;

        public DepartamentoForm(DepartamentoModel model, IDepartamentoService service) {
            InitializeComponent();
            this.Service = service;
            this.Current = model;
        }

        private void DepartamentoForm_Load(object sender, EventArgs e) {
            if (this.Current == null) {
                this.Current = new DepartamentoModel();
            }
            this.Area.DataSource = this.Service.GetList<AreaModel>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>());
            this.CreateBinding();
        }

        private void CreateBinding() {
            this.Descripcion.DataBindings.Add("Text", this.Current, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.IsActivo.DataBindings.Add("Checked", this.Current, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Area.DataBindings.Add("SelectedValue", this.Current, "IdArea", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CuentaContable.DataBindings.Add("Text", this.Current, "CtaContable", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.Clasificacion.DataBindings.Add("Text", this.Current, "Clasificacion", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void TDepartamento_Guardar_Click(object sender, EventArgs e) {
            if (!this.Validar()) {
                return;
            }
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.ShowDialog(this);
            }
            if (this.Current.IdDepartamento > 0) {
                RadMessageBox.Show(this, "Registro guardado con éxito", "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Error al guardar el registro", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void TDepartamento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private bool Validar() {
            if (string.IsNullOrEmpty(this.Current.Descripcion)) {
               RadMessageBox.Show(this, "La descripción es requerida", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                return false;
            }
            return true;
        }

        private void Guardar() {
            this.Current = this.Service.Save(this.Current);
        }
    }
}
