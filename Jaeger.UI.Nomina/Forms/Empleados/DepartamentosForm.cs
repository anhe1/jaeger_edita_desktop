﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Nomina.Builder;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class DepartamentosForm : RadForm {
        protected IDepartamentoService Service;
        protected BindingList<DepartamentoModel> _DataSource;
        protected List<AreaModel> _Areas;

        public DepartamentosForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void DepartamentosForm_Load(object sender, EventArgs e) {
            using (IDepartamentoGridBuilder view = new DepartamentoGridBuilder()) {
                this.TDepartamento.GridData.Columns.AddRange(view.Templetes().Master().Build());
            }
            this.TDepartamento.Nuevo.Click += TDepartamento_Nuevo_Click;
            this.TDepartamento.Editar.Click += TDepartamento_Editar_Click;  
            this.TDepartamento.Remover.Click += TDepartamento_Remover_Click;
            this.TDepartamento.Actualizar.Click += TDepartmanto_Actualizar_Click;
            this.TDepartamento.Cerrar.Click += TDepartamento_Cerrar_Click;
        }

        private void TDepartamento_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new DepartamentoForm(null, this.Service);
            nuevo.ShowDialog(this);
        }

        private void TDepartamento_Editar_Click(object sender, EventArgs e) {
            if (this.TDepartamento.GridData.CurrentRow != null) {
                var seleccionado = this.TDepartamento.GridData.CurrentRow.DataBoundItem as DepartamentoModel;
                if (seleccionado != null) {
                    var editar = new DepartamentoForm(seleccionado, this.Service);
                    editar.ShowDialog(this);
                }
            }
        }

        private void TDepartamento_Remover_Click(object sender, EventArgs e) {
            if (this.TDepartamento.GridData.CurrentRow != null) {
                if (RadMessageBox.Show(this, "", "", System.Windows.Forms.MessageBoxButtons.YesNo, RadMessageIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes) {

                }
            }
        }

        private void TDepartmanto_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TDepartamento.GridData.DataSource = this._DataSource;
            var areaCol = this.TDepartamento.GridData.Columns["IdArea"] as GridViewComboBoxColumn;
            areaCol.DisplayMember = "Descripcion";
            areaCol.ValueMember = "Id";
            areaCol.DataSource = this._Areas;
        }

        private void TDepartamento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            this._DataSource = new BindingList<DepartamentoModel>(this.Service.GetList<Domain.Empresa.Entities.DepartamentoModel>(new List<Domain.Base.Builder.IConditional>()).ToList());
            this._Areas = this.Service.GetList<AreaModel>(new List<Domain.Base.Builder.IConditional>()).ToList();
        }
    }
}
