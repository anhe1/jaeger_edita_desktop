﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class VacacionesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TVacaciones = new Jaeger.UI.Nomina.Forms.Empleados.VacacionesGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TVacaciones
            // 
            this.TVacaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TVacaciones.Location = new System.Drawing.Point(0, 0);
            this.TVacaciones.Name = "TVacaciones";
            this.TVacaciones.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TVacaciones.Permisos = uiAction1;
            this.TVacaciones.ShowActualizar = true;
            this.TVacaciones.ShowAutosuma = false;
            this.TVacaciones.ShowCancelar = false;
            this.TVacaciones.ShowCerrar = true;
            this.TVacaciones.ShowEditar = false;
            this.TVacaciones.ShowEjercicio = true;
            this.TVacaciones.ShowExportarExcel = false;
            this.TVacaciones.ShowFiltro = true;
            this.TVacaciones.ShowHerramientas = true;
            this.TVacaciones.ShowImprimir = false;
            this.TVacaciones.ShowItem = false;
            this.TVacaciones.ShowNuevo = false;
            this.TVacaciones.ShowPeriodo = false;
            this.TVacaciones.ShowSeleccionMultiple = true;
            this.TVacaciones.Size = new System.Drawing.Size(800, 450);
            this.TVacaciones.TabIndex = 0;
            // 
            // VacacionesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TVacaciones);
            this.Name = "VacacionesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Registro de Vacaciones";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.VacacionesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VacacionesGridControl TVacaciones;
    }
}