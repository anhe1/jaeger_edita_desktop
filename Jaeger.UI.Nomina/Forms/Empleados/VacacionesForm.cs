﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class VacacionesForm : RadForm {
        protected IRegistroAusenciasService _Service;
        protected List<IEmpleadoVacacionesModel> _DataSource;

        public VacacionesForm() {
            InitializeComponent();
        }

        private void VacacionesForm_Load(object sender, EventArgs e) {
            this.TVacaciones._Calendario.Click += TVacaciones_Calendario_Click;
            this.TVacaciones.Actualizar.Click += TVacaciones_Actualizar_Click;
            this.TVacaciones.Cerrar.Click += TVacaciones_Cerrar_Click;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
        }

        private void TVacaciones_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TVacaciones.GridData.DataSource = this._DataSource;
        }

        private void TVacaciones_Calendario_Click(object sender, EventArgs e) {
            
        }

        private void TVacaciones_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            this._DataSource = this._Service.GetList<EmpleadoVacacionesModel>(new List<Domain.Base.Builder.IConditional>()).ToList<IEmpleadoVacacionesModel>();
        }
    }
}
