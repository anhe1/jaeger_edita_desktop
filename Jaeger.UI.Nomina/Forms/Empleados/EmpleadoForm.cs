using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class EmpleadoForm : RadForm {
        #region 
        private Aplication.Nomina.Helpers.ClaveCalculatorHelper claveCalculator;
        private IEmpleadoDetailModel empleado;
        protected IDepartamentoService departamentoService;
        protected IPuestoService puestoService;
        private readonly IEmpleadoService Service;
        private GridViewMultiComboBoxColumn cboProveedor;
        protected internal RadMenuItem Importar = new RadMenuItem { Text = "Importar" };
        #endregion

        public EmpleadoForm(IEmpleadoService service, IEmpleadoDetailModel empleado = null) {
            InitializeComponent();
            this.empleado = empleado;
            this.Service = service;
        }

        private void EmpleadoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridViewEmpleados.Standard();
            this.gridBanco.Standard();
            this.gridConceptos.Standard();
            this.gridConceptos.AllowEditRow = true;
            this.gridConceptos.Tag = false;

            this.Genero.DataSource = EmpleadoService.GetGeneros();
            this.Genero.DisplayMember = "Descripcion";
            this.Genero.ValueMember = "Id";

            this.EntidadFedNacimiento.DataSource = EmpleadoService.GetEstado();
            this.EntidadFedNacimiento.DisplayMember = "Descripcion";
            this.EntidadFedNacimiento.ValueMember = "Id";

            this.cboProveedor = this.gridConceptos.Columns["IdConcepto"] as GridViewMultiComboBoxColumn;
            this.cboProveedor.DisplayMember = "Concepto";
            this.cboProveedor.ValueMember = "IdConcepto";

            this.TBanco.Nuevo.Click += this.TBanco_Nuevo_Click;
            this.TBanco.Editar.Click += this.TBanco_Editar_Click;
            this.TContrato.Editar.Click += (this.TContrato_Editar_Click);
            this.TPrestacion.Nuevo.Click += this.TPrestacion_Nuevo_Click;
            this.TPrestacion.Remover.Click += this.TPrestacion_Remover_Click;
            this.TPrestacion.Filtro.Click += this.TPrestacion_Filtro_Click;
            this.TEmpleado.Herramientas.Items.Add(Importar);
            this.TEmpleado.Guardar.Click += this.TEmpleado_ButtonGuardar_Click;
            this.TEmpleado.Actualizar.Click += this.TEmpleado_Actualizar_Click;
            this.TEmpleado.Cerrar.Click += this.TEmpleado_Cerrar_Click;
            this.Importar.Click += TEmpleado_Importar_Click;
            this.TEmpleado.Actualizar.PerformClick();
        }

        #region barra de herramientas
        private void TEmpleado_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Obteniendo datos del empleado";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void TEmpleado_ButtonGuardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guandando ...";
                espera.ShowDialog(this);
            }
        }

        private void TEmpleado_Importar_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" };
            if (openFile.ShowDialog() == DialogResult.OK) {
                this.empleado = this.Service.Importar(new System.IO.FileInfo(openFile.FileName));
                if (this.empleado != null) {
                    this.CreateBinding();
                } else {
                    RadMessageBox.Show(this, "El comprobante seleccionado no es un recibo de n�mina", "Atenci�n", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private void TEmpleado_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region contratos
        private void TContrato_Editar_Click(object sender, EventArgs e) {
            var _seleccionado = this.gridViewEmpleados.CurrentRow.DataBoundItem as ContratoModel;
            if (_seleccionado != null) {
                //var _editar = new EmpleadoContratoForm(_seleccionado, this.Service, this.departamentoService, this.puestoService);
                //_editar.ShowDialog(this);
            }
        }
        #endregion

        #region cuentas bancarias
        private void TBanco_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new CuentaBancariaForm(new EmpleadoCuentaBancariaModel { IdEmpleado = this.empleado.IdEmpleado });
            _nuevo.ShowDialog(this);
        }

        private void TBanco_Editar_Click(object sender, EventArgs e) {
            if (this.gridBanco.CurrentRow != null) {
                var _seleccionado = this.gridBanco.CurrentRow.DataBoundItem as EmpleadoCuentaBancariaModel;
                if (_seleccionado != null) {
                    var _editar = new CuentaBancariaForm(_seleccionado);
                    _editar.ShowDialog(this);
                }
            }
        }
        #endregion

        #region conceptos de nomina
        private void TPrestacion_Nuevo_Click(object sender, EventArgs e) {
            if (this.empleado.Conceptos == null)
                this.empleado.Conceptos = new System.ComponentModel.BindingList<IEmpleadoConceptoNominaModel>();
            this.empleado.Conceptos.Add(new EmpleadoConceptoNominaModel());
        }

        private void TPrestacion_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                var _seleccionado = this.gridConceptos.CurrentRow.DataBoundItem as EmpleadoConceptoNominaModel;
                if (_seleccionado.Id == 0) {
                    this.gridConceptos.Rows.Remove(this.gridConceptos.CurrentRow);
                } else {
                    _seleccionado.Activo = false;
                    this.gridConceptos.CurrentRow.IsVisible = false;
                }
            }
        }

        private void TPrestacion_Filtro_Click(object sender, EventArgs e) {

        }
        #endregion

        private void CreateBinding() {
            this.TxbNum.DataBindings.Clear();
            this.TxbNum.DataBindings.Add("Text", this.empleado, "IdEmpleado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbClave.DataBindings.Clear();
            this.TxbClave.DataBindings.Add("Text", this.empleado, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbRFC.DataBindings.Clear();
            this.TxbRFC.DataBindings.Add("Text", this.empleado, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbCURP.DataBindings.Clear();
            this.TxbCURP.DataBindings.Add("Text", this.empleado, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nombre.DataBindings.Clear();
            this.Nombre.DataBindings.Add("Text", this.empleado, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ApellidoPaterno.DataBindings.Clear();
            this.ApellidoPaterno.DataBindings.Add("Text", this.empleado, "ApellidoPaterno", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ApellidoMaterno.DataBindings.Clear();
            this.ApellidoMaterno.DataBindings.Add("Text", this.empleado, "ApellidoMaterno", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbTelefono.DataBindings.Clear();
            this.TxbTelefono.DataBindings.Add("Text", this.empleado, "Telefono1", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbTelefono2.DataBindings.Clear();
            this.TxbTelefono2.DataBindings.Add("Text", this.empleado, "Telefono2", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbCorreo.DataBindings.Clear();
            this.TxbCorreo.DataBindings.Add("Text", this.empleado, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbSitio.DataBindings.Clear();
            this.TxbSitio.DataBindings.Add("Text", this.empleado, "Sitio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbNumFonacot.DataBindings.Clear();
            this.TxbNumFonacot.DataBindings.Add("Text", this.empleado, "FONACOT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbAfore.DataBindings.Clear();
            this.TxbAfore.DataBindings.Add("Text", this.empleado, "AFORE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbNumSeguridadSocial.DataBindings.Clear();
            this.TxbNumSeguridadSocial.DataBindings.Add("Text", this.empleado, "NSS", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbUMF.DataBindings.Clear();
            this.TxbUMF.DataBindings.Add("Text", this.empleado, "UMF", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaNacimiento.DataBindings.Clear();
            this.FechaNacimiento.DataBindings.Add("Value", this.empleado, "FecNacimiento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Genero.DataBindings.Clear();
            this.Genero.DataBindings.Add("SelectedValue", this.empleado, "IdGenero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.EntidadFedNacimiento.DataBindings.Clear();
            this.EntidadFedNacimiento.DataBindings.Add("SelectedValue", this.empleado, "IdEntidadFed", true, DataSourceUpdateMode.OnPropertyChanged);

            this.switchActivo.DataBindings.Clear();
            this.switchActivo.DataBindings.Add("Value", this.empleado, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbClave.Enabled = !(this.empleado.IdEmpleado > 0);
            this.gridViewEmpleados.DataSource = this.empleado;
            this.gridViewEmpleados.DataMember = "Contratos";
            this.gridBanco.DataSource = this.empleado;
            this.gridBanco.DataMember = "Cuentas";
            this.gridConceptos.DataSource = this.empleado;
            this.gridConceptos.DataMember = "Conceptos";
        }

        private void GridViewConceptos_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (this.gridConceptos.CurrentColumn is GridViewMultiComboBoxColumn) {
                if ((bool)this.gridConceptos.Tag == false) {
                    this.gridConceptos.Tag = true;
                    RadMultiColumnComboBoxElement editor = (RadMultiColumnComboBoxElement)this.gridConceptos.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("IdConcepto", "IdConcepto"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Concepto", "Concepto"));
                    editor.AutoSizeDropDownToBestFit = true;
                }
            }
        }

        private void Guardar() {
            this.empleado = this.Service.Save(this.empleado);
        }

        private void Consultar() {
            if (this.empleado == null) {
                this.claveCalculator = new Aplication.Nomina.Helpers.ClaveCalculatorHelper();
                this.Nombre.TextChanged += this.Nombre_TextChanged;
                this.ApellidoPaterno.TextChanged += this.Nombre_TextChanged;
                this.ApellidoMaterno.TextChanged += this.Nombre_TextChanged;
                this.empleado = new EmpleadoDetailModel();
            }
            this.cboProveedor.DataSource = this.Service.GetList<NominaConceptoDetailModel>(ConceptoService.Query().IsActive().Build());
        }

        private void Nombre_TextChanged(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(this.Nombre.Text))
                return;
            if (string.IsNullOrEmpty(this.ApellidoPaterno.Text))
                return;
            //if (string.IsNullOrEmpty(this.ApellidoMaterno.Text))
            //    return;
            this.empleado.Clave = this.claveCalculator.Calculate((EmpleadoModel)this.empleado);
        }
    }
}
