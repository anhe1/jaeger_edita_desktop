﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class AreaCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TArea = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TArea
            // 
            this.TArea.Caption = "";
            this.TArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TArea.Location = new System.Drawing.Point(0, 0);
            this.TArea.Name = "TArea";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TArea.Permisos = uiAction1;
            this.TArea.ReadOnly = false;
            this.TArea.ShowActualizar = true;
            this.TArea.ShowAutorizar = false;
            this.TArea.ShowAutosuma = false;
            this.TArea.ShowCerrar = true;
            this.TArea.ShowEditar = true;
            this.TArea.ShowExportarExcel = false;
            this.TArea.ShowFiltro = true;
            this.TArea.ShowHerramientas = false;
            this.TArea.ShowImagen = false;
            this.TArea.ShowImprimir = false;
            this.TArea.ShowItem = false;
            this.TArea.ShowNuevo = true;
            this.TArea.ShowRemover = true;
            this.TArea.ShowSeleccionMultiple = true;
            this.TArea.Size = new System.Drawing.Size(800, 450);
            this.TArea.TabIndex = 0;
            // 
            // AreaCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TArea);
            this.Name = "AreaCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Áreas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.AreaCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Common.Forms.GridStandarControl TArea;
    }
}