﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class DepartamentoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.IsActivo = new Telerik.WinControls.UI.RadCheckBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.CuentaContable = new Telerik.WinControls.UI.RadTextBox();
            this.Clasificacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Responsable = new Telerik.WinControls.UI.RadTextBox();
            this.DescripcionLabel = new Telerik.WinControls.UI.RadLabel();
            this.CuentaContableLabel = new Telerik.WinControls.UI.RadLabel();
            this.ClasificacionLabel = new Telerik.WinControls.UI.RadLabel();
            this.ResposableLabel = new Telerik.WinControls.UI.RadLabel();
            this.TDepartamento_Guardar = new Telerik.WinControls.UI.RadButton();
            this.TDepartamento_Cerrar = new Telerik.WinControls.UI.RadButton();
            this.InformacionBox = new Telerik.WinControls.UI.RadGroupBox();
            this.AreaLabel = new Telerik.WinControls.UI.RadLabel();
            this.Area = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContableLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResposableLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDepartamento_Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDepartamento_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformacionBox)).BeginInit();
            this.InformacionBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AreaLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Area)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Area.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Area.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(332, 33);
            this.Encabezado.TabIndex = 191;
            this.Encabezado.TabStop = false;
            // 
            // IsActivo
            // 
            this.IsActivo.Location = new System.Drawing.Point(258, 19);
            this.IsActivo.Name = "IsActivo";
            this.IsActivo.Size = new System.Drawing.Size(51, 18);
            this.IsActivo.TabIndex = 192;
            this.IsActivo.Text = "Activo";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(83, 70);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(226, 20);
            this.Descripcion.TabIndex = 193;
            // 
            // CuentaContable
            // 
            this.CuentaContable.Location = new System.Drawing.Point(109, 96);
            this.CuentaContable.Name = "CuentaContable";
            this.CuentaContable.Size = new System.Drawing.Size(200, 20);
            this.CuentaContable.TabIndex = 193;
            // 
            // Clasificacion
            // 
            // 
            // Clasificacion.NestedRadGridView
            // 
            this.Clasificacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Clasificacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clasificacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Clasificacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Clasificacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Clasificacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Clasificacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Clasificacion.EditorControl.Name = "NestedRadGridView";
            this.Clasificacion.EditorControl.ReadOnly = true;
            this.Clasificacion.EditorControl.ShowGroupPanel = false;
            this.Clasificacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Clasificacion.EditorControl.TabIndex = 0;
            this.Clasificacion.Location = new System.Drawing.Point(109, 122);
            this.Clasificacion.Name = "Clasificacion";
            this.Clasificacion.Size = new System.Drawing.Size(200, 20);
            this.Clasificacion.TabIndex = 193;
            this.Clasificacion.TabStop = false;
            // 
            // Responsable
            // 
            this.Responsable.Location = new System.Drawing.Point(109, 148);
            this.Responsable.Name = "Responsable";
            this.Responsable.Size = new System.Drawing.Size(200, 20);
            this.Responsable.TabIndex = 193;
            // 
            // DescripcionLabel
            // 
            this.DescripcionLabel.Location = new System.Drawing.Point(13, 71);
            this.DescripcionLabel.Name = "DescripcionLabel";
            this.DescripcionLabel.Size = new System.Drawing.Size(64, 18);
            this.DescripcionLabel.TabIndex = 194;
            this.DescripcionLabel.Text = "Descripción";
            // 
            // CuentaContableLabel
            // 
            this.CuentaContableLabel.Location = new System.Drawing.Point(13, 97);
            this.CuentaContableLabel.Name = "CuentaContableLabel";
            this.CuentaContableLabel.Size = new System.Drawing.Size(90, 18);
            this.CuentaContableLabel.TabIndex = 194;
            this.CuentaContableLabel.Text = "Cuenta Contable";
            // 
            // ClasificacionLabel
            // 
            this.ClasificacionLabel.Location = new System.Drawing.Point(13, 123);
            this.ClasificacionLabel.Name = "ClasificacionLabel";
            this.ClasificacionLabel.Size = new System.Drawing.Size(68, 18);
            this.ClasificacionLabel.TabIndex = 194;
            this.ClasificacionLabel.Text = "Clasificación";
            // 
            // ResposableLabel
            // 
            this.ResposableLabel.Location = new System.Drawing.Point(13, 149);
            this.ResposableLabel.Name = "ResposableLabel";
            this.ResposableLabel.Size = new System.Drawing.Size(72, 18);
            this.ResposableLabel.TabIndex = 194;
            this.ResposableLabel.Text = "Responsable:";
            // 
            // TDepartamento_Guardar
            // 
            this.TDepartamento_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TDepartamento_Guardar.Location = new System.Drawing.Point(94, 238);
            this.TDepartamento_Guardar.Name = "TDepartamento_Guardar";
            this.TDepartamento_Guardar.Size = new System.Drawing.Size(110, 24);
            this.TDepartamento_Guardar.TabIndex = 195;
            this.TDepartamento_Guardar.Text = "Guardar";
            this.TDepartamento_Guardar.Click += new System.EventHandler(this.TDepartamento_Guardar_Click);
            // 
            // TDepartamento_Cerrar
            // 
            this.TDepartamento_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TDepartamento_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TDepartamento_Cerrar.Location = new System.Drawing.Point(210, 238);
            this.TDepartamento_Cerrar.Name = "TDepartamento_Cerrar";
            this.TDepartamento_Cerrar.Size = new System.Drawing.Size(110, 24);
            this.TDepartamento_Cerrar.TabIndex = 196;
            this.TDepartamento_Cerrar.Text = "Cerrar";
            this.TDepartamento_Cerrar.Click += new System.EventHandler(this.TDepartamento_Cerrar_Click);
            // 
            // InformacionBox
            // 
            this.InformacionBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.InformacionBox.Controls.Add(this.AreaLabel);
            this.InformacionBox.Controls.Add(this.ResposableLabel);
            this.InformacionBox.Controls.Add(this.Area);
            this.InformacionBox.Controls.Add(this.ClasificacionLabel);
            this.InformacionBox.Controls.Add(this.CuentaContableLabel);
            this.InformacionBox.Controls.Add(this.DescripcionLabel);
            this.InformacionBox.Controls.Add(this.Responsable);
            this.InformacionBox.Controls.Add(this.Clasificacion);
            this.InformacionBox.Controls.Add(this.CuentaContable);
            this.InformacionBox.Controls.Add(this.Descripcion);
            this.InformacionBox.Controls.Add(this.IsActivo);
            this.InformacionBox.HeaderText = "Información";
            this.InformacionBox.Location = new System.Drawing.Point(4, 39);
            this.InformacionBox.Name = "InformacionBox";
            this.InformacionBox.Size = new System.Drawing.Size(328, 181);
            this.InformacionBox.TabIndex = 197;
            this.InformacionBox.TabStop = false;
            this.InformacionBox.Text = "Información";
            // 
            // AreaLabel
            // 
            this.AreaLabel.Location = new System.Drawing.Point(13, 45);
            this.AreaLabel.Name = "AreaLabel";
            this.AreaLabel.Size = new System.Drawing.Size(32, 18);
            this.AreaLabel.TabIndex = 196;
            this.AreaLabel.Text = "Área:";
            // 
            // Area
            // 
            this.Area.DisplayMember = "Descriptor";
            // 
            // Area.NestedRadGridView
            // 
            this.Area.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Area.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Area.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Area.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Area.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Area.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Area.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Descriptor";
            gridViewTextBoxColumn1.HeaderText = "Descriptor";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Descriptor";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Id";
            gridViewTextBoxColumn2.HeaderText = "ID";
            gridViewTextBoxColumn2.Name = "Id";
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 140;
            this.Area.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Area.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Area.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Area.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Area.EditorControl.Name = "NestedRadGridView";
            this.Area.EditorControl.ReadOnly = true;
            this.Area.EditorControl.ShowGroupPanel = false;
            this.Area.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Area.EditorControl.TabIndex = 0;
            this.Area.Location = new System.Drawing.Point(90, 44);
            this.Area.Name = "Area";
            this.Area.Size = new System.Drawing.Size(219, 20);
            this.Area.TabIndex = 195;
            this.Area.TabStop = false;
            this.Area.ValueMember = "Id";
            // 
            // DepartamentoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TDepartamento_Cerrar;
            this.ClientSize = new System.Drawing.Size(332, 274);
            this.Controls.Add(this.InformacionBox);
            this.Controls.Add(this.TDepartamento_Guardar);
            this.Controls.Add(this.TDepartamento_Cerrar);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DepartamentoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Departamento";
            this.Load += new System.EventHandler(this.DepartamentoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContableLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResposableLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDepartamento_Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDepartamento_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformacionBox)).EndInit();
            this.InformacionBox.ResumeLayout(false);
            this.InformacionBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AreaLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Area.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Area.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Area)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadCheckBox IsActivo;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadTextBox CuentaContable;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Clasificacion;
        private Telerik.WinControls.UI.RadTextBox Responsable;
        private Telerik.WinControls.UI.RadLabel DescripcionLabel;
        private Telerik.WinControls.UI.RadLabel CuentaContableLabel;
        private Telerik.WinControls.UI.RadLabel ClasificacionLabel;
        private Telerik.WinControls.UI.RadLabel ResposableLabel;
        private Telerik.WinControls.UI.RadButton TDepartamento_Guardar;
        private Telerik.WinControls.UI.RadButton TDepartamento_Cerrar;
        private Telerik.WinControls.UI.RadGroupBox InformacionBox;
        private Telerik.WinControls.UI.RadLabel AreaLabel;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Area;
    }
}