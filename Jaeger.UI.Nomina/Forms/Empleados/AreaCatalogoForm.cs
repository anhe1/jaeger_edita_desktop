﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class AreaCatalogoForm : RadForm {
        protected Aplication.Nomina.Contracts.IAreaService _AreaService;
        protected BindingList<Domain.Empresa.Entities.AreaModel> _Areas;

        public AreaCatalogoForm() {
            InitializeComponent();
        }

        private void AreaCatalogoForm_Load(object sender, EventArgs e) {
            using (Builder.IAreaGridBuilder builder = new Builder.AreaGridBuilder()) {
                this.TArea.GridData.Columns.AddRange(builder.Templetes().Master().Build());
            }
            this.TArea.Nuevo.Click += TArea_Nuevo_Click;
            this.TArea.Editar.Click += TArea_Editar_Click;
            this.TArea.Actualizar.Click += TArea_Actualizar_Click;
            this.TArea.Cerrar.Click += TArea_Cerrar_Click;
        }

        private void TArea_Nuevo_Click(object sender, EventArgs e) {
            using (var form = new AreaForm(this._AreaService, new Domain.Empresa.Entities.AreaModel())) {
                form.ShowDialog(this);
            }
            this.TArea.GridData.DataSource = this._Areas;
        }

        private void TArea_Editar_Click(object sender, EventArgs e) {
            if (this.TArea.GridData.SelectedRows.Count == 0) {
                RadMessageBox.Show(this, "Seleccione un registro", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            var area = this.TArea.GridData.SelectedRows[0].DataBoundItem as Domain.Empresa.Entities.AreaModel;
            using (var form = new AreaForm(this._AreaService, area)) {
                form.ShowDialog(this);
            }
            this.TArea.GridData.DataSource = this._Areas;
        }

        private void TArea_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
            this.TArea.GridData.DataSource = this._Areas;
        }

        private void TArea_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consulta() {
            this._Areas = new BindingList<Domain.Empresa.Entities.AreaModel>(this._AreaService.GetList<Domain.Empresa.Entities.AreaModel>(new List<Domain.Base.Builder.IConditional>()).ToList());
        }
    }
}
