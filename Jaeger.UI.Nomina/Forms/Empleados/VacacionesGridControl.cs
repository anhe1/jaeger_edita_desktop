﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public class VacacionesGridControl :UI.Common.Forms.GridCommonControl {
        public RadMenuItem _Calendario = new RadMenuItem() { Text = "Calendario", Name = "Calendario" };
        public VacacionesGridControl() : base() {
            this.Load += VacacionesGridControl_Load;
        }

        private void VacacionesGridControl_Load(object sender, EventArgs e) {
            this.ShowHerramientas = true;
            this.ShowEjercicio = true;
            this.ShowPeriodo = false;
            this.ShowEditar = false;
            this.ShowCancelar = false;
            this.ShowNuevo = false;
            IRegistroVacacionesGridBuilder view = new RegistroVacacionesGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
            this.Herramientas.Items.Add(this._Calendario);
        }
    }
}
