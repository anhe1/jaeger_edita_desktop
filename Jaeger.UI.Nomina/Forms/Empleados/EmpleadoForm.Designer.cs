namespace Jaeger.UI.Nomina.Forms.Empleados
{
    partial class EmpleadoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject2 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ButtonRFC = new Telerik.WinControls.UI.RadButton();
            this.label21 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNumSeguridadSocial = new Telerik.WinControls.UI.RadTextBox();
            this.FechaNacimientoLabel = new Telerik.WinControls.UI.RadLabel();
            this.GeneroLabel = new Telerik.WinControls.UI.RadLabel();
            this.Genero = new Telerik.WinControls.UI.RadDropDownList();
            this.FechaNacimiento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.SegundoApellidoLabel = new Telerik.WinControls.UI.RadLabel();
            this.PrimerApellidoLabel = new Telerik.WinControls.UI.RadLabel();
            this.NombresLabel = new Telerik.WinControls.UI.RadLabel();
            this.RFCLabel = new Telerik.WinControls.UI.RadLabel();
            this.CURPLabel = new Telerik.WinControls.UI.RadLabel();
            this.ClaveLabel = new Telerik.WinControls.UI.RadLabel();
            this.NumeroLabel = new Telerik.WinControls.UI.RadLabel();
            this.TxbSitio = new Telerik.WinControls.UI.RadTextBox();
            this.TxbCorreo = new Telerik.WinControls.UI.RadTextBox();
            this.TxbTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.ApellidoMaterno = new Telerik.WinControls.UI.RadTextBox();
            this.ApellidoPaterno = new Telerik.WinControls.UI.RadTextBox();
            this.Nombre = new Telerik.WinControls.UI.RadTextBox();
            this.TxbRFC = new Telerik.WinControls.UI.RadTextBox();
            this.TxbCURP = new Telerik.WinControls.UI.RadTextBox();
            this.TxbClave = new Telerik.WinControls.UI.RadTextBox();
            this.TxbNum = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label25 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.switchActivo = new Telerik.WinControls.UI.RadToggleSwitch();
            this.CiudadNacimientoLabel = new Telerik.WinControls.UI.RadLabel();
            this.CiudadNacimiento = new Telerik.WinControls.UI.RadTextBox();
            this.EntidadFedNacimientoLabel = new Telerik.WinControls.UI.RadLabel();
            this.EntidadFedNacimiento = new Telerik.WinControls.UI.RadDropDownList();
            this.label30 = new Telerik.WinControls.UI.RadLabel();
            this.TxbAfore = new Telerik.WinControls.UI.RadTextBox();
            this.label29 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNumFonacot = new Telerik.WinControls.UI.RadTextBox();
            this.tabControl = new Telerik.WinControls.UI.RadPageView();
            this.pageGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.TxbUMF = new Telerik.WinControls.UI.RadTextBox();
            this.label49 = new Telerik.WinControls.UI.RadLabel();
            this.TxbTelefono2 = new Telerik.WinControls.UI.RadTextBox();
            this.pagePago = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridBanco = new Telerik.WinControls.UI.RadGridView();
            this.TBanco = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.pageContrato = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridViewEmpleados = new Telerik.WinControls.UI.RadGridView();
            this.TContrato = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.pagePrestacion = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.TPrestacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.StatusBar = new Telerik.WinControls.UI.RadStatusStrip();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBarElement();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.TEmpleado = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumSeguridadSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimientoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneroLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Genero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SegundoApellidoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimerApellidoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombresLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURPLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSitio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApellidoMaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApellidoPaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox)).BeginInit();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimientoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimientoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAfore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumFonacot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbUMF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono2)).BeginInit();
            this.pagePago.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBanco.MasterTemplate)).BeginInit();
            this.pageContrato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados.MasterTemplate)).BeginInit();
            this.pagePrestacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonRFC
            // 
            this.ButtonRFC.Location = new System.Drawing.Point(141, 153);
            this.ButtonRFC.Name = "ButtonRFC";
            this.ButtonRFC.Size = new System.Drawing.Size(38, 23);
            this.ButtonRFC.TabIndex = 101;
            this.ButtonRFC.Text = "RFC";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(270, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(122, 18);
            this.label21.TabIndex = 93;
            this.label21.Text = "Núm. Seguridad Social:";
            // 
            // TxbNumSeguridadSocial
            // 
            this.TxbNumSeguridadSocial.Location = new System.Drawing.Point(270, 79);
            this.TxbNumSeguridadSocial.Name = "TxbNumSeguridadSocial";
            this.TxbNumSeguridadSocial.Size = new System.Drawing.Size(125, 20);
            this.TxbNumSeguridadSocial.TabIndex = 92;
            // 
            // FechaNacimientoLabel
            // 
            this.FechaNacimientoLabel.Location = new System.Drawing.Point(139, 94);
            this.FechaNacimientoLabel.Name = "FechaNacimientoLabel";
            this.FechaNacimientoLabel.Size = new System.Drawing.Size(114, 18);
            this.FechaNacimientoLabel.TabIndex = 76;
            this.FechaNacimientoLabel.Text = "Fecha de Nacimiento:";
            // 
            // GeneroLabel
            // 
            this.GeneroLabel.Location = new System.Drawing.Point(9, 94);
            this.GeneroLabel.Name = "GeneroLabel";
            this.GeneroLabel.Size = new System.Drawing.Size(45, 18);
            this.GeneroLabel.TabIndex = 75;
            this.GeneroLabel.Text = "Genero:";
            // 
            // Genero
            // 
            this.Genero.Location = new System.Drawing.Point(12, 112);
            this.Genero.Name = "Genero";
            this.Genero.Size = new System.Drawing.Size(125, 20);
            this.Genero.TabIndex = 74;
            // 
            // FechaNacimiento
            // 
            this.FechaNacimiento.Location = new System.Drawing.Point(142, 112);
            this.FechaNacimiento.Name = "FechaNacimiento";
            this.FechaNacimiento.Size = new System.Drawing.Size(175, 20);
            this.FechaNacimiento.TabIndex = 72;
            this.FechaNacimiento.TabStop = false;
            this.FechaNacimiento.Text = "lunes, 21 de enero de 2019";
            this.FechaNacimiento.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 174);
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(269, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 18);
            this.label10.TabIndex = 71;
            this.label10.Text = "Sitio:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(6, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 18);
            this.label9.TabIndex = 70;
            this.label9.Text = "Correo:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 18);
            this.label8.TabIndex = 69;
            this.label8.Text = "Telefono 1:";
            // 
            // SegundoApellidoLabel
            // 
            this.SegundoApellidoLabel.Location = new System.Drawing.Point(335, 51);
            this.SegundoApellidoLabel.Name = "SegundoApellidoLabel";
            this.SegundoApellidoLabel.Size = new System.Drawing.Size(96, 18);
            this.SegundoApellidoLabel.TabIndex = 68;
            this.SegundoApellidoLabel.Text = "Apellido Materno:";
            // 
            // PrimerApellidoLabel
            // 
            this.PrimerApellidoLabel.Location = new System.Drawing.Point(172, 51);
            this.PrimerApellidoLabel.Name = "PrimerApellidoLabel";
            this.PrimerApellidoLabel.Size = new System.Drawing.Size(92, 18);
            this.PrimerApellidoLabel.TabIndex = 67;
            this.PrimerApellidoLabel.Text = "Apellido Paterno:";
            // 
            // NombresLabel
            // 
            this.NombresLabel.Location = new System.Drawing.Point(6, 51);
            this.NombresLabel.Name = "NombresLabel";
            this.NombresLabel.Size = new System.Drawing.Size(62, 18);
            this.NombresLabel.TabIndex = 66;
            this.NombresLabel.Text = "Nombre(s):";
            // 
            // RFCLabel
            // 
            this.RFCLabel.Location = new System.Drawing.Point(9, 136);
            this.RFCLabel.Name = "RFCLabel";
            this.RFCLabel.Size = new System.Drawing.Size(50, 18);
            this.RFCLabel.TabIndex = 65;
            this.RFCLabel.Text = "RFC (F7):";
            // 
            // CURPLabel
            // 
            this.CURPLabel.Location = new System.Drawing.Point(182, 136);
            this.CURPLabel.Name = "CURPLabel";
            this.CURPLabel.Size = new System.Drawing.Size(58, 18);
            this.CURPLabel.TabIndex = 64;
            this.CURPLabel.Text = "CURP (F7):";
            // 
            // ClaveLabel
            // 
            this.ClaveLabel.Location = new System.Drawing.Point(106, 20);
            this.ClaveLabel.Name = "ClaveLabel";
            this.ClaveLabel.Size = new System.Drawing.Size(35, 18);
            this.ClaveLabel.TabIndex = 63;
            this.ClaveLabel.Text = "Clave:";
            // 
            // NumeroLabel
            // 
            this.NumeroLabel.Location = new System.Drawing.Point(6, 20);
            this.NumeroLabel.Name = "NumeroLabel";
            this.NumeroLabel.Size = new System.Drawing.Size(36, 18);
            this.NumeroLabel.TabIndex = 62;
            this.NumeroLabel.Text = "Núm.:";
            // 
            // TxbSitio
            // 
            this.TxbSitio.Location = new System.Drawing.Point(269, 123);
            this.TxbSitio.Name = "TxbSitio";
            this.TxbSitio.Size = new System.Drawing.Size(257, 20);
            this.TxbSitio.TabIndex = 61;
            // 
            // TxbCorreo
            // 
            this.TxbCorreo.Location = new System.Drawing.Point(6, 123);
            this.TxbCorreo.Name = "TxbCorreo";
            this.TxbCorreo.Size = new System.Drawing.Size(256, 20);
            this.TxbCorreo.TabIndex = 60;
            // 
            // TxbTelefono
            // 
            this.TxbTelefono.Location = new System.Drawing.Point(6, 35);
            this.TxbTelefono.Name = "TxbTelefono";
            this.TxbTelefono.Size = new System.Drawing.Size(176, 20);
            this.TxbTelefono.TabIndex = 59;
            // 
            // ApellidoMaterno
            // 
            this.ApellidoMaterno.Location = new System.Drawing.Point(335, 68);
            this.ApellidoMaterno.Name = "ApellidoMaterno";
            this.ApellidoMaterno.Size = new System.Drawing.Size(162, 20);
            this.ApellidoMaterno.TabIndex = 58;
            // 
            // ApellidoPaterno
            // 
            this.ApellidoPaterno.Location = new System.Drawing.Point(172, 68);
            this.ApellidoPaterno.Name = "ApellidoPaterno";
            this.ApellidoPaterno.Size = new System.Drawing.Size(157, 20);
            this.ApellidoPaterno.TabIndex = 57;
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(9, 68);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(157, 20);
            this.Nombre.TabIndex = 56;
            // 
            // TxbRFC
            // 
            this.TxbRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbRFC.Location = new System.Drawing.Point(12, 154);
            this.TxbRFC.MaxLength = 16;
            this.TxbRFC.Name = "TxbRFC";
            this.TxbRFC.Size = new System.Drawing.Size(125, 20);
            this.TxbRFC.TabIndex = 55;
            // 
            // TxbCURP
            // 
            this.TxbCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbCURP.Location = new System.Drawing.Point(185, 154);
            this.TxbCURP.MaxLength = 18;
            this.TxbCURP.Name = "TxbCURP";
            this.TxbCURP.Size = new System.Drawing.Size(132, 20);
            this.TxbCURP.TabIndex = 54;
            // 
            // TxbClave
            // 
            this.TxbClave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbClave.Location = new System.Drawing.Point(149, 19);
            this.TxbClave.Name = "TxbClave";
            this.TxbClave.Size = new System.Drawing.Size(64, 20);
            this.TxbClave.TabIndex = 53;
            // 
            // TxbNum
            // 
            this.TxbNum.Location = new System.Drawing.Point(47, 19);
            this.TxbNum.Mask = "d6";
            this.TxbNum.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbNum.Name = "TxbNum";
            this.TxbNum.NullText = "000000";
            this.TxbNum.ReadOnly = true;
            this.TxbNum.Size = new System.Drawing.Size(53, 20);
            this.TxbNum.TabIndex = 52;
            this.TxbNum.TabStop = false;
            this.TxbNum.Text = "000000";
            this.TxbNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(46, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(195, 18);
            this.label25.TabIndex = 104;
            this.label25.Text = "Información del Empleado Registrado";
            // 
            // groupBox
            // 
            this.groupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox.Controls.Add(this.pictureBox1);
            this.groupBox.Controls.Add(this.switchActivo);
            this.groupBox.Controls.Add(this.NumeroLabel);
            this.groupBox.Controls.Add(this.TxbNum);
            this.groupBox.Controls.Add(this.TxbClave);
            this.groupBox.Controls.Add(this.Nombre);
            this.groupBox.Controls.Add(this.CiudadNacimientoLabel);
            this.groupBox.Controls.Add(this.ApellidoPaterno);
            this.groupBox.Controls.Add(this.CiudadNacimiento);
            this.groupBox.Controls.Add(this.ApellidoMaterno);
            this.groupBox.Controls.Add(this.EntidadFedNacimientoLabel);
            this.groupBox.Controls.Add(this.ClaveLabel);
            this.groupBox.Controls.Add(this.EntidadFedNacimiento);
            this.groupBox.Controls.Add(this.NombresLabel);
            this.groupBox.Controls.Add(this.PrimerApellidoLabel);
            this.groupBox.Controls.Add(this.TxbCURP);
            this.groupBox.Controls.Add(this.SegundoApellidoLabel);
            this.groupBox.Controls.Add(this.GeneroLabel);
            this.groupBox.Controls.Add(this.FechaNacimientoLabel);
            this.groupBox.Controls.Add(this.RFCLabel);
            this.groupBox.Controls.Add(this.FechaNacimiento);
            this.groupBox.Controls.Add(this.ButtonRFC);
            this.groupBox.Controls.Add(this.CURPLabel);
            this.groupBox.Controls.Add(this.Genero);
            this.groupBox.Controls.Add(this.TxbRFC);
            this.groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox.HeaderText = "";
            this.groupBox.Location = new System.Drawing.Point(0, 65);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(674, 184);
            this.groupBox.TabIndex = 106;
            this.groupBox.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(524, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 118);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 129;
            this.pictureBox1.TabStop = false;
            // 
            // switchActivo
            // 
            this.switchActivo.Location = new System.Drawing.Point(447, 19);
            this.switchActivo.Name = "switchActivo";
            this.switchActivo.Size = new System.Drawing.Size(50, 20);
            this.switchActivo.TabIndex = 110;
            // 
            // CiudadNacimientoLabel
            // 
            this.CiudadNacimientoLabel.Location = new System.Drawing.Point(323, 136);
            this.CiudadNacimientoLabel.Name = "CiudadNacimientoLabel";
            this.CiudadNacimientoLabel.Size = new System.Drawing.Size(118, 18);
            this.CiudadNacimientoLabel.TabIndex = 128;
            this.CiudadNacimientoLabel.Text = "Ciudad de Nacimiento";
            // 
            // CiudadNacimiento
            // 
            this.CiudadNacimiento.Location = new System.Drawing.Point(323, 154);
            this.CiudadNacimiento.Name = "CiudadNacimiento";
            this.CiudadNacimiento.Size = new System.Drawing.Size(174, 20);
            this.CiudadNacimiento.TabIndex = 127;
            // 
            // EntidadFedNacimientoLabel
            // 
            this.EntidadFedNacimientoLabel.Location = new System.Drawing.Point(323, 94);
            this.EntidadFedNacimientoLabel.Name = "EntidadFedNacimientoLabel";
            this.EntidadFedNacimientoLabel.Size = new System.Drawing.Size(144, 18);
            this.EntidadFedNacimientoLabel.TabIndex = 126;
            this.EntidadFedNacimientoLabel.Text = "Entidad Fed. de Nacimiento";
            // 
            // EntidadFedNacimiento
            // 
            this.EntidadFedNacimiento.Location = new System.Drawing.Point(323, 112);
            this.EntidadFedNacimiento.Name = "EntidadFedNacimiento";
            this.EntidadFedNacimiento.Size = new System.Drawing.Size(176, 20);
            this.EntidadFedNacimiento.TabIndex = 125;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(137, 61);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 18);
            this.label30.TabIndex = 108;
            this.label30.Text = "Afore";
            // 
            // TxbAfore
            // 
            this.TxbAfore.Location = new System.Drawing.Point(137, 79);
            this.TxbAfore.Name = "TxbAfore";
            this.TxbAfore.Size = new System.Drawing.Size(125, 20);
            this.TxbAfore.TabIndex = 107;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(6, 61);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 18);
            this.label29.TabIndex = 109;
            this.label29.Text = "Núm. FONACOT";
            // 
            // TxbNumFonacot
            // 
            this.TxbNumFonacot.Location = new System.Drawing.Point(6, 79);
            this.TxbNumFonacot.Name = "TxbNumFonacot";
            this.TxbNumFonacot.Size = new System.Drawing.Size(125, 20);
            this.TxbNumFonacot.TabIndex = 108;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.pageGeneral);
            this.tabControl.Controls.Add(this.pagePago);
            this.tabControl.Controls.Add(this.pageContrato);
            this.tabControl.Controls.Add(this.pagePrestacion);
            this.tabControl.DefaultPage = this.pageGeneral;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 249);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedPage = this.pageGeneral;
            this.tabControl.Size = new System.Drawing.Size(674, 271);
            this.tabControl.TabIndex = 123;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pageGeneral
            // 
            this.pageGeneral.Controls.Add(this.radLabel1);
            this.pageGeneral.Controls.Add(this.TxbUMF);
            this.pageGeneral.Controls.Add(this.label49);
            this.pageGeneral.Controls.Add(this.TxbTelefono2);
            this.pageGeneral.Controls.Add(this.label30);
            this.pageGeneral.Controls.Add(this.label10);
            this.pageGeneral.Controls.Add(this.TxbNumSeguridadSocial);
            this.pageGeneral.Controls.Add(this.label9);
            this.pageGeneral.Controls.Add(this.label8);
            this.pageGeneral.Controls.Add(this.TxbAfore);
            this.pageGeneral.Controls.Add(this.TxbSitio);
            this.pageGeneral.Controls.Add(this.label29);
            this.pageGeneral.Controls.Add(this.TxbNumFonacot);
            this.pageGeneral.Controls.Add(this.label21);
            this.pageGeneral.Controls.Add(this.TxbCorreo);
            this.pageGeneral.Controls.Add(this.TxbTelefono);
            this.pageGeneral.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.pageGeneral.Location = new System.Drawing.Point(10, 37);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(653, 223);
            this.pageGeneral.Text = "General";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(188, 17);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(62, 18);
            this.radLabel1.TabIndex = 132;
            this.radLabel1.Text = "Telefono 2:";
            // 
            // TxbUMF
            // 
            this.TxbUMF.Location = new System.Drawing.Point(401, 79);
            this.TxbUMF.Name = "TxbUMF";
            this.TxbUMF.Size = new System.Drawing.Size(125, 20);
            this.TxbUMF.TabIndex = 129;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(401, 61);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(124, 18);
            this.label49.TabIndex = 130;
            this.label49.Text = "Unidad Medica Familiar";
            // 
            // TxbTelefono2
            // 
            this.TxbTelefono2.Location = new System.Drawing.Point(188, 35);
            this.TxbTelefono2.Name = "TxbTelefono2";
            this.TxbTelefono2.Size = new System.Drawing.Size(168, 20);
            this.TxbTelefono2.TabIndex = 131;
            // 
            // pagePago
            // 
            this.pagePago.Controls.Add(this.gridBanco);
            this.pagePago.Controls.Add(this.TBanco);
            this.pagePago.ItemSize = new System.Drawing.SizeF(106F, 28F);
            this.pagePago.Location = new System.Drawing.Point(10, 37);
            this.pagePago.Name = "pagePago";
            this.pagePago.Size = new System.Drawing.Size(653, 223);
            this.pagePago.Text = "Cuentas Bancarias";
            // 
            // gridBanco
            // 
            this.gridBanco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridBanco.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridBanco.MasterTemplate.AutoGenerateColumns = false;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = false";
            expressionFormattingObject1.Name = "Inactivo";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "IdEmpleado";
            gridViewTextBoxColumn1.FormatString = "{0:0000#}";
            gridViewTextBoxColumn1.HeaderText = "Núm. ";
            gridViewTextBoxColumn1.Name = "IdEmpleado1";
            gridViewTextBoxColumn1.Width = 75;
            gridViewTextBoxColumn2.Expression = "";
            gridViewTextBoxColumn2.FieldName = "ClaveTipoRegimen";
            gridViewTextBoxColumn2.HeaderText = "T. Régimen";
            gridViewTextBoxColumn2.Name = "ClaveTipoRegimen";
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "ClaveRiesgoPuesto";
            gridViewTextBoxColumn3.HeaderText = "Riesgo Puesto";
            gridViewTextBoxColumn3.Name = "ClaveRiesgoPuesto";
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "ClavePeriodicidadPago";
            gridViewTextBoxColumn4.HeaderText = "P. Pago";
            gridViewTextBoxColumn4.Name = "ClavePeriodicidadPago";
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "ClaveMetodoPago";
            gridViewTextBoxColumn5.HeaderText = "Método Pago";
            gridViewTextBoxColumn5.Name = "ClaveMetodoPago";
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.FieldName = "ClaveTipoContrato";
            gridViewTextBoxColumn6.HeaderText = "T. Contrato";
            gridViewTextBoxColumn6.Name = "ClaveTipoContrato";
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.FieldName = "ClaveTipoJornada";
            gridViewTextBoxColumn7.HeaderText = "T. Jornada";
            gridViewTextBoxColumn7.Name = "ClaveTipoJornada";
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.FieldName = "Departamento";
            gridViewTextBoxColumn8.HeaderText = "Departamento";
            gridViewTextBoxColumn8.Name = "Departamento";
            gridViewTextBoxColumn8.Width = 180;
            gridViewTextBoxColumn9.FieldName = "Puesto";
            gridViewTextBoxColumn9.HeaderText = "Puesto";
            gridViewTextBoxColumn9.Name = "Puesto";
            gridViewTextBoxColumn9.Width = 120;
            gridViewDateTimeColumn1.FieldName = "FecInicioRelLaboral";
            gridViewDateTimeColumn1.HeaderText = "In. Rel. Laboral";
            gridViewDateTimeColumn1.Name = "FecInicioRelLaboral";
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "SalarioDiario";
            gridViewTextBoxColumn10.FormatString = "{0:n2}";
            gridViewTextBoxColumn10.HeaderText = "SD";
            gridViewTextBoxColumn10.Name = "SalarioDiario";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn11.FormatString = "{0:n2}";
            gridViewTextBoxColumn11.HeaderText = "SDI";
            gridViewTextBoxColumn11.Name = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "Jornadas";
            gridViewTextBoxColumn12.HeaderText = "J. Trabajo";
            gridViewTextBoxColumn12.Name = "Jornadas";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn13.FieldName = "SalarioBase";
            gridViewTextBoxColumn13.HeaderText = "S. Base";
            gridViewTextBoxColumn13.Name = "SalarioBase";
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn14.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn14.FieldName = "FechaNuevo";
            gridViewTextBoxColumn14.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn14.HeaderText = "Fc. Sist.";
            gridViewTextBoxColumn14.Name = "FechaNuevo";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.FieldName = "Creo";
            gridViewTextBoxColumn15.HeaderText = "Creó";
            gridViewTextBoxColumn15.Name = "Creo";
            gridViewTextBoxColumn15.Width = 65;
            gridViewTextBoxColumn16.FieldName = "Modifica";
            gridViewTextBoxColumn16.HeaderText = "Modifica";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Modifica";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn16.Width = 65;
            gridViewTextBoxColumn17.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn17.FieldName = "FechaModifica";
            gridViewTextBoxColumn17.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "FechaModifica";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn17.Width = 75;
            this.gridBanco.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17});
            this.gridBanco.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridBanco.Name = "gridBanco";
            this.gridBanco.Size = new System.Drawing.Size(653, 193);
            this.gridBanco.TabIndex = 130;
            // 
            // TBanco
            // 
            this.TBanco.Dock = System.Windows.Forms.DockStyle.Top;
            this.TBanco.Etiqueta = "";
            this.TBanco.Location = new System.Drawing.Point(0, 0);
            this.TBanco.Name = "TBanco";
            this.TBanco.ReadOnly = false;
            this.TBanco.ShowActualizar = false;
            this.TBanco.ShowAutorizar = false;
            this.TBanco.ShowCerrar = false;
            this.TBanco.ShowEditar = true;
            this.TBanco.ShowExportarExcel = false;
            this.TBanco.ShowFiltro = false;
            this.TBanco.ShowGuardar = false;
            this.TBanco.ShowHerramientas = false;
            this.TBanco.ShowImagen = false;
            this.TBanco.ShowImprimir = true;
            this.TBanco.ShowNuevo = true;
            this.TBanco.ShowRemover = false;
            this.TBanco.Size = new System.Drawing.Size(653, 30);
            this.TBanco.TabIndex = 129;
            // 
            // pageContrato
            // 
            this.pageContrato.Controls.Add(this.gridViewEmpleados);
            this.pageContrato.Controls.Add(this.TContrato);
            this.pageContrato.ItemSize = new System.Drawing.SizeF(65F, 28F);
            this.pageContrato.Location = new System.Drawing.Point(10, 37);
            this.pageContrato.Name = "pageContrato";
            this.pageContrato.Size = new System.Drawing.Size(653, 223);
            this.pageContrato.Text = "Contratos";
            // 
            // gridViewEmpleados
            // 
            this.gridViewEmpleados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridViewEmpleados.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridViewEmpleados.MasterTemplate.AutoGenerateColumns = false;
            expressionFormattingObject2.ApplyToRow = true;
            expressionFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.Expression = "Activo = false";
            expressionFormattingObject2.Name = "Inactivo";
            expressionFormattingObject2.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            expressionFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(expressionFormattingObject2);
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "Activo";
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn18.FieldName = "IdEmpleado";
            gridViewTextBoxColumn18.FormatString = "{0:0000#}";
            gridViewTextBoxColumn18.HeaderText = "Núm. ";
            gridViewTextBoxColumn18.Name = "IdEmpleado1";
            gridViewTextBoxColumn18.Width = 75;
            gridViewTextBoxColumn19.Expression = "";
            gridViewTextBoxColumn19.FieldName = "ClaveTipoRegimen";
            gridViewTextBoxColumn19.HeaderText = "T. Régimen";
            gridViewTextBoxColumn19.Name = "ClaveTipoRegimen";
            gridViewTextBoxColumn19.Width = 75;
            gridViewTextBoxColumn20.FieldName = "ClaveRiesgoPuesto";
            gridViewTextBoxColumn20.HeaderText = "Riesgo Puesto";
            gridViewTextBoxColumn20.Name = "ClaveRiesgoPuesto";
            gridViewTextBoxColumn20.Width = 75;
            gridViewTextBoxColumn21.FieldName = "ClavePeriodicidadPago";
            gridViewTextBoxColumn21.HeaderText = "P. Pago";
            gridViewTextBoxColumn21.Name = "ClavePeriodicidadPago";
            gridViewTextBoxColumn21.Width = 75;
            gridViewTextBoxColumn22.FieldName = "ClaveMetodoPago";
            gridViewTextBoxColumn22.HeaderText = "Método Pago";
            gridViewTextBoxColumn22.Name = "ClaveMetodoPago";
            gridViewTextBoxColumn22.Width = 75;
            gridViewTextBoxColumn23.FieldName = "ClaveTipoContrato";
            gridViewTextBoxColumn23.HeaderText = "T. Contrato";
            gridViewTextBoxColumn23.Name = "ClaveTipoContrato";
            gridViewTextBoxColumn23.Width = 75;
            gridViewTextBoxColumn24.FieldName = "ClaveTipoJornada";
            gridViewTextBoxColumn24.HeaderText = "T. Jornada";
            gridViewTextBoxColumn24.Name = "ClaveTipoJornada";
            gridViewTextBoxColumn24.Width = 75;
            gridViewTextBoxColumn25.FieldName = "Departamento";
            gridViewTextBoxColumn25.HeaderText = "Departamento";
            gridViewTextBoxColumn25.Name = "Departamento";
            gridViewTextBoxColumn25.Width = 180;
            gridViewTextBoxColumn26.FieldName = "Puesto";
            gridViewTextBoxColumn26.HeaderText = "Puesto";
            gridViewTextBoxColumn26.Name = "Puesto";
            gridViewTextBoxColumn26.Width = 120;
            gridViewDateTimeColumn2.FieldName = "FecInicioRelLaboral";
            gridViewDateTimeColumn2.HeaderText = "In. Rel. Laboral";
            gridViewDateTimeColumn2.Name = "FecInicioRelLaboral";
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "SalarioDiario";
            gridViewTextBoxColumn27.FormatString = "{0:n2}";
            gridViewTextBoxColumn27.HeaderText = "SD";
            gridViewTextBoxColumn27.Name = "SalarioDiario";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 75;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn28.FormatString = "{0:n2}";
            gridViewTextBoxColumn28.HeaderText = "SDI";
            gridViewTextBoxColumn28.Name = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 75;
            gridViewTextBoxColumn29.DataType = typeof(int);
            gridViewTextBoxColumn29.FieldName = "Jornadas";
            gridViewTextBoxColumn29.HeaderText = "J. Trabajo";
            gridViewTextBoxColumn29.Name = "Jornadas";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn30.FieldName = "SalarioBase";
            gridViewTextBoxColumn30.HeaderText = "S. Base";
            gridViewTextBoxColumn30.Name = "SalarioBase";
            gridViewTextBoxColumn30.Width = 75;
            gridViewTextBoxColumn31.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn31.FieldName = "FechaNuevo";
            gridViewTextBoxColumn31.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn31.HeaderText = "Fc. Sist.";
            gridViewTextBoxColumn31.Name = "FechaNuevo";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn31.Width = 75;
            gridViewTextBoxColumn32.FieldName = "Creo";
            gridViewTextBoxColumn32.HeaderText = "Creó";
            gridViewTextBoxColumn32.Name = "Creo";
            gridViewTextBoxColumn32.Width = 65;
            gridViewTextBoxColumn33.FieldName = "Modifica";
            gridViewTextBoxColumn33.HeaderText = "Modifica";
            gridViewTextBoxColumn33.IsVisible = false;
            gridViewTextBoxColumn33.Name = "Modifica";
            gridViewTextBoxColumn33.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn33.Width = 65;
            gridViewTextBoxColumn34.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn34.FieldName = "FechaModifica";
            gridViewTextBoxColumn34.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn34.IsVisible = false;
            gridViewTextBoxColumn34.Name = "FechaModifica";
            gridViewTextBoxColumn34.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn34.Width = 75;
            this.gridViewEmpleados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34});
            this.gridViewEmpleados.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridViewEmpleados.Name = "gridViewEmpleados";
            this.gridViewEmpleados.Size = new System.Drawing.Size(653, 193);
            this.gridViewEmpleados.TabIndex = 128;
            // 
            // TContrato
            // 
            this.TContrato.Dock = System.Windows.Forms.DockStyle.Top;
            this.TContrato.Etiqueta = "";
            this.TContrato.Location = new System.Drawing.Point(0, 0);
            this.TContrato.Name = "TContrato";
            this.TContrato.ReadOnly = false;
            this.TContrato.ShowActualizar = false;
            this.TContrato.ShowAutorizar = false;
            this.TContrato.ShowCerrar = false;
            this.TContrato.ShowEditar = true;
            this.TContrato.ShowExportarExcel = false;
            this.TContrato.ShowFiltro = false;
            this.TContrato.ShowGuardar = false;
            this.TContrato.ShowHerramientas = false;
            this.TContrato.ShowImagen = false;
            this.TContrato.ShowImprimir = true;
            this.TContrato.ShowNuevo = true;
            this.TContrato.ShowRemover = false;
            this.TContrato.Size = new System.Drawing.Size(653, 30);
            this.TContrato.TabIndex = 127;
            // 
            // pagePrestacion
            // 
            this.pagePrestacion.Controls.Add(this.gridConceptos);
            this.pagePrestacion.Controls.Add(this.TPrestacion);
            this.pagePrestacion.ItemSize = new System.Drawing.SizeF(79F, 28F);
            this.pagePrestacion.Location = new System.Drawing.Point(10, 37);
            this.pagePrestacion.Name = "pagePrestacion";
            this.pagePrestacion.Size = new System.Drawing.Size(653, 223);
            this.pagePrestacion.Text = "Prestaciones";
            // 
            // gridConceptos
            // 
            this.gridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn35.FieldName = "Id";
            gridViewTextBoxColumn35.HeaderText = "Id";
            gridViewTextBoxColumn35.IsVisible = false;
            gridViewTextBoxColumn35.Name = "Id";
            gridViewCheckBoxColumn3.FieldName = "Activo";
            gridViewCheckBoxColumn3.HeaderText = "Activo";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "Activo";
            gridViewTextBoxColumn36.FieldName = "IdEmpleado";
            gridViewTextBoxColumn36.HeaderText = "IdEmpleado";
            gridViewTextBoxColumn36.IsVisible = false;
            gridViewTextBoxColumn36.Name = "IdEmpleado";
            gridViewMultiComboBoxColumn1.DataType = typeof(int);
            gridViewMultiComboBoxColumn1.FieldName = "IdConcepto";
            gridViewMultiComboBoxColumn1.HeaderText = "Concepto";
            gridViewMultiComboBoxColumn1.Name = "IdConcepto";
            gridViewMultiComboBoxColumn1.Width = 300;
            gridViewTextBoxColumn37.DataType = typeof(decimal);
            gridViewTextBoxColumn37.FieldName = "ImporteGravado";
            gridViewTextBoxColumn37.FormatString = "{0:n}";
            gridViewTextBoxColumn37.HeaderText = "Monto Fijo";
            gridViewTextBoxColumn37.Name = "ImporteGravado";
            gridViewTextBoxColumn37.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn37.Width = 85;
            this.gridConceptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn35,
            gridViewCheckBoxColumn3,
            gridViewTextBoxColumn36,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn37});
            this.gridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridConceptos.Name = "gridConceptos";
            this.gridConceptos.Size = new System.Drawing.Size(653, 193);
            this.gridConceptos.TabIndex = 128;
            this.gridConceptos.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridViewConceptos_CellBeginEdit);
            // 
            // TPrestacion
            // 
            this.TPrestacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPrestacion.Etiqueta = "";
            this.TPrestacion.Location = new System.Drawing.Point(0, 0);
            this.TPrestacion.Name = "TPrestacion";
            this.TPrestacion.ReadOnly = false;
            this.TPrestacion.ShowActualizar = false;
            this.TPrestacion.ShowAutorizar = false;
            this.TPrestacion.ShowCerrar = false;
            this.TPrestacion.ShowEditar = false;
            this.TPrestacion.ShowExportarExcel = false;
            this.TPrestacion.ShowFiltro = true;
            this.TPrestacion.ShowGuardar = false;
            this.TPrestacion.ShowHerramientas = false;
            this.TPrestacion.ShowImagen = false;
            this.TPrestacion.ShowImprimir = false;
            this.TPrestacion.ShowNuevo = true;
            this.TPrestacion.ShowRemover = true;
            this.TPrestacion.Size = new System.Drawing.Size(653, 30);
            this.TPrestacion.TabIndex = 127;
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Espera});
            this.StatusBar.Location = new System.Drawing.Point(0, 520);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(674, 26);
            this.StatusBar.SizingGrip = false;
            this.StatusBar.TabIndex = 125;
            // 
            // Espera
            // 
            this.Espera.Name = "Espera";
            this.StatusBar.SetSpring(this.Espera, false);
            this.Espera.Text = "...";
            this.Espera.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(674, 35);
            this.Encabezado.TabIndex = 102;
            this.Encabezado.TabStop = false;
            // 
            // TEmpleado
            // 
            this.TEmpleado.Dock = System.Windows.Forms.DockStyle.Top;
            this.TEmpleado.Etiqueta = "";
            this.TEmpleado.Location = new System.Drawing.Point(0, 35);
            this.TEmpleado.Name = "TEmpleado";
            this.TEmpleado.ReadOnly = false;
            this.TEmpleado.ShowActualizar = true;
            this.TEmpleado.ShowAutorizar = false;
            this.TEmpleado.ShowCerrar = true;
            this.TEmpleado.ShowEditar = false;
            this.TEmpleado.ShowExportarExcel = false;
            this.TEmpleado.ShowFiltro = false;
            this.TEmpleado.ShowGuardar = true;
            this.TEmpleado.ShowHerramientas = true;
            this.TEmpleado.ShowImagen = false;
            this.TEmpleado.ShowImprimir = true;
            this.TEmpleado.ShowNuevo = true;
            this.TEmpleado.ShowRemover = false;
            this.TEmpleado.Size = new System.Drawing.Size(674, 30);
            this.TEmpleado.TabIndex = 126;
            // 
            // EmpleadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 546);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.TEmpleado);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.StatusBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmpleadoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Empleado";
            this.Load += new System.EventHandler(this.EmpleadoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumSeguridadSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimientoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneroLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Genero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SegundoApellidoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimerApellidoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombresLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURPLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSitio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApellidoMaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApellidoPaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimientoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimientoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAfore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNumFonacot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.pageGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbUMF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono2)).EndInit();
            this.pagePago.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridBanco.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBanco)).EndInit();
            this.pageContrato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados)).EndInit();
            this.pagePrestacion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton ButtonRFC;
        private Telerik.WinControls.UI.RadLabel label21;
        private Telerik.WinControls.UI.RadTextBox TxbNumSeguridadSocial;
        private Telerik.WinControls.UI.RadLabel FechaNacimientoLabel;
        private Telerik.WinControls.UI.RadLabel GeneroLabel;
        private Telerik.WinControls.UI.RadDropDownList Genero;
        private Telerik.WinControls.UI.RadDateTimePicker FechaNacimiento;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel SegundoApellidoLabel;
        private Telerik.WinControls.UI.RadLabel PrimerApellidoLabel;
        private Telerik.WinControls.UI.RadLabel NombresLabel;
        private Telerik.WinControls.UI.RadLabel RFCLabel;
        private Telerik.WinControls.UI.RadLabel CURPLabel;
        private Telerik.WinControls.UI.RadLabel ClaveLabel;
        private Telerik.WinControls.UI.RadLabel NumeroLabel;
        private Telerik.WinControls.UI.RadTextBox TxbSitio;
        private Telerik.WinControls.UI.RadTextBox TxbCorreo;
        private Telerik.WinControls.UI.RadTextBox TxbTelefono;
        private Telerik.WinControls.UI.RadTextBox ApellidoMaterno;
        private Telerik.WinControls.UI.RadTextBox ApellidoPaterno;
        private Telerik.WinControls.UI.RadTextBox Nombre;
        private Telerik.WinControls.UI.RadTextBox TxbRFC;
        private Telerik.WinControls.UI.RadTextBox TxbCURP;
        private Telerik.WinControls.UI.RadTextBox TxbClave;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbNum;
        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel label25;
        private Telerik.WinControls.UI.RadGroupBox groupBox;
        private Telerik.WinControls.UI.RadLabel label30;
        private Telerik.WinControls.UI.RadTextBox TxbAfore;
        private Telerik.WinControls.UI.RadLabel label29;
        private Telerik.WinControls.UI.RadTextBox TxbNumFonacot;
        private Telerik.WinControls.UI.RadPageView tabControl;
        private Telerik.WinControls.UI.RadPageViewPage pageGeneral;
        private Telerik.WinControls.UI.RadPageViewPage pagePago;
        private Telerik.WinControls.UI.RadLabel EntidadFedNacimientoLabel;
        private Telerik.WinControls.UI.RadDropDownList EntidadFedNacimiento;
        private Telerik.WinControls.UI.RadLabel CiudadNacimientoLabel;
        private Telerik.WinControls.UI.RadTextBox CiudadNacimiento;
        private Telerik.WinControls.UI.RadTextBox TxbUMF;
        private Telerik.WinControls.UI.RadLabel label49;
        private Telerik.WinControls.UI.RadStatusStrip StatusBar;
        private Telerik.WinControls.UI.RadWaitingBarElement Espera;
        private Telerik.WinControls.UI.RadToggleSwitch switchActivo;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox TxbTelefono2;
        private Common.Forms.ToolBarStandarControl TEmpleado;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadPageViewPage pageContrato;
        private Common.Forms.ToolBarStandarControl TContrato;
        internal Telerik.WinControls.UI.RadGridView gridViewEmpleados;
        internal Telerik.WinControls.UI.RadGridView gridBanco;
        private Common.Forms.ToolBarStandarControl TBanco;
        private Telerik.WinControls.UI.RadPageViewPage pagePrestacion;
        private Telerik.WinControls.UI.RadGridView gridConceptos;
        private Common.Forms.ToolBarStandarControl TPrestacion;
    }
}