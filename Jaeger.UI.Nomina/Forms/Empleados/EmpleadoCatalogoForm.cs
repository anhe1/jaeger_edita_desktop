﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    /// <summary>
    /// catalogo de empleados
    /// </summary>
    public partial class EmpleadoCatalogoForm : RadForm {
        #region declaraciones
        protected BindingList<IEmpleadoDetailModel> empleados;
        protected IEmpleadoService Service;
        protected IContratoService ServiceContrato;
        protected RadMenuItem Importar = new RadMenuItem { Text = "Importar" };
        protected internal RadMenuItem MenuContextual_Copiar = new RadMenuItem { Text = "Copiar" };
        protected internal RadMenuItem MenuContextual_ValidarRFC = new RadMenuItem { Text = "Validación de RFC" };
        protected internal RadMenuItem MenuContextual_MultiSeleccion = new RadMenuItem { Text = "Selección múltiple" };
        protected internal RadMenuItem ContextMenuContrato = new RadMenuItem { Text = "Agregar contrato" };
        protected internal RadMenuItem ContextMenuCuentaBancaria = new RadMenuItem { Text = "Agregar cuenta bancaria" };
        private Domain.Base.ValueObjects.UIAction _permisos;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="menuElement"></param>
        public EmpleadoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void CatalogoEmpleados_Load(object sender, EventArgs e) {
            this.TEmpleado.Permisos = this._permisos;
            this.TEmpleado.Herramientas.Items.Add(Importar);

            this.TEmpleado.Nuevo.Click += this.TEmpleado_Nuevo_Click;
            this.TEmpleado.Editar.Click += this.TEmpleado_Editar_Click;
            this.TEmpleado.Remover.Click += this.TEmpleado_Remover_Click;
            this.TEmpleado.Actualizar.Click += this.TEmpleado_Actualizar_Click;
            this.TEmpleado.Cerrar.Click += this.TEmpleado_Cerrar_Click;
            this.Importar.Click += TEmpleado_Importar_Click;
            this.Importar.Enabled = this.TEmpleado.Nuevo.Enabled;

            this.TEmpleado.menuContextual.Items.AddRange(this.MenuContextual_Copiar, this.MenuContextual_MultiSeleccion, this.MenuContextual_ValidarRFC, this.ContextMenuContrato, this.ContextMenuCuentaBancaria);

            this.MenuContextual_Copiar.Click += this.MenuContextual_Copiar_Click;
            this.MenuContextual_MultiSeleccion.Click += this.MenuContextual_Seleccion_Click;
            this.MenuContextual_ValidarRFC.Click += this.MenuContextual_ValidarRFC_Click;
            this.ContextMenuContrato.Click += this.ContextMenuContrato_Click;
            this.ContextMenuCuentaBancaria.Click += this.ContextMenuCuentaBancaria_Click;

            this.TEmpleado.GridData.RowSourceNeeded += GridViewEmpleados_RowSourceNeeded;
            this.TEmpleado.gridContrato.HierarchyDataProvider = new GridViewEventDataProvider(this.TEmpleado.gridContrato);
            this.TEmpleado.gridBanco.HierarchyDataProvider = new GridViewEventDataProvider(this.TEmpleado.gridBanco);
        }

        #region barra de herramientas
        private void TEmpleado_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new EmpleadoForm(this.Service, null);
            _nuevo.ShowDialog(this);
        }

        private void TEmpleado_Editar_Click(object sender, EventArgs e) {
            if (this.TEmpleado.GridData.CurrentRow != null) {
                var _seleccionado = this.TEmpleado.GridData.CurrentRow.DataBoundItem as EmpleadoDetailModel;
                if (_seleccionado != null) {
                    var _editar = new EmpleadoForm(this.Service, _seleccionado);
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TEmpleado_Remover_Click(object sender, EventArgs e) {
            if (this.TEmpleado.GridData.CurrentRow != null) {
                var _seleccionado = this.TEmpleado.GridData.CurrentRow.DataBoundItem as IEmpleadoDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Activo) {
                        if (RadMessageBox.Show(this, Properties.Resources.Msg_Empleado_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                            _seleccionado = this.Service.Remove(_seleccionado);
                        }
                    }
                }
            }
        }

        private void TEmpleado_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TEmpleado.GridData.DataSource = this.empleados;
        }

        private void TEmpleado_Importar_Click(object sender, EventArgs e) {
            //var openFile = new OpenFileDialog() { Filter = "*.xml|*.XML" };
            //if (openFile.ShowDialog() == DialogResult.OK) {
            //    var empleado = this.Service.Importar(openFile.FileName);
            //    if (empleado != null) {
            //        var _nuevo = new EmpleadoForm(this.Service, empleado);
            //        _nuevo.ShowDialog(this);
            //    } else {
            //        RadMessageBox.Show(this, "El comprobante seleccionado no es un recibo de nómina", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
            //    }
            //}
        }

        private void TEmpleado_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        public virtual void MenuContextual_Copiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.TEmpleado.GridData.CurrentCell.Value.ToString());
        }

        public virtual void MenuContextual_Seleccion_Click(object sender, EventArgs e) {
            this.MenuContextual_MultiSeleccion.IsChecked = !this.MenuContextual_MultiSeleccion.IsChecked;
            this.TEmpleado.GridData.MultiSelect = this.MenuContextual_MultiSeleccion.IsChecked;
        }

        public virtual void MenuContextual_ValidarRFC_Click(object sender, EventArgs e) {
            //if (this.gridViewEmpleados.ChildRows.Count > 0) {
            //    var _seleccion = this.gridViewEmpleados.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as EmpleadoDetailModel);
            //    var registos = new BindingList<LayoutValidaRFCModel>();
            //    foreach (var item in _seleccion) {
            //        registos.Add(new LayoutValidaRFCModel {
            //            CodigoPostal = item.CodigoPostal,
            //            NombreRazonSocial = string.Format("{0} {1} {2}", item.Nombre, item.ApellidoPaterno, item.ApellidoMaterno),
            //            RFC = item.RFC,
            //            Registro = registos.Count + 1
            //        });
            //    }
            //    var validarRFC = new ValidaRFCForm(registos);
            //    validarRFC.ShowDialog(this);
            //}
        }

        private void ContextMenuContrato_Click(object sender, EventArgs e) {

        }

        private void ContextMenuCuentaBancaria_Click(object sender, EventArgs e) {
            var _seleccionado = this.TEmpleado.GridData.CurrentRow.DataBoundItem as EmpleadoDetailModel;
            if (_seleccionado != null) {
                var _cuenta = new CuentaBancariaForm(new EmpleadoCuentaBancariaModel { IdEmpleado = _seleccionado.IdEmpleado });
                _cuenta.ShowDialog(this);
            }
        }
        #endregion

        #region acciones del grid
        private void GridViewEmpleados_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.TEmpleado.gridContrato.Caption) {
                var rowData = e.ParentRow.DataBoundItem as EmpleadoDetailModel;
                if (rowData != null) {
                    if (rowData.Contratos != null) {
                        foreach (var item in rowData.Contratos) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["IdEmpleado"].Value = item.IdEmpleado;
                            row.Cells["Activo"].Value = item.Activo;
                            row.Cells["ClaveTipoRegimen"].Value = item.ClaveTipoRegimen;
                            row.Cells["ClaveRiesgoPuesto"].Value = item.ClaveRiesgoPuesto;
                            row.Cells["ClavePeriodicidadPago"].Value = item.ClavePeriodicidadPago;
                            row.Cells["ClaveMetodoPago"].Value = item.ClaveMetodoPago;
                            row.Cells["ClaveTipoContrato"].Value = item.ClaveTipoContrato;
                            row.Cells["ClaveTipoJornada"].Value = item.ClaveTipoJornada;
                            row.Cells["RegistroPatronal"].Value = item.RegistroPatronal;
                            row.Cells["Departamento"].Value = item.Departamento;
                            row.Cells["FecInicioRelLaboral"].Value = item.FecInicioRelLaboral;
                            row.Cells["FecTerminoRelLaboral"].Value = item.FecTerminoRelLaboral;
                            row.Cells["SalarioBase"].Value = item.SalarioBase;
                            row.Cells["SalarioBaseCotizacion"].Value = item.SalarioBaseCotizacion;
                            row.Cells["SalarioDiario"].Value = item.SalarioDiario;
                            row.Cells["Jornadas"].Value = item.Jornadas;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.TEmpleado.gridBanco.Caption) {
                var rowData = e.ParentRow.DataBoundItem as EmpleadoDetailModel;
                if (rowData != null) {
                    if (rowData.Cuentas != null) {
                        foreach (var item in rowData.Cuentas) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["Activo"].Value = item.Activo;
                            row.Cells["Verificado"].Value = item.Verificado;
                            row.Cells["IdTipoCuenta"].Value = item.IdTipoCuenta;
                            row.Cells["Banco"].Value = item.Banco;
                            row.Cells["Clave"].Value = item.Clave;
                            row.Cells["InsitucionBancaria"].Value = item.InsitucionBancaria;
                            row.Cells["NumeroDeCuenta"].Value = item.NumCuenta;
                            row.Cells["Creo"].Value = item.Creo;
                            row.Cells["FechaNuevo"].Value = item.FechaNuevo;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        #endregion

        #region metodos privados
        protected virtual void Consultar() {
            var query = this.Service.GetList<EmpleadoDetailModel>(EmpleadoService.Query().OnlyActive(!this.TEmpleado.RegistroInactivo.IsChecked).Build()).ToList<IEmpleadoDetailModel>();
            this.empleados = new BindingList<IEmpleadoDetailModel>(query);
        }
        #endregion
    }
}
