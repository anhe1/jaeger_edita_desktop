﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class PuestoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.SueldoLabel = new Telerik.WinControls.UI.RadLabel();
            this.ClasificacionLabel = new Telerik.WinControls.UI.RadLabel();
            this.CuentaContableLabel = new Telerik.WinControls.UI.RadLabel();
            this.DescripcionLabel = new Telerik.WinControls.UI.RadLabel();
            this.Sueldo = new Telerik.WinControls.UI.RadTextBox();
            this.Clasificacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CuentaContable = new Telerik.WinControls.UI.RadTextBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.IsActivo = new Telerik.WinControls.UI.RadCheckBox();
            this.InformacionBox = new Telerik.WinControls.UI.RadGroupBox();
            this.DepartamentoLabel = new Telerik.WinControls.UI.RadLabel();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TPuesto_Guardar = new Telerik.WinControls.UI.RadButton();
            this.TPuesto_Cerrar = new Telerik.WinControls.UI.RadButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SueldoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContableLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sueldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformacionBox)).BeginInit();
            this.InformacionBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepartamentoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPuesto_Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPuesto_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(353, 33);
            this.Encabezado.TabIndex = 198;
            this.Encabezado.TabStop = false;
            // 
            // SueldoLabel
            // 
            this.SueldoLabel.Location = new System.Drawing.Point(13, 149);
            this.SueldoLabel.Name = "SueldoLabel";
            this.SueldoLabel.Size = new System.Drawing.Size(72, 18);
            this.SueldoLabel.TabIndex = 194;
            this.SueldoLabel.Text = "Responsable:";
            // 
            // ClasificacionLabel
            // 
            this.ClasificacionLabel.Location = new System.Drawing.Point(13, 123);
            this.ClasificacionLabel.Name = "ClasificacionLabel";
            this.ClasificacionLabel.Size = new System.Drawing.Size(68, 18);
            this.ClasificacionLabel.TabIndex = 194;
            this.ClasificacionLabel.Text = "Clasificación";
            // 
            // CuentaContableLabel
            // 
            this.CuentaContableLabel.Location = new System.Drawing.Point(13, 97);
            this.CuentaContableLabel.Name = "CuentaContableLabel";
            this.CuentaContableLabel.Size = new System.Drawing.Size(90, 18);
            this.CuentaContableLabel.TabIndex = 194;
            this.CuentaContableLabel.Text = "Cuenta Contable";
            // 
            // DescripcionLabel
            // 
            this.DescripcionLabel.Location = new System.Drawing.Point(13, 71);
            this.DescripcionLabel.Name = "DescripcionLabel";
            this.DescripcionLabel.Size = new System.Drawing.Size(64, 18);
            this.DescripcionLabel.TabIndex = 194;
            this.DescripcionLabel.Text = "Descripción";
            // 
            // Sueldo
            // 
            this.Sueldo.Location = new System.Drawing.Point(109, 148);
            this.Sueldo.Name = "Sueldo";
            this.Sueldo.Size = new System.Drawing.Size(200, 20);
            this.Sueldo.TabIndex = 193;
            // 
            // Clasificacion
            // 
            // 
            // Clasificacion.NestedRadGridView
            // 
            this.Clasificacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Clasificacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clasificacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Clasificacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Clasificacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Clasificacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Clasificacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.Clasificacion.EditorControl.Name = "NestedRadGridView";
            this.Clasificacion.EditorControl.ReadOnly = true;
            this.Clasificacion.EditorControl.ShowGroupPanel = false;
            this.Clasificacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Clasificacion.EditorControl.TabIndex = 0;
            this.Clasificacion.Location = new System.Drawing.Point(109, 122);
            this.Clasificacion.Name = "Clasificacion";
            this.Clasificacion.Size = new System.Drawing.Size(200, 20);
            this.Clasificacion.TabIndex = 193;
            this.Clasificacion.TabStop = false;
            // 
            // CuentaContable
            // 
            this.CuentaContable.Location = new System.Drawing.Point(109, 96);
            this.CuentaContable.Name = "CuentaContable";
            this.CuentaContable.Size = new System.Drawing.Size(200, 20);
            this.CuentaContable.TabIndex = 193;
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(83, 70);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(226, 20);
            this.Descripcion.TabIndex = 193;
            // 
            // IsActivo
            // 
            this.IsActivo.Location = new System.Drawing.Point(258, 19);
            this.IsActivo.Name = "IsActivo";
            this.IsActivo.Size = new System.Drawing.Size(51, 18);
            this.IsActivo.TabIndex = 192;
            this.IsActivo.Text = "Activo";
            // 
            // InformacionBox
            // 
            this.InformacionBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.InformacionBox.Controls.Add(this.DepartamentoLabel);
            this.InformacionBox.Controls.Add(this.SueldoLabel);
            this.InformacionBox.Controls.Add(this.Departamento);
            this.InformacionBox.Controls.Add(this.ClasificacionLabel);
            this.InformacionBox.Controls.Add(this.CuentaContableLabel);
            this.InformacionBox.Controls.Add(this.DescripcionLabel);
            this.InformacionBox.Controls.Add(this.Sueldo);
            this.InformacionBox.Controls.Add(this.Clasificacion);
            this.InformacionBox.Controls.Add(this.CuentaContable);
            this.InformacionBox.Controls.Add(this.Descripcion);
            this.InformacionBox.Controls.Add(this.IsActivo);
            this.InformacionBox.HeaderText = "Información";
            this.InformacionBox.Location = new System.Drawing.Point(12, 39);
            this.InformacionBox.Name = "InformacionBox";
            this.InformacionBox.Size = new System.Drawing.Size(328, 181);
            this.InformacionBox.TabIndex = 201;
            this.InformacionBox.TabStop = false;
            this.InformacionBox.Text = "Información";
            // 
            // DepartamentoLabel
            // 
            this.DepartamentoLabel.Location = new System.Drawing.Point(13, 45);
            this.DepartamentoLabel.Name = "DepartamentoLabel";
            this.DepartamentoLabel.Size = new System.Drawing.Size(81, 18);
            this.DepartamentoLabel.TabIndex = 196;
            this.DepartamentoLabel.Text = "Departamento:";
            // 
            // Departamento
            // 
            this.Departamento.DisplayMember = "Descriptor";
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Descriptor";
            gridViewTextBoxColumn7.HeaderText = "Descriptor";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "Descriptor";
            gridViewTextBoxColumn7.VisibleInColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "Id";
            gridViewTextBoxColumn8.HeaderText = "ID";
            gridViewTextBoxColumn8.Name = "Id";
            gridViewTextBoxColumn9.FieldName = "Descripcion";
            gridViewTextBoxColumn9.HeaderText = "Descripción";
            gridViewTextBoxColumn9.Name = "Descripcion";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 140;
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(100, 44);
            this.Departamento.Name = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(209, 20);
            this.Departamento.TabIndex = 195;
            this.Departamento.TabStop = false;
            this.Departamento.ValueMember = "Id";
            // 
            // TPuesto_Guardar
            // 
            this.TPuesto_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TPuesto_Guardar.Location = new System.Drawing.Point(115, 241);
            this.TPuesto_Guardar.Name = "TPuesto_Guardar";
            this.TPuesto_Guardar.Size = new System.Drawing.Size(110, 24);
            this.TPuesto_Guardar.TabIndex = 199;
            this.TPuesto_Guardar.Text = "Guardar";
            this.TPuesto_Guardar.Click += new System.EventHandler(this.TPuesto_Guardar_Click);
            // 
            // TPuesto_Cerrar
            // 
            this.TPuesto_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TPuesto_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TPuesto_Cerrar.Location = new System.Drawing.Point(231, 241);
            this.TPuesto_Cerrar.Name = "TPuesto_Cerrar";
            this.TPuesto_Cerrar.Size = new System.Drawing.Size(110, 24);
            this.TPuesto_Cerrar.TabIndex = 200;
            this.TPuesto_Cerrar.Text = "Cerrar";
            this.TPuesto_Cerrar.Click += new System.EventHandler(this.TPuesto_Cerrar_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // PuestoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TPuesto_Cerrar;
            this.ClientSize = new System.Drawing.Size(353, 277);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.InformacionBox);
            this.Controls.Add(this.TPuesto_Guardar);
            this.Controls.Add(this.TPuesto_Cerrar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PuestoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PuestoForm";
            this.Load += new System.EventHandler(this.PuestoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SueldoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContableLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sueldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformacionBox)).EndInit();
            this.InformacionBox.ResumeLayout(false);
            this.InformacionBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepartamentoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPuesto_Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPuesto_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel SueldoLabel;
        private Telerik.WinControls.UI.RadLabel ClasificacionLabel;
        private Telerik.WinControls.UI.RadLabel CuentaContableLabel;
        private Telerik.WinControls.UI.RadLabel DescripcionLabel;
        private Telerik.WinControls.UI.RadTextBox Sueldo;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Clasificacion;
        private Telerik.WinControls.UI.RadTextBox CuentaContable;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadCheckBox IsActivo;
        private Telerik.WinControls.UI.RadGroupBox InformacionBox;
        private Telerik.WinControls.UI.RadLabel DepartamentoLabel;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        private Telerik.WinControls.UI.RadButton TPuesto_Guardar;
        private Telerik.WinControls.UI.RadButton TPuesto_Cerrar;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}