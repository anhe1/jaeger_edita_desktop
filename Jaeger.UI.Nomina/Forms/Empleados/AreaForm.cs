﻿using Jaeger.UI.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class AreaForm : RadForm {
        protected Aplication.Nomina.Contracts.IAreaService _areaService;
        protected Domain.Empresa.Entities.AreaModel Current;

        public AreaForm(Aplication.Nomina.Contracts.IAreaService areaService, Domain.Empresa.Entities.AreaModel areaModel) {
            InitializeComponent();
            _areaService = areaService;
            Current = areaModel;
        }

        private void AreaForm_Load(object sender, EventArgs e) {
            if (this.Current == null) {
                this.Current = new Domain.Empresa.Entities.AreaModel();
            }
            this.CreateBinding();
        }

        private void TArea_Guardar_Click(object sender, EventArgs e) {
            if (!this.Validar()) {
                return;
            }
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.ShowDialog(this);
            }
            if (this.Current.Id > 0) {
                RadMessageBox.Show(this, "Registro guardado con éxito", "Información", MessageBoxButtons.OK, RadMessageIcon.Info);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Error al guardar el registro", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void TArea_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.Descripcion.DataBindings.Add("Text", this.Current, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.CentroCostos.DataBindings.Add("Text", this.Current., "CCostos", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CuentaContable.DataBindings.Add("Text", this.Current, "CtaContable", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.Clasificacion.DataBindings.Add("Text", this.Current, "Clasificacion", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private bool Validar() {
            if (string.IsNullOrEmpty(this.Current.Descripcion)) {
                RadMessageBox.Show(this, "La descripción es requerida", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                return false;
            }
            return true;
        }

        private void Guardar() {
            this.Current = this._areaService.Save(this.Current);
        }
    }
}
