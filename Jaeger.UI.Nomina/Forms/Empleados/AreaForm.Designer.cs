﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class AreaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.ResposableLabel = new Telerik.WinControls.UI.RadLabel();
            this.ClasificacionLabel = new Telerik.WinControls.UI.RadLabel();
            this.CuentaContableLabel = new Telerik.WinControls.UI.RadLabel();
            this.DescripcionLabel = new Telerik.WinControls.UI.RadLabel();
            this.Responsable = new Telerik.WinControls.UI.RadTextBox();
            this.Clasificacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CuentaContable = new Telerik.WinControls.UI.RadTextBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.InformacionBox = new Telerik.WinControls.UI.RadGroupBox();
            this.TArea_Guardar = new Telerik.WinControls.UI.RadButton();
            this.TArea_Cerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResposableLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContableLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformacionBox)).BeginInit();
            this.InformacionBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TArea_Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TArea_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(366, 33);
            this.Encabezado.TabIndex = 198;
            this.Encabezado.TabStop = false;
            // 
            // ResposableLabel
            // 
            this.ResposableLabel.Location = new System.Drawing.Point(13, 108);
            this.ResposableLabel.Name = "ResposableLabel";
            this.ResposableLabel.Size = new System.Drawing.Size(72, 18);
            this.ResposableLabel.TabIndex = 194;
            this.ResposableLabel.Text = "Responsable:";
            // 
            // ClasificacionLabel
            // 
            this.ClasificacionLabel.Location = new System.Drawing.Point(13, 82);
            this.ClasificacionLabel.Name = "ClasificacionLabel";
            this.ClasificacionLabel.Size = new System.Drawing.Size(68, 18);
            this.ClasificacionLabel.TabIndex = 194;
            this.ClasificacionLabel.Text = "Clasificación";
            // 
            // CuentaContableLabel
            // 
            this.CuentaContableLabel.Location = new System.Drawing.Point(13, 56);
            this.CuentaContableLabel.Name = "CuentaContableLabel";
            this.CuentaContableLabel.Size = new System.Drawing.Size(90, 18);
            this.CuentaContableLabel.TabIndex = 194;
            this.CuentaContableLabel.Text = "Cuenta Contable";
            // 
            // DescripcionLabel
            // 
            this.DescripcionLabel.Location = new System.Drawing.Point(13, 30);
            this.DescripcionLabel.Name = "DescripcionLabel";
            this.DescripcionLabel.Size = new System.Drawing.Size(64, 18);
            this.DescripcionLabel.TabIndex = 194;
            this.DescripcionLabel.Text = "Descripción";
            // 
            // Responsable
            // 
            this.Responsable.Location = new System.Drawing.Point(109, 107);
            this.Responsable.Name = "Responsable";
            this.Responsable.Size = new System.Drawing.Size(200, 20);
            this.Responsable.TabIndex = 193;
            // 
            // Clasificacion
            // 
            // 
            // Clasificacion.NestedRadGridView
            // 
            this.Clasificacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Clasificacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clasificacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Clasificacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Clasificacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Clasificacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Clasificacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Clasificacion.EditorControl.Name = "NestedRadGridView";
            this.Clasificacion.EditorControl.ReadOnly = true;
            this.Clasificacion.EditorControl.ShowGroupPanel = false;
            this.Clasificacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Clasificacion.EditorControl.TabIndex = 0;
            this.Clasificacion.Location = new System.Drawing.Point(109, 81);
            this.Clasificacion.Name = "Clasificacion";
            this.Clasificacion.Size = new System.Drawing.Size(200, 20);
            this.Clasificacion.TabIndex = 193;
            this.Clasificacion.TabStop = false;
            // 
            // CuentaContable
            // 
            this.CuentaContable.Location = new System.Drawing.Point(109, 55);
            this.CuentaContable.Name = "CuentaContable";
            this.CuentaContable.Size = new System.Drawing.Size(200, 20);
            this.CuentaContable.TabIndex = 193;
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(83, 29);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(226, 20);
            this.Descripcion.TabIndex = 193;
            // 
            // InformacionBox
            // 
            this.InformacionBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.InformacionBox.Controls.Add(this.ResposableLabel);
            this.InformacionBox.Controls.Add(this.ClasificacionLabel);
            this.InformacionBox.Controls.Add(this.CuentaContableLabel);
            this.InformacionBox.Controls.Add(this.DescripcionLabel);
            this.InformacionBox.Controls.Add(this.Responsable);
            this.InformacionBox.Controls.Add(this.Clasificacion);
            this.InformacionBox.Controls.Add(this.CuentaContable);
            this.InformacionBox.Controls.Add(this.Descripcion);
            this.InformacionBox.HeaderText = "Información";
            this.InformacionBox.Location = new System.Drawing.Point(12, 39);
            this.InformacionBox.Name = "InformacionBox";
            this.InformacionBox.Size = new System.Drawing.Size(328, 152);
            this.InformacionBox.TabIndex = 201;
            this.InformacionBox.TabStop = false;
            this.InformacionBox.Text = "Información";
            // 
            // TArea_Guardar
            // 
            this.TArea_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TArea_Guardar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TArea_Guardar.Location = new System.Drawing.Point(128, 220);
            this.TArea_Guardar.Name = "TArea_Guardar";
            this.TArea_Guardar.Size = new System.Drawing.Size(110, 24);
            this.TArea_Guardar.TabIndex = 199;
            this.TArea_Guardar.Text = "Guardar";
            this.TArea_Guardar.Click += new System.EventHandler(this.TArea_Guardar_Click);
            // 
            // TArea_Cerrar
            // 
            this.TArea_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TArea_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TArea_Cerrar.Location = new System.Drawing.Point(244, 220);
            this.TArea_Cerrar.Name = "TArea_Cerrar";
            this.TArea_Cerrar.Size = new System.Drawing.Size(110, 24);
            this.TArea_Cerrar.TabIndex = 200;
            this.TArea_Cerrar.Text = "Cerrar";
            this.TArea_Cerrar.Click += new System.EventHandler(this.TArea_Cerrar_Click);
            // 
            // AreaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TArea_Cerrar;
            this.ClientSize = new System.Drawing.Size(366, 256);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.InformacionBox);
            this.Controls.Add(this.TArea_Guardar);
            this.Controls.Add(this.TArea_Cerrar);
            this.Name = "AreaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AreaForm";
            this.Load += new System.EventHandler(this.AreaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResposableLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContableLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformacionBox)).EndInit();
            this.InformacionBox.ResumeLayout(false);
            this.InformacionBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TArea_Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TArea_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel ResposableLabel;
        private Telerik.WinControls.UI.RadLabel ClasificacionLabel;
        private Telerik.WinControls.UI.RadLabel CuentaContableLabel;
        private Telerik.WinControls.UI.RadLabel DescripcionLabel;
        private Telerik.WinControls.UI.RadTextBox Responsable;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Clasificacion;
        private Telerik.WinControls.UI.RadTextBox CuentaContable;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadGroupBox InformacionBox;
        private Telerik.WinControls.UI.RadButton TArea_Guardar;
        private Telerik.WinControls.UI.RadButton TArea_Cerrar;
    }
}