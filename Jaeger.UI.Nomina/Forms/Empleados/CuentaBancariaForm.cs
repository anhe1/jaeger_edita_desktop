﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Aplication.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class CuentaBancariaForm : RadForm {
        protected ICuentaBancariaService service;
        protected IBancosCatalogo bancoCatalogo = new BancosCatalogo();
        private EmpleadoCuentaBancariaModel cuentaBancaria;

        public CuentaBancariaForm() {
            InitializeComponent();
        }

        public CuentaBancariaForm(EmpleadoCuentaBancariaModel model) {
            InitializeComponent();
            this.cuentaBancaria = model;
        }

        private void CuentaBancariaForm_Load(object sender, EventArgs e) {
            this.bancoCatalogo.Load();
            this.NumCuenta.KeyPress += Extensions.TextBoxOnlyNumbers_KeyPress;
            this.ClaveBanco.DataSource = this.bancoCatalogo.Items;
            this.ClaveBanco.SelectedIndex = -1;
            this.ClaveBanco.SelectedIndexChanged += CboClaveBanco_SelectedIndexChanged;

            if (this.cuentaBancaria.IdCuentaB > 0) {
                using (var espera = new UI.Common.Forms.Waiting2Form(this.Actualizar)) {
                    espera.ShowDialog(this);
                }
            }
            this.CreateBinding();
        }

        private void CboClaveBanco_SelectedIndexChanged(object sender, EventArgs e) {
            var seleccionado = ((GridViewDataRowInfo)this.ClaveBanco.SelectedItem).DataBoundItem as ClaveBanco;
            if (seleccionado != null) {
                this.cuentaBancaria.Banco = seleccionado.Descripcion;
            }
        }

        private void CreateBinding() {
            this.Beneficiario.DataBindings.Clear();
            this.Beneficiario.DataBindings.Add("Text", this.cuentaBancaria, "Beneficiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BeneficiarioRFC.DataBindings.Clear();
            this.BeneficiarioRFC.DataBindings.Add("Text", this.cuentaBancaria, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Activo.DataBindings.Clear();
            this.Activo.DataBindings.Add("Checked", this.cuentaBancaria, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCuenta.DataBindings.Clear();
            this.NumCuenta.DataBindings.Add("Text", this.cuentaBancaria, "NumCuenta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Verificado.DataBindings.Clear();
            this.Verificado.DataBindings.Add("Value", this.cuentaBancaria, "Verificado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Clabe.DataBindings.Clear();
            this.Clabe.DataBindings.Add("Text", this.cuentaBancaria, "CLABE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CtaContable.DataBindings.Clear();
            //this.CtaContable.DataBindings.Add("Text", this.cuentaBancaria, "CuentaContable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCliente.DataBindings.Clear();
            //this.NumCliente.DataBindings.Add("Text", this.cuentaBancaria, "NumCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.DataBindings.Add("Text", this.cuentaBancaria, "Moneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ClaveBanco.DataBindings.Clear();
            this.ClaveBanco.DataBindings.Add("SelectedValue", this.cuentaBancaria, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Sucursal.DataBindings.Clear();
            this.Sucursal.DataBindings.Add("Text", this.cuentaBancaria, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaApertura.DataBindings.Clear();
            this.FechaApertura.DataBindings.Add("Value", this.cuentaBancaria, "FechaNuevo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoExtranjero.DataBindings.Clear();
            this.BancoExtranjero.DataBindings.Add("Checked", this.cuentaBancaria, "Extranjero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TCuenta.DataSource = this.service.GetTipos();
            this.TCuenta.ValueMember = "Id";
            this.TCuenta.DisplayMember = "Descripcion";
            this.TCuenta.DataBindings.Clear();
            this.TCuenta.DataBindings.Add("SelectedValue", this.cuentaBancaria, "IdTipoCuenta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataSource = this.service.GetMonedas();
            this.Moneda.ValueMember = "Id";
            this.Moneda.DisplayMember = "Id";
            this.Moneda.DataBindings.Clear();
            this.Moneda.DataBindings.Add("SelectedValue", this.cuentaBancaria, "Moneda", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.Activo.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.Beneficiario.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.BeneficiarioRFC.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.NumCuenta.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.NumCliente.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.Clabe.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.CtaContable.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.BancoRFC.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.NumCliente.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.Moneda.Enabled = !this.cuentaBancaria.ReadOnly;
            //this.ClaveBanco.Enabled = !this.cuentaBancaria.ReadOnly;
            //this.Sucursal.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.Telefono.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.Funcionario.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.Alias.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.SaldoInicial.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.NoCheque.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.DiaCorte.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.FechaApertura.ReadOnly = this.cuentaBancaria.ReadOnly;
            //this.BancoExtranjero.ReadOnly = this.cuentaBancaria.ReadOnly;
        }

        private void Guardar() {
            this.cuentaBancaria = this.service.Save(this.cuentaBancaria);
        }

        private void Actualizar() {
            this.cuentaBancaria = this.service.GetById(this.cuentaBancaria.IdCuentaB);
        }

        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        private void TxbFolio_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }

        private bool Validar() {
            this.errorCuentaBancaria.Clear();

            if (this.cuentaBancaria.NumCuenta == null) {
                this.errorCuentaBancaria.SetError(this.NumCuenta, "Es necesario asociar un número de cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.NumCuenta.Trim())) {
                this.errorCuentaBancaria.SetError(this.NumCuenta, "Es necesario asociar un número de cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.Clave) && this.cuentaBancaria.IdTipoCuenta != 2) {
                this.errorCuentaBancaria.SetError(this.ClaveBanco, "Selecciona una clave de banco para la cuenta.");
                return false;
            }
            return true;
        }

        private void bGuardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using (var espera = new UI.Common.Forms.Waiting2Form(this.Guardar)) {
                    espera.ShowDialog(this);
                    this.Close();
                }
            }
        }

        private void bCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
