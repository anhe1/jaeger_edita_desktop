﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Nomina.Builder;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class PuestosForm : RadForm {
        protected Aplication.Nomina.Contracts.IPuestoService Service;
        protected BindingList<PuestoModel> DataSource;

        public PuestosForm() {
            InitializeComponent();
        }

        private void PuestosForm_Load(object sender, EventArgs e) {
            using (IPuestosGridBuilder builder = new PuestosGridBuilder()) {
                this.TPuesto.GridData.Columns.AddRange(builder.Templetes().Master().Build());
            }
            this.TPuesto.Nuevo.Click += TPuesto_Nuevo_Click;
            this.TPuesto.Editar.Click += TPuesto_Editar_Click;
            this.TPuesto.Remover.Click += TPuesto_Remover_Click;
            this.TPuesto.Actualizar.Click += TPuesto_Actualizar_Click;
            this.TPuesto.Cerrar.Click += TPuesto_Cerrar_Click;
        }

        private void TPuesto_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new PuestoForm(this.Service, null);
            nuevo.ShowDialog(this);
        }

        private void TPuesto_Editar_Click(object sender, EventArgs e) {
            if (this.TPuesto.GridData.CurrentRow != null) {
                var seleccionado = this.TPuesto.GridData.CurrentRow.DataBoundItem as PuestoModel;
                if (seleccionado != null) {
                    var editar = new PuestoForm(this.Service, seleccionado);
                    editar.ShowDialog(this);
                }
            }
            throw new NotImplementedException();
        }

        private void TPuesto_Remover_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        private void TPuesto_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new UI.Common.Forms.Waiting2Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
            this.TPuesto.GridData.DataSource = this.DataSource;
        }

        private void TPuesto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consulta() {
            this.DataSource = new BindingList<PuestoModel>(this.Service.GetList<PuestoModel>(new List<Domain.Base.Builder.IConditional>()).ToList());
        }
    }
}
