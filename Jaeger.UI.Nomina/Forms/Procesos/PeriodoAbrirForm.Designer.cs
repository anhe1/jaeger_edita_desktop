﻿namespace Jaeger.UI.Nomina.Forms.Procesos
{
    partial class PeriodoAbrirForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.TPeriodo = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewComboBoxColumn1.FieldName = "IdStatus";
            gridViewComboBoxColumn1.HeaderText = "Estado";
            gridViewComboBoxColumn1.Name = "IdStatus";
            gridViewComboBoxColumn1.ReadOnly = true;
            gridViewComboBoxColumn1.Width = 65;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 215;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.FieldName = "FechaInicio";
            gridViewTextBoxColumn3.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn3.HeaderText = "Fec. Inicio";
            gridViewTextBoxColumn3.Name = "FechaInicio";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 85;
            gridViewTextBoxColumn4.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn4.FieldName = "FechaFinal";
            gridViewTextBoxColumn4.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn4.HeaderText = "Fec. Final";
            gridViewTextBoxColumn4.Name = "FechaFin";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 85;
            gridViewTextBoxColumn5.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn5.FieldName = "FechaPago";
            gridViewTextBoxColumn5.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn5.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn5.Name = "FechaPago";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "DiasPeriodo";
            gridViewTextBoxColumn6.HeaderText = "Días";
            gridViewTextBoxColumn6.Name = "DiasPeriodo";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewComboBoxColumn2.FieldName = "IdPeriodoTipo";
            gridViewComboBoxColumn2.HeaderText = "Tipo";
            gridViewComboBoxColumn2.Name = "IdPeriodoTipo";
            gridViewComboBoxColumn2.ReadOnly = true;
            gridViewComboBoxColumn2.Width = 65;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn2});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(713, 250);
            this.GridData.TabIndex = 1;
            // 
            // TPeriodo
            // 
            this.TPeriodo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPeriodo.Location = new System.Drawing.Point(0, 0);
            this.TPeriodo.Name = "TPeriodo";
            this.TPeriodo.ShowActualizar = true;
            this.TPeriodo.ShowAutosuma = false;
            this.TPeriodo.ShowCancelar = false;
            this.TPeriodo.ShowCerrar = true;
            this.TPeriodo.ShowEditar = true;
            this.TPeriodo.ShowEjercicio = true;
            this.TPeriodo.ShowExportarExcel = false;
            this.TPeriodo.ShowFiltro = false;
            this.TPeriodo.ShowHerramientas = false;
            this.TPeriodo.ShowImprimir = false;
            this.TPeriodo.ShowItem = false;
            this.TPeriodo.ShowNuevo = false;
            this.TPeriodo.ShowPeriodo = true;
            this.TPeriodo.Size = new System.Drawing.Size(713, 30);
            this.TPeriodo.TabIndex = 2;
            // 
            // PeriodoAbrirForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 280);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TPeriodo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PeriodoAbrirForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Abrir Nómina";
            this.Load += new System.EventHandler(this.PeriodoAbrir_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView GridData;
        private Common.Forms.ToolBarCommonControl TPeriodo;
    }
}
