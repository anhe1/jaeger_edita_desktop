﻿using System;
using System.IO;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Util;
using Jaeger.QRCode.Helpers;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources _recursos = new EmbeddedResources("Jaeger.Domain.Nomina");
        private readonly string _dominio = "Jaeger.Domain.Nomina.Reports.";

        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(EmpleadoAguinaldoPrinter)) {
                this.CrearReporte1();
            }
        }

        private void CrearReporte1() {
            var current = (EmpleadoAguinaldoPrinter)this.CurrentObject;
            if (current.Resumen == false) { 
                this.LoadDefinition = this._recursos.GetStream(string.Concat(_dominio, "EmpleadoAguinaldoV10Report.rdlc"));
            } else {
                this.LoadDefinition = this._recursos.GetStream(string.Concat(_dominio, "EmpleadoAguinaldoRV10Report.rdlc"));
            }

            this.ImagenQR = QRCodeExtension.GetQRBase64(new string[] { "" });
            this.Procesar();
            this.SetDisplayName("Reporte");
            this.SetParameter("Domicilio", "");
            this.SetDataSource("Empleado", Jaeger.Domain.Base.Services.DbConvert.ConvertToDataTable(current.Empleados));
            this.Finalizar();
        }
    }
}
