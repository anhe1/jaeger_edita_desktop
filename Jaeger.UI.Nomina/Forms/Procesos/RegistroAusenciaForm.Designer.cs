﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class RegistroAusenciaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.Aceptar = new Telerik.WinControls.UI.RadButton();
            this.Cancelar = new Telerik.WinControls.UI.RadButton();
            this.TipoLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.Tipo = new Telerik.WinControls.UI.RadDropDownList();
            this.Calendario = new Telerik.WinControls.UI.RadCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calendario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(280, 33);
            this.Encabezado.TabIndex = 103;
            this.Encabezado.TabStop = false;
            // 
            // Aceptar
            // 
            this.Aceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Aceptar.Location = new System.Drawing.Point(41, 324);
            this.Aceptar.Name = "Aceptar";
            this.Aceptar.Size = new System.Drawing.Size(110, 24);
            this.Aceptar.TabIndex = 115;
            this.Aceptar.Text = "Agregar";
            this.Aceptar.Click += new System.EventHandler(this.Aceptar_Click);
            // 
            // Cancelar
            // 
            this.Cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancelar.Location = new System.Drawing.Point(157, 324);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(110, 24);
            this.Cancelar.TabIndex = 116;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.Click += new System.EventHandler(this.Cancelar_Click);
            // 
            // TipoLabel
            // 
            this.TipoLabel.Location = new System.Drawing.Point(12, 272);
            this.TipoLabel.Name = "TipoLabel";
            this.TipoLabel.Size = new System.Drawing.Size(84, 18);
            this.TipoLabel.TabIndex = 113;
            this.TipoLabel.Text = "Registrar como:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 6);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(114, 18);
            this.radLabel4.TabIndex = 111;
            this.radLabel4.Text = "Registro de ausencias";
            // 
            // Tipo
            // 
            this.Tipo.Location = new System.Drawing.Point(102, 272);
            this.Tipo.Name = "Tipo";
            this.Tipo.Size = new System.Drawing.Size(167, 20);
            this.Tipo.TabIndex = 117;
            // 
            // Calendario
            // 
            this.Calendario.Location = new System.Drawing.Point(12, 39);
            this.Calendario.Name = "Calendario";
            this.Calendario.Size = new System.Drawing.Size(257, 227);
            this.Calendario.TabIndex = 118;
            // 
            // RegistroAusenciasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 360);
            this.ControlBox = false;
            this.Controls.Add(this.Calendario);
            this.Controls.Add(this.Tipo);
            this.Controls.Add(this.Aceptar);
            this.Controls.Add(this.Cancelar);
            this.Controls.Add(this.TipoLabel);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RegistroAusenciasForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registro de Ausencias";
            this.Load += new System.EventHandler(this.RegistroAusenciaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calendario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadButton Aceptar;
        private Telerik.WinControls.UI.RadButton Cancelar;
        private Telerik.WinControls.UI.RadLabel TipoLabel;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList Tipo;
        private Telerik.WinControls.UI.RadCalendar Calendario;
    }
}