﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class TbPeriodoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Periodo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Nuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.Abrir = new Telerik.WinControls.UI.RadMenuItem();
            this.Reciente = new Telerik.WinControls.UI.RadMenuItem();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Eliminar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Calcular = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.ToolBar)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.ToolBar.Size = new System.Drawing.Size(1004, 30);
            this.ToolBar.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            this.commandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.UseCompatibleTextRendering = false;
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Periodo,
            this.Editar,
            this.Eliminar,
            this.Actualizar,
            this.Calcular,
            this.Filtro,
            this.Guardar,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.UseCompatibleTextRendering = false;
            // 
            // Periodo
            // 
            this.Periodo.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Periodo.DisplayName = "Nómina";
            this.Periodo.DrawText = true;
            this.Periodo.Image = global::Jaeger.UI.Nomina.Properties.Resources.apply_16px;
            this.Periodo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Nuevo,
            this.Abrir,
            this.Reciente});
            this.Periodo.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Periodo.Name = "Periodo";
            this.Periodo.Text = "Periodo";
            this.Periodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Periodo.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Periodo.UseCompatibleTextRendering = false;
            // 
            // Nuevo
            // 
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            // 
            // Abrir
            // 
            this.Abrir.Name = "Abrir";
            this.Abrir.Text = "Abrir";
            // 
            // Reciente
            // 
            this.Reciente.Name = "Reciente";
            this.Reciente.Text = "Abrir último";
            // 
            // Editar
            // 
            this.Editar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Editar.DisplayName = "Editar";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Nomina.Properties.Resources.edit_16px;
            this.Editar.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Editar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Editar.UseCompatibleTextRendering = false;
            // 
            // Eliminar
            // 
            this.Eliminar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Eliminar.DisplayName = "Eliminar";
            this.Eliminar.DrawText = true;
            this.Eliminar.Image = global::Jaeger.UI.Nomina.Properties.Resources.delete_16px;
            this.Eliminar.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.Text = "Remover";
            this.Eliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Eliminar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Eliminar.UseCompatibleTextRendering = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Nomina.Properties.Resources.refresh_16px;
            this.Actualizar.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Actualizar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.UseCompatibleTextRendering = false;
            // 
            // Calcular
            // 
            this.Calcular.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Calcular.DisplayName = "Calcular";
            this.Calcular.DrawText = true;
            this.Calcular.Image = global::Jaeger.UI.Nomina.Properties.Resources.accounting_16px;
            this.Calcular.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Calcular.Name = "Calcular";
            this.Calcular.Text = "Calcular";
            this.Calcular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Calcular.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Calcular.UseCompatibleTextRendering = false;
            // 
            // Filtro
            // 
            this.Filtro.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Nomina.Properties.Resources.filter_16px;
            this.Filtro.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Filtro.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Filtro.UseCompatibleTextRendering = false;
            // 
            // Guardar
            // 
            this.Guardar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Image = global::Jaeger.UI.Nomina.Properties.Resources.save_16px;
            this.Guardar.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Guardar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Guardar.UseCompatibleTextRendering = false;
            // 
            // Imprimir
            // 
            this.Imprimir.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Nomina.Properties.Resources.print_16px;
            this.Imprimir.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Imprimir.UseCompatibleTextRendering = false;
            // 
            // Herramientas
            // 
            this.Herramientas.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.Image = global::Jaeger.UI.Nomina.Properties.Resources.toolbox_16px;
            this.Herramientas.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Herramientas.UseCompatibleTextRendering = false;
            // 
            // Cerrar
            // 
            this.Cerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Nomina.Properties.Resources.close_window_16px;
            this.Cerrar.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.UseCompatibleTextRendering = false;
            // 
            // TbPeriodoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolBar);
            this.Name = "TbPeriodoControl";
            this.Size = new System.Drawing.Size(1004, 30);
            this.Load += new System.EventHandler(this.TbPeriodoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ToolBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar ToolBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Periodo;
        protected internal Telerik.WinControls.UI.RadMenuItem Nuevo;
        protected internal Telerik.WinControls.UI.RadMenuItem Abrir;
        protected internal Telerik.WinControls.UI.CommandBarButton Editar;
        protected internal Telerik.WinControls.UI.CommandBarButton Eliminar;
        protected internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        protected internal Telerik.WinControls.UI.CommandBarButton Calcular;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        protected internal Telerik.WinControls.UI.CommandBarButton Guardar;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Imprimir;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        protected internal Telerik.WinControls.UI.RadMenuItem Reciente;
    }
}
