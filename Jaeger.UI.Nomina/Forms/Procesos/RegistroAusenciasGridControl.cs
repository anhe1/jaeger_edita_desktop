﻿using System;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class RegistroAusenciasGridControl : Common.Forms.GridCommonControl {
        public RegistroAusenciasGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            IRegistroAusenciasGridBuilder view = new RegistroAusenciasGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }
    }
}
