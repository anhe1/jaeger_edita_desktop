﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class PeriodoCalcularForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PeriodoCalcularForm));
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.FechaPago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.LabelDiasPeriodo = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.FechaFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaInicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.TipoPeriodo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.TipoNomina = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Estado = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.gridEmpleados = new Jaeger.UI.Nomina.Forms.Procesos.PeriodoGridControl();
            this.TPeriodo = new Jaeger.UI.Nomina.Forms.Procesos.TbPeriodoControl();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDiasPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 662);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(1127, 26);
            this.radStatusStrip1.SizingGrip = false;
            this.radStatusStrip1.TabIndex = 2;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 30);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 30, 150, 470);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel7);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.FechaPago);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.LabelDiasPeriodo);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel8);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.FechaFinal);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.FechaInicio);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel6);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel5);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TipoPeriodo);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel4);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.TipoNomina);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel3);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.Descripcion);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel2);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.Estado);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radLabel1);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(239, 630);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(267, 632);
            this.radCollapsiblePanel1.TabIndex = 5;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(6, 309);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(90, 18);
            this.radLabel7.TabIndex = 12;
            this.radLabel7.Text = "Dias del Periodo:";
            // 
            // FechaPago
            // 
            this.FechaPago.Location = new System.Drawing.Point(6, 357);
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.Size = new System.Drawing.Size(222, 20);
            this.FechaPago.TabIndex = 10;
            this.FechaPago.TabStop = false;
            this.FechaPago.Text = "martes, 22 de enero de 2019";
            this.FechaPago.Value = new System.DateTime(2019, 1, 22, 22, 26, 13, 706);
            // 
            // LabelDiasPeriodo
            // 
            this.LabelDiasPeriodo.Location = new System.Drawing.Point(102, 309);
            this.LabelDiasPeriodo.Name = "LabelDiasPeriodo";
            this.LabelDiasPeriodo.Size = new System.Drawing.Size(48, 18);
            this.LabelDiasPeriodo.TabIndex = 10;
            this.LabelDiasPeriodo.Text = "Periodo:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(6, 333);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(79, 18);
            this.radLabel8.TabIndex = 11;
            this.radLabel8.Text = "Fecha de Pago";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Location = new System.Drawing.Point(6, 283);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(222, 20);
            this.FechaFinal.TabIndex = 8;
            this.FechaFinal.TabStop = false;
            this.FechaFinal.Text = "martes, 22 de enero de 2019";
            this.FechaFinal.Value = new System.DateTime(2019, 1, 22, 22, 26, 13, 706);
            // 
            // FechaInicio
            // 
            this.FechaInicio.Location = new System.Drawing.Point(6, 233);
            this.FechaInicio.Name = "FechaInicio";
            this.FechaInicio.Size = new System.Drawing.Size(222, 20);
            this.FechaInicio.TabIndex = 1;
            this.FechaInicio.TabStop = false;
            this.FechaInicio.Text = "martes, 22 de enero de 2019";
            this.FechaInicio.Value = new System.DateTime(2019, 1, 22, 22, 26, 13, 706);
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(6, 259);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(98, 18);
            this.radLabel6.TabIndex = 9;
            this.radLabel6.Text = "Fecha de Termino:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(6, 209);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(83, 18);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "Fecha de Inicio:";
            // 
            // TipoPeriodo
            // 
            this.TipoPeriodo.Location = new System.Drawing.Point(6, 183);
            this.TipoPeriodo.Name = "TipoPeriodo";
            this.TipoPeriodo.NullText = "Selecciona";
            this.TipoPeriodo.Size = new System.Drawing.Size(98, 20);
            this.TipoPeriodo.TabIndex = 6;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(6, 159);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(48, 18);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "Periodo:";
            // 
            // TipoNomina
            // 
            this.TipoNomina.Location = new System.Drawing.Point(6, 133);
            this.TipoNomina.Name = "TipoNomina";
            this.TipoNomina.NullText = "Selecciona";
            this.TipoNomina.Size = new System.Drawing.Size(98, 20);
            this.TipoNomina.TabIndex = 4;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(6, 109);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(89, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Tipo de Nómina:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(6, 83);
            this.Descripcion.MaxLength = 100;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.NullText = "Descripción";
            this.Descripcion.Size = new System.Drawing.Size(222, 20);
            this.Descripcion.TabIndex = 1;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(6, 59);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(67, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Descripción:";
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(6, 33);
            this.Estado.Name = "Estado";
            this.Estado.NullText = "Selecciona";
            this.Estado.Size = new System.Drawing.Size(98, 20);
            this.Estado.TabIndex = 1;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(6, 9);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(40, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Estado";
            // 
            // gridEmpleados
            // 
            this.gridEmpleados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEmpleados.Location = new System.Drawing.Point(267, 30);
            // 
            // 
            // 
            this.gridEmpleados.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridEmpleados.Name = "gridEmpleados";
            this.gridEmpleados.Size = new System.Drawing.Size(860, 632);
            this.gridEmpleados.TabIndex = 7;
            // 
            // TPeriodo
            // 
            this.TPeriodo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPeriodo.Location = new System.Drawing.Point(0, 0);
            this.TPeriodo.Name = "TPeriodo";
            this.TPeriodo.Size = new System.Drawing.Size(1127, 30);
            this.TPeriodo.TabIndex = 6;
            // 
            // PeriodoCalcularForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 688);
            this.Controls.Add(this.gridEmpleados);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Controls.Add(this.radStatusStrip1);
            this.Controls.Add(this.TPeriodo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PeriodoCalcularForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Nómina: Calcular";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PeriodoCalcularForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDiasPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker FechaPago;
        private Telerik.WinControls.UI.RadLabel LabelDiasPeriodo;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDateTimePicker FechaFinal;
        private Telerik.WinControls.UI.RadDateTimePicker FechaInicio;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList TipoPeriodo;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList TipoNomina;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList Estado;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        
        
        private TbPeriodoControl TPeriodo;
        public PeriodoGridControl gridEmpleados;
    }
}
