﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class AguinaldoForm : RadForm {
        protected IAguinaldoService Service;
        private BindingList<IEmpleadoCalcularAguinaldo> empleados;
        private readonly RadMenuItem _ToolBar_Imprimir_Resumen = new RadMenuItem { Text = "Resumen" };
        private readonly RadMenuItem _ToolBar_Imprimir_Completo = new RadMenuItem { Text = "Listado completo"};
        private readonly RadMenuItem _ToolBar_Layout = new RadMenuItem { Text = "Layout Banco" };

        public AguinaldoForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void AguinaldoForm_Load(object sender, EventArgs e) {
            this.ToolBar.Actualizar.Click += this.ToolBar_Actualizar_Click;
            this.ToolBar.ExportarExcel.Click += this.ToolBar_ExportarExcel_Click;
            this.ToolBar.Filtro.Click += this.ToolBar_Filtro_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_Cerrar_Click;

            this.ToolBar.Actualizar.Text = "Cargar";
            this.ToolBar.Imprimir.Items.AddRange(new Telerik.WinControls.RadItem[] { this._ToolBar_Imprimir_Completo, this._ToolBar_Imprimir_Resumen });
            this._ToolBar_Imprimir_Completo.Click += this.ToolBar_Imprimir_Completo_Click;
            this._ToolBar_Imprimir_Resumen.Click += ToolBar_Imprimir_Resumen_Click;
            this.ToolBar.Herramientas.Items.Add(this._ToolBar_Layout);
            this._ToolBar_Layout.Click += ToolBar_Layout_Click;
        }

        #region barra de herramientas
        private void ToolBar_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Cargar)) {
                espera.Text = "Cargando empleados";
                espera.ShowDialog(this);
            }
            this.gridEmpleados.DataSource = this.empleados;
        }

        private void ToolBar_Filtro_Click(object sender, EventArgs e) {
            this.gridEmpleados.ShowFilteringRow = this.ToolBar.Filtro.ToggleState == ToggleState.Off;
            if (this.gridEmpleados.ShowFilteringRow == false) {
                this.gridEmpleados.FilterDescriptors.Clear();
            }
        }

        private void ToolBar_ExportarExcel_Click(object sender, EventArgs e) {
            var _exportar = new TelerikGridExportForm(this.gridEmpleados);
            _exportar.ShowDialog(this);
        }

        private void ToolBar_Imprimir_Completo_Click(object sender, EventArgs e) {
            var _reporte = new ReporteForm(new EmpleadoAguinaldoPrinter { Empleados = this.empleados });
            _reporte.Show();
        }

        private void ToolBar_Imprimir_Resumen_Click(object sender, EventArgs e) {
            var _reporte = new ReporteForm(new EmpleadoAguinaldoPrinter { Empleados = this.empleados, Resumen = true });
            _reporte.Show();
        }

        private void ToolBar_Layout_Click(object sender, EventArgs e) {
            //var _layout = new LayoutBancoExportarForm();
            //_layout.ShowDialog(this);
        }

        private void ToolBar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Cargar() {
            this.empleados = this.Service.GetAguinaldos(this.ToolBar.GetEjercicio());
        }
        #endregion
    }
}
