﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class PeriodoAbrirForm : RadForm {
        #region declaraciones
        protected IPeriodoService service;
        private List<PeriodoModel> currentPeriodos;
        #endregion

        #region eventos
        public void OnSeleccionar(PeriodoModel e) {
            if (this.PeriodoSeleccionado != null) {
                this.PeriodoSeleccionado(this, e);
            }
        }

        public event EventHandler<PeriodoModel> PeriodoSeleccionado;
        #endregion

        #region formulario
        public PeriodoAbrirForm(IPeriodoService service) {
            InitializeComponent();
            this.service = service;
        }

        private void PeriodoAbrir_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            this.GridData.ShowFilteringRow = true;
            this.GridData.AllowEditRow = true;

            var comboStatus = this.GridData.Columns["IdStatus"] as GridViewComboBoxColumn;
            comboStatus.DisplayMember = "Descripcion";
            comboStatus.ValueMember = "Id";
            comboStatus.DataSource = CommonService.GetPeriodoStatus();

            var comboPeriodoTipo = this.GridData.Columns["IdPeriodoTipo"] as GridViewComboBoxColumn;
            comboPeriodoTipo.DisplayMember = "Descripcion";
            comboPeriodoTipo.ValueMember = "Id";
            comboPeriodoTipo.DataSource = CommonService.GetPeriodos();

            this.TPeriodo.Editar.Click += this.TPeriodo_Editar_Click;
            this.TPeriodo.Actualizar.Click += this.TPeriodo_Actualizar_Click;
            this.TPeriodo.Cerrar.Click += this.TPeriodo_Cerrar_Click;
        }
        #endregion

        #region barra de herramientas
        private void TPeriodo_Editar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as PeriodoModel;
                if (_seleccionado != null) {
                    this.OnSeleccionar(_seleccionado);
                } else {
                    this.OnSeleccionar(null);
                }
                this.Close();
            }
        }

        private void TPeriodo_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Common.Forms.Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.currentPeriodos;
        }

        private void TPeriodo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.currentPeriodos = this.service.GetList<PeriodoModel>(PeriodoService.Query().Year(this.TPeriodo.GetEjercicio()).Month(this.TPeriodo.GetMes()).Build()).ToList();
        }
        #endregion
    }
}
