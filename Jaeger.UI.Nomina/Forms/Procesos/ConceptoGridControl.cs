﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class ConceptoGridControl : Common.Forms.GridStandarControl {
        public ConceptoGridControl() : base() {
            this.Load += ConceptoGridControl_Load;
        }

        private void ConceptoGridControl_Load(object sender, EventArgs e) {
            this.ShowNuevo = true;
            this.ShowEditar = true;
            this.ShowRemover = true;
            this.ShowHerramientas = true;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this.CreateView();
        }

        public virtual void CreateView() {
            IConceptoGridBuilder view = new ConceptoGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());

            var _idTipo = this.GridData.Columns["IdTipo"] as GridViewComboBoxColumn;
            _idTipo.DataSource = CommonService.GetConceptoTipo();
            _idTipo.ValueMember = "Id";
            _idTipo.DisplayMember = "Descripcion";

            var _IdAplicacion = this.GridData.Columns["IdAplicacion"] as GridViewComboBoxColumn;
            _IdAplicacion.DataSource = CommonService.GetAplicacion();
            _IdAplicacion.ValueMember = "Id";
            _IdAplicacion.DisplayMember = "Descripcion";

            var _IdBaseISR = this.GridData.Columns["IdBaseISR"] as GridViewComboBoxColumn;
            _IdBaseISR.DataSource = CommonService.GetConceptoBase();
            _IdBaseISR.ValueMember = "Id";
            _IdBaseISR.DisplayMember = "Descripcion";
        }
    }
}
