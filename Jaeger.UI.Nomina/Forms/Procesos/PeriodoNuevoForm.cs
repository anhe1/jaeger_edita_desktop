﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.UI.Common.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class PeriodoNuevoForm : RadForm {
        protected NominaPeriodoDetailModel currentPeriodo;
        protected IPeriodoService service;

        public PeriodoNuevoForm(IPeriodoService service) {
            InitializeComponent();
            this.service = service;
        }

        private void PeriodoNuevoForm_Load(object sender, EventArgs e) {
            this.TipoPeriodo.DataSource = CommonService.GetPeriodos();
            this.TipoPeriodo.ValueMember = "Id";
            this.TipoNomina.DataSource = CommonService.GetTiposNomina();
            this.TipoNomina.ValueMember = "Id";
            this.currentPeriodo = new NominaPeriodoDetailModel();
            this.currentPeriodo.FechaInicio = CommonService.GetPrimerDiaSemana();
            this.currentPeriodo.Status = PeriodoStatusEnum.PorAutorizar;
            this.currentPeriodo.FechaFinal = currentPeriodo.FechaInicio.AddDays(value: 6);
            this.currentPeriodo.FechaPago = currentPeriodo.FechaFinal.AddDays(1);
            this.currentPeriodo.Descripcion = "Semana Núm.: " + CommonService.NumeroSemana(currentPeriodo.FechaInicio).ToString();
            this.currentPeriodo.Tipo = NominaTipoEnum.Extraordinaria;
            this.currentPeriodo.TipoPeriodo = PeriodoTipoEnum.Semanal;
            this.CreateBinding();
        }

        private void CreateBinding() {
            this.Descripcion.DataBindings.Add("Text", this.currentPeriodo, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FecInicio.DataBindings.Add("Value", this.currentPeriodo, "FechaInicio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FecFinal.DataBindings.Add("Value", this.currentPeriodo, "FechaFinal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FecPago.DataBindings.Add("Value", this.currentPeriodo, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoPeriodo.DataBindings.Add("SelectedValue", this.currentPeriodo, "IdPeriodoTipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoNomina.DataBindings.Add("SelectedValue", this.currentPeriodo, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void BAceptar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Crear)) {
                espera.Text = "Creando periodo, un momento por favor...";
                espera.ShowDialog(this);
            }
        }

        private void BCancelar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Crear() {
            this.service.Save(this.currentPeriodo);
        }
    }
}
