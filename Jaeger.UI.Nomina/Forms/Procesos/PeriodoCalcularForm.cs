﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class PeriodoCalcularForm : RadForm {
        protected ICalculoService service;
        protected IPeriodoService periodoService;
        private INominaPeriodoDetailModel currentPeriodo;
        private BackgroundWorker preparar;

        public PeriodoCalcularForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void PeriodoCalcularForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridEmpleados.CreateView();
            this.TPeriodo.Enabled = false;
            this.TPeriodo.Abrir.Click += this.TPeriodo_Abrir_Click;
            this.TPeriodo.Nuevo.Click += this.TPeriodo_Nuevo_Click;
            this.TPeriodo.Reciente.Click += TPeriodo_Reciente_Click;
            this.TPeriodo.Guardar.Click += this.TPeriodo_Guardar_Click;
            this.TPeriodo.Calcular.Click += this.TPeriodo_Calcular_Click;
            this.TPeriodo.Cerrar.Click += this.TPeriodo_Cerrar_Click;
            this.TPeriodo.Eliminar.Click += this.TPeriodo_Eliminar_Click;
            this.TPeriodo.Filtro.Click += this.TPeriodo_Filtro_Click;
            this.TPeriodo.Actualizar.Visibility = ElementVisibility.Collapsed;
            this.gridEmpleados.AgregarHorasExtra.Click += TPeriodo_AgregarHorasExtra_Click;
            this.gridEmpleados.AgregarAusencias.Click += TPeriodo_AgregarAusencias_Click;
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.preparar.RunWorkerAsync();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e); 
        }

        #region barra de herramientas
        private void TPeriodo_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new PeriodoNuevoForm(this.periodoService);
            _nuevo.ShowDialog(this);
        }

        private void TPeriodo_Abrir_Click(object sender, EventArgs e) {
            var _periodo = new PeriodoAbrirForm(this.periodoService);
            _periodo.PeriodoSeleccionado += Periodo_Seleccionado;
            _periodo.ShowDialog(this);
        }

        private void Periodo_Seleccionado(object sender, PeriodoModel e) {
            if (e != null) {
                this.currentPeriodo = new NominaPeriodoDetailModel {
                    IdPeriodo = e.IdPeriodo
                };

                using (var espera = new Waiting2Form(this.Abrir)) {
                    espera.Text = "Cargando datos ...";
                    espera.ShowDialog(this);
                    this.CreateBinding();
                }
            } else {
                MessageBox.Show("No");
            }
        }

        private void TPeriodo_Reciente_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Ultimo)) {
                espera.ShowDialog(this);
            }
        }

        private void TPeriodo_Eliminar_Click(object sender, EventArgs e) {
            var seleccionado = this.gridEmpleados.ReturnRowSelected() as NominaPeriodoEmpleadoDetailModel;
            if (seleccionado != null) {
                if (seleccionado.Id > 0) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover el elemento seleccionado? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    }
                } else {
                    this.currentPeriodo.Empleados.Remove(seleccionado);
                }
            }
        }

        private void TPeriodo_Actualizar_Click(object sender, EventArgs e) {
            this.CreateBinding();
        }

        private void TPeriodo_Calcular_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Calcular)) {
                espera.Text = "Procesando ...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void TPeriodo_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void TPeriodo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TPeriodo_Filtro_Click(object sender, EventArgs e) {
            this.gridEmpleados.ShowFilteringRow = this.TPeriodo.Filtro.ToggleState != ToggleState.On;
            if (this.gridEmpleados.ShowFilteringRow == false) {
                this.gridEmpleados.FilterDescriptors.Clear();
            }
        }

        private void TPeriodo_Exportar_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExportForm(this.gridEmpleados) { StartPosition = FormStartPosition.CenterParent };
            exportar.ShowDialog(this);
        }
        #endregion

        #region menu contextual
        private void TPeriodo_AgregarHorasExtra_Click(object sender, EventArgs e) {
            using (var d0 = new HorasExtraForm()) {
                d0.Agregar += HorasExtra_Agregar;
                d0.ShowDialog(this);
            }
        }

        private void HorasExtra_Agregar(object sender, IRegistroHoraExtraModel e) {
            if (e != null) {
                var seleccionado = this.gridEmpleados.CurrentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.HorasExtra == null) { seleccionado.HorasExtra = new BindingList<IRegistroHoraExtraModel>(); }
                    e.IdNomina = this.currentPeriodo.IdPeriodo;
                    e.IdEmpleado = seleccionado.IdEmpleado;
                    e.IdTipoHora = 0;
                    e.IdConcepto = 1;

                    seleccionado.HorasExtra.Add(e);
                }
                seleccionado.IsChange = true;
                var row = ((GridViewHierarchyRowInfo)this.gridEmpleados.CurrentRow);
                foreach (var item in row.Views) {
                    item.Refresh();
                }
            }
        }

        public virtual void TPeriodo_AgregarAusencias_Click(object sender, EventArgs e) {
            var s1 = this.gridEmpleados.CurrentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
            using (var v1 = new RegistroAusenciaForm(s1)) {
                v1.AgregarDias += TPeriodo_AgregarDias;
                v1.ShowDialog(this);
            }
        }

        private void TPeriodo_AgregarDias(object sender, System.Collections.Generic.List<IRegistroAusenciasModel> e) {
            if (e != null) {
                if (e.Count > 0) {
                    var s1 = this.gridEmpleados.CurrentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
                    s1.IsChange = true;
                    if (s1.Ausencias == null) { s1.Ausencias = new BindingList<IRegistroAusenciasModel>(); }
                    foreach (var v1 in e) {
                        s1.Ausencias.Add(v1);
                    }
                }
            }
        }
        #endregion

        private void CreateBinding() {
            this.Estado.DataBindings.Clear();
            this.Estado.DataBindings.Add("SelectedValue", this.currentPeriodo, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoNomina.DataBindings.Clear();
            this.TipoNomina.DataBindings.Add("SelectedValue", this.currentPeriodo, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoPeriodo.DataBindings.Clear();
            this.TipoPeriodo.DataBindings.Add("SelectedValue", this.currentPeriodo, "IdPeriodoTipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this.currentPeriodo, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaInicio.DataBindings.Clear();
            this.FechaInicio.DataBindings.Add("Value", this.currentPeriodo, "FechaInicio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaFinal.DataBindings.Clear();
            this.FechaFinal.DataBindings.Add("Value", this.currentPeriodo, "FechaFinal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaPago.DataBindings.Clear();
            this.FechaPago.SetToNullValue();
            this.FechaPago.DataBindings.Add("Value", this.currentPeriodo, "FechaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.gridEmpleados.DataSource = this.currentPeriodo.Empleados;
            this.Desabilitar();
        }

        private void Desabilitar() {
            //this.textBoxDescripcion.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.comboBoxTipoNomina.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.comboBoxTipoPeriodo.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.dtpFechaInicio.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.dtpFechaFinal.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.DtpFechaPago.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.comboBoxEstado.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.GridData.AllowEditRow = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
            //this.ToolBarButtonGuardar.Enabled = !(this.currentPeriodo.Estado == Domain.Nomina.ValueObjects.NominaStatusEnum.Autorizada);
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {

            this.Estado.DataSource = CommonService.GetPeriodoStatus();
            this.Estado.DisplayMember = "Descripcion";
            this.Estado.ValueMember = "Id";

            this.TipoNomina.DataSource = CommonService.GetTiposNomina();
            this.TipoNomina.DisplayMember = "Descripcion";
            this.TipoNomina.ValueMember = "Id";

            this.TipoPeriodo.DataSource = CommonService.GetPeriodos();
            this.TipoPeriodo.DisplayMember = "Descripcion";
            this.TipoPeriodo.ValueMember = "Id";
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.TPeriodo.Enabled = true;
        }

        #region metodos privados
        private void Guardar() {
            this.currentPeriodo = this.service.Save(this.currentPeriodo);
        }

        private void Calcular() {
            this.service.CalcularPeriodo(this.currentPeriodo);
            this.currentPeriodo = this.service.GetPeriodo(this.currentPeriodo.IdPeriodo);
        }

        private void Ultimo() {
            var cPeriodo = this.service.GetLast() as NominaPeriodoDetailModel;
            if (cPeriodo != null) {
                Periodo_Seleccionado(null, cPeriodo);
            }
        }

        private void Abrir() {
            if (this.currentPeriodo.IdPeriodo > 0) {
                if (this.currentPeriodo.Status == Domain.Nomina.ValueObjects.PeriodoStatusEnum.PorAutorizar) {
                    this.currentPeriodo = this.service.GetPeriodo(this.currentPeriodo.IdPeriodo);
                }
            }
        }
        #endregion
    }
}
