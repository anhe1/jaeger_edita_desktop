﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class ConceptoCatalogoForm : RadForm {
        protected internal IConceptoService service;
        private BindingList<INominaConceptoDetailModel> _DataSource;

        public ConceptoCatalogoForm() {
            InitializeComponent();
        }

        private void ConceptoCatalogoForm_Load(object sender, EventArgs e) {
            this.TConcepto.Nuevo.Click += this.TConcepto_Agregar_Click;
            this.TConcepto.Editar.Click += this.TConcepto_Editar_Click;
            this.TConcepto.Remover.Click += this.TConcepto_Remover_Click;
            this.TConcepto.Actualizar.Click += this.TConcepto_Actualizar_Click;
            this.TConcepto.Cerrar.Click += this.TConcepto_Cerrar_Click;
        }

        #region barra de herramientas
        private void TConcepto_Agregar_Click(object sender, EventArgs e) {
            using (var nuevo = new ConceptoForm(null, this.service)) {
                nuevo.StartPosition = FormStartPosition.CenterParent;
                nuevo.ShowDialog(this);
            }
        }

        private void TConcepto_Editar_Click(object sender, EventArgs e) {
            if (this.TConcepto.GridData.CurrentRow != null) {
                var seleccionado = this.TConcepto.GridData.CurrentRow.DataBoundItem as NominaConceptoDetailModel;
                if (seleccionado != null) {
                    using (var nuevo = new ConceptoForm(seleccionado, this.service)) {
                        nuevo.StartPosition = FormStartPosition.CenterParent;
                        nuevo.ShowDialog(this);
                    }
                }
            }
        }

        private void TConcepto_Remover_Click(object sender, EventArgs e) {
            if (this.TConcepto.GridData.CurrentRow != null) {
                var seleccionado = this.TConcepto.GridData.CurrentRow.DataBoundItem as NominaConceptoDetailModel;
                if (seleccionado != null) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover el objeto selecciondo?", "Ateención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                        return;
                    }
                    seleccionado.Activo = false;
                    using (var espera = new Waiting2Form(this.Guardar)) {
                        espera.Text = "Un momento por favor...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        private void TConcepto_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consulta)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.TConcepto.GridData.DataSource = this._DataSource;
        }

        private void ToolBar_Exportar_Click(object sender, EventArgs e) {
            using (var exportar = new TelerikGridExportForm(this.TConcepto.GridData))
                exportar.ShowDialog();
        }

        private void TConcepto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Consulta() {
            this._DataSource = this.service.GetList(true);
        }

        private void Guardar() {
            var seleccionado = this.TConcepto.GridData.CurrentRow.DataBoundItem as INominaConceptoDetailModel;
            if (seleccionado != null) {
                seleccionado = this.service.Save(seleccionado);
                this.TConcepto.GridData.Rows.Remove(this.TConcepto.GridData.CurrentRow);
            }
        }
        #endregion
    }
}
