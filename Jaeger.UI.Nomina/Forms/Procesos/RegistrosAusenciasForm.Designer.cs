﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class RegistrosAusenciasForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TRegistro = new Jaeger.UI.Nomina.Forms.Procesos.RegistroAusenciasGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TRegistro
            // 
            this.TRegistro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TRegistro.Location = new System.Drawing.Point(0, 0);
            this.TRegistro.Name = "TRegistro";
            this.TRegistro.PDF = null;
            this.TRegistro.ShowActualizar = true;
            this.TRegistro.ShowAutosuma = false;
            this.TRegistro.ShowCancelar = false;
            this.TRegistro.ShowCerrar = true;
            this.TRegistro.ShowEditar = true;
            this.TRegistro.ShowEjercicio = true;
            this.TRegistro.ShowExportarExcel = false;
            this.TRegistro.ShowFiltro = true;
            this.TRegistro.ShowHerramientas = false;
            this.TRegistro.ShowImprimir = false;
            this.TRegistro.ShowItem = false;
            this.TRegistro.ShowNuevo = true;
            this.TRegistro.ShowPeriodo = true;
            this.TRegistro.ShowSeleccionMultiple = true;
            this.TRegistro.Size = new System.Drawing.Size(800, 450);
            this.TRegistro.TabIndex = 0;
            // 
            // RegistrosVacacionesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TRegistro);
            this.Name = "RegistrosVacacionesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RegistrosVacacionesForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RegistrosAusenciasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private RegistroAusenciasGridControl TRegistro;
    }
}