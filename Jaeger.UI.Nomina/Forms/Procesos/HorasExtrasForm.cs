﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class HorasExtrasForm : RadForm {
        protected internal IHorasExtraService _horasExtraService;
        protected internal BindingList<IRegistroHoraExtraModel> _DataSource;

        public HorasExtrasForm() {
            InitializeComponent();
        }

        private void HorasExtrasForm_Load(object sender, EventArgs e) {
            this.THorasExtra.Nuevo.Click += THorasExtra_Nuevo_Click;
            this.THorasExtra.Actualizar.Click += Actualizar_Click;
        }

        private void THorasExtra_Nuevo_Click(object sender, EventArgs e) {
            
        }

        protected virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
        }

        protected virtual void Consultar() {
            //var query = HorasExtraService.Query().IdNomina(0).Build();
            //this._DataSource= new BindingList<IRegistroHoraExtraModel>(
            //    this._horasExtraService.GetList<RegistroHoraExtraModel>(query).ToList<IRegistroHoraExtraModel>());
        }
    }
}
