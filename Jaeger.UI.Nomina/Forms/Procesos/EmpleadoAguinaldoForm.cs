﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class EmpleadoAguinaldoForm : RadForm {
        protected IAguinaldoService service;
        private BindingList<IEmpleadoCalcularAguinaldo> empleados;
        private readonly RadMenuItem _TEmpleado_Imprimir_Resumen = new RadMenuItem { Text = "Resumen" };
        private readonly RadMenuItem _TEmpleado_Imprimir_Completo = new RadMenuItem { Text = "Listado completo" };
        private readonly RadMenuItem _TEmpleado_Layout = new RadMenuItem { Text = "Layout Banco" };

        public EmpleadoAguinaldoForm() {
            InitializeComponent();
        }

        private void EmpleadoAguinaldoForm_Load(object sender, EventArgs e) {
            this.Text = "Movimientos: Aguinaldo";
            this.TEmpleado.Actualizar.Text = "Cargar";
            this.TEmpleado.Actualizar.Click += this.TEmpleado_Actualizar_Click;
            this.TEmpleado.ExportarExcel.Click += this.TEmpleado_ExportarExcel_Click;
            this.TEmpleado.Cerrar.Click += this.TEmpleado_Cerrar_Click;
            this.TEmpleado.Imprimir.Items.AddRange(new Telerik.WinControls.RadItem[] { this._TEmpleado_Imprimir_Completo, this._TEmpleado_Imprimir_Resumen });
            this._TEmpleado_Imprimir_Completo.Click += this.TEmpleado_Imprimir_Completo_Click;
            this._TEmpleado_Imprimir_Resumen.Click += TEmpleado_Imprimir_Resumen_Click;
            this.TEmpleado.Herramientas.Items.Add(this._TEmpleado_Layout);
            this._TEmpleado_Layout.Click += TEmpleado_Layout_Click;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this.service = new AguinaldoService();
        }

        #region barra de herramientas
        private void TEmpleado_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Cargar)) {
                espera.Text = "Cargando empleados";
                espera.ShowDialog(this);
            }
            this.TEmpleado.GridData.DataSource = this.empleados;
        }

        private void TEmpleado_ExportarExcel_Click(object sender, EventArgs e) {
            var _exportar = new TelerikGridExportForm(this.TEmpleado.GridData);
            _exportar.ShowDialog(this);
        }

        private void TEmpleado_Imprimir_Completo_Click(object sender, EventArgs e) {
            var _reporte = new ReporteForm(new EmpleadoAguinaldoPrinter { Empleados = this.empleados });
            _reporte.Show();
        }

        private void TEmpleado_Imprimir_Resumen_Click(object sender, EventArgs e) {
            var _reporte = new ReporteForm(new EmpleadoAguinaldoPrinter { Empleados = this.empleados, Resumen = true });
            _reporte.Show();
        }

        private void TEmpleado_Layout_Click(object sender, EventArgs e) {
            //var _layout = new LayoutBancoExportarForm();
            //_layout.ShowDialog(this);
        }

        private void TEmpleado_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Cargar() {
            this.empleados = this.service.GetAguinaldos(this.TEmpleado.GetEjercicio());
        }
        #endregion
    }
}
