﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class HorasExtraForm : RadForm {
        public event EventHandler<IRegistroHoraExtraModel> Agregar;
        public void OnAgregar(IRegistroHoraExtraModel e) {
            if (this.Agregar != null) {
                this.Agregar(this, e);
            }
        }

        public HorasExtraForm() {
            InitializeComponent();
        }

        private void HorasExtraForm_Load(object sender, EventArgs e) {

        }

        private void Agregar_Click(object sender, EventArgs e) {
            var d0 = new RegistroHoraExtraModel {
                Fecha = this.Fecha.Value,
                Cantidad = int.Parse(this.Cantidad.Value.ToString())
            };
            this.OnAgregar(d0);
            this.Close();
        }

        private void Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
