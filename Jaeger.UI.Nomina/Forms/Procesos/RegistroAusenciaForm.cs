﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Aplication.Nomina.Services;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class RegistroAusenciaForm : RadForm {
        #region 
        private NominaPeriodoEmpleadoDetailModel _IdEmpleado;
        protected internal IRegistroAusenciasService _Service;
        protected internal List<IRegistroAusenciasModel> _DataSoruce;
        #endregion

        public event EventHandler<List<IRegistroAusenciasModel>> AgregarDias;
        public void OnAgregarDias(List<IRegistroAusenciasModel> e) {
            if (this.AgregarDias != null) {
                this.AgregarDias(this, e);
            }
        }

        public RegistroAusenciaForm(NominaPeriodoEmpleadoDetailModel idEmpleado) {
            InitializeComponent();
            this._IdEmpleado = idEmpleado;
        }

        private void RegistroAusenciaForm_Load(object sender, EventArgs e) {
            this._DataSoruce = new List<IRegistroAusenciasModel>();
            
            this.Calendario.AllowMultipleSelect = true;
            this.Calendario.ShowItemToolTips = true;
            this.Tipo.DataSource = RegistroAusenciasService.TipoDias();
            this.Tipo.DisplayMember = "Descriptor";
            this.Tipo.ValueMember = "Id";

            //var d1 = this._Service.GetDiasFestivos();
            //foreach (var item in d1) {
            //    var c0 = new RadCalendarDay(item.Fecha) {
            //        ToolTip = item.Descripcion
            //    };
            //    this.Calendario.SpecialDays.Add(c0);
            //}
        }

        private void Aceptar_Click(object sender, EventArgs e) {
            if (this.Calendario.SelectedDates.Count == 0) {
                RadMessageBox.Show(this, "Selecciona al menos un día", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            var s1 = this.Tipo.SelectedItem.DataBoundItem as DiasTipoModel;
            var seleccionados = this.Calendario.SelectedDates;
            foreach (var item in seleccionados) {
                this._DataSoruce.Add(new RegistroAusenciasModel {
                    Fecha = item,
                    IdEmpleado = this._IdEmpleado.IdEmpleado,
                    IdNomina = this._IdEmpleado.IdPeriodo,
                    IdTipo = s1.Id
                });
            }

            this.OnAgregarDias(this._DataSoruce);
            this.Close();
        }

        private void Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
