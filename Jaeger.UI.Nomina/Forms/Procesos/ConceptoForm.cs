﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class ConceptoForm : RadForm {
        protected IConceptoService service;
        protected ITipoPercepcionCatalogo percepcionCatalogo;
        protected ITipoDeduccionCatalogo deduccionCatalogo;

        private INominaConceptoDetailModel concepto;

        public ConceptoForm(NominaConceptoDetailModel concepto, IConceptoService service) {
            InitializeComponent();
            this.concepto = concepto;
            this.service = service;
        }

        private void ConceptoNominaForm_Load(object sender, EventArgs e) {

            this.TConcepto.Guardar.Click += this.TConcepto_Guardar_Click;
            this.TConcepto.Cerrar.Click += this.TConcepto_Cerrar_Click;

            this.percepcionCatalogo = new TipoPercepcionCatalogo();
            this.percepcionCatalogo.Load();

            this.deduccionCatalogo = new TipoDeduccionCatalogo();
            this.deduccionCatalogo.Load();

            
            
            if (this.concepto == null)
                this.concepto = new NominaConceptoDetailModel();

            
            this.Aplicacion.DataSource = CommonService.GetAplicacion();
            this.TipoNomina.DataSource = CommonService.GetTiposNomina();
            this.ConceptoBase.DataSource = CommonService.GetConceptoBase();
            this.ConceptoTipo.DataSource = CommonService.GetConceptoTipo();
            this.CreateBinding();
        }

        private void TConcepto_Guardar_Click(object sender, EventArgs e) {
            if (this.concepto.ClaveSAT != null) {
                if (this.concepto.ClaveSAT.Trim().Length != 3) {
                    RadMessageBox.Show(this, "Es necesario un proporcionar clave SAT de 3 caracteres", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
            }

            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando...";
                espera.ShowDialog(this);
            }
            this.Close();
        }

        private void TConcepto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            
            this.Clave.DataBindings.Clear();
            this.Clave.DataBindings.Add("Text", this.concepto, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ConceptoTipo.DataBindings.Clear();
            this.ConceptoTipo.DataBindings.Add("SelectedValue", this.concepto, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.checkActivo.DataBindings.Clear();
            this.checkActivo.DataBindings.Add("Checked", this.concepto, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this.concepto, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Aplicacion.DataBindings.Clear();
            this.Aplicacion.DataBindings.Add("SelectedValue", this.concepto,"IdAplicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoNomina.DataBindings.Clear();
            this.TipoNomina.DataBindings.Add("SelectedValue", this.concepto, "IdAplicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Formula.DataBindings.Clear();
            this.Formula.DataBindings.Add("Text", this.concepto, "Formula", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ClaveSAT.DataBindings.Clear();
            this.ClaveSAT.DataBindings.Add("Text", this.concepto, "ClaveSAT", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this.concepto, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PagoEspecie.DataBindings.Clear();
            this.PagoEspecie.DataBindings.Add("Checked", this.concepto, "PagoEspecie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Ocultar.DataBindings.Clear();
            this.Ocultar.DataBindings.Add("Checked", this.concepto, "Visible", true, DataSourceUpdateMode.OnPropertyChanged);


            this.ConceptoBase.DataBindings.Clear();
            this.ConceptoBase.DataBindings.Add("SelectedValue", this.concepto, "IdBaseISR", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ConceptoBaseFormula.DataBindings.Clear();
            this.ConceptoBaseFormula.DataBindings.Add("Text", this.concepto, "FormulaISR", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Clave.ReadOnly = this.concepto.IdConcepto > 0;
            this.ConceptoTipo.SetEditable(!(this.concepto.IdConcepto > 0));
            this.ISR.Enabled = this.concepto.Tipo == NominaConceptoTipoEnum.Percepcion;
        }

        private void Guardar() {
            this.concepto = this.service.Save(this.concepto);
        }

        private void ConceptoTipo_Validated(object sender, EventArgs e) {
            if (this.concepto.Tipo == NominaConceptoTipoEnum.Percepcion) {
                this.ConceptoBase.Enabled = true;
                this.ConceptoBaseFormula.Enabled = true;
                this.ClaveSAT.DataSource = this.percepcionCatalogo.Items;
            } else {
                this.ConceptoBase.Enabled = false;
                this.ConceptoBaseFormula.Enabled = false;
                this.ClaveSAT.DataSource = this.deduccionCatalogo.Items;
            }
        }
    }
}
