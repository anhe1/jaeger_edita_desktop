﻿using System;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class HorasExtraGridControl : Common.Forms.GridCommonControl {
        public HorasExtraGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e); 
            IHorasExtraGridViewBuilder view = new HorasExtraGridViewBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }
    }
}
