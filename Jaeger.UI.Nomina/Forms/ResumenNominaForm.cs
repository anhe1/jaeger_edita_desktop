﻿using System;
using System.Windows.Forms;
using Telerik.Pivot.Core;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Nomina.Forms {
    public partial class ResumenNominaForm : RadForm {
        protected internal IControlService _Service;
        public ResumenNominaForm() {
            InitializeComponent();
        }

        private void ResumenNominaForm_Load(object sender, EventArgs e) {
            this.ToolBarButtonPeriodo.DisplayMember = "Descripcion";
            this.ToolBarButtonPeriodo.ValueMember = "Id";
            this.ToolBarButtonPeriodo.DataSource = ConfigService.GetMeses();
            this.ToolBarButtonPeriodo.SelectedIndex = DateTime.Now.Month;
            Ejercicio.Maximum = DateTime.Now.Year;
            Ejercicio.Value = DateTime.Now.Year;
            this.CreateDefault();
        }

        private void BActualizar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Solicitando datos al servidor";
                espera.ShowDialog(this);
            }
        }

        private void BExportar_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog = new SaveFileDialog() {
                Filter = "Excel ML|*.xlsx",
                Title = "Export to File"
            };
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                bool correcto = HelperTelerikExport.RunExportPivotGrid(this.GridData, saveFileDialog.FileName, "Resumen");
                if (correcto) {
                    if (RadMessageBox.Show("Se exporto correctamente. ¿Quieres abrir el documento?", "Exportar", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                        try {
                            System.Diagnostics.Process.Start(saveFileDialog.FileName);
                        } catch (Exception ex) {
                            string message = string.Format("El archivo no se puede abrir en su sistema. Error message: {0}", ex.Message);
                            RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                    } else {
                        RadMessageBox.Show("No fué posible exportar la información al formato solicitado.", "Exportar", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        private void BCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            if (this.radioPorPeriodo.IsChecked) {
                this.GridData.DataSource = this._Service.GetResumen(int.Parse(this.Ejercicio.Value.ToString()), this.GetMes());
            } else if (this.radioPorNomina.IsChecked) {
                this.GridData.DataSource = this._Service.GetResumen(int.Parse(this.cboNominas.SelectedValue.ToString()));
            }
        }

        private void CreateDefault() {
            //configuracion de la tabla pivote
            var m1 = new SumAggregateFunction();

            var pConcepto = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Concepto",
                PropertyName = "Concepto"
            };

            var pNumEmpleado = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Num. Empleado",
                PropertyName = "NumEmpleado"
            };

            var pIdDocumento = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Id Documento",
                PropertyName = "iddocumento"
            };

            var pEstado = new PropertyGroupDescription() {
                GroupComparer = new GroupNameComparer(),
                CustomName = "Estado",
                PropertyName = "Estado"
            };

            var sumAggregateImporteGravado = new PropertyAggregateDescription() {
                AggregateFunction = new SumAggregateFunction(),
                CustomName = "Gravado",
                PropertyName = "ImporteGravado",
                StringFormat = "#,###0.00"
            };

            var sumAggregateImporteExento = new PropertyAggregateDescription() {
                AggregateFunction = new SumAggregateFunction(),
                CustomName = "Exento",
                PropertyName = "ImporteExento",
                StringFormat = "#,###0.00"
            };

            this.GridData.ColumnGroupDescriptions.Add(pConcepto);
            this.GridData.RowGroupDescriptions.Add(pEstado);
            this.GridData.RowGroupDescriptions.Add(pNumEmpleado);
            this.GridData.RowGroupDescriptions.Add(pIdDocumento);
            this.GridData.AggregateDescriptions.Add(sumAggregateImporteGravado);
            this.GridData.AggregateDescriptions.Add(sumAggregateImporteExento);
        }

        private void Options_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            if (this.radioPorNomina.IsChecked) {
                this.cboNominas.Enabled = true;
                this.ToolBarButtonPeriodo.Enabled = false;
                this.Ejercicio.Enabled = false;
                this.cboNominas.DataSource = this._Service.GetNominas();
            } else if (this.radioPorPeriodo.IsChecked) {
                this.cboNominas.Enabled = false;
                this.ToolBarButtonPeriodo.Enabled = true;
                this.Ejercicio.Enabled = true;
            }
        }

        private void GridData_DataProviderChanged(object sender, DataProviderChangedEventArgs e) {
            this.buttonExportar.Enabled = true;
        }

        public int GetMes() {
            if (this.ToolBarButtonPeriodo.SelectedValue != null) {
                return (int)this.ToolBarButtonPeriodo.SelectedValue;
            } else {
                return (int)Enum.Parse(typeof(MesesEnum), this.ToolBarButtonPeriodo.Text);
            }
        }
    }
}
