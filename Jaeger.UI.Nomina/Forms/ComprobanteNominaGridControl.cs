﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Nomina.Builder;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Util;

namespace Jaeger.UI.Nomina.Forms {
    public class ComprobanteNominaGridControl : UI.Common.Forms.GridCommonControl {
        public ComprobanteNominaGridControl() : base() {
            IComprobanteGridBuilder view = new ComprobanteGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }

        protected override void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name.ToLower() == "UrlFileXml".ToLower() || e.Column.Name.ToLower() == "UrlFilePdf".ToLower()) {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name.ToLower() != "Status".ToLower()) {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        protected override void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name.ToLower() == "UrlFileXml".ToLower() || e.Column.Name.ToLower() == "UrlFilePdf".ToLower()) {
                        var single = this.GridData.CurrentRow.DataBoundItem as ComprobanteNominaSingleModel;
                        string liga = "";
                        if (e.Column.Name.ToLower() == "UrlFileXml".ToLower())
                            liga = single.UrlFileXML;
                        else
                            if (e.Column.Name.ToLower() == "UrlFilePdf".ToLower())
                            liga = single.UrlFilePDF;
                        // validar la liga de descarga
                        if (ValidacionService.URL(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = System.IO.Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
