﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;
using Jaeger.Util;

namespace Jaeger.UI.Nomina.Forms {
    public partial class ComprobantesFiscalesForm : RadForm {
        protected internal RadContextMenu menuContextual = new RadContextMenu();
        protected internal RadMenuItem MenuContextual_Copiar = new RadMenuItem { Text = "Copiar" };
        protected internal RadMenuItem MenuContextual_MultiSeleccion = new RadMenuItem { Text = "Selección múltiple" };
        protected internal RadMenuItem TImportar = new RadMenuItem { Text = "Importar" };
        protected internal IControlService _Service;
        protected internal List<ComprobanteNominaSingleModel> _DataSource;

        public ComprobantesFiscalesForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ComprobantesFiscalesForm_Load(object sender, EventArgs e) {
            this.GridData.Standard();

            this.menuContextual.Items.Add(this.MenuContextual_Copiar);
            this.menuContextual.Items.Add(this.MenuContextual_MultiSeleccion);

            this.ButtonBuscar.Click += new EventHandler(this.TNomina_Actualizar_Click);
            this.TNomina.Filtro.Click += this.TNomina_Filtro_Click;
            this.TNomina.Herramientas.Items.Add(this.TImportar);
        }

        #region barra de herramientas
        private void TNomina_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Solicitando datos al servidor";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._DataSource;
        }

        private void TNomina_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        private void ButtonExportarExcel_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExportForm(this.GridData);
            exportar.ShowDialog(this);
        }

        private void ButtonDescargarTodo_Click(object sender, EventArgs e) {
            var folder = new FolderBrowserDialog() { Description = "Selecciona la carpeta de trabajo para" };
            if (folder.ShowDialog() != DialogResult.OK)
                return;
            this.GridData.SelectAll();
            this.Tag = folder.SelectedPath;
            using (var espera = new Waiting1Form(this.DescargaBackup)) {
                espera.Text = "Descargando ...";
                espera.ShowDialog();
            }
        }

        private void ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        private void ContextMenuSeleccion_Click(object sender, EventArgs e) {
            //this.contextMenuSeleccion.IsChecked = !this.contextMenuSeleccion.IsChecked;
            //this.GridData.MultiSelect = this.contextMenuSeleccion.IsChecked;
        }

        private void ContextMenuCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        private void contextMenuEstadoSAT_Click(object sender, EventArgs e) {
            //using (var espera = new Waiting2Form(this.EstadoSAT)) {
            //    espera.Text = "Consultando estado, un momento por favor...";
            //    espera.ShowDialog(this);
            //}
            //var response = (string)this.contextMenuEstadoSAT.Tag;
            //if (response != null)
            //    RadMessageBox.Show(this, response, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            //this.contextMenuEstadoSAT.Tag = null;
        }

        private void ContextButtonSubirXML_Click(object sender, EventArgs e) {
            //var openFileDialog = new OpenFileDialog() { Filter = "*.xml|*.XML", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            //if (openFileDialog.ShowDialog() != DialogResult.OK) {
            //    return;
            //}

            //var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteNominaSingleModel;
            //if (seleccionado != null) {
            //    if (ValidacionService.URL(seleccionado.UrlFileXML)) {
            //        if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName, ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            //            return;
            //    }

            //    if (File.Exists(openFileDialog.FileName) == false) {
            //        RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
            //        return;
            //    }
            //    this.ContextButtonSubirXML.Tag = openFileDialog.FileName;
            //    //using (var espera = new Waiting2Form(this.ReemplazaXML)) {
            //    //    espera.Text = "Reemplazando, espere un momento...";
            //    //    espera.ShowDialog(this);
            //    //}

            //    if (this.ContextButtonSubirXML.Tag != null) {
            //        if ((bool)this.ContextButtonSubirXML.Tag == false) {
            //            RadMessageBox.Show(this, "Ocurrio un error al procesar el archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
            //            RadMessageBox.Show(this, (string)this.Tag, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
            //            this.Tag = null;
            //        }
            //    }
            //}
        }

        private void ContextButtonSubirPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var openFileDialog = new OpenFileDialog() { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
                if (openFileDialog.ShowDialog() != DialogResult.OK) {
                    return;
                }

                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteNominaSingleModel;
                if (seleccionado != null) {
                    if (ValidacionService.URL(seleccionado.UrlFilePDF)) {
                        if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName, ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                            return;
                    }

                    if (File.Exists(openFileDialog.FileName) == false) {
                        RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                    //this.ContextButtonSubirPDF.Tag = openFileDialog.FileName;
                    //using (var espera = new Waiting2Form(this.ReemplazaPDF)) {
                    //    espera.Text = "Reemplazando, espere un momento...";
                    //    espera.ShowDialog(this);
                    //}

                    //if (this.ContextButtonSubirPDF.Tag != null) {
                    //    if ((bool)this.ContextButtonSubirPDF.Tag == false) {
                    //        RadMessageBox.Show(this, "Ocurrio un error al procesar el archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    //    }
                    //}
                }
            }
        }

        private void ContextMenuSerializarCFDI_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteNominaSingleModel;
                //this.service.RecuperaXML(seleccionado.Id);
            }
        }

        private void contextMenuDescargar_Click(object sender, EventArgs e) {
            var folder = new FolderBrowserDialog() { Description = "Selecciona la carpeta de trabajo para" };
            if (folder.ShowDialog() != DialogResult.OK)
                return;
            this.Tag = folder.SelectedPath;
            using (var espera = new Waiting1Form(this.DescargaBackup)) {
                espera.Text = "Descargando ...";
                espera.ShowDialog();
            }
        }

        private void DescargaBackup() {
            var seleccion = new List<ComprobanteNominaSingleModel>();
            seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ComprobanteNominaSingleModel));
            if (seleccion.Count > 0) {
                var root = (string)this.Tag;
                var descargas = new BindingList<DescargaElemento>();
                foreach (var item in seleccion) {
                    if (ValidacionService.UUID(item.IdDocumento)) {
                        string nuevaCarpeta = root;
                        string depto = item.Departamento;
                        if (depto.Length > 0)
                            nuevaCarpeta = Path.Combine(root, depto);

                        if (Directory.Exists(nuevaCarpeta) == false) {
                            try {
                                Directory.CreateDirectory(nuevaCarpeta);
                            } catch (Exception ex) {
                                Console.WriteLine(ex.Message);
                                nuevaCarpeta = root;
                            }
                        }

                        if (ValidacionService.URL(item.UrlFileXML))
                            descargas.Add(new DescargaElemento { URL = item.UrlFileXML, KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(item.UrlFileXML)) });

                        if (ValidacionService.URL(item.UrlFilePDF))
                            descargas.Add(new DescargaElemento { URL = item.UrlFilePDF, KeyName = Path.Combine(nuevaCarpeta, Path.GetFileName(item.UrlFilePDF)) });
                    }
                }

                // descarga
                var descarga = new DownloadExtended();
                descarga.ProcessDownload += Descarga_ProcessDownload;
                descarga.RunDownloadParallelSync(descargas, @"");
            }

        }
        #endregion

        #region acciones del grid
        private void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate) {
                    e.ContextMenu = this.menuContextual.DropDown;
                }
            }
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name.ToLower() == "urlfilexml" || e.Column.Name.ToLower() == "urlfilepdf") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                e.CellElement.Children.Clear();
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name.ToLower() == "urlfilexml" || e.Column.Name.ToLower() == "urlfilepdf") {
                        var single = this.GridData.CurrentRow.DataBoundItem as ComprobanteNominaSingleModel;
                        string liga = "";
                        if (e.Column.Name.ToLower() == "urlfilexml")
                            liga = single.UrlFileXML;
                        else
                            if (e.Column.Name.ToLower() == "urlfilepdf")
                            liga = single.UrlFilePDF;
                        // validar la liga de descarga
                        if (ValidacionService.URL(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        private void CheckPorNomina_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            if (this.PorNomina.CheckState == CheckState.Checked) {
                this.Nominas.DataSource = this._Service.GetNominas();
            } else {
                this.Nominas.Enabled = false;
            }
        }

        public virtual void GetNominas() {
            this.Nominas.DataSource = this._Service.GetNominas();
        }

        public virtual void Consultar() {
            this._DataSource = this._Service.GetCfdNominas((int)this.Nominas.SelectedValue);
        }

        private void EstadoSAT() {
            //if (this.gridData.CurrentRow != null) {
            //    var seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            //    if (seleccionado != null) {
            //        var response = this.service.EstadoSAT(seleccionado.EmisorRFC, seleccionado.ReceptorRFC, seleccionado.Total, seleccionado.IdDocumento);
            //        seleccionado.Estado = response;
            //        seleccionado.FechaEstado = DateTime.Now;
            //        this.contextMenuEstadoSAT.Tag = response;
            //    }
            //}
        }

        private void Descarga_ProcessDownload(object sender, DownloadChanged e) {
            this.BarraEstadoLabel.Text = e.Caption;
        }
    }
}
