﻿
namespace Jaeger.UI.Nomina.Forms {
    partial class ComprobanteNominaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.PanelIzquierdo = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEstadoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.TNomina = new Jaeger.UI.Nomina.Forms.ComprobanteNominaGridControl();
            this.Control = new Jaeger.UI.Nomina.Forms.ConsultaRecibosControl();
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).BeginInit();
            this.PanelIzquierdo.PanelContainer.SuspendLayout();
            this.PanelIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelIzquierdo
            // 
            this.PanelIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelIzquierdo.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.PanelIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.PanelIzquierdo.Name = "PanelIzquierdo";
            this.PanelIzquierdo.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 150, 648);
            // 
            // PanelIzquierdo.PanelContainer
            // 
            this.PanelIzquierdo.PanelContainer.Controls.Add(this.Control);
            this.PanelIzquierdo.PanelContainer.Size = new System.Drawing.Size(331, 567);
            this.PanelIzquierdo.Size = new System.Drawing.Size(359, 569);
            this.PanelIzquierdo.TabIndex = 1;
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEstadoLabel});
            this.BarraEstado.Location = new System.Drawing.Point(359, 543);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(740, 26);
            this.BarraEstado.TabIndex = 3;
            // 
            // BarraEstadoLabel
            // 
            this.BarraEstadoLabel.Name = "BarraEstadoLabel";
            this.BarraEstado.SetSpring(this.BarraEstadoLabel, false);
            this.BarraEstadoLabel.Text = "Listo.";
            this.BarraEstadoLabel.TextWrap = true;
            this.BarraEstadoLabel.UseCompatibleTextRendering = false;
            // 
            // TNomina
            // 
            this.TNomina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TNomina.Location = new System.Drawing.Point(359, 0);
            this.TNomina.Name = "TNomina";
            this.TNomina.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TNomina.Permisos = uiAction1;
            this.TNomina.ShowActualizar = true;
            this.TNomina.ShowAgrupar = false;
            this.TNomina.ShowAutosuma = true;
            this.TNomina.ShowCancelar = false;
            this.TNomina.ShowCerrar = true;
            this.TNomina.ShowEditar = false;
            this.TNomina.ShowEjercicio = false;
            this.TNomina.ShowExportarExcel = true;
            this.TNomina.ShowFiltro = true;
            this.TNomina.ShowHerramientas = true;
            this.TNomina.ShowImprimir = false;
            this.TNomina.ShowItem = false;
            this.TNomina.ShowNuevo = false;
            this.TNomina.ShowPeriodo = false;
            this.TNomina.ShowSeleccionMultiple = true;
            this.TNomina.Size = new System.Drawing.Size(740, 543);
            this.TNomina.TabIndex = 4;
            // 
            // Control
            // 
            this.Control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Control.Location = new System.Drawing.Point(0, 0);
            this.Control.Name = "Control";
            this.Control.Size = new System.Drawing.Size(331, 567);
            this.Control.TabIndex = 0;
            // 
            // ComprobanteNominaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 569);
            this.Controls.Add(this.TNomina);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.PanelIzquierdo);
            this.Name = "ComprobanteNominaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Recibo de Nómina";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobanteNominaForm_Load);
            this.PanelIzquierdo.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).EndInit();
            this.PanelIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCollapsiblePanel PanelIzquierdo;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadLabelElement BarraEstadoLabel;
        protected internal ComprobanteNominaGridControl TNomina;
        private ConsultaRecibosControl Control;
    }
}