﻿using System;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Nomina.Forms.Empresa {
    public partial class ConfiguracionForm : Common.Forms.ConfiguracionForm {
        public ConfiguracionForm(UIMenuElement menuElement) : base(menuElement) {
        }

        public override void ConfiguracionForm_Load(object sender, EventArgs e) {
            this.lblTitulo.Text = string.Format(this.lblTitulo.Text, ConfigService.Synapsis.Empresa.RFC);
            this.dataConfiguracion.SelectedObject = ConfigService.Synapsis;
        }
    }
}
