﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Nomina.Forms {
    public partial class ComprobanteNominaForm : RadForm {
        protected internal IControlService _Service;
        protected internal List<ComprobanteNominaSingleModel> _DataSource;

        public ComprobanteNominaForm() {
            InitializeComponent();
        }

        private void ComprobanteNominaForm_Load(object sender, EventArgs e) {
            this.TNomina.ShowActualizar = false;
            this.TNomina.Cerrar.Click += this.TNomina_Cerrar_Click;

            this.Control.Buscar.Click += TNomina_Actualizar_Click;
            this.Control.Nominas.EnabledChanged += Nominas_EnabledChanged;
            this.Control.Departamentos.EnabledChanged += Departamentos_EnabledChanged;
            this.Control.Empleados.EnabledChanged += Empleados_EnabledChanged;
            this.Control.FechaInicial.EnabledChanged += FechaInicial_EnabledChanged;
        }

        private void FechaInicial_EnabledChanged(object sender, EventArgs e) {
            if (this.Control.FechaInicial.Enabled == false) { return; }
            this.Control.FechaInicial.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            this.Control.FechaFinal.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        private void Empleados_EnabledChanged(object sender, EventArgs e) {
            if (this.Control.Empleados.Enabled == false) { return; }
            using (var espera = new Waiting1Form(this.GetEmpleados)) {
                espera.ShowDialog(this);
            }
        }

        private void Departamentos_EnabledChanged(object sender, EventArgs e) {
            if (this.Control.Departamentos.Enabled == false) { return; }
            using (var espera = new Waiting1Form(this.GetDepartamentos)) {
                espera.ShowDialog(this);
            }
        }

        private void Nominas_EnabledChanged(object sender, EventArgs e) {
            if (this.Control.Nominas.Enabled == false) { return; }
            using (var espera = new Waiting1Form(this.GetNominas)) {
                espera.ShowDialog(this);
            }
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._Service = new Aplication.Nomina.Services.ControlService();
        }

        private void TNomina_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TNomina.GridData.DataSource = _DataSource;
        }

        private void TNomina_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            var query = Aplication.Nomina.Services.ControlService.Query();
            if (this.Control.IdDocumento.Enabled) {
                query.IdComprobante(this.Control.IdDocumento.Text);
            } else if (this.Control.Nominas.Enabled) {
                query.IdControl(this.Control.GetIdNomina());
            } else if (this.Control.FechaInicial.Enabled) {
                query.FechaInicial(this.Control.FechaInicial.Value).FechaFinal(this.Control.FechaFinal.Value);
            }

            if (this.Control.Departamentos.Enabled) {
                query.IdDepartamento(this.Control.Departamentos.Text);
            } else if (this.Control.Empleados.Enabled) {
                query.IdEmpleado(this.Control.Empleados.Text);
            }

            this._DataSource = this._Service.GetList<ComprobanteNominaSingleModel>(query.Build()).ToList();
        }

        private void GetNominas() {
            this.Control.Nominas.DataSource = this._Service.GetNominas();
        }

        private void GetDepartamentos() {
            this.Control.Departamentos.DataSource = this._Service.GetList<DepartamentoSingleModel>(new List<Domain.Base.Builder.IConditional>());
        }

        private void GetEmpleados() {
            this.Control.Empleados.DataSource = this._Service.GetList<EmpleadoModel>(new List<Domain.Base.Builder.IConditional>());
        }
    }
}
