﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.Repositories {
    /// <summary>
    /// repositorio de catalogo de productos
    /// </summary>
    public class SqlSugarProdServRepository : MySqlSugarContext<ProductoServicioModel>, ISqlProdServRepository {
        #region declaraciones
        protected ISqlModelosRepository modelosRepository;
        protected ISqlMediosRepository mediosRepository;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public SqlSugarProdServRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            modelosRepository = new SqlSugarModelosRepository(configuracion, user);
            mediosRepository = new SqlSugarMediosRepository(configuracion, user);
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ProductoServicioModeloModel)) {
                var sql = "SELECT * FROM CTMDL LEFT JOIN CTPRD ON CTMDL.CTMDL_CTPRD_ID = CTPRD.CTPRD_ID @wcondiciones";
                return this.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(ProductoModeloExistenciaModel)) {
                var sql = @"SELECT CTPRD.*, CTMDL.*, CTMDLX.*, (CTPRD.CTPRD_A*CTMDL.CTMDL_A) AS ACTIVO FROM CTPRD LEFT JOIN CTMDL ON CTMDL.CTMDL_CTPRD_ID = CTPRD.CTPRD_ID LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID = CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID = CTPRD.CTPRD_ID @wcondiciones";
                return this.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(ProductoModeloExistencia)) {
                var sql = @"";
                return this.GetMapper<T1>(sql, conditionals);
            }
            return base.GetList<T1>(conditionals);
        }

        public ProductoServicioDetailModel Save(ProductoServicioDetailModel model) {
            if (model.IdProducto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdProducto = Insert(model);
            } else {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                Update(model);
            }
            // modelos
            for (int i = 0; i < model.Modelos.Count; i++) {
                model.Modelos[i].IdProducto = model.IdProducto;
                model.Modelos[i].IdCategoria = model.IdCategoria;
                model.Modelos[i] = modelosRepository.Save(model.Modelos[i]);
            }

            return model;
        }

        public ProductoServicioModel Save(ProductoServicioModel model) {
            if (model.IdProducto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdProducto = Insert(model);
            } else {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                Update(model);
            }
            return model;
        }
    }
}
