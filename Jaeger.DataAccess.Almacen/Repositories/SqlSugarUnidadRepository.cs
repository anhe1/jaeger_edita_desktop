﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.Repositories {
    public class SqlSugarUnidadRepository : MySqlSugarContext<UnidadModel>, ISqlUnidadRepository {
        public SqlSugarUnidadRepository(DataBaseConfiguracion objeto) : base(objeto) {
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            return GetMapper<T1>(conditionals);
        }

        public UnidadModel Save(UnidadModel model) {
            if (model.IdUnidad == 0) {
                model.IdUnidad = Insert(model);
            } else {
                Update(model);
            }
            return model;
        }

        public bool Create() {
            try {
                Db.CodeFirst.InitTables<UnidadModel>();
                return true;
            } catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
