﻿using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Almacen.Repositories {
    public class SqlSugarAlmacenRepository : MySqlSugarContext<AlmacenModel>, ISqlAlmacenRepository {
        public SqlSugarAlmacenRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sql = "SELECT CTALM.* FROM CTALM @condiciones";
            return this.GetMapper<T1>(sql, conditionals);
        }

        public IEnumerable<IAlmacenModel> GetList(List<IConditional> conditionals) {
            return new List<IAlmacenModel> {
                new AlmacenModel(0, "AlmacenGR", "Almacén General", Domain.Base.ValueObjects.AlmacenEnum.MP),
                //new AlmacenModel(1, "AlmacenMP", "Almacén Materia Prima", Domain.Base.ValueObjects.AlmacenEnum.MP),
                //new AlmacenModel(2, "AlmacenPT", "Almacén Producto Terminado", Domain.Base.ValueObjects.AlmacenEnum.PT),
            };
        }

        public AlmacenModel Save(AlmacenModel model) {
            model.Creo = this.User;
            model.FechaNuevo = System.DateTime.Now;
            if (model.IdAlmacen == 0) {
                model.IdAlmacen = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
