﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.Repositories {
    /// <summary>
    /// catalogo de modelos, utilizado para productos del almacen de materia prima
    /// </summary>
    public class SqlSugarModelosRepository : MySqlSugarContext<ModeloModel>, ISqlModelosRepository {
        protected ISqlMediosRepository mediosRepository;
        protected ISqlProdServProveedorRepository prodServProveedorRepository;
        protected ISqlModeloExistenciaRepository modeloExistenciaRepository;
        public SqlSugarModelosRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            mediosRepository = new SqlSugarMediosRepository(configuracion, user);
            prodServProveedorRepository = new SqlSugarProdServProveedorRepository(configuracion, user);
            this.modeloExistenciaRepository = new SqlSugarModeloExistenciaRepository(configuracion);
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ModeloDetailModel)) {
                var sql = @"SELECT * FROM CTMDL LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID=CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID=CTMDL.CTMDL_CTPRD_ID @wcondiciones";
                return GetMapper<T1>(sql, conditionals).ToList();
            } else if (typeof(T1) == typeof(ProductoServicioModeloModel)) {
                var sql = @"SELECT * FROM CTPRD P LEFT JOIN CTMDL M ON (P.CTPRD_ID = M.CTMDL_CTPRD_ID) LEFT JOIN CTMDLX X ON  (M.CTMDL_ID = X.CTMDLX_CTMDL_ID) @wcondiciones";
                var d0 = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (d0 != null) {
                    sql = sql.Replace("@wcondiciones", "WHERE (P.CTPRD_NOM LIKE LOWER(CONCAT('%',@search,'%'))) OR LOWER((M.CTMDL_DSCRC LIKE CONCAT('%',@search,'%'))) @condiciones");
                    sql = sql.Replace("@search", "'" + d0.FieldValue + "'");
                    conditionals = conditionals.Where(it => !it.FieldName.ToLower().Contains("@search")).ToList();
                }
                return GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        public IEnumerable<ModeloDetailModel> GetList(AlmacenEnum tipo, int index, bool onlyActive = true) {
            var result = Db.Queryable<ModeloDetailModel>().Where(it => it.IdAlmacen == (int)tipo)
                .WhereIF(index > 0, it => it.IdModelo == index)
                .WhereIF(onlyActive == true, it => it.Activo == true).Mapper((modelo, cache) => {
                    var allMedios = cache.Get(modelosList => {
                        var allIds2 = modelosList.Select(it2 => it2.IdModelo).ToList();
                        return Db.Queryable<MedioModel>().Where(it => allIds2.Contains(it.IdModelo)).ToList();
                    });
                    modelo.Medios = new BindingList<MedioModel>(allMedios.Where(it => it.IdModelo == modelo.IdModelo).ToList());
                }).ToList();

            return result;
        }

        public bool Autoriza(ModeloModel model) {
            var sqlCommand = @"UPDATE CTMDL SET CTMDL_ATRZD=@CTMDL_ATRZD, CTMDL_FM=@CTMDL_FM, CTMDL_USR_M=@CTMDL_USR_M WHERE CTMDL_ID=@CTMDL_ID;";
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@CTMDL_ID", model.IdModelo),
                new SugarParameter("@CTMDL_ATRZD", model.Autorizado),
                new SugarParameter("@CTMDL_USR_M", model.Modifica),
                new SugarParameter("@CTMDL_FM", model.FechaModifica)
            };
            return ExecuteTransaction(sqlCommand, parameters)>0;
        }

        /// <summary>
        /// almacenar modelo
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ModeloModel Save(ModeloModel item) {
            if (item.IdModelo == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                item.IdModelo = Insert(item);
            } else {
                item.Modifica = this.User;
                item.FechaModifica=DateTime.Now;
                Update(item); }
            return item;
        }

        public ModeloDetailModel Save(ModeloDetailModel model) {
            if (model.IdModelo == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                var insertable = this.Db.Insertable(model);
                model.IdModelo = this.Execute(insertable);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                var updateable = this.Db.Updateable(model);
                this.Execute(updateable);
            }
            model.Existencias.IdTipoAlmacen = model.IdAlmacen;
            model.Existencias.IdProducto = model.IdProducto;
            model.Existencias.IdModelo = model.IdModelo;
            model.Existencias.IdEspecificacion = -1;
            this.modeloExistenciaRepository.Save(model.Existencias);
            return model;
        }
    }
}
