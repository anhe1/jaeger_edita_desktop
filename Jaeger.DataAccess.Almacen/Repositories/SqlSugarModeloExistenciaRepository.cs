﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Almacen.Repositories {
    /// <summary>
    /// repositorio de exitencias de modelos
    /// </summary>
    public class SqlSugarModeloExistenciaRepository : MySqlSugarContext<ModeloExistenciaModel>, ISqlModeloExistenciaRepository {
        public SqlSugarModeloExistenciaRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public IModeloExistenciaModel Save(ModeloExistenciaModel model) {
            var sqlCommand = @"INSERT INTO CTMDLX (CTMDLX_CTPRD_ID, CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID, CTMDLX_CTALM_ID, CTMDLX_TPALM_ID, CTMDLX_EXT, CTMDLX_MIN, CTMDLX_MAX, CTMDLX_REORD) 
                                          VALUES (@CTMDLX_CTPRD_ID,@CTMDLX_CTMDL_ID,@CTMDLX_CTESPC_ID,@CTMDLX_CTALM_ID,@CTMDLX_TPALM_ID,@CTMDLX_EXT,@CTMDLX_MIN,@CTMDLX_MAX,@CTMDLX_REORD)
ON DUPLICATE KEY UPDATE CTMDLX_CTPRD_ID=@CTMDLX_CTPRD_ID, CTMDLX_CTMDL_ID=@CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID=@CTMDLX_CTESPC_ID, CTMDLX_CTALM_ID=@CTMDLX_CTALM_ID, CTMDLX_TPALM_ID=@CTMDLX_TPALM_ID, CTMDLX_EXT=@CTMDLX_EXT, CTMDLX_MIN=@CTMDLX_MIN, CTMDLX_MAX=@CTMDLX_MAX, CTMDLX_REORD=@CTMDLX_REORD";
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@CTMDLX_CTPRD_ID", model.IdProducto),
                new SugarParameter("@CTMDLX_CTMDL_ID", model.IdModelo),
                new SugarParameter("@CTMDLX_CTESPC_ID", model.IdEspecificacion),
                new SugarParameter("@CTMDLX_CTALM_ID", model.IdAlmacen),
                new SugarParameter("@CTMDLX_TPALM_ID", model.IdTipoAlmacen),
                new SugarParameter("@CTMDLX_EXT", model.Existencia),
                new SugarParameter("@CTMDLX_MIN", model.Minimo),
                new SugarParameter("@CTMDLX_MAX", model.Maximo),
                new SugarParameter("@CTMDLX_REORD", model.ReOrden),
            };
            this.ExecuteTransaction(sqlCommand, parameters);
            return model;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ModeloDetailModel)) {
                var sql = @"SELECT * FROM CTMDL LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID=CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID=CTMDL.CTMDL_CTPRD_ID @wcondiciones";
                return GetMapper<T1>(sql, conditionals).ToList();
            } else if (typeof(T1) == typeof(ProductoServicioModeloModel)) {
                var sql = @"SELECT * FROM CTPRD P INNER JOIN CTMDL M ON (P.CTPRD_ID = M.CTMDL_CTPRD_ID) @wcondiciones";
                var d0 = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (d0 != null) {
                    sql = sql.Replace("@wcondiciones", "where (p.ctprd_nom like lower(concat('%',@search,'%'))) or lower((m.ctmdl_dscrc like concat('%',@search,'%'))) @condiciones");
                    sql = sql.Replace("@search", "'" + d0.FieldValue + "'");
                    conditionals = conditionals.Where(it => !it.FieldName.ToLower().Contains("@search")).ToList();
                }
                return GetMapper<T1>(sql, conditionals).ToList();
            } 
            //else if (typeof(T1) == typeof(ProductoExistenciaModel)) {
            //    var sql1 = @"SELECT CTMDL.CTMDL_ID, CTMDL.CTMDL_CTCLS_ID, CTMDL.CTMDL_CTPRD_ID, CTMDL.CTMDL_TIPO, CTPRD.CTPRD_NOM, CTMDL.CTMDL_MRC, CTMDL.CTMDL_ESPC, CTMDL.CTMDL_DSCRC, SUM(MVAMP.MVAMP_CNTE) AS ENTRADA, SUM(MVAMP.MVAMP_CNTS) AS SALIDA, CTMDL.CTMDL_UNDD
            //                FROM CTMDL 
            //                LEFT JOIN CTPRD ON CTMDL.CTMDL_CTPRD_ID = CTPRD.CTPRD_ID
            //                LEFT JOIN MVAMP ON MVAMP.MVAMP_CTMDL_ID = CTMDL.CTMDL_ID
            //                LEFT JOIN ALMMP ON ALMMP.ALMMP_ID = MVAMP.MVAMP_ALMMP_ID
            //                GROUP BY CTMDL.CTMDL_ID
            //                ORDER BY CTPRD.CTPRD_NOM, CTMDL.CTMDL_DSCRC";
            //                                return GetMapper<T1>(sql1, conditionals).ToList();
            //}
            return base.GetList<T1>(conditionals);
        }

        public bool Existencia(List<IConditional> conditionals) {
            throw new NotImplementedException();
        }
    }
}
