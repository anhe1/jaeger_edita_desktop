﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.DataAccess.Almacen.Repositories {
    public class SqlSugarEspecificacionRepository : MySqlSugarContext<EspecificacionModel>, ISqlEspecificacionRepository {
        public SqlSugarEspecificacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT CTESPC.* FROM CTESPC @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public EspecificacionModel Save(EspecificacionModel model) {
            if (model.IdEspecificacion == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdEspecificacion = this.Insert(model);
                model.SetModified = false;
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                if (this.Update(model) > 0)
                    model.SetModified = false;
            }
            return model;
        }
    }
}
