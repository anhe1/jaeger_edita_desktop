﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.DataAccess.Almacen.Repositories {
    public class SqlSugarModeloEspecificacionRepository : MySqlSugarContext<ModeloYEspecificacionModel>, ISqlModeloEspecificacionRepository {
        public SqlSugarModeloEspecificacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT * FROM CTMDLE @condiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public ModeloYEspecificacionModel Save(ModeloYEspecificacionModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.Id = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }

            return model;
        }
    }
}
