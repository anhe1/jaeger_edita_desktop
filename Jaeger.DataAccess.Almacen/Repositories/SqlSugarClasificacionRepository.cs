﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Almacen.Repositories {
    /// <summary>
    /// catalogo de clasificacion de categorias de productos, bienes y servicios (BPS) 
    /// </summary>
    public class SqlSugarClasificacionRepository : MySqlSugarContext<ClasificacionModel>, ISqlClasificacionRepository {
        public SqlSugarClasificacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public int Salveable(ClasificacionModel model) {
            var sqlCommand = "UPDATE OR INSERT INTO CTCAT (CTCAT_ID, CTCAT_CTCAT_ID, CTCAT_SEC, CTCAT_ALM_ID, CTCAT_NOM) VALUES (@CTCAT_ID, @CTCAT_CTCAT_ID, @CTCAT_SEC, @CTCAT_ALM_ID, @CTCAT_NOM) MATCHING(CTCAT_ID, CTCAT_CTCAT_ID);";
            var Parameters = new List<SugarParameter> {
                new SugarParameter("@CTCAT_ID", model.IdCategoria),
                new SugarParameter("@CTCAT_CTCAT_ID", model.IdSubCategoria),
                new SugarParameter("@CTCAT_SEC", model.Secuencia),
                new SugarParameter("@CTCAT_ALM_ID", model.IdAlmacen),
                new SugarParameter("@CTCAT_NOM", model.Descripcion)
            };

            return this.ExecuteTransaction(sqlCommand, Parameters);
        }

        /// <summary>
        /// Lista generica condicional
        /// </summary>
        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            // modelo de clasificacion
            if (typeof(T1) == typeof(ClasificacionModel)) {
                var sqlCommand0 = "SELECT CTCAT.* FROM CTCAT @condiciones";
                return GetMapper<T1>(sqlCommand0, conditionals);
            } else if (typeof(T1) == typeof(ClasificacionSingle)) {
                // clasificacion simple, vista
                var sqlCommand =
                            @"WITH RECURSIVE CATEGORIA (IDCATEGORIA, SUBID, ACTIVO, SECUENCIA, CLASE, DESCRIPCION, NIVEL, CHILDS) AS
                                (
                                  SELECT CTCAT_ID AS IDCATEGORIA, CTCAT_CTCAT_ID AS SUBID, CTCAT_A AS ACTIVO, CTCAT_SEC AS SECUENCIA, CTCAT_NOM AS CLASE, CONCAT(RIGHT(CTCAT_NOM,129), '/') AS DESCRIPCION, 1, 
  	                                (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = CTCAT_ID) AS CHILDS
                                    FROM CTCAT
                                    WHERE CTCAT_ID = 1  -- THE TREE NODE
                                  UNION ALL
                                  SELECT T.CTCAT_ID, T.CTCAT_CTCAT_ID, (T.CTCAT_A * TP.ACTIVO), T.CTCAT_SEC, T.CTCAT_NOM, LEFT(CONCAT(TP.DESCRIPCION, T.CTCAT_NOM, ' |'),129), TP.NIVEL +1,
	                                (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = T.CTCAT_ID) AS CHILDS
                                    FROM CATEGORIA AS TP 
	                                 JOIN CTCAT AS T ON TP.IDCATEGORIA = T.CTCAT_CTCAT_ID 
                                )
                                SELECT  * FROM CATEGORIA 
                                ORDER BY NIVEL, CHILDS, SECUENCIA";
                return this.GetMapper<T1>(sqlCommand, conditionals);
            } else if (typeof(T1) == typeof(ClasificacionProductoModel)) {
                // consulta para la vista de clasificacion y producto
                var sqlCommand1 =
                            @"WITH RECURSIVE Clasificacion (IDCATEGORIA, SUBID,ACTIVO, CLASE, DESCRIPCION, NIVEL, CHILDS) AS
                                (
                                  SELECT CTCAT_ID AS IDCATEGORIA, CTCAT_CTCAT_ID AS SUBID, CTCAT_A AS ACTIVO, CTCAT_NOM AS CLASE, CONCAT(CTCAT_NOM, '/') AS DESCRIPCION, 1,
                                  (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = CTCAT_ID) AS CHILDS
                                    FROM CTCAT
                                    WHERE CTCAT_ID = 1 -- THE TREE NODE
                                  UNION ALL
                                  SELECT T.CTCAT_ID AS IDCATEGORIA, T.CTCAT_CTCAT_ID AS SUBID, (T.CTCAT_A * TP.ACTIVO) AS ACTIVO, T.CTCAT_NOM AS  CLASE, LEFT(CONCAT(TP.DESCRIPCION, T.CTCAT_NOM, ' | '),128) AS DESCRIPCION, TP.NIVEL + 1,
                                  (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = T.CTCAT_ID) AS CHILDS
                                    FROM Clasificacion AS TP 
                                     JOIN CTCAT AS T ON TP.IDCATEGORIA = T.CTCAT_CTCAT_ID
                                )
                                SELECT * FROM Clasificacion
                                RIGHT JOIN CTPRD ON Clasificacion.IdCategoria = CTPRD.CTPRD_CTCLS_ID 
                                WHERE CTPRD.CTPRD_A = 1
                                ORDER BY NIVEL, CHILDS;";

                return GetMapper<T1>(sqlCommand1, conditionals);
            } 
            throw new NotImplementedException();
        }
    }
}
