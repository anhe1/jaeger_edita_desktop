﻿using System.Collections.Generic;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.Repositories {
    /// <summary>
    /// repositorio de medios (imagenes) en los productos
    /// </summary>
    public class SqlSugarMediosRepository : MySqlSugarContext<MedioModel>, ISqlMediosRepository {
        public SqlSugarMediosRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            return this.GetMapper<T1>(conditionals);
        }

        /// <summary>
        /// almacenar una imagen
        /// </summary>
        public MedioModel Save(MedioModel medio) {
            if (medio.Id == 0) {
                medio.Id = Insert(medio);
            } else {
                Update(medio);
            }
            return medio;
        }
    }
}
