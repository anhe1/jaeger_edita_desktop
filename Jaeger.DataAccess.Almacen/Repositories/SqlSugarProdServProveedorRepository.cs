﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.DataAccess.Almacen.Repositories {
    /// <summary>
    /// clase para control de proveedores de productos o servicios
    /// </summary>
    public class SqlSugarProdServProveedorRepository : MySqlSugarContext<ModeloProveedorModel>, ISqlProdServProveedorRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlSugarProdServProveedorRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(ModeloProveedorModel proveedor) {
            throw new NotImplementedException();
        }

        public IEnumerable<ReceptorModel> GetList(TipoRelacionComericalEnum tipo) {
            return Db.Queryable<ReceptorModel>().Where(it => it.Relacion.Contains(Enum.GetName(typeof(TipoRelacionComericalEnum), tipo))).Where(it => it.Activo == true).Select((d) => new ReceptorModel()).ToList();
        }

        public IEnumerable<ModeloProveedorModel> GetListBy(int index, bool activo = true) {
            return Db.Queryable<ModeloProveedorModel>().Where(it => it.IdModelo == index).WhereIF(activo == true, it => it.Activo == true).Select((u) => new ModeloProveedorModel {
                Id = u.Id,
                Activo = u.Activo,
                Creo = u.Creo,
                FechaModifica = u.FechaModifica,
                FechaNuevo = u.FechaNuevo,
                IdModelo = u.IdModelo,
                IdProveedor = u.IdProveedor,
                Modifico = u.Modifico,
                Moneda = u.Moneda,

                Nota = u.Nota,
                Unidad = u.Unidad,
                Unitario = u.Unitario
            }).ToList();
        }

        public ModeloProveedorModel Save(ModeloProveedorModel proveedor) {
            if (proveedor.Id == 0) {
                proveedor.FechaNuevo = DateTime.Now;
                proveedor.Id = Insert(proveedor);
            } else {
                proveedor.FechaModifica = DateTime.Now;
                Update(proveedor);
            }
            return proveedor;
        }

        public IEnumerable<ModeloProveedorModel> Save(List<ModeloProveedorModel> proveedor) {
            for (int i = 0; i < proveedor.Count; i++) {
                proveedor[i] = Save(proveedor[i]);
            }
            return proveedor;
        }
    }
}
