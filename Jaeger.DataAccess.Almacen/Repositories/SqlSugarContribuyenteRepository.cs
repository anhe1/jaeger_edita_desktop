﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.DataAccess.Almacen.Repositories {
    public class SqlSugarContribuyenteRepository : MySqlSugarContext<ReceptorModel>, ISqlContribuyenteRepository {
        public SqlSugarContribuyenteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// obtener listado de contribuyentes del directorio
        /// </summary>
        /// <param name="onlyActive">verdadero solo regristros activos</param>
        /// <param name="tipoRelacion">Tipo de relaciona con el directorio</param>
        public IEnumerable<ReceptorDetailModel> GetList(bool onlyActive, TipoRelacionComericalEnum tipoRelacion = TipoRelacionComericalEnum.None) {
            var result = Db.Queryable<ReceptorDetailModel>()
                .WhereIF(tipoRelacion != TipoRelacionComericalEnum.None, it => it.Relacion.Contains(Enum.GetName(typeof(TipoRelacionComericalEnum), tipoRelacion)))
                .Mapper((itemModel, cache) => {

                    var domicilios = cache.Get(directorio => {
                        var allIds = directorio.Select(it => it.Id).ToList();
                        return Db.Queryable<ReceptorDomicilioModel>().Where(it => allIds.Contains(it.SubId)).ToList();
                    });
                    itemModel.Domicilios = new BindingList<ReceptorDomicilioModel>(domicilios.Where(it => it.SubId == itemModel.Id).ToList());

                }).ToList();

            return result;
        }

        public IEnumerable<ReceptorModel> GetList(TipoRelacionComericalEnum tipoRelacion, bool onlyActive) {
            return Db.Queryable<ReceptorModel>().Where(it => it.Relacion.Contains(Enum.GetName(typeof(TipoRelacionComericalEnum), tipoRelacion))).ToList();
        }
    }
}
