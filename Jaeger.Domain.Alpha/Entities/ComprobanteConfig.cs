﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Empresa.Entities {
    public class ComprobanteConfig {
        [JsonProperty("general")]
        public EmpresaGeneral General {
            get; set;
        }

        [JsonProperty("domicilioFiscal")]
        public DomicilioFiscal DomicilioFiscal {
            get; set;
        }

        public string Json() {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, conf);
        }

        public static ComprobanteConfig Json(string inputString) {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<ComprobanteConfig>(inputString, conf);
        }
    }
}
