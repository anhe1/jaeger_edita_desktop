﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Domain.Empresa.Entities {
    public class SettingsBasicPac : BasePropertyChangeImplementation {
        private IServiceProvider settingsField;

        private string cerBase64Field;

        private string keyBase64Field;

        private string passKeyField;

        public string CerBase64 {
            get {
                return this.cerBase64Field;
            }
            set {
                this.cerBase64Field = value;
            }
        }

        public string KeyBase64 {
            get {
                return this.keyBase64Field;
            }
            set {
                this.keyBase64Field = value;
            }
        }

        public string PassKey {
            get {
                return this.passKeyField;
            }
            set {
                this.passKeyField = value;
            }
        }

        public IServiceProvider Settings {
            get {
                return this.settingsField;
            }
            set {
                this.settingsField = value;
            }
        }

        public SettingsBasicPac() {
            this.settingsField = new ServiceProvider();
            this.cerBase64Field = string.Empty;
            this.keyBase64Field = string.Empty;
            this.passKeyField = string.Empty;
        }
    }
}