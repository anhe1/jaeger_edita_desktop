﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    [JsonObject("proxy")]
    public class ConfigurationProxy : BasePropertyChangeImplementation {
        private bool enabled;
        private string password;
        private string port;
        private string server;
        private string user;

        [JsonProperty("enabled")]
        public bool Enabled {
            get {
                return this.enabled;
            }
            set {
                this.enabled = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("server")]
        public string Server {
            get {
                return this.server;
            }
            set {
                this.server = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("port")]
        public string Port {
            get {
                return this.port;
            }
            set {
                this.port = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("user")]
        public string User {
            get {
                return this.user;
            }
            set {
                this.user = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("password")]
        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        public static ConfigurationProxy GetProxyConfiguration() {
            throw new NotImplementedException();
        }
    }
}
