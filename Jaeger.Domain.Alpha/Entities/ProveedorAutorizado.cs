﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase que contiene la configuracion del proveedor autorizado de certificacion
    /// </summary>
    [JsonObject("pac")]
    public class ProveedorAutorizado : BasePropertyChangeImplementation {
        private IServiceProvider certificacion;
        private IServiceProvider cancelacion;
        private IServiceProvider validacion;

        /// <summary>
        /// constructor de la clase
        /// </summary>
        public ProveedorAutorizado() {
            this.Certificacion = new ServiceProvider();
            this.Cancelacion = new ServiceProvider();
            this.Validacion = new ServiceProvider();
        }

        /// <summary>
        /// obtener o establecer el proveedor autorizado de certificacion
        /// </summary>
        [DisplayName("Certificación"), Description("Proveedor autorizado de certificación de comprobantes fiscales por internet. (CFDI)")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("certification")]
        public IServiceProvider Certificacion {
            get { return this.certificacion; }
            set {
                if (this.certificacion != value) {
                    this.certificacion = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el proveedor autorizado de cancelacion
        /// </summary>
        [DisplayName("Cancelación"), Description("Proveedor autorizado de cancelacion de comprobantes fiscales por internet. (CFDI)")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("cancelation")]
        public IServiceProvider Cancelacion {
            get { return this.cancelacion; }
            set {
                if (this.cancelacion != value) {
                    this.cancelacion = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el proveedor autorizado de validacion
        /// </summary>
        [DisplayName("Validación"), Description("Proveedor autorizado de validación de comprobantes fiscales por internet. (CFDI)")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("validation")]
        public IServiceProvider Validacion {
            get { return this.validacion; }
            set {
                if (this.validacion != value) {
                    this.validacion = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}
