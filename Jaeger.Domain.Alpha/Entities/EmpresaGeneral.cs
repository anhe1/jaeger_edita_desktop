﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase que contiene la configuracion de la empresa
    /// </summary>
    [JsonObject("general")]
    public class EmpresaGeneral {
        public EmpresaGeneral() {
            this.RegimenFiscal = "";
            this.DomicilioFiscal = new DomicilioFiscal();
        }
        /// <summary>
        /// obtener o establecer la clave o subdominio de la empresa
        /// </summary>
        [DisplayName("Clave")]
        public string Clave {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre de la razon social
        /// </summary>
        [DisplayName("Nombre ó Razon Social")]
        [JsonProperty("razonSocial")]
        public string RazonSocial {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre de la razon social
        /// </summary>
        [DisplayName("Nombre Comercial")]
        [JsonProperty("nombreComercial")]
        public string NombreComercial {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Causantes (RFC)
        /// </summary>
        [DisplayName("Registro Federal de Causantes (RFC)")]
        [JsonProperty("rfc")]
        public string RFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer regimen fiscal
        /// </summary>
        [DisplayName("Régimen Fiscal")]
        [JsonProperty("regimenFiscal")]
        public string RegimenFiscal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer registro patronal
        /// </summary>
        [DisplayName("Registro Patronal")]
        [JsonProperty("registroPatronal")]
        public string RegistroPatronal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el domicilio fiscal
        /// </summary>
        [DisplayName("Domicilio Fiscal")]
        [JsonProperty("domicilioFiscal")]
        [TypeConverter(typeof(PropertiesConvert))]
        public DomicilioFiscal DomicilioFiscal {
            get; set;
        }

        /// <summary>
        /// obtener string en formato json de la configuracion de la empresa 
        /// </summary>
        /// <returns></returns>
        public string Json() {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, conf);
        }

        /// <summary>
        /// obtener objeto de configuracion de la empresa a partir de un string json
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static EmpresaGeneral Json(string inputString) {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<EmpresaGeneral>(inputString, conf);
        }
    }
}
