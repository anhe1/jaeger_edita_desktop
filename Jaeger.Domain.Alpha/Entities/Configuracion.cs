﻿using System.ComponentModel;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase que contiene la configuracion de la empresa
    /// </summary>
    public class Configuracion {
        /// <summary>
        /// constructor de la clase
        /// </summary>
        public Configuracion() {
            this.Empresa = new EmpresaGeneral();
            this.RDS = new BaseDatos();
            this.Amazon = new Amazon();
            this.ProveedorAutorizado = new ProveedorAutorizado();
        }

        /// <summary>
        /// obtener o establecer la configuracion de la empresa
        /// </summary>
        [TypeConverter(typeof(PropertiesConvert))]
        public EmpresaGeneral Empresa { get; set; }

        /// <summary>
        /// obtener o establecer informacion de base de datos
        /// </summary>
        [TypeConverter(typeof(PropertiesConvert))]
        public BaseDatos RDS { get; set; }

        /// <summary>
        /// obtener o establecer informacion de los servicios de amazon
        /// </summary>
        [TypeConverter(typeof(PropertiesConvert))]
        public Amazon Amazon { get; set; }

        /// <summary>
        /// obtener o establecer informacion del proveedor autorizado de certificacion (PAC)
        /// </summary>
        [TypeConverter(typeof(PropertiesConvert))]
        public ProveedorAutorizado ProveedorAutorizado { get; set; }
    }
}
