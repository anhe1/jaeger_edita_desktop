﻿using System;
using Newtonsoft.Json;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase que contiene domicilio fiscal de la empresa
    /// </summary>
    [JsonObject("domicilioFiscal")]
    public class DomicilioFiscal {
        /// <summary>
        /// obtener o establecer la calle
        /// </summary>
        [JsonProperty("_drccn_cll")]
        public string Calle { get; set; }

        /// <summary>
        /// obtener o establecer la ciudad
        /// </summary>
        [JsonProperty("_drccn_cdd")]
        public string Ciudad { get; set; }

        /// <summary>
        /// obtener o establecer el codigo de pais
        /// </summary>
        [JsonProperty("_drccn_cpa")]
        public string CodigoDePais { get; set; }

        /// <summary>
        /// obtener o establecer el codigo postal
        /// </summary>
        [JsonProperty("_drccn_cp")]
        public string CodigoPostal { get; set; }

        /// <summary>
        /// obtener o establecer la colonia
        /// </summary>
        [JsonProperty("_drccn_cln")]
        public string Colonia { get; set; }

        /// <summary>
        /// obtener o establecer la delegacion
        /// </summary>
        [JsonProperty("_drccn_dlg")]
        public string Delegacion { get; set; }

        /// <summary>
        /// obtener o establecer el estado
        /// </summary>
        [JsonProperty("_drccn_std")]
        public string Estado { get; set; }

        /// <summary>
        /// obtener o establecer la localidad
        /// </summary>
        [JsonProperty("_drccn_lcldd")]
        public string Localidad { get; set; }

        /// <summary>
        /// obtener o establecer numero exterior
        /// </summary>
        [JsonProperty("_drccn_extr")]
        public string NoExterior { get; set; }

        /// <summary>
        /// obtener o establecer el numero interior
        /// </summary>
        [JsonProperty("_drccn_intr")]
        public string NoInterior { get; set; }

        /// <summary>
        /// obtener o establecer el pais
        /// </summary>
        [JsonProperty("_drccn_ps")]
        public string Pais { get; set; }

        /// <summary>
        /// obtener o establecer la referencia
        /// </summary>
        [JsonProperty("_drccn_rfrnc")]
        public string Referencia { get; set; }

        /// <summary>
        /// obtener string del domicilio fiscal 
        /// </summary>
        public override string ToString() {
            string direccion = string.Empty;

            if (!(String.IsNullOrEmpty(this.Calle))) {
                direccion = string.Concat("Calle ", this.Calle);
            }

            if (!(String.IsNullOrEmpty(this.NoExterior))) {
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);
            }

            if (!(String.IsNullOrEmpty(this.NoInterior))) {
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);
            }

            if (!(String.IsNullOrEmpty(this.Colonia))) {
                direccion = string.Concat(direccion, " Col. ", this.Colonia);
            }

            if (!(String.IsNullOrEmpty(this.CodigoPostal))) {
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);
            }

            if (!(String.IsNullOrEmpty(this.Delegacion))) {
                direccion = string.Concat(direccion, " ", this.Delegacion);
            }

            if (!(String.IsNullOrEmpty(this.Estado))) {
                direccion = string.Concat(direccion, ", ", this.Estado);
            }

            if (!(String.IsNullOrEmpty(this.Referencia))) {
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);
            }

            if (!(String.IsNullOrEmpty(this.Localidad))) {
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);
            }
            return direccion;
        }
    }
}
