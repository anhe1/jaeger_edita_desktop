﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase para serialización de la configuracion de la tabla empresas, campo empr_conf
    /// </summary>
    public class SynapsisModel : BasePropertyChangeImplementation {
        #region declaraciones
        private BaseDatos _BaseDatos;
        private Amazon _Amazon;
        private ProveedorAutorizado _PAC;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public SynapsisModel() {
            this._BaseDatos = new BaseDatos();
            this._Amazon = new Amazon();
            this._PAC = new ProveedorAutorizado();
        }

        /// <summary>
        /// obtener o establecer la configuracion para el grupo de base de datos
        /// </summary>
        [DisplayName("Bases de datos (RDS)"), Description("Configuraciones de las bases de datos relacionadas.")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("dbs")]
        public BaseDatos BaseDatos {
            get {
                return this._BaseDatos;
            }
            set {
                this._BaseDatos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la configuracion de los servicios de amazon
        /// </summary>
        [DisplayName("Servicios Amazon"), Description("Configuraciones de ser servicios de Amazon, Simple Email Service, etc.")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("amazon")]
        public Amazon Amazon {
            get {
                return this._Amazon;
            }
            set {
                this._Amazon = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la configuracion del proveedor de certificacion
        /// </summary>
        [DisplayName("Proveedor Autorizado de Certificación (PAC)"), Description("Configuraciones de los proveedores de Certificación, Cancelación y Validación.")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("pac")]
        public ProveedorAutorizado PAC {
            get {
                return this._PAC;
            }
            set {
                this._PAC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener objeto synapsis de una cadena de texto
        /// </summary>
        public static SynapsisModel Json(string inputString) {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<SynapsisModel>(inputString, conf);
        }

        /// <summary>
        /// convertir la instancia actual en formato json
        /// </summary>
        /// <returns></returns>
        public string Json() {
            return JsonConvert.SerializeObject(this);
        }
    }
}
