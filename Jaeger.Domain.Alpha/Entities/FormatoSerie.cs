﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    [TypeConverter(typeof(PropertiesConvert))]
    public class FormatoSerie {
        public List<FormatoSerieObjeto> Serie { get; set; }
        public List<FormatoSerieObjeto> Folio { get; set; }
    }

    [TypeConverter(typeof(PropertiesConvert))]
    public partial class FormatoSerieObjeto {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
