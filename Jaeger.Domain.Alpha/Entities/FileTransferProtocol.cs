﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    public class FileTransferProtocol : BasePropertyChangeImplementation {
        private string ftp;
        private string user;
        private string password;

        [Description("")]
        [DisplayName("server")]
        [JsonProperty]
        public string Server {
            get {
                return this.ftp;
            }
            set {
                this.ftp = value;
                this.OnPropertyChanged();
            }
        }

        [Description("")]
        [DisplayName("user")]
        [JsonProperty]
        public string User {
            get {
                return this.user;
            }
            set {
                this.user = value;
                this.OnPropertyChanged();
            }
        }

        [Description("")]
        [DisplayName("password")]
        [JsonProperty]
        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
                this.OnPropertyChanged();
            }
        }
    }
}
