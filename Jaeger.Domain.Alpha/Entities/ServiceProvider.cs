﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase de configuracion para proveedor autorizado de certificacion
    /// </summary>
    [JsonObject]
    public class ServiceProvider : BasePropertyChangeImplementation, IServiceProvider {
        public enum EnumServicePAC {
            Ninguno,
            Factorum,
            FiscoClic,
            SolucionFactible,
            Interno
        }

        private EnumServicePAC provider;
        private string userID;
        private string password;
        private string rfc;
        private bool produccion;
        private bool estricto;

        /// <summary>
        /// constructor
        /// </summary>
        public ServiceProvider() {
            this.Provider = EnumServicePAC.Ninguno;
        }

        /// <summary>
        /// obtener o establecer el proveedor del servicio
        /// </summary>
        [DisplayName("Proveedor"), Description("Servicios disponibles del proveedor autorizado de certificación.")]
        [JsonProperty("provider")]
        public EnumServicePAC Provider {
            get { return this.provider; }
            set {
                if (this.provider != value) {
                    this.provider = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// nombre de usuario del servicio
        /// </summary>
        [DisplayName("Usuario"), Description("Nombre de usuario para el uso del servicio.")]
        [JsonProperty("user")]
        public string User {
            get { return this.userID; }
            set {
                if (this.userID != value) {
                    this.userID = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el password del servicio
        /// </summary>
        [DisplayName("Contraseña"), Description("Contraseña del servicio.")]
        [JsonProperty("pass")]
        public string Pass {
            get { return this.password; }
            set {
                if (this.password != value) {
                    this.password = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// registro federal de contribuyentes
        /// </summary>
        [DisplayName("RFC"), Description("Registro federal de contribuyentes, requerido por algunos servicios de certificación o validación")]
        [JsonProperty("rfc")]
        public string RFC {
            get { return this.rfc; }
            set {
                if (this.rfc != value) {
                    this.rfc = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Establece el modo de timbrado, para pruebas solo debe deseleccionar la opción.
        /// </summary>
        [DisplayName("Modo Producto"), Description("Establecer el servicio en modo productivo.")]
        [JsonProperty("production")]
        public bool Production {
            get { return this.produccion; }
            set {
                if (this.produccion != value) {
                    this.produccion = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer si el servicio esta en modo estricto
        /// </summary>
        [DisplayName("Estricto"), Description("Delimitar el servicio en modo estricto.")]
        [JsonProperty("strict")]
        public bool Strinct {
            get { return this.estricto; }
            set {
                if (this.estricto != value) {
                    this.estricto = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}
