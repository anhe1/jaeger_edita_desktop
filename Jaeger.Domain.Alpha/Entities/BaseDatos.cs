﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    [JsonObject("dbs")]
    public partial class BaseDatos : BasePropertyChangeImplementation {
        #region declaraciones
        private DataBaseConfiguracion _Edita;
        private DataBaseConfiguracion _LiteCP;
        private DataBaseConfiguracion _CP;
        #endregion

        public BaseDatos() {
            this.Edita = new DataBaseConfiguracion();
            this.LiteCP = new DataBaseConfiguracion();
            this.CP = new DataBaseConfiguracion();
        }

        /// <summary>
        /// obtener o establecer la configuracion de la base de datos de la instancia de RDS de AMAZON
        /// </summary>
        [DisplayName("RDS EDITA"), Description("Configuración de la base de datos de la instancia de RDS de AMAZON")]
        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("edita")]
        public DataBaseConfiguracion Edita {
            get {
                return this._Edita;
            }
            set {
                this._Edita = value;
                this.OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("Bolsa")]
        public DataBaseConfiguracion LiteCP {
            get {
                return this._LiteCP;
            }
            set {
                this._LiteCP = value;
                this.OnPropertyChanged();
            }
        }

        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("CP")]
        public DataBaseConfiguracion CP {
            get {
                return this._CP;
            }
            set {
                this._CP = value;
                this.OnPropertyChanged();
            }
        }
    }
}
