﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// Amazon Simple Email Service (Amazon SES)
    /// </summary>
    public class SimpleEmailService : BasePropertyChangeImplementation {
        private bool enabled;
        private string host;
        private string port;
        private bool ssl;
        private string email;
        private string userID;
        private string password;

        public SimpleEmailService() {
        }

        [DisplayName("Habilitado"), Description("")]
        [JsonProperty("enabled")]
        public bool Enabled {
            get { return this.enabled; }
            set {
                if (this.enabled != value) {
                    this.enabled = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("Servidor"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("host")]
        public string Host {
            get { return this.host; }
            set {
                if (this.host != value) {
                    this.host = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("Puerto"), Description("")]
        [JsonProperty("port")]
        public string Port {
            get { return this.port; }
            set {
                if (this.port != value) {
                    this.port = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("SSL"), Description("")]
        [JsonProperty("ssl")]
        public bool SSL {
            get { return this.ssl; }
            set {
                if (this.ssl != value) {
                    this.ssl = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("Correo"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("email")]
        public string Email {
            get { return this.email; }
            set {
                if (this.email != value) {
                    this.email = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("Usuario"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("user")]
        public string User {
            get { return this.userID; }
            set {
                if (this.userID != value) {
                    this.userID = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("Contraseña"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("pass")]
        public string Password {
            get { return this.password; }
            set {
                if (this.password != value) {
                    this.password = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}
