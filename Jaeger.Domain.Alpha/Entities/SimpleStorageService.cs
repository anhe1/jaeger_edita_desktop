﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    public class SimpleStorageService : BasePropertyChangeImplementation {
        #region declaraciones
        private string _AccessKeyId;
        private bool _AllowToStore;
        private string _BucketName;
        private string _Folder = "CFDI";
        private string _Region;
        private string _SecretAccessKey;
        private bool _SendAutomatically;
        #endregion

        public SimpleStorageService() {
        }

        /// <summary>
        /// obtener o establecer el identificador único que asociado con una clave de acceso secreta.
        /// </summary>
        [DisplayName("AccessKeyId"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("accessKeyId")]
        public string AccessKeyId {
            get { return this._AccessKeyId; }
            set {
                if (this._AccessKeyId != value) {
                    this._AccessKeyId = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Permitir guardar en bucket de amazon
        /// </summary>
        [DisplayName("AllowToStore"), Description("")]
        [JsonProperty("allowToStore")]
        public bool AllowToStore {
            get { return this._AllowToStore; }
            set {
                if (this._AllowToStore != value) {
                    this._AllowToStore = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del contenedor bucket
        /// </summary>
        [DisplayName("Bucket Name"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("bucketName")]
        public string BucketName {
            get { return this._BucketName; }
            set {
                if (this._BucketName != value) {
                    this._BucketName = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establcer el folder que contene los comprobantes
        /// </summary>
        [DisplayName("Folder"), Description("")]
        [JsonProperty("folder")]
        public string Folder {
            get { return this._Folder; }
            set {
                if (this._Folder != value) {
                    this._Folder = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la region del bucket
        /// </summary>
        [DisplayName("Region"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("region")]
        public string Region {
            get { return this._Region; }
            set {
                if (this._Region != value) {
                    this._Region = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer secrect access key
        /// </summary>
        [DisplayName("SecretAccessKey"), Description(""), PasswordPropertyText(true)]
        [JsonProperty("secretAccessKey")]
        public string SecretAccessKey {
            get { return this._SecretAccessKey; }
            set {
                if (this._SecretAccessKey != value) {
                    this._SecretAccessKey = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o estblacer si debe enviar archivos despúes de validar
        /// </summary>
        [DisplayName(""), Description("")]
        [JsonProperty("sendAutomatically")]
        public bool SendAutomatically {
            get { return this._SendAutomatically; }
            set {
                if (this._SendAutomatically != value) {
                    this._SendAutomatically = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string ToJson() {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }
    }
}
