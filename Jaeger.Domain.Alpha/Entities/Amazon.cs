﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Empresa.Entities {
    [JsonObject("amazon")]
    public class Amazon : BasePropertyChangeImplementation {
        private SimpleStorageService s3;
        private SimpleEmailService ses;

        public Amazon() {
            this.S3 = new SimpleStorageService();
            this.SES = new SimpleEmailService();
        }

        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("s3")]
        public SimpleStorageService S3 {
            get { return this.s3; }
            set {
                if (this.s3 != value) {
                    this.s3 = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [TypeConverter(typeof(PropertiesConvert))]
        [JsonProperty("ses")]
        public SimpleEmailService SES {
            get { return this.ses; }
            set {
                if (this.ses != value) {
                    this.ses = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}
