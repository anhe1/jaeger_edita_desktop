﻿using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Domain.Empresa.Contracts {
    public interface IServiceProvider {
        /// <summary>
        /// obtener o establecer el proveedor del servicio
        /// </summary>
        ServiceProvider.EnumServicePAC Provider {
            get; set;
        }

        /// <summary>
        /// nombre de usuario del servicio
        /// </summary>
        string User {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el password del servicio
        /// </summary>
        string Pass {
            get; set;
        }

        /// <summary>
        /// registro federal de contribuyentes
        /// </summary>
        string RFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        bool Production {
            get; set;
        }

        /// <summary>
        /// obtener o establecer si el servicio esta en modo estricto
        /// </summary>
        bool Strinct {
            get; set;
        }
    }
}
