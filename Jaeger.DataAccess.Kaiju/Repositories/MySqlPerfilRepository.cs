﻿using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.DataAccess.Kaiju {
    public class MySqlPerfilRepository : RepositoryMaster<PerfilModel>, ISqlPerfilRepository {
        public MySqlPerfilRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }

        public PerfilModel GetById(int index) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<PerfilDetailModel> GetList() {
            throw new System.NotImplementedException();
        }

        public int Insert(PerfilModel item) {
            throw new System.NotImplementedException();
        }

        public int Update(PerfilModel item) {
            throw new System.NotImplementedException();
        }

        IEnumerable<PerfilModel> IGenericRepository<PerfilModel>.GetList() {
            throw new System.NotImplementedException();
        }
    }
}
