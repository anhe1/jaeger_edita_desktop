﻿using System.Linq;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.DataAccess.Kaiju {
    /// <summary>
    /// roles de usuario
    /// </summary>
    public class SqlSugarUserRolRepository : Abstractions.MySqlSugarContext<UserRolModel>, Domain.Kaiju.Contracts.ISqlUserRolRepository {
        public SqlSugarUserRolRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion dbase, string user) : base(dbase) {
            this.User = user;
        }

        ~SqlSugarUserRolRepository() { }

        public UserRolModel GetByIdUser(int idUser) {
            var CommandText = @"select _rlusrl.*, _rlsusr.* from _rlusrl left join _rlsusr on _rlusrl._rlusrl_rlsusr_id = _rlsusr._rlsusr_id where _rlusrl._rlusrl_usr_id = @index limit 1";
            var query = this.GetMapper<UserRolModel>(CommandText, new System.Collections.Generic.List<SqlSugar.SugarParameter> { new SqlSugar.SugarParameter("@index", idUser.ToString()) });
            return query.FirstOrDefault();
        }

        public UserRolModel Save(UserRolModel item) {
            if (item.IdUserRol == 0) {
                item.FechaNuevo = System.DateTime.Now;
                item.Creo = this.User;
                var _insert = this.Db.Insertable(item);
                item.IdUserRol = this.Execute(_insert);
            } else {
                item.FechaModifica = System.DateTime.Now;
                item.Modifica = this.User;
                var _update = this.Db.Updateable(item);
                this.Execute(_update);
            }
            return item;
        }
    }
}
