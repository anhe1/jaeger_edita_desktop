﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.DataAccess.Kaiju {
    /// <summary>
    /// repositorio de usuarios
    /// </summary>
    public class SqlSugarUserRepository : Abstractions.MySqlSugarContext<UserModel>, Domain.Kaiju.Contracts.ISqlUserRepository {
        public SqlSugarUserRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) { }

        /// <summary>
        /// buscar usuario por clave
        /// </summary>
        /// <param name="clave"></param>
        /// <returns></returns>
        public int Search(string clave) {
            return this.Db.Queryable<UserModel>().Where(it => it.Clave == clave).Select((c) => c.IdUser).Single();
        }

        /// <summary>
        /// guardar usuario
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UserModel Save(UserModel model) {
            if (model.IdUser == 0) {
                var _insert = this.Db.Insertable<UserModel>(model);
                model.IdUser = this.Execute(_insert);
            } else {
                var _update = this.Db.Updateable<UserModel>(model);
                this.Execute(_update);
            }
            return model;
        }

        /// <summary>
        /// obtener usuario por clave o correo
        /// </summary>
        /// <param name="clave">clave de usuario</param>
        /// <param name="correo">es corrreo</param>
        /// <returns>Usuario</returns>
        public UserModel GetUserBy(string clave, bool correo) {
            try {
                var response = this.Db.Queryable<UserModel>()
                    .WhereIF(correo, it => it.Correo == clave)
                    .WhereIF(correo == false, it => it.Clave == clave.ToUpper().Trim()).Single();
                return response;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                var response = this.Db.Queryable<UserModel>()
                    .WhereIF(correo, it => it.Correo == clave)
                    .WhereIF(correo == false, it => it.Clave == clave.ToUpper().Trim()).ToSql();
                var result = this.GetMapper<UserModel>(response.Key, response.Value).First();
                return result;
            }
        }

        /// <summary>
        /// obtener lista de objetos similares por condiciones
        /// </summary>
        public new IEnumerable<T1> GetList<T1>(List<Domain.Base.Builder.IConditional> conditionals) where T1 : class, new() {
            var sql = @"SELECT _user.* FROM _user @wcondiciones";
            return this.GetMapper<T1>(sql, conditionals);
        }

        ~SqlSugarUserRepository() { this.Db.Close(); }
    }
}
