﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Kaiju {
    public class MySqlUsuarioRepository : RepositoryMaster<UsuarioModel>, ISqlUsuarioRepository {
        public MySqlUsuarioRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Buscar(string clave) {
            throw new NotImplementedException();
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public bool Desactiva(UsuarioDetailModel item) {
            throw new NotImplementedException();
        }

        public UsuarioDetailModel GetByID(int index) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "SELECT * FROM _user WHERE _user_id = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<UsuarioModel>();
            var dto = mapper.Map(tabla).SingleOrDefault();
            if (dto != null) {
                var response = new UsuarioDetailModel();
                response.Clave = dto.Clave;
                response.Nombre = dto.Nombre;
                response.Correo = dto.Correo;
                response.Password = dto.Password;
                return response;
            }
            return null;
        }

        public UsuarioModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<UsuarioDetailModel> GetList() {
            throw new NotImplementedException();
        }

        public UsuarioDetailModel GetUsuario(string clave, bool correo) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "SELECT * FROM _user WHERE "
            };

            sqlCommand.CommandText = sqlCommand.CommandText + (correo == false ? "_user_clv = @clave" : "_user_mail = @clave");
            sqlCommand.Parameters.AddWithValue("@clave", clave);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<UsuarioModel>();
            var dto = mapper.Map(tabla).SingleOrDefault();

            if (dto != null) {
                var response = new UsuarioDetailModel {
                    Clave = dto.Clave,
                    Nombre = dto.Nombre,
                    Correo = dto.Correo,
                    IdPerfil = dto.IdPerfil,
                    Password = dto.Password
                };
                return response;
            }
            return null;
        }

        public int Insert(UsuarioModel item) {
            throw new NotImplementedException();
        }

        public UsuarioDetailModel Save(UsuarioDetailModel item) {
            throw new NotImplementedException();
        }

        public int Update(UsuarioModel item) {
            throw new NotImplementedException();
        }

        IEnumerable<UsuarioModel> IGenericRepository<UsuarioModel>.GetList() {
            throw new NotImplementedException();
        }
    }
}
