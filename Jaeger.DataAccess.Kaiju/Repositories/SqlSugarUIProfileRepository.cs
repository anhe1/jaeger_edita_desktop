﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Services;

namespace Jaeger.DataAccess.Kaiju {
    /// <summary>
    /// interface para perfiles de usuario
    /// </summary>
    public class SqlSugarUIProfileRepository : Abstractions.MySqlSugarContext<UIMenuProfileModel>, ISqlUIProfileRepository {
        protected ISqlUIMenuRepository menuRepository;
        protected ISqlUserRolRelacionRepository relacionRepository;

        public SqlSugarUIProfileRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.menuRepository = new SqlSugarUIMenuRepository(configuracion, user);
            this.relacionRepository = new SqlSugarUserRolRelacionRepository(configuracion, user);
        }

        #region CRUD
        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = @"SELECT _uiperfil.* FROM _uiperfil @wcondiciones";

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
        #endregion

        /// <summary>
        /// metodo para insertar o actualizar registro de permisos
        /// </summary>
        /// <param name="menu">IMenu</param>
        /// <returns>numero filas modificadas</returns>
        public int Salveable(UIMenuProfileModel menu) {
            var sqlCommand = @"INSERT INTO _uiperfil (_uiperfil_a,_uiperfil_rlsusr_id,_uiperfil_key,_uiperfil_action,_uiperfil_fn,_uiperfil_usr_n) 
                                              VALUES (@UIPERFIL_A,@UIPERFIL_RLSUSR_ID,@UIPERFIL_KEY,@UIPERFIL_ACTION,@UIPERFIL_FN,@UIPERFIL_USR_N) ON DUPLICATE KEY UPDATE
                            _uiperfil_action = @UIPERFIL_ACTION,
                            _uiperfil_key = @UIPERFIL_KEY,
                            _uiperfil_a = @UIPERFIL_A,
                            _uiperfil_rlsusr_id = @UIPERFIL_RLSUSR_ID,
                            _uiperfil_fn = @UIPERFIL_FN,
                            _uiperfil_usr_n = @UIPERFIL_USR_N";

            var Parameters = new List<SugarParameter> {
                new SugarParameter("@UIPERFIL_A", menu.Activo),
                new SugarParameter("@UIPERFIL_RLSUSR_ID", menu.IdRol),
                new SugarParameter("@UIPERFIL_ACTION", menu.Action2),
                new SugarParameter("@UIPERFIL_KEY", menu.Key),
                new SugarParameter("@UIPERFIL_FN", menu.FechaNuevo),
                new SugarParameter("@UIPERFIL_USR_N", menu.Creo)
             };

            return this.ExecuteTransaction(sqlCommand, Parameters);
        }

        public UIMenuProfileDetailModel Save(UIMenuProfileDetailModel model) {
            model.Creo = this.User;
            this.Salveable(model);
            return model;
        }

        public IEnumerable<UserRolDetailModel> GetProfile(bool onlyActive) {
            var result = this.Db.Queryable<UserRolDetailModel>().Where(it => it.Activo == true).ToList();
            var _perfiles = this.GetList<UIMenuProfileDetailModel>(new List<IConditional>());
            var _relaciones = this.relacionRepository.GetList<UIMenuRolRelacionModel>(new List<IConditional>());

            if (_perfiles != null) {
                for (int i = 0; i < result.Count(); i++) {
                    result[i].Perfil = new BindingList<UIMenuProfileDetailModel>(_perfiles.Where(it => it.IdRol == result[i].IdUserRol).ToList());
                    result[i].Relacion = new BindingList<UIMenuRolRelacionModel>(_relaciones.Where(it => it.IdUserRol == result[i].IdUserRol).ToList());
                }
            }

            return result;
        }

        public IEnumerable<UIMenuProfileDetailModel> GetBy(int idUser) {
            var _sql = @"SELECT _rlsusr.*, _rlusrl.*, _uiperfil.* FROM _rlusrl LEFT JOIN _rlsusr ON _rlusrl._rlusrl_rlsusr_id = _rlsusr._rlsusr_id LEFT JOIN _uiperfil ON _rlsusr._rlsusr_id = _uiperfil._uiperfil_rlsusr_id AND _uiperfil_a > 0 WHERE _rlusrl._rlusrl_usr_id = @INDEX";

            try {
                var _result = this.GetMapper<UIMenuProfileDetailModel>(_sql, new List<SugarParameter> { new SugarParameter("@index", idUser) }).ToList();
                return _result;
            } catch (Exception exp) {
                LogErrorService.LogWrite("ProfileRepository:OnError: " + exp.Message);
                Console.WriteLine(exp.Message);
            }
            return null;
        }
    }
}
