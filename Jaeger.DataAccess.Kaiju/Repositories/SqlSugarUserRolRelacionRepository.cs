﻿using System.Collections.Generic;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.DataAccess.Kaiju {
    public class SqlSugarUserRolRelacionRepository : Abstractions.MySqlSugarContext<UIMenuRolRelacionModel>, Domain.Kaiju.Contracts.ISqlUserRolRelacionRepository {
        public SqlSugarUserRolRelacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public new IEnumerable<T1> GetList<T1>(List<Domain.Base.Builder.IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = @"SELECT * FROM _rlusrl @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public UIMenuRolRelacionModel Save(UIMenuRolRelacionModel item) {
            if (item.IdRelacion == 0) {
                item.IdRelacion = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }
    }
}
