﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.DataAccess.Kaiju {
    /// <summary>
    /// repositorio para menus
    /// </summary>
    public class SqlSugarUIMenuRepository : Abstractions.MySqlSugarContext<UIMenuModel>, Domain.Kaiju.Contracts.ISqlUIMenuRepository {
        public SqlSugarUIMenuRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public UIMenuModel Save(UIMenuModel model) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<UIMenuElement> GetMenus() {
            var response = new List<UIMenuElement> {
                // tab administracion
                this.TabAdministracion()
            };
            response.AddRange(this.Group_Cliente());
            response.AddRange(this.GroupTesoreria());
            response.AddRange(this.Group_Almacenes());
            response.AddRange(this.Group_Proveedor());
            response.AddRange(this.Group_Nomina());
            // tab produccion
            response.AddRange(this.TabProduccion());
            // tab herramientas
            response.Add(this.TabTools());
            response.Add(this.GroupConfiguration());
            response.AddRange(this.ButtonParameters());
            response.Add(this.ButtonCategorias());
            response.Add(this.ButtonSeries());
            response.AddRange(this.ButtonCertificado());
            response.AddRange(this.GroupRetenciones());
            response.Add(this.GroupHerramientas());
            response.AddRange(this.GroupRepositorio());
            response.AddRange(this.ButtonBackups());
            response.AddRange(this.ButtonValidador());
            response.AddRange(this.ButtonUpdates());
            response.AddRange(this.GroupTheme());
            response.AddRange(this.GroupHelp());

            return response;
        }

        #region tab administacion
        private UIMenuElement TabAdministracion() {
            return new UIMenuElement { Id = 11000, ParentId = 0, Name = "tadmon", Label = "Administración", Parent = "0" };
        }

        private IEnumerable<UIMenuElement> Group_Cliente() {
            var response = new List<UIMenuElement> {
                //new UIMenuElement { Id = 11000, ParentId = 0, Name = "tadmon", Label = "Administración", Parent = "0"},
                new UIMenuElement { Id = 11100, ParentId = 11000, Name = "adm_grp_cliente", Label = "Cliente", Parent = "tadmon"},
                new UIMenuElement { Id = 11110, ParentId = 11100, Name = "adm_gcli_expediente", Label = "Expedientes", Parent = "adm_grp_cliente", Form = "Forms.Clientes.ExpedienteForm", Assembly = "Jaeger.UI", ToolTipText = "Directorio / Expedientes de clientes", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Remover }},
                new UIMenuElement { Id = 11120, ParentId = 11100, Name = "adm_gcli_subgrupo", Label = "SubGrupo", Parent = "adm_grp_cliente"},
                new UIMenuElement { Id = 11121, ParentId = 11120, Name = "adm_gcli_ordcliente", Label = "Orden de Cliente", Parent = "adm_gcli_subgrupo", Form = "Forms.OrdenClienteCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Orden de compra de cliente", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Autorizar, UIActionEnum.Cancelar }},
                new UIMenuElement { Id = 11122, ParentId = 11120, Name = "adm_gcli_remisionado", Label = "Remisionado", Parent = "adm_gcli_subgrupo", Form = "Forms.Clientes.RemisionadoForm", Assembly = "Jaeger.UI", ToolTipText = "Remisionado al cliente", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Status }},
                new UIMenuElement { Id = 11123, ParentId = 11122, Name = "adm_gcli_remxcobrar", Label = "Por Cobrar", Parent = "adm_gcli_remisionado", Form = "Forms.Clientes.RemisionadoPorCobrarForm", Assembly = "Jaeger.UI", ToolTipText = "Remisionado por cobrar"},
                new UIMenuElement { Id = 11124, ParentId = 11122, Name = "adm_gcli_remxpartida", Label = "Por Partidas", Parent = "adm_gcli_remisionado", Form = "Forms.Clientes.RemisionadoPorPartidaForm", Assembly = "Jaeger.UI", ToolTipText = "Remisionado por partidas"},
                new UIMenuElement { Id = 11146, ParentId = 11122, Name = "adm_gcli_porcliente", Label = "Por cliente", Parent = "adm_gcli_remisionado", Form = "Forms.Clientes.RemisionCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11125, ParentId = 11120, Name = "adm_gcli_facturacion", Label = "Facturación", Parent = "adm_gcli_subgrupo", Form = "Forms.Clientes.EmitidosForm", Assembly = "Jaeger.UI", ToolTipText = "Facturación a clientes", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Reporte }},
                new UIMenuElement { Id = 11126, ParentId = 11125, Name = "adm_gcli_comxcobrar", Label = "Por Cobrar", Parent = "adm_gcli_facturacion", Form = "Forms.Clientes.EmitidosPorCobrarForm", Assembly = "Jaeger.UI", ToolTipText = "Facturación a clientes por cobrar", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar }},
                new UIMenuElement { Id = 11147, ParentId = 11125, Name = "adm_gcli_comxpart", Label = "Por Partidas", Parent = "adm_gcli_facturacion", Form = "Forms.Clientes.ComprobantesFiscalesPartidaForm", Assembly = "Jaeger.UI", ToolTipText = "Facturación a clientes por partidas", LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar }},
                new UIMenuElement { Id = 11148, ParentId = 11125, Name = "adm_gcli_cpagos", Label = "Complemento de Pagos", Parent = "adm_gcli_facturacion", Form = "Forms.Proveedor.ReciboPagoCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11130, ParentId = 11100, Name = "adm_gcli_cobranza", Label = "Cobranza", Parent = "adm_grp_cliente", Form = "Forms.Clientes.ReciboCobroCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Cobranza a clientes", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar, UIActionEnum.Status, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 11140, ParentId = 11100, Name = "adm_gcli_reportes", Label = "Reportes", Parent = "adm_grp_cliente", ToolTipText = "Sección de reportes"},
                new UIMenuElement { Id = 11141, ParentId = 11140, Name = "adm_gcli_bedocuenta", Label = "Estado de Cuenta", Parent = "adm_gcli_reportes", Form = "Forms.Clientes.EstadoCuentaForm", Assembly = "Jaeger.UI", ToolTipText = "Estado de cuenta por cliente"},
                new UIMenuElement { Id = 11142, ParentId = 11140, Name = "adm_gcli_bcredito", Label = "Crédito", Parent = "adm_gcli_reportes", Form = "Forms.Clientes.EmitidosResumen2", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11143, ParentId = 11140, Name = "adm_gcli_bresumen", Label = "Resumen", Parent = "adm_gcli_reportes", Form = "Forms.Clientes.EmitidosResumenForm", Assembly = "Jaeger.UI", ToolTipText = "Resumen de factuación a cliente"},
                new UIMenuElement { Id = 11144, ParentId = 11100, Name = "adm_gcli_bret", Label = "Retenciones", Parent = "adm_grp_cliente", Form = "Forms.Retenciones.RetencionesEmitidoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11145, ParentId = 11140, Name = "adm_gcli_recfiscal", Label = "Recibos Fiscales", Parent = "adm_gcli_reportes", Form = "Forms.Clientes.RecibosFiscalesReporteForm", Assembly = "Jaeger.UI", ToolTipText = "Comprobantes de retenciones e informaición de pagos"},
                new UIMenuElement { Id = 11150, ParentId = 11140, Name = "adm_gcli_cpago2", Label = "Complementos de Pago", Parent = "adm_gcli_reportes", Form = "Forms.Clientes.ComplementoPagoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar }},
            };
            return response;
        }

        /// <summary>
        /// grupo tesoreria
        /// </summary>
        private IEnumerable<UIMenuElement> GroupTesoreria() {
            var _response = new List<UIMenuElement>() {
                new UIMenuElement { Id = 12000, ParentId = 11000, Name = "adm_grp_tesoreria", Label = "Tesorería", Parent = "tadmon"},
                new UIMenuElement { Id = 12001, ParentId = 12000, Name = "adm_gtes_bancoctas", Label = "Cuentas Bancarias", Parent = "adm_grp_tesoreria", ToolTipText = "Catálogo de cuentas bancarias",
                    Form = "Forms.Tesoreria.CuentasBancariaCatalogoForm", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 12002, ParentId = 12000, Name = "adm_gtes_banco", Label = "Banco", Parent = "adm_grp_tesoreria"},
                new UIMenuElement { Id = 12003, ParentId = 12002, Name = "adm_gtes_bformapago", Label = "Forma de Pago", Parent = "adm_gtes_banco", 
                    Form = "Forms.Tesoreria.FormaPagoCatalogoForm", ToolTipText = "Catálogo de formas de pago", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }},
                new UIMenuElement { Id = 12004, ParentId = 12002, Name = "adm_gtes_bconceptos", Label = "Conceptos", Parent = "adm_gtes_banco", 
                    Form = "Forms.Tesoreria.ConceptoCatalogoForm", ToolTipText = "Catálogo de tipos o conceptos de operación", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }},
                new UIMenuElement { Id = 12005, ParentId = 12002, Name = "adm_gtes_bbeneficiario", Label = "Beneficiario", Parent = "adm_gtes_banco", 
                    Form = "Forms.Tesoreria.BeneficiarioCatalogoForms", ToolTipText = "Catálogo de beneficiarios", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 12006, ParentId = 12002, Name = "adm_gtes_bmovimiento", Label = "Movimientos", Parent = "adm_gtes_banco", 
                    Form = "Forms.Tesoreria.MovimientoCatalogoForm", ToolTipText = "Movimientos bancarios por cuentas bancarias", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Status }},

            };
            return _response;
        }

        private IEnumerable<UIMenuElement> Group_Almacenes() {
            var _response = new List<UIMenuElement>() {
                new UIMenuElement { Id = 11400, ParentId = 11000, Name = "adm_grp_almacen", Label = "Almacén", Parent = "tadmon"},
                new UIMenuElement { Id = 11410, ParentId = 11400, Name = "adm_galmacen_mprima", Label = "Materia Prima", Parent = "adm_grp_almacen"},
                new UIMenuElement { Id = 11427, ParentId = 11410, Name = "adm_galmmp_categoria", Label = "Categorías", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.ClasificacionCategoriaForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 11411, ParentId = 11410, Name = "adm_galmmp_catalogo", Label = "Catálogo de Productos", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.ProductosCatalogoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 11412, ParentId = 11410, Name = "adm_galmmp_modelo", Label = "Producto - Modelos", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.ProductoModeloCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11413, ParentId = 11410, Name = "adm_galmmp_reqcompra", Label = "Requerimiento de Compra", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.RequermientoCompraForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 11414, ParentId = 11410, Name = "adm_galmmp_ordcompra", Label = "Orden a Proveedor", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.OrdenCompraCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11415, ParentId = 11410, Name = "adm_galmmp_vales", Label = "Movimientos", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.ValeAlmacenCatalogoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Status, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 11417, ParentId = 11410, Name = "adm_galmmp_existencia", Label = "Existencia", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.ProductoExistenciaForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11425, ParentId = 11410, Name = "adm_galmmp_cunidad", Label = "Catálogo de unidades", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.MP.UnidadCatalogoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Exportar }},

                new UIMenuElement { Id = 11420, ParentId = 11400, Name = "adm_galmacen_pterminado", Label = "Prod. Terminado", Parent = "adm_grp_almacen"},
                new UIMenuElement { Id = 11421, ParentId = 11420, Name = "adm_galmpt_catalogo", Label = "Catálogo", Parent = "adm_galmacen_pterminado"},
                new UIMenuElement { Id = 11422, ParentId = 11420, Name = "adm_galmpt_entrada", Label = "Movimientos", Parent = "adm_galmacen_pterminado", Form = "Forms.Almacen.PT.ValeAlmacenCatalogoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Status, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 11423, ParentId = 11420, Name = "adm_galmpt_salida", Label = "Remisionado", Parent = "adm_galmacen_pterminado", Form = "Forms.Almacen.PT.RemisionadoForm", Assembly = "Jaeger.UI", ToolTipText = "Remisionado al cliente", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 11424, ParentId = 11420, Name = "adm_galmpt_existencia", Label = "Existencia", Parent = "adm_galmacen_pterminado"},
                new UIMenuElement { Id = 11426, ParentId = 11410, Name = "adm_galmpt_cunidad", Label = "Catálogo de unidades", Parent = "adm_galmacen_mprima", Form = "Forms.Almacen.PT.UnidadCatalogoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }},
            };
            return _response;
        }

        private IEnumerable<UIMenuElement> Group_Proveedor() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Id = 11500, ParentId = 11000, Name = "adm_grp_proveedor", Label = "Proveedor", Parent = "tadmon"},
                new UIMenuElement { Id = 11510, ParentId = 11500, Name = "adm_gprv_expediente", Label = "Expedientes", Parent = "adm_grp_proveedor", 
                    Form = "Forms.Proveedor.ExpedienteForm", Assembly = "Jaeger.UI", ToolTipText = "Directorio de proveedores", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Remover }},
                new UIMenuElement { Id = 11520, ParentId = 11500, Name = "adm_gprv_subgrupo", Label = "Subgrupo Proveedor", Parent = "adm_grp_proveedor"},
                new UIMenuElement { Id = 11521, ParentId = 11520, Name = "adm_gprv_ordcompra", Label = "Orden a Proveedor", Parent = "adm_gprv_subgrupo", 
                    Form = "Forms.Proveedor.OrdenProveedorCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Orden de compra a proveedores", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 11522, ParentId = 11521, Name = "adm_gprv_solcotiza", Label = "Solicitud de Cotización", Parent = "adm_gprv_ordcompra", 
                    Form = "Forms.Proveedor.SolicitudCotizacionesForm", Assembly = "Jaeger.UI", ToolTipText = "Solicitud de cotizaciones a proveedor", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 11523, ParentId = 11520, Name = "adm_gprv_validador", Label = "Validar", Parent = "adm_gprv_subgrupo", 
                    Form = "Forms.Proveedor.Validador", Assembly = "Jaeger.UI", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Reporte }},
                new UIMenuElement { Id = 11524, ParentId = 11520, Name = "adm_gprv_facturacion", Label = "Facturación", Parent = "adm_gprv_subgrupo", 
                    Form = "Forms.Proveedor.RecibidosForm", Assembly = "Jaeger.UI", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Reporte, UIActionEnum.Status }},
                new UIMenuElement { Id = 11525, ParentId = 11524, Name = "adm_gprv_comxpagar", Label = "Por Pagar", Parent = "adm_gprv_facturacion", 
                    Form = "Forms.Proveedor.RecibidosPorPagarForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11531, ParentId = 11524, Name = "adm_gprv_comxpart", Label = "Por Partidas", Parent = "adm_gprv_facturacion", 
                    Form = "Forms.Proveedor.RecibidosPorPagarForm", Assembly = "Jaeger.UI", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Reporte }},
                new UIMenuElement { Id = 11544, ParentId = 11540, Name = "adm_gprv_cpago", Label = "Complemento de Pago", Parent = "adm_gprv_reportes", 
                    Form = "Forms.Proveedor.ComplementoPagoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11530, ParentId = 11524, Name = "adm_gprv_pagos", Label = "Pagos", Parent = "adm_grp_proveedor", 
                    Form = "Forms.Proveedor.ReciboPagoCatalogoForm", Assembly = "Jaeger.UI", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Status, UIActionEnum.Autorizar, UIActionEnum.Exportar }},
                
                new UIMenuElement { Id = 11540, ParentId = 11500, Name = "adm_gprv_reportes", Label = "Reportes", Parent = "adm_grp_proveedor"},
                new UIMenuElement { Id = 11545, ParentId = 11540, Name = "adm_gprv_cpago2", Label = "Complementos de Pago", Parent = "adm_gprv_reportes", 
                    Form = "Forms.Proveedor.ComplementoPagoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11541, ParentId = 11540, Name = "adm_gprv_edocuenta", Label = "Estado de Cuenta", Parent = "adm_gprv_reportes", 
                    Form = "Forms.Proveedor.EstadoCuentaForm", Assembly = "Jaeger.UI", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Status, UIActionEnum.Autorizar, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 11542, ParentId = 11540, Name = "adm_gprv_credito", Label = "Reporte 1", Parent = "adm_gprv_reportes", 
                    Form = "Forms.Proveedor.RecibidosResumen2Form", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 11543, ParentId = 11540, Name = "adm_gprv_resumen", Label = "Resumen", Parent = "adm_gprv_reportes", 
                    Form = "Forms.Proveedor.RecibidosResumenForm", Assembly = "Jaeger.UI"},
            };
            return response;
        }

        public IEnumerable<UIMenuElement> Group_Nomina() {
            var _response = new List<UIMenuElement>() {

                new UIMenuElement { Id = 90000, ParentId = 11000, Name = "adm_grp_nomina", Label = "Nómina", Parent = "tadmon" },
                new UIMenuElement { Id = 91001, ParentId = 90000, Name = "adm_gnom_empleado", Label = "Empleados", Parent = "adm_grp_nomina" },
                new UIMenuElement { Id = 91002, ParentId = 91001, Name = "adm_gnom_empexp", Label = "Expediente", Parent = "adm_gnom_empleado", 
                    Form = "Forms.Nomina.Empleados.EmpleadoCatalogoForm",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Remover, UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Importar }},
                new UIMenuElement { Id = 91003, ParentId = 91001, Name = "adm_gnom_empcont", Label = "Contratos", Parent = "adm_gnom_empleado", 
                    Form = "Forms.Nomina.Empleados.ContratoCatalogoForm", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Remover, UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar }},

                new UIMenuElement { Id = 93000, ParentId = 90000, Name = "adm_gnom_gmov", Label = "Movimientos", Parent = "adm_grp_nomina" },
                new UIMenuElement { Id = 93001, ParentId = 93000, Name = "adm_gnom_periodo", Label = "Periodo", Parent = "adm_gnom_gmov", 
                    Form = "Forms.Nomina.Movimientos.PeriodoCalcularForm",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar } },
                new UIMenuElement { Id = 93002, ParentId = 93001, Name = "adm_gnom_sgmov", Label = "Registros", Parent = "adm_gnom_gmov"},
                new UIMenuElement { Id = 93003, ParentId = 93002, Name = "adm_gnom_movvac", Label = "Vacaciones", Parent = "adm_gnom_sgmov",
                    Form = "Forms.Nomina.Movimientos.RegistrosVacacionesForm"},
                new UIMenuElement { Id = 93004, ParentId = 93002, Name = "adm_gnom_movfal", Label = "Faltas", Parent = "adm_gnom_sgmov",
                    Form = "Forms.Nomina.Movimientos.RegistrosAusenciasCalendarioForm"},
                new UIMenuElement { Id = 93005, ParentId = 93002, Name = "adm_gnom_movacu", Label = "Acumulados", Parent = "adm_gnom_sgmov"},

                new UIMenuElement { Id = 93006, ParentId = 93000, Name = "adm_gnom_aguina", Label = "Aguinaldo", Parent = "adm_gnom_gmov", 
                    Form = "Forms.Nomina.Movimientos.AguinaldoForm" },

                new UIMenuElement { Id = 94000, ParentId = 93000, Name = "adm_gnom_recibo", Label = "Recibos", Parent = "adm_grp_nomina" },
                new UIMenuElement { Id = 94001, ParentId = 94000, Name = "adm_gnom_reccon", Label = "Consulta", Parent = "adm_gnom_recibo", 
                    Form = "Forms.Nomina.Movimientos.RecibosForm" },
                new UIMenuElement { Id = 94002, ParentId = 94000, Name = "adm_gnom_recres", Label = "Resumen", Parent = "adm_gnom_recibo", 
                    Form = "Forms.Nomina.Movimientos.ResumenForm" },

                new UIMenuElement { Id = 95000, ParentId = 90000, Name = "adm_gnom_parame", Label = "Parametros", Parent = "adm_grp_nomina"},
                new UIMenuElement { Id = 96000, ParentId = 95000, Name = "adm_gnom_conf", Label = "Configuración", Parent = "adm_gnom_parame", 
                    Form = "Forms.Nomina.Parametros.ParametrosForm" },
                new UIMenuElement { Id = 95001, ParentId = 95000, Name = "adm_gnom_gconcep", Label = "Conceptos", Parent = "adm_gnom_parame", 
                    Form = "Forms.Nomina.Parametros.ConceptoCatalogoForm",  
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Remover, UIActionEnum.Agregar, UIActionEnum.Editar }},
                new UIMenuElement { Id = 95002, ParentId = 95000, Name = "adm_gnom_tablas", Label = "Tablas", Parent = "adm_gnom_parame" },
                new UIMenuElement { Id = 95003, ParentId = 95002, Name = "adm_gnom_tisr", Label = "Tabla ISR", Parent = "adm_gnom_tablas", 
                    Form = "Forms.Nomina.Parametros.TablaISRForm" },
                new UIMenuElement { Id = 95004, ParentId = 95002, Name = "adm_gnom_tsbe", Label = "Tabla Subsidio", Parent = "adm_gnom_tablas", 
                    Form = "Forms.Nomina.Parametros.TablaSubsidioForm" },
                new UIMenuElement { Id = 95005, ParentId = 95002, Name = "adm_gnom_tsm", Label = "Tabla Salario Minimo", Parent = "adm_gnom_tablas", 
                    Form = "Forms.Nomina.Parametros.TablaSalarioMinimoForm" },
                new UIMenuElement { Id = 95006, ParentId = 95002, Name = "adm_gnom_tuma", Label = "Tabla UMA", Parent = "adm_gnom_tablas",
                    Form = "Forms.Nomina.Parametros.TablaUMAForm" },
            };
            return _response;
        }
        #endregion

        #region tab produccion
        private IEnumerable<UIMenuElement> TabProduccion() {
            // los tabs se estan omitiendo dentro del menu, este fracmento de codigo esta relacionado con la version CP.Beta
            var _response = new List<UIMenuElement>() {
                new UIMenuElement { Id = 80000, ParentId = 0, Name = "tprod", Label = "Producción", Parent = "0", ToolTipText = "Todas las opciones disponibles son de CP"},

                new UIMenuElement { Id = 81000, ParentId = 80000, Name = "tcpempresa", Label = "Empresa", Parent = "tprod"},
                new UIMenuElement { Id = 81100, ParentId = 81000, Name = "cpemp_grp_directorio", Label = "Empresa", Parent = "tcpempresa"},
                new UIMenuElement { Id = 81110, ParentId = 81100, Name = "cpemp_gdir_directorio", Label = "Directorio", Parent = "cpemp_grp_directorio", 
                    Form = "Forms.CP.Empresa.ContribuyentesForm", Assembly = "Jaeger.UI", ToolTipText = "Directorio de CP", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 81200, ParentId = 81100, Name = "cpemp_grp_producto", Label = "g. Productos y Servicios", Parent = "cpemp_grp_directorio"},
                new UIMenuElement { Id = 81210, ParentId = 81200, Name = "cpemp_gprd_clasificacion", Label = "Clasificación", Parent = "cpemp_grp_producto", 
                    Form = "Forms.Empresa.ClasificacionCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 81220, ParentId = 81200, Name = "cpemp_gprd_catalogo", Label = "Productos Servicios", Parent = "cpemp_grp_producto", 
                    Form = "Forms.Cotizacion.ProductoServicioCatalogoForm", Assembly = "Jaeger.UI"},
                
                new UIMenuElement { Id = 84000, ParentId = 80000, Name = "tcpcotizacion", Label = "Cotizaciones", Parent = "tprod"},
                new UIMenuElement { Id = 84100, ParentId = 84000, Name = "cpcot_grp_cotizacion", Label = "Cotizaciones", Parent = "tcpcotizacion"},
                new UIMenuElement { Id = 84110, ParentId = 84100, Name = "cpcot_gcot_productos", Label = "Productos", Parent = "cpcot_grp_cotizacion"},
                new UIMenuElement { Id = 84111, ParentId = 84200, Name = "cpcot_bprd_subproducto", Label = "Sub Productos", Parent = "cpcot_gcot_presupuesto"},
                new UIMenuElement { Id = 84200, ParentId = 84100, Name = "cpcot_gcot_presupuesto", Label = "Presupuestos", Parent = "cpcot_grp_cotizacion"},
                new UIMenuElement { Id = 84300, ParentId = 84100, Name = "cpcot_gcot_cotizaciones", Label = "Cotizaciones", Parent = "cpcot_grp_cotizacion", Form = "Forms.Cotizacion.CotizacionCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 84400, ParentId = 84100, Name = "cpcot_gcot_cliente", Label = "Clientes", Parent = "cpcot_grp_cotizacion", 
                    Form = "Forms.CP.Cotizacion.ClientesForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 84500, ParentId = 84100, Name = "cpcot_gcot_conf", Label = "Configuración", Parent = "cpcot_grp_cotizacion"},
                
                new UIMenuElement { Id = 85000, ParentId = 80000, Name = "tcpproduccion", Label = "Producción", Parent = "tprod"},
                new UIMenuElement { Id = 85100, ParentId = 85000, Name = "cppro_grp_produccion", Label = "Producción", Parent = "tcpproduccion"},
                new UIMenuElement { Id = 85110, ParentId = 85100, Name = "cppro_gpro_orden", Label = "Ord. Producción", Parent = "cppro_grp_produccion"},
                new UIMenuElement { Id = 85111, ParentId = 85110, Name = "cppro_bord_historial", Label = "Historial", Parent = "cppro_gpro_orden", 
                    Form = "Forms.CP.Produccion.OrdenProduccionCatalogoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Autorizar, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 85112, ParentId = 85110, Name = "cppro_bord_proceso", Label = "En Proceso", Parent = "cppro_gpro_orden", 
                    Form = "Forms.CP.Produccion.OrdenEnProcesoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Autorizar, UIActionEnum.Exportar }},
                new UIMenuElement { Id = 85113, ParentId = 85100, Name = "cppro_gpro_reqcompra", Label = "Requerimiento de Compra", Parent = "cppro_grp_produccion", 
                    Form = "Forms.Adquisiciones.ReqsCompraForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }},
                new UIMenuElement { Id = 85120, ParentId = 85100, Name = "cppro_gpro_planea", Label = "Planeación", Parent = "cppro_grp_produccion", 
                    Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI.CP"},
                new UIMenuElement { Id = 85136, ParentId = 85120, Name = "cppro_gpro_calendario", Label = "Calendario de Entregas", Parent = "cppro_gpro_planea", Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85137, ParentId = 85120, Name = "cppro_gpro_gantt", Label = "Gantt de Entregas", Parent = "cppro_gpro_planea", Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85138, ParentId = 85120, Name = "cppro_gpro_plan", Label = "Departamento", Parent = "cppro_gpro_planea", Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85139, ParentId = 85120, Name = "cppro_gpro_tieadd", Label = "Tiempo Extra", Parent = "cppro_gpro_planea", Form = "Forms.Produccion.TiempoExtraForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85130, ParentId = 85100, Name = "cppro_gpro_sgrupo", Label = "Sub Grupo Producción", Parent = "cppro_grp_produccion"},
                new UIMenuElement { Id = 85131, ParentId = 85130, Name = "cppro_sgrp_depto", Label = "Departamento", Parent = "cppro_gpro_sgrupo"},
                new UIMenuElement { Id = 85146, ParentId = 85131, Name = "cppro_bdepto_trabaj", Label = "Trabajadores", Parent = "cppro_sgrp_depto", Form = "Forms.CP.Produccion.EmpleadoCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85147, ParentId = 85131, Name = "cppro_bdepto_proces", Label = "Procesos", Parent = "cppro_sgrp_depto", Form = "Forms.CP.Produccion.DepartamentoCalendarioEntregaForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85132, ParentId = 85130, Name = "cppro_sgrp_avance", Label = "Avance", Parent = "cppro_gpro_sgrupo", Form = "Forms.CP.Produccion.DepartamentoProduccionForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85133, ParentId = 85130, Name = "cppro_sgrp_evaluacion", Label = "Evaluación", Parent = "cppro_gpro_sgrupo"},
                new UIMenuElement { Id = 85134, ParentId = 85133, Name = "cppro_beva_eproduccion", Label = "Producción", Parent = "cppro_sgrp_evaluacion", Form = "Forms.CP.Produccion.DepartamentoEvaluacionForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85135, ParentId = 85133, Name = "cppro_beva_eperiodo", Label = "Período", Parent = "cppro_sgrp_evaluacion", Form = "Forms.CP.Produccion.ProduccionEvaluacion1Form", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85144, ParentId = 85133, Name = "cppro_beva_eprodof", Label = "Por Oficial", Parent = "cppro_sgrp_evaluacion", Form = "Forms.CP.Produccion.DepartamentoEvaluacionPForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 85140, ParentId = 85100, Name = "cppro_gpro_remision", Label = "Remisionado", Parent = "cppro_grp_produccion"},
                new UIMenuElement { Id = 85141, ParentId = 85140, Name = "cppro_brem_emitido", Label = "Emitido", Parent = "cppro_gpro_remision", 
                    Form = "Forms.CP.Almacen.DP.RemisionadoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }},
                new UIMenuElement { Id = 85142, ParentId = 85140, Name = "tcppro_brem_epartida", Label = "Recibido", Parent = "cppro_gpro_remision", 
                    Form = "Forms.Produccion.RemisionadoPartidaForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar }},
                new UIMenuElement { Id = 85143, ParentId = 85140, Name = "tcppro_brem_recibido", Label = "Recibido", Parent = "cppro_gpro_remision", Form = "Forms.Produccion.RemisionesForm", Assembly = "Jaeger.UI"},

                new UIMenuElement { Id = 86000, ParentId = 80000, Name = "tcpventas", Label = "Ventas", Parent = "tprod"},
                new UIMenuElement { Id = 86100, ParentId = 86000, Name = "cpvnt_grp_ventas", Label = "Ventas", Parent = "tcpventas"},
                new UIMenuElement { Id = 86110, ParentId = 86000, Name = "cpvnt_gven_vendedor", Label = "Vendedores", Parent = "tcpventas"},
                new UIMenuElement { Id = 86120, ParentId = 86100, Name = "cpvnt_gven_prodvend", Label = "Productos Vendidos", Parent = "cpvnt_grp_ventas", Form = "Forms.CP.Ventas.ProductoVendidoCatalogoForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 86130, ParentId = 86000, Name = "cpvnt_gven_group1", Label = "Sub grupo de ventas", Parent = "tcpventas"},
                new UIMenuElement { Id = 86131, ParentId = 86000, Name = "cpvnt_gven_pedidos", Label = "Pedidos", Parent = "tcpventas"},
                new UIMenuElement { Id = 86132, ParentId = 86000, Name = "cpvnt_gven_comision", Label = "Comisiones", Parent = "tcpventas"},
                new UIMenuElement { Id = 86133, ParentId = 86000, Name = "cpvnt_gven_ventcosto", Label = "Venta vs Costo", Parent = "tcpventas", Form = "Forms.Ventas.ProductoVendidoCatalogoCForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 86140, ParentId = 86100, Name = "cpvnt_gven_remisionado", Label = "Remisionado", Parent = "cpvnt_grp_ventas"},
                new UIMenuElement { Id = 86141, ParentId = 86140, Name = "cpvnt_brem_historial", Label = "Historial", Parent = "cpvnt_gven_remisionado", Form = "Forms.CP.RemisionadoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }},
                new UIMenuElement { Id = 86142, ParentId = 86140, Name = "cpvnt_brem_porcobrar", Label = "Por Cobrar", Parent = "cpvnt_gven_remisionado", Form = "Forms.Almacen.PT.RemisionadoPorCobrar", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 86143, ParentId = 86140, Name = "cpvnt_brem_rempart", Label = "Remisión Partidas", Parent = "cpvnt_gven_remisionado", Form = "Forms.Almacen.PT.RemisionadoPartidaForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar, UIActionEnum.Imprimir }},
                new UIMenuElement { Id = 86145, ParentId = 86000, Name = "cpvnt_gven_reportes", Label = "Reportes", Parent = "tcpventas"},
                new UIMenuElement { Id = 86146, ParentId = 88104, Name = "cpvnt_gven_reporte1", Label = "Reporte 1", Parent = "ccal_gnco_reporte", Form = "Forms.CP.Ventas.VentasReporte1Form", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 88147, ParentId = 88104, Name = "cpvnt_gven_reporte2", Label = "Reporte 2", Parent = "ccal_gnco_reporte", Form = "Forms.CP.Ventas.VentasReporte2Form", Assembly = "Jaeger.UI"},
                
                new UIMenuElement { Id = 88100, ParentId = 80000, Name = "ccal_grp_nconf", Label = "No Conformidades", Parent = "tprod"},
                new UIMenuElement { Id = 88101, ParentId = 88100, Name = "ccal_gnco_new", Label = "No Conformidad", Parent = "ccal_grp_nconf"},
                new UIMenuElement { Id = 88102, ParentId = 88101, Name = "ccal_gnco_todo", Label = "Historial", Parent = "ccal_gnco_new", Form = "Forms.CP.CCalidad.NoConformidadesForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }},
                new UIMenuElement { Id = 88103, ParentId = 88101, Name = "ccal_gnco_proceso", Label = "En Proceso", Parent = "ccal_gnco_new", Form = "Forms.CP.CCalidad.NoConformidadProcesoForm", Assembly = "Jaeger.UI", LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }},
                new UIMenuElement { Id = 88104, ParentId = 88100, Name = "ccal_gnco_reporte", Label = "Reportes", Parent = "ccal_grp_nconf"},
                new UIMenuElement { Id = 88105, ParentId = 88104, Name = "ccal_gnco_reporte1", Label = "Reporte Anual", Parent = "ccal_gnco_reporte", Form = "Forms.CP.CCalidad.NoConformidadEvaluarReporteForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 88106, ParentId = 88104, Name = "ccal_gnco_reporte2", Label = "Reporte 2", Parent = "ccal_gnco_reporte", Form = "Forms.CP.CCalidad.CCalidadEvaluarReporteForm", Assembly = "Jaeger.UI"},
                new UIMenuElement { Id = 88107, ParentId = 88104, Name = "ccal_gnco_reporte3", Label = "Reporte 3", Parent = "ccal_gnco_reporte", Form = "Forms.CCalidad.RemisionesFiscalesForm", Assembly = "Jaeger.UI"},
          };
            return _response;
        }
        #endregion

        #region tab herramientas
        private UIMenuElement TabTools() {
            return new UIMenuElement { Id = 100001, ParentId = 0, Name = "ttools", Label = "Herramientas", Parent = "0", Default = true, Rol = "*", IsAvailable = true };
        }

        private UIMenuElement GroupHerramientas() {
            return new UIMenuElement { Id = 10009, ParentId = 10001, Name = "dsk_grp_tools", Label = "Herramientas", Parent = "ttools" };
        }

        private UIMenuElement GroupConfiguration() {
            return new UIMenuElement { Id = 10002, ParentId = 10001, Name = "dsk_grp_config", Label = "Configuración", Parent = "ttools" };
        }

        private IEnumerable<UIMenuElement> ButtonParameters() {
            var groupConfiguration = new List<UIMenuElement> {
                new UIMenuElement { Name = "dsk_gconfig_param", Label = "Parametros", Parent = "dsk_grp_config", ToolTipText = "Parametros de Empresa"},
                new UIMenuElement { Name = "dsk_gconfig_emisor", Label = "Emisor", Parent = "dsk_gconfig_param", ToolTipText = "Información General de la Empresa",
                    Form = "Forms.Herramientas.Configuracion.ConfigurationEmisorForm" },
                new UIMenuElement { Name = "dsk_gconfig_avanzado", Label = "Avanzado", Parent = "dsk_gconfig_param", ToolTipText = "Configuraciones y Conexiones",
                    Form = "Forms.Herramientas.Configuracion.ConfigurationForm" },
                new UIMenuElement { Name = "dsk_gconfig_menu", Label = "Menú", Parent = "dsk_gconfig_param",
                    Form = "Forms.Herramientas.Configuracion.Profile.MenusCatalogForm" },
                new UIMenuElement { Name = "dsk_gconfig_usuario", Label = "Usuarios", Parent = "dsk_gconfig_param", ToolTipText = "Usuarios del Sistema",
                    Form = "Forms.Herramientas.Configuracion.Profile.UsersCatalogForm",
                    LPermisos = new List<UIActionEnum>{ UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover } },
                new UIMenuElement { Name = "dsk_gconfig_perfil", Label = "Perfil", Parent = "dsk_gconfig_param", ToolTipText = "Perfiles de usuarios",
                    Form = "Forms.Herramientas.Configuracion.Profile.ProfileActiveForm" }
            };
            return groupConfiguration;
        }

        private IEnumerable<UIMenuElement> ButtonUpdates() {
            return new List<UIMenuElement> {
                new UIMenuElement { Id = 10103, ParentId = 10009, Name = "dsk_gtools_actualiza", Label = "Actualizar", Parent = "dsk_grp_tools"},
                new UIMenuElement { Id = 10104, ParentId = 10103, Name = "dsk_gtools_catsat", Label = "Catálogos SAT", Parent = "dsk_gtools_actualiza", 
                    Form = "Forms.Herramientas.ActualizarForm" }
            };
        }

        private IEnumerable<UIMenuElement> ButtonBackups() {
            return new List<UIMenuElement> {
                new UIMenuElement { Id = 10021, ParentId = 10009, Name = "dsk_gtools_backup", Label = "Backup", Parent = "dsk_grp_tools"},
                new UIMenuElement { Id = 10022, ParentId = 10009, Name = "dsk_gtools_backcfdi", Label = "Backup CFDI", Parent = "dsk_gtools_backup", 
                    Form = "Forms.ComprobantesBackupForm", Assembly = "Jaeger.UI.Comprobante", ToolTipText = "Respaldo del historial de comprobantes fiscales"},
                new UIMenuElement { Id = 10023, ParentId = 10009, Name = "dsk_gtools_backdir", Label = "Backup Directorio", Parent = "dsk_gtools_backup", ToolTipText = "Respado del directorio del sistema" },
                new UIMenuElement { Id = 10123, ParentId = 10009, Name = "dsk_gtools_backret", Label = "Backup Retenciones", Parent = "dsk_gtools_backup", ToolTipText = "Respado del comprobantes de Retenciones e Información de Pagos" },
                new UIMenuElement { Id = 10124, ParentId = 10009, Name = "dsk_gtools_backrec", Label = "Backup Receptores", Parent = "dsk_gtools_backup", ToolTipText = "Respado del directorio de receptores" }
            };
        }

        private IEnumerable<UIMenuElement> GroupRepositorio() {
            return new List<UIMenuElement> {
                new UIMenuElement { Id = 10010, ParentId = 10009, Name = "dsk_grp_repositorio", Label = "Repositorio", Parent = "ttools", ToolTipText = "Repositorio de comprobantes fiscales" },
                new UIMenuElement { Id = 10012, ParentId = 10010, Name = "dsk_gre_comprobante", Label = "Comprobantes", Parent = "dsk_grp_repositorio", 
                    Form = "Forms.Repositorio.ComprobantesForm", },
                new UIMenuElement { Id = 10013, ParentId = 10012, Name = "dsk_gtools_descmasiva", Label = "Descarga masiva", Parent = "dsk_gre_comprobante" },
                new UIMenuElement { Id = 10014, ParentId = 10012, Name = "dsk_gtools_descnavegador", Label = "Descarga navegador", Parent = "dsk_gre_comprobante", ToolTipText = "Método por hilos" },
                new UIMenuElement { Id = 10015, ParentId = 10012, Name = "dsk_gtools_descterceros", Label = "Descarga API Terceros", Parent = "dsk_gre_comprobante", ToolTipText = "Descarga por API de terceros" },
                new UIMenuElement { Id = 10017, ParentId = 10010, Name = "dsk_gtools_consulta", Label = "Consulta", Parent = "dsk_gre_comprobante", ToolTipText = "Descarga por API de terceros" },
                
                new UIMenuElement { Id = 10018, ParentId = 10010, Name = "dsk_gre_solicitud", Label = "Solicitudes", Parent = "dsk_grp_repositorio", 
                    Form = "Forms.Repositorio.SolicitudesForm", ToolTipText = "Registros de solicitudes de descagas de comprobantes" },
                new UIMenuElement { Id = 10016, ParentId = 10010, Name = "dsk_gre_directorio", Label = "Directorio", Parent = "dsk_grp_repositorio", ToolTipText = "Directorio de emisores y receptores",
                    Form = "Forms.Repositorio.DirectorioForm"},


                new UIMenuElement { Id = 10019, ParentId = 10010, Name = "dsk_gtools_param", Label = "Parametros", Parent = "dsk_gtools_repositorio", 
                    Form = "Forms.Repositorio.ConfigurationForm", Assembly = "Jaeger.UI", ToolTipText = "Parametros de configuración" }
                //new UIMenuElement { Id = 10011, ParentId = 10010, Name = "dsk_gtools_asistido", Label = "Asistido", Parent = "dsk_gtools_repositorio", ToolTipText = "Descarga programada de comprobantes fiscales SAT" },
            };
        }

        private UIMenuElement ButtonCategorias() {
            return new UIMenuElement { Id = 10042, ParentId = 10003, Name = "dsk_gconfig_cate", Label = "Categorías", Parent = "dsk_grp_config", Form = "Forms.Herramientas.Configuracion.ClasificacionCategoriaForm",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }};
        }

        private IEnumerable<UIMenuElement> ButtonCertificado() {
            return new List<UIMenuElement> {
                new UIMenuElement { Name = "dsk_gconfig_cert", Label = "Certificados", Parent = "dsk_grp_config",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Remover } },
                new UIMenuElement { Name = "dsk_gconfig_certc", Label = "CSD", Parent = "dsk_grp_config", Form = "Forms.Herramientas.Configuracion.CertificadosForm",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Remover } },
                new UIMenuElement { Name = "dsk_gconfig_certf", Label = "Fiel", Parent = "dsk_grp_config", Form = "Forms.Herramientas.Configuracion.EFirmaForm",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Remover } }
            };
        }

        private UIMenuElement ButtonSeries() {
            return new UIMenuElement { Id = 10008, ParentId = 10002, Name = "dsk_gconfig_series", Label = "Series", Parent = "dsk_grp_config",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }};
        }

        private IEnumerable<UIMenuElement> ButtonValidador() {
            return new List<UIMenuElement> {
                new UIMenuElement { Id = 10024, ParentId = 10009, Name = "dsk_gtools_validador", Label = "Validador", Parent = "dsk_grp_tools"},
                new UIMenuElement { Id = 10025, ParentId = 10017, Name = "dsk_gtools_validar", Label = "Validar CFDI", Parent = "dsk_gtools_validador", 
                    Form = "Forms.Herramientas.Validador.ComprobanteFiscalForm", Assembly = "Jaeger.UI" },
                new UIMenuElement { Id = 10026, ParentId = 10017, Name = "dsk_gtools_validarfc", Label = "Valida RFC", Parent = "dsk_gtools_validador", 
                    Form = "Forms.Herramientas.Validador.ValidaRFCForm" },
                new UIMenuElement { Id = 10027, ParentId = 10017, Name = "dsk_gtools_validacedula", Label = "Cedula de Identificación Fiscal", Parent = "dsk_gtools_validador", 
                    Form = "Forms.Herramientas.Validador.CedulaForm" },
                new UIMenuElement { Id = 10028, ParentId = 10017, Name = "dsk_gtools_validaret", Label = "Valida Retención", Parent = "dsk_gtools_validador", 
                    Form = "Forms.Herramientas.Validador.ValidaRetencionForm" },
                new UIMenuElement { Id = 10029, ParentId = 10017, Name = "dsk_gtools_cancelado", Label = "Lista Cancelados", Parent = "dsk_gtools_validador" },
                new UIMenuElement { Id = 10030, ParentId = 10017, Name = "dsk_gtools_presuntos", Label = "Lista Presuntos", Parent = "dsk_gtools_validador" },
                new UIMenuElement { Id = 10031, ParentId = 10017, Name = "dsk_gtools_certificado", Label = "Certificado", Parent = "dsk_gtools_validador", 
                    Form = "Forms.Herramientas.Validador.CertificadoForm" }
            };
        }

        private IEnumerable<UIMenuElement> GroupHelp() {
            var groupUpdate = new List<UIMenuElement> {
                new UIMenuElement { Id = 10100, ParentId = 10001, Name = "dsk_grp_about", Label = "Ayuda", Parent = "ttools", Default = true },
                new UIMenuElement { Id = 10101, ParentId = 10001, Name = "dsk_btn_manual", Label = "Manual", Parent = "dsk_grp_about", Form = "Forms.Herramientas.Ayuda.AboutBox", Assembly = "Jaeger.UI", Default = true },
                new UIMenuElement { Id = 10102, ParentId = 10001, Name = "dsk_btn_about", Label = "Acerca de", Parent = "dsk_grp_about", Form = "Forms.Herramientas.Ayuda.AboutBox", Assembly = "Jaeger.UI", Default = true }
            };
            return groupUpdate;
        }

        private IEnumerable<UIMenuElement> GroupTheme() {
            var groupTheme = new List<UIMenuElement> {
                new UIMenuElement { Name = "dsk_grp_theme", Label = "Tema", Parent = "ttools", Default = true },
                new UIMenuElement { Name = "dsk_gtheme_2010black", Label = "Office 2010 Black", Parent = "dsk_grp_theme", Form = "Office2010Black", Default = true },
                new UIMenuElement { Name = "dsk_gtheme_2010blue", Label = "Office 2010 Blue", Parent = "dsk_grp_theme", Form = "Office2010Blue", Default = true },
                new UIMenuElement { Name = "dsk_gtheme_2010silver", Label = "Office 2010 Silver", Parent = "dsk_grp_theme", Form = "Office2010Silver", Default = true }
            };
            return groupTheme;
        }

        private IEnumerable<UIMenuElement> GroupRetenciones() {
            var _response = new List<UIMenuElement>() {
                new UIMenuElement { Id = 11101, ParentId = 10001, Name = "dsk_grp_retencion", Label = "Retención e Información de Pagos", Parent = "ttools"},
                new UIMenuElement { Id = 11102, ParentId = 11101, Name = "tdks_gretencion_recep", Label = "Receptores", Parent = "dsk_grp_retencion", 
                    Form = "Forms.Retencion.ComprobanteRetencionesForm" },
                new UIMenuElement { Id = 11103, ParentId = 11101, Name = "tdks_gretencion_comprobante", Label = "Comprobantes", Parent = "dsk_grp_retencion", 
                    Form = "Forms.Retencion.ComprobanteRetencionesForm"},
            };
            return _response;
        }
        #endregion

        #region tab Control de Calidad
        private IEnumerable<UIMenuElement> GetTabCalidad() {
            var response = new List<UIMenuElement> {
                TabCCalidad()
            };
            response.AddRange(this.GroupCCalidad());
            return response;
        }

        private UIMenuElement TabCCalidad() {
            return new UIMenuElement { Id = 88000, ParentId = 0, Name = "tccalidad", Label = "C. Calidad", Parent = "0", IsAvailable = true };
        }

        private IEnumerable<UIMenuElement> GroupCCalidad() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Id = 88100, ParentId = 88000, Name = "ccal_grp_nconf", Label = "No Conformidades", Parent = "tccalidad", IsAvailable = true },
                new UIMenuElement { Id = 88101, ParentId = 88100, Name = "ccal_gnco_new", Label = "No Conformidad", Parent = "ccal_grp_nconf", IsAvailable = true },
                new UIMenuElement { Id = 88102, ParentId = 88101, Name = "ccal_gnco_todo", Label = "Historial", Parent = "ccal_gnco_new",
                    Form = "Forms.CCalidad.NoConformidadesForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }, IsAvailable = true },
                new UIMenuElement { Id = 88103, ParentId = 88101, Name = "ccal_gnco_proceso", Label = "En Proceso", Parent = "ccal_gnco_new",
                    Form = "Forms.CCalidad.NoConformidadProcesoForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }, IsAvailable = true },
                new UIMenuElement { Id = 88104, ParentId = 88100, Name = "ccal_gnco_reporte", Label = "Reportes", Parent = "ccal_grp_nconf", IsAvailable = true },
                new UIMenuElement { Id = 88105, ParentId = 88104, Name = "ccal_gnco_reporte1", Label = "Reporte Anual", Parent = "ccal_gnco_reporte",
                    Form = "Forms.CCalidad.NoConformidadEvaluarReporteForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 88106, ParentId = 88104, Name = "ccal_gnco_reporte2", Label = "Reporte 2", Parent = "ccal_gnco_reporte",
                    Form = "Forms.CCalidad.CCalidadEvaluarReporteForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 88107, ParentId = 88104, Name = "ccal_gnco_reporte3", Label = "Reporte 3", Parent = "ccal_gnco_reporte",
                    Form = "Forms.CCalidad.RemisionesFiscalesForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true }
            };
            return response;
        }
        #endregion

        #region betas
        public IEnumerable<UIMenuElement> GetStrikerEureka() {
            var response = new List<UIMenuElement>() {
                this.TabAdministracion()
            };
            response.AddRange(this.Group_Cliente());
            response.AddRange(this.GroupTesoreria());
            
            response.AddRange(this.Group_Proveedor());
            response.AddRange(this.Group_Nomina());
            // tab produccion
            
            // tab herramientas
            response.Add(this.TabTools());
            response.Add(this.GroupConfiguration());
            response.AddRange(this.ButtonParameters());
            response.Add(this.ButtonCategorias());
            response.Add(this.ButtonSeries());
            response.AddRange(this.ButtonCertificado());
            response.AddRange(this.GroupRetenciones());
            response.Add(this.GroupHerramientas());
            response.AddRange(this.GroupRepositorio());
            response.AddRange(this.ButtonBackups());
            response.AddRange(this.ButtonValidador());
            response.AddRange(this.ButtonUpdates());
            response.AddRange(this.GroupTheme());
            response.AddRange(this.GroupHelp());

            return response;
        }

        public IEnumerable<UIMenuElement> GetCPLite() {
            var response = new List<UIMenuElement> {
                // Tab Administracion
                new UIMenuElement { Id = 71000, ParentId = 0, Name = "tcplite", Label = "Administración", Parent = "0", IsAvailable = true },
                new UIMenuElement { Id = 71101, ParentId = 71000, Name = "tcpl_grp_cliente", Label = "Cliente", Parent = "tcplite", IsAvailable = true },
                new UIMenuElement { Id = 71102, ParentId = 71101, Name = "tcpl_gcliente_expediente", Label = "Expedientes", Parent = "tcpl_grp_cliente",
                    Form = "Forms.Clientes.ClientesCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Directorio de clientes",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Remover }, IsAvailable = true },
                new UIMenuElement { Id = 71103, ParentId = 71101, Name = "tcpl_gcliente_subgrupo", Label = "gClientes", Parent = "tcpl_grp_cliente", IsAvailable = true },
                new UIMenuElement { Id = 71104, ParentId = 71103, Name = "tcpl_gcliente_pedido", Label = "Pedidos", Parent = "tcpl_gcliente_subgrupo",
                    Form = "Forms.Clientes.PedidoCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Pedidos de cliente",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71105, ParentId = 71103, Name = "tcpl_gcliente_remisionado", Label = "Remisionado", Parent = "tcpl_gcliente_subgrupo",
                    Form = "Forms.Clientes.RemisionadoForm", Assembly = "Jaeger.UI", ToolTipText = "Remisión de entrega a cliente",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Cancelar, UIActionEnum.Agregar, UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Status }, IsAvailable = true },
                new UIMenuElement { Id = 71106, ParentId = 71105, Name = "adm_gcli_porcliente", Label = "Por cliente", Parent = "tcpl_gcliente_remisionado",
                    Form = "Forms.Clientes.RemisionCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Cancelar, UIActionEnum.Agregar, UIActionEnum.Autorizar, UIActionEnum.Exportar }, IsAvailable = true },
                new UIMenuElement { Id = 71107, ParentId = 71105, Name = "adm_gcli_remxcobrar", Label = "Por cobrar", Parent = "tcpl_gcliente_remisionado",
                    Form = "Forms.Clientes.RemisionCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Cancelar, UIActionEnum.Agregar, UIActionEnum.Autorizar, UIActionEnum.Exportar }, IsAvailable = true },
                new UIMenuElement { Id = 71108, ParentId = 71105, Name = "adm_gcli_remxpartida", Label = "Remisión Partida", Parent = "tcpl_gcliente_remisionado", IsAvailable = true },
                new UIMenuElement { Id = 71109, ParentId = 71103, Name = "tcpl_gcliente_ndescuento", Label = "Nota de Descuento", Parent = "tcpl_gcliente_subgrupo",
                    Form = "Forms.Clientes.NotaDescuentoCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Notas de descuenta emitidas a cliente",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Cancelar, UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Autorizar, UIActionEnum.Status }, IsAvailable = true },
                new UIMenuElement { Id = 71110, ParentId = 71101, Name = "tcpl_gcliente_cobranza", Label = "Cobranza", Parent = "tcpl_grp_cliente",
                    Form = "Forms.Clientes.ReciboCobroCatalogoForm", Assembly = "Jaeger.UI", ToolTipText = "Cobros a cliente",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Autorizar, UIActionEnum.Cancelar, UIActionEnum.Status, UIActionEnum.Editar }, IsAvailable = true },
                new UIMenuElement { Id = 71111, ParentId = 71101, Name = "tcpl_gcliente_reportes", Label = "Reportes", Parent = "tcpl_grp_cliente", IsAvailable = true },
                new UIMenuElement { Id = 71112, ParentId = 71111, Name = "tcpl_gcliente_reporte1", Label = "Remisiones por Cliente", Parent = "tcpl_gcliente_reportes",
                    Form = "Forms.Clientes.RemisionPorClienteForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71113, ParentId = 71111, Name = "tcpl_gcliente_reporte2", Label = "Pedidos por Cliente", Parent = "tcpl_gcliente_reportes",
                    Form = "Forms.Clientes.PedidosPorClienteForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71114, ParentId = 71111, Name = "adm_gcli_bresumen", Label = "Resumen", Parent = "tcpl_gcliente_reportes",
                    Form = "Forms.Clientes.RemisionadoResumenForm", Assembly = "Jaeger.UI", ToolTipText = "Resumen de remisionado a cliente", IsAvailable = true },
            };
            response.AddRange(this.GroupTesoreria());
            response.AddRange(new[] {
                new UIMenuElement { Id = 71300, ParentId = 71000, Name = "tcpl_grp_almacen", Label = "Almacén", Parent = "tcplite", IsAvailable = true },
                new UIMenuElement { Id = 71301, ParentId = 71300, Name = "tcpl_galmacen_producto", Label = "Catálogo de\r\n Productos", Parent = "tcpl_grp_almacen", IsAvailable = true },
                new UIMenuElement { Id = 71315, ParentId = 71301, Name = "tcpl_galmacen_categoria", Label = "Categorías", Parent = "tcpl_galmacen_producto",
                    Form = "Forms.Almacen.PT.CategoriasForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Remover, UIActionEnum.Editar, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71302, ParentId = 71301, Name = "tcpl_galmacen_pcatalogo", Label = "Productos", Parent = "tcpl_galmacen_producto",
                    Form = "Forms.Almacen.ProductosCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Remover, UIActionEnum.Editar, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71303, ParentId = 71301, Name = "tcpl_galmacen_pmodelo", Label = "Productos-Modelos", Parent = "tcpl_galmacen_producto",
                    Form = "Forms.Almacen.ProductosModeloCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Remover, UIActionEnum.Editar, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71304, ParentId = 71301, Name = "tcpl_galmacen_unidad", Label = "Unidades", Parent = "tcpl_galmacen_producto",
                    Form = "Forms.Almacen.PT.UnidadCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }, IsAvailable = true },
                new UIMenuElement { Id = 71305, ParentId = 71301, Name = "tcpl_galmacen_especif", Label = "Especificación", Parent = "tcpl_galmacen_producto",
                    Form = "Forms.Almacen.EspecificacionCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }, IsAvailable = true },
                new UIMenuElement { Id = 71306, ParentId = 71300, Name = "tcpl_galmacen_pedido", Label = "Pedidos", Parent = "tcpl_grp_almacen", IsAvailable = true },
                new UIMenuElement { Id = 71307, ParentId = 71306, Name = "tcpl_galmacen_pedido1", Label = "Historial", Parent = "tcpl_galmacen_pedido",
                    Form = "Forms.Almacen.PedidoCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71308, ParentId = 71306, Name = "tcpl_galmacen_pedido2", Label = "Por Surtir", Parent = "tcpl_galmacen_pedido",
                    Form = "Forms.Almacen.PedidoPorSutirForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71309, ParentId = 71300, Name = "tcpl_galmacen_movimiento", Label = "Movimientos", Parent = "tcpl_grp_almacen", IsAvailable = true },
                new UIMenuElement { Id = 71310, ParentId = 71309, Name = "tcpl_galmacen_vales", Label = "Vales", Parent = "tcpl_galmacen_movimiento",
                    Form = "Forms.Almacen.PT.ValeAlmacenCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71311, ParentId = 71309, Name = "tcpl_galmacen_devolucion", Label = "Devoluciones", Parent = "tcpl_galmacen_movimiento",
                    Form = "Forms.Almacen.PT.DevolucionCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71312, ParentId = 71309, Name = "tcpl_galmacen_remisionado", Label = "Remisionado", Parent = "tcpl_galmacen_movimiento",
                    Form = "Forms.Almacen.PT.RemisionadoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71313, ParentId = 71309, Name = "tcpl_galmacen_existencia", Label = "Existencia", Parent = "tcpl_galmacen_movimiento",
                    Form = "Forms.Almacen.PT.ProductoExistenciaForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71314, ParentId = 71300, Name = "tcpl_galmacen_reporte", Label = "Reportes", Parent = "tcpl_grp_almacen",
                    Form = "Forms.Almacen.PT.ProductoExistenciaForm", Assembly = "Jaeger.UI", IsAvailable = true },

                new UIMenuElement { Id = 71400, ParentId = 71000, Name = "tcpl_grp_tienda", Label = "Tienda", Parent = "tcplite", IsAvailable = true },
                new UIMenuElement { Id = 71401, ParentId = 71400, Name = "tcpl_gtienda_conf", Label = "Ajustes", Parent = "tcpl_grp_tienda", IsAvailable = true },
                new UIMenuElement { Id = 71402, ParentId = 71401, Name = "tcpl_gtienda_seccion", Label = "Secciones", Parent = "tcpl_gtienda_conf",
                    Form = "Forms.Tienda.SeccionesForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71403, ParentId = 71401, Name = "tcpl_gtienda_categoria", Label = "Categorías", Parent = "tcpl_gtienda_conf",
                    Form = "Forms.Tienda.CategoriasCatalogoForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71404, ParentId = 71400, Name = "tcpl_gtienda_grupo1", Label = "Sub grupo 1", Parent = "tcpl_grp_tienda", IsAvailable = true },
                new UIMenuElement { Id = 71405, ParentId = 71404, Name = "tcpl_gtienda_cupon", Label = "Cupones", Parent = "tcpl_gtienda_grupo1",
                    Form = "Forms.Tienda.CuponCatalogoForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71406, ParentId = 71404, Name = "tcpl_gtienda_banner", Label = "Banner", Parent = "tcpl_gtienda_grupo1",
                    Form = "Forms.Tienda.BannerCatalogoForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71407, ParentId = 71404, Name = "tcpl_gtienda_faqs", Label = "FAQ's", Parent = "tcpl_gtienda_grupo1",
                    Form = "Forms.Tienda.PreguntaFrecuenteCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }, IsAvailable = true },

                new UIMenuElement { Id = 71500, ParentId = 71000, Name = "tcpl_grp_ventas", Label = "Ventas", Parent = "tcplite", IsAvailable = true },
                new UIMenuElement { Id = 71501, ParentId = 71500, Name = "tcpl_gventas_cprecio", Label = "Catálogo de\r\n Precios", Parent = "tcpl_grp_ventas",
                    Form = "Forms.Ventas.PrecioCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71502, ParentId = 71500, Name = "tcpl_gventas_cdescuento", Label = "Catálogo de Descuentos", Parent = "tcpl_grp_ventas",
                    Form = "Forms.Ventas.DescuentoCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71503, ParentId = 71500, Name = "tcpl_gventas_pedido", Label = "Pedidos", Parent = "tcpl_grp_ventas",
                    Form = "Forms.Tienda.PedidoCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71504, ParentId = 71500, Name = "tcpl_gventas_vendedor", Label = "Vendedores", Parent = "tcpl_grp_ventas", IsAvailable = true },
                new UIMenuElement { Id = 71505, ParentId = 71504, Name = "tcpl_gventas_vendedores", Label = "Catálogo", Parent = "tcpl_gventas_vendedor",
                    Form = "Forms.Ventas.VendedorCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71506, ParentId = 71504, Name = "tcpl_gventas_ccomision", Label = "Catálogo de Comisiones", Parent = "tcpl_gventas_vendedor",
                    Form = "Forms.Ventas.ComisionCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover }, IsAvailable = true },
                new UIMenuElement { Id = 71507, ParentId = 71504, Name = "tcpl_gventas_pcomision", Label = "Comisión por Remisión", Parent = "tcpl_gventas_vendedor",
                    Form = "Forms.Ventas.ComisionRemisionForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 71508, ParentId = 71504, Name = "tcpl_gventas_crecibo", Label = "Recibos de Comisión", Parent = "tcpl_gventas_vendedor",
                    Form = "Forms.Ventas.ComisionReciboCatalogoForm", Assembly = "Jaeger.UI",
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 71509, ParentId = 71500, Name = "tcpl_gventas_reportes", Label = "Reportes", Parent = "tcpl_grp_ventas", IsAvailable = true },
                new UIMenuElement { Id = 71510, ParentId = 71509, Name = "tcpl_gventas_reporte1", Label = "Ventas por vendedor", Parent = "tcpl_gventas_reportes",
                    Form = "Forms.Ventas.ReporteVentasForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71511, ParentId = 71509, Name = "tcpl_gventas_reporte2", Label = "Estado de Cuenta Vendedor", Parent = "tcpl_gventas_reportes",
                    Form = "Forms.Ventas.EstadoCuentaForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71512, ParentId = 71509, Name = "tcpl_gventas_reporte3", Label = "Estado de Cuenta Cliente", Parent = "tcpl_gventas_reportes",
                    Form = "Forms.Ventas.EstadoCuentaClienteForm", Assembly = "Jaeger.UI", IsAvailable = true },
                new UIMenuElement { Id = 71513, ParentId = 71509, Name = "tcpl_gventas_reporte4", Label = "Resumen de Ventas", Parent = "tcpl_gventas_reportes",
                    Form = "Forms.Ventas.ResumenForm", Assembly = "Jaeger.UI", IsAvailable = true },
                });
            response.Add(this.TabTools());
            response.Add(this.GroupConfiguration());
            response.AddRange(this.ButtonParameters());
            response.Add(this.ButtonCategorias());
            response.AddRange(this.GroupTheme());
            response.AddRange(this.GroupHelp());
            return response;
        }

        public IEnumerable<UIMenuElement> GetNominaBeta() {
            var response = new List<UIMenuElement>();
            response.AddRange(this.Group_Nomina());
            response[0].Parent = "0";
            response.Add(this.TabTools());
            response.Add(this.GroupConfiguration());
            response.AddRange(this.ButtonParameters());
            response.AddRange(this.ButtonCertificado());
            response.Add(this.ButtonSeries());
            response.Add(this.GroupHerramientas());
            response.AddRange(this.ButtonValidador());
            response.AddRange(this.ButtonUpdates());
            response.AddRange(this.GroupTheme());
            response.AddRange(this.GroupHelp());
            return response;
        }

        /// <summary>
        /// menu y permisos para CP.Beta
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UIMenuElement> GetCPBeta() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Id = 81000, ParentId = 0, Name = "tcpempresa", Label = "Empresa", Parent = "0", IsAvailable = true },
                new UIMenuElement { Id = 81100, ParentId = 81000, Name = "cpemp_grp_directorio", Label = "Directorio", Parent = "tcpempresa", IsAvailable = true },
                new UIMenuElement { Id = 81110, ParentId = 81100, Name = "cpemp_gdir_directorio", Label = "Directorio", Parent = "cpemp_grp_directorio", 
                    Form = "Forms.Empresa.ContribuyentesForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 81200, ParentId = 81100, Name = "cpemp_grp_producto", Label = "Productos y Servicios", Parent = "tcpempresa", IsAvailable = true },
                new UIMenuElement { Id = 81210, ParentId = 81200, Name = "cpemp_gprd_clasificacion", Label = "Clasificación", Parent = "cpemp_grp_producto", 
                    Form = "Forms.Empresa.ClasificacionCatalogoForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 81220, ParentId = 81200, Name = "cpemp_gprd_catalogo", Label = "Productos\r\nServicios", Parent = "cpemp_grp_producto", 
                    Form = "Forms.Cotizacion.ProductoServicioCatalogoForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },

                new UIMenuElement { Id = 84000, ParentId = 0, Name = "tcpcotizacion", Label = "Cotizaciones", Parent = "0", IsAvailable = true },
                new UIMenuElement { Id = 84100, ParentId = 84000, Name = "cpcot_grp_cotizacion", Label = "Productos", Parent = "tcpcotizacion", IsAvailable = true },
                new UIMenuElement { Id = 84110, ParentId = 84100, Name = "cpcot_gcot_productos", Label = "Productos", Parent = "cpcot_grp_cotizacion", IsAvailable = true },
                new UIMenuElement { Id = 84111, ParentId = 84200, Name = "cpcot_bprd_subproducto", Label = "Sub\r\nProductos", Parent = "cpcot_gcot_presupuesto", IsAvailable = true },
                new UIMenuElement { Id = 84200, ParentId = 84100, Name = "cpcot_gcot_presupuesto", Label = "Presupuestos", Parent = "cpcot_grp_cotizacion", 
                    Form = "Forms.CCalidad.NoConformidadesForm", Assembly = "Jaeger.UI.Beta", IsAvailable = true },
                new UIMenuElement { Id = 84300, ParentId = 84100, Name = "cpcot_gcot_cotizaciones", Label = "Cotizaciones", Parent = "cpcot_grp_cotizacion", 
                    Form = "Forms.Cotizacion.CotizacionCatalogoForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 84400, ParentId = 84100, Name = "cpcot_gcot_cliente", Label = "Clientes", Parent = "cpcot_grp_cotizacion", 
                    Form = "Forms.Cotizacion.ClientesForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 84500, ParentId = 84100, Name = "cpcot_gcot_conf", Label = "Configuración", Parent = "cpcot_grp_cotizacion", IsAvailable = true },
                
                new UIMenuElement { Id = 85000, ParentId = 0, Name = "tcpproduccion", Label = "Producción", Parent = "0", IsAvailable = true },
                new UIMenuElement { Id = 85100, ParentId = 85000, Name = "cppro_grp_produccion", Label = "Producción", Parent = "tcpproduccion", IsAvailable = true },
                new UIMenuElement { Id = 85110, ParentId = 85100, Name = "cppro_gpro_orden", Label = "Ord. Producción", Parent = "cppro_grp_produccion", IsAvailable = true },
                new UIMenuElement { Id = 85111, ParentId = 85110, Name = "cppro_bord_historial", Label = "Historial", Parent = "cppro_gpro_orden", 
                    Form = "Forms.Produccion.OrdenProduccionHistorialForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 85112, ParentId = 85110, Name = "cppro_bord_proceso", Label = "En Proceso", Parent = "cppro_gpro_orden", 
                    Form = "Forms.Produccion.OrdenEnProcesoForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Autorizar, UIActionEnum.Exportar, UIActionEnum.Cancelar }, IsAvailable = true },
                new UIMenuElement { Id = 85113, ParentId = 85100, Name = "cppro_gpro_reqcompra", Label = "Requerimiento\r\nde Compra", Parent = "cppro_grp_produccion", 
                    Form = "Forms.Produccion.ReqsCompraForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar }, IsAvailable = true },
                new UIMenuElement { Id = 85120, ParentId = 85100, Name = "cppro_gpro_planea", Label = "Planeación", Parent = "cppro_grp_produccion", 
                    Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85136, ParentId = 85120, Name = "cppro_gpro_calendario", Label = "Calendario de Entregas", Parent = "cppro_gpro_planea", 
                    Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85137, ParentId = 85120, Name = "cppro_gpro_gantt", Label = "Gantt de Entregas", Parent = "cppro_gpro_planea", 
                    Form = "Forms.Produccion.Planeacion3Form", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85138, ParentId = 85120, Name = "cppro_gpro_plan", Label = "Departamento", Parent = "cppro_gpro_planea", 
                    Form = "Forms.Produccion.Planeacion2Form", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85139, ParentId = 85120, Name = "cppro_gpro_tieadd", Label = "Tiempo Extra", Parent = "cppro_gpro_planea", 
                    Form = "Forms.Produccion.TiempoExtraForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85145, ParentId = 85120, Name = "cppro_gpro_plan4", Label = "Planeación 04", Parent = "cppro_gpro_planea", 
                    Form = "Forms.Produccion.Planeacion4Form", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85130, ParentId = 85100, Name = "cppro_gpro_sgrupo", Label = "Sub Grupo Producción", Parent = "cppro_grp_produccion", IsAvailable = true },
                new UIMenuElement { Id = 85131, ParentId = 85130, Name = "cppro_sgrp_depto", Label = "Departamento", Parent = "cppro_gpro_sgrupo", IsAvailable = true },
                new UIMenuElement { Id = 85146, ParentId = 85131, Name = "cppro_bdepto_trabaj", Label = "Trabajadores", Parent = "cppro_sgrp_depto", 
                    Form = "Forms.Produccion.EmpleadoCatalogoForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85147, ParentId = 85131, Name = "cppro_bdepto_proces", Label = "Procesos", Parent = "cppro_sgrp_depto", 
                    Form = "Forms.Produccion.DepartamentoCalendarioEntregaForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85132, ParentId = 85130, Name = "cppro_sgrp_avance", Label = "Avance", Parent = "cppro_gpro_sgrupo", 
                    Form = "Forms.Produccion.DepartamentoProduccionForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85133, ParentId = 85130, Name = "cppro_sgrp_evaluacion", Label = "Evaluación", Parent = "cppro_gpro_sgrupo", IsAvailable = true },
                new UIMenuElement { Id = 85134, ParentId = 85133, Name = "cppro_beva_eproduccion", Label = "Producción", Parent = "cppro_sgrp_evaluacion", 
                    Form = "Forms.Produccion.DepartamentoEvaluacionForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85135, ParentId = 85133, Name = "cppro_beva_eperiodo", Label = "Período", Parent = "cppro_sgrp_evaluacion", 
                    Form = "Forms.Produccion.ProduccionEvaluacion1Form", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85144, ParentId = 85133, Name = "cppro_beva_eprodof", Label = "Por Oficial", Parent = "cppro_sgrp_evaluacion", 
                    Form = "Forms.Produccion.DepartamentoEvaluacionPForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 85140, ParentId = 85100, Name = "cppro_gpro_remision", Label = "Remisionado", Parent = "cppro_grp_produccion", IsAvailable = true },
                new UIMenuElement { Id = 85141, ParentId = 85140, Name = "cppro_brem_emitido", Label = "Emitido", Parent = "cppro_gpro_remision", 
                    Form = "Forms.Almacen.DP.RemisionadoForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }, IsAvailable = true },
                new UIMenuElement { Id = 85142, ParentId = 85140, Name = "tcppro_brem_epartida", Label = "Recibido", Parent = "cppro_gpro_remision", 
                    Form = "Forms.Produccion.RemisionadoPartidaForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar }, IsAvailable = true },
                new UIMenuElement { Id = 85143, ParentId = 85140, Name = "tcppro_brem_recibido", Label = "Recibido", Parent = "cppro_gpro_remision", 
                    Form = "Forms.Produccion.RemisionesForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                
                new UIMenuElement { Id = 86000, ParentId = 0, Name = "tcpventas", Label = "Ventas", Parent = "0", IsAvailable = true },
                new UIMenuElement { Id = 86100, ParentId = 86000, Name = "cpvnt_grp_ventas", Label = "Ventas", Parent = "tcpventas", IsAvailable = true },
                new UIMenuElement { Id = 86110, ParentId = 86000, Name = "cpvnt_gven_vendedor", Label = "Vendedores", Parent = "tcpventas", 
                    Form = "Forms.Ventas.VendedoresCatalogoForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 86120, ParentId = 86100, Name = "cpvnt_gven_prodvend", Label = "Productos \r\nVendidos", Parent = "cpvnt_grp_ventas", 
                    Form = "Forms.Ventas.ProductoVendidoCatalogoForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 86130, ParentId = 86000, Name = "cpvnt_gven_group1", Label = "Sub grupo de ventas", Parent = "tcpventas", IsAvailable = true },
                new UIMenuElement { Id = 86131, ParentId = 86000, Name = "cpvnt_gven_pedidos", Label = "Pedidos", Parent = "tcpventas", IsAvailable = true },
                new UIMenuElement { Id = 86132, ParentId = 86000, Name = "cpvnt_gven_comision", Label = "Comisiones", Parent = "tcpventas", IsAvailable = true },
                new UIMenuElement { Id = 86133, ParentId = 86000, Name = "cpvnt_gven_ventcosto", Label = "Venta vs Costo", Parent = "tcpventas", 
                    Form = "Forms.Ventas.ProductoVendidoCatalogoCForm", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 86140, ParentId = 86100, Name = "cpvnt_gven_remisionado", Label = "Remisionado", Parent = "cpvnt_grp_ventas", IsAvailable = true },
                new UIMenuElement { Id = 86141, ParentId = 86140, Name = "cpvnt_brem_historial", Label = "Historial", Parent = "cpvnt_gven_remisionado", 
                    Form = "Forms.Ventas.RemisionadoForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }, IsAvailable = true },
                new UIMenuElement { Id = 86142, ParentId = 86140, Name = "adm_gcli_remxcobrar", Label = "Por Cobrar", Parent = "cpvnt_gven_remisionado", 
                    Form = "Forms.Almacen.PT.RemisionadoPorCobrar", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 86143, ParentId = 86140, Name = "adm_gcli_remxpartida", Label = "Remisión Partidas", Parent = "cpvnt_gven_remisionado", 
                    Form = "Forms.Almacen.PT.RemisionadoPartidaForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar, UIActionEnum.Imprimir }, IsAvailable = true },
                new UIMenuElement { Id = 86144, ParentId = 86140, Name = "adm_gcli_porcliente", Label = "Por Cliente", Parent = "cpvnt_gven_remisionado", 
                    Form = "Forms.Almacen.PT.RemisionadoClienteForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>() { UIActionEnum.Exportar, UIActionEnum.Imprimir }, IsAvailable = true },
                new UIMenuElement { Id = 86145, ParentId = 86000, Name = "cpvnt_gven_reportes", Label = "Reportes", Parent = "tcpventas", IsAvailable = true },
                new UIMenuElement { Id = 86146, ParentId = 88104, Name = "cpvnt_gven_reporte1", Label = "Reporte 1", Parent = "ccal_gnco_reporte", 
                    Form = "Forms.Ventas.VentasReporte1Form", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true },
                new UIMenuElement { Id = 88147, ParentId = 88104, Name = "cpvnt_gven_reporte2", Label = "Reporte 2", Parent = "ccal_gnco_reporte", 
                    Form = "Forms.Ventas.VentasReporte2Form", Assembly = "Jaeger.UI.CP.Beta", IsAvailable = true }
            };
            // TAB Control de Calidad
            response.AddRange(this.GetTabCalidad());
            // TAB Herramientas
            response.Add(this.TabTools());
            // Grupo Configuracion
            response.Add(this.GroupConfiguration());
            response.AddRange(this.ButtonParameters());
            response.AddRange(this.GroupTheme());
            response.AddRange(this.GroupHelp());
            return response;
        }

        public IEnumerable<UIMenuElement> GetRepositorioBeta() {
            var response = new List<UIMenuElement> {
               new UIMenuElement { Id = 10010, ParentId = 11009, Name = "dsk_trepositorio", Label = "Repositorio", Parent = "0", ToolTipText = "Repositorio de comprobantes fiscales" }
            };

            response.AddRange(this.GroupRepositorio());
            response[0].Parent = "dsk_trepositorio";
            response.Add(this.TabTools());

            response.Add(this.GroupConfiguration());
            response.AddRange(this.ButtonParameters());
            
            
            response.AddRange(this.ButtonCertificado());
            
            response.Add(this.GroupHerramientas());
            response.AddRange(this.ButtonValidador());
            response.AddRange(this.ButtonUpdates());
            response.AddRange(this.GroupTheme());
            response.AddRange(this.GroupHelp());

            return response;
        }
        #endregion
    }
}
