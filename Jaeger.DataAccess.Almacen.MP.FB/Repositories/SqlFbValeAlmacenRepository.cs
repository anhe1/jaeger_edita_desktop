﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.FB.Almacen.MP.Repositories {
    public class SqlFbValeAlmacenRepository : RepositoryMaster<ValeAlmacenModel> {
        public SqlFbValeAlmacenRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }
    }
}
