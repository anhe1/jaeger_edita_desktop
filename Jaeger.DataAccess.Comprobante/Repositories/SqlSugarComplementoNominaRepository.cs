﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComplementoNominaRepository : MySqlSugarContext<ComplementoNominaModel>, ISqlComplementoNominaRepository {
        protected internal ISqlComplmentoNominaParteRepository _Partes;

        public SqlSugarComplementoNominaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this._Partes = new SqlSugarComplementoNominaParteRepository(configuracion, user);
        }

        public override int Insert(ComplementoNominaModel complemento) {
            var sqlCommand = @"insert into _cfdnmn (_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tipo,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_usr_n,_cfdnmn_uuid,_cfdnmn_rfc)
                                            values (@cfdnmn_drctr_id,@cfdnmn_cfdi_id,@cfdnmn_tipo,@cfdnmn_tpreg,@cfdnmn_rsgpst,@cfdnmn_qnc,@cfdnmn_anio,@cfdnmn_antgdd,@cfdnmn_dspgds,@cfdnmn_bsctap,@cfdnmn_drintg,@cfdnmn_pttlgrv,@cfdnmn_pttlexn,@cfdnmn_dttlgrv,@cfdnmn_dttlexn,@cfdnmn_dscnt,@cfdnmn_hrsxtr,@cfdnmn_ver,@cfdnmn_banco,@cfdnmn_rgp,@cfdnmn_numem,@cfdnmn_curp,@cfdnmn_numsgr,@cfdnmn_clabe,@cfdnmn_depto,@cfdnmn_puesto,@cfdnmn_tpcntr,@cfdnmn_tpjrn,@cfdnmn_prddpg,@cfdnmn_fchpgo,@cfdnmn_fchini,@cfdnmn_fchfnl,@cfdnmn_fchrel,@cfdnmn_fn,@cfdnmn_usr_n,@cfdnmn_uuid,@cfdnmn_rfc); select last_insert_id();";
            var parameters = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@cfdnmn_tipo", complemento.TipoNomina),
                new SqlSugar.SugarParameter("@cfdnmn_drctr_id", complemento.ReceptorId),
                new SqlSugar.SugarParameter("@cfdnmn_cfdi_id", complemento.SubId),
                new SqlSugar.SugarParameter("@cfdnmn_tpreg", complemento.TipoRegimen),
                new SqlSugar.SugarParameter("@cfdnmn_rsgpst", complemento.RiesgoPuesto),
                new SqlSugar.SugarParameter("@cfdnmn_qnc", complemento.FechaPago.Value.Day > 15 ? 2 : 1),
                new SqlSugar.SugarParameter("@cfdnmn_anio", complemento.FechaPago.Value.Year),
                new SqlSugar.SugarParameter("@cfdnmn_antgdd", complemento.Antiguedad),
                new SqlSugar.SugarParameter("@cfdnmn_dspgds", complemento.NumDiasPagados),
                new SqlSugar.SugarParameter("@cfdnmn_bsctap", complemento.SalarioBaseCotApor),
                new SqlSugar.SugarParameter("@cfdnmn_drintg", complemento.SalarioDiarioIntegrado),
                new SqlSugar.SugarParameter("@cfdnmn_pttlgrv", complemento.PercepcionTotalGravado),
                new SqlSugar.SugarParameter("@cfdnmn_pttlexn", complemento.PercepcionTotalExento),
                new SqlSugar.SugarParameter("@cfdnmn_dttlgrv", complemento.DeduccionTotalGravado),
                new SqlSugar.SugarParameter("@cfdnmn_dttlexn", complemento.DeduccionTotalExento),
                new SqlSugar.SugarParameter("@cfdnmn_hrsxtr", 0),
                new SqlSugar.SugarParameter("@cfdnmn_ver", complemento.Version),
                new SqlSugar.SugarParameter("@cfdnmn_banco", complemento.Banco),
                new SqlSugar.SugarParameter("@cfdnmn_rgp", complemento.RegistroPatronal),
                new SqlSugar.SugarParameter("@cfdnmn_numem", complemento.NumEmpleado),
                new SqlSugar.SugarParameter("@cfdnmn_curp", complemento.CURP),
                new SqlSugar.SugarParameter("@cfdnmn_numsgr", complemento.NumSeguridadSocial),
                new SqlSugar.SugarParameter("@cfdnmn_clabe", complemento.CtaBanco),
                new SqlSugar.SugarParameter("@cfdnmn_depto", complemento.Departamento),
                new SqlSugar.SugarParameter("@cfdnmn_puesto", complemento.Puesto),
                new SqlSugar.SugarParameter("@cfdnmn_tpcntr", complemento.TipoContrato),
                new SqlSugar.SugarParameter("@cfdnmn_tpjrn", complemento.TipoJornada),
                new SqlSugar.SugarParameter("@cfdnmn_prddpg", complemento.PeriodicidadPago),
                new SqlSugar.SugarParameter("@cfdnmn_fchpgo", complemento.FechaPago),
                new SqlSugar.SugarParameter("@cfdnmn_fchini", complemento.FechaInicialPago),
                new SqlSugar.SugarParameter("@cfdnmn_fchfnl", complemento.FechaFinalPago),
                new SqlSugar.SugarParameter("@cfdnmn_fchrel", complemento.FechaInicioRelLaboral),
                new SqlSugar.SugarParameter("@cfdnmn_fn", DateTime.Now),
                new SqlSugar.SugarParameter("@cfdnmn_usr_n", complemento.Creo),
                new SqlSugar.SugarParameter("@cfdnmn_uuid", complemento.IdDocumento),
                new SqlSugar.SugarParameter("@cfdnmn_dscnt", complemento.Descuento),
                new SqlSugar.SugarParameter("@cfdnmn_rfc", complemento.RFC) 
            };
            return this.Db.Ado.ExecuteCommand(sqlCommand, parameters);
        }

        /// <summary>
        /// almacenar complemento de nomina
        /// </summary>
        public ComplementoNominaModel Save(ComplementoNominaModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                var result = this.Db.Insertable<ComplementoNominaModel>(model);
                model.Id = this.Execute(result);
            } else {
                var result = this.Db.Updateable<ComplementoNominaModel>(model);
                if (this.Execute(result) == 0) {
                    Services.LogErrorService.LogWrite("No se guardo");
                }
            }
            return model;
        }

        public IComprobanteFiscalNominaModel Save(IComprobanteFiscalNominaModel model) {
            throw new NotImplementedException();
        }

        public IComplementoNominaDetailModel Save(IComplementoNominaDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                var result = this.Db.Insertable<ComplementoNominaDetailModel>(model);
                model.Id = this.Execute(result);
            } else {
                var result = this.Db.Updateable<ComplementoNominaDetailModel>(model);
                if (this.Execute(result) == 0) {
                    Services.LogErrorService.LogWrite("No se guardo");
                }
            }

            if (model.Id > 0) {
                for (int i = 0; i < model.Partes.Count; i++) {
                    model.Partes[i].SubId = model.Id;
                    model.Partes[i] = this.Save(model.Partes[i]);
                }
            }
            return model;
        }

        public ComplementoNominaParteDetailModel Save(ComplementoNominaParteDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                var result = this.Db.Insertable<ComplementoNominaParteModel>(model);
                model.Id = this.Execute(result);
            } else {
                model.Modifica = this.User;
                model.FechaMod = DateTime.Now;
                var result = this.Db.Updateable<ComplementoNominaParteModel>(model);
                if (this.Execute(result) == 0) {
                    Services.LogErrorService.LogWrite("No se guardo");
                }
            }
            return model;
        }
    }
}
