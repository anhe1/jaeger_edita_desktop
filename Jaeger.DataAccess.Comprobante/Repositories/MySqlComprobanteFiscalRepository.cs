﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Services;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class MySqlComprobanteFiscalRepository : RepositoryMaster<ComprobanteFiscalModel>, ISqlComprobanteFiscalRepository {
        public MySqlComprobanteFiscalRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @"update _cfdi set _cfdi_a = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ComprobanteFiscalModel GetById(int index) {
            return this.GetComprobante(index);
        }

        public ComprobanteFiscalDetailModel GetComprobante(int index) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * from _cfdi where _cfdi_id = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalDetailModel>();
            var response = mapper.Map(tabla).FirstOrDefault();
            if (response != null)
                response.Conceptos = new BindingList<ComprobanteConceptoDetailModel>(this.GetConceptos(index).ToList());
            return response;
        }

        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetList(string[] uuids) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select * from _cfdi where _cfdi_a = 1 and _cfdi_uuid in (@array);"
            };
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@array", "'" + string.Join("','", uuids) + "'");

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalDetailSingleModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComprobanteConceptoView> GetList(int index) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * from _cfdcnp, _cfdi where _cfdcnp_a = 1 and _cfdcnp_cfds_id = _cfdi_id and _cfdcnp_pdd_id = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteConceptoView>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComprobanteFiscalModel> GetList() {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select * from _cfdi where _cfdi_a = 1 "
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// Realizar busqueda de un comprobante por el folio de control interno
        /// </summary>
        /// <param name="subTipo">subtipo del comprobante fiscal (Emitido, Recibido, Nomina)</param>
        /// <param name="rfc">RFC del emisor o receptor</param>
        /// <param name="folio">folio a buscar</param>
        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, string rfc, string folio) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * 
                                from _cfdi
                                where _cfdi_folio like '%@folio%' and _cfdi_doc_id = @subtipo @rfc'
                                order by _cfdi_id desc"
            };

            if (subTipo == CFDISubTipoEnum.Emitido) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "and _cfdi_rfcr = @rfc");
            } else if (subTipo == CFDISubTipoEnum.Emitido) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "and _cfdi_rfce = @rfc");
            }

            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            sqlCommand.Parameters.AddWithValue("@folio", folio);
            sqlCommand.Parameters.AddWithValue("@subtipo", (int)subTipo);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalDetailSingleModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// Realizar busqueda de un comprobante por el folio de control interno
        /// </summary>
        /// <param name="subTipo">subtipo del comprobante fiscal (Emitido, Recibido, Nomina)</param>
        /// <param name="rfc">RFC del emisor o receptor</param>
        /// <param name="status">status a buscar</param>
        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, string rfc, string[] status) {
            var s = status[0];
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * 
                                from _cfdi
                                where _cfdi_status like '%@status%' 
                                and _cfdi_doc_id = @subtipo 
                                @rfc
                                order by _cfdi_id desc"
            };

            if (subTipo == CFDISubTipoEnum.Emitido) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "and _cfdi_rfcr = @rfc");
            } else if (subTipo == CFDISubTipoEnum.Emitido) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "and _cfdi_rfce = @rfc");
            }

            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            sqlCommand.Parameters.AddWithValue("@status", s);
            sqlCommand.Parameters.AddWithValue("@subtipo", (int)subTipo);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalDetailSingleModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado simple de comprobantes fiscales
        /// </summary>
        /// <param name="year">año</param>
        /// <param name="month">mes</param>
        /// <param name="subTipo">sub tipo de comprobante</param>
        /// <returns>retorna listado de modelos</returns>
        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSingleModels(int year, int month, int subTipo) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * from _cfdi where _cfdi_doc_id = @subTipo and year(_cfdi_fecems)=@year and month(_cfdi_fecems)=@month"
            };
            sqlCommand.Parameters.AddWithValue("@subTipo", subTipo);
            sqlCommand.Parameters.AddWithValue("@year", year);
            sqlCommand.Parameters.AddWithValue("@month", month);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalDetailSingleModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSingles(CFDISubTipoEnum subTipo, string rfc) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * from _cfdi where _cfdi_doc_id = @subTipo @rfc"
            };

            if (subTipo == CFDISubTipoEnum.Emitido) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "and _cfdi_rfcr like @rfc");
            } else if (subTipo == CFDISubTipoEnum.Emitido) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "and _cfdi_rfce like @rfc");
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@rfc", "");
            }

            sqlCommand.Parameters.AddWithValue("@subTipo", subTipo);
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalDetailSingleModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(ComprobanteFiscalModel objeto) {
            var sqlInsert = new MySqlCommand() {
                CommandText = string.Concat("insert into _cfdi (_cfdi_a, _cfdi_doc_id, _cfdi_drctr_id, _cfdi_vence, _cfdi_sbttl, _cfdi_dscnt, _cfdi_trsiva, _cfdi_trsieps, _cfdi_retieps, _cfdi_retiva, _cfdi_retisr, _cfdi_total, _cfdi_cbrd, _cfdi_per, _cfdi_dec, _cfdi_status, _cfdi_usr_n, _cfdi_rfce, _cfdi_rfcr, _cfdi_pac, _cfdi_moneda, _cfdi_folio, _cfdi_cntpg, _cfdi_edita, _cfdi_nocert, _cfdi_serie, _cfdi_estado, _cfdi_rslt, _cfdi_uuid, _cfdi_doc, _cfdi_frmpg, _cfdi_desct, _cfdi_mtdpg, _cfdi_cndpg, _cfdi_nome, _cfdi_nomr, _cfdi_obsrv, _cfdi_val, _cfdi_fn, _cfdi_fecems, _cfdi_feccert, _cfdi_fecval, _cfdi_acuse, _cfdi_adenda, _cfdi_usocfdi, _cfdi_efecto, _cfdi_comrel, _cfdi_lgrexp, _cfdi_compl, _cfdi_ver,_cfdi_pagos, _cfdi_save, _cfdi_nomina) ",
                                                       "values (@cfdi_a, @cfdi_doc_id, @cfdi_drctr_id, @cfdi_vence, @cfdi_sbttl, @cfdi_dscnt, @cfdi_trsiva, @cfdi_trsieps, @cfdi_retieps, @cfdi_retiva, @cfdi_retisr, @cfdi_total, @cfdi_cbrd, @cfdi_per, @cfdi_dec, @cfdi_status, @cfdi_usr_n, @cfdi_rfce, @cfdi_rfcr, @cfdi_pac, @cfdi_moneda, @cfdi_folio, @cfdi_cntpg, @cfdi_edita, @cfdi_nocert, @cfdi_serie, @cfdi_estado, @cfdi_rslt, @cfdi_uuid, @cfdi_doc, @cfdi_frmpg, @cfdi_desct, @cfdi_mtdpg, @cfdi_cndpg, @cfdi_nome, @cfdi_nomr, @cfdi_obsrv, @cfdi_val, @cfdi_fn, @cfdi_fecems, @cfdi_feccert, @cfdi_fecval, @cfdi_acuse, @cfdi_adenda, @cfdi_usocfdi, @cfdi_efecto, @cfdi_comrel, @cfdi_lgrexp, @cfdi_compl, @cfdi_ver,@cfdi_pagos, @cfdi_save, @cfdi_nomina);")
            };
            if (objeto.IdDocumento != null) {
                sqlInsert.CommandText = string.Concat(sqlInsert.CommandText, "select _cfdi_id from _cfdi where _cfdi_uuid = @uuid;");
                sqlInsert.Parameters.AddWithValue("@uuid", objeto.IdDocumento);
            } else {
                objeto.Id = (int)this.GetIndex("_cfdi_id", "_cfdi");
                sqlInsert.CommandText = sqlInsert.CommandText.Replace("(_cfdi_a", "(_cfdi_id, _cfdi_a");
                sqlInsert.CommandText = sqlInsert.CommandText.Replace("(@cfdi_a", "(@cfdi_id, @cfdi_a");
                sqlInsert.Parameters.AddWithValue("@cfdi_id", objeto.Id);
            }
            if (objeto.SubTipoInt == (int)CFDISubTipoEnum.Emitido | objeto.SubTipoInt == (int)CFDISubTipoEnum.Nomina) {
                sqlInsert.Parameters.AddWithValue("@cfdi_drctr_id", objeto.IdDirectorio);
            } else {
                sqlInsert.Parameters.AddWithValue("@cfdi_drctr_id", objeto.IdDirectorio);
            }

            sqlInsert.Parameters.AddWithValue("@cfdi_a", 1);
            sqlInsert.Parameters.AddWithValue("@cfdi_doc_id", objeto.SubTipoInt);
            sqlInsert.Parameters.AddWithValue("@cfdi_vence", 0);
            sqlInsert.Parameters.AddWithValue("@cfdi_sbttl", objeto.SubTotal);
            sqlInsert.Parameters.AddWithValue("@cfdi_dscnt", objeto.Descuento);
            sqlInsert.Parameters.AddWithValue("@cfdi_retisr", objeto.RetencionISR);
            sqlInsert.Parameters.AddWithValue("@cfdi_retiva", objeto.RetencionIVA);
            sqlInsert.Parameters.AddWithValue("@cfdi_trsiva", objeto.TrasladoIVA);
            sqlInsert.Parameters.AddWithValue("@cfdi_retieps", objeto.RetencionIEPS);
            sqlInsert.Parameters.AddWithValue("@cfdi_trsieps", objeto.TrasladoIEPS);
            sqlInsert.Parameters.AddWithValue("@cfdi_total", objeto.Total);
            sqlInsert.Parameters.AddWithValue("@cfdi_cbrd", objeto.Acumulado);
            sqlInsert.Parameters.AddWithValue("@cfdi_per", objeto.TotalPecepcion);
            sqlInsert.Parameters.AddWithValue("@cfdi_dec", objeto.TotalDeduccion);
            sqlInsert.Parameters.AddWithValue("@cfdi_status", objeto.Status);
            sqlInsert.Parameters.AddWithValue("@cfdi_efecto", objeto.TipoComprobanteText);
            sqlInsert.Parameters.AddWithValue("@cfdi_ver", objeto.Version);
            sqlInsert.Parameters.AddWithValue("@cfdi_usr_n", objeto.Creo);
            sqlInsert.Parameters.AddWithValue("@cfdi_rfce", objeto.EmisorRFC);
            sqlInsert.Parameters.AddWithValue("@cfdi_rfcr", objeto.ReceptorRFC);
            sqlInsert.Parameters.AddWithValue("@cfdi_moneda", objeto.ClaveMoneda);
            sqlInsert.Parameters.AddWithValue("@cfdi_cntpg", objeto.CuentaPago);
            sqlInsert.Parameters.AddWithValue("@cfdi_edita", "");
            sqlInsert.Parameters.AddWithValue("@cfdi_nocert", objeto.NoCertificado);
            sqlInsert.Parameters.AddWithValue("@cfdi_serie", objeto.Serie);
            sqlInsert.Parameters.AddWithValue("@cfdi_rslt", objeto.Result);
            sqlInsert.Parameters.AddWithValue("@cfdi_doc", "");
            sqlInsert.Parameters.AddWithValue("@cfdi_frmpg", objeto.ClaveFormaPago);
            sqlInsert.Parameters.AddWithValue("@cfdi_desct", objeto.MotivoDescuento);
            sqlInsert.Parameters.AddWithValue("@cfdi_mtdpg", objeto.ClaveMetodoPago);
            sqlInsert.Parameters.AddWithValue("@cfdi_cndpg", objeto.CondicionPago);
            sqlInsert.Parameters.AddWithValue("@cfdi_nome", objeto.EmisorNombre);
            sqlInsert.Parameters.AddWithValue("@cfdi_nomr", objeto.ReceptorNombre);
            sqlInsert.Parameters.AddWithValue("@cfdi_obsrv", objeto.Notas);
            sqlInsert.Parameters.AddWithValue("@cfdi_fn", DateTime.Now);
            sqlInsert.Parameters.AddWithValue("@cfdi_fecems", objeto.FechaEmision);
            sqlInsert.Parameters.AddWithValue("@cfdi_usocfdi", objeto.ClaveUsoCFDI);
            sqlInsert.Parameters.AddWithValue("@cfdi_lgrexp", objeto.LugarExpedicion);
            sqlInsert.Parameters.AddWithValue("@cfdi_save", objeto.XML);
            sqlInsert.Parameters.AddWithValue("@cfdi_folio", objeto.Folio);

            // cfdi's relacionados
            if (objeto.JCfdiRelacionados == null)
                sqlInsert.Parameters.AddWithValue("@cfdi_comrel", DBNull.Value);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_comrel", objeto.JCfdiRelacionados);

            // informacion del timbre fiscal
            if (objeto.IdDocumento == null) {
                sqlInsert.Parameters.AddWithValue("@cfdi_uuid", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_feccert", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_pac", DBNull.Value);
            } else {
                sqlInsert.Parameters.AddWithValue("@cfdi_uuid", objeto.IdDocumento.ToString().ToUpper());
                sqlInsert.Parameters.AddWithValue("@cfdi_feccert", objeto.FechaTimbre);
                sqlInsert.Parameters.AddWithValue("@cfdi_pac", objeto.RFCProvCertif);
            }

            // objeto de validacion del comprobante
            if (objeto.JValidacion != null) {
                sqlInsert.Parameters.AddWithValue("@cfdi_val", objeto.JValidacion);
                sqlInsert.Parameters.AddWithValue("@cfdi_fecval", objeto.FechaValidacion);
                sqlInsert.Parameters.AddWithValue("@cfdi_estado", objeto.Estado);
            } else {
                sqlInsert.Parameters.AddWithValue("@cfdi_val", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_fecval", DBNull.Value);
                sqlInsert.Parameters.AddWithValue("@cfdi_estado", DBNull.Value);
            }

            // informacion del acuse de cancelacion
            if (objeto.JAccuse != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_acuse", objeto.JAccuse);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_acuse", DBNull.Value);

            // informacion de addendas
            if (objeto.JAddenda != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_adenda", objeto.JAddenda);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_adenda", DBNull.Value);

            // informacion de addendas
            if (objeto.JComplementos != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_compl", objeto.JComplementos);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_compl", DBNull.Value);

            // complemento de pagos
            if (objeto.JComplementoPagos != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_pagos", objeto.JComplementoPagos);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_pagos", DBNull.Value);

            // complemento de nomina
            if (objeto.JComplementoNomina != null)
                sqlInsert.Parameters.AddWithValue("@cfdi_nomina", objeto.JComplementoNomina);
            else
                sqlInsert.Parameters.AddWithValue("@cfdi_nomina", DBNull.Value);

            // ejecucion de la consulta
            objeto.Id = (int)this.ExecuteScalar(sqlInsert);

            return objeto.Id;
        }

        public ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel comprobante) {
            // insertar el comprobante
            if (comprobante.Id == 0) {
                // si es un comprobante emitido creamos el nuevo folio a partir de la serie
                if (comprobante.SubTipo == CFDISubTipoEnum.Emitido)
                    comprobante.Folio = this.GetFolio(comprobante.Emisor.RFC, comprobante.Serie).ToString();
                comprobante.FechaNuevo = DateTime.Now;
                comprobante.Id = this.Insert(comprobante);
            } else {
                if (this.Update(comprobante) == 0)
                    return comprobante;
            }

            // almacenar los conceptos
            if (comprobante.Conceptos != null) {
                for (int i = 0; i < comprobante.Conceptos.Count; i++) {
                    if (comprobante.Conceptos[i].Id == 0) {
                        comprobante.Conceptos[i].SubId = comprobante.Id;
                        comprobante.Conceptos[i].FechaNuevo = DateTime.Now;
                        comprobante.Conceptos[i].Creo = comprobante.Creo;
                        comprobante.Conceptos[i].Id = this.Insert(comprobante.Conceptos[i]);
                    } else {
                        this.Update(comprobante.Conceptos[i]);
                    }
                }
            }

            if (comprobante.TimbreFiscal != null) {
                // si contiene un complemento de pagos
                if (comprobante.ComplementoPagos != null)
                    this.AplicarComprobantePagos(comprobante.ComplementoPagos);

                // cfdi relacionados
                if (comprobante.CfdiRelacionados != null)
                    if (comprobante.CfdiRelacionados.CfdiRelacionado != null)
                        this.AplicarCFDIRelacionados(comprobante.CfdiRelacionados);
            }

            //if (comprobante.Nomina != null) {

            //}

            return comprobante;
        }

        public int Update(ComprobanteFiscalModel item) {
            MySqlCommand sqlCommand = new MySqlCommand() {
                CommandText = String.Concat("update _cfdi set ",
                                            "_cfdi_a=@cfdi_a, _cfdi_doc_id=@cfdi_doc_id, _cfdi_vence=@cfdi_vence,_cfdi_sbttl=@cfdi_sbttl,_cfdi_dscnt=@cfdi_dscnt,_cfdi_retieps=@cfdi_retieps,_cfdi_trsiva=@cfdi_trsiva,_cfdi_trsieps=@cfdi_trsieps,_cfdi_retiva=@cfdi_retiva,_cfdi_retisr=@cfdi_retisr,_cfdi_total=@cfdi_total,_cfdi_cbrd=@cfdi_cbrd,_cfdi_per=@cfdi_per,_cfdi_dec=@cfdi_dec,_cfdi_status=@cfdi_status,_cfdi_efecto=@cfdi_efecto,_cfdi_usr_m=@cfdi_usr_m,_cfdi_rfce=@cfdi_rfce,_cfdi_rfcr=@cfdi_rfcr,_cfdi_pac=@cfdi_pac,_cfdi_moneda=@cfdi_moneda,_cfdi_folio=@cfdi_folio,_cfdi_cntpg=@cfdi_cntpg,_cfdi_edita=@cfdi_edita,_cfdi_nocert=@cfdi_nocert,_cfdi_serie=@cfdi_serie,_cfdi_estado=@cfdi_estado,_cfdi_rslt=@cfdi_rslt,_cfdi_uuid=@cfdi_uuid,_cfdi_doc=@cfdi_doc,_cfdi_frmpg=@cfdi_frmpg,_cfdi_desct=@cfdi_desct,_cfdi_mtdpg=@cfdi_mtdpg,_cfdi_cndpg=@cfdi_cndpg,_cfdi_nome=@cfdi_nome,_cfdi_nomr=@cfdi_nomr,_cfdi_obsrv=@cfdi_obsrv,_cfdi_val=@cfdi_val,_cfdi_fm=@cfdi_fm,_cfdi_fecems=@cfdi_fecems,_cfdi_feccert=@cfdi_feccert,_cfdi_fecval=@cfdi_fecval,_cfdi_acuse=@cfdi_acuse,_cfdi_adenda=@cfdi_adenda,_cfdi_usocfdi=@cfdi_usocfdi, _cfdi_comrel=@cfdi_comrel, _cfdi_lgrexp=@cfdi_lgrexp, _cfdi_compl=@cfdi_compl, _cfdi_ver=@cfdi_ver, _cfdi_url_xml=@cfdi_url_xml, _cfdi_url_pdf=@cfdi_url_pdf, ",
                                            "_cfdi_pagos=@cfdi_pagos, _cfdi_save=@cfdi_save ",
                                            "where _cfdi_id = @index")
            };

            if (item.IdDocumento == null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("_cfdi_uuid=@cfdi_uuid,", "");
            }

            sqlCommand.Parameters.AddWithValue("@index", item.Id);
            sqlCommand.Parameters.AddWithValue("@cfdi_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cfdi_doc_id", item.SubTipoInt);
            sqlCommand.Parameters.AddWithValue("@cfdi_vence", 0);
            sqlCommand.Parameters.AddWithValue("@cfdi_sbttl", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@cfdi_dscnt", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@cfdi_retisr", item.RetencionISR);
            sqlCommand.Parameters.AddWithValue("@cfdi_retiva", item.RetencionIVA);
            sqlCommand.Parameters.AddWithValue("@cfdi_trsiva", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@cfdi_retieps", item.RetencionIEPS);
            sqlCommand.Parameters.AddWithValue("@cfdi_trsieps", item.TrasladoIEPS);
            sqlCommand.Parameters.AddWithValue("@cfdi_total", item.Total);
            sqlCommand.Parameters.AddWithValue("@cfdi_cbrd", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@cfdi_per", item.TotalPecepcion);
            sqlCommand.Parameters.AddWithValue("@cfdi_dec", item.TotalDeduccion);
            sqlCommand.Parameters.AddWithValue("@cfdi_status", item.Status);
            sqlCommand.Parameters.AddWithValue("@cfdi_efecto", item.TipoComprobanteText);
            sqlCommand.Parameters.AddWithValue("@cfdi_ver", item.Version);
            sqlCommand.Parameters.AddWithValue("@cfdi_usr_m", item.Creo);
            sqlCommand.Parameters.AddWithValue("@cfdi_rfce", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@cfdi_rfcr", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@cfdi_moneda", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@cfdi_folio", item.Folio);
            sqlCommand.Parameters.AddWithValue("@cfdi_cntpg", item.CuentaPago);
            sqlCommand.Parameters.AddWithValue("@cfdi_edita", "");
            sqlCommand.Parameters.AddWithValue("@cfdi_nocert", item.NoCertificado);
            sqlCommand.Parameters.AddWithValue("@cfdi_serie", item.Serie);
            sqlCommand.Parameters.AddWithValue("@cfdi_estado", item.Estado);
            sqlCommand.Parameters.AddWithValue("@cfdi_rslt", item.Result);
            sqlCommand.Parameters.AddWithValue("@cfdi_doc", "");
            sqlCommand.Parameters.AddWithValue("@cfdi_frmpg", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@cfdi_desct", item.MotivoDescuento);
            sqlCommand.Parameters.AddWithValue("@cfdi_mtdpg", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@cfdi_cndpg", item.CondicionPago);
            sqlCommand.Parameters.AddWithValue("@cfdi_nome", item.EmisorNombre);
            sqlCommand.Parameters.AddWithValue("@cfdi_nomr", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@cfdi_obsrv", item.Notas);
            sqlCommand.Parameters.AddWithValue("@cfdi_fm", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@cfdi_fecems", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@cfdi_fecval", item.FechaValidacion);
            sqlCommand.Parameters.AddWithValue("@cfdi_usocfdi", item.ClaveUsoCFDI);
            sqlCommand.Parameters.AddWithValue("@cfdi_lgrexp", item.LugarExpedicion);
            sqlCommand.Parameters.AddWithValue("@cfdi_url_xml", item.UrlFileXML);
            sqlCommand.Parameters.AddWithValue("@cfdi_url_pdf", item.UrlFilePDF);
            sqlCommand.Parameters.AddWithValue("@cfdi_save", item.XML);

            if (item.IdDocumento == null) {
                sqlCommand.Parameters.AddWithValue("@cfdi_uuid", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@cfdi_pac", DBNull.Value);
                sqlCommand.Parameters.AddWithValue("@cfdi_feccert", DBNull.Value);
            } else {
                sqlCommand.Parameters.AddWithValue("@cfdi_uuid", item.IdDocumento);
                sqlCommand.Parameters.AddWithValue("@cfdi_pac", item.RFCProvCertif);
                sqlCommand.Parameters.AddWithValue("@cfdi_feccert", item.FechaTimbre);
            }

            if (item.JCfdiRelacionados == null)
                sqlCommand.Parameters.AddWithValue("@cfdi_comrel", DBNull.Value);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_comrel", item.JCfdiRelacionados);

            if (!(item.SubTipoInt == (int)CFDISubTipoEnum.Emitido | item.SubTipoInt == (int)CFDISubTipoEnum.Nomina))
                sqlCommand.Parameters.AddWithValue("@cfdi_drctr_id", item.IdDirectorio);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_drctr_id", item.IdDirectorio);

            if (item.JAddenda == null)
                sqlCommand.Parameters.AddWithValue("@cfdi_adenda", DBNull.Value);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_adenda", item.JAddenda);

            if (item.JAccuse == null)
                sqlCommand.Parameters.AddWithValue("@cfdi_acuse", DBNull.Value);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_acuse", item.JAccuse);

            if (item.JValidacion == null)
                sqlCommand.Parameters.AddWithValue("@cfdi_val", DBNull.Value);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_val", item.JValidacion);

            if (item.JComplementos == null)
                sqlCommand.Parameters.AddWithValue("@cfdi_compl", DBNull.Value);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_compl", item.JComplementos);

            // complemento de pagos
            if (item.JComplementoPagos != null)
                sqlCommand.Parameters.AddWithValue("@cfdi_pagos", item.JComplementoPagos);
            else
                sqlCommand.Parameters.AddWithValue("@cfdi_pagos", DBNull.Value);
            return this.ExecuteTransaction(sqlCommand);
        }

        /// <summary>
        /// metodo para actualizar el status del comprobante fiscal
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="status">nuevo status</param>
        /// <param name="usuario">clave del usuario</param>
        public bool Update(int indice, string status, string usuario) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_status = @status, _cfdi_fecent = @fecha1, _cfdi_fm = @fecha2, _cfdi_usr_m = @modifica where _cfdi_id = @index;" };
            sqlCommand.Parameters.AddWithValue("@index", indice);
            sqlCommand.Parameters.AddWithValue("@status", status);
            sqlCommand.Parameters.AddWithValue("@fecha1", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@fecha2", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@modifica", usuario);
            if (this.ExecuteTransaction(sqlCommand) > 0) {
                Console.WriteLine("No se actualizo el estado del comprobante.");
                return false;
            }
            return true;
        }

        public bool UpdateEstado(string idDocumento, string estado, string usuario) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_estado=@estado, _cfdi_fecedo=@fecha where _cfdi_uuid=@index;" };
            sqlCommand.Parameters.AddWithValue("@estado", estado);
            sqlCommand.Parameters.AddWithValue("@fecha", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@index", idDocumento);
            if (this.ExecuteTransaction(sqlCommand) > 0) {
                Service.LogErrorService.LogWrite("No se actualizo el estado del comprobante.");
                return false;
            }
            return true;
        }

        /// <summary>
        /// actualizar url de la representacion impresa del comprobante fiscal (pfd)
        /// </summary>
        /// <param name="indice">indice</param>
        /// <param name="url">url</param>
        /// <returns></returns>
        public bool UpdateUrlPdf(int index, string url) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "update _cfdi set _cfdi_url_pdf=@url where _cfdi_id=@index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            sqlCommand.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlPdfAcuse(int index, string url) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "update _cfdi set _cfdi_url_pdfacu=@url where _cfdi_id=@index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            sqlCommand.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public bool UpdateUrlXml(int index, string url) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_url_xml=@url where _cfdi_id=@index;" };
            sqlCommand.Parameters.AddWithValue("@index", index);
            sqlCommand.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public bool UpdateUrlXmlAcuse(int index, string url) {
            var sqlCommand = new MySqlCommand() { CommandText = "update _cfdi set _cfdi_url_xmlacu=@url where _cfdi_id=@index;" };
            sqlCommand.Parameters.AddWithValue("@index", index);
            sqlCommand.Parameters.AddWithValue("@url", url);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        #region conceptos
        /// <summary>
        /// insertar un objeto nuevo concepto en la base de datos
        /// </summary>
        public int Insert(ComprobanteConceptoDetailModel concepto) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @"insert into _cfdcnp (_cfdcnp_a,_cfdcnp_cfds_id,_cfdcnp_undd,_cfdcnp_noidnt,_cfdcnp_cncpt,_cfdcnp_cntdd,_cfdcnp_untr,_cfdcnp_sbttl,_cfdcnp_usr_n,_cfdcnp_fn,_cfdcnp_ctapre,_cfdcnp_clvprds,_cfdcnp_clvund,_cfdcnp_dscnt, _cfdcnp_parte, _cfdcnp_impto, _cfdcnp_pdd_id) 
                                             values (@cfdcnp_a,@cfdcnp_cfds_id,@cfdcnp_undd,@cfdcnp_noidnt,@cfdcnp_cncpt,@cfdcnp_cntdd,@cfdcnp_untr,@cfdcnp_sbttl,@cfdcnp_usr_n,@cfdcnp_fn,@cfdcnp_ctapre,@cfdcnp_clvprds,@cfdcnp_clvund,@cfdcnp_dscnt, @cfdcnp_parte, @cfdcnp_impto, @cfdcnp_pdd_id); 
                                            select last_insert_id();"
            };

            sqlCommand.Parameters.AddWithValue("@index", concepto.Id);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_a", 1);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_cfds_id", concepto.SubId);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_cntdd", concepto.Cantidad);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_cncpt", concepto.Descripcion);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_sbttl", concepto.Importe);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_noidnt", concepto.NoIdentificacion);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_undd", concepto.Unidad);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_untr", concepto.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_usr_n", concepto.Creo);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_fn", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_ctapre", concepto.CtaPredial);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_clvprds", concepto.ClaveProdServ);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_clvund", concepto.ClaveUnidad);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_dscnt", concepto.Descuento);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_parte", concepto.JParte);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_impto", concepto.JImpuestos);
            sqlCommand.Parameters.AddWithValue("@cfdcnp_pdd_id", concepto.NumPedido);
            concepto.Id = this.ExecuteScalar(sqlCommand);
            return concepto.Id;
        }

        /// <summary>
        /// actualizar un objeto concepto en la base de datos
        /// </summary>
        public bool Update(ComprobanteConceptoDetailModel concepto) {
            var sqlUpdate = new MySqlCommand {
                CommandText = @"update _cfdcnp set _cfdcnp_a=@cfdcnp_a,_cfdcnp_cfds_id=@cfdcnp_cfds_id,_cfdcnp_undd=@cfdcnp_undd,_cfdcnp_noidnt=@cfdcnp_noidnt,_cfdcnp_cncpt=@cfdcnp_cncpt,_cfdcnp_cntdd=@cfdcnp_cntdd,_cfdcnp_untr=@cfdcnp_untr,_cfdcnp_sbttl=@cfdcnp_sbttl,_cfdcnp_usr_m=@cfdcnp_usr_m,_cfdcnp_fm=@cfdcnp_fm,_cfdcnp_ctapre=@cfdcnp_ctapre,_cfdcnp_clvprds=@cfdcnp_clvprds,_cfdcnp_clvund=@cfdcnp_clvund,_cfdcnp_dscnt=@cfdcnp_dscnt, _cfdcnp_parte=@cfdcnp_parte, _cfdcnp_impto=@cfdcnp_impto, _cfdcnp_pdd_id=@cfdcnp_pdd_id 
                                where _cfdcnp_id=@index"
            };

            sqlUpdate.Parameters.AddWithValue("@index", concepto.Id);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_a", concepto.Activo);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_cfds_id", concepto.SubId);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_cntdd", concepto.Cantidad);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_cncpt", concepto.Descripcion);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_sbttl", concepto.Importe);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_noidnt", concepto.NoIdentificacion);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_undd", concepto.Unidad);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_untr", concepto.ValorUnitario);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_usr_m", concepto.Modifica);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_fm", DateTime.Now);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_ctapre", concepto.CtaPredial);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_clvprds", concepto.ClaveProdServ);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_clvund", concepto.ClaveUnidad);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_dscnt", concepto.Descuento);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_parte", concepto.JParte);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_impto", concepto.JImpuestos);
            sqlUpdate.Parameters.AddWithValue("@cfdcnp_pdd_id", concepto.NumPedido);
            return this.ExecuteTransaction(sqlUpdate) > 0;
        }

        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        public IEnumerable<ComprobanteConceptoDetailModel> GetConceptos(int index) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"SELECT `_cfdcnp_id`,`_cfdcnp_cfds_id`,`_cfdcnp_a`,`_cfdcnp_pdd_id`,`_cfdcnp_clvprds`,`_cfdcnp_noidnt`,`_cfdcnp_cntdd`,`_cfdcnp_clvund`,`_cfdcnp_undd`,`_cfdcnp_cncpt`,`_cfdcnp_untr`,`_cfdcnp_sbttl`,`_cfdcnp_dscnt`,`_cfdcnp_ctapre`,`_cfdcnp_impt1`,`_cfdcnp_imts1`,`_cfdcnp_imtvl1`,`_cfdcnp_impt2`,`_cfdcnp_imtvl2`,`_cfdcnp_imts2`,`_cfdcnp_retn1`,`_cfdcnp_retvl1`,`_cfdcnp_retn2`,`_cfdcnp_retvl2`,`_cfdcnp_retn3`,`_cfdcnp_retvl3`,`_cfdcnp_usr_n`,`_cfdcnp_usr_m`,`_cfdcnp_fn`,`_cfdcnp_fm`,`_cfdcnp_impto`,`_cfdcnp_parte` 
                                FROM `_cfdcnp`  
                                WHERE _cfdcnp_cfds_id IN (@index)"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteConceptoDetailModel>();
            return mapper.Map(tabla).ToList();
        }
        #endregion

        #region metodos privados
        /// <summary>
        /// actualizar objeto Complemento de pagos, con las partidas de los comprobantes relacionados
        /// </summary>
        public bool AplicarComprobantePagos(ComplementoPagos objeto) {
            if (objeto != null) {
                if (objeto.Pago != null) {
                    // recorremos la lista de objetos de pago
                    foreach (ComplementoPagosPago pago in objeto.Pago) {
                        // recorremos la lista de documentos relacionados
                        if (pago.DoctoRelacionado != null) {
                            foreach (ComplementoPagoDoctoRelacionado docto in pago.DoctoRelacionado) {
                                var tabla = this.Rep(docto.IdDocumento);
                                decimal suma = docto.ImpPagado;
                                if (tabla != null) {
                                    if (tabla.Count > 0) {
                                        suma = tabla.Where(p => ValidacionService.UUID(p.IdDocumento)).Sum(p => p.ImpPagado);
                                    }
                                }
                                //this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel() { Acumulado = suma, NumParcialidad = docto.NumParcialidad, FechaRecepcionPago = pago.FechaPago }).Where(it => it.IdDocumento == docto.IdDocumento).ExecuteCommand();
                                var sqlCommand = new MySqlCommand {
                                    CommandText = @"update _cfdi set _cfdi_cbrd = @suma, _cfdi_par = @parcialidad, _cfdi_fecpgr = @fechapago where _cfdi_uuid = @uuid;"
                                };

                                sqlCommand.Parameters.AddWithValue("@suma", suma);
                                sqlCommand.Parameters.AddWithValue("@parcialidad", docto.NumParcialidad);
                                sqlCommand.Parameters.AddWithValue("@fechapago", pago.FechaPago);
                                sqlCommand.Parameters.AddWithValue("@uuid", docto.IdDocumento);
                                if (this.ExecuteTransaction(sqlCommand) == 0)
                                    Console.WriteLine("No se actualizo la aplicacion del comprobante de pagos.");
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// obtener lista de comprobantes fiscales relacionados (Recibo Electronico de Pago)
        /// </summary>
        public List<ComplementoPagoDoctoRelacionado> Rep(string idDocumento) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * from _cfdi where _cfdi_pagos like %@buscar%"
            };

            sqlCommand.Parameters.AddWithValue("@buscar", idDocumento);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComprobanteFiscalModel>();
            var lista = mapper.Map(tabla).ToList();
            var lista1 = new List<ComplementoPagoDoctoRelacionado>();

            foreach (ComprobanteFiscalModel item in lista) {
                var _complemento = ComplementoPagos.Json(item.JComplementoPagos);
                if (_complemento != null) {
                    foreach (ComplementoPagosPago pagosPago in _complemento.Pago) {
                        List<ComplementoPagoDoctoRelacionado> doctos = pagosPago.DoctoRelacionado.Where(p => p.IdDocumento == idDocumento).ToList();
                        if (doctos != null) {
                            foreach (ComplementoPagoDoctoRelacionado docto in doctos) {
                                lista1.Add(docto);
                            }
                        }
                    }
                }
            }
            return lista1;
        }

        /// <summary>
        /// aplicar reglas a los cfdi relacionados del comprobante fiscal
        /// </summary>
        public bool AplicarCFDIRelacionados(ComprobanteCfdiRelacionados objeto) {
            if (objeto != null) {
                // para la clave de notas de credito relacionadas
                if (objeto.TipoRelacion.Clave == "01" | objeto.TipoRelacion.Clave == "02" | objeto.TipoRelacion.Clave == "03") {
                    foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado) {
                        decimal suma = 0;
                        List<ComplementoDoctoRelacionado> notas = this.Notas(item.IdDocumento);
                        if (notas != null) {
                            if (notas.Count > 0) {
                                suma = notas.Where(p => ValidacionService.UUID(p.IdDocumento)).Sum(p => p.Total);
                            }
                        }
                        //Entities.Base.ComprobanteGeneral d = this.GetComprobanteGeneral(item.IdDocumento);
                        var sqlCommand = new MySqlCommand {
                            CommandText = "update _cfdi set _cfdi_dscntc=@descuento where _cfdi_uuid=@index"
                        };

                        sqlCommand.Parameters.AddWithValue("@descuento", suma);
                        sqlCommand.Parameters.AddWithValue("@@index", item.IdDocumento);
                        return this.ExecuteScalar(sqlCommand) > 0;

                    }
                }
            }
            return false;
        }

        /// <summary>
        /// obtener notas de credito relacionados al comprobante fiscal
        /// </summary>
        public List<ComplementoDoctoRelacionado> Notas(string folioFiscal) {
            if (ValidacionService.UUID(folioFiscal)) {
                var sqlCommand = new MySqlCommand {
                    CommandText = @"select _cfdi_doc_id, _cfdi_estado,_cfdi_folio, _cfdi_serie, _cfdi_uuid,_cfdi_rfce,_cfdi_rfcr,_cfdi_nome,_cfdi_nomr, _cfdi_fecems, _cfdi_comrel, _cfdi_total, _cfdi_moneda,_cfdi_frmpg,_cfdi_mtdpg,_cfdi_par,_cfdi_estado,(_cfdi_total - (_cfdi_cbrd + _cfdi_dscntc)) as _cfdi_saldo 
                                    from _cfdi where _cfdi_comrel like '%buscar%'"
                };
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("buscar", folioFiscal);

                var tabla = this.ExecuteReader(sqlCommand);
                List<ComplementoDoctoRelacionado> lista = new List<ComplementoDoctoRelacionado>();
                foreach (System.Data.DataRow fila in tabla.Rows) {
                    ComprobanteCfdiRelacionados t = ComprobanteCfdiRelacionados.Json(DbConvert.ConvertString(fila["_cfdi_comrel"]));
                    if (t != null) {
                        ComplementoDoctoRelacionado docto = new ComplementoDoctoRelacionado();
                        docto.TipoRelacion.Clave = t.TipoRelacion.Clave;
                        docto.TipoRelacion.Descripcion = t.TipoRelacion.Descripcion;
                        docto.SubTipoText = DbConvert.ConvertString(fila["_cfdi_doc_id"]);
                        docto.Folio = DbConvert.ConvertString(fila["_cfdi_folio"]);
                        docto.Serie = DbConvert.ConvertString(fila["_cfdi_serie"]);
                        docto.IdDocumento = DbConvert.ConvertString(fila["_cfdi_uuid"]);
                        docto.FechaEmision = DbConvert.ConvertDateTime(fila["_cfdi_fecems"]);
                        docto.Total = DbConvert.ConvertDecimal(fila["_cfdi_total"]);
                        docto.EstadoSAT = DbConvert.ConvertString(fila["_cfdi_estado"]);
                        docto.Moneda = DbConvert.ConvertString(fila["_cfdi_moneda"]);
                        docto.FormaPago = DbConvert.ConvertString(fila["_cfdi_frmpg"]);
                        docto.MetodoPago = DbConvert.ConvertString(fila["_cfdi_mtdpg"]);
                        docto.EstadoSAT = DbConvert.ConvertString(fila["_cfdi_estado"]);
                        if (docto.SubTipo == CFDISubTipoEnum.Emitido) {
                            docto.RFC = DbConvert.ConvertString(fila["_cfdi_rfcr"]);
                            docto.Nombre = DbConvert.ConvertString(fila["_cfdi_nomr"]);
                        } else if (docto.SubTipo == CFDISubTipoEnum.Recibido) {
                            docto.RFC = DbConvert.ConvertString(fila["_cfdi_rfce"]);
                            docto.Nombre = DbConvert.ConvertString(fila["_cfdi_nome"]);
                        }
                        lista.Add(docto);
                    }
                }
                return lista;
            }
            return null;
        }

        /// <summary>
        /// obtener nuevo folio del comprobante emitido a partir del rfc del emisor y el nombre de la serie
        /// </summary>
        public string GetFolio(string rfc, string serie) {
            MySqlCommand sqlCommand = new MySqlCommand() {
                CommandText = "select (Max(cast(_cfdi._cfdi_folio as UNSIGNED)) + 1) as folio from _cfdi where _cfdi._cfdi_rfce = @rfc and _cfdi._cfdi_serie = @serie;"
            };

            sqlCommand.Parameters.AddWithValue("@serie", serie);
            sqlCommand.Parameters.AddWithValue("@rfc", rfc);
            long folio = this.ExecuteScalar(sqlCommand);
            if (folio == 0) {
                folio = 1;
            }
            return folio.ToString();
        }
        #endregion
    }
}
