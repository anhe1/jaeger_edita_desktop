﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarContribuyenteRepository : MySqlSugarContext<ComprobanteContribuyenteModel>, ISqlContribuyenteRepository {
        public SqlSugarContribuyenteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// obtener indice del del contribuyente por el registro federal de contribuyentes
        /// </summary>
        public int ReturnId(string rfc) {
            int indice = this.Db.Queryable<ComprobanteContribuyenteModel>().Where(it => it.RFC == rfc.ToUpper().Trim()).Select(it => it.IdDirectorio).First();
            return indice;
        }

        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        /// <param name="relacion">relacion con el directorio</param>
        public IEnumerable<ComprobanteContribuyenteModel> GetList(string relacion) {
            return this.Db.Queryable<ComprobanteContribuyenteModel>().Where(it => it.Relacion.Contains(relacion))
                .Where(it => it.Activo == true)
                .OrderBy(it => it.Nombre, OrderByType.Asc).ToList();
        }

        public ComprobanteContribuyenteModel Save(ComprobanteContribuyenteModel model) {
            model.IdDirectorio = this.ReturnId(model.RFC);
            if (model.IdDirectorio == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                var result = this.Db.Insertable(model);
                model.IdDirectorio = this.Execute(result);
            } else {
                var result = this.Db.Updateable<ComprobanteContribuyenteModel>(model);
                this.Execute(result);
            }
            return model;
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var condiciones = new List<IConditionalModel>();
            foreach (var item in conditionals) {
                condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
            }

            if (typeof(T1) == typeof(ComprobanteContribuyenteModel)) {
                var relacion = conditionals[0].FieldValue;
                var d0 = this.Db.Queryable<ComprobanteContribuyenteModel>().Where(it => it.Relacion.Contains(relacion))
                .Where(it => it.Activo == true)
                .OrderBy(it => it.Nombre, OrderByType.Asc).ToList();
                return new List<T1>(d0 as List<T1>);
            }
            return this.Db.Queryable<T1>().Where(condiciones).ToList();
        }
    }
}
