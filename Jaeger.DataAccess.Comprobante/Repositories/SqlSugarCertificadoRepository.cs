﻿using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    /// <summary>
    /// repositorio de certificados
    /// </summary>
    public class SqlSugarCertificadoRepository : SqlSugarContext<CertificadoModel>, ISqlCertificadoRepository {
        public SqlSugarCertificadoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {

        }

        /// <summary>
        /// obtener certificado activo en el sistema
        /// </summary>
        public CertificadoModel GetCertificado() {
            var response = this.Db.Queryable<CertificadoModel>().Where(it => it.Activo == true).OrderBy(it => it.Id, OrderByType.Desc).Single();
            int startIndex = response.CerB64.IndexOf("-----BEGIN CERTIFICATE-----");
            int endIndex = response.CerB64.IndexOf("-----END CERTIFICATE-----");
            string cerB64 = response.CerB64.Substring(startIndex + 27, endIndex - startIndex - 27).Trim();
            // quitamos etiquetas
            string keyB64 = response.KeyB64;
            keyB64 = keyB64.Replace("-----BEGIN PRIVATE KEY-----", "");
            keyB64 = keyB64.Replace("-----END PRIVATE KEY-----", "");
            keyB64 = keyB64.Replace("-----BEGIN RSA PRIVATE KEY-----", "");
            keyB64 = keyB64.Replace("-----END RSA PRIVATE KEY-----", "");

            response.CerB64 = cerB64;
            response.KeyB64 = keyB64;
            return response;
        }
    }
}
