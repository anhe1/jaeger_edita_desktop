﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Data;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComprobanteFiscalRepository : MySqlSugarContext<ComprobanteFiscalModel>, ISqlComprobanteFiscalRepository {
        protected ISqlComprobanteFiscalConceptoRepository conceptoRepository;

        public SqlSugarComprobanteFiscalRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.conceptoRepository = new SqlSugarComprobanteFiscalConceptoRepository(configuracion, user);
            this.User = user;
        }

        /// <summary>
        /// obtener comprobante fiscal detalle
        /// </summary>
        /// <param name="index">indice</param>
        public ComprobanteFiscalDetailModel GetComprobante(int index) {
            //Manual mode
            var result = this.Db.Queryable<ComprobanteFiscalDetailModel>().Mapper((itemModel, cache) => {
                // conceptos del comprobante
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.Id).ToList();
                    return this.Db.Queryable<ComprobanteConceptoDetailModel>().Where(it => allIds.Contains(it.SubId)).Where(it => it.Activo == true).ToList();
                });
                itemModel.Conceptos = new BindingList<ComprobanteConceptoDetailModel>(allItems.Where(it => it.SubId == itemModel.Id).ToList());
            }).Where(it => it.Id == index).Single();

            if (result.Conceptos != null) {
                for (int i = 0; i < result.Conceptos.Count; i++) {
                    result.Conceptos[i].PresionDecimal = result.PrecisionDecimal;
                }
            }

            if (result != null) {
                if (result.JCfdiRelacionados != null) {
                    result.CfdiRelacionados = ComprobanteCfdiRelacionadosModel.Json(result.JCfdiRelacionados);
                }
            }
            return result;
        }

        /// <summary>
        /// Realizar busqueda de un comprobante por el folio de control interno
        /// </summary>
        /// <param name="subTipo">subtipo del comprobante fiscal (Emitido, Recibido, Nomina)</param>
        /// <param name="rfc">RFC del emisor o receptor</param>
        /// <param name="status">status a buscar</param>
        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, string rfc, string[] status) {
            var s = status[0];
            return this.Db.Queryable<ComprobanteFiscalDetailSingleModel>()
                .WhereIF(subTipo == CFDISubTipoEnum.Emitido, it => it.ReceptorRFC == rfc && it.IdSubTipo == (int)subTipo)
                .WhereIF(subTipo == CFDISubTipoEnum.Recibido, it => it.EmisorRFC == rfc && it.IdSubTipo == (int)subTipo)
                .Where(it => it.Status.Contains(s))
                .OrderBy(it => it.Id, OrderByType.Desc)
                .ToList();
        }

        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, List<Conditional> condicionValues, int year = 0, int month = 0) {
            var _conditionalList = new List<IConditionalModel>();
            if (condicionValues != null) {
                foreach (var item in condicionValues) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            // solo registros activos
            _conditionalList.Add(new ConditionalModel() { ConditionalType = ConditionalType.Equal, FieldName = "_cfdi_a", FieldValue = "1" });

            var response1 = this.Db.Queryable<ComprobanteFiscalDetailSingleModel>()
                .Where(it => !SqlFunc.IsNullOrEmpty(it.IdDocumento))
                .WhereIF(year > 0, it => it.FechaEmision.Year == year)
                .WhereIF(month > 0, it => it.FechaEmision.Month == month)
                .Where(_conditionalList)
                .OrderBy(it => it.Id, OrderByType.Desc)
                .ToSql();

            var d = this.GetMapper<ComprobanteFiscalDetailSingleModel>(response1.Key, response1.Value);

            return d;
        }

        public IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, string rfc, List<Conditional> keyValues, int year = 0, int month = 0) {
            var _conditionalList = new List<IConditionalModel>();
            if (keyValues != null) {
                foreach (var item in keyValues) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            // solo registros activos
            _conditionalList.Add(new ConditionalModel() { ConditionalType = ConditionalType.Equal, FieldName = "_cfdi_a", FieldValue = "1" });
            var response1 = this.Db.Queryable<ComprobanteFiscalDetailSingleModel>()
                .WhereIF(subTipo == CFDISubTipoEnum.Emitido, it => it.ReceptorRFC == rfc)
                .WhereIF(subTipo == CFDISubTipoEnum.Recibido, it => it.EmisorRFC == rfc)
                .WhereIF(year > 0, it => it.FechaEmision.Year == year)
                .WhereIF(month > 0, it => it.FechaEmision.Month == month)
                .Where(it => !SqlFunc.IsNullOrEmpty(it.IdDocumento))
                .Where(it => it.IdSubTipo == (int)subTipo)
                .Where(_conditionalList)
                .OrderBy(it => it.Id, OrderByType.Desc)
                .ToSql();
            var d = this.GetMapper<ComprobanteFiscalDetailSingleModel>(response1.Key, response1.Value);

            return d;
        }

        /// <summary>
        /// obtener resumen de comprobantes fiscales para tabla dinamica
        /// </summary>
        /// <param name="subtipo">sub tipo de comprobante Emitido, Recibido, Nomina</param>
        /// <param name="anio">año</param>
        /// <param name="mes">mes</param>
        public IEnumerable<EstadoCuentaSingleView> GetResumen(CFDISubTipoEnum subtipo, int anio, int mes = 0) {
            if (subtipo == CFDISubTipoEnum.Emitido) {
                return this.Db.Queryable<ComprobanteFiscalDetailSingleModel>()
                    .WhereIF(mes != (int)MesesEnum.Todos, it => it.FechaEmision.Month == (int)mes)
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.IdSubTipo == (int)subtipo)
                    .Select(c => new EstadoCuentaSingleView {
                        Tipo = c.TipoComprobanteText,
                        Status = c.Status,
                        Receptor = c.ReceptorNombre,
                        RFC = c.ReceptorRFC,
                        IdDocumento = c.IdDocumento,
                        Descuento = c.Descuento,
                        Acumulado = c.Acumulado,
                        Total = c.Total,
                        FechaEmision = c.FechaEmision,
                        FechaCobro = c.FechaUltimoPago,
                        IEPSRetenido = c.RetencionIEPS,
                        IEPSTraslado = c.TrasladoIEPS,
                        ISRRetenido = c.RetencionISR,
                        IVARetencion = c.RetencionIVA,
                        IVATraslado = c.TrasladoIVA
                    }).ToList();
            } else if (subtipo == CFDISubTipoEnum.Recibido) {
                return this.Db.Queryable<ComprobanteFiscalDetailSingleModel>()
                    .WhereIF(mes != (int)MesesEnum.Todos, it => it.FechaEmision.Month == (int)mes)
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.IdSubTipo == (int)subtipo)
                    .Select(c => new EstadoCuentaSingleView {
                        Tipo = c.TipoComprobanteText,
                        Status = c.Status,
                        Receptor = c.EmisorNombre,
                        RFC = c.EmisorRFC,
                        IdDocumento = c.IdDocumento,
                        Descuento = c.Descuento,
                        Acumulado = c.Acumulado,
                        Total = c.Total,
                        FechaEmision = c.FechaEmision,
                        FechaCobro = c.FechaUltimoPago
                    }).ToList();
            }
            return null;
        }

        public DataTable GetResumen(CFDISubTipoEnum subtipo, int year, string[] estado, string rfc = "") {

            var sqlCommand = @"SELECT _cfdi_nome Receptor, _cfdi_rfce RFC, ELT(EXTRACT(MONTH FROM _cfdi._cfdi_fecems),'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') Mes, 
                            AVG(DATEDIFF(_cfdi_fecupc, _cfdi_fecems)) DiasCobro, count(_cfdi_uuid) TotalComprobantes, 
                            sum(_cfdi_total) TotalFacturado, 
                            SUM(case when _cfdi_efecto LIKE 'I%' THEN _cfdi_total ELSE 0 end) AS Ingreso,
                            SUM(case when _cfdi_efecto LIKE 'E%' THEN _cfdi_total ELSE 0 end) AS Egreso,
                            sum(_cfdi_cbrd) AS COBRADO,
                            (sum(_cfdi_cbrd) + SUM(case when _cfdi_efecto LIKE 'E%' THEN _cfdi_total ELSE 0 END)) ACUMULADO, 
                            (SUM(case when _cfdi_efecto LIKE 'I%' THEN _cfdi_total ELSE 0 end) - (sum(_cfdi_cbrd) + SUM(case when _cfdi_efecto LIKE 'E%' THEN _cfdi_total ELSE 0 END))) Saldo ,
                            AVG(_cfdi_total) PromedioFacturado, 
                            AVG(_cfdi_cbrd) PromedioCobrado
                            FROM _cfdi  
                            WHERE _cfdi_doc_id = @subtipo

                            AND _cfdi_status IN (@status)
                            AND EXTRACT(YEAR FROM _cfdi_fecems) = @anio
                            AND _cfdi_uuid IS NOT NULL 
                            and _cfdi_rfce LIKE @rfc
                            GROUP BY _cfdi_rfce,EXTRACT(month FROM _cfdi_fecems) 
                            ORDER BY _cfdi_rfce,_cfdi_fecems asc";

            if (subtipo == CFDISubTipoEnum.Emitido) {
                sqlCommand = sqlCommand.Replace("_cfdi_rfce", "_cfdi_rfcr");
                sqlCommand = sqlCommand.Replace("_cfdi_nome", "_cfdi_nomr");
            }
            var _subtipo = (int)subtipo;
            var _status = new List<string>();
            foreach (var item in estado) {
                _status.Add("'" + item + "'");
            }
            sqlCommand = sqlCommand.Replace("@status", string.Join(",", _status));

            return this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>() { new SugarParameter("@anio", year.ToString()), new SugarParameter("@rfc", rfc), new SugarParameter("@subtipo", _subtipo.ToString()) });
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var condiciones = this.Convierte(conditionals);

            if (typeof(T1) == typeof(ComprobanteFiscalDetailSingleModel)) {
                return this.Db.Queryable<T1>().Where(condiciones).ToList();
            } else if (typeof(T1) == typeof(ComprobanteFiscalConceptoView) | typeof(T1) == typeof(ComprobanteFiscalConceptoView)) {
                var sql = "SELECT * FROM _cfdcnp, _cfdi WHERE _cfdcnp._cfdcnp_cfds_id = _cfdi._cfdi_id @condiciones";
                return this.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(EstadoCuentaSingleView)) {

            } else if (typeof(T1) == typeof(ComprobanteCfdiRelacionadosModel)) {
                var sql = "SELECT * FROM _cfdcnp, _cfdi WHERE _cfdcnp._cfdcnp_cfds_id = _cfdi._cfdi_id @condiciones";
                return this.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(EstadoCuenta)) {
                var sql = @"SELECT _cfdi._cfdi_drctr_id, _cfdi._cfdi_rfce, _cfdi._cfdi_nome, _cfdi._cfdi_rfcr, _cfdi._cfdi_nomr, SUM(_cfdi._cfdi_sbttl) as _cfdi_sbttl, SUM(_cfdi._cfdi_dscnt) as _cfdi_dscnt, SUM(_cfdi._cfdi_total) as _cfdi_total, SUM(_cfdi._cfdi_cbrd) as _cfdi_cbrd,  
                SUM(_cfdi._cfdi_trsiva) as _cfdi_trsiva, SUM(_cfdi._cfdi_trsieps) as _cfdi_trsieps, SUM(_cfdi._cfdi_retiva) as _cfdi_retiva, SUM(_cfdi._cfdi_retisr) as _cfdi_retisr, SUM(_cfdi._cfdi_retieps) as _cfdi_retieps,COUNT(_cfdi._cfdi_drctr_id) as contador, 
                MAX(_cfdi._cfdi_fecems) as _cfdi_fecems, MAX(_cfdi._cfdi_fecupc) as _cfdi_fecupc
                FROM _cfdi
                WHERE _cfdi._cfdi_a = 1 @condiciones
                GROUP BY _cfdi._cfdi_drctr_id ORDER BY _cfdi._cfdi_nome, _cfdi._cfdi_nomr";
                return this.GetMapper<T1>(sql, conditionals);
            }
            return this.Db.Queryable<T1>().Where(condiciones).ToList();
        }

        /// <summary>
        /// almacenar comprobante fiscal
        /// </summary>
        public ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel item) {
            if (item.Id == 0) {
                // si es un comprobante emitido creamos el nuevo folio a partir de la serie
                if (item.SubTipo == CFDISubTipoEnum.Emitido) {
                    if (string.IsNullOrEmpty(item.Folio))
                        item.Folio = this.GetFolio(item.Emisor.RFC, item.Serie).ToString();
                }
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                var result = this.Db.Insertable(item);
                item.Id = this.Execute(result);
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                var result = this.Db.Updateable<ComprobanteFiscalModel>(item);
                this.Execute(result);
            }

            if (item.Conceptos.Count > 0) {
                for (int i = 0; i < item.Conceptos.Count; i++) {
                    if (item.Conceptos[i].Id == 0) {
                        item.Conceptos[i].SubId = item.Id;
                        item.Conceptos[i].Creo = item.Creo;
                        var d = this.Db.Insertable<ComprobanteConceptoModel>(item.Conceptos[i]);
                        item.Conceptos[i].Id = this.Insert(d);
                    } else {
                        item.Conceptos[i].Modifica = item.Modifica;
                        item.Conceptos[i] = this.conceptoRepository.Save(item.Conceptos[i]);
                    }
                }
            }

            if (item.TimbreFiscal != null) {
                // cfdi relacionados
                if (item.CfdiRelacionados != null)
                    if (item.CfdiRelacionados.CfdiRelacionado != null)
                        this.AplicarCFDIRelacionados(item.CfdiRelacionados);
            }

            return item;
        }

        /// <summary>
        /// aplicar reglas a los cfdi relacionados del comprobante fiscal
        /// </summary>
        public bool AplicarCFDIRelacionados(ComprobanteCfdiRelacionadosModel objeto) {
            if (objeto != null) {
                // para la clave de notas de credito relacionadas
                if (objeto.TipoRelacion.Clave == "01" | objeto.TipoRelacion.Clave == "02" | objeto.TipoRelacion.Clave == "03") {
                    foreach (ComprobanteCfdiRelacionadosCfdiRelacionadoModel item in objeto.CfdiRelacionado) {
                        //decimal suma = 0;
                        //List<ComplementoDoctoRelacionado> notas = this.Notas(item.IdDocumento);
                        //if (notas != null) {
                        //    if (notas.Count > 0) {
                        //        suma = notas.Where(p => ValidacionService.UUID(p.IdDocumento)).Sum(p => p.Total);
                        //    }
                        //}
                        ////Entities.Base.ComprobanteGeneral d = this.GetComprobanteGeneral(item.IdDocumento);
                        //string CommandText = "update _cfdi set _cfdi_dscntc=@descuento where _cfdi_uuid=@index";
                        //return this.Db.Ado.ExecuteCommand(CommandText, new List<SugarParameter>()
                        //{
                        //    new SugarParameter("@descuento", suma),
                        //    new SugarParameter("@index", item.IdDocumento)
                        //}) > 0;

                    }
                }
            }
            return false;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlXml(int index, string url) {
            var update = this.Db.Updateable<ComprobanteFiscalModel>()
                .SetColumns(it => new ComprobanteFiscalModel() {
                    UrlFileXML = url
                }).Where(it => it.Id == index);
            return this.Execute(update) > 0;
        }

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        public bool UpdateUrlPdf(int index, string url) {
            var update = this.Db.Updateable<ComprobanteFiscalModel>()
                .SetColumns(it => new ComprobanteFiscalModel() {
                    UrlFilePDF = url
                }).Where(it => it.Id == index);
            return this.Execute(update) > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlXmlAcuse(int index, string url) {
            var update = this.Db.Updateable<ComprobanteFiscalModel>()
                .SetColumns(it => new ComprobanteFiscalModel() {
                    FileAccuseXML = url
                }).Where(it => it.Id == index);
            return this.Execute(update) > 0;
        }

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        public bool UpdateUrlPdfAcuse(int index, string url) {
            var update = this.Db.Updateable<ComprobanteFiscalModel>()
                .SetColumns(it => new ComprobanteFiscalModel() {
                    FileAccusePDF = url
                }).Where(it => it.Id == index);
            return this.Execute(update) > 0;
        }

        /// <summary>
        /// actualizar estado del comprobante
        /// </summary>
        /// <param name="idDocumento">folio fiscal del comprobante</param>
        /// <param name="estado">estado del comprobante</param>
        /// <param name="usuario">clave del usuario</param>
        /// <returns>true al actualizar base de datos</returns>
        public bool UpdateEstado(string idDocumento, string estado, string usuario) {
            var result = this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel { Estado = estado, FechaEstado = DateTime.Now }).Where(it => it.IdDocumento == idDocumento);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// metodo para actualizar el status del comprobante fiscal
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="status">nuevo status</param>
        /// <param name="usuario">clave del usuario</param>
        public bool Update(int indice, string status, string usuario) {
            var result = this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel {
                Status = status,
                Modifica = usuario,
                FechaModifica = DateTime.Now,
                FechaEntrega = DateTime.Now
            })
                .Where(it => it.Id == indice);
            return this.Execute(result) > 0;
        }

        #region conceptos
        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        public IEnumerable<ComprobanteConceptoDetailModel> GetConceptos(int index) {
            return this.conceptoRepository.GetList(index);
        }
        #endregion

        #region complemento de pago
        private bool AplicarComprobantePago(BindingList<ComplementoPagoDetailModel> recepcionPago) {
            if (recepcionPago != null) {

            }
            return false;
        }
        #endregion

        #region complemento nomina
        public void CNomina(ComplementoNomina complemento, int idComprobante, string idDocumento) {
            var d = new ComplementoNominaModel {
                Id = complemento.IdComplemento,
                Activo = true,
                IdDocumento = idDocumento,
                SubId = idComprobante,
                Anio = 0,
                DeduccionTotalExento = complemento.Deducciones.TotalExento,
                DeduccionTotalGravado = complemento.Deducciones.TotalExento,
                FechaFinalPago = complemento.FechaFinalPago,
                FechaInicialPago = complemento.FechaInicialPago,
                CtaBanco = complemento.Receptor.CuentaBancaria,
                Antiguedad = complemento.Receptor.Antiguedad,
                EntidadFederativa = complemento.Receptor.ClaveEntFed,
                Banco = complemento.Receptor.Banco,
                CLABE = complemento.Receptor.CuentaBancaria,
                CURP = complemento.Receptor.CURP,
                Departamento = complemento.Receptor.Departamento,
                Descuento = complemento.Descuento,
                FechaInicioRelLaboral = complemento.Receptor.FechaInicioRelLaboral,
                FechaPago = complemento.FechaPago,
                NumDiasPagados = complemento.NumDiasPagados,
                NumEmpleado = complemento.Receptor.NumEmpleado,
                NumSeguridadSocial = complemento.Receptor.NumSeguridadSocial,
                PercepcionTotalExento = complemento.Percepciones.TotalExento,
                PercepcionTotalGravado = complemento.Percepciones.TotalGravado,
                PeriodicidadPago = complemento.Receptor.PeriodicidadPago,
                RegistroPatronal = complemento.Emisor.RegistroPatronal,
                Puesto = complemento.Receptor.Puesto,
                RFC = complemento.Receptor.RFC,
                RiesgoPuesto = complemento.Receptor.RiesgoPuesto,
                SalarioBaseCotApor = complemento.Receptor.SalarioBaseCotApor,
                SalarioDiarioIntegrado = complemento.Receptor.SalarioDiarioIntegrado,
                Sindicalizado = complemento.Receptor.Sindicalizado == true ? "Sí" : "No",
                TipoContrato = complemento.Receptor.TipoContrato,
                TipoJornada = complemento.Receptor.TipoJornada,
                TipoNomina = complemento.TipoNomina,
                TipoRegimen = complemento.Receptor.TipoRegimen,
                Version = complemento.Version,
                FechaNuevo = DateTime.Now,
                ImportePagado = complemento.Percepciones.Percepcion[0].HorasExtra[0].ImportePagado
            };

            foreach (var item in complemento.Percepciones.Percepcion) {

            }
        }
        #endregion

        #region backup
        public IEnumerable<ComprobanteBackup> GetInfoBackup(CFDISubTipoEnum subTipo) {
            string commandText = @"select concat(year(_cfdi_feccert),'0') as Indice, 0 as ParentId,year(_cfdi_feccert) as ejercicio,0 as periodo, count(_cfdi_uuid) as contador, year(_cfdi_feccert) as descripcion from _cfdi where _cfdi_doc_id=@index and _cfdi_uuid<>'' and _cfdi_feccert is not null group by ejercicio union 
                                   select concat(year(_cfdi_feccert), month(_cfdi_feccert)) as Indice,date_format(_cfdi_feccert,'%Y%0') as ParentId,year(_cfdi_feccert) as ejercicio,month(_cfdi_feccert) as periodo, count(_cfdi_uuid) as contador,month(_cfdi_feccert) as descripcion from _cfdi where _cfdi_doc_id=@index and _cfdi_uuid<>'' and _cfdi_feccert is not null group by ejercicio,periodo";


            return this.GetMapper<ComprobanteBackup>(commandText, new List<SugarParameter> { new SugarParameter("@index", (int)subTipo) });
        }
        #endregion

        /// <summary>
        ///  Generar folio para un comprobante
        /// </summary>
        /// <param name="rfc">RFC del emisor del comprobante</param>
        /// <param name="serie">Serie del comprobante fiscal</param>
        /// <returns>numero entero convertido en cadena</returns>
        private string GetFolio(string rfc, string serie) {
            try {
                int folio = this.Db.Ado.GetInt("select (Max(cast(_cfdi._cfdi_folio as UNSIGNED)) + 1) as folio from _cfdi where _cfdi_rfce = @rfc and _cfdi._cfdi_serie = @serie", new {
                    rfc = rfc,
                    serie = serie
                });
                return folio.ToString();
            } catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                return "0";
            }
        }
    }
}
