﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComplementoNominaParteRepository : MySqlSugarContext<ComplementoNominaParteModel>, ISqlComplmentoNominaParteRepository {
        public SqlSugarComplementoNominaParteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override int Insert(ComplementoNominaParteModel single) {
            var sqlCommand = @"insert into _nmnprt (_nmnprt_a,_nmnprt_cfdnmn_id,_nmnprt_doc,_nmnprt_tipo,_nmnprt_qnc,_nmnprt_anio,_nmnprt_dias,_nmnprt_hrsxtr,_nmnprt_dsincp,_nmnprt_impgrv,_nmnprt_impext,_nmnprt_dscnt,_nmnprt_imppgd,_nmnprt_tphrs,_nmnprt_clv,_nmnprt_cncpt,_nmnprt_fn,_nmnprt_usr_n,_nmnprt_numem,_nmnprt_imprt) 
                                            values (@nmnprt_a,@nmnprt_cfdnmn_id,@nmnprt_doc,@nmnprt_tipo,@nmnprt_qnc,@nmnprt_anio,@nmnprt_dias,@nmnprt_hrsxtr,@nmnprt_dsincp,@nmnprt_impgrv,@nmnprt_impext,@nmnprt_dscnt,@nmnprt_imppgd,@nmnprt_tphrs,@nmnprt_clv,@nmnprt_cncpt,@nmnprt_fn,@nmnprt_usr_n,@nmnprt_numem,@nmnprt_imprt); select last_insert_id();";

            var parameters = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@nmnprt_a", 1),
                new SqlSugar.SugarParameter("@nmnprt_cfdnmn_id", single.SubId),
                new SqlSugar.SugarParameter("@nmnprt_doc", single.IdDoc),
                new SqlSugar.SugarParameter("@nmnprt_tipo", single.Tipo),
                new SqlSugar.SugarParameter("@nmnprt_clv", single.Clave),
                new SqlSugar.SugarParameter("@nmnprt_qnc", single.Quincena),
                new SqlSugar.SugarParameter("@nmnprt_anio", single.Anio),
                new SqlSugar.SugarParameter("@nmnprt_dias", single.DiasIncapacidad),
                new SqlSugar.SugarParameter("@nmnprt_hrsxtr", single.HorasExtra),
                new SqlSugar.SugarParameter("@nmnprt_dsincp", single.DiasIncapacidad),
                new SqlSugar.SugarParameter("@nmnprt_impgrv", single.ImporteGravado),
                new SqlSugar.SugarParameter("@nmnprt_impext", single.ImporteExento),
                new SqlSugar.SugarParameter("@nmnprt_dscnt", single.DescuentoIncapcidad),
                new SqlSugar.SugarParameter("@nmnprt_imppgd", single.ImportePagadoHorasExtra),
                new SqlSugar.SugarParameter("@nmnprt_tphrs", single.TipoHoras),
                new SqlSugar.SugarParameter("@nmnprt_cncpt", single.Concepto),
                new SqlSugar.SugarParameter("@nmnprt_numem", single.NumEmpleado),
                new SqlSugar.SugarParameter("@nmnprt_imprt", single.Importe),
                new SqlSugar.SugarParameter("@nmnprt_fn", DateTime.Now),
                new SqlSugar.SugarParameter("@nmnprt_usr_n", single.Creo)
            };

            return this.Db.Ado.ExecuteCommand(sqlCommand, parameters);
        }

        /// <summary>
        /// almacenar complemento de nomina
        /// </summary>
        public ComplementoNominaParteModel Save(ComplementoNominaParteModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                var result = this.Db.Insertable<ComplementoNominaParteModel>(model);
                model.Id = this.Execute(result);
            } else {
                var result = this.Db.Updateable<ComplementoNominaParteModel>(model);
                if (this.Execute(result) == 0) {
                    Services.LogErrorService.LogWrite("No se guardo");
                }
            }
            return model;
        }
    }
}
