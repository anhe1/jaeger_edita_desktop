﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarSerieRepository : SqlSugarContext<SerieFolioModel>, ISqlSerieRepository {
        public SqlSugarSerieRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }
    }
}
