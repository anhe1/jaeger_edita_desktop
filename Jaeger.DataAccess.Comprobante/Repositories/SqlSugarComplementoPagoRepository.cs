﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComplementoPagoRepository : MySqlSugarContext<ComplementoPagoModel>, ISqlComplementoPagoRepository {
        public SqlSugarComplementoPagoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public new ComplementoPagoDetailModel GetById(int index) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<ComplementoPagoDetailModel> GetComplementoById(int index) {
            var _response = this.Db.Queryable<ComplementoPagoDetailModel>().Where(it => it.IdComprobanteP == index);
            return _response.ToList();
        }

        public ComplementoPagoDetailModel Save(ComplementoPagoDetailModel model) {
            if (model.IdPago == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                var _result = this.Db.Insertable(model);
                model.IdPago = this.Execute(_result);
            } else if (model.IdPago > 0) {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                var _result = this.Db.Updateable<ComplementoPagoDetailModel>(model);
                this.Execute(_result);
            }

            if (model.DoctoRelacionados != null) {
                if (model.DoctoRelacionados.Count > 0) {
                    for (int i = 0; i < model.DoctoRelacionados.Count; i++) {
                        if (model.DoctoRelacionados[i].IdRelacion == 0) {
                            model.DoctoRelacionados[i].IdPago = model.IdPago;
                            model.DoctoRelacionados[i].IdComprobanteP = model.IdComprobanteP;
                            model.DoctoRelacionados[i] = this.Save(model.DoctoRelacionados[i]);
                        } else {
                            model.DoctoRelacionados[i].IdPago = model.IdPago;
                            model.DoctoRelacionados[i].IdComprobanteP = model.IdComprobanteP;
                            model.DoctoRelacionados[i] = this.Save(model.DoctoRelacionados[i]);
                        }
                    }
                }
            }
            return model;
        }

        public BindingList<ComplementoPagoDetailModel> Save(BindingList<ComplementoPagoDetailModel> models) {
            if (models != null) {
                for (int i = 0; i < models.Count; i++) {
                    models[i] = this.Save(models[i]);
                }
            }
            return models;
        }

        public PagosPagoDoctoRelacionadoDetailModel Save(PagosPagoDoctoRelacionadoDetailModel model) {
            if (model.IdRelacion == 0) {
                var _query = this.Db.Insertable<PagosPagoDoctoRelacionadoDetailModel>(model);
                model.IdRelacion = this.Execute(_query);
            } else {
                var _query = this.Db.Updateable<PagosPagoDoctoRelacionadoDetailModel>(model);
                this.Execute(_query);
            }
            return model;
        }

        public bool AplicarComprobantePagos(ComplementoPagoDetailModel pagos) {
            if (pagos != null) {
                var item = pagos.DoctoRelacionados.Where(it => it.Activo).ToList();
                var lista = this.Rep(item);
                if (lista != null) {
                    if (lista.Count > 0) {
                        foreach (var item1 in lista) {
                            var _queryUpdate = this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel() {
                                Acumulado = item1.ImpPagado,
                                NumParcialidad = item1.NumParcialidad,
                                FechaRecepcionPago = pagos.FechaPagoP
                            }).Where(it => it.IdDocumento == item1.IdDocumentoDR);
                            this.Execute(_queryUpdate);
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// obtener lista de comprobantes fiscales relacionados al comprobante de pago, se calcula el maximo del numero de partida y la suma de los importes pagados
        /// </summary>
        public List<PagosPagoDoctoRelacionadoDetailModel> Rep(List<PagosPagoDoctoRelacionadoDetailModel> idDocumento) {
            var _origen = idDocumento.Select(it => it.IdDocumentoDR).ToList();
            var sqlCommand = @"SELECT _cmppgd_uuid, MAX(_cmppgd_nmpar) as _cmppgd_nmpar, SUM(_cmppgd_imppgd) as _cmppgd_imppgd FROM _cmppgd WHERE _cmppgd_a = 1 AND _cmppgd_uuid IN (@array) GROUP BY _cmppgd_uuid";
            sqlCommand = sqlCommand.Replace("@array", "'" + string.Join("','", _origen) + "'");

            var _re = this.GetMapper<PagosPagoDoctoRelacionadoDetailModel>(sqlCommand);

            return _re.ToList();
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ComplementoPagoDetailModel)) {
                return (IEnumerable<T1>)this.GetList0(conditionals);
            } else if (typeof(T1) == typeof(ComplementoPagoMergeDoctoModel)) {
                return (IEnumerable<T1>)this.Get3List(conditionals);
            } else if (typeof(T1) == typeof(ComprobanteReciboEletronicoPagoSingle)) {
                return this.GetMapper<T1>("SELECT * FROM _cmppg LEFT JOIN _cfdi  on _cmppg._cmppg_cfd_id = _cfdi._cfdi_id @wcondiciones", conditionals);
            } else if (typeof(T1) == typeof(ComplementoVsPagoMergeDoctoModel)) {
                return this.GetMapper<T1>(@"SELECT _cfdi.*,
                    _cmppg.*, _cmppgd.*, a._cfdi_uuid idDocumentoP
                    FROM _cfdi 
                    LEFT JOIN _cmppgd ON _cmppgd._cmppgd_cfdi_id = _cfdi._cfdi_id
                    LEFT JOIN _cmppg ON _cmppg._cmppg_id = _cmppgd._cmppgd_idpg
                    LEFT JOIN _cfdi a ON a._cfdi_id = _cmppg._cmppg_cfd_id
                    WHERE _cfdi._cfdi_efecto like 'I%' AND _cfdi._cfdi_frmpg = '99' AND _cfdi._cfdi_mtdpg = 'PPD' @condiciones
                    ORDER BY _cfdi._cfdi_uuid ASC, _cfdi._cfdi_fecems ASC, _cmppgd._cmppgd_nmpar ASC  ;", conditionals);
            }

            var condiciones = new List<IConditionalModel>();
            foreach (var item in conditionals) {
                condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
            }
            return this.Db.Queryable<T1>().Where(condiciones).ToList();
        }

        /// <summary>
        /// complemento de pago con detalle
        /// </summary>
        private IEnumerable<ComplementoPagoDetailModel> GetList0(List<IConditional> conditionals) {
            var _condiciones = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }

            // solo registros activos
            _condiciones.Add(new ConditionalModel() { ConditionalType = ConditionalType.Equal, FieldName = "_cmppg_a", FieldValue = "1" });

            var result = this.Db.Queryable<ComplementoPagoDetailModel>().Mapper((model, cache) => {
                var items = cache.Get(lista => {
                    var ids = lista.Select(it => it.IdPago).ToList();
                    return this.Db.Queryable<PagosPagoDoctoRelacionadoDetailModel>().Where(it => ids.Contains(it.IdPago)).Where(it => it.Activo == true).ToList();
                });
                model.DoctoRelacionados = new BindingList<PagosPagoDoctoRelacionadoDetailModel>(items.Where(it => it.IdPago == model.IdPago).ToList());
            }).Where(_condiciones).OrderBy(it => it.IdPago, OrderByType.Asc);
            return result.ToList();
        }

        /// <summary>
        /// vista de complemento de pago con sus partidas
        /// </summary>
        private IEnumerable<ComplementoPagoMergeDoctoModel> Get3List(List<IConditional> conditionals) {
            var _condiciones = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }

            // solo registros activos
            _condiciones.Add(new ConditionalModel() { ConditionalType = ConditionalType.Equal, FieldName = "_cmppg_a", FieldValue = "1" });
            _condiciones.Add(new ConditionalModel() { ConditionalType = ConditionalType.Equal, FieldName = "_cmppgd_a", FieldValue = "1" });

            var sql = this.Db.Queryable<ComplementoPagoModel, PagosPagoDoctoRelacionadoModel, ComprobanteFiscalSingleModel>(
                (p, d, c) => new JoinQueryInfos(
                    JoinType.Left, p.IdPago == d.IdPago,
                    JoinType.Left, p.IdComprobanteP == c.Id)
                ).Where(_condiciones)
                .Select((p, d, c) => new ComplementoPagoMergeDoctoModel {
                    IdPago = p.IdPago,
                    Activo = p.Activo,
                    CadPago = p.CadPago,
                    CertPago = p.CertPago,
                    Creo = p.Creo,
                    CtaBeneficiario = p.CtaBeneficiario,
                    CtaOrdenante = p.CtaOrdenante,
                    FechaModifica = p.FechaModifica,
                    FechaNuevo = p.FechaNuevo,
                    FechaPagoP = p.FechaPagoP,
                    FormaDePagoP = p.FormaDePagoP,
                    Version = p.Version,
                    IdComprobanteP = p.IdComprobanteP,
                    IdComprobanteR = d.IdComprobanteR,
                    IdDocumentoDR = d.IdDocumentoDR,
                    ImpPagado = d.ImpPagado,
                    ImpSaldoAnt = d.ImpSaldoAnt,
                    ImpSaldoInsoluto = d.ImpSaldoInsoluto,
                    Modifica = p.Modifica,
                    MonedaP = p.MonedaP,
                    Monto = p.Monto,
                    NomBancoOrdExt = p.NomBancoOrdExt,
                    NumOperacion = p.NumOperacion,
                    NumParcialidad = d.NumParcialidad,
                    RfcEmisorCtaBen = p.RfcEmisorCtaBen,
                    RfcEmisorCtaOrd = p.RfcEmisorCtaOrd,
                    SelloPago = p.SelloPago,
                    TipoCadPago = p.TipoCadPago,
                    TipoCambioP = p.TipoCambioP,
                    IdDocumentoP = c.IdDocumento,
                    FolioP = c.Folio,
                    SerieP = c.Serie,
                    FechaEmisionP = c.FechaEmision,
                    StatusP = c.Status,
                    EmisorNombreP = c.EmisorNombre,
                    EmisorRFCP = c.EmisorRFC,
                    ReceptorNombreP = c.ReceptorNombre,
                    ReceptorRFCP = c.ReceptorRFC,
                    EstadoP = c.Estado
                }).ToList();
            return sql;
        }

        public override bool CreateTable() {
            this.Db.CodeFirst.InitTables<PagosPagoDoctoRelacionadoModel>();
            return base.CreateTable();
        }
    }
}
