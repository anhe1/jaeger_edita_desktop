﻿using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComprobanteInformacionGlobalModel : SqlSugarContext<ComprobanteInformacionGlobalModel>, ISqlComprobanteInformacionGlobalModel {
        public SqlSugarComprobanteInformacionGlobalModel(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public ComprobanteInformacionGlobalDetailModel GetBy(List<Conditional> condiciones) {
            return null;
        }
    }
}
