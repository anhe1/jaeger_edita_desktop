﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlRelacionComercialRepository : MySqlSugarContext<RelacionComercialModel>, ISqlRelacionComercialRepository {
        public SqlRelacionComercialRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<RelacionComercialDetailModel> GetList(List<IConditional> conditionals) {
            return this.GetMapper<RelacionComercialDetailModel>(conditionals);
        }

        public int Saveable_Old(RelacionComercialDetailModel item) {
            item.FechaModifica = DateTime.Now;
            item.Modifica = this.User;
            return this.Db.Saveable((RelacionComercialModel)item).ExecuteCommand();
        }

        public int Saveable(RelacionComercialDetailModel item) {
            var sql = @"INSERT INTO DRCTRR ( DRCTRR_A, DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID, DRCTRR_CTCMS_ID, DRCTRR_CTDSC_ID, DRCTRR_CTPRC_ID, DRCTRR_NOTA, DRCTRR_USR_M, DRCTRR_FM) 
                                    VALUES (@DRCTRR_A,@DRCTRR_DRCTR_ID,@DRCTRR_CTREL_ID,@DRCTRR_CTCMS_ID,@DRCTRR_CTDSC_ID,@DRCTRR_CTPRC_ID,@DRCTRR_NOTA,@DRCTRR_USR_M,@DRCTRR_FM) 
                    ON DUPLICATE KEY UPDATE 
                        DRCTRR_A = @DRCTRR_A, DRCTRR_DRCTR_ID = @DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID = @DRCTRR_CTREL_ID, DRCTRR_CTCMS_ID = @DRCTRR_CTCMS_ID, DRCTRR_CTDSC_ID = @DRCTRR_CTDSC_ID, 
                 DRCTRR_CTPRC_ID = @DRCTRR_CTPRC_ID, DRCTRR_NOTA = @DRCTRR_NOTA, DRCTRR_USR_M=@DRCTRR_USR_M, DRCTRR_FM = @DRCTRR_FM;";

            item.FechaModifica = DateTime.Now;
            item.Modifica = this.User;

            var parameters = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@DRCTRR_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCTRR_DRCTR_ID", item.IdDirectorio),
                new SqlSugar.SugarParameter("@DRCTRR_CTREL_ID", item.IdTipoRelacion),
                new SqlSugar.SugarParameter("@DRCTRR_CTCMS_ID", item.IdComision),
                new SqlSugar.SugarParameter("@DRCTRR_CTDSC_ID", 0),
                new SqlSugar.SugarParameter("@DRCTRR_CTPRC_ID", 0),
                new SqlSugar.SugarParameter("@DRCTRR_NOTA", item.Nota),
                new SqlSugar.SugarParameter("@DRCTRR_USR_M", item.Modifica),
                new SqlSugar.SugarParameter("@DRCTRR_FM", item.FechaModifica),
            };
            return this.Db.Ado.ExecuteCommand(sql, parameters);
        }

        public int Saveable(IRelacionComercialModel item) {
            var sql = @"INSERT INTO DRCTRR ( DRCTRR_A, DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID, DRCTRR_CTCMS_ID, DRCTRR_CTDSC_ID, DRCTRR_CTPRC_ID, DRCTRR_NOTA, DRCTRR_USR_M, DRCTRR_FM) 
                                    VALUES (@DRCTRR_A,@DRCTRR_DRCTR_ID,@DRCTRR_CTREL_ID,@DRCTRR_CTCMS_ID,@DRCTRR_CTDSC_ID,@DRCTRR_CTPRC_ID,@DRCTRR_NOTA,@DRCTRR_USR_M,@DRCTRR_FM) 
                    ON DUPLICATE KEY UPDATE 
                        DRCTRR_A = @DRCTRR_A, DRCTRR_DRCTR_ID = @DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID = @DRCTRR_CTREL_ID, DRCTRR_CTCMS_ID = @DRCTRR_CTCMS_ID, DRCTRR_CTDSC_ID = @DRCTRR_CTDSC_ID, 
                 DRCTRR_CTPRC_ID = @DRCTRR_CTPRC_ID, DRCTRR_NOTA = @DRCTRR_NOTA, DRCTRR_USR_M=@DRCTRR_USR_M, DRCTRR_FM = @DRCTRR_FM;";

            item.FechaModifica = DateTime.Now;
            item.Modifica = this.User;

            var parameters = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@DRCTRR_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCTRR_DRCTR_ID", item.IdDirectorio),
                new SqlSugar.SugarParameter("@DRCTRR_CTREL_ID", item.IdTipoRelacion),
                new SqlSugar.SugarParameter("@DRCTRR_CTCMS_ID", item.IdComision),
                new SqlSugar.SugarParameter("@DRCTRR_CTDSC_ID", 0),
                new SqlSugar.SugarParameter("@DRCTRR_CTPRC_ID", 0),
                new SqlSugar.SugarParameter("@DRCTRR_NOTA", item.Nota),
                new SqlSugar.SugarParameter("@DRCTRR_USR_M", item.Modifica),
                new SqlSugar.SugarParameter("@DRCTRR_FM", item.FechaModifica),
            };
            return this.Db.Ado.ExecuteCommand(sql, parameters);
        }

        public void Saveable(List<IRelacionComercialModel> item) {
            foreach (var relacion in item) {
                this.Saveable(relacion);
            }
        }
    }
}
