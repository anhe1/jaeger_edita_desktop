﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComplementoCartaPorteRepository : MySqlSugarContext<CartaPorteModel>, ISqlComplementoCartaPorteRepository {
        public SqlSugarComplementoCartaPorteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public new CartaPorteDetailModel GetById(int index) {
            return this.Db.Queryable<CartaPorteDetailModel>().Where(it => it.IdComprobante == index).First();
        }

        public CartaPorteDetailModel Save(CartaPorteDetailModel model) {
            if (model.IdCartaPorte == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                var _result = this.Db.Insertable(model);
                model.IdCartaPorte = this.Execute(_result);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                var _result = this.Db.Updateable<CartaPorteModel>(model);
                this.Execute(_result);
            }
            return model;
        }

        public override bool CreateTable() {
            return base.CreateTable();
        }
    }
}
