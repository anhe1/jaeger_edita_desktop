﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Comprobante.Repositories {
    public class SqlSugarComprobanteFiscalConceptoRepository : MySqlSugarContext<ComprobanteConceptoModel>, ISqlComprobanteFiscalConceptoRepository {
        public SqlSugarComprobanteFiscalConceptoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// almacenar un concepto de un comprobante fiscal
        /// </summary>
        /// <param name="concepto">objeto detail ComprobanteConceptoDetailModel</param>
        public ComprobanteConceptoDetailModel Save(ComprobanteConceptoDetailModel concepto) {
            if (concepto.Id == 0) {
                concepto.FechaNuevo = DateTime.Now;
                concepto.Creo = this.User;
                var result = this.Db.Insertable<ComprobanteConceptoModel>(concepto);
                concepto.Id = this.Execute(result);
            } else {
                concepto.FechaModifica = DateTime.Now;
                concepto.Modifica = this.User;
                var result = this.Db.Updateable<ComprobanteConceptoModel>(concepto);
                this.Execute(result);
            }
            return concepto;
        }

        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        public IEnumerable<ComprobanteConceptoDetailModel> GetList(int index) {
            return this.Db.Queryable<ComprobanteConceptoDetailModel>().Where(it => it.SubId == index).Where(it => it.Activo == true).ToList();
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var condiciones = new List<IConditionalModel>();
            foreach (var item in conditionals) {
                condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
            }
            return this.Db.Queryable<T1>().Where(condiciones).ToList();
        }
    }
}
