﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarMediosRepository : Almacen.Repositories.SqlSugarMediosRepository, ISqlMediosRepository {
        public SqlSugarMediosRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion, user) {
            User = user;
        }

        public override int Insert(MedioModel item) {
            var sqlCommand = @"INSERT INTO CTMDLM ( CTMDLM_ID, CTMDLM_A, CTMDLM_CTMDL_ID, CTMDLM_URL)
                                              VALUES (@CTMDLM_ID,@CTMDLM_A,@CTMDLM_CTMDL_ID,@CTMDLM_URL) RETURNING CTMDLM_ID";
            var parameters = new List<SugarParameter> {
                new SugarParameter("@CTMDLM_ID", DBNull.Value),
                new SugarParameter("@CTMDLM_A", item.Activo),
                new SugarParameter("@CTMDLM_CTMDL_ID", item.IdModelo),
                new SugarParameter("@CTMDLM_URL", item.URL)
            };

            item.Id = this.ExecuteScalar(sqlCommand, parameters);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public override int Update(MedioModel item) {
            var sqlCommand = @"UPDATE CTMDLM SET CTMDLM_A = @CTMDLM_A, CTMDLM_CTMDL_ID = @CTMDLM_CTMDL_ID, CTMDLM_URL = @CTMDLM_URL WHERE CTMDLM_ID = @CTMDLM_ID";
            var parameters = new List<SugarParameter> {
                new SugarParameter("@CTMDLM_ID", item.Id),
                new SugarParameter("@CTMDLM_A", item.Activo),
                new SugarParameter("@CTMDLM_CTMDL_ID", item.IdModelo),
                new SugarParameter("@CTMDLM_URL", item.URL)
            };
            return this.ExecuteTransaction(sqlCommand, parameters);
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT * FROM CTMDLM @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
