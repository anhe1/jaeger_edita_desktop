﻿using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarCuponRepository : MySqlSugarContext<CuponModel>, ISqlCuponRepository {
        public SqlSugarCuponRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<CuponModel> GetCupones(bool onlyActive) {
            return this.Db.Queryable<CuponModel>().WhereIF(onlyActive == true, it => it.Activo == true).ToList();
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            return this.GetMapper<T1>(conditionals);
        }

        public CuponModel Save(CuponModel model) {
            if (model.IdCupon == 0) {
                model.Creo = this.User;
                model.FechaNuevo = System.DateTime.Now;
                model.IdCupon = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = System.DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
