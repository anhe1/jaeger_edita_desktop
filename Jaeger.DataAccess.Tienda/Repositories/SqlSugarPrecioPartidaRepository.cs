﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarPrecioPartidaRepository : MySqlSugarContext<PrecioPartidaModel>, ISqlPrecioPartidaRepository {
        public SqlSugarPrecioPartidaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<PrecioPartidaModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = "SELECT * FROM CTPRCP @wcondiciones ORDER BY CTPRCP_ID ASC";
            return this.GetMapper<PrecioPartidaModel>(sqlCommand, conditionals);
        }

        public PrecioPartidaModel Save(PrecioPartidaModel model) {
            if (model.IdPartida == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdPartida = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
