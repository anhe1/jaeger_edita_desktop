﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarModelosRepository : Almacen.Repositories.SqlSugarModelosRepository, Domain.Almacen.Contracts.ISqlModelosRepository, ISqlModelosXRepository {
        public SqlSugarModelosRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion, user) {
        }

        public override bool Delete(int index) {
            var sqlCommand = @"UPDATE CTMDL SET CTMDL_A = 0, CTMDL_USR_M = @CTMDL_USR_M, CTMDL_FM = @CTMDL_FM WHERE (CTMDL_ID = @CTMDL_ID)";
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@CTMDL_USR_M", User),
                new SugarParameter("@CTMDL_FM", DateTime.Now),
                new SugarParameter("@CTMDL_ID", index) 
            };
            return ExecuteTransaction(sqlCommand, parameters) > 0;
        }

        public override int Insert(ModeloModel item) {
            var sqlCommand = @"INSERT INTO CTMDL ( CTMDL_ID, CTMDL_CTCLS_ID, CTMDL_CTPRD_ID, CTMDL_CTPRC_ID, CTMDL_ATRZD, CTMDL_ALM_ID, CTMDL_A, CTMDL_TIPO, CTMDL_VIS, CTMDL_UNDDXY, CTMDL_UNDDZ, CTMDL_UNDDA, CTMDL_RETISR, CTMDL_RETISRF, CTMDL_RETIVA, CTMDL_RETIVAF, CTMDL_TRSIVA, CTMDL_TRSIVAF, CTMDL_RETIEPS, CTMDL_RETIEPSF, CTMDL_TRSIEPS, CTMDL_TRSIEPSF, CTMDL_UNTR, CTMDL_LARGO, CTMDL_ANCHO, CTMDL_ALTO, CTMDL_PESO, CTMDL_CLVUND, CTMDL_CLVPRDS, CTMDL_CLVOBJ, CTMDL_CDG, CTMDL_UNDD, CTMDL_CTAPRE, CTMDL_SKU, CTMDL_NUMREQ, CTMDL_CLV, CTMDL_MRC, CTMDL_ESPC, CTMDL_DSCRC, CTMDL_ETQTS, CTMDL_DSCR, CTMDL_USR_N, CTMDL_FN, CTMDL_SEC, CTMDL_DIS)
                                          VALUES (@CTMDL_ID,@CTMDL_CTCLS_ID,@CTMDL_CTPRD_ID,@CTMDL_CTPRC_ID,@CTMDL_ATRZD,@CTMDL_ALM_ID,@CTMDL_A,@CTMDL_TIPO,@CTMDL_VIS,@CTMDL_UNDDXY,@CTMDL_UNDDZ,@CTMDL_UNDDA,@CTMDL_RETISR,@CTMDL_RETISRF,@CTMDL_RETIVA,@CTMDL_RETIVAF,@CTMDL_TRSIVA,@CTMDL_TRSIVAF,@CTMDL_RETIEPS,@CTMDL_RETIEPSF,@CTMDL_TRSIEPS,@CTMDL_TRSIEPSF,@CTMDL_UNTR,@CTMDL_LARGO,@CTMDL_ANCHO,@CTMDL_ALTO,@CTMDL_PESO,@CTMDL_CLVUND,@CTMDL_CLVPRDS,@CTMDL_CLVOBJ,@CTMDL_CDG,@CTMDL_UNDD,@CTMDL_CTAPRE,@CTMDL_SKU,@CTMDL_NUMREQ,@CTMDL_CLV,@CTMDL_MRC,@CTMDL_ESPC,@CTMDL_DSCRC,@CTMDL_ETQTS,@CTMDL_DSCR,@CTMDL_USR_N,@CTMDL_FN,@CTMDL_SEC,@CTMDL_DIS) RETURNING CTMDL_ID";

            var parameters = new List<SugarParameter>() {
                new SugarParameter("@CTMDL_ID", DBNull.Value),
                new SugarParameter("@CTMDL_CTCLS_ID", item.IdCategoria),
                new SugarParameter("@CTMDL_CTPRD_ID", item.IdProducto),
                new SugarParameter("@CTMDL_CTPRC_ID", item.IdPrecio),
                new SugarParameter("@CTMDL_ATRZD", item.Autorizado),
                new SugarParameter("@CTMDL_ALM_ID", item.IdAlmacen),
                new SugarParameter("@CTMDL_A", item.Activo),
                new SugarParameter("@CTMDL_SEC", item.Secuencia),
                new SugarParameter("@CTMDL_TIPO", item.IdTipo),
                new SugarParameter("@CTMDL_VIS", item.IdVisible),
                new SugarParameter("@CTMDL_DIS", item.IsAvailable),
                new SugarParameter("@CTMDL_UNDDXY", item.UnidadXY),
                new SugarParameter("@CTMDL_UNDDZ", item.UnidadZ),
                new SugarParameter("@CTMDL_UNDDA", item.IdUnidad),
                new SugarParameter("@CTMDL_RETISR", item.ValorRetecionISR),
                new SugarParameter("@CTMDL_RETISRF", DBNull.Value),
                new SugarParameter("@CTMDL_RETIVA", item.ValorRetencionIVA),
                new SugarParameter("@CTMDL_RETIVAF", DBNull.Value),
                new SugarParameter("@CTMDL_TRSIVA", item.ValorTrasladoIVA),
                new SugarParameter("@CTMDL_TRSIVAF", item.FactorTrasladoIVA),
                new SugarParameter("@CTMDL_RETIEPS", item.ValorRetencionIEPS),
                new SugarParameter("@CTMDL_RETIEPSF", item.FactorRetencionIEPS),
                new SugarParameter("@CTMDL_TRSIEPS", item.ValorTrasladoIEPS),
                new SugarParameter("@CTMDL_TRSIEPSF", item.FactorTrasladoIEPS),
                new SugarParameter("@CTMDL_UNTR", item.Unitario),
                new SugarParameter("@CTMDL_LARGO", item.Largo),
                new SugarParameter("@CTMDL_ANCHO", item.Ancho),
                new SugarParameter("@CTMDL_ALTO", item.Alto),
                new SugarParameter("@CTMDL_PESO", item.Peso),
                new SugarParameter("@CTMDL_CLVUND", item.ClaveUnidad),
                new SugarParameter("@CTMDL_CLVPRDS", item.ClaveProdServ),
                new SugarParameter("@CTMDL_CLVOBJ", item.ObjetoImp),
                new SugarParameter("@CTMDL_CDG", item.Codigo),
                new SugarParameter("@CTMDL_UNDD", item.Unidad),
                new SugarParameter("@CTMDL_CTAPRE", item.CtaPredial),
                new SugarParameter("@CTMDL_SKU", item.NoIdentificacion),
                new SugarParameter("@CTMDL_NUMREQ", item.NumRequerimiento),
                new SugarParameter("@CTMDL_CLV", item.Clave),
                new SugarParameter("@CTMDL_MRC", item.Marca),
                new SugarParameter("@CTMDL_ESPC", item.Especificacion),
                new SugarParameter("@CTMDL_DSCRC", item.Descripcion),
                //new SugarParameter("@CTMDL_ETQTS", item.Etiquetas),
                //new SugarParameter("@CTMDL_DSCR", item.DescripcionLarga),
                new SugarParameter("@CTMDL_USR_N", item.Creo),
                new SugarParameter("@CTMDL_FN", item.FechaNuevo)
            };
            item.IdModelo = ExecuteScalar(sqlCommand, parameters);
            if (item.IdModelo > 0)
                return item.IdModelo;
            return 0;
        }

        public override int Update(ModeloModel item) {
            var sqlCommand = @"UPDATE CTMDL SET CTMDL_CTCLS_ID = @CTMDL_CTCLS_ID, CTMDL_CTPRD_ID = @CTMDL_CTPRD_ID, CTMDL_CTPRC_ID = @CTMDL_CTPRC_ID, CTMDL_ATRZD = @CTMDL_ATRZD, CTMDL_ALM_ID = @CTMDL_ALM_ID, CTMDL_A = @CTMDL_A, 
                CTMDL_TIPO = @CTMDL_TIPO, CTMDL_VIS = @CTMDL_VIS, CTMDL_UNDDXY = @CTMDL_UNDDXY, CTMDL_UNDDZ = @CTMDL_UNDDZ, CTMDL_UNDDA = @CTMDL_UNDDA, CTMDL_RETISR = @CTMDL_RETISR, CTMDL_RETISRF = @CTMDL_RETISRF, 
                CTMDL_RETIVA = @CTMDL_RETIVA, CTMDL_RETIVAF = @CTMDL_RETIVAF, CTMDL_TRSIVA = @CTMDL_TRSIVA, CTMDL_TRSIVAF = @CTMDL_TRSIVAF, CTMDL_RETIEPS = @CTMDL_RETIEPS, CTMDL_RETIEPSF = @CTMDL_RETIEPSF, 
                CTMDL_TRSIEPS = @CTMDL_TRSIEPS, CTMDL_TRSIEPSF = @CTMDL_TRSIEPSF, CTMDL_UNTR = @CTMDL_UNTR, CTMDL_LARGO = @CTMDL_LARGO, CTMDL_ANCHO = @CTMDL_ANCHO, CTMDL_ALTO = @CTMDL_ALTO, 
                CTMDL_PESO = @CTMDL_PESO, CTMDL_CLVUND = @CTMDL_CLVUND, CTMDL_CLVPRDS = @CTMDL_CLVPRDS, CTMDL_CLVOBJ = @CTMDL_CLVOBJ, CTMDL_CDG = @CTMDL_CDG, CTMDL_UNDD = @CTMDL_UNDD, CTMDL_CTAPRE = @CTMDL_CTAPRE, 
                CTMDL_SKU = @CTMDL_SKU, CTMDL_NUMREQ = @CTMDL_NUMREQ, CTMDL_CLV = @CTMDL_CLV, CTMDL_MRC = @CTMDL_MRC, CTMDL_ESPC = @CTMDL_ESPC, CTMDL_DSCRC = @CTMDL_DSCRC, CTMDL_ETQTS = @CTMDL_ETQTS, 
                CTMDL_DSCR = @CTMDL_DSCR, CTMDL_USR_M = @CTMDL_USR_M, CTMDL_FM = @CTMDL_FM, CTMDL_SEC = @CTMDL_SEC, CTMDL_DIS = @CTMDL_DIS WHERE (CTMDL_ID = @CTMDL_ID)";

            var parameters = new List<SugarParameter> {
                new SugarParameter("@CTMDL_ID", item.IdModelo),
                new SugarParameter("@CTMDL_CTCLS_ID", item.IdCategoria),
                new SugarParameter("@CTMDL_CTPRD_ID", item.IdProducto),
                new SugarParameter("@CTMDL_CTPRC_ID", item.IdPrecio),
                new SugarParameter("@CTMDL_ATRZD", item.Autorizado),
                new SugarParameter("@CTMDL_ALM_ID", item.IdAlmacen),
                new SugarParameter("@CTMDL_A", item.Activo),
                new SugarParameter("@CTMDL_SEC", item.Secuencia),
                new SugarParameter("@CTMDL_TIPO", item.IdTipo),
                new SugarParameter("@CTMDL_VIS", item.IdVisible),
                new SugarParameter("@CTMDL_DIS", item.IsAvailable),
                new SugarParameter("@CTMDL_UNDDXY", item.UnidadXY),
                new SugarParameter("@CTMDL_UNDDZ", item.UnidadZ),
                new SugarParameter("@CTMDL_UNDDA", item.IdUnidad),
                new SugarParameter("@CTMDL_RETISR", item.ValorRetecionISR),
                new SugarParameter("@CTMDL_RETISRF", DBNull.Value),
                new SugarParameter("@CTMDL_RETIVA", item.ValorRetencionIVA),
                new SugarParameter("@CTMDL_RETIVAF", DBNull.Value),
                new SugarParameter("@CTMDL_TRSIVA", item.ValorTrasladoIVA),
                new SugarParameter("@CTMDL_TRSIVAF", item.FactorTrasladoIVA),
                new SugarParameter("@CTMDL_RETIEPS", item.ValorRetencionIEPS),
                new SugarParameter("@CTMDL_RETIEPSF", item.FactorRetencionIEPS),
                new SugarParameter("@CTMDL_TRSIEPS", item.ValorTrasladoIEPS),
                new SugarParameter("@CTMDL_TRSIEPSF", item.FactorTrasladoIEPS),
                new SugarParameter("@CTMDL_UNTR", item.Unitario),
                new SugarParameter("@CTMDL_LARGO", item.Largo),
                new SugarParameter("@CTMDL_ANCHO", item.Ancho),
                new SugarParameter("@CTMDL_ALTO", item.Alto),
                new SugarParameter("@CTMDL_PESO", item.Peso),
                new SugarParameter("@CTMDL_CLVUND", item.ClaveUnidad),
                new SugarParameter("@CTMDL_CLVPRDS", item.ClaveProdServ),
                new SugarParameter("@CTMDL_CLVOBJ", item.ObjetoImp),
                new SugarParameter("@CTMDL_CDG", item.Codigo),
                new SugarParameter("@CTMDL_UNDD", item.Unidad),
                new SugarParameter("@CTMDL_CTAPRE", item.CtaPredial),
                new SugarParameter("@CTMDL_SKU", item.NoIdentificacion),
                new SugarParameter("@CTMDL_NUMREQ", item.NumRequerimiento),
                new SugarParameter("@CTMDL_CLV", item.Clave),
                new SugarParameter("@CTMDL_MRC", item.Marca),
                new SugarParameter("@CTMDL_ESPC", item.Especificacion),
                new SugarParameter("@CTMDL_DSCRC", item.Descripcion),
                //new SugarParameter("@CTMDL_ETQTS", item.Etiquetas),
                //new SugarParameter("@CTMDL_DSCR", item.DescripcionLarga),
                new SugarParameter("@CTMDL_USR_M", item.Modifica),
                new SugarParameter("@CTMDL_FM", item.FechaModifica)
            };
            return ExecuteTransaction(sqlCommand, parameters);
        }

        public override ModeloModel GetById(int index) {
            return GetList<ModeloModel>(new List<IConditional>() { new Conditional("CTMDL_ID", index.ToString()) }).FirstOrDefault();
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT * FROM CTMDL @wcondiciones";
            return GetMapper<T1>(sqlCommand, conditionals);
        }

        public ModeloXDetailModel Save(ModeloXDetailModel model) {
            if (model.IdModelo == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdModelo = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }
    }
}
