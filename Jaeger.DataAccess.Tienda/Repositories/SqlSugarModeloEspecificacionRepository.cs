﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarModeloEspecificacionRepository : MySqlSugarContext<ModeloYEspecificacionModel> , ISqlModeloEspecificacionRepository{
        public SqlSugarModeloEspecificacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {

        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT * FROM CTMDLE @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public ModeloYEspecificacionModel Save(ModeloYEspecificacionModel model) {
            throw new NotImplementedException();
        }
    }
}
