﻿using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarPrecioRepository : MySqlSugarContext<PrecioModel>, ISqlPrecioRepository {
        public SqlSugarPrecioRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<PrecioDetailModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = "SELECT * FROM CTPRC @wcondiciones";
            return this.GetMapper<PrecioDetailModel>(sqlCommand, conditionals);
        }

        public IEnumerable<LPrecioModel> GetList(int idCliente, int idCatalogo = 0) {
            var sqlCommand = "CALL LPRECIO(@idCatalogo, @idCliente)";
            sqlCommand = sqlCommand.Replace("@idCatalogo", idCatalogo.ToString());
            sqlCommand = sqlCommand.Replace("@idCliente", idCliente.ToString());
            return this.GetMapper<LPrecioModel>(sqlCommand, new List<SqlSugar.SugarParameter>());
        }

        public PrecioDetailModel Save(PrecioDetailModel model) {
            if (model.IdPrecio == 0)
                model.IdPrecio = this.Insert(model);
            else
                this.Update(model);
            return model;
        }
    }
}
