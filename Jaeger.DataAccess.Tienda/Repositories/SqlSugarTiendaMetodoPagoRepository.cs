﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarTiendaMetodoPagoRepository : MySqlSugarContext<TiendaMetodoPagoModel>, ISqlTiendaMetodoPagoRepository {
        public SqlSugarTiendaMetodoPagoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }
    }
}
