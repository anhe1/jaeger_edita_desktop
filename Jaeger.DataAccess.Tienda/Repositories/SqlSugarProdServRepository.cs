﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarProdServRepository : Almacen.Repositories.SqlSugarProdServRepository, ISqlProdServXRepository {
        public SqlSugarProdServRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion, user) {

        }

        public int Delete(ProductoServicioModel item) {
            throw new NotImplementedException();
        }

        public override int Insert(ProductoServicioModel item) {
            var sqlCommand = @"INSERT INTO CTPRD (CTPRD_ID, CTPRD_A, CTPRD_ALM_ID, CTPRD_CTCLS_ID, CTPRD_TIPO, CTPRD_CLV, CTPRD_NOM, CTPRD_USR_N, CTPRD_FN, CTPRD_SEC) 
                                          VALUES (@CTPRD_ID,@CTPRD_A,@CTPRD_ALM_ID,@CTPRD_CTCLS_ID,@CTPRD_TIPO,@CTPRD_CLV,@CTPRD_NOM,@CTPRD_USR_N,@CTPRD_FN,@CTPRD_SEC) RETURNING CTPRD_ID;";
            var parameters = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@CTPRD_ID", DBNull.Value),
                new SqlSugar.SugarParameter("@CTPRD_A", item.Activo),
                new SqlSugar.SugarParameter("@CTPRD_ALM_ID", item.IdAlmacen),
                new SqlSugar.SugarParameter("@CTPRD_CTCLS_ID", item.IdCategoria),
                new SqlSugar.SugarParameter("@CTPRD_TIPO", item.IdTipo),
                new SqlSugar.SugarParameter("@CTPRD_CLV", item.Clave),
                new SqlSugar.SugarParameter("@CTPRD_NOM", item.Nombre),
                new SqlSugar.SugarParameter("@CTPRD_USR_N", item.Creo),
                new SqlSugar.SugarParameter("@CTPRD_FN", item.FechaNuevo),
                new SqlSugar.SugarParameter("@CTPRD_SEC", item.Secuencia)
            };

            item.IdProducto = this.ExecuteScalar(sqlCommand, parameters);
            if (item.IdProducto > 0)
                return item.IdProducto;
            return 0;
        }

        public override int Update(ProductoServicioModel item) {
            var sqlCommand = @"UPDATE CTPRD SET CTPRD_A = @CTPRD_A, CTPRD_ALM_ID = @CTPRD_ALM_ID, CTPRD_CTCLS_ID = @CTPRD_CTCLS_ID, CTPRD_TIPO = @CTPRD_TIPO, CTPRD_CLV = @CTPRD_CLV, CTPRD_NOM = @CTPRD_NOM, CTPRD_USR_N = @CTPRD_USR_N, CTPRD_FN = @CTPRD_FN, CTPRD_SEC = @CTPRD_SEC WHERE (CTPRD_ID = @CTPRD_ID)";
            var parameters = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@CTPRD_ID", item.IdProducto),
                new SqlSugar.SugarParameter("@CTPRD_A", item.Activo),
                new SqlSugar.SugarParameter("@CTPRD_ALM_ID", item.IdAlmacen),
                new SqlSugar.SugarParameter("@CTPRD_CTCLS_ID", item.IdCategoria),
                new SqlSugar.SugarParameter("@CTPRD_TIPO", item.IdTipo),
                new SqlSugar.SugarParameter("@CTPRD_CLV", item.Clave),
                new SqlSugar.SugarParameter("@CTPRD_NOM", item.Nombre),
                new SqlSugar.SugarParameter("@CTPRD_USR_N", item.Creo),
                new SqlSugar.SugarParameter("@CTPRD_FN", item.FechaNuevo),
                new SqlSugar.SugarParameter("@CTPRD_SEC", item.Secuencia)
            };
            return this.ExecuteTransaction(sqlCommand,parameters);
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT * FROM CTPRD @wcondiciones";

            var result = GetMapper<T1>(sqlCommand, conditionals).ToList();
            return result;
        }

        public ProductoServicioXDetailModel Save(ProductoServicioXDetailModel model) {
            if (model.IdProducto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdProducto = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
