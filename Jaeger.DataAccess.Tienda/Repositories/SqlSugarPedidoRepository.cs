﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarPedidoRepository : Abstractions.MySqlSugarContext<PedidoClienteModel>, ISqlPedidoClienteRepository {
        public SqlSugarPedidoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Cancelar(IPedidoClienteStatusDetailModel model) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<PedidoClienteDetailModel> GetList(List<IConditional> condiciones) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<PedidoClientePrinter> GetListE(string sql, List<IConditional> condiciones) {
            throw new System.NotImplementedException();
        }

        public PedidoClienteDetailModel GetPedido(int index) {
            throw new System.NotImplementedException();
        }

        public PedidoClienteDetailModel Save(PedidoClienteDetailModel model) {
            throw new System.NotImplementedException();
        }
    }
}
