﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    /// <summary>
    /// repositorio: tienda web seccion preguntas frecuentes
    /// </summary>
    public class SqlSugarFAQRepository : MySqlSugarContext<PreguntaFrecuenteModel>, ISqlFAQRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlSugarFAQRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public PreguntaFrecuenteModel Save(PreguntaFrecuenteModel model) {
            model.Creo = this.User;
            model.FechaNuevo = DateTime.Now;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = "SELECT * FROM TWFAQ @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
