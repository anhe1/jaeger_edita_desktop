﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarUnidadRepository: Jaeger.DataAccess.Almacen.Repositories.SqlSugarUnidadRepository, ISqlUnidadRepository {
        public SqlSugarUnidadRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT CTUND.* FROM CTUND @wcondiciones";
            return GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
