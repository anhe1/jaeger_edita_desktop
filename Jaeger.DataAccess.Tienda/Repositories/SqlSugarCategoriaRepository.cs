﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Tienda.Repositories {
    public class SqlSugarCategoriaRepository : MySqlSugarContext<CategoriaModel>, Domain.Almacen.Contracts.ISqlCategoriaRepository {
        public SqlSugarCategoriaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        //public IEnumerable<CategoriaProductoTerminado> GetCategorias() {
        //    var sqlCommand = @"SELECT C2.CTCLS_ID AS IDCATEGORIA, C1.CTCLS_ID AS SUBID, C2.CTCLS_A AS ACTIVO, C1.CTCLS_ALM_ID IDALMACEN,
        //                        CONCAT(IF (C1.CTCLS_CLASS1 = '/', '', IF(C1.CTCLS_CLASS1 = '/', C2.CTCLS_CLASS1, CONCAT('/' , C1.CTCLS_CLASS1))) , '/' , IF (C2.CTCLS_CLASS1 = '', C1.CTCLS_CLASS1, C2.CTCLS_CLASS1)) AS TIPO,
        //                        CONCAT(IF (C1.CTCLS_CLASS2 = '/', '', IF(C1.CTCLS_CLASS2 = '/', C2.CTCLS_CLASS2, CONCAT('/' , C1.CTCLS_CLASS2))) , '/' , IF (C2.CTCLS_CLASS2 = '', C1.CTCLS_CLASS2, C2.CTCLS_CLASS2)) AS DESCRIPCION,
        //                        IF (C2.CTCLS_CLASS2 = '', C1.CTCLS_CLASS2, C2.CTCLS_CLASS2) AS CLASE
        //                    FROM CTCLS C1 INNER JOIN CTCLS C2 ON (C2.CTCLS_SBID = C1.CTCLS_ID)
        //                    ORDER BY C2.CTCLS_SEC ASC, DESCRIPCION";

        //    return GetMapper<CategoriaProductoTerminado>(sqlCommand);
        //}

        //public IEnumerable<CategoriaProductoTerminadoSingle> GetCatProductos() {
        //    var sqlCommand = @"SELECT CTPRD.CTPRD_ID AS IDPRODUCTO,CTPRD.CTPRD_CTCLS_ID AS IDCATEGORIA, C5.SUBID, CTPRD.CTPRD_A AS ACTIVO, CTPRD.CTPRD_ALM_ID AS IDALMACEN,C5.TIPO, C5.DESCRIPCION, C5.CLASE, CTPRD.CTPRD_NOM
        //                        FROM CTPRD 
        //                        LEFT JOIN (
        //                            SELECT C2.CTCLS_ID AS IDCATEGORIA, C1.CTCLS_ID AS SUBID, C2.CTCLS_A AS ACTIVO, C1.CTCLS_ALM_ID IDALMACEN,
        //                            CONCAT(IF (C1.CTCLS_CLASS1 = '/', '', IF(C1.CTCLS_CLASS1 = '/', C2.CTCLS_CLASS1, CONCAT('/' , C1.CTCLS_CLASS1))) , '/' , IF (C2.CTCLS_CLASS1 = '', C1.CTCLS_CLASS1, C2.CTCLS_CLASS1)) AS TIPO,
        //                            CONCAT(IF (C1.CTCLS_CLASS2 = '/', '', IF(C1.CTCLS_CLASS2 = '/', C2.CTCLS_CLASS2, CONCAT('/' , C1.CTCLS_CLASS2))) , '/' , IF (C2.CTCLS_CLASS2 = '', C1.CTCLS_CLASS2, C2.CTCLS_CLASS2)) AS DESCRIPCION,
        //                            IF (C2.CTCLS_CLASS2 = '', C1.CTCLS_CLASS2, C2.CTCLS_CLASS2) AS CLASE
        //                            FROM CTCLS C1 INNER JOIN CTCLS C2 ON (C2.CTCLS_SBID = C1.CTCLS_ID)
        //                        ) C5 ON C5.IDCATEGORIA = CTPRD.CTPRD_CTCLS_ID 
        //                        WHERE CTPRD.CTPRD_A > 0";

        //    return GetMapper<CategoriaProductoTerminadoSingle>(sqlCommand);
        //}
    }
}
