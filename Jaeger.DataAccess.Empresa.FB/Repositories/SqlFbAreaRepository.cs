﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.Empresa.Repositories {
    public class SqlFbAreaRepository : RepositoryMaster<AreaModel>, ISqlAreaRepository {
        public SqlFbAreaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTAREA WHERE CTAREA_ID = @CTAREA_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@CTAREA_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public AreaModel GetById(int index) {
            return this.GetList<AreaModel>(new List<IConditional>() { new Conditional("CTAREA_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<AreaModel> GetList() {
            return this.GetList<AreaModel>(new List<IConditional>());
        }

        public int Insert(AreaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTAREA (CTAREA_ID, CTAREA_CTCAT_ID, CTAREA_CNTBL_ID, CTAREA_NOM, CTAREA_RESP, CTAREA_FN, CTAREA_USR_N)
                                           VALUES (@CTAREA_ID,@CTAREA_CTCAT_ID,@CTAREA_CNTBL_ID,@CTAREA_NOM,@CTAREA_RESP,@CTAREA_FN,@CTAREA_USR_N) RETURNING CTAREA_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@CTAREA_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTAREA_CTCAT_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTAREA_CNTBL_ID", item.CtaContable);
            sqlCommand.Parameters.AddWithValue("@CTAREA_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTAREA_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@CTAREA_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTAREA_USR_N", item.Creo);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(AreaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTAREA SET CTAREA_CTCAT_ID = @CTAREA_CTCAT_ID, CTAREA_CNTBL_ID = @CTAREA_CNTBL_ID, CTAREA_NOM = @CTAREA_NOM, CTAREA_RESP = @CTAREA_RESP, CTAREA_FN = @CTAREA_FN, CTAREA_USR_N = CTAREA_USR_N WHERE CTAREA_ID = @CTAREA_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@CTAREA_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTAREA_CTCAT_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTAREA_CNTBL_ID", item.CtaContable);
            sqlCommand.Parameters.AddWithValue("@CTAREA_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTAREA_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@CTAREA_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTAREA_USR_N", item.Creo);
            return ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTAREA.* FROM CTAREA @wcondiciones"
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public AreaModel Save(AreaModel model) {
            model.FechaNuevo = DateTime.Now;
            model.Creo = this.User;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
