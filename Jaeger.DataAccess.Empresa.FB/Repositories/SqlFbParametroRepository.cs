﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Empresa.Enums;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Empresa.Repositories {
    /// <summary>
    /// repositorio de parametros
    /// </summary>
    public class SqlFbParametroRepository : RepositoryMaster<ParametroModel>, ISqlParametroRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        public SqlFbParametroRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) { }

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="group">Llave de grupo</param>
        public IEnumerable<IParametroModel> Get(ConfigGroupEnum group) {
            var d1 = (int)group;
            var d0 = new List<Conditional> {
                new Conditional("CONF_GRP", d1.ToString())
            };
            return this.GetList<ParametroModel>(d0);
        }

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="groups">array de llave de grupos</param>
        public IEnumerable<IParametroModel> Get(ConfigGroupEnum[] groups) {
            int[] result = Array.ConvertAll(groups, value => (int)value);
            var d0 = new List<Conditional> {
                new Conditional("CONF_GRP", string.Join(",", result), ConditionalTypeEnum.In)
            };
            return this.GetList<ParametroModel>(d0);
        }

        /// <summary>
        /// obtener lista de llaves 
        /// </summary>
        /// <param name="keys">array de indices de llaves</param>
        public IEnumerable<IParametroModel> Get(ConfigKeyEnum[] keys) {
            int[] result = Array.ConvertAll(keys, value => (int)value);
            var d0 = new List<Conditional> {
                new Conditional("CONF_KEY", string.Join(",", result), ConditionalTypeEnum.In)
            };
            return this.GetList<ParametroModel>(d0);
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CONF_KEY, CONF_GRP, CONF_DATA FROM CONF @wcondiciones"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            var result = this.GetMapper<T1>(sqlCommand).ToList();
            return result;
        }

        /// <summary>
        /// almacenar parametro
        /// </summary>
        /// <param name="parameter">IParameter</param>
        /// <returns>verdadero</returns>
        public bool Save(IParametroModel parameter) {
            var sqlCommand = new FbCommand() {
                CommandText = "UPDATE OR INSERT INTO CONF (CONF_KEY, CONF_GRP, CONF_DATA) VALUES(@CONF_KEY, @CONF_GRP, @CONF_DATA) MATCHING(CONF_KEY, CONF_GRP)"
            };
            sqlCommand.Parameters.AddWithValue("@CONF_KEY", parameter.Key);
            sqlCommand.Parameters.AddWithValue("@CONF_GRP", parameter.Group);
            sqlCommand.Parameters.AddWithValue("@CONF_DATA", parameter.Data);

            var response = this.ExecuteScalar(sqlCommand);
            return response > 0;
        }

        /// <summary>
        /// almacenar lista de parametros
        /// </summary>
        /// <param name="parametros">Lista de parametros</param>
        public bool Save(List<IParametroModel> parameters) {
            var ounter = 0;
            foreach (var parameter in parameters) {
                if (this.Save(parameter)) {
                    ounter++;
                }
            }

            return parameters.Count() == ounter;
        }
    }
}
