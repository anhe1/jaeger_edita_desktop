﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.Empresa.Repositories {
    /// <summary>
    /// repositorio para departamentos
    /// </summary>
    public class SqlFbDepartamentoRepository : RepositoryMaster<DepartamentoModel>, ISqlDepartamentoRepository {
        public SqlFbDepartamentoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(DepartamentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTDPT ( CTDPT_ID, CTDPT_A, CTDPT_CTCAT_ID, CTDPT_CTAREA_ID, CTDPT_CNTBL_ID, CTDPT_NOM, CTDPT_RESP, CTDPT_FN, CTDPT_USR_N)
                                           VALUES (@CTDPT_ID,@CTDPT_A,@CTDPT_CTCAT_ID,@CTDPT_CTAREA_ID,@CTDPT_CNTBL_ID,@CTDPT_NOM,@CTDPT_RESP,@CTDPT_FN,@CTDPT_USR_N) RETURNING CTDPT_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@CTDPT_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTDPT_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTDPT_CTCAT_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTDPT_CTAREA_ID", item.IdArea);
            sqlCommand.Parameters.AddWithValue("@CTDPT_CNTBL_ID", item.CtaContable);
            sqlCommand.Parameters.AddWithValue("@CTDPT_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTDPT_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@CTDPT_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTDPT_USR_N", item.Creo);
            item.IdDepartamento = this.ExecuteScalar(sqlCommand);
            if (item.IdDepartamento > 0)
                return item.IdDepartamento;
            return 0;
        }

        public int Update(DepartamentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTDPT SET CTDPT_A=@CTDPT_A,CTDPT_CTCAT_ID=@CTDPT_CTCAT_ID,CTDPT_CTAREA_ID=@CTDPT_CTAREA_ID,CTDPT_CNTBL_ID=@CTDPT_CNTBL_ID,CTDPT_NOM=@CTDPT_NOM,CTDPT_RESP=@CTDPT_RESP,CTDPT_FN=@CTDPT_FN,CTDPT_USR_N=@CTDPT_USR_N WHERE CTDPT_ID=@CTDPT_ID"
            };

            sqlCommand.Parameters.AddWithValue("@CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@CTDPT_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTDPT_CTCAT_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTDPT_CTAREA_ID", item.IdArea);
            sqlCommand.Parameters.AddWithValue("@CTDPT_CNTBL_ID", item.CtaContable);
            sqlCommand.Parameters.AddWithValue("@CTDPT_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTDPT_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@CTDPT_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTDPT_USR_N", item.Creo);
            return ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTDPT SET CTDPT_A=0 WHERE CTDPT_ID=@CTDPT_ID"
            };

            sqlCommand.Parameters.AddWithValue("@CTDPT_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public IEnumerable<DepartamentoModel> GetList() {
            return this.GetList<DepartamentoModel>(new List<IConditional>());
        }

        public DepartamentoModel GetById(int index) {
            return this.GetList<DepartamentoModel>(new List<IConditional>() { new Conditional("CTDPT_ID", index.ToString()) }).FirstOrDefault();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTDPT.* FROM CTDPT @wcondiciones"
            };
           
            var result = this.GetMapper<T1>(sqlCommand, conditionals).ToList();
            return result;
        }

        public DepartamentoModel Save(DepartamentoModel departamento) {
            if (departamento.IdDepartamento == 0) {
                departamento.Creo = this.User;
                departamento.FechaNuevo = DateTime.Now;
                departamento.IdDepartamento = this.Insert(departamento);
            } else {
                departamento.Creo = this.User;
                departamento.FechaNuevo = DateTime.Now;
                this.Update(departamento);
            }
            return departamento;
        }
    }
}
