﻿namespace Jaeger.Aplication.Comprobante.Printer {
    public class CatalogosNominaService {
        protected internal Catalogos.Contracts.ITipoContratoCatalogo TipoContratoCatalogo;
        public CatalogosNominaService() {
            this.TipoContratoCatalogo = new Catalogos.Repositories.TipoContratoCatalogo();
            this.TipoContratoCatalogo.Load();
        }
    }
}
