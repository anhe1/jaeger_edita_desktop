﻿using System;
using System.IO;
using System.Text;
using System.Drawing;
using ThoughtWorks.QRCode.Codec.Data;
using ThoughtWorks.QRCode.Codec;
using System.Drawing.Imaging;

namespace Jaeger.Aplication.Comprobante.Printer {
    public class QRCodeExtensions {
        public QRCodeExtensions() {
        }

        /// <summary>
        /// configuracion para version 3.3
        /// </summary>
        public static Image GetCodigoQR(string url, string emisor, string receptor, string total, string uuid, string sello) {
            Image image = null;
            int num = 2;
            string str = num.ToString();
            var qRCodeEncoder = new QRCodeEncoder() {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = num,
                QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M
            };

            if (str == "Alto (25%)") {
                qRCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
            }
            qRCodeEncoder.QRCodeVersion = 0;

            try {
                string[] contenido = { url, "&id=", uuid, "&re=", emisor, "&rr=", receptor, "&tt=", total, "&fe=", sello.Substring(sello.Length - 8, 8) };
                image = qRCodeEncoder.Encode(string.Concat(contenido), Encoding.UTF8);
            } catch (Exception) {
                throw;
            }
            return image;
        }

        public static Image GetCodigoQR(string emisor, string receptor, string total, string uuid) {
            Image image = null;
            int num = 2;
            string str = num.ToString();
            var qRCodeEncoder = new QRCodeEncoder() {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = num,
                QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M
            };

            if (str == "Alto (25%)") {
                qRCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
            }
            qRCodeEncoder.QRCodeVersion = 0;

            try {
                string[] strArrays = new string[] { "?re=", emisor, "&rr=", receptor, "&tt=", total, "&id=", uuid };
                string str1 = string.Concat(strArrays);
                image = qRCodeEncoder.Encode(str1, Encoding.UTF8);
            } catch (Exception) {
                throw;
            }
            return image;
        }

        public static Image GetQRRemision(string emisor, string receptor, string total, string folio) {
            Image image = null;
            int num = 2;
            string str = num.ToString();
            var qRCodeEncoder = new QRCodeEncoder() {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = num,
                QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q
            };

            if (str == "Alto (25%)") {
                qRCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
            }

            qRCodeEncoder.QRCodeVersion = 0;

            try {
                string[] strArrays = new string[] { "?re=", emisor, "&rr=", receptor, "&tt=", total, "&folio=", folio };
                string str1 = string.Concat(strArrays);
                image = qRCodeEncoder.Encode(str1, Encoding.UTF8);
            } catch (Exception) {
                throw;
            }
            return image;
        }

        public static Image GetQRImage(string contenido) {
            Image image = null;
            var qRCodeEncoder = new QRCodeEncoder() {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = 2,
                QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q
            };
            qRCodeEncoder.QRCodeVersion = 0;

            try {
                image = qRCodeEncoder.Encode(contenido, Encoding.UTF8);
            } catch (Exception) {
                throw;
            }
            return image;
        }

        public static byte[] GetQRBytes(string[] contenido) {
            return QRCodeExtensions.CopyImageToByteArray(QRCodeExtensions.GetQRImage(string.Concat(contenido)));
        }

        public static string GetQRBase64(string[] contenido) {
            return Convert.ToBase64String(GetQRBytes(contenido));
        }

        public static string[] QrDecode(Image img) {
            string[] strArray;
            var qRCodeDecoder = new QRCodeDecoder();
            var qRCodeBitmapImage = new QRCodeBitmapImage(new Bitmap(img));
            string empty = string.Empty;
            string[] strArray1 = null;
            try {
                if (img.Width == img.Height) {
                    string str = qRCodeDecoder.decode(qRCodeBitmapImage);
                    if (str.Length > 0) {
                        str = str.Replace('?', ' ');
                        str = str.TrimStart(new char[0]);
                        char[] chrArray = new char[] { '&' };
                        string[] strArray2 = str.Split(chrArray);
                        if (strArray2.Length == 4) {
                            string str1 = strArray2[0];
                            chrArray = new char[] { '=' };
                            string str2 = str1.Split(chrArray)[1];
                            string str3 = strArray2[1];
                            chrArray = new char[] { '=' };
                            empty = str3.Split(chrArray)[1];
                            string str4 = strArray2[2];
                            chrArray = new char[] { '=' };
                            string str5 = str4.Split(chrArray)[1];
                            chrArray = new char[] { '0' };
                            str5.TrimStart(chrArray);
                            string str6 = strArray2[3];
                            chrArray = new char[] { '=' };
                            string upper = str6.Split(chrArray)[1].Trim().ToUpper();
                            strArray1 = new string[] { str2, empty, upper };
                        }
                    }
                } else {
                    strArray = null;
                    return strArray;
                }
            } catch {
            }
            strArray = strArray1;
            return strArray;
        }

        public static Byte[] CopyImageToByteArray(Image theImage) {
            try {
                using (MemoryStream memoryStream = new MemoryStream()) {
                    theImage.Save(memoryStream, ImageFormat.Png);
                    return memoryStream.ToArray();
                }
            } catch (Exception) {
                return null;
            }
        }
    }
}
