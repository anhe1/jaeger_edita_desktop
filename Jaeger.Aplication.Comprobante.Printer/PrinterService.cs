﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Comprobante.Mappers;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Printer {
    /// <summary>
    /// servicio de impresion standard para comprobantes fiscales
    /// </summary>
    public class PrinterService {
        #region declaraciones
        private Dictionary<string, string> RegimenesFiscales;
        private Dictionary<string, string> Monedas;
        private Dictionary<string, string> UsosCFDI;
        private Dictionary<string, string> MetodosPago;
        private Dictionary<string, string> FormasPago;
        private Dictionary<string, string> Exportacion;
        private Dictionary<string, string> MesesCatalogo;
        private Dictionary<string, string> PeriodicidadCatalogo;
        private Dictionary<string, string> ConfigAutotransporteCatalogo;
        private Dictionary<string, string> figuraTransporteCatalogo;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public PrinterService() {
            this.RegimenesFiscales = CatalogosService.GetRegimenesFiscales();
            this.Monedas = CatalogosService.GetMoneda();
            this.Exportacion = Domain.Comprobante.ValueObjects.CFDICatalogos.GetExportacion().ToDictionary(keySelector: it => it.Id, elementSelector: it => it.Descriptor);
            this.MetodosPago = Domain.Comprobante.ValueObjects.CFDICatalogos.GetMetodoPagos().ToDictionary(keySelector: it => it.Id, elementSelector: it => it.Descriptor);
            this.UsosCFDI = CatalogosService.GetUsoCFDI();
            this.FormasPago = CatalogosService.GetFormaPago();
            this.MesesCatalogo = CatalogosService.GetMeses();
            this.PeriodicidadCatalogo = CatalogosService.GetPeriodicidad();
            this.ConfigAutotransporteCatalogo = CatalogosService.ConfigAutotransporteCatalogo();
            this.figuraTransporteCatalogo = CatalogosService.FiguraTransporte();
        }

        /// <summary>
        /// crear objeto printer
        /// </summary>
        /// <param name="comprobante">comprobante detail</param>
        /// <returns>ComprobanteFiscalPrinter</returns>
        private ComprobanteFiscalPrinter ToPrinter(ComprobanteFiscalDetailModel comprobante) {
            var response = new ComprobanteFiscalPrinter {
                EmisorRFC = comprobante.Emisor.RFC,
                EmisorNombre = comprobante.Emisor.Nombre,
                EmisorRegimenFiscal = comprobante.Emisor.RegimenFiscal,
                ReceptorNombre = comprobante.Receptor.Nombre,
                ReceptorRFC = comprobante.Receptor.RFC,
                ReceptorDomicilioFiscal = comprobante.DomicilioFiscal,
                NoCertificado = comprobante.NoCertificado,
                CondicionPago = comprobante.CondicionPago,
                Version = comprobante.Version,
                CadenaOriginalOff = "",
                Total = comprobante.Total,
                SubTotal = comprobante.SubTotal,
                Descuento = comprobante.Descuento,
                ClaveExportacion = "",
                TipoComprobanteText = comprobante.TipoComprobanteText,
                LugarExpedicion = comprobante.LugarExpedicion,
                IdSubTipo = comprobante.IdSubTipo,
                TipoComprobante = comprobante.TipoComprobante,
                RetencionISR = comprobante.RetencionISR,
                TrasladoIEPS = comprobante.TrasladoIEPS,
                TrasladoIVA = comprobante.TrasladoIVA,
                RetencionIVA = comprobante.RetencionIVA,
                FechaEmision = comprobante.FechaEmision,
                FechaCancela = comprobante.FechaCancela,
                FechaTimbre = comprobante.FechaTimbre,
                FechaRecepcionPago = comprobante.FechaRecepcionPago,
                FechaUltimoPago = comprobante.FechaUltimoPago,
                FechaValidacion = comprobante.FechaValidacion
            };

            if (comprobante.Folio != null) {
                response.Folio = comprobante.Folio;
            }

            if (comprobante.Serie != null) {
                response.Serie = comprobante.Serie;
            }

            response.EmisorRegimenFiscal = this.GetRegimenFiscal(response.EmisorRegimenFiscal);
            response.ClaveExportacion = this.GetClaveExportacion(comprobante);
            response.ClaveRegimenFiscal = this.GetRegimenFiscal(comprobante);
            response.ClaveUsoCFDI = this.GetUsoCFDI(comprobante);
            response.ClaveMoneda = this.GetMoneda(comprobante);
            response.ClaveMetodoPago = this.GetMetodoPago(comprobante);
            response.ClaveFormaPago = this.GetFormaPago(comprobante.ClaveFormaPago);
            response.InformacionGlobal = this.InformacionGlobal(comprobante);
            response.Conceptos = comprobante.Conceptos;
            response.TrasladoIVA = comprobante.TrasladoIVA;
            response.TrasladoIEPS = comprobante.TrasladoIEPS;
            response.RetencionIEPS = comprobante.RetencionIEPS;
            response.RetencionISR = comprobante.RetencionISR;

            if (comprobante.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                if (comprobante.RecepcionPago != null) {
                    response.RecepcionPago = comprobante.RecepcionPago;
                } else {

                }
            } else if (comprobante.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && comprobante.Documento == "porte") {
                if (comprobante.CartaPorte != null) {
                    response.Documento = comprobante.Documento;
                    response.CartaPorte = comprobante.CartaPorte;
                    //_response.CartaPorte.Mercancias.Autotransporte.PermSCT = this.ConfigAutotransporteCatalogo[_response.CartaPorte.Mercancias.Autotransporte.PermSCT];
                    if (response.CartaPorte.FiguraTransporte != null) {
                        for (int i = 0; i < response.CartaPorte.FiguraTransporte.Count; i++) {
                            response.CartaPorte.FiguraTransporte[i].TipoFigura = !this.figuraTransporteCatalogo.ContainsKey(response.CartaPorte.FiguraTransporte[i].TipoFigura) ? response.CartaPorte.FiguraTransporte[i].TipoFigura : this.figuraTransporteCatalogo[response.CartaPorte.FiguraTransporte[i].TipoFigura];
                        }
                    }
                } else {
                    comprobante.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
                }
            }

            response.CantidadEnLetra = CatalogosService.TotalEnLetra(comprobante.Total);

            if (comprobante.RecepcionPago != null) {
                response.CantidadEnLetra = CatalogosService.TotalEnLetra(response.RecepcionPago.Select(it => it.Monto).First());
                response.RecepcionPago[0].MonedaP = !this.Monedas.ContainsKey(response.RecepcionPago[0].MonedaP) ? response.RecepcionPago[0].MonedaP : this.Monedas[response.RecepcionPago[0].MonedaP];
                response.RecepcionPago[0].FormaDePagoP = !this.FormasPago.ContainsKey(response.RecepcionPago[0].FormaDePagoP) ? response.RecepcionPago[0].FormaDePagoP : this.FormasPago[response.RecepcionPago[0].FormaDePagoP];
            }

            if (comprobante.Nomina12 != null) {
                response.Nomina = comprobante.Nomina;
            }


            return response;
        }

        public ComprobanteFiscalPrinter Printer(ComprobanteFiscalDetailModel comprobante) {
            var response = this.ToPrinter(comprobante);
            if (!Jaeger.Domain.Base.Services.ValidacionService.UUID(response.IdDocumento)) {
                response.TimbreFiscal = GetTimbreFiscalDummy();
                response.QR = CatalogosService.ImagenQR(response.EmisorRFC, response.ReceptorRFC, response.Total, response.TimbreFiscal.UUID, response.TimbreFiscal.SelloSAT, "");
            }
            return response;
        }

        /// <summary>
        /// crear objeto comprobante printer
        /// </summary>
        /// <param name="base64">cfdi en base64</param>
        /// <returns>ComprobanteFiscalPrinter</returns>
        public ComprobanteFiscalPrinter PrinterB64(string base64) {
            var version = SAT.Reader.CFD.CFDReader.GetVersionB64(base64);
            if (version == "3.3") {
                SAT.CFDI.V33.Comprobante cfdi33 = SAT.CFDI.V33.Comprobante.LoadBase64(base64);
                return this.Printer(cfdi33);
            }
            SAT.CFDI.V40.Comprobante cfdi40 = SAT.CFDI.V40.Comprobante.LoadBase64(base64);
            return this.Printer(cfdi40);
        }

        public ComprobanteFiscalPrinter Printer(string xmlString) {
            var version = SAT.Reader.CFD.CFDReader.GetVersion(xmlString);
            if (version == "3.3") {
                SAT.CFDI.V33.Comprobante cfdi33 = SAT.CFDI.V33.Comprobante.LoadXml(xmlString);
                return this.Printer(cfdi33);
            }
            SAT.CFDI.V40.Comprobante cfdi40 = SAT.CFDI.V40.Comprobante.LoadXml(xmlString);
            return this.Printer(cfdi40);
        }

        #region metodos privados
        private ComprobanteInformacionGlobalDetailModel InformacionGlobal(ComprobanteFiscalDetailModel comprobante) {
            if (comprobante.InformacionGlobal != null) {
                comprobante.InformacionGlobal = comprobante.InformacionGlobal;
                comprobante.InformacionGlobal.ClaveMeses = !this.MesesCatalogo.ContainsKey(comprobante.InformacionGlobal.ClaveMeses) ? comprobante.InformacionGlobal.ClaveMeses : this.MesesCatalogo[comprobante.InformacionGlobal.ClaveMeses];
                comprobante.InformacionGlobal.Periodicidad = !this.PeriodicidadCatalogo.ContainsKey(comprobante.InformacionGlobal.Periodicidad) ? comprobante.InformacionGlobal.Periodicidad : this.PeriodicidadCatalogo[comprobante.InformacionGlobal.Periodicidad];
            }
            return null;
        }

        /// <summary>
        /// obtener clave y descripcion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        private string GetFormaPago(string clave) {
            if (clave != null) {
                var ClaveFormaPago = !this.FormasPago.ContainsKey(clave) ? clave : this.FormasPago[clave];
                return ClaveFormaPago;
            }
            return null;
        }

        /// <summary>
        /// obtener clave y descripcion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        private string GetMetodoPago(ComprobanteFiscalDetailModel comprobante) {
            if (comprobante.ClaveMetodoPago != null) {
                return !this.MetodosPago.ContainsKey(comprobante.ClaveMetodoPago) ? comprobante.ClaveMetodoPago : this.MetodosPago[comprobante.ClaveMetodoPago];
            }
            return string.Empty;
        }

        /// <summary>
        /// obtener clave y descripcion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        private string GetMoneda(ComprobanteFiscalDetailModel comprobante) {
            if (comprobante.ClaveMoneda != null) {
                if (comprobante.ClaveMoneda != "XXX") {
                    return !this.Monedas.ContainsKey(comprobante.ClaveMoneda) ? comprobante.ClaveMoneda : this.Monedas[comprobante.ClaveMoneda];
                }
                return comprobante.ClaveMoneda;
            }
            return string.Empty;
        }

        /// <summary>
        /// obtener clave y descripcion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        private string GetUsoCFDI(ComprobanteFiscalDetailModel comprobante) {
            if (comprobante.Receptor.ClaveUsoCFDI != null) {
                if (comprobante.Receptor.ClaveUsoCFDI != "") {
                    return !this.UsosCFDI.ContainsKey(comprobante.Receptor.ClaveUsoCFDI) ? comprobante.Receptor.ClaveUsoCFDI : this.UsosCFDI[comprobante.Receptor.ClaveUsoCFDI];
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// obtener clave y descripcion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        private string GetRegimenFiscal(ComprobanteFiscalDetailModel comprobante) {
            // receptor Regimen Fiscal
            if (comprobante.ClaveRegimenFiscal != null) {
                if (comprobante.ClaveRegimenFiscal != "") {
                    return !this.RegimenesFiscales.ContainsKey(comprobante.ClaveRegimenFiscal) ? comprobante.ClaveRegimenFiscal : this.RegimenesFiscales[comprobante.ClaveRegimenFiscal];
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// obtener clave y descripcion de la clave de exportacion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        private string GetClaveExportacion(ComprobanteFiscalDetailModel comprobante) {
            if (comprobante.Version != "4.0") {
                return string.Empty;
            }
            if (comprobante.ClaveExportacion != null) {
                if (comprobante.ClaveExportacion == "01")
                    return "01 - No Aplica";
                else if (comprobante.ClaveExportacion == "02")
                    return "02 - Definitiva";
                else if (comprobante.ClaveExportacion == "03")
                    return "03 - Temporal";
                else if (comprobante.ClaveExportacion == "04")
                    return "04 - Definitiva con clave distinta a A1 o cuando no existe enajenación en términos del CFF";
                return comprobante.ClaveExportacion;
            }
            return string.Empty;
        }
        #endregion

        #region 
        /// <summary>
        /// obtener clave y descripcion
        /// </summary>
        /// <param name="comprobante"></param>
        /// <returns>{clave}-{descripcion}</returns>
        protected string GetRegimenFiscal(string claveRegimen) {
            try {
                return this.RegimenesFiscales[claveRegimen];
            } catch (Exception ex) {
                Console.WriteLine(ex.StackTrace);
            }
            return string.Empty;
        }

        protected Domain.Comprobante.Entities.Complemento.ComplementoTimbreFiscal GetTimbreFiscalDummy() {
            var timbreFiscal = new Domain.Comprobante.Entities.Complemento.ComplementoTimbreFiscal() {
                UUID = "00000000-0000-0000-0000-000000000000",
                FechaTimbrado = DateTime.Now,
                NoCertificadoSAT = "0000000000000",
                RFCProvCertif = "XXXXXXXXXX",
                SelloCFD = "XXXXXXXXXXXXXXXXXXXXXXXXXX=",
                SelloSAT = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=",
                Tag = string.Empty
            };
            return timbreFiscal;
        }
        #endregion

        #region parser de comprobante 
        protected ComprobanteFiscalPrinter Printer(SAT.CFDI.V33.Comprobante cfdi) {
            var comprobante = ComprobanteExtensions.ConvertTo(cfdi);
            var response = this.Printer(comprobante);
            response.CadenaOriginalOff = cfdi.CadenaOriginalOff;
            response.TimbreFiscal = ComplementoTimbreFiscalExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
            if (Jaeger.Domain.Base.Services.ValidacionService.UUID(response.IdDocumento)) {
                response.TimbreFiscal = GetTimbreFiscalDummy();
            }
            response.TimbreFiscal.Tag = cfdi.Complemento.TimbreFiscalDigital.CadenaOriginal;
            response.QR = CatalogosService.ImagenQR(cfdi.Emisor.Rfc, cfdi.Receptor.Rfc, cfdi.Total, response.TimbreFiscal.UUID, response.TimbreFiscal.SelloSAT, "");
            return response;
        }

        protected ComprobanteFiscalPrinter Printer(SAT.CFDI.V40.Comprobante cfdi) {
            var comprobante = ComprobanteExtensions.ConvertTo(cfdi);
            var response = this.Printer(comprobante);
            response.CadenaOriginalOff = cfdi.CadenaOriginalOff;
            response.TimbreFiscal = ComplementoTimbreFiscalExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
            if (Jaeger.Domain.Base.Services.ValidacionService.UUID(response.IdDocumento)) {
                response.TimbreFiscal = GetTimbreFiscalDummy();
            }
            response.TimbreFiscal.Tag = cfdi.Complemento.TimbreFiscalDigital.CadenaOriginal;
            response.QR = CatalogosService.ImagenQR(cfdi.Emisor.Rfc, cfdi.Receptor.Rfc, cfdi.Total, response.TimbreFiscal.UUID, response.TimbreFiscal.SelloSAT, "");
            return response;
        }
        #endregion
    }
}
