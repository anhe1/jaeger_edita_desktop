﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Comprobante.Printer {
    public static class CatalogosService {
        #region catalogos
        /// <summary>
        /// obtener listado de tipos de comprobantee
        /// </summary>
        public static List<CFDITipoComprobanteModel> GetTipoComprobantes() {
            return new List<CFDITipoComprobanteModel>(((CFDITipoComprobanteEnum[])Enum.GetValues(typeof(CFDITipoComprobanteEnum))).Select(c => new CFDITipoComprobanteModel((int)c, c.ToString())).ToList());
        }

        public static Dictionary<string, string> GetMeses() {
            var catalogo = new MesesCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        public static Dictionary<string, string> GetPeriodicidad() {
            var catalogo = new PeriodicidadCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        public static Dictionary<string, string> GetRegimenesFiscales() {
            var catalogo = new Jaeger.Catalogos.RegimenesFiscalesCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        /// <summary>
        /// Catalogo de Metodos de Pago
        /// </summary>
        public static List<MetodoPagoModel> GetMetodoPagos() {
            return new List<MetodoPagoModel> {
                new MetodoPagoModel("PUE", "Pago en una sola exhibición"),
                new MetodoPagoModel("PPD", "Pago en parcialidades o diferido")
            };
        }

        public static Dictionary<string, string> GetFormaPago() {
            var catalogo = new FormaPagoCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        /// <summary>
        /// Catalogo de exportacion para version CFDI 4.0
        /// </summary>
        public static List<ExportacionModel> GetExportacion() {
            return new List<ExportacionModel> {
                new ExportacionModel("01", "No Aplica"),
                new ExportacionModel("02", "Definitiva"),
                new ExportacionModel("03", "Temporal")
            };
        }

        /// <summary>
        /// listado de objetos de impuesto para el concepto
        /// </summary>
        public static List<ConceptoObjetoImpuestoModel> GetObjetoImpuesto() {
            return new List<ConceptoObjetoImpuestoModel> {
                new ConceptoObjetoImpuestoModel("01", "No objeto de impuesto"),
                new ConceptoObjetoImpuestoModel("02", "Sí objeto de impuesto"),
                new ConceptoObjetoImpuestoModel("03", "Sí objeto del impuesto y no obligado al desglose."),
                new ConceptoObjetoImpuestoModel("04", "Sí objeto del impuesto y no causa impuesto.")
            };
        }

        public static Dictionary<string, string> GetUsoCFDI() {
            var catalogo = new UsoCFDICatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        public static Dictionary<string, string> GetMoneda() {
            var catalogo = new MonedaCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        /// <summary>
        /// obtener catalogo de motivos de cancelacion permitidos.
        /// </summary>
        public static BindingList<MotivoCancelacionModel> GetMotivoCancelacion() {
            return new BindingList<MotivoCancelacionModel> {
                new MotivoCancelacionModel("01", "Comprobante emitido con errores con relación."),
                new MotivoCancelacionModel("02", "Comprobante emitido con errores sin relación."),
                new MotivoCancelacionModel("03", "No se llevó a cabo la operación."),
                new MotivoCancelacionModel("04", "Operación nominativa relacionada en una factura global.")
            };
        }

        #region catalogos para complemento de carta porte
        /// <summary>
        /// Catalogo simple de tipo de tranposte para carta porte
        /// </summary>
        public static List<CartaPorteTranspInternacModel> GetTipoTranporte() {
            return new List<CartaPorteTranspInternacModel> {
                new CartaPorteTranspInternacModel(0, "Nacional"),
                new CartaPorteTranspInternacModel(1, "Internacional") };
        }

        public static List<CartaPorteEntradaSalidaMercModel> GetCartaPorteEntradaSalidaMercs() {
            return new List<CartaPorteEntradaSalidaMercModel> {
                new CartaPorteEntradaSalidaMercModel("Entrada", "Entrada"),
                new CartaPorteEntradaSalidaMercModel("Salida", "Salida")};
        }
        #endregion

        #region catalogos complemento de nomina
        public static List<Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel> GetTipoNominas() {
            return new List<Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel>() {
                new Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel("O", "Ordinaria"),
                new Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel("E", "Extraordinaria")
            };
        }
        #endregion

        public static Dictionary<string, string> ConfigAutotransporteCatalogo() {
            var catalogo = new ConfigAutotransporteCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }

        public static Dictionary<string, string> FiguraTransporte() {
            var catalogo = new CveFiguraTransporteCatalogo();
            catalogo.Load();
            return catalogo.Items.ToDictionary(it => it.Clave, it => it.Descriptor);
        }
        #endregion
        
        #region QR
        public static string ImagenQR(string emisorRFC, string receptorRFC, decimal total, string uuid, string selloSAT, string htmlContent) {
            byte[] qr = QRCodeExtensions.CopyImageToByteArray(
                            QRCodeExtensions.GetCodigoQR(
                                "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?",
                                emisorRFC, receptorRFC, total.ToString(), uuid, selloSAT));
            return Convert.ToBase64String(qr);
        }
        #endregion

        public static string TotalEnLetra(decimal total) {
            string totalEnLetra = NumeroALetras.Convertir(Double.Parse(total.ToString()), 1);
            return totalEnLetra;
        }
    }
}
