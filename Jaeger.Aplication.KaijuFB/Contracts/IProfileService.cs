﻿using Jaeger.Domain.Abstractions;
using Jaeger.Domain.KaijuFB.Profile.Entities;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Aplication.KaijuFB.Contracts {
    public interface IProfileService {
        UserRolModel GetByIdUser(int idUser);
        BindingList<UIMenuProfileModel> GetList(int index);
        List<UIMenuElement> GetMenus();
        BindingList<UserRolDetailModel> GetProfile(bool onlyActive);
        UIMenuProfileModel Save(UIMenuProfileModel selccionado);
        UserRolDetailModel Save(UserRolDetailModel model);
    }
}