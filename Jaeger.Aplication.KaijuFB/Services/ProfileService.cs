﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base;
using Jaeger.DataAccess.KaijuFB.Repositories;
using Jaeger.Domain.Abstractions;
using Jaeger.Domain.KaijuFB.Profile.Entities;
using Jaeger.Domain.KaijuFB.Profile.Contracts;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.KaijuFB.Contracts;

namespace Jaeger.Aplication.KaijuFB.Service {
    public class ProfileService : IProfileService {
        protected ISqlUIProfileRepository profileRepository;
        protected ISqlUserRolRepository userRolRepository;
        protected ISqlUserRolRelacionRepository relacionRepository;
        protected ISqlUIMenuRepository menuRepository;

        public ProfileService() {
            this.profileRepository = new SqlFbUIProfileRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
            this.userRolRepository = new SqlFbUserRolRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
            this.relacionRepository = new SqlFbUserRolRelacionRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
            this.menuRepository = new SqlFbUIMenuRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public UserRolModel GetByIdUser(int idUser) {
            return this.userRolRepository.GetByIdUser(idUser);
        }

        public List<UIMenuElement> GetMenus() {
            ConfigService.Menus = this.profileRepository.GetByIdUser(ConfigService.Piloto.Id).ToList();
            return ConfigService.Menus;
        }

        public BindingList<UIMenuProfileModel> GetList(int index) {
            return new BindingList<UIMenuProfileModel>(this.profileRepository.GetList(index).ToList());
        }

        public BindingList<UserRolDetailModel> GetProfile(bool onlyActive) {
            var result = new BindingList<UserRolDetailModel>(this.profileRepository.GetProfile(onlyActive).ToList());
            return result;
        }

        public UIMenuProfileModel Save(UIMenuProfileModel selccionado) {
            return this.profileRepository.Save(selccionado);
        }

        public UserRolDetailModel Save(UserRolDetailModel model) {
            model.IdUserRol = this.userRolRepository.Save(model).IdUserRol;
            if (model.IdUserRol > 0) {
                for (int i = 0; i < model.Perfil.Count; i++) {
                    System.Console.WriteLine(i.ToString());
                    model.Perfil[i].IdRol = model.IdUserRol;
                    if (model.Perfil[i].IdRol > 0 && model.Perfil[i].IdPerfilMenu > 0) {
                        if (model.Perfil[i].SetModified)
                        model.Perfil[i] = this.profileRepository.Save(model.Perfil[i]);
                    }
                }

                for (int i = 0; i < model.Relacion.Count; i++) {
                    model.Relacion[i].IdRol = model.IdUserRol;
                    model.Relacion[i] = this.relacionRepository.Save(model.Relacion[i]);
                }
            }
            return model;
        }
    }
}
