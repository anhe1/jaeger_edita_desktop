﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.Localization;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LocalizationProvider<RadMessageLocalizationProvider>.CurrentProvider = new LocalizationProviderCustomerMessageBox();
            
            // creamos y comprobamos los directorios para uso local
            ManagerPathService.CreatePaths();
            using (var login = new Forms.LoginForm(args)) {
                login.ShowDialog();
            }

            if (!(ConfigService.Synapsis == null) & !(ConfigService.Piloto == null)) {
                Application.Run(new Forms.MainRibbonForm());
            }
        }
    }
}