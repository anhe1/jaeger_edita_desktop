﻿using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.CP.Produccion {
    public class OrdenProduccionCatalogoForm : UI.CP.Forms.Produccion.OrdenProduccionHistorialForm {
        private RadMenuItem _RemisionCliente = new RadMenuItem { Text = "Crear remisión cliente", Name = "adm_galmpt_salida" };
        public OrdenProduccionCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.RemisionService = new Aplication.CP.Service.RemisionadoService();
            var d2 = new Domain.Base.ValueObjects.UIAction(ConfigService.GeMenuElement(_RemisionCliente.Name).Permisos);
            this._RemisionCliente.Visibility = (d2.Agregar ? ElementVisibility.Visible : ElementVisibility.Collapsed);
            this.TOrden.menuContextual.Items.Add(_RemisionCliente);
            this._RemisionCliente.Click += _RemisionCliente_Click;
        }

        private void _RemisionCliente_Click(object sender, System.EventArgs e) {
            base.CrearRemision_Click(sender, e);
        }
    }
}
