﻿using System;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.Produccion {
    internal class EmpleadoCatalogoForm : UI.CP.Forms.Produccion.EmpleadoCatalogoForm {

        public EmpleadoCatalogoForm(UIMenuElement menuElement) {
            this.Load += EmpleadoCatalogoForm_Load;
        }

        private void EmpleadoCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new EmpleadosService();
        }
    }
}
