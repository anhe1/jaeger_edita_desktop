﻿using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.CP.Produccion {
    public class OrdenEnProcesoForm : UI.CP.Forms.Produccion.OrdenEnProcesoForm {
        private RadMenuItem _RemisionCliente = new RadMenuItem { Text = "Crear remisión cliente", Name = "adm_galmpt_salida" };
        public OrdenEnProcesoForm(UIMenuElement menuElement) : base(menuElement) {
            var d2 = new Domain.Base.ValueObjects.UIAction(ConfigService.GeMenuElement(_RemisionCliente.Name).Permisos);
            this._RemisionCliente.Visibility = (d2.Agregar ? ElementVisibility.Visible : ElementVisibility.Collapsed);
            this.TOrden.menuContextual.Items.Add(_RemisionCliente);
        }
    }
}
