﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.Produccion {
    internal class DepartamentoProduccionForm : UI.Cotizador.Forms.Produccion.DepartamentoProduccionForm {
        public DepartamentoProduccionForm(UIMenuElement menuElement) : base() {
            this.TDepartamento.Service = new ProduccionService();
        }
    }
}
