﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.Ventas {
    /// <summary>
    /// formulario del directorio 
    /// </summary>
    public class ClientesForm : UI.CP.Forms.Empresa.ContribuyentesForm {
        public ClientesForm(UIMenuElement menuElement) : base(menuElement) {
            Load += DirectorioForm_Load;
        }

        private void DirectorioForm_Load(object sender, EventArgs e) { }
    }
}
