﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Builder;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Cotizador.Ventas.Entities;

namespace Jaeger.UI.Forms.CP.Ventas {
    /// <summary>
    /// Productos vendidos, hereda desde CP la informacion de la orden de produccion, cotizacion y el remisionado
    /// </summary>
    public class ProductoVendidoCatalogoForm : UI.CP.Forms.Ventas.ProductosVendidosForm {
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService _ComprobanteFiscalService;
        protected internal GridViewTemplate _GridFacturacion = new GridViewTemplate() { Caption = "Facturación" };

        public ProductoVendidoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            Load += ProductoVendidoCatalogoForm_Load;
        }

        private void ProductoVendidoCatalogoForm_Load(object sender, EventArgs e) {
            var contextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
            menuContextual.Items.Add(contextMenuFacturar);
            contextMenuFacturar.Click += new EventHandler(ContextMenuFacturar_Click);

            TempleteFacturacion();
            _GridFacturacion.HierarchyDataProvider = new GridViewEventDataProvider(_GridFacturacion);
            gridData.RowSourceNeeded += GridData_RowSourceNeeded1;
        }

        private void ContextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV33 = Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV33 != null) {
                var nuevo = new Comprobante.Forms.ComprobanteFiscalGeneralForm(cfdiV33, CFDISubTipoEnum.Emitido, -1) { MdiParent = ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            Tag = null;
        }

        #region acciones del grid de datos
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            var rowview = e.ParentRow.DataBoundItem as ProductoVendidoModel;
            if (e.Template.Caption == _GridFacturacion.Caption) {
                if (rowview != null) {
                    _GridFacturacion.Tag = rowview.IdOrden;
                    using (var espera = new Waiting1Form(Facturacion)) {
                        espera.Text = "Consultando facturación ...";
                        espera.ShowDialog(this);
                    }
                    if (Tag != null) {
                        var tabla = Tag as List<Domain.Comprobante.Entities.ComprobanteFiscalConceptoView>;
                        if (tabla != null) {
                            foreach (var item in tabla) {
                                var newRow = e.Template.Rows.NewRow();
                                Comprobante.Services.GridViewComprobantesBuilder.ConvertTo(item, newRow);
                                e.SourceCollection.Add(newRow);
                            }
                        }
                    }
                    Tag = null;
                }
            }
        }
        #endregion

        private void CrearFactura() {
            if (gridData.CurrentRow != null) {
                var seleccion = new List<ProductoVendidoModel>();
                seleccion.AddRange(gridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ProductoVendidoModel));
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = ComprobanteFiscalService.Builder().New(CFDITipoComprobanteEnum.Ingreso).Build();
                    IConceptoBuilder d0 = new ConceptoBuilder();
                    foreach (var item in seleccion) {
                        var concepto = d0.Cantidad(item.Cantidad)
                            .NumPedido(item.IdOrden)
                            .ClaveUnidad("H87")
                            .ClaveProducto("82121511")
                            .Descripcion(item.Nombre)
                            .ValorUnitario(item.ValorUnitario)
                            .ObjetoDeImpuestos(CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto)
                            .AddTrasladoIVA().Build();
                        cfdiV33.Conceptos.Add((Domain.Comprobante.Entities.ComprobanteConceptoDetailModel)concepto);
                    }
                    Tag = cfdiV33;
                }
            }
        }

        private void Facturacion() {
            // solo en caso de que el servicio no este activo
            if (_ComprobanteFiscalService == null)
                _ComprobanteFiscalService = new ComprobantesFiscalesService();
            if (_GridFacturacion.Tag != null) {
                var index = (int)_GridFacturacion.Tag;
                var query = ComprobantesFiscalesService.Query().ByOrdenProduccion(index).OnlyActive().Build();
                var dataSource = _ComprobanteFiscalService.GetList<Domain.Comprobante.Entities.ComprobanteFiscalConceptoView>(query).ToList();
                if (dataSource != null) {
                    Tag = dataSource;
                } else {
                    Tag = null;
                }
            }
        }

        private void TempleteFacturacion() {
            _GridFacturacion.Columns.AddRange(new Comprobante.Services.GridViewComprobantesBuilder().Templetes().Facturacionconceptos().Build());
            gridData.MasterTemplate.Templates.Add(_GridFacturacion);
            _GridFacturacion.Standard();
        }
    }
}
