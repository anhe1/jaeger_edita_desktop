﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.Ventas {
    internal class VentasReporte2Form : UI.CP.Forms.Ventas.VentasReporte2Form {
        public VentasReporte2Form(UIMenuElement menuElement) : base(menuElement) {
            this.Load += VentasReporte2Form_Load;
        }

        private void VentasReporte2Form_Load(object sender, EventArgs e) {
            this._Service = new Aplication.Cotizador.Services.VentasService();
        }
    }
}
