﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.Ventas {
    internal class VentasReporte1Form : UI.CP.Forms.Ventas.VentasReporte1Form {
        public VentasReporte1Form(UIMenuElement menuElement) : base(menuElement) {
            this.Load += VentasReporte1Form_Load;
        }

        private void VentasReporte1Form_Load(object sender, EventArgs e) {
            this._Service = new Aplication.Cotizador.Services.VentasService();
        }
    }
}
