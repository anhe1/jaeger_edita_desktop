﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP {
    public class RemisionadoForm : UI.CP.Forms.Almacen.PT.RemisionadoForm {
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService comprobanteFiscalService;
        protected internal RadMenuItem contextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
        protected internal RadMenuItem contextMenuReciboCobro = new RadMenuItem { Text = "Crear Recibo de Cobro" };
        protected internal RadMenuItem PorCobrar = new RadMenuItem { Text = "Por Cobrar", Name = "tadm_gcli_remxcobrar" };
        protected internal RadMenuItem PorPartida = new RadMenuItem { Text = "Por Partidas", Name = "tadm_gcli_remxpartida" };

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Load += RemisionesCatalogoForm_Load;
        }

        private void RemisionesCatalogoForm_Load(object sender, EventArgs e) {

        }

        private void PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new RemisionadoPartidaForm(new UIMenuElement(), this._Service) { MdiParent = this.ParentForm, Text = this.PorPartida.Text };
            porPartida.Show();
        }

        private void PorCobrar_Click(object sender, EventArgs e) {
            var porPartida = new RemisionadoPorCobrarForm(new UIMenuElement()) { MdiParent = this.ParentForm, Text = this.PorCobrar.Text };
            porPartida.Show();
        }
    }
}
