﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Builder;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Cotizador.Ventas.Entities;

namespace Jaeger.UI.Forms.CP {
    /// <summary>
    /// Productos vendidos, hereda desde CP la informacion de la orden de produccion, cotizacion y el remisionado
    /// </summary>
    public class ProductoVendidoCatalogoForm : UI.CP.Forms.Ventas.ProductosVendidosForm {
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService _ComprobanteFiscalService;
        protected internal GridViewTemplate _GridFacturacion = new GridViewTemplate() { Caption = "Facturación" };

        public ProductoVendidoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += ProductoVendidoCatalogoForm_Load;
        }

        private void ProductoVendidoCatalogoForm_Load(object sender, EventArgs e) {
            var contextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
            this.menuContextual.Items.Add(contextMenuFacturar);
            contextMenuFacturar.Click += new EventHandler(this.ContextMenuFacturar_Click);
            
            this.TempleteFacturacion();
            this._GridFacturacion.HierarchyDataProvider = new GridViewEventDataProvider(this._GridFacturacion);
            this.gridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
        }

        private void ContextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV33 = this.Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV33 != null) {
                var nuevo = new Comprobante.Forms.ComprobanteFiscalGeneralForm(cfdiV33, CFDISubTipoEnum.Emitido, -1) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            this.Tag = null;
        }

        #region acciones del grid de datos
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            var rowview = e.ParentRow.DataBoundItem as ProductoVendidoModel;
            if (e.Template.Caption == this._GridFacturacion.Caption) {
                if (rowview != null) {
                    this._GridFacturacion.Tag = rowview.IdOrden;
                    using (var espera = new Waiting1Form(this.Facturacion)) {
                        espera.Text = "Consultando facturación ...";
                        espera.ShowDialog(this);
                    }
                    if (this.Tag != null) {
                        var tabla = this.Tag as List<Domain.Comprobante.Entities.ComprobanteFiscalConceptoView>;
                        if (tabla != null) {
                            foreach (var item in tabla) {
                                var newRow = e.Template.Rows.NewRow();
                                Comprobante.Services.GridViewComprobantesBuilder.ConvertTo(item, newRow);
                                e.SourceCollection.Add(newRow);
                            }
                        }
                    }
                    this.Tag = null;
                }
            }
        }
        #endregion

        private void CrearFactura() {
            if (this.gridData.CurrentRow != null) {
                var seleccion = new List<ProductoVendidoModel>();
                seleccion.AddRange(this.gridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ProductoVendidoModel));
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = ComprobanteFiscalService.Builder().New(CFDITipoComprobanteEnum.Ingreso).Build();
                    IConceptoBuilder d0 = new ConceptoBuilder();
                    foreach (var item in seleccion) {
                        var concepto = d0.Cantidad(item.Cantidad)
                            .NumPedido(item.IdOrden)
                            .ClaveUnidad("H87")
                            .ClaveProducto("82121511")
                            .Descripcion(item.Nombre)
                            .ValorUnitario(item.ValorUnitario)
                            .ObjetoDeImpuestos(CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto)
                            .AddTrasladoIVA().Build();
                        cfdiV33.Conceptos.Add((Domain.Comprobante.Entities.ComprobanteConceptoDetailModel)concepto);
                    }
                    this.Tag = cfdiV33;
                }
            }
        }

        private void Facturacion() {
            // solo en caso de que el servicio no este activo
            if (this._ComprobanteFiscalService == null) this._ComprobanteFiscalService = new ComprobantesFiscalesService();
            if (this._GridFacturacion.Tag != null) {
                var index = (int)this._GridFacturacion.Tag;
                var query = ComprobantesFiscalesService.Query().ByOrdenProduccion(index).OnlyActive().Build();
                var dataSource = this._ComprobanteFiscalService.GetList<Domain.Comprobante.Entities.ComprobanteFiscalConceptoView>(query).ToList();
                if (dataSource != null) {
                    this.Tag = dataSource;
                } else {
                    this.Tag = null;
                }
            }
        }

        private void TempleteFacturacion() {
            this._GridFacturacion.Columns.AddRange(new Comprobante.Services.GridViewComprobantesBuilder().Templetes().Facturacionconceptos().Build());
            this.gridData.MasterTemplate.Templates.Add(this._GridFacturacion);
            this._GridFacturacion.Standard();
        }
    }
}
