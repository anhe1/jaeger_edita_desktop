﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.UI.Forms.CP.CCalidad {
    internal class NoConformidadesForm : UI.CCalidad.Forms.NoConformidadesForm {
        public NoConformidadesForm(UIMenuElement menuElement) : base() {
            this.Load += NoConformidadesForm1_Load;
            this.TNoConformidad.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void NoConformidadesForm1_Load(object sender, EventArgs e) {
            this.TNoConformidad.Service = new Aplication.CP.Service.NoConformidadesService();
        }

        protected override void TNoConformidad_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new NoConformidadForm(this.TNoConformidad.Service, null) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        protected override void TNoConformidad_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.TNoConformidad.GetCurrent<NoConformidadSingleModel>();
            if (seleccionado != null) {
                var nuevo = new NoConformidadForm(this.TNoConformidad.Service, seleccionado) { MdiParent = this.ParentForm };
                nuevo.Show();
            }
        }
    }
}
