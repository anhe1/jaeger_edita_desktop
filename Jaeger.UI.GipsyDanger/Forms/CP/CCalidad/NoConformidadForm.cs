﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Aplication.CCalidad.Contracts;

namespace Jaeger.UI.Forms.CP.CCalidad {
    internal class NoConformidadForm : UI.CCalidad.Forms.NoConformidadForm {
        public NoConformidadForm(INoConformidadesService service, NoConformidadDetailModel model) : base(service, model) {
            this.Load += NoConformidadForm_Load;
        }

        private void NoConformidadForm_Load(object sender, EventArgs e) {
            this.Text = "No Conformidad";
        }

        protected override void TOrdenRelacion_Nuevo_Click(object sender, EventArgs e) {
            using (var _catalogoProductos = new Jaeger.UI.Cotizador.Forms.Produccion.BuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.Producto_Selected;
                _catalogoProductos.ShowDialog(this);
            }
        }

        private void Producto_Selected(object sender, Domain.Cotizador.Produccion.Entities.OrdenProduccionDetailModel e) {
            if (e != null) {
                this._NoConformidad.OrdenProduccion.Add(new OrdenProduccionDetailModel {
                    Descripcion = e.Nombre,
                    FechaNuevo = e.Fecha,
                    IdOrden = e.IdOrden,
                    Cliente = e.Cliente
                });
            }
        }

        protected override void TCosto_Nuevo_Click(object sender, EventArgs e) {
            using (var _catalogoProductos = new OrdenProduccionBuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.Producto_Selected1;
                _catalogoProductos.ShowDialog(this);
            }
        }

        private void Producto_Selected1(object sender, CostoNoCalidadDetailModel e) {
            if (e != null) {
                this._NoConformidad.CostosNoCalidad.Add(e);
            }
        }
    }
}
