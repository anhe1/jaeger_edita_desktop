﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.CCalidad {
    internal class NoConformidadEvaluarReporteForm : UI.CCalidad.Forms.NoConformidadEvaluarReporteForm {
        private UIMenuElement menuElement;

        public NoConformidadEvaluarReporteForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Service = new Aplication.CP.Service.NoConformidadesService();
            this.TNoConformidad.Service = new Aplication.CP.Service.NoConformidadesService();
        }
    }
}
