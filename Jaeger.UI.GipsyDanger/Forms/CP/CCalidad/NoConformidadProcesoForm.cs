﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP.CCalidad {
    internal class NoConformidadProcesoForm : NoConformidadesForm {
        public NoConformidadProcesoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += NoConformidadProcesoForm_Load;
        }

        private void NoConformidadProcesoForm_Load(object sender, EventArgs e) {
            this.Text = "No Conformidad: En Proceso";
            this.TNoConformidad.ShowPeriodo = false;
            this.TNoConformidad.GridData.AllowEditRow = this.TNoConformidad.Permisos.Status;
        }
    }
}
