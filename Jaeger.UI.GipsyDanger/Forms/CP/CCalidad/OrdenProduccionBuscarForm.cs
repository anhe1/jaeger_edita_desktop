﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Cotizador.Contracts;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.UI.Forms.CP.CCalidad {
    internal class OrdenProduccionBuscarForm : Cotizador.Forms.Produccion.BuscarForm {
        protected internal IOrdenProduccionService _Service;
        public new event EventHandler<CostoNoCalidadDetailModel> Selected;

        public virtual void OnSelected(CostoNoCalidadDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }
        public OrdenProduccionBuscarForm() : base() {

        }

        public override void OnSelected(Domain.Cotizador.Produccion.Entities.OrdenProduccionDetailModel e) {
            if (e != null) {
                if (_Service == null) {
                    this._Service = new OrdenProduccionService();
                }
                var seleccionado = this._Service.GetList<Domain.Cotizador.Produccion.Entities.OrdenProduccionCostosModel>(new List<Domain.Base.Builder.IConditional> {
                 new Conditional("PEDPRD_ID", e.IdOrden.ToString())
             }).FirstOrDefault();
                this.OnSelected(new CostoNoCalidadDetailModel {
                    Activo = true,
                    Cliente = seleccionado.Cliente,
                    Consumibles = seleccionado.TotalConsumibles,
                    ManoObra = seleccionado.TotalManoObra,
                    Maquinaria = seleccionado.TotalCentro,
                    Descripcion = seleccionado.Descipcion,
                    FechaNuevo = DateTime.Now,
                    IdOrden = seleccionado.IdOrden,
                    MateriaPrima = seleccionado.TotalMateriaPrima,
                    MedioAmbiente = 0
                });
            }
        }
    }
}
