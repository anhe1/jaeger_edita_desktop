﻿using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Aplication.CCalidad.Contracts;

namespace Jaeger.UI.Forms.CP.CCalidad {
    internal class CausaRaizForm : UI.CCalidad.Forms.CausaRaizForm {
        public CausaRaizForm(INoConformidadesService service, CausaRaizDetailModel model) : base(service, model) {
            this.Load += CausaRaizForm_Load;
        }

        private void CausaRaizForm_Load(object sender, System.EventArgs e) {

        }
    }
}
