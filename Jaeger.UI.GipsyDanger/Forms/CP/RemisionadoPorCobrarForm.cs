﻿using System;
using System.Linq;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP {
    public class RemisionadoPorCobrarForm : RemisionadoForm {
        public RemisionadoPorCobrarForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.RemisionadoPorCobrarForm_Load;
        }

        private void RemisionadoPorCobrarForm_Load(object sender, EventArgs e) {
            //this.TRemision.ShowPeriodo = false;
            //this.TRemision.Herramientas.Items.Remove(this.PorCobrar);
            this.Text = "Remisionado por cobrar";
        }

        //public override void Actualizar() {
        //    var d0 = Jaeger.Aplication.Almacen.PT.Services.RemisionadoService.Create().WithYear(this.TRemision.GetEjercicio()).WithIdStatus(4).Build();
        //    this._DataSource = new System.ComponentModel.BindingList<RemisionSingleModel>(
        //            this._Service.GetList<RemisionSingleModel>(d0).ToList()
        //        );
        //}
    }
}
