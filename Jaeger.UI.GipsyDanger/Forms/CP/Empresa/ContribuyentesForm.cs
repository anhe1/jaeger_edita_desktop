﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Cotizador.Empresa.Entities;

namespace Jaeger.UI.Forms.CP.Empresa {
    internal class ContribuyentesForm : Cotizador.Forms.Empresa.ContribuyentesForm {
        public ContribuyentesForm(UIMenuElement menuElement) : base() {
            this.TContribuyente.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += DirectorioForm_Load;
        }

        private void DirectorioForm_Load(object sender, EventArgs e) {
            this.Text = "Empresa: Directorio";
            this._Service = new Aplication.Cotizador.Services.ContribuyenteService();
            this.TContribuyente.IdClase.DisplayMember = "Descripcion";
            this.TContribuyente.IdClase.ValueMember = "IdClase";
            this.TContribuyente.IdClase.DataSource = this._Service.GetList<ContribuyenteClaseModel>(new List<IConditional> { new Conditional("DIRCLA_A", "1") });
        }
    }
}
