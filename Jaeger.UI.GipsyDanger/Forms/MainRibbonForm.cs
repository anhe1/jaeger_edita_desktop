﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Reflection;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Localization;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Builder;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Kaiju.Services;
using Jaeger.Aplication.Kaiju.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms {
    public partial class MainRibbonForm : RadRibbonForm {
        private BackgroundWorker descargarLogo;

        public MainRibbonForm() {
            InitializeComponent();
            this.AllowAero = false;
        }

        private void MainRibbonForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            
            this.Change_Theme(this.office2010BlueTheme1.ThemeName);
            RadGridLocalizationProvider.CurrentProvider = new LocalizationProviderCustomGrid();
            RadMessageLocalizationProvider.CurrentProvider = new LocalizationProviderCustomerMessageBox();
            
            this.descargarLogo = new BackgroundWorker();
            this.descargarLogo.DoWork += DescargarLogo_DoWork;
            this.descargarLogo.RunWorkerCompleted += DescargarLogo_RunWorkerCompleted;
            this.descargarLogo.RunWorkerAsync();

            this.BEstado.Items.Add(new ButtonEmpresaBuilder().Usuario(ConfigService.Piloto.Clave).Version(Application.ProductVersion.ToString()).Empresa(ConfigService.Synapsis.Empresa.RFC).Build());

            using (var espera = new Common.Forms.Waiting1Form(this.SetConfiguration)) {
                espera.Text = ConfigService.EsperarServidor;
                espera.ShowDialog(this);
            }

            this.MenuRibbonBar.SetDefaultTab();

            ConfigService.IsTesting();
            this.Text = ConfigService.Titulo();
            this.RibbonBar.Visible = true;
        }

        #region metodos privados
        private void Menu_UI_AplicarTema_Click(object sender, EventArgs e) {
            this.Change_Theme(((RadButtonElement)sender).Tag.ToString());
        }

        private void Activar_Form(Type type, UIMenuElement sender) {
            try {
                Form _form = (Form)Activator.CreateInstance(type, sender);
                if (_form.WindowState == FormWindowState.Maximized) {
                    _form.MdiParent = this;
                    _form.WindowState = FormWindowState.Maximized;
                    _form.Show();
                    this.RadDock.ActivateMdiChild(_form);
                } else {
                    _form.StartPosition = FormStartPosition.CenterParent;
                    _form.ShowDialog(this);
                }
            } catch (Exception ex) {
                RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        /// <summary>
        /// cambiar tema
        /// </summary>
        private void Change_Theme(string nameTheme) {
            ThemeResolutionService.ApplicationThemeName = nameTheme;
        }

        private void Menu_Main_Click(object sender, EventArgs e) {
            var _item = (RadItem)sender;
            var _menuElement = ConfigService.GeMenuElement(_item.Name);

            if (_menuElement != null) {
                var localAssembly = new object() as Assembly;
                if (_menuElement.Assembly == "Jaeger.UI")
                    localAssembly = UIMenuRibbonHelper.GetAssembly("jaeger_gipsy_danger");
                else
                    localAssembly = UIMenuRibbonHelper.GetAssembly(_menuElement.Assembly);

                if (localAssembly != null) {
                    try {
                        Type type = localAssembly.GetType(string.Format("{0}.{1}", _menuElement.Assembly, _menuElement.Form), true);
                        this.Activar_Form(type, _menuElement);
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, "Main: " + ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                } else {
                    RadMessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private void SetConfiguration() {
            var clave = ConfigService.Synapsis.Empresa.Clave;
            // configuracion de la empresa
            Aplication.Empresa.Contracts.IEmpresaService empresa = new Aplication.Empresa.Service.ConfigurationService();
            ConfigService.Synapsis.Empresa = empresa.GetEmpresa();
            ConfigService.Synapsis.Empresa.Clave = clave;

            // configuracion de CP
            Aplication.Cotizador.Contracts.IConfigurationService cp = new Aplication.Cotizador.Services.ConfigurationService();
            Aplication.Cotizador.Services.GeneralService.Configuration = cp.Get();

            //Aplication.Nomina.Contracts.IConfigurationService nomina = new Aplication.Nomina.Services.ConfigurationService();
            //Aplication.Nomina.Services.GeneralService.Configuration = nomina.Get();

            // configurar repositorio de comprobantes
            Aplication.Repositorio.Contracts.IConfigurationService repositorio = new Aplication.Repositorio.Services.ConfigurationService();
            Aplication.Repositorio.Services.GeneralService.Configuration = new Domain.Repositorio.Entities.ConfigurationRepositorio {
                Parametros = repositorio.Get()
            };

            // configuracion del perfil del usuario
            IProfileToService profile = new Aplication.EditaCP.Profile.Services.ProfileService();
            ConfigService.Piloto = profile.CreateProfile(ConfigService.Piloto);
            ConfigService.Menus = profile.CreateMenus(ConfigService.Piloto.Id).ToList();

            // aplicar permisos
            var permissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            permissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, permissions);

            this.MenuRibbonBar.Refresh();
        }

        private void DescargarLogo_DoWork(object sender, DoWorkEventArgs e) {
            // logo tipo
            string linkLogo = ConfigService.UrlLogo();
            string fileLogo = ManagerPathService.JaegerPath(PathsEnum.Media, string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            Util.FileService.DownloadFile(linkLogo, fileLogo);
        }

        private void DescargarLogo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Text = ConfigService.Titulo();
        }
        #endregion
    }
}
