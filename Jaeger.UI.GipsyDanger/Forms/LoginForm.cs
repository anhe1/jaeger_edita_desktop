﻿namespace Jaeger.UI.Forms {
    /// <summary>
    /// login personalizado
    /// </summary>
    internal class LoginForm : Login.Forms.LoginForm {
        public LoginForm(string[] args) : base(args) {
            this.Text = "EDITA Repositorio";
            this.LogoPicture.Image = Properties.Resources.edita_desktop_cp;
            this.LogoPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPicture.Padding = new System.Windows.Forms.Padding(20, 20, 20, 20);
            this.LogoPicture.ClientSize = new System.Drawing.Size(120, 120);
            this.LogoPicture.Location = new System.Drawing.Point(90, 50);
            this.lblInformacion.Text = "EDITA \r\nRepositorio";
        }
    }
}
