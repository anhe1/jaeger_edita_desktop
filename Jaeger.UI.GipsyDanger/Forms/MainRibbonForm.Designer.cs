﻿using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    partial class MainRibbonForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRibbonForm));
            this.MenuRibbonBar = new Telerik.WinControls.UI.RadRibbonBar();
            this.TAdmon = new Telerik.WinControls.UI.RibbonTab();
            this.adm_grp_cliente = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gcli_expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gcli_ordcliente = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_remisionado = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_facturacion = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_cobranza = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gcli_bedocuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcli_bcredito = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcli_cpago2 = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcli_bresumen = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_tesoreria = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gtes_bancoctas = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gtes_banco = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gtes_bformapago = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gtes_bconceptos = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gtes_bbeneficiario = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gtes_bmovimiento = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_contable = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gcontable_catcuentas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gcontable_catrubros = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcontable_ctacontable = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcontable_polizas = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcontable_activos = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcontable_configurar = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gcontable_cattipo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcontable_creartablas = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_almacen = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_galmacen_mprima = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_galmmp_categoria = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_catalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_modelo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_reqcompra = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_ordcompra = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_vales = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_devolucion = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_existencia = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_cunidad = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmacen_pterminado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_galmpt_catalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_entrada = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_salida = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_existencia = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_cunidad = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_proveedor = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gprv_expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gprv_ordcompra = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_validador = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_facturacion = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_pagos = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gprv_edocuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gprv_credito = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gprv_cpago2 = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gprv_resumen = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_nomina = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gnom_empleado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gnom_empexp = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_empcont = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_gmov = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gnom_periodo = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_sgmov = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gnom_movvac = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_movfal = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_movacu = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_aguina = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_recibo = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gnom_reccon = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_recres = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_parame = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gnom_conf = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_gconcep = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_tablas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gnom_tisr = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tsbe = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tsm = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tuma = new Telerik.WinControls.UI.RadMenuItem();
            this.TProd = new Telerik.WinControls.UI.RibbonTab();
            this.cpemp_grp_directorio = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpemp_gdir_directorio = new Telerik.WinControls.UI.RadButtonElement();
            this.cpemp_grp_producto = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cpemp_gprd_clasificacion = new Telerik.WinControls.UI.RadButtonElement();
            this.cpemp_gprd_catalogo = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_grp_cotizacion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpcot_gcot_productos = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpcot_bprd_producto = new Telerik.WinControls.UI.RadMenuItem();
            this.cpcot_bprd_subproducto = new Telerik.WinControls.UI.RadMenuItem();
            this.cpcot_gcot_presupuesto = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_cotizaciones = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_cliente = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_conf = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_grp_produccion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cppro_gpro_orden = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_bord_historial = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_bord_proceso = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_reqcompra = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_gpro_planea = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_gpro_calendario = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_gantt = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_plan = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_tieadd = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_sgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cppro_sgrp_depto = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_bdepto_trabaj = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_bdepto_proces = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_sgrp_avance = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_sgrp_evaluacion = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_beva_eprodof = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_beva_eproduccion = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_beva_eperiodo = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_remision = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_brem_Emitido = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_brem_Recibido = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_grp_ventas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpvnt_gven_vendedor = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_prodvend = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_group1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cpvnt_gven_pedidos = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_comision = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_ventcosto = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_remisionado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpvnt_brem_historial = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_brem_porcobrar = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_brem_rempart = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_gven_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpvnt_gven_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_gven_reporte2 = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_grp_nconf = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.ccal_gnco_new = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.ccal_gnco_todo = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_proceso = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_reporte = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.ccal_gnco_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_reporte2 = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_reporte3 = new Telerik.WinControls.UI.RadMenuItem();
            this.TVentas = new Telerik.WinControls.UI.RibbonTab();
            this.cmr_grp_ventas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cmr_gventas_vededores = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gventas_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cmr_gventas_pedidos = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gventas_comision = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gventas_ccomision = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_grp_cp = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cmr_gcp_directorio = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gcp_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cmr_gcp_ordproduccion = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gcp_prodvendido = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gcp_remisionado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cmr_gcp_bremisionado = new Telerik.WinControls.UI.RadMenuItem();
            this.cmr_gcp_rempartida = new Telerik.WinControls.UI.RadMenuItem();
            this.cmr_gcp_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cmr_gcp_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.cmr_gcp_resumen = new Telerik.WinControls.UI.RadButtonElement();
            this.TTools = new Telerik.WinControls.UI.RibbonTab();
            this.dsk_grp_config = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gconfig_param = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gconfig_emisor = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_avanzado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_menu = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_usuario = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_perfil = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_cate = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gconfig_cert = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gconfig_certc = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_certf = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_series = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_retencion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tdks_gretencion_recep = new Telerik.WinControls.UI.RadButtonElement();
            this.tdks_gretencion_comprobante = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_tools = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtools_backup = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_backcfdi = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_backdir = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_backret = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_backrec = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validador = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_validar = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validarfc = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validacedula = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validaret = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_cancelado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_presuntos = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_certificado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_actualiza = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_catsat = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_grp_theme = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtheme_2010Black = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Blue = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Silver = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_about = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_btn_manual = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_btn_about = new Telerik.WinControls.UI.RadButtonElement();
            this.BEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.RadDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.RadContainer = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.windows8Theme1 = new Telerik.WinControls.Themes.Windows8Theme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.dsk_grp_repositorio = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gre_comprobante = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gre_solicitud = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gre_directorio = new Telerik.WinControls.UI.RadButtonElement();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).BeginInit();
            this.RadDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuRibbonBar
            // 
            this.MenuRibbonBar.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.TAdmon,
            this.TProd,
            this.TVentas,
            this.TTools});
            // 
            // 
            // 
            this.MenuRibbonBar.ExitButton.Text = "Exit";
            this.MenuRibbonBar.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.MenuRibbonBar.Location = new System.Drawing.Point(0, 0);
            this.MenuRibbonBar.Name = "MenuRibbonBar";
            // 
            // 
            // 
            this.MenuRibbonBar.OptionsButton.Text = "Options";
            this.MenuRibbonBar.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.MenuRibbonBar.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.MenuRibbonBar.Size = new System.Drawing.Size(1590, 162);
            this.MenuRibbonBar.StartButtonImage = global::Jaeger.UI.Properties.Resources.logo_editaCP;
            this.MenuRibbonBar.TabIndex = 0;
            this.MenuRibbonBar.Text = "MainRibbonForm";
            this.MenuRibbonBar.Visible = false;
            // 
            // TAdmon
            // 
            this.TAdmon.IsSelected = false;
            this.TAdmon.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_grp_cliente,
            this.adm_grp_tesoreria,
            this.adm_grp_contable,
            this.adm_grp_almacen,
            this.adm_grp_proveedor,
            this.adm_grp_nomina});
            this.TAdmon.Name = "TAdmon";
            this.TAdmon.Text = "TAdministracion";
            this.TAdmon.UseMnemonic = false;
            // 
            // adm_grp_cliente
            // 
            this.adm_grp_cliente.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcli_expediente,
            this.adm_gcli_subgrupo,
            this.adm_gcli_cobranza,
            this.adm_gcli_reportes});
            this.adm_grp_cliente.Name = "adm_grp_cliente";
            this.adm_grp_cliente.Text = "adm_grp_cliente";
            // 
            // adm_gcli_expediente
            // 
            this.adm_gcli_expediente.Image = global::Jaeger.UI.Properties.Resources.people_30px;
            this.adm_gcli_expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcli_expediente.Name = "adm_gcli_expediente";
            this.adm_gcli_expediente.Text = "b.Expediente";
            this.adm_gcli_expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gcli_expediente.TextWrap = true;
            this.adm_gcli_expediente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_subgrupo
            // 
            this.adm_gcli_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcli_ordcliente,
            this.adm_gcli_remisionado,
            this.adm_gcli_facturacion});
            this.adm_gcli_subgrupo.MinSize = new System.Drawing.Size(22, 22);
            this.adm_gcli_subgrupo.Name = "adm_gcli_subgrupo";
            this.adm_gcli_subgrupo.Opacity = 1D;
            this.adm_gcli_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gcli_subgrupo.Text = "g.SubGrupo";
            this.adm_gcli_subgrupo.UseCompatibleTextRendering = false;
            // 
            // adm_gcli_ordcliente
            // 
            this.adm_gcli_ordcliente.Image = global::Jaeger.UI.Properties.Resources.purchase_order_16px;
            this.adm_gcli_ordcliente.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gcli_ordcliente.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gcli_ordcliente.Name = "adm_gcli_ordcliente";
            this.adm_gcli_ordcliente.ShowBorder = false;
            this.adm_gcli_ordcliente.Text = "b.OrdenCliente";
            this.adm_gcli_ordcliente.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gcli_ordcliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gcli_ordcliente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_remisionado
            // 
            this.adm_gcli_remisionado.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gcli_remisionado.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gcli_remisionado.Name = "adm_gcli_remisionado";
            this.adm_gcli_remisionado.ShowBorder = false;
            this.adm_gcli_remisionado.Text = "b.Remision";
            this.adm_gcli_remisionado.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gcli_remisionado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gcli_remisionado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_facturacion
            // 
            this.adm_gcli_facturacion.Image = global::Jaeger.UI.Properties.Resources.invoice_16px;
            this.adm_gcli_facturacion.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gcli_facturacion.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gcli_facturacion.Name = "adm_gcli_facturacion";
            this.adm_gcli_facturacion.ShowBorder = false;
            this.adm_gcli_facturacion.Text = "b.Comprobante";
            this.adm_gcli_facturacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gcli_facturacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gcli_facturacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_cobranza
            // 
            this.adm_gcli_cobranza.Image = global::Jaeger.UI.Properties.Resources.cash_register_32px;
            this.adm_gcli_cobranza.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcli_cobranza.Name = "adm_gcli_cobranza";
            this.adm_gcli_cobranza.Text = "b.Cobro";
            this.adm_gcli_cobranza.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gcli_cobranza.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_reportes
            // 
            this.adm_gcli_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gcli_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gcli_reportes.ExpandArrowButton = false;
            this.adm_gcli_reportes.Image = global::Jaeger.UI.Properties.Resources.document_30px;
            this.adm_gcli_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcli_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcli_bedocuenta,
            this.adm_gcli_bcredito,
            this.adm_gcli_cpago2,
            this.adm_gcli_bresumen});
            this.adm_gcli_reportes.Name = "adm_gcli_reportes";
            this.adm_gcli_reportes.Text = "b.Reporte";
            this.adm_gcli_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcli_bedocuenta
            // 
            this.adm_gcli_bedocuenta.Name = "adm_gcli_bedocuenta";
            this.adm_gcli_bedocuenta.Text = "b.EstadoCuenta";
            this.adm_gcli_bedocuenta.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_bcredito
            // 
            this.adm_gcli_bcredito.Name = "adm_gcli_bcredito";
            this.adm_gcli_bcredito.Text = "b.Credito";
            this.adm_gcli_bcredito.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_cpago2
            // 
            this.adm_gcli_cpago2.Name = "adm_gcli_cpago2";
            this.adm_gcli_cpago2.Text = "b.ComplementoPago";
            this.adm_gcli_cpago2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_bresumen
            // 
            this.adm_gcli_bresumen.Name = "adm_gcli_bresumen";
            this.adm_gcli_bresumen.Text = "b.Resumen";
            this.adm_gcli_bresumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_tesoreria
            // 
            this.adm_grp_tesoreria.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gtes_bancoctas,
            this.adm_gtes_banco});
            this.adm_grp_tesoreria.Name = "adm_grp_tesoreria";
            this.adm_grp_tesoreria.Text = "adm_grp_tesoreria";
            // 
            // adm_gtes_bancoctas
            // 
            this.adm_gtes_bancoctas.AutoSize = false;
            this.adm_gtes_bancoctas.Bounds = new System.Drawing.Rectangle(1, 2, 83, 62);
            this.adm_gtes_bancoctas.Image = global::Jaeger.UI.Properties.Resources.bank_cards_30px;
            this.adm_gtes_bancoctas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gtes_bancoctas.Name = "adm_gtes_bancoctas";
            this.adm_gtes_bancoctas.Text = "b.Cuentas";
            this.adm_gtes_bancoctas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gtes_bancoctas.TextWrap = true;
            this.adm_gtes_bancoctas.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_banco
            // 
            this.adm_gtes_banco.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gtes_banco.AutoSize = false;
            this.adm_gtes_banco.Bounds = new System.Drawing.Rectangle(0, 0, 76, 62);
            this.adm_gtes_banco.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gtes_banco.ExpandArrowButton = false;
            this.adm_gtes_banco.Image = global::Jaeger.UI.Properties.Resources.bank_30px;
            this.adm_gtes_banco.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gtes_banco.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gtes_bformapago,
            this.adm_gtes_bconceptos,
            this.adm_gtes_bbeneficiario,
            this.adm_gtes_bmovimiento});
            this.adm_gtes_banco.Name = "adm_gtes_banco";
            this.adm_gtes_banco.StretchHorizontally = true;
            this.adm_gtes_banco.Text = "b.Banco";
            this.adm_gtes_banco.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gtes_bformapago
            // 
            this.adm_gtes_bformapago.Name = "adm_gtes_bformapago";
            this.adm_gtes_bformapago.Text = "b.FormaPago";
            this.adm_gtes_bformapago.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_bconceptos
            // 
            this.adm_gtes_bconceptos.Name = "adm_gtes_bconceptos";
            this.adm_gtes_bconceptos.Text = "b.Conceptos";
            this.adm_gtes_bconceptos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_bbeneficiario
            // 
            this.adm_gtes_bbeneficiario.Name = "adm_gtes_bbeneficiario";
            this.adm_gtes_bbeneficiario.Text = "b.Beneficiario";
            this.adm_gtes_bbeneficiario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_bmovimiento
            // 
            this.adm_gtes_bmovimiento.Name = "adm_gtes_bmovimiento";
            this.adm_gtes_bmovimiento.Text = "b.Movimientos";
            this.adm_gtes_bmovimiento.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_contable
            // 
            this.adm_grp_contable.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcontable_catcuentas,
            this.adm_gcontable_polizas,
            this.adm_gcontable_activos,
            this.adm_gcontable_configurar});
            this.adm_grp_contable.Name = "adm_grp_contable";
            this.adm_grp_contable.Text = "adm_grp_contable";
            // 
            // adm_gcontable_catcuentas
            // 
            this.adm_gcontable_catcuentas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gcontable_catcuentas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gcontable_catcuentas.ExpandArrowButton = false;
            this.adm_gcontable_catcuentas.Image = global::Jaeger.UI.Properties.Resources.cashbook_32;
            this.adm_gcontable_catcuentas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_catcuentas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcontable_catrubros,
            this.adm_gcontable_ctacontable});
            this.adm_gcontable_catcuentas.Name = "adm_gcontable_catcuentas";
            this.adm_gcontable_catcuentas.Text = "b.CatCtas";
            this.adm_gcontable_catcuentas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_catrubros
            // 
            this.adm_gcontable_catrubros.Name = "adm_gcontable_catrubros";
            this.adm_gcontable_catrubros.Text = "b.Rubros";
            // 
            // adm_gcontable_ctacontable
            // 
            this.adm_gcontable_ctacontable.Name = "adm_gcontable_ctacontable";
            this.adm_gcontable_ctacontable.Text = "b.CatCuentas";
            // 
            // adm_gcontable_polizas
            // 
            this.adm_gcontable_polizas.Image = global::Jaeger.UI.Properties.Resources.general_ledger_30px;
            this.adm_gcontable_polizas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_polizas.Name = "adm_gcontable_polizas";
            this.adm_gcontable_polizas.Text = "b.Polizas";
            this.adm_gcontable_polizas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_activos
            // 
            this.adm_gcontable_activos.Image = global::Jaeger.UI.Properties.Resources.book_shelf_30px;
            this.adm_gcontable_activos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_activos.Name = "adm_gcontable_activos";
            this.adm_gcontable_activos.Text = "b.Activos";
            this.adm_gcontable_activos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_configurar
            // 
            this.adm_gcontable_configurar.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gcontable_configurar.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gcontable_configurar.ExpandArrowButton = false;
            this.adm_gcontable_configurar.Image = global::Jaeger.UI.Properties.Resources.settings_30px;
            this.adm_gcontable_configurar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_configurar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcontable_cattipo,
            this.adm_gcontable_creartablas});
            this.adm_gcontable_configurar.Name = "adm_gcontable_configurar";
            this.adm_gcontable_configurar.Text = "b.Configura";
            this.adm_gcontable_configurar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_cattipo
            // 
            this.adm_gcontable_cattipo.Name = "adm_gcontable_cattipo";
            this.adm_gcontable_cattipo.Text = "b.CatTipo";
            // 
            // adm_gcontable_creartablas
            // 
            this.adm_gcontable_creartablas.Name = "adm_gcontable_creartablas";
            this.adm_gcontable_creartablas.Text = "b.CrearTablas";
            // 
            // adm_grp_almacen
            // 
            this.adm_grp_almacen.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_galmacen_mprima,
            this.adm_galmacen_pterminado});
            this.adm_grp_almacen.Name = "adm_grp_almacen";
            this.adm_grp_almacen.Text = "adm_grp_almacen";
            // 
            // adm_galmacen_mprima
            // 
            this.adm_galmacen_mprima.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_galmacen_mprima.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_galmacen_mprima.ExpandArrowButton = false;
            this.adm_galmacen_mprima.Image = global::Jaeger.UI.Properties.Resources.product_30px;
            this.adm_galmacen_mprima.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_galmacen_mprima.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_galmmp_categoria,
            this.adm_galmmp_catalogo,
            this.adm_galmmp_modelo,
            this.adm_galmmp_reqcompra,
            this.adm_galmmp_ordcompra,
            this.adm_galmmp_vales,
            this.adm_galmmp_devolucion,
            this.adm_galmmp_existencia,
            this.adm_galmmp_cunidad});
            this.adm_galmacen_mprima.Name = "adm_galmacen_mprima";
            this.adm_galmacen_mprima.Text = "b.MatPrima";
            this.adm_galmacen_mprima.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_galmmp_categoria
            // 
            this.adm_galmmp_categoria.Name = "adm_galmmp_categoria";
            this.adm_galmmp_categoria.Text = "b.Categoria";
            this.adm_galmmp_categoria.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_catalogo
            // 
            this.adm_galmmp_catalogo.Name = "adm_galmmp_catalogo";
            this.adm_galmmp_catalogo.Text = "b.Catalogo";
            this.adm_galmmp_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_modelo
            // 
            this.adm_galmmp_modelo.Name = "adm_galmmp_modelo";
            this.adm_galmmp_modelo.Text = "b.ProdModelo";
            this.adm_galmmp_modelo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_reqcompra
            // 
            this.adm_galmmp_reqcompra.Image = global::Jaeger.UI.Properties.Resources.purchase_order_16px;
            this.adm_galmmp_reqcompra.MinSize = new System.Drawing.Size(0, 20);
            this.adm_galmmp_reqcompra.Name = "adm_galmmp_reqcompra";
            this.adm_galmmp_reqcompra.Text = "bReqCompra";
            this.adm_galmmp_reqcompra.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_galmmp_reqcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_galmmp_reqcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_ordcompra
            // 
            this.adm_galmmp_ordcompra.Name = "adm_galmmp_ordcompra";
            this.adm_galmmp_ordcompra.Text = "b.OrdCompra";
            this.adm_galmmp_ordcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_vales
            // 
            this.adm_galmmp_vales.Name = "adm_galmmp_vales";
            this.adm_galmmp_vales.Text = "b.Vales";
            this.adm_galmmp_vales.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_devolucion
            // 
            this.adm_galmmp_devolucion.Name = "adm_galmmp_devolucion";
            this.adm_galmmp_devolucion.Text = "b.Devolucion";
            this.adm_galmmp_devolucion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_existencia
            // 
            this.adm_galmmp_existencia.Name = "adm_galmmp_existencia";
            this.adm_galmmp_existencia.Text = "b.Existencia";
            this.adm_galmmp_existencia.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_cunidad
            // 
            this.adm_galmmp_cunidad.Name = "adm_galmmp_cunidad";
            this.adm_galmmp_cunidad.Text = "b.Cat. Unidad";
            this.adm_galmmp_cunidad.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmacen_pterminado
            // 
            this.adm_galmacen_pterminado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_galmacen_pterminado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_galmacen_pterminado.ExpandArrowButton = false;
            this.adm_galmacen_pterminado.Image = global::Jaeger.UI.Properties.Resources.new_product_30px;
            this.adm_galmacen_pterminado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_galmacen_pterminado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_galmpt_catalogo,
            this.adm_galmpt_entrada,
            this.adm_galmpt_salida,
            this.adm_galmpt_existencia,
            this.adm_galmpt_cunidad});
            this.adm_galmacen_pterminado.Name = "adm_galmacen_pterminado";
            this.adm_galmacen_pterminado.Text = "b.ProdTerminado";
            this.adm_galmacen_pterminado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_galmpt_catalogo
            // 
            this.adm_galmpt_catalogo.Name = "adm_galmpt_catalogo";
            this.adm_galmpt_catalogo.Text = "b.Catalogo";
            this.adm_galmpt_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_entrada
            // 
            this.adm_galmpt_entrada.Name = "adm_galmpt_entrada";
            this.adm_galmpt_entrada.Text = "bVales";
            this.adm_galmpt_entrada.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_salida
            // 
            this.adm_galmpt_salida.Name = "adm_galmpt_salida";
            this.adm_galmpt_salida.Text = "b.Salida";
            this.adm_galmpt_salida.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_existencia
            // 
            this.adm_galmpt_existencia.Name = "adm_galmpt_existencia";
            this.adm_galmpt_existencia.Text = "b.Existencia";
            this.adm_galmpt_existencia.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_cunidad
            // 
            this.adm_galmpt_cunidad.Name = "adm_galmpt_cunidad";
            this.adm_galmpt_cunidad.Text = "b.Cat.Unidad";
            this.adm_galmpt_cunidad.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_proveedor
            // 
            this.adm_grp_proveedor.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gprv_expediente,
            this.adm_gprv_subgrupo,
            this.adm_gprv_pagos,
            this.adm_gprv_reportes});
            this.adm_grp_proveedor.Name = "adm_grp_proveedor";
            this.adm_grp_proveedor.Text = "adm_grp_proveedor";
            // 
            // adm_gprv_expediente
            // 
            this.adm_gprv_expediente.Image = global::Jaeger.UI.Properties.Resources.supplier_32;
            this.adm_gprv_expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gprv_expediente.Name = "adm_gprv_expediente";
            this.adm_gprv_expediente.Text = "b.Expediente";
            this.adm_gprv_expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gprv_expediente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_subgrupo
            // 
            this.adm_gprv_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gprv_ordcompra,
            this.adm_gprv_validador,
            this.adm_gprv_facturacion});
            this.adm_gprv_subgrupo.Name = "adm_gprv_subgrupo";
            this.adm_gprv_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gprv_subgrupo.Text = "g.SubGrupo";
            // 
            // adm_gprv_ordcompra
            // 
            this.adm_gprv_ordcompra.Image = global::Jaeger.UI.Properties.Resources.purchase_order_16px;
            this.adm_gprv_ordcompra.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gprv_ordcompra.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gprv_ordcompra.Name = "adm_gprv_ordcompra";
            this.adm_gprv_ordcompra.ShowBorder = false;
            this.adm_gprv_ordcompra.Text = "b.OrdenCompra";
            this.adm_gprv_ordcompra.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gprv_ordcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gprv_ordcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_validador
            // 
            this.adm_gprv_validador.Image = global::Jaeger.UI.Properties.Resources.protect_16px;
            this.adm_gprv_validador.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gprv_validador.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gprv_validador.Name = "adm_gprv_validador";
            this.adm_gprv_validador.ShowBorder = false;
            this.adm_gprv_validador.Text = "b.Validar";
            this.adm_gprv_validador.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gprv_validador.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gprv_validador.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_facturacion
            // 
            this.adm_gprv_facturacion.Image = global::Jaeger.UI.Properties.Resources.invoice_16px;
            this.adm_gprv_facturacion.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gprv_facturacion.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gprv_facturacion.Name = "adm_gprv_facturacion";
            this.adm_gprv_facturacion.ShowBorder = false;
            this.adm_gprv_facturacion.Text = "b.Comprobante";
            this.adm_gprv_facturacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gprv_facturacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gprv_facturacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_pagos
            // 
            this.adm_gprv_pagos.Image = global::Jaeger.UI.Properties.Resources.check_book_30px;
            this.adm_gprv_pagos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gprv_pagos.Name = "adm_gprv_pagos";
            this.adm_gprv_pagos.Text = "b.Pago";
            this.adm_gprv_pagos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gprv_pagos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_reportes
            // 
            this.adm_gprv_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gprv_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gprv_reportes.ExpandArrowButton = false;
            this.adm_gprv_reportes.Image = global::Jaeger.UI.Properties.Resources.edit_graph_report_30px;
            this.adm_gprv_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gprv_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gprv_edocuenta,
            this.adm_gprv_credito,
            this.adm_gprv_cpago2,
            this.adm_gprv_resumen});
            this.adm_gprv_reportes.Name = "adm_gprv_reportes";
            this.adm_gprv_reportes.Text = "b.Reporte";
            this.adm_gprv_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gprv_edocuenta
            // 
            this.adm_gprv_edocuenta.Name = "adm_gprv_edocuenta";
            this.adm_gprv_edocuenta.Text = "b.EdoCuenta";
            this.adm_gprv_edocuenta.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_credito
            // 
            this.adm_gprv_credito.Name = "adm_gprv_credito";
            this.adm_gprv_credito.Text = "b.Reporte1";
            this.adm_gprv_credito.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_cpago2
            // 
            this.adm_gprv_cpago2.Name = "adm_gprv_cpago2";
            this.adm_gprv_cpago2.Text = "b.ComplementoPago";
            this.adm_gprv_cpago2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_resumen
            // 
            this.adm_gprv_resumen.Name = "adm_gprv_resumen";
            this.adm_gprv_resumen.Text = "b.Resumen";
            this.adm_gprv_resumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_nomina
            // 
            this.adm_grp_nomina.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_empleado,
            this.adm_gnom_gmov,
            this.adm_gnom_recibo,
            this.adm_gnom_parame});
            this.adm_grp_nomina.Name = "adm_grp_nomina";
            this.adm_grp_nomina.Text = "adm_grp_nomina";
            // 
            // adm_gnom_empleado
            // 
            this.adm_gnom_empleado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gnom_empleado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gnom_empleado.ExpandArrowButton = false;
            this.adm_gnom_empleado.Image = global::Jaeger.UI.Properties.Resources.workers_30px;
            this.adm_gnom_empleado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_empleado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_empexp,
            this.adm_gnom_empcont});
            this.adm_gnom_empleado.Name = "adm_gnom_empleado";
            this.adm_gnom_empleado.Text = "b.Empleados";
            this.adm_gnom_empleado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gnom_empexp
            // 
            this.adm_gnom_empexp.Name = "adm_gnom_empexp";
            this.adm_gnom_empexp.Text = "b.Expediente";
            // 
            // adm_gnom_empcont
            // 
            this.adm_gnom_empcont.Name = "adm_gnom_empcont";
            this.adm_gnom_empcont.Text = "b.Contratos";
            // 
            // adm_gnom_gmov
            // 
            this.adm_gnom_gmov.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_gmov.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_periodo,
            this.adm_gnom_sgmov,
            this.adm_gnom_aguina});
            this.adm_gnom_gmov.Name = "adm_gnom_gmov";
            this.adm_gnom_gmov.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gnom_gmov.Text = "g.Movimientos";
            // 
            // adm_gnom_periodo
            // 
            this.adm_gnom_periodo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gnom_periodo.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_periodo.Name = "adm_gnom_periodo";
            this.adm_gnom_periodo.ShowBorder = false;
            this.adm_gnom_periodo.Text = "b.Periodo";
            this.adm_gnom_periodo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gnom_periodo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_sgmov
            // 
            this.adm_gnom_sgmov.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gnom_sgmov.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gnom_sgmov.ExpandArrowButton = false;
            this.adm_gnom_sgmov.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_movvac,
            this.adm_gnom_movfal,
            this.adm_gnom_movacu});
            this.adm_gnom_sgmov.Margin = new System.Windows.Forms.Padding(0);
            this.adm_gnom_sgmov.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_sgmov.Name = "adm_gnom_sgmov";
            this.adm_gnom_sgmov.Text = "b.Registro";
            this.adm_gnom_sgmov.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adm_gnom_movvac
            // 
            this.adm_gnom_movvac.Name = "adm_gnom_movvac";
            this.adm_gnom_movvac.Text = "b.Vacaciones";
            this.adm_gnom_movvac.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_movfal
            // 
            this.adm_gnom_movfal.Name = "adm_gnom_movfal";
            this.adm_gnom_movfal.Text = "b.Faltas";
            this.adm_gnom_movfal.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_movacu
            // 
            this.adm_gnom_movacu.Name = "adm_gnom_movacu";
            this.adm_gnom_movacu.Text = "b.Acumulados";
            this.adm_gnom_movacu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_aguina
            // 
            this.adm_gnom_aguina.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gnom_aguina.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_aguina.Name = "adm_gnom_aguina";
            this.adm_gnom_aguina.ShowBorder = false;
            this.adm_gnom_aguina.Text = "b.Aguinaldo";
            this.adm_gnom_aguina.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adm_gnom_recibo
            // 
            this.adm_gnom_recibo.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gnom_recibo.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gnom_recibo.ExpandArrowButton = false;
            this.adm_gnom_recibo.Image = global::Jaeger.UI.Properties.Resources.money_transfer_30px;
            this.adm_gnom_recibo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_recibo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_reccon,
            this.adm_gnom_recres});
            this.adm_gnom_recibo.Name = "adm_gnom_recibo";
            this.adm_gnom_recibo.Text = "b.Recibos";
            this.adm_gnom_recibo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gnom_reccon
            // 
            this.adm_gnom_reccon.Name = "adm_gnom_reccon";
            this.adm_gnom_reccon.Text = "b.Consulta";
            this.adm_gnom_reccon.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_recres
            // 
            this.adm_gnom_recres.Name = "adm_gnom_recres";
            this.adm_gnom_recres.Text = "b.Resumen";
            this.adm_gnom_recres.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_parame
            // 
            this.adm_gnom_parame.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_parame.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_conf,
            this.adm_gnom_gconcep,
            this.adm_gnom_tablas});
            this.adm_gnom_parame.Name = "adm_gnom_parame";
            this.adm_gnom_parame.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gnom_parame.Text = "g.Parametros";
            // 
            // adm_gnom_conf
            // 
            this.adm_gnom_conf.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gnom_conf.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_conf.Name = "adm_gnom_conf";
            this.adm_gnom_conf.ShowBorder = false;
            this.adm_gnom_conf.Text = "b.Configuración";
            // 
            // adm_gnom_gconcep
            // 
            this.adm_gnom_gconcep.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gnom_gconcep.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_gconcep.Name = "adm_gnom_gconcep";
            this.adm_gnom_gconcep.ShowBorder = false;
            this.adm_gnom_gconcep.Text = "b.Conceptos";
            this.adm_gnom_gconcep.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adm_gnom_tablas
            // 
            this.adm_gnom_tablas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gnom_tablas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gnom_tablas.ExpandArrowButton = false;
            this.adm_gnom_tablas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_tisr,
            this.adm_gnom_tsbe,
            this.adm_gnom_tsm,
            this.adm_gnom_tuma});
            this.adm_gnom_tablas.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gnom_tablas.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_tablas.Name = "adm_gnom_tablas";
            this.adm_gnom_tablas.Text = "b.Tablas";
            this.adm_gnom_tablas.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // adm_gnom_tisr
            // 
            this.adm_gnom_tisr.Name = "adm_gnom_tisr";
            this.adm_gnom_tisr.Text = "ISR";
            // 
            // adm_gnom_tsbe
            // 
            this.adm_gnom_tsbe.Name = "adm_gnom_tsbe";
            this.adm_gnom_tsbe.Text = "b.Subsidio al Empleo";
            // 
            // adm_gnom_tsm
            // 
            this.adm_gnom_tsm.Name = "adm_gnom_tsm";
            this.adm_gnom_tsm.Text = "b.Salarios Mínimos";
            // 
            // adm_gnom_tuma
            // 
            this.adm_gnom_tuma.Name = "adm_gnom_tuma";
            this.adm_gnom_tuma.Text = "b.UMA";
            // 
            // TProd
            // 
            this.TProd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TProd.IsSelected = false;
            this.TProd.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_grp_directorio,
            this.cpcot_grp_cotizacion,
            this.cppro_grp_produccion,
            this.cpvnt_grp_ventas,
            this.ccal_grp_nconf});
            this.TProd.Name = "TProd";
            this.TProd.Text = "TProducción";
            this.TProd.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TProd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.TProd.UseMnemonic = false;
            // 
            // cpemp_grp_directorio
            // 
            this.cpemp_grp_directorio.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_gdir_directorio,
            this.cpemp_grp_producto});
            this.cpemp_grp_directorio.Name = "cpemp_grp_directorio";
            this.cpemp_grp_directorio.Text = "g.Directorio";
            // 
            // cpemp_gdir_directorio
            // 
            this.cpemp_gdir_directorio.Image = global::Jaeger.UI.Properties.Resources.male_user_32px;
            this.cpemp_gdir_directorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpemp_gdir_directorio.Name = "cpemp_gdir_directorio";
            this.cpemp_gdir_directorio.Text = "b.Directorio";
            this.cpemp_gdir_directorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpemp_gdir_directorio.UseCompatibleTextRendering = false;
            this.cpemp_gdir_directorio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpemp_grp_producto
            // 
            this.cpemp_grp_producto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_gprd_clasificacion,
            this.cpemp_gprd_catalogo});
            this.cpemp_grp_producto.Name = "cpemp_grp_producto";
            this.cpemp_grp_producto.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cpemp_grp_producto.Text = "cpemp_grp_producto";
            // 
            // cpemp_gprd_clasificacion
            // 
            this.cpemp_gprd_clasificacion.Name = "cpemp_gprd_clasificacion";
            this.cpemp_gprd_clasificacion.ShowBorder = false;
            this.cpemp_gprd_clasificacion.Text = "b.Clasificacion";
            this.cpemp_gprd_clasificacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpemp_gprd_clasificacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpemp_gprd_catalogo
            // 
            this.cpemp_gprd_catalogo.Name = "cpemp_gprd_catalogo";
            this.cpemp_gprd_catalogo.ShowBorder = false;
            this.cpemp_gprd_catalogo.Text = "b.Catalogo";
            this.cpemp_gprd_catalogo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpemp_gprd_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_grp_cotizacion
            // 
            this.cpcot_grp_cotizacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_gcot_productos,
            this.cpcot_gcot_presupuesto,
            this.cpcot_gcot_cotizaciones,
            this.cpcot_gcot_cliente,
            this.cpcot_gcot_conf});
            this.cpcot_grp_cotizacion.Name = "cpcot_grp_cotizacion";
            this.cpcot_grp_cotizacion.Text = "g.Cotizacion";
            // 
            // cpcot_gcot_productos
            // 
            this.cpcot_gcot_productos.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpcot_gcot_productos.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpcot_gcot_productos.ExpandArrowButton = false;
            this.cpcot_gcot_productos.Image = global::Jaeger.UI.Properties.Resources.product_30px;
            this.cpcot_gcot_productos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_productos.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_bprd_producto,
            this.cpcot_bprd_subproducto});
            this.cpcot_gcot_productos.Name = "cpcot_gcot_productos";
            this.cpcot_gcot_productos.Text = "b.Productos";
            this.cpcot_gcot_productos.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_productos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpcot_bprd_producto
            // 
            this.cpcot_bprd_producto.Name = "cpcot_bprd_producto";
            this.cpcot_bprd_producto.Text = "b.Productos";
            // 
            // cpcot_bprd_subproducto
            // 
            this.cpcot_bprd_subproducto.Name = "cpcot_bprd_subproducto";
            this.cpcot_bprd_subproducto.Text = "b.SubProductos";
            // 
            // cpcot_gcot_presupuesto
            // 
            this.cpcot_gcot_presupuesto.Image = global::Jaeger.UI.Properties.Resources.estimate_30px;
            this.cpcot_gcot_presupuesto.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_presupuesto.Name = "cpcot_gcot_presupuesto";
            this.cpcot_gcot_presupuesto.StretchHorizontally = true;
            this.cpcot_gcot_presupuesto.StretchVertically = true;
            this.cpcot_gcot_presupuesto.Text = "b.Presupuestos";
            this.cpcot_gcot_presupuesto.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_presupuesto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpcot_gcot_cotizaciones
            // 
            this.cpcot_gcot_cotizaciones.Image = global::Jaeger.UI.Properties.Resources.accounting_30px;
            this.cpcot_gcot_cotizaciones.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_cotizaciones.Name = "cpcot_gcot_cotizaciones";
            this.cpcot_gcot_cotizaciones.StretchHorizontally = true;
            this.cpcot_gcot_cotizaciones.StretchVertically = true;
            this.cpcot_gcot_cotizaciones.Text = "b.Cotizaciones";
            this.cpcot_gcot_cotizaciones.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_cotizaciones.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpcot_gcot_cotizaciones.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_cliente
            // 
            this.cpcot_gcot_cliente.Image = global::Jaeger.UI.Properties.Resources.male_user_32px;
            this.cpcot_gcot_cliente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_cliente.Name = "cpcot_gcot_cliente";
            this.cpcot_gcot_cliente.Text = "b.Cliente";
            this.cpcot_gcot_cliente.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_cliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpcot_gcot_cliente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_conf
            // 
            this.cpcot_gcot_conf.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpcot_gcot_conf.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpcot_gcot_conf.ExpandArrowButton = false;
            this.cpcot_gcot_conf.Image = global::Jaeger.UI.Properties.Resources.administrative_tools_32px;
            this.cpcot_gcot_conf.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_conf.Name = "cpcot_gcot_conf";
            this.cpcot_gcot_conf.Text = "b.Configuracion";
            this.cpcot_gcot_conf.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_conf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cppro_grp_produccion
            // 
            this.cppro_grp_produccion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_gpro_orden,
            this.cppro_gpro_reqcompra,
            this.cppro_gpro_planea,
            this.cppro_gpro_sgrupo,
            this.cppro_gpro_remision});
            this.cppro_grp_produccion.Name = "cppro_grp_produccion";
            this.cppro_grp_produccion.Text = "g.Produccion";
            this.cppro_grp_produccion.UseCompatibleTextRendering = false;
            // 
            // cppro_gpro_orden
            // 
            this.cppro_gpro_orden.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_orden.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_orden.ExpandArrowButton = false;
            this.cppro_gpro_orden.Image = global::Jaeger.UI.Properties.Resources.production_order_history_30px;
            this.cppro_gpro_orden.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_orden.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_bord_historial,
            this.cppro_bord_proceso});
            this.cppro_gpro_orden.Name = "cppro_gpro_orden";
            this.cppro_gpro_orden.Text = "b.Ord.Produccion";
            this.cppro_gpro_orden.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_orden.UseCompatibleTextRendering = false;
            // 
            // cppro_bord_historial
            // 
            this.cppro_bord_historial.Name = "cppro_bord_historial";
            this.cppro_bord_historial.Text = "b.Historial";
            this.cppro_bord_historial.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_bord_proceso
            // 
            this.cppro_bord_proceso.Name = "cppro_bord_proceso";
            this.cppro_bord_proceso.Text = "b.EnProceso";
            this.cppro_bord_proceso.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_reqcompra
            // 
            this.cppro_gpro_reqcompra.Image = global::Jaeger.UI.Properties.Resources.archive_list_of_parts_30px;
            this.cppro_gpro_reqcompra.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_reqcompra.Name = "cppro_gpro_reqcompra";
            this.cppro_gpro_reqcompra.Text = "b.Req.Compra";
            this.cppro_gpro_reqcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_reqcompra.TextWrap = true;
            this.cppro_gpro_reqcompra.UseCompatibleTextRendering = false;
            this.cppro_gpro_reqcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_planea
            // 
            this.cppro_gpro_planea.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_planea.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_planea.ExpandArrowButton = false;
            this.cppro_gpro_planea.Image = global::Jaeger.UI.Properties.Resources.calendar_30px;
            this.cppro_gpro_planea.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_planea.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_gpro_calendario,
            this.cppro_gpro_gantt,
            this.cppro_gpro_plan,
            this.cppro_gpro_tieadd});
            this.cppro_gpro_planea.Name = "cppro_gpro_planea";
            this.cppro_gpro_planea.Text = "b.Calendario";
            this.cppro_gpro_planea.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_planea.UseCompatibleTextRendering = false;
            // 
            // cppro_gpro_calendario
            // 
            this.cppro_gpro_calendario.Name = "cppro_gpro_calendario";
            this.cppro_gpro_calendario.Text = "cppro_gpro_calendario";
            this.cppro_gpro_calendario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_gantt
            // 
            this.cppro_gpro_gantt.Name = "cppro_gpro_gantt";
            this.cppro_gpro_gantt.Text = "cppro_gpro_gantt";
            this.cppro_gpro_gantt.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_plan
            // 
            this.cppro_gpro_plan.Name = "cppro_gpro_plan";
            this.cppro_gpro_plan.Text = "cppro_gpro_plan";
            this.cppro_gpro_plan.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_tieadd
            // 
            this.cppro_gpro_tieadd.Name = "cppro_gpro_tieadd";
            this.cppro_gpro_tieadd.Text = "b.TieExtra";
            // 
            // cppro_gpro_sgrupo
            // 
            this.cppro_gpro_sgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_sgrp_depto,
            this.cppro_sgrp_avance,
            this.cppro_sgrp_evaluacion});
            this.cppro_gpro_sgrupo.Name = "cppro_gpro_sgrupo";
            this.cppro_gpro_sgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cppro_gpro_sgrupo.Text = "g.SubGrupo";
            this.cppro_gpro_sgrupo.UseCompatibleTextRendering = false;
            // 
            // cppro_sgrp_depto
            // 
            this.cppro_sgrp_depto.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_sgrp_depto.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_sgrp_depto.ExpandArrowButton = false;
            this.cppro_sgrp_depto.Image = global::Jaeger.UI.Properties.Resources.calendar_16;
            this.cppro_sgrp_depto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_bdepto_trabaj,
            this.cppro_bdepto_proces});
            this.cppro_sgrp_depto.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_depto.Name = "cppro_sgrp_depto";
            this.cppro_sgrp_depto.Text = "g.Departamento";
            this.cppro_sgrp_depto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_depto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // cppro_bdepto_trabaj
            // 
            this.cppro_bdepto_trabaj.Name = "cppro_bdepto_trabaj";
            this.cppro_bdepto_trabaj.Text = "b.Trabajadores";
            this.cppro_bdepto_trabaj.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_bdepto_proces
            // 
            this.cppro_bdepto_proces.Name = "cppro_bdepto_proces";
            this.cppro_bdepto_proces.Text = "b.Procesos";
            this.cppro_bdepto_proces.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_sgrp_avance
            // 
            this.cppro_sgrp_avance.Image = global::Jaeger.UI.Properties.Resources.production_in_progress_16px;
            this.cppro_sgrp_avance.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_avance.Name = "cppro_sgrp_avance";
            this.cppro_sgrp_avance.ShowBorder = false;
            this.cppro_sgrp_avance.Text = "b.Avance";
            this.cppro_sgrp_avance.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_avance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cppro_sgrp_avance.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_sgrp_evaluacion
            // 
            this.cppro_sgrp_evaluacion.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_sgrp_evaluacion.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_sgrp_evaluacion.ExpandArrowButton = false;
            this.cppro_sgrp_evaluacion.Image = global::Jaeger.UI.Properties.Resources.edit_graph_report_16px;
            this.cppro_sgrp_evaluacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_beva_eprodof,
            this.cppro_beva_eproduccion,
            this.cppro_beva_eperiodo});
            this.cppro_sgrp_evaluacion.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_evaluacion.Name = "cppro_sgrp_evaluacion";
            this.cppro_sgrp_evaluacion.Text = "b.Evaluacion";
            this.cppro_sgrp_evaluacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_evaluacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // cppro_beva_eprodof
            // 
            this.cppro_beva_eprodof.Name = "cppro_beva_eprodof";
            this.cppro_beva_eprodof.Text = "e.Evaluación";
            this.cppro_beva_eprodof.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_beva_eproduccion
            // 
            this.cppro_beva_eproduccion.Name = "cppro_beva_eproduccion";
            this.cppro_beva_eproduccion.Text = "b.E.Produccion";
            this.cppro_beva_eproduccion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_beva_eperiodo
            // 
            this.cppro_beva_eperiodo.Name = "cppro_beva_eperiodo";
            this.cppro_beva_eperiodo.Text = "b.E.Periodo";
            this.cppro_beva_eperiodo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_remision
            // 
            this.cppro_gpro_remision.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_remision.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_remision.ExpandArrowButton = false;
            this.cppro_gpro_remision.Image = global::Jaeger.UI.Properties.Resources.production_finished_30px;
            this.cppro_gpro_remision.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_remision.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_brem_Emitido,
            this.cppro_brem_Recibido});
            this.cppro_gpro_remision.Name = "cppro_gpro_remision";
            this.cppro_gpro_remision.Text = "b.Remisionado";
            this.cppro_gpro_remision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cppro_brem_Emitido
            // 
            this.cppro_brem_Emitido.Name = "cppro_brem_Emitido";
            this.cppro_brem_Emitido.Text = "b.Emitido";
            this.cppro_brem_Emitido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_brem_Recibido
            // 
            this.cppro_brem_Recibido.Name = "cppro_brem_Recibido";
            this.cppro_brem_Recibido.Text = "b.Recibido";
            this.cppro_brem_Recibido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_grp_ventas
            // 
            this.cpvnt_grp_ventas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_vendedor,
            this.cpvnt_gven_prodvend,
            this.cpvnt_gven_group1,
            this.cpvnt_gven_remisionado,
            this.cpvnt_gven_reportes});
            this.cpvnt_grp_ventas.Name = "cpvnt_grp_ventas";
            this.cpvnt_grp_ventas.Text = "g.Ventas";
            // 
            // cpvnt_gven_vendedor
            // 
            this.cpvnt_gven_vendedor.Image = global::Jaeger.UI.Properties.Resources.businessman_30px;
            this.cpvnt_gven_vendedor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_vendedor.Name = "cpvnt_gven_vendedor";
            this.cpvnt_gven_vendedor.Text = "b.Vendedor";
            this.cpvnt_gven_vendedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpvnt_gven_vendedor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_prodvend
            // 
            this.cpvnt_gven_prodvend.Image = global::Jaeger.UI.Properties.Resources.new_product_30px;
            this.cpvnt_gven_prodvend.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_prodvend.Name = "cpvnt_gven_prodvend";
            this.cpvnt_gven_prodvend.Text = "b.Productos";
            this.cpvnt_gven_prodvend.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpvnt_gven_prodvend.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_group1
            // 
            this.cpvnt_gven_group1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_pedidos,
            this.cpvnt_gven_comision,
            this.cpvnt_gven_ventcosto});
            this.cpvnt_gven_group1.Name = "cpvnt_gven_group1";
            this.cpvnt_gven_group1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cpvnt_gven_group1.Text = "g.Pedidos";
            // 
            // cpvnt_gven_pedidos
            // 
            this.cpvnt_gven_pedidos.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_pedidos.Name = "cpvnt_gven_pedidos";
            this.cpvnt_gven_pedidos.ShowBorder = false;
            this.cpvnt_gven_pedidos.Text = "b.Pedido";
            this.cpvnt_gven_pedidos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_pedidos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_pedidos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_comision
            // 
            this.cpvnt_gven_comision.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_comision.Name = "cpvnt_gven_comision";
            this.cpvnt_gven_comision.ShowBorder = false;
            this.cpvnt_gven_comision.Text = "b.Comision";
            this.cpvnt_gven_comision.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_comision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_comision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_ventcosto
            // 
            this.cpvnt_gven_ventcosto.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_ventcosto.Name = "cpvnt_gven_ventcosto";
            this.cpvnt_gven_ventcosto.ShowBorder = false;
            this.cpvnt_gven_ventcosto.Text = "b.VentaVsCosto";
            this.cpvnt_gven_ventcosto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_ventcosto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_ventcosto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_remisionado
            // 
            this.cpvnt_gven_remisionado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpvnt_gven_remisionado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpvnt_gven_remisionado.ExpandArrowButton = false;
            this.cpvnt_gven_remisionado.Image = global::Jaeger.UI.Properties.Resources.new_product_30px;
            this.cpvnt_gven_remisionado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_remisionado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_brem_historial,
            this.cpvnt_brem_porcobrar,
            this.cpvnt_brem_rempart});
            this.cpvnt_gven_remisionado.Name = "cpvnt_gven_remisionado";
            this.cpvnt_gven_remisionado.Text = "b.Remisionado";
            this.cpvnt_gven_remisionado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpvnt_brem_historial
            // 
            this.cpvnt_brem_historial.Name = "cpvnt_brem_historial";
            this.cpvnt_brem_historial.Text = "b.Remision";
            this.cpvnt_brem_historial.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_brem_porcobrar
            // 
            this.cpvnt_brem_porcobrar.Name = "cpvnt_brem_porcobrar";
            this.cpvnt_brem_porcobrar.Text = "b.PorCobrar";
            this.cpvnt_brem_porcobrar.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_brem_rempart
            // 
            this.cpvnt_brem_rempart.Name = "cpvnt_brem_rempart";
            this.cpvnt_brem_rempart.Text = "b.Partida";
            this.cpvnt_brem_rempart.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_reportes
            // 
            this.cpvnt_gven_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpvnt_gven_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpvnt_gven_reportes.ExpandArrowButton = false;
            this.cpvnt_gven_reportes.Image = global::Jaeger.UI.Properties.Resources.ratings_32px;
            this.cpvnt_gven_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_reporte1,
            this.cpvnt_gven_reporte2});
            this.cpvnt_gven_reportes.Name = "cpvnt_gven_reportes";
            this.cpvnt_gven_reportes.Text = "b.Reportes";
            this.cpvnt_gven_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpvnt_gven_reporte1
            // 
            this.cpvnt_gven_reporte1.Name = "cpvnt_gven_reporte1";
            this.cpvnt_gven_reporte1.Text = "b.Reporte";
            this.cpvnt_gven_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_reporte2
            // 
            this.cpvnt_gven_reporte2.Name = "cpvnt_gven_reporte2";
            this.cpvnt_gven_reporte2.Text = "b.Reporte";
            this.cpvnt_gven_reporte2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_grp_nconf
            // 
            this.ccal_grp_nconf.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gnco_new,
            this.ccal_gnco_reporte});
            this.ccal_grp_nconf.Name = "ccal_grp_nconf";
            this.ccal_grp_nconf.Text = "g.No Conformidad";
            // 
            // ccal_gnco_new
            // 
            this.ccal_gnco_new.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.ccal_gnco_new.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.ccal_gnco_new.ExpandArrowButton = false;
            this.ccal_gnco_new.Image = global::Jaeger.UI.Properties.Resources.documents_30px;
            this.ccal_gnco_new.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ccal_gnco_new.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gnco_todo,
            this.ccal_gnco_proceso});
            this.ccal_gnco_new.Name = "ccal_gnco_new";
            this.ccal_gnco_new.Text = "b.Nuevo";
            this.ccal_gnco_new.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ccal_gnco_todo
            // 
            this.ccal_gnco_todo.Name = "ccal_gnco_todo";
            this.ccal_gnco_todo.Text = "b.Historial";
            this.ccal_gnco_todo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_proceso
            // 
            this.ccal_gnco_proceso.Name = "ccal_gnco_proceso";
            this.ccal_gnco_proceso.Text = "b.EnProceso";
            this.ccal_gnco_proceso.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_reporte
            // 
            this.ccal_gnco_reporte.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.ccal_gnco_reporte.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.ccal_gnco_reporte.ExpandArrowButton = false;
            this.ccal_gnco_reporte.Image = global::Jaeger.UI.Properties.Resources.ratings_32px;
            this.ccal_gnco_reporte.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ccal_gnco_reporte.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gnco_reporte1,
            this.ccal_gnco_reporte2,
            this.ccal_gnco_reporte3});
            this.ccal_gnco_reporte.Name = "ccal_gnco_reporte";
            this.ccal_gnco_reporte.Text = "b.Reportes";
            this.ccal_gnco_reporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ccal_gnco_reporte1
            // 
            this.ccal_gnco_reporte1.Name = "ccal_gnco_reporte1";
            this.ccal_gnco_reporte1.Text = "b.Reporte 1";
            this.ccal_gnco_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_reporte2
            // 
            this.ccal_gnco_reporte2.Name = "ccal_gnco_reporte2";
            this.ccal_gnco_reporte2.Text = "b.Reporte 2";
            this.ccal_gnco_reporte2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_reporte3
            // 
            this.ccal_gnco_reporte3.Name = "ccal_gnco_reporte3";
            this.ccal_gnco_reporte3.Text = "b.Reporte 3";
            this.ccal_gnco_reporte3.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TVentas
            // 
            this.TVentas.IsSelected = false;
            this.TVentas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_grp_ventas,
            this.cmr_grp_cp});
            this.TVentas.Name = "TVentas";
            this.TVentas.Text = "TComercial";
            this.TVentas.UseMnemonic = false;
            // 
            // cmr_grp_ventas
            // 
            this.cmr_grp_ventas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gventas_vededores,
            this.cmr_gventas_subgrupo});
            this.cmr_grp_ventas.Name = "cmr_grp_ventas";
            this.cmr_grp_ventas.Text = "cmr_grp_ventas";
            // 
            // cmr_gventas_vededores
            // 
            this.cmr_gventas_vededores.Image = global::Jaeger.UI.Properties.Resources.businessman_30px;
            this.cmr_gventas_vededores.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gventas_vededores.Name = "cmr_gventas_vededores";
            this.cmr_gventas_vededores.Text = "b.Vendedor";
            this.cmr_gventas_vededores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmr_gventas_vededores.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gventas_subgrupo
            // 
            this.cmr_gventas_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gventas_pedidos,
            this.cmr_gventas_comision,
            this.cmr_gventas_ccomision});
            this.cmr_gventas_subgrupo.Name = "cmr_gventas_subgrupo";
            this.cmr_gventas_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cmr_gventas_subgrupo.Text = "cmr_gventas_subgrupo";
            // 
            // cmr_gventas_pedidos
            // 
            this.cmr_gventas_pedidos.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_pedidos.Name = "cmr_gventas_pedidos";
            this.cmr_gventas_pedidos.ShowBorder = false;
            this.cmr_gventas_pedidos.Text = "b.Pedido";
            this.cmr_gventas_pedidos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_pedidos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gventas_comision
            // 
            this.cmr_gventas_comision.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_comision.Name = "cmr_gventas_comision";
            this.cmr_gventas_comision.ShowBorder = false;
            this.cmr_gventas_comision.Text = "b.Comision";
            this.cmr_gventas_comision.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_comision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gventas_ccomision
            // 
            this.cmr_gventas_ccomision.Name = "cmr_gventas_ccomision";
            this.cmr_gventas_ccomision.ShowBorder = false;
            this.cmr_gventas_ccomision.Text = "b.CComision";
            this.cmr_gventas_ccomision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_grp_cp
            // 
            this.cmr_grp_cp.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_directorio,
            this.cmr_gcp_subgrupo,
            this.cmr_gcp_reportes,
            this.cmr_gcp_resumen});
            this.cmr_grp_cp.Name = "cmr_grp_cp";
            this.cmr_grp_cp.Text = "cmr_grp_cp";
            // 
            // cmr_gcp_directorio
            // 
            this.cmr_gcp_directorio.Image = global::Jaeger.UI.Properties.Resources.address_book_30px;
            this.cmr_gcp_directorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gcp_directorio.Name = "cmr_gcp_directorio";
            this.cmr_gcp_directorio.Text = "b.Directorio";
            this.cmr_gcp_directorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmr_gcp_directorio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_subgrupo
            // 
            this.cmr_gcp_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_ordproduccion,
            this.cmr_gcp_prodvendido,
            this.cmr_gcp_remisionado});
            this.cmr_gcp_subgrupo.Name = "cmr_gcp_subgrupo";
            this.cmr_gcp_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cmr_gcp_subgrupo.Text = "g.subgrupo";
            // 
            // cmr_gcp_ordproduccion
            // 
            this.cmr_gcp_ordproduccion.Name = "cmr_gcp_ordproduccion";
            this.cmr_gcp_ordproduccion.ShowBorder = false;
            this.cmr_gcp_ordproduccion.Text = "b.OrdProduccion";
            this.cmr_gcp_ordproduccion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gcp_ordproduccion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_prodvendido
            // 
            this.cmr_gcp_prodvendido.Name = "cmr_gcp_prodvendido";
            this.cmr_gcp_prodvendido.ShowBorder = false;
            this.cmr_gcp_prodvendido.Text = "b.ProdVendidos";
            this.cmr_gcp_prodvendido.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gcp_prodvendido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_remisionado
            // 
            this.cmr_gcp_remisionado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cmr_gcp_remisionado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cmr_gcp_remisionado.ExpandArrowButton = false;
            this.cmr_gcp_remisionado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_bremisionado,
            this.cmr_gcp_rempartida});
            this.cmr_gcp_remisionado.Name = "cmr_gcp_remisionado";
            this.cmr_gcp_remisionado.Text = "b.Remisionado";
            this.cmr_gcp_remisionado.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmr_gcp_bremisionado
            // 
            this.cmr_gcp_bremisionado.Name = "cmr_gcp_bremisionado";
            this.cmr_gcp_bremisionado.Text = "b.Remisionado";
            this.cmr_gcp_bremisionado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_rempartida
            // 
            this.cmr_gcp_rempartida.Name = "cmr_gcp_rempartida";
            this.cmr_gcp_rempartida.Text = "b.RemPartida";
            this.cmr_gcp_rempartida.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_reportes
            // 
            this.cmr_gcp_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cmr_gcp_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cmr_gcp_reportes.ExpandArrowButton = false;
            this.cmr_gcp_reportes.Image = global::Jaeger.UI.Properties.Resources.ratings_32px;
            this.cmr_gcp_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gcp_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_reporte1});
            this.cmr_gcp_reportes.Name = "cmr_gcp_reportes";
            this.cmr_gcp_reportes.Text = "b.Reportes";
            this.cmr_gcp_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cmr_gcp_reporte1
            // 
            this.cmr_gcp_reporte1.Name = "cmr_gcp_reporte1";
            this.cmr_gcp_reporte1.Text = "b.Reporte1";
            this.cmr_gcp_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_resumen
            // 
            this.cmr_gcp_resumen.Image = global::Jaeger.UI.Properties.Resources.play_graph_report_32px;
            this.cmr_gcp_resumen.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gcp_resumen.Name = "cmr_gcp_resumen";
            this.cmr_gcp_resumen.Text = "b.Resumen";
            this.cmr_gcp_resumen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmr_gcp_resumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TTools
            // 
            this.TTools.AutoEllipsis = false;
            this.TTools.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TTools.IsSelected = true;
            this.TTools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_grp_config,
            this.dsk_grp_retencion,
            this.dsk_grp_repositorio,
            this.dsk_grp_tools,
            this.dsk_grp_theme,
            this.dsk_grp_about});
            this.TTools.Name = "TTools";
            this.TTools.Text = "THerramientas";
            this.TTools.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TTools.UseCompatibleTextRendering = false;
            this.TTools.UseMnemonic = false;
            // 
            // dsk_grp_config
            // 
            this.dsk_grp_config.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_param,
            this.dsk_gconfig_cate,
            this.dsk_gconfig_cert,
            this.dsk_gconfig_series});
            this.dsk_grp_config.Name = "dsk_grp_config";
            this.dsk_grp_config.Text = "g.Configuracion";
            // 
            // dsk_gconfig_param
            // 
            this.dsk_gconfig_param.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gconfig_param.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gconfig_param.ExpandArrowButton = false;
            this.dsk_gconfig_param.Image = global::Jaeger.UI.Properties.Resources.control_panel_30px;
            this.dsk_gconfig_param.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_param.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_emisor,
            this.dsk_gconfig_avanzado,
            this.dsk_gconfig_menu,
            this.dsk_gconfig_usuario,
            this.dsk_gconfig_perfil});
            this.dsk_gconfig_param.Name = "dsk_gconfig_param";
            this.dsk_gconfig_param.Text = "b.Parametros";
            this.dsk_gconfig_param.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gconfig_emisor
            // 
            this.dsk_gconfig_emisor.Name = "dsk_gconfig_emisor";
            this.dsk_gconfig_emisor.Text = "b.Emisor";
            this.dsk_gconfig_emisor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_avanzado
            // 
            this.dsk_gconfig_avanzado.Name = "dsk_gconfig_avanzado";
            this.dsk_gconfig_avanzado.Text = "b.Avanzado";
            this.dsk_gconfig_avanzado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_menu
            // 
            this.dsk_gconfig_menu.Name = "dsk_gconfig_menu";
            this.dsk_gconfig_menu.Text = "b.Menu";
            this.dsk_gconfig_menu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_usuario
            // 
            this.dsk_gconfig_usuario.Name = "dsk_gconfig_usuario";
            this.dsk_gconfig_usuario.Text = "b.Usuarios";
            this.dsk_gconfig_usuario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_perfil
            // 
            this.dsk_gconfig_perfil.Name = "dsk_gconfig_perfil";
            this.dsk_gconfig_perfil.Text = "b.Perfil";
            this.dsk_gconfig_perfil.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_cate
            // 
            this.dsk_gconfig_cate.Image = global::Jaeger.UI.Properties.Resources.category_30px;
            this.dsk_gconfig_cate.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_cate.Name = "dsk_gconfig_cate";
            this.dsk_gconfig_cate.Text = "b.Categorías";
            this.dsk_gconfig_cate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dsk_gconfig_cate.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_cert
            // 
            this.dsk_gconfig_cert.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gconfig_cert.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gconfig_cert.ExpandArrowButton = false;
            this.dsk_gconfig_cert.Image = global::Jaeger.UI.Properties.Resources.certificate_32px;
            this.dsk_gconfig_cert.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_cert.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_certc,
            this.dsk_gconfig_certf});
            this.dsk_gconfig_cert.Name = "dsk_gconfig_cert";
            this.dsk_gconfig_cert.Text = "b.Certificado";
            this.dsk_gconfig_cert.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gconfig_certc
            // 
            this.dsk_gconfig_certc.Name = "dsk_gconfig_certc";
            this.dsk_gconfig_certc.Text = "b.CSD";
            this.dsk_gconfig_certc.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_certf
            // 
            this.dsk_gconfig_certf.Name = "dsk_gconfig_certf";
            this.dsk_gconfig_certf.Text = "b.Fiel";
            this.dsk_gconfig_certf.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_series
            // 
            this.dsk_gconfig_series.Image = global::Jaeger.UI.Properties.Resources.counter_32px;
            this.dsk_gconfig_series.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_series.Name = "dsk_gconfig_series";
            this.dsk_gconfig_series.Text = "b.Series";
            this.dsk_gconfig_series.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_grp_retencion
            // 
            this.dsk_grp_retencion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tdks_gretencion_recep,
            this.tdks_gretencion_comprobante});
            this.dsk_grp_retencion.Name = "dsk_grp_retencion";
            this.dsk_grp_retencion.Text = "g.Retencion";
            // 
            // tdks_gretencion_recep
            // 
            this.tdks_gretencion_recep.Image = global::Jaeger.UI.Properties.Resources.customer_30px;
            this.tdks_gretencion_recep.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tdks_gretencion_recep.Name = "tdks_gretencion_recep";
            this.tdks_gretencion_recep.Text = "b.Receptores";
            this.tdks_gretencion_recep.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tdks_gretencion_comprobante
            // 
            this.tdks_gretencion_comprobante.Image = global::Jaeger.UI.Properties.Resources.invoice_30px;
            this.tdks_gretencion_comprobante.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tdks_gretencion_comprobante.Name = "tdks_gretencion_comprobante";
            this.tdks_gretencion_comprobante.Text = "b.Comprobante";
            this.tdks_gretencion_comprobante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_grp_tools
            // 
            this.dsk_grp_tools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_backup,
            this.dsk_gtools_validador,
            this.dsk_gtools_actualiza});
            this.dsk_grp_tools.MaxSize = new System.Drawing.Size(0, 0);
            this.dsk_grp_tools.MinSize = new System.Drawing.Size(0, 0);
            this.dsk_grp_tools.Name = "dsk_grp_tools";
            this.dsk_grp_tools.Text = "gHerramientas";
            // 
            // dsk_gtools_backup
            // 
            this.dsk_gtools_backup.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_backup.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_backup.ExpandArrowButton = false;
            this.dsk_gtools_backup.Image = global::Jaeger.UI.Properties.Resources.cloud_storage_32px;
            this.dsk_gtools_backup.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_backup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_backcfdi,
            this.dsk_gtools_backdir,
            this.dsk_gtools_backret,
            this.dsk_gtools_backrec});
            this.dsk_gtools_backup.Name = "dsk_gtools_backup";
            this.dsk_gtools_backup.Text = "b.Backup";
            this.dsk_gtools_backup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_backcfdi
            // 
            this.dsk_gtools_backcfdi.Name = "dsk_gtools_backcfdi";
            this.dsk_gtools_backcfdi.Text = "b.BackCFDI";
            this.dsk_gtools_backcfdi.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_backdir
            // 
            this.dsk_gtools_backdir.Name = "dsk_gtools_backdir";
            this.dsk_gtools_backdir.Text = "b.BackupContribuyentes";
            this.dsk_gtools_backdir.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_backret
            // 
            this.dsk_gtools_backret.Name = "dsk_gtools_backret";
            this.dsk_gtools_backret.Text = "b.BackRet";
            this.dsk_gtools_backret.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_backrec
            // 
            this.dsk_gtools_backrec.Name = "dsk_gtools_backrec";
            this.dsk_gtools_backrec.Text = "b.BackRecep";
            this.dsk_gtools_backrec.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validador
            // 
            this.dsk_gtools_validador.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_validador.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_validador.ExpandArrowButton = false;
            this.dsk_gtools_validador.Image = global::Jaeger.UI.Properties.Resources.protect_32px;
            this.dsk_gtools_validador.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_validador.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_validar,
            this.dsk_gtools_validarfc,
            this.dsk_gtools_validacedula,
            this.dsk_gtools_validaret,
            this.dsk_gtools_cancelado,
            this.dsk_gtools_presuntos,
            this.dsk_gtools_certificado});
            this.dsk_gtools_validador.Name = "dsk_gtools_validador";
            this.dsk_gtools_validador.Text = "b.Validador";
            this.dsk_gtools_validador.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_validar
            // 
            this.dsk_gtools_validar.Name = "dsk_gtools_validar";
            this.dsk_gtools_validar.Text = "b.Comprobante";
            this.dsk_gtools_validar.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validarfc
            // 
            this.dsk_gtools_validarfc.Name = "dsk_gtools_validarfc";
            this.dsk_gtools_validarfc.Text = "Valida RFC";
            this.dsk_gtools_validarfc.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validacedula
            // 
            this.dsk_gtools_validacedula.Image = global::Jaeger.UI.Properties.Resources.smart_card_16;
            this.dsk_gtools_validacedula.Name = "dsk_gtools_validacedula";
            this.dsk_gtools_validacedula.Text = "b.Cedula";
            this.dsk_gtools_validacedula.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validaret
            // 
            this.dsk_gtools_validaret.Name = "dsk_gtools_validaret";
            this.dsk_gtools_validaret.Text = "Valida Retención";
            this.dsk_gtools_validaret.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_cancelado
            // 
            this.dsk_gtools_cancelado.Name = "dsk_gtools_cancelado";
            this.dsk_gtools_cancelado.Text = "b.Cancelados";
            // 
            // dsk_gtools_presuntos
            // 
            this.dsk_gtools_presuntos.Name = "dsk_gtools_presuntos";
            this.dsk_gtools_presuntos.Text = "b.Presuntos";
            // 
            // dsk_gtools_certificado
            // 
            this.dsk_gtools_certificado.Name = "dsk_gtools_certificado";
            this.dsk_gtools_certificado.Text = "b.Certificado";
            this.dsk_gtools_certificado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_actualiza
            // 
            this.dsk_gtools_actualiza.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_actualiza.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_actualiza.ExpandArrowButton = false;
            this.dsk_gtools_actualiza.Image = global::Jaeger.UI.Properties.Resources.available_updates_30px;
            this.dsk_gtools_actualiza.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_actualiza.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_catsat});
            this.dsk_gtools_actualiza.Name = "dsk_gtools_actualiza";
            this.dsk_gtools_actualiza.Text = "b.Actualizar";
            this.dsk_gtools_actualiza.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_catsat
            // 
            this.dsk_gtools_catsat.Name = "dsk_gtools_catsat";
            this.dsk_gtools_catsat.Text = "b.Catálogos";
            this.dsk_gtools_catsat.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_grp_theme
            // 
            this.dsk_grp_theme.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtheme_2010Black,
            this.dsk_gtheme_2010Blue,
            this.dsk_gtheme_2010Silver});
            this.dsk_grp_theme.Name = "dsk_grp_theme";
            this.dsk_grp_theme.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dsk_grp_theme.Text = "gTheme";
            // 
            // dsk_gtheme_2010Black
            // 
            this.dsk_gtheme_2010Black.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Black.Image")));
            this.dsk_gtheme_2010Black.Name = "dsk_gtheme_2010Black";
            this.dsk_gtheme_2010Black.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Black.Tag = "Office2010Black";
            this.dsk_gtheme_2010Black.Text = "Office 2010 Black";
            this.dsk_gtheme_2010Black.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Black.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Black.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Blue
            // 
            this.dsk_gtheme_2010Blue.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Blue.Image")));
            this.dsk_gtheme_2010Blue.Name = "dsk_gtheme_2010Blue";
            this.dsk_gtheme_2010Blue.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Blue.Tag = "Office2010Blue";
            this.dsk_gtheme_2010Blue.Text = "Office 2010 Blue";
            this.dsk_gtheme_2010Blue.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Blue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Blue.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Silver
            // 
            this.dsk_gtheme_2010Silver.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Silver.Image")));
            this.dsk_gtheme_2010Silver.Name = "dsk_gtheme_2010Silver";
            this.dsk_gtheme_2010Silver.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Silver.Tag = "Office2010Silver";
            this.dsk_gtheme_2010Silver.Text = "Office 2010 Silver";
            this.dsk_gtheme_2010Silver.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Silver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Silver.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_grp_about
            // 
            this.dsk_grp_about.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_btn_manual,
            this.dsk_btn_about});
            this.dsk_grp_about.Name = "dsk_grp_about";
            this.dsk_grp_about.Text = "Acerca de";
            // 
            // dsk_btn_manual
            // 
            this.dsk_btn_manual.Image = global::Jaeger.UI.Properties.Resources.user_manual_30px;
            this.dsk_btn_manual.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_btn_manual.Name = "dsk_btn_manual";
            this.dsk_btn_manual.Text = "b.Manual";
            this.dsk_btn_manual.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_btn_about
            // 
            this.dsk_btn_about.Image = global::Jaeger.UI.Properties.Resources.about_30px;
            this.dsk_btn_about.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_btn_about.Name = "dsk_btn_about";
            this.dsk_btn_about.Text = "Edita CP";
            this.dsk_btn_about.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dsk_btn_about.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // BEstado
            // 
            this.BEstado.Location = new System.Drawing.Point(0, 643);
            this.BEstado.Name = "BEstado";
            this.BEstado.Size = new System.Drawing.Size(1590, 26);
            this.BEstado.SizingGrip = false;
            this.BEstado.TabIndex = 1;
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1";
            // 
            // RadDock
            // 
            this.RadDock.AutoDetectMdiChildren = true;
            this.RadDock.Controls.Add(this.RadContainer);
            this.RadDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadDock.IsCleanUpTarget = true;
            this.RadDock.Location = new System.Drawing.Point(0, 162);
            this.RadDock.MainDocumentContainer = this.RadContainer;
            this.RadDock.Name = "RadDock";
            // 
            // 
            // 
            this.RadDock.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadDock.Size = new System.Drawing.Size(1590, 481);
            this.RadDock.TabIndex = 5;
            this.RadDock.TabStop = false;
            // 
            // RadContainer
            // 
            this.RadContainer.Name = "RadContainer";
            // 
            // 
            // 
            this.RadContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadContainer.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // dsk_grp_repositorio
            // 
            this.dsk_grp_repositorio.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gre_comprobante,
            this.dsk_gre_solicitud,
            this.dsk_gre_directorio});
            this.dsk_grp_repositorio.Name = "dsk_grp_repositorio";
            this.dsk_grp_repositorio.Text = "g.Repositorio";
            // 
            // dsk_gre_comprobante
            // 
            this.dsk_gre_comprobante.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gre_comprobante.Image")));
            this.dsk_gre_comprobante.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gre_comprobante.Name = "dsk_gre_comprobante";
            this.dsk_gre_comprobante.Text = "b.Comprobantes";
            this.dsk_gre_comprobante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gre_solicitud
            // 
            this.dsk_gre_solicitud.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gre_solicitud.Image")));
            this.dsk_gre_solicitud.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gre_solicitud.Name = "dsk_gre_solicitud";
            this.dsk_gre_solicitud.Text = "b.Solicitud";
            this.dsk_gre_solicitud.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gre_directorio
            // 
            this.dsk_gre_directorio.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gre_directorio.Image")));
            this.dsk_gre_directorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gre_directorio.Name = "dsk_gre_directorio";
            this.dsk_gre_directorio.Text = "b.Directorio";
            this.dsk_gre_directorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // MainRibbonForm
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1590, 669);
            this.Controls.Add(this.RadDock);
            this.Controls.Add(this.BEstado);
            this.Controls.Add(this.MenuRibbonBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainRibbonForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainRibbonForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainRibbonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).EndInit();
            this.RadDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar MenuRibbonBar;
        private Telerik.WinControls.UI.RadStatusStrip BEstado;
        private Telerik.WinControls.UI.RibbonTab TAdmon;
        private Telerik.WinControls.UI.RibbonTab TProd;
        private Telerik.WinControls.UI.RibbonTab TVentas;
        private Telerik.WinControls.UI.RibbonTab TTools;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_cliente;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_expediente;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gcli_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_ordcliente;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_facturacion;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.Docking.RadDock RadDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer RadContainer;
        private Telerik.WinControls.Themes.Windows8Theme windows8Theme1;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_cobranza;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gcli_reportes;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_bedocuenta;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_bcredito;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_bresumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_tesoreria;
        private Telerik.WinControls.UI.RadButtonElement adm_gtes_bancoctas;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gtes_banco;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bbeneficiario;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bconceptos;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bformapago;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bmovimiento;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_contable;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_almacen;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_proveedor;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gcontable_catcuentas;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_catrubros;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_ctacontable;
        private Telerik.WinControls.UI.RadButtonElement adm_gcontable_polizas;
        private Telerik.WinControls.UI.RadButtonElement adm_gcontable_activos;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gcontable_configurar;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_cattipo;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_creartablas;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_galmacen_mprima;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_catalogo;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_vales;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_devolucion;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_existencia;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_galmacen_pterminado;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_catalogo;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_entrada;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_salida;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_existencia;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_expediente;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gprv_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_ordcompra;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_validador;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_facturacion;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_pagos;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gprv_reportes;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_edocuenta;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_resumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup cppro_grp_produccion;
        private Telerik.WinControls.UI.RadRibbonBarGroup cmr_grp_cp;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_directorio;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cmr_gcp_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_ordproduccion;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_prodvendido;
        private Telerik.WinControls.UI.RadDropDownButtonElement cmr_gcp_remisionado;
        private Telerik.WinControls.UI.RadMenuItem cmr_gcp_bremisionado;
        private Telerik.WinControls.UI.RadMenuItem cmr_gcp_rempartida;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_resumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_config;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gconfig_param;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_avanzado;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_tools;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_theme;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Black;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Blue;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Silver;
        private Telerik.WinControls.UI.RadRibbonBarGroup cmr_grp_ventas;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_vededores;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cmr_gventas_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_pedidos;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_comision;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_usuario;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_ordcompra;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_credito;
        private Telerik.WinControls.UI.RadDropDownButtonElement cmr_gcp_reportes;
        private Telerik.WinControls.UI.RadMenuItem cmr_gcp_reporte1;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_menu;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_perfil;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_backup;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_validador;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_cancelado;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_presuntos;
        private Telerik.WinControls.UI.RadButtonElement dsk_gconfig_series;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_ccomision;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_remisionado;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_nomina;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validar;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_retencion;
        private Telerik.WinControls.UI.RadButtonElement tdks_gretencion_recep;
        private Telerik.WinControls.UI.RadButtonElement tdks_gretencion_comprobante;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_certificado;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validarfc;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_reqcompra;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_orden;
        private Telerik.WinControls.UI.RadMenuItem cppro_bord_historial;
        private Telerik.WinControls.UI.RadMenuItem cppro_bord_proceso;
        private Telerik.WinControls.UI.RadButtonElement cppro_gpro_reqcompra;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_planea;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cppro_gpro_sgrupo;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_sgrp_depto;
        private Telerik.WinControls.UI.RadButtonElement cppro_sgrp_avance;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_remision;
        private Telerik.WinControls.UI.RadMenuItem cppro_brem_Emitido;
        private Telerik.WinControls.UI.RadMenuItem cppro_brem_Recibido;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_sgrp_evaluacion;
        private Telerik.WinControls.UI.RadMenuItem cppro_beva_eproduccion;
        private Telerik.WinControls.UI.RadMenuItem cppro_beva_eperiodo;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_modelo;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpvnt_grp_ventas;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_vendedor;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_prodvend;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cpvnt_gven_group1;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_pedidos;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_comision;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_ventcosto;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpvnt_gven_remisionado;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_historial;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_porcobrar;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_rempart;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpemp_grp_directorio;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gdir_directorio;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpcot_grp_cotizacion;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpcot_gcot_productos;
        private Telerik.WinControls.UI.RadMenuItem cpcot_bprd_producto;
        private Telerik.WinControls.UI.RadMenuItem cpcot_bprd_subproducto;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_presupuesto;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_cotizaciones;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_cliente;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpcot_gcot_conf;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validaret;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cpemp_grp_producto;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gprd_clasificacion;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gprd_catalogo;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_backcfdi;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_backdir;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_cunidad;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validacedula;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_about;
        private Telerik.WinControls.UI.RadButtonElement dsk_btn_about;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_emisor;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_cunidad;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_calendario;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_gantt;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_plan;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_tieadd;
        private Telerik.WinControls.UI.RadMenuItem cppro_beva_eprodof;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_categoria;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_cpago2;
        private Telerik.WinControls.UI.RadRibbonBarGroup ccal_grp_nconf;
        private Telerik.WinControls.UI.RadDropDownButtonElement ccal_gnco_new;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_todo;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_proceso;
        private Telerik.WinControls.UI.RadDropDownButtonElement ccal_gnco_reporte;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_reporte1;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_reporte2;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_reporte3;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpvnt_gven_reportes;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_gven_reporte1;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_gven_reporte2;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_cpago2;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_actualiza;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_catsat;
        private Telerik.WinControls.UI.RadMenuItem cppro_bdepto_trabaj;
        private Telerik.WinControls.UI.RadMenuItem cppro_bdepto_proces;
        private Telerik.WinControls.UI.RadButtonElement dsk_gconfig_cate;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gconfig_cert;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_backret;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_backrec;
        private Telerik.WinControls.UI.RadButtonElement dsk_btn_manual;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gnom_empleado;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_empexp;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_empcont;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gnom_gmov;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_periodo;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gnom_sgmov;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_movvac;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_movfal;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_movacu;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_aguina;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gnom_recibo;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_reccon;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_recres;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gnom_parame;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_conf;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_gconcep;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gnom_tablas;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tisr;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tsbe;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tsm;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tuma;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_certc;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_certf;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_repositorio;
        private Telerik.WinControls.UI.RadButtonElement dsk_gre_comprobante;
        private Telerik.WinControls.UI.RadButtonElement dsk_gre_solicitud;
        private Telerik.WinControls.UI.RadButtonElement dsk_gre_directorio;
    }
}
