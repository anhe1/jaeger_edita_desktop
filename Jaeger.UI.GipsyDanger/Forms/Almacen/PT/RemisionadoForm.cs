﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.PT {
    public class RemisionadoForm : UI.CP.Forms.Almacen.PT.RemisionadoForm {
        protected internal RadMenuItem PorPartida = new RadMenuItem { Text = "Por Partidas", Name = "tadm_gcli_remxpartida" };

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = false;
            this.Text = "Almacén PT: Remisionado";
            this.Load += RemisionadoForm_Load;
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.CP.Service.RemisionadoService();
        }

        public override void TRemision_PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new CP.RemisionadoPartidaForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm, Text = this.PorPartida.Text };
            porPartida.Show();
        }

        public override void TRemision_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new UI.CP.Forms.Almacen.PT.RemisionFiscalForm(this._Service) { MdiParent = this.ParentForm, Text = "Remision: Nueva" };
            nuevo.Show();
        }
    }
}
