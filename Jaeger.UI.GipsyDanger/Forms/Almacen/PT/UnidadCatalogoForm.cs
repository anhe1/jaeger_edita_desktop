﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.PT {
    public class UnidadCatalogoForm : UI.Almacen.Forms.UnidadCatalogoForm {

        public UnidadCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Service = new Aplication.Almacen.PT.Services.UnidadService();
        }
    }
}
