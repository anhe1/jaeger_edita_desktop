﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class ProductosCatalogoForm : UI.Almacen.MP.Forms.ProductoCatalogoForm {
        
        public ProductosCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.menuElement = menuElement;
            this.Action = new UIAction(this.menuElement.Permisos);
            this.Load += this.ProductosCatalogoForm_Load;
        }

        private void ProductosCatalogoForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.Almacen.MP.Services.MateriaPrimaService();
        }
    }
}
