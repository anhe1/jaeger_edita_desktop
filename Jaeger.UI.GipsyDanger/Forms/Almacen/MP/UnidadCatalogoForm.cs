﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class UnidadCatalogoForm : UI.Almacen.Forms.UnidadCatalogoForm {

        public UnidadCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Service = new Aplication.Almacen.Services.UnidadService(AlmacenEnum.MP);
        }
    }
}
