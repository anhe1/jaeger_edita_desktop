﻿using System;
using Telerik.WinControls;
using System.Windows.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Almacen.MP.Forms;

namespace Jaeger.UI.Forms.Almacen.MP {
    /// <summary>
    /// Catalogo de productos y servicios, vista Productos-Modelos
    /// </summary>
    internal class ProductoModeloCatalogoForm : UI.Almacen.Forms.ProductoModeloCatalogoForm {
        public ProductoModeloCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += ProductoModeloCatalogoForm_Load;
        }

        private void ProductoModeloCatalogoForm_Load(object sender, EventArgs e) {
            this._CatalogoService = new Aplication.Almacen.MP.Services.MateriaPrimaService();
        }

        protected override void Editar_Click(object sender, EventArgs e) {
            if (this.TModelos.GridData.CurrentRow != null) {
                var seleccionado = this.TModelos.GridData.CurrentRow.DataBoundItem as ProductoServicioModeloModel;
                if (seleccionado == null) return;
                if (seleccionado.Autorizado == 0) {
                    var producto = new ProductoModeloForm((Aplication.Almacen.MP.Contracts.IMateriaPrimaService)this._CatalogoService, seleccionado);
                    producto.ShowDialog(this);
                } else {
                    RadMessageBox.Show(this, "Properties.Resources.Message_Error_ProductoAutorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
    }
}
