﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Almacen.Builder;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Almacen.MP.Services;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.MP {
    internal class ClasificacionCategoriaForm : UI.Almacen.Forms.CategoriasTreeViewForm {

        public ClasificacionCategoriaForm(UIMenuElement menuElement) : base() {
            this.Load += ClasificacionCategoriaForm_Load;
        }

        private void ClasificacionCategoriaForm_Load(object sender, EventArgs e) {
            this.Text = "Almacen MP: Categorías";
            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.TControl.GridData.Columns.AddRange(view.Templetes().ProductoModeloAlmacen().Build());
            }
            this._ClasificacionService = new ClasificacionService();
            this._CatalogoService = new MateriaPrimaService();
        }

        protected override void TControl_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            
            var categoriaColumn = this.TControl.GridData.Columns["IdCategoria"] as GridViewMultiComboBoxColumn;
            categoriaColumn.DataSource = this._Clasificacion2;
            categoriaColumn.DisplayMember = "Descripcion";
            categoriaColumn.ValueMember = "IdCategoria";

            var IdUnidadAlmacen = this.TControl.GridData.Columns["IdUnidad"] as GridViewComboBoxColumn;
            IdUnidadAlmacen.DataSource = this._UnidadesAlmacen;
            IdUnidadAlmacen.DisplayMember = "Descripcion";
            IdUnidadAlmacen.ValueMember = "IdUnidad";

            var unidadXYColumn = this.TControl.GridData.Columns["UnidadXY"] as GridViewComboBoxColumn;
            unidadXYColumn.DataSource = this._UnidadesXY;
            unidadXYColumn.DisplayMember = "Descripcion";
            unidadXYColumn.ValueMember = "IdUnidad";

            var unidadZColumn = this.TControl.GridData.Columns["UnidadZ"] as GridViewComboBoxColumn;
            unidadZColumn.DataSource = this._UnidadesZ;
            unidadZColumn.DisplayMember = "Descripcion";
            unidadZColumn.ValueMember = "IdUnidad";

            this.TControl.ValueMember = "IdCategoria";
            this.TControl.IndexChanged += TControl_IndexChanged;
            this.TControl.TreeData.DataSource = this._Clasificacion;
            this.TControl.TreeData.DisplayMember = "Clase";
            this.TControl.TreeData.ValueMember = "IdCategoria";
            this.TControl.TreeData.ParentMember = "IdSubCategoria";
            this.TControl.TreeData.ChildMember = "IdCategoria";
            this.TControl.TreeData.SelectedNode = this.TControl.TreeData.Nodes[0];
            this.TControl.GridData.DataSource = this._DataSource;
        }

        private void TControl_IndexChanged(object sender, object e) {
         
        }
    }
}
