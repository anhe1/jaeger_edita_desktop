﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class ValeAlmacenCatalogoForm : UI.Almacen.MP.Forms.ValesAlmacenCatalogoForm {
        public ValeAlmacenCatalogoForm() : base() {

        }

        public ValeAlmacenCatalogoForm(UIMenuElement element) : base(element) {
            this.Load += ValeAlmacenCatalogoForm_Load;
        }

        private void ValeAlmacenCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new Aplication.Almacen.MP.Services.ValeAlmacenService();
            this.Almacen.DataSource = this.Service.GetAlmacenes();
        }

        public override void TVale_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new ValeAlmacenForm(this.Service) { MdiParent = this.ParentForm };
            _nuevo.Show();
        }
    }
}
