﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class ProductoExistenciaForm : UI.Almacen.MP.Forms.ProductoExistenciaForm {
        public ProductoExistenciaForm(UIMenuElement menuElement) : base() {
            this.Text = "Almacén MP: Existencias";
            this.Load += ProductoExistenciaForm_Load;
        }

        private void ProductoExistenciaForm_Load(object sender, EventArgs e) {
            this._CatalogoService = new Aplication.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.MP);
            this.Almacen.DataSource = this._CatalogoService.GetAlmacenes();
        }
    }
}
