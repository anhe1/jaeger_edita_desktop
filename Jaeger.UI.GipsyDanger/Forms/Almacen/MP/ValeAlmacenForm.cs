﻿using Jaeger.Aplication.Almacen.MP.Contracts;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class ValeAlmacenForm : UI.Almacen.MP.Forms.ValeAlmacenForm {
        public ValeAlmacenForm() : base() {

        }

        public ValeAlmacenForm(IValeAlmacenService service) : base(service) {
            this.Unidad = new Aplication.Almacen.Services.UnidadService(Domain.Base.ValueObjects.AlmacenEnum.MP);
            this.TVale.Receptor.Service = new Aplication.Contribuyentes.Services.DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor);
            this.Load += ValeAlmacenForm_Load;
        }

        private void ValeAlmacenForm_Load(object sender, System.EventArgs e) {
            this.TVale.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor;
        }
    }
}
