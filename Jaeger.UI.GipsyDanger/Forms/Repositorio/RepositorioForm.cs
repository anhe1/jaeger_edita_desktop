﻿using System;

namespace Jaeger.UI.Forms.Repositorio {
    public class RepositorioForm : UI.Repositorio.Forms.ComprobantesRepositorioForm {

        public RepositorioForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += this.RepositorioForm_Load;
        }

        private void RepositorioForm_Load(object sender, EventArgs e) {
            this.Text = "| Repositorio";
        }
    }
}
