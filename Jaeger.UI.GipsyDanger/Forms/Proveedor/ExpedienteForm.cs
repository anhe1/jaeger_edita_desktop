﻿using Jaeger.Aplication.Contribuyentes.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Proveedor {
    public class ExpedienteForm : UI.Contribuyentes.Forms.ProveedoresCatalogoForm {
        public ExpedienteForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ExpedienteForm_Load;
        }

        private void ExpedienteForm_Load(object sender, System.EventArgs e) {
            this.service = new DirectorioService(this.relacionComericalEnum);
        }
    }
}
