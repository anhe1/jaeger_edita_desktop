﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.Proveedor {
    public class ComprobantesFiscalesPartidaForm : Comprobante.Forms.ComprobantesFiscalesPartidaForm {
        protected internal RadMenuItem TProductoModelo = new RadMenuItem { Text = "Agregar al catálogo", Name = "adm_galmmp_catalogo", ToolTipText = "Agregar modelo al catálogo de productos" };
        public ComprobantesFiscalesPartidaForm(IComprobantesFiscalesService service, UIMenuElement menuElement) : base(menuElement) {
            this._Service = service;
            this.Load += ComprobantesFiscalesPartidaForm_Load;
        }

        private void ComprobantesFiscalesPartidaForm_Load(object sender, EventArgs e) {
            this.Text = "Proveedores: Fact. Partidas";

            var Action = new UIAction(ConfigService.GeMenuElement(this.TProductoModelo.Name).Permisos);

            this.TProductoModelo.Enabled = Action.Agregar;
            this.TComprobante.menuContextual.Items.Add(this.TProductoModelo);
            this.TProductoModelo.Click += TProductoModelo_Click;
        }

        public override void Actualizar_Click(object sender, EventArgs e) {
            base.Actualizar_Click(sender, e);
        }

        public virtual void TProductoModelo_Click(object sender, EventArgs e) {
            var p0 = this.TComprobante.GetSeleccion<ComprobanteFiscalConceptoView>();
            var m0 = new UI.Almacen.MP.Forms.ProductoModeloForm(new Aplication.Almacen.MP.Services.MateriaPrimaService(), 
                new Domain.Almacen.Entities.ModeloDetailModel() { 
                    IdAlmacen = 1,
                    Descripcion = p0[0].Descripcion, 
                    Tipo = ProdServTipoEnum.Producto, 
                    Unitario = p0[0].ValorUnitario 
                });
            m0.ShowDialog(this);
        }

        public override void Consulta() {
            var q0 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Recibido().WithYear(this.TComprobante.GetEjercicio()).WithMonth(this.TComprobante.GetPeriodo()).Build();
            this._DataSource = new BindingList<ComprobanteFiscalConceptoView>(this._Service.GetList<ComprobanteFiscalConceptoView>(q0).ToList<ComprobanteFiscalConceptoView>());
        }
    }
}
