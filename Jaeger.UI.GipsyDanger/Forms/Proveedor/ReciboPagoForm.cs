﻿using System;
using System.Linq;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.Forms.Proveedor {
    public class ReciboPagoForm : UI.Banco.Forms.MovimientoBancarioForm {

        public ReciboPagoForm(IBancoService service) : base(service) {
            this.Text = "Registro de Pago a Proveedor";
            this.Load += this.ReciboPagoForm_Load;
        }

        public ReciboPagoForm(IMovimientoBancarioDetailModel movimiento, IBancoService service) : base(movimiento, service) {
            this.Text = "Registro de Pago a Proveedor";
            this.Load += this.ReciboPagoForm_Load;
        }

        private void ReciboPagoForm_Load(object sender, EventArgs e) {

        }

        protected override void GetTiposMovimiento() {
            this._TiposMovimiento = new System.ComponentModel.BindingList<MovimientoConceptoDetailModel>(
                this.TMovimiento.Service.GetList<MovimientoConceptoDetailModel>(Aplication.Banco.BancoConceptoService.Query().Efecto(MovimientoBancarioEfectoEnum.Egreso).Build()
                ).ToList());
        }
    }
}
