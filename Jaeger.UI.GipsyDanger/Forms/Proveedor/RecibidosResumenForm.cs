﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Proveedor {
    public class RecibidosResumenForm : Comprobante.Forms.ComprobantesFiscalesResumenForm {
        public RecibidosResumenForm(UIMenuElement menuElement) : base(CFDISubTipoEnum.Recibido, menuElement) {
            this.Text = "Proveedores: Facuración vs Pagos";
        }
    }
}
