﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Forms.Proveedor {
    public class RecibidosPorPagarForm : RecibidosForm {
        public RecibidosPorPagarForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Proveedores facturación: Por Pagar";
            this.Load += RecibidosPorPagarForm_Load;
        }

        private void RecibidosPorPagarForm_Load(object sender, System.EventArgs e) {
            this.TComprobante.ShowPeriodo = false;
        }

        public override void Consultar() {
            // utilizando builder para consulta
            var d0 = ComprobantesFiscalesService.Query().Recibido().WithStatus(CFDStatusEgresoEnum.PorPagar).WithYear(this.TComprobante.GetEjercicio()).Build();
            this._DataSource = new BindingList<IComprobanteFiscalDetailSingleModel>(
                    this._Service.GetList<ComprobanteFiscalDetailSingleModel>(d0).ToList<IComprobanteFiscalDetailSingleModel>()
                );
        }
    }
}
