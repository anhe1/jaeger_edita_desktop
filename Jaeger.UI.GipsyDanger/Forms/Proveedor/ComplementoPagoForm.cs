﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.UI.Forms.Proveedor {
    internal class ComplementoPagoForm : Comprobante.Forms.ComprobanteVsComplementoPagoForm {
        public ComplementoPagoForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Load += ComplementoPagoForm_Load;
        }

        private void ComplementoPagoForm_Load(object sender, EventArgs e) {
            this.Text = "Proveedores: Complemento de Pago";
            this.TComprobantes.GridData.AutoGenerateColumns = true;
            this._Service = new ComprobantesFiscalesService();
            var permisos = new UIAction(this.menuElement.Permisos);
            this.TComprobantes.Permisos = permisos;
        }

        public override void Consultar() {
            var query = ComprobantesFiscalesService.Query().Recibido().WithYear(this.TComprobantes.GetEjercicio()).WithMonth(this.TComprobantes.GetPeriodo()).Build();
            this._DataSource = new BindingList<ComplementoVsPagoMergeDoctoModel>(this._Service.GetList<ComplementoVsPagoMergeDoctoModel>(query).ToList());
        }
    }
}