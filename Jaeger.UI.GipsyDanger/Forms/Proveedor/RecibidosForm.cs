﻿using System;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Comprobante.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Banco.Builder;

namespace Jaeger.UI.Forms.Proveedor {
    public class RecibidosForm : ComprobantesFiscalesRecibidoForm {
        protected internal GridViewTemplate _GridPagos = new GridViewTemplate();
        protected internal RadMenuItem _MenuContextual_CrearPago = new RadMenuItem { Text = "Registrar Pago", ToolTipText = "Crear movimiento Bancario" };
        protected internal RadMenuItem _PorPagar = new RadMenuItem { Text = "Por pagar", ToolTipText = "Comprobantes por pagar" };
        protected internal RadMenuItem PorPartidas = new RadMenuItem { Text = "Por Partidas", ToolTipText = "Comprobantes por Partidas", Name = "PorPartidas" };
        //protected internal RadMenuItem ComplementoPago = new RadMenuItem { Text = "Complemento de Pago", ToolTipText = "", Name = "ComplementoPago" };
        protected Aplication.Banco.IBancoMovientosSearchService _BancoService;
        protected internal UIMenuElement _MenuElement;

        public RecibidosForm(UIMenuElement menuElement) : base() {
            this._MenuElement = menuElement;
            this.Load += this.RecibidosCatalogoForm_Load;
        }

        private void RecibidosCatalogoForm_Load(object sender, EventArgs e) {
            var permisos = new UIAction(this._MenuElement.Permisos);
            this.TComprobante.Editar.Text = "Ver";
            this.TComprobante.Cancelar.Text = "Rechazado";
            this.TComprobante.ShowHerramientas = true;
            this.TComprobante.Herramientas.Items.Add(this._PorPagar);
            this.TComprobante.Herramientas.Items.Add(this.PorPartidas);
            //this.TComprobante.Herramientas.Items.Add(this.ComplementoPago);
            this.TComprobante.ShowExportarExcel = permisos.Exportar;

            this.menuContextual.Items.Add(this._MenuContextual_CrearPago);
            this._MenuContextual_CrearPago.Click += this.MenuContextual_CrearPago_Click;
            this._BancoService = new Aplication.Banco.BancoMovientosSearchService();
            this.TempletePagos();
            this._GridPagos.HierarchyDataProvider = new GridViewEventDataProvider(_GridPagos);
            this.GridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
            this._PorPagar.Click += TComprobante_PorPagar_Click;
            this.PorPartidas.Click += TComprobante_PorPartidas_Click;
            //this.ComplementoPago.Click += TComprobante_ComplementoPago_Click;
        }

        #region barra de herramientsa
        public override void TComprobante_Cancelar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, "¿Esta seguro de rechazar el comprobante seleccionado?",
                "Atención", System.Windows.Forms.MessageBoxButtons.YesNo, RadMessageIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes) {

            }
        }

        public override void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                var _visualizar = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                _visualizar.Show();
            }
        }

        private void TComprobante_PorPagar_Click(object sender, EventArgs e) {
            var d0 = new RecibidosPorPagarForm(this._MenuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }

        private void TComprobante_PorPartidas_Click(object sender, EventArgs e) {
            var d0 = new ComprobantesFiscalesPartidaForm(this._Service, this._MenuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }

        //private void TComprobante_ComplementoPago_Click(object sender, EventArgs e) {
        //    var d0 = new ComplementoPagoForm(this._Service, this._MenuElement) { MdiParent = this.ParentForm };
        //    d0.Show();
        //}
        #endregion

        #region opciones del menu contextual
        public virtual void MenuContextual_CrearPago_Click(object sender, EventArgs e) {
            var seleccion = this.GetSelection();
            if (seleccion == null) {
                return;
            }
            // revisamos los status
            if (seleccion.Count > 0) {
                var _si = seleccion.Where(it => it.Status == "Cancelado" | it.Status == "Importado" | it.Status == "Pagado" | it.Estado == "Cancelado");
                if (_si.Count() > 0) {
                    RadMessageBox.Show(this, "No es posible crear el registro de pago. Es posible que uno o más comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }
            }
            var d0 = seleccion.Select(it => it.IdDocumento).ToList();
            var d1 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().WithIdDocumento(d0.ToArray()).Build();
            var seleccion1 = this._Service.GetList<ComprobanteFiscalDetailModel>(d1).ToList();
            foreach (var item in seleccion1) {
                if (item.RecepcionPago != null) {
                    foreach (var item1 in item.RecepcionPago) {
                        if (item1.DoctoRelacionados != null) {
                            foreach (var item2 in item1.DoctoRelacionados) {
                                d0.Add(item2.IdDocumentoDR);
                            }
                        }
                    }
                }
            }
            d1 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().WithIdDocumento(d0.ToArray()).Build();
            var documentos = this._Service.GetList<ComprobanteFiscalDetailSingleModel>(d1).ToList();
            var _movimiento = Aplication.Banco.BancoService.Create().New(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso).Status();
            foreach (var item in documentos) {
                if (item.Estado != "Cancelado") {
                    _movimiento.Add(new MovimientoBancarioComprobanteDetailModel {
                        Activo = true,
                        ClaveFormaPago = item.ClaveFormaPago,
                        ClaveMetodoPago = item.ClaveMetodoPago,
                        ClaveMoneda = item.ClaveMoneda,
                        EmisorNombre = item.EmisorNombre,
                        EmisorRFC = item.EmisorRFC,
                        Estado = item.Estado,
                        FechaEmision = item.FechaEmision,
                        FechaNuevo = item.FechaNuevo,
                        IdComprobante = item.Id,
                        IdDocumento = item.IdDocumento,
                        NumParcialidad = item.NumParcialidad,
                        ReceptorNombre = item.ReceptorNombre,
                        ReceptorRFC = item.ReceptorRFC,
                        Serie = item.Serie,
                        Version = item.Version,
                        Total = item.Total,
                        SubTipoComprobante = (CFDISubTipoEnum)item.IdSubTipo,
                        TipoComprobanteText = item.TipoComprobanteText,
                        Folio = item.Folio,
                        Acumulado = item.Acumulado,
                        PorCobrar = item.Saldo
                    });
                    _movimiento.IdBeneficiario(item.IdDirectorio);
                }
            }

            var movimiento = _movimiento.Build();
            if (movimiento.Comprobantes.Count == 0) {
                RadMessageBox.Show(this, "No es posible crear el registro de pago. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
            } else {
                var _nuevoMovimiento = new ReciboPagoForm(movimiento, new Aplication.Banco.BancoService()) { MdiParent = this.ParentForm };
                _nuevoMovimiento.Show();
            }
        }

        public virtual void MenuContextual_CrearPago_Click1(object sender, EventArgs e) {
            var _seleccion = this.GetSelection();
            if (_seleccion != null) {
                if (_seleccion.Count > 0) {
                    var _si = _seleccion.Where(it => it.Status == "Cancelado" | it.Status == "Importado" | it.Status == "Pagado" | it.Estado == "Cancelado");
                    if (_si.Count() > 0) {
                        RadMessageBox.Show(this, "No es posible crear el registro de pago. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
            var d0 = _seleccion.Select(it => it.IdDocumento).ToList();
            var d1 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().WithIdDocumento(d0.ToArray()).Build();
            var seleccion = this._Service.GetList<ComprobanteFiscalDetailModel>(d1).ToList();

            foreach (var item in seleccion) {
                if (item.RecepcionPago != null) {
                    foreach (var item1 in item.RecepcionPago) {
                        if (item1.DoctoRelacionados != null) {
                            foreach (var item2 in item1.DoctoRelacionados) {
                                d0.Add(item2.IdDocumentoDR);
                            }
                        }
                    }
                }
            }
            d1 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().WithIdDocumento(d0.ToArray()).Build();
            seleccion = this._Service.GetList<ComprobanteFiscalDetailModel>(d1).ToList();

            var _movimiento = Aplication.Banco.BancoService.Create().New(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso).Status();
            foreach (var item in seleccion) {
                if (item.Estado != "Cancelado") {
                    _movimiento.Add(new MovimientoBancarioComprobanteDetailModel {
                        Activo = true,
                        ClaveFormaPago = item.ClaveFormaPago,
                        ClaveMetodoPago = item.ClaveMetodoPago,
                        ClaveMoneda = item.ClaveMoneda,
                        EmisorNombre = item.EmisorNombre,
                        EmisorRFC = item.EmisorRFC,
                        Estado = item.Estado,
                        FechaEmision = item.FechaEmision,
                        FechaNuevo = item.FechaNuevo,
                        IdComprobante = item.Id,
                        IdDocumento = item.IdDocumento,
                        NumParcialidad = item.NumParcialidad,
                        ReceptorNombre = item.ReceptorNombre,
                        ReceptorRFC = item.ReceptorRFC,
                        Serie = item.Serie,
                        Version = item.Version,
                        Total = item.Total,
                        SubTipoComprobante = (CFDISubTipoEnum)item.IdSubTipo,
                        TipoComprobanteText = item.TipoComprobanteText,
                        Folio = item.Folio,
                        Acumulado = item.Acumulado
                    });
                    _movimiento.IdBeneficiario(item.IdDirectorio);
                }
            }
            var movimiento = _movimiento.Build();
            if (movimiento.Comprobantes.Count == 0) {
                RadMessageBox.Show(this, "No es posible crear el registro de pago. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
            } else {
                var _nuevoMovimiento = new ReciboPagoForm(movimiento, new Aplication.Banco.BancoService()) { MdiParent = this.ParentForm };
                _nuevoMovimiento.Show();
            }
        }
        #endregion

        #region acciones del grid
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this._GridPagos.Caption) {
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    var tabla = this._BancoService.GetSearchMovimientos(rowView.Id, Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.CFDI);
                    foreach (var item in tabla) {
                        var newRow = e.Template.Rows.NewRow();
                        e.SourceCollection.Add(GridViewBancoBuilder.ConverTo(newRow, item));
                    }
                }
            }
        }
        private void TempletePagos() {
            var tableViewDefinition1 = new TableViewDefinition();
            _GridPagos.Caption = "Pagos";
            _GridPagos.Columns.AddRange(new GridViewBancoBuilder().Templetes().Pagos().Build());
            _GridPagos.ViewDefinition = tableViewDefinition1;
            this.GridData.MasterTemplate.Templates.Add(_GridPagos);
            this._GridPagos.Standard();
        }
        #endregion
    }
}
