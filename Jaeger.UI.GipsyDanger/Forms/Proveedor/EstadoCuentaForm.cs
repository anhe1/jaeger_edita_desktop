﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Proveedor {
    internal class EstadoCuentaForm : Comprobante.Forms.EstadoCuentaGeneralForm {
        public EstadoCuentaForm(UIMenuElement menuElement) : base() {
            this.Load += EstadoCuentaForm_Load;
        }

        private void EstadoCuentaForm_Load(object sender, EventArgs e) {
            this.Text = "Proveedores: Edo. de Cuenta";
            this.TComprobante.ShowPeriodo = false;
            if (this.TComprobante._Service == null)
                this.TComprobante._Service = new Aplication.Comprobante.Services.ComprobantesFiscalesService(Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido);
        }

        protected override void Actualizar() {
            this._DataSource = new BindingList<EstadoCuenta>(
                this.TComprobante._Service.GetList<EstadoCuenta>(
                   Aplication.Comprobante.Services.ComprobantesFiscalesService.Query()
                       .Recibido().WithStatus(Domain.Comprobante.ValueObjects.CFDStatusEgresoEnum.PorPagar)
                       .WithYear(this.TComprobante.GetEjercicio())
                       .Build()).ToList<EstadoCuenta>()
                );
        }
    }

    internal class EstadoCuentaForm1 : Comprobante.Forms.ComprobantesFiscalesContribuyenteForm {
        public EstadoCuentaForm1(UIMenuElement menuElement) : base(null, menuElement) {
            this.Load += EstadoCuentaForm_Load;
        }

        public EstadoCuentaForm1(IComprobantesFiscalesService service, UIMenuElement menuElement) : base(service, menuElement) {
        }

        private void EstadoCuentaForm_Load(object sender, EventArgs e) {
            this.Text = "Proveedores: Edo. de Cuenta";
            if (this.TComprobante._Service == null)
                this.TComprobante._Service = new Aplication.Comprobante.Services.ComprobantesFiscalesService(Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido);
            this._ClientesLoad.RunWorkerAsync();
            this.TComprobante.ItemLbl.Text = "Proveedor: ";
            this.TComprobante.ShowPeriodo = true;
            this.TComprobante.Periodo.Items.Clear();
            this.TComprobante.lblPeriodo.Text = "Status: ";
            this.TComprobante.Periodo.DataSource = Aplication.Comprobante.Services.ComprobanteFiscalService.GetStatus( Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido);
            this.TComprobante.Periodo.DisplayMember = "Descriptor";
            this.TComprobante.Periodo.ValueMember = "Id";
        }

        protected override void Actualizar() {
            if (this.Nombre.SelectedItem != null) {
                var seleccionado = ((GridViewDataRowInfo)this.Nombre.SelectedItem).DataBoundItem as ComprobanteContribuyenteModel;
                if (seleccionado != null) {
                    this._DataSource = new BindingList<IComprobanteFiscalDetailSingleModel>(
                        this.TComprobante._Service.GetList<ComprobanteFiscalDetailSingleModel>(
                           Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Recibido().WithYear(this.TComprobante.GetEjercicio()).Build()).ToList<IComprobanteFiscalDetailSingleModel>()
                        );
                }
            }
        }

        protected override void LoadClientes_DoWork(object sender, DoWorkEventArgs e) {
            var d0 = Aplication.Contribuyentes.Services.DirectorioService.Query().WithRelacionComercial(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente).RegistroActivo().Build();
            this.Nombre.DataSource = this.TComprobante._Service.GetList<ComprobanteContribuyenteModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("_drctr_rlcn", "Proveedor", Domain.Base.ValueObjects.ConditionalTypeEnum.In) }).ToList();
        }
    }
}
