﻿using System;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.UI.Forms.Proveedor {
    /// <summary>
    /// formulario de recibos de pagos a proveedores
    /// </summary>
    public class ReciboPagoCatalogoForm : Banco.Forms.MovimientosForm {
        #region declaraciones
        protected internal IComprobantesFiscalesService _Service1;
        protected internal RadMenuItem THelpeme = new RadMenuItem { Text = "Helpme!", ToolTipText = "Para ayudar a la piche Ruthilia. :D" };
        #endregion

        public ReciboPagoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso) {
            this.Text = "Proveedores: Pagos";
            this._Permisos = new UIAction(menuElement.Permisos);
            this.Load += ReciboPagoCatalogoForm_Load;
        }

        private void ReciboPagoCatalogoForm_Load(object sender, EventArgs e) {
            this.TMovimiento.Nuevo.Click += this.TReciboPago_Nuevo_Click;
            this.TMovimiento.Nuevo.Enabled = this._Permisos.Agregar;
            this.TMovimiento.Cancelar.Enabled = this._Permisos.Cancelar;
            this.TMovimiento.Editar.Enabled = this._Permisos.Editar;
            this.TMovimiento.ExportarExcel.Enabled = this._Permisos.Exportar;

            this.Movimientos.TEmportar.Enabled = this._Permisos.Exportar;
            //this.Movimientos.TAuxiliar.Enabled = this._Permisos.Importar;
            //this.Movimientos.TCAuxiliar.Enabled = this._Permisos.Importar;
            this.Movimientos.Cancelar = this._Permisos.Cancelar;
            this.Movimientos.Auditar = this._Permisos.Autorizar;
            this.Movimientos.GridData.AllowEditRow = this._Permisos.Status;
            
            this.Service = new Aplication.Banco.BancoService();
            this.THelpeme.Click += THelpeme_Click;
            this.Movimientos.MenuContextual.Items.Add(this.THelpeme);
        }

        private void THelpeme_Click(object sender, EventArgs e) {
            using (var result = new UI.Common.Forms.Waiting1Form(this.ProgressWorkerMethod)) {
                result.ShowDialog(this);
            }
        }

        private void TReciboPago_Nuevo_Click(object sender, EventArgs e) {
            var recibo = new ReciboPagoForm(this.Service) { MdiParent = this.ParentForm };
            recibo.Show();
        }
        private void ProgressWorkerMethod() {
            var salida = this.Service.Test(this.Movimientos.Data);
            var dalida = new Banco.Forms.ComplementoPagoForm();
            dalida.Resultado.Text = salida;
            dalida.ShowDialog(this);
        }
        private void ProgressWorkerMethod0(object sender, Common.Forms.WaitWindowEventArgs e) {
            e.Window.Message = "Buscando comprobantes ...";
            var documentos = this.Movimientos.Data.SelectMany(it => it.Comprobantes).Where(it => it.TipoComprobante == Domain.Banco.ValueObjects.TipoCFDIEnum.Ingreso).Select(it => it.IdDocumento).ToArray();
            var query = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().Vista().GetComplementoPagoD(documentos).Build();

            var response = this.Service.GetList<MovimientoBancarioComprobantePago>(query).ToList();

            if (response != null) {
                if (response.Count > 0) {
                    for (int i1 = 0; i1 < this.Movimientos.Data.Count; i1++) {
                        if (this.Movimientos.Data[i1].Status != Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Cancelado) {
                            for (int i = 0; i < this.Movimientos.Data[i1].Comprobantes.Count; i++) {
                                if (this.Movimientos.Data[i1].Comprobantes[i].TipoComprobante == Domain.Banco.ValueObjects.TipoCFDIEnum.Ingreso) {
                                    var existe = response.Where(it => it.IdDocumentoR == this.Movimientos.Data[i1].Comprobantes[i].IdDocumento).Where(it => it.ImpPago == this.Movimientos.Data[i1].Comprobantes[i].Abono).ToList();
                                    if (existe != null) {
                                        if (existe.Count() == 1) {
                                            if (existe.FirstOrDefault().ImpPago == this.Movimientos.Data[i1].Comprobantes[i].Abono) {
                                                if (this.Movimientos.Data[i1].Agregar(existe.FirstOrDefault()) == false) {
                                                    Console.WriteLine("Agregado   = " + this.Movimientos.Data[i1].Identificador + " | " + this.Movimientos.Data[i1].Comprobantes[i].GetOriginalString + "| Abono: " + this.Movimientos.Data[i1].Comprobantes[i].Abono);
                                                    Console.WriteLine("           = " + this.Movimientos.Data[i1].Identificador + " |          " + existe.FirstOrDefault().GetOriginalString + "| Imp. Pagado: " + existe.FirstOrDefault().ImpPago);
                                                } else {
                                                    Console.WriteLine("Duplicado  = " + this.Movimientos.Data[i1].Identificador + " |          " + existe.FirstOrDefault().GetOriginalString);
                                                }
                                            }
                                        } else if (existe.Count() == 0) {
                                            Console.WriteLine("No existe  = " + this.Movimientos.Data[i1].Identificador + " | " + this.Movimientos.Data[i1].Comprobantes[i].GetOriginalString);
                                        } else {
                                            Console.WriteLine("Mas de uno = " + this.Movimientos.Data[i1].Identificador + " | " + this.Movimientos.Data[i1].Comprobantes[i].GetOriginalString);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ProgressWorkerMethod1(object sender, Common.Forms.WaitWindowEventArgs e) {
            if (this._Service1 == null) { this._Service1 = new ComprobantesFiscalesService(); }
            e.Window.Message = "Aguanta, cargando servicio ...";
            var seleccion = this.Movimientos.Current();
            var documentos = seleccion.Comprobantes.Select(it => it.IdDocumento).ToArray();
            var query = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().Vista().GetComplementoPagoD(documentos).Build();
            var response = this._Service1.GetList<ComplementoPagoMergeDoctoModel>(query).ToList();
            e.Window.Message = "Consultado ...";
            if (response != null) {
                for (int i = 0; i < seleccion.Comprobantes.Count; i++) {
                    var existe = response.Where(it => it.IdDocumentoDR == seleccion.Comprobantes[i].IdDocumento).FirstOrDefault();
                    if (existe != null) {
                        if (existe.ImpPagado == seleccion.Comprobantes[i].Abono) {
                            var query0 = ComprobantesFiscalesService.Query().Recibido().WithIdDocumento(existe.IdDocumentoP).Build();
                            var comprobantePago = this.Service.GetList<MovimientoBancarioComprobanteDetailModel>(query0).FirstOrDefault();
                            seleccion.Agregar(comprobantePago);
                        }
                    }
                }
                e.Window.Message = "Almacenando ...";
                this.Service.Save(seleccion);
                this.Movimientos.GridData.CurrentRow.RowRefresh();
            }
        }
        /// <summary>
        /// obtener listado de complemento de pago relacionado a un comprobante fiscal
        /// </summary>
        private void ComplementoPagoD() {
            if (this._Service1 == null) { this._Service1 = new ComprobantesFiscalesService(); }
            var seleccion = this.Movimientos.Current();
            var documentos = seleccion.Comprobantes.Select(it => it.IdDocumento).ToArray();
            var query = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().Vista().GetComplementoPagoD(documentos).Build();
            var response = this._Service1.GetList<ComplementoPagoMergeDoctoModel>(query).ToList();

            if (response != null) {
                for (int i = 0; i < seleccion.Comprobantes.Count; i++) {
                    var existe = response.Where(it => it.IdDocumentoDR == seleccion.Comprobantes[i].IdDocumento).FirstOrDefault();
                    if (existe != null) {
                        if (existe.ImpPagado == seleccion.Comprobantes[i].Abono) {
                            var query0 = ComprobantesFiscalesService.Query().Recibido().WithIdDocumento(existe.IdDocumentoP).Build();
                            var comprobantePago = this.Service.GetList<MovimientoBancarioComprobanteDetailModel>(query0).FirstOrDefault();
                            seleccion.Agregar(comprobantePago);
                        }
                    }
                }
                this.Service.Save(seleccion);
                this.Movimientos.GridData.CurrentRow.RowRefresh();
            }
        }
    }
}
