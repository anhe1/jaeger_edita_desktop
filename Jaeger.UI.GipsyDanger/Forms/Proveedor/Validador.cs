﻿using Jaeger.UI.Validador.Builder;
using Jaeger.Aplication.Validador.Adapter;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.Proveedor {
    public class Validador : UI.Validador.Forms.ComprobanteFiscalForm {
        public Validador(UIMenuElement menuElement) : base() {
            this.Text = "Proveedor: Validar Comprobantes";
            this.Load += Validador_Load;
        }

        private void Validador_Load(object sender, System.EventArgs e) {
            this.WorkerBackup = new BackupWorker(new BackupWorkerParameters {
                Configuration = this.Manager.Configuration,
                RFC = ConfigService.Synapsis.Empresa.RFC,
            });
        }

        protected override void PDFValidacion() {
            foreach (var item in this.Manager.DataSource) {
                if (item.Version == "3.3") {
                    var val = ComprobanteValidacionPrinter.Json(item.Validacion.Json());
                    if (val != null) {
                        Jaeger.UI.Comprobante.Services.ComunService.ValidacionPDF(val, System.IO.Path.Combine((string)this.Tag, val.KeyName() + ".pdf"));
                    }
                }
            }
        }

        protected override void ViewBuilder() {
            using (IValidadorGridViewBuilder builder = new ValidadorGridViewBuilder()) {
                this.gridDocumentos.Columns.AddRange(builder.Templetes().Master2().Build());
                this.gridConceptos.Columns.AddRange(builder.Templetes().Conceptos().Master().Build());
            }
        }
    }
}
