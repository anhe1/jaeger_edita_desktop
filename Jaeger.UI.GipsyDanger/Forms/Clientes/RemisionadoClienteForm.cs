﻿using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Clientes {
    public class RemisionadoClienteForm : UI.Almacen.PT.Forms.RemisionadoClienteForm {

        public RemisionadoClienteForm(UIMenuElement uIMenuElement, IRemisionadoService service) : base(uIMenuElement, service) {
        }
    }
}
