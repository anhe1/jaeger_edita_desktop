﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Banco;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Clientes {
    /// <summary>
    /// buscar comprobante a traves del servicio del banco
    /// </summary>
    public class BuscarComprobanteForm : UI.Banco.Forms.BuscarComprobanteForm {
        public BuscarComprobanteForm() : base() {
            this.Load += this.BuscarComprobanteForm_Load;
        }

        public BuscarComprobanteForm(MovimientoConceptoDetailModel conceptoDetailModel, int idDirectorio, IBancoService service) : 
            base(conceptoDetailModel, idDirectorio, Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision, service) {
            this.Load += this.BuscarComprobanteForm_Load;
        }

        private void BuscarComprobanteForm_Load(object sender, System.EventArgs e) {
            this.Text = "Remisionado al Cliente (CP)";
            this.TBuscar.lblCaption.Text = "Folio: ";
            this.TBuscar.lblCaption.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.TBuscar.Descripcion.MaxSize = new System.Drawing.Size(50, 20);
            this.TBuscar.ShowTextBox = true;
            this.TBuscar.Descripcion.KeyPress += Extensions.TextBoxOnlyNumbers_KeyPress;
            this.TBuscar.Descripcion.TextChanged += this.Descripcion_TextChanged;
            this.service = new Aplication.CP.Service.BancoService();
        }

        private void Descripcion_TextChanged(object sender, System.EventArgs e) {
            if (!string.IsNullOrEmpty(this.TBuscar.Descripcion.Text))
                this._IdDirectorio = int.Parse(this.TBuscar.Descripcion.Text);
        }

        public override void WorkerSearch_DoWork(object sender, DoWorkEventArgs e) {
            var d0 = this.service.GetList<MovimientoBancarioComprobanteDetailModel>(
                new Domain.Banco.Builder.ComprobanteSearchQueryBuilder().ForRemision().With(this._CurrentConcepto).WithFolio(int.Parse(this.TBuscar.Descripcion.Text)).Build()
                ).ToList();
            this.datos = new BindingList<MovimientoBancarioComprobanteDetailModel>(d0);
            e.Result = true;
        }
    }
}
