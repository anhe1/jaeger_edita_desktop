﻿using System;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Clientes {
    /// <summary>
    /// recibos de cobro a clientes
    /// </summary>
    public class ReciboCobroCatalogoForm : Banco.Forms.MovimientosForm {
        #region declaraciones
        protected internal RadMenuItem context_CrearComprobantePago = new RadMenuItem { Text = "Comprobante de Pago" };
        protected internal RadMenuItem TReciboCobro_Nuevo = new RadMenuItem { Text = "Nuevo" };
        protected IComprobantesFiscalesSearchService searchService;
        #endregion

        public ReciboCobroCatalogoForm(UIMenuElement menuElement) : base(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Ingreso) {
            this.Text = "Clientes: Cobranza";
            this._Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += ReciboCobroCatalogoForm_Load;
        }

        private void ReciboCobroCatalogoForm_Load(object sender, EventArgs e) {
            this.Icon = base.Icon;
            this.Text = "Clientes: Cobranza";
            this.Movimientos.TBancoLayout.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.searchService = new ComprobanteFiscalSearchService();
            this.TReciboCobro_Nuevo.Click += TReciboCobro_Nuevo_Click;
            this.TMovimiento.Nuevo.Enabled = this._Permisos.Agregar;
            this.TMovimiento.Cancelar.Enabled = this._Permisos.Cancelar;
            this.TMovimiento.Editar.Enabled = this._Permisos.Editar;
            this.Movimientos.Cancelar = this._Permisos.Cancelar;
            this.Movimientos.Auditar = this._Permisos.Autorizar;
            this.Movimientos.GridData.AllowEditRow = this._Permisos.Status;
            this.TMovimiento.ExportarExcel.Enabled = this._Permisos.Exportar;
            this.Movimientos.TEmportar.Enabled = this._Permisos.Exportar;
            this.context_CrearComprobantePago.Click += CrearComprobantePago_Click;
            this.TMovimiento.Nuevo.Items.Add(TReciboCobro_Nuevo);
            this.Movimientos.MenuContextual.Items.Add(context_CrearComprobantePago);
            this.Service = new Aplication.Banco.BancoService();
            this._Empresa = new Aplication.Empresa.Service.ConfigurationService();
            var d0 = this._Empresa.Get(Domain.Empresa.Enums.ConfigGroupEnum.Bancos).ToList<Domain.Empresa.Contracts.IParametroModel>();
            this._Configuration = new Aplication.Banco.Builder.ConfigurationBuilder().Build(d0);
        }

        private void TReciboCobro_Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ReciboCobroForm(Aplication.Banco.BancoService.Create().New().Status(Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Transito).Build(), this.Service)) {
                nuevo.ShowDialog(this);
            }
        }

        private void CrearComprobantePago_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                using (var espera = new UI.Common.Forms.Waiting1Form(this.CrearComprobantePago)) {
                    espera.Text = "Espera un momento...";
                    espera.ShowDialog(this);

                    if (seleccionado.Tag != null) {
                        // mostrar la vista
                        var recepcionPago = new Comprobante.Forms.ComprobanteFiscalRecepcionPagoForm((Domain.Comprobante.Entities.ComprobanteFiscalDetailModel)seleccionado.Tag, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
                        recepcionPago.Show();
                    } else {
                        //  RadMessageBox.Show(this, Properties.Resources.Msg_CrearComprobante_Error, "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        private void CrearComprobantePago() {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                if (seleccionado.Comprobantes != null) {
                    // lista de comprobantes relacionados
                    var lista = seleccionado.Comprobantes.Select(it => it.IdDocumento).ToList();
                    var _doctos = this.searchService.SearchUUID(lista);
                    if (_doctos != null) {
                        if (_doctos.Count > 0) {
                            IComprobantesFiscalesService c1 = new ComprobantesFiscalesService();
                            var _d1 = ComprobanteFiscalService.Builder().Templete().RecepcionPago().Add(_doctos.ToList()).Build();
                            if (_d1 != null) {
                                _d1.RecepcionPago[0].FechaPagoP = seleccionado.FechaAplicacion.Value;
                                _d1.RecepcionPago[0].FormaDePagoP = seleccionado.ClaveFormaPago;
                                seleccionado.Tag = _d1;
                            }
                        }
                    }
                }
            }
        }
    }
}
