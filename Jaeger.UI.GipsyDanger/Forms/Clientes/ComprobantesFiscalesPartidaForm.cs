﻿using System;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Clientes {
    public class ComprobantesFiscalesPartidaForm : Comprobante.Forms.ComprobantesFiscalesPartidaForm {
        public ComprobantesFiscalesPartidaForm(IComprobantesFiscalesService service, UIMenuElement menuElement) : base(menuElement) {
            this._Service = service;
            this.Load += ComprobantesFiscalesPartidaForm_Load;
        }

        private void ComprobantesFiscalesPartidaForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes: Fact. Partidas";
        }

        public override void Actualizar_Click(object sender, EventArgs e) {
            base.Actualizar_Click(sender, e);
        }
    }
}
