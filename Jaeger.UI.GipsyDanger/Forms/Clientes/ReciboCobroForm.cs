﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.UI.Forms.Clientes {
    public class ReciboCobroForm : Banco.Forms.MovimientoBancarioForm {
        private bool IsReady = false;
        public ReciboCobroForm() : base() {
            this.IsReady = false;
        }

        public ReciboCobroForm(Aplication.Banco.IBancoService service) : base(service) {
            this.Load += ReciboCobroForm_Load;
        }

        public ReciboCobroForm(IMovimientoBancarioDetailModel movimiento, Aplication.Banco.IBancoService service) : base(movimiento, service) {
            this.Load += ReciboCobroForm_Load;
        }

        private void ReciboCobroForm_Load(object sender, EventArgs e) {
            this.Text = "Banco: Recibo de Cobro";
        }

        protected override void TMovimiento_Imprimir_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento != null) {
                this.TMovimiento.Movimiento.FormatoImpreso = MovimientoBancarioFormatoImpreso.ReciboCobro;
                var imprimir = new UI.Banco.Forms.ReporteForm(new MovimientoBancarioPrinter(this.TMovimiento.Movimiento));
                imprimir.Show();
            }
        }

        protected override void GetTiposMovimiento() {
            this._TiposMovimiento = new System.ComponentModel.BindingList<MovimientoConceptoDetailModel>(
                this.TMovimiento.Service.GetList<MovimientoConceptoDetailModel>(Aplication.Banco.BancoConceptoService.Query().TipoOperacion(BancoTipoOperacionEnum.ReciboCobro).OnlyActive().Build()
                ).ToList());
        }

        protected override void BindingCompleted(object sender, EventArgs e) {
            base.BindingCompleted(sender, e);
            if (this.IsReady == false) {
                ConceptoMovimiento_DropDownClosed(sender, null);
                this.IsReady = true;
            }
        }

        protected override void TComprobante_Buscar_Click(object sender, EventArgs e) {
            if (this.TComprobante.Documento.SelectedItem != null) {
                var seleccionado = this.TComprobante.Documento.SelectedItem.DataBoundItem as ItemSelectedModel;
                if (seleccionado != null) {
                    if (seleccionado.Id == (int)MovimientoBancarioTipoComprobanteEnum.Remision) {
                        var buscar = new BuscarComprobanteForm(this._CurrentTipoMovimiento, this.TMovimiento.Movimiento.IdDirectorio, this.TMovimiento.Service);
                        buscar.AgregarComprobante += BTComprobante_Buscar_AgregarComprobante;
                        buscar.ShowDialog(this);
                    } else {
                        var buscar = new Banco.Forms.BuscarComprobanteForm(this._CurrentTipoMovimiento, this.TMovimiento.Movimiento.IdDirectorio, this.TComprobante.GetTipoComprobante(), this.TMovimiento.Service);
                        buscar.AgregarComprobante += BTComprobante_Buscar_AgregarComprobante;
                        buscar.ShowDialog(this);
                    }
                }
            } else {
                RadMessageBox.Show("Tipo de comprobante no soportado.", "Atención", MessageBoxButtons.OK);
            }
        }

        private void BTComprobante_Buscar_AgregarComprobante(object sender, MovimientoBancarioComprobanteDetailModel e) {
            if (e != null) {
                if (e != null) {
                    if (this.TMovimiento.Movimiento.Agregar(e) == true)
                        RadMessageBox.Show("El comprobante seleccionado ya esta en la lista.", "Atención", MessageBoxButtons.OK);
                }
            }
        }
    }
}
