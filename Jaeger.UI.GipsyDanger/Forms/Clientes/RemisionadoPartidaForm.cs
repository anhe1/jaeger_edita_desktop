﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Common.Forms;
//using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;

namespace Jaeger.UI.Forms.Clientes {
    public class RemisionadoPartidaForm : UI.Almacen.PT.Forms.RemisionadoPartidaForm {
        #region declaraciones
        protected internal RadMenuItem _ContextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService _ComprobanteFiscalService;
        #endregion

        public RemisionadoPartidaForm(UIMenuElement menuElement, IRemisionadoService service) : base(menuElement, service) {
            this.Load += RemisionesFiscalesPartidasForm_Load;
        }

        private void RemisionesFiscalesPartidasForm_Load(object sender, EventArgs e) {
            this.TRemision.menuContextual.Items.Add(this._ContextMenuFacturar);
            // para esta version es necesario utilizar el servicio de CP
            this._Service = new Aplication.CP.Service.RemisionadoService();
            this._ComprobanteFiscalService = new Aplication.Comprobante.Services.ComprobantesFiscalesService();
            this._ContextMenuFacturar.Click += this.ContextMenuFacturar_Click;
        }

        private void ContextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV33 = this.Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV33 != null) {
                var nuevo = new UI.Comprobante.Forms.ComprobanteFiscalGeneralForm(cfdiV33, CFDISubTipoEnum.Emitido, -1) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            this.Tag = null;
        }

        private void CrearFactura() {
            if (this.TRemision.GridData.CurrentRow != null) {
                var seleccion = new List<RemisionPartidaModel>();
                seleccion.AddRange(this.TRemision.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as RemisionPartidaModel));
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = Aplication.Comprobante.Services.ComprobanteFiscalService.Builder().New(CFDITipoComprobanteEnum.Ingreso).Build();// this._ComprobanteFiscalService.GetNew(CFDITipoComprobanteEnum.Ingreso);
                    foreach (var item in seleccion) {
                        cfdiV33.Conceptos.Add(Helpers.ConversionHelper.ConverTo(item));
                    }
                    this.Tag = cfdiV33;
                }
            }
        }
    }
}
