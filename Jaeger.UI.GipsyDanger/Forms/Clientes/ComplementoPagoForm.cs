﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.UI.Forms.Clientes {
    internal class ComplementoPagoForm : Comprobante.Forms.ComprobanteVsComplementoPagoForm {
        public ComplementoPagoForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Load += ComplementoPagoForm_Load;
        }

        private void ComplementoPagoForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes: Complemento de Pago";
            this.TComprobantes.GridData.AutoGenerateColumns = true;
            this._Service = new ComprobantesFiscalesService();
            var permisos = new UIAction(this.menuElement.Permisos);
            this.TComprobantes.Permisos = permisos;
        }

        public override void Consultar() {
            var query = ComprobantesFiscalesService.Query().Emitido().WithYear(this.TComprobantes.GetEjercicio()).WithMonth(this.TComprobantes.GetPeriodo()).Build();
            this._DataSource = new BindingList<ComplementoVsPagoMergeDoctoModel>(this._Service.GetList<ComplementoVsPagoMergeDoctoModel>(query).ToList());
        }
    }
    //internal class ComplementoPagoForm : Comprobante.Forms.RecibosElectronicosPagoForm {
    //    public ComplementoPagoForm(IComprobantesFiscalesService service, UIMenuElement menuElement) : base(service) {
    //        this.TComprobantes.Permisos = new UIAction(menuElement.Permisos);
    //        this.Load += ComplementoPagoForm_Load;
    //    }

    //    private void ComplementoPagoForm_Load(object sender, EventArgs e) {
    //        this.Text = "Clientes: Complemento de Pago";
    //        this.TComprobantes.TipoComprobante = CFDISubTipoEnum.Emitido;
    //        this.TComprobantes.Templete = Comprobante.Builder.ComprobanteFiscalGridBuilder.TempleteEnum.ComplementoPago;
    //        this.TComprobantes.GridData.AllowEditRow = this.TComprobantes.Permisos.Status;
    //        this.TComprobantes.ShowExportarExcel = this.TComprobantes.Permisos.Exportar;
    //    }

    //    public override void Consultar() {
    //        var d0 = ComprobantesFiscalesService.Query().Emitido().WithYear(this.TComprobantes.GetEjercicio()).WithMonth(this.TComprobantes.GetPeriodo()).WithTipo(Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos).Build();
    //        this._DataSource = new BindingList<ComprobanteReciboEletronicoPagoSingle>(this._Service.GetList<ComprobanteReciboEletronicoPagoSingle>(d0).ToList());
    //    }
    //}
}