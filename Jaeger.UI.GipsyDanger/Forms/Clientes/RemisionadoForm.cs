﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.UI.Banco.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.Clientes {
    public class RemisionadoForm : UI.Almacen.PT.Forms.RemisionadoForm {
        #region declaraciones
        protected Aplication.Banco.IBancoService _BancoService;
        protected Aplication.Banco.IBancoMovientosSearchService _BancoSearchService;
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService _ComprobanteFiscalService;
        protected internal GridViewTemplate _GridCobranza = new GridViewTemplate() { Caption = "Cobranza" };
        protected internal RadMenuItem _ContextMenuFacturar = new RadMenuItem { Text = "Crear Factura", Name = "Facturar" };
        protected internal RadMenuItem _ContextMenuReciboCobro = new RadMenuItem { Text = "Registrar Cobro", Name = "ReciboCobro" };
        #endregion

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Text = "Clientes: Remisionado";
            this.Load += RemisionesCatalogoForm_Load;
        }

        private void RemisionesCatalogoForm_Load(object sender, EventArgs e) {
            this.TRemision.menuContextual.Items.Add(this._ContextMenuFacturar);
            this.TRemision.menuContextual.Items.Add(this._ContextMenuReciboCobro);
            var d3 = ConfigService.GeMenuElement(this._ContextMenuReciboCobro.Name);
            this._ContextMenuReciboCobro.Enabled = ConfigService.HasPermission(d3);

            // para esta version es necesario utilizar el servicio de CP
            this._Service = new Aplication.CP.Service.RemisionadoService();
            
            this._ContextMenuFacturar.Click += this.ContextMenuFacturar_Click;
            this._ContextMenuReciboCobro.Click += ContextMenuReciboCobro_Click;
            this.TempleteCobranza();
        }

        #region barra de herramientas
        public override void TRemision_PorCliente_Click(object sender, EventArgs e) {
            var porCliente = new RemisionadoClienteForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            porCliente.Show();
        }

        public override void TRemision_PorCobrar_Click(object sender, EventArgs e) {
            var porPartida = new RemisionadoPorCobrarForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            porPartida.Show();
        }

        public override void TRemision_PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new RemisionadoPartidaForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            porPartida.Show();
        }
        #endregion

        #region menu contextual
        private void ContextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV40 = this.Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV40 != null) {
                var nuevo = new Comprobante1FiscalForm(cfdiV40, CFDISubTipoEnum.Emitido, -1) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            this.Tag = null;
        }

        private void ContextMenuReciboCobro_Click(object sender, EventArgs e) {
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            var correcto = seleccion.Where(it => it.Status == Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Count();
            if (seleccion.Count != correcto) {
                RadMessageBox.Show(this, "No es posible crear el registro de cobranza. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", 
                    "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var espera = new Waiting1Form(this.CrearReciboCobro)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var remision = this.Tag as MovimientoBancarioDetailModel;
            if (remision != null) {
                var _nuevoMovimiento = new ReciboCobroForm(remision, this._BancoService);
                _nuevoMovimiento.ShowDialog(this);
            }
            this.Tag = null;
        }
        #endregion

        #region metodos privados
        private void CrearFactura() {
            this._ComprobanteFiscalService = new ComprobantesFiscalesService();
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = ComprobanteFiscalService.Builder().New(CFDITipoComprobanteEnum.Ingreso).Build();
                    foreach (var seleccionado in seleccion) {
                        var remision = this._Service.GetById(seleccionado.IdRemision);
                        foreach (var item in remision.Conceptos) {
                            cfdiV33.Conceptos.Add(Helpers.ConversionHelper.ConverTo(item));
                        }
                    }
                    this.Tag = cfdiV33;
                }
            }
        }

        private void CrearReciboCobro() {
            if (this._BancoService == null) this._BancoService = new Aplication.Banco.BancoService();
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = Aplication.Banco.BancoService.Create().New().Status();
                    foreach (var seleccionado in seleccion) {
                        cfdiV33.Add(Helpers.ConversionHelper.ConverTo(seleccionado));
                    }
                    this.Tag = cfdiV33.Build();
                }
            }
        }

        private void TempleteCobranza() {
            using (IGridViewBancoBuilder view = new GridViewBancoBuilder()) {
                this._GridCobranza.Columns.AddRange(view.Templetes().Cobranza().Build());
            }
            this._GridCobranza.HierarchyDataProvider = new GridViewEventDataProvider(_GridCobranza);
            this.TRemision.GridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
            this.TRemision.GridData.MasterTemplate.Templates.Add(_GridCobranza);
            this._GridCobranza.Standard();
        }

        private void InfoCobranza() {
            var rowView = this.TRemision.GridData.CurrentRow.DataBoundItem as IRemisionSingleModel;
            if (rowView != null) {
                if (this._BancoSearchService == null) this._BancoSearchService = new Aplication.Banco.BancoMovientosSearchService();
                var tabla = this._BancoSearchService.GetSearchMovimientos(rowView.IdRemision, Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision);
                if (tabla != null) {
                    this.Tag = tabla;
                }
            }
        }
        #endregion

        #region acciones del grid
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this._GridCobranza.Caption) {
                var rowView = e.ParentRow.DataBoundItem as IRemisionSingleModel;
                if (rowView != null) {
                    using (var espera = new Waiting1Form(this.InfoCobranza)) {
                        espera.Text = "Busncado información de cobranza";
                        espera.ShowDialog(this);
                    }
                    if (this.Tag != null) {
                        var tabla = (BindingList<MovimientoComprobanteMergeModelView>)this.Tag;
                        if (tabla != null) {
                            foreach (var item in tabla) {
                                var newRow = e.Template.Rows.NewRow();
                                e.SourceCollection.Add(GridViewBancoBuilder.ConverTo(newRow, item));
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
