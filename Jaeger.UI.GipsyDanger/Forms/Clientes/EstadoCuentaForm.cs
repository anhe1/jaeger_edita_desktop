﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Clientes {
    internal class EstadoCuentaForm : Comprobante.Forms.EstadoCuentaGeneralForm {
        public EstadoCuentaForm(UIMenuElement menuElement) : base() {
            this.Load += EstadoCuentaForm_Load;
        }

        private void EstadoCuentaForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes: Edo. de Cuenta";
            this.TComprobante.TipoComprobante = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido;
            this.TComprobante.Templete = Comprobante.Builder.ComprobanteFiscalGridBuilder.TempleteEnum.EdoCuenta;
            this.TComprobante.ShowPeriodo = false;
            if (this.TComprobante._Service == null)
                this.TComprobante._Service = new Aplication.Comprobante.Services.ComprobantesFiscalesService(Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido);
        }

        protected override void Actualizar() {
            this._DataSource = new BindingList<EstadoCuenta>(
                this.TComprobante._Service.GetList<EstadoCuenta>(
                   Aplication.Comprobante.Services.ComprobantesFiscalesService.Query()
                       .Emitido().WithStatus(Domain.Comprobante.ValueObjects.CFDStatusIngresoEnum.PorCobrar)
                       .WithYear(this.TComprobante.GetEjercicio())
                       .Build()).ToList<EstadoCuenta>()
                );
        }

        protected override void GetComprobantes() {
            if (this.TComprobante.GridData.CurrentRow is null)
                return;

            var d1 = this.TComprobante.GridData.CurrentRow.DataBoundItem as EstadoCuenta;
            if (d1 != null) {
                var d0 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Emitido().WithStatus(Domain.Comprobante.ValueObjects.CFDStatusIngresoEnum.PorCobrar).WithYear(this.TComprobante.GetEjercicio()).WithIdDirectorio(d1.IdDirectorio).Build();
                var d2 = this.TComprobante._Service.GetList<ComprobanteFiscalDetailSingleModel>(d0);
                d1.Tag = d2.ToList();
            }
        }
    }
}
