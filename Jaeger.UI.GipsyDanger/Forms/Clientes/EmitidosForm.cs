﻿using System;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Comprobante.Forms;
using Jaeger.UI.Banco.Builder;

namespace Jaeger.UI.Forms.Clientes {
    /// <summary>
    /// comprobantes emitidos
    /// </summary>
    public class EmitidosForm : ComprobantesFiscalesEmitidoForm {
        protected internal RadMenuItem MenuContextual_ReciboCobro = new RadMenuItem { Text = "Registrar Cobro" };
        protected internal RadMenuItem PorCobrar = new RadMenuItem { Text = "Por Cobrar", ToolTipText = "Comprobantes por cobrar", Name = "adm_gcli_comxcobrar" };
        protected internal RadMenuItem PorPartidas = new RadMenuItem { Text = "Por Partidas", ToolTipText = "Comprobantes por Partidas", Name = "adm_gcli_comxpart" };
        protected internal RadMenuItem PorRecetor = new RadMenuItem { Text = "Por Receptor", ToolTipText = "Comprobantes por Receptor", Name = "adm_gcli_comxpart1" };
        //protected internal RadMenuItem ComplementoPago = new RadMenuItem { Text = "Complemento de Pago", ToolTipText = "", Name = "ComplementoPago" };
        protected internal GridViewTemplate _GridCobranza = new GridViewTemplate();
        protected internal Aplication.Banco.IBancoMovientosSearchService _BancoSearch;
        protected Aplication.Banco.IBancoService _BancoService;
        protected internal UIMenuElement menuElement;

        public EmitidosForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Load += EmitidosForm_Load;
        }

        private void EmitidosForm_Load(object sender, EventArgs e) {
            var permisos = new UIAction(this.menuElement.Permisos);
            this._BancoSearch = new Aplication.Banco.BancoMovientosSearchService();
            this.menuContextual.Items.Add(MenuContextual_ReciboCobro);
            this.MenuContextual_ReciboCobro.Click += new EventHandler(this.TMenuContextual_ReciboCobro_Click);
            this.TComprobante.ShowExportarExcel = permisos.Exportar;
            this.TComprobante.Herramientas.Items.Add(this.PorCobrar);
            this.TComprobante.Herramientas.Items.Add(this.PorPartidas);
            this.TComprobante.Herramientas.Items.Add(this.PorRecetor);
          //  this.TComprobante.Herramientas.Items.Add(this.ComplementoPago);
            this.TComprobante.ShowHerramientas = true;
            this.TempleteCobranza();
            _GridCobranza.HierarchyDataProvider = new GridViewEventDataProvider(_GridCobranza);
            this.GridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
            this.PorCobrar.Click += TComprobante_PorCobrar_Click;
            this.PorPartidas.Click += TComprobante_PorPartidas_Click;
            this.PorRecetor.Click += TComprobante_PorRecetor_Click;
            //this.ComplementoPago.Click += TComprobante_ComplementoPago_Click;
        }

        private void TComprobante_PorRecetor_Click(object sender, EventArgs e) {
            var d0 = new ComprobantesFiscalesContribuyenteForm(this._Service, this.menuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }

        private void TComprobante_PorPartidas_Click(object sender, EventArgs e) {
            var d0 = new ComprobantesFiscalesPartidaForm(this._Service, this.menuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }

        #region barra de herramientas
        private void TComprobante_PorCobrar_Click(object sender, EventArgs e) {
            var d0 = new EmitidosPorCobrarForm(this.menuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }
        public override void TComprobante_Nuevo_Click(object sender, EventArgs e) {
            var _facturaNuevo = new Comprobante1FiscalForm(CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
            _facturaNuevo.Show();
        }

        public override void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    if (_seleccionado.IdDocumento != null && _seleccionado.IdDocumento != "") {
                        var _visualizar = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                        _visualizar.Show();
                    } else {
                        if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                            var _editar = new ComprobanteFiscalRecepcionPagoForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if ((_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Ingreso | _seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Traslado) && _seleccionado.Documento == "porte") {
                            var _editar = new ComprobanteFiscalCartaPorteForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Nomina) {
                            RadMessageBox.Show(this, "Tipo de comprobante no soportado.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                        } else {
                            var _editar = new Comprobante1FiscalForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        }
                    }
                }
            }
        }

        //private void TComprobante_ComplementoPago_Click(object sender, EventArgs e) { 
        //    var d0 = new ComplementoPagoForm(this._Service, this.menuElement) { MdiParent = this.ParentForm };
        //    d0.Show();
        //}
        #endregion

        #region menu contextual
        private void TMenuContextual_ReciboCobro_Click(object sender, EventArgs e) {
            var _seleccion = this.GetSelection();
            if (_seleccion != null) {
                if (_seleccion.Count > 0) {
                    var _si = _seleccion.Where(it => it.Status == "Cancelado" | it.Status == "Importado" | it.Status == "Cobrado" | it.Estado == "Cancelado");
                    if (_si.Count() > 0) {
                        RadMessageBox.Show(this, "No es posible crear el registro de cobranza. Es posible que uno o más comprobantes no tengan el status adecuado para esta acción.", 
                            "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }

            var d0 = _seleccion.Select(it => it.IdDocumento).ToList();
            var d1 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().WithIdDocumento(d0.ToArray()).Build();
            var seleccion = this._Service.GetList<ComprobanteFiscalDetailModel>(d1).ToList();

            foreach (var item in seleccion) {
                if (item.RecepcionPago != null) {
                    foreach (var item1 in item.RecepcionPago) {
                        if (item1.DoctoRelacionados != null) {
                            foreach (var item2 in item1.DoctoRelacionados) {
                                d0.Add(item2.IdDocumentoDR);
                            }
                        }
                    }
                }
            }
            d1 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().WithIdDocumento(d0.ToArray()).Build();
            seleccion = this._Service.GetList<ComprobanteFiscalDetailModel>(d1).ToList();
            if (this._BancoService == null)
                this._BancoService = new Aplication.Banco.BancoService();
            var _movimiento = Aplication.Banco.BancoService.Create().New().Status().IdConcepto(this._BancoService.Configuration.IdReciboCobro);
            foreach (var item in _seleccion) {
                _movimiento.Add(new MovimientoBancarioComprobanteDetailModel {
                    Activo = true,
                    ClaveFormaPago = item.ClaveFormaPago,
                    ClaveMetodoPago = item.ClaveMetodoPago,
                    ClaveMoneda = item.ClaveMoneda,
                    EmisorNombre = item.EmisorNombre,
                    EmisorRFC = item.EmisorRFC,
                    Estado = item.Estado,
                    FechaEmision = item.FechaEmision,
                    FechaNuevo = item.FechaNuevo,
                    IdComprobante = item.Id,
                    IdDocumento = item.IdDocumento,
                    NumParcialidad = item.NumParcialidad,
                    ReceptorNombre = item.ReceptorNombre,
                    ReceptorRFC = item.ReceptorRFC,
                    Serie = item.Serie,
                    Version = item.Version,
                    Total = item.Total,
                    SubTipoComprobante = (CFDISubTipoEnum)item.IdSubTipo,
                    TipoComprobanteText = item.TipoComprobanteText,
                    Folio = item.Folio, 
                    Acumulado = item.Acumulado, 
                    PorCobrar = item.Saldo
                });
                _movimiento.IdBeneficiario(item.IdDirectorio);
            }
            var _nuevoMovimiento = new ReciboCobroForm(_movimiento.Build(), new Aplication.Banco.BancoService());
            _nuevoMovimiento.ShowDialog(this);
        }
        #endregion

        #region acciones del grid
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this._GridCobranza.Caption) {
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    var tabla = this._BancoSearch.GetSearchMovimientos(rowView.Id, Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.CFDI);
                    foreach (var item in tabla) {
                        var newRow = e.Template.Rows.NewRow();
                        e.SourceCollection.Add(GridViewBancoBuilder.ConverTo(newRow, item));
                    }
                }
            }
        }

        private void TempleteCobranza() {
            var tableViewDefinition1 = new TableViewDefinition();
            _GridCobranza.Caption = "Cobranza";
            _GridCobranza.Columns.AddRange(new GridViewBancoBuilder().Templetes().Cobranza().Build());
            _GridCobranza.ViewDefinition = tableViewDefinition1;
            this.GridData.MasterTemplate.Templates.Add(_GridCobranza);
            this._GridCobranza.Standard();
        }
        #endregion
    }
}
