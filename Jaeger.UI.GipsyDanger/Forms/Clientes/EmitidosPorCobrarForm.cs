﻿using System.Linq;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Clientes {
    public class EmitidosPorCobrarForm : EmitidosForm {
        public EmitidosPorCobrarForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Clientes facturación: Por Cobrar";
            this.Load += EmitidosPorCobrarForm_Load;
        }

        private void EmitidosPorCobrarForm_Load(object sender, System.EventArgs e) {
            this.TComprobante.ShowPeriodo = false;
        }

        public override void Consultar() {
            var d0 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Emitido().WithStatus(Domain.Comprobante.ValueObjects.CFDStatusIngresoEnum.PorCobrar).WithYear(this.TComprobante.GetEjercicio()).Build();
            this._DataSource = new System.ComponentModel.BindingList<Domain.Comprobante.Contracts.IComprobanteFiscalDetailSingleModel>(
                    this._Service.GetList<Domain.Comprobante.Entities.ComprobanteFiscalDetailSingleModel>(d0).ToList<Domain.Comprobante.Contracts.IComprobanteFiscalDetailSingleModel>()
                );
        }
    }
}
