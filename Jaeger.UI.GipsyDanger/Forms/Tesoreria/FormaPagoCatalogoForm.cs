﻿namespace Jaeger.UI.Forms.Tesoreria {
    internal class FormaPagoCatalogoForm : Banco.Forms.FormaPagoCatalogoForm {
        public FormaPagoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Service = new Aplication.Banco.BancoFormaPagoService();
        }
    }
}
