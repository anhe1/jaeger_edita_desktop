﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Tesoreria {
    internal class CuentasBancariasForm : Banco.Forms.CuentaBancariaCatalogoForm {
        public CuentasBancariasForm(UIMenuElement menuElement) : base(menuElement) { }
    }
}
