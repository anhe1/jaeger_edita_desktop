﻿namespace Jaeger.UI.Forms.Tesoreria {
    internal class CuentasBancariaCatalogoForm : Banco.Forms.CuentaBancariaCatalogoForm {
        public CuentasBancariaCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.TCuentaB._Service = new Aplication.Banco.BancoCuentaService();
        }
    }
}
