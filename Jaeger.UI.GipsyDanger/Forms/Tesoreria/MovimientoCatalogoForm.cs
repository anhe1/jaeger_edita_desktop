﻿namespace Jaeger.UI.Forms.Tesoreria {
    internal class MovimientoCatalogoForm : Banco.Forms.MovimientoCatalogoForm {
        public MovimientoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.TMovimiento.Service = new Aplication.Banco.BancoService();
            this.Load += MovimientoCatalogoForm_Load;
        }

        private void MovimientoCatalogoForm_Load(object sender, System.EventArgs e) {
            
        }
    }
}
