﻿namespace Jaeger.UI.Forms.Tesoreria {
    internal class ConceptoCatalogoForm : Banco.Forms.ConceptoCatalogoForm {
        public ConceptoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this._Service = new Aplication.Banco.BancoConceptoService();
        }
    }
}
