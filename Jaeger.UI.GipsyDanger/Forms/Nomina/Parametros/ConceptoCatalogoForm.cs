﻿using System;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class ConceptoCatalogoForm : UI.Nomina.Forms.Procesos.ConceptoCatalogoForm {
        protected internal UIAction _Permisos;
        public ConceptoCatalogoForm(UIMenuElement menuElement) : base() {
            this._Permisos = new UIAction(menuElement.Permisos);
            this.Load += ConceptoNominaCatalogoForm_Load;
        }

        private void ConceptoNominaCatalogoForm_Load(object sender, System.EventArgs e) {
            this.TConcepto.Permisos = this._Permisos;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this.service = new ConceptoService();
        }
    }
}
