﻿using System;

namespace Jaeger.UI.Forms.Herramientas.Validador {
    internal class CertificadoForm : Certificado.Forms.CertificadoForm {
        public CertificadoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += CertificadoForm_Load;
        }

        private void CertificadoForm_Load(object sender, EventArgs e) {

        }

        protected override void TVerificarButton_Click(object sender, EventArgs eventArgs) {
            base.TVerificarButton_Click(sender, eventArgs);
            this.Certificado = Aplication.Validador.Services.CertificadoService.Certificado(this.Certificate.PathCer.Text);
            this.TBinding();
        }
    }
}
