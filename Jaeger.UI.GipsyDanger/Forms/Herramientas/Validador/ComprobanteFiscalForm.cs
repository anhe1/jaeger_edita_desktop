﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Validador.Adapter;
using Jaeger.Aplication.Validador.Entities;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Forms.Herramientas.Validador {
    internal class ComprobanteFiscalForm : UI.Validador.Forms.ComprobanteFiscalForm {
        public ComprobanteFiscalForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Text = "Proveedor: Validar Comprobantes";
            this.Load += ComprobanteFiscalForm_Load;
        }

        private void ComprobanteFiscalForm_Load(object sender, System.EventArgs e) {
            this.WorkerBackup = new BackupWorker(new BackupWorkerParameters {
                Configuration = this.Manager.Configuration,
                RFC = ConfigService.Synapsis.Empresa.RFC,
            });
        }

        protected override void PDFValidacion() {
            foreach (var item in this.Manager.DataSource) {
                if (item.Version == "3.3") {
                    var val = ComprobanteValidacionPrinter.Json(item.Validacion.Json());
                    if (val != null) {
                        Comprobante.Services.ComunService.ValidacionPDF(val, System.IO.Path.Combine((string)this.Tag, val.KeyName() + ".pdf"));
                    }
                }
            }
        }
    }
}
