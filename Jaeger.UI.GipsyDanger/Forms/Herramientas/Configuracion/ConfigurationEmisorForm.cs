﻿namespace Jaeger.UI.Forms.Herramientas.Configuracion {
    /// <summary>
    /// Clase que representa el formulario de configuración del emisor.
    /// </summary>
    public class ConfigurationEmisorForm : Empresa.Forms.ConfigurationEmisorForm {
        private Domain.Base.Abstractions.UIMenuElement TEmisor;

        /// <summary>
        /// constructor
        /// </summary>
        public ConfigurationEmisorForm(Domain.Base.Abstractions.UIMenuElement element) : base(element) {
            this.TEmisor = element;
            this._Service = new Aplication.Empresa.Service.ConfigurationService();
            this._Configuracion = (this._Service as Aplication.Empresa.Contracts.IEmpresaService).GetEmpresa1();

            this.TEmisor_Guardar.Enabled = this.TEmisor.Action.Agregar;
            this.TEmisor_CIF.Enabled = this.TEmisor.Action.Agregar;
            this.ReadOnly();
        }
    }
}
