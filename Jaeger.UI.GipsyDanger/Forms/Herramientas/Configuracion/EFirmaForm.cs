﻿namespace Jaeger.UI.Forms.Herramientas.Configuracion {
    internal class EFirmaForm : UI.Repositorio.Forms.FielCatalogoForm {
        private Domain.Base.Abstractions.UIMenuElement MenuElement;

        public EFirmaForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.MenuElement = menuElement;
            this.Load += EFirmaForm_Load;
        }

        private void EFirmaForm_Load(object sender, System.EventArgs e) {
            this.TCertificado.Permisos = this.MenuElement.Action;
        }
    }
}
