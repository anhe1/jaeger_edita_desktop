﻿namespace Jaeger.UI.Forms.Herramientas.Configuracion {
    public class ClasificacionCategoriaForm : UI.Almacen.Forms.ClasificacionCategoriaForm {
        public ClasificacionCategoriaForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Load += ClasificacionCategoriaForm_Load;
        }

        private void ClasificacionCategoriaForm_Load(object sender, System.EventArgs e) {
            this.Text = "Configuración: Categorías";
        }
    }
}
