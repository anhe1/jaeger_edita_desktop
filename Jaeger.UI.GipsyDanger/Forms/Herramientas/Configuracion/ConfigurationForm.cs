﻿using System;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.Herramientas.Configuracion {
    internal class ConfigurationForm : Empresa.Forms.ConfigurationForm {
        private Telerik.WinControls.UI.RadPageViewPage pageNomina = new Telerik.WinControls.UI.RadPageViewPage() { Text = "| Nómina" };
        private Telerik.WinControls.UI.RadPageViewPage pageCP = new Telerik.WinControls.UI.RadPageViewPage() { Text = " || CP" };
        private Telerik.WinControls.UI.RadPageViewPage pageRepositorio = new Telerik.WinControls.UI.RadPageViewPage() { Text = " || Repositorio" };

        public ConfigurationForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += ConfigurationForm_Load;
        }

        private void ConfigurationForm_Load(object sender, EventArgs e) {
            this.Title.Text = $"|| Configuración de la empresa registrada: {ConfigService.Synapsis.Empresa.RFC}";

            var d1 = new UI.Repositorio.Forms.ConfigurationControl() {
                Location = new System.Drawing.Point(0, 0),
                Dock = System.Windows.Forms.DockStyle.Fill
            };

            var d2 = new UI.Nomina.Forms.Parametros.ConfigurationControl {
                Location = new System.Drawing.Point(0, 0),
                Dock = System.Windows.Forms.DockStyle.Fill
            };

            var d3 = new UI.Cotizador.Forms.Empresa.ConfigurationControl() {
                Location = new System.Drawing.Point(0, 0),
                Dock = System.Windows.Forms.DockStyle.Fill
            };

            pageRepositorio.Controls.Add(d1);
            pageNomina.Controls.Add(d2);
            pageCP.Controls.Add(d3);

            this.TabsControl.Pages.Add(pageRepositorio);
            this.TabsControl.Pages.Add(pageNomina);
            this.TabsControl.Pages.Add(pageCP);

            this.SynapsisProperty.SelectedObject = ConfigService.Synapsis;
            d1._Parametros = Aplication.Repositorio.Services.GeneralService.Configuration.Parametros;
            d1.CreateBinding();

            d3.Configuration = Aplication.Cotizador.Services.GeneralService.Configuration;
            d3.Preset();
        }
    }
}
