﻿namespace Jaeger.UI.Forms.Herramientas.Configuracion.Profile {
    public class ProfileActiveForm : Kaiju.Forms.ProfileActiveForm {
        public ProfileActiveForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ProfileActiveForm_Load;
        }

        private void ProfileActiveForm_Load(object sender, System.EventArgs e) {
            this.Text = "Parametros: Perfiles";
            this.Service = new Aplication.Kaiju.Services.ProfileService();
        }
    }
}
