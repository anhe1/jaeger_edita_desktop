﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.UI.Helpers {
    public class ConversionHelper {
        /// <summary>
        /// convertir de objeto concepto remision a concepto de facturacion
        /// </summary>
        /// <param name="item">RemisionConceptoDetailModel</param>
        public static Domain.Comprobante.Entities.ComprobanteConceptoDetailModel ConverTo(Domain.Almacen.PT.Entities.RemisionConceptoDetailModel item) {
            // concepto de comprobante
            var concepto = new Domain.Comprobante.Entities.ComprobanteConceptoDetailModel {
                Cantidad = item.Cantidad,
                ValorUnitario = item.Unitario,
                Descripcion = item.Descripcion,
                NumPedido = item.IdPedido,
                ClaveUnidad = "H87",
                ClaveProdServ = "82121511"
            };
            // impuestos
            concepto.Impuestos.Add(
                new Domain.Comprobante.Entities.ComprobanteConceptoImpuesto {
                    Impuesto = ImpuestoEnum.IVA,
                    TasaOCuota = new decimal(0.16),
                    Tipo = TipoImpuestoEnum.Traslado,
                    Base = concepto.Importe
                });
            return concepto;
        }

        public static Domain.Comprobante.Entities.ComprobanteConceptoDetailModel ConverTo(Domain.Almacen.PT.Entities.RemisionPartidaModel item) {
            // concepto de comprobante
            var concepto = new Domain.Comprobante.Entities.ComprobanteConceptoDetailModel {
                Cantidad = item.Cantidad,
                ValorUnitario = item.Unitario,
                Descripcion = item.Descripcion,
                NumPedido = item.IdPedido,
                ClaveUnidad = "H87",
                ClaveProdServ = "82121511"
            };
            // impuestos
            concepto.Impuestos.Add(
                new Domain.Comprobante.Entities.ComprobanteConceptoImpuesto {
                    Impuesto = ImpuestoEnum.IVA,
                    TasaOCuota = new decimal(0.16),
                    Tipo = TipoImpuestoEnum.Traslado,
                    Base = concepto.Importe
                });
            return concepto;
        }

        public static Domain.Banco.Entities.MovimientoBancarioComprobanteDetailModel ConverTo(Domain.Almacen.PT.Contracts.IRemisionSingleModel item) {
            var movimiento = new Domain.Banco.Entities.MovimientoBancarioComprobanteDetailModel() {
                Activo = true,
                TipoDocumento = Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision,
                ClaveFormaPago = item.ClaveFormaPago,
                ClaveMetodoPago = item.ClaveMetodoPago,
                ClaveMoneda = item.ClaveMoneda,
                EmisorNombre = item.EmisorRFC,
                EmisorRFC = item.EmisorRFC,
                Estado = item.Estado,
                FechaEmision = item.FechaEmision,
                FechaNuevo = item.FechaNuevo,
                IdComprobante = item.IdRemision,
                IdDocumento = item.IdDocumento,
                //NumParcialidad = item.NumParcialidad,
                ReceptorNombre = item.ReceptorNombre,
                ReceptorRFC = item.ReceptorRFC,
                Serie = item.Serie,
                Version = item.Version,
                Total = item.Total,
                SubTipoComprobante = CFDISubTipoEnum.Emitido,
                TipoComprobanteText = "Ingreso",
                Folio = item.Folio.ToString()
            };
            return movimiento;
        }
    }
}
