﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.MP.Repositories {
    public class SqlSugarValeAlmacenRepository : MySqlSugarContext<ValeAlmacenModel>, ISqValeAlmacenRepository {
        public SqlSugarValeAlmacenRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ValeAlmacenSingleModel)) {
                var sql = @"SELECT * FROM ALMMP LEFT JOIN ALMMPS ON ALMMP.ALMMP_ID = ALMMPS.ALMMPS_ALMMP_ID AND ALMMPS.ALMMPS_STTSB_ID = 0 @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            } else if (typeof(T1) == typeof(ValeAlmacenProductoView)) {
                var sql = @"SELECT * FROM MVAMP LEFT JOIN ALMMP ON MVAMP.MVAMP_ALMMP_ID = ALMMP.ALMMP_ID @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        /// <summary>
        /// recuperar objeto vale de almacen por su indice
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public new ValeAlmacenDetailModel GetById(int index) {
            //Manual mode
            var response = this.Db.Queryable<ValeAlmacenDetailModel>().Where(it => it.IdComprobante == index).Select<ValeAlmacenDetailModel>().Mapper((vale, cache) => {
                var allItems = cache.Get(valeList => {
                    var allIds = valeList.Select(it => it.IdComprobante).ToList();
                    return this.Db.Queryable<ValeAlmacenConceptoDetailModel>().Where(it => allIds.Contains(it.IdComprobante)).ToList();//Execute only once
                });

                var status = cache.Get(item1 => {
                    var allIds = item1.Select(it => it.IdComprobante).ToList();
                    return this.Db.Queryable<ValeAlmacenStatusModel>().Where(it => allIds.Contains(it.IdComprobante)).ToList();//Execute only once
                });

                var relaciones = cache.Get(item1 => {
                    var allIds = item1.Select(it => it.IdComprobante).ToList();
                    return this.Db.Queryable<ValeAlmacenRelacionModel>().Where(it => allIds.Contains(it.IdComprobante)).ToList();//Execute only once
                });

                vale.Conceptos = new BindingList<ValeAlmacenConceptoDetailModel>(allItems.Where(it => it.IdComprobante == vale.IdComprobante).ToList());//Every time it's executed
                vale.Autorizacion = new BindingList<ValeAlmacenStatusModel>(status.Where(it => it.IdComprobante == vale.IdComprobante).ToList());//Every time it's executed
                vale.Relaciones = new BindingList<ValeAlmacenRelacionModel>(relaciones.Where(it => it.IdComprobante == vale.IdComprobante).ToList());//Every time it's executed
            }).Single();

            return response;
        }

        /// <summary>
        /// almacenar un vale de almacen
        /// </summary>
        public IValeAlmacenDetailModel Save(IValeAlmacenDetailModel item) {
            bool isInsert = false;
            if (item.IdComprobante == 0) {
                item.Folio = this.Folio(item as ValeAlmacenDetailModel);
                item.IdDocumento = this.CreateGuid(new string[] { item.Folio.ToString(), item.Serie, item.FechaEmision.ToShortDateString(), item.IdDirectorio.ToString(), item.ReceptorRFC, item.Receptor, item.IdComprobante.ToString() });
                item.Periodo = item.FechaEmision.Month;
                item.Ejercicio = item.FechaEmision.Year;
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                var result = this.Db.Insertable(item as ValeAlmacenDetailModel);
                item.IdComprobante = this.Execute(result);
                isInsert = item.IdComprobante > 0;
            } else {
                item.FechaModifica = DateTime.Now;
                item.Modifica = this.User;
                var result = this.Db.Updateable(item as ValeAlmacenDetailModel);
                this.Execute(result);
            }

            if (item.IdComprobante > 0) {
                for (int i = 0; i < item.Conceptos.Count; i++) {
                    if (item.Conceptos[i].IdConcepto == 0) {
                        item.Conceptos[i].IdComprobante = item.IdComprobante;
                        item.Conceptos[i].IdTipoMovimiento = item.IdTipoComprobante;
                        item.Conceptos[i].FechaNuevo = DateTime.Now;
                        item.Conceptos[i].Creo = this.User;
                        //item.Conceptos[i].IdConcepto = this.Db.Insertable<ValeAlmacenConceptoModel>(item.Conceptos[i]).ExecuteReturnIdentity();
                        var insertable = this.Db.Insertable<ValeAlmacenConceptoModel>(item.Conceptos[i]);
                        item.Conceptos[i].IdConcepto = this.Execute(insertable);
                    } else {
                        item.Conceptos[i].Modifica = this.User;
                        item.Conceptos[i].FechaModifica = DateTime.Now;
                        //this.Db.Updateable<ValeAlmacenConceptoModel>(item.Conceptos[i]).ExecuteCommand();
                        var updateable = this.Db.Updateable<ValeAlmacenConceptoModel>(item.Conceptos[i]);
                        this.Execute(updateable);
                    }
                }
            }
            // actualizar existencias
            if (isInsert) {
                if (!this.Existencias(item)) {
                    Console.WriteLine("No se aplicaron");
                }
            }
            return item;
        }

        private bool Existencias(IValeAlmacenDetailModel item) {
            var sqlCommand = @"INSERT INTO CTMDLX (CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID, CTMDLX_CTPRD_ID, CTMDLX_EXT)
SELECT  MVAMP_CTMDL_ID,  -1,  MVAMP_CTPRD_ID, IF (MVAMP_CTEFC_ID = 1, MVAMP_CNTE, (-1*MVAMP_CNTS))
FROM MVAMP
WHERE MVAMP_ALMMP_ID = @INDEX ON DUPLICATE KEY
UPDATE CTMDLX_EXT= CTMDLX_EXT + IF (MVAMP_CTEFC_ID = 1, MVAMP_CNTE, (-1*MVAMP_CNTS))";

            return this.ExecuteTransaction(sqlCommand, new List<SugarParameter>() { new SugarParameter("@INDEX", item.IdComprobante.ToString()) }) > 0;
        }

        /// <summary>
        /// crear tablas del sistema de almacen
        /// </summary>
        /// <returns>verdadero si la tarea se completa correctamente.</returns>
        public bool CrearTablas() {
            try {
                base.Db.CodeFirst.InitTables<ValeAlmacenModel, ValeAlmacenConceptoModel>();
            } catch (Exception ex) {
                var exSugar = ex as SqlSugarException;
                if (exSugar != null)
                    Console.WriteLine("SQL: " + exSugar.Message);
                Console.WriteLine(ex.Message);
            }
            return this.CreateTable();
        }

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo
        /// </summary>
        public int Folio(ValeAlmacenModel objeto) {
            int folio;
            try {
                folio = this.Db.Queryable<ValeAlmacenModel>().Where(it => it.IdTipoComprobante == objeto.IdTipoComprobante).Where(it => it.IdAlmacen == objeto.IdAlmacen)
                    .Select(it => new { maximus = SqlFunc.AggregateMax(it.Folio) }).Single().maximus + 1;
            } catch (SqlSugarException ex) {
                Console.WriteLine("SQL:" + ex.Message);
                folio = 0;
            }

            return folio;
        }

        #region anterior
        //public bool Aplicar(ValeAlmacenDetailModel item) {
        //    string sumar = string.Concat("UPDATE _ctlmdl t2 ",
        //        "JOIN _ctlmdl t1 ON t1._ctlmdl_id = t2._ctlmdl_id ",
        //        "SET t2._ctlmdl_ext = t1._ctlmdl_ext + @cantidad WHERE t1._ctlmdl_id = t2._ctlmdl_id AND t2._ctlmdl_id = @indice");

        //    string restar = string.Concat("UPDATE _ctlmdl t2 ",
        //        "JOIN _ctlmdl t1 ON t1._ctlmdl_id = t2._ctlmdl_id ",
        //        "SET t2._ctlmdl_ext = t1._ctlmdl_ext - @cantidad WHERE t1._ctlmdl_id = t2._ctlmdl_id AND t2._ctlmdl_id = @indice");

        //    string command = "";

        //    foreach (var producto in item.Conceptos) {
        //        if (item.Efecto == MovimientoTipoEnum.Entrada) {
        //            command = sumar;
        //            this.Db.Ado.ExecuteCommand(command, new List<SugarParameter> { new SugarParameter("@cantidad", (decimal)producto.Entrada), new SugarParameter("@indice", producto.IdModelo) });
        //        }
        //    }
        //    return false;
        //}
        #endregion
    }
}
