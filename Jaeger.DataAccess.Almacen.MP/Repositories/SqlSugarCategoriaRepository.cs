﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.MP.Repositories {
    /// <summary>
    /// catalogo de clasificacion de categorias de productos, bienes y servicios (BPS) del almacen de materia prima
    /// </summary>
    public class SqlSugarCategoriaRepository : MySqlSugarContext<CategoriaModel>, ISqlCategoriaRepository {
        public SqlSugarCategoriaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener una categoria por el indice
        /// </summary>
        public new CategoriaModel GetById(int id) {
            throw new NotImplementedException();
        }

        /// <summary>
        /// obtener lista de categorias
        /// </summary>
        public new IEnumerable<CategoriaModel> GetList() {
            return base.GetList()
                .Select((c) => new CategoriaModel {
                    Activo = c.Activo,
                    Creo = c.Creo,
                    Descripcion = c.Descripcion,
                    FechaNuevo = c.FechaNuevo,
                    IdCategoria = c.IdCategoria,
                    IdSubCategoria = c.IdSubCategoria
                }).ToList();
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand =
                @"WITH RECURSIVE CATEGORIA (ID, SUBID, ACTIVO, SECUENCIA, CLASE, DESCRIPCION, NIVEL, CHILDS) AS
                (
                  SELECT CTCAT_ID AS ID, CTCAT_CTCAT_ID AS SUBID, CTCAT_A AS ACTIVO, CTCAT_SEC AS SECUENCIA, CTCAT_NOM AS CLASE, CONCAT(RIGHT(CTCAT_NOM,129), '/') AS DESCRIPCION, 1, 
  	                (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = CTCAT_ID) AS CHILDS
                    FROM CTCAT
                    WHERE CTCAT_ID = 3  -- THE TREE NODE
                  UNION ALL
                  SELECT T.CTCAT_ID, T.CTCAT_CTCAT_ID, (T.CTCAT_A * TP.ACTIVO), T.CTCAT_SEC, T.CTCAT_NOM, LEFT(CONCAT(TP.DESCRIPCION, T.CTCAT_NOM, ' | '),129), TP.NIVEL +1,
	                (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = T.CTCAT_ID) AS CHILDS
                    FROM CATEGORIA AS TP 
	                 JOIN CTCAT AS T ON TP.ID = T.CTCAT_CTCAT_ID 
                )
                SELECT  * FROM CATEGORIA 
                ORDER BY SUBID, SECUENCIA";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        /// <summary>
        /// obtener vista de categorias
        /// </summary>
        public IEnumerable<CategoriaSingle> GetCategorias() {
            return this.GetList<CategoriaSingle>(new List<IConditional>());
        }

        /// <summary>
        /// obtener vista de categorias
        /// </summary>
        public IEnumerable<CategoriaSingle> GetCategorias_Old() {
            return Db.Queryable<CategoriaModel, CategoriaModel, CategoriaModel>((c1, c2, c3) => new JoinQueryInfos(
                JoinType.Inner, c2.IdSubCategoria == c1.IdCategoria,
                JoinType.Inner, c3.IdSubCategoria == c2.IdCategoria
            )).Select((c1, c2, c3) => new CategoriaSingle {
                IdCategoria = c1.IdCategoria,
                IdSubCategoria = c2.IdCategoria,
                IdClase = c3.IdCategoria,
                Tipo = SqlFunc.IF(c1.Descripcion == "" && c2.Descripcion == "").Return(c3.Descripcion).ElseIF(c1.Descripcion == "").Return(c2.Descripcion).End(c1.Descripcion),
                Clase = SqlFunc.IIF(c2.Descripcion == "", c3.Descripcion, c2.Descripcion),
                Descripcion = c3.Descripcion
            }).ToList();
        }

        public IEnumerable<CategoriaMateriaPrimaSingle> GetCatProductos() {
            var categoria = Db.Queryable<CategoriaModel, CategoriaModel, CategoriaModel>((c1, c2, c3) => new JoinQueryInfos(
                JoinType.Inner, c2.IdSubCategoria == c1.IdCategoria,
                JoinType.Inner, c3.IdSubCategoria == c2.IdCategoria
            )).Select((c1, c2, c3) => new CategoriaSingle {
                IdCategoria = c1.IdCategoria,
                IdSubCategoria = c2.IdCategoria,
                IdClase = c3.IdCategoria,
                Tipo = SqlFunc.IF(c1.Descripcion == "" && c2.Descripcion == "").Return(c3.Descripcion).ElseIF(c1.Descripcion == "").Return(c2.Descripcion).End(c1.Descripcion),
                Clase = SqlFunc.IIF(c2.Descripcion == "", c3.Descripcion, c2.Descripcion),
                Descripcion = c3.Descripcion
            });

            //var producto1 = Db.Queryable<ProductoServicioModel>().ToList();
            var producto = Db.Queryable<ProductoServicioModel>();
            var list = Db.Queryable(categoria, producto, JoinType.Right, (c3, p1) => c3.IdClase == p1.IdCategoria)
                .Select((c3, p1) => new CategoriaMateriaPrimaSingle {
                    IdSubCategoria = c3.IdSubCategoria,
                    IdCategoria = c3.IdClase,
                    IdProducto = p1.IdProducto,
                    Tipo = c3.Tipo,
                    Clase = c3.Clase,
                    Producto = p1.Nombre,
                    Descripcion = c3.Descripcion
                    //Descripcion = SqlFunc.IIF(p1.Nombre == "", c3.Descripcion, SqlFunc.MergeString(c3.Descripcion, "-", p1.Nombre))
                }).ToList();
            return list;
        }
    }
}
