﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.MP.Contracts;

namespace Jaeger.DataAccess.Almacen.MP.Repositories {
    public class SqlSugarValeRelacionRepository : MySqlSugarContext<ValeAlmacenRelacionModel>, ISqlValeRelacionRepository {
        public SqlSugarValeRelacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ValeAlmacenSingleModel)) {
                var sql = @"SELECT * FROM ALMMPR @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        public IValeAlmacenRelacionModel Save(IValeAlmacenRelacionModel item) {
            item.FechaNuevo = DateTime.Now;
            item.Creo = this.User;
            var sql = @"INSERT INTO ALMMPR ( ALMMPR_ALMMP_ID, ALMMPR_DOC_ID, ALMMPR_CTREL_ID, ALMMPR_CTREL, ALMMPR_UUID, ALMMPR_DRCTR_ID, ALMMPR_FOLIO, ALMMPR_SERIE, ALMMPR_CLV, ALMMPR_RFCR, ALMMPR_NOM, ALMMPR_USR_N, ALMMPR_FN, ALMMPR_FECEMS) 
                                    VALUES (@ALMMPR_ALMMP_ID,@ALMMPR_DOC_ID,@ALMMPR_CTREL_ID,@ALMMPR_CTREL,@ALMMPR_UUID,@ALMMPR_DRCTR_ID,@ALMMPR_FOLIO,@ALMMPR_SERIE,@ALMMPR_CLV,@ALMMPR_RFCR,@ALMMPR_NOM,@ALMMPR_USR_N,@ALMMPR_FN,@ALMMPR_FECEMS)
                       ON DUPLICATE KEY UPDATE  
                                     ALMMPR_ALMMP_ID=@ALMMPR_ALMMP_ID, ALMMPR_DOC_ID=@ALMMPR_DOC_ID, ALMMPR_CTREL_ID=@ALMMPR_CTREL_ID, ALMMPR_CTREL=@ALMMPR_CTREL, ALMMPR_UUID=@ALMMPR_UUID, ALMMPR_DRCTR_ID=@ALMMPR_DRCTR_ID, 
                                              ALMMPR_FOLIO=@ALMMPR_FOLIO, ALMMPR_SERIE=@ALMMPR_SERIE, ALMMPR_CLV=@ALMMPR_CLV, ALMMPR_RFCR=@ALMMPR_RFCR, ALMMPR_NOM=@ALMMPR_NOM, ALMMPR_USR_N=@ALMMPR_USR_N, ALMMPR_FN=@ALMMPR_FN;";
            var parametros = new List<SugarParameter> {
                new SugarParameter("@ALMMPR_ALMMP_ID", item.IdComprobante),
                new SugarParameter("@ALMMPR_DOC_ID", item.IdTipoComprobante),
                new SugarParameter("@ALMMPR_CTREL_ID", item.IdClaveRelacion),
                new SugarParameter("@ALMMPR_CTREL", item.ClaveRelacion),
                new SugarParameter("@ALMMPR_UUID", item.IdDocumento),
                new SugarParameter("@ALMMPR_DRCTR_ID", item.IdDirectorio),
                new SugarParameter("@ALMMPR_FOLIO", item.Folio),
                new SugarParameter("@ALMMPR_SERIE", item.Serie),
                new SugarParameter("@ALMMPR_CLV", item.Clave),
                new SugarParameter("@ALMMPR_RFCR", item.ReceptorRFC),
                new SugarParameter("@ALMMPR_NOM", item.Receptor),
                new SugarParameter("@ALMMPR_USR_N", item.Creo),
                new SugarParameter("@ALMMPR_FN", item.FechaNuevo),
                new SugarParameter("@ALMMPR_FECEMS", item.FechaEmision)
            };
            var d0 = this.Db.Ado.ExecuteCommand(sql, parametros) > 0;
            return item;
        }
    }
}
