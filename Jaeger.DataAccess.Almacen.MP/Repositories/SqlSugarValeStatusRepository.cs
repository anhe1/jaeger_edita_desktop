﻿using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.MP.Repositories {
    public class SqlSugarValeStatusRepository : MySqlSugarContext<ValeAlmacenStatusModel>, ISqlValeStatusRepository {
        public SqlSugarValeStatusRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ValeAlmacenStatusModel)) {
                var sql = @"SELECT * FROM ALMMPS @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        public IValeAlmacenStatusModel Save(IValeAlmacenStatusModel item) {
            var sql = @"INSERT INTO ALMMPS ( ALMMPS_ALMMP_ID, ALMMPS_STTS_ID, ALMMPS_STTSB_ID, ALMMPS_CTMTV_ID, ALMMPS_DRCTR_ID, ALMMPS_CTMTV, ALMMPS_NOTA, ALMMPS_USR_N, ALMMPS_FN) 
                                    VALUES (@ALMMPS_ALMMP_ID,@ALMMPS_STTS_ID,@ALMMPS_STTSB_ID,@ALMMPS_CTMTV_ID,@ALMMPS_DRCTR_ID,@ALMMPS_CTMTV,@ALMMPS_NOTA,@ALMMPS_USR_N,@ALMMPS_FN) 
                    ON DUPLICATE KEY UPDATE ALMMPS_ALMMP_ID = @ALMMPS_ALMMP_ID, ALMMPS_STTS_ID = @ALMMPS_STTS_ID, ALMMPS_STTSB_ID = @ALMMPS_STTSB_ID, ALMMPS_CTMTV_ID = @ALMMPS_CTMTV_ID,
ALMMPS_DRCTR_ID = @ALMMPS_DRCTR_ID, ALMMPS_CTMTV = @ALMMPS_CTMTV, ALMMPS_NOTA = @ALMMPS_NOTA, ALMMPS_USR_N = @ALMMPS_USR_N, ALMMPS_FN = @ALMMPS_FN;";

            var parametros = new List<SugarParameter> {
                new SugarParameter("@ALMMPS_ALMMP_ID", item.IdComprobante),
                new SugarParameter("@ALMMPS_STTS_ID", item.IdStatus),
                new SugarParameter("@ALMMPS_STTSB_ID", item.IdStatusB),
                new SugarParameter("@ALMMPS_CTMTV_ID", item.IdCveMotivo),
                new SugarParameter("@ALMMPS_DRCTR_ID", item.IdDirectorio),
                new SugarParameter("@ALMMPS_CTMTV", item.CveMotivo),
                new SugarParameter("@ALMMPS_NOTA", item.Nota),
                new SugarParameter("@ALMMPS_USR_N", item.Creo),
                new SugarParameter("@ALMMPS_FN", item.FechaNuevo)
            };
            var d0 = this.Db.Ado.ExecuteCommand(sql, parametros) > 0;
            return item;
        }
    }
}
