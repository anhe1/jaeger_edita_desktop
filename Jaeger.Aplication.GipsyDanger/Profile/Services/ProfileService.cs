﻿using System.Collections.Generic;
using System.Linq;

namespace Jaeger.Aplication.EditaCP.Profile.Services {
    /// <summary>
    /// servicio de perfiles de usuario
    /// </summary>
    public class ProfileService : Kaiju.Services.ProfileService, Kaiju.Contracts.IProfileToService {
        /// <summary>
        /// constructor
        /// </summary>
        public ProfileService() : base() { }

        /// <summary>
        /// Menus
        /// </summary>
        /// <returns>List</returns>
        public override List<Domain.Base.Abstractions.UIMenuElement> GetMenus() {
            return _MenuRepository.GetMenus().ToList();
        }
    }
}
