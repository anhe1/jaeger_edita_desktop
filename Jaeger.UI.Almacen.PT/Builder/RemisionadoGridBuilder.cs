﻿using System;
using System.Drawing;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.PT.Builder {
    public class RemisionadoGridBuilder : GridViewBuilder, IGridViewBuilder, IRemisionadoGridBuilder, IRemisionadoTempletesGridBuilder, IRemisionadoColumnsGridBuilder {

        public RemisionadoGridBuilder() {
            this._Columns = new List<GridViewDataColumn>();
        }

        public IRemisionadoColumnsGridBuilder Columns() {
            return this;
        }

        #region templetes
        public override IGridViewTempleteBuild Set(GridViewTemplate template) {
            return base.Set(template);
        }
        public IRemisionadoTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public IRemisionadoTempletesGridBuilder Templetes(GridViewTemplate template) {
            this._Columns.Clear();
            return this;
        }

        public IRemisionadoTempletesGridBuilder Remisionado() {
            this._Columns.Clear();
            this.AddFolio().AddIdPedido().AddStatus().AddReceptor().AddClave().AddContacto().AddFechaEmision().AddFechaEntrega().AddFechaVencimiento().AddFechaCobranza().AddFechaUltimoPago().AddCancela().AddFechaCancela()
                .AddSubTotal().AddTotalDescuento().AddImporte().AddTotalTrasladoIVA().AddTotal().AddTasaIVAPactado().AddTotalIVAPactado().AddGranTotal().AddFactorPactado().AddPorCobrarPactado().AddDescuentoTipo()
                .AddDescuentoFactor().AddDescuentoTotal().AddPorCobrar().AddAcumulado().Saldo().AddClaveMoneda().AddDiasTranscurridos().AddDiasUltCobro().AddDiasCobranza().AddVendedor().AddFilePDF().Guia().AddNota().AddCreo().AddFechaNuevo()
                .AddModifica().AddFechaModifica();
            return this;
        }

        /// <summary>
        /// vista de remisiones simple
        /// </summary>
        public IRemisionadoTempletesGridBuilder Simple() {
            this._Columns.Clear();
            this.AddFolio().AddIdPedido().AddStatus().AddReceptor().AddFechaEmision().AddFechaEntrega().AddFechaCobranza().AddFechaUltimoPago().AddCancela().AddFechaCancela().AddSubTotal()
                .AddTotalDescuento().AddImporte().AddTotalTrasladoIVA().AddTotal().AddPorCobrar().AddAcumulado().Saldo().AddClaveMoneda().AddVendedor().AddFilePDF().AddCreo().AddFechaNuevo().AddIdDocumento();
            return this;
        }

        public IRemisionadoTempletesGridBuilder PorCliente() {
            this._Columns.Clear();
            this.AddFolio().AddStatus().AddContacto().AddFechaEmision().AddFechaEntrega().AddFechaVencimiento().AddFechaCobranza().AddFechaUltimoPago().AddCancela().AddFechaCancela()
                .AddSubTotal().AddTotalDescuento().AddImporte().AddTotalTrasladoIVA().AddTotal().AddTasaIVAPactado().AddTotalIVAPactado().AddGranTotal().AddFactorPactado().AddPorCobrarPactado().AddDescuentoTipo()
                .AddDescuentoFactor().AddDescuentoTotal().AddPorCobrar().AddAcumulado().AddClaveMoneda().AddDiasTranscurridos().AddDiasUltCobro().AddDiasCobranza().AddVendedor().AddFilePDF().AddNota().AddCreo().AddFechaNuevo()
                .AddModifica().AddFechaModifica();
            return this;
        }

        public IRemisionadoTempletesGridBuilder RemisionadoCP() {
            this._Columns.Clear();
            this.AddFolio().AddStatus().AddReceptor().AddClave().AddContacto().AddFechaEmision().AddFechaEntrega().AddFechaVencimiento().AddFechaCobranza().AddFechaUltimoPago().AddCancela().AddFechaCancela()
                .AddSubTotal().AddTotalDescuento().AddImporte().AddTotalTrasladoIVA().AddTotal().AddTasaIVAPactado().AddTotalIVAPactado().AddGranTotal().AddPorCobrarPactado()
                .AddPorCobrar().AddAcumulado().AddClaveMoneda().AddDiasTranscurridos().AddDiasUltCobro().AddDiasCobranza().AddVendedor().AddFilePDF().AddNota().AddCreo().AddFechaNuevo().AddModifica().AddFechaModifica();
            return this;
        }

        /// <summary>
        /// conceptos de la remision con importes
        /// </summary>
        public IRemisionadoTempletesGridBuilder Conceptos(bool showValues = false) {
            this._Columns.Clear();
            if (showValues) {
                this.AddCantidad().AddCboUnidad().AddIdPedido().AddProducto().AddIdentificador().AddUnitario().AddSubTotal().AddDescuento().AddImporte().AddTasaIVA().AddTrasladoIVA().AddTotal();
            } else {
                this.AddCantidad().AddCboUnidad().AddIdPedido().AddProducto().AddIdentificador();
            }
            return this;
        }

        /// <summary>
        /// conceptos de la remision con importes
        /// </summary>
        public IRemisionadoTempletesGridBuilder Concepto() {
            this._Columns.Clear();
            this.AddCantidad().AddCboUnidad().AddIdPedido().AddProducto().AddIdentificador().AddUnitario().AddSubTotal().AddDescuento().AddImporte().AddTasaIVA().AddTrasladoIVA().AddTotal();
            return this;
        }

        public IRemisionadoTempletesGridBuilder ConceptoParte() {
            this.AddIdentificador().AddCantidad().AddCboUnidad().AddProducto().AddUnitario().AddImporte();
            return this;
        }

        /// <summary>
        /// templete de remisionado sin valores
        /// </summary>
        public IRemisionadoTempletesGridBuilder RemisionadoSinValores() {
            this.AddFolio().AddStatus().AddReceptor().AddClave().AddContacto().AddFechaEmision().AddFechaEntrega().AddFechaCobranza().AddCancela().AddFechaCancela().Guia(true).AddFilePDF()
                .AddVendedor().AddCreo().AddFechaNuevo().AddModifica().AddFechaModifica();
            return this;
        }

        /// <summary>
        /// templete de conceptos sin valores
        /// </summary>
        public IRemisionadoTempletesGridBuilder ConceptosSinValores() {
            this.AddCantidad().AddCboUnidad().AddIdPedido().AddProducto().AddIdentificador();
            return this;
        }

        /// <summary>
        /// vista para remisiones y conceptos sin valores
        /// </summary>
        public IRemisionadoTempletesGridBuilder RemisionYConceptosSinValores() {
            this.AddFolio().AddStatus().AddReceptor().AddClave().AddFechaEmision().AddCantidad().AddCboUnidad().AddIdPedido().AddProducto().AddIdentificador().AddVendedor().AddIdDocumento().AddFilePDF()
                .AddCreo().AddFechaNuevo().AddModifica().AddFechaModifica();
            return this;
        }

        /// <summary>
        /// vista para remisiones y conceptos con importes
        /// </summary>
        public IRemisionadoTempletesGridBuilder RemisionYConceptosConImportes() {
            this.AddFolio().AddStatus().AddReceptor().AddClave().AddFechaEmision().AddCantidad().AddCboUnidad().AddIdPedido().AddProducto().AddIdentificador().AddUnitario().AddSubTotal().AddDescuento().AddImporte()
                .AddTasaIVA().AddTrasladoIVA().AddTotal().AddVendedor().AddIdDocumento().AddFilePDF()
                .AddCreo().AddFechaNuevo().AddModifica().AddFechaModifica();
            return this;
        }

        public IRemisionadoTempletesGridBuilder Autorizacion() {
            this.AddStatus().AddFechaAutoriza().AddUsuarioAutoriza().AddClaveMotivo().AddNota();
            return this;
        }

        public IRemisionadoTempletesGridBuilder Search() {
            this.AddFolio().AddStatus().AddReceptor().AddFechaEmision().AddGranTotal();
            return this;
        }

        /// <summary>
        /// templete para vista de remisiones relacionadas
        /// </summary>
        public IRemisionadoTempletesGridBuilder Relacion() {
            this.AddFolio().AddSerie().AddReceptor().AddFechaEmision().AddIdDocumento().AddGranTotal();
            return this;
        }
        #endregion

        #region remision
        public IRemisionadoColumnsGridBuilder AddFolio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddSerie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// combo box para columna de status
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddStatus() {
            var combo = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75,
                DataSource = Aplication.Almacen.PT.Services.RemisionService.GetStatus(),
                DisplayMember = "Descriptor",
                ValueMember = "Id",
                AllowResize = true
            };
            this._Columns.Add(combo);
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddFechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEmision",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddReceptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ReceptorNombre",
                HeaderText = "Cliente",
                Name = "ReceptorNombre",
                Width = 220,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddClave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ReceptorClave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 65
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddContacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Contacto",
                HeaderText = "Recibe",
                Name = "ClienteRecibe",
                Width = 150,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }

        /// <summary>
        /// columna de fecha de cobranza
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddFechaCobranza() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaCobranza",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Cobranza",
                Name = "FechaCobranza",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// columna fecha de entrega
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddFechaEntrega() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEntrega",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Entrega",
                Name = "FechaEntrega",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// columa fehca de ultimo pago de la remision
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddFechaUltimoPago() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaUltPago",
                FormatString = this.FormatStringDate,
                HeaderText = "Últ. Cobro",
                Name = "FechaUltPago",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// columna de fecha de vencimineto del pagare
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddFechaVencimiento() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaVence",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Vence",
                Name = "FechaVence",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// columna del importe total del descuento 
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddTotalDescuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalDescuento",
                FormatString = this.FormatStringMoney,
                HeaderText = "Descuento",
                Name = "TotalDescuento",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// STotal - Desc.
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddImporte() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = this.FormatStringMoney,
                HeaderText = "SubTotal \r\n- Desc.",
                Name = "Importe",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddSubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = this.FormatStringMoney,
                HeaderText = "SubTotal",
                Name = "SubTotal",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotalTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalTrasladoIVA",
                FormatString = this.FormatStringMoney,
                HeaderText = "Traslado \r\n IVA",
                Name = "TotalTrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotalRetencionISR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalRetencionISR",
                FormatString = this.FormatStringMoney,
                HeaderText = "Ret.\r\n ISR",
                Name = "TotalRetencionISR",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotalRetencionIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalRetencionIVA",
                FormatString = this.FormatStringMoney,
                HeaderText = "Ret.\r\n IVA",
                Name = "TotalRetencionIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotalRetencionIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalRetencionIEPS",
                FormatString = this.FormatStringMoney,
                HeaderText = "Ret.\r\n IEPS",
                Name = "TotalRetencionIEPS",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotalTrasladoIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalTrasladoIEPS",
                FormatString = this.FormatStringMoney,
                HeaderText = "Traslado\r\n IEPS",
                Name = "TotalTrasladoIEPS",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringMoney,
                FieldName = "Total",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddGranTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringMoney,
                FieldName = "GTotal",
                HeaderText = "GTotal",
                Name = "GTotal",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddClaveMoneda() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveMoneda",
                HeaderText = "Moneda",
                Name = "ClaveMoneda",
                IsVisible = false
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDiasTranscurridos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "DiasTranscurridos",
                FormatString = "{0:N0}",
                HeaderText = "Días Trans.",
                Name = "DiasTranscurridos",
                TextAlignment = ContentAlignment.MiddleCenter,
                WrapText = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDiasUltCobro() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "DiasUltCobro",
                FormatString = "{0:N0}",
                HeaderText = "Días Trans.",
                Name = "DiasUltCobro",
                TextAlignment = ContentAlignment.MiddleCenter,
                WrapText = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDiasCobranza() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "DiasCobranza",
                FormatString = "{0:N0}",
                HeaderText = "Días Cobranza",
                Name = "DiasCobranza",
                TextAlignment = ContentAlignment.MiddleCenter,
                WrapText = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddVendedor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Vendedor",
                HeaderText = "Vendedor",
                Name = "Vendedor",
                ReadOnly = true,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddFilePDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "UrlFilePDF",
                HeaderText = "PDF",
                Name = "UrlFilePDF",
                Width = 30,
                ReadOnly = true,
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddCancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cancela",
                HeaderText = "Usu. Cancela",
                IsVisible = false,
                Name = "Cancela",
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddFechaCancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaCancela",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTasaIVAPactado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TasaIVAPactado",
                FormatString = this.FormatStringP,
                HeaderText = "% IVA \r\nPact.",
                Name = "TasaIVAPactado",
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTotalIVAPactado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalIVAPactado",
                FormatString = this.FormatStringMoney,
                HeaderText = "IVA Pact.",
                Name = "TotalIVAPactado",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddFactorPactado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "FactorPactado",
                FormatString = "{0:N4}",
                HeaderText = "Fac. \r\nPactado",
                Name = "FactorPactado",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddPorCobrarPactado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "PorCobrarPactado",
                FormatString = this.FormatStringMoney,
                HeaderText = "X Cobrar Pactado",
                Name = "PorCobrarPactadoD",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDescuentoTipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "DescuentoTipo",
                HeaderText = "Tipo Desc. Aplicado",
                Name = "DescuentoTipo",
                ReadOnly = true,
                Width = 120,
                WrapText = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDescuentoFactor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "DescuentoFactor",
                FormatString = "{0:N4}",
                HeaderText = "FAC Desc.",
                Name = "DescuentoFactor",
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDescuentoTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "DescuentoTotal",
                FormatString = this.FormatStringMoney,
                HeaderText = "Desc. Actual",
                Name = "DescuentoTotal",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                WrapText = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddPorCobrar() {
            this._Columns.Add(new GridViewTextBoxColumn {
                TextAlignment = ContentAlignment.MiddleRight,
                DataType = typeof(decimal),
                FieldName = "PorCobrar",
                FormatString = this.FormatStringMoney,
                HeaderText = "Por Cobrar",
                Name = "PorCobrar",
                Width = 80
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddAcumulado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Acumulado",
                FormatString = this.FormatStringMoney,
                HeaderText = "Cobrado",
                Width = 80,
                Name = "Acumulado",
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder Saldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Saldo",
                FormatString = this.FormatStringMoney,
                HeaderText = "Saldo",
                Width = 80,
                Name = "Saldo",
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddIdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                Name = "IdDocumento",
                ReadOnly = true,
                Width = 180
            });
            return this;
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public new IRemisionadoColumnsGridBuilder AddFechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener columna de fecha de modificacion 
        /// </summary>
        public new IRemisionadoColumnsGridBuilder AddFechaModifica() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Mod.",
                Name = "FechaModifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public new IRemisionadoColumnsGridBuilder AddCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener columna de usuario que modifica
        /// </summary>
        public new IRemisionadoColumnsGridBuilder AddModifica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = false,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddUrlFilePDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "UrlFilePDF",
                HeaderText = "PDF",
                Name = "UrlFilePDF",
                Width = 30,
                ReadOnly = true,
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddNota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                Width = 150,
                ReadOnly = true,
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder Guia(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "NoGuia",
                HeaderText = "NoGuia",
                Name = "NoGuia",
                Width = 95,
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = isVisible
            });
            return this;
        }

        #endregion

        #region conceptos
        /// <summary>
        /// Columna para cantidad (Cantidad)
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddCantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = this.FormatStringMoney,
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddCboUnidad() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                ReadOnly = false,
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddIdPedido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdPedido",
                DataType = typeof(int),
                HeaderText = "# Pedido",
                Name = "IdPedido",
                TextAlignment = ContentAlignment.MiddleCenter,
                MaxLength = 5,
                Width = 65
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripcion",
                Name = "Descripcion",
                Width = 510,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddIdentificador() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Identificador",
                HeaderText = "Identificador",
                Name = "Identificador",
                Width = 100,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddUnitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Unitario",
                FormatString = "{0:N2}",
                HeaderText = "P. Unitario",
                Name = "Unitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddDescuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:N2}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTasaIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TasaIVA",
                FormatString = "{0:P0}",
                HeaderText = "% IVA",
                Name = "TasaIVA",
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = "{0:N2}",
                HeaderText = "T. IVA",
                Name = "TrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }
        #endregion

        #region autorizaciones
        /// <summary>
        /// fecha de autorizacion
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddFechaAutoriza() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "Fecha",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha",
                Name = "Fecha",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// usuario autoriza
        /// </summary>
        public IRemisionadoColumnsGridBuilder AddUsuarioAutoriza() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Usuario",
                ReadOnly = true,
                HeaderText = "Usuario",
                DataType = typeof(string),
                Name = "Usuario",
                Width = 75
            });
            return this;
        }

        public IRemisionadoColumnsGridBuilder AddClaveMotivo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CvMotivo",
                ReadOnly = true,
                HeaderText = "Clave",
                DataType = typeof(string),
                Name = "CvMotivo",
                Width = 175
            });
            return this;
        }
        #endregion

        #region metodos estaticos
        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, RemisionSingleModel item) {
            row.Cells["Folio"].Value = item.Folio;
            row.Cells["IdStatus"].Value = item.IdStatus;
            row.Cells["IdPedido"].Value = item.IdPedido;
            row.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
            row.Cells["FechaEmision"].Value = item.FechaEmision;
            row.Cells["FechaEntrega"].Value = item.FechaEntrega;
            row.Cells["FechaCobranza"].Value = item.FechaCobranza;
            row.Cells["FechaUltPago"].Value = item.FechaUltPago;
            row.Cells["Cancela"].Value = item.Cancela;
            row.Cells["FechaCancela"].Value = item.FechaCancela;
            row.Cells["SubTotal"].Value = item.SubTotal;
            row.Cells["TotalDescuento"].Value = item.TotalDescuento;
            row.Cells["Importe"].Value = item.Importe;
            row.Cells["TotalTrasladoIVA"].Value = item.TotalTrasladoIVA;
            row.Cells["Total"].Value = item.Total;
            row.Cells["PorCobrar"].Value = item.PorCobrar;
            row.Cells["Acumulado"].Value = item.Acumulado;
            row.Cells["Saldo"].Value = item.Saldo;
            row.Cells["ClaveMoneda"].Value = item.ClaveMoneda;
            row.Cells["Vendedor"].Value = item.Vendedor;
            row.Cells["UrlFilePDF"].Value = item.UrlFilePDF;
            row.Cells["Creo"].Value = item.Creo;
            row.Cells["FechaNuevo"].Value = item.FechaNuevo;
            row.Cells["IdDocumento"].Value = item.IdDocumento;
            return row;
        }

        /// <summary>
        /// para vista de partidas, incluye importes
        /// </summary>
        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, RemisionPartidaModel item) {
            row.Cells["Folio"].Value = item.Folio;
            row.Cells["IdStatus"].Value = item.IdStatus;
            row.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
            row.Cells["Clave"].Value = item.ReceptorClave;
            row.Cells["FechaEmision"].Value = item.FechaEmision;
            row.Cells["Cantidad"].Value = item.Cantidad;
            row.Cells["Unidad"].Value = item.Unidad;
            row.Cells["IdPedido"].Value = item.IdPedido;
            row.Cells["Descripcion"].Value = item.Descripcion;
            row.Cells["Identificador"].Value = item.Identificador;
            row.Cells["Unitario"].Value = item.Unitario;
            row.Cells["Descuento"].Value = item.Descuento;
            row.Cells["Importe"].Value = item.Importe;
            row.Cells["SubTotal"].Value = item.SubTotal;
            row.Cells["TasaIVA"].Value = item.TasaIVA;
            row.Cells["TrasladoIVA"].Value = item.TrasladoIVA;
            row.Cells["Total"].Value = item.Total;
            row.Cells["Vendedor"].Value = item.IdVendedor;
            row.Cells["IdDocumento"].Value = item.IdDocumento;
            row.Cells["UrlFilePDF"].Value = item.UrlFilePDF;
            row.Cells["Creo"].Value = item.Creo;
            row.Cells["FechaNuevo"].Value = item.FechaNuevo;
            row.Cells["Modifica"].Value = item.Modifica;
            row.Cells["FechaModifica"].Value = item.FechaModifica;
            return row;
        }

        /// <summary>
        /// para vista de partidas, no incluye importes
        /// </summary>
        public static GridViewRowInfo ConvertToSinImportes(GridViewRowInfo row, RemisionPartidaModel item) {
            row.Cells["Folio"].Value = item.Folio;
            row.Cells["IdStatus"].Value = item.IdStatus;
            row.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
            row.Cells["Clave"].Value = item.ReceptorClave;
            row.Cells["FechaEmision"].Value = item.FechaEmision;
            row.Cells["Cantidad"].Value = item.Cantidad;
            row.Cells["Unidad"].Value = item.Unidad;
            row.Cells["IdPedido"].Value = item.IdPedido;
            row.Cells["Descripcion"].Value = item.Descripcion;
            row.Cells["Identificador"].Value = item.Identificador;
            row.Cells["Vendedor"].Value = item.IdVendedor;
            row.Cells["IdDocumento"].Value = item.IdDocumento;
            row.Cells["UrlFilePDF"].Value = item.UrlFilePDF;
            row.Cells["Creo"].Value = item.Creo;
            row.Cells["FechaNuevo"].Value = item.FechaNuevo;
            
            return row;
        }

        /// <summary>
        /// para vista simple sin valores
        /// </summary>
        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, RemisionConceptoDetailModel item) {
            row.Cells["Cantidad"].Value = item.Cantidad;
            row.Cells["IdPedido"].Value = item.IdPedido;
            row.Cells["Unidad"].Value = item.Unidad;
            row.Cells["Descripcion"].Value = item.Descripcion;
            row.Cells["Identificador"].Value = item.Identificador;
            return row;
        }

        /// <summary>
        /// para la vista de conceptos con valores
        /// </summary>
        public static GridViewRowInfo ConvertToImportes(GridViewRowInfo row, RemisionConceptoDetailModel item) {
            row.Cells["Cantidad"].Value = item.Cantidad;
            row.Cells["IdPedido"].Value = item.IdPedido;
            row.Cells["Unidad"].Value = item.Unidad;
            row.Cells["Descripcion"].Value = item.Descripcion;
            row.Cells["Identificador"].Value = item.Identificador;
            row.Cells["Unitario"].Value = item.Unitario;
            row.Cells["Descuento"].Value = item.Descuento;
            row.Cells["Importe"].Value = item.Importe;
            row.Cells["SubTotal"].Value = item.SubTotal;
            row.Cells["TasaIVA"].Value = item.TasaIVA;
            row.Cells["TrasladoIVA"].Value = item.TrasladoIVA;
            row.Cells["Total"].Value = item.Total;
            return row;
        }

        /// <summary>
        /// para la vista del templete de control de status
        /// </summary>
        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, RemisionStatusModel item) {
            row.Cells["IdStatus"].Value = item.IdStatusB;
            row.Cells["Usuario"].Value = item.User;
            row.Cells["CvMotivo"].Value = item.CvMotivo;
            row.Cells["Nota"].Value = item.Nota;
            row.Cells["Fecha"].Value = item.FechaNuevo;
            return row;
        }

        public static ExpressionFormattingObject Estado() {
            return new ExpressionFormattingObject() {
                Name = "Cancelado",
                ApplyOnSelectedRows = true,
                Expression = "IdStatus = 0",
                RowForeColor = Color.DarkGray,
                ApplyToRow = true,
            };
        }

        /// <summary>
        /// Item Crear Remision de Producto Terminado
        /// </summary>
        /// <returns>RadMenuItem</returns>
        public static RadMenuItem RemisionCliente() {
            return new RadMenuItem { Text = "Crear remisión cliente", Name = "cpvnt_brem_historial" };
        }
        #endregion
    }
}
