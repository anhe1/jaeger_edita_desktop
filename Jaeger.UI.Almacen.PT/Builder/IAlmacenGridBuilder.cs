﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.PT.Builder {
    public interface IAlmacenGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IAlmacenGridTempleteBuilder Templetes();
    }

    public interface IAlmacenGridTempleteBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IAlmacenGridTempleteBuilder Master();

        IAlmacenGridTempleteBuilder Conceptos();

        IAlmacenGridTempleteBuilder Relacion();

        IAlmacenGridTempleteBuilder CStatus();

        IAlmacenGridTempleteBuilder Search();
    }

    public interface IAlmacenGridColumnsBuilder : IGridViewColumnsBuild {
        #region vales
        IAlmacenGridColumnsBuilder IdTipoComprobante();

        IAlmacenGridColumnsBuilder CheckActivo();

        IAlmacenGridColumnsBuilder SubId();

        IAlmacenGridColumnsBuilder TipoComprobante();

        IAlmacenGridColumnsBuilder Status();

        /// <summary>
        /// status del vales de almacen 
        /// </summary>
        IAlmacenGridColumnsBuilder ComboStatus();

        IAlmacenGridColumnsBuilder Folio();

        IAlmacenGridColumnsBuilder Serie();

        IAlmacenGridColumnsBuilder NoOrden();

        IAlmacenGridColumnsBuilder Receptor();

        IAlmacenGridColumnsBuilder ReceptorRFC();

        IAlmacenGridColumnsBuilder FechaEmision();

        IAlmacenGridColumnsBuilder FechaEntrega();

        IAlmacenGridColumnsBuilder FechaCancela();

        IAlmacenGridColumnsBuilder Cancela();

        IAlmacenGridColumnsBuilder ClaveCancelacion();

        IAlmacenGridColumnsBuilder Contacto();

        IAlmacenGridColumnsBuilder Referencia();

        IAlmacenGridColumnsBuilder Nota();

        IAlmacenGridColumnsBuilder Creo();

        IAlmacenGridColumnsBuilder IdDocumento();

        IAlmacenGridColumnsBuilder PDF();

        IAlmacenGridColumnsBuilder Total();
        IAlmacenGridColumnsBuilder Modifica();
        IAlmacenGridColumnsBuilder FechaNuevo();
        IAlmacenGridColumnsBuilder FechaModifica();
        #endregion

        #region conceptos
        IAlmacenGridColumnsBuilder IdConcepto();
        IAlmacenGridColumnsBuilder IdUnidad();
        IAlmacenGridColumnsBuilder IdProducto();
        IAlmacenGridColumnsBuilder IdModelo();
        IAlmacenGridColumnsBuilder OrdenCompra();
        IAlmacenGridColumnsBuilder OrdenProduccion();
        IAlmacenGridColumnsBuilder Cantidad();
        IAlmacenGridColumnsBuilder Entrada();
        IAlmacenGridColumnsBuilder Salida();
        IAlmacenGridColumnsBuilder Unidad();
        IAlmacenGridColumnsBuilder Descripcion();
        IAlmacenGridColumnsBuilder Especificacion();
        IAlmacenGridColumnsBuilder Marca();
        IAlmacenGridColumnsBuilder NoIdentificacion();
        #endregion

        IAlmacenGridColumnsBuilder CveMotivo();
    }
}
