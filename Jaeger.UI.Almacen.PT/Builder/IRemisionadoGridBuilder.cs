﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.PT.Builder {
    public interface IRemisionadoGridBuilder : IGridViewBuilder, IDisposable {
        IRemisionadoTempletesGridBuilder Templetes();
        IRemisionadoColumnsGridBuilder Columns();
    }

    public interface IRemisionadoTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        /// <summary>
        /// vista de remisiones simple
        /// </summary>
        IRemisionadoTempletesGridBuilder Simple();

        IRemisionadoTempletesGridBuilder PorCliente();

        IRemisionadoTempletesGridBuilder Remisionado();

        IRemisionadoTempletesGridBuilder RemisionadoCP();

        /// <summary>
        /// conceptos de la remision con importes
        /// </summary>
        IRemisionadoTempletesGridBuilder Concepto();

        IRemisionadoTempletesGridBuilder ConceptoParte();

        IRemisionadoTempletesGridBuilder Conceptos(bool showValues = false);

        /// <summary>
        /// templete de remisionado sin valores
        /// </summary>
        /// <returns></returns>
        IRemisionadoTempletesGridBuilder RemisionadoSinValores();

        /// <summary>
        /// templete de conceptos sin valores
        /// </summary>
        IRemisionadoTempletesGridBuilder ConceptosSinValores();

        /// <summary>
        /// vista para remisiones y conceptos sin valores
        /// </summary>
        IRemisionadoTempletesGridBuilder RemisionYConceptosSinValores();

        /// <summary>
        /// vista para remisiones y conceptos con importes
        /// </summary>
        IRemisionadoTempletesGridBuilder RemisionYConceptosConImportes();

        IRemisionadoTempletesGridBuilder Autorizacion();

        IRemisionadoTempletesGridBuilder Search();

        IRemisionadoTempletesGridBuilder Relacion();
    }

    public interface IRemisionadoColumnsGridBuilder : IGridViewColumnsBuild {
        #region remision
        IRemisionadoColumnsGridBuilder AddIdDocumento();
        IRemisionadoColumnsGridBuilder AddFolio();
        IRemisionadoColumnsGridBuilder AddSerie();
        /// <summary>
        /// combo box para columna de status
        /// </summary>
        IRemisionadoColumnsGridBuilder AddStatus();
        IRemisionadoColumnsGridBuilder AddFechaEmision();
        IRemisionadoColumnsGridBuilder AddReceptor();
        IRemisionadoColumnsGridBuilder AddClave();
        IRemisionadoColumnsGridBuilder AddContacto();
        /// <summary>
        /// columna de fecha de cobranza
        /// </summary>
        IRemisionadoColumnsGridBuilder AddFechaCobranza();
        /// <summary>
        /// columna fecha de entrega
        /// </summary>
        IRemisionadoColumnsGridBuilder AddFechaEntrega();
        /// <summary>
        /// columa fehca de ultimo pago de la remision
        /// </summary>
        IRemisionadoColumnsGridBuilder AddFechaUltimoPago();
        /// <summary>
        /// columna de fecha de vencimineto del pagare
        /// </summary>
        IRemisionadoColumnsGridBuilder AddFechaVencimiento();
        /// <summary>
        /// columna del importe total del descuento 
        /// </summary>
        IRemisionadoColumnsGridBuilder AddTotalDescuento();
        /// <summary>
        /// STotal - Desc.
        /// </summary>
        IRemisionadoColumnsGridBuilder AddImporte();
        IRemisionadoColumnsGridBuilder AddSubTotal();
        IRemisionadoColumnsGridBuilder AddTotalTrasladoIVA();
        IRemisionadoColumnsGridBuilder AddTotalRetencionISR();
        IRemisionadoColumnsGridBuilder AddTotalRetencionIVA();
        IRemisionadoColumnsGridBuilder AddTotalRetencionIEPS();
        IRemisionadoColumnsGridBuilder AddTotalTrasladoIEPS();
        IRemisionadoColumnsGridBuilder AddTotal();
        IRemisionadoColumnsGridBuilder AddGranTotal();
        IRemisionadoColumnsGridBuilder Saldo();
        IRemisionadoColumnsGridBuilder AddClaveMoneda();
        IRemisionadoColumnsGridBuilder AddDiasTranscurridos();
        IRemisionadoColumnsGridBuilder AddDiasUltCobro();
        IRemisionadoColumnsGridBuilder AddDiasCobranza();
        IRemisionadoColumnsGridBuilder AddVendedor();
        IRemisionadoColumnsGridBuilder AddFilePDF();
        IRemisionadoColumnsGridBuilder AddCancela();
        IRemisionadoColumnsGridBuilder AddFechaCancela();
        IRemisionadoColumnsGridBuilder AddTasaIVAPactado();
        IRemisionadoColumnsGridBuilder AddTotalIVAPactado();
        IRemisionadoColumnsGridBuilder AddFactorPactado();
        IRemisionadoColumnsGridBuilder AddPorCobrarPactado();
        IRemisionadoColumnsGridBuilder AddDescuentoTipo();
        IRemisionadoColumnsGridBuilder AddDescuentoFactor();
        IRemisionadoColumnsGridBuilder AddDescuentoTotal();
        IRemisionadoColumnsGridBuilder AddPorCobrar();
        IRemisionadoColumnsGridBuilder AddAcumulado();
        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        new IRemisionadoColumnsGridBuilder AddFechaNuevo();
        new IRemisionadoColumnsGridBuilder AddFechaModifica();
        new IRemisionadoColumnsGridBuilder AddCreo();
        new IRemisionadoColumnsGridBuilder AddModifica();
        IRemisionadoColumnsGridBuilder AddUrlFilePDF();
        IRemisionadoColumnsGridBuilder AddNota();
        IRemisionadoColumnsGridBuilder Guia(bool isVisible = false);
        #endregion

        #region conceptos
        /// <summary>
        /// Columna para cantidad (Cantidad)
        /// </summary>
        IRemisionadoColumnsGridBuilder AddCantidad();

        IRemisionadoColumnsGridBuilder AddCboUnidad();

        IRemisionadoColumnsGridBuilder AddIdPedido();

        IRemisionadoColumnsGridBuilder AddProducto();

        IRemisionadoColumnsGridBuilder AddIdentificador();

        IRemisionadoColumnsGridBuilder AddUnitario();

        IRemisionadoColumnsGridBuilder AddDescuento();
        
        IRemisionadoColumnsGridBuilder AddTasaIVA();

        IRemisionadoColumnsGridBuilder AddTrasladoIVA();
        #endregion

        #region autorizaciones
        /// <summary>
        /// fecha de autorizacion
        /// </summary>
        IRemisionadoColumnsGridBuilder AddFechaAutoriza();

        /// <summary>
        /// usuario autoriza
        /// </summary>
        IRemisionadoColumnsGridBuilder AddUsuarioAutoriza();

        IRemisionadoColumnsGridBuilder AddClaveMotivo();
        #endregion

        
    }
}
