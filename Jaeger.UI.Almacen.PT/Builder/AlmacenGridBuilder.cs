﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.PT.Builder {
    public class AlmacenGridBuilder : GridViewBuilder, IAlmacenGridBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IAlmacenGridColumnsBuilder,
        IGridViewColumnsBuild, IAlmacenGridTempleteBuilder {
        public AlmacenGridBuilder() : base() { }

        #region templetes
        public IAlmacenGridTempleteBuilder Templetes() {
            return this;
        }

        public IAlmacenGridTempleteBuilder Master() {
            this._Columns.Clear();
            this.IdTipoComprobante().CheckActivo().SubId().TipoComprobante().Status().Folio().Serie().ReceptorRFC().Receptor().IdDocumento().FechaEmision().Contacto().Referencia().FechaCancela().Cancela()
                .ClaveCancelacion().Nota().PDF().Creo().FechaNuevo();
            return this;
        }

        public IAlmacenGridTempleteBuilder Conceptos() {
            this._Columns.Clear();
            this.IdConcepto().CheckActivo().SubId().Cantidad().IdUnidad().Unidad().Descripcion().Marca().Especificacion().NoIdentificacion().OrdenCompra().OrdenProduccion();
            return this;
        }

        /// <summary>
        /// relaciones entre comprobantes
        /// </summary>
        public IAlmacenGridTempleteBuilder Relacion() {
            this._Columns.Clear();
            this.IdTipoComprobante().Folio().Serie().FechaEmision().ReceptorRFC().Receptor().IdDocumento().Creo();
            return this;
        }

        /// <summary>
        /// templete de cambios de status
        /// </summary>
        public IAlmacenGridTempleteBuilder CStatus() {
            this._Columns.Clear();
            this.ComboStatus().CveMotivo().Nota().Creo().FechaNuevo();
            return this;
        }

        public IAlmacenGridTempleteBuilder Search() {
            this._Columns.Clear();
            this.IdTipoComprobante().Folio().Serie().ReceptorRFC().Receptor().FechaEmision().IdDocumento().Total();
            return this;
        }
        #endregion

        #region columnas
        public IAlmacenGridColumnsBuilder IdTipoComprobante() {
            var combo = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdTipoComprobante",
                HeaderText = "Comprobante",
                IsVisible = true,
                Name = "IdTipoComprobante",
                ReadOnly = true,
                Width = 95
            };

            combo.DataSource = ValeAlmacenService.TipoOperacion();
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "IdDocumento";
            this._Columns.Add(combo);
            return this;
        }

        public new IAlmacenGridColumnsBuilder CheckActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                IsVisible = false,
                Name = "Activo",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder SubId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "SubId",
                HeaderText = "SubId",
                IsVisible = false,
                Name = "SubId",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder TipoComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoMovimiento",
                HeaderText = "Tipo",
                Name = "TipoMovimiento",
                ReadOnly = true,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Status() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Status",
                HeaderText = "Status",
                Name = "Status",
                Width = 85, });
            return this;
        }

        /// <summary>
        /// status del vales de almacen 
        /// </summary>
        public IAlmacenGridColumnsBuilder ComboStatus() {
            var d0 = new GridViewComboBoxColumn {
                FieldName = "Status",
                HeaderText = "Status",
                Name = "Status",
                Width = 85
            };
            d0.DataSource = ValeAlmacenService.GetStatus();
            d0.DisplayMember = "Descriptor";
            d0.ValueMember = "Id";
            this._Columns.Add(d0);
            return this;
        }

        public IAlmacenGridColumnsBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                FormatString = "{0:00000#}",
                HeaderText = "Folio",
                Name = "Folio",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85, });
            return this;
        }

        public IAlmacenGridColumnsBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                IsVisible = false,
                Name = "Serie",
                ReadOnly = true,
                Width = 75, });
            return this;
        }

        public IAlmacenGridColumnsBuilder NoOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "NoOrden",
                FormatString = "{0:00000#}",
                HeaderText = "O. C.",
                Name = "NoOrden",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Receptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Receptor",
                HeaderText = "Receptor",
                Name = "Receptor",
                ReadOnly = true,
                Width = 240
            }); return this;
        }

        public IAlmacenGridColumnsBuilder ReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "RFC",
                Name = "ReceptorRFC",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder FechaEmision() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaEmision",
                DataType = typeof(DateTime),
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder FechaEntrega() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaEntrega",
                DataType = typeof(DateTime),
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Entrega",
                Name = "FechaEntrega",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85, });
            return this;
        }

        public IAlmacenGridColumnsBuilder FechaCancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaCancela",
                DataType = typeof(DateTime),
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Cancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cancela",
                HeaderText = "Cancela",
                Name = "Cancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder ClaveCancelacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveCancelacion",
                HeaderText = "Cv. Cancelación",
                Name = "ClaveCancelacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Contacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Contacto",
                HeaderText = "Recibe",
                Name = "Contacto",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Referencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                ReadOnly = true,
                Width = 200
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                Name = "IdDocumento",
                ReadOnly = true,
                Width = 180,
                IsVisible = false
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder PDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFilePDF",
                HeaderText = "PDF",
                Name = "UrlFilePDF",
                ReadOnly = true,
                Width = 45,
                IsVisible = true
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Total() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Total",
                HeaderText = "Importe",
                Name = "Total",
                FormatString = this.FormatStringMoney,
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                IsVisible = true
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Modifica() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = false,
                ReadOnly = true
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder FechaModifica() {
            _Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Mod.",
                Name = "FechaModifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }
        #endregion

        #region conceptos
        public IAlmacenGridColumnsBuilder IdConcepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdConcepto",
                HeaderText = "IdConcepto",
                Name = "IdConcepto",
                ReadOnly = true,
                IsVisible = false,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder IdUnidad() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdUnidad",
                HeaderText = "Unidad",
                Name = "IdUnidad",
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder IdProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdProducto",
                HeaderText = "IdProducto",
                Name = "IdProducto",
                ReadOnly = true,
                IsVisible = false,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder IdModelo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdModelo",
                HeaderText = "IdModelo",
                Name = "IdModelo",
                ReadOnly = true,
                IsVisible = false,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder OrdenCompra() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "OrdenCompra",
                HeaderText = "Ord. Compra",
                Name = "OrdenCompra",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder OrdenProduccion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "OrdenProduccion",
                HeaderText = "Ord. Prod.",
                Name = "OrdenProduccion",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringNumber,
                FieldName = "Cantidad",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                ReadOnly = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Entrada() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringNumber,
                FieldName = "Entrada",
                HeaderText = "Cantidad",
                Name = "Entrada",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Salida() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringNumber,
                FieldName = "Salida",
                HeaderText = "Cantidad",
                Name = "Salida",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                ReadOnly = true,
                Width = 285
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Especificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Especificacion",
                HeaderText = "Especificación",
                Name = "Especificacion",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder Marca() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Marca",
                HeaderText = "Marca",
                Name = "Marca",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenGridColumnsBuilder NoIdentificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "NoIdentificacion",
                HeaderText = "No. Identificación",
                Name = "NoIdentificacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 95
            });
            return this;
        }
        #endregion

        #region historial de status
        public IAlmacenGridColumnsBuilder CveMotivo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CveMotivo",
                HeaderText = "Cv. Motivo",
                Name = "CveMotivo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 85
            });
            return this;
        }
        #endregion

        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, ValeAlmacenConceptoModel item) {
            row.Cells["Cantidad"].Value = item.Cantidad;
            row.Cells["IdUnidad"].Value = item.IdUnidad;
            row.Cells["Unidad"].Value = item.Unidad;
            row.Cells["Descripcion"].Value = item.Descripcion;
            row.Cells["Especificacion"].Value = item.Especificacion;
            row.Cells["Marca"].Value = item.Marca;
            row.Cells["NoIdentificacion"].Value = item.Identificador;
            row.Cells["OrdenCompra"].Value = item.OrdenCompra;
            row.Cells["OrdenProduccion"].Value = item.OrdenProduccion;
            return row;
        }

        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, ValeAlmacenRelacionModel item) {
            row.Cells["IdTipoComprobante"].Value = item.IdTipoComprobante;
            row.Cells["Folio"].Value = item.Folio;
            row.Cells["Serie"].Value = item.Serie;
            row.Cells["ReceptorRFC"].Value = item.ReceptorRFC;
            row.Cells["Receptor"].Value = item.Receptor;
            row.Cells["FechaEmision"].Value = item.FechaEmision;
            row.Cells["IdDocumento"].Value = item.IdDocumento;
            row.Cells["Creo"].Value = item.Creo;
            //row.Cells["OrdenProduccion"].Value = item.OrdenProduccion;
            return row;
        }

        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, ValeAlmacenStatusModel item) {
            row.Cells["Status"].Value = item.IdStatusB;
            row.Cells["CveMotivo"].Value = item.CveMotivo;
            row.Cells["Nota"].Value = item.Nota;
            row.Cells["Creo"].Value = item.Creo;
            row.Cells["FecNuevo"].Value = item.FechaNuevo;
            return row;
        }
    }
}
