﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Almacen.PT.Contracts;

namespace Jaeger.UI.Almacen.PT.Forms {
    public class RemisionadoGridControl : GridCommonControl {
        #region declaraciones
        protected internal IRemisionadoService _Service;
        protected internal IConfiguration _Configuration;
        protected internal BindingList<RemisionSingleModel> _DataSource;
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal GridViewTemplate Autorizacion = new GridViewTemplate() { Caption = "Cambios de Status" };
        protected internal GridViewTemplate Relacion = new GridViewTemplate() { Caption = "Remisión Relacionada" };
        protected internal RadMenuItem TImprimirComprobante = new RadMenuItem { Text = "Comprobante", Name = "TImprimirComprobante" };
        protected internal RadMenuItem TImprimirListado = new RadMenuItem { Text = "Listado", Name = "TImprimirListado" };
        protected internal RadMenuItem TAcciones = new RadMenuItem { Text = "Acciones" };
        protected internal RadMenuItem TCargarPDF = new RadMenuItem { Text = "Cargar" };
        protected internal RadMenuItem TDescargaPDF = new RadMenuItem { Text = "Descargar" };
        private readonly string[] _columns = new string[] { "FechaEntrega", "FechaCobranza" };
        #endregion

        /// <summary>
        /// mostrar o ocultar los importes de los comprobantes
        /// </summary>
        public bool ShowValues { get; set; } = true;

        public bool Export { get; set; } = true;

        public RemisionadoGridControl() : base() {
            this.Conceptos.Standard();
            this.Autorizacion.Standard();
            this.Relacion.Standard();

            using (IRemisionadoGridBuilder builder = new RemisionadoGridBuilder()) {
                if (this.ShowValues) {
                    this.GridData.Columns.AddRange(builder.Templetes().Remisionado().Build());
                    this.Conceptos.Columns.AddRange(builder.Templetes().Concepto().Build());
                } else {
                    this.GridData.Columns.AddRange(builder.Templetes().RemisionadoSinValores().Build());
                    this.Conceptos.Columns.AddRange(builder.Templetes().ConceptosSinValores().Build());
                }
                this.Autorizacion.Columns.AddRange(builder.Templetes().Autorizacion().Build());
                this.Relacion.Columns.AddRange(builder.Templetes().Relacion().Build());
            }
            var formato = this.GridData.Columns["IdStatus"] as GridViewComboBoxColumn;
            formato.ConditionalFormattingObjectList.Add(RemisionadoGridBuilder.Estado());
            this.GridData.MasterTemplate.Templates.AddRange(this.Conceptos);
            this.GridData.MasterTemplate.Templates.AddRange(this.Autorizacion);
            this.GridData.MasterTemplate.Templates.AddRange(this.Relacion);
            
            this.GridData.AllowEditRow = true;
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            
            this.Conceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.Conceptos);
            this.Autorizacion.HierarchyDataProvider = new GridViewEventDataProvider(this.Autorizacion);
            this.Relacion.HierarchyDataProvider = new GridViewEventDataProvider(this.Relacion);

            this.GridData.CellBeginEdit += this.CellBeginEdit;
            this.GridData.CellEndEdit += this.CellEndEdit;
            this.GridData.CellDoubleClick += this.CellDoubleClick;
            this.GridData.CellFormatting += this.CellFormatting;
            this.Imprimir.Items.AddRange(this.TImprimirComprobante, this.TImprimirListado);
        }

        /// <summary>
        /// inicio de la edicion de la celda seleccionada
        /// </summary>
        protected virtual void CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Row is GridViewFilteringRowInfo) { return; }
            if (_columns.Contains(e.Column.Name)) {
                var seleccionado = this.GetCurrent<RemisionDetailModel>();
                if (seleccionado.IdStatus == 0 || seleccionado.IdStatus == 5 || seleccionado.Status == Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Pendiente) {
                    e.Cancel = true;
                    return;
                }

                if (string.IsNullOrEmpty(seleccionado.IdDocumento)) { e.Cancel = true; return; }
                if (e.Column.Name == "FechaEntrega" && seleccionado.FechaEntrega != null) { e.Cancel = true; return; }
                if (e.Column.Name == "FechaCobranza" && seleccionado.FechaCobranza != null) { e.Cancel = true; return; }

                if (e.Column.Name == "FechaCobranza" && seleccionado.FechaCobranza == null && seleccionado.FechaEntrega == null) {
                    e.Cancel = true;
                    RadMessageBox.Show(this, "No es permite actualizar la fecha de cobranza porque no se cuenta con fecha de entrega", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                } else {
                    if (e.Column.Name == "FechaCobranza") {
                        var d = e.ActiveEditor as RadDateTimeEditor;
                        d.MinValue = seleccionado.FechaEntrega.Value;
                    } else {
                        var d = e.ActiveEditor as RadDateTimeEditor;
                        d.MinValue = seleccionado.FechaEmision.AddHours(-24);
                    }
                }
            } else {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// evento al terminar la edicion de la celda seleccionada
        /// </summary>
        protected virtual void CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (_columns.Contains(e.Column.Name)) {
                var seleccionado = e.Row.DataBoundItem as IRemisionDetailModel;
                if (e.Column.Name == "FechaEntrega") {
                    if (seleccionado.FechaEntrega == null) { return; }
                    this.FechaEntrega();
                } else if (e.Column.Name == "FechaCobranza") {
                    if (seleccionado.FechaCobranza == null) { return; }
                    this.FechaCobranza();
                }
            }
        }

        protected virtual void CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name == "UrlFilePDF") {
                        this.TDescargaPDF.PerformClick();
                    }
                }
            }
        }

        /// <summary>
        /// formato de celdas
        /// </summary>
        protected virtual void CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "UrlFilePDF") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status" && e.Column.Name != "FechaEntrega" && e.Column.Name != "FechaCobranza") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    if (e.CellElement.Children != null) {
                        if (e.CellElement.Children.Count > 0) {
                            e.CellElement.Children.Clear();
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        protected virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.Conceptos.Caption) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.ShowDialog(this);
                }
                var seleccionado = this.GetCurrent<RemisionSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Conceptos != null) {
                        foreach (var item in seleccionado.Conceptos) {
                            var row = e.Template.Rows.NewRow();
                            e.SourceCollection.Add(RemisionadoGridBuilder.ConvertToImportes(row, item));
                        }
                    }
                }
            } else if (e.Template.Caption == this.Autorizacion.Caption) {
                var seleccionado = this.GetCurrent<RemisionSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Autorizaciones != null) {
                        foreach (var item in seleccionado.Autorizaciones) {
                            var row = e.Template.Rows.NewRow();
                            row = RemisionadoGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.Relacion.Caption) {
                var seleccionado = this.GetCurrent<RemisionSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Autorizaciones != null) {
                        foreach (var item in seleccionado.RemisionRelacionada) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["Folio"].Value = item.Folio;
                            row.Cells["Serie"].Value = item.Serie;
                            row.Cells["IdDocumento"].Value = item.IdDocumento;
                            row.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
                            row.Cells["FechaEmision"].Value = item.FechaEmision;
                            row.Cells["GTotal"].Value = item.GTotal;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        protected virtual void GetComprobante() {
            var seleccionado = this.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var d = this._Service.GetById(seleccionado.IdRemision);
                if (d != null) {
                    seleccionado.SetValues(d);
                }
            }
        }

        /// <summary>
        /// actualizar fecha de entrega al cliente
        /// </summary>
        protected virtual void FechaEntrega() {
            this.GridData.Enabled = false;
            var seleccionado = this.GetCurrent<RemisionDetailModel>();
            if (seleccionado != null) {
                seleccionado = this._Service.FechaEntrega(seleccionado);
            }
            this.GridData.Enabled = true;
        }

        /// <summary>
        /// actualizar fecha de recepcion para cobranza
        /// </summary>
        protected virtual void FechaCobranza() {
            this.GridData.Enabled = false;
            var seleccionado = this.GetCurrent<RemisionDetailModel>();
            if (seleccionado != null) {
                seleccionado = this._Service.FechaCobranza(seleccionado);
            }
            this.GridData.Enabled = true;
        }
    }
}
