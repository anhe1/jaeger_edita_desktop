﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class RemisionStatusForm : RadForm {
        private readonly RemisionDetailModel _CurrentRemision;
        protected internal IRemisionadoService _Service;
        protected internal IRemisionStatusDetailModel _StatusRemision;

        public event EventHandler<IRemisionStatusDetailModel> Selected;
        public void OnSelected(IRemisionStatusDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public RemisionStatusForm(RemisionDetailModel model, IRemisionadoService service) {
            InitializeComponent();
            this._CurrentRemision = model;
            this._Service = service;
            this._StatusRemision = new RemisionStatusDetailModel();
        }

        private void RemisionStatusForm_Load(object sender, EventArgs e) {
            this.FechaEntrega.Value = DateTime.Now;
            this.FechaEntrega.SetEditable(false);
            this.IdDocumento.Text = this._CurrentRemision.IdRemision.ToString();
            this.Cliente.Text = this._CurrentRemision.ReceptorNombre;
            this.TDocumento.Nuevo.Text = "Agregar";
            this.TCancelar.Autorizar.Enabled = true;
            this.TCancelar.Cerrar.Enabled = true;
            this.TCancelar.Autorizar.Click += this.TCancelar_Autorizar_Click;
            this.TCancelar.Cerrar.Click += this.TCancelar_Cerrar_Click;
            this.TDocumento.Nuevo.Click += Nuevo_Click;
            this.MotivoCancelacion.DataSource = RemisionService.GetTipoRelacion();
            this.IdStatus.DataSource = RemisionService.GetStatus();
            using (IRemisionadoGridBuilder view = new RemisionadoGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Relacion().Build());
                view.Template().GridCommon(this.GridData);
            }
            this.GridData.DataSource = this._StatusRemision.Relaciones;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            var buscar = new RemisionBuscarForm(this._Service, this._CurrentRemision.IdCliente);
            buscar.Selected += Buscar_Selected;
            buscar.ShowDialog(this);
        }

        #region barra de herramientas
        private void TCancelar_Autorizar_Click(object sender, EventArgs e) {
            var _status = this.IdStatus.SelectedItem.DataBoundItem as StatusModel;
            var _seleccionado = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as RemisionRelacionModel;
            if (_seleccionado != null) {

                this._StatusRemision.IdRemision = _CurrentRemision.IdRemision;
                this._StatusRemision.CvMotivo = _seleccionado.Descriptor;
                this._StatusRemision.User = ConfigService.Piloto.Clave;
                this._StatusRemision.FechaNuevo = DateTime.Now;
                this._StatusRemision.IdCliente = _CurrentRemision.IdCliente;
                this._StatusRemision.IdStatusA = _CurrentRemision.IdStatus;
                this._StatusRemision.IdStatusB = _status.Id;
                this._StatusRemision.Nota = this.Nota.Text;

                this.OnSelected(this._StatusRemision);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Buscar_Selected(object sender, RemisionSingleModel e) {
            if (e != null) {
                this._StatusRemision.Relaciones.Add(new RemisionRelacionadaModel() {
                    Folio = e.Folio,
                    Serie = e.Serie,
                    FechaEmision = e.FechaEmision,
                    IdDirectorio = e.IdCliente,
                    IdRemision = e.IdRemision,
                    IdDocumento = e.IdDocumento,
                    ReceptorNombre = e.ReceptorNombre,
                    GTotal = e.GTotal
                });
            }
        }
    }
}
