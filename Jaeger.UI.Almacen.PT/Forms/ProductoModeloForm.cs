﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class ProductoModeloForm : RadForm {
        protected internal ProductoServicioDetailModel producto;
        private BindingList<UnidadModel> unidades = new BindingList<UnidadModel>();

        public ProductoModeloForm() {
            InitializeComponent();
        }

        private void ProductoModeloForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridProveedor.Standard();

            this.cboCategorias.DisplayMember = "Resume";
            this.cboCategorias.ValueMember = "IdProducto";

            this.CboUnidadAlmacen.DataSource = this.unidades;
            this.CboUnidadAlmacen.ValueMember = "Descripcion";
            this.CboUnidadAlmacen.DisplayMember = "Descripcion";

            this.cboUnidadXY.DataSource = new BindingList<UnidadModel>(this.unidades);
            this.cboUnidadXY.ValueMember = "Id";
            this.cboUnidadXY.DisplayMember = "Descripcion";

            this.cboUnidadZ.DataSource = new BindingList<UnidadModel>(this.unidades);
            this.cboUnidadZ.ValueMember = "Id";
            this.cboUnidadZ.DisplayMember = "Descripcion";


            cboTrasladoIVA.DisplayMember = "Descripcion";
            cboTrasladoIVA.ValueMember = "Descripcion";

            this.CreateBinding();
            this.TProducto.Guardar.Click += this.TModelo_Guardar_Click;
            this.TProducto.Cerrar.Click += this.TModelo_Cerrar_Click;
        }

        public virtual void TModelo_Guardar_Click(object sender, EventArgs eventArgs) {
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        public virtual void TModelo_Cerrar_Click(object sender, EventArgs eventArgs) {
            this.Close();
        }

        public virtual void CreateBinding() {
            this.cboCategorias.DataBindings.Clear();
            this.cboCategorias.DataBindings.Add("SelectedValue", this.producto, "IdProducto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbModelo.DataBindings.Clear();
            this.txbModelo.DataBindings.Add("Text", this.producto, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbMarca.DataBindings.Clear();
            this.txbMarca.DataBindings.Add("Text", this.producto, "Marca", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbExpecificacion.DataBindings.Clear();
            this.txbExpecificacion.DataBindings.Add("Text", this.producto, "Especificacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTipo.DataBindings.Clear();
            this.cboTipo.DataBindings.Add("SelectedValue", this.producto, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbCodigoBarras.DataBindings.Clear();
            this.txbCodigoBarras.DataBindings.Add("Text", this.producto, "Codigo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbEtiquetas.DataBindings.Clear();
            this.txbEtiquetas.DataBindings.Add("Text", this.producto, "Etiquetas", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbLargo.DataBindings.Clear();
            this.txbLargo.DataBindings.Add("Text", this.producto, "Largo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbAncho.DataBindings.Clear();
            this.txbAncho.DataBindings.Add("Text", this.producto, "Ancho", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbAlto.DataBindings.Clear();
            this.txbAlto.DataBindings.Add("Text", this.producto, "Alto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbPrecioBase.DataBindings.Clear();
            this.txbPrecioBase.DataBindings.Add("Value", this.producto, "Unitario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockMinimo.DataBindings.Clear();
            this.TxbStockMinimo.DataBindings.Add("Text", this.producto, "Minimo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockMaximo.DataBindings.Clear();
            this.TxbStockMaximo.DataBindings.Add("Text", this.producto, "Maximo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockReorden.DataBindings.Clear();
            this.TxbStockReorden.DataBindings.Add("Text", this.producto, "ReOrden", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbExistencia.DataBindings.Clear();
            this.TxbExistencia.DataBindings.Add("Text", this.producto, "Existencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboUnidadAlmacen.DataBindings.Clear();
            this.CboUnidadAlmacen.DataBindings.Add("Text", this.producto, "Unidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTrasladoIVA.DataBindings.Clear();
            this.cboTrasladoIVA.DataBindings.Add("Text", this.producto, "FactorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbTrasladoIVA.DataBindings.Clear();
            this.txbTrasladoIVA.DataBindings.Add("Value", this.producto, "ValorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

        }

        public virtual void Guardar() {
        }
    }
}
