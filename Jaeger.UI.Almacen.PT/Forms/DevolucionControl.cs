﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class DevolucionControl : UserControl {
        private bool _ReadOnly = false;
        public DevolucionDetailModel Comprobante;
        public Aplication.Almacen.PT.Contracts.IValeAlmacenService Service;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public DevolucionControl() {
            InitializeComponent();
        }

        private void DevolucionControl_Load(object sender, EventArgs e) {
            //this.Documento.DataSource = ValeAlmacenService.GetTipoOperacion();
            this.Documento.SelectedIndex = -1;
            this.Documento.SelectedValue = null;

            //this.Efecto.DataSource = ValeAlmacenService.GetTipos1();
            this.Efecto.SelectedIndex = -1;
            this.Efecto.SelectedValue = null;

            this.TDocumento.Nuevo.Click += this.Nuevo_Click;
            this.TDocumento.Actualizar.Click += this.Actualizar_Click;
            this.TDocumento.Guardar.Click += this.Guardar_Click;
            this.TDocumento.Imprimir.Click += this.Imprimir_Click;

            this.Documento.TextChanged += Documento_TextChanged;
            this.Receptor.Seleccionado += this.Receptor_Seleccionado;
        }

        private void Receptor_Seleccionado(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                this.Vendedor.DataSource = e.Vendedores;
            }
        }

        private void Documento_TextChanged(object sender, EventArgs e) {
            if (this.Documento.SelectedItem == null) {
                return;
            }
            var seleccionado = ((GridViewDataRowInfo)this.Documento.SelectedItem).DataBoundItem as DocumentoAlmacenDetailModel;
            if (seleccionado != null) {
                //this.Comprobante.IdTipo = seleccionado.IdDocumento;
                //this.Comprobante.IdEfecto = seleccionado.IdTipo;
                //this.Comprobante.IdAlmacen = 2;
                this.Efecto.SelectedValue = seleccionado.IdTipo;
            }
        }

        public bool ReadOnly {
            get { return _ReadOnly; }
            set {
                _ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        #region barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.Actualizar_Click(sender, e);
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Comprobante = DevolucionService.Create().Build();
            } else {
                using (var espera = new Waiting1Form(this.Cargar)) {
                    espera.Text = "Consultando ...";
                    espera.ShowDialog(this);
                }
            }

            if (this.Comprobante.IsEditable) {
                this.Receptor.Init();
            }

            this.CreateBinding();
        }

        public virtual void Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, Properties.Resources.msg_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Almacenando ...";
                espera.ShowDialog(this);
            }
            this.Actualizar_Click(sender, e);
        }

        public virtual void Imprimir_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, Properties.Resources.msg_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            var printer = new DevolucionDetailPrinterModel(this.Comprobante);
            var reporte = new ReporteForm(printer);
            reporte.Show();
        }
        #endregion

        public virtual void CreateBinding() {

            this.TDocumento.Status.DataBindings.Clear();
            this.TDocumento.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.IdDocumento.DataBindings.Clear();
            this.TDocumento.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.lblVersion.DataBindings.Clear();
            this.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Nombre.DataBindings.Clear();
            this.Receptor.Nombre.SetEditable(this.Comprobante.IsEditable);
            this.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "Receptor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.RFC.DataBindings.Clear();
            this.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Documento.DataBindings.Clear();
            this.Documento.DataBindings.Add("SelectedValue", this.Comprobante, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Documento.SetEditable(this.Comprobante.IsEditable);

            //this.Efecto.DataBindings.Clear();
            //this.Efecto.DataBindings.Add("SelectedValue", this.Comprobante, "IdEfecto", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.Efecto.SetEditable(this.Comprobante.IsEditable);

            this.DevolucionTipo.DataBindings.Clear();
            this.DevolucionTipo.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveDevolucion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TasaIVAPactado.DataBindings.Clear();
            this.TasaIVAPactado.DataBindings.Add("Value", this.Comprobante, "TasaIVAPactado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contacto.DataBindings.Clear();
            this.Contacto.ReadOnly = !this.Comprobante.IsEditable;
            this.Contacto.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Clave.DataBindings.Clear();
            this.Receptor.Clave.DataBindings.Add("Text", this.Comprobante, "ReceptorClave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDirectorio.DataBindings.Clear();
            this.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Folio.DataBindings.Clear();
            this.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdSerie.DataBindings.Clear();
            this.IdSerie.SetEditable(this.Comprobante.IsEditable);
            this.IdSerie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Vendedor.DataBindings.Clear();
            this.Vendedor.SetEditable(this.Comprobante.IsEditable);
            this.Vendedor.DataBindings.Add("Text", this.Comprobante, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdVendedor2.DataBindings.Clear();
            this.IdVendedor2.DataBindings.Add("Value", this.Comprobante, "IdVendedor", false, DataSourceUpdateMode.OnPropertyChanged);

            this.Nota.DataBindings.Clear();
            this.Nota.ReadOnly = !this.Comprobante.IsEditable;
            this.Nota.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaIngreso.DataBindings.Clear();
            this.FechaIngreso.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            this.FechaIngreso.DateTimePickerElement.ReadOnly = true;
            this.FechaIngreso.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.FechaIngreso.DataBindings.Add("Value", this.Comprobante, "FechaIngreso", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaIngreso.SetEditable(this.Comprobante.IsEditable);

            this.IdPedido.DataBindings.Clear();
            this.IdPedido.DataBindings.Add("Text", this.Comprobante, "IdPedido", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Departamento.DataBindings.Clear();
            this.Departamento.DataBindings.Add("SelectedValue", this.Comprobante, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Departamento.SetEditable(this.Comprobante.IsEditable);

            this.Creo.DataBindings.Clear();
            this.Creo.ReadOnly = true;
            this.Creo.DataBindings.Add("Text", this.Comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.FilePDF.DataBindings.Clear();
            this.TDocumento.FilePDF.DataBindings.Add("Enabled", this.Comprobante, "IdComprobante", true, DataSourceUpdateMode.OnPropertyChanged);

            this.OnBindingClompleted(null);

            this.TDocumento.Guardar.Enabled = this.Comprobante.IsEditable;
            this.TDocumento.Imprimir.Enabled = !this.Comprobante.IsEditable;
            this.TDocumento.Actualizar.Enabled = !this.Comprobante.IsEditable;
        }

        public virtual void CreateReadOnly() {
            this.Receptor.ReadOnly = this.ReadOnly;
            this.Contacto.ReadOnly = this.ReadOnly;
            this.Nota.ReadOnly = this.ReadOnly;
        }

        public virtual void Cargar() {
            //this.Comprobante = this.Service.GetById(this.Comprobante.IdComprobante);
        }

        public virtual void Guardar() {
            //this.Comprobante = this.Service.Save(this.Comprobante);
        }

        public virtual bool Validar() {
            this.Advertencia.Clear();
            if (this.Comprobante.IdDirectorio == 0) {
                this.Receptor.Advertencia.SetError(this.Receptor.Nombre, "Selecciona un receptor válido para este comprobante.");
                return false;
            }

            if (string.IsNullOrEmpty(this.Comprobante.Contacto)) {
                this.Advertencia.SetError(this.Contacto, "Por favor ingresa un nombre o referencia.");
                return false;
            }

            //if (this.Comprobante.IdTipo == 0) {
            //    this.Advertencia.SetError(this.Documento, "Es necesario seleccionar el tipo de comprobante.");
            //    return false;
            //}

            if (this.Comprobante.Conceptos != null) {
                if (this.Comprobante.Conceptos.Count == 0) {
                    this.Receptor.Advertencia.SetError(this.Receptor.Nombre, "No se agregaron conceptos para este comprobante.");
                    return false;
                }
            }
            return true;
        }
    }
}
