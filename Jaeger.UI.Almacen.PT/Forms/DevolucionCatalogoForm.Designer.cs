﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class DevolucionCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevolucionCatalogoForm));
            this.TDevolucion = new Jaeger.UI.Common.Forms.GridCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TDevolucion
            // 
            this.TDevolucion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TDevolucion.Location = new System.Drawing.Point(0, 0);
            this.TDevolucion.Name = "TDevolucion";
            this.TDevolucion.ShowActualizar = true;
            this.TDevolucion.ShowAutosuma = false;
            this.TDevolucion.ShowCancelar = false;
            this.TDevolucion.ShowCerrar = true;
            this.TDevolucion.ShowEditar = true;
            this.TDevolucion.ShowEjercicio = true;
            this.TDevolucion.ShowExportarExcel = false;
            this.TDevolucion.ShowFiltro = true;
            this.TDevolucion.ShowHerramientas = true;
            this.TDevolucion.ShowImprimir = true;
            this.TDevolucion.ShowNuevo = true;
            this.TDevolucion.ShowPeriodo = true;
            this.TDevolucion.ShowSeleccionMultiple = true;
            this.TDevolucion.Size = new System.Drawing.Size(1134, 646);
            this.TDevolucion.TabIndex = 0;
            // 
            // DevolucionCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 646);
            this.Controls.Add(this.TDevolucion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DevolucionCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: Devoluciones";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DevolucionCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.GridCommonControl TDevolucion;
    }
}
