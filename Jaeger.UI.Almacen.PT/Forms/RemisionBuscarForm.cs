﻿using System;
using System.Linq;
using System.Collections.Generic;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class RemisionBuscarForm : RadForm {
        #region declaraciones
        protected internal IRemisionadoService _Service;
        protected internal List<RemisionSingleModel> _SourceData;
        protected internal int _IdReceptor;
        #endregion

        public event EventHandler<RemisionSingleModel> Selected;
        public void OnSelected(RemisionSingleModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public RemisionBuscarForm(IRemisionadoService service, int idReceptor) {
            InitializeComponent();
            this._Service = service;
            this._IdReceptor = idReceptor;
            this.TBuscar.Descripcion.MaxSize = new System.Drawing.Size(75, 20);
            this.TBuscar.Etiqueta = "Folio: ";
            this.TBuscar.Descripcion.TextBoxElement.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        }

        public virtual void UnidadBuscarForm_Load(object sender, EventArgs e) {
            using (Builder.IRemisionadoGridBuilder view = new Builder.RemisionadoGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Search().Build());
                this.GridData.Standard();
            }
            this.TBuscar.Agregar.Click += this.GridData_DoubleClick;
            this.TBuscar.Buscar.Click += this.TBuscar_Actualizar_Click;
            this.GridData.DoubleClick += this.GridData_DoubleClick;
        }

        #region barra de herramientas
        public virtual void TBuscar_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._SourceData;
        }

        public virtual void TBuscar_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.TBuscar.Filtro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        public virtual void TBuscar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_DoubleClick(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as RemisionSingleModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void Consultar() {
            var condicional = RemisionadoService.Query().WithIdReceptor(this._IdReceptor).OnlyActive(true).Build();
            try {
                var d0 = int.Parse(this.TBuscar.Descripcion.Text);
                condicional = RemisionadoService.Query().WithFolio(d0).Build();
            } catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }
            this._SourceData = this._Service.GetList<RemisionSingleModel>(condicional).ToList();
        }
        #endregion
    }
}
