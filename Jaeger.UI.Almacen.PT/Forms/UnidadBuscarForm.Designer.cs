﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class UnidadBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TUnidad = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.gridUnidad = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidad.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TUnidad
            // 
            this.TUnidad.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUnidad.Etiqueta = "";
            this.TUnidad.Location = new System.Drawing.Point(0, 0);
            this.TUnidad.Name = "TUnidad";
            this.TUnidad.ShowAgregar = true;
            this.TUnidad.ShowBuscar = true;
            this.TUnidad.ShowCerrar = true;
            this.TUnidad.ShowExistencia = false;
            this.TUnidad.ShowFiltro = true;
            this.TUnidad.Size = new System.Drawing.Size(517, 30);
            this.TUnidad.TabIndex = 14;
            this.TUnidad.ButtonFiltro_Click += new System.EventHandler<System.EventArgs>(this.TUnidad_Filtro_Click);
            this.TUnidad.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TUnidad_Cerrar_Click);
            // 
            // gridUnidad
            // 
            this.gridUnidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUnidad.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridUnidad.MasterTemplate.AllowAddNewRow = false;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn1.FieldName = "Descripcion";
            gridViewTextBoxColumn1.HeaderText = "Descripción";
            gridViewTextBoxColumn1.Name = "Descripcion";
            gridViewTextBoxColumn1.Width = 200;
            gridViewTextBoxColumn2.DataType = typeof(decimal);
            gridViewTextBoxColumn2.FieldName = "Factor";
            gridViewTextBoxColumn2.FormatString = "{0:N4}";
            gridViewTextBoxColumn2.HeaderText = "Factor";
            gridViewTextBoxColumn2.Name = "Factor";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "Nota";
            gridViewTextBoxColumn3.HeaderText = "Nota";
            gridViewTextBoxColumn3.Name = "Nota";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn4.FieldName = "FechaNuevo";
            gridViewTextBoxColumn4.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn4.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "FechaNuevo";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Creo";
            gridViewTextBoxColumn5.HeaderText = "Creó";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Creo";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 65;
            this.gridUnidad.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.gridUnidad.MasterTemplate.EnableFiltering = true;
            this.gridUnidad.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor1.PropertyName = "Inicio";
            this.gridUnidad.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.gridUnidad.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridUnidad.Name = "gridUnidad";
            this.gridUnidad.ShowGroupPanel = false;
            this.gridUnidad.Size = new System.Drawing.Size(517, 270);
            this.gridUnidad.TabIndex = 15;
            // 
            // UnidadBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 300);
            this.Controls.Add(this.gridUnidad);
            this.Controls.Add(this.TUnidad);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnidadBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CPLite: Unidades";
            this.Load += new System.EventHandler(this.UnidadBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidad.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarBuscarControl TUnidad;
        private Telerik.WinControls.UI.RadGridView gridUnidad;
    }
}