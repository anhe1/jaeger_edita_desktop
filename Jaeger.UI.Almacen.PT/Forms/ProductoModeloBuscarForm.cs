﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class ProductoModeloBuscarForm : Telerik.WinControls.UI.RadForm {
        protected IProductosCatalogoService service;
        protected BindingList<ProductoXModelo> modelos;

        public ProductoModeloBuscarForm() {
            InitializeComponent();
        }

        public ProductoModeloBuscarForm(IProductosCatalogoService service) {
            InitializeComponent();
            this.service = service;
        }

        public event EventHandler<ProductoXModelo> Selected;
        public void OnSelected(ProductoXModelo e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public virtual void ProductoModeloBuscarForm_Load(object sender, EventArgs e) {
            
            this.ToolBar.Descripcion.TextChanged += Buscar_TextChanged;
            this.dataGridResult.Standard();
        }

        public virtual void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.ToolBar.Descripcion.Text.Length > 3) {
                this.ToolBar.Buscar.PerformClick();
            }
        }

        public virtual void TModelo_Actualizar_Click(object sender, EventArgs e) {
            this.modelos = new BindingList<ProductoXModelo>(this.service.GetList<ProductoXModelo>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>() { 
                new Conditional("@search", this.ToolBar.Descripcion.Text),
                new Conditional("ACTIVO", "1")
            }).ToList());
            this.dataGridResult.DataSource = this.modelos;
        }

        public virtual void Modelos_DoubleClick(object sender, EventArgs e) {
            if (this.dataGridResult.CurrentRow != null) {
                var seleccionado = this.dataGridResult.CurrentRow.DataBoundItem as ProductoXModelo;
                if (seleccionado != null) {
                    if (seleccionado.FactorUnidad <= 0) {
                        RadMessageBox.Show(this, "No existe unidad relacionada para el modelo seleccionado", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    this.OnSelected(seleccionado);
                }
            }
        }

        public virtual void TModelo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
