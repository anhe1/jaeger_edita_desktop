﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class RemisionFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Grupo = new Telerik.WinControls.UI.RadGroupBox();
            this.Condiciones = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Receptor = new Jaeger.UI.Contribuyentes.Forms.DirectorioControl();
            this.lblRecibe = new Telerik.WinControls.UI.RadLabel();
            this.Recibe = new Telerik.WinControls.UI.RadDropDownList();
            this.lblPedido = new Telerik.WinControls.UI.RadLabel();
            this.IdPedido = new Telerik.WinControls.UI.RadTextBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.lblVendedor = new Telerik.WinControls.UI.RadLabel();
            this.Vendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.MetodoPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblDecimales = new Telerik.WinControls.UI.RadLabel();
            this.TipoCambio = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblFormaPago = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.lblCondiciones = new Telerik.WinControls.UI.RadLabel();
            this.Decimales = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblMetodoPago = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblGuia = new Telerik.WinControls.UI.RadLabel();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaEntrega = new Telerik.WinControls.UI.RadLabel();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.lblEnvio = new Telerik.WinControls.UI.RadLabel();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.Creo = new Telerik.WinControls.UI.RadTextBox();
            this.IdSerie = new Telerik.WinControls.UI.RadDropDownList();
            this.lblCreo = new Telerik.WinControls.UI.RadLabel();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblSerie = new Telerik.WinControls.UI.RadLabel();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.FormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.MetodoEnvio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Guia = new Telerik.WinControls.UI.RadTextBox();
            this.TasaIVAPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.FactorPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.IdVendedor = new Telerik.WinControls.UI.RadSpinEditor();
            this.TDocumento = new Jaeger.UI.Common.Forms.TbDocumentoControl();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Grupo)).BeginInit();
            this.Grupo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCondiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGuia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            this.SuspendLayout();
            // 
            // Grupo
            // 
            this.Grupo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Grupo.Controls.Add(this.Condiciones);
            this.Grupo.Controls.Add(this.Receptor);
            this.Grupo.Controls.Add(this.lblRecibe);
            this.Grupo.Controls.Add(this.Recibe);
            this.Grupo.Controls.Add(this.lblPedido);
            this.Grupo.Controls.Add(this.IdPedido);
            this.Grupo.Controls.Add(this.lblVersion);
            this.Grupo.Controls.Add(this.lblVendedor);
            this.Grupo.Controls.Add(this.Vendedor);
            this.Grupo.Controls.Add(this.MetodoPago);
            this.Grupo.Controls.Add(this.lblDecimales);
            this.Grupo.Controls.Add(this.TipoCambio);
            this.Grupo.Controls.Add(this.lblFormaPago);
            this.Grupo.Controls.Add(this.Nota);
            this.Grupo.Controls.Add(this.lblCondiciones);
            this.Grupo.Controls.Add(this.Decimales);
            this.Grupo.Controls.Add(this.lblMetodoPago);
            this.Grupo.Controls.Add(this.FechaEmision);
            this.Grupo.Controls.Add(this.lblGuia);
            this.Grupo.Controls.Add(this.FechaEntrega);
            this.Grupo.Controls.Add(this.lblMoneda);
            this.Grupo.Controls.Add(this.lblFechaEmision);
            this.Grupo.Controls.Add(this.lblFechaEntrega);
            this.Grupo.Controls.Add(this.lblTipoCambio);
            this.Grupo.Controls.Add(this.lblNota);
            this.Grupo.Controls.Add(this.lblEnvio);
            this.Grupo.Controls.Add(this.Folio);
            this.Grupo.Controls.Add(this.Creo);
            this.Grupo.Controls.Add(this.IdSerie);
            this.Grupo.Controls.Add(this.lblCreo);
            this.Grupo.Controls.Add(this.Moneda);
            this.Grupo.Controls.Add(this.lblSerie);
            this.Grupo.Controls.Add(this.lblFolio);
            this.Grupo.Controls.Add(this.FormaPago);
            this.Grupo.Controls.Add(this.MetodoEnvio);
            this.Grupo.Controls.Add(this.Guia);
            this.Grupo.Controls.Add(this.TasaIVAPactado);
            this.Grupo.Controls.Add(this.FactorPactado);
            this.Grupo.Controls.Add(this.IdVendedor);
            this.Grupo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grupo.HeaderText = "";
            this.Grupo.Location = new System.Drawing.Point(0, 30);
            this.Grupo.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.Grupo.Name = "Grupo";
            this.Grupo.Padding = new System.Windows.Forms.Padding(2, 22, 2, 2);
            this.Grupo.Size = new System.Drawing.Size(1138, 105);
            this.Grupo.TabIndex = 93;
            this.Grupo.TabStop = false;
            // 
            // Condiciones
            // 
            this.Condiciones.AutoSizeDropDownToBestFit = true;
            this.Condiciones.DisplayMember = "Descriptor";
            this.Condiciones.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Condiciones.NestedRadGridView
            // 
            this.Condiciones.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Condiciones.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Condiciones.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Condiciones.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Condiciones.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Condiciones.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Condiciones.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn17.DataType = typeof(int);
            gridViewTextBoxColumn17.FieldName = "Id";
            gridViewTextBoxColumn17.HeaderText = "Clave";
            gridViewTextBoxColumn17.Name = "Clave";
            gridViewTextBoxColumn18.FieldName = "Descripcion";
            gridViewTextBoxColumn18.HeaderText = "Descripción";
            gridViewTextBoxColumn18.Name = "Descripcion";
            gridViewTextBoxColumn19.FieldName = "Descriptor";
            gridViewTextBoxColumn19.HeaderText = "Descriptor";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Descriptor";
            this.Condiciones.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19});
            this.Condiciones.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Condiciones.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Condiciones.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Condiciones.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.Condiciones.EditorControl.Name = "NestedRadGridView";
            this.Condiciones.EditorControl.ReadOnly = true;
            this.Condiciones.EditorControl.ShowGroupPanel = false;
            this.Condiciones.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Condiciones.EditorControl.TabIndex = 0;
            this.Condiciones.Location = new System.Drawing.Point(625, 78);
            this.Condiciones.Name = "Condiciones";
            this.Condiciones.NullText = "Condiciones de pago";
            this.Condiciones.Size = new System.Drawing.Size(170, 20);
            this.Condiciones.TabIndex = 455;
            this.Condiciones.TabStop = false;
            this.Condiciones.ValueMember = "Clave";
            // 
            // Receptor
            // 
            this.Receptor.AllowAddNew = false;
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.LDomicilio = "Domicilio:";
            this.Receptor.LNombre = "Nombre:";
            this.Receptor.Location = new System.Drawing.Point(6, 30);
            this.Receptor.Margin = new System.Windows.Forms.Padding(4);
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = false;
            this.Receptor.Relacion = Jaeger.Domain.Base.ValueObjects.TipoRelacionComericalEnum.None;
            this.Receptor.ShowDomicilio = true;
            this.Receptor.ShowRFC = true;
            this.Receptor.ShowSearch = false;
            this.Receptor.Size = new System.Drawing.Size(541, 44);
            this.Receptor.TabIndex = 454;
            // 
            // lblRecibe
            // 
            this.lblRecibe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecibe.Location = new System.Drawing.Point(553, 55);
            this.lblRecibe.Name = "lblRecibe";
            this.lblRecibe.Size = new System.Drawing.Size(42, 18);
            this.lblRecibe.TabIndex = 452;
            this.lblRecibe.Text = "Recibe:";
            // 
            // Recibe
            // 
            this.Recibe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Recibe.Location = new System.Drawing.Point(603, 54);
            this.Recibe.MaxLength = 32;
            this.Recibe.Name = "Recibe";
            this.Recibe.NullText = "Recibe";
            this.Recibe.Size = new System.Drawing.Size(276, 20);
            this.Recibe.TabIndex = 453;
            // 
            // lblPedido
            // 
            this.lblPedido.Location = new System.Drawing.Point(293, 6);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(53, 18);
            this.lblPedido.TabIndex = 450;
            this.lblPedido.Text = "# Pedido:";
            // 
            // IdPedido
            // 
            this.IdPedido.Location = new System.Drawing.Point(350, 5);
            this.IdPedido.MaxLength = 16;
            this.IdPedido.Name = "IdPedido";
            this.IdPedido.NullText = "# Pedido";
            this.IdPedido.Size = new System.Drawing.Size(82, 20);
            this.IdPedido.TabIndex = 451;
            this.IdPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(5, 6);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(26, 18);
            this.lblVersion.TabIndex = 449;
            this.lblVersion.Text = "Ver.";
            // 
            // lblVendedor
            // 
            this.lblVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVendedor.Location = new System.Drawing.Point(886, 55);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(58, 18);
            this.lblVendedor.TabIndex = 447;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // Vendedor
            // 
            this.Vendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Vendedor.AutoSizeDropDownToBestFit = true;
            this.Vendedor.DisplayMember = "Clave";
            this.Vendedor.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Vendedor.NestedRadGridView
            // 
            this.Vendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Vendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Vendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Vendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Vendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Vendedor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn20.DataType = typeof(int);
            gridViewTextBoxColumn20.FieldName = "IdVendedor";
            gridViewTextBoxColumn20.HeaderText = "IdVendedor";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "IdVendedor";
            gridViewTextBoxColumn21.FieldName = "Clave";
            gridViewTextBoxColumn21.HeaderText = "Vendedor";
            gridViewTextBoxColumn21.Name = "Clave";
            gridViewTextBoxColumn22.FieldName = "Nombre";
            gridViewTextBoxColumn22.HeaderText = "Nombre";
            gridViewTextBoxColumn22.Name = "Nombre";
            gridViewTextBoxColumn22.Width = 150;
            this.Vendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22});
            this.Vendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Vendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Vendedor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Vendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.Vendedor.EditorControl.Name = "NestedRadGridView";
            this.Vendedor.EditorControl.ReadOnly = true;
            this.Vendedor.EditorControl.ShowGroupPanel = false;
            this.Vendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Vendedor.EditorControl.TabIndex = 0;
            this.Vendedor.Location = new System.Drawing.Point(951, 54);
            this.Vendedor.Name = "Vendedor";
            this.Vendedor.NullText = "Vendedor";
            this.Vendedor.Size = new System.Drawing.Size(180, 20);
            this.Vendedor.TabIndex = 448;
            this.Vendedor.TabStop = false;
            this.Vendedor.ValueMember = "IdVendedor";
            // 
            // MetodoPago
            // 
            this.MetodoPago.AutoSizeDropDownToBestFit = true;
            this.MetodoPago.DisplayMember = "Descriptor";
            this.MetodoPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MetodoPago.NestedRadGridView
            // 
            this.MetodoPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MetodoPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MetodoPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MetodoPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn23.FieldName = "Clave";
            gridViewTextBoxColumn23.HeaderText = "Clave";
            gridViewTextBoxColumn23.Name = "Clave";
            gridViewTextBoxColumn24.FieldName = "Descripcion";
            gridViewTextBoxColumn24.HeaderText = "Descripción";
            gridViewTextBoxColumn24.Name = "Descripcion";
            gridViewTextBoxColumn25.FieldName = "Descriptor";
            gridViewTextBoxColumn25.HeaderText = "Descriptor";
            gridViewTextBoxColumn25.IsVisible = false;
            gridViewTextBoxColumn25.Name = "Descriptor";
            this.MetodoPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25});
            this.MetodoPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MetodoPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MetodoPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MetodoPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.MetodoPago.EditorControl.Name = "NestedRadGridView";
            this.MetodoPago.EditorControl.ReadOnly = true;
            this.MetodoPago.EditorControl.ShowGroupPanel = false;
            this.MetodoPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MetodoPago.EditorControl.TabIndex = 0;
            this.MetodoPago.Location = new System.Drawing.Point(105, 78);
            this.MetodoPago.Name = "MetodoPago";
            this.MetodoPago.NullText = "Método de Pago";
            this.MetodoPago.Size = new System.Drawing.Size(170, 20);
            this.MetodoPago.TabIndex = 439;
            this.MetodoPago.TabStop = false;
            this.MetodoPago.ValueMember = "Clave";
            // 
            // lblDecimales
            // 
            this.lblDecimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDecimales.Location = new System.Drawing.Point(1038, 6);
            this.lblDecimales.Name = "lblDecimales";
            this.lblDecimales.Size = new System.Drawing.Size(30, 18);
            this.lblDecimales.TabIndex = 445;
            this.lblDecimales.Text = "Dec.:";
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.DecimalPlaces = 2;
            this.TipoCambio.Location = new System.Drawing.Point(951, 30);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.Size = new System.Drawing.Size(54, 20);
            this.TipoCambio.TabIndex = 444;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFormaPago
            // 
            this.lblFormaPago.Location = new System.Drawing.Point(283, 79);
            this.lblFormaPago.Name = "lblFormaPago";
            this.lblFormaPago.Size = new System.Drawing.Size(84, 18);
            this.lblFormaPago.TabIndex = 440;
            this.lblFormaPago.Text = "Forma de Pago:";
            // 
            // Nota
            // 
            this.Nota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nota.Location = new System.Drawing.Point(840, 78);
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(291, 20);
            this.Nota.TabIndex = 431;
            // 
            // lblCondiciones
            // 
            this.lblCondiciones.Location = new System.Drawing.Point(553, 79);
            this.lblCondiciones.Name = "lblCondiciones";
            this.lblCondiciones.Size = new System.Drawing.Size(70, 18);
            this.lblCondiciones.TabIndex = 442;
            this.lblCondiciones.Text = "Condiciones:";
            // 
            // Decimales
            // 
            this.Decimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Decimales.Location = new System.Drawing.Point(1079, 5);
            this.Decimales.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.Decimales.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Name = "Decimales";
            this.Decimales.NullableValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Size = new System.Drawing.Size(52, 20);
            this.Decimales.TabIndex = 446;
            this.Decimales.TabStop = false;
            this.Decimales.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Decimales.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lblMetodoPago
            // 
            this.lblMetodoPago.Location = new System.Drawing.Point(5, 79);
            this.lblMetodoPago.Name = "lblMetodoPago";
            this.lblMetodoPago.Size = new System.Drawing.Size(93, 18);
            this.lblMetodoPago.TabIndex = 438;
            this.lblMetodoPago.Text = "Método de Pago:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(512, 5);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(211, 20);
            this.FechaEmision.TabIndex = 425;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // lblGuia
            // 
            this.lblGuia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGuia.Location = new System.Drawing.Point(553, 31);
            this.lblGuia.Name = "lblGuia";
            this.lblGuia.Size = new System.Drawing.Size(31, 18);
            this.lblGuia.TabIndex = 432;
            this.lblGuia.Text = "Guía:";
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEntrega.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaEntrega.Location = new System.Drawing.Point(819, 5);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.NullText = "--/--/----";
            this.FechaEntrega.ReadOnly = true;
            this.FechaEntrega.Size = new System.Drawing.Size(87, 20);
            this.FechaEntrega.TabIndex = 427;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "05/12/2017";
            this.FechaEntrega.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // lblMoneda
            // 
            this.lblMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneda.Location = new System.Drawing.Point(1008, 30);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 434;
            this.lblMoneda.Text = "Moneda:";
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(439, 6);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(70, 18);
            this.lblFechaEmision.TabIndex = 424;
            this.lblFechaEmision.Text = "Fec. Emisión:";
            // 
            // lblFechaEntrega
            // 
            this.lblFechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEntrega.Location = new System.Drawing.Point(729, 6);
            this.lblFechaEntrega.Name = "lblFechaEntrega";
            this.lblFechaEntrega.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEntrega.TabIndex = 426;
            this.lblFechaEntrega.Text = "Fec. de Entrega:";
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoCambio.Location = new System.Drawing.Point(886, 31);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(59, 18);
            this.lblTipoCambio.TabIndex = 443;
            this.lblTipoCambio.Text = "T. Cambio:";
            // 
            // lblNota
            // 
            this.lblNota.Location = new System.Drawing.Point(801, 79);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(33, 18);
            this.lblNota.TabIndex = 430;
            this.lblNota.Text = "Nota:";
            this.lblNota.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblEnvio
            // 
            this.lblEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnvio.Location = new System.Drawing.Point(699, 31);
            this.lblEnvio.Name = "lblEnvio";
            this.lblEnvio.Size = new System.Drawing.Size(36, 18);
            this.lblEnvio.TabIndex = 428;
            this.lblEnvio.Text = "Envío:";
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(204, 5);
            this.Folio.MaxLength = 16;
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.ReadOnly = true;
            this.Folio.Size = new System.Drawing.Size(82, 20);
            this.Folio.TabIndex = 423;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Creo
            // 
            this.Creo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Creo.Location = new System.Drawing.Point(951, 5);
            this.Creo.MaxLength = 10;
            this.Creo.Name = "Creo";
            this.Creo.NullText = "Creó";
            this.Creo.Size = new System.Drawing.Size(82, 20);
            this.Creo.TabIndex = 437;
            this.Creo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IdSerie
            // 
            this.IdSerie.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdSerie.Location = new System.Drawing.Point(67, 5);
            this.IdSerie.Name = "IdSerie";
            this.IdSerie.NullText = "Serie";
            this.IdSerie.Size = new System.Drawing.Size(86, 20);
            this.IdSerie.TabIndex = 421;
            // 
            // lblCreo
            // 
            this.lblCreo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCreo.Location = new System.Drawing.Point(912, 6);
            this.lblCreo.Name = "lblCreo";
            this.lblCreo.Size = new System.Drawing.Size(32, 18);
            this.lblCreo.TabIndex = 436;
            this.lblCreo.Text = "Creó:";
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn26.FieldName = "Clave";
            gridViewTextBoxColumn26.HeaderText = "Clave";
            gridViewTextBoxColumn26.Name = "Clave";
            gridViewTextBoxColumn27.FieldName = "Descripcion";
            gridViewTextBoxColumn27.HeaderText = "Descripción";
            gridViewTextBoxColumn27.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27});
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(1064, 30);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(67, 20);
            this.Moneda.TabIndex = 435;
            this.Moneda.TabStop = false;
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(28, 6);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(33, 18);
            this.lblSerie.TabIndex = 420;
            this.lblSerie.Text = "Serie:";
            // 
            // lblFolio
            // 
            this.lblFolio.Location = new System.Drawing.Point(160, 6);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 422;
            this.lblFolio.Text = "Folio:";
            // 
            // FormaPago
            // 
            this.FormaPago.AutoSizeDropDownToBestFit = true;
            this.FormaPago.DisplayMember = "Descriptor";
            this.FormaPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // FormaPago.NestedRadGridView
            // 
            this.FormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.FormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.FormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn28.FieldName = "Clave";
            gridViewTextBoxColumn28.HeaderText = "Clave";
            gridViewTextBoxColumn28.Name = "Clave";
            gridViewTextBoxColumn29.FieldName = "Descripcion";
            gridViewTextBoxColumn29.HeaderText = "Descripción";
            gridViewTextBoxColumn29.Name = "Descripcion";
            gridViewTextBoxColumn30.FieldName = "Descriptor";
            gridViewTextBoxColumn30.HeaderText = "Descriptor";
            gridViewTextBoxColumn30.IsVisible = false;
            gridViewTextBoxColumn30.Name = "Descriptor";
            this.FormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30});
            this.FormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.FormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition11;
            this.FormaPago.EditorControl.Name = "NestedRadGridView";
            this.FormaPago.EditorControl.ReadOnly = true;
            this.FormaPago.EditorControl.ShowGroupPanel = false;
            this.FormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.FormaPago.EditorControl.TabIndex = 0;
            this.FormaPago.Location = new System.Drawing.Point(373, 78);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.NullText = "Forma de pago";
            this.FormaPago.Size = new System.Drawing.Size(170, 20);
            this.FormaPago.TabIndex = 441;
            this.FormaPago.TabStop = false;
            this.FormaPago.ValueMember = "Clave";
            // 
            // MetodoEnvio
            // 
            this.MetodoEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MetodoEnvio.AutoSizeDropDownHeight = true;
            this.MetodoEnvio.AutoSizeDropDownToBestFit = true;
            this.MetodoEnvio.DisplayMember = "Descriptor";
            this.MetodoEnvio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MetodoEnvio.NestedRadGridView
            // 
            this.MetodoEnvio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MetodoEnvio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoEnvio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MetodoEnvio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn31.DataType = typeof(int);
            gridViewTextBoxColumn31.FieldName = "Id";
            gridViewTextBoxColumn31.HeaderText = "Clave";
            gridViewTextBoxColumn31.Name = "Id";
            gridViewTextBoxColumn32.FieldName = "Descriptor";
            gridViewTextBoxColumn32.HeaderText = "Descriptor";
            gridViewTextBoxColumn32.Name = "Descriptor";
            this.MetodoEnvio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32});
            this.MetodoEnvio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition12;
            this.MetodoEnvio.EditorControl.Name = "NestedRadGridView";
            this.MetodoEnvio.EditorControl.ReadOnly = true;
            this.MetodoEnvio.EditorControl.ShowGroupPanel = false;
            this.MetodoEnvio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MetodoEnvio.EditorControl.TabIndex = 0;
            this.MetodoEnvio.Location = new System.Drawing.Point(741, 30);
            this.MetodoEnvio.Name = "MetodoEnvio";
            this.MetodoEnvio.NullText = "Método de Envío";
            this.MetodoEnvio.Size = new System.Drawing.Size(138, 20);
            this.MetodoEnvio.TabIndex = 429;
            this.MetodoEnvio.TabStop = false;
            this.MetodoEnvio.ValueMember = "Id";
            // 
            // Guia
            // 
            this.Guia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Guia.Location = new System.Drawing.Point(588, 30);
            this.Guia.MaxLength = 32;
            this.Guia.Name = "Guia";
            this.Guia.NullText = "Guía";
            this.Guia.Size = new System.Drawing.Size(108, 20);
            this.Guia.TabIndex = 433;
            // 
            // TasaIVAPactado
            // 
            this.TasaIVAPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TasaIVAPactado.DecimalPlaces = 4;
            this.TasaIVAPactado.Location = new System.Drawing.Point(468, 186);
            this.TasaIVAPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.TasaIVAPactado.Name = "TasaIVAPactado";
            this.TasaIVAPactado.ShowBorder = false;
            this.TasaIVAPactado.ShowUpDownButtons = false;
            this.TasaIVAPactado.Size = new System.Drawing.Size(37, 20);
            this.TasaIVAPactado.TabIndex = 96;
            this.TasaIVAPactado.TabStop = false;
            // 
            // FactorPactado
            // 
            this.FactorPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FactorPactado.DecimalPlaces = 4;
            this.FactorPactado.Location = new System.Drawing.Point(468, 212);
            this.FactorPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.FactorPactado.Name = "FactorPactado";
            this.FactorPactado.ShowBorder = false;
            this.FactorPactado.ShowUpDownButtons = false;
            this.FactorPactado.Size = new System.Drawing.Size(37, 20);
            this.FactorPactado.TabIndex = 30;
            this.FactorPactado.TabStop = false;
            // 
            // IdVendedor
            // 
            this.IdVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdVendedor.Location = new System.Drawing.Point(468, 238);
            this.IdVendedor.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdVendedor.Name = "IdVendedor";
            this.IdVendedor.ShowBorder = false;
            this.IdVendedor.ShowUpDownButtons = false;
            this.IdVendedor.Size = new System.Drawing.Size(37, 20);
            this.IdVendedor.TabIndex = 30;
            this.IdVendedor.TabStop = false;
            // 
            // TDocumento
            // 
            this.TDocumento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDocumento.Location = new System.Drawing.Point(0, 0);
            this.TDocumento.Name = "TDocumento";
            this.TDocumento.ShowActualizar = true;
            this.TDocumento.ShowComplemento = false;
            this.TDocumento.ShowEnviar = true;
            this.TDocumento.ShowGuardar = true;
            this.TDocumento.ShowIdDocumento = false;
            this.TDocumento.ShowImprimir = true;
            this.TDocumento.ShowPDF = true;
            this.TDocumento.Size = new System.Drawing.Size(1138, 30);
            this.TDocumento.TabIndex = 0;
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // RemisionFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Grupo);
            this.Controls.Add(this.TDocumento);
            this.Name = "RemisionFiscalControl";
            this.Size = new System.Drawing.Size(1138, 135);
            this.Load += new System.EventHandler(this.RemisionFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grupo)).EndInit();
            this.Grupo.ResumeLayout(false);
            this.Grupo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCondiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGuia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGroupBox Grupo;
        internal Telerik.WinControls.UI.RadSpinEditor TasaIVAPactado;
        internal Telerik.WinControls.UI.RadSpinEditor FactorPactado;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdVendedor;
        public Common.Forms.TbDocumentoControl TDocumento;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Condiciones;
        internal Telerik.WinControls.UI.RadLabel lblRecibe;
        internal Telerik.WinControls.UI.RadDropDownList Recibe;
        internal Telerik.WinControls.UI.RadLabel lblPedido;
        internal Telerik.WinControls.UI.RadTextBox IdPedido;
        protected internal Telerik.WinControls.UI.RadLabel lblVersion;
        internal Telerik.WinControls.UI.RadLabel lblVendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Vendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MetodoPago;
        internal Telerik.WinControls.UI.RadLabel lblDecimales;
        internal Telerik.WinControls.UI.RadSpinEditor TipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblFormaPago;
        protected internal Telerik.WinControls.UI.RadTextBox Nota;
        internal Telerik.WinControls.UI.RadLabel lblCondiciones;
        internal Telerik.WinControls.UI.RadSpinEditor Decimales;
        internal Telerik.WinControls.UI.RadLabel lblMetodoPago;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblGuia;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        internal Telerik.WinControls.UI.RadLabel lblMoneda;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaEntrega;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblNota;
        internal Telerik.WinControls.UI.RadLabel lblEnvio;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadTextBox Creo;
        internal Telerik.WinControls.UI.RadDropDownList IdSerie;
        internal Telerik.WinControls.UI.RadLabel lblCreo;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadLabel lblSerie;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox FormaPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MetodoEnvio;
        internal Telerik.WinControls.UI.RadTextBox Guia;
        public UI.Contribuyentes.Forms.DirectorioControl Receptor;
        private System.Windows.Forms.ErrorProvider Advertencia;
    }
}
