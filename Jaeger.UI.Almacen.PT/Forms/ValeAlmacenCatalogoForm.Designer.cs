﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class ValeAlmacenCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValeAlmacenCatalogoForm));
            this.TVale = new Jaeger.UI.Almacen.PT.Forms.ValeAlmacenGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TVale
            // 
            this.TVale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TVale.Location = new System.Drawing.Point(0, 0);
            this.TVale.Name = "TVale";
            this.TVale.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TVale.Permisos = uiAction1;
            this.TVale.ShowActualizar = true;
            this.TVale.ShowAgrupar = false;
            this.TVale.ShowAutosuma = false;
            this.TVale.ShowCancelar = false;
            this.TVale.ShowCerrar = true;
            this.TVale.ShowEditar = true;
            this.TVale.ShowEjercicio = true;
            this.TVale.ShowExportarExcel = false;
            this.TVale.ShowFiltro = true;
            this.TVale.ShowHerramientas = false;
            this.TVale.ShowImprimir = true;
            this.TVale.ShowItem = true;
            this.TVale.ShowNuevo = true;
            this.TVale.ShowPeriodo = true;
            this.TVale.ShowSeleccionMultiple = true;
            this.TVale.ShowValues = true;
            this.TVale.Size = new System.Drawing.Size(800, 450);
            this.TVale.TabIndex = 0;
            // 
            // ValeAlmacenCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TVale);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ValeAlmacenCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Almacén PT: Vales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ValeAlmacenCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public ValeAlmacenGridControl TVale;
    }
}