﻿namespace Jaeger.UI.Almacen.PT.Forms
{
    partial class ProductoModeloBuscarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoModeloBuscarForm));
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.dataGridResult = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Producto / Servicio:";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowAgregar = true;
            this.ToolBar.ShowBuscar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowExistencia = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.Size = new System.Drawing.Size(755, 30);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.ButtonBuscar_Click += new System.EventHandler<System.EventArgs>(this.TModelo_Actualizar_Click);
            this.ToolBar.ButtonAgregar_Click += new System.EventHandler<System.EventArgs>(this.Modelos_DoubleClick);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TModelo_Cerrar_Click);
            // 
            // dataGridResult
            // 
            this.dataGridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridResult.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Categoria";
            gridViewTextBoxColumn1.HeaderText = "Catálogo";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Categoria";
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Producto";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 150;
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn3.Width = 200;
            gridViewTextBoxColumn4.FieldName = "Variante";
            gridViewTextBoxColumn4.HeaderText = "Especificación";
            gridViewTextBoxColumn4.Name = "Variante";
            gridViewTextBoxColumn4.Width = 150;
            gridViewTextBoxColumn5.FieldName = "Marca";
            gridViewTextBoxColumn5.HeaderText = "Marca";
            gridViewTextBoxColumn5.Name = "Marca";
            gridViewTextBoxColumn5.Width = 150;
            gridViewTextBoxColumn6.FieldName = "Unidad";
            gridViewTextBoxColumn6.HeaderText = "Unidad";
            gridViewTextBoxColumn6.Name = "Unidad";
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.FieldName = "Identificador";
            gridViewTextBoxColumn7.HeaderText = "Identificador";
            gridViewTextBoxColumn7.Name = "Identificador";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 100;
            this.dataGridResult.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.dataGridResult.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridResult.Name = "dataGridResult";
            this.dataGridResult.Size = new System.Drawing.Size(755, 429);
            this.dataGridResult.TabIndex = 1;
            this.dataGridResult.DoubleClick += new System.EventHandler(this.Modelos_DoubleClick);
            // 
            // ProductoModeloBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 459);
            this.Controls.Add(this.dataGridResult);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoModeloBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar Modelo";
            this.Load += new System.EventHandler(this.ProductoModeloBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal Common.Forms.ToolBarStandarBuscarControl ToolBar;
        protected internal Telerik.WinControls.UI.RadGridView dataGridResult;
    }
}
