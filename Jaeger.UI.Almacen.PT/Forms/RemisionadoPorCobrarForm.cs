﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Almacen.PT.Forms {
    public class RemisionadoPorCobrarForm : RemisionadoForm {
        public RemisionadoPorCobrarForm(UIMenuElement menuElement, IRemisionadoService service) : base(menuElement) {
            this._Service = service;
            this.Text = "Remisionado: Por Cobrar";
            this.TRemision.ShowPeriodo = false;
            this.TRemision.ShowEjercicio = false;
            this.TRemision.ShowEditar = false;
            this.TRemision.ShowNuevo = false;
        }

        public override void Actualizar() {
            this._DataSource = new BindingList<RemisionSingleModel>(
                this._Service.GetList<RemisionSingleModel>(
                    Aplication.Almacen.PT.Services.RemisionadoService.Query().WithIdStatus(Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Build()).ToList()
                );
        }
    }
}
