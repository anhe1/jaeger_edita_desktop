﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.UI.Almacen.PT.Builder;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class RemisionadoPartidaForm : RadForm {
        #region declaraciones
        protected internal IRemisionadoService _Service;
        protected internal BindingList<RemisionPartidaModel> _DataSource;
        protected internal RadMenuItem TImprimirComprobante = new RadMenuItem { Text = "Comprobante", Name = "TImprimirComprobante" };
        protected internal RadMenuItem TImprimirListado = new RadMenuItem { Text = "Listado", Name = "TImprimirListado" };
        protected internal RadMenuItem ContextMenuCancelar = new RadMenuItem() { Text = "Cancelar" };
        #endregion

        /// <summary>
        /// mostrar o ocultar los importes de los comprobantes
        /// </summary>
        public bool ShowValues { get; set; } = true;

        public RemisionadoPartidaForm() {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction("0000000000");
        }

        public RemisionadoPartidaForm(UIMenuElement menuElement, IRemisionadoService service) {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this._Service = service;
        }

        public virtual void RemisionPartidaForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.CreateView();

            this.TRemision.ShowExportarExcel = true;
            this.TRemision.ExportarExcel.Enabled = this.TRemision.Permisos.Exportar;

            this.TRemision.ShowNuevo = false;
            this.TRemision.Nuevo.Click += this.Nuevo_Click;
            this.TRemision.ShowEditar = false;
            this.TRemision.Editar.Click += this.Editar_Click;
            this.TRemision.ShowCancelar = false;
            this.TRemision.Cancelar.Click += this.Cancelar_Click;
            this.TRemision.Actualizar.Click += this.Actualizar_Click;
            this.TRemision.Cerrar.Click += this.Cerrar_Click;
            this.TRemision.Imprimir.Items.AddRange(this.TImprimirComprobante, this.TImprimirListado);
            this.TImprimirComprobante.Click += this.TImprimir_Comprobante_Click;
            this.TImprimirListado.Click += this.TImprimir_Listado_Click;

            this.TRemision.GridData.CellBeginEdit += CellBeginEdit;
            this.TRemision.GridData.CellEndEdit += CellEndEdit;
            this.TRemision.GridData.CellFormatting += CellFormatting;

            this.TRemision.GridData.AllowEditRow = true;
            this.TRemision.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
        }

        #region barra de herramientas
        /// <summary>
        /// nuevo comprobante
        /// </summary>
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// editar comprobante seleccionado
        /// </summary>
        public virtual void Editar_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// cancelar comprobante seleccionado
        /// </summary>
        public virtual void Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado.IdStatus == 0) {
                MessageBox.Show(this, "¡El comprobante seleccionado ya se encuentra cancelado!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else {
                if (MessageBox.Show(this, "Esta seguro de cancelar? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    var cancela = new RemisionStatusForm(seleccionado, this._Service);
                    cancela.Selected += this.Cancelar_Selected;
                    cancela.ShowDialog(this);
                }
            }
        }

        public virtual void Cancelar_Selected(object sender, IRemisionStatusDetailModel e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// actualizar vista
        /// </summary>
        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.ShowDialog(this);
            }
            this.TRemision.GridData.DataSource = this._DataSource;
        }

        /// <summary>
        /// imprimir listado
        /// </summary>
        public virtual void TImprimir_Listado_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// imprimir comprobante seleccionado
        /// </summary>
        public virtual void TImprimir_Comprobante_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// cerrar ventana 
        /// </summary>
        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        /// <summary>
        /// inicio de la edicion de la celda seleccionada
        /// </summary>
        public virtual void CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            
        }

        /// <summary>
        /// evento al terminar la edicion de la celda seleccionada
        /// </summary>
        public virtual void CellEndEdit(object sender, GridViewCellEventArgs e) {
            
        }

        /// <summary>
        /// formato de celdas
        /// </summary>
        public virtual void CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "UrlFilePDF") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status" && e.Column.Name != "FechaEntrega" && e.Column.Name != "FechaCobranza") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            
        }

        #endregion

        /// <summary>
        /// obtener comprobante seleccionado
        /// </summary>
        public virtual void GetComprobante() { }

        /// <summary>
        /// actualizar fecha de entrega al cliente
        /// </summary>
        public virtual void FechaEntrega() { }

        /// <summary>
        /// actualizar fecha de recepcion para cobranza
        /// </summary>
        public virtual void FechaCobranza() { }

        /// <summary>
        /// cancelacion del comprobante
        /// </summary>
        public virtual void Cancelar() { }

        /// <summary>
        /// actualizar el listado
        /// </summary>
        public virtual void Actualizar() {
            var d0 = Aplication.Almacen.PT.Services.RemisionadoService.Query().WithYear(this.TRemision.GetEjercicio()).WithMonth(this.TRemision.GetPeriodo()).Build();
            this._DataSource = new BindingList<RemisionPartidaModel>(this._Service.GetList<RemisionPartidaModel>(d0).ToList());
        }

        /// <summary>
        /// carga vista basica del grid
        /// </summary>
        public virtual void CreateView() {
            using (IRemisionadoGridBuilder builder = new RemisionadoGridBuilder()) {
                if (this.ShowValues) {
                    this.TRemision.GridData.Columns.AddRange(builder.Templetes().RemisionYConceptosConImportes().Build());
                } else {
                    this.TRemision.GridData.Columns.AddRange(builder.Templetes().RemisionYConceptosSinValores().Build());
                }
            }
        }
    }
}
