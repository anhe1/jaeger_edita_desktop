﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.UI.Almacen.PT.Builder;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class ValeAlmacenForm : RadForm {
        #region declaraciones
        protected internal IUnidadService Unidad;
        protected internal List<IUnidad> Unidades;
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };
        public RadContextMenu menuContextual = new RadContextMenu();
        #endregion

        public ValeAlmacenForm(IValeAlmacenService service) {
            InitializeComponent();
            this.TVale.Service = service;
        }

        private void ValeAlmacenForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            using (IAlmacenGridBuilder view = new AlmacenGridBuilder()) {
                this.Conceptos.Columns.AddRange(view.Templetes().Conceptos().Build());
            }
            this.menuContextual.Items.AddRange(this.menuContextAplicar, this.menuContextDuplicar);
            this.TVale.BindingCompleted += TVale_BindingCompleted;
            this.TVale.TDocumento.Cerrar.Click += this.Cerrar_Click;
            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;
            this.Conceptos.CellEndEdit += GridConceptos_CellEndEdit;
            this.PanelTotales.Visible = true;
            this.TVale.TVale_Actualizar_Click(sender, e);
        }

        private void GridConceptos_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "IdUnidad") {
                var seleccionado = this.Conceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoModel;
                if (seleccionado != null) {
                    GridViewComboBoxColumn comboBoxColumn = e.Column as GridViewComboBoxColumn;
                    var unidades = comboBoxColumn.DataSource as List<UnidadModel>;
                    var unidad = unidades.Where(it => it.IdUnidad == (int)e.Value).FirstOrDefault();
                    if (unidad != null) {
                        seleccionado.Unidad = unidad.Descripcion;
                        seleccionado.UnidadFactor = unidad.Factor;
                    }
                }
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region barra de herramientas conceptos del comprobante
        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.TVale.Comprobante.Conceptos.Count == 0) {
                this.TVale.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
                this.Conceptos.DataSource = this.TVale.Comprobante.Conceptos;
            }

            this.TVale.Comprobante.Conceptos.Add(new ValeAlmacenConceptoModel() { IdTipoMovimiento = this.TVale.Comprobante.IdTipoMovimiento });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.Conceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Remision_RemoverObjeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.Conceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoModel;
                    if (seleccionado.IdMovimiento == 0) {
                        try {
                            this.TVale.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.Conceptos.Rows.Count == 0) {
                                this.TVale.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.Conceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.Conceptos.CurrentRow != null) {
                var seleccionado = this.Conceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.TVale.Comprobante.Conceptos.Add(duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TVale.Comprobante.IdDirectorio == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var _catalogoProductos = new ProductoModeloBuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        public virtual void TConceptos_Agregar_Producto(object sender, ProductoXModelo e) {
            if (this.TVale.Comprobante.HasConcepts()) {
                this.Conceptos.DataSource = this.TVale.Comprobante.Conceptos;
                this.GridConceptoParte.DataSource = this.TVale.Comprobante.Conceptos;
            }

            if (e != null) {
                var nuevo = Aplication.Almacen.PT.Builder.ValeAlmacenBuilder.Build(e);
                if (this.SinDuplicados.IsChecked == true) {
                    if (this.TVale.Comprobante.AddConcepto(nuevo) == false) {
                        RadMessageBox.Show(this, "El producto o modelo ya se encuentra en la lista actual.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                } else {
                    this.TVale.Comprobante.Conceptos.Add(nuevo);
                }
            }
        }
        #endregion

        private void TVale_BindingCompleted(object sender, EventArgs e) {
            this.Conceptos.DataSource = this.TVale.Comprobante.Conceptos;
            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.TVale.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.TVale.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.TVale.Comprobante, "TotalTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.TVale.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TConceptos.IsEditable = this.TVale.Comprobante.IsEditable;
            this.TConceptoParte.IsEditable = this.TVale.Comprobante.IsEditable;
            GridViewComboBoxColumn comboUnidades = this.Conceptos.Columns["IdUnidad"] as GridViewComboBoxColumn;
            comboUnidades.DisplayMember = "Descripcion";
            comboUnidades.ValueMember = "IdUnidad";
            comboUnidades.AutoSizeMode = BestFitColumnMode.DisplayedCells;
            comboUnidades.DropDownStyle = RadDropDownStyle.DropDownList;
            comboUnidades.DataSource = this.Unidad.GetList<UnidadModel>(Aplication.Almacen.PT.Services.UnidadService.Query().ByAlmacen(Domain.Base.ValueObjects.AlmacenEnum.PT).Activo().Build());
        }
    }
}
