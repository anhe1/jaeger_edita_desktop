﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class DevolucionForm : RadForm {
        
        protected internal IUnidadService unidad;
        protected RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };

        public DevolucionForm(IValeAlmacenService service) {
            InitializeComponent();
            this.TDevolucion.Service = service;
        }

        private void DevolucionForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.TDevolucion.TDocumento.Cerrar.Click += this.Cerrar_Click;
            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;
            this.TDevolucion.Actualizar_Click(sender, e);
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region barra de herramientas conceptos del comprobante
        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.TDevolucion.Comprobante.Conceptos.Count == 0) {
                this.TDevolucion.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
                this.gConceptos.DataSource = this.TDevolucion.Comprobante.Conceptos;
            }

            //this.TDevolucion.Comprobante.Conceptos.Add(new ValeAlmacenConceptoModel() { IdEfecto = this.TDevolucion.Comprobante.IdEfecto });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.gConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Remision_RemoverObjeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.gConceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoModel;
                    if (seleccionado.IdMovimiento == 0) {
                        try {
                            this.TDevolucion.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.gConceptos.Rows.Count == 0) {
                                this.TDevolucion.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.gConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.gConceptos.CurrentRow != null) {
                var seleccionado = this.gConceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.TDevolucion.Comprobante.Conceptos.Add(duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TDevolucion.Comprobante.IdDirectorio == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var _catalogoProductos = new ProductoModeloBuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        public virtual void TConceptos_Agregar_Producto(object sender, ProductoXModelo e) {
            if (this.TDevolucion.Comprobante.Conceptos.Count == 0) {
                this.TDevolucion.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
                this.gConceptos.DataSource = this.TDevolucion.Comprobante.Conceptos;
                this.gConceptoParte.DataSource = this.TDevolucion.Comprobante.Conceptos;
                //this.GridConceptoParte.DataMember = "Parte";
            }
            if (e != null) {
                var nuevo = new ValeAlmacenConceptoModel {
                    IdProducto = e.IdProducto,
                    Identificador = e.Identificador,
                    IdModelo = e.IdModelo,
                    Activo = e.Activo,
                    Catalogo = e.Categoria,
                    Especificacion = e.Especificacion,
                    IdEspecificacion = e.IdEspecificacion,
                    IdUnidad = e.IdUnidad,
                    Marca = e.Marca,
                    Modelo = e.Descripcion,
                    Producto = e.Nombre,
                    Tamanio = e.Variante,
                    TasaIVA = e.TasaIVA,
                    Unidad = e.Unidad,
                    UnidadFactor = e.FactorUnidad,
                    //IdEfecto = this.TDevolucion.Comprobante.IdEfecto
                };

                if (this.SinDuplicados.IsChecked == true) {
                    var ed = this.TDevolucion.Comprobante.Conceptos.Where(it => it.Identificador == nuevo.Identificador);
                    if (ed.Count() > 0) {
                        RadMessageBox.Show(this, "El producto o modelo ya se encuentra en la lista actual.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }

                //try {
                //    var precio = this.lPrecios.Where(it => it.IdEspecificacion == e.IdEspecificacion).FirstOrDefault();
                //    if (precio != null) {
                //        nuevo.ValorUnitario = precio.Unitario;
                //        nuevo.IdPrecio = precio.IdPrecio;
                //        nuevo.ValorUnitario = precio.Unitario;
                //        nuevo.CostoUnitario = precio.Costo;
                //    }
                this.TDevolucion.Comprobante.Conceptos.Add(nuevo);
                //} catch (Exception ex) {
                //    RadMessageBox.Show(this, "No existe precio asignado a este producto, informe el error al administrador.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                //    Console.WriteLine(ex.ToString());
                //}
            }
        }
        #endregion

        private void TDevolucion_BindingCompleted(object sender, EventArgs e) {
            this.gConceptos.DataSource = this.TDevolucion.Comprobante.Conceptos;
            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.TDevolucion.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.TDevolucion.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.TDevolucion.Comprobante, "TotalTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.TDevolucion.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TConceptos.IsEditable = this.TDevolucion.Comprobante.IsEditable;
            this.TConceptoParte.IsEditable = this.TDevolucion.Comprobante.IsEditable;
        }
    }
}
