﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class RemisionadoClienteForm : RadForm {
        protected internal BackgroundWorker _ClientesLoad = new BackgroundWorker();

        public RemisionadoClienteForm(UIMenuElement menuElement, IRemisionadoService service) {
            InitializeComponent();
            this.TRemision._Service = service;
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void RemisionadoClienteForm_Load(object sender, EventArgs e) {
            this.Text = "Cliente: Remisiones por cliente";
            this.TRemision.Enabled = false;
            this.TRemision.ShowNuevo = false;
            this.TRemision.ShowEditar = false;
            this.TRemision.ShowPeriodo = false;
            this.TRemision.ShowAutosuma = true;
            this.TRemision.ItemLbl.Text = "Cliente: ";
            this.TRemision.ShowItem = true;
            this.TRemision.ItemHost.HostedItem = this.Nombre.MultiColumnComboBoxElement;
            this.TRemision.GridData.AllowEditRow = true;
            this.TRemision.ShowAutosuma = true;
            this.TRemision.ShowFiltro = true;
        
            this.Nombre.AutoFilter = true;
            this.Nombre.EditorControl.AllowRowResize = false;
            this.Nombre.EditorControl.AllowSearchRow = true;
            this.Nombre.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            this._ClientesLoad.DoWork += LoadClientes_DoWork;
            this._ClientesLoad.RunWorkerCompleted += LoadClientes_RunWorkerCompleted;
            this._ClientesLoad.RunWorkerAsync();

            this.TRemision.Actualizar.Click += this.TRemision_Actualizar_Click;
            this.TRemision.Cerrar.Click += this.TRemision_Cerrar_Click;
        }

        /// <summary>
        /// actualizar vista
        /// </summary>
        public virtual void TRemision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.ShowDialog(this);
            }
            this.TRemision.GridData.DataSource = this.TRemision._DataSource;
        }

        /// <summary>
        /// cerrar ventana 
        /// </summary>
        public virtual void TRemision_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        /// <summary>
        /// actualizar el listado
        /// </summary>
        public virtual void Actualizar() {
            if (this.Nombre.SelectedItem != null) {
                var seleccionado = ((GridViewDataRowInfo)this.Nombre.SelectedItem).DataBoundItem as RemisionReceptorModel;
                if (seleccionado != null) {
                    this.TRemision._DataSource = new BindingList<RemisionSingleModel>(
                        this.TRemision._Service.GetList<RemisionSingleModel>(
                            RemisionadoService.Query().WithYear(this.TRemision.GetEjercicio()).WithIdReceptor(seleccionado.IdCliente).Build()).ToList()
                        );
                }
            }
        }

        #region metodos privados
        private void LoadClientes_DoWork(object sender, DoWorkEventArgs e) {
            this.Nombre.DataSource = this.TRemision._Service.GetList<RemisionReceptorModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("RMSN_A", "1") }).ToList();
        }

        private void LoadClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.TRemision.Enabled = true;
            this.Nombre.EditorControl.FilterDescriptors.Add(new FilterDescriptor("ReceptorNombre", FilterOperator.Contains, ""));
            this.Nombre.AutoFilter = true;
        }
        #endregion
    }
}
