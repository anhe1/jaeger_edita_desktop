﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class DevolucionCatalogoForm : RadForm {
        #region declaraciones
        protected internal IValeAlmacenService Service;
        protected internal BindingList<DevolucionDetailModel> vales;
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal RadMenuItem Nuevo = new RadMenuItem { Text = "Nuevo", Name = "tcpvnt_brem_remision" };
        protected internal RadMenuItem ImprimirVale = new RadMenuItem { Text = "Comprobante", Name = "tcpvnt_brem_remision1" };
        #endregion

        /// <summary>
        /// mostrar o ocultar los importes de los comprobantes
        /// </summary>
        public bool ShowValues { get; set; } = true;

        public DevolucionCatalogoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TDevolucion.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void DevolucionCatalogoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.CreateView();

            this.Conceptos.Standard();
            this.TDevolucion.GridData.MasterTemplate.Templates.AddRange(this.Conceptos);

            this.TDevolucion.Nuevo.Click += this.Nuevo_Click;
            this.TDevolucion.Actualizar.Click += this.Actualizar_Click;
            this.TDevolucion.Imprimir.Items.Add(this.ImprimirVale);
            this.ImprimirVale.Click += this.ImprimirVale_Click;
            this.TDevolucion.Cerrar.Click += this.Cerrar_Click;

            this.TDevolucion.GridData.CellBeginEdit += this.CellBeginEdit;
            this.TDevolucion.GridData.CellEndEdit += this.CellEndEdit;
            this.TDevolucion.GridData.RowSourceNeeded += this.RowSourceNeeded;
            this.TDevolucion.Nuevo.Enabled = true;
        }

        #region barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new DevolucionForm(this.Service) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        public virtual void TAlmacen_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.TDevolucion.GridData.CurrentRow.DataBoundItem as ValeAlmacenDetailModel;
            if (seleccionado != null) {
                if (seleccionado.IdStatus == 0) {
                    MessageBox.Show(this, "¡El comprobante seleccionado ya se encuentra cancelado!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else {
                    if (MessageBox.Show(this, "Esta seguro de cancelar? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        var cancela = new ValeAlmacenStatusForm(this.Service, seleccionado);
                        cancela.Selected += this.Cancela_Selected;
                        cancela.ShowDialog(this);

                    }
                }
            }
        }

        private void Cancela_Selected(object sender, IValeAlmacenStatusDetailModel e) {
            if (e != null) {
                this.Tag = e;
                using (var espera = new Waiting1Form(this.Cancelar)) {
                    espera.Text = "Cancelando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.TDevolucion.GridData.DataSource = this.vales;
        }

        public virtual void ImprimirVale_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetPrinter)) {
                espera.Text = "Espera un momento ...";
                espera.ShowDialog(this);
            }
            var seleccionado = this.TDevolucion.GetCurrent<DevolucionDetailModel>();
            var printer = new DevolucionDetailPrinterModel(seleccionado);
            var reporte = new ReporteForm(printer);
            reporte.Show();
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {

        }

        public virtual void CellEndEdit(object sender, GridViewCellEventArgs e) {

        }

        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var _rowData = e.ParentRow.DataBoundItem as DevolucionDetailModel;
            if (_rowData != null) {
                if (e.Template.Caption == this.Conceptos.Caption) {
                    using (var espera = new Waiting1Form(this.GetComprobante)) {
                        espera.Text = "Consultando recibos ...";
                        espera.ShowDialog(this);
                    }

                    if (_rowData.Conceptos != null) {
                        foreach (var item in _rowData.Conceptos) {
                            var _row = e.Template.Rows.NewRow();
                            _row.Cells["CantidadS"].Value = item.Cantidad;
                            _row.Cells["Unidad"].Value = item.Unidad;
                            _row.Cells["IdPedido"].Value = item.IdPedido;
                            _row.Cells["Descripcion"].Value = item.Descripcion;
                            _row.Cells["Identificador"].Value = item.Identificador;
                            _row.Cells["Unitario"].Value = item.Unitario;
                            _row.Cells["SubTotal"].Value = item.SubTotal;
                            _row.Cells["Descuento"].Value = item.Descuento;
                            _row.Cells["Importe"].Value = item.Importe;
                            _row.Cells["TasaIVA"].Value = item.TasaIVA;
                            _row.Cells["TasladoIVA"].Value = item.TrasladoIVA;
                            _row.Cells["Total"].Value = item.Total;
                            e.SourceCollection.Add(_row);
                        }
                    }
                }
            }
        }

        public virtual void GetComprobante() {

        }

        public virtual void Consultar() {
         //   this.vales = this.Service.GetList(this.TDevolucion.GetEjercicio(), this.TDevolucion.GetPeriodo());
        }

        public virtual void GetPrinter() {
            var seleccionado = this.TDevolucion.GetCurrent<DevolucionDetailModel>();
            if (seleccionado != null) {
                var d = this.Service.GetPrinter(seleccionado.IdComprobante);
                if (d != null) {
                    //seleccionado.SetValues(d);
                }
            }
        }

        public virtual void Cancelar() {

        }

        public virtual void CreateView() {
            //this.TDevolucion.GridData.Columns.AddRange(GridValeAlmacenService.GetDevolucionColumns());
            //this.Conceptos.Columns.AddRange(GridRemisionService.GetColumnasConceptos(this.ShowValues));
            //var status = this.TDevolucion.GridData.Columns[GridTelerikCommon.ColStatus.Name] as GridViewComboBoxColumn;
            //status.DataSource = DevolucionService.GetStatus();
            //var tipoDocumento = this.TDevolucion.GridData.Columns[GridValeAlmacenService.ColIdTipo.Name] as GridViewComboBoxColumn;
            ////tipoDocumento.DataSource = AlmacenPTService.GetTipoOperacion();
            //tipoDocumento.ValueMember = "IdDocumento";
            //tipoDocumento.DisplayMember = "Descripcion";
        }
        #endregion
    }
}
