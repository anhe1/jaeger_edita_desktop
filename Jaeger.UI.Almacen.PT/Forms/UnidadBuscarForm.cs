﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class UnidadBuscarForm : RadForm {
        protected internal Aplication.Almacen.Contracts.IUnidadService Service;
        protected internal BindingList<UnidadModel> SourceData;

        public event EventHandler<UnidadModel> Selected;

        public void OnSelected(UnidadModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public UnidadBuscarForm() {
            InitializeComponent();
        }

        public virtual void UnidadBuscarForm_Load(object sender, EventArgs e) {
            this.gridUnidad.Standard();
            this.TUnidad.Agregar.Click += this.GridData_DoubleClick;
            this.TUnidad.Buscar.Click += this.TUnidad_Actualizar_Click;
            this.gridUnidad.DoubleClick += this.GridData_DoubleClick;
        }

        #region barra de herramientas
        public virtual void TUnidad_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.gridUnidad.DataSource = this.SourceData;
        }

        public virtual void TUnidad_Filtro_Click(object sender, EventArgs e) {
            this.gridUnidad.ShowFilteringRow = this.TUnidad.Filtro.ToggleState != ToggleState.On;
            if (this.gridUnidad.ShowFilteringRow == false)
                this.gridUnidad.FilterDescriptors.Clear();
        }

        public virtual void TUnidad_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_DoubleClick(object sender, EventArgs e) {
            if (this.gridUnidad.CurrentRow != null) {
                var seleccionado = this.gridUnidad.CurrentRow.DataBoundItem as Domain.Almacen.Entities.UnidadModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void Consultar() {
            var d0 = UnidadService.Query().ByAlmacen(this.Service.Almacen).Activo().Build();
            this.SourceData = new BindingList<UnidadModel>(this.Service.GetList<UnidadModel>(d0).ToList());
        }
        #endregion
    }
}
