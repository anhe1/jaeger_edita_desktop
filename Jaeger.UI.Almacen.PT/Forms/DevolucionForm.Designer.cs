﻿
namespace Jaeger.UI.Almacen.PT.Forms {
    partial class DevolucionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptos = new Telerik.WinControls.UI.RadPageViewPage();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.gConceptos = new Telerik.WinControls.UI.RadGridView();
            this.TConceptos = new Jaeger.UI.Common.Forms.ToolBarConceptoControl();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.TabConceptoParte = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoParte = new Telerik.WinControls.UI.RadPageViewPage();
            this.gConceptoParte = new Telerik.WinControls.UI.RadGridView();
            this.TConceptoParte = new Jaeger.UI.Common.Forms.ToolBarConceptoControl();
            this.PanelTotales = new Telerik.WinControls.UI.RadPanel();
            this.Total = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Descuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionIva = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SubTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblSubTotal = new Telerik.WinControls.UI.RadLabel();
            this.TrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblTotal = new Telerik.WinControls.UI.RadLabel();
            this.lblDescuentoTotal = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalIEPS = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalRetencionISR = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalIVA = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalRetencionIVA = new Telerik.WinControls.UI.RadLabel();
            this.TDevolucion = new Jaeger.UI.Almacen.PT.Forms.DevolucionControl();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.PageConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).BeginInit();
            this.TabConceptoParte.SuspendLayout();
            this.PageConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptoParte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptoParte.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).BeginInit();
            this.PanelTotales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescuentoTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.PageConceptos);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 134);
            this.TabControl.Margin = new System.Windows.Forms.Padding(1);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.PageConceptos;
            this.TabControl.Size = new System.Drawing.Size(1115, 366);
            this.TabControl.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            // 
            // PageConceptos
            // 
            this.PageConceptos.Controls.Add(this.radSplitContainer1);
            this.PageConceptos.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.PageConceptos.Location = new System.Drawing.Point(8, 32);
            this.PageConceptos.Name = "PageConceptos";
            this.PageConceptos.Size = new System.Drawing.Size(1099, 329);
            this.PageConceptos.Text = "Conceptos";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1099, 329);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.gConceptos);
            this.splitPanel1.Controls.Add(this.TConceptos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1099, 160);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1226415F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 59);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // gConceptos
            // 
            this.gConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gConceptos.MasterTemplate.AllowAddNewRow = false;
            this.gConceptos.MasterTemplate.AllowDeleteRow = false;
            this.gConceptos.MasterTemplate.AllowRowResize = false;
            this.gConceptos.MasterTemplate.EnableGrouping = false;
            this.gConceptos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gConceptos.Name = "gConceptos";
            this.gConceptos.Size = new System.Drawing.Size(1099, 130);
            this.gConceptos.TabIndex = 194;
            // 
            // TConceptos
            // 
            this.TConceptos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptos.IsEditable = true;
            this.TConceptos.Location = new System.Drawing.Point(0, 0);
            this.TConceptos.Margin = new System.Windows.Forms.Padding(1);
            this.TConceptos.Name = "TConceptos";
            this.TConceptos.ShowAgregar = true;
            this.TConceptos.ShowComboConceptos = true;
            this.TConceptos.ShowDuplicar = false;
            this.TConceptos.ShowNuevo = false;
            this.TConceptos.ShowProductos = true;
            this.TConceptos.ShowRemover = true;
            this.TConceptos.ShowUnidades = true;
            this.TConceptos.Size = new System.Drawing.Size(1099, 30);
            this.TConceptos.TabIndex = 195;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.TabConceptoParte);
            this.splitPanel2.Controls.Add(this.PanelTotales);
            this.splitPanel2.Location = new System.Drawing.Point(0, 164);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1099, 165);
            this.splitPanel2.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 165);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1226415F);
            this.splitPanel2.SizeInfo.MaximumSize = new System.Drawing.Size(0, 165);
            this.splitPanel2.SizeInfo.MinimumSize = new System.Drawing.Size(0, 165);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -59);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // TabConceptoParte
            // 
            this.TabConceptoParte.Controls.Add(this.PageConceptoParte);
            this.TabConceptoParte.DefaultPage = this.PageConceptoParte;
            this.TabConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.TabConceptoParte.Name = "TabConceptoParte";
            this.TabConceptoParte.SelectedPage = this.PageConceptoParte;
            this.TabConceptoParte.Size = new System.Drawing.Size(867, 165);
            this.TabConceptoParte.TabIndex = 169;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemCloseButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            // 
            // PageConceptoParte
            // 
            this.PageConceptoParte.Controls.Add(this.gConceptoParte);
            this.PageConceptoParte.Controls.Add(this.TConceptoParte);
            this.PageConceptoParte.ItemSize = new System.Drawing.SizeF(96F, 28F);
            this.PageConceptoParte.Location = new System.Drawing.Point(7, 33);
            this.PageConceptoParte.Name = "PageConceptoParte";
            this.PageConceptoParte.Size = new System.Drawing.Size(853, 125);
            this.PageConceptoParte.Text = "Concepto: Parte";
            // 
            // gConceptoParte
            // 
            this.gConceptoParte.AutoGenerateHierarchy = true;
            this.gConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gConceptoParte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gConceptoParte.MasterTemplate.AllowAddNewRow = false;
            this.gConceptoParte.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Cantidad";
            gridViewTextBoxColumn1.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn1.HeaderText = "Cantidad";
            gridViewTextBoxColumn1.Name = "Cantidad";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Unidad";
            gridViewTextBoxColumn2.HeaderText = "Unidad";
            gridViewTextBoxColumn2.Name = "Unidad";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn3.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn3.Name = "ClaveProdServ";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn4.HeaderText = "No. Ident.";
            gridViewTextBoxColumn4.Name = "NoIdentificacion";
            gridViewTextBoxColumn4.Width = 90;
            gridViewTextBoxColumn5.FieldName = "Descripcion";
            gridViewTextBoxColumn5.HeaderText = "Descripción";
            gridViewTextBoxColumn5.Name = "Descripcion";
            gridViewTextBoxColumn5.Width = 250;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "ValorUnitario";
            gridViewTextBoxColumn6.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn6.HeaderText = "Valor \n\rUnitario";
            gridViewTextBoxColumn6.Name = "ValorUnitario";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Importe";
            gridViewTextBoxColumn7.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn7.HeaderText = "Importe";
            gridViewTextBoxColumn7.Name = "Importe";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 80;
            this.gConceptoParte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.gConceptoParte.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gConceptoParte.Name = "gConceptoParte";
            this.gConceptoParte.ShowGroupPanel = false;
            this.gConceptoParte.Size = new System.Drawing.Size(853, 95);
            this.gConceptoParte.TabIndex = 0;
            // 
            // TConceptoParte
            // 
            this.TConceptoParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptoParte.IsEditable = true;
            this.TConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.TConceptoParte.Name = "TConceptoParte";
            this.TConceptoParte.ShowAgregar = false;
            this.TConceptoParte.ShowComboConceptos = true;
            this.TConceptoParte.ShowDuplicar = false;
            this.TConceptoParte.ShowNuevo = false;
            this.TConceptoParte.ShowProductos = false;
            this.TConceptoParte.ShowRemover = false;
            this.TConceptoParte.ShowUnidades = false;
            this.TConceptoParte.Size = new System.Drawing.Size(853, 30);
            this.TConceptoParte.TabIndex = 1;
            // 
            // PanelTotales
            // 
            this.PanelTotales.Controls.Add(this.Total);
            this.PanelTotales.Controls.Add(this.RetencionISR);
            this.PanelTotales.Controls.Add(this.Descuento);
            this.PanelTotales.Controls.Add(this.RetencionIva);
            this.PanelTotales.Controls.Add(this.SubTotal);
            this.PanelTotales.Controls.Add(this.TrasladoIEPS);
            this.PanelTotales.Controls.Add(this.lblSubTotal);
            this.PanelTotales.Controls.Add(this.TrasladoIVA);
            this.PanelTotales.Controls.Add(this.lblTotal);
            this.PanelTotales.Controls.Add(this.lblDescuentoTotal);
            this.PanelTotales.Controls.Add(this.lblTotalIEPS);
            this.PanelTotales.Controls.Add(this.lblTotalRetencionISR);
            this.PanelTotales.Controls.Add(this.lblTotalIVA);
            this.PanelTotales.Controls.Add(this.lblTotalRetencionIVA);
            this.PanelTotales.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotales.Location = new System.Drawing.Point(867, 0);
            this.PanelTotales.Name = "PanelTotales";
            this.PanelTotales.Size = new System.Drawing.Size(232, 165);
            this.PanelTotales.TabIndex = 170;
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(97, 134);
            this.Total.Mask = "n4";
            this.Total.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Total.Name = "Total";
            this.Total.NullText = "SubTotal";
            this.Total.Size = new System.Drawing.Size(125, 20);
            this.Total.TabIndex = 7;
            this.Total.TabStop = false;
            this.Total.Text = "0.0000";
            this.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionISR
            // 
            this.RetencionISR.Location = new System.Drawing.Point(97, 113);
            this.RetencionISR.Mask = "n4";
            this.RetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.NullText = "SubTotal";
            this.RetencionISR.Size = new System.Drawing.Size(125, 20);
            this.RetencionISR.TabIndex = 6;
            this.RetencionISR.TabStop = false;
            this.RetencionISR.Text = "0.0000";
            this.RetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Descuento
            // 
            this.Descuento.Location = new System.Drawing.Point(97, 29);
            this.Descuento.Mask = "n4";
            this.Descuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Descuento.Name = "Descuento";
            this.Descuento.NullText = "SubTotal";
            this.Descuento.Size = new System.Drawing.Size(125, 20);
            this.Descuento.TabIndex = 2;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "0.0000";
            this.Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionIva
            // 
            this.RetencionIva.Location = new System.Drawing.Point(97, 92);
            this.RetencionIva.Mask = "n4";
            this.RetencionIva.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionIva.Name = "RetencionIva";
            this.RetencionIva.NullText = "SubTotal";
            this.RetencionIva.Size = new System.Drawing.Size(125, 20);
            this.RetencionIva.TabIndex = 5;
            this.RetencionIva.TabStop = false;
            this.RetencionIva.Text = "0.0000";
            this.RetencionIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SubTotal
            // 
            this.SubTotal.Location = new System.Drawing.Point(97, 8);
            this.SubTotal.Mask = "n4";
            this.SubTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.NullText = "SubTotal";
            this.SubTotal.Size = new System.Drawing.Size(125, 20);
            this.SubTotal.TabIndex = 1;
            this.SubTotal.TabStop = false;
            this.SubTotal.Text = "0.0000";
            this.SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.Location = new System.Drawing.Point(97, 71);
            this.TrasladoIEPS.Mask = "n4";
            this.TrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.NullText = "SubTotal";
            this.TrasladoIEPS.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIEPS.TabIndex = 4;
            this.TrasladoIEPS.TabStop = false;
            this.TrasladoIEPS.Text = "0.0000";
            this.TrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Location = new System.Drawing.Point(16, 9);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(50, 18);
            this.lblSubTotal.TabIndex = 179;
            this.lblSubTotal.Text = "SubTotal";
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.Location = new System.Drawing.Point(97, 50);
            this.TrasladoIVA.Mask = "n4";
            this.TrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.NullText = "SubTotal";
            this.TrasladoIVA.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIVA.TabIndex = 3;
            this.TrasladoIVA.TabStop = false;
            this.TrasladoIVA.Text = "0.0000";
            this.TrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(16, 135);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 18);
            this.lblTotal.TabIndex = 182;
            this.lblTotal.Text = "Total";
            // 
            // lblDescuentoTotal
            // 
            this.lblDescuentoTotal.Location = new System.Drawing.Point(16, 30);
            this.lblDescuentoTotal.Name = "lblDescuentoTotal";
            this.lblDescuentoTotal.Size = new System.Drawing.Size(59, 18);
            this.lblDescuentoTotal.TabIndex = 176;
            this.lblDescuentoTotal.Text = "Descuento";
            // 
            // lblTotalIEPS
            // 
            this.lblTotalIEPS.Location = new System.Drawing.Point(16, 72);
            this.lblTotalIEPS.Name = "lblTotalIEPS";
            this.lblTotalIEPS.Size = new System.Drawing.Size(27, 18);
            this.lblTotalIEPS.TabIndex = 178;
            this.lblTotalIEPS.Text = "IEPS";
            // 
            // lblTotalRetencionISR
            // 
            this.lblTotalRetencionISR.Location = new System.Drawing.Point(16, 114);
            this.lblTotalRetencionISR.Name = "lblTotalRetencionISR";
            this.lblTotalRetencionISR.Size = new System.Drawing.Size(75, 18);
            this.lblTotalRetencionISR.TabIndex = 181;
            this.lblTotalRetencionISR.Text = "Retención ISR";
            // 
            // lblTotalIVA
            // 
            this.lblTotalIVA.Location = new System.Drawing.Point(16, 51);
            this.lblTotalIVA.Name = "lblTotalIVA";
            this.lblTotalIVA.Size = new System.Drawing.Size(24, 18);
            this.lblTotalIVA.TabIndex = 177;
            this.lblTotalIVA.Text = "IVA";
            // 
            // lblTotalRetencionIVA
            // 
            this.lblTotalRetencionIVA.Location = new System.Drawing.Point(16, 93);
            this.lblTotalRetencionIVA.Name = "lblTotalRetencionIVA";
            this.lblTotalRetencionIVA.Size = new System.Drawing.Size(77, 18);
            this.lblTotalRetencionIVA.TabIndex = 180;
            this.lblTotalRetencionIVA.Text = "Retención IVA";
            // 
            // TDevolucion
            // 
            this.TDevolucion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDevolucion.Location = new System.Drawing.Point(0, 0);
            this.TDevolucion.Name = "TDevolucion";
            this.TDevolucion.ReadOnly = false;
            this.TDevolucion.Size = new System.Drawing.Size(1115, 134);
            this.TDevolucion.TabIndex = 0;
            this.TDevolucion.BindingCompleted += new System.EventHandler<System.EventArgs>(this.TDevolucion_BindingCompleted);
            // 
            // DevolucionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 500);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.TDevolucion);
            this.Name = "DevolucionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ValeAlmacenForm";
            this.Load += new System.EventHandler(this.DevolucionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.PageConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).EndInit();
            this.TabConceptoParte.ResumeLayout(false);
            this.PageConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gConceptoParte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptoParte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            this.PanelTotales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescuentoTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal DevolucionControl TDevolucion;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageConceptos;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        protected internal Telerik.WinControls.UI.RadGridView gConceptos;
        protected internal Common.Forms.ToolBarConceptoControl TConceptos;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        internal Telerik.WinControls.UI.RadPageView TabConceptoParte;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoParte;
        protected internal Telerik.WinControls.UI.RadGridView gConceptoParte;
        protected internal Common.Forms.ToolBarConceptoControl TConceptoParte;
        private Telerik.WinControls.UI.RadPanel PanelTotales;
        private Telerik.WinControls.UI.RadMaskedEditBox Total;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox Descuento;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionIva;
        private Telerik.WinControls.UI.RadMaskedEditBox SubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIEPS;
        internal Telerik.WinControls.UI.RadLabel lblSubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIVA;
        internal Telerik.WinControls.UI.RadLabel lblTotal;
        internal Telerik.WinControls.UI.RadLabel lblDescuentoTotal;
        internal Telerik.WinControls.UI.RadLabel lblTotalIEPS;
        internal Telerik.WinControls.UI.RadLabel lblTotalRetencionISR;
        internal Telerik.WinControls.UI.RadLabel lblTotalIVA;
        internal Telerik.WinControls.UI.RadLabel lblTotalRetencionIVA;
    }
}