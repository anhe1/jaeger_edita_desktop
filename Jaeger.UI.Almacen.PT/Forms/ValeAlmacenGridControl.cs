﻿using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.UI.Almacen.PT.Forms {
    public class ValeAlmacenGridControl : Common.Forms.GridCommonControl {
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal GridViewTemplate Relaciones = new GridViewTemplate() { Caption = "Relaciones" };
        protected internal GridViewTemplate Status = new GridViewTemplate() { Caption = "Cambio de Status" };
        public RadMultiColumnComboBox Almacen = new RadMultiColumnComboBox();
        public ValeAlmacenGridControl() : base() {
            this.Load += ValesAlmacenControl_Load;
        }

        private void ValesAlmacenControl_Load(object sender, System.EventArgs e) {
            this.ShowItem = true;
            this.CreateComboAlmacen();
            this.ItemLbl.Text = "Almacén:  ";
            this.ItemHost.HostedItem = this.Almacen.MultiColumnComboBoxElement;
            using (IAlmacenGridBuilder view = new AlmacenGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Master().Build());
                this.Conceptos.Columns.AddRange(view.Templetes().Conceptos().Build());
                this.Relaciones.Columns.AddRange(view.Templetes().Relacion().Build());
                this.Status.Columns.AddRange(view.Templetes().CStatus().Build());
            }
            this.Conceptos.Columns["IdUnidad"].IsVisible = false;
            this.Conceptos.Columns["IdUnidad"].VisibleInColumnChooser = false;
            this.Conceptos.Standard();
            this.Relaciones.Standard();
            this.Status.Standard();

            this.GridData.MasterTemplate.Templates.AddRange(this.Conceptos);
            this.GridData.MasterTemplate.Templates.AddRange(this.Relaciones);
            this.GridData.MasterTemplate.Templates.AddRange(this.Status);
        }

        /// <summary>
        /// mostrar o ocultar los importes de los comprobantes
        /// </summary>
        public bool ShowValues { get; set; } = true;

        public virtual IAlmacenModel GetAlmacen() {
            if (this.Almacen.SelectedItem != null)
            return ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            return null;
        }

        private void CreateComboAlmacen() {
            this.Controls.Add(this.Almacen);
            this.Almacen.AutoSizeDropDownToBestFit = true;
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Almacen.EditorControl.Location = new System.Drawing.Point(0, 0);
            this.Almacen.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Almacen.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Almacen.EditorControl.MasterTemplate.AllowColumnChooser = false;
            GridViewTextBoxColumn idAlmacen = new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdAlmacen",
                HeaderText = "IdAlmacen",
                IsVisible = false,
                Name = "IdAlmacen"
            };
            GridViewTextBoxColumn descripcion = new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion"
            };
            GridViewTextBoxColumn descriptor = new GridViewTextBoxColumn {
                FieldName = "Descriptor",
                HeaderText = "Descriptor",
                IsVisible = false,
                Name = "Descriptor"
            };
            this.Almacen.EditorControl.MasterTemplate.Columns.AddRange(new GridViewDataColumn[] { idAlmacen, descripcion, descriptor });
            this.Almacen.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Almacen.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Almacen.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Almacen.EditorControl.Name = "NestedRadGridView";
            this.Almacen.EditorControl.ReadOnly = true;
            this.Almacen.EditorControl.ShowGroupPanel = false;
            this.Almacen.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Almacen.EditorControl.MinimumSize = new System.Drawing.Size(240, 150);
            this.Almacen.Location = new System.Drawing.Point(36, 4);
            this.Almacen.Name = "Almacen";
            this.Almacen.NullText = "Almacén";
            this.Almacen.Size = new System.Drawing.Size(151, 20);
            this.Almacen.ValueMember = "IdDocumento";
            this.ItemHost.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
        }
    }
}
