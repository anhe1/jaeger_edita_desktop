﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Almacen.PT.Forms {
    public class UnidadCatalogoForm : UI.Almacen.Forms.UnidadCatalogoForm {
        public UnidadCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Service = new Aplication.Almacen.Services.UnidadService(Domain.Base.ValueObjects.AlmacenEnum.PT);
        }
    }
}
