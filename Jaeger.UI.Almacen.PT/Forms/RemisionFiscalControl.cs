﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class RemisionFiscalControl : UserControl {
        private string _Mensaje = string.Empty;
        private bool _ReadOnly = false;
        public RemisionDetailModel Comprobante;
        public Aplication.Almacen.PT.Contracts.IRemisionService Service;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }

        #endregion

        public RemisionFiscalControl() {
            InitializeComponent();
        }

        private void RemisionFiscalControl_Load(object sender, EventArgs e) {
            this.MetodoEnvio.DataSource = RemisionService.GetMetodoEnvio();
            this.MetodoEnvio.SelectedIndex = -1;
            this.MetodoEnvio.SelectedValue = null;

            this.Condiciones.DataSource = RemisionService.GetCondicionPago();

            this.TDocumento.Status.DataSource = RemisionService.GetStatus();
            this.TDocumento.Status.DisplayMember = "Descriptor";
            this.TDocumento.Status.ValueMember = "Id";
            this.Receptor.Seleccionado += this.Receptor_Seleccionado;
            this.Receptor.ShowSearch = true;
            this.Receptor.Domicilio.DisplayMember = "Completo";
            this.Guia.KeyPress += TelerikTextBoxExtension.TxbFolio_KeyPress;
            if (ConfigService.Synapsis != null) {
                this.TDocumento.Emisor.Text = ConfigService.Synapsis.Empresa.RFC;
            } else {
                this.TDocumento.Emisor.Text = "XXXXXXXXXXXXXX";
            }

            this.TDocumento.Nuevo.Click += this.Nuevo_Click;
            this.TDocumento.Actualizar.Click += this.Actualizar_Click;
            this.TDocumento.Guardar.Click += this.Guardar_Click;
            this.TDocumento.Imprimir.Click += this.Imprimir_Click;
        }

        private void Receptor_Seleccionado(object sender, Domain.Contribuyentes.Entities.ContribuyenteDetailModel e) {
            if (e != null) {
                this.Vendedor.DataSource = e.Vendedores;
                if (e.DiasCredito > 0) {
                    if (e.Credito > 0) {
                        this.Comprobante.CondicionPago = RemisionService.GetCondicionPago((int)RemisionCondicionesPagoEnum.ConCredito).Descriptor;
                    } else {
                        this.Comprobante.CondicionPago = RemisionService.GetCondicionPago((int)RemisionCondicionesPagoEnum.PagoContraEntrega).Descriptor;
                    }
                }
            }
        }

        public bool ReadOnly {
            get { return _ReadOnly; }
            set {
                _ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        #region barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.TDocumento.Actualizar.PerformClick();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Comprobante = (RemisionDetailModel)RemisionService.Create().Build();
            } else if (this.Comprobante.IdRemision > 0) {
                using (var espera = new Waiting1Form(this.Cargar)) {
                    espera.ShowDialog(this);
                }
            }

            if (this.Comprobante.IsEditable) {
                this.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente;
                this.Receptor.Init();
            }

            this.CreateBinding();
        }

        public virtual void Guardar_Click(object sender, EventArgs e) {
            if (this.Comprobante.IdPedido == 0) {
                if (RadMessageBox.Show(this, "No se asigno un número de pedido, ¿Esta seguro de continuar?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                    return;
                }
            }

            if (this.Validar() == false) {
                RadMessageBox.Show(this, Properties.Resources.msg_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                if (!string.IsNullOrEmpty(this._Mensaje)) {
                    RadMessageBox.Show(this, this._Mensaje, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
                return;
            }

            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.ShowDialog(this);
            }
        }

        public virtual void Imprimir_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, Properties.Resources.msg_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                if (!string.IsNullOrEmpty(this._Mensaje)) {
                    RadMessageBox.Show(this, this._Mensaje, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
                return;
            }

            using (var espera = new Waiting2Form(this.GetPrinter)) {
                espera.ShowDialog(this);
            }

            if (!string.IsNullOrEmpty(this.Comprobante.IdDocumento)) {
                if (this.Comprobante.Tag != null) {
                    var reporte = new ReporteForm((RemisionDetailPrinter)this.Comprobante.Tag);
                    reporte.Show();
                }
            }
            this.Actualizar_Click(sender, e);
        }
        #endregion

        #region metodos privados
        public virtual void CreateBinding() {
            this.TDocumento.Status.DataBindings.Clear();
            this.TDocumento.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.lblVersion.DataBindings.Clear();
            this.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Folio.DataBindings.Clear();
            this.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdSerie.DataBindings.Clear();
            this.IdSerie.SetEditable(this.Comprobante.IsEditable);
            this.IdSerie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Nombre.DataBindings.Clear();
            this.Receptor.Nombre.SetEditable(this.Comprobante.IsEditable);
            this.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.RFC.DataBindings.Clear();
            this.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.IdDocumento.DataBindings.Clear();
            this.TDocumento.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Clave.DataBindings.Clear();
            this.Receptor.Clave.DataBindings.Add("Text", this.Comprobante, "ReceptorClave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDirectorio.DataBindings.Clear();
            this.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDomicilio.DataBindings.Clear();
            this.Receptor.IdDomicilio.SetEditable(this.Comprobante.IsEditable);
            this.Receptor.IdDomicilio.DataBindings.Add("Value", this.Comprobante, "IdDomicilio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Domicilio.DataBindings.Clear();
            this.Receptor.Domicilio.SetEditable(this.Comprobante.IsEditable);
            this.Receptor.Domicilio.DataBindings.Add("Text", this.Comprobante, "DomicilioEntrega", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FactorPactado.DataBindings.Clear();
            this.FactorPactado.DataBindings.Add("Value", this.Comprobante, "FactorPactado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TasaIVAPactado.DataBindings.Clear();
            this.TasaIVAPactado.DataBindings.Add("Value", this.Comprobante, "TasaIVAPactado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Recibe.DataBindings.Clear();
            this.Recibe.ReadOnly = !this.Comprobante.IsEditable;
            this.Recibe.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Guia.DataBindings.Clear();
            this.Guia.ReadOnly = !this.Comprobante.IsEditable;
            this.Guia.DataBindings.Add("Text", this.Comprobante, "NoGuia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Vendedor.DataBindings.Clear();
            this.Vendedor.SetEditable(this.Comprobante.IsEditable);
            this.Vendedor.DataBindings.Add("Text", this.Comprobante, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);
            this.IdVendedor.DataBindings.Clear();
            this.IdVendedor.DataBindings.Add("Value", this.Comprobante, "IdVendedor", false, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoCambio.DataBindings.Clear();
            this.TipoCambio.SetEditable(this.Comprobante.IsEditable);
            this.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Decimales.DataBindings.Clear();
            this.Decimales.SetEditable(this.Comprobante.IsEditable);
            this.Decimales.DataBindings.Add("Value", this.Comprobante, "PrecisionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nota.DataBindings.Clear();
            this.Nota.ReadOnly = !this.Comprobante.IsEditable;
            this.Nota.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.MetodoEnvio.DataBindings.Clear();
            this.MetodoEnvio.SetEditable(this.Comprobante.IsEditable);
            this.MetodoEnvio.DataBindings.Add("SelectedValue", this.Comprobante, "IdMetodoEnvio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.MetodoPago.DataBindings.Clear();
            this.MetodoPago.SetEditable(this.Comprobante.IsEditable);
            this.MetodoPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FormaPago.DataBindings.Clear();
            this.FormaPago.SetEditable(this.Comprobante.IsEditable);
            this.FormaPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Condiciones.DataBindings.Clear();
            this.Condiciones.SetEditable(this.Comprobante.IsEditable);
            this.Condiciones.DataBindings.Add("Text", this.Comprobante, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.SetEditable(this.Comprobante.IsEditable);
            this.Moneda.DataBindings.Add("Text", this.Comprobante, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEntrega.DataBindings.Clear();
            this.FechaEntrega.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            this.FechaEntrega.DateTimePickerElement.ReadOnly = true;
            this.FechaEntrega.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.FechaEntrega.DataBindings.Add("Value", this.Comprobante, "FechaEntrega", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaEntrega.SetEditable(this.Comprobante.IsEditable);

            this.IdPedido.DataBindings.Clear();
            this.IdPedido.DataBindings.Add("Text", this.Comprobante, "IdPedido", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Creo.DataBindings.Clear();
            this.Creo.ReadOnly = true;
            this.Creo.DataBindings.Add("Text", this.Comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.FilePDF.DataBindings.Clear();
            this.TDocumento.FilePDF.DataBindings.Add("Enabled", this.Comprobante, "IdRemision", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TDocumento.Imprimir.DataBindings.Clear();
            this.TDocumento.Imprimir.DataBindings.Add("Enabled", this.Comprobante, "IdRemision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.Guardar.DataBindings.Clear();
            this.TDocumento.Guardar.DataBindings.Add("Enabled", this.Comprobante, "IsEditable", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.Receptor.ReadOnly = !this.Comprobante.IsEditable;

            this.OnBindingClompleted(null);

            this.TDocumento.Guardar.Enabled = this.Comprobante.IsEditable;
        }

        private void CreateReadOnly() { }

        private void Cargar() {
            this.Comprobante = this.Service.GetById(this.Comprobante.IdRemision);
        }

        private void Guardar() {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        public virtual void GetPrinter() {
            var d0 = this.Service.GetPrinter(this.Comprobante.IdRemision);
            this.Comprobante.IdDocumento = d0.IdDocumento;
            this.Comprobante.Tag = d0;
        }

        public virtual bool Validar() {
            this.Advertencia.Clear();
            if (this.Comprobante.IdCliente == 0 || string.IsNullOrEmpty(this.Comprobante.ReceptorNombre)) {
                this.Receptor.Advertencia.SetError(this.Receptor.Nombre, "Selecciona un receptor válido para este comprobante.");
                return false;
            }

            if (this.Comprobante.Conceptos != null) {
                if (this.Comprobante.Conceptos.Count == 0) {
                    this._Mensaje = "Faltan conceptos para este comprobante";
                    return false;
                }

                if (this.Comprobante.Conceptos.Where(it => it.Cantidad == 0).Count() > 0) {
                    this._Mensaje = "Uno o más conceptos no tienen una cantidad válida.";
                    return false;
                }
            } else {
                this._Mensaje = "Objeto no válido.";
                return false;
            }

            if (string.IsNullOrEmpty(this.Comprobante.Contacto)) {
                this.Advertencia.SetError(this.Recibe, "Por favor ingresa un nombre o referencia.");
                return false;
            }
            this._Mensaje = string.Empty;
            return true;
        }
        #endregion
    }
}
