﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Util;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.PT.Forms {
    /// <summary>
    /// Formulario para listado de remisiones fiscales al cliente
    /// </summary>
    public partial class RemisionadoForm : RadForm {
        #region declaraciones
        protected internal IRemisionadoService _Service;
        protected internal IConfiguration _Configuration;
        protected internal BindingList<RemisionSingleModel> _DataSource;
        protected internal RadMenuItem TImprimirComprobante = new RadMenuItem { Text = "Comprobante", Name = "TImprimirComprobante" };
        protected internal RadMenuItem TImprimirListado = new RadMenuItem { Text = "Listado", Name = "TImprimirListado" };
        protected internal RadMenuItem TAcciones = new RadMenuItem { Text = "Acciones" };
        protected internal RadMenuItem TCargarPDF = new RadMenuItem { Text = "Cargar" };
        protected internal RadMenuItem TDescargaPDF = new RadMenuItem { Text = "Descargar" };
        protected internal RadMenuItem _PorCobrar = new RadMenuItem { Text = "Por Cobrar", Name = "adm_gcli_remxcobrar" };
        protected internal RadMenuItem _PorPartida = new RadMenuItem { Text = "Por Partidas", Name = "adm_gcli_remxpartida" };
        protected internal RadMenuItem _PorCliente = new RadMenuItem() { Text = "Por Cliente", Name = "adm_gcli_porcliente" };
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal GridViewTemplate Autorizacion = new GridViewTemplate() { Caption = "Cambios de Status" };
        protected internal GridViewTemplate Relacion = new GridViewTemplate() { Caption = "Remisión Relacionada" };
        protected internal UIMenuElement _MenuElement;
        protected internal UIMenuElement _MenuPorPartida;
        protected internal UIMenuElement _MenuPorCobrar;
        protected internal UIMenuElement _MenuPorCliente;
        private readonly string[] _columns = new string[] { "FechaEntrega", "FechaCobranza" };
        #endregion

        /// <summary>
        /// mostrar o ocultar los importes de los comprobantes
        /// </summary>
        public bool ShowValues { get; set; } = true;

        public RemisionadoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this._MenuElement = menuElement;
            this.Initialization();
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.CreateView();
            this.TRemision.GridData.AllowEditRow = this.TRemision.Permisos.Status;
            this.TRemision.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.Conceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.Conceptos);
            this.Autorizacion.HierarchyDataProvider = new GridViewEventDataProvider(this.Autorizacion);
            this.Relacion.HierarchyDataProvider = new GridViewEventDataProvider(this.Relacion);
        }

        #region barra de herramientas
        /// <summary>
        /// nuevo comprobante
        /// </summary>
        public virtual void TRemision_Nuevo_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// editar comprobante seleccionado
        /// </summary>
        public virtual void TRemision_Editar_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// cancelar comprobante seleccionado
        /// </summary>
        public virtual void TRemision_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado == null)
                return;
            if (seleccionado.IdStatus == 0) {
                RadMessageBox.Show(this, "¡El comprobante seleccionado ya se encuentra cancelado!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
            } else {
                if (RadMessageBox.Show(this, "Esta seguro de cancelar? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    var cancela = new RemisionStatusForm(seleccionado, this._Service);
                    cancela.Selected += this.TRemision_Cancelar_Selected;
                    cancela.ShowDialog(this);
                }
            }
        }

        public virtual void TRemision_Cancelar_Selected(object sender, IRemisionStatusDetailModel e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado == null)
                return;
            seleccionado.IdStatus = e.IdStatusB;
            seleccionado.Autorizaciones.Add(e as RemisionStatusModel);
            if (e.Relaciones != null) {
                if (seleccionado.RemisionRelacionada == null)
                    seleccionado.RemisionRelacionada = new BindingList<RemisionRelacionadaModel>();
                for (int i = 0; i < e.Relaciones.Count; i++) {
                    e.Relaciones[i].Relacion = e.CvMotivo;
                    seleccionado.RemisionRelacionada.Add(e.Relaciones[i] as RemisionRelacionadaModel);
                }
            }
            this._Service.Save(seleccionado);
        }

        /// <summary>
        /// actualizar vista
        /// </summary>
        public virtual void TRemision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.ShowDialog(this);
            }
            this.TRemision.GridData.DataSource = this._DataSource;
        }

        /// <summary>
        /// imprimir listado
        /// </summary>
        public virtual void TImprimir_Listado_Click(object sender, EventArgs e) {
            var seleccion = this.TRemision.GetFiltro<RemisionSingleModel>();
            var printer = new List<RemisionDetailPrinter>();

            if (seleccion.Count > 0) {
                foreach (var item in seleccion) {
                    printer.Add(new RemisionDetailPrinter(item));
                }

                var _reporte = new ReporteForm(printer);
                _reporte.Show();
            }
        }

        /// <summary>
        /// imprimir comprobante seleccionado
        /// </summary>
        public virtual void TImprimir_Comprobante_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (!string.IsNullOrEmpty(seleccionado.IdDocumento)) {

                using (var espera = new Waiting1Form(this.GetPrinter)) {
                    espera.Text = "Espera un momento ...";
                    espera.ShowDialog(this);
                }

                if (seleccionado.Tag != null) {
                    var printer = (RemisionDetailPrinter)(seleccionado.Tag);
                    var reporte = new ReporteForm(printer);
                    reporte.Show();
                }
            }
        }

        /// <summary>
        /// imprimir comprobante seleccionado con valores
        /// </summary>
        public virtual void TImprimirC_Comprobante_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetPrinter)) {
                espera.Text = "Espera un momento ...";
                espera.ShowDialog(this);
            }
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (!string.IsNullOrEmpty(seleccionado.IdDocumento)) {
                var printer = new RemisionDetailPrinter(seleccionado) {
                    Formato = Domain.Base.ValueObjects.RemisionFormatoEnum.SinPagare,
                    IsValuesVisible = true
                };
                var reporte = new ReporteForm(printer);
                reporte.Show();
            }
        }

        public virtual void TRemision_PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new RemisionadoPartidaForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            porPartida.Show();
        }

        public virtual void TRemision_PorCliente_Click(object sender, EventArgs e) {
            var porCliente = new RemisionadoClienteForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            porCliente.Show();
        }

        public virtual void TRemision_PorCobrar_Click(object sender, EventArgs e) {
            var porCliente = new RemisionadoPorCobrarForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            porCliente.Show();
        }

        /// <summary>
        /// cerrar ventana 
        /// </summary>
        public virtual void TRemision_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        public virtual void TDescargaPDF_Click(object sender, EventArgs e) {
            var single = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (single != null) {
                string liga = single.UrlFilePDF;
                // validar la liga de descarga
                if (ValidacionService.URL(liga)) {
                    var savefiledialog = new SaveFileDialog {
                        FileName = System.IO.Path.GetFileName(liga)
                    };
                    if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                        return;
                    if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                        DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                        if (dr == DialogResult.Yes) {
                            try {
                                System.Diagnostics.Process.Start(savefiledialog.FileName);
                            } catch (Exception ex) {
                                string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        public virtual void TCargarPDF_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var openFileDialog = new OpenFileDialog() { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
                if (openFileDialog.ShowDialog() != DialogResult.OK) {
                    return;
                }

                if (!string.IsNullOrEmpty(seleccionado.UrlFilePDF)) {
                    if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName,
                        ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                }

                if (System.IO.File.Exists(openFileDialog.FileName) == false) {
                    RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }

                seleccionado.Tag = openFileDialog.FileName;
                using (var espera = new Waiting1Form(this.UploadPDF)) {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        public virtual void ContextMenuClonar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Clonar)) {
                espera.ShowDialog(this);
            }
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var _nuevo = new RemisionFiscalForm(this._Service, (RemisionDetailModel)seleccionado.Tag) { MdiParent = ParentForm };
                _nuevo.Show();
            }
        }
        #endregion

        #region acciones del grid
        /// <summary>
        /// inicio de la edicion de la celda seleccionada
        /// </summary>
        public virtual void CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Row is GridViewFilteringRowInfo) { return; }
            if (_columns.Contains(e.Column.Name)) {
                var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
                if (seleccionado.IdStatus == 0 || seleccionado.IdStatus == 5 || seleccionado.Status == Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Pendiente) {
                    e.Cancel = true;
                    return;
                }

                if (string.IsNullOrEmpty(seleccionado.IdDocumento)) { e.Cancel = true; return; }
                if (e.Column.Name == "FechaEntrega" && seleccionado.FechaEntrega != null) { e.Cancel = true; return; }
                if (e.Column.Name == "FechaCobranza" && seleccionado.FechaCobranza != null) { e.Cancel = true; return; }

                if (e.Column.Name == "FechaCobranza" && seleccionado.FechaCobranza == null && seleccionado.FechaEntrega == null) {
                    e.Cancel = true;
                    RadMessageBox.Show(this, "No es permite actualizar la fecha de cobranza porque no se cuenta con fecha de entrega", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                } else {
                    if (e.Column.Name == "FechaCobranza") {
                        var d = e.ActiveEditor as RadDateTimeEditor;
                        d.MinValue = seleccionado.FechaEntrega.Value;
                    } else {
                        var d = e.ActiveEditor as RadDateTimeEditor;
                        d.MinValue = seleccionado.FechaEmision.AddHours(-24);
                    }
                }
            } else {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// evento al terminar la edicion de la celda seleccionada
        /// </summary>
        public virtual void CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (_columns.Contains(e.Column.Name)) {
                var seleccionado = e.Row.DataBoundItem as RemisionDetailModel;
                if (e.Column.Name == "FechaEntrega") {
                    if (seleccionado.FechaEntrega == null) { return; }
                    using (var espera = new Waiting1Form(this.FechaEntrega)) {
                        espera.Text = "Actualizando, espere un momento ...";
                        espera.ShowDialog(this);
                    }
                } else if (e.Column.Name == "FechaCobranza") {
                    if (seleccionado.FechaCobranza == null) { return; }
                    using (var espera = new Waiting1Form(this.FechaCobranza)) {
                        espera.Text = "Actualizando, espere un momento ...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        /// <summary>
        /// formato de celdas
        /// </summary>
        public virtual void CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "UrlFilePDF") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status" && e.Column.Name != "FechaEntrega" && e.Column.Name != "FechaCobranza") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    if (e.CellElement.Children != null) {
                        if (e.CellElement.Children.Count > 0) {
                            e.CellElement.Children.Clear();
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public virtual void CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.TRemision.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name == "UrlFilePDF") {
                        this.TDescargaPDF.PerformClick();
                    }
                }
            }
        }

        /// <summary>
        /// vista de los templetes
        /// </summary>
        private void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.Conceptos.Caption) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.ShowDialog(this);
                }
                var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Conceptos != null) {
                        foreach (var item in seleccionado.Conceptos) {
                            var row = e.Template.Rows.NewRow();
                            if (this.ShowValues) {
                                e.SourceCollection.Add(RemisionadoGridBuilder.ConvertToImportes(row, item));
                            } else {
                                e.SourceCollection.Add(RemisionadoGridBuilder.ConvertTo(row, item));
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.Autorizacion.Caption) {
                var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Autorizaciones != null) {
                        foreach (var item in seleccionado.Autorizaciones) {
                            var row = e.Template.Rows.NewRow();
                            row = RemisionadoGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.Relacion.Caption) {
                var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Autorizaciones != null) {
                        foreach (var item in seleccionado.RemisionRelacionada) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["Folio"].Value = item.Folio;
                            row.Cells["Serie"].Value = item.Serie;
                            row.Cells["IdDocumento"].Value = item.IdDocumento;
                            row.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
                            row.Cells["FechaEmision"].Value = item.FechaEmision;
                            row.Cells["GTotal"].Value = item.GTotal;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos
        public virtual void Initialization() {
            CheckForIllegalCrossThreadCalls = false;

            this.Conceptos.Standard();
            this.TRemision.GridData.MasterTemplate.Templates.AddRange(this.Conceptos);
            this.Autorizacion.Standard();
            this.TRemision.GridData.MasterTemplate.Templates.AddRange(this.Autorizacion);
            this.Relacion.Standard();
            this.TRemision.GridData.MasterTemplate.Templates.AddRange(this.Relacion);
            this.TRemision.Nuevo.Click += this.TRemision_Nuevo_Click;
            this.TRemision.Editar.Click += this.TRemision_Editar_Click;
            this.TRemision.ContextEditar.Click += this.TRemision_Editar_Click;
            this.TRemision.Cancelar.Click += this.TRemision_Cancelar_Click;
            this.TRemision.ContextCancelar.Click += this.TRemision_Cancelar_Click;
            this.TRemision.Actualizar.Click += this.TRemision_Actualizar_Click;
            this.TRemision.AutoSuma.Click += TRemision_AutoSuma_Click;
            this.TRemision.Cerrar.Click += this.TRemision_Cerrar_Click;
            this.TRemision.Imprimir.Items.AddRange(this.TImprimirComprobante, this.TImprimirListado);
            this.TImprimirComprobante.Click += this.TImprimir_Comprobante_Click;

            this.TRemision.ContextImprimir.Click += this.TImprimir_Comprobante_Click;
            // aqui lo hago visible por que inicialmente esta oculto
            this.TRemision.ContextImprimirC.Visibility = ElementVisibility.Visible;
            this.TRemision.ContextImprimirC.Click += this.TImprimirC_Comprobante_Click;
            this.TImprimirListado.Click += this.TImprimir_Listado_Click;
            this.TRemision.ContextDuplicar.Enabled = this.TRemision.Permisos.Agregar;
            this.TRemision.ContextDuplicar.Click += this.ContextMenuClonar_Click;
            this.TRemision.GridData.CellBeginEdit += CellBeginEdit;
            this.TRemision.GridData.CellEndEdit += CellEndEdit;
            this.TRemision.GridData.CellFormatting += CellFormatting;
            this.TRemision.GridData.CellDoubleClick += this.CellDoubleClick;
            this.TAcciones.Items.AddRange(new RadItem[] { this.TCargarPDF, this.TDescargaPDF });
            this.TRemision.menuContextual.Items.Add(this.TAcciones);
            this.TRemision.ShowExportarExcel = true;
            this.TRemision.ExportarExcel.Enabled = this.TRemision.Permisos.Exportar;

            this._MenuPorCobrar = ConfigService.GeMenuElement(this._PorCobrar.Name);
            this._MenuPorCliente = ConfigService.GeMenuElement(this._PorCliente.Name);
            this._MenuPorPartida = ConfigService.GeMenuElement(this._PorPartida.Name);
            this._PorCliente.Enabled = ConfigService.HasPermission(this._MenuPorCliente);
            this._PorCobrar.Enabled = ConfigService.HasPermission(this._MenuPorCobrar);
            this._PorPartida.Enabled = ConfigService.HasPermission(this._MenuPorPartida);

            this.TRemision.Herramientas.Items.Add(this._PorPartida);
            this.TRemision.Herramientas.Items.Add(this._PorCobrar);
            this.TRemision.Herramientas.Items.Add(this._PorCliente);

            this._PorCobrar.Click += this.TRemision_PorCobrar_Click;
            this._PorPartida.Click += this.TRemision_PorPartida_Click;
            this._PorCliente.Click += this.TRemision_PorCliente_Click;

            this.TCargarPDF.Click += this.TCargarPDF_Click;
            this.TDescargaPDF.Click += this.TDescargaPDF_Click;
        }

        private void TRemision_AutoSuma_Click(object sender, EventArgs e) {
            //TRemision.AutoSuma_Click(sender, e);
            this.Conceptos.AutoSum(!(this.TRemision.AutoSuma.ToggleState == ToggleState.On));
        }

        /// <summary>
        /// carga vista basica del grid
        /// </summary>
        public virtual void CreateView() {
            using (IRemisionadoGridBuilder builder = new RemisionadoGridBuilder()) {
                if (this.ShowValues) {
                    this.TRemision.GridData.Columns.AddRange(builder.Templetes().Remisionado().Build());
                    this.Conceptos.Columns.AddRange(builder.Templetes().Concepto().Build());
                } else {
                    this.TRemision.GridData.Columns.AddRange(builder.Templetes().RemisionadoSinValores().Build());
                    this.Conceptos.Columns.AddRange(builder.Templetes().ConceptosSinValores().Build());
                }
                this.Autorizacion.Columns.AddRange(builder.Templetes().Autorizacion().Build());
                this.Relacion.Columns.AddRange(builder.Templetes().Relacion().Build());
            }
        }

        /// <summary>
        /// obtener comprobante seleccionado
        /// </summary>
        public virtual void GetComprobante() {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var d = this._Service.GetById(seleccionado.IdRemision);
                if (d != null) {
                    seleccionado.SetValues(d);
                }
            }
        }

        public virtual void GetPrinter() {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var printer = this._Service.GetPrinter(seleccionado.IdRemision);
                if (printer != null) {
                    seleccionado.Tag = printer;
                }
            }
        }

        /// <summary>
        /// actualizar fecha de entrega al cliente
        /// </summary>
        public virtual void FechaEntrega() {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (seleccionado != null) {
                seleccionado = this._Service.FechaEntrega(seleccionado);
            }
        }

        /// <summary>
        /// actualizar fecha de recepcion para cobranza
        /// </summary>
        public virtual void FechaCobranza() {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (seleccionado != null) {
                seleccionado = this._Service.FechaCobranza(seleccionado);
            }
        }

        /// <summary>
        /// actualizar el listado
        /// </summary>
        public virtual void Actualizar() {
            this._DataSource = new BindingList<RemisionSingleModel>(
                this._Service.GetList<RemisionSingleModel>(
                    Aplication.Almacen.PT.Services.RemisionadoService.Query().WithYear(this.TRemision.GetEjercicio()).WithMonth(this.TRemision.GetPeriodo()).Build()).ToList()
                );
        }

        /// <summary>
        /// duplicar comprobante seleccionado
        /// </summary>
        public virtual void Clonar() {
            this.GetComprobante();
            var d1 = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (d1 != null) {
                d1.Tag = Aplication.Almacen.PT.Services.RemisionService.Create().Clone(d1).Build();
                //var clon = this._Service.Clonar(d1);
                //if (clon != null) {
                //    d1.Tag = clon;
                //}
            }
        }

        public virtual void UploadPDF() {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var d1 = this._Service.UpdatePDF(seleccionado);
                seleccionado.SetValues(d1);
            }
        }
        #endregion

        private void Prueba() {
            var dir = System.IO.Directory.GetFiles(@"C:\Users\anhed\Downloads\Nueva carpeta\remision", "*.pdf");
            var dir1 = string.Join("", dir).Replace(@"C:\Users\anhed\Downloads\Nueva carpeta\remision\", "");
            var dir2 = dir1.Split(new string[] { ".pdf" }, StringSplitOptions.None);
            dir2 = dir2.Where(it => !string.IsNullOrEmpty(it)).ToArray();
            var dir3 = string.Join(",", dir2);
            var d0 = this._Service.GetList<RemisionSingleModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("RMSN_FOLIO", dir3, Domain.Base.ValueObjects.ConditionalTypeEnum.In) }).ToList();
            if (d0 != null) {
                for (int i = 0; i < d0.Count; i++) {
                    d0[i].Tag = string.Format(@"C:\Users\anhed\Downloads\Nueva carpeta\remision\{0}.pdf", d0[i].Folio);
                    var d1 = this._Service.UpdatePDF(d0[i]);
                }
            }
        }
    }
}
