﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Util;
using Jaeger.QRCode.Helpers;

namespace Jaeger.UI.Almacen.PT.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources localResource = new EmbeddedResources("Jaeger.Domain.Almacen.PT");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.NombreComercial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(RemisionDetailPrinter)) {
                this.CrearRemision();
            } else if (this.CurrentObject.GetType() == typeof(List<RemisionDetailPrinter>)) {
                this.ListaRemision();
            } else if (this.CurrentObject.GetType() == typeof(ValeAlmacenPrinter)) {
                this.CrearVale();
            } else if (this.CurrentObject.GetType() == typeof(List<ValeAlmacenPrinter>)) {
                this.ListaVale();
            } else if (this.CurrentObject.GetType() == typeof(DevolucionDetailPrinterModel)) {
                this.CrearDevolucion();
            }
        }

        public virtual void CrearRemision() {
            var current = (RemisionDetailPrinter)this.CurrentObject;
            if (current.ShowPagare) {
                this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionPagareV201.rdlc");
            } else if (current.IsValuesVisible) {
                this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionImportesV20.rdlc");
            } else {
                this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionSimpleV20.rdlc");
            }

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter>(new List<RemisionDetailPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }
        //public virtual void CrearRemision() {
        //    var current = (RemisionDetailPrinter)this.CurrentObject;
        //    if (current.IsValuesVisible) {
        //        this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionFiscal1v20C.rdlc");
        //    } else {
        //        this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionFiscal1v20.rdlc");
        //    }

        //    this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
        //    this.Procesar();
        //    this.SetDisplayName("Remision");
        //    var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter>(new List<RemisionDetailPrinter>() { current });
        //    this.SetDataSource("Comprobante", d);
        //    this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
        //    this.Finalizar();
        //}

        private void ListaRemision() {
            var current = (List<RemisionDetailPrinter>)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream(@"Jaeger.Domain.Almacen.PT.Reports.RemisionFiscalLReporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Remisiones");
            this.SetDataSource("Remision", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }

        private void ListaVale() {
            var current = (List<ValeAlmacenPrinter>)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream(@"Jaeger.Domain.Almacen.PT.Reports.ValeAlmacen15LVReporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Vales");
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }

        private void CrearVale() {
            var current = (ValeAlmacenPrinter)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.ValeAlmacen15Reporte.rdlc");
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Vale Almacén");
            var d = DbConvert.ConvertToDataTable<ValeAlmacenPrinter>(new List<ValeAlmacenPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearDevolucion() {
            var current = (DevolucionDetailPrinterModel)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Almacen.PT.Reports.ValeAlmacen15Reporte.rdlc");
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Vale Almacén");
            var d = DbConvert.ConvertToDataTable<DevolucionDetailPrinterModel>(new List<DevolucionDetailPrinterModel>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }
    }
}
