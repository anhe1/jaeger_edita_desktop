﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class RemisionBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TBuscar = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TBuscar
            // 
            this.TBuscar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TBuscar.Etiqueta = "";
            this.TBuscar.Location = new System.Drawing.Point(0, 0);
            this.TBuscar.Name = "TBuscar";
            this.TBuscar.ShowAgregar = true;
            this.TBuscar.ShowBuscar = true;
            this.TBuscar.ShowCerrar = true;
            this.TBuscar.ShowExistencia = false;
            this.TBuscar.ShowFiltro = true;
            this.TBuscar.Size = new System.Drawing.Size(517, 30);
            this.TBuscar.TabIndex = 14;
            this.TBuscar.ButtonFiltro_Click += new System.EventHandler<System.EventArgs>(this.TBuscar_Filtro_Click);
            this.TBuscar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TBuscar_Cerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowAddNewRow = false;
            this.GridData.MasterTemplate.EnableFiltering = true;
            this.GridData.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor2.PropertyName = "Inicio";
            this.GridData.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(517, 270);
            this.GridData.TabIndex = 15;
            // 
            // RemisionBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 300);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TBuscar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RemisionBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CPLite: Unidades";
            this.Load += new System.EventHandler(this.UnidadBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarBuscarControl TBuscar;
        private Telerik.WinControls.UI.RadGridView GridData;
    }
}