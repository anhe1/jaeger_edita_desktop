﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class ValeAlmacenCatalogoForm : RadForm {
        #region declaraciones
        protected internal BindingList<ValeAlmacenSingleModel> vales;
        protected internal RadMenuItem ImprimirVale = new RadMenuItem { Text = "Comprobante", Name = "tcpvnt_brem_remision1" };
        protected internal Aplication.Almacen.PT.Contracts.IValeAlmacenService Service;
        #endregion

        /// <summary>
        /// mostrar o ocultar los importes de los comprobantes
        /// </summary>
        public bool ShowValues { get; set; } = true;

        public ValeAlmacenCatalogoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TVale.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void ValeAlmacenCatalogoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.TVale.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.TVale.Conceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.TVale.Conceptos);
            this.TVale.Relaciones.HierarchyDataProvider = new GridViewEventDataProvider(this.TVale.Relaciones);
            this.TVale.Status.HierarchyDataProvider = new GridViewEventDataProvider(this.TVale.Status);
            
            this.TVale.Nuevo.Click += this.TVale_Nuevo_Click;
            this.TVale.Actualizar.Click += this.TVale_Actualizar_Click;
            this.TVale.Cancelar.Click += this.TVale_Cancelar_Click;
            this.TVale.Imprimir.Items.Add(this.ImprimirVale);
            this.ImprimirVale.Click += this.ImprimirVale_Click;
            this.TVale.Cerrar.Click += this.Cerrar_Click;

            //this.TVale.ShowCancelar = true; 
            //this.TVale.Cancelar.Enabled = true;
            //this.TVale.ShowImprimir = true;
        }

        #region barra de herramientas
        public virtual void TVale_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new ValeAlmacenForm(this.Service) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        public virtual void TVale_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenDetailModel>();
            if (seleccionado != null) {
                if (seleccionado.IdStatus == 0) {
                    MessageBox.Show(this, "¡El comprobante seleccionado ya se encuentra cancelado!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else {
                    if (MessageBox.Show(this, "Esta seguro de cancelar? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        var cancela = new ValeAlmacenStatusForm(this.Service, seleccionado);
                        cancela.Selected += this.TVale_Cancela_Selected;
                        cancela.ShowDialog(this);

                    }
                }
            }
        }

        private void TVale_Cancela_Selected(object sender, IValeAlmacenStatusDetailModel e) {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado == null)
                return;
            seleccionado.IdStatus = e.IdStatusB;
            seleccionado.Cancela = e.Creo;
            seleccionado.FechaCancela = e.FechaNuevo;
            seleccionado.ClaveCancelacion = e.CveMotivo;
            seleccionado.Autorizacion.Add(e as ValeAlmacenStatusModel);
            if (e.Relaciones != null) { // no se actualizan debido a que la relacion se modifica en el formulario de status *TODO*
                if (seleccionado.Relaciones == null)
                    seleccionado.Relaciones = new BindingList<ValeAlmacenRelacionModel>();
                for (int i = 0; i < e.Relaciones.Count; i++) {
                    e.Relaciones[i].ClaveRelacion = e.CveMotivo;
                    e.Relaciones[i].IdClaveRelacion = e.IdCveMotivo;
                    seleccionado.Relaciones.Add(e.Relaciones[i]);
                }
            }
            this.Service.Save(seleccionado);
        }

        public virtual void TVale_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.TVale.GridData.DataSource = this.vales;
        }

        public virtual void ImprimirVale_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetPrinter)) {
                espera.Text = "Espera un momento ...";
                espera.ShowDialog(this);
            }
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenDetailModel>();
            var printer = new ValeAlmacenPrinter(seleccionado);
            var reporte = new ReporteForm(printer);
            reporte.Show();
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.TVale.Conceptos.Caption) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.ShowDialog(this);
                }
                var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Conceptos != null) {
                        foreach (var item in seleccionado.Conceptos) {
                            var row = e.Template.Rows.NewRow();
                            AlmacenGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.TVale.Relaciones.Caption) {
                var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Relaciones != null) {
                        foreach (var item in seleccionado.Relaciones) {
                            var row = e.Template.Rows.NewRow();
                            AlmacenGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.TVale.Status.Caption) {
                var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Autorizacion != null) {
                        foreach (var item in seleccionado.Autorizacion) {
                            var row = e.Template.Rows.NewRow();
                            AlmacenGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        public virtual void GetComprobante() {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado != null) {
                var d0 = this.Service.GetComprobante(seleccionado.IdComprobante);
                if (d0 != null) {
                    seleccionado.SetValues(d0 as ValeAlmacenDetailModel);
                }
            }
        }

        public virtual void Consultar() {
            if (this.TVale.Almacen.SelectedItem == null)
                return;
            var current = ((GridViewDataRowInfo)this.TVale.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            var d0 = ValeAlmacenService.Query().ByIdAlmacen(current.IdAlmacen).ByTipoAlmacen(Domain.Base.ValueObjects.AlmacenEnum.PT).WithYear(this.TVale.GetEjercicio()).WithMonth(this.TVale.GetPeriodo()).Build();
            this.vales = new BindingList<ValeAlmacenSingleModel>(this.Service.GetList<ValeAlmacenSingleModel>(d0).ToList());
        }

        public virtual void GetPrinter() {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenDetailModel>();
            if (seleccionado != null) {
                var d = this.Service.GetPrinter(seleccionado.IdComprobante);
                if (d != null) {
                    seleccionado.SetValues(d);
                }
            }
        }
        #endregion
    }
}
