﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class DevolucionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TDocumento = new Jaeger.UI.Common.Forms.TbDocumentoControl();
            this.Group = new Telerik.WinControls.UI.RadGroupBox();
            this.Contacto = new Telerik.WinControls.UI.RadTextBox();
            this.Receptor = new Jaeger.UI.Contribuyentes.Forms.DirectorioControl();
            this.lblMotivo = new Telerik.WinControls.UI.RadLabel();
            this.DevolucionTipo = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.DocumentoID = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblEfecto = new Telerik.WinControls.UI.RadLabel();
            this.Efecto = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TasaIVAPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.FactorPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.IdVendedor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblContacto = new Telerik.WinControls.UI.RadLabel();
            this.lblPedido = new Telerik.WinControls.UI.RadLabel();
            this.IdPedido = new Telerik.WinControls.UI.RadTextBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.lblVendedor = new Telerik.WinControls.UI.RadLabel();
            this.Vendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblDocumento = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.lblDepartamento = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaIngreso = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaEntrega = new Telerik.WinControls.UI.RadLabel();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.Creo = new Telerik.WinControls.UI.RadTextBox();
            this.IdSerie = new Telerik.WinControls.UI.RadDropDownList();
            this.lblCreo = new Telerik.WinControls.UI.RadLabel();
            this.lblSerie = new Telerik.WinControls.UI.RadLabel();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.Documento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Group)).BeginInit();
            this.Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMotivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevolucionTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevolucionTipo.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevolucionTipo.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentoID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEfecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            this.SuspendLayout();
            // 
            // TDocumento
            // 
            this.TDocumento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDocumento.Location = new System.Drawing.Point(0, 0);
            this.TDocumento.Name = "TDocumento";
            this.TDocumento.ShowComplemento = true;
            this.TDocumento.ShowIdDocumento = false;
            this.TDocumento.Size = new System.Drawing.Size(870, 30);
            this.TDocumento.TabIndex = 3;
            // 
            // Group
            // 
            this.Group.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Group.Controls.Add(this.Contacto);
            this.Group.Controls.Add(this.Receptor);
            this.Group.Controls.Add(this.lblMotivo);
            this.Group.Controls.Add(this.DevolucionTipo);
            this.Group.Controls.Add(this.DocumentoID);
            this.Group.Controls.Add(this.lblEfecto);
            this.Group.Controls.Add(this.Efecto);
            this.Group.Controls.Add(this.TasaIVAPactado);
            this.Group.Controls.Add(this.FactorPactado);
            this.Group.Controls.Add(this.IdVendedor2);
            this.Group.Controls.Add(this.lblContacto);
            this.Group.Controls.Add(this.lblPedido);
            this.Group.Controls.Add(this.IdPedido);
            this.Group.Controls.Add(this.lblVersion);
            this.Group.Controls.Add(this.lblVendedor);
            this.Group.Controls.Add(this.Vendedor);
            this.Group.Controls.Add(this.Departamento);
            this.Group.Controls.Add(this.lblDocumento);
            this.Group.Controls.Add(this.Nota);
            this.Group.Controls.Add(this.lblDepartamento);
            this.Group.Controls.Add(this.FechaEmision);
            this.Group.Controls.Add(this.FechaIngreso);
            this.Group.Controls.Add(this.lblFechaEmision);
            this.Group.Controls.Add(this.lblFechaEntrega);
            this.Group.Controls.Add(this.lblNota);
            this.Group.Controls.Add(this.Folio);
            this.Group.Controls.Add(this.Creo);
            this.Group.Controls.Add(this.IdSerie);
            this.Group.Controls.Add(this.lblCreo);
            this.Group.Controls.Add(this.lblSerie);
            this.Group.Controls.Add(this.lblFolio);
            this.Group.Controls.Add(this.Documento);
            this.Group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Group.HeaderText = "";
            this.Group.Location = new System.Drawing.Point(0, 30);
            this.Group.Margin = new System.Windows.Forms.Padding(0);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(870, 102);
            this.Group.TabIndex = 93;
            this.Group.TabStop = false;
            // 
            // Contacto
            // 
            this.Contacto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Contacto.Location = new System.Drawing.Point(68, 53);
            this.Contacto.MaxLength = 32;
            this.Contacto.Name = "Contacto";
            this.Contacto.NullText = "Contacto";
            this.Contacto.Size = new System.Drawing.Size(305, 20);
            this.Contacto.TabIndex = 379;
            // 
            // Receptor
            // 
            this.Receptor.AllowAddNew = false;
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.LDomicilio = "Domicilio:";
            this.Receptor.LNombre = "Receptor:";
            this.Receptor.Location = new System.Drawing.Point(8, 29);
            this.Receptor.Margin = new System.Windows.Forms.Padding(4);
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = false;
            this.Receptor.Relacion = Jaeger.Domain.Base.ValueObjects.TipoRelacionComericalEnum.None;
            this.Receptor.ShowDomicilio = false;
            this.Receptor.ShowRFC = false;
            this.Receptor.Size = new System.Drawing.Size(609, 20);
            this.Receptor.TabIndex = 378;
            // 
            // lblMotivo
            // 
            this.lblMotivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMotivo.Location = new System.Drawing.Point(623, 78);
            this.lblMotivo.Name = "lblMotivo";
            this.lblMotivo.Size = new System.Drawing.Size(44, 18);
            this.lblMotivo.TabIndex = 376;
            this.lblMotivo.Text = "Motivo:";
            // 
            // DevolucionTipo
            // 
            this.DevolucionTipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DevolucionTipo.AutoSizeDropDownToBestFit = true;
            this.DevolucionTipo.DisplayMember = "Descriptor";
            this.DevolucionTipo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // DevolucionTipo.NestedRadGridView
            // 
            this.DevolucionTipo.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.DevolucionTipo.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DevolucionTipo.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DevolucionTipo.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.DevolucionTipo.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.DevolucionTipo.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.DevolucionTipo.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "Descriptor";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "Descriptor";
            this.DevolucionTipo.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.DevolucionTipo.EditorControl.MasterTemplate.EnableGrouping = false;
            this.DevolucionTipo.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.DevolucionTipo.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.DevolucionTipo.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.DevolucionTipo.EditorControl.Name = "NestedRadGridView";
            this.DevolucionTipo.EditorControl.ReadOnly = true;
            this.DevolucionTipo.EditorControl.ShowGroupPanel = false;
            this.DevolucionTipo.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.DevolucionTipo.EditorControl.TabIndex = 0;
            this.DevolucionTipo.Enabled = false;
            this.DevolucionTipo.Location = new System.Drawing.Point(673, 77);
            this.DevolucionTipo.Name = "DevolucionTipo";
            this.DevolucionTipo.NullText = "Motivo de devolución";
            this.DevolucionTipo.Size = new System.Drawing.Size(187, 20);
            this.DevolucionTipo.TabIndex = 377;
            this.DevolucionTipo.TabStop = false;
            this.DevolucionTipo.ValueMember = "Id";
            // 
            // DocumentoID
            // 
            this.DocumentoID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DocumentoID.DecimalPlaces = 4;
            this.DocumentoID.Location = new System.Drawing.Point(1131, 80);
            this.DocumentoID.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.DocumentoID.Name = "DocumentoID";
            this.DocumentoID.ShowBorder = false;
            this.DocumentoID.ShowUpDownButtons = false;
            this.DocumentoID.Size = new System.Drawing.Size(34, 20);
            this.DocumentoID.TabIndex = 375;
            this.DocumentoID.TabStop = false;
            // 
            // lblEfecto
            // 
            this.lblEfecto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEfecto.Location = new System.Drawing.Point(622, 175);
            this.lblEfecto.Name = "lblEfecto";
            this.lblEfecto.Size = new System.Drawing.Size(40, 18);
            this.lblEfecto.TabIndex = 97;
            this.lblEfecto.Text = "Efecto:";
            // 
            // Efecto
            // 
            this.Efecto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Efecto.AutoSizeDropDownToBestFit = true;
            this.Efecto.DisplayMember = "Descriptor";
            this.Efecto.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Efecto.NestedRadGridView
            // 
            this.Efecto.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Efecto.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Efecto.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Efecto.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Efecto.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Efecto.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Efecto.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "Id";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Id";
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn5.FieldName = "Descriptor";
            gridViewTextBoxColumn5.HeaderText = "Descriptor";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Descriptor";
            this.Efecto.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.Efecto.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Efecto.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Efecto.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Efecto.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Efecto.EditorControl.Name = "NestedRadGridView";
            this.Efecto.EditorControl.ReadOnly = true;
            this.Efecto.EditorControl.ShowGroupPanel = false;
            this.Efecto.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Efecto.EditorControl.TabIndex = 0;
            this.Efecto.Location = new System.Drawing.Point(668, 174);
            this.Efecto.Name = "Efecto";
            this.Efecto.NullText = "Tipo";
            this.Efecto.Size = new System.Drawing.Size(191, 20);
            this.Efecto.TabIndex = 98;
            this.Efecto.TabStop = false;
            this.Efecto.ValueMember = "Id";
            // 
            // TasaIVAPactado
            // 
            this.TasaIVAPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TasaIVAPactado.DecimalPlaces = 4;
            this.TasaIVAPactado.Location = new System.Drawing.Point(1528, -16);
            this.TasaIVAPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.TasaIVAPactado.Name = "TasaIVAPactado";
            this.TasaIVAPactado.ShowBorder = false;
            this.TasaIVAPactado.ShowUpDownButtons = false;
            this.TasaIVAPactado.Size = new System.Drawing.Size(37, 20);
            this.TasaIVAPactado.TabIndex = 96;
            this.TasaIVAPactado.TabStop = false;
            // 
            // FactorPactado
            // 
            this.FactorPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FactorPactado.DecimalPlaces = 4;
            this.FactorPactado.Location = new System.Drawing.Point(1131, 56);
            this.FactorPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.FactorPactado.Name = "FactorPactado";
            this.FactorPactado.ShowBorder = false;
            this.FactorPactado.ShowUpDownButtons = false;
            this.FactorPactado.Size = new System.Drawing.Size(34, 20);
            this.FactorPactado.TabIndex = 30;
            this.FactorPactado.TabStop = false;
            // 
            // IdVendedor2
            // 
            this.IdVendedor2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdVendedor2.Location = new System.Drawing.Point(1131, 7);
            this.IdVendedor2.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdVendedor2.Name = "IdVendedor2";
            this.IdVendedor2.ShowBorder = false;
            this.IdVendedor2.ShowUpDownButtons = false;
            this.IdVendedor2.Size = new System.Drawing.Size(34, 20);
            this.IdVendedor2.TabIndex = 30;
            this.IdVendedor2.TabStop = false;
            // 
            // lblContacto
            // 
            this.lblContacto.Location = new System.Drawing.Point(8, 54);
            this.lblContacto.Name = "lblContacto";
            this.lblContacto.Size = new System.Drawing.Size(54, 18);
            this.lblContacto.TabIndex = 94;
            this.lblContacto.Text = "Contacto:";
            // 
            // lblPedido
            // 
            this.lblPedido.Location = new System.Drawing.Point(337, 6);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(74, 18);
            this.lblPedido.TabIndex = 92;
            this.lblPedido.Text = "Núm. Pedido:";
            // 
            // IdPedido
            // 
            this.IdPedido.Location = new System.Drawing.Point(417, 5);
            this.IdPedido.MaxLength = 32;
            this.IdPedido.Name = "IdPedido";
            this.IdPedido.NullText = "# Pedido";
            this.IdPedido.Size = new System.Drawing.Size(85, 20);
            this.IdPedido.TabIndex = 93;
            this.IdPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(8, 6);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(26, 18);
            this.lblVersion.TabIndex = 91;
            this.lblVersion.Text = "Ver.";
            // 
            // lblVendedor
            // 
            this.lblVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVendedor.Location = new System.Drawing.Point(379, 54);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(58, 18);
            this.lblVendedor.TabIndex = 89;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // Vendedor
            // 
            this.Vendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Vendedor.AutoSizeDropDownToBestFit = true;
            this.Vendedor.DisplayMember = "Clave";
            this.Vendedor.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Vendedor.NestedRadGridView
            // 
            this.Vendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Vendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Vendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Vendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Vendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowEditRow = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "IdVendedor";
            gridViewTextBoxColumn6.HeaderText = "IdVendedor";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "IdVendedor";
            gridViewTextBoxColumn7.FieldName = "Clave";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn8.FieldName = "Nombre";
            gridViewTextBoxColumn8.HeaderText = "Vendedor";
            gridViewTextBoxColumn8.Name = "Nombre";
            this.Vendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.Vendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Vendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Vendedor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Vendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.Vendedor.EditorControl.Name = "NestedRadGridView";
            this.Vendedor.EditorControl.ReadOnly = true;
            this.Vendedor.EditorControl.ShowGroupPanel = false;
            this.Vendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Vendedor.EditorControl.TabIndex = 0;
            this.Vendedor.Location = new System.Drawing.Point(444, 53);
            this.Vendedor.Name = "Vendedor";
            this.Vendedor.NullText = "Vendedor";
            this.Vendedor.Size = new System.Drawing.Size(170, 20);
            this.Vendedor.TabIndex = 90;
            this.Vendedor.TabStop = false;
            this.Vendedor.ValueMember = "IdVendedor";
            // 
            // Departamento
            // 
            this.Departamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Departamento.AutoSizeDropDownToBestFit = true;
            this.Departamento.DisplayMember = "Descripcion";
            this.Departamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "IdDepartamento";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "IdDepartamento";
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Descripcion";
            gridViewTextBoxColumn11.FieldName = "Descriptor";
            gridViewTextBoxColumn11.HeaderText = "Descriptor";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Descriptor";
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(713, 29);
            this.Departamento.Name = "Departamento";
            this.Departamento.NullText = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(147, 20);
            this.Departamento.TabIndex = 32;
            this.Departamento.TabStop = false;
            this.Departamento.ValueMember = "IdDepartamento";
            // 
            // lblDocumento
            // 
            this.lblDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocumento.Location = new System.Drawing.Point(622, 54);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.Size = new System.Drawing.Size(31, 18);
            this.lblDocumento.TabIndex = 33;
            this.lblDocumento.Text = "Tipo:";
            // 
            // Nota
            // 
            this.Nota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nota.Location = new System.Drawing.Point(47, 77);
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(567, 20);
            this.Nota.TabIndex = 19;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDepartamento.Location = new System.Drawing.Point(623, 30);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(81, 18);
            this.lblDepartamento.TabIndex = 31;
            this.lblDepartamento.Text = "Departamento:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(649, 5);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(211, 20);
            this.FechaEmision.TabIndex = 7;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // FechaIngreso
            // 
            this.FechaIngreso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaIngreso.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaIngreso.Location = new System.Drawing.Point(896, 126);
            this.FechaIngreso.Name = "FechaIngreso";
            this.FechaIngreso.NullText = "--/--/----";
            this.FechaIngreso.ReadOnly = true;
            this.FechaIngreso.Size = new System.Drawing.Size(94, 20);
            this.FechaIngreso.TabIndex = 9;
            this.FechaIngreso.TabStop = false;
            this.FechaIngreso.Text = "05/12/2017";
            this.FechaIngreso.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(577, 6);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(70, 18);
            this.lblFechaEmision.TabIndex = 6;
            this.lblFechaEmision.Text = "Fec. Emisión:";
            // 
            // lblFechaEntrega
            // 
            this.lblFechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEntrega.Location = new System.Drawing.Point(804, 127);
            this.lblFechaEntrega.Name = "lblFechaEntrega";
            this.lblFechaEntrega.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEntrega.TabIndex = 8;
            this.lblFechaEntrega.Text = "Fec. de Entrega:";
            // 
            // lblNota
            // 
            this.lblNota.Location = new System.Drawing.Point(8, 78);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(33, 18);
            this.lblNota.TabIndex = 18;
            this.lblNota.Text = "Nota:";
            this.lblNota.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(234, 5);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.Size = new System.Drawing.Size(92, 20);
            this.Folio.TabIndex = 5;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Creo
            // 
            this.Creo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Creo.Location = new System.Drawing.Point(1032, 130);
            this.Creo.MaxLength = 10;
            this.Creo.Name = "Creo";
            this.Creo.NullText = "Creó";
            this.Creo.Size = new System.Drawing.Size(82, 20);
            this.Creo.TabIndex = 30;
            this.Creo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IdSerie
            // 
            this.IdSerie.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdSerie.Location = new System.Drawing.Point(80, 5);
            this.IdSerie.Name = "IdSerie";
            this.IdSerie.NullText = "Serie";
            this.IdSerie.Size = new System.Drawing.Size(110, 20);
            this.IdSerie.TabIndex = 3;
            // 
            // lblCreo
            // 
            this.lblCreo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCreo.Location = new System.Drawing.Point(993, 131);
            this.lblCreo.Name = "lblCreo";
            this.lblCreo.Size = new System.Drawing.Size(32, 18);
            this.lblCreo.TabIndex = 28;
            this.lblCreo.Text = "Creó:";
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(41, 6);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(33, 18);
            this.lblSerie.TabIndex = 2;
            this.lblSerie.Text = "Serie:";
            // 
            // lblFolio
            // 
            this.lblFolio.Location = new System.Drawing.Point(195, 6);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 4;
            this.lblFolio.Text = "Folio:";
            // 
            // Documento
            // 
            this.Documento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Documento.AutoSizeDropDownToBestFit = true;
            this.Documento.DisplayMember = "Descriptor";
            this.Documento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Documento.NestedRadGridView
            // 
            this.Documento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Documento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Documento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Documento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Documento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Documento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Documento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "IdDocumento";
            gridViewTextBoxColumn12.HeaderText = "Clave";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "IdDocumento";
            gridViewTextBoxColumn13.FieldName = "Descripcion";
            gridViewTextBoxColumn13.HeaderText = "Descripción";
            gridViewTextBoxColumn13.Name = "Descripcion";
            gridViewTextBoxColumn14.FieldName = "Descriptor";
            gridViewTextBoxColumn14.HeaderText = "Descriptor";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "Descriptor";
            this.Documento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.Documento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Documento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Documento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Documento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.Documento.EditorControl.Name = "NestedRadGridView";
            this.Documento.EditorControl.ReadOnly = true;
            this.Documento.EditorControl.ShowGroupPanel = false;
            this.Documento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Documento.EditorControl.TabIndex = 0;
            this.Documento.Location = new System.Drawing.Point(668, 53);
            this.Documento.Name = "Documento";
            this.Documento.NullText = "Documento";
            this.Documento.Size = new System.Drawing.Size(192, 20);
            this.Documento.TabIndex = 34;
            this.Documento.TabStop = false;
            this.Documento.ValueMember = "IdDocumento";
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // DevolucionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Group);
            this.Controls.Add(this.TDocumento);
            this.Name = "DevolucionControl";
            this.Size = new System.Drawing.Size(870, 132);
            this.Load += new System.EventHandler(this.DevolucionControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Group)).EndInit();
            this.Group.ResumeLayout(false);
            this.Group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMotivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevolucionTipo.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevolucionTipo.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevolucionTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentoID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEfecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.TbDocumentoControl TDocumento;
        private Telerik.WinControls.UI.RadGroupBox Group;
        internal Telerik.WinControls.UI.RadLabel lblMotivo;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox DevolucionTipo;
        internal Telerik.WinControls.UI.RadSpinEditor DocumentoID;
        internal Telerik.WinControls.UI.RadLabel lblEfecto;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Efecto;
        internal Telerik.WinControls.UI.RadSpinEditor TasaIVAPactado;
        internal Telerik.WinControls.UI.RadSpinEditor FactorPactado;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdVendedor2;
        internal Telerik.WinControls.UI.RadLabel lblContacto;
        internal Telerik.WinControls.UI.RadLabel lblPedido;
        internal Telerik.WinControls.UI.RadTextBox IdPedido;
        protected internal Telerik.WinControls.UI.RadLabel lblVersion;
        internal Telerik.WinControls.UI.RadLabel lblVendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Vendedor;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        internal Telerik.WinControls.UI.RadLabel lblDocumento;
        protected internal Telerik.WinControls.UI.RadTextBox Nota;
        internal Telerik.WinControls.UI.RadLabel lblDepartamento;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaIngreso;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaEntrega;
        internal Telerik.WinControls.UI.RadLabel lblNota;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadTextBox Creo;
        internal Telerik.WinControls.UI.RadDropDownList IdSerie;
        internal Telerik.WinControls.UI.RadLabel lblCreo;
        internal Telerik.WinControls.UI.RadLabel lblSerie;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Documento;
        internal Telerik.WinControls.UI.RadTextBox Contacto;
        public Contribuyentes.Forms.DirectorioControl Receptor;
        private System.Windows.Forms.ErrorProvider Advertencia;
    }
}
