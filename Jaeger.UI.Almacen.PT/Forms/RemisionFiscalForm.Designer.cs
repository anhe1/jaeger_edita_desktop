﻿namespace Jaeger.UI.Almacen.PT.Forms {
    partial class RemisionFiscalForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TRemision = new Jaeger.UI.Almacen.PT.Forms.RemisionFiscalControl();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptos = new Telerik.WinControls.UI.RadPageViewPage();
            this.SplitContainerConceptos = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitConceptos = new Telerik.WinControls.UI.SplitPanel();
            this.GridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.TConceptos = new Jaeger.UI.Common.Forms.ToolBarConceptoControl();
            this.splitConceptoParte = new Telerik.WinControls.UI.SplitPanel();
            this.TabConceptoParte = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoParte = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoParte = new Telerik.WinControls.UI.RadGridView();
            this.TConceptoParte = new Jaeger.UI.Common.Forms.ToolBarConceptoControl();
            this.PanelTotales = new Telerik.WinControls.UI.RadPanel();
            this.Total = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Descuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionIva = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SubTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblSubTotal = new Telerik.WinControls.UI.RadLabel();
            this.TrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblTotal = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalDescuento = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalIEPS = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalRetencionISR = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalIvaTraslado = new Telerik.WinControls.UI.RadLabel();
            this.lblTotalRetencionIVA = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.PageConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerConceptos)).BeginInit();
            this.SplitContainerConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitConceptos)).BeginInit();
            this.splitConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitConceptoParte)).BeginInit();
            this.splitConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).BeginInit();
            this.TabConceptoParte.SuspendLayout();
            this.PageConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).BeginInit();
            this.PanelTotales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIvaTraslado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TRemision
            // 
            this.TRemision.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRemision.Location = new System.Drawing.Point(0, 0);
            this.TRemision.Name = "TRemision";
            this.TRemision.ReadOnly = false;
            this.TRemision.Size = new System.Drawing.Size(1098, 134);
            this.TRemision.TabIndex = 0;
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.PageConceptos);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 134);
            this.TabControl.Margin = new System.Windows.Forms.Padding(1);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.PageConceptos;
            this.TabControl.Size = new System.Drawing.Size(1098, 435);
            this.TabControl.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            // 
            // PageConceptos
            // 
            this.PageConceptos.Controls.Add(this.SplitContainerConceptos);
            this.PageConceptos.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.PageConceptos.Location = new System.Drawing.Point(8, 32);
            this.PageConceptos.Name = "PageConceptos";
            this.PageConceptos.Size = new System.Drawing.Size(1082, 398);
            this.PageConceptos.Text = "Conceptos";
            // 
            // SplitContainerConceptos
            // 
            this.SplitContainerConceptos.Controls.Add(this.splitConceptos);
            this.SplitContainerConceptos.Controls.Add(this.splitConceptoParte);
            this.SplitContainerConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerConceptos.Location = new System.Drawing.Point(0, 0);
            this.SplitContainerConceptos.Name = "SplitContainerConceptos";
            this.SplitContainerConceptos.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.SplitContainerConceptos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.SplitContainerConceptos.Size = new System.Drawing.Size(1082, 398);
            this.SplitContainerConceptos.TabIndex = 3;
            this.SplitContainerConceptos.TabStop = false;
            // 
            // splitConceptos
            // 
            this.splitConceptos.Controls.Add(this.GridConceptos);
            this.splitConceptos.Controls.Add(this.TConceptos);
            this.splitConceptos.Location = new System.Drawing.Point(0, 0);
            this.splitConceptos.Name = "splitConceptos";
            // 
            // 
            // 
            this.splitConceptos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitConceptos.Size = new System.Drawing.Size(1082, 229);
            this.splitConceptos.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1226415F);
            this.splitConceptos.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 59);
            this.splitConceptos.TabIndex = 0;
            this.splitConceptos.TabStop = false;
            this.splitConceptos.Text = "splitPanel1";
            // 
            // GridConceptos
            // 
            this.GridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptos.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptos.MasterTemplate.AllowDeleteRow = false;
            this.GridConceptos.MasterTemplate.AllowRowResize = false;
            this.GridConceptos.MasterTemplate.EnableGrouping = false;
            this.GridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridConceptos.Name = "GridConceptos";
            this.GridConceptos.Size = new System.Drawing.Size(1082, 199);
            this.GridConceptos.TabIndex = 194;
            this.GridConceptos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridConceptos_KeyDown);
            // 
            // TConceptos
            // 
            this.TConceptos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptos.IsEditable = true;
            this.TConceptos.Location = new System.Drawing.Point(0, 0);
            this.TConceptos.Margin = new System.Windows.Forms.Padding(1);
            this.TConceptos.Name = "TConceptos";
            this.TConceptos.ShowAgregar = true;
            this.TConceptos.ShowComboConceptos = true;
            this.TConceptos.ShowDuplicar = true;
            this.TConceptos.ShowNuevo = true;
            this.TConceptos.ShowProductos = true;
            this.TConceptos.ShowRemover = true;
            this.TConceptos.ShowUnidades = true;
            this.TConceptos.Size = new System.Drawing.Size(1082, 30);
            this.TConceptos.TabIndex = 195;
            // 
            // splitConceptoParte
            // 
            this.splitConceptoParte.Controls.Add(this.TabConceptoParte);
            this.splitConceptoParte.Controls.Add(this.PanelTotales);
            this.splitConceptoParte.Location = new System.Drawing.Point(0, 233);
            this.splitConceptoParte.Name = "splitConceptoParte";
            // 
            // 
            // 
            this.splitConceptoParte.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitConceptoParte.Size = new System.Drawing.Size(1082, 165);
            this.splitConceptoParte.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 165);
            this.splitConceptoParte.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1226415F);
            this.splitConceptoParte.SizeInfo.MaximumSize = new System.Drawing.Size(0, 165);
            this.splitConceptoParte.SizeInfo.MinimumSize = new System.Drawing.Size(0, 165);
            this.splitConceptoParte.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -59);
            this.splitConceptoParte.TabIndex = 1;
            this.splitConceptoParte.TabStop = false;
            this.splitConceptoParte.Text = "splitPanel2";
            // 
            // TabConceptoParte
            // 
            this.TabConceptoParte.Controls.Add(this.PageConceptoParte);
            this.TabConceptoParte.DefaultPage = this.PageConceptoParte;
            this.TabConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.TabConceptoParte.Name = "TabConceptoParte";
            this.TabConceptoParte.SelectedPage = this.PageConceptoParte;
            this.TabConceptoParte.Size = new System.Drawing.Size(850, 165);
            this.TabConceptoParte.TabIndex = 169;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemCloseButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            // 
            // PageConceptoParte
            // 
            this.PageConceptoParte.Controls.Add(this.GridConceptoParte);
            this.PageConceptoParte.Controls.Add(this.TConceptoParte);
            this.PageConceptoParte.ItemSize = new System.Drawing.SizeF(96F, 28F);
            this.PageConceptoParte.Location = new System.Drawing.Point(7, 33);
            this.PageConceptoParte.Name = "PageConceptoParte";
            this.PageConceptoParte.Size = new System.Drawing.Size(836, 125);
            this.PageConceptoParte.Text = "Concepto: Parte";
            // 
            // GridConceptoParte
            // 
            this.GridConceptoParte.AutoGenerateHierarchy = true;
            this.GridConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoParte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoParte.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptoParte.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Cantidad";
            gridViewTextBoxColumn1.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn1.HeaderText = "Cantidad";
            gridViewTextBoxColumn1.Name = "Cantidad";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Unidad";
            gridViewTextBoxColumn2.HeaderText = "Unidad";
            gridViewTextBoxColumn2.Name = "Unidad";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn3.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn3.Name = "ClaveProdServ";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn4.HeaderText = "No. Ident.";
            gridViewTextBoxColumn4.Name = "NoIdentificacion";
            gridViewTextBoxColumn4.Width = 90;
            gridViewTextBoxColumn5.FieldName = "Descripcion";
            gridViewTextBoxColumn5.HeaderText = "Descripción";
            gridViewTextBoxColumn5.Name = "Descripcion";
            gridViewTextBoxColumn5.Width = 250;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "ValorUnitario";
            gridViewTextBoxColumn6.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn6.HeaderText = "Valor \n\rUnitario";
            gridViewTextBoxColumn6.Name = "ValorUnitario";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Importe";
            gridViewTextBoxColumn7.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn7.HeaderText = "Importe";
            gridViewTextBoxColumn7.Name = "Importe";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 80;
            this.GridConceptoParte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.GridConceptoParte.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridConceptoParte.Name = "GridConceptoParte";
            this.GridConceptoParte.ShowGroupPanel = false;
            this.GridConceptoParte.Size = new System.Drawing.Size(836, 95);
            this.GridConceptoParte.TabIndex = 0;
            // 
            // TConceptoParte
            // 
            this.TConceptoParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptoParte.IsEditable = true;
            this.TConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.TConceptoParte.Name = "TConceptoParte";
            this.TConceptoParte.ShowAgregar = true;
            this.TConceptoParte.ShowComboConceptos = true;
            this.TConceptoParte.ShowDuplicar = true;
            this.TConceptoParte.ShowNuevo = true;
            this.TConceptoParte.ShowProductos = true;
            this.TConceptoParte.ShowRemover = true;
            this.TConceptoParte.ShowUnidades = true;
            this.TConceptoParte.Size = new System.Drawing.Size(836, 30);
            this.TConceptoParte.TabIndex = 1;
            // 
            // PanelTotales
            // 
            this.PanelTotales.Controls.Add(this.Total);
            this.PanelTotales.Controls.Add(this.RetencionISR);
            this.PanelTotales.Controls.Add(this.Descuento);
            this.PanelTotales.Controls.Add(this.RetencionIva);
            this.PanelTotales.Controls.Add(this.SubTotal);
            this.PanelTotales.Controls.Add(this.TrasladoIEPS);
            this.PanelTotales.Controls.Add(this.lblSubTotal);
            this.PanelTotales.Controls.Add(this.TrasladoIVA);
            this.PanelTotales.Controls.Add(this.lblTotal);
            this.PanelTotales.Controls.Add(this.lblTotalDescuento);
            this.PanelTotales.Controls.Add(this.lblTotalIEPS);
            this.PanelTotales.Controls.Add(this.lblTotalRetencionISR);
            this.PanelTotales.Controls.Add(this.lblTotalIvaTraslado);
            this.PanelTotales.Controls.Add(this.lblTotalRetencionIVA);
            this.PanelTotales.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotales.Location = new System.Drawing.Point(850, 0);
            this.PanelTotales.Name = "PanelTotales";
            this.PanelTotales.Size = new System.Drawing.Size(232, 165);
            this.PanelTotales.TabIndex = 170;
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(97, 134);
            this.Total.Mask = "n4";
            this.Total.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Total.Name = "Total";
            this.Total.NullText = "SubTotal";
            this.Total.Size = new System.Drawing.Size(125, 20);
            this.Total.TabIndex = 7;
            this.Total.TabStop = false;
            this.Total.Text = "0.0000";
            this.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionISR
            // 
            this.RetencionISR.Location = new System.Drawing.Point(97, 113);
            this.RetencionISR.Mask = "n4";
            this.RetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.NullText = "SubTotal";
            this.RetencionISR.Size = new System.Drawing.Size(125, 20);
            this.RetencionISR.TabIndex = 6;
            this.RetencionISR.TabStop = false;
            this.RetencionISR.Text = "0.0000";
            this.RetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Descuento
            // 
            this.Descuento.Location = new System.Drawing.Point(97, 29);
            this.Descuento.Mask = "n4";
            this.Descuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Descuento.Name = "Descuento";
            this.Descuento.NullText = "SubTotal";
            this.Descuento.Size = new System.Drawing.Size(125, 20);
            this.Descuento.TabIndex = 2;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "0.0000";
            this.Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionIva
            // 
            this.RetencionIva.Location = new System.Drawing.Point(97, 92);
            this.RetencionIva.Mask = "n4";
            this.RetencionIva.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionIva.Name = "RetencionIva";
            this.RetencionIva.NullText = "SubTotal";
            this.RetencionIva.Size = new System.Drawing.Size(125, 20);
            this.RetencionIva.TabIndex = 5;
            this.RetencionIva.TabStop = false;
            this.RetencionIva.Text = "0.0000";
            this.RetencionIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SubTotal
            // 
            this.SubTotal.Location = new System.Drawing.Point(97, 8);
            this.SubTotal.Mask = "n4";
            this.SubTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.NullText = "SubTotal";
            this.SubTotal.Size = new System.Drawing.Size(125, 20);
            this.SubTotal.TabIndex = 1;
            this.SubTotal.TabStop = false;
            this.SubTotal.Text = "0.0000";
            this.SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.Location = new System.Drawing.Point(97, 71);
            this.TrasladoIEPS.Mask = "n4";
            this.TrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.NullText = "SubTotal";
            this.TrasladoIEPS.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIEPS.TabIndex = 4;
            this.TrasladoIEPS.TabStop = false;
            this.TrasladoIEPS.Text = "0.0000";
            this.TrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Location = new System.Drawing.Point(16, 9);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(50, 18);
            this.lblSubTotal.TabIndex = 179;
            this.lblSubTotal.Text = "SubTotal";
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.Location = new System.Drawing.Point(97, 50);
            this.TrasladoIVA.Mask = "n4";
            this.TrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.NullText = "SubTotal";
            this.TrasladoIVA.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIVA.TabIndex = 3;
            this.TrasladoIVA.TabStop = false;
            this.TrasladoIVA.Text = "0.0000";
            this.TrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(16, 135);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 18);
            this.lblTotal.TabIndex = 182;
            this.lblTotal.Text = "Total";
            // 
            // lblTotalDescuento
            // 
            this.lblTotalDescuento.Location = new System.Drawing.Point(16, 30);
            this.lblTotalDescuento.Name = "lblTotalDescuento";
            this.lblTotalDescuento.Size = new System.Drawing.Size(59, 18);
            this.lblTotalDescuento.TabIndex = 176;
            this.lblTotalDescuento.Text = "Descuento";
            // 
            // lblTotalIEPS
            // 
            this.lblTotalIEPS.Location = new System.Drawing.Point(16, 72);
            this.lblTotalIEPS.Name = "lblTotalIEPS";
            this.lblTotalIEPS.Size = new System.Drawing.Size(27, 18);
            this.lblTotalIEPS.TabIndex = 178;
            this.lblTotalIEPS.Text = "IEPS";
            // 
            // lblTotalRetencionISR
            // 
            this.lblTotalRetencionISR.Location = new System.Drawing.Point(16, 114);
            this.lblTotalRetencionISR.Name = "lblTotalRetencionISR";
            this.lblTotalRetencionISR.Size = new System.Drawing.Size(75, 18);
            this.lblTotalRetencionISR.TabIndex = 181;
            this.lblTotalRetencionISR.Text = "Retención ISR";
            // 
            // lblTotalIvaTraslado
            // 
            this.lblTotalIvaTraslado.Location = new System.Drawing.Point(16, 51);
            this.lblTotalIvaTraslado.Name = "lblTotalIvaTraslado";
            this.lblTotalIvaTraslado.Size = new System.Drawing.Size(24, 18);
            this.lblTotalIvaTraslado.TabIndex = 177;
            this.lblTotalIvaTraslado.Text = "IVA";
            // 
            // lblTotalRetencionIVA
            // 
            this.lblTotalRetencionIVA.Location = new System.Drawing.Point(16, 93);
            this.lblTotalRetencionIVA.Name = "lblTotalRetencionIVA";
            this.lblTotalRetencionIVA.Size = new System.Drawing.Size(77, 18);
            this.lblTotalRetencionIVA.TabIndex = 180;
            this.lblTotalRetencionIVA.Text = "Retención IVA";
            // 
            // RemisionFiscalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 569);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.TRemision);
            this.Name = "RemisionFiscalForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RemisionFiscalForm";
            this.Load += new System.EventHandler(this.RemisionFiscalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.PageConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerConceptos)).EndInit();
            this.SplitContainerConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitConceptos)).EndInit();
            this.splitConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitConceptoParte)).EndInit();
            this.splitConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).EndInit();
            this.TabConceptoParte.ResumeLayout(false);
            this.PageConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            this.PanelTotales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalIvaTraslado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalRetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public RemisionFiscalControl TRemision;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageConceptos;
        private Telerik.WinControls.UI.RadSplitContainer SplitContainerConceptos;
        private Telerik.WinControls.UI.SplitPanel splitConceptos;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptos;
        protected internal Common.Forms.ToolBarConceptoControl TConceptos;
        private Telerik.WinControls.UI.SplitPanel splitConceptoParte;
        internal Telerik.WinControls.UI.RadPageView TabConceptoParte;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoParte;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptoParte;
        protected internal Common.Forms.ToolBarConceptoControl TConceptoParte;
        private Telerik.WinControls.UI.RadPanel PanelTotales;
        private Telerik.WinControls.UI.RadMaskedEditBox Total;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox Descuento;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionIva;
        private Telerik.WinControls.UI.RadMaskedEditBox SubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIEPS;
        internal Telerik.WinControls.UI.RadLabel lblSubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIVA;
        internal Telerik.WinControls.UI.RadLabel lblTotal;
        internal Telerik.WinControls.UI.RadLabel lblTotalDescuento;
        internal Telerik.WinControls.UI.RadLabel lblTotalIEPS;
        internal Telerik.WinControls.UI.RadLabel lblTotalRetencionISR;
        internal Telerik.WinControls.UI.RadLabel lblTotalIvaTraslado;
        internal Telerik.WinControls.UI.RadLabel lblTotalRetencionIVA;
    }
}