﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.PT.Forms {
    public partial class RemisionFiscalForm : RadForm {
        #region declaraciones
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected internal RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };
        private bool autosuma = false;
        public bool ShowValues { get; set; } = true;
        #endregion

        public RemisionFiscalForm() {
            InitializeComponent();
        }

        public RemisionFiscalForm(Aplication.Almacen.PT.Contracts.IRemisionService service, bool showValues = false) {
            InitializeComponent();
            this.TRemision.Service = service;
            this.ShowValues = showValues;
        }

        public RemisionFiscalForm(Aplication.Almacen.PT.Contracts.IRemisionService service, RemisionDetailModel model, bool showValues = false) {
            InitializeComponent();
            this.TRemision.Service = service;
            this.TRemision.Comprobante = model;
            this.ShowValues = showValues;
        }

        private void RemisionFiscalForm_Load(object sender, EventArgs e) {
            // grid de conceptos
            using (var v0 = new Builder.RemisionadoGridBuilder()) {
                this.GridConceptos.Columns.AddRange(v0.Templetes().Conceptos(this.ShowValues).Build());
            }

            this.PanelTotales.Visible = this.ShowValues;
            this.TRemision.BindingCompleted += this.TRemision_BindingCompleted;
            this.TRemision.Actualizar_Click(sender, e);
            this.TRemision.TDocumento.Cerrar.Click += this.Cerrar_Click;
            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region barra de herramientas conceptos del comprobante
        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.TRemision.Comprobante.Conceptos.Count == 0) {
                this.TRemision.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                this.GridConceptos.DataSource = this.TRemision.Comprobante.Conceptos;
            }

            this.TRemision.Comprobante.Conceptos.Add(new RemisionConceptoDetailModel { Activo = true });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Remision_RemoverObjeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as RemisionConceptoDetailModel;
                    if (seleccionado.IdMovimiento == 0) {
                        try {
                            this.TRemision.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.GridConceptos.Rows.Count == 0) {
                                this.TRemision.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.GridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as RemisionConceptoDetailModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.TRemision.Comprobante.Conceptos.Add(duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TRemision.Comprobante.IdCliente == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
        }
        #endregion

        private void TRemision_BindingCompleted(object sender, EventArgs e) {
            this.GridConceptos.DataSource = this.TRemision.Comprobante.Conceptos;
            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.TRemision.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.TRemision.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.TRemision.Comprobante, "TotalTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.TRemision.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TRemision.TDocumento.Guardar.Enabled = true;
            this.TConceptos.IsEditable = this.TRemision.Comprobante.IsEditable;
            this.TConceptoParte.IsEditable = this.TRemision.Comprobante.IsEditable;
            this.GridConceptos.ReadOnly = !this.TRemision.Comprobante.IsEditable;
            this.GridConceptoParte.ReadOnly = !this.TRemision.Comprobante.IsEditable;
            this.TConceptos.Nuevo.Enabled = false;
            this.TConceptos.Agregar.Enabled = false;
            this.TConceptos.Duplicar.Enabled = false;
        }

        private void GridConceptos_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F11) {
                autosuma = !autosuma;
                this.GridConceptos.AutoSum(autosuma);
            }
        }
    }
}
