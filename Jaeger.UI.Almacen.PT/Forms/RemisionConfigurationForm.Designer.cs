﻿
namespace Jaeger.UI.Almacen.PT.Forms {
    partial class RemisionConfigurationForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemisionConfigurationForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.CheckPagare = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckTotalEnLetra = new Telerik.WinControls.UI.RadCheckBox();
            this.CheckDesgloseIVA = new Telerik.WinControls.UI.RadCheckBox();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.Pagare = new Telerik.WinControls.UI.RadTextBox();
            this.Leyenda1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.Guardar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox3)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPagare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckTotalEnLetra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckDesgloseIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pagare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Leyenda1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(646, 40);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(20, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Parametros del Remisionado al Cliente";
            // 
            // groupBox3
            // 
            this.groupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox3.Controls.Add(this.CheckDesgloseIVA);
            this.groupBox3.Controls.Add(this.CheckTotalEnLetra);
            this.groupBox3.Controls.Add(this.CheckPagare);
            this.groupBox3.HeaderText = "General";
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(269, 92);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "General";
            // 
            // CheckPagare
            // 
            this.CheckPagare.Location = new System.Drawing.Point(7, 17);
            this.CheckPagare.Name = "CheckPagare";
            this.CheckPagare.Size = new System.Drawing.Size(88, 18);
            this.CheckPagare.TabIndex = 2;
            this.CheckPagare.Text = "Incluir Pagaré";
            // 
            // CheckTotalEnLetra
            // 
            this.CheckTotalEnLetra.Location = new System.Drawing.Point(7, 41);
            this.CheckTotalEnLetra.Name = "CheckTotalEnLetra";
            this.CheckTotalEnLetra.Size = new System.Drawing.Size(117, 18);
            this.CheckTotalEnLetra.TabIndex = 3;
            this.CheckTotalEnLetra.Text = "Incluir total en letra";
            // 
            // CheckDesgloseIVA
            // 
            this.CheckDesgloseIVA.Location = new System.Drawing.Point(7, 65);
            this.CheckDesgloseIVA.Name = "CheckDesgloseIVA";
            this.CheckDesgloseIVA.Size = new System.Drawing.Size(163, 18);
            this.CheckDesgloseIVA.TabIndex = 4;
            this.CheckDesgloseIVA.Text = "Desglose de Traslado de IVA";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPageView1.Location = new System.Drawing.Point(0, 40);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage2;
            this.radPageView1.Size = new System.Drawing.Size(646, 305);
            this.radPageView1.TabIndex = 7;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.groupBox3);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(625, 257);
            this.radPageViewPage1.Text = "General";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.radLabel3);
            this.radPageViewPage2.Controls.Add(this.Pagare);
            this.radPageViewPage2.Controls.Add(this.Leyenda1);
            this.radPageViewPage2.Controls.Add(this.radLabel4);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(51F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(625, 257);
            this.radPageViewPage2.Text = "Pagaré";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(4, 15);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(41, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Pagaré";
            // 
            // Pagare
            // 
            this.Pagare.AutoSize = false;
            this.Pagare.Location = new System.Drawing.Point(4, 39);
            this.Pagare.Multiline = true;
            this.Pagare.Name = "Pagare";
            this.Pagare.Size = new System.Drawing.Size(611, 82);
            this.Pagare.TabIndex = 5;
            // 
            // Leyenda1
            // 
            this.Leyenda1.AutoSize = false;
            this.Leyenda1.Location = new System.Drawing.Point(4, 151);
            this.Leyenda1.Multiline = true;
            this.Leyenda1.Name = "Leyenda1";
            this.Leyenda1.Size = new System.Drawing.Size(611, 91);
            this.Leyenda1.TabIndex = 6;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(1, 127);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(57, 18);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "Leyenda 1";
            // 
            // Cerrar
            // 
            this.Cerrar.Location = new System.Drawing.Point(559, 351);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 8;
            this.Cerrar.Text = "Cerrar";
            // 
            // Guardar
            // 
            this.Guardar.Location = new System.Drawing.Point(478, 351);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(75, 23);
            this.Guardar.TabIndex = 9;
            this.Guardar.Text = "Guardar";
            // 
            // RemisionConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 386);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RemisionConfigurationForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.RemisionConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox3)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPagare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckTotalEnLetra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckDesgloseIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage2.ResumeLayout(false);
            this.radPageViewPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pagare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Leyenda1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadGroupBox groupBox3;
        private Telerik.WinControls.UI.RadCheckBox CheckDesgloseIVA;
        private Telerik.WinControls.UI.RadCheckBox CheckTotalEnLetra;
        private Telerik.WinControls.UI.RadCheckBox CheckPagare;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox Pagare;
        private Telerik.WinControls.UI.RadTextBox Leyenda1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadButton Guardar;
    }
}