﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Abstracts {
    public abstract class ComprobanteService : CertificacionService {
        #region declaraciones
        protected ISqlComprobanteFiscalRepository _ComprobanteFiscalRepository;
        protected ISqlSerieRepository _SerieRepository;
        protected IEditaBucketService s3;
        #endregion

        public ComprobanteService() {
            this._ComprobanteFiscalRepository = new SqlSugarComprobanteFiscalRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._SerieRepository = new SqlSugarSerieRepository(ConfigService.Synapsis.RDS.Edita);
            this.s3 = new EditaBucketService();
        }

        /// <summary>
        /// listado de series disponibles para comprobantes
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public BindingList<SerieFolioModel> GetSeries(bool onlyActive) {
            return new BindingList<SerieFolioModel>(this._SerieRepository.GetList().ToList());
        }
    }
}
