﻿using System.Collections.Generic;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Aplication.Comprobante.Builder {
    public interface IComprobanteFiscalBuilder {
        IComprobanteFiscalConceptoBuilder New(CFDITipoComprobanteEnum tipo);
        IComprobanteFiscalTempleteBuilder Templete();
    }

    public interface IComprobanteFiscalConceptoBuilder {
        IComprobanteFiscalConceptoBuilder Add(IComprobanteConceptoDetailModel concepto);
        IComprobanteFiscalDetailModel Build();
    }

    public interface IComprobanteFiscalImpuestoBuilder {
        IComprobanteFiscalImpuestoBuilder Add(ComprobanteConceptoImpuesto impuesto);
        IComprobanteFiscalDetailModel Build();
    }

    /// <summary>
    /// Templetes disponibles
    /// </summary>
    public interface IComprobanteFiscalTempleteBuilder {
        IComprobanteInfoGlobalBuilder Global();
        IComprobanteFiscalBuild CartaPorte();
        IComprobanteFiscalRecepcionPago RecepcionPago();

        IComprobanteFiscalNotaCredito NotaCredito();
    }

    /// <summary>
    /// Comprobante nota de credito
    /// </summary>
    public interface IComprobanteFiscalNotaCredito {
        IComprobanteFiscalBuild With(List<ComprobanteFiscalDetailSingleModel> cfdis);
    }

    /// <summary>
    /// Comprobante de publico en general Informacion Global
    /// </summary>
    public interface IComprobanteInfoGlobalBuilder {
        IComprobanteFiscalBuild Add(ComprobanteInformacionGlobalDetailModel InformacionGlobal);
        IComprobanteFiscalDetailModel Build();
    }

    /// <summary>
    /// Comprobante de Recepcion de Pagos
    /// </summary>
    public interface IComprobanteFiscalRecepcionPago {
        IComprobanteFiscalRecepcionPago Add(PagosPagoDoctoRelacionadoDetailModel docto);
        IComprobanteFiscalBuild Add(List<ComprobanteFiscalDetailSingleModel> lista);
        IComprobanteFiscalDetailModel Build();
    }

    public interface IComprobanteFiscalBuild {
        IComprobanteFiscalDetailModel Build();
    }
}
