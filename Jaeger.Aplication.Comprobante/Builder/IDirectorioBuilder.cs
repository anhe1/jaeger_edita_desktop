﻿using System.Collections.Generic;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Comprobante.Builder {
    public interface IDirectorioBuilder {
        IDirectorioRelacionBuilder Add(IComprobanteContribuyente model);
        IEnumerable<IComprobanteContribuyente> GetList(TipoRelacionComericalEnum relacion);
    }

    public interface IDirectorioRelacionBuilder {
        IDirectorioExecuteBuilder AddRelacion(TipoRelacionComericalEnum relacion);
    }

    public interface IDirectorioExecuteBuilder {
        IDirectorioBuild Execute();
    }

    public interface IDirectorioBuild {
        IComprobanteContribuyente Get();
    }
}
