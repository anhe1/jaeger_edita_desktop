﻿using Jaeger.Domain.Comprobante.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Aplication.Comprobante.Builder {
    public interface IComprobanteSerializerBuilder {
        IComprobanteSerializerXMLBuilder Load(string xml);
        IComprobanteSerializerXMLBuilder Load(FileInfo xml);
        IComprobanteSerializerXMLBuilder Load(int idComprobante);
        IComprobanteSerializerXMLBuilder Load(ComprobanteFiscalDetailSingleModel singleModel);
    }

    public interface IComprobanteSerializerXMLBuilder {
    }

    public class ComprobanteSerializerBuilder : IComprobanteSerializerBuilder {
        public IComprobanteSerializerXMLBuilder Load(string xml) {
            throw new NotImplementedException();
        }

        public IComprobanteSerializerXMLBuilder Load(FileInfo xml) {
            throw new NotImplementedException();
        }

        public IComprobanteSerializerXMLBuilder Load(int idComprobante) {
            throw new NotImplementedException();
        }

        public IComprobanteSerializerXMLBuilder Load(ComprobanteFiscalDetailSingleModel singleModel) {
            throw new NotImplementedException();
        }
    }
}
