﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Comprobante.Builder {
    public class DirectorioBuilder : IDirectorioBuilder, IDirectorioRelacionBuilder, IDirectorioExecuteBuilder, IDirectorioBuild {
        #region declaraciones
        protected internal Domain.Comprobante.Contracts.ISqlContribuyenteRepository _Directorio;
        protected internal ISqlRelacionComercialRepository _Relacion;
        protected internal IComprobanteContribuyente _Contribuyente;
        #endregion

        public DirectorioBuilder() {
            this._Directorio = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._Relacion  = new SqlRelacionComercialRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IDirectorioRelacionBuilder Add(IComprobanteContribuyente model) {
            this._Contribuyente = model;
            return this;
        }

        public IDirectorioExecuteBuilder AddRelacion(TipoRelacionComericalEnum tipoRelacion) {
            this._Contribuyente.Relaciones.Add(new RelacionComercialModel { Activo = true, IdTipoRelacion = (int)tipoRelacion });
            this._Contribuyente.Relacion = tipoRelacion.ToString();
            return this;
        }

        public IDirectorioBuild Execute() {
            var d0 = this._Directorio.Save(this._Contribuyente as ComprobanteContribuyenteModel);
            if (d0.IdDirectorio > 0) {
                for (int i = 0; i < d0.Relaciones.Count; i++) {
                    d0.Relaciones[i].AddIndice(d0.IdDirectorio);
                    this._Relacion.Saveable(d0.Relaciones[i]);
                }
            }
            return this;
        }

        public IComprobanteContribuyente Get() {
            return this._Contribuyente;
        }

        public IEnumerable<IComprobanteContribuyente> GetList(TipoRelacionComericalEnum relacion) {
            var d0 = this._Directorio.GetList<ComprobanteContribuyenteModel>(new List<IConditional> { new Conditional("_drctr_rlcn", relacion.ToString(), ConditionalTypeEnum.In) });
            return d0.ToList<IComprobanteContribuyente>();
        }

        public static IContribuyenteQueryBuilder Create() {
            return new ContribuyenteQueryBuilder();
        }
    }
}
