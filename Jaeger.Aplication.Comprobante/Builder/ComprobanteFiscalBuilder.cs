﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Comprobante.Builder {
    public class ComprobanteFiscalBuilder : IComprobanteFiscalBuilder, IComprobanteFiscalBuild, IComprobanteFiscalConceptoBuilder, IComprobanteFiscalImpuestoBuilder, IComprobanteInfoGlobalBuilder,
        IComprobanteFiscalTempleteBuilder, IComprobanteFiscalRecepcionPago, IComprobanteFiscalNotaCredito {
        #region declaraciones
        private readonly IComprobanteFiscalDetailModel _Comprobante;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalBuilder() {
            this._Comprobante = new ComprobanteFiscalDetailModel() {
                Activo = true,
                Status = "Pendiente",
                SubTipo = CFDISubTipoEnum.Emitido,
                PrecisionDecimal = 2,
                FechaEmision = DateTime.Now,
                TipoComprobante = CFDITipoComprobanteEnum.Ingreso,
                LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal,
                Emisor = new ComprobanteContribuyenteModel {
                    RFC = ConfigService.Synapsis.Empresa.RFC,
                    Nombre = ConfigService.Synapsis.Empresa.RazonSocial,
                    RegimenFiscal = ConfigService.Synapsis.Empresa.RegimenFiscal
                },
                Creo = ConfigService.Piloto.Clave
            };
            this._Comprobante.Default();
            this._Comprobante.Conceptos = new BindingList<ComprobanteConceptoDetailModel>();
        }

        public IComprobanteFiscalDetailModel Build() {
            return this._Comprobante;
        }

        public IComprobanteFiscalConceptoBuilder New(CFDITipoComprobanteEnum tipo) {
            this._Comprobante.TipoComprobante = tipo;
            return this;
        }

        public IComprobanteFiscalConceptoBuilder Add(IComprobanteConceptoDetailModel concepto) {
            this._Comprobante.Conceptos.Add(concepto as ComprobanteConceptoDetailModel);
            return this;
        }

        public IComprobanteFiscalImpuestoBuilder Add(ComprobanteConceptoImpuesto impuesto) {
            if (this._Comprobante.Conceptos[this._Comprobante.Conceptos.Count].Impuestos == null) {
                this._Comprobante.Conceptos[this._Comprobante.Conceptos.Count].Impuestos = new BindingList<ComprobanteConceptoImpuesto>();
            }
            this._Comprobante.Conceptos[this._Comprobante.Conceptos.Count].Impuestos.Add(impuesto);
            return this;
        }

        #region templetes
        public IComprobanteFiscalTempleteBuilder Templete() {
            return this;
        }

        public IComprobanteFiscalBuild CartaPorte() {
            this._Comprobante.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
            return this;
        }

        public IComprobanteFiscalRecepcionPago RecepcionPago() {
            this._Comprobante.TipoComprobante = CFDITipoComprobanteEnum.Pagos;
            this._Comprobante.Exportacion = CFDIExportacionEnum.No_Aplica;
            this._Comprobante.ClaveMoneda = "XXX";
            this._Comprobante.ClaveMetodoPago = null;
            this._Comprobante.LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal;
            this._Comprobante.RecepcionPago = new BindingList<ComplementoPagoDetailModel>();
            this.Add(new ComprobanteConceptoDetailModel() {
                Activo = true,
                Cantidad = 1,
                ClaveUnidad = "ACT",
                ClaveProdServ = "84111506",
                Descripcion = "Pago",
                ValorUnitario = 0,
                ObjetoDeImpuesto = CFDIObjetoImpuestoEnum.No_objeto_de_impuesto
            });
            var pago = new ComplementoPagoDetailModel {
                DoctoRelacionados = new BindingList<PagosPagoDoctoRelacionadoDetailModel>()
            };
            this._Comprobante.RecepcionPago.Add(pago);
            return this;
        }

        public IComprobanteFiscalRecepcionPago SPEI(string localFileName) {
            var spei = SAT.CFDI.Complemento_SPEI.Load(localFileName);
            if (spei == null) {
                SAT.CFDI.SpeiTercero spei3 = SAT.CFDI.SpeiTercero.LoadX(localFileName);
                if (spei3 == null) {
                    this._Comprobante.RecepcionPago.Add(SPEIExtensions.Create(spei3));
                }
            }
            return this;
        }

        public IComprobanteFiscalRecepcionPago Add(PagosPagoDoctoRelacionadoDetailModel docto) {

            throw new NotImplementedException();
        }

        public IComprobanteFiscalBuild Add(List<ComprobanteFiscalDetailSingleModel> lista) {
            var pago = this._Comprobante.RecepcionPago[0];
            foreach (ComprobanteFiscalDetailSingleModel documento in lista) {
                var d = new PagosPagoDoctoRelacionadoDetailModel {
                    IdDocumentoDR = documento.IdDocumento,
                    SerieDR = documento.Serie,
                    Folio = documento.Folio,
                    FechaEmision = documento.FechaEmision,
                    MetodoPago = documento.ClaveMetodoPago,
                    Nombre = documento.ReceptorNombre,
                    RFC = documento.ReceptorRFC,
                    MonedaDR = documento.ClaveMoneda,
                    ImpPagado = documento.SaldoPagos,
                    ImpSaldoAnt = documento.SaldoPagos,
                    ImpSaldoInsoluto = 0,
                    NumParcialidad = documento.NumParcialidad + 1,
                    FormaDePagoP = documento.ClaveFormaPago,
                    EquivalenciaDR = documento.TipoCambio,
                    Activo = true,
                    IdComprobanteR = documento.Id,
                    IdSubTipo = documento.IdSubTipo,
                    Total = documento.Total,
                };

                if (documento.Version == "4.0") {
                    d.ObjetoDeImpuesto = CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto;
                }

                if (documento.SaldoPagos == 0) {
                    d.ImpSaldoAnt = documento.Total;
                    d.ImpPagado = documento.Total;
                } else {
                    d.ImpSaldoAnt = documento.SaldoPagos;
                    d.ImpPagado = documento.Total - documento.SaldoPagos;
                }

                pago.DoctoRelacionados.Add(d);
                this._Comprobante.Receptor.RFC = documento.ReceptorRFC;
                this._Comprobante.Receptor.Nombre = documento.ReceptorNombre;
            }
            pago.Monto = pago.DoctoRelacionados.Sum(x => x.ImpPagado);
            pago.FechaPagoP = DateTime.Now;
            pago.MonedaP = "MXN";
            pago.FormaDePagoP = "03";
            ///_response.RecepcionPago.Add(pago);
            return this;
        }

        public IComprobanteInfoGlobalBuilder Global() {
            // informacion del receptor
            this._Comprobante.ReceptorRFC = "XAXX010101000";
            this._Comprobante.ClaveUsoCFDI = "P01";
            // informacion del concepto
            this._Comprobante.Documento = "Global";
            this.Add(new ComprobanteConceptoDetailModel() { Activo = true, Cantidad = 1, ClaveUnidad = "ACT", ClaveProdServ = "01010101", Descripcion = "Venta" });
            this.Add(new ComprobanteInformacionGlobalDetailModel { Activo = true, Anio = DateTime.Now.Year, Periodicidad = "02", ClaveMeses = DateTime.Now.Month.ToString("00") });
            return this;
        }

        public IComprobanteFiscalBuild Add(ComprobanteInformacionGlobalDetailModel InformacionGlobal) {
            this._Comprobante.InformacionGlobal = InformacionGlobal;
            return this;
        }

        public IComprobanteFiscalNotaCredito NotaCredito() {
            this._Comprobante.TipoComprobante = CFDITipoComprobanteEnum.Egreso;
            this._Comprobante.ClaveMetodoPago = "PUE";
            this._Comprobante.ClaveFormaPago = "03";
            this._Comprobante.CfdiRelacionados = new ComprobanteCfdiRelacionadosModel {
                TipoRelacion = new ComprobanteCfdiRelacionadoRelacion {
                    Clave = "01",
                    Descripcion = "Nota de crédito de los documentos relacionados"
                }
            };
            return this;
        }

        public IComprobanteFiscalBuild With(List<ComprobanteFiscalDetailSingleModel> cfdis) {
            decimal subTotal = 0;
            foreach (var item in cfdis) {
                this._Comprobante.CfdiRelacionados.CfdiRelacionado.Add(new ComprobanteCfdiRelacionadosCfdiRelacionadoModel {
                    IdDocumento = item.IdDocumento,
                    EstadoSAT = item.Estado,
                    FechaEmision = item.FechaEmision,
                    Folio = item.Folio,
                    FormaPago = item.ClaveFormaPago,
                    MetodoPago = item.ClaveMetodoPago,
                    Moneda = item.ClaveMoneda,
                    Nombre = item.ReceptorNombre,
                    RFC = item.ReceptorRFC,
                    Serie = item.Serie,
                    Estado = EdoPagoDoctoRelEnum.Relacionado,
                    SubTipo = item.SubTipo,
                    Total = item.Total
                });
                subTotal = subTotal + item.SubTotal;
                //this._Comprobante.ReceptorNombre = item.ReceptorNombre;
                //this._Comprobante.ReceptorRFC = item.ReceptorRFC;
            }

            this._Comprobante.Conceptos = new BindingList<ComprobanteConceptoDetailModel> {
                new ComprobanteConceptoDetailModel {
                    Cantidad = 1,
                    ClaveUnidad = "ACT",
                    ClaveProdServ = "84111506",
                    Descripcion = "Servicios de Facturación",
                    ValorUnitario = subTotal
                }
            };

            this._Comprobante.Conceptos[0].Impuestos = new BindingList<ComprobanteConceptoImpuesto> {
                new ComprobanteConceptoImpuesto {
                    Tipo = TipoImpuestoEnum.Traslado,
                    Impuesto = ImpuestoEnum.IVA,
                    TipoFactor = FactorEnum.Tasa,
                    TasaOCuota = new decimal(0.16),
                    Base = this._Comprobante.Conceptos[0].Importe
                }
            };
            this._Comprobante.Conceptos[0].Cantidad = 1;
            return this;
        }
        #endregion

        #region metodos estaticos
        public static ComprobanteConceptoImpuesto GetImpuestoTrasladoIVA(decimal base1) {
            return new ComprobanteConceptoImpuesto {
                Tipo = TipoImpuestoEnum.Traslado,
                TipoFactor = FactorEnum.Tasa,
                Impuesto = ImpuestoEnum.IVA,
                Base = base1
            };
        }

        public static IComprobanteFiscalDetailModel Build(string base64) {
            SAT.Reader.CFD.Interfaces.ICFDReader _Reader = new SAT.Reader.CFD.CFDReader();
            var bytes64 = Convert.FromBase64String(base64);
            var documentoFiscal = _Reader.Read(bytes64);
            IComprobanteFiscalDetailModel c0 = new ComprobanteFiscalDetailModel {
                Folio = documentoFiscal.Folio,
                Serie = documentoFiscal.Serie,
                FechaEmision = documentoFiscal.FechaEmision.Value,
                ClaveFormaPago = documentoFiscal.FormaPago,
                ClaveMetodoPago = documentoFiscal.MetodoPago,
                ClaveMoneda = documentoFiscal.Moneda,
                ReceptorRFC = documentoFiscal.ReceptorRFC,
                ReceptorNombre = documentoFiscal.ReceptorNombre,
                EmisorNombre = documentoFiscal.EmisorNombre,
                EmisorRFC = documentoFiscal.EmisorRFC,
                DomicilioFiscal = documentoFiscal.ReceptorDomicilioFiscal,
                IdDocumento = documentoFiscal.IdDocumento,
                Total = documentoFiscal.Total,
                SubTotal = documentoFiscal.SubTotal,
                RetencionIVA = documentoFiscal.RetencionIVA,
                RetencionIEPS = documentoFiscal.RetencionIEPS,
                TrasladoIVA = documentoFiscal.TrasladoIVA,
                TrasladoIEPS = documentoFiscal.TrasladoIEPS,
                ClaveUsoCFDI = documentoFiscal.UsoCFDI,
                Version = documentoFiscal.Version,
                FechaTimbre = documentoFiscal.FechaTimbrado,
                CondicionPago = documentoFiscal.CondicionPago,
                RFCProvCertif = documentoFiscal.RFCProvCertif,
                Descuento = documentoFiscal.Descuento,
                ClaveExportacion = documentoFiscal.ClaveExportacion,
                LugarExpedicion = documentoFiscal.LugarExpedicion,
                NoCertificado = documentoFiscal.NoCertificado,
            };

            foreach (var item in documentoFiscal.Conceptos) {
                c0.Conceptos.Add(new ComprobanteConceptoDetailModel { 
                    Cantidad = item.Cantidad, 
                    NoIdentificacion = item.NoIdentificacion , Descripcion = item.Descripcion
                });
            }

            return c0;
        }
        #endregion
    }
}
