﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Services {
    public class FolioYSeriesService : IFolioYSeriesService {
        protected ISqlSerieRepository serieRepository;

        public FolioYSeriesService() {
            this.serieRepository = new SqlSugarSerieRepository(ConfigService.Synapsis.RDS.Edita);
        }

        /// <summary>
        /// listado de series disponibles para comprobantes
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public BindingList<SerieFolioModel> GetSeries(bool onlyActive) {
            return new BindingList<SerieFolioModel>(this.serieRepository.GetList().ToList());
        }
    }
}
