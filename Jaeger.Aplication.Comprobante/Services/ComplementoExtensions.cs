﻿using System;
using System.Text.RegularExpressions;
using Jaeger.Domain.Comprobante.Entities.Complemento;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComplementoExtensions {
        #region metodos publicos
        public decimal FormatoNumero(decimal numero, int decimales) {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        public decimal ShowDecimalRound(decimal Argument, int Digits = 6) {
            return decimal.Round(Argument, Digits);
        }

        public decimal Ajustar(decimal value, int decimales = 2) {
            var _variante = new decimal(0.0000000000000000001);
            return Math.Round(value + _variante, decimales, MidpointRounding.AwayFromZero);
        }

        public decimal FixDecimal(decimal value, int minDecimal = 2, int maxDecimal = 6) {
            var decimalPlace = this.DecimalPlace(value);
            if (!EsPar(decimalPlace)) {
                decimalPlace += 1;
            }

            decimal d;
            if (decimalPlace <= minDecimal) {
                d = this.Ajustar(value, minDecimal);
            } else if (decimalPlace > maxDecimal) {
                var d1 = value.ToString().Split('.');
                
                string output = Regex.Replace(d1[1], @"[0]*$", "");
                if (output.Length > 0) {
                    d = this.Ajustar(value, output.Length);
                } else {
                    d = this.Ajustar(value, maxDecimal);
                }
            } else {
                d = this.Ajustar(value, decimalPlace);
            }

            return d;
        }

        public int DecimalPlace(decimal argument) {
            int count = BitConverter.GetBytes(decimal.GetBits(argument)[3])[2];
            return count;
        }

        public decimal RegresaDecimalesXOpcion(decimal valor, short OpcionDecimales, short NumeroDecimales) {
            decimal tmpValor;

            if (OpcionDecimales == 1) // Truncar los resultados 
            {
                var formula = Math.Pow(10, NumeroDecimales);
                decimal tmpValDecimal = valor * 100;
                tmpValor = Convert.ToDecimal(Math.Truncate(Convert.ToDouble(tmpValDecimal)) / formula);
            } else // Redondear al numero de decimales hacia arriba 
              {
                tmpValor = Math.Round(valor, NumeroDecimales, MidpointRounding.AwayFromZero);
            }

            tmpValor = Convert.ToDecimal(string.Format("{0:N" + NumeroDecimales + "}", tmpValor));

            return tmpValor;
        }
        public string RegresaStringDecimalesXOpcion(decimal valor, short OpcionDecimales, short NumeroDecimales) {
            return RegresaDecimalesXOpcion(valor, OpcionDecimales, NumeroDecimales).ToString();
        }

        public static bool EsPar(int numero) {
            if ((numero % 2) == 0) {
                return true;
            } else {
                return false;
            }
        }

        /// <summary>
        /// convertir un objeto TimbreFiscal v11 al objeto comun 
        /// </summary>
        public static ComplementoTimbreFiscal TimbreFiscal(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbre11) {
            var response = new ComplementoTimbreFiscal {
                UUID = timbre11.UUID.ToUpper(),
                Version = timbre11.Version,
                FechaTimbrado = timbre11.FechaTimbrado,
                Leyenda = timbre11.Leyenda,
                NoCertificadoSAT = timbre11.NoCertificadoSAT,
                RFCProvCertif = timbre11.RfcProvCertif,
                SelloCFD = timbre11.SelloCFD,
                SelloSAT = timbre11.SelloSAT
            };
            return response;
        }

        /// <summary>
        /// convertir un objeto TimbreFiscal v10 al objeto comun 
        /// </summary>
        public static ComplementoTimbreFiscal TimbreFiscal(SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital timbre10) {
            var response = new ComplementoTimbreFiscal {
                UUID = timbre10.UUID.ToUpper(),
                Version = timbre10.version,
                FechaTimbrado = timbre10.FechaTimbrado,
                Leyenda = string.Empty,
                NoCertificadoSAT = timbre10.noCertificadoSAT,
                RFCProvCertif = string.Empty,
                SelloCFD = timbre10.selloCFD,
                SelloSAT = timbre10.selloSAT
            };
            return response;
        }
        #endregion
    }
}
