﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.Aplication.Comprobante.Services {
    public class SPEIExtensions {
        #region complemento spei

        public ComplementoSPEI SPEI(SAT.CFDI.Complemento_SPEI objeto) {
            if (objeto != null) {
                ComplementoSPEI newItem = new ComplementoSPEI();
                newItem.SpeiTercero = SPEI(objeto.SPEI_Tercero);
                return newItem;
            }
            return null;
        }

        public BindingList<ComplementoSpeiTercero> SPEI(SAT.CFDI.Complemento_SPEISPEI_Tercero[] objetos) {
            if (objetos != null) {
                BindingList<ComplementoSpeiTercero> newItems = new BindingList<ComplementoSpeiTercero>();
                foreach (SAT.CFDI.Complemento_SPEISPEI_Tercero item in objetos) {
                    ComplementoSpeiTercero newItem = new ComplementoSpeiTercero();
                    newItem.Beneficiario = SPEI(item.Beneficiario);
                    newItem.CadenaCda = item.cadenaCDA;
                    newItem.ClaveSpei = item.ClaveSPEI;
                    newItem.FechaOperacion = item.FechaOperacion;
                    newItem.Hora = item.Hora;
                    newItem.NumeroCertificado = item.numeroCertificado;
                    newItem.Ordenante = this.Spei(item.Ordenante);
                    newItem.Sello = item.sello;
                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        public ComplementoSpeiTerceroBeneficiario SPEI(SAT.CFDI.Complemento_SPEISPEI_TerceroBeneficiario objeto) {
            if (objeto != null) {
                ComplementoSpeiTerceroBeneficiario newItem = new ComplementoSpeiTerceroBeneficiario();
                newItem.BancoReceptor = objeto.BancoReceptor;
                newItem.Concepto = objeto.Concepto;
                newItem.Cuenta = objeto.Cuenta;
                newItem.IVA = objeto.IVA;
                newItem.MontoPago = objeto.MontoPago;
                newItem.Nombre = objeto.Nombre;
                newItem.RFC = objeto.RFC;
                newItem.TipoCuenta = objeto.TipoCuenta;
            }
            return null;
        }

        public ComplementoSpeiTerceroOrdenante Spei(SAT.CFDI.Complemento_SPEISPEI_TerceroOrdenante objeto) {
            if (objeto != null) {
                ComplementoSpeiTerceroOrdenante newItem = new ComplementoSpeiTerceroOrdenante();
                newItem.BancoEmisor = objeto.BancoEmisor;
                newItem.Cuenta = objeto.Cuenta;
                newItem.Nombre = objeto.Nombre;
                newItem.RFC = objeto.RFC;
                newItem.TipoCuenta = objeto.TipoCuenta;
                return newItem;
            }
            return null;
        }

        public static ComplementoPagoDetailModel Create(SAT.CFDI.SpeiTercero objeto) {
            if (objeto != null) {
                var newItem = new ComplementoPagoDetailModel();
                newItem.FechaPagoP = new DateTime(objeto.FechaOperacion.Year, objeto.FechaOperacion.Month, objeto.FechaOperacion.Day, objeto.Hora.Hour, objeto.Hora.Minute, objeto.Hora.Second);
                newItem.FormaDePagoP = "03";
                newItem.MonedaP = "MXN";
                newItem.Monto = objeto.Beneficiario.MontoPago;
                newItem.RfcEmisorCtaOrd = objeto.Ordenante.RFC;
                newItem.NomBancoOrdExt = objeto.Ordenante.BancoEmisor;
                newItem.CtaOrdenante = objeto.Ordenante.Cuenta.ToString();
                newItem.RfcEmisorCtaBen = objeto.Beneficiario.RFC;
                newItem.CtaBeneficiario = objeto.Beneficiario.Cuenta.ToString();
                newItem.CertPago = objeto.numeroCertificado.ToString();
                newItem.CadPago = objeto.cadenaCDA;
                newItem.SelloPago = objeto.sello;
                return newItem;
            }
            return null;
        }

        #endregion
    }
}
