﻿using System.Linq;
using System.ComponentModel;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ContribuyenteService : IContribuyenteService {
        protected CFDISubTipoEnum subTipo;
        protected ISqlContribuyenteRepository comprobanteContribuyenteRepository;

        public ContribuyenteService(CFDISubTipoEnum subTipo) {
            this.subTipo = subTipo;
            this.comprobanteContribuyenteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        public BindingList<ComprobanteContribuyenteModel> GetContribuyentes() {
            if (this.subTipo == CFDISubTipoEnum.Emitido)
                return new BindingList<ComprobanteContribuyenteModel>(this.comprobanteContribuyenteRepository.GetList("Cliente").ToList());
            else if (this.subTipo == CFDISubTipoEnum.Recibido)
                return new BindingList<ComprobanteContribuyenteModel>(this.comprobanteContribuyenteRepository.GetList("Proveedor").ToList());
            return null;
        }
    }
}
