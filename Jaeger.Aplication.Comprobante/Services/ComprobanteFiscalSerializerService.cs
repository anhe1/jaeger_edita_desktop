﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Mappers;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteFiscalSerializerService : IComprobanteFiscalSerializerService {
        #region declaraciones
        protected ISqlComprobanteFiscalRepository _ComprobanteFiscalRepository;
        protected ISqlComplementoPagoRepository _ComplementoPagoRepository;
        protected ISqlComplementoCartaPorteRepository _ComplementoCartaPorteRepository;
        protected ISqlSerieRepository _SerieRepository;
        protected internal ISqlComprobanteFiscalConceptoRepository _ConceptoRepository;
        protected IEditaBucketService s3;
        protected internal ISqlComplementoNominaRepository nominaRepository;
        protected internal ISqlComplmentoNominaParteRepository parteRepository;
        #endregion

        public ComprobanteFiscalSerializerService() {
            this._ComprobanteFiscalRepository = new SqlSugarComprobanteFiscalRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._ComplementoPagoRepository = new SqlSugarComplementoPagoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._ComplementoCartaPorteRepository = new SqlSugarComplementoCartaPorteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._ConceptoRepository = new SqlSugarComprobanteFiscalConceptoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._SerieRepository = new SqlSugarSerieRepository(ConfigService.Synapsis.RDS.Edita);
            this.nominaRepository = new SqlSugarComplementoNominaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.parteRepository = new SqlSugarComplementoNominaParteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.s3 = new EditaBucketService();
        }

        /// <summary>
        /// obtener comprobante fiscal detalle
        /// </summary>
        /// <param name="index">indice</param>
        public ComprobanteFiscalDetailModel GetComprobante(int index) {
            var response = this._ComprobanteFiscalRepository.GetComprobante(index);
            if (response != null) {
                if (response.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                    response.RecepcionPago = new BindingList<ComplementoPagoDetailModel>(this._ComplementoPagoRepository.GetList<ComplementoPagoDetailModel>(new List<IConditional> { new Conditional("_cmppg_cfd_id", index.ToString()) }).ToList());
                    if (response.RecepcionPago.Count == 0 && response.IsEditable) {
                        response.RecepcionPago = new BindingList<ComplementoPagoDetailModel>() { new ComplementoPagoDetailModel { FechaPagoP = DateTime.Now } };
                    }
                } else if (response.TipoComprobante == CFDITipoComprobanteEnum.Ingreso | response.TipoComprobante == CFDITipoComprobanteEnum.Traslado) {
                    if (response.Documento != null) {
                        if (response.Documento == "porte") {
                            response.CartaPorte = this._ComplementoCartaPorteRepository.GetById(index);

                            if (response.CartaPorte == null && response.IsEditable) {
                                response.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
                            }
                        }
                    }
                }
            }
            return response;
        }

        /// <summary>
        /// almacenar modelo comprobante
        /// </summary>
        public ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel comprobante) {
            comprobante = this._ComprobanteFiscalRepository.Save(comprobante);
            if (comprobante.Id > 0) {
                // en caso de existir complemento de pago se actualizan las referencias del comprobante
                if (comprobante.RecepcionPago != null) {
                    for (int i = 0; i < comprobante.RecepcionPago.Count; i++) {
                        comprobante.RecepcionPago[i].IdComprobanteP = comprobante.Id;
                    }
                    comprobante.RecepcionPago = this._ComplementoPagoRepository.Save(comprobante.RecepcionPago);
                }

                if (comprobante.CartaPorte != null) {
                    comprobante.CartaPorte.IdComprobante = comprobante.Id;
                    comprobante.CartaPorte = this._ComplementoCartaPorteRepository.Save(comprobante.CartaPorte);
                }

                if (comprobante.Nomina12 != null) {
                    comprobante.Nomina12.SubId = comprobante.Id;
                    comprobante.Nomina12.IdDocumento = comprobante.IdDocumento;
                    comprobante.Nomina12 = this.nominaRepository.Save(comprobante.Nomina12) as ComplementoNominaDetailModel;
                }
            }
            return comprobante;
        }

        /// <summary>
        /// Buscar listado de comprobantes fiscales utilizando el id de documento (UUID)
        /// </summary>
        public BindingList<ComprobanteFiscalDetailSingleModel> SearchUUID(List<string> vs) {
            if (vs != null) {
                if (vs.Count > 0) {
                    var uuids = string.Join(",", vs);
                    return new BindingList<ComprobanteFiscalDetailSingleModel>(this._ComprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(
                        new List<IConditional> {
                            new Conditional("_cfdi_uuid", uuids, ConditionalTypeEnum.In)}).ToList());
                }
            }
            return null;
        }

        /// <summary>
        /// verificar y relacionar documentos del complemento de pagos
        /// </summary>
        public BindingList<ComplementoPagoDetailModel> Verificar(BindingList<ComplementoPagoDetailModel> models) {
            if (models != null) {
                if (models.Count > 0) {
                    for (int p = 0; p < models.Count; p++) {
                        if (models[p].DoctoRelacionados != null) {
                            if (models[p].DoctoRelacionados.Count > 0) {
                                var uuids = models[p].DoctoRelacionados.Select(it => it.IdDocumentoDR.ToUpper()).ToList();
                                var registros = this.SearchUUID(uuids);
                                if (registros != null) {
                                    if (registros.Count > 0) {
                                        for (int i = 0; i < models[p].DoctoRelacionados.Count; i++) {
                                            try {
                                                var uuid = registros.Where(it => it.IdDocumento.ToUpper() == models[p].DoctoRelacionados[i].IdDocumentoDR.ToUpper()).FirstOrDefault();
                                                if (uuid != null) {
                                                    models[p].DoctoRelacionados[i].IdComprobanteR = uuid.Id;
                                                    models[p].DoctoRelacionados[i].FechaEmision = uuid.FechaEmision;
                                                    models[p].DoctoRelacionados[i].FormaDePagoP = uuid.ClaveFormaPago;
                                                    models[p].DoctoRelacionados[i].MetodoPago = uuid.ClaveMetodoPago;
                                                    models[p].DoctoRelacionados[i].IdSubTipo = uuid.IdSubTipo;
                                                    if (uuid.SubTipo == CFDISubTipoEnum.Emitido) {
                                                        models[p].DoctoRelacionados[i].Nombre = uuid.ReceptorNombre;
                                                        models[p].DoctoRelacionados[i].RFC = uuid.ReceptorRFC;
                                                    } else if (uuid.SubTipo == CFDISubTipoEnum.Recibido) {
                                                        models[p].DoctoRelacionados[i].Nombre = uuid.EmisorNombre;
                                                        models[p].DoctoRelacionados[i].RFC = uuid.EmisorRFC;
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                Console.WriteLine(ex.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return models;
        }

        /// <summary>
        /// verificar la existencia de los documentos relacionados
        /// </summary>
        public ComprobanteCfdiRelacionadosModel Verificar(ComprobanteCfdiRelacionadosModel model) {
            if (model != null) {
                if (model.CfdiRelacionado != null) {
                    if (model.CfdiRelacionado.Count > 0) {
                        var uuids = model.CfdiRelacionado.Select(it => it.IdDocumento).ToList();
                        var registros = this.SearchUUID(uuids);
                        if (registros != null) {
                            if (registros.Count > 0) {
                                for (int i = 0; i < model.CfdiRelacionado.Count; i++) {
                                    try {
                                        var uuid = registros.Where(it => it.IdDocumento == model.CfdiRelacionado[i].IdDocumento.ToUpper()).FirstOrDefault();
                                        if (uuid != null) {

                                            model.CfdiRelacionado[i].FechaEmision = uuid.FechaEmision;
                                            model.CfdiRelacionado[i].Folio = uuid.Folio;
                                            model.CfdiRelacionado[i].FormaPago = uuid.ClaveFormaPago;
                                            model.CfdiRelacionado[i].MetodoPago = uuid.ClaveFormaPago;
                                            model.CfdiRelacionado[i].Moneda = uuid.ClaveMoneda;
                                            model.CfdiRelacionado[i].Serie = uuid.Serie;
                                            model.CfdiRelacionado[i].TipoCambio = uuid.TipoCambio;
                                            model.CfdiRelacionado[i].SubTipo = uuid.SubTipo;
                                            model.CfdiRelacionado[i].Total = uuid.Total;

                                            if (uuid.SubTipo == CFDISubTipoEnum.Emitido) {
                                                model.CfdiRelacionado[i].Nombre = uuid.ReceptorNombre;
                                                model.CfdiRelacionado[i].RFC = uuid.ReceptorRFC;
                                            } else if (uuid.SubTipo == CFDISubTipoEnum.Recibido) {
                                                model.CfdiRelacionado[i].Nombre = uuid.EmisorNombre;
                                                model.CfdiRelacionado[i].RFC = uuid.EmisorRFC;
                                            }
                                        }
                                    } catch (Exception ex) {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return model;
        }

        public ComprobanteFiscalDetailModel Serializar(int index) {
            // obtener comprobante fiscal
            var cfdi = this.GetComprobante(index);
            if (cfdi != null) {
                // en caso de no existir un comprobante alojado en la propiedad XML 
                // descargamos desde la liga valida
                if (string.IsNullOrEmpty(cfdi.XML)) {
                    var d0 = DownloadFile(cfdi.UrlFileXML);
                    if (d0 != null) {
                        cfdi.XML = Encoding.UTF8.GetString(d0);
                    }
                }

                // volvemos a comprobar si existe XML
                if (!string.IsNullOrEmpty(cfdi.XML)) {
                    if (cfdi.Version == "3.2") {
                        var xml = SAT.CFDI.V32.Comprobante.Load(cfdi.XML);
                        if (xml == null) {
                            xml = SAT.CFDI.V32.Comprobante.LoadBytes(DownloadFile(cfdi.UrlFileXML));
                        }
                        if (xml != null) {
                            var modelo = ComprobanteExtensions.Create(xml);
                            return modelo;
                        }
                    } else if (cfdi.Version == "3.3") {
                        var xml = SAT.CFDI.V33.Comprobante.Load(cfdi.XML);
                        if (xml == null) {
                            xml = SAT.CFDI.V33.Comprobante.LoadBytes(DownloadFile(cfdi.UrlFileXML));
                        }
                        if (xml != null) {
                            var modelo = ComprobanteExtensions.Create(xml);
                            return modelo;
                        }
                    } else if (cfdi.Version == "4.0") {
                        var xml = SAT.CFDI.V40.Comprobante.Load(cfdi.XML);
                        if (xml == null) {
                            xml = SAT.CFDI.V40.Comprobante.LoadBytes(DownloadFile(cfdi.UrlFileXML));
                        }
                        if (xml != null) {
                            var modelo = ComprobanteExtensions.Create(xml);
                            return modelo;
                        }
                    }
                }
            }
            return null;
        }

        public ComprobanteFiscalDetailModel Serializar0(ComprobanteFiscalDetailSingleModel cfdi) {
            // obtener comprobante fiscal

            if (cfdi != null) {
                if (!string.IsNullOrEmpty(cfdi.UrlFileXML)) {
                    cfdi.Tag = DownloadFile(cfdi.UrlFileXML);
                }

                if (cfdi.Tag != null) {
                    if (cfdi.Version == "3.2") {
                        var xml = SAT.CFDI.V32.Comprobante.LoadBytes(DownloadFile(cfdi.UrlFileXML));
                        if (xml != null) {
                            var modelo = ComprobanteExtensions.Create(xml);
                            return modelo;
                        }
                    } else if (cfdi.Version == "3.3") {
                        var xml = SAT.CFDI.V33.Comprobante.LoadBytes(DownloadFile(cfdi.UrlFileXML));
                        if (xml != null) {
                            var modelo = ComprobanteExtensions.Create(xml);
                            return modelo;
                        }
                    } else if (cfdi.Version == "4.0") {
                        var xml = SAT.CFDI.V40.Comprobante.LoadBytes(DownloadFile(cfdi.UrlFileXML));
                        if (xml != null) {
                            var modelo = ComprobanteExtensions.Create(xml);
                            return modelo;
                        }
                    }
                }
            }
            return null;
        }

        public bool Serializar(ComprobanteFiscalDetailSingleModel singleModel) {
            if (singleModel != null) {
                if (singleModel.Version == "3.2") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    if (_xml != null) {
                        if (_xml.XML == null) {
                            var d0 = DownloadFile(_xml.UrlFileXML);
                            if (d0 != null) {
                                _xml.XML = Encoding.UTF8.GetString(d0);
                            }
                        }
                        if (string.IsNullOrEmpty(_xml.XML))
                            return false;
                        var _cfdi = SAT.CFDI.V32.Comprobante.LoadXml(_xml.XML);
                        if (_cfdi == null) {
                            _cfdi = SAT.CFDI.V32.Comprobante.LoadBytes(DownloadFile(_xml.UrlFileXML));
                        }
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.RecepcionPago == null) {
                                _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                this.Save(_xml);
                            } else {
                                if (_xml.RecepcionPago != null && _xml.RecepcionPago.Count == 0) {
                                    _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                    this.Save(_xml);
                                } else {
                                    _xml.RecepcionPago = this.Verificar(_xml.RecepcionPago);
                                    this.Save(_xml);
                                }
                            }
                        }
                    }
                } else if (singleModel.Version == "3.3") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    if (_xml != null) {
                        if (_xml.XML == null) {
                            var d0 = DownloadFile(_xml.UrlFileXML);
                            if (d0 != null) {
                                _xml.XML = Encoding.UTF8.GetString(d0);
                            }
                        }
                        if (string.IsNullOrEmpty(_xml.XML))
                            return false;
                        var _cfdi = SAT.CFDI.V33.Comprobante.LoadXml(_xml.XML);
                        if (_cfdi == null) {
                            _cfdi = SAT.CFDI.V33.Comprobante.LoadBytes(DownloadFile(_xml.UrlFileXML));
                        }
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.RecepcionPago == null) {
                                _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                this.Save(_xml);
                            } else {
                                if (_xml.RecepcionPago != null && _xml.RecepcionPago.Count == 0) {
                                    _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                    this.Save(_xml);
                                } else {
                                    _xml.RecepcionPago = this.Verificar(_xml.RecepcionPago);
                                    this.Save(_xml);
                                }
                            }
                        }
                    }
                } else if (singleModel.Version == "4.0") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    var d0 = DownloadFile(_xml.UrlFileXML);

                    if (_xml != null) {
                        var _cfdi = SAT.CFDI.V40.Comprobante.LoadBytes(d0);
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.RecepcionPago == null) {
                                _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                this.Save(_xml);
                            } else {
                                if (_xml.RecepcionPago != null && _xml.RecepcionPago.Count == 0) {
                                    _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                    this.Save(_xml);
                                } else {
                                    _xml.RecepcionPago = this.Verificar(_xml.RecepcionPago);
                                    this.Save(_xml);
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool Serializar1(int year, int month = 0) {
            var lista = this._ComprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(ComprobantesFiscalesService.Query().Recibido().WithYear(year).WithMonth(month).WithTipo(CFDITipoComprobanteEnum.Pagos).Build()).ToList();
            if (lista != null) {
                for (int i = 0; i < lista.Count(); i++) {
                    var comprobante = this.Serializar0(lista[i]);
                    if (comprobante != null) {
                        if (comprobante.RecepcionPago != null) {
                            this.Verificar(comprobante.RecepcionPago);
                        }
                    }
                }
            }
            return false;
        }

        public bool Serializar(int year, int month, CFDISubTipoEnum subtipo) {
            if (subtipo == CFDISubTipoEnum.Recibido) {
                var comprobantes = this._ComprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(ComprobantesFiscalesService.Query().Recibido().WithYear(year).WithMonth(month).WithTipo(CFDITipoComprobanteEnum.Pagos).Build()).ToList();
            }
            return false;
        }

        public bool Serializar(int year, int month = 0) {
            var comprobantes = this._ComprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(ComprobantesFiscalesService.Query().Recibido().WithYear(year).WithMonth(month).WithTipo(CFDITipoComprobanteEnum.Pagos).Build()).ToList();
            if (comprobantes != null) {
                var indices = comprobantes.Select(it => it.Id).ToArray();
                var complementos = this._ComplementoPagoRepository.GetList<ComplementoPagoDetailModel>(new Domain.Comprobante.Builder.ComplementoPagoQueryBuilder().WithIdComprobante(indices).Build()).ToList();
                for (int i = 0; i < comprobantes.Count(); i++) {
                    var comp = complementos.Where(it => it.IdComprobanteP == comprobantes[i].Id).ToList();
                    if (comp.Count() == 0) {
                        var comprobante = this.Serializar0(comprobantes[i]);
                        if (comprobante != null) {
                            var cPago = this.Verificar(comprobante.RecepcionPago);
                            for (int i1 = 0; i1 < cPago.Count; i1++) {
                                cPago[i1].AddIdComprobante(comprobantes[i].Id);
                            }
                            this._ComplementoPagoRepository.Save(new BindingList<ComplementoPagoDetailModel>(cPago));
                        }
                    }
                }
            }
            return false;
        }

        public static byte[] DownloadFile(string address) {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            byte[] downloadFile;
            try {
                downloadFile = (new WebClient()).DownloadData(address);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                downloadFile = null;
            }
            return downloadFile;
        }
    }
}
