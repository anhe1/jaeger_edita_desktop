﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.SAT.CFDI.Complemento.Pagos.V20;

namespace Jaeger.Aplication.Comprobante.Services {
    /// <summary>
    /// clase de extension para el complento de Pagos 2.0
    /// </summary>
    public class ComplementoPagosExtensions : ComplementoExtensions {
        // decimales utilizados en los totales del complemento
        private readonly int _Decimales = 2;
        #region complemento pagos v20
        public Pagos Create(BindingList<ComplementoPagoDetailModel> models) {
            if (models != null) {
                var _complemento = new List<PagosPago>();
                foreach (var item in models) {
                    var _pago = new PagosPago {
                        CtaBeneficiario = item.CtaBeneficiario,
                        CtaOrdenante = item.CtaOrdenante,
                        FechaPago = item.FechaPago,
                        FormaDePagoP = item.FormaDePagoP,
                        MonedaP = item.MonedaP,
                        Monto = this.Ajustar(item.Monto, this._Decimales),
                        NomBancoOrdExt = item.NomBancoOrdExt,
                        NumOperacion = item.NumOperacion,
                        RfcEmisorCtaBen = item.RfcEmisorCtaBen,
                        RfcEmisorCtaOrd = item.RfcEmisorCtaOrd,
                        TipoCadPago = item.TipoCadPago,
                        TipoCambioP = item.TipoCambioP
                    };

                    if (item.MonedaP == "MXN")
                        _pago.TipoCambioP = 1;

                    // tipo de cambio
                    _pago.TipoCambioPSpecified = _pago.TipoCambioP > 0;

                    // documentos relacionados
                    if (item.DoctoRelacionados != null) {
                        _pago.DoctoRelacionado = this.Pagos20(item.DoctoRelacionados);
                    }
                    // impuestos de los documentos relacionados
                    var _impuestosP = this.Pagos20(_pago.DoctoRelacionado);
                    if (_impuestosP != null) {
                        _pago.ImpuestosP = _impuestosP;
                    }
                    _complemento.Add(_pago);
                }
                var _pagos = new Pagos {
                    Pago = _complemento.ToArray()
                };
                _pagos.Totales = this.Totales(_pagos);

                return _pagos;
            }
            return null;
        }

        /// <summary>
        /// resumen de los totales para el complemento
        /// </summary>
        private SAT.CFDI.Complemento.Pagos.V20.PagosTotales Totales(Pagos pagos) {
            // 001 - ISR  002 - IVA 003 - IEPS
            if (pagos != null) {
                var _totales = new SAT.CFDI.Complemento.Pagos.V20.PagosTotales();
                foreach (var _pago in pagos.Pago) {
                    // total de importes pagados
                    _totales.MontoTotalPagos = +_pago.DoctoRelacionado.Sum(it => it.ImpPagado);
                    foreach (var _doctoRelacionado in _pago.DoctoRelacionado) {
                        if (_doctoRelacionado.ImpuestosDR != null) {
                            // impuestras traslados
                            if (_doctoRelacionado.ImpuestosDR.TrasladosDR != null) {
                                foreach (var trasladoDR in _doctoRelacionado.ImpuestosDR.TrasladosDR) {
                                    if (trasladoDR.ImpuestoDR == "002" && trasladoDR.TipoFactorDR == "Exento" && trasladoDR.TasaOCuotaDR == new decimal(0)) {
                                        _totales.TotalTrasladosBaseIVAExento = _totales.TotalTrasladosBaseIVAExento + trasladoDR.BaseDR;
                                    } else if (trasladoDR.ImpuestoDR == "002" && trasladoDR.TipoFactorDR == "Tasa" && trasladoDR.TasaOCuotaDR == new decimal(0)) {
                                        _totales.TotalTrasladosBaseIVA0 = _totales.TotalTrasladosBaseIVA0 + trasladoDR.BaseDR;
                                        _totales.TotalTrasladosImpuestoIVA0 = _totales.TotalTrasladosImpuestoIVA0 + trasladoDR.ImporteDR;
                                    } else if (trasladoDR.ImpuestoDR == "002" && trasladoDR.TipoFactorDR == "Tasa" && trasladoDR.TasaOCuotaDR == new decimal(0.8)) {
                                        _totales.TotalTrasladosBaseIVA8 = _totales.TotalTrasladosBaseIVA8 + trasladoDR.BaseDR;
                                        _totales.TotalTrasladosImpuestoIVA8 = _totales.TotalTrasladosImpuestoIVA8 + trasladoDR.ImporteDR;
                                    } else if (trasladoDR.ImpuestoDR == "002" && trasladoDR.TipoFactorDR == "Tasa" && trasladoDR.TasaOCuotaDR == new decimal(0.16)) {
                                        _totales.TotalTrasladosBaseIVA16 = _totales.TotalTrasladosBaseIVA16 + trasladoDR.BaseDR;
                                        _totales.TotalTrasladosImpuestoIVA16 = _totales.TotalTrasladosImpuestoIVA16 + trasladoDR.ImporteDR;
                                    }
                                }
                            }

                            // impuestos retenciones
                            if (_doctoRelacionado.ImpuestosDR.RetencionesDR != null) {
                                foreach (var retencionDR in _doctoRelacionado.ImpuestosDR.RetencionesDR) {
                                    if (retencionDR.ImpuestoDR == "001") {
                                        _totales.TotalRetencionesISR = _totales.TotalRetencionesISR + retencionDR.ImporteDR;
                                    } else if (retencionDR.ImpuestoDR == "002") {
                                        _totales.TotalRetencionesIVA = _totales.TotalRetencionesIVA + retencionDR.ImporteDR;
                                    } else if (retencionDR.ImpuestoDR == "003") {
                                        _totales.TotalRetencionesIEPS = _totales.TotalRetencionesIEPS + retencionDR.ImporteDR;
                                    }
                                }
                            }
                        }
                    }

                    _totales.TotalTrasladosBaseIVAExentoSpecified = _totales.TotalTrasladosBaseIVAExento > 0;
                    if (_totales.TotalTrasladosBaseIVAExentoSpecified) {
                        _totales.TotalTrasladosBaseIVAExento = this.Ajustar(_totales.TotalTrasladosBaseIVAExento, 2);
                    }

                    _totales.TotalTrasladosBaseIVA0Specified = _totales.TotalTrasladosBaseIVA0 > 0;
                    if (_totales.TotalTrasladosBaseIVA0Specified) {
                        _totales.TotalTrasladosBaseIVA0 = this.Ajustar(_totales.TotalTrasladosBaseIVA0, 2);
                    }

                    _totales.TotalTrasladosBaseIVA8Specified = _totales.TotalTrasladosBaseIVA8 > 0;
                    if (_totales.TotalTrasladosBaseIVA8Specified) {
                        _totales.TotalTrasladosBaseIVA8 = this.Ajustar(_totales.TotalTrasladosBaseIVA8, 2);
                    }

                    _totales.TotalTrasladosBaseIVA16Specified = _totales.TotalTrasladosBaseIVA16 > 0;
                    if (_totales.TotalTrasladosBaseIVA16Specified) {
                        _totales.TotalTrasladosBaseIVA16 = this.Ajustar(_totales.TotalTrasladosBaseIVA16, 2);
                    }

                    _totales.TotalTrasladosImpuestoIVA16Specified = _totales.TotalTrasladosImpuestoIVA16 > 0;
                    if (_totales.TotalTrasladosImpuestoIVA16Specified) {
                        _totales.TotalTrasladosImpuestoIVA16 = this.Ajustar(_totales.TotalTrasladosImpuestoIVA16, 2);
                    }

                    _totales.TotalRetencionesIEPSSpecified = _totales.TotalRetencionesIEPS > 0;
                    if (_totales.TotalRetencionesIEPSSpecified) {
                        _totales.TotalRetencionesIEPS = this.Ajustar(_totales.TotalRetencionesIEPS, 2);
                    }

                    _totales.TotalRetencionesISRSpecified = _totales.TotalRetencionesISR > 0;
                    if (_totales.TotalRetencionesISRSpecified) {
                        _totales.TotalRetencionesISR = this.Ajustar(_totales.TotalRetencionesISR, 2);
                    }

                    _totales.TotalRetencionesIVASpecified = _totales.TotalRetencionesIVA > 0;
                    if (_totales.TotalRetencionesIVASpecified) {
                        _totales.TotalRetencionesIVA = this.Ajustar(_totales.TotalRetencionesIVA, 2);
                    }
                }
                return _totales;
            }
            return null;
        }

        private PagosPagoImpuestosP Pagos20(PagosPagoDoctoRelacionado[] doctoRelacionado) {
            var _ImpuestosP = new PagosPagoImpuestosP();
            if (doctoRelacionado != null) {
                var lista1 = new List<PagosPagoImpuestosPTrasladoP>();
                var lista = new List<PagosPagoImpuestosPRetencionP>();
                foreach (var docto in doctoRelacionado) {
                    if (docto.ImpuestosDR != null) {
                        if (docto.ImpuestosDR.TrasladosDR != null) {
                            foreach (var item in docto.ImpuestosDR.TrasladosDR) {
                                lista1.Add(new PagosPagoImpuestosPTrasladoP {
                                    BaseP = this.Ajustar(item.BaseDR, this._Decimales),
                                    ImporteP = this.Ajustar(item.ImporteDR, this._Decimales),
                                    ImpuestoP = item.ImpuestoDR,
                                    TasaOCuotaP = this.Ajustar(item.TasaOCuotaDR, 6),
                                    TipoFactorP = item.TipoFactorDR,
                                    ImportePSpecified = item.ImporteDR > 0,
                                    TasaOCuotaPSpecified = item.TasaOCuotaDR > 0
                                });
                            }
                            //_ImpuestosP.TrasladosP = lista1.ToArray();
                        } else if (docto.ImpuestosDR.RetencionesDR != null) {
                            foreach (var item in docto.ImpuestosDR.RetencionesDR) {
                                lista.Add(new PagosPagoImpuestosPRetencionP {
                                    ImporteP = this.Ajustar(item.ImporteDR, this._Decimales),
                                    ImpuestoP = item.ImpuestoDR
                                });
                            }
                            //_ImpuestosP.RetencionesP = lista.ToArray();
                        }
                    }
                }
                if (lista1.Count > 0) {
                    _ImpuestosP.TrasladosP = lista1
                        .GroupBy(it => new { it.ImpuestoP, it.TasaOCuotaP, it.TipoFactorP })
                        .Select(g => new PagosPagoImpuestosPTrasladoP {
                            ImpuestoP = g.Key.ImpuestoP,
                            TasaOCuotaP = g.First().TasaOCuotaP,
                            TipoFactorP = g.Key.TipoFactorP,
                            BaseP = g.Sum(x => x.BaseP),
                            TasaOCuotaPSpecified = true,
                            ImporteP = g.Sum(x => x.ImporteP),
                            ImportePSpecified = true
                        })
                        .ToArray();
                }

                if (lista.Count > 0) {
                    _ImpuestosP.RetencionesP = lista
                        .GroupBy(it => new { it.ImpuestoP })
                        .Select(g => new PagosPagoImpuestosPRetencionP {
                            ImpuestoP = g.First().ImpuestoP,
                            ImporteP = g.Sum(x => x.ImporteP)
                        }).ToArray();
                }

                if (_ImpuestosP.RetencionesP == null && _ImpuestosP.TrasladosP == null) {
                    return null;
                }

                return _ImpuestosP;
            }
            return null;
        }

        private PagosPagoDoctoRelacionado[] Pagos20(BindingList<PagosPagoDoctoRelacionadoDetailModel> doctoRelacionados) {
            var _relacionados = new List<PagosPagoDoctoRelacionado>();
            foreach (var item in doctoRelacionados) {
                var _docto = new PagosPagoDoctoRelacionado {
                    EquivalenciaDR = Math.Round(item.EquivalenciaDR, 0),
                    Folio = item.Folio,
                    IdDocumento = item.IdDocumento,
                    ImpPagado = Math.Round(item.ImpPagado, 2),
                    ImpSaldoAnt = Math.Round(item.ImpSaldoAnt, 2),
                    ImpSaldoInsoluto = Math.Round(item.SaldoInsoluto, 2),
                    MonedaDR = item.MonedaDR,
                    NumParcialidad = item.NumParcialidad.ToString(),
                    ObjetoImpDR = item.ObjetoImpDR,
                    Serie = item.Serie
                };
                // equivalencia o tipo de cambio
                _docto.EquivalenciaDRSpecified = _docto.EquivalenciaDR > 0;

                // impuestos
                var _ImpuestosDR = this.Pagos20(item);
                if (_ImpuestosDR != null) {
                    _docto.ImpuestosDR = _ImpuestosDR;
                }

                _relacionados.Add(_docto);
            }
            return _relacionados.ToArray();
        }

        /// <summary>
        /// Nodo impuestos
        /// </summary>
        private PagosPagoDoctoRelacionadoImpuestosDR Pagos20(PagosPagoDoctoRelacionadoDetailModel docto) {
            var _impuestos = new PagosPagoDoctoRelacionadoImpuestosDR();
            if (docto.Traslados != null) {
                var _traslados = new List<SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR>();
                foreach (var _impuesto in docto.Traslados) {
                    var _traslado = new SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR {
                        BaseDR = this.Ajustar(_impuesto.Base, this._Decimales),
                        ImporteDR = _impuesto.Importe,
                        TasaOCuotaDR = this.Ajustar(_impuesto.TasaOCuota, 6),
                        TipoFactorDR = _impuesto.TipoFactor.ToString()
                    };

                    if (_impuesto.Impuesto == Domain.Base.ValueObjects.ImpuestoEnum.ISR)
                        _traslado.ImpuestoDR = "001";
                    else if (_impuesto.Impuesto == Domain.Base.ValueObjects.ImpuestoEnum.IVA)
                        _traslado.ImpuestoDR = "002";
                    else if (_impuesto.Impuesto == Domain.Base.ValueObjects.ImpuestoEnum.IEPS)
                        _traslado.ImpuestoDR = "003";
                    _traslado.ImporteDRSpecified = _traslado.ImporteDR > 0;
                    _traslado.TasaOCuotaDRSpecified = true;
                    _traslados.Add(_traslado);
                }
                if (_traslados.Count > 0)
                    _impuestos.TrasladosDR = _traslados.ToArray();
            }

            if (docto.Retenciones != null) {
                var _retenciones = new List<SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionadoImpuestosDRRetencionDR>();
                foreach (var _impuesto in docto.Retenciones) {
                    var _retencion = new SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionadoImpuestosDRRetencionDR {
                        BaseDR = this.Ajustar(_impuesto.Base, this._Decimales),
                        ImporteDR = _impuesto.Importe,
                        TasaOCuotaDR = this.Ajustar(_impuesto.TasaOCuota, 6),
                        TipoFactorDR = _impuesto.TipoFactor.ToString()
                    };

                    if (_impuesto.Impuesto == Domain.Base.ValueObjects.ImpuestoEnum.ISR)
                        _retencion.ImpuestoDR = "001";
                    else if (_impuesto.Impuesto == Domain.Base.ValueObjects.ImpuestoEnum.IVA)
                        _retencion.ImpuestoDR = "002";
                    else if (_impuesto.Impuesto == Domain.Base.ValueObjects.ImpuestoEnum.IEPS)
                        _retencion.ImpuestoDR = "003";

                    _retenciones.Add(_retencion);
                }
                if (_retenciones.Count > 0)
                    _impuestos.RetencionesDR = _retenciones.ToArray();
            }

            if (_impuestos.RetencionesDR == null && _impuestos.TrasladosDR == null) {
                return null;
            }

            return _impuestos;
        }
        #endregion
    }
}
