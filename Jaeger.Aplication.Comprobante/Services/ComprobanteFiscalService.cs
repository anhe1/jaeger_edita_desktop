﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobante.Builder;
using Jaeger.Aplication.Comprobante.Mappers;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Builder;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteFiscalService : CertificacionService, IComprobanteFiscalService {
        #region declaraciones
        protected ISqlComprobanteFiscalRepository _ComprobanteFiscalRepository;
        protected ISqlComplementoPagoRepository _ComplementoPagoRepository;
        protected ISqlComplementoCartaPorteRepository _ComplementoCartaPorteRepository;
        protected ISqlSerieRepository _SerieRepository;
        protected internal ISqlComprobanteFiscalConceptoRepository _ConceptoRepository;
        protected IEditaBucketService s3;
        protected internal ISqlComplementoNominaRepository nominaRepository;
        protected internal ISqlComplmentoNominaParteRepository parteRepository;
        #endregion

        public ComprobanteFiscalService() : base() {
            this._ComprobanteFiscalRepository = new SqlSugarComprobanteFiscalRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._ComplementoPagoRepository = new SqlSugarComplementoPagoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._ComplementoCartaPorteRepository = new SqlSugarComplementoCartaPorteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._ConceptoRepository = new SqlSugarComprobanteFiscalConceptoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._SerieRepository = new SqlSugarSerieRepository(ConfigService.Synapsis.RDS.Edita);
            this.nominaRepository = new SqlSugarComplementoNominaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.parteRepository = new SqlSugarComplementoNominaParteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.s3 = new EditaBucketService();
        }

        /// <summary>
        /// listado de series disponibles para comprobantes
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public BindingList<SerieFolioModel> GetSeries(bool onlyActive) {
            return new BindingList<SerieFolioModel>(this._SerieRepository.GetList().ToList());
        }

        /// <summary>
        /// obtener comprobante fiscal detalle
        /// </summary>
        /// <param name="index">indice</param>
        public ComprobanteFiscalDetailModel GetComprobante(int index) {
            var _response = this._ComprobanteFiscalRepository.GetComprobante(index);
            if (_response != null) {
                if (_response.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                    _response.RecepcionPago = new BindingList<ComplementoPagoDetailModel>(this._ComplementoPagoRepository.GetList<ComplementoPagoDetailModel>(new List<IConditional> { new Conditional("_cmppg_cfd_id", index.ToString()) }).ToList());
                    if (_response.RecepcionPago.Count == 0 && _response.IsEditable) {
                        _response.RecepcionPago = new BindingList<ComplementoPagoDetailModel>() { new ComplementoPagoDetailModel { FechaPagoP = DateTime.Now } };
                    }
                } else if (_response.TipoComprobante == CFDITipoComprobanteEnum.Ingreso | _response.TipoComprobante == CFDITipoComprobanteEnum.Traslado) {
                    if (_response.Documento != null) {
                        if (_response.Documento == "porte") {
                            _response.CartaPorte = this._ComplementoCartaPorteRepository.GetById(index);

                            if (_response.CartaPorte == null && _response.IsEditable) {
                                _response.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
                            }
                        }
                    }
                }
            }
            return _response;
        }

        // este metodo ya existe en otro servicio (ComprobanteFiscalSearch),|
        public List<ComprobanteFiscalDetailSingleModel> GetComprobante1(string[] uuid) {
            var response = this._ComprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(new List<IConditional>() {
                new Conditional("_cfdi_uuid", string.Join(",",uuid), ConditionalTypeEnum.In)}
            ).ToList();
            return response;
        }

        /// <summary>
        /// almacenar modelo comprobante
        /// </summary>
        public ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel comprobante) {
            comprobante = this._ComprobanteFiscalRepository.Save(comprobante);
            if (comprobante.Id > 0) {
                // en caso de existir complemento de pago se actualizan las referencias del comprobante
                if (comprobante.RecepcionPago != null) {
                    for (int i = 0; i < comprobante.RecepcionPago.Count; i++) {
                        comprobante.RecepcionPago[i].IdComprobanteP = comprobante.Id;
                    }
                    comprobante.RecepcionPago = this._ComplementoPagoRepository.Save(comprobante.RecepcionPago);
                }

                if (comprobante.CartaPorte != null) {
                    comprobante.CartaPorte.IdComprobante = comprobante.Id;
                    comprobante.CartaPorte = this._ComplementoCartaPorteRepository.Save(comprobante.CartaPorte);
                }

                if (comprobante.Nomina12 != null) {
                    comprobante.Nomina12.SubId = comprobante.Id;
                    comprobante.Nomina12.IdDocumento = comprobante.IdDocumento;
                    comprobante.Nomina12 = this.nominaRepository.Save(comprobante.Nomina12) as ComplementoNominaDetailModel;
                }
            }
            return comprobante;
        }

        public IComprobanteFiscalSingleModel Cancelar(IComprobanteFiscalSingleModel model, string claveCancelacion) {
            var _response = this.Cancelar(model.IdDocumento, claveCancelacion);
            if (_response != null) {

            }
            return model;
        }

        public ComprobanteFiscalDetailModel Procesar(ComprobanteFiscalDetailModel model, ref int codigo, ref string mensaje) {
            // se deben agregar los datos referentes al emisor del comprobante, esto debido a que son temporales
            // igualamos el valor del emisor debido a que es una clase temporal dentro del modelo
            var _emisor = model.Emisor;
            _emisor.RegimenFiscal = ConfigService.Synapsis.Empresa.RegimenFiscal;
            _emisor.Nombre = ConfigService.Synapsis.Empresa.RazonSocial;
            _emisor.RFC = ConfigService.Synapsis.Empresa.RFC;
            model.LugarExpedicion = ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal;
            model.Emisor = _emisor;
            var cfdi = this.Timbrar(model);
            if (cfdi != null) {
                if (this.Codigo == 0) {
                    model.TimbreFiscal = ComplementoTimbreFiscalExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
                    model.NoCertificado = cfdi.NoCertificado;
                    model.Version = cfdi.Version;
                    model.XML = cfdi.Serialize();
                    // almacenar la respuesta
                    cfdi.Save(ManagerPathService.JaegerPath(PathsEnum.Comprobantes, model.KeyName() + ".xml"));
                    model.Status = "PorCobrar";
                    model.Estado = "Vigente";
                    model = this.Save(model);
                    model.UrlFileXML = this.s3.Upload(ManagerPathService.JaegerPath(PathsEnum.Comprobantes, model.KeyName() + ".xml"), string.Concat(model.KeyName(), ".xml"));
                    this._ComprobanteFiscalRepository.UpdateUrlXml(model.Id, model.UrlFileXML);

                    // creamos la representacion impresa, si es un exito actualizamos
                    if (System.IO.File.Exists(this.pdf.Procesar(cfdi, model.KeyName()))) {
                        model.UrlFilePDF = s3.Upload(this.pdf.PDF, string.Concat(model.KeyName(), ".pdf"));
                        if (this._ComprobanteFiscalRepository.UpdateUrlPdf(model.Id, model.UrlFilePDF) == false)
                            Console.WriteLine("No se actualiza URL del PDF");
                    }
                } else {
                    codigo = this.Codigo;
                    mensaje = this.Mensaje;
                }
            }
            codigo = this.Codigo;
            mensaje = this.Mensaje;
            return model;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            if (typeof(T1) == typeof(ComprobanteFiscalDetailModel) | typeof(T1) == typeof(ComprobanteFiscalDetailSingleModel)) {
                var d0 = this._ComprobanteFiscalRepository.GetList<T1>(conditionales).ToList();
                if (typeof(T1) == typeof(ComprobanteFiscalDetailModel)) {
                    for (int i = 0; i < d0.Count(); i++) {
                        if ((d0[i] as ComprobanteFiscalDetailModel).TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                            (d0[i] as ComprobanteFiscalDetailModel).RecepcionPago = new BindingList<ComplementoPagoDetailModel>(
                                this._ComplementoPagoRepository.GetList<ComplementoPagoDetailModel>(new List<IConditional> { new Conditional("_cmppg_cfd_id", (d0[i] as ComprobanteFiscalDetailModel).Id.ToString()) }).ToList());
                        }
                    }
                }
                return d0;
            } else if (typeof(T1) == typeof(ComprobanteFiscalConceptoView)) {
                return this._ComprobanteFiscalRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComprobanteConceptoDetailModel) | typeof(T1) == typeof(ComprobanteConceptoModel)) {
                return this._ConceptoRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComprobanteFiscalConceptoView)) {
                return this._ComprobanteFiscalRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComplementoPagoDetailModel)) {
                // complemento de recepcion de pagos
                return this._ComplementoPagoRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComplementoPagoMergeDoctoModel)) {
                // vista de complemento de pago y sus partidas
                return this._ComplementoPagoRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComplementoVsPagoMergeDoctoModel)) {
                return this._ComplementoPagoRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComprobanteReciboEletronicoPagoSingle)) {
                return this._ComplementoPagoRepository.GetList<T1>(conditionales);
            } else if (typeof(T1) == typeof(ComprobanteCfdiRelacionadosModel)) {

            } else if (typeof(T1) == typeof(EstadoCuenta)) {
                return this._ComprobanteFiscalRepository.GetList<T1>(conditionales);
            }
            return new List<T1>();
        }

        /// <summary>
        /// obtener el estado del comprobante fiscal mediante el servicio SAT
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="emisor">RFC del emisor del comprobante</param>
        /// <param name="receptor">RFC del receptor del comprobante</param>
        /// <param name="total">total del comprobante</param>
        /// <param name="idDocumento">folio fiscal del comprobante (uuid)</param>
        /// <returns>string que representa el estado del comprobante</returns>
        public string EstadoSAT(int indice, string emisor, string receptor, decimal total, string idDocumento) {
            var service = new SAT.CFDI.Consulta.Services.Status();
            var request = SAT.CFDI.Consulta.Request.Create().WithEmisorRFC(emisor).WithReceptorRFC(receptor).WithTotal(total).WithFolioFiscal(idDocumento).Build();
            var response = service.Execute(request);

            if (response != null) {
                this._ComprobanteFiscalRepository.UpdateEstado(idDocumento, response.Estado, ConfigService.Piloto.Clave);
                if (response.Estado.ToLower().Equals("cancelado")) {
                    this.UpdateStatus(indice, response.Estado);
                }
                return response.Estado;
            }

            return "No Disponible!";
        }

        /// <summary>
        /// actualizar status
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="status">nuevo status</param>
        /// <returns>verdadero si la operacion fue realizada</returns>
        public bool UpdateStatus(int indice, string status) {
            return this._ComprobanteFiscalRepository.Update(indice, status, ConfigService.Piloto.Clave);
        }

        public string Upload(string base64, string localFileName, string keyName) {
            return s3.Upload(base64, localFileName, keyName);
        }

        public bool Exits(string url) {
            return this.s3.Exists(url);
        }

        public bool UpdateUrlXml(int index, string url) {
            return this._ComprobanteFiscalRepository.UpdateUrlXml(index, url);
        }

        public bool UpdateUrlPdf(int index, string url) {
            return this._ComprobanteFiscalRepository.UpdateUrlPdf(index, url);
        }

        public bool UpdateUrlXmlAcuse(int index, string url) {
            return this._ComprobanteFiscalRepository.UpdateUrlXmlAcuse(index, url);
        }

        public bool UpdateUrlPdfAcuse(int index, string url) {
            return this._ComprobanteFiscalRepository.UpdateUrlPdfAcuse(index, url);
        }

        #region metodos estaticos
        /// <summary>
        /// obtener listado de status por el sub tipo de comprobante
        /// </summary>
        public static BindingList<CFDIStatusModel> GetStatus(CFDISubTipoEnum subTipo) {
            var response = new BindingList<CFDIStatusModel>();
            if (subTipo == CFDISubTipoEnum.Emitido) {
                response = new BindingList<CFDIStatusModel>(((CFDIStatusEmitidoEnum[])Enum.GetValues(typeof(CFDIStatusEmitidoEnum))).Where(c => c != CFDIStatusEmitidoEnum.Importado).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
            } else if (subTipo == CFDISubTipoEnum.Recibido) {
                response = new BindingList<CFDIStatusModel>(((CFDIStatusRecibidoEnum[])Enum.GetValues(typeof(CFDIStatusRecibidoEnum))).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
            }
            response.RemoveAt(0);
            return response;
        }

        /// <summary>
        /// Catalogo de tipos de relacion entre CFDI.
        /// </summary>
        public static List<IComprobanteTipoRelacionCFDI> GetCFDIRelaciones() {
            var d0 = new List<IComprobanteTipoRelacionCFDI> {
                new ComprobanteTipoRelacionCFDI("01", "Nota de crédito de los documentos relacionados"),
                new ComprobanteTipoRelacionCFDI("02", "Nota de débito de los documentos relacionados"),
                new ComprobanteTipoRelacionCFDI("03", "Devolución de mercancía sobre facturas o traslados previos"),
                new ComprobanteTipoRelacionCFDI("04", "Sustitución de los CFDI previos"),
                new ComprobanteTipoRelacionCFDI("05", "Traslados de mercancías facturados previamente"),
                new ComprobanteTipoRelacionCFDI("06", "Factura generada por los traslados previos"),
                new ComprobanteTipoRelacionCFDI("07", "CFDI por aplicación de anticipo")
            };
            return d0;
        }

        /// <summary>
        /// creacion de objteto
        /// </summary>
        public static IComprobanteFiscalBuilder Builder() {
            return new ComprobanteFiscalBuilder();
        }

        public static IConceptoQueryBuilder ConceptoQuery() {
            return new ConceptoQueryBuilder();
        }
        #endregion
    }
}
