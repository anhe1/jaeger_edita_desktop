﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante.Mappers;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteFiscalPrinterService : IComprobanteFiscalPrinterService {
        //private ComprobanteExtensions extensions = new ComprobanteExtensions();
        private Dictionary<string, string> RegimenesFiscales;
        private Dictionary<string, string> Monedas;
        private Dictionary<string, string> UsosCFDI;
        private Dictionary<string, string> MetodosPago;
        private Dictionary<string, string> FormasPago;
        private Dictionary<string, string> Exportacion;
        private Dictionary<string, string> MesesCatalogo;
        private Dictionary<string, string> PeriodicidadCatalogo;
        private Dictionary<string, string> ConfigAutotransporteCatalogo;
        private Dictionary<string, string> figuraTransporteCatalogo;

        public ComprobanteFiscalPrinterService() {
            this.RegimenesFiscales = new Dictionary<string, string>();
            this.RegimenesFiscales = Html.Services.CommonService.RegimenesFiscales();

            this.Monedas = Html.Services.CommonService.Moneda();

            this.Exportacion = new Dictionary<string, string>();
            this.Exportacion = ComprobanteCommonService.GetCatalogoExportacion().ToDictionary(keySelector: it => it.Id, elementSelector: it => it.Descriptor);

            this.MetodosPago = new Dictionary<string, string> {
                { "PUE", "PUE: Pago en una sola exhibición" },
                { "PPD", "PPD: Pago en parcialidades o diferido" }
            };
            this.UsosCFDI = Html.Services.CommonService.UsoCFDI();
            this.FormasPago = Html.Services.CommonService.FormaPago();

            this.MesesCatalogo = Html.Services.CommonService.MesesCatalogo();
            this.PeriodicidadCatalogo = Html.Services.CommonService.PeriodicidadCatalogo();
            this.ConfigAutotransporteCatalogo = Html.Services.CommonService.ConfigAutotransporteCatalogo();
            this.figuraTransporteCatalogo = Html.Services.CommonService.FiguraTransporte();
        }

        public ComprobanteFiscalPrinter Printer(ComprobanteFiscalDetailModel comprobante) {
            var _response = new ComprobanteFiscalPrinter {
                EmisorRFC = comprobante.Emisor.RFC,
                EmisorNombre = comprobante.Emisor.Nombre,
                EmisorRegimenFiscal = comprobante.Emisor.RegimenFiscal,
                ReceptorNombre = comprobante.Receptor.Nombre,
                ReceptorRFC = comprobante.Receptor.RFC,
                ReceptorDomicilioFiscal = comprobante.DomicilioFiscal,
                NoCertificado = comprobante.NoCertificado,
                CondicionPago = comprobante.CondicionPago,
                Version = comprobante.Version,
                CadenaOriginalOff = "",
                Total = comprobante.Total,
                SubTotal = comprobante.SubTotal,
                ClaveExportacion = "",
                TipoComprobanteText = comprobante.TipoComprobanteText,
                LugarExpedicion = comprobante.LugarExpedicion,
                IdSubTipo = comprobante.IdSubTipo
            };
            // en caso de ser el emisor del comprobante
            if (Base.ConfigService.Synapsis.Empresa.RFC == _response.EmisorRFC) {
                _response.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido;
                if (Base.ConfigService.Synapsis.Empresa.RegimenFiscal != null) {
                    _response.EmisorRegimenFiscal = this.GetRegimenFiscal(Base.ConfigService.Synapsis.Empresa.RegimenFiscal);
                } else {
                    _response.EmisorRegimenFiscal = string.Empty;
                }
            } else {
                _response.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido;
                if (!string.IsNullOrEmpty(_response.EmisorRegimenFiscal)) {
                    _response.EmisorRegimenFiscal = this.GetRegimenFiscal(_response.EmisorRegimenFiscal);
                } else {
                    _response.EmisorRegimenFiscal = string.Empty;
                }
            }

            _response.Conceptos = comprobante.Conceptos;

            _response.ClaveExportacion = string.Empty;
            if (comprobante.ClaveExportacion != null) {
                _response.ClaveExportacion = comprobante.ClaveExportacion;
            }

            if (comprobante.Version != "4.0") {
                _response.ClaveExportacion = string.Empty;
            } else {

            }
            // receptor Regimen Fiscal
            _response.ClaveRegimenFiscal = "";
            if (comprobante.ClaveRegimenFiscal != null) {
                if (comprobante.ClaveRegimenFiscal != "") {
                    _response.ClaveRegimenFiscal = !this.RegimenesFiscales.ContainsKey(comprobante.ClaveRegimenFiscal) ? comprobante.ClaveRegimenFiscal : this.RegimenesFiscales[comprobante.ClaveRegimenFiscal];
                }
            }

            _response.ClaveUsoCFDI = "";
            if (comprobante.Receptor.ClaveUsoCFDI != null) {
                if (comprobante.Receptor.ClaveUsoCFDI != "") {
                    _response.ClaveUsoCFDI = !this.UsosCFDI.ContainsKey(comprobante.Receptor.ClaveUsoCFDI) ? comprobante.Receptor.ClaveUsoCFDI : this.UsosCFDI[comprobante.Receptor.ClaveUsoCFDI];
                }
            }

            if (comprobante.Folio != null) {
                _response.Folio = comprobante.Folio;
            }

            if (comprobante.Serie != null) {
                _response.Serie = comprobante.Serie;
            }

            if (comprobante.ClaveMoneda != null) {
                if (comprobante.ClaveMoneda != "XXX") {
                    _response.ClaveMoneda = !this.Monedas.ContainsKey(comprobante.ClaveMoneda) ? comprobante.ClaveMoneda : this.Monedas[comprobante.ClaveMoneda];
                } else {
                    _response.ClaveMoneda = comprobante.ClaveMoneda;
                }
            }

            if (comprobante.InformacionGlobal != null) {
                _response.InformacionGlobal = comprobante.InformacionGlobal;
                _response.InformacionGlobal.ClaveMeses = !this.MesesCatalogo.ContainsKey(_response.InformacionGlobal.ClaveMeses) ? _response.InformacionGlobal.ClaveMeses : this.MesesCatalogo[_response.InformacionGlobal.ClaveMeses];
                _response.InformacionGlobal.Periodicidad = !this.PeriodicidadCatalogo.ContainsKey(_response.InformacionGlobal.Periodicidad) ? _response.InformacionGlobal.Periodicidad : this.PeriodicidadCatalogo[_response.InformacionGlobal.Periodicidad];
            }

            if (comprobante.ClaveMetodoPago != null) {
                _response.ClaveMetodoPago = !this.MetodosPago.ContainsKey(comprobante.ClaveMetodoPago) ? comprobante.ClaveMetodoPago : this.MetodosPago[comprobante.ClaveMetodoPago];
            } else {
                _response.ClaveMetodoPago = string.Empty;
            }

            if (comprobante.ClaveFormaPago != null) {
                _response.ClaveFormaPago = !this.FormasPago.ContainsKey(comprobante.ClaveFormaPago) ? comprobante.ClaveFormaPago : this.FormasPago[comprobante.ClaveFormaPago];
            } else {
                _response.ClaveFormaPago = string.Empty;
            }

            _response.TrasladoIVA = comprobante.TrasladoIVA;

            if (comprobante.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                if (comprobante.RecepcionPago != null) {
                    _response.RecepcionPago = comprobante.RecepcionPago;
                } else {

                }
            } else if (comprobante.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && comprobante.Documento == "porte") {
                if (comprobante.CartaPorte != null) {
                    _response.Documento = comprobante.Documento;
                    _response.CartaPorte = comprobante.CartaPorte;
                    //_response.CartaPorte.Mercancias.Autotransporte.PermSCT = this.ConfigAutotransporteCatalogo[_response.CartaPorte.Mercancias.Autotransporte.PermSCT];
                    if (_response.CartaPorte.FiguraTransporte != null) {
                        for (int i = 0; i < _response.CartaPorte.FiguraTransporte.Count; i++) {
                            _response.CartaPorte.FiguraTransporte[i].TipoFigura = !this.figuraTransporteCatalogo.ContainsKey(_response.CartaPorte.FiguraTransporte[i].TipoFigura) ? _response.CartaPorte.FiguraTransporte[i].TipoFigura : this.figuraTransporteCatalogo[_response.CartaPorte.FiguraTransporte[i].TipoFigura];
                        }
                    }
                } else {
                    comprobante.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
                }
            }

            _response.CantidadEnLetra = Html.Services.CommonService.TotalEnLetra(comprobante.Total);

            if (comprobante.RecepcionPago != null) {
                _response.CantidadEnLetra = Html.Services.CommonService.TotalEnLetra(_response.RecepcionPago.Select(it => it.Monto).First());
                _response.RecepcionPago[0].MonedaP = !this.Monedas.ContainsKey(_response.RecepcionPago[0].MonedaP) ? _response.RecepcionPago[0].MonedaP : this.Monedas[_response.RecepcionPago[0].MonedaP];
                _response.RecepcionPago[0].FormaDePagoP = !this.FormasPago.ContainsKey(_response.RecepcionPago[0].FormaDePagoP) ? _response.RecepcionPago[0].FormaDePagoP : this.FormasPago[_response.RecepcionPago[0].FormaDePagoP];
            }

            _response.TimbreFiscal = this.GetTimbreFiscalTest();

            _response.QR = Html.Services.CommonService.ImagenQR(_response.EmisorRFC, _response.ReceptorRFC, comprobante.Total, _response.TimbreFiscal.UUID, _response.TimbreFiscal.SelloSAT, "");

            return _response;
        }

        public ComprobanteFiscalPrinter Printer(SAT.CFDI.V33.Comprobante cfdi) {
            var _comprobante = ComprobanteExtensions.Create(cfdi);
            var _response = this.Printer(_comprobante);
            _response.QR = Html.Services.CommonService.ImagenQR(cfdi.Emisor.Rfc, cfdi.Receptor.Rfc, cfdi.Total, _response.TimbreFiscal.UUID, _response.TimbreFiscal.SelloSAT, "");
            _response.CadenaOriginalOff = cfdi.CadenaOriginalOff;

            _response.TimbreFiscal = ComplementoExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
            _response.TimbreFiscal.Tag = cfdi.Complemento.TimbreFiscalDigital.CadenaOriginal;
            return _response;
        }

        public ComprobanteFiscalPrinter Printer(SAT.CFDI.V40.Comprobante cfdi) {
            var _comprobante = ComprobanteExtensions.Create(cfdi);
            var _response = this.Printer(_comprobante);
            _response.QR = Html.Services.CommonService.ImagenQR(cfdi.Emisor.Rfc, cfdi.Receptor.Rfc, cfdi.Total, _response.TimbreFiscal.UUID, _response.TimbreFiscal.SelloSAT, "");
            _response.CadenaOriginalOff = cfdi.CadenaOriginalOff;

            _response.TimbreFiscal = ComplementoExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
            _response.TimbreFiscal.Tag = cfdi.Complemento.TimbreFiscalDigital.CadenaOriginal;
            return _response;
        }

        private string GetRegimenFiscal(string claveRegimen) {
            try {
                return this.RegimenesFiscales[claveRegimen];
            } catch (Exception ex) {
                Console.WriteLine(ex.StackTrace);
            }
            return string.Empty;
        }

        private Domain.Comprobante.Entities.Complemento.ComplementoTimbreFiscal GetTimbreFiscalTest() {
            var timbreFiscal = new Domain.Comprobante.Entities.Complemento.ComplementoTimbreFiscal() {
                UUID = "00000000-0000-0000-0000-000000000000",
                FechaTimbrado = DateTime.Now,
                NoCertificadoSAT = "0000000000000",
                RFCProvCertif = "XXXXXXXXXX",
                SelloCFD = "XXXXXXXXXXXXXXXXXXXXXXXXXX=",
                SelloSAT = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=",
                Tag = string.Empty
            };
            return timbreFiscal;
        }
    }
}
