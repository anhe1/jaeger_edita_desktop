﻿using System.Collections.Generic;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Certificado.Entities;
using Jaeger.Domain.Certificado.Contracts;
using Jaeger.DataAccess.Certificado.Repositories;

namespace Jaeger.Aplication.Comprobante.Services {
    public class CertificadoService : ICertificadoService {
        protected ISqlCertificadoRepository sqlCertificadoRepository;

        public CertificadoService() { this.OnLoad(); }

        protected virtual void OnLoad() {
            this.sqlCertificadoRepository = new SqlSugarCertificadoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public CertificadoModel Save(CertificadoModel model) {
            return this.sqlCertificadoRepository.Salveable(model);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.sqlCertificadoRepository.GetList<T1>(conditionals);
        }

        #region metodos estaticos
        public static Domain.Certificado.Builder.ICertificadoQueryBuilder Query() {
            return new Domain.Certificado.Builder.CertificadoQueryBuilder();
        }

        public static Response Verificar(string fileCer, string fileKey, string password) {
            var certificate = new Crypto.Services.Certificate();
            certificate.Cargar(fileCer);
            if (certificate.CodigoDeError == 0) {
                var privateKey = new Crypto.Services.PrivateKey();
                privateKey.Cargar(fileKey, password);
                if (privateKey.CodigoDeError == 0) {
                    certificate.CargarLlavePrivada(privateKey);
                    if (certificate.CodigoDeError != 0) {
                        return new Response(false, certificate.MensajeDeError, null);
                    } else {
                        var certificado = new CertificadoModel {
                            Activo = true,
                            Tipo = certificate.TipoCertificado,
                            Emisor = certificate.AutoridadEmisora,
                            RazonSocial = certificate.RazonSocial,
                            NotAfter = certificate.Inicio.Value,
                            NotBefore = certificate.Fin.Value,
                            RFC = certificate.RFC,
                            Serie = certificate.NoSerie,
                            KeyPassB64 = System.Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(password)),
                            FechaNuevo = System.DateTime.Now,
                            CerB64 = certificate.ExportarPemB64(),
                            KeyB64 = privateKey.ExportarPemB64(),
                            CerFile = System.IO.File.ReadAllBytes(fileCer),
                            KeyFile = System.IO.File.ReadAllBytes(fileKey),
                        };
                        return new Response(true, "Todos los datos del certificado actual son correctos.", certificado);
                    }
                } else {
                    return new Response(false, privateKey.MensajeDeError, null);
                }
            } else {
                return new Response(false, certificate.MensajeDeError, null);
            }
        }

        public static CertificadoModel Certificado(string fileName) {
            var informacion = new Crypto.Services.CertificateInfo(fileName);
            if (informacion != null) {
                if (informacion.Valid) {
                    var _certificado = new CertificadoModel {
                        NotAfter = informacion.Inicio.Value,
                        NotBefore = informacion.Fin.Value,
                        RFC = informacion.RFC,
                        Tipo = informacion.TipoCertificado,
                        Serie = informacion.NoSerie,
                        CerB64 = informacion.CerB64,
                        RazonSocial = informacion.UserName
                    };
                    return _certificado;
                }
            }
            return null;
        }

        public static string ComprobanteKey(string fileCer, string fileKey, string password) {
            var certificate = new Crypto.Services.Certificate();
            certificate.Cargar(fileCer);
            if (certificate.CodigoDeError == 0) {
                var privateKey = new Crypto.Services.PrivateKey();
                privateKey.Cargar(fileKey, password);
                if (privateKey.CodigoDeError == 0) {
                    certificate.CargarLlavePrivada(privateKey);
                    if (certificate.CodigoDeError != 0) {
                        return certificate.MensajeDeError;
                    } else {
                        return "Todos los datos del certificado actual son correctos.";
                    }
                } else {
                    return privateKey.MensajeDeError;
                }
            } else {
                return certificate.MensajeDeError;
            }
        }
        #endregion
    }
}
