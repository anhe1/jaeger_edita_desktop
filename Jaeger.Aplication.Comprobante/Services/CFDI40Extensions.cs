﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Aplication.Comprobante.Mappers;

namespace Jaeger.Aplication.Comprobante.Services {
    public class CFDI40Extensions : ComplementoExtensions {
        #region declaraciones
        private readonly ComplementoPagosExtensions complementoPagos;
        //private readonly ComplementoNominaExtensions complementoNomina;
        // decimales
        //private readonly int _MaxDecimales = 4;
        // decimales aplicables en el impuesto TasaOCuota
        //private readonly int _TasaOCuotaDecimales = 6;
        #endregion

        /// <summary>
        /// clase de extension para comprobante CFDI version 4.0
        /// </summary>
        public CFDI40Extensions() {
            this.complementoPagos = new ComplementoPagosExtensions();
            //this.complementoNomina = new ComplementoNominaExtensions();
        }

        #region version 4.0
        /// <summary>
        /// Crear comprobante fiscal
        /// </summary>
        public SAT.CFDI.V40.Comprobante Create(ComprobanteFiscalDetailModel model) {
            // datos generales del comprobante
            var _comprobante = new SAT.CFDI.V40.Comprobante {
                TipoDeComprobante = Enum.GetName(typeof(CFDITipoComprobanteEnum), model.TipoComprobante).ToString().Substring(0, 1),
                Fecha = Convert.ToDateTime(model.FechaEmision.ToString("yyyy-MM-dd\\THH:mm:ss")),
                Moneda = model.ClaveMoneda,
                TipoCambio = Convert.ToDecimal(model.TipoCambio),
                TipoCambioSpecified = Convert.ToDecimal(model.TipoCambio) > 0,
                MetodoPago = model.ClaveMetodoPago,
                MetodoPagoSpecified = true,
                FormaPago = model.ClaveFormaPago,
                FormaPagoSpecified = true,
                LugarExpedicion = model.LugarExpedicion,
                CondicionesDePago = null
            };

            // clave de moneda
            if (model.ClaveMoneda == "MXN" | model.ClaveMoneda == "XXX") {
                _comprobante.TipoCambioSpecified = false;
            }

            // condiciones de pago
            if (model.CondicionPago != null) {
                if (model.CondicionPago.Trim() != "")
                    _comprobante.CondicionesDePago = model.CondicionPago;
            }

            // clave del metodo de pago
            _comprobante.MetodoPago = model.ClaveMetodoPago;

            // comprobar folio del comprobante
            _comprobante.Folio = null;
            if (model.Folio != null) {
                if (model.Folio.Trim() != "")
                    _comprobante.Folio = model.Folio;
            }


            // comprobar serie del comprobante
            _comprobante.Serie = null;
            if (model.Serie != null) {
                if (model.Serie.Trim() != "")
                    _comprobante.Serie = model.Serie;
            }

            if (model.Exportacion == Domain.Base.ValueObjects.CFDIExportacionEnum.No_Aplica)
                _comprobante.Exportacion = "01";
            else if (model.Exportacion == Domain.Base.ValueObjects.CFDIExportacionEnum.Definitiva)
                _comprobante.Exportacion = "02";
            else if (model.Exportacion == Domain.Base.ValueObjects.CFDIExportacionEnum.Temporal)
                _comprobante.Exportacion = "03";
            else if (model.Exportacion == Domain.Base.ValueObjects.CFDIExportacionEnum.DefinitivaA1)
                _comprobante.Exportacion = "04";

            // comprobantes relacionados
            if (model.CfdiRelacionados != null) {
                if (model.CfdiRelacionados.CfdiRelacionado.Count > 0)
                    _comprobante.CfdiRelacionados = this.ComprobanteCfdiRelacionados(model.CfdiRelacionados);
            }

            // informacion en el caso de ser un comprobante general
            if (model.InformacionGlobal != null) {
                _comprobante.InformacionGlobal = this.InformacionGlobal(model.InformacionGlobal);
            }

            // crear emisor y receptor del comprobante
            _comprobante.Emisor = this.Emisor(model.Emisor);
            _comprobante.Receptor = this.Receptor(model.Receptor);

            // crear conceptos
            _comprobante.Conceptos = this.Conceptos(model.Conceptos);

            // impuestos del comprobante
            if (model.TipoComprobante != CFDITipoComprobanteEnum.Pagos && model.TipoComprobante != CFDITipoComprobanteEnum.Traslado)
                _comprobante.Impuestos = this.Impuestos(model);

            // totales del comprobante
            if (model.SubTotal <= 0)
                _comprobante.SubTotal = 0;
            else {
                //_comprobante.SubTotal = Math.Round(_comprobante.Conceptos.Sum(f => f.Importe), model.PrecisionDecimal);
                _comprobante.SubTotal = this.Ajustar(_comprobante.Conceptos.Sum(f => f.Importe), 2);
            }

            if (_comprobante.Conceptos != null) {
                if (_comprobante.Conceptos.Length > 0)
                    _comprobante.Descuento = Math.Round(_comprobante.Conceptos.Sum(p => p.Descuento), 2);
            }

            if (model.Total <= 0)
                _comprobante.Total = 0;
            else {
                //_comprobante.Total = Math.Round(((_comprobante.SubTotal - _comprobante.Descuento) + (_comprobante.Impuestos.TotalImpuestosTrasladados)) - (_comprobante.Impuestos.TotalImpuestosRetenidos), model.PrecisionDecimal);
                _comprobante.Total = this.Ajustar(((_comprobante.SubTotal - _comprobante.Descuento) + (_comprobante.Impuestos.TotalImpuestosTrasladados)) - (_comprobante.Impuestos.TotalImpuestosRetenidos), 2);
            }

            // cuando el tipo de comprobante es pagos
            if (model.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                _comprobante.DescuentoSpecified = false;
                _comprobante.TipoCambioSpecified = false;
                _comprobante.MetodoPagoSpecified = false;
                _comprobante.FormaPagoSpecified = false;
                _comprobante.CondicionesDePago = null;
                _comprobante.Conceptos[0].Cantidad = 1;
                _comprobante.Conceptos[0].ValorUnitario = 0;
                _comprobante.Conceptos[0].Importe = 0;
            }

            // cuando el tipo de comprobante es traslado
            if (model.TipoComprobante == CFDITipoComprobanteEnum.Traslado) {
                _comprobante.DescuentoSpecified = false;
                _comprobante.MetodoPagoSpecified = false;
                _comprobante.FormaPagoSpecified = false;
                _comprobante.CondicionesDePago = null;
            }

            // en caso de tener complemento de pagos y es tipo traslado
            if (model.TipoComprobante == CFDITipoComprobanteEnum.Traslado && model.CartaPorte != null) {
                _comprobante.DescuentoSpecified = false;
                _comprobante.MetodoPagoSpecified = false;
                _comprobante.FormaPagoSpecified = false;
                _comprobante.CondicionesDePago = null;
            }

            // complemento recepcion de pago
            if (model.RecepcionPago != null) {
                //var _complemento = new CFDI.V40.ComprobanteComplemento();
                var _pagos = this.complementoPagos.Create(model.RecepcionPago);
                if (_comprobante.Complemento == null) {
                    _comprobante.Complemento = new SAT.CFDI.V40.ComprobanteComplemento();
                }
                _comprobante.Complemento.Pagos = _pagos;
            }

            // complemento carta porte
            if (model.CartaPorte != null) {
                var _ext = new ComplementoCartaPorteExtensions();
                var _cartaPorte = _ext.Create(model.CartaPorte);
                if (_cartaPorte != null) {
                    if (_comprobante.Complemento == null) {
                        _comprobante.Complemento = new SAT.CFDI.V40.ComprobanteComplemento();
                    }
                    _comprobante.Complemento.CartaPorte = _cartaPorte;
                }
            }

            // complemento nomina
            if (model.Nomina != null) {
                //_comprobante.Complemento.Nomina = this.complementoNomina.Create(model.Nomina);
            }

            // informacion de un CFDI global
            // Se debe registrar la clave “I” de conformidad con el catálogo c_TipoDeComprobante.
            if (model.InformacionGlobal != null && model.TipoComprobante == CFDITipoComprobanteEnum.Ingreso) {
                //En este campo se debe de registrar el valor “PUBLICO EN GENERAL”.
                _comprobante.Receptor.Nombre = "PUBLICO EN GENERAL";
                _comprobante.Receptor.ResidenciaFiscal = null; //Este campo no debe existir.
                _comprobante.Receptor.ResidenciaFiscalSpecified = false;
                _comprobante.Receptor.RegimenFiscalReceptor = "616"; // En este campo se debe registrar la clave “616”.
                _comprobante.Receptor.NumRegIdTrib = null; // Este campo no debe existir.
                _comprobante.Receptor.UsoCFDI = "S01"; // Se debe registrar la clave “S01” (Sin efectos fiscales).
                _comprobante.Receptor.DomicilioFiscalReceptor = _comprobante.LugarExpedicion; // En este campo se debe registrar el mismo código postal señalado en el campo LugarExpedicion.
                _comprobante.CondicionesDePago = null; // Este campo no debe existir
                _comprobante.MetodoPago = "PUE"; // Se debe registrar siempre la clave “PUE” (Pago en una sola exhibición), de conformidad con el catálogo c_MetodoPago.
                _comprobante.MetodoPagoSpecified = true;
                _comprobante.Exportacion = "01"; //Se debe registrar la clave “ 01” (No aplica)
            } else if (model.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                _comprobante.Receptor.UsoCFDI = "CP01";
            }

            return _comprobante;
        }

        public SAT.CFDI.V40.ComprobanteEmisor Emisor(ComprobanteContribuyenteModel model) {
            var emisor = new SAT.CFDI.V40.ComprobanteEmisor {
                Nombre = model.Nombre,
                Rfc = model.RFC,
                RegimenFiscal = model.RegimenFiscal
                //TODO: falta implementar este atributo FacAtrAdquirente
            };
            return emisor;
        }

        public SAT.CFDI.V40.ComprobanteReceptor Receptor(ComprobanteContribuyenteModel model) {
            var receptor = new SAT.CFDI.V40.ComprobanteReceptor {
                Nombre = model.Nombre,
                Rfc = model.RFC,
                UsoCFDI = model.ClaveUsoCFDI,
                NumRegIdTrib = model.NumRegIdTrib,
                ResidenciaFiscal = model.ResidenciaFiscal,
                RegimenFiscalReceptor = model.RegimenFiscal,
                DomicilioFiscalReceptor = model.DomicilioFiscal
            };
            return receptor;
        }

        /// <summary>
        /// Impuestos
        /// </summary>
        /// <param name="model">objeto comprobante fiscal</param>
        private SAT.CFDI.V40.ComprobanteImpuestos Impuestos(ComprobanteFiscalDetailModel model) {

            if (model != null) {
                if (model.Conceptos != null) {
                    if (model.Conceptos.Count > 0) {
                        var _impuestos = new SAT.CFDI.V40.ComprobanteImpuestos();
                        var _retenciones = new List<SAT.CFDI.V40.ComprobanteImpuestosRetencion>();
                        var _traslados = new List<SAT.CFDI.V40.ComprobanteImpuestosTraslado>();
                        foreach (var _concepto in model.Conceptos) {
                            foreach (var _impuesto in _concepto.Impuestos) {
                                int numeral = (int)_impuesto.Impuesto;
                                if (_impuesto.Tipo == TipoImpuestoEnum.Traslado) {
                                    // this.FormatoNumero <- modificamos para obtener el formato de decimales
                                    var newTraslado = new SAT.CFDI.V40.ComprobanteImpuestosTraslado {
                                        Impuesto = string.Format("{0:000}", numeral),
                                        Importe = this.FixDecimal(_impuesto.Importe, 2, 2),
                                        //TasaOCuota = this.Ajustar(_impuesto.TasaOCuota, this._TasaOCuotaDecimales),
                                        TasaOCuota = this.Ajustar(_impuesto.TasaOCuota, 6),
                                        TipoFactor = Enum.GetName(typeof(FactorEnum), _impuesto.TipoFactor),
                                        Base = this.Ajustar(_impuesto.Base, 2),
                                        ImporteSpecified = _impuesto.Importe > 0,
                                        TasaOCuotaSpecified = _impuesto.TasaOCuota > 0
                                    };
                                    // buscar si existe
                                    var buscar = _traslados.Find(
                                        (SAT.CFDI.V40.ComprobanteImpuestosTraslado x)
                                               => x.Impuesto == newTraslado.Impuesto
                                                & x.TasaOCuota == newTraslado.TasaOCuota
                                                & x.TipoFactor == newTraslado.TipoFactor);

                                    if (buscar == null)
                                        _traslados.Add(newTraslado);
                                    else {
                                        newTraslado.Base = this.FixDecimal(buscar.Base + newTraslado.Base, 2, 2);
                                        newTraslado.Importe = this.Ajustar(buscar.Importe + newTraslado.Importe, 2);
                                        _traslados.Remove(buscar);
                                        _traslados.Add(newTraslado);
                                    }
                                } else if (_impuesto.Tipo == TipoImpuestoEnum.Retencion) {
                                    var newRetencion = new SAT.CFDI.V40.ComprobanteImpuestosRetencion {
                                        Impuesto = string.Format("{0:000}", numeral),
                                        Importe = this.FixDecimal(_impuesto.Importe, 2, 2)
                                    };
                                    var busca = new SAT.CFDI.V40.ComprobanteImpuestosRetencion();
                                    if (busca != null) {
                                        newRetencion.Importe = this.FixDecimal(decimal.Add(newRetencion.Importe, busca.Importe));
                                        _retenciones.Remove(busca);
                                    }
                                    _retenciones.Add(newRetencion);
                                }
                            }
                        }
                        if (_traslados.Count <= 0)
                            _impuestos.TotalImpuestosTrasladadosSpecified = false;
                        else {
                            _impuestos.TotalImpuestosTrasladadosSpecified = true;
                            _impuestos.Traslados = _traslados.ToArray();
                            _impuestos.TotalImpuestosTrasladados = this.Ajustar(_traslados.Sum<SAT.CFDI.V40.ComprobanteImpuestosTraslado>((SAT.CFDI.V40.ComprobanteImpuestosTraslado p) => p.Importe), 2);
                        }

                        if (_retenciones.Count <= 0)
                            _impuestos.TotalImpuestosRetenidosSpecified = false;
                        else {
                            _impuestos.TotalImpuestosRetenidosSpecified = true;
                            _impuestos.Retenciones = _retenciones.ToArray();
                            _impuestos.TotalImpuestosRetenidos = this.Ajustar(_retenciones.Sum<SAT.CFDI.V40.ComprobanteImpuestosRetencion>((SAT.CFDI.V40.ComprobanteImpuestosRetencion p) => p.Importe), 2);
                        }
                        return _impuestos;
                    }
                }
            }
            return null;
        }

        private SAT.CFDI.V40.ComprobanteCfdiRelacionados[] ComprobanteCfdiRelacionados(ComprobanteCfdiRelacionadosModel objeto) {
            var newItem = new SAT.CFDI.V40.ComprobanteCfdiRelacionados();
            var lista = new List<SAT.CFDI.V40.ComprobanteCfdiRelacionadosCfdiRelacionado>();

            newItem.TipoRelacion = objeto.TipoRelacion.Clave;
            foreach (var item in objeto.CfdiRelacionado) {
                lista.Add(new SAT.CFDI.V40.ComprobanteCfdiRelacionadosCfdiRelacionado() { UUID = item.IdDocumento });
            }
            newItem.CfdiRelacionado = lista.ToArray();
            return new SAT.CFDI.V40.ComprobanteCfdiRelacionados[] { newItem };
        }

        private SAT.CFDI.V40.ComprobanteConcepto[] Conceptos(BindingList<ComprobanteConceptoDetailModel> conceptos) {
            var _conceptos = new List<SAT.CFDI.V40.ComprobanteConcepto>();
            if (conceptos != null) {
                if (conceptos.Count > 0) {
                    foreach (var item in conceptos) {
                        if (item.Activo == true) {
                            _conceptos.Add(this.Concepto(item));
                        }
                    }
                    return _conceptos.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Nodo condicional para precisar la información relacionada con el comprobante global.
        /// </summary>
        /// <param name="informacionGlobal"></param>
        /// <returns></returns>
        private SAT.CFDI.V40.ComprobanteInformacionGlobal InformacionGlobal(ComprobanteInformacionGlobalDetailModel informacionGlobal) {
            if (informacionGlobal != null) {
                var _response = new SAT.CFDI.V40.ComprobanteInformacionGlobal {
                    Año = informacionGlobal.Anio,
                    Meses = informacionGlobal.ClaveMeses,
                    Periodicidad = informacionGlobal.Periodicidad
                };
                return _response;
            }
            return null;
        }

        private SAT.CFDI.V40.ComprobanteConcepto Concepto(ComprobanteConceptoDetailModel concepto) {
            var _concepto = new SAT.CFDI.V40.ComprobanteConcepto() {
                Cantidad = this.Ajustar(concepto.Cantidad),
                ClaveProdServ = concepto.ClaveProdServ,
                ClaveUnidad = concepto.ClaveUnidad,
                Descripcion = concepto.Descripcion,
                ValorUnitario = this.Ajustar(concepto.ValorUnitario, 2),
                Importe = this.Ajustar(concepto.Importe, 2),
                Descuento = this.Ajustar(concepto.Descuento, 2),
                ObjetoImp = concepto.ObjetoImp
            };

            /// si existe descuento
            _concepto.DescuentoSpecified = concepto.Descuento > 0;

            /// opcional unidad
            if (!(concepto.Unidad == null))
                if (concepto.Unidad.Trim() != "")
                    _concepto.Unidad = concepto.Unidad;

            /// opcional no de identificacion
            if (!(concepto.NoIdentificacion == null))
                if (concepto.NoIdentificacion == null)
                    if (concepto.NoIdentificacion.Trim() != "")
                        _concepto.NoIdentificacion = concepto.NoIdentificacion;

            if (!(concepto.CtaPredial == null)) {
                if (concepto.CtaPredial.Trim().Length > 0) {
                    var cuentaPredial = new SAT.CFDI.V40.ComprobanteConceptoCuentaPredial() {
                        Numero = concepto.CtaPredial
                    };
                    _concepto.CuentaPredial = new SAT.CFDI.V40.ComprobanteConceptoCuentaPredial[] { cuentaPredial };
                }
            }

            // informacion aduanera
            if (!(concepto.InformacionAduanera != null))
                _concepto.InformacionAduanera = this.ConceptoInformacionAduanera(concepto.InformacionAduanera);

            /// partes del concepto
            if (concepto.Parte != null) {
                if (concepto.Parte.Count > 0)
                    _concepto.Parte = this.ConceptoParte(concepto.Parte);
            }

            /// impuestos del concepto
            if (concepto.Impuestos.Count > 0)
                _concepto.Impuestos = this.ConceptoImpuestos(concepto.Impuestos);

            // complemento a cuenta de teceros
            if (concepto.ACuentaTerceros != null) {
                _concepto.ACuentaTerceros = this.ACuentaTerceros(concepto.ACuentaTerceros);
            }
            //TODO: falta este objeto ComplementoConcepto
            _concepto.ComplementoConcepto = null;

            return _concepto;
        }

        private SAT.CFDI.V40.ComprobanteConceptoACuentaTerceros ACuentaTerceros(ComprobanteConceptoACuentaTerceros aCuentaTerceros) {
            var response = new SAT.CFDI.V40.ComprobanteConceptoACuentaTerceros() {
                DomicilioFiscalACuentaTerceros = aCuentaTerceros.DomicilioFiscalACuentaTerceros,
                NombreACuentaTerceros = aCuentaTerceros.NombreACuentaTerceros,
                RegimenFiscalACuentaTerceros = aCuentaTerceros.RegimenFiscalACuentaTerceros,
                RfcACuentaTerceros = aCuentaTerceros.RfcACuentaTerceros
            };

            return response;
        }

        private SAT.CFDI.V40.ComprobanteConceptoImpuestos ConceptoImpuestos(BindingList<ComprobanteConceptoImpuesto> impuestos) {
            var conceptoImpuestos = new SAT.CFDI.V40.ComprobanteConceptoImpuestos();
            var traslados = new List<SAT.CFDI.V40.ComprobanteConceptoImpuestosTraslado>();
            var retenciones = new List<SAT.CFDI.V40.ComprobanteConceptoImpuestosRetencion>();

            foreach (var item in impuestos) {
                if (item.Tipo == TipoImpuestoEnum.Traslado) {
                    var _traslado = new SAT.CFDI.V40.ComprobanteConceptoImpuestosTraslado() {
                        Base = this.Ajustar(item.Base, 2),
                        Importe = this.Ajustar(item.Importe, 2),
                        Impuesto = string.Format("{0:000}", (int)item.Impuesto),
                        //TasaOCuota = decimal.Round(item.TasaOCuota + new decimal(0.0000001), 6),
                        //TasaOCuota = this.Ajustar(item.TasaOCuota, this._TasaOCuotaDecimales),
                        TasaOCuota = this.Ajustar(item.TasaOCuota, 6),
                        TipoFactor = Enum.GetName(typeof(FactorEnum), item.TipoFactor),
                        ImporteSpecified = item.Importe > 0,
                        TasaOCuotaSpecified = item.TasaOCuota > 0
                    };
                    traslados.Add(_traslado);
                } else if (item.Tipo == TipoImpuestoEnum.Retencion) {
                    var _retencion = new SAT.CFDI.V40.ComprobanteConceptoImpuestosRetencion() {
                        Base = this.Ajustar(item.Base, 2),
                        Importe = this.Ajustar(item.Importe, 2),
                        Impuesto = string.Format("{0:000}", (int)item.Impuesto),
                        //TasaOCuota = this.FixDecimal(item.TasaOCuota, this._TasaOCuotaDecimales),
                        TasaOCuota = this.FixDecimal(item.TasaOCuota, 6),
                        TipoFactor = Enum.GetName(typeof(FactorEnum), item.TipoFactor)
                    };
                    retenciones.Add(_retencion);
                }
            }

            if (traslados.Count > 0) {
                conceptoImpuestos.Traslados = traslados.ToArray();
            }

            if (retenciones.Count > 0) {
                conceptoImpuestos.Retenciones = retenciones.ToArray();
            }
            return conceptoImpuestos;
        }

        private SAT.CFDI.V40.ComprobanteConceptoInformacionAduanera[] ConceptoInformacionAduanera(BindingList<ComprobanteInformacionAduanera> informacionAduanera) {
            var infoAduanera = new List<SAT.CFDI.V40.ComprobanteConceptoInformacionAduanera>();
            foreach (var item in informacionAduanera) {
                infoAduanera.Add(new SAT.CFDI.V40.ComprobanteConceptoInformacionAduanera { NumeroPedimento = item.NumeroPedimento });
            }
            return infoAduanera.ToArray();
        }

        private SAT.CFDI.V40.ComprobanteConceptoParte[] ConceptoParte(BindingList<ConceptoParte> partes) {
            if (partes != null) {
                if (partes.Count > 0) {
                    var parte = new List<SAT.CFDI.V40.ComprobanteConceptoParte>();
                    foreach (var item in partes) {
                        var conceptoParte = new SAT.CFDI.V40.ComprobanteConceptoParte {
                            Cantidad = this.Ajustar(item.Cantidad),
                            ClaveProdServ = item.ClaveProdServ,
                            Descripcion = item.Descripcion,
                            Importe = item.Importe,
                            NoIdentificacion = item.NoIdentificacion,
                            Unidad = item.Unidad,
                            ValorUnitario = this.Ajustar(item.ValorUnitario),
                            ImporteSpecified = item.Importe > 0,
                            ValorUnitarioSpecified = item.ValorUnitario > 0
                        };
                        if (item.InformacionAduanera != null) {
                            conceptoParte.InformacionAduanera = this.InformacionAduanera(item.InformacionAduanera);
                        }
                        parte.Add(conceptoParte);
                    }
                    return parte.ToArray();
                }
            }
            return null;
        }

        private SAT.CFDI.V40.ComprobanteConceptoParteInformacionAduanera[] InformacionAduanera(BindingList<ComprobanteInformacionAduanera> informacionAduanera) {
            var infoAduanera = new List<SAT.CFDI.V40.ComprobanteConceptoParteInformacionAduanera>();
            foreach (var item in informacionAduanera) {
                infoAduanera.Add(new SAT.CFDI.V40.ComprobanteConceptoParteInformacionAduanera {
                    NumeroPedimento = item.NumeroPedimento
                });
            }
            return infoAduanera.ToArray();
        }
        #endregion
    }
}
