﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteFiscalSearchService : IComprobantesFiscalesSearchService {
        protected ISqlComprobanteFiscalRepository comprobanteFiscalRepository;
        protected CFDISubTipoEnum subTipo;

        public ComprobanteFiscalSearchService() {
            this.comprobanteFiscalRepository = new SqlSugarComprobanteFiscalRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public ComprobanteFiscalSearchService(CFDISubTipoEnum subTipo) {
            this.subTipo = subTipo;
            this.comprobanteFiscalRepository = new SqlSugarComprobanteFiscalRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener listado para CFDI relacionados
        /// </summary>
        /// <param name="rfc">rfc del emisor o receptor</param>
        /// <param name="year">ejercicio</param>
        /// <param name="month">periodo</param>
        public BindingList<ComprobanteFiscalDetailSingleModel> GetCFDIRelacionado(string rfc, int year = 0, int month = 0) {
            var keyValues = new List<Conditional> {
                new Conditional("_cfdi_efecto", CFDITipoComprobanteEnum.Pagos.ToString()),
                new Conditional("_cfdi_status", "Cancelado", ConditionalTypeEnum.NoEqual)
            };
            return new BindingList<ComprobanteFiscalDetailSingleModel>(this.comprobanteFiscalRepository.GetSearch(this.subTipo, rfc, keyValues, year).ToList());
        }

        /// <summary>
        /// buscar comprobantes fiscales emitidos que requieren comprobante de pagos, siempre que no esten cancelados
        /// </summary>
        public BindingList<ComprobanteFiscalDetailSingleModel> GetCFDI(string rfc, int year = 0, int month = 0) {
            var keyValues = new List<Conditional> {
                new Conditional("_cfdi_status", "Cancelado", ConditionalTypeEnum.NoEqual),
                new Conditional("_cfdi_frmpg", "99"),
                new Conditional("_cfdi_mtdpg", "PPD")
            };

            // para el caso del comprobante emitidos
            if (this.subTipo == CFDISubTipoEnum.Emitido) {
                keyValues.Add(new Conditional("_cfdi_rfcr", rfc, ConditionalTypeEnum.Equal));
            } else if (this.subTipo == CFDISubTipoEnum.Recibido) {
                keyValues.Add(new Conditional("_cfdi_rfce", rfc, ConditionalTypeEnum.Equal));
            }
            return new BindingList<ComprobanteFiscalDetailSingleModel>(this.comprobanteFiscalRepository.GetSearch(this.subTipo, keyValues, year, month).ToList());
        }

        public BindingList<ComprobanteFiscalDetailSingleModel> GetSearch(string rfc, string text) {
            var keyValues = new List<Conditional> {
                new Conditional("_cfdi_status", "Cancelado", ConditionalTypeEnum.IsNot)
            };

            if (this.subTipo == CFDISubTipoEnum.Emitido) {
                keyValues.Add(new Conditional("_cfdi_rfcr", rfc, ConditionalTypeEnum.Equal));
            }
            return new BindingList<ComprobanteFiscalDetailSingleModel>(this.comprobanteFiscalRepository.GetSearch(this.subTipo, rfc, keyValues).ToList());
        }

        /// <summary>
        /// Buscar listado de comprobantes fiscales utilizando el id de documento (UUID)
        /// </summary>
        public BindingList<ComprobanteFiscalDetailSingleModel> SearchUUID(List<string> vs) {
            if (vs != null) {
                if (vs.Count > 0) {
                    var uuids = string.Join(",", vs);
                    return new BindingList<ComprobanteFiscalDetailSingleModel>(this.comprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(
                        new List<IConditional> { 
                            new Conditional("_cfdi_uuid", uuids, ConditionalTypeEnum.In)}).ToList());
                }
            }
            return null;
        }
    }
}
