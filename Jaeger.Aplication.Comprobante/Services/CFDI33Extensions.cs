﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Aplication.Services {
    /// <summary>
    /// clase para extenciones CFDI, para la conversion entre tipos de objetos
    /// </summary>
    public class CFDI33Extensions {
        public CFDI33Extensions() {

        }

        #region version 3.3

        /// <summary>
        /// convertir Cfd.Contribuyente a V33.ComprobanteEmisor
        /// </summary>
        public CFDI.V33.ComprobanteEmisor Emisor(ComprobanteContribuyenteModel objeto) {
            var emisor = new CFDI.V33.ComprobanteEmisor() {
                Nombre = objeto.Nombre,
                Rfc = objeto.RFC,
                RegimenFiscal = objeto.RegimenFiscal
            };
            return emisor;
        }

        /// <summary>
        /// convertir Cfd.Contribyente a V33.ComprobanteReceptor
        /// </summary>
        public CFDI.V33.ComprobanteReceptor Receptor(ComprobanteContribuyenteModel objeto, string usoCFDI) {
            var receptor = new CFDI.V33.ComprobanteReceptor() {
                Nombre = objeto.Nombre,
                Rfc = objeto.RFC,
                NumRegIdTrib = objeto.NumRegIdTrib,
                UsoCFDI = usoCFDI,
                ResidenciaFiscalSpecified = false
            };

            if (!(objeto.RegimenFiscal == null)) {
                if (objeto.RegimenFiscal.Trim() != "") {
                    receptor.ResidenciaFiscalSpecified = true;
                    receptor.ResidenciaFiscal = objeto.ResidenciaFiscal;
                }
            }
            return receptor;
        }

        /// <summary>
        /// convertir objeto cfd.Comprobante a V33.Comprobante
        /// </summary>
        public CFDI.V33.Comprobante Create(ComprobanteFiscalDetailModel objeto) {
            // crear datos generales del comprobante
            var newItem = new CFDI.V33.Comprobante {
                TipoDeComprobante = Enum.GetName(typeof(CFDITipoComprobanteEnum), objeto.TipoComprobante).ToString().Substring(0, 1),
                Fecha = Convert.ToDateTime(objeto.FechaEmision.ToString("yyyy-MM-dd\\THH:mm:ss")),
                Moneda = objeto.ClaveMoneda,
                TipoCambio = Convert.ToDecimal(objeto.TipoCambio),
                TipoCambioSpecified = Convert.ToDecimal(objeto.TipoCambio) > 0,
                MetodoPago = objeto.ClaveMetodoPago,
                MetodoPagoSpecified = true,
                FormaPago = objeto.ClaveFormaPago,
                FormaPagoSpecified = true,
                LugarExpedicion = objeto.LugarExpedicion,
                CondicionesDePago = null
            };

            // condiciones de pago
            if (objeto.CondicionPago != null) {
                if (objeto.CondicionPago.Trim() != "")
                    newItem.CondicionesDePago = objeto.CondicionPago;
            }

            // comprobar folio del comprobante
            newItem.Folio = null;
            if (objeto.Folio.Trim() != "")
                newItem.Folio = objeto.Folio;

            // comprobar serie del comprobante
            newItem.Serie = null;
            if (objeto.Serie != null) {
                if (objeto.Serie.Trim() != "")
                    newItem.Serie = objeto.Serie;
            }

            // comprobantes relacionados
            if (objeto.CfdiRelacionados != null) {
                if (objeto.CfdiRelacionados.CfdiRelacionado.Count > 0)
                    newItem.CfdiRelacionados = this.ComprobanteCfdiRelacionados(objeto.CfdiRelacionados);
            }

            // procesar complementos
            //if (objeto.Complementos != null) {
            //    if (objeto.Complementos.Objeto.Count > 0) {
            //        var itemComplemento = new CFDI.V33.ComprobanteComplemento();
            //        foreach (Complemento item in objeto.Complementos.Objeto) {
            //            if (item.Nombre == EnumCfdiComplementos.Pagos10) {
            //                var itemPagos10 = ComplementoPagos.Json(item.Data);
            //                if (!(itemPagos10 == null))
            //                    itemComplemento.Pagos = this.Create(itemPagos10);
            //            }
            //        }
            //        newItem.Complemento = itemComplemento;
            //    }
            //}
            //TODO: es necesario hacer la implementacion de las addendas
            // procesar adendas
            //if (objeto.Addendas != null) {
            //    if (objeto.Addendas.Objeto.Count > 0) {
            //        CFDI.V33.ComprobanteAddenda itemAddenda = new CFDI.V33.ComprobanteAddenda();
            //        //foreach (Domain.Comprobantes.Entities.Addenda.Addenda item in objeto.Addendas.Objeto) {
            //        //    if (item.Nombre == EnumCfdiAddendas.AfasaMiscelanea)
            //        //        itemAddenda.Miscelanea = Domain.Comprobantes.Entities.Addenda.AddendaMiscelanea.Json(item.Data);
            //        //    else if (item.Nombre == EnumCfdiAddendas.AfasaReciboFiscal)
            //        //        itemAddenda.ReciboFiscal = Domain.Comprobantes.Entities.Addenda.AddendaReciboFiscal.Json(item.Data);
            //        //    else if (item.Nombre == EnumCfdiAddendas.CartaPorte)
            //        //        itemAddenda.CartaPorte = Domain.Comprobantes.Entities.Addenda.AddendaCartaPorte.Json(item.Data);
            //        //}
            //        newItem.Addenda = itemAddenda;
            //    }
            //}

            // crear emisor y receptor del comprobante
            newItem.Emisor = this.Emisor(objeto.Emisor);
            newItem.Receptor = this.Receptor(objeto.Receptor, objeto.ClaveUsoCFDI);

            // crear conceptos
            newItem.Conceptos = this.Conceptos(objeto.Conceptos);

            // impuestos del comprobante
            if (objeto.TipoComprobante != CFDITipoComprobanteEnum.Pagos && objeto.TipoComprobante != CFDITipoComprobanteEnum.Traslado)
                newItem.Impuestos = this.Impuestos(objeto);

            // totales del comprobante
            if (objeto.SubTotal <= 0)
                newItem.SubTotal = 0;
            else
                newItem.SubTotal = Math.Round(newItem.Conceptos.Sum(f => f.Importe), objeto.PrecisionDecimal);

            newItem.Descuento = Math.Round(newItem.Conceptos.Sum(p => p.Descuento), objeto.PrecisionDecimal);
            if (objeto.Total <= 0)
                newItem.Total = 0;
            else
                newItem.Total = Math.Round(((newItem.SubTotal - newItem.Descuento) + (newItem.Impuestos.TotalImpuestosTrasladados)) - (newItem.Impuestos.TotalImpuestosRetenidos), objeto.PrecisionDecimal);

            // cuando el tipo de comprobante es pagos
            if (objeto.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                newItem.DescuentoSpecified = false;
                newItem.TipoCambioSpecified = false;
                newItem.MetodoPagoSpecified = false;
                newItem.FormaPagoSpecified = false;
                newItem.CondicionesDePago = null;
                newItem.Conceptos[0].Cantidad = 1;
                newItem.Conceptos[0].ValorUnitario = 0;
                newItem.Conceptos[0].Importe = 0;
            }

            // cuando el tipo de comprobante es traslado
            if (objeto.TipoComprobante == CFDITipoComprobanteEnum.Traslado) {
                newItem.DescuentoSpecified = false;
                newItem.MetodoPagoSpecified = false;
                newItem.FormaPagoSpecified = false;
                newItem.CondicionesDePago = null;
            }
            return newItem;

        }

        /// <summary>
        /// convertir objeto Cfd.ComprobantesRelacionados a V33.ComprobanteCfdiRelacionados
        /// </summary>
        private CFDI.V33.ComprobanteCfdiRelacionados ComprobanteCfdiRelacionados(ComprobanteCfdiRelacionados objeto) {
            var newItem = new CFDI.V33.ComprobanteCfdiRelacionados();
            List<CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado> lista = new List<CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado>();

            foreach (ComprobanteCfdiRelacionadosCfdiRelacionado item in objeto.CfdiRelacionado) {
                lista.Add(new CFDI.V33.ComprobanteCfdiRelacionadosCfdiRelacionado {
                    UUID = item.IdDocumento
                });
            }

            newItem.TipoRelacion = objeto.TipoRelacion.Clave;
            newItem.CfdiRelacionado = lista.ToArray();

            return newItem;
        }

        /// <summary>
        /// convertir Cfd.Conceptos a V33.ComprobanteConcepto
        /// </summary>
        public CFDI.V33.ComprobanteConcepto[] Conceptos(BindingList<ComprobanteConceptoDetailModel> objetos) {
            if (!(objetos == null)) {
                List<CFDI.V33.ComprobanteConcepto> newItem = new List<CFDI.V33.ComprobanteConcepto>();
                foreach (ComprobanteConceptoDetailModel item in objetos) {
                    if (item.Activo == true)
                        newItem.Add(this.Concepto(item));
                }
                return newItem.ToArray();
            }
            return null;
        }

        /// <summary>
        /// Crear Cfd.Concepto a V33.ComprobanteConcepto
        /// </summary>
        public CFDI.V33.ComprobanteConcepto Concepto(ComprobanteConceptoModel objeto) {
            var newItem = new CFDI.V33.ComprobanteConcepto {
                Cantidad = decimal.Round(objeto.Cantidad, 2, MidpointRounding.AwayFromZero),
                ClaveProdServ = objeto.ClaveProdServ,
                ClaveUnidad = objeto.ClaveUnidad,
                Descripcion = objeto.Descripcion,
                ValorUnitario = decimal.Round(objeto.ValorUnitario, 2, MidpointRounding.AwayFromZero),
                Importe = decimal.Round(objeto.Importe, 2, MidpointRounding.AwayFromZero),
                Descuento = decimal.Round(objeto.Descuento, 2, MidpointRounding.AwayFromZero)
            };

            /// si existe descuento
            if (objeto.Descuento > 0)
                newItem.DescuentoSpecified = true;
            else
                newItem.DescuentoSpecified = false;

            /// opcional unidad
            if (!(objeto.Unidad == null))
                if (objeto.Unidad.Trim() != "")
                    newItem.Unidad = objeto.Unidad;

            /// opcional no de identificacion
            if (!(objeto.NoIdentificacion == null))
                if (objeto.NoIdentificacion == null)
                    if (objeto.NoIdentificacion.Trim() != "")
                        newItem.NoIdentificacion = objeto.NoIdentificacion;

            if (!(objeto.CtaPredial == null)) {
                if (objeto.CtaPredial.Trim().Length > 0) {
                    var cuentaPredial = new CFDI.V33.ComprobanteConceptoCuentaPredial() {
                        Numero = objeto.CtaPredial
                    };
                    newItem.CuentaPredial = cuentaPredial;
                }
            }

            // informacion aduanera
            if (!(objeto.InformacionAduanera == null))
                newItem.InformacionAduanera = this.ConceptoInformacionAduanera(objeto.InformacionAduanera);

            /// partes del concepto
            if (objeto.Parte.Count > 0)
                newItem.Parte = this.ConceptoParte(objeto.Parte);

            /// impuestos del concepto
            if (objeto.Impuestos.Count > 0)
                newItem.Impuestos = this.ConceptoImpuestos(objeto.Impuestos);

            return newItem;
        }

        public CFDI.V33.ComprobanteConceptoInformacionAduanera[] ConceptoInformacionAduanera(BindingList<ComprobanteInformacionAduanera> objetos) {
            if (!(objetos == null)) {
                List<CFDI.V33.ComprobanteConceptoInformacionAduanera> newItems = new List<CFDI.V33.ComprobanteConceptoInformacionAduanera>();
                foreach (ComprobanteInformacionAduanera item in objetos) {
                    var newItem = new CFDI.V33.ComprobanteConceptoInformacionAduanera {
                        NumeroPedimento = item.NumeroPedimento
                    };
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public CFDI.V33.ComprobanteConceptoParteInformacionAduanera[] ConceptoParteInformacionAduanera(BindingList<ComprobanteInformacionAduanera> objetos) {
            if (!(objetos == null)) {
                List<CFDI.V33.ComprobanteConceptoParteInformacionAduanera> newItems = new List<CFDI.V33.ComprobanteConceptoParteInformacionAduanera>();
                foreach (ComprobanteInformacionAduanera item in objetos) {
                    var newItem = new CFDI.V33.ComprobanteConceptoParteInformacionAduanera {
                        NumeroPedimento = item.NumeroPedimento
                    };
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        /// <summary>
        /// crear Cfd.ConceptoParte V33.ComprobanteConceptoParte
        /// </summary>
        public CFDI.V33.ComprobanteConceptoParte[] ConceptoParte(BindingList<ConceptoParte> partes) {
            List<CFDI.V33.ComprobanteConceptoParte> item = new List<CFDI.V33.ComprobanteConceptoParte>();

            foreach (ConceptoParte parte in partes) {
                var newItem = new CFDI.V33.ComprobanteConceptoParte {
                    Cantidad = parte.Cantidad,
                    ClaveProdServ = parte.ClaveProdServ,
                    NoIdentificacion = parte.NoIdentificacion,
                    Unidad = parte.Unidad,
                    ValorUnitario = parte.ValorUnitario,
                    ValorUnitarioSpecified = parte.ValorUnitario > 0,
                    Descripcion = parte.Descripcion,
                    Importe = parte.Importe,
                    ImporteSpecified = parte.Importe > 0
                };
                if (!(parte.InformacionAduanera == null))
                    newItem.InformacionAduanera = this.ConceptoParteInformacionAduanera(parte.InformacionAduanera);
                item.Add(newItem);
            }
            return item.ToArray();
        }

        /// <summary>
        /// convertir lista Cfd.ConceptoImpuesto a V33.ComprobanteConceptoImpuestos
        /// </summary>
        public CFDI.V33.ComprobanteConceptoImpuestos ConceptoImpuestos(BindingList<ComprobanteConceptoImpuesto> objetos) {
            var impuestos = new CFDI.V33.ComprobanteConceptoImpuestos();
            List<CFDI.V33.ComprobanteConceptoImpuestosTraslado> listaTraslados = new List<CFDI.V33.ComprobanteConceptoImpuestosTraslado>();
            List<CFDI.V33.ComprobanteConceptoImpuestosRetencion> listaRetenciones = new List<CFDI.V33.ComprobanteConceptoImpuestosRetencion>();

            foreach (ComprobanteConceptoImpuesto item in objetos) {
                if (item.Tipo == TipoImpuestoEnum.Traslado) {
                    CFDI.V33.ComprobanteConceptoImpuestosTraslado newTraslado = new CFDI.V33.ComprobanteConceptoImpuestosTraslado();
                    newTraslado.Base = Math.Round(item.Base, 6);
                    newTraslado.Impuesto = string.Format("{0:000}", (int)item.Impuesto);
                    newTraslado.TasaOCuota = decimal.Round(item.TasaOCuota + new decimal(0.0000001), 6);//HelperComun.FormatoNumero(item.TasaOCuota, 6);
                    newTraslado.Importe = this.FormatoNumero(item.Importe, 2);
                    newTraslado.ImporteSpecified = item.Importe > 0;
                    newTraslado.TasaOCuotaSpecified = item.TasaOCuota > 0;
                    newTraslado.TipoFactor = Enum.GetName(typeof(FactorEnum), item.TipoFactor);
                    listaTraslados.Add(newTraslado);
                }
                else if (item.Tipo == TipoImpuestoEnum.Retencion) {
                    CFDI.V33.ComprobanteConceptoImpuestosRetencion newRetencion = new CFDI.V33.ComprobanteConceptoImpuestosRetencion();
                    newRetencion.Base = item.Base;
                    newRetencion.Importe = this.FormatoNumero(item.Importe, 2);
                    newRetencion.Impuesto = string.Format("{0:000}", (int)item.Impuesto);
                    newRetencion.TasaOCuota = decimal.Round(item.TasaOCuota + new decimal(0.0000001), 6); //HelperComun.FormatoNumero(item.TasaOCuota, 6);
                    newRetencion.TipoFactor = Enum.GetName(typeof(FactorEnum), item.TipoFactor);
                    listaRetenciones.Add(newRetencion);
                }
            }

            if (listaTraslados.Count > 0)
                impuestos.Traslados = listaTraslados.ToArray();

            if (listaRetenciones.Count > 0)
                impuestos.Retenciones = listaRetenciones.ToArray();
            return impuestos;
        }

        /// <summary>
        /// calcular los impuestos del objeto Cfd.Comprobante a V33.ComprobanteImpuestos
        /// </summary>
        public CFDI.V33.ComprobanteImpuestos Impuestos(ComprobanteFiscalDetailModel objeto) {
            var impuestos = new CFDI.V33.ComprobanteImpuestos();
            List<CFDI.V33.ComprobanteImpuestosTraslado> listaTraslados = new List<CFDI.V33.ComprobanteImpuestosTraslado>();
            List<CFDI.V33.ComprobanteImpuestosRetencion> listaRetenciones = new List<CFDI.V33.ComprobanteImpuestosRetencion>();

            if (!(objeto.Conceptos == null)) {
                foreach (ComprobanteConceptoModel item in objeto.Conceptos) {
                    foreach (ComprobanteConceptoImpuesto impuesto in item.Impuestos) {
                        int numeral = (int)impuesto.Impuesto;
                        if (impuesto.Tipo == TipoImpuestoEnum.Traslado) {
                            var newTraslado = new CFDI.V33.ComprobanteImpuestosTraslado {
                                Impuesto = string.Format("{0:000}", numeral),
                                Importe = this.FormatoNumero(impuesto.Importe, 2),
                                TasaOCuota = decimal.Round(impuesto.TasaOCuota + new decimal(0.0000001), 6), //HelperComun.FormatoNumero(impuesto.TasaOCuota, 6);
                                TipoFactor = Enum.GetName(typeof(FactorEnum), impuesto.TipoFactor)
                            };
                            var buscar = listaTraslados.Find((CFDI.V33.ComprobanteImpuestosTraslado x) => x.Impuesto == newTraslado.Impuesto & x.TasaOCuota == newTraslado.TasaOCuota & x.TipoFactor == newTraslado.TipoFactor);
                            if (buscar == null)
                                listaTraslados.Add(newTraslado);
                            else {
                                newTraslado.Importe = buscar.Importe + newTraslado.Importe;
                                listaTraslados.Remove(buscar);
                                listaTraslados.Add(newTraslado);
                            }
                        }
                        else if (impuesto.Tipo == TipoImpuestoEnum.Retencion) {
                            CFDI.V33.ComprobanteImpuestosRetencion newRetencion = new CFDI.V33.ComprobanteImpuestosRetencion();
                            newRetencion.Impuesto = string.Format("{0:000}", numeral);
                            newRetencion.Importe = this.FormatoNumero(impuesto.Importe, 2);
                            CFDI.V33.ComprobanteImpuestosRetencion busca = new CFDI.V33.ComprobanteImpuestosRetencion();
                            if (busca != null) {
                                newRetencion.Importe = decimal.Add(newRetencion.Importe, busca.Importe);
                                listaRetenciones.Remove(busca);
                            }
                            listaRetenciones.Add(newRetencion);
                        }
                    }
                }
            }

            if (listaTraslados.Count <= 0)
                impuestos.TotalImpuestosTrasladadosSpecified = false;
            else {
                impuestos.TotalImpuestosTrasladadosSpecified = true;
                impuestos.Traslados = listaTraslados.ToArray();
                impuestos.TotalImpuestosTrasladados = this.FormatoNumero(listaTraslados.Sum<CFDI.V33.ComprobanteImpuestosTraslado>((CFDI.V33.ComprobanteImpuestosTraslado p) => p.Importe), 2);
            }

            if (listaRetenciones.Count <= 0)
                impuestos.TotalImpuestosRetenidosSpecified = false;
            else {
                impuestos.TotalImpuestosRetenidosSpecified = true;
                impuestos.Retenciones = listaRetenciones.ToArray();
                impuestos.TotalImpuestosRetenidos = this.FormatoNumero(listaRetenciones.Sum<CFDI.V33.ComprobanteImpuestosRetencion>((CFDI.V33.ComprobanteImpuestosRetencion p) => p.Importe), 2);
            }
            return impuestos;
        }

        #endregion

        #region complemento timbre fiscal version 11

        public ComplementoTimbreFiscal TimbreFiscal(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital objeto) {
            ComplementoTimbreFiscal newItem = new ComplementoTimbreFiscal {
                UUID = objeto.UUID.ToUpper(),
                Version = objeto.Version,
                FechaTimbrado = objeto.FechaTimbrado,
                Leyenda = objeto.Leyenda,
                NoCertificadoSAT = objeto.NoCertificadoSAT,
                RFCProvCertif = objeto.RfcProvCertif,
                SelloCFD = objeto.SelloCFD,
                SelloSAT = objeto.SelloSAT
            };
            return newItem;
        }

        #endregion

        #region complemento de pagos10

        /// <summary>
        /// crear complemento pagos10
        /// </summary>
        public CFDI.Complemento.Pagos.V10.Pagos Create(ComplementoPagos objeto) {
            if (!(objeto == null)) {
                var newItem = new CFDI.Complemento.Pagos.V10.Pagos();
                newItem.Version = objeto.Version;
                newItem.Pago = this.Pagos10(objeto.Pago);
                return newItem;
            }
            return null;
        }

        public CFDI.Complemento.Pagos.V10.PagosPago[] Pagos10(BindingList<ComplementoPagosPago> objetos) {
            if (!(objetos == null)) {
                List<CFDI.Complemento.Pagos.V10.PagosPago> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPago>();
                foreach (ComplementoPagosPago item in objetos) {
                    CFDI.Complemento.Pagos.V10.PagosPago newItem = new CFDI.Complemento.Pagos.V10.PagosPago();
                    newItem.CadPago = item.CadPago;
                    newItem.CtaBeneficiario = item.CtaBeneficiario;
                    newItem.CtaOrdenante = item.CtaOrdenante;
                    newItem.FechaPago = DateTime.Parse(item.FechaPago.ToString());
                    newItem.FormaDePagoP = item.FormaDePagoP.Clave;
                    newItem.Impuestos = this.Pagos10(item.Impuestos);
                    newItem.MonedaP = item.MonedaP;
                    newItem.Monto = item.Monto;
                    newItem.NomBancoOrdExt = item.NomBancoOrdExt;
                    newItem.NumOperacion = item.NumOperacion;

                    newItem.RfcEmisorCtaBen = null;
                    if (item.RfcEmisorCtaBen != null)
                        if (item.RfcEmisorCtaBen.Trim().Length > 0)
                            newItem.RfcEmisorCtaBen = item.RfcEmisorCtaBen;

                    newItem.RfcEmisorCtaOrd = null;
                    if (item.RfcEmisorCtaOrd != null)
                        if (item.RfcEmisorCtaOrd.Trim().Length > 0)
                            newItem.RfcEmisorCtaOrd = item.RfcEmisorCtaOrd;

                    newItem.TipoCadPago = item.TipoCadPago;
                    newItem.TipoCadPagoSpecified = !String.IsNullOrEmpty(item.TipoCadPago);
                    newItem.TipoCambioP = item.TipoCambioP;
                    newItem.TipoCambioPSpecified = item.TipoCambioP > 0;
                    newItem.DoctoRelacionado = this.Pagos10(item.DoctoRelacionado);

                    item.SelloPago = null;
                    if (item.SelloPago != null)
                        newItem.SelloPago = Convert.FromBase64String(item.SelloPago);

                    newItem.CertPago = null;
                    if (item.CertPago != null)
                        newItem.CertPago = Convert.FromBase64String(item.CertPago);

                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado[] Pagos10(BindingList<ComplementoPagoDoctoRelacionado> objetos) {
            if (!(objetos == null)) {
                List<CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado>();
                foreach (ComplementoPagoDoctoRelacionado item in objetos) {
                    var newItem = new CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado {
                        Folio = item.Folio,
                        IdDocumento = item.IdDocumento,
                        ImpPagado = item.ImpPagado,
                        ImpPagadoSpecified = item.ImpPagado > 0,
                        ImpSaldoAnt = item.ImpSaldoAnt,
                        ImpSaldoAntSpecified = item.ImpSaldoAnt > 0,
                        MetodoDePagoDR = item.MetodoPago,
                        MonedaDR = item.Moneda,
                        NumParcialidad = item.NumParcialidad.ToString(),
                        Serie = item.Serie,
                        TipoCambioDR = item.TipoCambio,
                        TipoCambioDRSpecified = item.TipoCambio > 0,
                        ImpSaldoInsoluto = item.ImpSaldoInsoluto,
                        ImpSaldoInsolutoSpecified = item.ImpSaldoInsoluto > 0
                    };

                    if (newItem.MetodoDePagoDR == "PPD")
                        newItem.ImpSaldoInsolutoSpecified = true;

                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public CFDI.Complemento.Pagos.V10.PagosPagoImpuestos[] Pagos10(BindingList<ComplementoPagoImpuestos> objetos) {
            if (!(objetos == null)) {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestos> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestos>();
                foreach (ComplementoPagoImpuestos item in objetos) {
                    var newItem = new CFDI.Complemento.Pagos.V10.PagosPagoImpuestos {
                        Retenciones = this.Pagos10(item.Retenciones),
                        TotalImpuestosRetenidos = item.TotalImpuestosRetenidos,
                        Traslados = this.Pagos10(item.Traslados),
                        TotalImpuestosTrasladados = item.TotalImpuestosTrasladados
                    };

                    if (newItem.Retenciones == null)
                        newItem.TotalImpuestosRetenidosSpecified = false;
                    else
                        newItem.TotalImpuestosRetenidosSpecified = true;

                    if (newItem.Traslados == null)
                        newItem.TotalImpuestosTrasladadosSpecified = false;
                    else
                        newItem.TotalImpuestosTrasladadosSpecified = true;
                }
                return newItems.ToArray();
            }
            return null;
        }

        public CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion[] Pagos10(BindingList<ComplementoPagoImpuestosRetencion> objetos) {
            if (!(objetos == null)) {
                List<Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion>();
                foreach (ComplementoPagoImpuestosRetencion item in objetos) {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion newItem = new CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion();
                    newItem.Importe = item.Importe;
                    newItem.Impuesto = item.Impuesto;
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        public CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado[] Pagos10(BindingList<ComplementoPagoImpuestosTraslado> objetos) {
            if (!(objetos == null)) {
                List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado> newItems = new List<CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado>();
                foreach (ComplementoPagoImpuestosTraslado item in objetos) {
                    Jaeger.CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado newItem = new CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado();
                    newItem.Importe = item.Importe;
                    newItem.Impuesto = item.Impuesto;
                    newItem.TasaOCuota = item.TasaOCuota;
                    newItem.TipoFactor = item.TipoFactor;
                    newItems.Add(newItem);
                }
                return newItems.ToArray();
            }
            return null;
        }

        #endregion

        #region funciones locales

        private decimal FormatoNumero(decimal numero, int decimales) {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        #endregion
    }

    
}
