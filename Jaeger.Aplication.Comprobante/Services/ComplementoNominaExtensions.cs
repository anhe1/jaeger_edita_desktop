﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities.Complemento;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComplementoNominaExtensions : ComplementoExtensions {
        #region complemento Nomina 12
        /// <summary>
        /// crear complemento de nomina a partir de la clase comun ComplementoNomina
        /// </summary>
        public SAT.CFDI.Complemento.Nomina.V12.Nomina Create(ComplementoNomina nomina) {
            if (nomina != null) {
                var _complemento = new SAT.CFDI.Complemento.Nomina.V12.Nomina {
                    Emisor = this.Nomina12(nomina.Emisor),
                    Receptor = this.Nomina12(nomina.Receptor),
                    FechaInicialPago = nomina.FechaInicialPago.Value,
                    FechaFinalPago = nomina.FechaFinalPago.Value,
                    FechaPago = nomina.FechaPago.Value,
                    NumDiasPagados = nomina.NumDiasPagados,
                    TipoNomina = nomina.TipoNomina,
                    Percepciones = this.Nomina12(nomina.Percepciones),
                    Deducciones = this.Nomina12(nomina.Deducciones),
                    Incapacidades = this.Nomina12(nomina.Incapacidades),
                    OtrosPagos = this.Nomina12(nomina.OtrosPagos)
                };
                return _complemento;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaEmisor Nomina12(ComplementoNominaEmisor emisor) {
            if (emisor != null) {
                var _emisor = new SAT.CFDI.Complemento.Nomina.V12.NominaEmisor {
                    Curp = emisor.CURP,
                    RegistroPatronal = emisor.RegistroPatronal,
                    RfcPatronOrigen = null
                };

                if (emisor.RFCPatronOrigen != null)
                    _emisor.RfcPatronOrigen = emisor.RFCPatronOrigen;

                _emisor.EntidadSNCF = null;
                if (emisor.EntidadSncf != null) {
                    _emisor.EntidadSNCF = new SAT.CFDI.Complemento.Nomina.V12.NominaEmisorEntidadSNCF {
                        MontoRecursoPropio = emisor.EntidadSncf.MontoRecursoPropio,
                        OrigenRecurso = emisor.EntidadSncf.OrigenRecurso,
                        MontoRecursoPropioSpecified = true
                    };
                }
                return _emisor;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaReceptor Nomina12(ComplementoNominaReceptor receptor) {
            if (receptor != null) {
                var _receptor = new SAT.CFDI.Complemento.Nomina.V12.NominaReceptor {
                    Antigüedad = receptor.Antiguedad,
                    Banco = receptor.Banco,
                    BancoSpecified = true,
                    ClaveEntFed = receptor.ClaveEntFed,
                    CuentaBancaria = receptor.CuentaBancaria,
                    Curp = receptor.CURP
                };
                return _receptor;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones Nomina12(ComplementoNominaPercepciones percepciones) {
            if (percepciones != null) {
                var _percepciones = new SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones();
                var _lista = new List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion>();
                // lista de percepciones
                foreach (ComplementoNominaPercepcion item in percepciones.Percepcion) {
                    var _percepcion = new SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion {
                        TipoPercepcion = item.TipoPercepcion,
                        Clave = item.Clave,
                        Concepto = item.Concepto,
                        ImporteExento = item.ImporteExento,
                        ImporteGravado = item.ImporteGravado,
                        AccionesOTitulos = this.Nomina12(item.AccionesOTitulos),
                        HorasExtra = this.Nomina12(item.HorasExtra)
                    };
                    _lista.Add(_percepcion);
                }
                _percepciones.Percepcion = _lista.ToArray();
                _percepciones.JubilacionPensionRetiro = this.Nomina12(percepciones.JubilacionPensionRetiro);
                _percepciones.SeparacionIndemnizacion = this.Nomina12(percepciones.SeparacionIndemnizacion);
                return _percepciones;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaOtroPago[] Nomina12(BindingList<ComplementoNominaOtroPago> otrosPagos) {
            if (otrosPagos != null) {
                var _otrosPagos = new List<SAT.CFDI.Complemento.Nomina.V12.NominaOtroPago>();
                foreach (ComplementoNominaOtroPago item in otrosPagos) {
                    var _otroPago = new SAT.CFDI.Complemento.Nomina.V12.NominaOtroPago {
                        Clave = item.Clave,
                        Concepto = item.Concepto,
                        Importe = item.Importe,
                        TipoOtroPago = item.TipoOtroPago,
                        CompensacionSaldosAFavor = this.Nomina12(item.CompensacionSaldosAFavor)
                    };
                    _otrosPagos.Add(_otroPago);
                }
                return _otrosPagos.ToArray();
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaDeducciones Nomina12(ComplementoNominaDeducciones deducciones) {
            if (deducciones != null) {
                var _deducciones = new SAT.CFDI.Complemento.Nomina.V12.NominaDeducciones();
                var _lista = new List<SAT.CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion>();
                foreach (ComplementoNominaDeduccion item in deducciones.Deduccion) {
                    var _deduccion = new SAT.CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion {
                        Clave = item.Clave,
                        Concepto = item.Concepto,
                        Importe = item.Importe,
                        TipoDeduccion = item.TipoDeduccion
                    };
                    _lista.Add(_deduccion);
                }
                _deducciones.Deduccion = _lista.ToArray();
                _deducciones.TotalImpuestosRetenidos = deducciones.TotalImpuestosRetenidos;
                _deducciones.TotalOtrasDeducciones = deducciones.TotalOtrasDeducciones;
                return _deducciones;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor Nomina12(ComplementoNominaOtroPagoCompensacionSaldosAFavor compensacionSaldosAFavor) {
            if (compensacionSaldosAFavor != null) {
                var _compensacionSaldoAFavor = new SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor {
                    Año = compensacionSaldosAFavor.Anio,
                    RemanenteSalFav = compensacionSaldosAFavor.RemanenteSalFav,
                    SaldoAFavor = compensacionSaldosAFavor.SaldoAFavor
                };
                return _compensacionSaldoAFavor;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaIncapacidad[] Nomina12(BindingList<ComplementoNominaIncapacidad> incapacidades) {
            if (incapacidades != null) {
                var _incapaciades = new List<SAT.CFDI.Complemento.Nomina.V12.NominaIncapacidad>();
                foreach (ComplementoNominaIncapacidad item in incapacidades) {
                    var _incapacidad = new SAT.CFDI.Complemento.Nomina.V12.NominaIncapacidad {
                        DiasIncapacidad = item.DiasIncapacidad,
                        ImporteMonetario = item.ImporteMonetario,
                        TipoIncapacidad = item.TipoIncapacidad,
                        ImporteMonetarioSpecified = item.ImporteMonetario > 0
                    };
                    _incapaciades.Add(_incapacidad);
                }
                return _incapaciades.ToArray();
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion Nomina12(ComplementoNominaPercepcionesSeparacionIndemnizacion separacionIndemnizacion) {
            if (separacionIndemnizacion != null) {
                var nuevo = new SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesSeparacionIndemnizacion {
                    IngresoAcumulable = separacionIndemnizacion.IngresoAcumulable,
                    IngresoNoAcumulable = separacionIndemnizacion.IngresoNoAcumulable,
                    NumAñosServicio = separacionIndemnizacion.NumaniosServicio,
                    TotalPagado = separacionIndemnizacion.TotalPagado,
                    UltimoSueldoMensOrd = separacionIndemnizacion.UltimoSueldoMensOrd
                };
                return nuevo;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro Nomina12(ComplementoNominaPercepcionesJubilacionPensionRetiro jubilacionPensionRetiro) {
            if (jubilacionPensionRetiro != null) {
                var _juvilacionPensionRetiro = new SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesJubilacionPensionRetiro {
                    IngresoAcumulable = jubilacionPensionRetiro.IngresoAcumulable,
                    IngresoNoAcumulable = jubilacionPensionRetiro.IngresoNoAcumulable,
                    MontoDiario = jubilacionPensionRetiro.MontoDiario,
                    TotalParcialidad = jubilacionPensionRetiro.TotalParcialidad,
                    TotalUnaExhibicion = jubilacionPensionRetiro.TotalUnaExhibicion
                };
                return _juvilacionPensionRetiro;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos Nomina12(ComplementoNominaPercepcionAccionesOTitulos accionesOTitulos) {
            if (accionesOTitulos != null) {
                var _accionesOTitulos = new SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos {
                    PrecioAlOtorgarse = accionesOTitulos.PrecioAlOtorgarse,
                    ValorMercado = accionesOTitulos.ValorMercado
                };
                return _accionesOTitulos;
            }
            return null;
        }

        private SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra[] Nomina12(BindingList<ComplementoNominaPercepcionHorasExtra> horasExtra) {
            if (horasExtra != null) {
                var _horasExtra = new List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra>();
                foreach (ComplementoNominaPercepcionHorasExtra item in horasExtra) {
                    var _horaExtra = new SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra {
                        Dias = item.Dias,
                        HorasExtra = item.HorasExtra,
                        ImportePagado = item.ImportePagado,
                        TipoHoras = item.TipoHoras
                    };
                    _horasExtra.Add(_horaExtra);
                }
                return _horasExtra.ToArray();
            }
            return null;
        }
        #endregion
    }
}
