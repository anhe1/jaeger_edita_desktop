﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Services {
    public class SerializeComplementoNominaExtensions {
        #region complemento de nomina12
        public static IComplementoNominaDetailModel Create(SAT.CFDI.Complemento.Nomina.V12.Nomina nomina) {
            IComplementoNominaDetailModel d0 = new ComplementoNominaDetailModel {
                Activo = true,
                FechaFinalPago = nomina.FechaFinalPago,
                FechaInicialPago = nomina.FechaInicialPago,
                FechaPago = nomina.FechaPago,
                NumDiasPagados = nomina.NumDiasPagados,
                Version = nomina.Version,
                TipoNomina = nomina.TipoNomina,
            };
            d0 = Create(nomina.Receptor, ref d0);
            d0 = Create(nomina.Emisor, ref d0);
            var d1 = Create(nomina.Percepciones);
            var d2 = Create(nomina.Deducciones);
            var d3 = Create(nomina.OtrosPagos);
            d1.AddRange(d2);
            d1.AddRange(d3);
            d0.Partes = new BindingList<ComplementoNominaParteDetailModel>(d1);
            return d0;
        }

        private static IComplementoNominaDetailModel Create(SAT.CFDI.Complemento.Nomina.V12.NominaEmisor emisor, ref IComplementoNominaDetailModel nomina) {
            nomina.RegistroPatronal = nomina.RegistroPatronal;
            return nomina;
        }

        private static IComplementoNominaDetailModel Create(SAT.CFDI.Complemento.Nomina.V12.NominaReceptor receptor, ref IComplementoNominaDetailModel nomina) {
            nomina.Antiguedad = receptor.Antigüedad;
            nomina.Banco = receptor.Banco;
            nomina.EntidadFederativa = receptor.ClaveEntFed;
            nomina.CtaBanco = receptor.CuentaBancaria;
            nomina.CURP = receptor.Curp;
            nomina.Departamento = receptor.Departamento;
            nomina.FechaInicioRelLaboral = receptor.FechaInicioRelLaboral;
            nomina.NumEmpleado = receptor.NumEmpleado;
            nomina.NumSeguridadSocial = receptor.NumSeguridadSocial;
            nomina.PeriodicidadPago = receptor.PeriodicidadPago;
            nomina.Puesto = receptor.Puesto;
            nomina.RiesgoPuesto = receptor.RiesgoPuesto;
            nomina.SalarioBaseCotApor = receptor.SalarioBaseCotApor;
            nomina.SalarioDiarioIntegrado = receptor.SalarioDiarioIntegrado;
            nomina.TipoContrato = receptor.TipoContrato;
            nomina.TipoJornada = receptor.TipoJornada;
            nomina.TipoRegimen = receptor.TipoRegimen;
            return nomina;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones percepciones) {
            var d0 = new List<ComplementoNominaParteDetailModel>();
            foreach (var item in percepciones.Percepcion) {
                var d1 = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Clave = item.Clave,
                    Concepto = item.Concepto,
                    Tipo = item.TipoPercepcion,
                    ImporteGravado = item.ImporteGravado,
                    ImporteExento = item.ImporteExento,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.Percepcion
                };
                d0.Add(d1);
                if (item.HorasExtra != null) {
                    foreach (var item1 in item.HorasExtra) {
                        var d2 = new ComplementoNominaParteDetailModel {
                            HorasExtra = item1.HorasExtra,
                            ImportePagadoHorasExtra = item1.ImportePagado,
                            TipoHoras = item1.TipoHoras,
                            Dias = item1.Dias,
                            TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.HorasExtra,
                            Activo = true
                        };
                        d0.Add(d2);
                    }
                }

                if (item.AccionesOTitulos != null) {
                    var d3 = new ComplementoNominaParteDetailModel {
                        Activo = true,
                        IdDoc = 5
                    };
                    d0.Add(d3);
                }

            }
            return d0;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaDeducciones deducciones) {
            var d0 = new List<ComplementoNominaParteDetailModel>();
            foreach (var item in deducciones.Deduccion) {
                var d1 = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Clave = item.Clave,
                    Concepto = item.Concepto,
                    Tipo = item.TipoDeduccion,
                    Importe = item.Importe,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.Deduccion
                };
                d0.Add(d1);
            }
            return d0;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaOtroPago[] otroPago) {
            var d0 = new List<ComplementoNominaParteDetailModel>();
            foreach (var item in otroPago) {
                var d1 = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Tipo = item.TipoOtroPago,
                    Clave = item.Clave,
                    Concepto = item.Concepto,
                    Importe = item.Importe,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.OtrosPagos
                };
                d0.Add(d1);
            }
            return d0;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaIncapacidad[] incapacidades) {
            var d0 = new List<ComplementoNominaParteDetailModel>();
            foreach (var item in incapacidades) {
                var d1 = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Tipo = item.TipoIncapacidad,
                    DiasIncapacidad = item.DiasIncapacidad,
                    DescuentoIncapcidad = item.ImporteMonetario,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.Incapacidad
                };
                d0.Add(d1);
            }
            return d0;
        }
        #endregion
    }
}
