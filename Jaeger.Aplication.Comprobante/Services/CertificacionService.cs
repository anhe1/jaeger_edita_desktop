﻿using System;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Html.Contracts;
using Jaeger.Aplication.Html.Services;
using Jaeger.Certifica.Contracts;
using Jaeger.Certifica.Services;
using Jaeger.Domain.Certificado.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Crypto.Services;
using Jaeger.Domain.Certificado.Entities;
using Jaeger.DataAccess.Certificado.Repositories;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Aplication.Comprobante.Contracts;

namespace Jaeger.Aplication.Comprobante.Services {
    /// <summary>
    /// servicio de certificacion de comprobantes fiscales (CFDI)
    /// </summary>
    public partial class CertificacionService : ProveedorAutorizado, ICertificacionService {
        protected ISqlCertificadoRepository certificadoRepository;
        protected IProveedorCertificacion servicio;
        protected IHtmlToPDFService pdf;
        protected CFDI40Extensions cfd40Ext;

        public CertificacionService() {
            this.cfd40Ext = new CFDI40Extensions();
            this.certificadoRepository = new SqlSugarCertificadoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.Certificacion = ConfigService.Synapsis.ProveedorAutorizado.Certificacion;
            this.Certificacion.Provider = ServiceProvider.EnumServicePAC.SolucionFactible;
            this.Cancelacion = ConfigService.Synapsis.ProveedorAutorizado.Cancelacion;

            this.pdf = new HtmlToPDFService {
                Logo = ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media, string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave)),
                PathDefault = ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Comprobantes)
            };
        }

        public string Mensaje {
            get; set;
        }

        public int Codigo {
            get; set;
        }

        /// <summary>
        /// certificacion de un comprobante fiscal Model
        /// </summary>
        public SAT.CFDI.V40.Comprobante Timbrar(ComprobanteFiscalDetailModel cfdi) {
            if (cfdi != null) {
                var newItem = this.cfd40Ext.Create(cfdi);
                if (newItem != null) {
                    return this.Timbrar(newItem);
                } else {
                    this.Codigo = 2;
                    this.Mensaje = "Error al generar el comprobante fiscal (XML)";
                }
            } else {
                this.Codigo = 1;
                this.Mensaje = "No se especifico un objeto valido.";
            }
            return null;
        }

        /// <summary>
        /// certificacion de un objeto comprobante fiscal
        /// </summary>

        public SAT.CFDI.V40.Comprobante Timbrar(SAT.CFDI.V40.Comprobante cfdi) {
            if (cfdi != null) {
                cfdi = this.Sellar(cfdi);
                if (cfdi != null) {
                    if (this.Certificacion.Provider == ServiceProvider.EnumServicePAC.SolucionFactible)
                        this.servicio = new SolucionFactibleService(this.Configura(this.Certificacion));
                    else if (this.Certificacion.Provider == ServiceProvider.EnumServicePAC.FiscoClic) {
                        //this.servicio = new FiscoClic(this.Configura(this.Certificacion));
                    }
                    else if (this.Certificacion.Provider == ServiceProvider.EnumServicePAC.Factorum) {
                        //this.servicio = new Factorum(this.Configura(this.Certificacion));
                        //this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                    } else {
                        this.Codigo = 2;
                        this.Mensaje = "Error al generar el comprobante fiscal (XML)";
                        cfdi = null;
                    }

                    cfdi = this.servicio.Timbrar(cfdi.Serialize());
                    this.Codigo = this.servicio.Codigo;
                    this.Mensaje = this.servicio.Mensaje;

                    // resultado
                    if (cfdi != null)
                        return cfdi;
                } else {
                    this.Codigo = 3;
                    this.Mensaje = "No se envío un objeto válido";
                }
            }
            return null;
        }

        public SAT.CFDI.Cancel.CancelaCFDResponse Cancelar(string idDocumento, string clave) {
            if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.SolucionFactible) {
                // obtener información de certificado y llave
                var info = this.InfoCertificados(OrigenCertificadoEnum.BaseDeDatos);
                this.servicio = new SolucionFactibleService(this.Configura(this.Cancelacion));
                this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                this.servicio.CerBase64 = info.CerB64;
                this.servicio.KeyBase64 = info.KeyB64;
                this.servicio.PassKey = info.KeyPassB64;
            } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.FiscoClic) {
                //this.servicio = new FiscoClic(this.Configura(this.Cancelacion));
                //this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
            } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.Factorum) {
                //this.servicio = new Factorum(this.Configura(this.Cancelacion));
                //this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
            } else {
                this.Codigo = 1;
                this.Mensaje = "No se especifico Proveedor Autorizado de Certificación";
                return null;
            }

            var response = this.servicio.Cancelar(idDocumento, clave);
            this.Codigo = this.servicio.Codigo;
            this.Mensaje = this.servicio.Mensaje;
            if (response != null) {
                return SAT.CFDI.Cancel.CancelaCFDResponse.LoadXml(response.Xml());
            }
            return null;
        }

        public SAT.CFDI.Cancel.CancelaCFDResponse Cancelar(ComprobanteFiscalDetailModel cfdi, string clave) {
            if (cfdi != null) {
                if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.SolucionFactible) {
                    // obtener información de certificado y llave
                    var info = this.InfoCertificados(OrigenCertificadoEnum.BaseDeDatos);
                    this.servicio = new SolucionFactibleService(this.Configura(this.Cancelacion));
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                    this.servicio.CerBase64 = info.CerB64;
                    this.servicio.KeyBase64 = info.KeyB64;
                    this.servicio.PassKey = info.KeyPassB64;
                } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.FiscoClic) {
                    //this.servicio = new FiscoClic(this.Configura(this.Cancelacion));
                    //this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                } else if (this.Cancelacion.Provider == ServiceProvider.EnumServicePAC.Factorum) {
                    //this.servicio = new Factorum(this.Configura(this.Cancelacion));
                    this.servicio.Settings.RFC = ConfigService.Synapsis.Empresa.RFC;
                } else {
                    this.Codigo = 1;
                    this.Mensaje = "No se especifico Proveedor Autorizado de Certificación";
                    return null;
                }

                var response = this.servicio.Cancelar(cfdi.TimbreFiscal.UUID, clave);
                this.Codigo = this.servicio.Codigo;
                this.Mensaje = this.servicio.Mensaje;
                if (response != null)
                    return SAT.CFDI.Cancel.CancelaCFDResponse.LoadXml(response.Xml());
            } else {
                this.Codigo = 1;
                this.Mensaje = "No se envio un objeto válido";
            }
            return null;
        }

        #region metodos privados

        private SAT.CFDI.V40.Comprobante Sellar(SAT.CFDI.V40.Comprobante cfdi) {
            // obtener información de certificado y llave
            var info = this.InfoCertificados(OrigenCertificadoEnum.BaseDeDatos);

            if (info != null) {
                Certificate cert = new Certificate();
                PrivateKey key = new PrivateKey();
                // carga de informacion de llave y certificado
                cert.CargarB64(info.CerB64);
                key.CargarB64(info.KeyB64, info.KeyPassB64);
                cert.CargarLlavePrivada(key);
                // agregar no. de serie del certificado y el certificado en base64
                cfdi.NoCertificado = cert.NoSerie;
                cfdi.Certificado = cert.ExportarB64();
                // sellar el comprobante
                cfdi.Sello = key.SellarCadenaSha256(cfdi.CadenaOriginalOff);
                if (key.CodigoDeError != 0) {
                    this.Codigo = key.CodigoDeError;
                    this.Mensaje = string.Concat("Sello de Comprobante: ", key.MensajeDeError);
                    return null;
                }
                return cfdi;
            } else {
                this.Codigo = 3;
                this.Mensaje = "No existe información válida para llave y certificado";
            }
            return null;
        }

        private CertificadoModel InfoCertificados(OrigenCertificadoEnum origen) {
            if (origen == OrigenCertificadoEnum.BaseDeDatos) {
                var info = this.certificadoRepository.GetCertificado();
                return info;
            } else if (origen == OrigenCertificadoEnum.Archivo) {

            }
            return null;
        }

        private Certifica.Entities.ProviderService Configura(Domain.Empresa.Contracts.IServiceProvider conf) {
            var response = new Certifica.Entities.ProviderService {
                User = conf.User,
                Password = conf.Pass,
                Production = conf.Production,
                RFC = conf.RFC,
                Strinct = conf.Strinct
            };
            if (conf.Provider == ServiceProvider.EnumServicePAC.Factorum)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.Factorum;
            else if (conf.Provider == ServiceProvider.EnumServicePAC.FiscoClic)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.FiscoClic;
            else if (conf.Provider == ServiceProvider.EnumServicePAC.SolucionFactible)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.SolucionFactible;
            else if (conf.Provider == ServiceProvider.EnumServicePAC.Interno)
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.Interno;
            else
                response.Provider = Certifica.Entities.ProviderService.ServiceProviderEnum.Ninguno;
            return response;
        }

        #endregion

        public static bool ValidaFechaEmisionMenor72H(DateTime fechaComprobante, int offset) {
            bool flag = true;
            try {
                DateTime currentPactDateTime = DateTime.Now;
                currentPactDateTime = currentPactDateTime.ToUniversalTime();
                DateTimeOffset dateTimeOffset = new DateTimeOffset(fechaComprobante, new TimeSpan(offset, 0, 0));
                fechaComprobante = dateTimeOffset.ToUniversalTime().DateTime;
                if (fechaComprobante > currentPactDateTime)
                    flag = false;
                else if (fechaComprobante < currentPactDateTime.AddHours(-72))
                    flag = false;
            } catch (Exception) {
                flag = true;
            }
            return flag;
        }
    }
}
