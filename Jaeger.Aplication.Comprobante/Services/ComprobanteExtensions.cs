﻿using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteExtensions {

        static ComprobanteExtensions() {
        }

        /// <summary>
        /// obtener tipo de comprobante fiscal
        /// </summary>
        private static CFDITipoComprobanteEnum GetTipo(string TipoDeComprobante) {
            if (TipoDeComprobante.ToUpper().StartsWith("I")) {
                return CFDITipoComprobanteEnum.Ingreso;
            } else if (TipoDeComprobante.ToUpper().StartsWith("E")) {
                return CFDITipoComprobanteEnum.Egreso;
            } else if (TipoDeComprobante.ToUpper().StartsWith("T")) {
                return CFDITipoComprobanteEnum.Traslado;
            } else if (TipoDeComprobante.ToUpper().StartsWith("N")) {
                return CFDITipoComprobanteEnum.Nomina;
            } else if (TipoDeComprobante.ToUpper().StartsWith("P")) {
                return CFDITipoComprobanteEnum.Pagos;
            }
            return CFDITipoComprobanteEnum.Ingreso;
        }

        /// <summary>
        /// sin conceptos
        /// </summary>
        public static ComprobanteFiscalDetailModel ConvertTo(SAT.CFDI.V33.Comprobante cfdi) {
            // objeto
            var c1 = CreateB(cfdi);
            // complementos
            if (!(cfdi.Complemento == null))
                c1 = ComprobanteExtensions.Complementos(ref cfdi, c1);
            return c1;
        }

        public static ComprobanteFiscalDetailModel ConvertTo(SAT.CFDI.V40.Comprobante cfdi) {
            // objeto
            var c1 = CreateB(cfdi);
            // complementos
            if (!(cfdi.Complemento == null))
                c1 = ComprobanteExtensions.Complementos(ref cfdi, c1);
            return c1;
        }

        #region version 3.3
        public static ComprobanteFiscalDetailModel Create(SAT.CFDI.V33.Comprobante cfdi) {
            if (cfdi == null) return null;
            var _comprobante = new ComprobanteFiscalDetailModel {
                Version = cfdi.Version,
                Folio = cfdi.Folio,
                Serie = cfdi.Serie,
                //response.SubTipoInt = (int)cfdi.SubTipo;
                Receptor = ComprobanteExtensions.Receptor(cfdi.Receptor),
                Emisor = ComprobanteExtensions.Emisor(cfdi.Emisor),
                SubTotal = cfdi.SubTotal,
                Total = cfdi.Total,
                NoCertificado = cfdi.NoCertificado,
                FechaEmision = cfdi.Fecha,
                ClaveMoneda = cfdi.Moneda,
                LugarExpedicion = cfdi.LugarExpedicion,
                ClaveUsoCFDI = cfdi.Receptor.UsoCFDI,
                TipoComprobanteText = cfdi.TipoDeComprobante
            };

            if (cfdi.TipoDeComprobante.ToUpper().StartsWith("I")) {
                _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Ingreso;
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("E")) {
                _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Egreso;
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("T")) {
                _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Traslado;
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("N")) {
                _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Nomina;
            } else if (cfdi.TipoDeComprobante.ToUpper().StartsWith("P")) {
                _comprobante.TipoComprobante = CFDITipoComprobanteEnum.Pagos;
            }

            // descuento
            if (cfdi.Descuento > 0) {
                _comprobante.Descuento = cfdi.Descuento;
            }

            // tipo de campbio
            if (cfdi.TipoCambioSpecified)
                _comprobante.TipoCambio = cfdi.TipoCambio;

            // forma de pago
            if (cfdi.FormaPagoSpecified)
                _comprobante.ClaveFormaPago = cfdi.FormaPago;

            // condiciones de pago
            if (cfdi.CondicionesDePago != null) {
                _comprobante.CondicionPago = cfdi.CondicionesDePago;
            }

            // metodo de pago
            if (cfdi.MetodoPagoSpecified)
                _comprobante.ClaveMetodoPago = cfdi.MetodoPago;

            // comprobantes relacionados
            if (!(cfdi.CfdiRelacionados == null)) {
                _comprobante.CfdiRelacionados = ComprobanteExtensions.CfdiRelacionados(cfdi.CfdiRelacionados);
            } else {
                _comprobante.CfdiRelacionados = null;
            }

            // totales de los impuestos
            _comprobante = ComprobanteExtensions.Impuestos(ref cfdi, _comprobante);
            _comprobante.Conceptos = Conceptos(cfdi.Conceptos);
            // complementos
            if (!(cfdi.Complemento == null))
                _comprobante = ComprobanteExtensions.Complementos(ref cfdi, _comprobante);

            return _comprobante;
        }

        private static ComprobanteFiscalDetailModel CreateB(SAT.CFDI.V33.Comprobante cfdi) {
            var _comprobante = new ComprobanteFiscalDetailModel {
                Version = cfdi.Version,
                Folio = cfdi.Folio,
                Serie = cfdi.Serie,
                Receptor = ComprobanteExtensions.Receptor(cfdi.Receptor),
                Emisor = ComprobanteExtensions.Emisor(cfdi.Emisor),
                SubTotal = cfdi.SubTotal,
                Total = cfdi.Total,
                NoCertificado = cfdi.NoCertificado,
                FechaEmision = cfdi.Fecha,
                ClaveMoneda = cfdi.Moneda,
                LugarExpedicion = cfdi.LugarExpedicion,
                ClaveUsoCFDI = cfdi.Receptor.UsoCFDI,
                TipoComprobanteText = cfdi.TipoDeComprobante
            };

            _comprobante.TipoComprobante = ComprobanteExtensions.GetTipo(cfdi.TipoDeComprobante);

            // descuento
            if (cfdi.Descuento > 0) {
                _comprobante.Descuento = cfdi.Descuento;
            }

            // tipo de campbio
            if (cfdi.TipoCambioSpecified)
                _comprobante.TipoCambio = cfdi.TipoCambio;

            // forma de pago
            if (cfdi.FormaPagoSpecified)
                _comprobante.ClaveFormaPago = cfdi.FormaPago;

            // condiciones de pago
            if (cfdi.CondicionesDePago != null) {
                _comprobante.CondicionPago = cfdi.CondicionesDePago;
            }

            // metodo de pago
            if (cfdi.MetodoPagoSpecified)
                _comprobante.ClaveMetodoPago = cfdi.MetodoPago;

            // comprobantes relacionados
            if (!(cfdi.CfdiRelacionados == null)) {
                _comprobante.CfdiRelacionados = ComprobanteExtensions.CfdiRelacionados(cfdi.CfdiRelacionados);
            } else {
                _comprobante.CfdiRelacionados = null;
            }

            // totales de los impuestos
            _comprobante = ComprobanteExtensions.Impuestos(ref cfdi, _comprobante);
            return _comprobante;
        }

        private static ComprobanteContribuyenteModel Receptor(SAT.CFDI.V33.ComprobanteReceptor receptor) {
            var response = new ComprobanteContribuyenteModel() {
                Nombre = receptor.Nombre,
                ClaveUsoCFDI = receptor.UsoCFDI,
                NumRegIdTrib = receptor.NumRegIdTrib,
                RFC = receptor.Rfc,
            };
            if (receptor.ResidenciaFiscalSpecified)
                response.ResidenciaFiscal = receptor.ResidenciaFiscal;
            return response;
        }

        private static ComprobanteContribuyenteModel Emisor(SAT.CFDI.V33.ComprobanteEmisor emisor) {
            var response = new ComprobanteContribuyenteModel() {
                RFC = emisor.Rfc,
                Nombre = emisor.Nombre,
                RegimenFiscal = emisor.RegimenFiscal
            };
            return response;
        }

        /// <summary>
        /// convertir array V33.ComprobanteConcepto a lista Cfd.Concepto
        /// </summary>
        public static BindingList<ComprobanteConceptoDetailModel> Conceptos(SAT.CFDI.V33.ComprobanteConcepto[] pConceptos) {
            var response = new BindingList<ComprobanteConceptoDetailModel>();
            foreach (var concepto in pConceptos) {
                var _concepto = new ComprobanteConceptoDetailModel {
                    Cantidad = concepto.Cantidad,
                    Descripcion = concepto.Descripcion,
                    ValorUnitario = concepto.ValorUnitario,
                    NoIdentificacion = concepto.NoIdentificacion,
                    Unidad = concepto.Unidad,
                    ClaveProdServ = concepto.ClaveProdServ,
                    ClaveUnidad = concepto.ClaveUnidad,
                    Importe = concepto.Importe
                };

                // impuestos
                if (!(concepto.Impuestos == null)) {
                    //impuestos trasladados
                    if (!(concepto.Impuestos.Traslados == null)) {
                        foreach (var t in concepto.Impuestos.Traslados) {
                            ComprobanteConceptoImpuesto itemT = new ComprobanteConceptoImpuesto();
                            itemT.Tipo = TipoImpuestoEnum.Traslado;

                            if (t.Impuesto == "002") {
                                // traslado IVA-002
                                itemT.Impuesto = ImpuestoEnum.IVA;
                            } else if (t.Impuesto == "003") {
                                //traslado IEPS
                                itemT.Impuesto = ImpuestoEnum.IEPS;
                            }

                            if (t.TipoFactor.ToLower().Contains("tasa")) {
                                itemT.TipoFactor = FactorEnum.Tasa;
                            } else if (t.TipoFactor.ToLower().Contains("cuota")) {
                                itemT.TipoFactor = FactorEnum.Cuota;
                            }

                            if (t.TasaOCuotaSpecified) {
                                itemT.TasaOCuota = t.TasaOCuota;
                            }

                            if (t.ImporteSpecified) {
                                itemT.Importe = t.Importe;
                            }

                            itemT.Base = t.Base;
                            _concepto.Impuestos.Add(itemT);
                        }
                    }

                    // impuestos retenidos
                    if (!(concepto.Impuestos.Retenciones == null)) {
                        foreach (var r in concepto.Impuestos.Retenciones) {
                            ComprobanteConceptoImpuesto itemR = new ComprobanteConceptoImpuesto();
                            itemR.Base = r.Base;
                            itemR.Importe = r.Importe;
                            itemR.Tipo = TipoImpuestoEnum.Retencion;
                            if (r.Impuesto == "002") {
                                itemR.Impuesto = ImpuestoEnum.IVA;
                            } else if (r.Impuesto == "003") {
                                itemR.Impuesto = ImpuestoEnum.IEPS;
                            } else if (r.Impuesto == "001") {
                                itemR.Impuesto = ImpuestoEnum.ISR;
                            }

                            if (r.TipoFactor.ToLower().Contains("tasa")) {
                                itemR.TipoFactor = FactorEnum.Tasa;
                            } else if (r.TipoFactor.ToLower().Contains("cuota")) {
                                itemR.TipoFactor = FactorEnum.Cuota;
                            } else if (r.TipoFactor.ToLower().Contains("exento")) {
                                itemR.TipoFactor = FactorEnum.Exento;
                            }
                            _concepto.Impuestos.Add(itemR);
                        }
                    }
                }

                if (!(concepto.CuentaPredial == null)) {
                    if (!(concepto.CuentaPredial.Numero == null)) {
                        _concepto.CtaPredial = concepto.CuentaPredial.Numero;
                    }
                }

                if (concepto.DescuentoSpecified) {
                    _concepto.Descuento = concepto.Descuento;
                } else {
                    _concepto.Descuento = 0;
                }

                // objeto concepto parte
                if (!(concepto.Parte == null)) {
                    foreach (var subItem in concepto.Parte) {
                        _concepto.Parte.Add(ConceptoParte(subItem));
                    }
                }

                // objeto informacion aduanera
                if (!(concepto.InformacionAduanera == null)) {
                    _concepto.InformacionAduanera = ConceptoInformacionAduanera(concepto.InformacionAduanera);
                }

                // agregamos el nuevo objeto a la lista
                response.Add(_concepto);
            }
            return response;
        }

        /// <summary>
        /// convertir V33.ComprobanteConceptoParte a Cfd.ConceptoParte
        /// </summary>
        public static ConceptoParte ConceptoParte(SAT.CFDI.V33.ComprobanteConceptoParte conceptoParte) {
            var response = new ConceptoParte();
            response.Cantidad = conceptoParte.Cantidad;
            response.ClaveProdServ = conceptoParte.ClaveProdServ;
            response.Descripcion = conceptoParte.Descripcion;
            response.NoIdentificacion = conceptoParte.NoIdentificacion;
            response.Unidad = conceptoParte.Unidad;

            if (conceptoParte.ValorUnitarioSpecified) {
                response.ValorUnitario = conceptoParte.ValorUnitario;
            }

            if (conceptoParte.ImporteSpecified) {
                response.Importe = conceptoParte.Importe;
            }

            if (!(conceptoParte.InformacionAduanera == null)) {
                response.InformacionAduanera = ConceptoParteInformacionAduanera(conceptoParte.InformacionAduanera);
            }
            return response;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public static BindingList<ComprobanteInformacionAduanera> ConceptoInformacionAduanera(SAT.CFDI.V33.ComprobanteConceptoInformacionAduanera[] infoAduanera) {
            if (!(infoAduanera == null)) {
                var response = new BindingList<ComprobanteInformacionAduanera>();
                foreach (var aduana in infoAduanera) {
                    var newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = aduana.NumeroPedimento;
                    response.Add(newItem);
                }
                return response;
            }
            return null;
        }

        /// <summary>
        /// convertir un array ComprobanteConceptoParteInformacionAduanera a lista de ComprobanteInformacionAduanera comun
        /// </summary>
        public static BindingList<ComprobanteInformacionAduanera> ConceptoParteInformacionAduanera(SAT.CFDI.V33.ComprobanteConceptoParteInformacionAduanera[] objetos) {
            if (!(objetos == null)) {
                var response = new BindingList<ComprobanteInformacionAduanera>();
                foreach (var item in objetos) {
                    ComprobanteInformacionAduanera newItem = new ComprobanteInformacionAduanera();
                    newItem.NumeroPedimento = item.NumeroPedimento;
                    response.Add(newItem);
                }
                return response;
            }
            return null;
        }

        private static ComprobanteCfdiRelacionadosModel CfdiRelacionados(SAT.CFDI.V33.ComprobanteCfdiRelacionados relacionados) {
            if (!(relacionados == null)) {
                var response = new ComprobanteCfdiRelacionadosModel();
                response.TipoRelacion.Clave = relacionados.TipoRelacion;
                foreach (var item in relacionados.CfdiRelacionado) {
                    var newItem = new ComprobanteCfdiRelacionadosCfdiRelacionadoModel {
                        IdDocumento = item.UUID.ToUpper()
                    };
                    response.CfdiRelacionado.Add(newItem);
                }
                return response;
            }
            return null;
        }

        private static ComprobanteFiscalDetailModel Impuestos(ref SAT.CFDI.V33.Comprobante cfdi, ComprobanteFiscalDetailModel response) {
            // impuestos
            if (!(cfdi.Impuestos == null)) {
                if (cfdi.Impuestos.TotalImpuestosRetenidosSpecified) {
                    if (!(cfdi.Impuestos.Retenciones == null)) {
                        foreach (var imp in cfdi.Impuestos.Retenciones) {
                            if (imp.Impuesto == "001") {
                                // retencion ISR
                                response.RetencionISR = response.RetencionISR + imp.Importe;
                            } else if (imp.Impuesto == "002") {
                                // retencion IVA
                                response.RetencionIVA = response.RetencionIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // retencion IEPS
                                response.RetencionIEPS = response.RetencionIEPS + imp.Importe;
                            }
                        }
                    }
                }

                if (cfdi.Impuestos.TotalImpuestosTrasladadosSpecified) {
                    if (!(cfdi.Impuestos.Traslados == null)) {
                        foreach (var imp in cfdi.Impuestos.Traslados) {
                            if (imp.Impuesto == "002") {
                                // traslado IVA
                                response.TrasladoIVA = response.TrasladoIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // traslado IEPS
                                response.TrasladoIEPS = response.TrasladoIEPS + imp.Importe;
                            }
                        }
                    }
                }
            }
            return response;
        }

        private static ComprobanteFiscalDetailModel Complementos(ref SAT.CFDI.V33.Comprobante cfdi, ComprobanteFiscalDetailModel response) {
            // complementos
            if (!(cfdi.Complemento == null)) {
                // complemento timbre fiscal
                if (!(cfdi.Complemento.TimbreFiscalDigital == null)) {
                    response.TimbreFiscal = ComplementoExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
                } else {
                    response.TimbreFiscal = null;
                }

                // complemento nomina 1.2
                if (!(cfdi.Complemento.Nomina12 == null)) {
                    response.Nomina12 = (ComplementoNominaDetailModel)SerializeComplementoNominaExtensions.Create(cfdi.Complemento.Nomina12);
                    response.Nomina = ComprobanteExtensions.Create(cfdi.Complemento.Nomina12);
                    if (response.Nomina != null) {
                        response.Nomina.Emisor.RFC = cfdi.Emisor.Rfc;
                        response.Nomina.Receptor.RFC = cfdi.Receptor.Rfc;
                        //response.ComplementoNomina.Receptor.Nombre = cfdi.Receptor.Nombre;
                        //response.ComplementoNomina.Receptor.PrimerApellido = "";
                        //response.ComplementoNomina.Receptor.SegundoApellido = "";
                        response.Nomina.Descuento = response.Descuento;
                        response.Receptor.ClaveUsoCFDI = cfdi.Receptor.UsoCFDI;

                        if (response.TimbreFiscal != null) {
                            response.Nomina.IdDocumento = response.TimbreFiscal.UUID;
                        }
                    }
                } else {
                    response.Nomina = null;
                }

                // complemento de pagos
                if (!(cfdi.Complemento.Pagos == null)) {
                    var c2 = Create(cfdi.Complemento.Pagos);
                    if (!(c2 == null)) {
                        response.RecepcionPago = c2;
                    }
                }

                // complemento vales de despensa
                //if (!(cfdi.Complemento.ValesDeDespensa == null)) {
                //    ComplementoValesDeDespensa c3 = this.Create(cfdi.Complemento.ValesDeDespensa);
                //    if (c3 != null) {
                //        if (response.Complementos == null) {
                //            response.Complementos = new Complementos();
                //        }
                //        response.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ValesDeDespensa, Data = c3.Json() });
                //    }

                //}

                // complemento leyendas fiscales
                //if (!(cfdi.Complemento.LeyendasFiscales == null)) {
                //    ComplementoLeyendasFiscales c4 = this.Create(cfdi.Complemento.LeyendasFiscales);
                //    if (c4 != null) {
                //        if (response.Complementos == null) {
                //            response.Complementos = new Complementos();
                //        }
                //        response.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.LeyendasFiscales, Data = c4.Json() });
                //    }
                //}

                // complemento Impuestos Locales
                //if (!(cfdi.Complemento.ImpuestosLocales == null)) {
                //    ComplementoImpuestosLocales c5 = this.Create(cfdi.Complemento.ImpuestosLocales);
                //    if (c5 != null) {
                //        if (response.Complementos == null) {
                //            response.Complementos = new Complementos();
                //        }
                //        response.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c5.Json() });
                //    }
                //}

                // complemento aerolineas
                //if (!(cfdi.Complemento.Aerolineas == null)) {
                //    ComplementoAerolineas c6 = this.Create(cfdi.Complemento.Aerolineas);
                //    if (c6 != null) {
                //        if (response.Complementos == null) {
                //        }
                //        response.Complementos.Objeto.Add(new Complemento { Nombre = EnumCfdiComplementos.ImpuestosLocales, Data = c6.Json() });
                //    }
                //}
            }
            return response;
        }

        #endregion

        #region version 4.0
        public static ComprobanteFiscalDetailModel Create(SAT.CFDI.V40.Comprobante cfdi) {
            var _comprobante = new ComprobanteFiscalDetailModel {
                Version = cfdi.Version,
                Folio = cfdi.Folio,
                Serie = cfdi.Serie,
                Receptor = ComprobanteExtensions.Receptor(cfdi.Receptor),
                Emisor = ComprobanteExtensions.Emisor(cfdi.Emisor),
                SubTotal = cfdi.SubTotal,
                Total = cfdi.Total,
                NoCertificado = cfdi.NoCertificado,
                FechaEmision = cfdi.Fecha,
                ClaveMoneda = cfdi.Moneda,
                LugarExpedicion = cfdi.LugarExpedicion,
                ClaveUsoCFDI = cfdi.Receptor.UsoCFDI,
                DomicilioFiscal = cfdi.Receptor.DomicilioFiscalReceptor,
                // identificar tipo de comprobante
                TipoComprobanteText = cfdi.TipoDeComprobante
            };

            _comprobante.TipoComprobante = ComprobanteExtensions.GetTipo(cfdi.TipoDeComprobante);

            if (cfdi.Receptor.ResidenciaFiscalSpecified)
                _comprobante.ResidenciaFiscal = cfdi.Receptor.ResidenciaFiscal;
            // descuento
            if (cfdi.Descuento > 0) {
                _comprobante.Descuento = cfdi.Descuento;
            }

            // tipo de campbio
            if (cfdi.TipoCambioSpecified)
                _comprobante.TipoCambio = cfdi.TipoCambio;

            // forma de pago
            if (cfdi.FormaPagoSpecified)
                _comprobante.ClaveFormaPago = cfdi.FormaPago;

            // condiciones de pago
            if (cfdi.CondicionesDePago != null) {
                _comprobante.CondicionPago = cfdi.CondicionesDePago;
            }

            // metodo de pago
            if (cfdi.MetodoPagoSpecified)
                _comprobante.ClaveMetodoPago = cfdi.MetodoPago;

            // comprobantes relacionados
            if (!(cfdi.CfdiRelacionados == null)) {
                _comprobante.CfdiRelacionados = ComprobanteExtensions.CfdiRelacionados(cfdi.CfdiRelacionados);
            } else {
                _comprobante.CfdiRelacionados = null;
            }

            // objeto de exportacion
            if (cfdi.Exportacion == "01")
                _comprobante.Exportacion = CFDIExportacionEnum.No_Aplica;
            else if (cfdi.Exportacion == "02")
                _comprobante.Exportacion = CFDIExportacionEnum.Definitiva;
            else if (cfdi.Exportacion == "03")
                _comprobante.Exportacion = CFDIExportacionEnum.Temporal;

            // informacion global
            _comprobante.InformacionGlobal = InformacionGlobal(cfdi.InformacionGlobal);

            // totales de los impuestos
            _comprobante = Impuestos(ref cfdi, _comprobante);

            _comprobante.Conceptos = Conceptos(cfdi.Conceptos);
            // complementos
            if (!(cfdi.Complemento == null))
                _comprobante = ComprobanteExtensions.Complementos(ref cfdi, _comprobante);

            // validacion
            //if (cfdi.Validation != null) {
            //    response.JValidacion = cfdi.Validation.Json();
            //    response.Result = cfdi.Validation.ValidoText;
            //    response.Estado = cfdi.Validation.ProofStatus;
            //    response.FechaVal = cfdi.Validation.FechaValidacion;
            //    response.FechaEstado = response.FechaVal;
            //}

            return _comprobante;
        }

        private static ComprobanteFiscalDetailModel CreateB(SAT.CFDI.V40.Comprobante cfdi) {
            var _comprobante = new ComprobanteFiscalDetailModel {
                Version = cfdi.Version,
                Folio = cfdi.Folio,
                Serie = cfdi.Serie,
                Receptor = ComprobanteExtensions.Receptor(cfdi.Receptor),
                Emisor = ComprobanteExtensions.Emisor(cfdi.Emisor),
                SubTotal = cfdi.SubTotal,
                Total = cfdi.Total,
                NoCertificado = cfdi.NoCertificado,
                FechaEmision = cfdi.Fecha,
                ClaveMoneda = cfdi.Moneda,
                LugarExpedicion = cfdi.LugarExpedicion,
                ClaveUsoCFDI = cfdi.Receptor.UsoCFDI,
                DomicilioFiscal = cfdi.Receptor.DomicilioFiscalReceptor,
                // identificar tipo de comprobante
                TipoComprobanteText = cfdi.TipoDeComprobante
            };

            _comprobante.TipoComprobante = ComprobanteExtensions.GetTipo(cfdi.TipoDeComprobante);

            if (cfdi.Receptor.ResidenciaFiscalSpecified)
                _comprobante.ResidenciaFiscal = cfdi.Receptor.ResidenciaFiscal;
            // descuento
            if (cfdi.Descuento > 0) {
                _comprobante.Descuento = cfdi.Descuento;
            }

            // tipo de campbio
            if (cfdi.TipoCambioSpecified)
                _comprobante.TipoCambio = cfdi.TipoCambio;

            // forma de pago
            if (cfdi.FormaPagoSpecified)
                _comprobante.ClaveFormaPago = cfdi.FormaPago;

            // condiciones de pago
            if (cfdi.CondicionesDePago != null) {
                _comprobante.CondicionPago = cfdi.CondicionesDePago;
            }

            // metodo de pago
            if (cfdi.MetodoPagoSpecified)
                _comprobante.ClaveMetodoPago = cfdi.MetodoPago;

            // comprobantes relacionados
            if (!(cfdi.CfdiRelacionados == null)) {
                _comprobante.CfdiRelacionados = ComprobanteExtensions.CfdiRelacionados(cfdi.CfdiRelacionados);
            } else {
                _comprobante.CfdiRelacionados = null;
            }

            // objeto de exportacion
            if (cfdi.Exportacion == "01")
                _comprobante.Exportacion = CFDIExportacionEnum.No_Aplica;
            else if (cfdi.Exportacion == "02")
                _comprobante.Exportacion = CFDIExportacionEnum.Definitiva;
            else if (cfdi.Exportacion == "03")
                _comprobante.Exportacion = CFDIExportacionEnum.Temporal;

            // informacion global
            _comprobante.InformacionGlobal = InformacionGlobal(cfdi.InformacionGlobal);

            // totales de los impuestos
            _comprobante = Impuestos(ref cfdi, _comprobante);

            _comprobante.Conceptos = Conceptos(cfdi.Conceptos);
            // complementos
            if (!(cfdi.Complemento == null))
                _comprobante = ComprobanteExtensions.Complementos(ref cfdi, _comprobante);

            // validacion
            //if (cfdi.Validation != null) {
            //    response.JValidacion = cfdi.Validation.Json();
            //    response.Result = cfdi.Validation.ValidoText;
            //    response.Estado = cfdi.Validation.ProofStatus;
            //    response.FechaVal = cfdi.Validation.FechaValidacion;
            //    response.FechaEstado = response.FechaVal;
            //}

            return _comprobante;
        }

        private static ComprobanteContribuyenteModel Emisor(SAT.CFDI.V40.ComprobanteEmisor emisor) {
            var _emisor = new ComprobanteContribuyenteModel() {
                Nombre = emisor.Nombre,
                RFC = emisor.Rfc,
                RegimenFiscal = emisor.RegimenFiscal
            };
            return _emisor;
        }

        private static ComprobanteContribuyenteModel Receptor(SAT.CFDI.V40.ComprobanteReceptor receptor) {
            var _receptor = new ComprobanteContribuyenteModel {
                RFC = receptor.Rfc,
                Nombre = receptor.Nombre,
                RegimenFiscal = receptor.RegimenFiscalReceptor,
                ResidenciaFiscal = receptor.ResidenciaFiscal,
                Activo = true,
                ClaveUsoCFDI = receptor.UsoCFDI,
                DomicilioFiscal = receptor.DomicilioFiscalReceptor,
                NumRegIdTrib = receptor.NumRegIdTrib
            };
            return _receptor;
        }

        private static ComprobanteCfdiRelacionadosModel CfdiRelacionados(SAT.CFDI.V40.ComprobanteCfdiRelacionados[] cfdiRelacionados) {
            if (cfdiRelacionados != null) {
                var _response = new ComprobanteCfdiRelacionadosModel();
                foreach (var item in cfdiRelacionados) {
                    _response.TipoRelacion = new ComprobanteCfdiRelacionadoRelacion { Clave = item.TipoRelacion };
                    _response.CfdiRelacionado = new BindingList<ComprobanteCfdiRelacionadosCfdiRelacionadoModel>();
                    foreach (var _item in item.CfdiRelacionado) {
                        _response.CfdiRelacionado.Add(new ComprobanteCfdiRelacionadosCfdiRelacionadoModel { IdDocumento = _item.UUID });
                    }
                }
            }

            return null;
        }

        public static BindingList<ComprobanteConceptoDetailModel> Conceptos(SAT.CFDI.V40.ComprobanteConcepto[] conceptos) {
            var _response = new BindingList<ComprobanteConceptoDetailModel>();
            foreach (var concepto in conceptos) {
                var _concepto = new ComprobanteConceptoDetailModel {
                    Cantidad = concepto.Cantidad,
                    Descripcion = concepto.Descripcion,
                    Importe = concepto.Importe,
                    ValorUnitario = concepto.ValorUnitario,
                    NoIdentificacion = concepto.NoIdentificacion,
                    Unidad = concepto.Unidad,
                    ClaveProdServ = concepto.ClaveProdServ,
                    ClaveUnidad = concepto.ClaveUnidad,
                    Activo = true,
                    Descuento = concepto.Descuento,
                    ObjetoImp = concepto.ObjetoImp
                };

                if (!(concepto.Impuestos == null)) {
                    //impuestos trasladados
                    if (!(concepto.Impuestos.Traslados == null)) {
                        foreach (var t in concepto.Impuestos.Traslados) {
                            ComprobanteConceptoImpuesto itemT = new ComprobanteConceptoImpuesto {
                                Tipo = TipoImpuestoEnum.Traslado
                            };

                            if (t.Impuesto == "002") {
                                // traslado IVA-002
                                itemT.Impuesto = ImpuestoEnum.IVA;
                            } else if (t.Impuesto == "003") {
                                //traslado IEPS
                                itemT.Impuesto = ImpuestoEnum.IEPS;
                            }

                            if (t.TipoFactor.ToLower().Contains("tasa")) {
                                itemT.TipoFactor = FactorEnum.Tasa;
                            } else if (t.TipoFactor.ToLower().Contains("cuota")) {
                                itemT.TipoFactor = FactorEnum.Cuota;
                            }

                            if (t.TasaOCuotaSpecified) {
                                itemT.TasaOCuota = t.TasaOCuota;
                            }

                            if (t.ImporteSpecified) {
                                itemT.Importe = t.Importe;
                            }

                            itemT.Base = t.Base;
                            _concepto.Impuestos.Add(itemT);
                        }
                    }

                    // impuestos retenidos
                    if (!(concepto.Impuestos.Retenciones == null)) {
                        foreach (var r in concepto.Impuestos.Retenciones) {
                            ComprobanteConceptoImpuesto itemR = new ComprobanteConceptoImpuesto {
                                Base = r.Base,
                                Importe = r.Importe,
                                Tipo = TipoImpuestoEnum.Retencion
                            };

                            if (r.Impuesto == "002") {
                                itemR.Impuesto = ImpuestoEnum.IVA;
                            } else if (r.Impuesto == "003") {
                                itemR.Impuesto = ImpuestoEnum.IEPS;
                            } else if (r.Impuesto == "001") {
                                itemR.Impuesto = ImpuestoEnum.ISR;
                            }

                            if (r.TipoFactor.ToLower().Contains("tasa")) {
                                itemR.TipoFactor = FactorEnum.Tasa;
                            } else if (r.TipoFactor.ToLower().Contains("cuota")) {
                                itemR.TipoFactor = FactorEnum.Cuota;
                            } else if (r.TipoFactor.ToLower().Contains("exento")) {
                                itemR.TipoFactor = FactorEnum.Exento;
                            }
                            _concepto.Impuestos.Add(itemR);
                        }
                    }
                }
                _response.Add(_concepto);
            }

            // impuestos
            return _response;
        }

        /// <summary>
        /// importes de los impuestos totales del comprobante
        /// </summary>
        public static ComprobanteFiscalDetailModel Impuestos(ref SAT.CFDI.V40.Comprobante cfdi, ComprobanteFiscalDetailModel comprobante) {
            // impuestos
            if (!(cfdi.Impuestos == null)) {
                if (cfdi.Impuestos.TotalImpuestosRetenidosSpecified) {
                    if (!(cfdi.Impuestos.Retenciones == null)) {
                        foreach (var imp in cfdi.Impuestos.Retenciones) {
                            if (imp.Impuesto == "001") {
                                // retencion ISR
                                comprobante.RetencionISR = comprobante.RetencionISR + imp.Importe;
                            } else if (imp.Impuesto == "002") {
                                // retencion IVA
                                comprobante.RetencionIVA = comprobante.RetencionIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // retencion IEPS
                                comprobante.RetencionIEPS = comprobante.RetencionIEPS + imp.Importe;
                            }
                        }
                    }
                }

                if (cfdi.Impuestos.TotalImpuestosTrasladadosSpecified) {
                    if (!(cfdi.Impuestos.Traslados == null)) {
                        foreach (var imp in cfdi.Impuestos.Traslados) {
                            if (imp.Impuesto == "002") {
                                // traslado IVA
                                comprobante.TrasladoIVA = comprobante.TrasladoIVA + imp.Importe;
                            } else if (imp.Impuesto == "003") {
                                // traslado IEPS
                                comprobante.TrasladoIEPS = comprobante.TrasladoIEPS + imp.Importe;
                            }
                        }
                    }
                }
            }
            return comprobante;
        }

        /// <summary>
        /// informacion del comprobante global
        /// </summary>
        public static ComprobanteInformacionGlobalDetailModel InformacionGlobal(SAT.CFDI.V40.ComprobanteInformacionGlobal informacionGlobal) {
            if (informacionGlobal != null) {
                var response = new ComprobanteInformacionGlobalDetailModel {
                    Activo = true,
                    Anio = informacionGlobal.Año,
                    ClaveMeses = informacionGlobal.Meses,
                    Periodicidad = informacionGlobal.Periodicidad
                };
                return response;
            }
            return null;
        }

        private static ComprobanteFiscalDetailModel Complementos(ref SAT.CFDI.V40.Comprobante cfdi, ComprobanteFiscalDetailModel comprobante) {
            if (cfdi.Complemento != null) {
                if (cfdi.Complemento.Nomina != null) {
                    comprobante.Nomina = ComprobanteExtensions.Create(cfdi.Complemento.Nomina);
                    comprobante.Nomina12 = (ComplementoNominaDetailModel)SerializeComplementoNominaExtensions.Create(cfdi.Complemento.Nomina);
                    comprobante.Nomina12.IdDocumento = comprobante.IdDocumento;
                }

                if (cfdi.Complemento.TimbreFiscalDigital != null) {
                    comprobante.TimbreFiscal = ComplementoExtensions.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital);
                }

                if (cfdi.Complemento.CartaPorte != null) {
                    var complemento = new ComplementoCartaPorteExtensions();
                    comprobante.CartaPorte = complemento.Create(cfdi.Complemento.CartaPorte);
                }

                if (cfdi.Complemento.Pagos != null) {
                    var complemento = new ComplementoPagosExtensions();
                    comprobante.RecepcionPago = Create(cfdi.Complemento.Pagos);
                }
            }
            return comprobante;
        }
        #endregion

        #region complemento de nomina12
        public static ComplementoNomina Create(SAT.CFDI.Complemento.Nomina.V12.Nomina nomina12) {
            if (nomina12 != null) {
                var _complemento = new ComplementoNomina {
                    // general
                    FechaFinalPago = nomina12.FechaFinalPago,
                    FechaInicialPago = nomina12.FechaInicialPago,
                    FechaPago = nomina12.FechaPago,
                    TipoNomina = nomina12.TipoNomina,
                    Version = nomina12.Version,
                    NumDiasPagados = nomina12.NumDiasPagados,
                    // emisor
                    Emisor = Nomina12(nomina12.Emisor),
                    // receptor
                    Receptor = Nomina12(nomina12.Receptor),
                    // percepciones
                    Percepciones = Nomina12(nomina12.Percepciones),
                    // deducciones
                    Deducciones = Nomina12(nomina12.Deducciones),
                    // otros pafos
                    OtrosPagos = Nomina12(nomina12.OtrosPagos),
                    // incapacidades
                    Incapacidades = Nomina12(nomina12.Incapacidades),
                    // totales
                    TotalDeducciones = nomina12.TotalDeducciones,
                    TotalOtrosPagos = nomina12.TotalOtrosPagos,
                    TotalPercepciones = nomina12.TotalPercepciones
                };

                return _complemento;
            }
            return null;
        }

        public static ComplementoNominaEmisor Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaEmisor emisor) {
            var _emisor = new ComplementoNominaEmisor {
                CURP = emisor.Curp,
                RegistroPatronal = emisor.RegistroPatronal,
                RFCPatronOrigen = emisor.RfcPatronOrigen
            };

            if (emisor.EntidadSNCF != null) {
                if (emisor.EntidadSNCF.MontoRecursoPropioSpecified) {
                    _emisor.EntidadSncf = new ComplementoNominaEmisorEntidadSncf {
                        MontoRecursoPropio = emisor.EntidadSNCF.MontoRecursoPropio,
                        OrigenRecurso = emisor.EntidadSNCF.OrigenRecurso
                    };
                }
            }

            return _emisor;
        }

        public static ComplementoNominaReceptor Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaReceptor receptor) {
            var _receptor = new ComplementoNominaReceptor {
                Antiguedad = receptor.Antigüedad,
                Banco = receptor.Banco,
                ClaveEntFed = receptor.ClaveEntFed,
                CuentaBancaria = receptor.CuentaBancaria,
                CURP = receptor.Curp,
                Departamento = receptor.Departamento,
                FechaInicioRelLaboral = receptor.FechaInicioRelLaboral,
                NumEmpleado = receptor.NumEmpleado,
                NumSeguridadSocial = receptor.NumSeguridadSocial,
                PeriodicidadPago = receptor.PeriodicidadPago,
                Puesto = receptor.Puesto,
                RiesgoPuesto = receptor.RiesgoPuesto,
                SalarioBaseCotApor = receptor.SalarioBaseCotApor,
                SalarioDiarioIntegrado = receptor.SalarioDiarioIntegrado,
                TipoContrato = receptor.TipoContrato,
                TipoJornada = receptor.TipoJornada,
                TipoRegimen = receptor.TipoRegimen,
            };

            if (receptor.SindicalizadoSpecified) {
                _receptor.Sindicalizado = receptor.Sindicalizado == SAT.CFDI.Complemento.Nomina.V12.NominaReceptorSindicalizado.Sí ? true : false;
            }

            return _receptor;
        }

        public static ComplementoNominaPercepciones Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones percepciones) {
            var _percepciones = new ComplementoNominaPercepciones {
                Percepcion = new BindingList<ComplementoNominaPercepcion>()
            };

            foreach (var item in percepciones.Percepcion) {
                var _percepcion = new ComplementoNominaPercepcion {
                    Clave = item.Clave,
                    Concepto = item.Concepto,
                    TipoPercepcion = item.TipoPercepcion,
                    ImporteExento = item.ImporteExento,
                    ImporteGravado = item.ImporteGravado
                };

                if (item.AccionesOTitulos != null) {
                    _percepcion.AccionesOTitulos = new ComplementoNominaPercepcionAccionesOTitulos {
                        PrecioAlOtorgarse = item.AccionesOTitulos.PrecioAlOtorgarse,
                        ValorMercado = item.AccionesOTitulos.ValorMercado
                    };
                }

                if (item.HorasExtra != null) {
                    _percepcion.HorasExtra = new BindingList<ComplementoNominaPercepcionHorasExtra>();
                    foreach (var itemHorasExtra in item.HorasExtra) {
                        var newHorasExtra = new ComplementoNominaPercepcionHorasExtra {
                            Dias = itemHorasExtra.Dias,
                            HorasExtra = itemHorasExtra.HorasExtra,
                            ImportePagado = itemHorasExtra.ImportePagado,
                            TipoHoras = itemHorasExtra.TipoHoras
                        };
                        _percepcion.HorasExtra.Add(newHorasExtra);
                    }
                }
                _percepciones.Percepcion.Add(_percepcion);
            }

            if (percepciones.TotalJubilacionPensionRetiroSpecified) {
                _percepciones.TotalSeparacionIndemnizacion = percepciones.TotalSeparacionIndemnizacion;
            }

            if (percepciones.TotalSeparacionIndemnizacionSpecified) {
                _percepciones.TotalJubilacionPensionRetiro = percepciones.TotalJubilacionPensionRetiro;
            }
            _percepciones.TotalSueldos = percepciones.TotalSueldos;
            _percepciones.TotalExento = percepciones.TotalExento;
            _percepciones.TotalGravado = percepciones.TotalGravado;
            return _percepciones;
        }

        public static ComplementoNominaDeducciones Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaDeducciones deducciones) {
            if (deducciones != null) {
                var _complepemento = new ComplementoNominaDeducciones {
                    Deduccion = new BindingList<ComplementoNominaDeduccion>()
                };

                foreach (var item in deducciones.Deduccion) {
                    var _percepcion = new ComplementoNominaDeduccion();
                    _percepcion.Clave = item.Clave;
                    _percepcion.TipoDeduccion = item.TipoDeduccion;
                    _percepcion.Concepto = item.Concepto;
                    _percepcion.Importe = item.Importe;
                    _complepemento.Deduccion.Add(_percepcion);
                }
                _complepemento.TotalImpuestosRetenidos = deducciones.TotalImpuestosRetenidos;
                _complepemento.TotalOtrasDeducciones = deducciones.TotalOtrasDeducciones;
                return _complepemento;
            }
            return null;
        }

        public static BindingList<ComplementoNominaOtroPago> Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaOtroPago[] otroPagos) {
            if (otroPagos != null) {
                var _otrosPagos = new BindingList<ComplementoNominaOtroPago>();
                foreach (var item in otroPagos) {
                    var _otroPago = new ComplementoNominaOtroPago {
                        Clave = item.Clave,
                        Concepto = item.Concepto,
                        Importe = item.Importe,
                        TipoOtroPago = item.TipoOtroPago
                    };

                    if (item.SubsidioAlEmpleo != null) {
                        if (item.SubsidioAlEmpleo.SubsidioCausado > 0) {
                            _otroPago.SubsidioAlEmpleo = Nomina12(item.SubsidioAlEmpleo);
                        }
                    }

                    if (item.CompensacionSaldosAFavor != null) {
                        _otroPago.CompensacionSaldosAFavor = new ComplementoNominaOtroPagoCompensacionSaldosAFavor {
                            Anio = item.CompensacionSaldosAFavor.Año,
                            RemanenteSalFav = item.CompensacionSaldosAFavor.RemanenteSalFav,
                            SaldoAFavor = item.CompensacionSaldosAFavor.SaldoAFavor
                        };
                    }
                    _otrosPagos.Add(_otroPago);
                }
                return _otrosPagos;
            }
            return null;
        }

        public static ComplementoNominaOtroPagoSubsidioAlEmpleo Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoSubsidioAlEmpleo subsidioAlEmpleo) {
            if (subsidioAlEmpleo != null) {
                var _subsidioAlEmpleo = new ComplementoNominaOtroPagoSubsidioAlEmpleo {
                    SubsidioCausado = subsidioAlEmpleo.SubsidioCausado
                };
                return _subsidioAlEmpleo;
            }
            return null;
        }

        public static BindingList<ComplementoNominaIncapacidad> Nomina12(SAT.CFDI.Complemento.Nomina.V12.NominaIncapacidad[] incapacidades) {
            if (incapacidades != null) {
                var _incapaciades = new BindingList<ComplementoNominaIncapacidad>();
                foreach (var item in incapacidades) {
                    var _incapacidad = new ComplementoNominaIncapacidad {
                        DiasIncapacidad = item.DiasIncapacidad,
                        ImporteMonetario = item.ImporteMonetario,
                        TipoIncapacidad = item.TipoIncapacidad
                    };
                    _incapaciades.Add(_incapacidad);
                }
                return _incapaciades;
            }
            return null;
        }
        #endregion

        #region complemento TimbreFiscal V11
        //public ComplementoTimbreFiscal TimbreFiscal(CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbre) {
        //    var response = new ComplementoTimbreFiscal {
        //        UUID = timbre.UUID.ToUpper(),
        //        Version = timbre.Version,
        //        FechaTimbrado = timbre.FechaTimbrado,
        //        Leyenda = timbre.Leyenda,
        //        NoCertificadoSAT = timbre.NoCertificadoSAT,
        //        RFCProvCertif = timbre.RfcProvCertif,
        //        SelloCFD = timbre.SelloCFD,
        //        SelloSAT = timbre.SelloSAT
        //    };
        //    return response;
        //}
        #endregion

        #region complemento pagos v10

        /// <summary>
        /// convertir complemento de pagos version 10 a ComplementoPagos10 comun
        /// </summary>
        public static BindingList<ComplementoPagoDetailModel> Create(SAT.CFDI.Complemento.Pagos.V10.Pagos objeto) {
            var response = new BindingList<ComplementoPagoDetailModel>();
            foreach (var item in objeto.Pago) {
                var _response = new ComplementoPagoDetailModel {
                    Activo = true,
                    CadPago = item.CadPago,
                    CertPago = item.CertPagoB64,
                    CtaBeneficiario = item.CtaBeneficiario,
                    CtaOrdenante = item.CtaOrdenante,
                    FechaPago = item.FechaPago,
                    FormaDePagoP = item.FormaDePagoP,
                    MonedaP = item.MonedaP,
                    Monto = item.Monto,
                    NomBancoOrdExt = item.NomBancoOrdExt,
                    NumOperacion = item.NumOperacion,
                    RfcEmisorCtaBen = item.RfcEmisorCtaBen,
                    RfcEmisorCtaOrd = item.RfcEmisorCtaOrd,
                    SelloPago = item.SelloBase64,
                    TipoCadPago = item.TipoCadPago,
                    TipoCambioP = item.TipoCambioP,
                    Version = objeto.Version
                };

                _response.DoctoRelacionados = Pagos10(item.DoctoRelacionado);
                response.Add(_response);
            }
            return response;
        }

        /// <summary>
        /// convertir un array de Pagos10.PagosPago[] al objeto comun
        /// </summary>
        //public BindingList<ComplementoPagosPago> Pagos10(CFDI.Complemento.Pagos.V10.PagosPago[] objetos) {
        //    BindingList<ComplementoPagosPago> newItems = new BindingList<ComplementoPagosPago>();
        //    foreach (var item in objetos) {
        //        var newItem = new ComplementoPagosPago();
        //        newItem.CadPago = item.CadPago;
        //        newItem.CtaBeneficiario = item.CtaBeneficiario;
        //        newItem.CtaOrdenante = item.CtaOrdenante;
        //        newItem.FechaPago = item.FechaPago;
        //        newItem.FormaDePagoP = new ComplementoPagoFormaPago { Clave = item.FormaDePagoP };
        //        newItem.MonedaP = item.MonedaP;
        //        newItem.Monto = item.Monto;
        //        newItem.NomBancoOrdExt = item.NomBancoOrdExt;
        //        newItem.NumOperacion = item.NumOperacion;
        //        newItem.RfcEmisorCtaBen = item.RfcEmisorCtaBen;
        //        newItem.RfcEmisorCtaOrd = item.RfcEmisorCtaOrd;

        //        newItem.TipoCadPago = null;
        //        if (item.TipoCadPagoSpecified) {
        //            newItem.TipoCadPago = item.TipoCadPago;
        //        }

        //        if (item.TipoCambioPSpecified) {
        //            newItem.TipoCambioP = item.TipoCambioP;
        //        }

        //        newItem.CertPago = null;
        //        if (item.CertPago != null) {
        //            newItem.CertPago = Convert.ToBase64String(item.CertPago);
        //        }

        //        newItem.SelloPago = null;
        //        if (item.SelloPago != null) {
        //            newItem.SelloPago = Convert.ToBase64String(item.SelloPago);
        //        }

        //        newItem.DoctoRelacionado = this.Pagos10(item.DoctoRelacionado);
        //        newItem.Impuestos = this.Pagos10(item.Impuestos);
        //        newItems.Add(newItem);
        //    }
        //    return newItems;
        //}

        public static BindingList<PagosPagoDoctoRelacionadoDetailModel> Pagos10(SAT.CFDI.Complemento.Pagos.V10.PagosPagoDoctoRelacionado[] objetos) {
            if (objetos != null) {
                var newItems = new BindingList<PagosPagoDoctoRelacionadoDetailModel>();
                foreach (var item in objetos) {
                    var newItem = new PagosPagoDoctoRelacionadoDetailModel {
                        Folio = item.Folio,
                        IdDocumento = item.IdDocumento.ToUpper(),
                        //newItem.MetodoPago = item.MetodoDePagoDR;
                        MonedaDR = item.MonedaDR
                    };
                    if (item.NumParcialidad != null) newItem.NumParcialidad = int.Parse(item.NumParcialidad);
                    newItem.Serie = item.Serie;
                    if (item.ImpPagadoSpecified) {
                        newItem.ImpPagado = item.ImpPagado;
                    }
                    if (item.ImpSaldoAntSpecified) {
                        newItem.ImpSaldoAnt = item.ImpSaldoAnt;
                    }
                    if (item.ImpSaldoInsolutoSpecified) {
                        newItem.ImpSaldoInsoluto = item.ImpSaldoInsoluto;
                    }
                    if (item.TipoCambioDRSpecified) {
                        newItem.EquivalenciaDR = item.TipoCambioDR;
                    }

                    newItems.Add(newItem);
                }
                return newItems;
            }
            return null;
        }

        //public BindingList<ComplementoPagoImpuestos> Pagos10(CFDI.Complemento.Pagos.V10.PagosPagoImpuestos[] objetos) {
        //    if (!(objetos == null)) {
        //        BindingList<ComplementoPagoImpuestos> newItems = new BindingList<ComplementoPagoImpuestos>();
        //        foreach (var item in objetos) {
        //            var newItem = new ComplementoPagoImpuestos();
        //            newItem.Retenciones = this.Pagos10(item.Retenciones);
        //            newItem.Traslados = this.Pagos10(item.Traslados);
        //            if (item.TotalImpuestosRetenidosSpecified) {
        //                newItem.TotalImpuestosRetenidos = item.TotalImpuestosRetenidos;
        //            }
        //            if (item.TotalImpuestosTrasladadosSpecified) {
        //                newItem.TotalImpuestosTrasladados = item.TotalImpuestosTrasladados;
        //            }
        //            newItems.Add(newItem);
        //        }
        //        return newItems;
        //    }
        //    return null;
        //}

        //public BindingList<ComplementoPagoImpuestosRetencion> Pagos10(CFDI.Complemento.Pagos.V10.PagosPagoImpuestosRetencion[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoPagoImpuestosRetencion> newItems = new BindingList<ComplementoPagoImpuestosRetencion>();
        //        foreach (var item in objetos) {
        //            var newItem = new ComplementoPagoImpuestosRetencion();
        //            newItem.Impuesto = item.Impuesto;
        //            newItem.Importe = item.Importe;
        //            newItems.Add(newItem);
        //        }
        //        return newItems;
        //    }
        //    return null;
        //}

        //public BindingList<ComplementoPagoImpuestosTraslado> Pagos10(CFDI.Complemento.Pagos.V10.PagosPagoImpuestosTraslado[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoPagoImpuestosTraslado> newItems = new BindingList<ComplementoPagoImpuestosTraslado>();
        //        foreach (var item in objetos) {
        //            var newItem = new ComplementoPagoImpuestosTraslado();
        //            newItem.Importe = item.Importe;
        //            newItem.Impuesto = item.Impuesto;
        //            newItem.TasaOCuota = item.TasaOCuota;
        //            newItem.TipoFactor = item.TipoFactor;
        //            newItems.Add(newItem);
        //        }
        //        return newItems;
        //    }
        //    return null;
        //}

        #endregion

        #region complemento pagos v20 convertido a objeto ComplementoPagoDetailModel
        public static BindingList<ComplementoPagoDetailModel> Create(SAT.CFDI.Complemento.Pagos.V20.Pagos pagos) {
            var _response = new BindingList<ComplementoPagoDetailModel>();
            foreach (var pago in pagos.Pago) {
                var response = new ComplementoPagoDetailModel {
                    Version = pagos.Version,
                    Activo = true,
                    CadPago = pago.CadPago,
                    CertPago = pago.CertPagoB64,
                    CtaBeneficiario = pago.CtaBeneficiario,
                    CtaOrdenante = pago.CtaOrdenante,
                    FechaPago = pago.FechaPago,
                    FormaDePagoP = pago.FormaDePagoP,
                    MonedaP = pago.MonedaP,
                    Monto = pago.Monto,
                    NomBancoOrdExt = pago.NomBancoOrdExt,
                    NumOperacion = pago.NumOperacion,
                    RfcEmisorCtaBen = pago.RfcEmisorCtaBen,
                    RfcEmisorCtaOrd = pago.RfcEmisorCtaOrd,
                    SelloPago = pago.SelloBase64,
                    TipoCadPago = pago.TipoCadPago,
                    TipoCambioP = pago.TipoCambioP
                };
                response.DoctoRelacionados = Pagos20(pago.DoctoRelacionado);
                _response.Add(response);
            }
            return _response;
        }

        public static BindingList<PagosPagoDoctoRelacionadoDetailModel> Pagos20(SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionado[] doctosRelacionados) {
            if (doctosRelacionados != null) {
                var _doctoRelacionado = new BindingList<PagosPagoDoctoRelacionadoDetailModel>();
                foreach (var docto in doctosRelacionados) {
                    var _docto = new PagosPagoDoctoRelacionadoDetailModel {
                        Folio = docto.Folio,
                        Serie = docto.Serie,
                        IdDocumento = docto.IdDocumento.ToUpper(),
                        Activo = true,
                        EquivalenciaDR = docto.EquivalenciaDR,
                        NumParcialidad = int.Parse(docto.NumParcialidad),
                        ImpPagado = docto.ImpPagado,
                        ImpSaldoAnt = docto.ImpSaldoAnt,
                        ImpSaldoInsoluto = docto.ImpSaldoInsoluto,
                        ObjetoImpDR = docto.ObjetoImpDR,
                        MonedaDR = docto.MonedaDR
                    };

                    if (docto.ImpuestosDR != null) {
                        if (docto.ImpuestosDR.RetencionesDR != null)
                            _docto.Retenciones = Pagos20(docto.ImpuestosDR.RetencionesDR);
                        if (docto.ImpuestosDR.TrasladosDR != null) {
                            _docto.Traslados = Pagos20(docto.ImpuestosDR.TrasladosDR);
                        }
                    }

                    _doctoRelacionado.Add(_docto);
                }
                return _doctoRelacionado;
            }
            return null;
        }

        public static BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR> Pagos20(SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionadoImpuestosDRRetencionDR[] retencionesDR) {
            if (retencionesDR != null) {
                var _retenciones = new BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR>();
                foreach (var item in retencionesDR) {
                    var _retencion = new PagosPagoDoctoRelacionadoImpuestosDRRetencionDR {
                        Base = item.BaseDR,
                        Importe = item.ImporteDR,
                        TasaOCuota = item.TasaOCuotaDR
                    };

                    if (item.ImpuestoDR == "001")
                        _retencion.Impuesto = ImpuestoEnum.ISR;
                    else if (item.ImpuestoDR == "002")
                        _retencion.Impuesto = ImpuestoEnum.IVA;
                    else if (item.ImpuestoDR == "003")
                        _retencion.Impuesto = ImpuestoEnum.IEPS;

                    _retenciones.Add(_retencion);
                }
                return _retenciones;
            }

            return null;
        }

        public static BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR> Pagos20(SAT.CFDI.Complemento.Pagos.V20.PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR[] trasladosDR) {
            if (trasladosDR != null) {
                var _traslados = new BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR>();
                foreach (var item in trasladosDR) {
                    var _traslado = new PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR() {
                        Base = item.BaseDR,
                        Importe = item.ImporteDR,
                        TasaOCuota = item.TasaOCuotaDR
                    };

                    if (item.ImpuestoDR == "001")
                        _traslado.Impuesto = ImpuestoEnum.ISR;
                    else if (item.ImpuestoDR == "002")
                        _traslado.Impuesto = ImpuestoEnum.IVA;
                    else if (item.ImpuestoDR == "003")
                        _traslado.Impuesto = ImpuestoEnum.IEPS;
                    _traslados.Add(_traslado);
                }
                return _traslados;
            }
            return null;
        }
        #endregion

        #region complemento vales de despensa v10

        //public ComplementoValesDeDespensa Create(CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensa objeto) {
        //    if (objeto != null) {
        //        ComplementoValesDeDespensa nuevo = new ComplementoValesDeDespensa();
        //        nuevo.Version = objeto.version;
        //        nuevo.RegistroPatronal = objeto.registroPatronal;
        //        nuevo.TipoOperacion = objeto.tipoOperacion;
        //        nuevo.Total = objeto.total;
        //        nuevo.Conceptos = this.ValesDeDespensa10(objeto.Conceptos);
        //    }
        //    return null;
        //}

        //private BindingList<ComplementoValesDeDespensaConcepto> ValesDeDespensa10(CFDI.Complemento.ValesDeDespensa.V10.ValesDeDespensaConcepto[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoValesDeDespensaConcepto> lista = new BindingList<ComplementoValesDeDespensaConcepto>();
        //        foreach (var item in objetos) {
        //            ComplementoValesDeDespensaConcepto nuevo = new ComplementoValesDeDespensaConcepto();
        //            nuevo.CURP = item.curp;
        //            nuevo.Fecha = item.fecha;
        //            nuevo.Identificador = item.identificador;
        //            nuevo.Importe = item.importe;
        //            nuevo.Nombre = item.nombre;
        //            nuevo.NumSeguridadSocial = item.numSeguridadSocial;
        //            nuevo.RFC = item.rfc;
        //            lista.Add(nuevo);
        //        }
        //        return lista;
        //    }
        //    return null;
        //}

        #endregion

        #region complemento LeyendasFiscales

        //public ComplementoLeyendasFiscales Create(CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscales objeto) {
        //    if (objeto != null) {
        //        ComplementoLeyendasFiscales nuevo = new ComplementoLeyendasFiscales();
        //        nuevo.Version = objeto.version;
        //        nuevo.Leyenda = this.LeyendasFiscales(objeto.Leyenda);
        //    }
        //    return null;
        //}

        //private BindingList<ComplementoLeyendasFiscalesLeyenda> LeyendasFiscales(Certifica.CFDI.Complemento.LeyendasFiscales.V10.LeyendasFiscalesLeyenda[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoLeyendasFiscalesLeyenda> lista = new BindingList<ComplementoLeyendasFiscalesLeyenda>();
        //        foreach (var item in objetos) {
        //            ComplementoLeyendasFiscalesLeyenda nuevo = new ComplementoLeyendasFiscalesLeyenda();
        //            nuevo.DisposicionFiscal = item.disposicionFiscal;
        //            nuevo.Norma = item.norma;
        //            nuevo.TextoLeyenda = item.textoLeyenda;
        //            lista.Add(nuevo);
        //        }
        //        return lista;
        //    }
        //    return null;
        //}

        #endregion

        #region complemento impuestos locales v10

        //public ComplementoImpuestosLocales Create(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocales objeto) {
        //    if (objeto != null) {
        //        ComplementoImpuestosLocales nuevo = new ComplementoImpuestosLocales();
        //        nuevo.Version = objeto.version;
        //        nuevo.TotaldeRetenciones = objeto.TotaldeRetenciones;
        //        nuevo.TotaldeTraslados = objeto.TotaldeTraslados;
        //        nuevo.RetencionesLocales = this.ImpuestosLocales10(objeto.RetencionesLocales);
        //        nuevo.TrasladosLocales = this.ImpuestosLocales10(objeto.TrasladosLocales);
        //        return nuevo;
        //    }
        //    return null;
        //}

        //private BindingList<ComplementoImpuestosLocalesTrasladosLocales> ImpuestosLocales10(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesTrasladosLocales[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoImpuestosLocalesTrasladosLocales> lista = new BindingList<ComplementoImpuestosLocalesTrasladosLocales>();
        //        foreach (var item in objetos) {
        //            ComplementoImpuestosLocalesTrasladosLocales nuevo = new ComplementoImpuestosLocalesTrasladosLocales();
        //            nuevo.ImpLocTrasladado = item.ImpLocTrasladado;
        //            nuevo.Importe = item.Importe;
        //            nuevo.TasadeTraslado = item.TasadeTraslado;
        //            lista.Add(nuevo);
        //        }
        //        return lista;
        //    }
        //    return null;
        //}

        //private BindingList<ComplementoImpuestosLocalesRetencionesLocales> ImpuestosLocales10(CFDI.Complemento.ImpuestosLocales.V10.ImpuestosLocalesRetencionesLocales[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoImpuestosLocalesRetencionesLocales> lista = new BindingList<ComplementoImpuestosLocalesRetencionesLocales>();
        //        foreach (var item in objetos) {
        //            ComplementoImpuestosLocalesRetencionesLocales nuevo = new ComplementoImpuestosLocalesRetencionesLocales();
        //            nuevo.ImpLocRetenido = item.ImpLocRetenido;
        //            nuevo.Importe = item.Importe;
        //            nuevo.TasadeRetencion = item.TasadeRetencion;
        //            lista.Add(nuevo);
        //        }
        //        return lista;
        //    }
        //    return null;
        //}

        #endregion

        #region complemento aerolineas v10

        //public ComplementoAerolineas Create(CFDI.Complemento.Aerolineas.V10.Aerolineas objeto) {
        //    if (objeto != null) {
        //        ComplementoAerolineas nuevo = new ComplementoAerolineas();
        //        nuevo.Version = objeto.Version;
        //        nuevo.TUA = objeto.TUA;
        //        nuevo.OtrosCargos = this.Aerolineas10(objeto.OtrosCargos);
        //        return nuevo;
        //    }
        //    return null;
        //}

        //private ComplementoAerolineasOtrosCargos Aerolineas10(CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargos objeto) {
        //    if (objeto != null) {
        //        ComplementoAerolineasOtrosCargos nuevo = new ComplementoAerolineasOtrosCargos();
        //        nuevo.TotalCargos = objeto.TotalCargos;
        //        nuevo.Cargo = this.Aerolineas10(objeto.Cargo);
        //        return nuevo;
        //    }
        //    return null;
        //}

        //private BindingList<ComplementoAerolineasOtrosCargosCargo> Aerolineas10(CFDI.Complemento.Aerolineas.V10.AerolineasOtrosCargosCargo[] objetos) {
        //    if (objetos != null) {
        //        BindingList<ComplementoAerolineasOtrosCargosCargo> lista = new BindingList<ComplementoAerolineasOtrosCargosCargo>();
        //        foreach (var item in objetos) {
        //            ComplementoAerolineasOtrosCargosCargo nuevo = new ComplementoAerolineasOtrosCargosCargo();
        //            nuevo.CodigoCargo = item.CodigoCargo;
        //            nuevo.Importe = item.Importe;
        //        }
        //        return lista;
        //    }
        //    return null;
        //}

        #endregion
    }
}
