﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Mappers;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Builder;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobantesFiscalesService : ComprobanteFiscalService, IComprobantesFiscalesService {
        #region declaraciones
        protected CFDISubTipoEnum subTipo;
        protected IComprobantesFiscalesSearchService searchService;
        protected ISqlContribuyenteRepository contribuyenteRepository;
        protected Jaeger.Aplication.Comprobante.Printer.PrinterService printer;
        #endregion

        public ComprobantesFiscalesService() : base() {
            this.contribuyenteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.searchService = new ComprobanteFiscalSearchService();
            this.printer = new Printer.PrinterService();
        }

        public ComprobantesFiscalesService(CFDISubTipoEnum subTipo) : base() {
            this.subTipo = subTipo;
            this.contribuyenteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.searchService = new ComprobanteFiscalSearchService();
            this.printer = new Printer.PrinterService();
        }

        public CFDISubTipoEnum GetSubTipo {
            get { return this.subTipo; }
            set { this.subTipo = value; }
        }

        public ComprobanteFiscalPrinter Printer(IComprobanteSingle model) {
            var d = this.GetComprobante(model.Id);
            if (ValidacionService.UUID(d.IdDocumento)) {
                return this.printer.Printer(d.XML);
            } else {
                return this.printer.Printer(d);
            }
        }

        public ComprobanteFiscalPrinter Printer(ComprobanteFiscalDetailSingleModel model) {
            var d = this.GetComprobante(model.Id);
            if (ValidacionService.UUID(d.IdDocumento)) {
                return this.printer.Printer(d.XML);
            } else {
                return this.printer.Printer(d);
            }
        }

        public ComprobanteFiscalPrinter ToPrinter(ComprobanteFiscalDetailSingleModel model) {
            var d = this.GetComprobante(model.Id);
            if (ValidacionService.UUID(d.IdDocumento)) {
                return this.printer.Printer(d.XML);
            } else {
                return this.printer.Printer(d);
            }
        }

        public string ToHtml(IComprobanteSingle model) {
            var d = this.GetComprobante(model.Id);
            if (ValidacionService.UUID(d.IdDocumento)) {
                if (d.Version == "3.3") {
                    var x1 = SAT.CFDI.V33.Comprobante.LoadXml(d.XML);
                    return this.pdf.Procesar(x1, d.KeyName());
                } else if (d.Version == "4.0") {
                    var x2 = SAT.CFDI.V40.Comprobante.LoadXml(d.XML);
                    return this.pdf.Procesar(x2, d.KeyName());
                }
            }
            return string.Empty;
        }

        public string ToHtml(ComprobanteFiscalDetailSingleModel model) {
            var d = this.GetComprobante(model.Id);
            if (ValidacionService.UUID(d.IdDocumento)) {
                if (d.Version == "3.3") {
                    var x1 = SAT.CFDI.V33.Comprobante.LoadXml(d.XML);
                    return this.pdf.Procesar(x1, d.KeyName());
                } else if (d.Version == "4.0") {
                    var x2 = SAT.CFDI.V40.Comprobante.LoadXml(d.XML);
                    return this.pdf.Procesar(x2, d.KeyName());
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// obtener resumen de comprobantes fiscales para tabla dinamica
        /// </summary>
        /// <param name="anio">año</param>
        /// <param name="mes">mes</param>
        public BindingList<EstadoCuentaSingleView> GetResumen(int year, int month = 0) {
            return new BindingList<EstadoCuentaSingleView>(this._ComprobanteFiscalRepository.GetResumen(this.subTipo, year, month).ToList());
        }

        public System.Data.DataTable GetResumen(int year, string[] estado, string rfc = "") {
            return this._ComprobanteFiscalRepository.GetResumen(this.subTipo, year, estado, rfc);
        }

        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        public BindingList<ComprobanteConceptoDetailModel> GetConceptos(int index) {
            return new BindingList<ComprobanteConceptoDetailModel>(this._ComprobanteFiscalRepository.GetConceptos(index).ToList());
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            if (typeof(T1) == typeof(ComprobanteContribuyenteModel)) {
                return this.contribuyenteRepository.GetList<T1>(conditionales);
            }
            return base.GetList<T1>(conditionales);
        }

        public void Dispose() {
            GC.SuppressFinalize(this);
        }

        #region metodos estaticos
        public static IComprobanteQueryBuilder Query() {
            return new ComprobanteQueryBuilder();
        }
        #endregion

        #region crear
        public bool CrearTablaRecepcionPago() {
            return this._ComplementoPagoRepository.CreateTable();
        }

        public bool CrearTablaCartaPorte() {
            return this._ComplementoCartaPorteRepository.CreateTable();
        }

        public BindingList<ComprobanteBackup> GetBackups(CFDISubTipoEnum subTipo) {
            return new BindingList<ComprobanteBackup>(this._ComprobanteFiscalRepository.GetInfoBackup(subTipo).ToList());
        }
        #endregion

        #region serializar
        public IComprobanteFiscalDetailModel Serializar(ComprobanteFiscalDetailSingleModel singleModel) {
            this.Serializar(singleModel, false);
            return null;
        }

        public IComprobanteFiscalDetailModel Serializar0(ComprobanteFiscalDetailSingleModel singleModel) {
            IComprobanteFiscalDetailModel model = new ComprobanteFiscalDetailModel();
            if (singleModel != null) {
                var _xml = this.GetComprobante(singleModel.Id);
                if (_xml != null) {
                    if (_xml.XML == null) {
                        var d0 = DownloadFile(_xml.UrlFileXML);
                        if (d0 != null) {
                            var d = new System.Text.UTF8Encoding(false);
                            _xml.XML = d.GetString(d0);//1 System.Text.Encoding.UTF8.GetString(d0, );
                        }
                    }
                }
                if (_xml != null) {
                    Builder.IDirectorioBuilder d1 = new Builder.DirectorioBuilder();
                    if (singleModel.Version == "3.3") {
                        var _cfdi = SAT.CFDI.V33.Comprobante.LoadXml(_xml.XML);
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo.EmisorRFC == ConfigService.Synapsis.Empresa.RFC) {
                            d1.Add(_modelo.Receptor).AddRelacion(TipoRelacionComericalEnum.Cliente).Execute();
                        } else if (_modelo.ReceptorRFC == ConfigService.Synapsis.Empresa.RFC) {
                            d1.Add(_modelo.Emisor).AddRelacion(TipoRelacionComericalEnum.Proveedor).Execute();
                        }
                        return _modelo;
                    } else if (singleModel.Version == "4.0") {
                        var _cfdi = SAT.CFDI.V40.Comprobante.LoadXml(_xml.XML);
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo.EmisorRFC == ConfigService.Synapsis.Empresa.RFC) {
                            d1.Add(_modelo.Receptor).AddRelacion(TipoRelacionComericalEnum.Cliente).Execute();
                        } else if (_modelo.ReceptorRFC == ConfigService.Synapsis.Empresa.RFC) {
                            d1.Add(_modelo.Emisor).AddRelacion(TipoRelacionComericalEnum.Proveedor).Execute();
                        }
                        return _modelo;
                    }
                }
            }
            return model;
        }

        public IComprobanteFiscalDetailModel GetModelo(int indice) {
            IComprobanteFiscalDetailModel model = new ComprobanteFiscalDetailModel();
            var _xml = this.GetComprobante(indice);
            var d0 = DownloadFile(_xml.UrlFileXML);
            if (d0 != null) {
                if (_xml.Version == "3.3") {
                    var _cfdi = SAT.CFDI.V33.Comprobante.LoadBytes(d0);
                    model = ComprobanteExtensions.Create(_cfdi);
                } else if (_xml.Version == "4.0") {
                    var _cfdi = SAT.CFDI.V40.Comprobante.LoadBytes(d0);
                    model = ComprobanteExtensions.Create(_cfdi);
                }
                return model;
            }
            return null;
        }

        public bool Serializar(ComprobanteFiscalDetailSingleModel singleModel, bool full = false) {
            if (singleModel != null) {
                if (singleModel.Version == "3.2") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    if (_xml != null) {
                        if (_xml.XML == null) {
                            var d0 = DownloadFile(_xml.UrlFileXML);
                            if (d0 != null) {
                                _xml.XML = System.Text.Encoding.UTF8.GetString(d0);
                            }
                        }
                        if (string.IsNullOrEmpty(_xml.XML))
                            return false;
                        var _cfdi = SAT.CFDI.V32.Comprobante.LoadXml(_xml.XML);
                        if (_cfdi == null) {
                            _cfdi = SAT.CFDI.V32.Comprobante.LoadBytes(DownloadFile(_xml.UrlFileXML));
                        }
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.RecepcionPago == null) {
                                _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                this.Save(_xml);
                            } else {
                                if (_xml.RecepcionPago != null && _xml.RecepcionPago.Count == 0) {
                                    _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                    this.Save(_xml);
                                } else {
                                    _xml.RecepcionPago = this.Verificar(_xml.RecepcionPago);
                                    this.Save(_xml);
                                }
                            }
                        }
                    }
                } else if (singleModel.Version == "3.3") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    if (_xml != null) {
                        if (_xml.XML == null) {
                            var d0 = DownloadFile(_xml.UrlFileXML);
                            if (d0 != null) {
                                _xml.XML = System.Text.Encoding.UTF8.GetString(d0);
                            }
                        }
                        if (string.IsNullOrEmpty(_xml.XML))
                            return false;
                        var _cfdi = SAT.CFDI.V33.Comprobante.LoadXml(_xml.XML);
                        if (_cfdi == null) {
                            _cfdi = SAT.CFDI.V33.Comprobante.LoadBytes(DownloadFile(_xml.UrlFileXML));
                        }
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.RecepcionPago == null) {
                                _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                this.Save(_xml);
                            } else {
                                if (_xml.RecepcionPago != null && _xml.RecepcionPago.Count == 0) {
                                    _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                    this.Save(_xml);
                                } else {
                                    _xml.RecepcionPago = this.Verificar(_xml.RecepcionPago);
                                    this.Save(_xml);
                                }
                            }
                        }
                    }
                } else if (singleModel.Version == "4.0") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    var d0 = DownloadFile(_xml.UrlFileXML);

                    if (d0 != null) {
                        var _cfdi = SAT.CFDI.V40.Comprobante.LoadBytes(d0);
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.RecepcionPago == null) {
                                _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                this.Save(_xml);
                            } else {
                                if (_xml.RecepcionPago != null && _xml.RecepcionPago.Count == 0) {
                                    _xml.RecepcionPago = this.Verificar(_modelo.RecepcionPago);
                                    this.Save(_xml);
                                } else {
                                    _xml.RecepcionPago = this.Verificar(_xml.RecepcionPago);
                                    this.Save(_xml);
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        // parche para actualizar tablas del complemento de pago
        public bool ParcheComplemento(int year) {
            var iddocumento = (int)this.subTipo;
            var query = this._ComprobanteFiscalRepository.GetList<ComprobanteFiscalDetailSingleModel>(new List<IConditional> {
                 new Conditional("_cfdi_anio", year.ToString()),
                 new Conditional("_cfdi_doc_id", iddocumento.ToString()),
                 new Conditional("_cfdi_efecto", "P%", ConditionalTypeEnum.Like)
            });

            foreach (var item in query) {
                if (item.TipoComprobanteText.ToLower().StartsWith("p"))
                    this.Serializar((ComprobanteFiscalDetailSingleModel)item, false);
            }
            return true;
        }

        public bool SerializarConceptos(ComprobanteFiscalDetailSingleModel singleModel) {
            if (singleModel != null) {
                if (singleModel.Version == "3.3") {
                    var _xml = this.GetComprobante(singleModel.Id);
                    if (_xml != null) {
                        var _cfdi = SAT.CFDI.V33.Comprobante.LoadXml(_xml.XML);
                        var _modelo = ComprobanteExtensions.Create(_cfdi);
                        if (_modelo != null) {
                            if (_xml.Conceptos.Count == 0) {
                                _xml.Conceptos = _modelo.Conceptos;
                                this.Save(_xml);
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool SerializarCFDIRelacionado(ComprobanteFiscalDetailSingleModel singleModel) {
            if (singleModel.CfdiRelacionados != null) {

            }
            return false;
        }

        /// <summary>
        /// verificar y relacionar documentos del complemento de pagos
        /// </summary>
        public BindingList<ComplementoPagoDetailModel> Verificar(BindingList<ComplementoPagoDetailModel> model) {
            if (model != null) {
                if (model.Count > 0) {
                    for (int p = 0; p < model.Count; p++) {
                        if (model[p].DoctoRelacionados != null) {
                            if (model[p].DoctoRelacionados.Count > 0) {
                                var uuids = model[p].DoctoRelacionados.Select(it => it.IdDocumentoDR.ToUpper()).ToList();
                                var registros = this.searchService.SearchUUID(uuids);
                                if (registros != null) {
                                    if (registros.Count > 0) {
                                        for (int i = 0; i < model[p].DoctoRelacionados.Count; i++) {
                                            try {
                                                var uuid = registros.Where(it => it.IdDocumento.ToUpper() == model[p].DoctoRelacionados[i].IdDocumentoDR.ToUpper()).FirstOrDefault();
                                                if (uuid != null) {
                                                    model[p].DoctoRelacionados[i].IdComprobanteR = uuid.Id;
                                                    model[p].DoctoRelacionados[i].FechaEmision = uuid.FechaEmision;
                                                    model[p].DoctoRelacionados[i].FormaDePagoP = uuid.ClaveFormaPago;
                                                    model[p].DoctoRelacionados[i].MetodoPago = uuid.ClaveMetodoPago;
                                                    model[p].DoctoRelacionados[i].IdSubTipo = uuid.IdSubTipo;
                                                    if (uuid.SubTipo == CFDISubTipoEnum.Emitido) {
                                                        model[p].DoctoRelacionados[i].Nombre = uuid.ReceptorNombre;
                                                        model[p].DoctoRelacionados[i].RFC = uuid.ReceptorRFC;
                                                    } else if (uuid.SubTipo == CFDISubTipoEnum.Recibido) {
                                                        model[p].DoctoRelacionados[i].Nombre = uuid.EmisorNombre;
                                                        model[p].DoctoRelacionados[i].RFC = uuid.EmisorRFC;
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                Console.WriteLine(ex.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// verificar la existencia de los documentos relacionados
        /// </summary>
        public ComprobanteCfdiRelacionadosModel Verificar(ComprobanteCfdiRelacionadosModel model) {
            if (model != null) {
                if (model.CfdiRelacionado != null) {
                    if (model.CfdiRelacionado.Count > 0) {
                        var uuids = model.CfdiRelacionado.Select(it => it.IdDocumento).ToList();
                        var registros = this.searchService.SearchUUID(uuids);
                        if (registros != null) {
                            if (registros.Count > 0) {
                                for (int i = 0; i < model.CfdiRelacionado.Count; i++) {
                                    try {
                                        var uuid = registros.Where(it => it.IdDocumento == model.CfdiRelacionado[i].IdDocumento.ToUpper()).FirstOrDefault();
                                        if (uuid != null) {

                                            model.CfdiRelacionado[i].FechaEmision = uuid.FechaEmision;
                                            model.CfdiRelacionado[i].Folio = uuid.Folio;
                                            model.CfdiRelacionado[i].FormaPago = uuid.ClaveFormaPago;
                                            model.CfdiRelacionado[i].MetodoPago = uuid.ClaveFormaPago;
                                            model.CfdiRelacionado[i].Moneda = uuid.ClaveMoneda;
                                            model.CfdiRelacionado[i].Serie = uuid.Serie;
                                            model.CfdiRelacionado[i].TipoCambio = uuid.TipoCambio;
                                            model.CfdiRelacionado[i].SubTipo = uuid.SubTipo;
                                            model.CfdiRelacionado[i].Total = uuid.Total;

                                            if (uuid.SubTipo == CFDISubTipoEnum.Emitido) {
                                                model.CfdiRelacionado[i].Nombre = uuid.ReceptorNombre;
                                                model.CfdiRelacionado[i].RFC = uuid.ReceptorRFC;
                                            } else if (uuid.SubTipo == CFDISubTipoEnum.Recibido) {
                                                model.CfdiRelacionado[i].Nombre = uuid.EmisorNombre;
                                                model.CfdiRelacionado[i].RFC = uuid.EmisorRFC;
                                            }
                                        }
                                    } catch (Exception ex) {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return model;
        }

        public ComprobanteFiscalDetailModel GetSerializar(ComprobanteFiscalDetailSingleModel singleModel) {
            var _xml = this.GetComprobante(singleModel.Id);
            if (_xml != null) {

                if (_xml.Version == "3.3") {
                    var _cfdi = SAT.CFDI.V33.Comprobante.LoadXml(_xml.XML);
                    var _modelo = ComprobanteExtensions.Create(_cfdi);
                    _modelo.Tag = _cfdi;
                    return _modelo;
                } else if (_xml.Version == "4.0") {
                    var _cfdi = SAT.CFDI.V40.Comprobante.LoadXml(_xml.XML);
                    var _modelo = ComprobanteExtensions.Create(_cfdi);
                    _modelo.Tag = _cfdi;
                    return _modelo;
                }
            }
            return null;
        }

        public static byte[] DownloadFile(string address) {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            byte[] downloadFile;
            try {
                downloadFile = (new WebClient()).DownloadData(address);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                downloadFile = null;
            }
            return downloadFile;
        }
        #endregion
    }
}
