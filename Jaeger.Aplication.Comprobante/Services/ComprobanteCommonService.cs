﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteCommonService : IComprobanteCommonService {
        //public static List<DocumentoCFDIModel> GetDocumentos() {
        //    return new List<DocumentoCFDIModel>() {
        //        new DocumentoCFDIModel("factura", "Factura"),
        //        new DocumentoCFDIModel("pago", "Recibo de Pago"),
        //        new DocumentoCFDIModel("cargo", "Nota Cargo"),
        //        new DocumentoCFDIModel("credito", "Nota Crédito"),
        //        new DocumentoCFDIModel("porte", "Carta Porte"), 
        //        new DocumentoCFDIModel("recibo", "Recibo fiscal"),
        //        new DocumentoCFDIModel("retencion", "Retención") };
        //}
        
        

        /// <summary>
        /// Catalogo de exportacion
        /// </summary>
        public static List<ExportacionModel> GetCatalogoExportacion() {
            return new List<ExportacionModel> {
                new ExportacionModel("01", "No Aplica"),
                new ExportacionModel("02", "Definitiva"),
                new ExportacionModel("03", "Temporal")
            };
        }

        /// <summary>
        /// listado de objetos de impuesto para el concepto
        /// </summary>
        public static List<ConceptoObjetoImpuestoModel> GetObjetoImpuesto() {
            return new List<ConceptoObjetoImpuestoModel> {
                new ConceptoObjetoImpuestoModel("01", "No objeto de impuesto"),
                new ConceptoObjetoImpuestoModel("02", "Sí objeto de impuesto"),
                new ConceptoObjetoImpuestoModel("03", "Sí objeto del impuesto y no obligado al desglose."),
                new ConceptoObjetoImpuestoModel("04", "Sí objeto del impuesto y no causa impuesto.")
            };
        }

        /// <summary>
        /// obtener listado de tipos de comprobantee
        /// </summary>
        public static BindingList<CFDITipoComprobanteModel> GetTipoComprobantes() {
            return new BindingList<CFDITipoComprobanteModel>(((CFDITipoComprobanteEnum[])Enum.GetValues(typeof(CFDITipoComprobanteEnum)))
                .Select(c => new CFDITipoComprobanteModel((int)c, c.ToString())).ToList());
        }

        /// <summary>
        /// obtener listado de status por el sub tipo de comprobante
        /// </summary>
        public static BindingList<CFDIStatusModel> GetStatus(CFDISubTipoEnum subTipo) {
            var response = new BindingList<CFDIStatusModel>();
            if (subTipo == CFDISubTipoEnum.Emitido) {
                response = new BindingList<CFDIStatusModel>(((CFDIStatusEmitidoEnum[])Enum.GetValues(typeof(CFDIStatusEmitidoEnum))).Where(c => c != CFDIStatusEmitidoEnum.Importado).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
            } else if (subTipo == CFDISubTipoEnum.Recibido) {
                response = new BindingList<CFDIStatusModel>(((CFDIStatusRecibidoEnum[])Enum.GetValues(typeof(CFDIStatusRecibidoEnum))).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
            }
            response.RemoveAt(0);
            return response;
        }

        /// <summary>
        /// obtener catalogo de motivos de cancelacion permitidos.
        /// </summary>
        public static BindingList<MotivoCancelacionModel> GetMotivoCancelacion() {
            return new BindingList<MotivoCancelacionModel> {
                new MotivoCancelacionModel("01", "Comprobante emitido con errores con relación."),
                new MotivoCancelacionModel("02", "Comprobante emitido con errores sin relación."),
                new MotivoCancelacionModel("03", "No se llevó a cabo la operación."),
                new MotivoCancelacionModel("04", "Operación nominativa relacionada en una factura global.")
            };
        }

        public static List<CartaPorteTranspInternacModel> GetTipoTranporte() {
            return new List<CartaPorteTranspInternacModel> { new CartaPorteTranspInternacModel(0, "Nacional"),
                new CartaPorteTranspInternacModel(1, "Internacional") };
        }

        public static List<CartaPorteEntradaSalidaMercModel> GetCartaPorteEntradaSalidaMercs() {
            return new List<CartaPorteEntradaSalidaMercModel> { new CartaPorteEntradaSalidaMercModel("Entrada", "Entrada"),
                new CartaPorteEntradaSalidaMercModel("Salida", "Salida")};
        }

        public static List<Jaeger.Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel> GetTipoNominas() {
            return new List<Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel>() {
                new Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel("O", "Ordinaria"), 
                new Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel("E", "Extraordinaria")
            };
        }
    }
}
