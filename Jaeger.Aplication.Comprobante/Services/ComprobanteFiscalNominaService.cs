﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Comprobante.Abstracts;
using Jaeger.DataAccess.Comprobante.Repositories;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Services {
    public class ComprobanteFiscalNominaService : ComprobanteService, IComprobanteFiscalNominaService {
        #region declaraciones
        protected internal ISqlComplementoNominaRepository nominaRepository;
        protected internal ISqlComplmentoNominaParteRepository parteRepository;
        #endregion

        public ComprobanteFiscalNominaService() : base() {
            this.nominaRepository = new SqlSugarComplementoNominaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.parteRepository = new SqlSugarComplementoNominaParteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IComprobanteFiscalNominaModel GetComprobante(int index) {
            return new ComprobanteFiscalNominaModel();
        }

        public IComprobanteFiscalNominaModel Save(IComprobanteFiscalNominaModel item) {
            if (item.Id == 0) {
                this.nominaRepository.Save(item);
            }
            return item;
        }
    }
}
