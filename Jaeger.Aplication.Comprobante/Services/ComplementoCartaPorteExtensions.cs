﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;

namespace Jaeger.Aplication.Comprobante.Services {
    /// <summary>
    /// Clase de extension para convertir modelo a clase XML
    /// </summary>
    public class ComplementoCartaPorteExtensions : ComplementoExtensions {
        public ComplementoCartaPorteExtensions() {

        }

        public SAT.CFDI.Complemento.CartaPorte.V20.CartaPorte Create(CartaPorteDetailModel model) {
            var _cartaPorte = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorte {
                Version = model.Version,
                ViaEntradaSalida = model.ViaEntradaSalida, 
                ViaEntradaSalidaSpecified = false, 
                TotalDistRec = model.TotalDistRec, 
                TotalDistRecSpecified = model.TotalDistRec > 0
            };

            if (model.TranspInternac == 0) {
                _cartaPorte.TranspInternac = "No";
                _cartaPorte.EntradaSalidaMerc = null;
                _cartaPorte.EntradaSalidaMercSpecified = false;
                _cartaPorte.PaisOrigenDestino = null;
                _cartaPorte.PaisOrigenDestinoSpecified = false;
            } else {
                _cartaPorte.TranspInternac = "Sí";
                _cartaPorte.EntradaSalidaMerc = model.EntradaSalidaMerc;
                _cartaPorte.EntradaSalidaMercSpecified = true;
                _cartaPorte.PaisOrigenDestino = model.PaisOrigenDestino;
                _cartaPorte.PaisOrigenDestinoSpecified = true;
            }

            _cartaPorte.Ubicaciones = this.Create(model.Ubicaciones);
            _cartaPorte.Mercancias = this.Create(model.Mercancias);
            _cartaPorte.FiguraTransporte = this.Create(model.FiguraTransporte);
            return _cartaPorte;
        }

        #region Model to XML
        /// <summary>
        /// Nodo requerido para registrar la ubicación que sirve para indicar el domicilio del origen y/o destino parcial o final, que tienen los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteUbicacion[] Create(BindingList<CartaPorteUbicacion> ubicaciones) {
            if (ubicaciones != null) {
                var _ubicaciones = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteUbicacion>();
                foreach (var _ubicacion in ubicaciones) {
                    var _nubicacion = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteUbicacion {
                        DistanciaRecorrida = _ubicacion.DistanciaRecorrida, 
                        DistanciaRecorridaSpecified = _ubicacion.DistanciaRecorrida > 0,
                        FechaHoraSalidaLlegada = _ubicacion.FechaHoraSalidaLlegada,
                        IDUbicacion = _ubicacion.IDUbicacion,
                        NombreEstacion = _ubicacion.NombreEstacion,
                        NumEstacion = _ubicacion.NumEstacion,
                        NumRegIdTrib = _ubicacion.NumRegIdTrib,
                        ResidenciaFiscal = _ubicacion.ResidenciaFiscal,
                        RFCRemitenteDestinatario = _ubicacion.RFCRemitenteDestinatario,
                        NombreRemitenteDestinatario = _ubicacion.NombreRemitenteDestinatario,
                        TipoUbicacion = (!string.IsNullOrEmpty(_ubicacion.TipoUbicacion) ? _ubicacion.TipoUbicacion : null)
                    };

                    if (_ubicacion.TipoEstacion != null) {
                        _nubicacion.TipoEstacion = _ubicacion.TipoEstacion;
                        _nubicacion.TipoEstacionSpecified = true;
                    } else {
                        _nubicacion.TipoEstacion = null;
                        _nubicacion.TipoEstacionSpecified = false;
                    }

                    if (string.IsNullOrEmpty(_ubicacion.ResidenciaFiscal)) {
                        _nubicacion.ResidenciaFiscalSpecified = false;
                    }

                    if (string.IsNullOrEmpty(_ubicacion.NumEstacion))
                        _nubicacion.NumEstacionSpecified = false;

                    if (_ubicacion.NavegacionTrafico == "No Aplica") {
                        _nubicacion.NavegacionTraficoSpecified = false;
                    } else {
                        _nubicacion.NavegacionTrafico = _ubicacion.NavegacionTrafico;
                        _nubicacion.NavegacionTraficoSpecified = true;
                    }

                    if (_ubicacion.Domicilio != null) {
                        _nubicacion.Domicilio = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteUbicacionDomicilio {
                            Calle = _ubicacion.Domicilio.Calle,
                            CodigoPostal = _ubicacion.Domicilio.CodigoPostal, 
                            Colonia = _ubicacion.Domicilio.Colonia,
                            Estado = _ubicacion.Domicilio.Estado,
                            Localidad = _ubicacion.Domicilio.Localidad,
                            Municipio = _ubicacion.Domicilio.Municipio,
                            NumeroExterior = _ubicacion.Domicilio.NumeroExterior,
                            NumeroInterior = _ubicacion.Domicilio.NumeroInterior,
                            Pais = _ubicacion.Domicilio.Pais,
                            Referencia = _ubicacion.Domicilio.Referencia
                        };
                    }

                    _ubicaciones.Add(_nubicacion);
                }
                if (_ubicaciones.Count > 0)
                    return _ubicaciones.ToArray();
            }
            return null;
        }

        /// <summary>
        /// Nodo requerido para registrar la información de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercancias Create(CartaPorteMercancias mercancias) {
            if (mercancias != null) {
                var _NMercancias = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercancias();
                if (mercancias.Mercancia != null) {
                    var lista = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercancia>();
                    foreach (var item in mercancias.Mercancia) {
                        var _mercancia = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercancia {
                            BienesTransp = item.BienesTransp,
                            Cantidad = item.Cantidad,
                            ClaveSTCC = item.ClaveSTCC,
                            ClaveUnidad = item.ClaveUnidad,
                            CveMaterialPeligroso = item.CveMaterialPeligroso,
                            Descripcion = item.Descripcion,
                            DescripEmbalaje = item.DescripEmbalaje,
                            Dimensiones = item.Dimensiones,
                            Embalaje = item.Embalaje,
                            FraccionArancelaria = item.FraccionArancelaria,
                            MaterialPeligroso = (item.MaterialPeligroso == true ? "Sí" : "No"),
                            Moneda = item.Moneda,
                            CveMaterialPeligrosoSpecified = !string.IsNullOrEmpty(item.CveMaterialPeligroso) ? true : false,
                            EmbalajeSpecified = !string.IsNullOrEmpty(item.Embalaje) ? true : false,
                            FraccionArancelariaSpecified = !string.IsNullOrEmpty(item.FraccionArancelaria) ? true : false,
                            MaterialPeligrosoSpecified = item.MaterialPeligroso,
                            PesoEnKg = item.PesoEnKg,
                            Unidad = item.Unidad,
                            ValorMercancia = item.ValorMercancia,
                            UUIDComercioExt = item.UUIDComercioExt,
                            MonedaSpecified = !string.IsNullOrEmpty(item.Moneda),
                            ValorMercanciaSpecified = item.ValorMercancia > 0
                        };

                        _mercancia.MaterialPeligrosoSpecified = true;
                        if (item.MaterialPeligroso == true) {
                            _mercancia.MaterialPeligroso = "Sí";
                        } else {
                            _mercancia.MaterialPeligroso = "No";
                        }

                        if (item.Pedimentos != null) {
                            _mercancia.Pedimentos = this.Crear(item.Pedimentos);
                        } else {
                            _mercancia.Pedimentos = null;
                        }

                        if (item.GuiasIdentificacion != null) {
                            _mercancia.GuiasIdentificacion = this.Crear(item.GuiasIdentificacion);
                        } else {
                            _mercancia.GuiasIdentificacion = null;
                        }

                        if (item.CantidadTransporta != null) {
                            _mercancia.CantidadTransporta = this.Crear(item.CantidadTransporta);
                        } else {
                            item.CantidadTransporta = null;
                        }

                        if (item.DetalleMercancia != null) {
                            _mercancia.DetalleMercancia = this.Crear(item.DetalleMercancia);
                        }

                        lista.Add(_mercancia);
                    }

                    _NMercancias.Mercancia = lista.ToArray();
                    _NMercancias.Autotransporte = this.Crear(mercancias.Autotransporte);
                    _NMercancias.TransporteAereo = this.Crear(mercancias.TransporteAereo);
                    _NMercancias.TransporteFerroviario = this.Crear(mercancias.TransporteFerroviario);
                    _NMercancias.TransporteMaritimo = this.Crear(mercancias.TransporteMaritimo);
                    _NMercancias.UnidadPeso = mercancias.UnidadPeso;
                    _NMercancias.PesoNetoTotal = mercancias.PesoNetoTotal;
                    _NMercancias.PesoNetoTotalSpecified = mercancias.PesoNetoTotal > 0;
                    _NMercancias.PesoNetoTotal = mercancias.PesoNetoTotal;
                    _NMercancias.PesoNetoTotalSpecified = mercancias.PesoNetoTotal > 0;
                    _NMercancias.PesoBrutoTotal = mercancias.PesoBrutoTotal;
                    _NMercancias.NumTotalMercancias = mercancias.NumTotalMercancias;
                    _NMercancias.CargoPorTasacion = mercancias.CargoPorTasacion;
                    _NMercancias.CargoPorTasacionSpecified = mercancias.CargoPorTasacion > 0;
                    return _NMercancias;
                }
            }
            return null;
        }

        /// <summary>
        /// Nodo condicional para indicar los datos del(los) tipo(s) de figura(s) que participan en el traslado de los bienes y/o mercancías en los distintos medios de transporte.
        /// </summary>
        private SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFigura[] Create(BindingList<CartaPorteTiposFigura> figuraTransporte) {
            if (figuraTransporte != null) {
                if (figuraTransporte.Count > 0) {
                    var _response = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFigura>();
                    foreach (var figura in figuraTransporte) {
                        var _figura = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFigura {
                            NombreFigura = figura.NombreFigura,
                            NumLicencia = figura.NumLicencia,
                            NumRegIdTribFigura = figura.NumRegIdTribFigura,
                            ResidenciaFiscalFigura = figura.ResidenciaFiscalFigura,
                            RFCFigura = figura.RFCFigura,
                            ResidenciaFiscalFiguraSpecified = !string.IsNullOrWhiteSpace(figura.ResidenciaFiscalFigura),
                            TipoFigura = figura.TipoFigura
                        };

                        if (figura.PartesTransporte != null) {
                            var _partes = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFiguraPartesTransporte>();
                            foreach (var parte in figura.PartesTransporte) {
                                _partes.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFiguraPartesTransporte { ParteTransporte = parte });
                            }
                            if (_partes.Count > 0) {
                                _figura.PartesTransporte = _partes.ToArray();
                            }
                        }

                        if (figura.Domicilio != null) {
                            _figura.Domicilio = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFiguraDomicilio() {
                                Calle = figura.Domicilio.Calle,
                                CodigoPostal = figura.Domicilio.CodigoPostal,
                                Colonia = figura.Domicilio.Colonia,
                                Estado = figura.Domicilio.Estado,
                                Localidad = figura.Domicilio.Localidad,
                                Municipio = figura.Domicilio.Municipio,
                                NumeroExterior = figura.Domicilio.NumeroExterior,
                                NumeroInterior = figura.Domicilio.NumeroInterior,
                                Pais = figura.Domicilio.Pais,
                                Referencia = figura.Domicilio.Referencia
                            };
                        }

                        _response.Add(_figura);
                    }
                    return _response.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Nodo condicional para registrar la información del(los) número(s) de pedimento(s) de importación que se encuentra(n) asociado(s) al traslado de los bienes y/o mercancías de procedencia extranjera para acreditar 
        /// la legal estancia o tenencia durante su traslado en territorio nacional.
        /// </summary>
        public SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaPedimentos[] Crear(BindingList<string> pedimentos) {
            if (pedimentos != null) {
                if (pedimentos.Count > 0) {
                    var lista = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaPedimentos>();
                    foreach (var item in pedimentos) {
                        lista.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaPedimentos { Pedimento = item });
                    }
                    if (lista.Count > 0)
                        return lista.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Nodo condicional para registrar la información del(los) número(s) de guía(s) que se encuentre(n) asociado(s) al(los) paquete(s) que se traslada(n) dentro del territorio nacional.
        /// </summary>
        public SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaGuiasIdentificacion[] Crear(BindingList<CartaPorteMercanciasMercanciaGuiasIdentificacion> guiasIdentificacion) {
            if (guiasIdentificacion != null) {
                if (guiasIdentificacion.Count > 0) {
                    var lista = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaGuiasIdentificacion>();
                    foreach (var item in guiasIdentificacion) {
                        lista.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaGuiasIdentificacion {
                            DescripGuiaIdentificacion = item.DescripGuiaIdentificacion,
                            NumeroGuiaIdentificacion = item.NumeroGuiaIdentificacion,
                            PesoGuiaIdentificacion = item.PesoGuiaIdentificacion
                        });
                    }
                    return lista.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Nodo opcional para registrar la cantidad de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte, que será captada o distribuida en distintos puntos, a fin de identificar el punto de origen y destino correspondiente.
        /// </summary>
        public SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaCantidadTransporta[] Crear(BindingList<CartaPorteMercanciasMercanciaCantidadTransporta> cantidadTransporta) {
            if (cantidadTransporta != null) {
                if (cantidadTransporta.Count > 0) {
                    var lista = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaCantidadTransporta>();
                    foreach (var item in cantidadTransporta) {
                        lista.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaCantidadTransporta {
                            Cantidad = item.Cantidad,
                            CvesTransporte = item.CvesTransporte,
                            IDDestino = item.IDDestino,
                            IDOrigen = item.IDOrigen,
                            CvesTransporteSpecified = !string.IsNullOrEmpty(item.CvesTransporte)
                        });
                    }
                    return lista.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Nodo condicional para registrar especificaciones de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        private SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaDetalleMercancia Crear(CartaPorteMercanciasMercanciaDetalleMercancia detalleMercancia) {
            if (detalleMercancia != null) {
                var _response = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasMercanciaDetalleMercancia {
                    PesoBruto = detalleMercancia.PesoBruto,
                    NumPiezas = detalleMercancia.NumPiezas,
                    PesoNeto = detalleMercancia.PesoNeto,
                    PesoTara = detalleMercancia.PesoNeto,
                    UnidadPesoMerc = detalleMercancia.UnidadPesoMerc,
                    NumPiezasSpecified = detalleMercancia.NumPiezas > 0
                };
                return _response;
            }
            return null;
        }

        /// <summary>
        /// Nodo condicional para registrar la información que permita la identificación del autotransporte de carga, por medio del cual se trasladan los bienes y/o mercancías, que transitan a través de las carreteras del territorio nacional.
        /// </summary>
        private SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporte Crear(CartaPorteMercanciasAutotransporte autotransporte) {
            if (autotransporte != null) {
                var _response = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporte {
                    PermSCT = autotransporte.PermSCT,
                    NumPermisoSCT = autotransporte.NumPermisoSCT
                };

                if (autotransporte.IdentificacionVehicular != null) {
                    _response.IdentificacionVehicular = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporteIdentificacionVehicular {
                        AnioModeloVM = autotransporte.IdentificacionVehicular.AnioModeloVM,
                        ConfigVehicular = autotransporte.IdentificacionVehicular.ConfigVehicular,
                        PlacaVM = autotransporte.IdentificacionVehicular.PlacaVM
                    };
                }

                if (autotransporte.Remolques != null) {
                    if (autotransporte.Remolques.Count > 0) {
                        var lista = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporteRemolque>();
                        foreach (var item in autotransporte.Remolques) {
                            lista.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporteRemolque {
                                Placa = item.Placa,
                                SubTipoRem = item.SubTipoRem
                            });
                        }
                        _response.Remolques = lista.ToArray();
                    }
                }

                if (autotransporte.Seguros != null) {
                    var _seguros = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporteSeguros {
                        PrimaSeguro = autotransporte.Seguros.PrimaSeguro,
                        PrimaSeguroSpecified = autotransporte.Seguros.PrimaSeguro > 0
                    };

                    if (!string.IsNullOrEmpty(autotransporte.Seguros.AseguraCarga)) {
                        _seguros.AseguraCarga = autotransporte.Seguros.AseguraCarga;
                        _seguros.PolizaCarga = autotransporte.Seguros.PolizaCarga;
                    }

                    if (!string.IsNullOrEmpty(autotransporte.Seguros.AseguraMedAmbiente)) {
                        _seguros.AseguraMedAmbiente = autotransporte.Seguros.AseguraMedAmbiente;
                        _seguros.PolizaMedAmbiente = autotransporte.Seguros.PolizaMedAmbiente;  
                    }

                    if (!string.IsNullOrEmpty(autotransporte.Seguros.AseguraRespCivil)) {
                        _seguros.AseguraRespCivil = autotransporte.Seguros.AseguraRespCivil;
                        _seguros.PolizaRespCivil = autotransporte.Seguros.PolizaRespCivil;
                    }

                    _response.Seguros = _seguros;
                }
                return _response;
            }
            return null;
        }

        private SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteAereo Crear(CartaPorteMercanciasTransporteAereo transporteAereo) {
            if (transporteAereo != null) {
                var _response = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteAereo {
                    NumPolizaSeguro = transporteAereo.NumPolizaSeguro,
                    CodigoTransportista = transporteAereo.CodigoTransportista,
                    LugarContrato = transporteAereo.LugarContrato,
                    MatriculaAeronave = transporteAereo.MatriculaAeronave,
                    NombreAseg = transporteAereo.NombreAseg,
                    NombreEmbarcador = transporteAereo.NombreEmbarcador,
                    NumeroGuia = transporteAereo.NumeroGuia,
                    NumPermisoSCT = transporteAereo.NumPermisoSCT,
                    NumRegIdTribEmbarc = transporteAereo.NombreEmbarcador,
                    PermSCT = transporteAereo.PermSCT,
                    RFCEmbarcador = transporteAereo.RFCEmbarcador,
                    ResidenciaFiscalEmbarc = transporteAereo.ResidenciaFiscalEmbarc,
                    ResidenciaFiscalEmbarcSpecified = !string.IsNullOrEmpty(transporteAereo.ResidenciaFiscalEmbarc)
                };
                return _response;
            }
            return null;
        }

        private SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviario Crear(CartaPorteMercanciasTransporteFerroviario transporteFerroviario) {
            if (transporteFerroviario != null) {
                var _response = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviario {
                    NumPolizaSeguro = transporteFerroviario.NumPolizaSeguro,
                    NombreAseg = transporteFerroviario.NombreAseg,
                    TipoDeServicio = transporteFerroviario.TipoDeServicio,
                    TipoDeTrafico = transporteFerroviario.TipoDeTrafico
                };

                if (transporteFerroviario.Carro != null) {
                    var lista = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviarioCarro>();
                    foreach (var item in transporteFerroviario.Carro) {
                        var carro = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviarioCarro {
                            GuiaCarro = item.GuiaCarro,
                            MatriculaCarro = item.MatriculaCarro,
                            ToneladasNetasCarro = item.ToneladasNetasCarro,
                            TipoCarro = item.TipoCarro
                        };

                        if (item.Contenedor != null) {
                            var _contenedores = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviarioCarroContenedor>();
                            foreach (var _contenedor in item.Contenedor) {
                                _contenedores.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviarioCarroContenedor {
                                    PesoContenedorVacio = _contenedor.PesoContenedorVacio,
                                    TipoContenedor = _contenedor.TipoContenedor,
                                    PesoNetoMercancia = _contenedor.PesoNetoMercancia
                                });
                            }
                            carro.Contenedor = _contenedores.ToArray();
                        }
                        lista.Add(carro);
                    }
                }

                if (transporteFerroviario.DerechosDePaso != null) {
                    var _derechosDePaso = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviarioDerechosDePaso>();
                    foreach (var item in transporteFerroviario.DerechosDePaso) {
                        _derechosDePaso.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviarioDerechosDePaso() {
                            KilometrajePagado = item.KilometrajePagado,
                            TipoDerechoDePaso = item.TipoDerechoDePaso
                        });
                    }
                }
                return _response;
            }
            return null;
        }

        private SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteMaritimo Crear(CartaPorteMercanciasTransporteMaritimo transporteMaritimo) {
            if (transporteMaritimo != null) {
                var _response = new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteMaritimo {
                    AnioEmbarcacion = transporteMaritimo.AnioEmbarcacion,
                    Calado = transporteMaritimo.Calado,
                    Eslora = transporteMaritimo.Eslora,
                    LineaNaviera = transporteMaritimo.LineaNaviera,
                    Manga = transporteMaritimo.Manga,
                    Matricula = transporteMaritimo.Matricula,
                    NacionalidadEmbarc = transporteMaritimo.NacionalidadEmbarc,
                    NombreAgenteNaviero = transporteMaritimo.NombreAgenteNaviero,
                    NombreEmbarc = transporteMaritimo.NombreEmbarc,
                    PermSCT = transporteMaritimo.PermSCT,
                    NumeroOMI = transporteMaritimo.NumeroOMI,
                    NumCertITC = transporteMaritimo.NumCertITC,
                    UnidadesDeArqBruto = transporteMaritimo.UnidadesDeArqBruto,
                    NombreAseg = transporteMaritimo.NombreAseg,
                    NumAutorizacionNaviero = transporteMaritimo.NumAutorizacionNaviero,
                    NumConocEmbarc = transporteMaritimo.NumConocEmbarc,
                    NumPermisoSCT = transporteMaritimo.NumPermisoSCT,
                    NumPolizaSeguro = transporteMaritimo.NumPolizaSeguro,
                    NumViaje = transporteMaritimo.NumViaje,
                    TipoCarga = transporteMaritimo.TipoCarga,
                    TipoEmbarcacion = transporteMaritimo.TipoEmbarcacion,
                    AnioEmbarcacionSpecified = transporteMaritimo.AnioEmbarcacion > 0,
                    CaladoSpecified = transporteMaritimo.Calado > 0,
                    EsloraSpecified = transporteMaritimo.Eslora > 0,
                    MangaSpecified = transporteMaritimo.Manga > 0,
                    PermSCTSpecified = string.IsNullOrEmpty(transporteMaritimo.PermSCT)
                };

                if (transporteMaritimo.Contenedor != null) {
                    var _contenedores = new List<SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteMaritimoContenedor>();
                    foreach (var item in transporteMaritimo.Contenedor) {
                        _contenedores.Add(new SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteMaritimoContenedor {
                            MatriculaContenedor = item.MatriculaContenedor,
                            NumPrecinto = item.NumPrecinto,
                            TipoContenedor = item.TipoContenedor
                        });
                    }
                }
                return _response;
            }
            return null;
        }
        #endregion

        #region XML v20 to model
        public CartaPorteDetailModel Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorte cartaPorte) {
            if (cartaPorte != null) {
                var _model = new CartaPorteDetailModel {
                    Version = cartaPorte.Version,
                    TranspInternac = cartaPorte.TranspInternac == "Sí" ? 0 : 1,
                    ViaEntradaSalida = cartaPorte.ViaEntradaSalida,
                    TotalDistRec = cartaPorte.TotalDistRec,
                    PaisOrigenDestino = cartaPorte.PaisOrigenDestino,
                    EntradaSalidaMerc = cartaPorte.EntradaSalidaMerc
                };

                _model.FiguraTransporte = this.Create(cartaPorte.FiguraTransporte);
                _model.Mercancias = this.Create(cartaPorte.Mercancias);
                _model.Ubicaciones = this.Create(cartaPorte.Ubicaciones);
                return _model;
            }
            return null;
        }

        private BindingList<CartaPorteUbicacion> Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteUbicacion[] ubicaciones) {
            if (ubicaciones != null) {
                var _ubicaciones = new BindingList<CartaPorteUbicacion>();
                foreach (var item in ubicaciones) {
                    var _ubicacion = new CartaPorteUbicacion {
                        DistanciaRecorrida = item.DistanciaRecorrida,
                        FechaHoraSalidaLlegada = item.FechaHoraSalidaLlegada,
                        IDUbicacion = item.IDUbicacion,
                        NavegacionTrafico = item.NavegacionTrafico,
                        NombreEstacion = item.NombreEstacion,
                        NombreRemitenteDestinatario = item.NombreRemitenteDestinatario,
                        NumEstacion = item.NumEstacion,
                        NumRegIdTrib = item.NumRegIdTrib,
                        TipoUbicacion = item.TipoUbicacion,
                        TipoEstacion = item.TipoEstacion,
                        ResidenciaFiscal = item.ResidenciaFiscal,
                        RFCRemitenteDestinatario = item.RFCRemitenteDestinatario
                    };

                    if (item.Domicilio != null) {
                        _ubicacion.Domicilio = new CartaPorteUbicacionDomicilio {
                            Calle = item.Domicilio.Calle,
                            CodigoPostal = item.Domicilio.CodigoPostal,
                            Colonia = item.Domicilio.Colonia,
                            Estado = item.Domicilio.Estado,
                            Localidad = item.Domicilio.Localidad,
                            Municipio = item.Domicilio.Municipio,
                            NumeroExterior = item.Domicilio.NumeroExterior,
                            NumeroInterior = item.Domicilio.NumeroInterior,
                            Pais = item.Domicilio.Pais,
                            Referencia = item.Domicilio.Referencia
                        };
                    }

                    _ubicaciones.Add(_ubicacion);
                }
                return _ubicaciones;
            }
            return null;
        }

        private CartaPorteMercancias Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercancias mercancias) {
            if (mercancias != null) {
                var _response = new CartaPorteMercancias {
                    PesoBrutoTotal = mercancias.PesoBrutoTotal,
                    PesoNetoTotal = mercancias.PesoNetoTotal,
                    NumTotalMercancias = mercancias.NumTotalMercancias,
                    CargoPorTasacion = mercancias.CargoPorTasacion,
                    UnidadPeso = mercancias.UnidadPeso
                };

                _response.Autotransporte = this.Create(mercancias.Autotransporte);
                _response.TransporteAereo = this.Create(mercancias.TransporteAereo);
                _response.TransporteFerroviario = this.Create(mercancias.TransporteFerroviario);
                _response.TransporteMaritimo = this.Create(mercancias.TransporteMaritimo);

                if (mercancias.Mercancia != null) {
                    _response.Mercancia = new BindingList<CartaPorteMercanciasMercancia>();
                    foreach (var item in mercancias.Mercancia) {
                        var _mercancia = new CartaPorteMercanciasMercancia {
                            BienesTransp = item.BienesTransp,
                            Cantidad = item.Cantidad,
                            ClaveSTCC = item.ClaveSTCC,
                            ClaveUnidad = item.ClaveUnidad,
                            CveMaterialPeligroso = item.CveMaterialPeligroso,
                            Descripcion = item.Descripcion,
                            DescripEmbalaje = item.DescripEmbalaje,
                            Dimensiones = item.Dimensiones,
                            FraccionArancelaria = item.FraccionArancelaria,
                            Embalaje = item.Embalaje,
                            PesoEnKg = item.PesoEnKg,
                            Unidad = item.Unidad,
                            UUIDComercioExt = item.UUIDComercioExt,
                            ValorMercancia = item.ValorMercancia,
                            Moneda = item.Moneda
                        };

                        if (item.DetalleMercancia != null) {
                            _mercancia.DetalleMercancia = new CartaPorteMercanciasMercanciaDetalleMercancia {
                                NumPiezas = item.DetalleMercancia.NumPiezas,
                                PesoBruto = item.DetalleMercancia.PesoBruto,
                                PesoNeto = item.DetalleMercancia.PesoNeto,
                                PesoTara = item.DetalleMercancia.PesoTara,
                                UnidadPesoMerc = item.DetalleMercancia.UnidadPesoMerc
                            };
                        }

                        if (item.GuiasIdentificacion != null) {
                            _mercancia.GuiasIdentificacion = new BindingList<CartaPorteMercanciasMercanciaGuiasIdentificacion>();
                            foreach (var item1 in item.GuiasIdentificacion) {
                                _mercancia.GuiasIdentificacion.Add(new CartaPorteMercanciasMercanciaGuiasIdentificacion {
                                    DescripGuiaIdentificacion = item1.NumeroGuiaIdentificacion,
                                    NumeroGuiaIdentificacion = item1.NumeroGuiaIdentificacion,
                                    PesoGuiaIdentificacion = item1.PesoGuiaIdentificacion
                                });
                            }
                        }

                        if (item.Pedimentos != null) {
                            _mercancia.Pedimentos = new BindingList<string>();
                            foreach (var item2 in item.Pedimentos) {
                                _mercancia.Pedimentos.Add(item2.Pedimento);
                            }
                        }

                        if (item.CantidadTransporta != null) {
                            _mercancia.CantidadTransporta = new BindingList<CartaPorteMercanciasMercanciaCantidadTransporta>();
                            foreach (var item3 in item.CantidadTransporta) {
                                _mercancia.CantidadTransporta.Add(new CartaPorteMercanciasMercanciaCantidadTransporta {
                                    Cantidad = item3.Cantidad,
                                    CvesTransporte = item3.CvesTransporte,
                                    IDDestino = item3.IDDestino,
                                    IDOrigen = item3.IDOrigen
                                });
                            }
                        }

                        _response.Mercancia.Add(_mercancia);
                    }
                    return _response;
                }
            }
            return null;
        }

        private CartaPorteMercanciasTransporteMaritimo Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteMaritimo transporteMaritimo) {
            if (transporteMaritimo != null) {
                var _response = new CartaPorteMercanciasTransporteMaritimo {
                    TipoCarga = transporteMaritimo.TipoCarga,
                    NumCertITC = transporteMaritimo.NumCertITC,
                    NumViaje = transporteMaritimo.NumViaje,
                    AnioEmbarcacion = transporteMaritimo.AnioEmbarcacion,
                    Calado = transporteMaritimo.Calado,
                    Eslora = transporteMaritimo.Eslora,
                    LineaNaviera = transporteMaritimo.LineaNaviera,
                    Manga = transporteMaritimo.Manga,
                    Matricula = transporteMaritimo.Matricula,
                    NombreAgenteNaviero = transporteMaritimo.NombreAgenteNaviero,
                    NacionalidadEmbarc = transporteMaritimo.NacionalidadEmbarc,
                    NombreAseg = transporteMaritimo.NombreAseg,
                    NombreEmbarc = transporteMaritimo.NombreEmbarc,
                    NumAutorizacionNaviero = transporteMaritimo.NumAutorizacionNaviero,
                    NumConocEmbarc = transporteMaritimo.NumConocEmbarc,
                    NumeroOMI = transporteMaritimo.NumeroOMI,
                    NumPermisoSCT = transporteMaritimo.NumPermisoSCT,
                    NumPolizaSeguro = transporteMaritimo.NumPolizaSeguro,
                    PermSCT = transporteMaritimo.PermSCT,
                    TipoEmbarcacion = transporteMaritimo.TipoEmbarcacion,
                    UnidadesDeArqBruto = transporteMaritimo.UnidadesDeArqBruto
                };

                if (transporteMaritimo.Contenedor != null) {
                    foreach (var item in transporteMaritimo.Contenedor) {
                        _response.Contenedor.Add(new CartaPorteMercanciasTransporteMaritimoContenedor {
                            MatriculaContenedor = item.MatriculaContenedor,
                            NumPrecinto = item.NumPrecinto,
                            TipoContenedor = item.TipoContenedor
                        });
                    }
                }
                return _response;
            }
            return null;
        }

        private CartaPorteMercanciasTransporteFerroviario Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteFerroviario transporteFerroviario) {
            if (transporteFerroviario != null) {
                var _response = new CartaPorteMercanciasTransporteFerroviario {
                    TipoDeTrafico = transporteFerroviario.TipoDeTrafico,
                    NombreAseg = transporteFerroviario.NombreAseg,
                    TipoDeServicio = transporteFerroviario.TipoDeServicio,
                    NumPolizaSeguro = transporteFerroviario.NumPolizaSeguro
                };

                foreach (var item in transporteFerroviario.DerechosDePaso) {
                    _response.DerechosDePaso.Add(new CartaPorteMercanciasTransporteFerroviarioDerechosDePaso {
                        KilometrajePagado = item.KilometrajePagado,
                        TipoDerechoDePaso = item.TipoDerechoDePaso
                    });
                }

                foreach (var item in transporteFerroviario.Carro) {
                    var _carro = new CartaPorteMercanciasTransporteFerroviarioCarro {
                        GuiaCarro = item.GuiaCarro,
                        MatriculaCarro = item.MatriculaCarro,
                        TipoCarro = item.TipoCarro,
                        ToneladasNetasCarro = item.ToneladasNetasCarro
                    };

                    if (item.Contenedor != null) {
                        _carro.Contenedor = new BindingList<CartaPorteMercanciasTransporteFerroviarioCarroContenedor>();
                        foreach (var contenedor in _carro.Contenedor) {
                            _carro.Contenedor.Add(new CartaPorteMercanciasTransporteFerroviarioCarroContenedor {
                                PesoContenedorVacio = contenedor.PesoContenedorVacio,
                                PesoNetoMercancia = contenedor.PesoContenedorVacio,
                                TipoContenedor = contenedor.TipoContenedor
                            });
                        }
                    }
                    _response.Carro.Add(_carro);
                    return _response;
                }
            }
            return null;
        }

        private CartaPorteMercanciasTransporteAereo Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasTransporteAereo transporteAereo) {
            if (transporteAereo != null) {
                var _response = new CartaPorteMercanciasTransporteAereo {
                    CodigoTransportista = transporteAereo.CodigoTransportista,
                    LugarContrato = transporteAereo.LugarContrato,
                    MatriculaAeronave = transporteAereo.MatriculaAeronave,
                    NombreAseg = transporteAereo.NombreAseg,
                    NombreEmbarcador = transporteAereo.NombreEmbarcador,
                    NumeroGuia = transporteAereo.NumeroGuia,
                    NumPermisoSCT = transporteAereo.NumPermisoSCT,
                    NumPolizaSeguro = transporteAereo.NumPolizaSeguro,
                    NumRegIdTribEmbarc = transporteAereo.NumRegIdTribEmbarc,
                    PermSCT = transporteAereo.PermSCT,
                    ResidenciaFiscalEmbarc = transporteAereo.ResidenciaFiscalEmbarc,
                    RFCEmbarcador = transporteAereo.RFCEmbarcador
                };
                return _response;
            }
            return null;
        }

        private CartaPorteMercanciasAutotransporte Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteMercanciasAutotransporte autotransporte) {
            if (autotransporte != null) {
                var _response = new CartaPorteMercanciasAutotransporte {
                    PermSCT = autotransporte.PermSCT,
                    NumPermisoSCT = autotransporte.NumPermisoSCT
                };

                if (autotransporte.Remolques != null) {
                    foreach (var item in autotransporte.Remolques) {
                        _response.Remolques = new BindingList<CartaPorteMercanciasAutotransporteRemolque>();
                        _response.Remolques.Add(new CartaPorteMercanciasAutotransporteRemolque {
                            Placa = item.Placa,
                            SubTipoRem = item.SubTipoRem
                        });
                    }
                }

                if (autotransporte.Seguros != null) {
                    _response.Seguros = new CartaPorteMercanciasAutotransporteSeguros {
                        AseguraCarga = autotransporte.Seguros.AseguraCarga,
                        AseguraMedAmbiente = autotransporte.Seguros.AseguraMedAmbiente,
                        AseguraRespCivil = autotransporte.Seguros.AseguraRespCivil,
                        PolizaCarga = autotransporte.Seguros.PolizaCarga,
                        PolizaMedAmbiente = autotransporte.Seguros.PolizaMedAmbiente,
                        PolizaRespCivil = autotransporte.Seguros.PolizaRespCivil,
                        PrimaSeguro = autotransporte.Seguros.PrimaSeguro
                    };
                }

                if (autotransporte.IdentificacionVehicular != null) {
                    _response.IdentificacionVehicular = new CartaPorteMercanciasAutotransporteIdentificacionVehicular {
                        PlacaVM = autotransporte.IdentificacionVehicular.PlacaVM,
                        AnioModeloVM = autotransporte.IdentificacionVehicular.AnioModeloVM,
                        ConfigVehicular = autotransporte.IdentificacionVehicular.ConfigVehicular
                    };
                }

                return _response;
            }
            return null;
        }

        private BindingList<CartaPorteTiposFigura> Create(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorteTiposFigura[] figuraTransporte) {
            if (figuraTransporte != null) {
                var _response = new BindingList<CartaPorteTiposFigura>();
                foreach (var item in figuraTransporte) {
                    var _TipoFigura = new CartaPorteTiposFigura {
                        NombreFigura = item.NombreFigura,
                        NumLicencia = item.NumLicencia,
                        NumRegIdTribFigura = item.NumRegIdTribFigura,
                        ResidenciaFiscalFigura = item.ResidenciaFiscalFigura,
                        RFCFigura = item.RFCFigura,
                        TipoFigura = item.TipoFigura
                    };

                    if (item.Domicilio != null) {
                        _TipoFigura.Domicilio = new CartaPorteTiposFiguraDomicilio {
                            Calle = item.Domicilio.Calle,
                            CodigoPostal = item.Domicilio.CodigoPostal,
                            Colonia = item.Domicilio.Colonia,
                            Estado = item.Domicilio.Estado,
                            Localidad = item.Domicilio.Localidad,
                            Municipio = item.Domicilio.Municipio,
                            NumeroExterior = item.Domicilio.NumeroExterior,
                            NumeroInterior = item.Domicilio.NumeroExterior,
                            Pais = item.Domicilio.Pais,
                            Referencia = item.Domicilio.Referencia
                        };
                    }

                    if (item.PartesTransporte != null) {
                        _TipoFigura.PartesTransporte = new BindingList<string>();
                        foreach (var item1 in item.PartesTransporte) {
                            _TipoFigura.PartesTransporte.Add(item1.ParteTransporte);
                        }
                    }
                    _response.Add(_TipoFigura);
                }

                if (_response.Count > 0)
                    return _response;
            }
            return null;
        }
        #endregion

        #region XML v10 to model
        public CartaPorteDetailModel Create(SAT.CFDI.Complemento.CartaPorte.V10.CartaPorte cartaPorte) {
            if (cartaPorte != null) {
                var _model = new CartaPorteDetailModel {
                    Version = cartaPorte.Version,
                    TranspInternac = cartaPorte.TranspInternac == SAT.CFDI.Complemento.CartaPorte.V10.CartaPorteTranspInternac.Sí ? 1 : 0,
                    ViaEntradaSalida = cartaPorte.ViaEntradaSalida.ToString(),
                    TotalDistRec = (cartaPorte.TotalDistRecSpecified ? cartaPorte.TotalDistRec : 0),
                    PaisOrigenDestino = string.Empty,
                    EntradaSalidaMerc = (cartaPorte.EntradaSalidaMercSpecified ? cartaPorte.EntradaSalidaMerc.ToString() : string.Empty),
                };

                _model.FiguraTransporte = this.Create(cartaPorte.FiguraTransporte);
                _model.Mercancias = this.Create(cartaPorte.Mercancias);
                _model.Ubicaciones = this.Create(cartaPorte.Ubicaciones);
                return _model;
            }
            return null;
        }

        private BindingList<CartaPorteTiposFigura> Create(SAT.CFDI.Complemento.CartaPorte.V10.CartaPorteFiguraTransporte figuraTransporte) {
            if (figuraTransporte != null) {
                var _response = new BindingList<CartaPorteTiposFigura>();
                if (figuraTransporte.Arrendatario != null) {
                    foreach (var item in figuraTransporte.Arrendatario) {
                        var _nuevo = new CartaPorteTiposFigura {
                            TipoFigura = "Arrendatario",
                            NumRegIdTribFigura = item.NumRegIdTribArrendatario,
                            RFCFigura = item.RFCArrendatario,
                            NombreFigura = item.NombreArrendatario,
                            ResidenciaFiscalFigura = item.ResidenciaFiscalArrendatarioSpecified ? item.ResidenciaFiscalArrendatario : string.Empty
                        };
                        if (item.Domicilio != null) {
                            _nuevo.Domicilio = new CartaPorteTiposFiguraDomicilio {
                                Calle = item.Domicilio.Calle,
                                CodigoPostal = item.Domicilio.CodigoPostal,
                                Colonia = item.Domicilio.Colonia,
                                Estado = item.Domicilio.Estado,
                                Localidad = item.Domicilio.Localidad,
                                Municipio = item.Domicilio.Municipio,
                                NumeroExterior = item.Domicilio.NumeroExterior,
                                NumeroInterior = item.Domicilio.NumeroInterior,
                                Pais = item.Domicilio.Pais,
                                Referencia = item.Domicilio.Referencia
                            };
                        }
                        _response.Add(_nuevo);
                    }
                }
            }
            throw new System.NotImplementedException();
        }

        private CartaPorteMercancias Create(SAT.CFDI.Complemento.CartaPorte.V10.CartaPorteMercancias mercancias) {
            if (mercancias != null) {
                var _response = new CartaPorteMercancias();
                foreach (var item in mercancias.Mercancia) {
                    _response.Mercancia.Add(new CartaPorteMercanciasMercancia {
                        BienesTransp = item.BienesTransp,
                        Cantidad = item.Cantidad,
                        ClaveSTCC = item.ClaveSTCC,
                        ClaveUnidad = item.ClaveUnidad,
                        CveMaterialPeligroso = item.CveMaterialPeligroso,
                        Descripcion = item.Descripcion,
                        DescripEmbalaje = item.DescripEmbalaje,
                        Dimensiones = item.Dimensiones,
                        Embalaje = item.Embalaje,
                        FraccionArancelaria = item.FraccionArancelaria,
                        Moneda = item.Moneda,
                        Unidad = item.Unidad,
                        PesoEnKg = item.PesoEnKg,
                        UUIDComercioExt = item.UUIDComercioExt,
                        ValorMercancia = item.ValorMercancia,
                    });
                }
            }
            throw new System.NotImplementedException();
        }

        private BindingList<CartaPorteUbicacion> Create(SAT.CFDI.Complemento.CartaPorte.V10.CartaPorteUbicacion[] ubicaciones) {
            if (ubicaciones != null) {
                var _response = new BindingList<CartaPorteUbicacion>();
                foreach (var item in ubicaciones) {
                    var _nuevo = new CartaPorteUbicacion();
                    _nuevo.DistanciaRecorrida = item.DistanciaRecorrida;
                    
                    if (item.Domicilio != null) {
                        _nuevo.Domicilio = new CartaPorteUbicacionDomicilio() {
                            Calle = item.Domicilio.Calle,
                            CodigoPostal = item.Domicilio.CodigoPostal,
                            Colonia = item.Domicilio.Colonia,
                            Estado = item.Domicilio.Estado,
                            Localidad = item.Domicilio.Localidad,
                            Municipio = item.Domicilio.Municipio,
                            NumeroExterior = item.Domicilio.NumeroExterior,
                            NumeroInterior = item.Domicilio.NumeroInterior,
                            Pais = item.Domicilio.NumeroInterior,
                            Referencia = item.Domicilio.Referencia
                        };
                    }
                    _response.Add(_nuevo);
                }
            }
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
