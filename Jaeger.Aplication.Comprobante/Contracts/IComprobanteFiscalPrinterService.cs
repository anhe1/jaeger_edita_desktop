﻿using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Contracts {
    public interface IComprobanteFiscalPrinterService {
        /// <summary>
        /// Representacion impresa del objeto detail del sistema
        /// </summary>
        /// <param name="comprobante">ComprobanteFiscalDetailModel</param>
        ComprobanteFiscalPrinter Printer(ComprobanteFiscalDetailModel comprobante);

        /// <summary>
        /// Representacion impresa del objeto detail del sistema
        /// </summary>
        /// <param name="cfdi">cfdi version 3.3</param>
        ComprobanteFiscalPrinter Printer(SAT.CFDI.V33.Comprobante cfdi);

        /// <summary>
        /// Representacion impresa del objeto detail del sistema
        /// </summary>
        /// <param name="cfdi">cfdi version 4.0</param>
        ComprobanteFiscalPrinter Printer(SAT.CFDI.V40.Comprobante cfdi);
    }
}