﻿using Jaeger.Domain.Comprobante.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Aplication.Comprobante.Contracts {
    public interface IComprobanteFiscalSerializerService {
        /// <summary>
        /// obtener comprobante fiscal detalle
        /// </summary>
        /// <param name="index">indice</param>
        ComprobanteFiscalDetailModel GetComprobante(int index);
        bool Serializar(int year, int month = 0);
    }
}
