﻿using System.Collections.Generic;

namespace Jaeger.Aplication.Comprobante.Contracts {
    public interface ICertificadoService {
        Domain.Certificado.Entities.CertificadoModel Save(Domain.Certificado.Entities.CertificadoModel model);

        IEnumerable<T1> GetList<T1>(List<Domain.Base.Builder.IConditional> conditionals) where T1 : class, new();
    }
}
