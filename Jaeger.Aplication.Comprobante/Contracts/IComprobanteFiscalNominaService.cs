﻿using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using System.ComponentModel;

namespace Jaeger.Aplication.Comprobante {
    public interface IComprobanteFiscalNominaService {
        BindingList<SerieFolioModel> GetSeries(bool onlyActive);
        IComprobanteFiscalNominaModel Save(IComprobanteFiscalNominaModel item);
    }
}
