﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante {
    /// <summary>
    /// Comprobante Fiscal Digital
    /// </summary>
    public interface IComprobanteFiscalService {
        /// <summary>
        /// listado de series disponibles para comprobantes
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        BindingList<SerieFolioModel> GetSeries(bool onlyActive);

        /// <summary>
        /// obtener comprobante fiscal detalle
        /// </summary>
        /// <param name="index">indice</param>
        ComprobanteFiscalDetailModel GetComprobante(int index);

        /// <summary>
        /// almacenar comprobante fiscal
        /// </summary>
        ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel comprobante);

        /// <summary>
        /// Timbrar comprobante fiscal
        /// </summary>
        ComprobanteFiscalDetailModel Procesar(ComprobanteFiscalDetailModel model, ref int codigo, ref string mensaje);

        /// <summary>
        /// Cancelar comprobante fiscal
        /// </summary>
        IComprobanteFiscalSingleModel Cancelar(IComprobanteFiscalSingleModel model, string claveCancelacion);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new();

        string Upload(string base64, string localFileName, string keyName);

        bool Exits(string url);

        bool UpdateUrlXml(int index, string url);

        bool UpdateUrlPdf(int index, string url);

        bool UpdateUrlXmlAcuse(int index, string url);

        bool UpdateUrlPdfAcuse(int index, string url);

        List<ComprobanteFiscalDetailSingleModel> GetComprobante1(string[] uuid);
    }
}
