﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Contracts {
    public interface IComprobantesFiscalesSearchService {
        BindingList<ComprobanteFiscalDetailSingleModel> GetSearch(string rfc, string text);

        BindingList<ComprobanteFiscalDetailSingleModel> GetCFDI(string rfc, int year = 0, int month = 0);

        BindingList<ComprobanteFiscalDetailSingleModel> SearchUUID(List<string> vs);
    }
}
