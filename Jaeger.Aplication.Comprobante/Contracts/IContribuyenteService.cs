﻿using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Contracts {
    /// <summary>
    /// servicio de directorio
    /// </summary>
    public interface IContribuyenteService {
        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        BindingList<ComprobanteContribuyenteModel> GetContribuyentes();
    }
}
