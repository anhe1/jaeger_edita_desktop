﻿using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Aplication.Comprobante.Contracts {
    public interface IFolioYSeriesService {
        BindingList<SerieFolioModel> GetSeries(bool onlyActive);
    }
}
