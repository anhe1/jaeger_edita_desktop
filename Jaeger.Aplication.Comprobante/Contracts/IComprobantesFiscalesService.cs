﻿using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Aplication.Comprobante.Contracts {
    public interface IComprobantesFiscalesService : IComprobanteFiscalService, ICertificacionService {
        CFDISubTipoEnum GetSubTipo { get; set; }

        //BindingList<ComprobanteFiscalDetailSingleModel> GetSearch(int iddirectorio, string rfc, string[] status);

        /// <summary>
        /// obtener resumen de comprobantes fiscales para tabla dinamica
        /// </summary>
        /// <param name="anio">año</param>
        /// <param name="mes">mes</param>
        BindingList<EstadoCuentaSingleView> GetResumen(int year, int month = 0);

        System.Data.DataTable GetResumen(int year, string[] estado, string rfc = "");

        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        BindingList<ComprobanteConceptoDetailModel> GetConceptos(int index);

        /// <summary>
        /// obtener el estado del comprobante fiscal mediante el servicio SAT
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="emisor">RFC del emisor del comprobante</param>
        /// <param name="receptor">RFC del receptor del comprobante</param>
        /// <param name="total">total del comprobante</param>
        /// <param name="idDocumento">folio fiscal del comprobante (uuid)</param>
        /// <returns>string que representa el estado del comprobante</returns>
        string EstadoSAT(int indice, string emisor, string receptor, decimal total, string idDocumento);

        /// <summary>
        /// actualizar status
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="status">nuevo status</param>
        /// <returns>verdadero si la operacion fue realizada</returns>
        bool UpdateStatus(int indice, string status);

        ComprobanteFiscalPrinter Printer(IComprobanteSingle model);

        ComprobanteFiscalPrinter Printer(ComprobanteFiscalDetailSingleModel model);

        

        ComprobanteFiscalPrinter ToPrinter(ComprobanteFiscalDetailSingleModel model);

        string ToHtml(IComprobanteSingle model);

        string ToHtml(ComprobanteFiscalDetailSingleModel model);

        void Dispose();

        #region crear
        /// <summary>
        /// crear tablas relacionadas al complemento de recepcion de pagos
        /// </summary>
        bool CrearTablaRecepcionPago();

        /// <summary>
        /// crear tablas relacionadas al complemento de carta porte
        /// </summary>
        bool CrearTablaCartaPorte();

        BindingList<ComprobanteBackup> GetBackups(CFDISubTipoEnum subTipo);
        #endregion

        #region Serializar
        IComprobanteFiscalDetailModel Serializar(ComprobanteFiscalDetailSingleModel singleModel);

        bool SerializarConceptos(ComprobanteFiscalDetailSingleModel singleModel);

        bool Serializar(ComprobanteFiscalDetailSingleModel singleModel, bool full = false);

        /// <summary>
        /// verificar complemento de pagos
        /// </summary>
        BindingList<ComplementoPagoDetailModel> Verificar(BindingList<ComplementoPagoDetailModel> model);

        ComprobanteCfdiRelacionadosModel Verificar(ComprobanteCfdiRelacionadosModel model);
        #endregion

        #region parches
        bool ParcheComplemento(int year);
        #endregion
    }
}
