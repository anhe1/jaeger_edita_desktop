﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.DataAccess.Empresa.Repositories {
    public class SqlAreaRepository : MySqlSugarContext<AreaModel>, ISqlAreaRepository {
        public SqlAreaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public AreaModel Save(AreaModel model) {
            model.FechaNuevo = System.DateTime.Now;
            model.Creo = this.User;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
