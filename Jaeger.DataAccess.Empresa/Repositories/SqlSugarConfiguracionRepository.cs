﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.Services;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.DataAccess.Empresa.Repositories {
    /// <summary>
    /// repositorio de configuraciones
    /// </summary>
    public class SqlSugarConfiguracionRepository : SqlSugarContext<EmpresaConfiguracionModel>, ISqlConfiguracionRepository {

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        public SqlSugarConfiguracionRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        ~SqlSugarConfiguracionRepository() {
            if (this.Db != null) {
                //this.Db.Close();
            }
        }

        /// <summary>
        /// obtener objeto de configuracion por la llave key
        /// </summary>
        public EmpresaConfiguracionModel GetByKey(string key1) {
            try {
                return this.Db.Queryable<EmpresaConfiguracionModel>().Where(it => it.Key == key1).Single();
            } catch (Exception ex) {
                LogErrorService.LogWrite(ex.Message);
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// obtener objeto de configuracion por la llave key
        /// </summary>
        //public EmpresaConfiguracionModel GetByKey(KeyConfiguracionEnum key) {
        //    return this.GetByKey(Enum.GetName(typeof(KeyConfiguracionEnum), key).ToLower());
        //}

        /// <summary>
        /// almacenar
        /// </summary>
        public EmpresaConfiguracionModel Save(EmpresaConfiguracionModel item) {
            var exite = this.GetByKey(item.Key);
            if (exite != null) {
                item.Id = exite.Id;
                return this.Db.Saveable<EmpresaConfiguracionModel>(item).ExecuteReturnEntity();
            } else {
                return this.Db.Saveable<EmpresaConfiguracionModel>(item).ExecuteReturnEntity();
            }
        }
    }
}
