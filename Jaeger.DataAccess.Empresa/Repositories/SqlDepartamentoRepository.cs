﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.DataAccess.Empresa.Repositories {
    /// <summary>
    /// repositorio para departamentos
    /// </summary>
    public class SqlDepartamentoRepository : MySqlSugarContext<DepartamentoModel>, ISqlDepartamentoRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        /// <param name="user"></param>
        public SqlDepartamentoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public DepartamentoModel Save(DepartamentoModel departamento) {
            if (departamento.IdDepartamento == 0) {
                departamento.Creo = this.User;
                departamento.FechaNuevo = DateTime.Now;
                departamento.IdDepartamento = this.Insert(departamento);
            } else {
                departamento.Creo = this.User;
                departamento.FechaNuevo = DateTime.Now;
                this.Update(departamento);
            }
            return departamento;
        }
    }
}
