﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Empresa.Enums;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Empresa.Repositories {
    /// <summary>
    /// repositorio de parametros de configuracion
    /// </summary>
    public class SqlParametroRepository : MySqlSugarContext<ParametroModel>, ISqlParametroRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">IDataBase</param>
        public SqlParametroRepository(Domain.DataBase.Entities.DataBaseConfiguracion configuracion) : base(configuracion) { }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT CONF.* FROM CONF @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="group">Llave de grupo</param>
        public IEnumerable<IParametroModel> Get(ConfigGroupEnum group) {
            return this.Db.Queryable<ParametroModel>().Where(it => it.Group == (ConfigGroupEnum)group).ToList();
        }

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="groups">array de llave de grupos</param>
        public IEnumerable<IParametroModel> Get(ConfigGroupEnum[] groups) {
            return this.Db.Queryable<ParametroModel>().Where(it => groups.Contains(it.Group)).ToList();
        }

        /// <summary>
        /// obtener lista de llaves 
        /// </summary>
        /// <param name="keys">array de indices de llaves</param>
        public IEnumerable<IParametroModel> Get(ConfigKeyEnum[] keys) {
            return this.Db.Queryable<ParametroModel>().Where(it => keys.Contains(it.Key)).ToList();
        }

        /// <summary>
        /// almacenar parametro
        /// </summary>
        /// <param name="parameter">IParameter</param>
        /// <returns>verdadero</returns>
        public bool Save(IParametroModel parameter) {
            var group = (int)parameter.Group;
            var key = (int)parameter.Key;
            var response = this.Db.Ado.ExecuteCommand($"INSERT INTO CONF (CONF_KEY, CONF_GRP, CONF_DATA) VALUES('{key}',{group},'{parameter.Data}') ON DUPLICATE KEY UPDATE CONF_DATA = '{parameter.Data}', CONF_KEY = '{key}', CONF_GRP={group}");
            return response > 0;
        }

        /// <summary>
        /// almacenar lista de parametros
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Save(List<IParametroModel> parameters) {
            foreach (var parameter in parameters) {
                var d0 = (int)parameter.Group;
                var d1 = (int)parameter.Key;
                this.Db.Ado.ExecuteCommand(string.Format("INSERT INTO CONF (CONF_KEY, CONF_GRP, CONF_DATA) VALUES('{0}',{1},'{2}') ON DUPLICATE KEY UPDATE CONF_DATA = '{3}', CONF_KEY = '{0}', CONF_GRP={1}",
                    d1.ToString(), d0.ToString(), parameter.Data, parameter.Data));
            }

            return true;
        }
    }
}
