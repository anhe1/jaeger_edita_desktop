﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.Empresa.Repositories {
    public class MySqlConfiguracionRepository : RepositoryMaster<EmpresaConfiguracionModel>, ISqlConfiguracionRepository {
        public MySqlConfiguracionRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public EmpresaConfiguracionModel GetById(int index) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select * from _conf where _conf_key = @index limit 1"
            };

            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<EmpresaConfiguracionModel>();

            return mapper.Map(tabla).First();
        }

        public EmpresaConfiguracionModel GetByKey(string key) {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select * from _conf where _conf_key = @key limit 1"
            };

            sqlCommand.Parameters.AddWithValue("@key", key);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<EmpresaConfiguracionModel>();

            return mapper.Map(tabla).First();
        }

        //public EmpresaConfiguracionModel GetByKey(KeyConfiguracionEnum key) {
        //    return this.GetByKey(Enum.GetName(typeof(KeyConfiguracionEnum), key).ToLower());
        //}

        public IEnumerable<EmpresaConfiguracionModel> GetList() {
            var sqlCommand = new MySqlCommand() {
                CommandText = "select * from _conf where _conf_key = @key limit 1"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<EmpresaConfiguracionModel>();
            throw new NotImplementedException();
        }

        public int Insert(EmpresaConfiguracionModel item) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @""
            };

            sqlCommand.Parameters.AddWithValue("@", item.Key);
            sqlCommand.Parameters.AddWithValue("@", item.Data);
            throw new NotImplementedException();
        }

        public int Update(EmpresaConfiguracionModel item) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @""
            };

            sqlCommand.Parameters.AddWithValue("@", item.Key);
            sqlCommand.Parameters.AddWithValue("@", item.Data);
            throw new NotImplementedException();
        }

        public bool Delete(int index) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @""
            };

            sqlCommand.Parameters.AddWithValue("@", index);
            throw new NotImplementedException();
        }

        public EmpresaConfiguracionModel Save(EmpresaConfiguracionModel item) {
            if (item.Id == 0) {
                item.Id = this.Insert(item);
            } else {
                this.Update(item);
            }
            throw new NotImplementedException();
        }
    }
}
