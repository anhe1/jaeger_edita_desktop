﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Builder {
    /// <summary>
    /// Constructor de configuración
    /// </summary>
    public class ConfigurationBuilder : Domain.Empresa.Builder.ConfigurationBuilder, Domain.Empresa.Builder.IConfigurationBuilder, Domain.Empresa.Builder.IConfigurationBuild {
        protected internal IConfiguration _Configuration;

        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigurationBuilder() {
            this._Configuration = new Configuration();
        }

        /// <summary>
        /// Construir configuración
        /// </summary>
        /// <param name="parametros">IParameters</param>
        /// <returns>IConfiguratioin</returns>
        public override Domain.Base.Contracts.IConfiguration Build(List<IParametroModel> parametros) {
            if (parametros.Count > 0) {
                foreach (var parametro in parametros) {
                    switch (parametro.Key) {
                        case Domain.Empresa.Enums.ConfigKeyEnum.ConfiguracionBD:
                            this._Configuration.DataBase = this.GetDataDB(parametro.Data);
                            break;
                        case Domain.Empresa.Enums.ConfigKeyEnum.ModoProductivo:
                            this._Configuration.ModoProductivo = DbConvert.ConvertBool(parametro.Data);
                            break;
                        case Domain.Empresa.Enums.ConfigKeyEnum.Folder:
                            this._Configuration.Folder = parametro.Data;
                            break;
                        default:
                            break;
                    }
                }
            }
            return this._Configuration;
        }

        /// <summary>
        /// Construir configuración
        /// </summary>
        /// <param name="configuration">IConfiguration</param>
        /// <returns>Parameters</returns>
        public override List<IParametroModel> Build(Domain.Base.Contracts.IConfiguration configuration) {
            this._Configuration = (IConfiguration)configuration;
            var lParameters = new List<IParametroModel> {
                new ParametroModel().WithKey(Domain.Empresa.Enums.ConfigKeyEnum.Folder).WithGroup(Domain.Empresa.Enums.ConfigGroupEnum.Nomina).With(this._Configuration.Folder),
                new ParametroModel().WithKey(Domain.Empresa.Enums.ConfigKeyEnum.ModoProductivo).WithGroup(Domain.Empresa.Enums.ConfigGroupEnum.Nomina).With(this._Configuration.ModoProductivo.ToString()),
                new ParametroModel().WithKey(Domain.Empresa.Enums.ConfigKeyEnum.ConfiguracionBD).WithGroup(Domain.Empresa.Enums.ConfigGroupEnum.Nomina).With(this._Configuration.DataBase.Json())
            };
            return lParameters;
        }

        /// <summary>
        /// Obtener configuración de base de datos
        /// </summary>
        /// <param name="data">string</param>
        /// <returns>IDataBase</returns>
        protected Domain.DataBase.Contracts.IDataBaseConfiguracion GetDataDB(string data) {
            if (!string.IsNullOrEmpty(data)) {
                var IData = new Domain.DataBase.Entities.DataBaseConfiguracion().WithString(data);
                return IData;
            }
            return null;
        }
    }
}
