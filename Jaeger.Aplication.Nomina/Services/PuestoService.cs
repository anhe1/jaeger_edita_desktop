﻿using System.Collections.Generic;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Services {
    public class PuestoService : IPuestoService {
        protected Domain.Nomina.Contracts.ISqlPuestoRepository puestoRepository;
        public PuestoService() : base() {
            this.OnLoad();
        }

        protected virtual void OnLoad() { }

        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            if (typeof(T1) == typeof(PuestoModel)) {
                return this.puestoRepository.GetList<T1>(conditionales);
            }
            return this.puestoRepository.GetList<T1>(conditionales);
        }

        public PuestoModel Save(PuestoModel model) {
            return this.puestoRepository.Save(model);
        }
    }
}
