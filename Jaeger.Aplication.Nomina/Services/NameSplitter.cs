﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Jaeger.Aplication.Nomina.Services {
	public class NameSplitter {

		private char _separator;

		public NameSplitter(char separator) {
			_separator = separator;
		}

		private List<string> particles = new List<string> {
		"DE",
		"DEL",
		"LA",
		"LOS",
		"LAS",
		"Y",
		"SAN",
		"SANTI",
		"SANTO",
		"SANTA",
		"MA",
		"VON",
		"MC",
		"MAC"

	};
		public PersonFullName SplitFullNameStartingWithPaterno(string fullName) {
			string rest = Regex.Replace(fullName, @"\s+", " ");
			string paterno = ProcessNamePart(ref rest);
			string materno = ProcessNamePart(ref rest);
			string nombre = rest.Trim(_separator);

			if (rest.Length == 0) {
				nombre = materno;
				materno = string.Empty;
			}

			return new PersonFullName(paterno, materno, nombre);
		}

		public PersonFullName SplitFullNameStartingWithName(string fullName) {
			string rest = Regex.Replace(fullName, @"\s+", " ");
			string materno = ProcessNamePartInverse(ref rest);
			string paterno = ProcessNamePartInverse(ref rest);
			string nombre = rest.Trim(_separator);

			if (rest.Length == 0) {
				var temp = paterno;
				paterno = materno;
				nombre = temp;
				materno = string.Empty;
			}

			return new PersonFullName(paterno, materno, nombre);
		}

		private string ProcessNamePart(ref string rest) {
			StringBuilder res = new StringBuilder();
			var firstWord = true;
			var includeNext = true;
			string word;

			while (includeNext && rest.Length > 0) {
				rest = rest.Trim(_separator);
				word = null;

				if (rest.IndexOf(_separator) == -1)
					word = rest;
				else
					word = rest.Substring(0, rest.IndexOf(_separator));

				rest = rest.Substring(word.Length, rest.Length - word.Length);
				includeNext = particles.Contains(word.ToUpper()) || word.Contains(".");

				if (!firstWord)
					res.Append(_separator);
				res.Append(word);
				firstWord = false;
			}

			return res.ToString();
		}

		private string ProcessNamePartInverse(ref string rest) {
			StringBuilder res = new StringBuilder();

			var includeNext = true;

			while (includeNext) {
				rest = rest.Trim(_separator);
				string word;
				string nextWord;
				if (rest.IndexOf(_separator) == -1) {
					word = rest;
					rest = String.Empty;
					nextWord = String.Empty;
				} else {
					word = rest.Substring(rest.LastIndexOf(_separator) + 1);
					rest = rest.Substring(0, rest.LastIndexOf(_separator));
					nextWord = rest.Substring(rest.LastIndexOf(_separator) + 1);
				}

				includeNext = particles.Contains(nextWord.ToUpper()) || nextWord.Contains(".");
				res.Insert(0, word);
				if (includeNext)
					res.Insert(0, _separator);
			}

			return res.ToString();
		}
	}
}
