﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Services {
    public class PeriodoService : Contracts.IPeriodoService {
        protected ISqlPeriodoRepository periodoRepository;

        public PeriodoService() {
            this.OnLoad();
        }

        protected virtual void OnLoad() {
         
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.periodoRepository.GetList<T1>(conditionals);
        }

        public INominaPeriodoDetailModel Save(INominaPeriodoDetailModel model) {
            var response = this.periodoRepository.Save(model);
            if (response != null) {
                if (response.IdPeriodo > 0) {
                    var a = this.periodoRepository.CrearPeriodo(model);
                }
            }
            return response;
        }

        public static IPeriodoQueryBuilder Query() {
            return new PeriodoQueryBuilder();
        }
    }
}
