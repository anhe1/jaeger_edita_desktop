﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Services {
    public class ParametrosService : IParametrosService {
        protected ISqlNominaConfiguracionRepository configuracionRepository;

        public ParametrosService() {
            this.OnLoad();
        }

        protected virtual void OnLoad() {

        }

        public IConfiguracionDetailModel GetConfiguracion() {
            return (IConfiguracionDetailModel)this.configuracionRepository.GetList<ConfiguracionDetailModel>(Query().Last().Build()).FirstOrDefault();
        }

        public IConfiguracionDetailModel Save(IConfiguracionDetailModel currentConf) {
            return this.configuracionRepository.Save(currentConf);
        }

        public IConfigurationQueryBuilder Query() {
            return new ConfigurationQueryBuilder();
        }
    }
}
