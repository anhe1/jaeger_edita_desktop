﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.DataAccess.Repositories;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Nomina.Services {
    /// <summary>
    /// servicio para control de empleados
    /// </summary>
    public class EmpleadoService : IEmpleadoService {
        #region
        protected internal ISqlEmpleadoRepository empleadoRepository;
        protected internal ISqlEmpleadoContratoRepository contratoRepository;
        protected internal ISqlEmpleadoCuentaBancariaRepository cuentaBancariaRepository;
        protected internal ISqlEmpleadoConceptoNominaRepository conceptoNominaRepository;
        protected internal ISqlNominaConceptoRepository nominaConceptoRepository;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public EmpleadoService() {
            this.OnLoad();
        }

        protected virtual void OnLoad() {
            this.empleadoRepository = new SqlSugarEmpleadosRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener lista de objetos
        /// </summary>
        /// <typeparam name="T1">T1</typeparam>
        /// <param name="conditionals">condicionantes</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(EmpleadoDetailModel)) {
                return this.empleadoRepository.GetList<T1>(conditionals).ToList<T1>();
            } else if (typeof(T1) == typeof(NominaConceptoDetailModel)) {
                return this.nominaConceptoRepository.GetList<T1>(conditionals);
            }
            return null;
        }

        /// <summary>
        /// almacenar registro de empleado
        /// </summary>
        /// <param name="model">IEmpleadoModel</param>
        /// <returns>IEmpleadoModel</returns>
        public IEmpleadoDetailModel Save(IEmpleadoDetailModel model) {
            model = this.empleadoRepository.Save(model);
            if (model.IdEmpleado > 0) {
                if (model.Contratos != null) {
                    for (int i = 0; i < model.Contratos.Count; i++) {
                        model.Contratos[i].IdEmpleado = model.IdEmpleado;
                        model.Contratos[i] = this.contratoRepository.Save(model.Contratos[i] as ContratoModel);
                    }
                }
                if (model.Conceptos != null) {
                    for (int i = 0; i < model.Conceptos.Count; i++) {
                        model.Conceptos[i].IdEmpleado = model.IdEmpleado;
                        model.Conceptos[i] = this.conceptoNominaRepository.Save(model.Conceptos[i] as EmpleadoConceptoNominaModel);
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// remover registro de empleado
        /// </summary>
        /// <param name="model">IEmpleadoModel</param>
        /// <returns>IEmpleadoModel</returns>
        public IEmpleadoDetailModel Remove(IEmpleadoDetailModel model) {
            model.Activo = false;
            return this.Save(model);
        }

        /// <summary>
        /// importar archivo de empleados
        /// </summary>
        /// <param name="fileName">nombre del archivo</param>
        /// <returns>IEmpleado</returns>
        public IEmpleadoDetailModel Importar(FileInfo fileName) {
            var comprobante = SAT.Reader.CFD.CFDReader.Reader(fileName);

            if (comprobante != null) {
                if (comprobante.Nomina == null)
                    return null;

                if (comprobante.Nomina.Count == 0)
                    return null;

                if (comprobante.Nomina[0] != null) {
                    IEmpleadoDetailModel nuevoEmpleado = new EmpleadoDetailModel();
                    var nombreCompleto = new NameSplitter(' ');
                    var nombre = nombreCompleto.SplitFullNameStartingWithName(comprobante.ReceptorNombre);
                    nuevoEmpleado.Nombre = nombre.Name;
                    nuevoEmpleado.ApellidoMaterno = nombre.Materno;
                    nuevoEmpleado.ApellidoPaterno = nombre.Paterno;
                    nuevoEmpleado.Clave = "";
                    nuevoEmpleado.RFC = comprobante.ReceptorRFC;
                    nuevoEmpleado.CURP = comprobante.Nomina[0].Receptor.Curp;
                    nuevoEmpleado.NSS = comprobante.Nomina[0].Receptor.NumSeguridadSocial;

                    IContratoModel contrato = new ContratoModel {
                        FecInicioRelLaboral = comprobante.Nomina[0].Receptor.FechaInicioRelLaboral,
                        Departamento = comprobante.Nomina[0].Receptor.Departamento,
                        ClaveTipoRegimen = comprobante.Nomina[0].Receptor.TipoRegimen,
                        IdPremio = 0,
                        ClaveRiesgoPuesto = comprobante.Nomina[0].Receptor.RiesgoPuesto,
                        ClaveTipoContrato = comprobante.Nomina[0].Receptor.TipoContrato,
                        SalarioDiario = comprobante.Nomina[0].Receptor.SalarioBaseCotApor / new decimal(1.0452),
                        SalarioDiarioIntegrado = comprobante.Nomina[0].Receptor.SalarioDiarioIntegrado,
                        SalarioBaseCotizacion = comprobante.Nomina[0].Receptor.SalarioBaseCotApor,
                        Jornadas = 7,
                        Horas = 8,
                        Puesto = comprobante.Nomina[0].Receptor.Puesto,
                        ClavePeriodicidadPago = comprobante.Nomina[0].Receptor.PeriodicidadPago,
                        ClaveTipoJornada = comprobante.Nomina[0].Receptor.TipoJornada
                    };
                    nuevoEmpleado.Contratos = new BindingList<IContratoModel> {
                 contrato
             };

                    return nuevoEmpleado;
                }
            }
            return null;
        }

        public void Prueba() {
            var carpeta = @"C:\Jaeger\Jaeger.Repositorio\CIP1805304S5\Emitidos\2023\11\30";
            List<string> archivos = Directory.GetFiles(carpeta, "*.xml", SearchOption.AllDirectories).ToList<string>();

            foreach (var item in archivos) {
                var comprobante = SAT.CFDI.V40.Comprobante.Load(item);
                if (comprobante != null) {
                    if (comprobante.Complemento.Nomina != null) {
                        IEmpleadoDetailModel nuevoEmpleado = new EmpleadoDetailModel();
                        var _t = new NameSplitter(' ');
                        var nombre = _t.SplitFullNameStartingWithName(comprobante.Receptor.Nombre);
                        nuevoEmpleado.Nombre = nombre.Name;
                        nuevoEmpleado.ApellidoMaterno = nombre.Materno;
                        nuevoEmpleado.ApellidoPaterno = nombre.Paterno;
                        nuevoEmpleado.Clave = "";
                        nuevoEmpleado.RFC = comprobante.Receptor.Rfc;
                        nuevoEmpleado.CURP = comprobante.Complemento.Nomina.Receptor.Curp;
                        nuevoEmpleado.NSS = comprobante.Complemento.Nomina.Receptor.NumSeguridadSocial;

                        var contrato = new ContratoModel {
                            FecInicioRelLaboral = comprobante.Complemento.Nomina.Receptor.FechaInicioRelLaboral,
                            Departamento = comprobante.Complemento.Nomina.Receptor.Departamento,
                            ClaveTipoRegimen = comprobante.Complemento.Nomina.Receptor.TipoRegimen,
                            IdPremio = 0,
                            ClaveRiesgoPuesto = comprobante.Complemento.Nomina.Receptor.RiesgoPuesto,
                            ClaveTipoContrato = comprobante.Complemento.Nomina.Receptor.TipoContrato,
                            SalarioDiario = comprobante.Complemento.Nomina.Receptor.SalarioBaseCotApor / new decimal(1.0452),
                            SalarioDiarioIntegrado = comprobante.Complemento.Nomina.Receptor.SalarioDiarioIntegrado,
                            SalarioBaseCotizacion = comprobante.Complemento.Nomina.Receptor.SalarioBaseCotApor,
                            Jornadas = 7,
                            Horas = 8,
                            Puesto = comprobante.Complemento.Nomina.Receptor.Puesto,
                            ClavePeriodicidadPago = comprobante.Complemento.Nomina.Receptor.PeriodicidadPago,
                            ClaveTipoJornada = comprobante.Complemento.Nomina.Receptor.TipoJornada
                        };
                        nuevoEmpleado = this.empleadoRepository.Save(nuevoEmpleado);
                        if (nuevoEmpleado.IdEmpleado > 0) {
                            contrato.IdEmpleado = nuevoEmpleado.IdEmpleado;
                            this.contratoRepository.Save(contrato);
                        }
                    }
                }
            }
        }

        #region metodos estaticos
        public static IEmpleadoQueryBuilder Query() {
            return new EmpleadoQueryBuilder();
        }

        public static List<GeneroModel> GetGeneros() {
            return ((GeneroEnum[])Enum.GetValues(typeof(GeneroEnum))).Select(e => new GeneroModel((int)e, e.ToString())).ToList();
        }

        public static List<EstadoModel> GetEstado() {
            return ((EstadoEnum[])Enum.GetValues(typeof(EstadoEnum))).Select(e => new EstadoModel((int)e, e.ToString())).ToList();
        }
        #endregion
    }
}
