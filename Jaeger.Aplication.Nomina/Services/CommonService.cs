﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Aplication.Nomina.Services {
    /// <summary>
    /// clase de servicios comunes
    /// </summary>
    public static class CommonService {
        #region periodos
        public static List<NominaPeriodoTipoModel> GetPeriodos() {
            return ((PeriodoTipoEnum[])Enum.GetValues(typeof(PeriodoTipoEnum))).Select(c => new NominaPeriodoTipoModel((int)c, c.ToString())).ToList();
        }

        public static List<TipoNominaModel> GetTiposNomina() {
            return ((NominaTipoEnum[])Enum.GetValues(typeof(NominaTipoEnum))).Select(c => new TipoNominaModel((int)c, c.ToString())).ToList();
        }

        public static List<PeriodoStatusModel> GetPeriodoStatus() {
            return ((PeriodoStatusEnum[])Enum.GetValues(typeof(PeriodoStatusEnum))).Select(c => new
            PeriodoStatusModel() {
                Id = (int)c,
                Descripcion = c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description
            }).ToList();
        }
        #endregion

        #region conceptos de nomina
        public static List<AplicacionModel> GetAplicacion() {
            return ((ConceptoAplicacionEnum[])Enum.GetValues(typeof(ConceptoAplicacionEnum))).Select(c => new AplicacionModel((int)c, c.ToString())).ToList();
        }

        public static List<ConceptoBaseFiscal> GetConceptoBase() {
            return ((ConceptoBaseEnum[])Enum.GetValues(typeof(ConceptoBaseEnum))).Select(c => new
            ConceptoBaseFiscal() {
                Id = (int)c,
                Descripcion = c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description
            }).ToList();
        }

        public static List<ConceptoTipoModel> GetConceptoTipo() {
            return ((NominaConceptoTipoEnum[])Enum.GetValues(typeof(NominaConceptoTipoEnum))).Select(c => new ConceptoTipoModel((int)c, c.ToString())).ToList();
        }
        #endregion

        #region funciones comunes
        /// <summary>
        /// devulve el numero de la semana correspondiente a la fecha
        /// </summary>
        public static int NumeroSemana(DateTime fechaInput) {
            // formatea la fecha de acuerdo a la zona del computador
            CultureInfo cul = CultureInfo.CurrentCulture;
            // Usa la fecha formateada y calcula el número de la semana
            return cul.Calendar.GetWeekOfYear(
                 fechaInput,
                 CalendarWeekRule.FirstDay,
                 DayOfWeek.Monday);
        }

        /// <summary>
        /// funcion para devolver el primer dia de la semana
        /// </summary>
        public static DateTime GetPrimerDiaSemana() {
            DateTime dt = DateTime.Now.AddDays(-6);
            DateTime wkStDt = DateTime.MinValue;
            wkStDt = dt.AddDays(1 - Convert.ToDouble(dt.DayOfWeek));
            DateTime fechadesdesemana = wkStDt.Date;
            return fechadesdesemana;
        }
        #endregion
    }
}
