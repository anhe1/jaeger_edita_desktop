﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Aplication.Nomina.Services {
    public class RegistroAusenciasService : IRegistroAusenciasService {
        protected internal ISqlRegistroAusenciasRepository registroDiasRepository;
        protected internal ISqlDiaFestivoRepository diaFestivoRepository;
        protected internal ISqlEmpleadoRepository empleadoRepository;

        public RegistroAusenciasService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {

        }

        public IRegistroAusenciasModel Save(IRegistroAusenciasModel model) {
            this.registroDiasRepository.Save(model);
            return model;
        }

        public List<IRegistroAusenciasModel> Save(List<IRegistroAusenciasModel> models) {
            return this.registroDiasRepository.Save(models);
        }

        public List<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(EmpleadoModel)) {
                return this.empleadoRepository.GetList<T1>(conditionals).ToList();
            }else if (typeof(T1) ==typeof(EmpleadoVacacionesModel)) {
                var response = this.registroDiasRepository.GetList<T1>(conditionals).ToList<T1>();
                var dias = this.registroDiasRepository.GetList<RegistroAusenciasModel>(new List<IConditional>());
                for (int i = 0; i < response.Count; i++) {
                    ((IEmpleadoVacacionesModel)response[i]).Ausencias = new BindingList<IRegistroAusenciasModel>(dias.Where(it => it.IdEmpleado == ((IEmpleadoVacacionesModel)response[i]).IdEmpleado).ToList<IRegistroAusenciasModel>());
                }
                return response;
            }
            return this.registroDiasRepository.GetList<T1>(conditionals).ToList<T1>();
        }

        public List<IRegistroAusenciasModel> GetList(int idPeriodo) {
            return this.registroDiasRepository.GetList<RegistroAusenciasModel>(Query().IdNomina(idPeriodo).Build()).ToList<IRegistroAusenciasModel>();
        }

        public List<IDiaFestivoModel> GetDiasFestivos() {
            return this.diaFestivoRepository.GetList().ToList<IDiaFestivoModel>();
        }

        public static IRegistroAusenciasQueryBuilder Query() {
            return new RegistroAusenciasQueryBuilder();
        }

        /// <summary>
        /// tipo de dias o ausencias
        /// </summary>
        public static List<DiasTipoModel> TipoDias() {
            return EnumerationExtension.GetEnumToClass<DiasTipoModel, DiasTipoEnum>().ToList();
        }
    }
}
