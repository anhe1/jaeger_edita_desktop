﻿using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Nomina.Services {
    public class ControlService : IControlService {
        protected BackgroundWorker preparar;
        protected ISqlControlNominaRepository controlNominaRepository;
        protected ISqlComprobanteNominaRepository comprobanteNominaRepository;

        public ControlService() {
            this.comprobanteNominaRepository = new SqlSugarComprobanteNominaRepository(ConfigService.Synapsis.RDS.Edita);
            this.controlNominaRepository = new SqlSugarControlNominaRepository(ConfigService.Synapsis.RDS.Edita);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ComprobanteNominaSingleModel)) {
                return this.comprobanteNominaRepository.GetList<T1>(conditionals);
            } if (typeof(T1) == typeof(DepartamentoSingleModel)) {
                return this.comprobanteNominaRepository.GetList<T1>(conditionals);
            } else if (typeof(T1) == typeof(EmpleadoSingleModel)) {
                return this.comprobanteNominaRepository.GetList<T1>(conditionals);
            }
            return null;
        }

        /// <summary>
        /// obtener listado de comprobantes de nómina por el indice del control 
        /// </summary>
        /// <param name="index">indice de control (_cfdnmn_nmnctrl_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        public List<ComprobanteNominaSingleModel> GetCfdNominas(int index, bool onlyActive = true) {
            return this.comprobanteNominaRepository.GetSearchBy(index, onlyActive).ToList();
        }

        public List<ControlNominaModel> GetNominas() {
            return this.controlNominaRepository.GetList(true).ToList();
        }

        public DataTable GetResumen(int indice) {
            return this.comprobanteNominaRepository.GetResumen(indice);
        }

        public DataTable GetResumen(int year, int month) {
            return this.comprobanteNominaRepository.GetResumen(year, month, -1);
        }

        public static INominaControlQueryBuilder Query() {
            return new NominaControlQueryBuilder();
        }
    }
}
