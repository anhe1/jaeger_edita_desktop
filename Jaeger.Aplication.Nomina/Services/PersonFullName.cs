﻿namespace Jaeger.Aplication.Nomina.Services {
    public struct PersonFullName {

		public string _paterno;
		public string _materno;
		public string _name;

		public PersonFullName(string paterno, string materno, string name) {
			_paterno = paterno;
			_materno = materno;
			_name = name;
		}
		public string Paterno {
			get { return _paterno; }
		}
		public string Materno {
			get { return _materno; }
		}
		public string Name {
			get { return _name; }
		}

		public override string ToString() {
			return string.Format("{0} {1} {2}", _name, _paterno, _materno);
		}

	}
}
