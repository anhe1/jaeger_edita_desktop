﻿using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Services {
    /// <summary>
    /// Servicio de configuración de la aplicación
    /// </summary>
    public class ConfigurationService : Empresa.Service.ConfigurationService, Empresa.Contracts.IConfigurationService, IConfigurationService {
        /// <summary>
        /// constructor
        /// </summary>
        public ConfigurationService() : base() { }

        public IConfiguration Get() {
            var parametros = this.Get(Domain.Empresa.Enums.ConfigGroupEnum.Nomina);
            var configuration = new Builder.ConfigurationBuilder().Build(parametros);
            return Validate((IConfiguration)configuration);
        }

        public IConfiguration Set(IConfiguration configuration) {
            var parametros = new Builder.ConfigurationBuilder().Build(configuration);
            this.Set(parametros);
            return configuration;
        }

        #region metodos estaticos
        public static IConfiguration Validate(IConfiguration configuration) {
            if (configuration == null) {
                return null;
            }
            return configuration;
        }
        #endregion
    }
}
