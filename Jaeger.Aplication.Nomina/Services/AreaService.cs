﻿using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.DataAccess.Empresa.Repositories;

namespace Jaeger.Aplication.Nomina.Services {
    public class AreaService : Contracts.IAreaService {
        protected Domain.Empresa.Contracts.ISqlAreaRepository areaRepository;
        
        public AreaService() : base() { this.OnLoad(); }

        protected virtual void OnLoad() {
            this.areaRepository = new SqlAreaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            return this.areaRepository.GetList<T1>(conditionales);
        }

        public AreaModel Save(AreaModel model) {
            return this.areaRepository.Save(model);
        }
    }
}
