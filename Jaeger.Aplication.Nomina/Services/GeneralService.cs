﻿using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Services {
    public class GeneralService : Base.Contracts.IConfigService {
        /// <summary>
        /// configuración general de la aplicación
        /// </summary>
        public static IConfiguration Configuration { get; set; }

        public static bool Validate() {
            var s0 = new ConfigurationService();
            GeneralService.Configuration = new Domain.Nomina.Entities.Configuration();
            GeneralService.Configuration = s0.Get();
            if (GeneralService.Configuration.DataBase == null) {
                return false;
            }
            return Configuration != null;
        }
    }
}
