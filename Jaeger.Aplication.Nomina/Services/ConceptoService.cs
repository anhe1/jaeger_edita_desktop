﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Builder;

namespace Jaeger.Aplication.Nomina.Services {
    public class ConceptoService : IConceptoService {
        protected ISqlNominaConceptoRepository conceptoRepository;

        public ConceptoService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            //this.conceptoRepository = new SqlNominaConceptoRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public BindingList<INominaConceptoDetailModel> GetList(bool onlyActive) {
            return new BindingList<INominaConceptoDetailModel>(this.conceptoRepository.GetList<NominaConceptoDetailModel>(
                new List<IConditional> { new Conditional("NMCNP_ID", "0", ConditionalTypeEnum.GreaterThan) }
                ).ToList<INominaConceptoDetailModel>());
        }

        public INominaConceptoDetailModel Save(INominaConceptoDetailModel model) {
            return this.conceptoRepository.Save(model);
        }

        public static IConceptoQueryBuilder Query() {
            return new ConceptoQueryBuilder();
        }
    }
}
