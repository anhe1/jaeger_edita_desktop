﻿using System.Collections.Generic;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Aplication.Nomina.Services {
    /// <summary>
    /// Departamento service interface
    /// </summary>
    public class DepartamentoService : IDepartamentoService {
        protected Domain.Empresa.Contracts.ISqlDepartamentoRepository departamentoRepository;
        protected Domain.Empresa.Contracts.ISqlAreaRepository areaRepository;

        /// <summary>
        /// constructor
        /// </summary>
        public DepartamentoService() {
            this.OnLoad();
        }
        protected virtual void OnLoad() {
            //this.departamentoRepository = new SqlDepartamentoRepository((Domain.DataBase.Entities.DataBaseConfiguracion)GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model">IDepartamentoModel</param>
        /// <returns>IDepartamentoModel</returns>
        public DepartamentoModel Save(DepartamentoModel model) {
            return this.departamentoRepository.Save(model);
        }

        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            if (typeof(T1) == typeof(AreaModel)) {
                return this.areaRepository.GetList<T1>(conditionales);
            }
            return this.departamentoRepository.GetList<T1>(conditionales);
        }
    }
}
