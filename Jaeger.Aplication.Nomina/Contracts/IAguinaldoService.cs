﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IAguinaldoService {
        BindingList<IEmpleadoCalcularAguinaldo> GetAguinaldos(int year);

     //   List<BancomerNominaRegistro> LayoutBanco(BindingList<IEmpleadoCalcularAguinaldo> empleados);
    }
}
