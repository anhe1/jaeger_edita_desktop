﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface ICuentaBancariaService {
        EmpleadoCuentaBancariaModel GetById(int index);
        BindingList<EmpleadoCuentaBancariaModel> GetList(bool onlyActive);
        EmpleadoCuentaBancariaModel Save(EmpleadoCuentaBancariaModel model);

        List<CuentaBancariaTipoModel> GetTipos();

        List<MonedaModel> GetMonedas();
    }
}
