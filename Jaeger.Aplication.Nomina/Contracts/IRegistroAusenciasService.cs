﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IRegistroAusenciasService {
        IRegistroAusenciasModel Save(IRegistroAusenciasModel model);

        List<IRegistroAusenciasModel> Save(List<IRegistroAusenciasModel> models);

        List<IRegistroAusenciasModel> GetList(int idPeriodo);

        List <T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        List<IDiaFestivoModel> GetDiasFestivos();
    }
}
