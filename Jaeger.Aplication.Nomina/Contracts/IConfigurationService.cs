﻿using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    /// <summary>
    /// Servicio de configuración de la aplicación
    /// </summary>
    public interface IConfigurationService : Empresa.Contracts.IConfigurationService {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>IConfiguration</returns>
        IConfiguration Get();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration">IConfiguration</param>
        /// <returns>IConfiguration</returns>
        IConfiguration Set(IConfiguration configuration);
    }
}
