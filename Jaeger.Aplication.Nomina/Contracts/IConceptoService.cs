﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IConceptoService {
        INominaConceptoDetailModel Save(INominaConceptoDetailModel model);
        BindingList<INominaConceptoDetailModel> GetList(bool onlyActive);
    }
}
