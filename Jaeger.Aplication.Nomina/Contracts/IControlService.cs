﻿using System.Data;
using System.Collections.Generic;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IControlService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener listado de comprobantes de nómina por el indice del control 
        /// </summary>
        /// <param name="index">indice de control (_cfdnmn_nmnctrl_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        List<ComprobanteNominaSingleModel> GetCfdNominas(int index, bool onlyActive = true);

        List<ControlNominaModel> GetNominas();

        DataTable GetResumen(int indice);

        DataTable GetResumen(int year, int month);
    }
}
