﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IContratoService {

        IContratoModel GetById(int id);

        bool Remove(int id);

        BindingList<IContratoModel> GetList();

        BindingList<IContratoDetailModel> GetList(bool onlyActive);

        IContratoModel Save(IContratoModel model);
    }
}
