﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IPuestoService {
        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new();

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PuestoModel Save(PuestoModel model);
    }
}
