﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface ISalarioMinimoService {
        BindingList<SalarioMinimoModel> GetList();

        BindingList<SalarioMinimoModel> Save(BindingList<SalarioMinimoModel> models);
    }
}
