﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface ISubsidioAlEmpleoService {
        BindingList<TablaSubsidioAlEmpleoModel> GetList();

        BindingList<TablaSubsidioAlEmpleoModel> Save(BindingList<TablaSubsidioAlEmpleoModel>  models);
    }
}
