﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IHorasExtraService {
        void OnLoad();
        List<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        IRegistroHoraExtraModel Save(IRegistroHoraExtraModel model);
    }
}
