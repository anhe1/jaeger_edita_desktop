﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface ICalculoService {
        BindingList<INominaPeriodoEmpleadoDetailModel> GetList(int idPeriodo);
        INominaPeriodoDetailModel Save(INominaPeriodoDetailModel currentPeriodo);

        INominaPeriodoDetailModel GetPeriodo(int idPeriodo);

        INominaPeriodoDetailModel GetLast();

        bool CalcularPeriodo(INominaPeriodoDetailModel model);
    }
}
