﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    /// <summary>
    /// servicio para control de empleados
    /// </summary>
    public interface IEmpleadoService {
        /// <summary>
        /// obtener lista de objetos
        /// </summary>
        /// <typeparam name="T1">T1</typeparam>
        /// <param name="conditionals">condicionantes</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// almacenar registro de empleado
        /// </summary>
        /// <param name="model">IEmpleadoModel</param>
        /// <returns>IEmpleadoModel</returns>
        IEmpleadoDetailModel Save(IEmpleadoDetailModel model);

        /// <summary>
        /// remover registro de empleado
        /// </summary>
        /// <param name="model">IEmpleadoModel</param>
        /// <returns>IEmpleadoModel</returns>
        IEmpleadoDetailModel Remove(IEmpleadoDetailModel model);

        /// <summary>
        /// importar archivo de empleados
        /// </summary>
        /// <param name="fileName">nombre del archivo</param>
        /// <returns>IEmpleado</returns>
        IEmpleadoDetailModel Importar(System.IO.FileInfo fileName);
    }
}
