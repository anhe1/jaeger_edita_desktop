﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    /// <summary>
    /// Departamento service interface
    /// </summary>
    public interface IDepartamentoService {
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model">IDepartamentoModel</param>
        /// <returns>IDepartamentoModel</returns>
        DepartamentoModel Save(DepartamentoModel model);

        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new();
    }
}
