﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IUMAService {
        BindingList<UMAModel> GetList();

        BindingList<UMAModel> Save(BindingList<UMAModel> models);
    }
}
