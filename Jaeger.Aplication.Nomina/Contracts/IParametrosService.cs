﻿using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IParametrosService {
        IConfiguracionDetailModel GetConfiguracion();

        IConfiguracionDetailModel Save(IConfiguracionDetailModel currentConf);
    }
}
