﻿using System.Collections.Generic;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface IPeriodoService {
        IEnumerable<T1> GetList<T1>(List<Domain.Base.Builder.IConditional> conditionals) where T1 : class, new();

        INominaPeriodoDetailModel Save(INominaPeriodoDetailModel model);
    }
}
