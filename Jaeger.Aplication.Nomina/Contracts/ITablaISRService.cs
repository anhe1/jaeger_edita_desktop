﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Contracts {
    public interface ITablaISRService {
        BindingList<TablaImpuestoSobreRentaModel> GetList();

        void Prueba();

        BindingList<TablaImpuestoSobreRentaModel> Save(BindingList<TablaImpuestoSobreRentaModel> tabla);
    }
}
