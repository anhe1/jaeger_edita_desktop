﻿using System;
using System.Threading;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Helpers;
using Jaeger.Aplication.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Helpers {
    public class ImporterWorker : Base.Abstracts.WorkerBase, IImporterWorker, IWorker {
        protected IEmpleadoService Service;

        public ImporterWorker(IEmpleadoService service) : base("ImporterWorker") {
            this.Service = service;
        }

        #region implementacion
        public override bool SupportsDualProgress {
            get { return false; }
        }

        public override bool IsCancelable {
            get { return false; }
        }

        public override object Result {
            get { return null; }
        }

        public override void BeginWork() {
            if (_worker != null && _worker.IsAlive)
                throw new ApplicationException($"{this._workerName} is already in progress");

            _cancelled = false;

            ThreadStart ts = delegate {
                try {

                    this.NotifyPrimaryProgress(false, 30, "Cargando archivos ...");
                    this.NotifyPrimaryProgress(true, 100, "Terminado.");
                } catch (UserCancellationException cex) {
                    this.NotifyPrimaryProgress(true, 100, cex);
                } catch (Exception ex) {
                    this.NotifyPrimaryProgress(true, 100, ex);
                }
            };
            this.Thread(ts).Start();
        }
        #endregion

        public void Execute(Jaeger.SAT.Reader.CFD.DocumentoFiscal documentoFiscal) {
            throw new NotImplementedException();
        }
    }
}
