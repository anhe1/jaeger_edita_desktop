﻿using System;
using System.Threading;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Nomina.Contracts;

namespace Jaeger.Aplication.Nomina.Helpers {
    public class ReaderWorker : Base.Abstracts.WorkerBase, IWorker, IReaderWorker {
        /// <summary>
        /// constructor
        /// </summary>
        public ReaderWorker() : base("ReaderWorker") {

        }

        #region implementacion
        public override object Result {
            get {
                return null;
            }
        }

        public override bool SupportsDualProgress {
            get { return true; }
        }

        public override bool IsCancelable {
            get {
                return false;
            }
        }

        public override void BeginWork() {
            if (_worker != null && _worker.IsAlive)
                throw new ApplicationException($"{this._workerName} is already in progress");

            _cancelled = false;

            ThreadStart ts = delegate {
                try {

                } catch (Exception) {

                    throw;
                }
            };
        }
        #endregion
    }
}
