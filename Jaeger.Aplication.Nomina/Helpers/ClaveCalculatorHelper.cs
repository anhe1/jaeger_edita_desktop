﻿using System;
using System.Text;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Aplication.Nomina.Helpers {
    public class ClaveCalculatorHelper {
        private EmpleadoModel person;

        private static readonly string[] SpecialParticles = {
            "DE", "LA", "LAS", "MC", "VON", "DEL", "LOS", "Y", "MAC", "VAN", "MI"};

        private static readonly string[] ForbiddenWords = {
            "BUEI", "BUEY", "CACA", "CACO", "CAGA", "KOGE", "KAKA", "MAME", "KOJO", "KULO",
            "CAGO", "COGE", "COJE", "COJO", "FETO", "JOTO", "KACO", "KAGO", "MAMO", "MEAR", "MEON",
            "MION", "MOCO", "MULA", "PEDA", "PEDO", "PENE", "PUTA", "PUTO", "QULO", "RATA", "RUIN", "GUTO"
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="person"></param>
        public ClaveCalculatorHelper(EmpleadoModel person) {
            this.person = person;
        }

        public ClaveCalculatorHelper() {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Calculate(EmpleadoModel person) {
            this.person = person;
            return ObfuscateForbiddenWords(NameCode());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Calculate() {
            return ObfuscateForbiddenWords(NameCode());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameCode"></param>
        /// <returns></returns>
        private string ObfuscateForbiddenWords(string nameCode) {
            foreach (var forbidden in ForbiddenWords) {
                if (forbidden.Equals(nameCode)) {
                    return string.Format("{0}{1}", nameCode.Substring(0, 3), "X");
                }
            }

            return nameCode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string NameCode() {
            if (IsFirstLastNameEmpty()) {
                return FirstLastNameEmptyForm();
            } else {
                if (IsSecondLastNameEmpty()) {
                    return SecondLastNameEmptyForm();
                } else {
                    if (IsFirstLastNameIsTooShort()) {
                        return FirstLastNameTooShortForm();
                    } else {
                        return NormalForm();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsFirstLastNameEmpty() {
            return string.IsNullOrEmpty(Normalize(person.ApellidoPaterno));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsSecondLastNameEmpty() {
            return string.IsNullOrEmpty(Normalize(person.ApellidoMaterno));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string FirstLastNameEmptyForm() {
            return
                FirstTwoLettersOf(person.ApellidoMaterno) +
                FirstTwoLettersOf(FilterName(person.Nombre));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string SecondLastNameEmptyForm() {
            return
                FirstTwoLettersOf(person.ApellidoPaterno) +
                FirstTwoLettersOf(FilterName(person.Nombre));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsFirstLastNameIsTooShort() {
            return Normalize(person.ApellidoPaterno).Length <= 2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string FirstLastNameTooShortForm() {
            return
                FirstLetterOf(person.ApellidoPaterno) +
                FirstLetterOf(person.ApellidoMaterno) +
                FirstTwoLettersOf(FilterName(person.Nombre));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string NormalForm() {
            return
                FirstTwoLettersOf(FilterName(person.Nombre)) +
                FirstTwoLettersOf(person.ApellidoPaterno);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private string FirstTwoLettersOf(string word) {
            string normalizedWord = Normalize(word);

            return normalizedWord.Substring(0, 2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string FilterName(string name) {
            bool isPopularGivenName = false;
            string rawName = Normalize(name).Trim();

            if (rawName.Contains(" ")) {
                isPopularGivenName = rawName.StartsWith("MARIA") || rawName.StartsWith("JOSE");

                if (isPopularGivenName) {
                    return rawName.Split(' ')[1];
                }
            }
            return name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private string FirstLetterOf(string word) {
            string normalizedWord = Normalize(word);

            return normalizedWord[0].ToString() + normalizedWord[1].ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private string Normalize(string word) {
            if (string.IsNullOrEmpty(word)) {
                return word;
            } else {
                string normalizedWord = UtilsHelper.StripAccents(word).ToUpper();
                return RemoveSpecialParticles(normalizedWord, SpecialParticles);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="normalizedWord"></param>
        /// <param name="SpecialParticles"></param>
        /// <returns></returns>
        private string RemoveSpecialParticles(string normalizedWord, string[] SpecialParticles) {
            StringBuilder newWord = new StringBuilder(normalizedWord);

            foreach (var particle in SpecialParticles) {
                var particlePositions = new[] { particle + " ", " " + particle };

                foreach (var p in particlePositions) {
                    while (newWord.ToString().Contains(p)) {
                        int i = newWord.ToString().IndexOf(p);
                        try {

                            newWord.Remove(i, i + p.ToString().Length);
                        } catch (Exception) {
                            break;
                        }
                    }
                }
            }

            return newWord.ToString();
        }
    }
}
