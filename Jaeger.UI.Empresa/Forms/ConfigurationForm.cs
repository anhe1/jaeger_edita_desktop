﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Empresa.Forms {
    /// <summary>
    /// Formulario de configuración de la empresa
    /// </summary>
    public partial class ConfigurationForm : RadForm {
        public ConfigurationForm() {
            InitializeComponent();
        }

        private void ConfigurationForm_Load(object sender, EventArgs e) {
            this.Title.Text = string.Format(this.Title.Text, ConfigService.Synapsis.Empresa.RFC);
            this.SynapsisProperty.SelectedObject = ConfigService.Synapsis;
        }

        private void TConfiguration_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TConfiguration_Guardar_Click(object sender, EventArgs e) {

        }
    }
}
