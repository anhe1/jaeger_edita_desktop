﻿
namespace Jaeger.UI.Empresa.Forms {
    partial class ConfigurationEmisorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationEmisorForm));
            this.picHeader = new System.Windows.Forms.PictureBox();
            this.lblNombreComercial = new Telerik.WinControls.UI.RadLabel();
            this.txtRegimenCapital = new Telerik.WinControls.UI.RadTextBox();
            this.lblRegimenCapital = new Telerik.WinControls.UI.RadLabel();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.txtNombreComercial = new Telerik.WinControls.UI.RadTextBox();
            this.grpDatosUbicacion = new Telerik.WinControls.UI.RadGroupBox();
            this.txtReferencia = new Telerik.WinControls.UI.RadTextBox();
            this.lblReferencia = new Telerik.WinControls.UI.RadLabel();
            this.txtLocalidad = new Telerik.WinControls.UI.RadTextBox();
            this.lblLocalidad = new Telerik.WinControls.UI.RadLabel();
            this.txtTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.lblTelefono = new Telerik.WinControls.UI.RadLabel();
            this.lblCiudad = new Telerik.WinControls.UI.RadLabel();
            this.txtCiudad = new Telerik.WinControls.UI.RadTextBox();
            this.txtCorreo = new Telerik.WinControls.UI.RadTextBox();
            this.lblTipoVialidad = new Telerik.WinControls.UI.RadLabel();
            this.lblCorreo = new Telerik.WinControls.UI.RadLabel();
            this.lblNumInterior = new Telerik.WinControls.UI.RadLabel();
            this.lblNumExterior = new Telerik.WinControls.UI.RadLabel();
            this.txtNumInterior = new Telerik.WinControls.UI.RadTextBox();
            this.txtNumExterior = new Telerik.WinControls.UI.RadTextBox();
            this.lblEntidad = new Telerik.WinControls.UI.RadLabel();
            this.lblNombreVialidad = new Telerik.WinControls.UI.RadLabel();
            this.txtCodigoPostal = new Telerik.WinControls.UI.RadTextBox();
            this.lblColonia = new Telerik.WinControls.UI.RadLabel();
            this.lblMunicipio = new Telerik.WinControls.UI.RadLabel();
            this.txtColonia = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoPostal = new Telerik.WinControls.UI.RadLabel();
            this.txtNombreVialidad = new Telerik.WinControls.UI.RadTextBox();
            this.txtEntidad = new Telerik.WinControls.UI.RadTextBox();
            this.txtTipoVialidad = new Telerik.WinControls.UI.RadTextBox();
            this.txtMunicipio = new Telerik.WinControls.UI.RadTextBox();
            this.TEmisor_Guardar = new Telerik.WinControls.UI.RadButton();
            this.TEmisor_Cerrar = new Telerik.WinControls.UI.RadButton();
            this.TEmisor_CIF = new Telerik.WinControls.UI.RadButton();
            this.txtRFC = new Telerik.WinControls.UI.RadTextBox();
            this.txtCIF = new Telerik.WinControls.UI.RadTextBox();
            this.lblCURP = new Telerik.WinControls.UI.RadLabel();
            this.grpInformation = new Telerik.WinControls.UI.RadGroupBox();
            this.txtRegistroPatronal = new Telerik.WinControls.UI.RadTextBox();
            this.lblRegimen = new Telerik.WinControls.UI.RadLabel();
            this.Regimen = new Telerik.WinControls.UI.RadDropDownList();
            this.lblRegistroPatronal = new Telerik.WinControls.UI.RadLabel();
            this.lblCIF = new Telerik.WinControls.UI.RadLabel();
            this.RegimenFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblRegimenFiscal = new Telerik.WinControls.UI.RadLabel();
            this.txtCURP = new Telerik.WinControls.UI.RadTextBox();
            this.Encabezado = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.picHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegimenCapital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenCapital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDatosUbicacion)).BeginInit();
            this.grpDatosUbicacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocalidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocalidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCiudad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCiudad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumInterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumInterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColonia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMunicipio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColonia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEmisor_Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEmisor_Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEmisor_CIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpInformation)).BeginInit();
            this.grpInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistroPatronal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Regimen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistroPatronal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // picHeader
            // 
            this.picHeader.BackColor = System.Drawing.Color.White;
            this.picHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.picHeader.Location = new System.Drawing.Point(0, 0);
            this.picHeader.Name = "picHeader";
            this.picHeader.Size = new System.Drawing.Size(556, 45);
            this.picHeader.TabIndex = 0;
            this.picHeader.TabStop = false;
            // 
            // lblNombreComercial
            // 
            this.lblNombreComercial.Location = new System.Drawing.Point(8, 92);
            this.lblNombreComercial.Name = "lblNombreComercial";
            this.lblNombreComercial.Size = new System.Drawing.Size(103, 18);
            this.lblNombreComercial.TabIndex = 12;
            this.lblNombreComercial.Text = "Nombre Comercial:";
            this.lblNombreComercial.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // txtRegimenCapital
            // 
            this.txtRegimenCapital.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegimenCapital.Location = new System.Drawing.Point(303, 39);
            this.txtRegimenCapital.Name = "txtRegimenCapital";
            this.txtRegimenCapital.NullText = "Régimen Capital";
            this.txtRegimenCapital.Size = new System.Drawing.Size(117, 20);
            this.txtRegimenCapital.TabIndex = 3;
            this.txtRegimenCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRegimenCapital
            // 
            this.lblRegimenCapital.Location = new System.Drawing.Point(303, 20);
            this.lblRegimenCapital.Name = "lblRegimenCapital";
            this.lblRegimenCapital.Size = new System.Drawing.Size(104, 18);
            this.lblRegimenCapital.TabIndex = 2;
            this.lblRegimenCapital.Text = "Régimen de capital:";
            this.lblRegimenCapital.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombre.Location = new System.Drawing.Point(8, 39);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.NullText = "Denominación o Razón Social";
            this.txtNombre.Size = new System.Drawing.Size(290, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(8, 20);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(157, 18);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Denominación o Razón Social:";
            this.lblNombre.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(8, 66);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 6;
            this.lblRFC.Text = "RFC:";
            this.lblRFC.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // txtNombreComercial
            // 
            this.txtNombreComercial.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreComercial.Location = new System.Drawing.Point(8, 117);
            this.txtNombreComercial.Name = "txtNombreComercial";
            this.txtNombreComercial.NullText = "Nombre Comercial";
            this.txtNombreComercial.Size = new System.Drawing.Size(330, 20);
            this.txtNombreComercial.TabIndex = 13;
            // 
            // grpDatosUbicacion
            // 
            this.grpDatosUbicacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpDatosUbicacion.Controls.Add(this.txtReferencia);
            this.grpDatosUbicacion.Controls.Add(this.lblReferencia);
            this.grpDatosUbicacion.Controls.Add(this.txtLocalidad);
            this.grpDatosUbicacion.Controls.Add(this.lblLocalidad);
            this.grpDatosUbicacion.Controls.Add(this.txtTelefono);
            this.grpDatosUbicacion.Controls.Add(this.lblTelefono);
            this.grpDatosUbicacion.Controls.Add(this.lblCiudad);
            this.grpDatosUbicacion.Controls.Add(this.txtCiudad);
            this.grpDatosUbicacion.Controls.Add(this.txtCorreo);
            this.grpDatosUbicacion.Controls.Add(this.lblTipoVialidad);
            this.grpDatosUbicacion.Controls.Add(this.lblCorreo);
            this.grpDatosUbicacion.Controls.Add(this.lblNumInterior);
            this.grpDatosUbicacion.Controls.Add(this.lblNumExterior);
            this.grpDatosUbicacion.Controls.Add(this.txtNumInterior);
            this.grpDatosUbicacion.Controls.Add(this.txtNumExterior);
            this.grpDatosUbicacion.Controls.Add(this.lblEntidad);
            this.grpDatosUbicacion.Controls.Add(this.lblNombreVialidad);
            this.grpDatosUbicacion.Controls.Add(this.txtCodigoPostal);
            this.grpDatosUbicacion.Controls.Add(this.lblColonia);
            this.grpDatosUbicacion.Controls.Add(this.lblMunicipio);
            this.grpDatosUbicacion.Controls.Add(this.txtColonia);
            this.grpDatosUbicacion.Controls.Add(this.lblCodigoPostal);
            this.grpDatosUbicacion.Controls.Add(this.txtNombreVialidad);
            this.grpDatosUbicacion.Controls.Add(this.txtEntidad);
            this.grpDatosUbicacion.Controls.Add(this.txtTipoVialidad);
            this.grpDatosUbicacion.Controls.Add(this.txtMunicipio);
            this.grpDatosUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDatosUbicacion.HeaderText = "Datos de Ubicación (domicilio fiscal, vigente)";
            this.grpDatosUbicacion.Location = new System.Drawing.Point(0, 193);
            this.grpDatosUbicacion.Name = "grpDatosUbicacion";
            this.grpDatosUbicacion.Size = new System.Drawing.Size(556, 180);
            this.grpDatosUbicacion.TabIndex = 2;
            this.grpDatosUbicacion.TabStop = false;
            this.grpDatosUbicacion.Text = "Datos de Ubicación (domicilio fiscal, vigente)";
            // 
            // txtReferencia
            // 
            this.txtReferencia.Location = new System.Drawing.Point(381, 149);
            this.txtReferencia.MaxLength = 40;
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.NullText = "Referencia";
            this.txtReferencia.Size = new System.Drawing.Size(163, 20);
            this.txtReferencia.TabIndex = 25;
            // 
            // lblReferencia
            // 
            this.lblReferencia.Location = new System.Drawing.Point(314, 150);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(61, 18);
            this.lblReferencia.TabIndex = 24;
            this.lblReferencia.Text = "Referencia:";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocalidad.Location = new System.Drawing.Point(70, 149);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.NullText = "Localidad";
            this.txtLocalidad.Size = new System.Drawing.Size(241, 20);
            this.txtLocalidad.TabIndex = 23;
            // 
            // lblLocalidad
            // 
            this.lblLocalidad.Location = new System.Drawing.Point(8, 150);
            this.lblLocalidad.Name = "lblLocalidad";
            this.lblLocalidad.Size = new System.Drawing.Size(56, 18);
            this.lblLocalidad.TabIndex = 22;
            this.lblLocalidad.Text = "Localidad:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(372, 123);
            this.txtTelefono.MaxLength = 40;
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.NullText = "Teléfono";
            this.txtTelefono.Size = new System.Drawing.Size(172, 20);
            this.txtTelefono.TabIndex = 21;
            // 
            // lblTelefono
            // 
            this.lblTelefono.Location = new System.Drawing.Point(314, 124);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 18);
            this.lblTelefono.TabIndex = 20;
            this.lblTelefono.Text = "Teléfono:";
            // 
            // lblCiudad
            // 
            this.lblCiudad.Location = new System.Drawing.Point(314, 98);
            this.lblCiudad.Name = "lblCiudad";
            this.lblCiudad.Size = new System.Drawing.Size(44, 18);
            this.lblCiudad.TabIndex = 16;
            this.lblCiudad.Text = "Ciudad:";
            // 
            // txtCiudad
            // 
            this.txtCiudad.BackColor = System.Drawing.SystemColors.Window;
            this.txtCiudad.Location = new System.Drawing.Point(364, 97);
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.NullText = "Ciudad";
            this.txtCiudad.Size = new System.Drawing.Size(180, 20);
            this.txtCiudad.TabIndex = 17;
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.SystemColors.Window;
            this.txtCorreo.Location = new System.Drawing.Point(115, 123);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.NullText = "usuario@dominio.com";
            this.txtCorreo.Size = new System.Drawing.Size(196, 20);
            this.txtCorreo.TabIndex = 19;
            // 
            // lblTipoVialidad
            // 
            this.lblTipoVialidad.Location = new System.Drawing.Point(8, 22);
            this.lblTipoVialidad.Name = "lblTipoVialidad";
            this.lblTipoVialidad.Size = new System.Drawing.Size(88, 18);
            this.lblTipoVialidad.TabIndex = 0;
            this.lblTipoVialidad.Text = "Tipo de vialidad:";
            // 
            // lblCorreo
            // 
            this.lblCorreo.Location = new System.Drawing.Point(8, 124);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(101, 18);
            this.lblCorreo.TabIndex = 18;
            this.lblCorreo.Text = "Correo electrónico:";
            // 
            // lblNumInterior
            // 
            this.lblNumInterior.Location = new System.Drawing.Point(202, 46);
            this.lblNumInterior.Name = "lblNumInterior";
            this.lblNumInterior.Size = new System.Drawing.Size(75, 18);
            this.lblNumInterior.TabIndex = 6;
            this.lblNumInterior.Text = "Núm. interior:";
            // 
            // lblNumExterior
            // 
            this.lblNumExterior.Location = new System.Drawing.Point(8, 46);
            this.lblNumExterior.Name = "lblNumExterior";
            this.lblNumExterior.Size = new System.Drawing.Size(77, 18);
            this.lblNumExterior.TabIndex = 4;
            this.lblNumExterior.Text = "Núm. exterior:";
            // 
            // txtNumInterior
            // 
            this.txtNumInterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumInterior.Location = new System.Drawing.Point(283, 45);
            this.txtNumInterior.Name = "txtNumInterior";
            this.txtNumInterior.NullText = "Núm. Interior";
            this.txtNumInterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumInterior.TabIndex = 7;
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumExterior.Location = new System.Drawing.Point(91, 45);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.NullText = "Núm. Interior";
            this.txtNumExterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumExterior.TabIndex = 5;
            // 
            // lblEntidad
            // 
            this.lblEntidad.Location = new System.Drawing.Point(314, 72);
            this.lblEntidad.Name = "lblEntidad";
            this.lblEntidad.Size = new System.Drawing.Size(101, 18);
            this.lblEntidad.TabIndex = 12;
            this.lblEntidad.Text = "Entidad Federativa:";
            // 
            // lblNombreVialidad
            // 
            this.lblNombreVialidad.Location = new System.Drawing.Point(211, 22);
            this.lblNombreVialidad.Name = "lblNombreVialidad";
            this.lblNombreVialidad.Size = new System.Drawing.Size(119, 18);
            this.lblNombreVialidad.TabIndex = 2;
            this.lblNombreVialidad.Text = "Nombre de la vialidad:";
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodigoPostal.Location = new System.Drawing.Point(474, 45);
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.NullText = "00000";
            this.txtCodigoPostal.Size = new System.Drawing.Size(70, 20);
            this.txtCodigoPostal.TabIndex = 9;
            this.txtCodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblColonia
            // 
            this.lblColonia.Location = new System.Drawing.Point(8, 72);
            this.lblColonia.Name = "lblColonia";
            this.lblColonia.Size = new System.Drawing.Size(47, 18);
            this.lblColonia.TabIndex = 10;
            this.lblColonia.Text = "Colonia:";
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Location = new System.Drawing.Point(8, 98);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(126, 18);
            this.lblMunicipio.TabIndex = 14;
            this.lblMunicipio.Text = "Municipio o delegación:";
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.SystemColors.Window;
            this.txtColonia.Location = new System.Drawing.Point(58, 71);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.NullText = "Colonia";
            this.txtColonia.Size = new System.Drawing.Size(253, 20);
            this.txtColonia.TabIndex = 11;
            // 
            // lblCodigoPostal
            // 
            this.lblCodigoPostal.Location = new System.Drawing.Point(393, 46);
            this.lblCodigoPostal.Name = "lblCodigoPostal";
            this.lblCodigoPostal.Size = new System.Drawing.Size(78, 18);
            this.lblCodigoPostal.TabIndex = 8;
            this.lblCodigoPostal.Text = "Codigo Postal:";
            // 
            // txtNombreVialidad
            // 
            this.txtNombreVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreVialidad.Location = new System.Drawing.Point(340, 21);
            this.txtNombreVialidad.Name = "txtNombreVialidad";
            this.txtNombreVialidad.NullText = "Nombre de la vialidad";
            this.txtNombreVialidad.Size = new System.Drawing.Size(204, 20);
            this.txtNombreVialidad.TabIndex = 3;
            // 
            // txtEntidad
            // 
            this.txtEntidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtEntidad.Location = new System.Drawing.Point(419, 71);
            this.txtEntidad.Name = "txtEntidad";
            this.txtEntidad.NullText = "Estado";
            this.txtEntidad.Size = new System.Drawing.Size(125, 20);
            this.txtEntidad.TabIndex = 13;
            // 
            // txtTipoVialidad
            // 
            this.txtTipoVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoVialidad.Location = new System.Drawing.Point(102, 21);
            this.txtTipoVialidad.Name = "txtTipoVialidad";
            this.txtTipoVialidad.NullText = "Tipo";
            this.txtTipoVialidad.Size = new System.Drawing.Size(103, 20);
            this.txtTipoVialidad.TabIndex = 1;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipio.Location = new System.Drawing.Point(140, 97);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.NullText = "Municipio o Delegación";
            this.txtMunicipio.Size = new System.Drawing.Size(171, 20);
            this.txtMunicipio.TabIndex = 15;
            // 
            // TEmisor_Guardar
            // 
            this.TEmisor_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TEmisor_Guardar.Location = new System.Drawing.Point(388, 389);
            this.TEmisor_Guardar.Name = "TEmisor_Guardar";
            this.TEmisor_Guardar.Size = new System.Drawing.Size(75, 23);
            this.TEmisor_Guardar.TabIndex = 4;
            this.TEmisor_Guardar.Text = "Guardar";
            this.TEmisor_Guardar.Click += new System.EventHandler(this.Button_Guardar_Click);
            // 
            // TEmisor_Cerrar
            // 
            this.TEmisor_Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TEmisor_Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TEmisor_Cerrar.Location = new System.Drawing.Point(469, 389);
            this.TEmisor_Cerrar.Name = "TEmisor_Cerrar";
            this.TEmisor_Cerrar.Size = new System.Drawing.Size(75, 23);
            this.TEmisor_Cerrar.TabIndex = 5;
            this.TEmisor_Cerrar.Text = "Cerrar";
            this.TEmisor_Cerrar.Click += new System.EventHandler(this.Button_Cerrar_Click);
            // 
            // TEmisor_CIF
            // 
            this.TEmisor_CIF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TEmisor_CIF.Image = global::Jaeger.UI.Empresa.Properties.Resources.pdf_16px;
            this.TEmisor_CIF.Location = new System.Drawing.Point(307, 389);
            this.TEmisor_CIF.Name = "TEmisor_CIF";
            this.TEmisor_CIF.Size = new System.Drawing.Size(75, 23);
            this.TEmisor_CIF.TabIndex = 3;
            this.TEmisor_CIF.Text = "CIF";
            this.TEmisor_CIF.Click += new System.EventHandler(this.Button_CIF_Click);
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(42, 65);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.NullText = "RFC";
            this.txtRFC.Size = new System.Drawing.Size(92, 20);
            this.txtRFC.TabIndex = 7;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCIF
            // 
            this.txtCIF.BackColor = System.Drawing.SystemColors.Window;
            this.txtCIF.Location = new System.Drawing.Point(447, 91);
            this.txtCIF.Name = "txtCIF";
            this.txtCIF.NullText = "Núm.";
            this.txtCIF.Size = new System.Drawing.Size(97, 20);
            this.txtCIF.TabIndex = 15;
            this.txtCIF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCURP
            // 
            this.lblCURP.Location = new System.Drawing.Point(137, 66);
            this.lblCURP.Name = "lblCURP";
            this.lblCURP.Size = new System.Drawing.Size(37, 18);
            this.lblCURP.TabIndex = 8;
            this.lblCURP.Text = "CURP:";
            this.lblCURP.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // grpInformation
            // 
            this.grpInformation.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpInformation.Controls.Add(this.txtRegistroPatronal);
            this.grpInformation.Controls.Add(this.lblRegimen);
            this.grpInformation.Controls.Add(this.Regimen);
            this.grpInformation.Controls.Add(this.txtNombreComercial);
            this.grpInformation.Controls.Add(this.lblRegistroPatronal);
            this.grpInformation.Controls.Add(this.lblNombre);
            this.grpInformation.Controls.Add(this.txtCIF);
            this.grpInformation.Controls.Add(this.lblCIF);
            this.grpInformation.Controls.Add(this.RegimenFiscal);
            this.grpInformation.Controls.Add(this.txtNombre);
            this.grpInformation.Controls.Add(this.lblRegimenFiscal);
            this.grpInformation.Controls.Add(this.lblRegimenCapital);
            this.grpInformation.Controls.Add(this.txtRegimenCapital);
            this.grpInformation.Controls.Add(this.lblNombreComercial);
            this.grpInformation.Controls.Add(this.txtRFC);
            this.grpInformation.Controls.Add(this.lblRFC);
            this.grpInformation.Controls.Add(this.txtCURP);
            this.grpInformation.Controls.Add(this.lblCURP);
            this.grpInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpInformation.HeaderText = "Información";
            this.grpInformation.Location = new System.Drawing.Point(0, 45);
            this.grpInformation.Name = "grpInformation";
            this.grpInformation.Size = new System.Drawing.Size(556, 148);
            this.grpInformation.TabIndex = 1;
            this.grpInformation.TabStop = false;
            this.grpInformation.Text = "Información";
            // 
            // txtRegistroPatronal
            // 
            this.txtRegistroPatronal.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegistroPatronal.Location = new System.Drawing.Point(447, 117);
            this.txtRegistroPatronal.Name = "txtRegistroPatronal";
            this.txtRegistroPatronal.NullText = "Reg. Patronal";
            this.txtRegistroPatronal.Size = new System.Drawing.Size(97, 20);
            this.txtRegistroPatronal.TabIndex = 17;
            // 
            // lblRegimen
            // 
            this.lblRegimen.Location = new System.Drawing.Point(426, 20);
            this.lblRegimen.Name = "lblRegimen";
            this.lblRegimen.Size = new System.Drawing.Size(48, 18);
            this.lblRegimen.TabIndex = 4;
            this.lblRegimen.Text = "Persona:";
            this.lblRegimen.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // Regimen
            // 
            this.Regimen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "NoDefinido";
            radListDataItem2.Text = "Fisica";
            radListDataItem3.Text = "Moral";
            this.Regimen.Items.Add(radListDataItem1);
            this.Regimen.Items.Add(radListDataItem2);
            this.Regimen.Items.Add(radListDataItem3);
            this.Regimen.Location = new System.Drawing.Point(426, 39);
            this.Regimen.Name = "Regimen";
            this.Regimen.NullText = "Selecciona";
            this.Regimen.Size = new System.Drawing.Size(118, 20);
            this.Regimen.TabIndex = 5;
            // 
            // lblRegistroPatronal
            // 
            this.lblRegistroPatronal.Location = new System.Drawing.Point(349, 118);
            this.lblRegistroPatronal.Name = "lblRegistroPatronal";
            this.lblRegistroPatronal.Size = new System.Drawing.Size(95, 18);
            this.lblRegistroPatronal.TabIndex = 16;
            this.lblRegistroPatronal.Text = "Registro Patronal:";
            this.lblRegistroPatronal.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblCIF
            // 
            this.lblCIF.Location = new System.Drawing.Point(404, 92);
            this.lblCIF.Name = "lblCIF";
            this.lblCIF.Size = new System.Drawing.Size(24, 18);
            this.lblCIF.TabIndex = 14;
            this.lblCIF.Text = "CIF:";
            this.lblCIF.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // RegimenFiscal
            // 
            // 
            // RegimenFiscal.NestedRadGridView
            // 
            this.RegimenFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.RegimenFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegimenFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RegimenFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            gridViewTextBoxColumn3.VisibleInColumnChooser = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.RegimenFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RegimenFiscal.EditorControl.Name = "NestedRadGridView";
            this.RegimenFiscal.EditorControl.ReadOnly = true;
            this.RegimenFiscal.EditorControl.ShowGroupPanel = false;
            this.RegimenFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.RegimenFiscal.EditorControl.TabIndex = 0;
            this.RegimenFiscal.Location = new System.Drawing.Point(392, 65);
            this.RegimenFiscal.Name = "RegimenFiscal";
            this.RegimenFiscal.NullText = "Régimen Fiscal";
            this.RegimenFiscal.Size = new System.Drawing.Size(152, 20);
            this.RegimenFiscal.TabIndex = 11;
            this.RegimenFiscal.TabStop = false;
            // 
            // lblRegimenFiscal
            // 
            this.lblRegimenFiscal.Location = new System.Drawing.Point(303, 66);
            this.lblRegimenFiscal.Name = "lblRegimenFiscal";
            this.lblRegimenFiscal.Size = new System.Drawing.Size(83, 18);
            this.lblRegimenFiscal.TabIndex = 10;
            this.lblRegimenFiscal.Text = "Régimen Fiscal:";
            this.lblRegimenFiscal.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // txtCURP
            // 
            this.txtCURP.BackColor = System.Drawing.SystemColors.Window;
            this.txtCURP.Location = new System.Drawing.Point(180, 65);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.NullText = "CURP";
            this.txtCURP.Size = new System.Drawing.Size(120, 20);
            this.txtCURP.TabIndex = 9;
            this.txtCURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Location = new System.Drawing.Point(12, 12);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(108, 18);
            this.Encabezado.TabIndex = 0;
            this.Encabezado.Text = "Información General";
            // 
            // ConfigurationEmisorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.TEmisor_Cerrar;
            this.ClientSize = new System.Drawing.Size(556, 424);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.grpDatosUbicacion);
            this.Controls.Add(this.grpInformation);
            this.Controls.Add(this.TEmisor_CIF);
            this.Controls.Add(this.TEmisor_Cerrar);
            this.Controls.Add(this.TEmisor_Guardar);
            this.Controls.Add(this.picHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigurationEmisorForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Configuración del emisor";
            this.Load += new System.EventHandler(this.ConfigurationEmisorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegimenCapital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenCapital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDatosUbicacion)).EndInit();
            this.grpDatosUbicacion.ResumeLayout(false);
            this.grpDatosUbicacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocalidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocalidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCiudad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCiudad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumInterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumInterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColonia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMunicipio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColonia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEmisor_Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEmisor_Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEmisor_CIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpInformation)).EndInit();
            this.grpInformation.ResumeLayout(false);
            this.grpInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistroPatronal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Regimen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistroPatronal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picHeader;
        private Telerik.WinControls.UI.RadLabel lblNombreComercial;
        private Telerik.WinControls.UI.RadTextBox txtRegimenCapital;
        private Telerik.WinControls.UI.RadLabel lblRegimenCapital;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        private Telerik.WinControls.UI.RadTextBox txtNombreComercial;
        private Telerik.WinControls.UI.RadGroupBox grpDatosUbicacion;
        private Telerik.WinControls.UI.RadTextBox txtCorreo;
        private Telerik.WinControls.UI.RadLabel lblTipoVialidad;
        private Telerik.WinControls.UI.RadLabel lblCorreo;
        private Telerik.WinControls.UI.RadLabel lblNumInterior;
        private Telerik.WinControls.UI.RadLabel lblNumExterior;
        private Telerik.WinControls.UI.RadTextBox txtNumInterior;
        private Telerik.WinControls.UI.RadTextBox txtNumExterior;
        private Telerik.WinControls.UI.RadLabel lblEntidad;
        private Telerik.WinControls.UI.RadLabel lblNombreVialidad;
        private Telerik.WinControls.UI.RadTextBox txtCodigoPostal;
        private Telerik.WinControls.UI.RadLabel lblColonia;
        private Telerik.WinControls.UI.RadLabel lblMunicipio;
        private Telerik.WinControls.UI.RadTextBox txtColonia;
        private Telerik.WinControls.UI.RadLabel lblCodigoPostal;
        private Telerik.WinControls.UI.RadTextBox txtNombreVialidad;
        private Telerik.WinControls.UI.RadTextBox txtEntidad;
        private Telerik.WinControls.UI.RadTextBox txtTipoVialidad;
        private Telerik.WinControls.UI.RadTextBox txtMunicipio;
        private Telerik.WinControls.UI.RadTextBox txtRFC;
        private Telerik.WinControls.UI.RadTextBox txtCIF;
        private Telerik.WinControls.UI.RadLabel lblCURP;
        private Telerik.WinControls.UI.RadGroupBox grpInformation;
        private Telerik.WinControls.UI.RadTextBox txtCURP;
        private Telerik.WinControls.UI.RadLabel lblCIF;
        private Telerik.WinControls.UI.RadTextBox txtRegistroPatronal;
        private Telerik.WinControls.UI.RadLabel lblRegistroPatronal;
        public Telerik.WinControls.UI.RadMultiColumnComboBox RegimenFiscal;
        public Telerik.WinControls.UI.RadLabel lblRegimenFiscal;
        public Telerik.WinControls.UI.RadDropDownList Regimen;
        public Telerik.WinControls.UI.RadLabel lblRegimen;
        private Telerik.WinControls.UI.RadLabel lblCiudad;
        private Telerik.WinControls.UI.RadTextBox txtCiudad;
        public Telerik.WinControls.UI.RadTextBox txtTelefono;
        public Telerik.WinControls.UI.RadLabel lblTelefono;
        private Telerik.WinControls.UI.RadLabel Encabezado;
        public Telerik.WinControls.UI.RadTextBox txtReferencia;
        public Telerik.WinControls.UI.RadLabel lblReferencia;
        private Telerik.WinControls.UI.RadTextBox txtLocalidad;
        private Telerik.WinControls.UI.RadLabel lblLocalidad;
        protected Telerik.WinControls.UI.RadButton TEmisor_Guardar;
        protected Telerik.WinControls.UI.RadButton TEmisor_Cerrar;
        protected Telerik.WinControls.UI.RadButton TEmisor_CIF;
    }
}