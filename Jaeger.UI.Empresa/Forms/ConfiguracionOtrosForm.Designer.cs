﻿
namespace Jaeger.UI.Empresa.Forms {
    partial class ConfiguracionOtrosForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfiguracionOtrosForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.lblTipoVialidad = new Telerik.WinControls.UI.RadLabel();
            this.lblNumExterior = new Telerik.WinControls.UI.RadLabel();
            this.txtNumExterior = new Telerik.WinControls.UI.RadTextBox();
            this.txtTipoVialidad = new Telerik.WinControls.UI.RadTextBox();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.bCerrar = new Telerik.WinControls.UI.RadButton();
            this.bGuardar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoVialidad)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bGuardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(364, 50);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.radPageViewPage1);
            this.tabControl1.Controls.Add(this.radPageViewPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 50);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedPage = this.radPageViewPage1;
            this.tabControl1.Size = new System.Drawing.Size(364, 352);
            this.tabControl1.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.groupBox1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(87F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(343, 304);
            this.radPageViewPage1.Text = "Base de Datos";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.radLabel1);
            this.groupBox1.Controls.Add(this.radTextBox1);
            this.groupBox1.Controls.Add(this.lblTipoVialidad);
            this.groupBox1.Controls.Add(this.lblNumExterior);
            this.groupBox1.Controls.Add(this.txtNumExterior);
            this.groupBox1.Controls.Add(this.txtTipoVialidad);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "groupBox1";
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(343, 304);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(5, 75);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(50, 18);
            this.radLabel1.TabIndex = 46;
            this.radLabel1.Text = "Servidor:";
            // 
            // radTextBox1
            // 
            this.radTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.radTextBox1.Location = new System.Drawing.Point(99, 74);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(103, 20);
            this.radTextBox1.TabIndex = 45;
            // 
            // lblTipoVialidad
            // 
            this.lblTipoVialidad.Location = new System.Drawing.Point(5, 25);
            this.lblTipoVialidad.Name = "lblTipoVialidad";
            this.lblTipoVialidad.Size = new System.Drawing.Size(48, 18);
            this.lblTipoVialidad.TabIndex = 42;
            this.lblTipoVialidad.Text = "User ID :";
            // 
            // lblNumExterior
            // 
            this.lblNumExterior.Location = new System.Drawing.Point(5, 49);
            this.lblNumExterior.Name = "lblNumExterior";
            this.lblNumExterior.Size = new System.Drawing.Size(56, 18);
            this.lblNumExterior.TabIndex = 44;
            this.lblNumExterior.Text = "Password:";
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumExterior.Location = new System.Drawing.Point(99, 48);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.Size = new System.Drawing.Size(103, 20);
            this.txtNumExterior.TabIndex = 43;
            // 
            // txtTipoVialidad
            // 
            this.txtTipoVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoVialidad.Location = new System.Drawing.Point(99, 24);
            this.txtTipoVialidad.Name = "txtTipoVialidad";
            this.txtTipoVialidad.Size = new System.Drawing.Size(103, 20);
            this.txtTipoVialidad.TabIndex = 41;
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.radGroupBox1);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(206F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(343, 304);
            this.radPageViewPage2.Text = "Proveedor Autorizado de Certificación";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radTextBox2);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.radTextBox3);
            this.radGroupBox1.Controls.Add(this.radTextBox4);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "Información";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(343, 304);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.TabStop = false;
            this.radGroupBox1.Text = "Información";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(5, 75);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(94, 18);
            this.radLabel2.TabIndex = 46;
            this.radLabel2.Text = "Modo productivo";
            // 
            // radTextBox2
            // 
            this.radTextBox2.BackColor = System.Drawing.SystemColors.Window;
            this.radTextBox2.Location = new System.Drawing.Point(105, 75);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(15, 15);
            this.radTextBox2.TabIndex = 45;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(5, 25);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(45, 18);
            this.radLabel3.TabIndex = 42;
            this.radLabel3.Text = "User ID:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(5, 49);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(92, 18);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "Secret Acces Key:";
            // 
            // radTextBox3
            // 
            this.radTextBox3.BackColor = System.Drawing.SystemColors.Window;
            this.radTextBox3.Location = new System.Drawing.Point(105, 48);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(129, 20);
            this.radTextBox3.TabIndex = 43;
            // 
            // radTextBox4
            // 
            this.radTextBox4.BackColor = System.Drawing.SystemColors.Window;
            this.radTextBox4.Location = new System.Drawing.Point(105, 24);
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(129, 20);
            this.radTextBox4.TabIndex = 41;
            // 
            // bCerrar
            // 
            this.bCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCerrar.Location = new System.Drawing.Point(277, 424);
            this.bCerrar.Name = "bCerrar";
            this.bCerrar.Size = new System.Drawing.Size(75, 23);
            this.bCerrar.TabIndex = 33;
            this.bCerrar.Text = "Cerrar";
            // 
            // bGuardar
            // 
            this.bGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGuardar.Location = new System.Drawing.Point(196, 424);
            this.bGuardar.Name = "bGuardar";
            this.bGuardar.Size = new System.Drawing.Size(75, 23);
            this.bGuardar.TabIndex = 34;
            this.bGuardar.Text = "Guardar";
            // 
            // ConfiguracionOtrosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 459);
            this.Controls.Add(this.bCerrar);
            this.Controls.Add(this.bGuardar);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfiguracionOtrosForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.ConfiguracionOtrosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoVialidad)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bGuardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadPageView tabControl1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadButton bCerrar;
        private Telerik.WinControls.UI.RadButton bGuardar;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadLabel lblTipoVialidad;
        private Telerik.WinControls.UI.RadLabel lblNumExterior;
        private Telerik.WinControls.UI.RadTextBox txtNumExterior;
        private Telerik.WinControls.UI.RadTextBox txtTipoVialidad;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadCheckBox radTextBox2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
    }
}