﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.SAT.CIF.Services;
using Jaeger.SAT.CIF.Entities;
using Jaeger.SAT.CIF.Helpers;
using Jaeger.SAT.CIF.Interfaces;

namespace Jaeger.UI.Empresa.Forms {
    /// <summary>
    /// Clase que representa el formulario de configuración del emisor.
    /// </summary>
    public partial class ConfigurationEmisorForm : RadForm {
        #region declaraciones
        protected internal Aplication.Empresa.Contracts.IConfigurationService _Service;
        protected internal Domain.Empresa.Entities.EmpresaDetalle _Configuracion;
        protected internal IResponse _Response;
        protected internal ICedulaFiscal _CedulaFiscal;
        protected internal IQueryService _CedulaFiscalService;
        protected internal IRequest _Request;
        private readonly Catalogos.Contracts.IRegimenesFiscalesCatalogo regimenesfiscales = new Catalogos.RegimenesFiscalesCatalogo();
        #endregion

        public ConfigurationEmisorForm(Domain.Base.Abstractions.UIMenuElement element) {
            InitializeComponent();
        }

        private void ConfigurationEmisorForm_Load(object sender, EventArgs e) {
            // catalogos
            this.regimenesfiscales.Load();

            this.RegimenFiscal.DisplayMember = "Descriptor";
            this.RegimenFiscal.ValueMember = "Clave";
            this.RegimenFiscal.AutoSizeDropDownToBestFit = true;
            this.RegimenFiscal.DataSource = this.regimenesfiscales.Items;

            using(var espera = new Common.Forms.Waiting2Form(this.Cargar)) {
                espera.Text = "Cargando...";
                espera.ShowDialog(this);
            }

            this.CreateBinding();
        }

        private void Button_Guardar_Click(object sender, EventArgs e) {
            this._Service.Set(this._Configuracion.Empresa.Build());
            this._Service.Set(this._Configuracion.DomicilioFiscal.Build());
        }

        private void Button_CIF_Click(object sender, EventArgs e) {
            var fileOpen = new OpenFileDialog() { Filter = "*.pdf|*.PDF|*.png|*.PNG|*.jpg|*.JPG" };
            if (fileOpen.ShowDialog(this) == DialogResult.OK) {
                if (Path.GetExtension(fileOpen.FileName) == ".png") {
                    var image = new Bitmap(fileOpen.FileName);
                    if (image != null) {
                        var d0 = new ServiceQR();
                        var url = d0.GetByQR(fileOpen.FileName);
                        if (url.Message.Contains("https://siat.sat.gob.mx/app/qr/faces/pages/mobile/validadorqr.jsf?D1")) {
                            IRequest request = Request.Create().AddURL(url.Message).Build();
                            this._Response = this._CedulaFiscalService.Execute(request);
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                } else if (Path.GetExtension(fileOpen.FileName) == ".pdf") {
                    var r = PDFExtractor.GetData(fileOpen.FileName);
                    if (r != null) {
                        if (r.Count > 0) {
                            this._Request = Request.Create().AddRFC(r["rfc"]).AddId(r["idcif"]).Build();
                            using (var espera = new Common.Forms.Waiting2Form(this.Buscar)) {
                                espera.Text = "Buscando...";
                                espera.ShowDialog(this);
                            }
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    } else {
                        MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                } else {
                    MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void Button_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Buscar() {
            if (this._CedulaFiscalService == null) {
                this._CedulaFiscalService = new QueryService();
            }

            this._Response = this._CedulaFiscalService.Execute(this._Request);
            if (this._Response.IsValida) {
                this._CedulaFiscal = this._Response.CedulaFiscal;
                this.Autoridad();
                //this.CreateBinding();
            } else {
                MessageBox.Show(this, this._Response.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void CreateBinding() {
            this.txtRFC.DataBindings.Add("Text", this._Configuracion.Empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCIF.DataBindings.Add("Text", this._Configuracion.Empresa, "CIF", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCURP.DataBindings.Add("Text", this._Configuracion.Empresa, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombre.DataBindings.Add("Text", this._Configuracion.Empresa, "RazonSocial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRegimenCapital.DataBindings.Add("Text", this._Configuracion.Empresa, "SociedadCapital", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombreComercial.DataBindings.Add("Text", this._Configuracion.Empresa, "NombreComercial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRegistroPatronal.DataBindings.Add("Text", this._Configuracion.Empresa, "RegistroPatronal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Regimen.DataBindings.Add("Text", this._Configuracion.Empresa, "TipoPersona", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RegimenFiscal.DataBindings.Add("Text", this._Configuracion.Empresa, "RegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txtTipoVialidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "TipoVialidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombreVialidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "NombreVialidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNumExterior.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "NumExterior", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNumInterior.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "NumInterior", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCodigoPostal.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "CodigoPostal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtColonia.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Colonia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtEntidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "EntidadFederativa", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtMunicipio.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "MunicipioDelegacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCiudad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Ciudad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtTelefono.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCorreo.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtReferencia.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtLocalidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Localidad", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Autoridad() {
            if (this._CedulaFiscal != null) {
                if (this._CedulaFiscal.TipoPersona == CedulaFiscal.TipoPersonaEnum.Moral) {
                    this._Configuracion.Empresa.RFC = this._CedulaFiscal.Moral.RFC;
                    this._Configuracion.Empresa.RazonSocial = this._CedulaFiscal.Moral.Nombre;
                    this._Configuracion.Empresa.NombreComercial = this._Configuracion.Empresa.RazonSocial + " " + this._CedulaFiscal.Moral.RegimenCapital;
                    this._Configuracion.Empresa.SociedadCapital = this._CedulaFiscal.Moral.RegimenCapital;
                    this._Configuracion.DomicilioFiscal.CodigoPostal = this._CedulaFiscal.Moral.DomicilioFiscal.CodigoPostal;
                    this._Configuracion.DomicilioFiscal.NombreVialidad = this._CedulaFiscal.Moral.DomicilioFiscal.NombreVialidad;
                    this._Configuracion.DomicilioFiscal.TipoVialidad = this._CedulaFiscal.Moral.DomicilioFiscal.TipoVialidad;
                    this._Configuracion.DomicilioFiscal.NumExterior = this._CedulaFiscal.Moral.DomicilioFiscal.NumExterior;
                    this._Configuracion.DomicilioFiscal.NumInterior = this._CedulaFiscal.Moral.DomicilioFiscal.NumInterior;
                    this._Configuracion.DomicilioFiscal.MunicipioDelegacion = this._CedulaFiscal.Moral.DomicilioFiscal.MunicipioDelegacion;
                    this._Configuracion.DomicilioFiscal.Colonia = this._CedulaFiscal.Moral.DomicilioFiscal.Colonia;
                    this._Configuracion.DomicilioFiscal.Correo = this._CedulaFiscal.Moral.DomicilioFiscal.Correo;
                    this._Configuracion.DomicilioFiscal.EntidadFederativa = this._CedulaFiscal.Moral.DomicilioFiscal.EntidadFederativa;
                    this._Configuracion.Empresa.CIF = this._CedulaFiscal.IdCIF;
                } else if (_Response.CedulaFiscal.TipoPersona == CedulaFiscal.TipoPersonaEnum.Fisica) {



                }
            }
        }

        private void Cargar() {
            this._Configuracion = (this._Service as Aplication.Empresa.Contracts.IEmpresaService).GetEmpresa1();
        }

        private bool Verificacion() {
            return true;
        }

        public void ReadOnly() {
            this.txtRegimenCapital.ReadOnly = true;
            this.txtNombre.ReadOnly = true;
            this.txtNombreComercial.ReadOnly = true;
            this.txtCorreo.ReadOnly = true;
            this.txtNumInterior.ReadOnly = true;
            this.txtNumExterior.ReadOnly = true;
            this.txtCodigoPostal.ReadOnly = true;
            this.txtColonia.ReadOnly = true;
            this.txtNombreVialidad.ReadOnly = true;
            this.txtEntidad.ReadOnly = true;
            this.txtTipoVialidad.ReadOnly = true;
            this.txtMunicipio.ReadOnly = true;
            this.TEmisor_Guardar.Enabled = false;
            this.TEmisor_CIF.Enabled = false;
            this.txtRFC.ReadOnly = true;
            this.txtCIF.ReadOnly = true;
            this.txtCURP.ReadOnly = true;
            this.txtRegistroPatronal.ReadOnly = true;
            this.RegimenFiscal.Enabled = false;
            this.Regimen.ReadOnly = true;
            this.txtCiudad.ReadOnly = true;
            this.txtTelefono.ReadOnly = true;
            this.txtReferencia.ReadOnly = true;
            this.txtLocalidad.ReadOnly = true;
        }
    }
}
