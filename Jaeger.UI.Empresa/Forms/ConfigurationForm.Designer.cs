﻿namespace Jaeger.UI.Empresa.Forms {
    partial class ConfigurationForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
            this.Title = new Telerik.WinControls.UI.RadLabel();
            this.SynapsisProperty = new Telerik.WinControls.UI.RadPropertyGrid();
            this.cerrarButton = new Telerik.WinControls.UI.RadButton();
            this.guardarButton = new Telerik.WinControls.UI.RadButton();
            this.TabsControl = new Telerik.WinControls.UI.RadPageView();
            this.Page1 = new Telerik.WinControls.UI.RadPageViewPage();
            ((System.ComponentModel.ISupportInitialize)(this.Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SynapsisProperty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cerrarButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guardarButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabsControl)).BeginInit();
            this.TabsControl.SuspendLayout();
            this.Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.Location = new System.Drawing.Point(10, 12);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(215, 18);
            this.Title.TabIndex = 4;
            this.Title.Text = "Configuración de la empesa registrada {0}";
            // 
            // SynapsisProperty
            // 
            this.SynapsisProperty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SynapsisProperty.Location = new System.Drawing.Point(0, 0);
            this.SynapsisProperty.Name = "SynapsisProperty";
            this.SynapsisProperty.Size = new System.Drawing.Size(401, 390);
            this.SynapsisProperty.TabIndex = 5;
            // 
            // cerrarButton
            // 
            this.cerrarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cerrarButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cerrarButton.Location = new System.Drawing.Point(301, 479);
            this.cerrarButton.Name = "cerrarButton";
            this.cerrarButton.Size = new System.Drawing.Size(110, 24);
            this.cerrarButton.TabIndex = 6;
            this.cerrarButton.Text = "Cerrar";
            this.cerrarButton.Click += new System.EventHandler(this.TConfiguration_Cerrar_Click);
            // 
            // guardarButton
            // 
            this.guardarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarButton.Location = new System.Drawing.Point(185, 479);
            this.guardarButton.Name = "guardarButton";
            this.guardarButton.Size = new System.Drawing.Size(110, 24);
            this.guardarButton.TabIndex = 6;
            this.guardarButton.Text = "Guardar";
            this.guardarButton.Click += new System.EventHandler(this.TConfiguration_Guardar_Click);
            // 
            // TabsControl
            // 
            this.TabsControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabsControl.Controls.Add(this.Page1);
            this.TabsControl.Location = new System.Drawing.Point(0, 35);
            this.TabsControl.Name = "TabsControl";
            this.TabsControl.SelectedPage = this.Page1;
            this.TabsControl.Size = new System.Drawing.Size(422, 438);
            this.TabsControl.TabIndex = 7;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabsControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // Page1
            // 
            this.Page1.Controls.Add(this.SynapsisProperty);
            this.Page1.ItemSize = new System.Drawing.SizeF(87F, 28F);
            this.Page1.Location = new System.Drawing.Point(10, 37);
            this.Page1.Name = "Page1";
            this.Page1.Size = new System.Drawing.Size(401, 390);
            this.Page1.Text = "Base de Datos";
            // 
            // ConfiguracionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cerrarButton;
            this.ClientSize = new System.Drawing.Size(422, 515);
            this.Controls.Add(this.TabsControl);
            this.Controls.Add(this.guardarButton);
            this.Controls.Add(this.cerrarButton);
            this.Controls.Add(this.Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfiguracionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuración Avanzada";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SynapsisProperty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cerrarButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guardarButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabsControl)).EndInit();
            this.TabsControl.ResumeLayout(false);
            this.Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected internal Telerik.WinControls.UI.RadPropertyGrid SynapsisProperty;
        protected internal Telerik.WinControls.UI.RadButton cerrarButton;
        protected internal Telerik.WinControls.UI.RadButton guardarButton;
        protected internal Telerik.WinControls.UI.RadPageViewPage Page1;
        protected internal Telerik.WinControls.UI.RadLabel Title;
        protected internal Telerik.WinControls.UI.RadPageView TabsControl;
    }
}
