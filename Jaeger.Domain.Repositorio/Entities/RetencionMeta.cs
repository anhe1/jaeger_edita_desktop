﻿using Jaeger.Domain.Repositorio.Contracts;

namespace Jaeger.Domain.Repositorio.Entities {
    public class RetencionMeta : Abstractions.RepositorioMetaData, IRetencionMeta {
        #region declaraciones
        private decimal _dMontoOperacion;
        private decimal _dMontoRetencion;
        #endregion

        public RetencionMeta() {

        }

        /// <summary>
        /// Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante
        /// </summary>
        public decimal MontoTotalOperacion {
            get {
                return this._dMontoOperacion;
            }
            set {
                this._dMontoOperacion = value;
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.
        /// </summary>
        public decimal MontoTotalRetencion {
            get {
                return this._dMontoRetencion;
            }
            set {
                this._dMontoRetencion = value;
            }
        }
    }
}
