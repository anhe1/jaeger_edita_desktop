﻿using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.Repositorio.Contracts;

namespace Jaeger.Domain.Repositorio.Entities {
    public class Configuration : IConfiguration {
        public Configuration() {
            this.IsLocal = true;
            this.StoragePDF = false;
            this.DataBase = new DataBase.Entities.DataBaseConfiguracion();
        }

        /// <summary>
        /// obtener o establecer si el repositorio es local
        /// </summary>
        public bool IsLocal { get; set; }

        /// <summary>
        /// obtener o establecer si se debe almacenar la representacion impresa
        /// </summary>
        public bool StoragePDF { get; set; }

        /// <summary>
        /// obtener o establecer si se debe almacenar la representacion impresa del acuse de cancelacion
        /// </summary>
        public bool StorageAcusePDF { get; set; }

        /// <summary>
        /// obtener o establecer si se debe almacenar la representacion impresa del acuse final de cancelacion
        /// </summary>
        public bool StorageAcuseFinalPDF { get; set; }

        /// <summary>
        /// obtener o establecer si se permite importar archivos
        /// </summary>
        public bool AllowImportXML { get; set; }

        /// <summary>
        /// obtener o establecer si se debe crear directorio de emisores y receptores
        /// </summary>
        public bool CreateDirectory { get; set; }

        /// <summary>
        /// obtener o establecer configuracion de la base de datos del repositorio
        /// </summary>
        public IDataBaseConfiguracion DataBase { get; set; }
    }
}
