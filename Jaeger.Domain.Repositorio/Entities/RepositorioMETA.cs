﻿using System;
using Jaeger.Domain.Repositorio.Contracts;

namespace Jaeger.Domain.Repositorio.Entities {
    public class RepositorioMeta : Abstractions.RepositorioMetaData, IRepositorioMeta {

        public RepositorioMeta() {

        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        public Char EfectoComprobante { get; set; }

        public string TipoComprobante {
            get { if (this.EfectoComprobante == 'I')
                    return "Ingreso";
                else if (this.EfectoComprobante == 'E')
                    return "Egreso";
                else if (this.EfectoComprobante == 'T')
                    return "Traslado";
                else if (this.EfectoComprobante == 'P')
                    return "Pago";
                else if (this.EfectoComprobante == 'N')
                    return "Nómina";
                return "NA";
            }
        }
    }
}
