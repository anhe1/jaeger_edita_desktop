﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Repositorio.Entities {
    /// <summary>
    /// clase que representa a un contribuyente
    /// </summary>
    [SugarTable("Contribuyente", "")]
    public class EmpresaModel : BasePropertyChangeImplementation {
        #region declaraciones
        private string rfc;
        private string nombre;
        private string telefono;
        private string correo;
        private string nota;
        private DateTime fechaNuevo;
        private string regimen;
        private string regimenFiscal;
        private string domicilioFiscalReceptor;
        private string _NombreComercial;
        private string _Repositorio;
        #endregion

        public EmpresaModel() {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "RFC", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsPrimaryKey = true, IsNullable = false)]
        public string RFC {
            get { return this.rfc; }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("RazonSocial")]
        [SugarColumn(ColumnName = "RazonSocial", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public string RazonSocial {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.
        /// </summary>
        [DataNames("NombreComercial")]
        [SugarColumn(ColumnName = "NombreComercial", ColumnDescription = "denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.", IsNullable = true, Length = 255, IsIgnore = true)]
        public string NombreComercial {
            get { return this._NombreComercial; }
            set {
                this._NombreComercial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero telefonico de contacto
        /// </summary>
        [DataNames("Telefono")]
        [SugarColumn(ColumnName = "Telefono", ColumnDescription = "telefono", IsNullable = true, Length = 64)]
        public string Telefono {
            get { return this.telefono; }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer regimen fiscal (Fisica o Moral)
        /// </summary>
        [DataNames("Regimen")]
        [SugarColumn(ColumnName = "Regimen", ColumnDescription = "regimen fiscal Fisica, Moral", Length = 64, IsNullable = true)]
        public string Regimen {
            get { return this.regimen; }
            set {
                this.regimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        [DataNames("RegimenFiscal")]
        [SugarColumn(ColumnName = "RegimenFiscal", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = true)]
        public string RegimenFiscal {
            get { return this.regimenFiscal; }
            set {
                this.regimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("DomicilioFiscal")]
        [SugarColumn(ColumnName = "DomicilioFiscal", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = true)]
        public string DomicilioFiscal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estabelcer direccion de correo electronico
        /// </summary>
        [DataNames("Correo")]
        [SugarColumn(ColumnName = "Correo", ColumnDescription = "correo electronico", IsNullable = true, Length = 128)]
        public string Correo {
            get { return this.correo; }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// observaciones
        /// </summary>
        [DataNames("Nota")]
        [SugarColumn(ColumnName = "Nota", ColumnDescription = "observaciones", IsNullable = true, Length = 255)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Repositorio")]
        [SugarColumn(ColumnName = "Repositorio", ColumnDescription = "ruta del repositorio", IsNullable = true, Length = 255)]
        public string Repositorio {
            get { return this._Repositorio; }
            set { this._Repositorio = value;
            this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("FechaSistema")]
        [SugarColumn(ColumnName = "FechaSistema", ColumnDescription = "fecha de creacion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
