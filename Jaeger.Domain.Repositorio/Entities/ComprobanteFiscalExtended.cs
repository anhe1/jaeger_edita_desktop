﻿using System;
using Jaeger.Domain.Repositorio.ValueObjects;
using Newtonsoft.Json;
using SqlSugar;

namespace Jaeger.Domain.Repositorio.Entities {
    [JsonObject]
    public class ComprobanteFiscalExtended : ComprobanteFiscalModel {
        private string urlXMLField;
        private string urlPDFField;
        private string urlDetalleField;
        private string urlRecuperarAcuseField;
        private string urlRecuperaAcuseFinalField;

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalExtended() {
            this.Comprobado = DescargaConciliaEnum.SinVerificar;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer URL de descarga del archivo XML del comprobante
        /// </summary>
        [JsonProperty("urlXML")]
        [SugarColumn(IsIgnore = true)]
        public string UrlXML {
            get {
                return this.urlXMLField;
            }
            set {
                this.urlXMLField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer URL de descarga de la representacion impresa (PDF) del comprobante
        /// </summary>
        [JsonProperty("urlPDF")]
        [SugarColumn(IsIgnore = true)]
        public string UrlPDF {
            get {
                return this.urlPDFField;
            }
            set {
                this.urlPDFField = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer URL de descarga del detalle del comprobante (PDF)
        /// </summary>
        [JsonProperty("urlDetalle")]
        [SugarColumn(IsIgnore = true)]
        public string UrlDetalle {
            get {
                return this.urlDetalleField;
            }
            set {
                this.urlDetalleField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url para recuperar la representación impresa (pdf) del acuse de solicitud  de cancelacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string UrlRecuperarAcuse {
            get {
                return this.urlRecuperarAcuseField;
            }
            set {
                this.urlRecuperarAcuseField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url para recuperar la representación impresa el Acuse del status final de la cancelacion del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string UrlRecuperaAcuseFinal {
            get {
                return this.urlRecuperaAcuseFinalField;
            }
            set {
                this.urlRecuperaAcuseFinalField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string ComprobadoText {
            get {
                return Enum.GetName(typeof(DescargaConciliaEnum), this.Comprobado);
            }
            set {
                if (value == Enum.GetName(typeof(DescargaConciliaEnum), DescargaConciliaEnum.SinVerificar))
                    this.Comprobado = DescargaConciliaEnum.SinVerificar;
                else if (value == Enum.GetName(typeof(DescargaConciliaEnum), DescargaConciliaEnum.Verificado))
                    this.Comprobado = DescargaConciliaEnum.Verificado;
                else if (value == Enum.GetName(typeof(DescargaConciliaEnum), DescargaConciliaEnum.NoEncontrado))
                    this.Comprobado = DescargaConciliaEnum.NoEncontrado;
            }
        }

        /// <summary>
        /// obtener 
        /// </summary>
        public string KeyName() {
            return string.Concat(this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaEmision.ToString("yyyyMMddHHmmss"));
        }

        public string KeyNameXml() {
            return string.Concat(this.KeyName(), ".xml");
        }

        public string KeyNamePdf() {
            return string.Concat(this.KeyName(), ".pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representación impresa del acuse de cancelacion, en este caso solo para los comprobantes emitidos.
        /// </summary>
        public string KeyNameAcuse() {
            return string.Concat(this.KeyName(), "-Acuse.pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representacion impresa del acuse final de la cancelacion del comprobante.
        /// </summary>
        public string KeyNameAcuseFinal() {
            return string.Concat(this.KeyName(), "-AcuseFinal.pdf");
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
