﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using System.Collections.Generic;

namespace Jaeger.Domain.Repositorio.Entities {
    [SugarTable("ComplementoPago", "CFDI: complemento de pagos version")]
    public class ComplementoPagoModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int indice;
        private string versionField;
        private int idComprobante;
        private DateTime fechaPagoField;
        private string formaDePagoPField;
        private string monedaPField;
        private decimal tipoCambioPField;
        private decimal montoField;
        private string numOperacionField;
        private string rfcEmisorCtaOrdField;
        private string nomBancoOrdExtField;
        private string ctaOrdenanteField;
        private string rfcEmisorCtaBenField;
        private string ctaBeneficiarioField;
        private string tipoCadPagoField;
        private string certPagoField;
        private string cadPagoField;
        private string selloPagoField;
        private List<PagosPagoDoctoRelacionadoModel> _Documentos;
        #endregion

        public ComplementoPagoModel() {
            this.versionField = "2.0";
            this.FechaPago = DateTime.Now;
        }

        [DataNames("IdPago")]
        [SugarColumn(ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPago {
            get { return this.indice; }
            set {
                this.indice = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la versión del complemento para recepción de pagos.
        /// </summary>
        [DataNames("Version")]
        [SugarColumn(ColumnDescription = "Version del complemento de pagos", DefaultValue = "2.0", Length = 5)]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el comprobante de pagos (cfdi)
        /// </summary>
        [DataNames("IdComprobante")]
        [SugarColumn(ColumnDescription = "indice de relacion del comprobante fiscal")]
        public int IdComprobante {
            get { return this.idComprobante; }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la 
        /// especificación ISO 8601.En caso de no contar con la hora se debe registrar 12:00:00.
        /// </summary>
        [DataNames("FechaPago")]
        [SugarColumn(ColumnDescription = "fecha y hora en la que el beneficiario recibe el pago")]
        public DateTime FechaPago {
            get {
                return this.fechaPagoField;
            }
            set {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        [DataNames("FormaDePagoP")]
        [SugarColumn(ColumnDescription = "clave de forma en que se realiza el pago")]
        public string FormaDePagoP {
            get {
                return this.formaDePagoPField;
            }
            set {
                this.formaDePagoPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para realizar el pago conforme a la especificación ISO 4217. Cuando se usa moneda nacional se registra MXN. 
        /// El atributo Pagos:Pago:Monto debe ser expresado en la moneda registrada en este atributo.
        /// </summary>
        [DataNames("MonedaP")]
        [SugarColumn(ColumnDescription = "clave de moneda")]
        public string MonedaP {
            get {
                return this.monedaPField;
            }
            set {
                this.monedaPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una 
        /// unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.
        /// </summary>
        [DataNames("TipoCambioP")]
        [SugarColumn(ColumnDescription = "tipo de cambio de la moneda a la fecha en que se realizo el pago", DecimalDigits = 6, DefaultValue = "1")]
        public decimal TipoCambioP {
            get {
                return this.tipoCambioPField;
            }
            set {
                this.tipoCambioPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del pago.
        /// </summary>
        [DataNames("Monto")]
        [SugarColumn(ColumnDescription = "importe del pago", Length = 18, DecimalDigits = 4)]
        public decimal Monto {
            get {
                return this.montoField;
            }
            set {
                this.montoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia 
        /// análogo que identifique la operación que ampara el pago efectuado.
        /// </summary>
        [DataNames("NumOperacion")]
        [SugarColumn(ColumnDescription = "numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, linea de captura o algun numero de referencia", Length = 100, IsNullable = true)]
        public string NumOperacion {
            get {
                return this.numOperacionField;
            }
            set {
                this.numOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc.,
        /// en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("RfcEmisorCtaOrd")]
        [SugarColumn(ColumnDescription = "RFC de la entidad emisora de la cuenta de origen, es decir, la peradora, el banco, etc.", Length = 13, IsNullable = true)]
        public string RfcEmisorCtaOrd {
            get {
                return this.rfcEmisorCtaOrdField;
            }
            set {
                this.rfcEmisorCtaOrdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de 
        /// acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("NomBancoOrdExt")]
        [SugarColumn(ColumnDescription = "nombre del banco ordenante", Length = 300, IsNullable = true)]
        public string NomBancoOrdExt {
            get {
                return this.nomBancoOrdExtField;
            }
            set {
                this.nomBancoOrdExtField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la cuenta con la que se realizo el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con 
        /// el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("CtaOrdenante")]
        [SugarColumn(ColumnDescription = "numero de la cuenta con la que se realizo el pago", Length = 50, IsNullable = true)]
        public string CtaOrdenante {
            get {
                return this.ctaOrdenanteField;
            }
            set {
                this.ctaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. 
        /// Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("RfcEmisorCtaBen")]
        [SugarColumn(ColumnDescription = "RFC de la entidad operadora de la cuenta destino", Length = 13, IsNullable = true)]
        public string RfcEmisorCtaBen {
            get {
                return this.rfcEmisorCtaBenField;
            }
            set {
                this.rfcEmisorCtaBenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del tipo de cadena de pago que genera la entidad receptora del pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para 
        /// éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("TipoCadPago")]
        [SugarColumn(ColumnDescription = "tipo de cadena de pago que genera la entidad receptora del pago", Length = 10, IsNullable = true)]
        public string TipoCadPago {
            get {
                return this.tipoCadPagoField;
            }
            set {
                this.tipoCadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo 
        /// con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("CtaBeneficiario")]
        [SugarColumn(ColumnDescription = "numero de cuenta en donde se recibio el pago.", Length = 20, IsNullable = true)]
        public string CtaBeneficiario {
            get {
                return this.ctaBeneficiarioField;
            }
            set {
                this.ctaBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria. Es requerido en caso de que el atributo TipoCadPago 
        /// contenga información.
        /// </summary>
        [DataNames("CadPago")]
        [SugarColumn(ColumnDescription = "cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria", Length = 8192, IsNullable = true)]
        public string CadPago {
            get {
                return this.cadPagoField;
            }
            set {
                this.cadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer certificado que ampara al pago, como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.
        /// </summary>
        [DataNames("CertPago")]
        [SugarColumn(ColumnDescription = "certificado que ampara al pago, como una cadena de texto en formato base 64")]
        public string CertPago {
            get {
                return this.certPagoField;
            }
            set {
                this.certPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el sello digital que se asocie al pago. La entidad que emite el comprobante de pago, ingresa una cadena original y el sello digital en una sección de 
        /// dicho comprobante, este sello digital es el que se debe registrar en este atributo. Debe ser expresado como una cadena de texto en formato base 64. Es requerido en caso 
        /// de que el atributo TipoCadPago contenga información.
        /// </summary>
        [DataNames("SelloPago")]
        [SugarColumn(ColumnDescription = "sello digital que se asocie al pago.")]
        public string SelloPago {
            get {
                return this.selloPagoField;
            }
            set {
                this.selloPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public List<PagosPagoDoctoRelacionadoModel> Documentos {
            get { return this._Documentos; }
            set { this._Documentos = value; }
        }
    }
}
