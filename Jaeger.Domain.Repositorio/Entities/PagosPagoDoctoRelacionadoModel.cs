﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Repositorio.Entities {
    /// <summary>
    /// Documento relacionado del complemento de pagos del comprobante fiscal
    /// </summary>
    [SugarTable("ComplementoPagoP", "CFDI complemento pagos: documento relacionado")]
    public class PagosPagoDoctoRelacionadoModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int idRelacion;
        private int _IdComprobanteR;
        private string idDocumentoField;
        private string serieField;
        private string folioField;
        private string monedaDRField;
        private decimal equivalenciaDRField;
        private int numParcialidadField;
        private decimal impSaldoAntField;
        
        private decimal impSaldoInsolutoField;
        private decimal _Total;
        private string objetoImpDRField;
        private int _IdPago;
        private string rfcField;
        private string nombreField;
        private DateTime fechaEmision;
        private string formaDePagoPField;
        private string metodoPagoField;
        private int _IdSubTipo;
        private int idComprobanteP;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public PagosPagoDoctoRelacionadoModel() {
        }

        /// <summary>
        /// obtener o establecer el indice de relacion de la tabla
        /// </summary>
        [DataNames("IdRelacion")]
        [SugarColumn(ColumnDescription = "indice de la tabla")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la tabla de pagos
        /// </summary>
        [DataNames("IdPago")]
        [SugarColumn(ColumnDescription = "indice de relacion con la tabla de pagos", DefaultValue = "0")]
        public int IdPago {
            get { return this._IdPago; }
            set {
                this._IdPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del comprobante fiscal de pagos
        /// </summary>
        [DataNames("IdComprobanteP")]
        [SugarColumn(ColumnDescription = "indice de relacion con la tabla de comprobantes fiscales (_cfdi) del comprobante de pago")]
        public int IdComprobanteP {
            get { return this.idComprobanteP; }
            set {
                this.idComprobanteP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("IdSubTipo")]
        [SugarColumn(ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)")]
        public int IdSubTipo {
            get { return this._IdSubTipo; }
            set {
                this._IdSubTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del comprobante relacionado al complemento del pago (CFDI)
        /// </summary>
        [DataNames("IdComprobanteR")]
        [SugarColumn(ColumnDescription = "indice de relacion con la tabla de comprobantes fiscales (_cfdi)")]
        public int IdComprobanteR {
            get { return this._IdComprobanteR; }
            set {
                this._IdComprobanteR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        [DataNames("IdDocumento")]
        [SugarColumn(ColumnDescription = "identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien el numero de operacion de un documento digital.")]
        public string IdDocumento {
            get {
                return this.idDocumentoField;
            }
            set {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [DataNames("Serie")]
        [SugarColumn(ColumnDescription = "serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.")]
        public string Serie {
            get {
                return this.serieField;
            }
            set {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [DataNames("Folio")]
        [SugarColumn(ColumnDescription = "folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.")]
        public string Folio {
            get {
                return this.folioField;
            }
            set {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnDescription = "rfc receptor del comprobante, esta es informacion adicional no se incluye en el complemento..")]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [DataNames("Nombre")]
        [SugarColumn(ColumnDescription = "receptor del comprobante, esta es informacion adicional no se incluye en el complemento.")]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("FechaEmision")]
        [SugarColumn(ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        [DataNames("FormaDePagoP")]
        [SugarColumn(ColumnDescription = "clave de forma en que se realiza el pago")]
        public string FormaDePagoP {
            get {
                return this.formaDePagoPField;
            }
            set {
                this.formaDePagoPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [DataNames("MetodoPago")]
        [SugarColumn(ColumnDescription = "clave del método de pago que se registró en el documento relacionado")]
        public string MetodoPago {
            get {
                return this.metodoPagoField;
            }
            set {
                this.metodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento 
        /// relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto”
        /// de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("MonedaDR")]
        [SugarColumn(ColumnDescription = "clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado")]
        public string MonedaDR {
            get {
                return this.monedaDRField;
            }
            set {
                this.monedaDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento 
        /// relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que 
        /// equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD. El pago se realiza por 100 EUR. Este 
        /// atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD.
        /// </summary>
        [DataNames("EquivalenciaDR")]
        [SugarColumn(ColumnDescription = "tipo de cambio conforme con la moneda registrada en el documento relacionado.")]
        public decimal EquivalenciaDR {
            get {
                return this.equivalenciaDRField;
            }
            set {
                this.equivalenciaDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        [DataNames("NumParcialidad")]
        [SugarColumn(ColumnDescription = "numero de parcialidad que corresponde al pago.")]
        public int NumParcialidad {
            get {
                return this.numParcialidadField;
            }
            set {
                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe total del comprobante relacionado
        /// </summary>
        [DataNames("Total")]
        [SugarColumn(ColumnDescription = "importe total del comprobante")]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        [DataNames("ImpSaldoAnt")]
        [SugarColumn(ColumnDescription = "saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe contener el importe total del documento relacionado.", Length = 14, DecimalDigits = 4, IsNullable = true, DefaultValue = "0")]
        public decimal ImpSaldoAnt {
            get {
                return this.impSaldoAntField;
            }
            set {
                this.impSaldoAntField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        [DataNames("ImpPagado")]
        [SugarColumn(ColumnDescription = "importe pagado para el documento relacionado.")]
        public decimal ImpPagado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        [DataNames("ImpSaldoInsoluto")]
        [SugarColumn(ColumnDescription = "diferencia entre el importe del saldo anterior y el monto del pago.")]
        public decimal ImpSaldoInsoluto {
            get {
                return this.impSaldoInsolutoField;
            }
            set {
                this.impSaldoInsolutoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el pago del documento relacionado es objeto o no de impuesto.
        /// </summary>
        [DataNames("ObjetoImpDR")]
        [SugarColumn(ColumnDescription = "el pago del documento relacionado es objeto o no de impuesto.")]
        public string ObjetoImpDR {
            get {
                return this.objetoImpDRField;
            }
            set {
                this.objetoImpDRField = value;
                this.OnPropertyChanged();
            }
        }

        #region propiedades ignoradas
        /// <summary>
        /// Sub tipo de comprobante 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal SaldoInsoluto {
            get { return this.ImpSaldoAnt - this.ImpPagado; }
        }

        #endregion
    }
}
