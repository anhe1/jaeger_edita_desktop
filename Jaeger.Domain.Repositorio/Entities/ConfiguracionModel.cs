﻿using SqlSugar;

namespace Jaeger.Domain.Repositorio.Entities {
    [SugarTable("Configuracion", "Configuracion del repositorio")]
    public class ConfiguracionModel {
        private string _Parametro;

        public ConfiguracionModel() { }

        [SugarColumn(ColumnName = "Parametro", ColumnDescription = "nombre del parametro", IsPrimaryKey = true, Length = 20)]
        public string Parametro {
            get { return _Parametro; }
            set { if (!string.IsNullOrEmpty(value))
                    _Parametro = value.ToLower();
            }
        }

        [SugarColumn(ColumnName = "Cataegoria", ColumnDescription = "", Length = 3)]
        public int IdCategoria { get; set; }

        [SugarColumn(ColumnName = "Valor", ColumnDescription = "Valor del parametro", Length = 100)]
        public string Valor { get; set; }
    }
}
