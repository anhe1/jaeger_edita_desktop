﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Repositorio.Entities {
    /// <summary>
    /// clase que representa a un contribuyente
    /// </summary>
    [SugarTable("Contribuyentes", "tabla de contribuyentes")]
    public class ContribuyenteModel : BasePropertyChangeImplementation {
        #region declaraciones
        private string rfc;
        private string nombre;
        private string telefono;
        private string regimenFiscal;
        private string domicilioFiscalReceptor;
        private string _NombreComercial;
        private string noExterior;
        private string noInterior;
        private string calle;
        private string colonia;
        private string municipio;
        private string ciudad;
        private string estado;
        private string pais;
        private string localidad;
        private string referencia;
        private string correo;
        private int relacion;
        #endregion

        public ContribuyenteModel() {
            this.relacion = 2;
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("RFC")]
        [SugarColumn(ColumnName = "RFC", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsPrimaryKey = true, IsNullable = false)]
        public string RFC {
            get { return this.rfc; }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("RazonSocial")]
        [SugarColumn(ColumnName = "RazonSocial", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public string RazonSocial {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.
        /// </summary>
        [DataNames("NombreComercial")]
        [SugarColumn(ColumnName = "NombreComercial", ColumnDescription = "denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.", IsNullable = true, Length = 255, IsIgnore = true)]
        public string NombreComercial {
            get { return this._NombreComercial; }
            set {
                this._NombreComercial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estabelcer direccion de correo electronico
        /// </summary>
        [DataNames("Correo")]
        [SugarColumn(ColumnName = "Correo", ColumnDescription = "correo electronico", IsNullable = false, Length = 128)]
        public string Correo {
            get { return this.correo; }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero telefonico de contacto
        /// </summary>
        [DataNames("Telefono")]
        [SugarColumn(ColumnName = "Telefono", ColumnDescription = "telefono", IsNullable = true, Length = 64)]
        public string Telefono {
            get { return this.telefono; }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        [DataNames("RegimenFiscal")]
        [SugarColumn(ColumnName = "RegimenFiscal", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = true)]
        public string RegimenFiscal {
            get { return this.regimenFiscal; }
            set {
                this.regimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Relacion")]
        [SugarColumn(ColumnName = "Relacion", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = true)]
        public int Relacion {
            get { return this.relacion; }
            set {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        #region domicilio fiscal
        /// <summary>
        /// obetener o establecer la avenida, calle, camino o carretera donde se da la ubicación.
        /// </summary>           
        [DataNames("Calle")]
        [SugarColumn(ColumnName = "Calle", ColumnDescription = "calle", IsNullable = true, Length = 124)]
        public string Calle {
            get { return this.calle; }
            set {
                this.calle = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número particular en donde se da la ubicación sobre una calle dada.
        /// </summary>           
        [DataNames("NoExterior")]
        [SugarColumn(ColumnName = "NoExterior", ColumnDescription = "numero exterior", IsNullable = true, Length = 64)]
        public string NoExterior {
            get { return this.noExterior; }
            set {
                this.noExterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer información adicional para especificar la ubicación cuando calle y número exterior (noExterior) no resulten suficientes para determinar la ubicación de forma precisa.
        /// </summary>           
        [DataNames("NoInterior")]
        [SugarColumn(ColumnName = "NoInterior", ColumnDescription = "numero interior", IsNullable = true, Length = 64)]
        public string NoInterior {
            get { return this.noInterior; }
            set {
                this.noInterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la colonia en donde se da la ubicación cuando se desea ser más específico en casos de ubicaciones urbanas.
        /// </summary>           
        [DataNames("Colonia")]
        [SugarColumn(ColumnName = "Colonia", ColumnDescription = "colonia", IsNullable = true, Length = 124)]
        public string Colonia {
            get { return this.colonia; }
            set {
                this.colonia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("CodigoPostal")]
        [SugarColumn(ColumnName = "CodigoPostal", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = true)]
        public string CodigoPostal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el municipio o delegación (en el caso del Distrito Federal) en donde se da la ubicación.
        /// </summary>           
        [DataNames("Delegacion")]
        [SugarColumn(ColumnName = "Delegacion", ColumnDescription = "delegacion o municipio", IsNullable = true, Length = 124)]
        public string Delegacion {
            get { return this.municipio; }
            set {
                this.municipio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ciudad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("Ciudad")]
        [SugarColumn(ColumnName = "Ciudad", ColumnDescription = "ciudad", IsNullable = true, Length = 124)]
        public string Ciudad {
            get { return this.ciudad; }
            set {
                this.ciudad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado o entidad federativa donde se da la ubicación.
        /// </summary>           
        [DataNames("Estado")]
        [SugarColumn(ColumnName = "Estado", ColumnDescription = "estado", IsNullable = true, Length = 124)]
        public string Estado {
            get { return this.estado; }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el país donde se da la ubicación.
        /// </summary>           
        [DataNames("Pais")]
        [SugarColumn(ColumnName = "Pais", ColumnDescription = "Pais", IsNullable = true, Length = 124)]
        public string Pais {
            get { return this.pais; }
            set {
                this.pais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ciudad o población donde se da la ubicación
        /// </summary>           
        [DataNames("Localidad")]
        [SugarColumn(ColumnName = "Localidad", ColumnDescription = "localidad", IsNullable = true, Length = 124)]
        public string Localidad {
            get { return this.localidad; }
            set {
                this.localidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia de ubicación adicional
        /// </summary>           
        [DataNames("Referencia")]
        [SugarColumn(ColumnName = "Referencia", ColumnDescription = "referencia", IsNullable = true, Length = 124)]
        public string Referencia {
            get { return this.referencia; }
            set {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
