﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Repositorio.Entities {
    [SugarTable("RFCWS", "Tabla para estadisticas de validacion de RFC.")]
    public class ValidacionRFCModel {
        public ValidacionRFCModel() {
            this.FechaNuevo = DateTime.Now;
            this.Metodo = "Captura";
        }

        [DataNames("RFCWS_ID")]
        [SugarColumn(ColumnName = "RFCWS_ID", ColumnDescription = "Identificador de la tabla.", IsIdentity = true, IsPrimaryKey = true)]
        public int Id { get; set; }

        [DataNames("RFCWS_METODO")]
        [SugarColumn(ColumnName = "RFCWS_METODO", ColumnDescription = "Metodo de la validacion.", Length = 10)]
        public string Metodo { get; set; }

        [DataNames("RFCWS_DATA")]
        [SugarColumn(ColumnName = "RFCWS_DATA", ColumnDescription = "Datos implementados en la validacion.", ColumnDataType = "TEXT")]
        public string Data { get; set; }

        [DataNames("RFCWS_RES")]
        [SugarColumn(ColumnName = "RFCWS_RES", ColumnDescription = "Respuesta de la validacion.", ColumnDataType = "TEXT")]
        public string Respuesta { get; set; }

        [DataNames("RFCWS_C")]
        [SugarColumn(ColumnName = "RFCWS_C", ColumnDescription = "Cantidad de validaciones correctas.", Length = 11)]
        public int Correctos { get; set; }

        [DataNames("RFCWS_I")]
        [SugarColumn(ColumnName = "RFCWS_I", ColumnDescription = "Cantidad de validaciones incorrectas", Length = 11)]
        public int Incorrectos { get; set; }

        [DataNames("RFCWS_FN")]
        [SugarColumn(ColumnName = "RFCWS_FN", ColumnDescription = "Fecha de la validacion.")]
        public DateTime FechaNuevo { get; set; }
    }
}
