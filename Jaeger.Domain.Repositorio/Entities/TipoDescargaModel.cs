﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Repositorio.Entities {
    public class TipoDescargaModel : BaseSingleModel {
        public TipoDescargaModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
