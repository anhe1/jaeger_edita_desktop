﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Repositorio.Entities {
    [SugarTable("Descargas", "Descarga de paquetes")]
    public class VerificacionMetaModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _index;
        private string _IdSolicitud;
        private List<string> idsPaquetesField;
        private string codEstatusField;
        private int estadoSolicitudField;
        private string codigoEstadoSolicitudField;
        private int numeroCFDIsField;
        private string mensajeField;
        #endregion

        public VerificacionMetaModel() {
            this.idsPaquetesField = new List<string>();
        }

        [SugarColumn(ColumnName = "Id", ColumnDescription = "", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return _index; }
            set { _index = value; 
            this.OnPropertyChanged(); 
            }
        }

        /// <summary>
        /// obtener o establecer identificador de la consulta que se pretende consultar
        /// </summary>
        [SugarColumn(ColumnName = "IdSolicitud", ColumnDescription = "id de solicitud", Length = 40, IsNullable = true)]
        public string IdSolicitud {
            get { return _IdSolicitud; }
            set {
                _IdSolicitud = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo de estatus de la peticion de verificacion
        /// </summary>
        [SugarColumn(ColumnName = "CodEstatus", ColumnDescription = "codigo de estatus de la peticion de verificacion", Length = 25, IsNullable = true)]
        public string CodEstatus {
            get { return codEstatusField; }
            set {
                codEstatusField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero correspondiente al estado de la solicitud de descarga (Aceptada = 1, EnProceso = 2, Terminada = 3, Error = 4, Rechazada = 5, Vencida = 6)
        /// </summary>
        [SugarColumn(ColumnName = "EstadoSolicitud", ColumnDescription = "numero correspondiente al estado de la solicitud de descarga (Aceptada = 1, EnProceso = 2, Terminada = 3, Error = 4, Rechazada = 5, Vencida = 6)", IsNullable = true)]
        public int EstadoSolicitud {
            get { return estadoSolicitudField; }
            set {
                estadoSolicitudField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo de estado de la solicitud de descarga, los cuales pueden ser 5000, 5002, 5003, 5004 o 5005
        /// </summary>
        [SugarColumn(ColumnName = "CodigoEstadoSolicitud", ColumnDescription = "codigo de estado de la solicitud de descarga, los cuales pueden ser 5000, 5002, 5003, 5004 o 5005", IsNullable = true)]
        public string CodigoEstadoSolicitud {
            get { return codigoEstadoSolicitudField; }
            set {
                codigoEstadoSolicitudField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de CFDIs que conforman la solicitud de descarga consultada
        /// </summary>
        [SugarColumn(ColumnName = "NumeroCFDIs", ColumnDescription = "numero de CFDIs que conforman la solicitud de descarga consultada", IsNullable = true)]
        public int NumeroCFDIs {
            get { return numeroCFDIsField; }
            set {
                numeroCFDIsField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion del codigo estatus correspondiente a la peticion de verificacion
        /// </summary>
        [SugarColumn(ColumnName = "Mensaje", ColumnDescription = "descripcion del codigo estatus correspondiente a la peticion de verificacion", Length = 25, IsNullable = true)]
        public string Mensaje {
            get { return mensajeField; }
            set {
                mensajeField = value;
                OnPropertyChanged();
            }
        }
        
        [SugarColumn(ColumnName = "IdsPaquetes", ColumnDescription = "", ColumnDataType = "Text", IsNullable = true, IsJson = false)]
        public string Temporal {
            get {
                if (this.IdsPaquetes != null)
                    return Newtonsoft.Json.JsonConvert.SerializeObject(this.IdsPaquetes);
                return null;
            }
            set { if (value != null) {
                    this.IdsPaquetes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer los identificadores de los paquetes que componen la solicitud de descarga. Solo se devuelve cuando la solicitud posee un status finalizado
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "IdsPaquetes", ColumnDescription = "identificadores de los paquetes que componen la solicitud de descarga. Solo se devuelve cuando la solicitud posee un status finalizado", IsNullable = true, IsArray = true)]
        public List<string> IdsPaquetes {
            get { return idsPaquetesField; }
            set {
                idsPaquetesField = value;
                OnPropertyChanged();
            }
        }
    }
}
