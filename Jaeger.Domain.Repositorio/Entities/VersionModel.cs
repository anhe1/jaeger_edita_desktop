﻿using SqlSugar;

namespace Jaeger.Domain.Repositorio.Entities {
    [SugarTable("Version", "comprobantes fiscales")]
    public class VersionModel {
        public VersionModel() {
            this.Version = "3.1";
        }

        [SugarColumn(ColumnName = "Numero", IsPrimaryKey = true, Length = 3)]
        public string Version {
            get;set;
        }
    }
}
