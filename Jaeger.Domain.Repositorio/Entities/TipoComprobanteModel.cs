﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Repositorio.Entities {
    public class TipoComprobanteModel : BaseSingleModel {
        public TipoComprobanteModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
