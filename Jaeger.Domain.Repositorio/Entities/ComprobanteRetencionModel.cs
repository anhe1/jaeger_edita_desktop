﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Repositorio.Contracts;

namespace Jaeger.Domain.Repositorio.Entities {
    /// <summary>
    /// Estándar de Documento Electrónico Retenciones e Información de Pagos.
    /// </summary>
    [SugarTable("Retenciones", "estandar de documento electronico retenciones e informacion de pagos.")]
    public class ComprobanteRetencionModel : BasePropertyChangeImplementation, IComprobanteRetencionModel {
        #region declaraciones
        private DateTime fechaEmision;
        private DateTime? fechaTimbrado;
        private string rfcProveedorCertificacion;
        private int mesInicial;
        private int mesFinal;
        private int ejercicio;
        private decimal montoTotalOperacion;
        private decimal montoTotalGravado;
        private string estado;
        private string emisorRFC;
        private string receptorRFC;
        private string idDocumento;
        private string emisorNombre;
        private string receptorNombre;
        #endregion

        public ComprobanteRetencionModel() {
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("IdDocumento ")]
        [SugarColumn(ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", Length = 36, IsNullable = true)]
        public string IdDocumento {
            get {
                return idDocumento;
            }
            set {
                idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("EmisorRFC")]
        [SugarColumn(ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC {
            get {
                return emisorRFC;
            }
            set {
                emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("EmisorNombre")]
        [SugarColumn(ColumnDescription = "para incorporar la clave en el Registro Federal de Contribuyentes correspondiente al contribuyente emisor del documento de retención e información de pagos, sin guiones o espacios.", Length = 300)]
        public string EmisorNombre {
            get {
                return emisorNombre;
            }
            set {
                emisorNombre = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("ReceptorRFC")]
        [SugarColumn(ColumnDescription = "registro federal de contribuyentes del receptor del comprobante", Length = 14)]
        public string ReceptorRFC {
            get {
                return receptorRFC;
            }
            set {
                receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("ReceptorNombre")]
        [SugarColumn(ColumnDescription = "para el nombre, denominación o razón social del contribuyente receptor del documento.", Length = 300)]
        public string ReceptorNombre {
            get {
                return receptorNombre;
            }
            set {
                receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// minInclusive value ="2014-01-01T00:00:00-06:00"
        /// pattern value = "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))"
        /// </summary>
        [DataNames("FechaExpedicion")]
        [SugarColumn(ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaExpedicion {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("FechaCertificacion")]
        [SugarColumn(ColumnName = "CFDRET_FECCERT", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaCertificacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                this.fechaTimbrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos
        /// </summary>
        [DataNames("MesInicial")]
        [SugarColumn(ColumnDescription = "atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos", Length = 2)]
        public int MesInicial {
            get { return this.mesInicial; }
            set {
                this.mesInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes final del periodo de la retención e información de pagos
        /// </summary>
        [DataNames("MesFinal")]
        [SugarColumn(ColumnDescription = "atributo requerido para la expresión del mes final del periodo de la retención e información de pagos", Length = 2)]
        public int MesFinal {
            get { return this.mesFinal; }
            set {
                this.mesFinal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del ejercicio fiscal (año)
        /// </summary>
        [DataNames("Ejercicio")]
        [SugarColumn(ColumnDescription = "atributo requerido para la expresión del ejercicio fiscal (año)", Length = 4)]
        public int Ejercicio {
            get { return this.ejercicio; }
            set {
                this.ejercicio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante
        /// </summary>
        [DataNames("MontoTotalOperacion")]
        [SugarColumn(ColumnDescription = "para expresar  el total del monto de la operación  que se relaciona en el comprobante", Length = 11)]
        public decimal MontoTotalOperacion {
            get { return this.montoTotalOperacion; }
            set {
                this.montoTotalOperacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.
        /// </summary>
        [DataNames("MontoTotalGravado")]
        [SugarColumn(ColumnDescription = "para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.", Length = 11)]
        public decimal MontoTotalGravado {
            get { return montoTotalGravado; }
            set {
                montoTotalGravado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        [DataNames("RFCProvCertif")]
        [SugarColumn(ColumnDescription = "RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.", Length = 14)]
        public string RFCProvCertif {
            get {
                if (!(string.IsNullOrEmpty(rfcProveedorCertificacion)))
                    return this.rfcProveedorCertificacion;
                return null;
            }
            set {
                if (!(string.IsNullOrEmpty(value)))
                    this.rfcProveedorCertificacion = value;
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("Estado")]
        [SugarColumn(ColumnDescription = "estado del comprobante del servicio del SAT", Length = 10)]
        public string Estado {
            get {
                return estado;
            }
            set {
                estado = value;
                this.OnPropertyChanged();
            }
        }
    }
}
