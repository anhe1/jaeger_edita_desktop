﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Repositorio.Entities {
    [SugarTable("Solicitud", "Solicitudes de consultas")]
    public class SolicitudModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _index;
        private string _IdSolicitud;
        private string _RFCSolicitante;
        private DateTime _FechaInicial;
        private DateTime _FechaFinal;
        private int _IdTipoSolicitud;
        private int _IdTipo;
        private string _RFCEmisor;
        private string _RFCReceptor;
        private string _Status;
        private string _Descripcion;
        private DateTime? _FechaProcesado;
        private int _TotalCFDIS;
        private bool _IsDescargaZIP;
        private string _PathZIP;
        private VerificacionMetaModel _Verificacion;
        #endregion

        public SolicitudModel() {

        }

        [SugarColumn(ColumnName = "Id", ColumnDescription = "", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return this._index; }
            set { this._index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador de la consulta que se pretende consultar
        /// </summary>
        [SugarColumn(ColumnName = "IdSolicitud", ColumnDescription = "id de solicitud", Length = 40, IsNullable = true)]
        public string IdSolicitud {
            get { return _IdSolicitud; }
            set { _IdSolicitud = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del solicitante de la peticion
        /// </summary>
        [SugarColumn(ColumnName = "RFCSolicitante", ColumnDescription = "rfc del solicitante", Length = 14, IsNullable = true)]
        public string RFCSolicitante {
            get { return _RFCSolicitante; }
            set { this._RFCSolicitante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de inicio
        /// </summary>
        [SugarColumn(ColumnName = "FechaInicio", ColumnDescription = "fecha de incio de la consulta", IsNullable = true)]
        public DateTime FechaInicio {
            get { return this._FechaInicial; }
            set { this._FechaInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha final de la consulta
        /// </summary>
        [SugarColumn(ColumnName = "FechaFinal", ColumnDescription = "fecha final de la solicitud", IsNullable = true)]
        public DateTime FechaFinal {
            get { return this._FechaFinal; }
            set { this._FechaFinal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de descarga (Metadata o CFDI)
        /// </summary>
        [SugarColumn(ColumnName = "TipoSolicitud", ColumnDescription = "tipo de solicitud", IsNullable = true)]
        public int IdTipoSolicitud {
            get { return this._IdTipoSolicitud; }
            set { this._IdTipoSolicitud = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "Tipo", IsNullable = true)]
        public int IdTipo {
            get { return this._IdTipo; }
            set { this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del emisor
        /// </summary>
        [SugarColumn(ColumnName = "RFCEmisor", Length = 14, ColumnDescription = "RFC del emisor", IsNullable = true)]
        public string RFCEmisor {
            get { return this._RFCEmisor; }
            set { this._RFCEmisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del receptor
        /// </summary>
        [SugarColumn(ColumnName = "RFCReceptor", Length = 14, ColumnDescription = "RFC del receptor", IsNullable = true)]
        public string RFCReceptor {
            get { return this._RFCReceptor; }
            set { this._RFCReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo de status de la solicitud
        /// </summary>
        [SugarColumn(ColumnName = "Estatus", Length = 14, ColumnDescription = "Codigo de estado de la solicitud", IsNullable = true)]
        public string Estatus {
            get { return this._Status; }
            set { this._Status = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "Descripcion", Length = 25, ColumnDescription = "Descripcion", IsNullable = true)]
        public string Descripcion {
            get { return this._Descripcion; }
            set { this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "PathZIP", Length = 25, ColumnDescription = "", IsNullable = true)]
        public string PathZIP {
            get { return this._PathZIP; }
            set { this._PathZIP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del proceso
        /// </summary>
        [SugarColumn(ColumnName = "FechaProcesado", ColumnDescription = "fecha de proceso", IsNullable = true)]
        public DateTime? FechaProcesado {
            get { if (this._FechaProcesado >= new DateTime(1900, 1, 1))
                    return this._FechaProcesado;
                return null;
            }
            set { this._FechaProcesado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de CFDI resultantes de la consulta
        /// </summary>
        [SugarColumn(ColumnName = "Total", ColumnDescription = "total de documentos resultantes de la consulta", IsNullable = true)]
        public int TotalCFDIS {
            get { return this._TotalCFDIS; }
            set { this._TotalCFDIS = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "IsDescargaZIP", ColumnDescription = "bandera para indicar si ya fue descargdo el archivo ZIP", IsNullable = true)]
        public bool IsDescargaZIP {
            get { return this._IsDescargaZIP; }
            set { this._IsDescargaZIP = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public VerificacionMetaModel Verificacion {
            get { return this._Verificacion; }
            set { this._Verificacion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
