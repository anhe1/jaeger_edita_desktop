﻿using System;
using Jaeger.Domain.Repositorio.ValueObjects;
using Jaeger.Domain.Services.Mapping;
using Newtonsoft.Json;
using SqlSugar;

namespace Jaeger.Domain.Repositorio.Entities {
    [JsonObject]
    [SugarTable("Comprobantes", "comprobantes fiscales")]
    public class ComprobanteFiscalDetailModel : ComprobanteFiscalModel {
        #region declaraciones
        private string exportacionField;
        private string regimenFiscalReceptor;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveUsoCFDI;
        private string claveMoneda;
        private int parcialidad;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalDetailModel() : base() {
            this.Comprobado = DescargaConciliaEnum.SinVerificar;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer si el comprobante ampara una operación de exportación.
        /// </summary>
        [DataNames("CveExportacion")]
        [SugarColumn(ColumnName = "CveExportacion", ColumnDescription = "expresar si el comprobante ampara una operación de exportación.", Length = 2, IsNullable = true)]
        public string ClaveExportacion {
            get {
                return this.exportacionField;
            }
            set {
                this.exportacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del regimen fiscal del contribuyente receptor al que aplicara el efecto fiscal de este comprobante
        /// </summary>
        [DataNames("CveRegimenFiscal")]
        [SugarColumn(ColumnName = "CveRegimenFiscal", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = true)]
        public string ClaveRegimenFiscal {
            get { return this.regimenFiscalReceptor; }
            set {
                this.regimenFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("CveMetodoPago")]
        [SugarColumn(ColumnName = "CveMetodoPago", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3, IsNullable = true)]
        public string ClaveMetodoPago {
            get { return this.claveMetodoPago; }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("CveFormaPago")]
        [SugarColumn(ColumnName = "CveFormaPago", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2, IsNullable = true)]
        public string ClaveFormaPago {
            get { return this.claveFormaPago; }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [DataNames("CveUsoCFDI")]
        [SugarColumn(ColumnName = "CveUsoCFDI", ColumnDescription = "atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.", Length = 3, IsNullable = true)]
        public string ClaveUsoCFDI {
            get { return this.claveUsoCFDI; }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("CveMoneda")]
        [SugarColumn(ColumnName = "CveMoneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3, IsNullable = true)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de parcialidad
        /// </summary>
        [DataNames("Parcialidad")]
        [SugarColumn(ColumnName = "Parcialidad", ColumnDescription = "numero de parcialidad en el caso de ser un comprobante de pago", IsNullable = true)]
        public int NumParcialidad {
            get { return this.parcialidad; }
            set {
                this.parcialidad = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string ComprobadoText {
            get {
                return Enum.GetName(typeof(DescargaConciliaEnum), this.Comprobado);
            }
            set {
                if (value == Enum.GetName(typeof(DescargaConciliaEnum), DescargaConciliaEnum.SinVerificar))
                    this.Comprobado = DescargaConciliaEnum.SinVerificar;
                else if (value == Enum.GetName(typeof(DescargaConciliaEnum), DescargaConciliaEnum.Verificado))
                    this.Comprobado = DescargaConciliaEnum.Verificado;
                else if (value == Enum.GetName(typeof(DescargaConciliaEnum), DescargaConciliaEnum.NoEncontrado))
                    this.Comprobado = DescargaConciliaEnum.NoEncontrado;
            }
        }

        /// <summary>
        /// obtener 
        /// </summary>
        public string KeyName() {
            return string.Concat(this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaEmision.ToString("yyyyMMddHHmmss"));
        }

        /// <summary>
        /// obtener el nombre del archivo de la representación impresa del acuse de cancelacion, en este caso solo para los comprobantes emitidos.
        /// </summary>
        public string KeyNameAcuse() {
            return string.Concat(this.KeyName(), "-Acuse.pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representacion impresa del acuse final de la cancelacion del comprobante.
        /// </summary>
        public string KeyNameAcuseFinal() {
            return string.Concat(this.KeyName(), "-AcuseFinal.pdf");
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
