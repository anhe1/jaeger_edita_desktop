﻿using System;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Repositorio.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Repositorio.Entities {
    [JsonObject]
    [SugarTable("Comprobantes", "comprobantes fiscales")]
    public class ComprobanteFiscalModel : BasePropertyChangeImplementation, IComprobanteFiscalModel {
        #region declaraciones
        private DateTime? fechaStatusCancelaField;
        private DateTime? fechaValidacionField;
        private DateTime? fechaNuevoField;
        private string idDocumento;
        private string tipo;
        private string emisorRFC;
        private string emisor;
        private string receptorRFC;
        private string receptor;
        private DateTime fechaEmision;
        private DateTime fechaCerfificacion;
        private string pACCertifica;
        private decimal total;
        private string efecto;
        private string statusCancelacion;
        private string estado;
        private string statusProcesoCancelar;
        private string pathXML;
        private string pathPDF;
        private string result;
        private string xmlContentB64;
        private string pdfRepresentacionImpresaB64;
        private string pdfAcuseB64;
        private string pdfAcuseFinalB64;
        private DescargaConciliaEnum comprobado;
        private decimal totalRetencionISR;
        private decimal totalRetencionIVA;
        private decimal totalRetencionIEPS;
        private decimal totalTrasladoIVA;
        private decimal totalTrasladoIEPS;
        private decimal subTotalField;
        private decimal descuentoField;
        private string _Version;
        private string _Motivo;
        private string _FolioSustitucion;
        private string _TerceroRFC;
        private string serie;
        private string _ReceptorDomicilioFiscal;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalModel() {
            this.Comprobado = DescargaConciliaEnum.SinVerificar;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [JsonProperty("idDocumento")]
        [SugarColumn(ColumnName = "IdDocumento", IsPrimaryKey = true, Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set { if (!string.IsNullOrEmpty(value)) {
                    this.idDocumento = value.ToUpper();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el tipo del comprobante (Emitido o Recibido)
        /// </summary>
        [SugarColumn(ColumnName = "Tipo", IsNullable = true, Length = 10)]
        public string Tipo {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer version del estandar bajo el que se encuentra expresado el comprobante
        /// </summary>
        [SugarColumn(ColumnName = "Version", IsNullable = true, Length = 3)]
        public string Version {
            get { return this._Version; }
            set { this._Version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [SugarColumn(ColumnName = "Folio", IsNullable = true, Length = 50)]
        public string Folio { get; set; }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("Serie")]
        [SugarColumn(ColumnName = "Serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25, IsNullable = true)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el rfc del emisor del comprobante
        /// </summary>
        [JsonProperty("emisorRFC")]
        [SugarColumn(ColumnName = "EmisorRFC", IsNullable = true, Length = 16)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Emisor
        /// </summary>
        [JsonProperty("emisor")]
        [SugarColumn(ColumnName = "Emisor", IsNullable = true, Length = 255)]
        public string Emisor {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el RFC del receptor del comprobante
        /// </summary>
        [JsonProperty("receptorRFC")]
        [SugarColumn(ColumnName = "ReceptorRFC", IsNullable = true, Length = 16)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Receptor
        /// </summary>
        [JsonProperty("receptor")]
        [SugarColumn(ColumnName = "Receptor", IsNullable = true, Length = 255)]
        public string Receptor {
            get {
                return this.receptor;
            }
            set {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el codigo postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("ReceptorDomicilioFiscal")]
        [SugarColumn(ColumnName = "ReceptorDomicilioFiscal", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = true)]
        public string ReceptorDomicilioFiscal {
            get { return this._ReceptorDomicilioFiscal; }
            set {
                this._ReceptorDomicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC a cuenta de terceros
        /// </summary>
        [JsonProperty("terceroRFC")]
        [SugarColumn(ColumnName = "TerceroRFC", ColumnDescription = "RFC a cuenta de terceros", IsNullable = true, Length = 14)]
        public string TerceroRFC {
            get { return this._TerceroRFC; }
            set { this._TerceroRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante
        /// </summary>
        [JsonProperty("fechaEmision")]
        [SugarColumn(ColumnName = "FechaEmision", IsNullable = true)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de certificación del comprobante
        /// </summary>
        [JsonProperty("fechacertificacion")]
        [SugarColumn(ColumnName = "FechaCerfificacion", IsNullable = true)]
        public DateTime FechaCerfificacion {
            get {
                return this.fechaCerfificacion;
            }
            set {
                this.fechaCerfificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del PAC que Certificó
        /// </summary>
        [JsonProperty("pac")]
        [SugarColumn(ColumnName = "PAC", IsNullable = true, Length = 16)]
        public string PACCertifica {
            get {
                return this.pACCertifica;
            }
            set {
                this.pACCertifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        [SugarColumn(ColumnName = "RetencionISR", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get {
                return this.totalRetencionISR;
            }
            set {
                this.totalRetencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        [SugarColumn(ColumnName = "RetencionIVA", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get {
                return this.totalRetencionIVA;
            }
            set {
                this.totalRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        [SugarColumn(ColumnName = "RetencionIEPS", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get {
                return this.totalRetencionIEPS;
            }
            set {
                this.totalRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [SugarColumn(ColumnName = "TrasladoIVA", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get {
                return this.totalTrasladoIVA;
            }
            set {
                this.totalTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        [SugarColumn(ColumnName = "TrasladoIEPS", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get {
                return this.totalTrasladoIEPS;
            }
            set {
                this.totalTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [SugarColumn(ColumnName = "SubTotal", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get {
                return this.subTotalField;
            }
            set {
                this.subTotalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [SugarColumn(ColumnName = "Descuento", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor total del comprobante
        /// </summary>
        [JsonProperty("total")]
        [SugarColumn(ColumnName = "Total", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.total;
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [JsonProperty("efecto")]
        [SugarColumn(ColumnName = "Efecto", IsNullable = true, Length = 16)]
        public string Efecto {
            get {
                return this.efecto;
            }
            set {
                this.efecto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer leyenda de Estatus de cancelación (si es posible cancelar con o sin aceptación)
        /// </summary>
        [JsonProperty("statusCancelacion")]
        [SugarColumn(ColumnName = "StatusCancelacion", IsNullable = true, Length = 32)]
        public string StatusCancelacion {
            get {
                return this.statusCancelacion;
            }
            set {
                this.statusCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante Cancelado o Vigente
        /// </summary>
        [JsonProperty("estado")]
        [SugarColumn(ColumnName = "Estado", IsNullable = true, Length = 10)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Estatus de Proceso de Cancelación
        /// </summary>
        [JsonProperty("statusProcesoCancelar")]
        [SugarColumn(ColumnName = "StatusProcesoCancelar", IsNullable = true, Length = 16)]
        public string StatusProcesoCancelar {
            get {
                return this.statusProcesoCancelar;
            }
            set {
                this.statusProcesoCancelar = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(ColumnName = "Motivo", ColumnDescription = "motivo de cancelacion", Length = 30, IsNullable = true)]
        public string Motivo {
            get { return this._Motivo; }
            set { this._Motivo = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(ColumnName = "FolioSustitucion", ColumnDescription = "folio de sustitucion", Length = 255, IsNullable = true)]
        public string FolioSustitucion {
            get { return this._FolioSustitucion; }
            set { this._FolioSustitucion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Fecha de Proceso de Cancelación
        /// </summary>
        [JsonProperty("fechaStatusCancela")]
        [SugarColumn(ColumnName = "FechaStatusCancela", IsNullable = true)]
        public DateTime? FechaStatusCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaStatusCancelaField >= firstGoodDate)
                    return this.fechaStatusCancelaField;
                else
                    return null;
            }
            set {
                this.fechaStatusCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("fechaValidacion")]
        [SugarColumn(ColumnName = "FechaValidacion", IsNullable = true)]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacionField >= firstGoodDate)
                    return this.fechaValidacionField;
                else
                    return null;
            }
            set {
                this.fechaValidacionField = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "FechaNuevo", IsNullable = true)]
        public DateTime? FechaNuevo {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaNuevoField > firstGoodDate)
                    return this.fechaNuevoField;
                return null;
            }
            set {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la ruta del archivo XML
        /// </summary>
        [SugarColumn(ColumnName = "PathXML", IsNullable = true)]
        public string PathXML {
            get {
                return this.pathXML;
            }
            set {
                this.pathXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ruta del archivo PDF
        /// </summary>
        [SugarColumn(ColumnName = "PathPDF", IsNullable = true)]
        public string PathPDF {
            get {
                return this.pathPDF;
            }
            set {
                this.pathPDF = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "Resultado", IsNullable = true, Length = 16)]
        public string Result {
            get {
                return this.result;
            }
            set {
                this.result = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el contenido del comprobante XML en base64
        /// </summary>
        [SugarColumn(ColumnName = "XMLB64", IsNullable = true)]
        public string XmlContentB64 {
            get {
                return this.xmlContentB64;
            }
            set {
                this.xmlContentB64 = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "PDFB64", IsNullable = true)]
        public string PdfRepresentacionImpresaB64 {
            get {
                return this.pdfRepresentacionImpresaB64;
            }
            set {
                this.pdfRepresentacionImpresaB64 = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "AcuseB64", IsNullable = true)]
        public string PdfAcuseB64 {
            get {
                return this.pdfAcuseB64;
            }
            set {
                this.pdfAcuseB64 = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "AcuseFinalB64", IsNullable = true)]
        public string PdfAcuseFinalB64 {
            get {
                return this.pdfAcuseFinalB64;
            }
            set {
                this.pdfAcuseFinalB64 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la bandera que identifica el status de la consilicacion (0 = Sin Comprobar, 1 = Comprobado 2 = No Encontrado)
        /// </summary>
        [SugarColumn(ColumnName = "Comprobado", IsNullable = true, Length = 8)]
        public DescargaConciliaEnum Comprobado {
            get {
                return this.comprobado;
            }
            set {
                this.comprobado = value;
                this.OnPropertyChanged();
            }
        }
    }
}
