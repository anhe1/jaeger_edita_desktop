﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Repositorio.Entities {
    public class SubTipoComprobanteModel : BaseSingleModel {
        public SubTipoComprobanteModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
