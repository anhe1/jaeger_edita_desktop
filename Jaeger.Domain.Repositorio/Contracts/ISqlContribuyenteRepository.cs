﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlContribuyenteRepository : IGenericRepository<ContribuyenteModel> {
        IEnumerable<ContribuyenteModel> GetList(int relacion);

        ContribuyenteModel Save(ContribuyenteModel model);

        void Test(string tipo);
    }
}
