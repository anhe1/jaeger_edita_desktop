﻿using System;
using Jaeger.Domain.Repositorio.ValueObjects;

namespace Jaeger.Domain.Repositorio.Entities {
    interface IComprobanteFiscalModel {
        /// <summary>
        /// obtener o establecer el folio fiscal (uuid) del comprobante
        /// </summary>
        string IdDocumento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el tipo del comprobante (Emitido o Recibido)
        /// </summary>
        string Tipo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el rfc del emisor del comprobante
        /// </summary>
        string EmisorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Emisor
        /// </summary>
        string Emisor {
            get; set;
        }

        /// <summary>
        /// obtener o establacer el RFC del receptor del comprobante
        /// </summary>
        string ReceptorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Receptor
        /// </summary>
        string Receptor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante
        /// </summary>
        DateTime FechaEmision {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de certificación del comprobante
        /// </summary>
        DateTime FechaCerfificacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el RFC del PAC que Certificó
        /// </summary>
        string PACCertifica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        decimal RetencionISR {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        decimal RetencionIVA {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        decimal RetencionIEPS {
            get; set;
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        decimal TrasladoIVA {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        decimal TrasladoIEPS {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        decimal SubTotal {
            get; set;
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        decimal Descuento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer valor total del comprobante
        /// </summary>
        decimal Total {
            get; set;
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Efecto {
            get; set;
        }

        /// <summary>
        /// obtener o establecer leyenda de Estatus de cancelación (si es posible cancelar con o sin aceptación)
        /// </summary>
        string StatusCancelacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante Cancelado o Vigente
        /// </summary>
        string Estado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Estatus de Proceso de Cancelación
        /// </summary>
        string StatusProcesoCancelar {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Fecha de Proceso de Cancelación
        /// </summary>
        DateTime? FechaStatusCancela {
            get; set;
        }

        DateTime? FechaValidacion {
            get; set;
        }

        DateTime? FechaNuevo {
            get; set;
        }

        /// <summary>
        /// obtener o establacer la ruta del archivo XML
        /// </summary>
        string PathXML {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ruta del archivo PDF
        /// </summary>
        string PathPDF {
            get; set;
        }

        string Result {
            get; set;
        }

        string XmlContentB64 {
            get; set;
        }

        string PdfRepresentacionImpresaB64 {
            get; set;
        }

        string PdfAcuseB64 {
            get; set;
        }

        string PdfAcuseFinalB64 {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la bandera que identifica el status de la consilicacion (0 = Sin Comprobar, 1 = Comprobado 2 = No Encontrado)
        /// </summary>
        DescargaConciliaEnum Comprobado {
            get; set;
        }
    }
}