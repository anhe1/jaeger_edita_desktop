﻿using System;

namespace Jaeger.Domain.Repositorio.Contracts {
    interface IRepositorioMeta {
        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string Emisor { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string Receptor { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        string RFCProvCertif { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// minInclusive value ="2014-01-01T00:00:00-06:00"
        /// pattern value = "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))"
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        DateTime FechaCertificacion { get; set; }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        Char EfectoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        int IdEstado { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancela { get; set; }
    }
}