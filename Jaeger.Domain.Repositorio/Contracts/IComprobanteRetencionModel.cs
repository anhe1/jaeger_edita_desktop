﻿using System;

namespace Jaeger.Domain.Repositorio.Contracts {
    interface IComprobanteRetencionModel {
        string IdDocumento { get; set; }
        
        string EmisorRFC { get; set; }
        
        string EmisorNombre { get; set; }
        
        string ReceptorRFC { get; set; }
        
        string ReceptorNombre { get; set; }
        
        DateTime FechaExpedicion { get; set; }
        
        DateTime? FechaCertificacion { get; set; }

        string RFCProvCertif { get; set; }
        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos
        /// </summary>
        int MesInicial { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del mes final del periodo de la retención e información de pagos
        /// </summary>
        int MesFinal { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión del ejercicio fiscal (año)
        /// </summary>
        int Ejercicio { get; set; }

        /// <summary>
        /// Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante
        /// </summary>
        decimal MontoTotalOperacion { get; set; }

        /// <summary>
        /// Atributo requerido para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.
        /// </summary>
        decimal MontoTotalGravado { get; set; }

        string Estado { get; set; }
    }
}