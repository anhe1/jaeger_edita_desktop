﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlComprobanteFiscalRepository : IGenericRepository<ComprobanteFiscalDetailModel> {
        ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel model);

        IEnumerable<ComprobanteFiscalDetailModel> GetList(int month = 0, int anio = 0, string efecto = "Todos", string tipo = "Recibidos");

        /// <summary>
        /// actualizar el estado del comprobante
        /// </summary>
        bool UpdateEstado(string idDocumento, string estado);
    }
}
