﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlVersionRepository : IGenericRepository<VersionModel> {
        VersionModel GetById();

        void test();
    }
}
