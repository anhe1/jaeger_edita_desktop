﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlEmpresaRepository : IGenericRepository<EmpresaModel> {
        EmpresaModel Save(EmpresaModel model);

        bool Delete(EmpresaModel model);

        bool Verificar();

        bool CreateDB();

        bool InitTables();
    }
}
