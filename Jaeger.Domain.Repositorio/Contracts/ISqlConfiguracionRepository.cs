﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using Jaeger.Domain.Repositorio.ValueObjects;
using System.Collections.Generic;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlConfiguracionRepository : IGenericRepository<ConfiguracionModel> {
        ConfiguracionModel GetParametro(string key);
        List<ConfiguracionModel> GetCatagoria(CategoriaEnum categoria);
        bool SetParametro(ConfiguracionModel key);
    }
}
