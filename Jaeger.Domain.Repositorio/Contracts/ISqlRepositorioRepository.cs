﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Domain.Repositorio.Contracts {
    /// <summary>
    /// repositorio de comprobantes fiscales
    /// </summary>
    public interface ISqlRepositorioRepository : IGenericRepository<ComprobanteFiscalModel> {
        /// <summary>
        /// obtener lista de comprobantes fiscales
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <param name="efecto">efecto o tipo de comprobante</param>
        /// <param name="tipo">tipo: Recibidos o Emitidos</param>
        /// <returns></returns>
        IEnumerable<ComprobanteFiscalExtended> GetList(int month = 0, int anio = 0, string efecto = "Todos", string tipo = "Recibidos");

        /// <summary>
        /// crear la base de datos del repositorio local
        /// </summary>
        /// <returns></returns>
        bool CreateDB();

        /// <summary>
        /// crear tablas del sistemta de repositorio
        /// </summary>
        /// <returns></returns>
        bool CreateTables();

        /// <summary>
        /// almacenar lista de objeto de comprobantes fiscales
        /// </summary>
        /// <param name="items"></param>
        void SaveResults(List<ComprobanteFiscalExtended> items);

        /// <summary>
        /// almacenar listado de comprobantes
        /// </summary>
        void Save(List<ComprobanteFiscalModel> items);

        ComprobanteFiscalExtended Update(ComprobanteFiscalExtended item);

        /// <summary>
        /// actualizar el estado del comprobante
        /// </summary>
        bool UpdateEstado(string idDocumento, string estado);
    }
}
