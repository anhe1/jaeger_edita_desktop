﻿using Jaeger.Domain.DataBase.Contracts;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface IConfiguration {
        /// <summary>
        /// obtener o establecer si el repositorio es local (0)
        /// </summary>
        bool IsLocal { get; set; }

        /// <summary>
        /// obtener o establecer si se debe almacenar la representacion impresa (1)
        /// </summary>
        bool StoragePDF { get; set; }

        /// <summary>
        /// obtener o establecer si se debe almacenar la representacion impresa del acuse de cancelacion (2)
        /// </summary>
        bool StorageAcusePDF { get; set; }

        /// <summary>
        /// obtener o establecer si se debe almacenar la representacion impresa del acuse final de cancelacion (3)
        /// </summary>
        bool StorageAcuseFinalPDF { get; set; }

        /// <summary>
        /// obtener o establecer si se permite importar archivos (4)
        /// </summary>
        bool AllowImportXML { get; set; }

        /// <summary>
        /// obtener o establecer si se debe crear directorio de emisores y receptores (5)
        /// </summary>
        bool CreateDirectory { get; set; }

        IDataBaseConfiguracion DataBase { get; set; }
    }
}
