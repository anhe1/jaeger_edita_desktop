﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlConsultaRepository : IGenericRepository<SolicitudModel> {
        IEnumerable<SolicitudModel> GetAll();

        SolicitudModel Save(SolicitudModel solicitudModel);
    }
}
