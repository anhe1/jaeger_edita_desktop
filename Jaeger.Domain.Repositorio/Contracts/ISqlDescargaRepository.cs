﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Repositorio.Entities;

namespace Jaeger.Domain.Repositorio.Contracts {
    public interface ISqlDescargaRepository : IGenericRepository<VerificacionMetaModel> {
        VerificacionMetaModel Save(VerificacionMetaModel model);
    }
}
