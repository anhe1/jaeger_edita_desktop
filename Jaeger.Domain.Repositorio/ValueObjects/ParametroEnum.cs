﻿using System.ComponentModel;

namespace Jaeger.Domain.Repositorio.ValueObjects {
    public enum ParametroEnum {
        [Description("Versión")]
        Version = 0,
        [Description("RFC")]
        RFC = 1,
        [Description("Razon Social")]
        RazonSocial = 2,
    }
}
