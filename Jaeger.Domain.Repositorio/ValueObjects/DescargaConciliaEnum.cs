﻿namespace Jaeger.Domain.Repositorio.ValueObjects {
    public enum DescargaConciliaEnum {
        SinVerificar,
        Verificado,
        NoEncontrado
    }
}
