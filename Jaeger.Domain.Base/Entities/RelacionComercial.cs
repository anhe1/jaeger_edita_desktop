﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Base.Entities {
    public class RelacionComercial : BaseCheckDataItem {

        public RelacionComercial() : base() {
            this.Selected = false;
        }

        public RelacionComercial(int id, string name) : base(id, name) {
            this.Selected = false;
        }

        public RelacionComercial(int id, string name, bool selected = false) : base(id, name, selected) { }
    }
}
