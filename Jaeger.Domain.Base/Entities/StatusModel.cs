﻿namespace Jaeger.Domain.Base.Entities {
    public class StatusModel : Abstractions.BaseSingleModel {
        public StatusModel() {
        }

        public StatusModel(int id, string descripcion) : base(id, descripcion) {

        }

        public object Tag { get; set; }
    }
}