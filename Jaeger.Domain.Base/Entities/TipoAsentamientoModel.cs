﻿namespace Jaeger.Domain.Base.Entities {
    public class TipoAsentamientoModel {
        public TipoAsentamientoModel() {
            this.Activo = 1;
        }

        public TipoAsentamientoModel(int id, int activo, string descripcion) {
            this.Id = id;
            this.Activo = activo;
            this.Descripcion = descripcion;
        }

        public int Id { get; set; }

        public int Activo { get; set; }

        public string Descripcion { get; set; }
    }
}
