﻿namespace Jaeger.Domain.Base.Entities {
    public class CFDIEstadoModel : Abstractions.BaseSingleModel {
        public CFDIEstadoModel() {
        }

        public CFDIEstadoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}