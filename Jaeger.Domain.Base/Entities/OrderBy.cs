﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Entities {
    public class OrderBy : IOrderBy {
        public string FieldName { get; set; }
        public OrderByTypeEnum Type { get; set; }
        public IOrderBy AddFieldName(string fieldName) {
            this.FieldName = fieldName;
            return this;
        }
        public IOrderBy AddOrderBy(OrderByTypeEnum type = OrderByTypeEnum.Asc) {
            this.Type = type;
            return this;
        }
    }
}
