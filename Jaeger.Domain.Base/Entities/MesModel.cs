﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Base.Entities {
    public class MesModel : BaseSingleModel {
        public MesModel() {
        }

        public MesModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
