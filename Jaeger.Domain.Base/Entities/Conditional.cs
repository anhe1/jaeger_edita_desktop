﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Entities {
    public class Conditional : IConditional {
        public Conditional() {
            ConditionalType = ConditionalTypeEnum.Equal;
        }

        public Conditional(string fieldName, string fieldValue, ConditionalTypeEnum conditionalType = ConditionalTypeEnum.Equal) {
            FieldName = fieldName;
            FieldValue = fieldValue;
            ConditionalType = conditionalType;
        }

        public string FieldName {
            get; set;
        }

        public string FieldValue {
            get; set;
        }

        public ConditionalTypeEnum ConditionalType {
            get; set;
        }
    }
}
