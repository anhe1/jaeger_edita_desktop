﻿namespace Jaeger.Domain.Base.Entities {
    public class CancelacionMotivo : Abstractions.BaseSingleModel {
        public CancelacionMotivo() { }

        public CancelacionMotivo(int id, string descripcion) : base(id, descripcion) {
        }
    }
}