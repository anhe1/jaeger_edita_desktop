﻿namespace Jaeger.Domain.Base.Entities {
    public class RecuperarPasswordResponse {
        public bool Exito { get; set; }
        public Mensajes Mensajes { get; set; }
    }

    public partial class Mensajes {
        public string[] App { get; set; }
    }
}