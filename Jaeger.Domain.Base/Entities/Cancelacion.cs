﻿using System;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Base.Entities {
    public class Cancelacion : Abstractions.BasePropertyChangeImplementation, ICancelacion {
        #region declaraciones
        private int _IdComprobante;
        private int _IdStatus;
        private int _IdRelacion;
        private DateTime? _FechaCancela;
        private string _Cancela;
        private int _ClaveCancela;
        private string _NotaCancelacion;
        #endregion

        public Cancelacion() { 
        }

        #region propiedades
        public int IdComprobante {
            get { return _IdComprobante; }
            set { _IdComprobante = value;
                this.OnPropertyChanged();
            }
        }

        public int IdStatus {
            get { return _IdStatus; }
            set { _IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        public int IdRelacion {
            get { return _IdRelacion; }
            set { _IdRelacion = value;
                this.OnPropertyChanged();
            }
        }

        public string Cancela {
            get { return this._Cancela; }
            set {
                this._Cancela = value;
                this.OnPropertyChanged();
            }
        }

        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaCancela {
            get {
                if (this._FechaCancela >= new DateTime(1900, 1, 1))
                    return this._FechaCancela;
                return null;
            }
            set {
                this._FechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        public string NotaCancelacion {
            get { return this._NotaCancelacion; }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion


        public object Tag { get; set; }
    }
}