﻿using System;

namespace Jaeger.Domain.Base.Entities {
    public class Certificado : Contracts.ICertificado {
        #region declaraciones
        private DateTime? _InicioVigencia;
        private DateTime? _FinVigencia;
        #endregion

        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social
        /// </summary>
        public string RazonSocial { get; set; }

        /// <summary>
        /// obtener o establecer numero de serie
        /// </summary>
        public string Serie { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de inicio de vigencia
        /// </summary>
        public DateTime? InicioVigencia {
            get {
                if (this._InicioVigencia >= new DateTime(1900, 1, 1)) {
                    return this._InicioVigencia;
                }
                return null;
            }
            set {
                this._InicioVigencia = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha final de vigencia
        /// </summary>
        public DateTime? FinalVigencia {
            get {
                if (this._FinVigencia >= new DateTime(1900, 1, 1)) {
                    return this._FinVigencia;
                }
                return null;
            }
            set {
                this._FinVigencia = value;
            }
        }

        public string Tipo { get; set; }

        /// <summary>
        /// obtener o establecer contrasena es la que corresponde la clave privada .key
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// obtener o establecer certificado base64
        /// </summary>
        public string FileCer { get; set; }

        /// <summary>
        /// obtener o establecer llave key en base64
        /// </summary>
        public string FileKey { get; set; }

        public DateTime FechaNuevo { get; set; }

        public bool IsActive() {
            if (this.InicioVigencia != null && this.FinalVigencia != null) {
                return this.FinalVigencia < DateTime.Now;
            }
            return false;
        }

        public object Tag { get; set; }
    }
}
