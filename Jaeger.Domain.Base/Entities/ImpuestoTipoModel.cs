﻿namespace Jaeger.Domain.Base.Entities {
    public class ImpuestoTipoModel : Abstractions.BaseSingleModel {
        public ImpuestoTipoModel() {
        }

        public ImpuestoTipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
