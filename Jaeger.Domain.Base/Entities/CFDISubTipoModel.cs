﻿namespace Jaeger.Domain.Base.Entities {
    public class CFDISubTipoModel : Abstractions.BaseSingleModel {
        public CFDISubTipoModel() {
        }

        public CFDISubTipoModel(int id, string descripcion) : base(id, descripcion) {

        }
    }
}
