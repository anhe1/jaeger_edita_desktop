﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Entities {
    public class OrderByBuilder : IOrderByBuilder, IOrderByTypeBuilder, IOrderByBuild {
        protected internal IOrderBy _OrderBy;

        public OrderByBuilder() {
            _OrderBy = new OrderBy();
        }

        public IOrderByBuild AddFieldName(string fieldName) {
            _OrderBy.AddFieldName(fieldName);
            return this;
        }

        public IOrderByBuild AddOrderBy(OrderByTypeEnum type = OrderByTypeEnum.Asc) {
            _OrderBy.AddOrderBy(type);
            return this;
        }

        public IOrderBy Build() {
            return _OrderBy;
        }
    }
}
