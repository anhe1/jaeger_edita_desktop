﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Base.Entities {
    public class MonedaModel : BaseSingleModel {
        public MonedaModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
