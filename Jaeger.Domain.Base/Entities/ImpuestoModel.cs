﻿namespace Jaeger.Domain.Base.Entities {
    public class ImpuestoModel : Abstractions.BaseSingleModel {
        public ImpuestoModel() {
        }

        public ImpuestoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
