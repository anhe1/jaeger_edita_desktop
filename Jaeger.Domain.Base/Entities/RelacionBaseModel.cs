﻿namespace Jaeger.Domain.Base.Entities {
    public class RelacionBaseModel : Abstractions.BasePropertyChangeImplementation {
        private int index;
        private bool selected;
        private string name;

        public RelacionBaseModel() {
            this.selected = true;
            this.name = string.Empty;
        }

        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        public string Name {
            get {
                return this.name;
            }
            set {
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        public bool Selected {
            get {
                return this.selected;
            }
            set {
                this.selected = value;
                this.OnPropertyChanged();
            }
        }
    }
}