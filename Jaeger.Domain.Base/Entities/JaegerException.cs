﻿using System;

namespace Jaeger.Domain.Base.Entities {
    public class JaegerException : Exception {
        public JaegerException() {
        }

        public JaegerException(string message)
            : base(message) {
        }

        public JaegerException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}