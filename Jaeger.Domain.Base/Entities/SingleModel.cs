﻿using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Base.Entities {
    public class SingleModel : Abstractions.BaseSingleModel, ISingleModel {

        public SingleModel() {

        }

        public SingleModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
