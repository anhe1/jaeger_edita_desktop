﻿namespace Jaeger.Domain.Base.Entities {
    /// <summary>
    /// clase para comun para seleccion
    /// </summary>
    public class ItemSelectedModel : Abstractions.BasePropertyChangeImplementation {
        private int id;
        private bool selected;
        private string name;

        public ItemSelectedModel() {

        }

        public ItemSelectedModel(int id, bool selected, string name) {
            Id = id;
            Selected = selected;
            Name = name;
        }

        public int Id {
            get { return id; }
            set {
                id = value;
                OnPropertyChanged();
            }
        }

        public bool Selected {
            get { return selected; }
            set {
                selected = value;
                OnPropertyChanged();
            }
        }

        public string Name {
            get { return name; }
            set {
                name = value;
                OnPropertyChanged();
            }
        }
    }
}