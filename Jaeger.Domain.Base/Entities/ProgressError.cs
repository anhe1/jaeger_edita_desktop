﻿namespace Jaeger.Domain.Base.Entities {
    public class ProgressError {
        public ProgressError(string data) {
            this.Data = data;
        }

        public string Data;
    }
}