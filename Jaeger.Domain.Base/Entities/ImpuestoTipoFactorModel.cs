﻿namespace Jaeger.Domain.Base.Entities {
    public class ImpuestoTipoFactorModel : Abstractions.BaseSingleModel {
        public ImpuestoTipoFactorModel() {
        }

        public ImpuestoTipoFactorModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
