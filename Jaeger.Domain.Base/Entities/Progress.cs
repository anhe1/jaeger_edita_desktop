﻿namespace Jaeger.Domain.Base.Entities {
    public class Progress {
        public Progress(int complete, int total, int counter, string caption) {
            this.Complete = complete;
            this.Counter = counter;
            this.Data = caption;
            this.Total = total;
        }

        public Progress(string caption) {
            this.Data = caption;
        }

        /// <summary>
        /// obtener o establecer el % completado
        /// </summary>
        public int Complete { get; set; }

        /// <summary>
        /// obtener o establecer el contador de items
        /// </summary>
        public int Counter { get; set; }

        public int Total { get; set; }

        /// <summary>
        /// obtener o establecer texto
        /// </summary>
        public string Data { get; set; }
    }
}