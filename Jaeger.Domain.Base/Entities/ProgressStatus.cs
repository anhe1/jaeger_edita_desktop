﻿namespace Jaeger.Domain.Base.Entities {
    public class ProgressStatus {
        public ProgressStatus(string data) {
            this.Data = data;
        }

        public string Data { get; set; }
    }
}