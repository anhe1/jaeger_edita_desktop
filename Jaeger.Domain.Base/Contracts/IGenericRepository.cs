﻿using System.Collections.Generic;

namespace Jaeger.Domain.Contracts {
    /// <summary>
    /// interface generica para repositorios
    /// </summary>
    public interface IGenericRepository<T> where T : class, new() {
        /// <summary>
        /// metodo para insertar registro
        /// </summary>
        int Insert(T item);

        /// <summary>
        /// actualizar registro
        /// </summary>
        int Update(T item);

        /// <summary>
        /// eliminar registro
        /// </summary>
        bool Delete(int index);

        /// <summary>
        /// obtener registro por el indice
        /// </summary>
        T GetById(int index);

        /// <summary>
        /// lista generica de elementos modelo
        /// </summary>
        IEnumerable<T> GetList();

        string Message { get; set; }

        //bool InitTable();
    }
}