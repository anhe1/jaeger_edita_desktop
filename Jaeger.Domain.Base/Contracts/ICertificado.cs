﻿namespace Jaeger.Domain.Base.Contracts {
    public interface ICertificado {
        /// <summary>
        /// obtener o establecer registro federal de contribuyentes (RFC)
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social
        /// </summary>
        string RazonSocial { get; set; }

        string Tipo { get; set; }

        /// <summary>
        /// obtener o establecer password
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// obtener o establecer numero de serie del certificado
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer fecha de inicio de la vigencia
        /// </summary>
        System.DateTime? InicioVigencia { get; set; }

        /// <summary>
        /// obtener o establecer fecha de final de la vigencia
        /// </summary>
        System.DateTime? FinalVigencia { get; set; }

        /// <summary>
        /// obtener o establecer contenido del archivo en base64
        /// </summary>
        string FileCer { get; set; }

        /// <summary>
        /// obtener o establecer contenido del archivo en base64
        /// </summary>
        string FileKey { get; set; }

        bool IsActive();

        object Tag { get; set; }
    }
}
