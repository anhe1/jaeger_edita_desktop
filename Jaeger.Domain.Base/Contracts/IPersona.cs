﻿namespace Jaeger.Domain.Base.Contracts {
    public interface IPersona {
        string RFC { get; set; }
        string CURP { get; set; }
        string Nombre { get; set; }
        string NombreComercial { get; set; }
        string DomicilioFiscal { get; set; }
    }
}
