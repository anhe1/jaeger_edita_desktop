﻿namespace Jaeger.Domain.Base.Contracts {
    /// <summary>
    /// contrato para clase unidad
    /// </summary>
    public interface IUnidad {
        /// <summary>
        /// obtener o establecer indice de la unidad
        /// </summary>
        int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la unidad
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer factor de conversion de unidad
        /// </summary>
        decimal Factor { get; set; }
    }
}
