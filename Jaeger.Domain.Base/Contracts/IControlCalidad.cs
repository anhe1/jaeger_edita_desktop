﻿using System;

namespace Jaeger.Domain.Base.Contracts {
    public interface IControlCalidad {
        /// <summary>
        /// obtener o establecer clave del usuario auditor
        /// </summary>
        string Auditor { get; set; }

        /// <summary>
        /// obtener o establecer fecha de liberacion calidad
        /// </summary>
        DateTime? FechaCalidad { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        string NotaCalidad { get; set; }
    }
}
