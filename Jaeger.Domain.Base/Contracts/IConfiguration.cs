﻿namespace Jaeger.Domain.Base.Contracts {
    /// <summary>
    /// interface de configuración
    /// </summary>
    public interface IConfiguration {
        /// <summary>
        /// obtener version de la aplicacion
        /// </summary>
        string Version { get; }
    }
}
