﻿using System;

namespace Jaeger.Domain.Base.Contracts {
    public interface ICancelacion {
        int IdStatus { get; set; }

        string Cancela { get; set; }

        int ClaveCancela { get; set; }

        DateTime? FechaCancela { get; set; }

        string NotaCancelacion { get; set; }
    }
}
