﻿namespace Jaeger.Domain.Base.Contracts {
    public interface ICategoria {
        /// <summary>
        /// indice de la categoria
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// indice de la sub categoria
        /// </summary>
        int IdSubCategoria { get; set; }

        /// <summary>
        /// obtener o establecer descripcion 
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave o ruta web
        /// </summary>
        string Clave { get; set; }
    }
}
