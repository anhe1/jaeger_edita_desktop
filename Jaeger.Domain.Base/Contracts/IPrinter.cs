﻿namespace Jaeger.Domain.Base.Contracts {
    public interface IPrinter {
        bool IsValues​Visible { get; set; }
        string ReportName { get; }
        string[] QrText { get; }
    }
}
