﻿namespace Jaeger.Domain.Base.Contracts {
    public interface ISingleModel {
        /// <summary>
        /// obtener o establecer el indice del elemento
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion del elemento
        /// </summary>
        string Descripcion { get; set; }

        string Descriptor {
            get;
        }
    }
}
