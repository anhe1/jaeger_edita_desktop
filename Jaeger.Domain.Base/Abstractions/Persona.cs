﻿namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// Clase que representa el menu para el usuario
    /// </summary>
    public class Persona : BasePropertyChangeImplementation, Contracts.IPersona {
        #region declaraciones
        private string rfc;
        private string nombre;
        private string _NombreComericial;
        private string _DomicilioFiscal;
        private string _CURP;
        #endregion

        public Persona() {
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        public string CURP {
            get { return this._CURP; }
            set { this._CURP = value;}
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        public string RFC {
            get { return this.rfc; }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        public string Nombre {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreComercial {
            get { return this._NombreComericial; }
            set { this._NombreComericial = value; }
        }

        public string DomicilioFiscal {
            get { return this._DomicilioFiscal; }
            set { this._DomicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }
    }
}