﻿using System.Linq;

namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// clase de elemento del menu
    /// </summary>
    public class UIMenuElement : UIMenuBaseItem {
        private System.Collections.Generic.List<ValueObjects.UIActionEnum> lPermisos;

        /// <summary>
        /// constructor
        /// </summary>
        public UIMenuElement() : base() {
            this.Permisos = "0000000000";
            this.lPermisos = new System.Collections.Generic.List<Base.ValueObjects.UIActionEnum>();
            this.Rol = "desarrollador";
            this.Parent = "0";
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">key</param>
        /// <param name="label">etiqueta</param>
        /// <param name="toolTipText">tip de ayufa</param>
        /// <param name="rol">nombres de los roles separados por comas (,)</param>
        public UIMenuElement(string name, string label, string toolTipText, string rol, string form = null) : base() {
            this.Label = label;
            this.Name = name;
            this.ToolTipText = toolTipText;
            this.Rol = rol;
            this.Form = form;
        }

        public UIMenuElement(UIMenuBaseItem baseItem) {
            this.Label = baseItem.Label;
            this.Name = baseItem.Name;
            this.ToolTipText = baseItem.ToolTipText;
            this.Rol = baseItem.Rol;
            this.Form = baseItem.Form;
        }

        public System.Collections.Generic.List<ValueObjects.UIActionEnum> LPermisos {
            get { return this.lPermisos; }
            set { this.lPermisos = value;
                var _temporal = this.Permisos.ToCharArray();
                if (value != null) {
                    foreach (var item in value) {
                        _temporal[(int)item] = '1';
                    }
                    this.Permisos = string.Join("", _temporal);
                }
            }
        }

        public bool IsEnabled { get; set; }

        public ValueObjects.UIAction Action {
            get { if (this.LPermisos != null)
                    return new ValueObjects.UIAction(this.Permisos);
                return new ValueObjects.UIAction();
            }
        }

        public override string ToString() {
            var salida = new System.Text.StringBuilder();
            salida.Append($"new UIMenuElement {{ Id = {this.Id}, ParentId = {this.ParentId}, Name = \"{this.Name}\", Label = \"{this.Label}\", Parent = \"{Parent}\"");
            if (!string.IsNullOrEmpty(this.Form)) {
                salida.Append($", Form = \"{this.Form}\", Assembly = \"{this.Assembly}\"");
            }
            if (!string.IsNullOrEmpty(this.ToolTipText)) {
                salida.Append($", ToolTipText = \"{this.ToolTipText}\"");
            }

            if (this.LPermisos != null) {
                if (this.LPermisos.Count > 0) {
                    salida.Append(", LPermisos = new List<UIActionEnum>() {");
                    var c0 = 0;
                    foreach (var item in LPermisos) {
                        salida.Append($" UIActionEnum.{item.ToString()}");
                        c0++;
                        if (!(c0 == LPermisos.Count))
                            salida.Append(",");
                    }
                    salida.Append(" }");
                }
            }

            if (this.Default == true) {
                salida.Append(", Default = true ");
            }

            if (this.Rol == "*") {
                salida.Append(", Rol = \"*\"");
            }
            if (this.IsAvailable == true) {
                salida.Append(", IsAvailable = true ");
            }
            salida.Append("},");
            return salida.ToString();
        }
    }
}