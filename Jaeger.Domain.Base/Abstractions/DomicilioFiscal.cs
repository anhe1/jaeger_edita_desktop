﻿using System;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// clase abstracta para representar domicilio fiscal estandar
    /// </summary>
    public abstract class DomicilioFiscal : BasePropertyChangeImplementation, IDomicilioFiscalModel {
        #region declaraciones
        private int index;
        private bool isActive;
        private int subId;
        private string codigoPais;
        private string creo;
        private string modifica;
        private string codigoPostal;
        private string tipo;
        private string asentamiento;
        private string noExterior;
        private string noInterior;
        private string calle;
        private string colonia;
        private string municipio;
        private string ciudad;
        private string estado;
        private string pais;
        private string localidad;
        private string referencia;
        private string telefono;
        private string notas;
        private string requerimiento;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        #endregion

        public DomicilioFiscal() {
            this.isActive = true;
            this.fechaNuevo = DateTime.Now;
            this.TipoEnum = TipoDomicilioEnum.Fiscal;
        }

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>           
        public bool Activo {
            get {
                return this.isActive;
            }
            set {
                this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        public int SubId {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo de pais
        /// </summary>           
        public string CodigoPais {
            get {
                return this.codigoPais;
            }
            set {
                this.codigoPais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>           
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>           
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:codigo postal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string CodigoPostal {
            get {
                return this.codigoPostal;
            }
            set {
                this.codigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Tipo {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de domicilio registrado
        /// </summary>
        public TipoDomicilioEnum TipoEnum {
            get {
                return (TipoDomicilioEnum)(Enum.Parse(typeof(TipoDomicilioEnum), this.Tipo));
            }
            set {
                this.Tipo = Enum.GetName(typeof(TipoDomicilioEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:asentamiento humano
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Asentamiento {
            get {
                return this.asentamiento;
            }
            set {
                this.asentamiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero exterior
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string NoExterior {
            get {
                return this.noExterior;
            }
            set {
                this.noExterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero interior
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string NoInterior {
            get {
                return this.noInterior;
            }
            set {
                this.noInterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:calle
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Calle {
            get {
                return this.calle;
            }
            set {
                this.calle = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:colonia
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Colonia {
            get {
                return this.colonia;
            }
            set {
                this.colonia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:delegacion / municipio
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Delegacion {
            get {
                return this.municipio;
            }
            set {
                this.municipio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ciudad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string Ciudad {
            get {
                return this.ciudad;
            }
            set {
                this.ciudad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:estado
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:pais
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Pais {
            get {
                return this.pais;
            }
            set {
                this.pais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:localidad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string Localidad {
            get {
                return this.localidad;
            }
            set {
                this.localidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:referencia
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string Referencia {
            get {
                return this.referencia;
            }
            set {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string Telefono {
            get {
                return this.telefono;
            }
            set {
                if (value != null)
                    this.telefono = value.Replace("-", "").Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion de la ubicacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string Notas {
            get {
                return this.notas;
            }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:horario y requisito de acceso
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public string Requerimiento {
            get {
                return this.requerimiento;
            }
            set {
                this.requerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>           
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1)) {
                    return this.fechaModifica;
                }
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// domicilio en formato texto
        /// </summary>
        public virtual string Domicilio {
            get {
                return this.ToString();
            }
        }

        /// <summary>
        /// direccion completa
        /// </summary>
        public override string ToString() {
            string direccion = string.Empty;

            if (!(string.IsNullOrEmpty(this.Calle)))
                direccion = string.Concat("Calle ", this.Calle);

            if (!(string.IsNullOrEmpty(this.NoExterior)))
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);

            if (!(string.IsNullOrEmpty(this.NoInterior)))
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);

            if (!(string.IsNullOrEmpty(this.Colonia)))
                direccion = string.Concat(direccion, " Col. ", this.Colonia);

            if (!(string.IsNullOrEmpty(this.CodigoPostal)))
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);

            if (!(string.IsNullOrEmpty(this.Delegacion)))
                direccion = string.Concat(direccion, " ", this.Delegacion);

            if (!(string.IsNullOrEmpty(this.Estado)))
                direccion = string.Concat(direccion, ", ", this.Estado);

            if (!(string.IsNullOrEmpty(this.Referencia)))
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);

            if (!(string.IsNullOrEmpty(this.Localidad)))
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);

            return direccion;
        }
    }
}
