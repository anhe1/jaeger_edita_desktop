﻿using System.Text;

namespace Jaeger.Domain.Base.ValueObjects {
    public class UIAction {
        private readonly string _Action;
        private bool _Cancelar = false;
        private bool _Agregar = false;
        private bool _Remover = false;
        private bool _Editar = false;
        private bool _Autorizar = false;
        private bool _Imprimir = false;
        private bool _Exportar = false;
        private bool _Importar = false;
        private bool _Status = false;
        private bool _Reporte = false;

        public UIAction() {

        }

        public UIAction(string action) {
            this._Action = action;
            if (this._Action != null) {
                if (this._Action.Length > 0) {
                    for (int i = 0; i < this._Action.Length; i++) {
                        if (i == 0) {
                            _Cancelar = int.Parse(action[0].ToString()) > 0;
                        } else if (i == 1) {
                            _Agregar = int.Parse(action[1].ToString()) > 0;
                        } else if (i == 2) {
                            _Remover = int.Parse(action[2].ToString()) > 0;
                        } else if (i == 3) {
                            _Editar = int.Parse(action[3].ToString()) > 0;
                        } else if (i == 4) {
                            _Autorizar = int.Parse(action[4].ToString()) > 0;
                        } else if (i == 5) {
                            _Imprimir = int.Parse(action[5].ToString()) > 0;
                        } else if (i == 6) {
                            _Exportar = int.Parse(action[6].ToString()) > 0;
                        } else if (i == 7) {
                            _Importar = int.Parse(action[7].ToString()) > 0;
                        } else if (i == 8) {
                            _Status = int.Parse(action[8].ToString()) > 0;
                        } else if (i == 9) {
                            _Reporte = int.Parse(action[9].ToString()) > 0;
                        }
                    }
                }
            }
        }

        public UIAction(System.Collections.Generic.List<UIActionEnum> actionEnums) {
            foreach (var item in actionEnums) {
                switch (item) {
                    case UIActionEnum.Cancelar:
                        this._Cancelar = true;
                        break;
                    case UIActionEnum.Agregar:
                        this._Agregar = true;
                        break;
                    case UIActionEnum.Remover:
                        this._Remover = true;
                        break;
                    case UIActionEnum.Editar:
                        this._Editar = true;
                        break;
                    case UIActionEnum.Autorizar:
                        this._Autorizar = true;
                        break;
                    case UIActionEnum.Imprimir:
                        this._Imprimir = true;
                        break;
                    case UIActionEnum.Exportar:
                        this._Exportar = true;
                        break;
                    case UIActionEnum.Importar:
                        this._Importar = true;
                        break;
                    case UIActionEnum.Status:
                        this._Status = true;
                        break;
                    case UIActionEnum.Reporte:
                        this._Reporte = true;
                        break;
                    default:
                        break;
                }
            }
        }

        public bool Cancelar {
            get { return this._Cancelar; }
            set { this._Cancelar = value; }
        }

        public bool Agregar {
            get { return this._Agregar; }
            set { this._Agregar = value;}
        }

        public bool Remover {
            get { return this._Remover; }
            set { this._Remover = value; }
        }

        public bool Editar {
            get { return this._Editar; }
            set { this._Editar = value; }
        }

        public bool Autorizar {
            get { return this._Autorizar; }
            set { this._Autorizar = value; }
        }

        public bool Imprimir {
            get { return this._Imprimir; }
            set {  this._Imprimir = value;}
        }

        public bool Exportar {
            get { return this._Exportar; }
            set {  this._Exportar = value; }
        }

        public bool Importar {
            get { return this._Importar; }
            set {  this._Importar = value; }    
        }

        public bool Status {
            get { return this._Status; }   
            set {  this._Status = value; }
        }

        public bool Reporte {
            get { return this._Reporte; }
            set {  this._Reporte = value; }
        }

        public string Acciones {
            get {
                var _salida = new StringBuilder();
                _salida.Append(Cancelar ? "1" : "0");
                _salida.Append(Agregar ? "1" : "0");
                _salida.Append(Remover ? "1" : "0");
                _salida.Append(Editar ? "1" : "0");
                _salida.Append(Autorizar ? "1" : "0");
                _salida.Append(Imprimir ? "1" : "0");
                _salida.Append(Exportar ? "1" : "0");
                _salida.Append(Importar ? "1" : "0");
                _salida.Append(Status ? "1" : "0");
                _salida.Append(Reporte ? "1" : "0");
                return _salida.ToString();
            }
        }
    }
}
