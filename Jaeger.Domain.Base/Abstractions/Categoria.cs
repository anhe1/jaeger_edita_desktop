﻿using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Base.Abstractions {
    public abstract class Categoria : BasePropertyChangeImplementation, ICategoria {
        /// <summary>
        /// indice de la categoria
        /// </summary>
        public int IdCategoria { get; set; }

        /// <summary>
        /// indice de la sub categoria
        /// </summary>
        public int IdSubCategoria { get; set; }

        /// <summary>
        /// obtener o establecer descripcion 
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave o ruta web
        /// </summary>
        public string Clave { get; set; }

        
    }
}
