﻿namespace Jaeger.Domain.Base.Abstractions {
    public abstract class UIMenuRolRelacion : BasePropertyChangeImplementation {
        #region declaraciones
        private int _IdRelacion;
        private int _IdUser;
        private int _IdRol;
        #endregion

        public UIMenuRolRelacion() { }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice de relacion
        /// </summary>
        public int IdRelacion {
            get { return this._IdRelacion; }
            set {
                this._IdRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del usuario
        /// </summary>
        public int IdUser {
            get { return this._IdUser; }
            set {
                this._IdUser = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion del ROL
        /// </summary>
        public int IdUserRol {
            get { return this._IdRol; }
            set {
                this._IdRol = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}