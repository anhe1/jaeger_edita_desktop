﻿namespace Jaeger.Domain.Base.Abstractions {
    public class Rol {

        private string name;
        public Rol() {
            this.name = "Guest";
        }

        public Rol(string roleName) {
            name = roleName;
        }

        public virtual string Name {
            get {
                return name;
            }
            set {
                name = value;
            }
        }

        // override object.Equals
        public override bool Equals(object obj) {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }

            if (Name.Equals((obj as Rol).Name)) {
                return true;
            }
            return false;
        }

        // override object.GetHashCode
        public override int GetHashCode() {
            // TODO: write your implementation of GetHashCode() here
            return base.GetHashCode();
        }
    }
}