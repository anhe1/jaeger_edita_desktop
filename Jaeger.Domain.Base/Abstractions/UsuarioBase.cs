﻿using System.Collections;

namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// 
    /// </summary>
    public abstract class UsuarioBase : BasePropertyChangeImplementation {
        #region declaraciones
        private int _Index;
        private int _IdPerfil;
        private int _IdDirectorio;
        private string _Clave;
        private string _Correo;
        private string _Nombre;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public UsuarioBase() {
            this.Roles = new ArrayList();
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// </summary>           
        public int Id {
            get {
                return this._Index;
            }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ID de roll
        /// </summary>           
        public int IdPerfil {
            get {
                return this._IdPerfil;
            }
            set {
                this._IdPerfil = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio, utilizado principalmente para clientes y vendedores
        /// </summary>
        public int IdDirectorio {
            get {
                return this._IdDirectorio;
            }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave unica de registro en sistema
        /// </summary>           
        public string Clave {
            get {
                return this._Clave;
            }
            set {
                this._Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:correo electronic, separados por (;)
        /// </summary>           
        public string Correo {
            get {
                return this._Correo;
            }
            set {
                this._Correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre
        /// </summary>           
        public string Nombre {
            get {
                return this._Nombre;
            }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }

        public IList Roles {
            get; set;
        }

        /// <summary>
        /// IsGuest property
        /// </summary>
        public bool IsGuest {
            get; set;
        }

        /// <summary>
        /// Check whether user has this role or not.
        /// Note : If have '*' or '@' in your roles , it will be override all roles
        /// </summary>
        /// <param name="checkRole"></param>
        /// <returns></returns>
        public virtual bool IsInRole(Rol checkRole) {

            // if checkRole = * so everyone can use
            if (checkRole.Name.Equals("*")) {
                return true;
            }
            // if checkRole = @ so just logged in user
            if (checkRole.Name.Equals("@")) {
                return !IsGuest;
            }

            if (checkRole.Name.Equals("%")) {
                return IsGuest;
            }

            IList innerRoles = Roles;
            // if does not has role then return false
            if (innerRoles == null) {
                return false;
            }

            foreach (Rol role in innerRoles) {

                // if has role name so check
                if (role.Equals(checkRole)) {
                    return true;
                }
            }
            return false;
        }
    }
}