﻿namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// interface de configuración
    /// </summary>
    public abstract class Configuration : BasePropertyChangeImplementation, Contracts.IConfiguration {
        /// <summary>
        /// obtener version de la aplicacion
        /// </summary>
        public string Version { get; set; }
    }
}
