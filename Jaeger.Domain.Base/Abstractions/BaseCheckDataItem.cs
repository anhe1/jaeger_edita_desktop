﻿namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// clase abstracta utiliza para la seleccion en objetos RadCheckedDropDownList de telerik
    /// </summary>
    public abstract class BaseCheckDataItem : BasePropertyChangeImplementation {
        #region declaraciones
        private int _Index;
        private bool _Selected;
        private string _Name;
        #endregion

        public BaseCheckDataItem() {
            this._Selected = true;
            this._Name = string.Empty;
        }

        protected BaseCheckDataItem(int id, string name, bool selected = false) {
            this.Id = id;
            this.Name = name;
            this.Selected = selected;
        }

        /// <summary>
        /// obtner o establecer indice
        /// </summary>
        public int Id {
            get {
                return this._Index;
            }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del elemento
        /// </summary>
        public string Name {
            get {
                return this._Name;
            }
            set {
                this._Name = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el objeto esta seleccionado
        /// </summary>
        public bool Selected {
            get {
                return this._Selected;
            }
            set {
                this._Selected = value;
                this.OnPropertyChanged();
            }
        }
    }
}
