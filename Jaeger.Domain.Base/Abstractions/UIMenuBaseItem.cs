﻿using System.Collections.Generic;
using System.Linq;

namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// 
    /// </summary>
    public class UIMenuBaseItem : BasePropertyChangeImplementation {
        #region declaraciones
        private string _Rol;
        private string _Name;
        private string _Parent;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public UIMenuBaseItem() {
            this.Default = false;
            this.IsAvailable = true;
            this.Assembly = "Jaeger.UI";
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">key</param>
        /// <param name="label">etiqueta</param>
        /// <param name="toolTipText">tip de ayuda</param>
        /// <param name="rol">nombres de los roles separados por comas (,)</param>
        public UIMenuBaseItem(string name, string label, string toolTipText, string rol) {
            this.Label = label;
            this.Name = name;
            this.ToolTipText = toolTipText;
            this.Rol = rol;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Default { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// obtener o establecer la llave 
        /// </summary>
        public string Name {
            get {
                if (_Name != null)
                    return _Name.ToLower();
                return string.Empty;
            }
            set {
                if (value != null) {
                    _Name = value.ToLower();
                    this.OnPropertyChanged();
                }
            }
        }

        public string Parent {
            get {
                if (_Parent != null)
                    return _Parent.ToLower();
                return string.Empty;
            }
            set {
                if (value != null) {
                    _Parent = value.ToLower();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la etiqueta que aparecera en el elemento del menu
        /// </summary>
        public string Label {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el tip de ayuda del elemento
        /// </summary>
        public string ToolTipText {
            get; set;
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        public string Form {
            get; set;
        }

        public string Assembly {
            get; set;
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Permisos {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el rol que debe ser aplicado separado por comas (,)
        /// </summary>
        public string Rol {
            get {
                if (this._Rol != null)
                    return this._Rol.ToLower();
                return string.Empty;
            }
            set {
                if (value != null) {
                    this._Rol = value.ToLower();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer lista de roles
        /// </summary>
        public List<Rol> ListRol {
            get {
                return new List<Rol>(this.Rol.Split(',').Select(c => new Rol(c)));
            }
        }

        /// <summary>
        /// obtener o establecer si objeto esta disponible
        /// </summary>
        public bool IsAvailable { get; set; }

        public object Tag { get; set; }
        #endregion
    }
}