﻿namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// Rol de usuario
    /// </summary>
    public abstract class UserRol : BasePropertyChangeImplementation {
        #region declaracion
        private int _idUserRol;
        private int idUserMaster;
        private string clave;
        private bool activo;
        private string nombre;
        #endregion

        public UserRol() {
            this.Activo = true;
        }

        /// <summary>
        /// obtener o establecer el indice del rol de usuarios
        /// </summary>
        public int IdUserRol {
            get {
                return this._idUserRol;
            }
            set {
                this._idUserRol = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice que indica si el usuario es master
        /// </summary>           
        public int IdMaster {
            get {
                return this.idUserMaster;
            }
            set {
                this.idUserMaster = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de ROL
        /// </summary>
        public string Clave {
            get {
                return this.clave;
            }
            set {
                if (value != null) {
                    this.clave = value.Trim().Replace(" ", "-").Replace("é", "e").Replace("á", "a").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").ToLower();
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del ROL
        /// </summary>
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }
    }
}