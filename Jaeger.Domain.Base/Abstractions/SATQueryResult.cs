﻿using System;

namespace Jaeger.Domain.Base.Abstractions {
    public class SATQueryResult : BasePropertyChangeImplementation {
        private int _index;
        private string _status;
        private string _codigo;
        private DateTime? _fecha;
        private string _key;

        public int Id {
            get {
                return this._index;
            }
            set {
                this._index = value;
                this.OnPropertyChanged();
            }
        }

        public string Key {
            get {
                return this._key;
            }
            set {
                this._key = value;
                this.OnPropertyChanged();
            }
        }

        public string Clave {
            get {
                return this._codigo;
            }
            set {
                this._codigo = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? Fecha {
            get {
                if (this._fecha >= new DateTime(1900, 1, 1))
                    return this._fecha;
                return null;
            }
            set {
                this._fecha = value;
                this.OnPropertyChanged();
            }
        }

        public string Status {
            get {
                return this._status;
            }
            set {
                this._status = value;
                this.OnPropertyChanged();
            }
        }
    }
}
