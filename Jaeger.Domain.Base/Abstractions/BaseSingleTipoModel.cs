﻿namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// clase base model generica
    /// </summary>
    public abstract class BaseSingleTipoModel : BasePropertyChangeImplementation {
        /// <summary>
        /// constructor
        /// </summary>
        public BaseSingleTipoModel() {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">indice del elemento</param>
        /// <param name="descripcion">descripcion del elemento</param>
        public BaseSingleTipoModel(string id, string descripcion) {
            this.Id = id;
            this.Descripcion = descripcion;
        }

        /// <summary>
        /// obtener o establecer el indice del elemento
        /// </summary>
        public string Id {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la descripcion del elemento
        /// </summary>
        public string Descripcion {
            get; set;
        }

        public string Descriptor {
            get { return string.Format("{0}: {1}", this.Id, this.Descripcion); }
        }
    }
}