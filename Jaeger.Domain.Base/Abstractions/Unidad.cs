﻿namespace Jaeger.Domain.Base.Abstractions {
    /// <summary>
    /// clase abstracta para unidad
    /// </summary>
    public abstract class Unidad : BasePropertyChangeImplementation, Contracts.IUnidad {
        #region declaraciones
        private int _IdUnidad;
        private bool _Activo;
        private string _Descripcion;
        private decimal _Factor;
        #endregion

        #region propiedades
        /// <summary>
        /// obtener o establecer indice de la unidad
        /// </summary>
        public int IdUnidad {
            get {
                return this._IdUnidad;
            }
            set {
                this._IdUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo {
            get {
                return this._Activo;
            }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la unidad
        /// </summary>
        public string Descripcion {
            get {
                return this._Descripcion;
            }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor de conversion de unidad
        /// </summary>
        public decimal Factor {
            get { return _Factor; }
            set {
                _Factor = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
        
        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdUnidad.ToString("00"), this.Descripcion); }
        }
    }
}
