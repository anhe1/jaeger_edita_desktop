﻿using System.Text.RegularExpressions;

namespace Jaeger.Domain.Base.Abstractions {
    public abstract class BasePropertyChangeImplementationDataError : BasePropertyChangeImplementation {
        public bool RegexValido(string valor, string patron) {
            if (!(valor == null)) {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
    }
}
