﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Builder {
    public interface IOrderBy {
        string FieldName { get; set; }
        OrderByTypeEnum Type { get; set; }
        IOrderBy AddFieldName(string fieldName);
        IOrderBy AddOrderBy(OrderByTypeEnum type = OrderByTypeEnum.Asc);
    }
}
