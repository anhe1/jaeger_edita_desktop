﻿using System.Collections.Generic;

namespace Jaeger.Domain.Base.Builder {
    public interface IConditionalBuilder {
        List<IConditional> Conditionals { get; set; }
        List<IOrderBy> OrderBy { get; set; }
        List<IConditional> Build();

    }

    public interface IConditionalBuild {
    }
}
