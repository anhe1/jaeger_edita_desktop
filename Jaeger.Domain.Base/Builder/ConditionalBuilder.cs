﻿using System.Collections.Generic;

namespace Jaeger.Domain.Base.Builder {
    public class ConditionalBuilder : IConditionalBuilder, IConditionalBuild {
        protected internal List<IConditional> _Conditionals;
        protected internal List<IOrderBy> _OrderBy;

        public ConditionalBuilder() {
            this._Conditionals = new List<IConditional>();
            this._OrderBy = new List<IOrderBy>();
        }

        public List<IConditional> Conditionals {
            get { return this._Conditionals; }
            set {
                this._Conditionals = value;
            }
        }

        public List<IOrderBy> OrderBy {
            get { return this._OrderBy; }
            set { this._OrderBy = value; }
        }

        public List<IConditional> Build() {
            return this._Conditionals;
        }
    }
}
