﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Contracts {
    public interface IOrderByBuilder {
        IOrderByBuild AddFieldName(string fieldName);
    }

    public interface IOrderByTypeBuilder {
        IOrderByBuild AddOrderBy(OrderByTypeEnum type = OrderByTypeEnum.Asc);
    }

    public interface IOrderByBuild {
        IOrderBy Build();
    }
}
