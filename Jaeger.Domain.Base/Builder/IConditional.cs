﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Builder {
    public interface IConditional {
        string FieldName { get; set; }

        string FieldValue { get; set; }

        ConditionalTypeEnum ConditionalType { get; set; }
    }
}
