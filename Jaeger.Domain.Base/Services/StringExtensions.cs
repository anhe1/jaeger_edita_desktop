﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Jaeger.Domain.Base.Services {
    public static class StringExtensions {
        public static string CleanInputV(string cadena) {
            string _response = string.Empty;
            try {
                if (cadena != null) {
                    _response = cadena.Replace("'", "").Replace("{", "").Replace("}", "").Replace(";", "").Replace(",", " ").Replace("\r\n", "").Replace(Environment.NewLine, "").Replace("&amp;", "&").Replace("|", "");
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return _response;
        }

        public static string CleanInput(this string cadena) {
            string _response = string.Empty;
            try {
                if (cadena != null) {
                    _response = CleanInputV(cadena);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return _response;
        }

        public static string QuitarAcentos(string inputString) {
            inputString = inputString.ToLower();
            string str = string.Concat<char>(
                from ch in inputString.Normalize(NormalizationForm.FormD)
                where CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark
                select ch).Normalize(NormalizationForm.FormC);
            return str;
        }

        public static string CleanRFC(this string cadena) {
            string _response = string.Empty;
            try {
                if (cadena != null) {
                    _response = cadena.Replace("'", "").Replace("{", "").Replace("}", "").Replace(";", "").Replace(",", " ").Replace("\r\n", "").Replace(Environment.NewLine, "").Replace("&amp;", "&").Replace("|", "").Replace("-", "").Replace(" ", "");
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return _response;
        }

        public static string CreateMD5(string input) {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create()) {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++) {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }
    }
}
