﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Xml.Serialization;

namespace Jaeger.Domain.Base.Services {
    public class SerializerService {
        public static T DeserializeObject<T>(string xml) {
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                MemoryStream memoryStream = new MemoryStream(StringToUtf8ByteArray(xml));
                return (T)xmlSerializer.Deserialize(memoryStream);
            } catch (Exception ex) {
                Console.WriteLine("DeserializeObject: " + ex.Message);
                return default;
            }

        }

        public static string SerializeObject<T>(T obj) {
            var utf8WithoutBom = new UTF8Encoding(false);
            string empty = null;
            try {
                string str = null;
                var memoryStream = new MemoryStream();
                var xmlSerializer = new XmlSerializer(typeof(T));
                var xmlTextWriter = new XmlTextWriter(memoryStream, utf8WithoutBom);
                xmlTextWriter.Formatting = Formatting.Indented;
                xmlTextWriter.Indentation = 4;
                xmlSerializer.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                str = utf8WithoutBom.GetString(memoryStream.ToArray());
                empty = str;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                empty = string.Empty;
            }
            return empty;
        }

        public static byte[] StringToUtf8ByteArray(string pXmlString) {
            return new UTF8Encoding(false).GetBytes(pXmlString);
        }
    }
}
