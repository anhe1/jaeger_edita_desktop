﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Domain.Base.Services {
    public class HelperCsTripleDes {
        public HelperCsTripleDes() {
        }

        public static string Decrypt(string toDecrypt, string key, bool useHashing) {
            byte[] bytes;
            string decrypt;
            string str;
            try {
                byte[] numArray = Convert.FromBase64String(toDecrypt);
                if (useHashing) {
                    MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                    bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
                } else {
                    bytes = Encoding.UTF8.GetBytes(key);
                }
                TripleDESCryptoServiceProvider tripleDesCryptoServiceProvider = new TripleDESCryptoServiceProvider() {
                    Key = bytes,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                ICryptoTransform cryptoTransform = tripleDesCryptoServiceProvider.CreateDecryptor();
                byte[] numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, numArray.Length);
                str = Encoding.UTF8.GetString(numArray1);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                decrypt = "";
                return decrypt;
            }
            decrypt = str;
            return decrypt;
        }

        public static string Encrypt(string toEncrypt, string key, bool useHashing) {
            byte[] bytes;
            byte[] numArray = Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing) {
                MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
            } else {
                bytes = Encoding.UTF8.GetBytes(key);
            }
            TripleDESCryptoServiceProvider tripleDesCryptoServiceProvider = new TripleDESCryptoServiceProvider() {
                Key = bytes,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cryptoTransform = tripleDesCryptoServiceProvider.CreateEncryptor();
            byte[] numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, numArray.Length);
            return Convert.ToBase64String(numArray1, 0, numArray1.Length);
        }
    }
}