﻿using System;

namespace Jaeger.Domain.Base.Services {
    /// <summary>
    /// clase estatica para control y manipulacion de fechas
    /// </summary>
    public static class DateTimeExtension {
        /// <summary>
        /// primer dia de la semana
        /// </summary>
        /// <param name="dateTime">fecha</param>
        public static DateTime FirstDayWeek(this DateTime dateTime) {
            var dt = dateTime.AddDays(-6);
            var wkStDt = dt.AddDays(1 - Convert.ToDouble(dt.DayOfWeek));
            var fechadesdesemana = wkStDt.Date;
            return fechadesdesemana;
        }

        /// <summary>
        /// obtener el primer dia de la semana de la fecha actual
        /// </summary>
        public static DateTime FirstDayWeek() {
            var dt = DateTime.Now.AddDays(-6);
            var wkStDt = dt.AddDays(1 - Convert.ToDouble(dt.DayOfWeek));
            var firstDayWeek = wkStDt.Date;
            return firstDayWeek;
        }

        /// <summary>
        /// obtener ultimo dia del mes, en caso de corresponder a la fecha actual entonces debolvemos el ultimo dia del mes corriente
        /// </summary>
        /// <param name="dateTime">fecha inicial</param>
        /// <returns>DateTime</returns>
        public static DateTime LastDayMonth(this DateTime dateTime) { 
            var firstDate = new DateTime(dateTime.Year, dateTime.Month, 1);
            if (firstDate.Year == DateTime.Now.Year && firstDate.Month == DateTime.Now.Month) {
                return DateTime.Now;
            }
            var lastDayMonth = firstDate.AddMonths(1).AddDays(-1);
            return lastDayMonth;
        }
    }
}
