﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Base.Services {
    public static class RelacionComercialService {
        public static ItemSelectedModel GetRelacion(TipoRelacionComericalEnum relacionComercia) {
            var _relacionEmpresa = ((TipoRelacionComericalEnum[])Enum.GetValues(typeof(TipoRelacionComericalEnum))).Where(it => it == relacionComercia)
                .Select(c => new ItemSelectedModel((int)c, true, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description)).ToList();
            return _relacionEmpresa.FirstOrDefault();
        }
    }
}
