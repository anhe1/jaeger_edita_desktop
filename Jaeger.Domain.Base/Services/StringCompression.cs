﻿using System;
using System.IO.Compression;
using System.IO;
using System.Text;

namespace Jaeger.Domain.Base.Services {
    public static class StringCompression {
        /// <summary>
        /// Compresses a string and returns a deflate compressed, Base64 encoded string.
        /// </summary>
        /// <param name="uncompressedString">String to compress</param>
        public static string Compress(string uncompressedString) {
            byte[] compressedBytes;

            using (var uncompressedStream = new MemoryStream(Encoding.UTF8.GetBytes(uncompressedString))) {
                using (var compressedStream = new MemoryStream()) {
                    // establecer el parametro LeaveOpen en verdadero para garantizar que el flujo comprimido no se cerrara cuando se elimine el flujo compresor
                    // esto permite que compressorStream cierre y vacie sus buferes apressedStream y garantiza que se pueda llamar apressedStream.ToArray() despues
                    // aunque la documentacion de MSDN establece que se puede llamar a ToArray() en un MemoryStream cerrado, no quiero confiar en ese comportamiento tan extraño si alguna vez cambia
                    using (var compressorStream = new DeflateStream(compressedStream, CompressionLevel.Fastest, true)) {
                        uncompressedStream.CopyTo(compressorStream);
                    }

                    // llamar apressedStream.ToArray() después de que el DeflateStream adjunto se haya cerrado y vaciado su búfer apressedStream
                    compressedBytes = compressedStream.ToArray();
                }
            }

            return Convert.ToBase64String(compressedBytes);
        }

        /// <summary>
        /// Decompresses a deflate compressed, Base64 encoded string and returns an uncompressed string.
        /// </summary>
        /// <param name="compressedString">String to decompress.</param>
        public static string Decompress(string compressedString) {
            byte[] decompressedBytes;

            var compressedStream = new MemoryStream(Convert.FromBase64String(compressedString));

            using (var decompressorStream = new DeflateStream(compressedStream, CompressionMode.Decompress)) {
                using (var decompressedStream = new MemoryStream()) {
                    decompressorStream.CopyTo(decompressedStream);

                    decompressedBytes = decompressedStream.ToArray();
                }
            }

            return Encoding.UTF8.GetString(decompressedBytes);
        }
    }
}
