﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Jaeger.Domain.Base.Services {
    public static class MapperClassExtensions {
        public static void MatchAndMap<TSource, TDestination>(this TSource source, TDestination destination) where TSource : class, new() where TDestination : class, new() {
            if (source != null && destination != null) {
                List<PropertyInfo> sourceProperties = source.GetType().GetProperties().ToList<PropertyInfo>();
                List<PropertyInfo> destinationProperties = destination.GetType().GetProperties().ToList<PropertyInfo>();

                foreach (PropertyInfo sourceProperty in sourceProperties) {
                    PropertyInfo destinationProperty = destinationProperties.Find(item => item.Name == sourceProperty.Name);

                    if (destinationProperty != null) {
                        try {
                            destinationProperty.SetValue(destination, sourceProperty.GetValue(source, null), null);
                        } catch (Exception ex) {
                            Console.WriteLine("[MapperClassExtensions] Error: " + ex.Message + ex.StackTrace);
                        }
                    }
                }
            }
        }

        public static TDestination MapProperties<TDestination>(this object source) where TDestination : class, new() {
            var destination = Activator.CreateInstance<TDestination>();
            MatchAndMap(source, destination);

            return destination;
        }

        private static IEnumerable<T> GetData<T>(string contenido, string columnNames, char separator = '|') {
            var _rows = contenido.Split(new char[] { '\r' });
            var _columnNames = columnNames.Split(separator);
            var _properties = typeof(T).GetProperties();

            var _response = new List<T>();

            _rows.ToList().ForEach(r => {
                var cells = r.Split('|');
                if (cells.Count() > 3) {
                    T obj = (T)Activator.CreateInstance(typeof(T));
                    int index = 0;
                    foreach (var columnaName in _columnNames) {
                        var prop = _properties.SingleOrDefault(p => p.Name == columnaName);
                        if (prop != null) {
                            Type propertyType = prop.PropertyType;
                            var value = cells[index++];
                            prop.SetValue(obj, Convert.ChangeType(value, propertyType));
                        }
                    }
                    _response.Add(obj);
                }
            });

            return _response;
        }

        public static void CopyPropertiesFrom(this object self, object parent) {
            var fromProperties = parent.GetType().GetProperties();
            var toProperties = self.GetType().GetProperties();

            foreach (var fromProperty in fromProperties) {
                foreach (var toProperty in toProperties) {
                    if (fromProperty.Name == toProperty.Name && fromProperty.PropertyType == toProperty.PropertyType) {
                        toProperty.SetValue(self, fromProperty.GetValue(parent));
                        break;
                    }
                }
            }
        }

        public static void MatchPropertiesFrom(this object self, object parent) {
            var childProperties = self.GetType().GetProperties();
            foreach (var childProperty in childProperties) {
                var attributesForProperty = childProperty.GetCustomAttributes(typeof(MatchParentAttribute), true);
                var isOfTypeMatchParentAttribute = false;

                MatchParentAttribute currentAttribute = null;

                foreach (var attribute in attributesForProperty) {
                    if (attribute.GetType() == typeof(MatchParentAttribute)) {
                        isOfTypeMatchParentAttribute = true;
                        currentAttribute = (MatchParentAttribute)attribute;
                        break;
                    }
                }

                if (isOfTypeMatchParentAttribute) {
                    var parentProperties = parent.GetType().GetProperties();
                    object parentPropertyValue = null;
                    foreach (var parentProperty in parentProperties) {
                        if (parentProperty.Name == currentAttribute.ParentPropertyName) {
                            if (parentProperty.PropertyType == childProperty.PropertyType) {
                                parentPropertyValue = parentProperty.GetValue(parent);
                            }
                        }
                    }
                    childProperty.SetValue(self, parentPropertyValue);
                }
            }
        }
    }

    /// <summary>
    /// clase para mapper para propiedades de clase
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MatchParentAttribute : Attribute {
        public readonly string ParentPropertyName;
        public MatchParentAttribute(string parentPropertyName) {
            ParentPropertyName = parentPropertyName;
        }
    }

    public class PropertyMatcher<TParent, TChild> where TParent : class
                                                  where TChild : class {
        public static void GenerateMatchedObject(TParent parent, TChild child) {
            var childProperties = child.GetType().GetProperties();
            foreach (var childProperty in childProperties) {
                var attributesForProperty = childProperty.GetCustomAttributes(typeof(MatchParentAttribute), true);
                var isOfTypeMatchParentAttribute = false;

                MatchParentAttribute currentAttribute = null;

                foreach (var attribute in attributesForProperty) {
                    if (attribute.GetType() == typeof(MatchParentAttribute)) {
                        isOfTypeMatchParentAttribute = true;
                        currentAttribute = (MatchParentAttribute)attribute;
                        break;
                    }
                }

                if (isOfTypeMatchParentAttribute) {
                    var parentProperties = parent.GetType().GetProperties();
                    object parentPropertyValue = null;
                    foreach (var parentProperty in parentProperties) {
                        if (parentProperty.Name == currentAttribute.ParentPropertyName) {
                            if (parentProperty.PropertyType == childProperty.PropertyType) {
                                parentPropertyValue = parentProperty.GetValue(parent);
                            }
                        }
                    }
                    childProperty.SetValue(child, parentPropertyValue);
                }
            }
        }
    }
}
