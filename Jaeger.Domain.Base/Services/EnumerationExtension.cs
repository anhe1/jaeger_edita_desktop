﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Jaeger.Domain.Base.Services {
    public static class EnumerationExtension {
        public static string Description(this Enum value) {
            // get attributes  
            var field = value.GetType().GetField(value.ToString());
            var attributes = field.GetCustomAttributes(false);

            // Description is in a hidden Attribute class called DisplayAttribute
            // Not to be confused with DisplayNameAttribute
            dynamic displayAttribute = null;

            if (attributes.Any()) {
                displayAttribute = attributes.ElementAt(0);
            }

            // return description
            if (displayAttribute != null) {
                return displayAttribute.Description;
            }
            return "Description Not Found";
        }

        public static T ParseEnum<T>(string value) {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Convertir enumeracion a lista de objeto 
        /// </summary>
        /// <typeparam name="T">clase objeto</typeparam>
        /// <typeparam name="TEnum">enumeracion</typeparam>
        public static IEnumerable<T> GetEnumToClass<T, TEnum>() where T : class, new() {
            var _dataResult = ((TEnum[])Enum.GetValues(typeof(TEnum)))
            .Select(c => Activator.CreateInstance(typeof(T), new object[] { c, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description }));

            var _response = new List<T>();
            foreach (var item in _dataResult) {
                _response.Add((T)item);
            }
            return _response;
        }

        public static string GetEnumDescription(Enum value) {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any()) {
                return attributes.First().Description;
            }

            return value.ToString();
        }

        public static Dictionary<string, T> EnumToDictionary<T>() {
            Dictionary<string, T> dic = new Dictionary<string, T>();
            if (!typeof(T).IsEnum) {
                return dic;
            }
            string desc = string.Empty;
            foreach (var item in Enum.GetValues(typeof(T))) {
                var key = item.ToString().ToLower();
                if (!dic.ContainsKey(key))
                    dic.Add(key, (T)item);
            }
            return dic;
        }
    }
}
