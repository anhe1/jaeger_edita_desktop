﻿using System;

namespace Jaeger.Domain.Base.Services {
    public class NumeroALetras {
        public static string Convertir(double importe, int tipoMoneda) {
            string resultado;
            string[] strArrays;
            try {
                string[] arrayNumeros = new string[] { null, "", "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE", "VEINTE", "VEINTIUN", "VEINTIDOS", "VEINTITRES", "VEINTICUATRO", "VEINTICINCO", "VEINTISEIS", "VEINTISIETE", "VEINTIOCHO", "VEINTINUEVE" };
                string[] arrayDecenas = new string[] { null, " ", " ", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA" };
                string[] arrayCentenas = new string[] { null, "CIEN", "DOSCIENTOS", "TRESCIENTOS", "CUATROCIENTOS", "QUINIENTOS", "SEISCIENTOS", "SETECIENTOS", "OCHOCIENTOS", "NOVECIENTOS" };
                string[] arrayMiles = new string[] { null, " MIL ", " MILLON", " MIL ", "" };
                string str1 = null;
                string str2 = null;
                double num = 0;
                double num1 = 0;
                int i = 0;
                int num2 = 0;
                int num3 = 0;
                int num4 = 0;
                int num5 = 0;
                double[] numArray = new double[5];
                str1 = string.Format("{0:0.00}", importe);
                str1 = str1.Substring(str1.IndexOf(".") + 1, 2);
                num = double.Parse(str1);
                str2 = "";
                num1 = Convert.ToDouble(importe);
                if (num1 == 0) {
                    str2 = "Cero";
                }
                if (importe == 0) {
                    str2 = "CERO";
                } else {
                    for (i = 4; i > 0; i--) {
                        numArray[i] = num1 - (int)(num1 / 1000) * 1000;
                        num1 = (num1 - numArray[i]) / 1000;
                    }
                    for (i = 1; i <= 4; i++) {
                        if (numArray[i] > 0) {
                            num2 = (int)(numArray[i] / 100);
                            num3 = (int)numArray[i] - num2 * 100;
                            num4 = num3 - num3 / 10 * 10;
                            num5 = num3 / 10;
                            if (num2 > 0) {
                                str2 = string.Concat(str2, arrayCentenas[num2]);
                                if (num2 == 1 & num3 > 0) {
                                    str2 = string.Concat(str2, "TO");
                                }
                                str2 = string.Concat(str2, " ");
                            }
                            if (num3 >= 30) {
                                str2 = string.Concat(str2, arrayDecenas[num5]);
                                if (num4 > 0) {
                                    str2 = string.Concat(str2, " Y ", arrayNumeros[num4 + 1]);
                                }
                            } else {
                                str2 = string.Concat(str2, arrayNumeros[num3 + 1]);
                            }
                            if (i == 4 & num4 == 1 & num5 != 1) {
                                str2 = string.Concat(str2, "O");
                            }
                            str2 = string.Concat(str2, arrayMiles[i]);
                            if (i == 2) {
                                str2 = numArray[i] != 1 ? string.Concat(str2, "ES ") : string.Concat(str2, " ");
                            }
                        }
                    }
                }
                string str3 = null;
                str3 = string.Format("{0:00}", num);
                switch (tipoMoneda) {
                    case 1: {
                            strArrays = new string[] { "(", str2, " PESOS ", str3, "/100 M.N.)" };
                            resultado = string.Concat(strArrays);
                            return resultado;
                        }
                    case 2: {
                            strArrays = new string[] { "(", str2, " DOLARES AMERICANOS ", str3, "/100 USD.)" };
                            resultado = string.Concat(strArrays);
                            return resultado;
                        }
                    case 3: {
                            strArrays = new string[] { "(", str2, " EUROS ", str3, "/100 EUROS.)" };
                            resultado = string.Concat(strArrays);
                            return resultado;
                        }
                }
                resultado = str2;
            } catch {
                Console.WriteLine(string.Concat("HelperNumeroALetras: ", "El importe total tiene una cantidad demasiado largo"));
                resultado = "";
            }
            return resultado;
        }
    }
}