﻿namespace Jaeger.Domain.Base.ValueObjects {
    public enum OrderByTypeEnum {
        Asc = 0,
        Desc = 1
    }
}
