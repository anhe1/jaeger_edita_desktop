﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// enumeracion para los estados SAT del comprobante fiscal (CFDI)
    /// </summary>
    public enum CFDIEstadoEnum {
        [Description("Cancelado")]
        Cancelado,
        [Description("Vigente")]
        Vigente,
        [Description("No Encontrado")]
        NoEncontado
    }
}