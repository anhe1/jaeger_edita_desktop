﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// NA = 0, MP = 1, PT = 2
    /// </summary>
    public enum AlmacenEnum {
        [Description("")]
        NA = 0,
        [Description("Almacén Materia Prima")]
        MP = 1,
        [Description("Almacén Producto Terminado")]
        PT = 2,
        [Description("Almacén Tienda Web")]
        TW = 3,
        [Description("Almacén Departamento")]
        DP = 4
    }
}