﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Base.ValueObjects {
    public class UIMenuItemPermission {
        private readonly IDictionary<string, UIMenuBaseItem> mapper;
        private UIMenuBaseItem info;

        public UIMenuItemPermission(UIPermissionEnum outerDenierAction) {
            this.DeniedAction = outerDenierAction;
            this.mapper = new Dictionary<string, UIMenuBaseItem>();
        }

        public UIPermissionEnum DeniedAction {
            get; set;
        }

        public virtual bool HasPermission(object accessingObject, UsuarioBase user) {
            if (accessingObject == null || user == null) {
                return false;
            }
            // get key
            string key = (string)accessingObject;
            // get valid role for accessing object
            try {
                this.info = mapper[key];
                var validRoles = info.ListRol;
                // check whether user is valid for accessing object
                for (int i = 0; i < validRoles.Count; i++) {
                    Rol validRole = validRoles[i];
                    if (user.IsInRole(validRole)) {
                        return true;
                    }
                }
            } catch (System.Exception ex) {
                this.info = null;
                System.Console.WriteLine(ex.Message);
            }
            return false;
        }

        public UIMenuBaseItem CurrentItem {
            get {
                return this.info;
            }
        }

        public virtual void Load(List<UIMenuElement> tabs) {
            for (int i = 0; i < tabs.Count; i++) {
                var _item = new UIMenuBaseItem() {
                    Id = tabs[i].Id,
                    Form = tabs[i].Form,
                    Default = tabs[i].Default,
                    Label = tabs[i].Label,
                    Name = tabs[i].Name,
                    ParentId = tabs[i].ParentId,
                    Permisos = tabs[i].Permisos,
                    Rol = tabs[i].Rol,
                    ToolTipText = tabs[i].ToolTipText,
                    Assembly = tabs[i].Assembly,
                    IsAvailable = tabs[i].IsAvailable,
                };
                System.Console.WriteLine(_item.Name);
                this.mapper.Add(tabs[i].Name, _item);
                //this.mapper.Add(tabs[i].Name, new UIMenuBaseItem(tabs[i].Name, tabs[i].Label, tabs[i].ToolTipText, tabs[i].Rol));
            }
        }
    }
}
