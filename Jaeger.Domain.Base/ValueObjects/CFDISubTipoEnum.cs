﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// representa el subtipo del comprobante fiscal (Emitido, Recibido, Nomina)
    /// </summary>
    public enum CFDISubTipoEnum {
        [Description("No definido")]
        None,
        [Description("Emitido")]
        Emitido,
        [Description("Recibido")]
        Recibido,
        [Description("Nómina")]
        Nomina
    }
}
