﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum TipoRegimenEnum {
        [Description("No Definido")]
        NoDefinido,
        [Description("Persona Fisica")]
        Fisica,
        [Description("Persona Moral")]
        Moral
    }
}