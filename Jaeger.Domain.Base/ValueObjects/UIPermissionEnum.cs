﻿namespace Jaeger.Domain.Base.ValueObjects {
    public enum UIPermissionEnum {
        Normal = 0,
        Disabled = 1,
        Invisible = 2,
        Popup = 3
    }
}
