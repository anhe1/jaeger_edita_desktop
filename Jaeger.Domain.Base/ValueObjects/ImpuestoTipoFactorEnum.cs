﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// Factores de impuesto aplicables TASA/CUOTA/Exento
    /// </summary>
    public enum ImpuestoTipoFactorEnum {
        [Description("No Aplica")]
        NoAplica = 3,
        [Description("Tasa")]
        Tasa = 0,
        [Description("Cuota")]
        Cuota = 1,
        [Description("Exento")]
        Exento = 2
    }
}
