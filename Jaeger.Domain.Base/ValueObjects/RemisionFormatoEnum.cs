﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum RemisionFormatoEnum {
        [Description("Con Pagaré")]
        ConPagare = 1,
        [Description("Sin Pagaré")]
        SinPagare = 2
    }
}