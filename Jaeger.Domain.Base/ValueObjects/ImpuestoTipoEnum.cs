﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// Tipo  de impuestos Traslado / Retención
    /// </summary>
    public enum ImpuestoTipoEnum {
        [Description("Traslado")]
        Traslado = 0,
        [Description("Retención")]
        Retencion = 1
    }
}
