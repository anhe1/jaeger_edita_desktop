﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// No Definido = 0, Producto = 1, Servicio = 2
    /// </summary>
    public enum ProdServTipoEnum {
        /// <summary>
        /// No Definido = 0
        /// </summary>
        [Description("No Definido")]
        NoDefinido,
        /// <summary>
        /// Producto = 1
        /// </summary>
        [Description("Producto")]
        Producto,
        /// <summary>
        /// Servicio = 2
        /// </summary>
        [Description("Servicio")]
        Servicio
    }
}