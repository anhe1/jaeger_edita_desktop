﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum TipoAsentamientoEnum {
        [Description("No definido")]
        NoDefinido = 1,
        [Description("Barrio")]
        Barrio = 2,
        [Description("Colonia")]
        Colonia = 3,
        [Description("Condominio")]
        Condominio = 4,
        [Description("Congregación")]
        Congregación = 5,
        [Description("Ejido")]
        Ejido = 6,
        [Description("Equipamiento")]
        Equipamiento = 7,
        [Description("Estación")]
        Estacion = 8,
        [Description("Fraccionamiento")]
        Fraccionamiento = 9,
        [Description("Granja")]
        Granja = 10,
        [Description("Hacienda")]
        Hacienda = 11,
        [Description("Poblado comunal")]
        Poblado_comunal = 12,
        [Description("Pueblo")]
        Pueblo = 13,
        [Description("Rancho o rancheria")]
        Rancho_rancheria = 14,
        [Description("Unidad habitacional")]
        Unidad_habitacional = 15,
        [Description("Zona comercial")]
        Zona_comercial = 16,
        [Description("Zona federal")]
        Zona_Federal = 17,
        [Description("Zona industrial")]
        Zona_Industrial = 18,
    }
}
