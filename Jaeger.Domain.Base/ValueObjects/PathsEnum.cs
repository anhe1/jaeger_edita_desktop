﻿namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// enumeracion de las carpetas del sistema
    /// </summary>
    public enum PathsEnum {
        Accuse,
        Catalogos,
        Comprobantes,
        Downloads,
        Google,
        Log,
        Media,
        Reportes,
        Repositorio,
        Resources,
        SAT,
        Templates,
        Temporal
    }
}
