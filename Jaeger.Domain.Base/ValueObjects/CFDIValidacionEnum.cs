﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum CFDIValidacionEnum {
        [Description("Error")]
        Error = 0,
        [Description("En espera")]
        EnEspera = 1,
        [Description("Válido")]
        Valido = 2,
        [Description("No Valido")]
        NoValido = 3
    }
}
