﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// Enumeracion para status de los comprobantes Recibidos (Cancelado, Importado, PorPagar, Rechazado, Pagado)
    /// </summary>
    public enum CFDIStatusRecibidoEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Importado")]
        Importado = 2,
        [Description("Por Pagar")]
        PorPagar = 4,
        [Description("Pagado")]
        Pagado = 5,
        [Description("Rechazado")]
        Rechazado = 6
    }
}