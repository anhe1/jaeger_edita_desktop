﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum GeneroEnum {
        [Description("No definido")]
        Ninguno = 0,
        [Description("Masculino")]
        Masculino = 1,
        [Description("Femenino")]
        Femenino = 2
    }
}