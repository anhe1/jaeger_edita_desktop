﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum CFDIFechaEnum {
        [Description("Todos")]
        None,
        [Description("Fecha Emisión")]
        FechaEmision,
        [Description("Fecha Timbre")]
        FechaTimbre,
        [Description("Fecha Pago")]
        FechaDePago,
        [Description("Fecha Validación")]
        FechaValidacion
    }
}
