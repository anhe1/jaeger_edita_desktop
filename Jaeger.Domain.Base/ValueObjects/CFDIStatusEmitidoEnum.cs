﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// Enumeracion para status de los comprobantes Emitidos
    /// </summary>
    public enum CFDIStatusEmitidoEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Pendiente")]
        Pendiente = 1,
        [Description("Importado")]
        Importado = 2,
        [Description("Entregado")]
        Entregado = 3,
        [Description("Por Cobrar")]
        PorCobrar = 4,
        [Description("Cobrado")]
        Cobrado = 5,
        [Description("Rechazado")]
        Rechazado = 6
    }
}