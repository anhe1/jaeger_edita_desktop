﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Base.ValueObjects {
    public class RelacionComercialData {
        public RelacionComercialData() {
            this.Items = EnumerationExtension.GetEnumToClass<RelacionComercial, TipoRelacionComericalEnum>().ToList();
        }

        public List<RelacionComercial> Items {
            get;set;
        }

        /// <summary>
        /// obtener lista de relaciones en cadena separada por comas
        /// </summary>
        public string GetRelacion() {
            return string.Join(",", this.Items.Where(it => it.Selected == true).Select(it => it.Name).ToArray());
        }

        public void SetRelacion(string relacion) {
            if (relacion != null) {
                string[] arrayRelacion = relacion.Split(new char[] { ',' });
                this.Items.Where(it => arrayRelacion.Contains(it.Name)).Select(it => { it.Selected = true; return it; }).ToList();
            }
        }
    }
}