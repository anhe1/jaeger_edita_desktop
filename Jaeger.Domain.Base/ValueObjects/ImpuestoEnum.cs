﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// tipos de impuestos aplicables ISR / IVA / IEPS
    /// </summary>
    public enum ImpuestoEnum {
        [Description("ISR")]
        ISR = 1,
        [Description("IVA")]
        IVA = 2,
        [Description("IEPS")]
        IEPS = 3
    }
}
