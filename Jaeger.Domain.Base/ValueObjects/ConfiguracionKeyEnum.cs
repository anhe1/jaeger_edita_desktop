﻿namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// objetos de configuracion
    /// </summary>
    public enum ConfiguracionKeyEnum {
        cfdi,
        ocompra11,
        nom12,
        valida6,
        valida7,
        empresa,
        amazon_s3,
        amazon_ses,
        remision2,
        menu
    }
}