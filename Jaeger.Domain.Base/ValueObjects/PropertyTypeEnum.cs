﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum PropertyTypeEnum {
        [Description("No definido")]
        None,
        [Description("Error")]
        Error,
        [Description("Advertencia")]
        Warning,
        [Description("Atención")]
        Attention,
        [Description("Información")]
        Information,
        [Description("Completado")]
        Completed
    }
}
