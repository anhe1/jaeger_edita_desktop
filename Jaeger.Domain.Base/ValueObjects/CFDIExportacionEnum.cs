﻿using System.ComponentModel;
namespace Jaeger.Domain.Base.ValueObjects {
    public enum CFDIExportacionEnum {
        [Description("01 - No aplica")]
        No_Aplica = 1,
        [Description("02 - Definitiva")]
        Definitiva = 2,
        [Description("03 - Temporal")]
        Temporal = 3,
        [Description("Definitiva con clave distinta a A1 o cuando no existe enajenación en términos del CFF")]
        DefinitivaA1 = 4
    }
}
