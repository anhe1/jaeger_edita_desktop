﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum UIActionEnum {
        [Description("Cancelar")]
        Cancelar = 0,
        [Description("Agregar")]
        Agregar = 1,
        [Description("Remover")]
        Remover = 2,
        [Description("Editar")]
        Editar = 3,
        [Description("Autorizar")]
        Autorizar = 4,
        [Description("Imprimir")]
        Imprimir = 5,
        [Description("Exportar")]
        Exportar = 6,
        [Description("Importar")]
        Importar = 7,
        [Description("Status")]
        Status = 8,
        [Description("Reportes")]
        Reporte = 9,
    }
}
