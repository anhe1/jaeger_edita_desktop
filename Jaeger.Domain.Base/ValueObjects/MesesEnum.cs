﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// meses del año
    /// </summary>
    public enum MesesEnum {
        [Description("Todos")]
        Todos = 0,
        [Description("Enero")]
        Enero = 1,
        [Description("Febrero")]
        Febrero = 2,
        [Description("Marzo")]
        Marzo = 3,
        [Description("Abril")]
        Abril = 4,
        [Description("Mayo")]
        Mayo = 5,
        [Description("Junio")]
        Junio = 6,
        [Description("Julio")]
        Julio = 7,
        [Description("Agosto")]
        Agosto = 8,
        [Description("Septiembre")]
        Septiembre = 9,
        [Description("Octubre")]
        Octubre = 10,
        [Description("Noviembre")]
        Noviembre = 11,
        [Description("Diciembre")]
        Diciembre = 12
    }
}
