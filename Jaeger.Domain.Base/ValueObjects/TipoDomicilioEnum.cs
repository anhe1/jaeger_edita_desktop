﻿using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    public enum TipoDomicilioEnum {
        [Description("No Definido")]
        No_Definido,
        [Description("Fiscal")]
        Fiscal,
        [Description("Particular")]
        Particular,
        [Description("Entrega")]
        Entrega,
        [Description("Envío")]
        Envio,
        [Description("Otro")]
        Otro
    }
}