﻿using System;
using System.ComponentModel;

namespace Jaeger.Domain.Base.ValueObjects {
    /// <summary>
    /// relaciones comerciales con la empresa, en el caso de bancos tipos de beneficiarios
    /// </summary>
    public enum TipoRelacionComericalEnum {
        [Description("No definido")]
        None = 0,
        [Description("Cliente")]
        Cliente = 1,
        [Description("Proveedor")]
        Proveedor = 2,
        [Description("Empleado")]
        Empleado = 3,
        [Description("Vendedor")]
        Vendedor = 4,
        [Description("Usuario"), Obsolete("No disponible")]
        Usuario = 5,
        [Description("Solicitante")]
        Solicitante = 6,
        [Description("Minorista")]
        Minorista = 7,
        [Description("Mayorista")]
        Mayorista = 8
    }
}
