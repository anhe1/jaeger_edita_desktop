﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSolicitudCotizacionConceptoRepository : MySqlSugarContext<SolicitudCotizacionConceptoModel>, ISqlSolicitudCotizacionConceptoRepository {
        public SqlSolicitudCotizacionConceptoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<T1>().Where(_conditionalList).ToList();
        }

        public SolicitudCotizacionConceptoModel Save(SolicitudCotizacionConceptoModel model) {
            if (model.IdConcepto == 0) {
                model.FechaNuevo = System.DateTime.Now;
                model.Creo = this.User;
                model.IdConcepto = this.Insert(model);
            } else {
                model.FechaModifica = System.DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }
    }
}
