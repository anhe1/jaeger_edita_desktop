﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.Domain.Adquisiciones.ValueObjects;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarOrdenCompraRepository : MySqlSugarContext<OrdenCompraModel>, ISqlOrdenCompraRepository {
        /// <summary>
        /// constructor
        /// </summary>
        public SqlSugarOrdenCompraRepository(DataBaseConfiguracion configuracion) : base(configuracion) {

        }

        /// <summary>
        /// cancelar una orden de compra
        /// </summary>
        public bool Cancelar(OrdenCompraModel item, string usuario) {
            return this.Db.Updateable<OrdenCompraModel>().SetColumns(it => new OrdenCompraModel() { FechaCancela = DateTime.Now, IdStatus = (int)OrdenCompraStatusEnum.Cancelado, Cancela = usuario }).Where(it => it.Id == item.Id).ExecuteCommand() > 0;
        }

        /// <summary>
        /// crear tablas
        /// </summary>
        /// <returns></returns>
        public bool CrearTablas() {
            this.Db.CodeFirst.InitTables<OrdenCompraConceptoModel>();
            return base.CreateTable();
        }

        public OrdenCompraDetailModel GetByFolio(int folio) {
            //Manual mode
            var result = this.Db.Queryable<OrdenCompraDetailModel>().Mapper((itemModel, cache) => {
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.Id).ToList();
                    return this.Db.Queryable<OrdenCompraConceptoDetailModel>().Where(it => allIds.Contains(it.SubId)).ToList();//Execute only once
                });
                itemModel.Conceptos = new BindingList<OrdenCompraConceptoDetailModel>(allItems.Where(it => it.SubId == itemModel.Id).ToList());//Every time it's executed
            }).Where(it => it.Folio == folio).Single();
            return result;
        }

        /// <summary>
        /// orden de compra
        /// </summary>
        /// <param name="index">indice de la orden de compra</param>
        /// <returns>orden de compra</returns>
        public new OrdenCompraDetailModel GetById(int index) {
            return this.GetList(0, 0, index).FirstOrDefault();
        }

        /// <summary>
        /// listado de ordenes de compra por año y mes
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <returns></returns>
        public IEnumerable<OrdenCompraDetailModel> GetList(int month, int year, int index = 0) {
            var result = this.Db.Queryable<OrdenCompraDetailModel>().Mapper((itemModel, cache) => {
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.Id).ToList();
                    return this.Db.Queryable<OrdenCompraConceptoDetailModel>().Where(it => allIds.Contains(it.SubId)).ToList();//Execute only once
                });
                itemModel.Conceptos = new BindingList<OrdenCompraConceptoDetailModel>(allItems.Where(it => it.SubId == itemModel.Id).ToList());//Every time it's executed
            }).WhereIF(month > 0, it => it.FechaEmision.Month == month).WhereIF(year > 0, it => it.FechaEmision.Year == year).WhereIF(index > 0, it => it.Id == index).OrderBy(it => it.Id, OrderByType.Desc).ToList();
            return result;
        }

        /// <summary>
        /// listado de ordenes de compra por año y mes
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        public IEnumerable<OrdenCompraSingleModel> GetList(int month, int year) {
            return this.Db.Queryable<OrdenCompraSingleModel>().WhereIF(month > 0, it => it.FechaEmision.Month == month).WhereIF(year > 0, it => it.FechaEmision.Year == year).OrderBy(it => it.Id, OrderByType.Desc).ToList();
        }

        /// <summary>
        /// listado simple de ordenes de compra
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        public IEnumerable<OrdenCompraSingleModel> GetList(List<Conditional> conditionals) {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<OrdenCompraSingleModel>().Where(_conditionalList).OrderBy(it => it.Id, OrderByType.Desc).ToList();
        }

        /// <summary>
        /// almacenar una orden de compra
        /// </summary>
        public OrdenCompraDetailModel Save(OrdenCompraDetailModel item) {
            if (item.OrdenRelacionada != null) {
                if (item.OrdenRelacionada.Relacionado != null) {
                    if (item.OrdenRelacionada.Relacionado.Count == 0)
                        item.OrdenRelacionada = null;
                }
            }

            if (item.Id == 0) {
                item.Folio = this.Folio(item);
                item.Id = this.Insert(item);
            } else {
                this.Update(item);
            }

            for (int i = 0; i < item.Conceptos.Count; i++) {
                item.Conceptos[i].SubId = item.Id;
                item.Conceptos[i] = this.Save(item.Conceptos[i]);
            }
            return item;
        }

        /// <summary>
        /// almacenar orden de compra
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public OrdenCompraConceptoDetailModel Save(OrdenCompraConceptoDetailModel item) {
            if (item.Id == 0) {
                item.Id = this.Db.Insertable<OrdenCompraConceptoDetailModel>(item).ExecuteReturnIdentity();
            } else {
                this.Db.Updateable<OrdenCompraConceptoDetailModel>(item).ExecuteCommand();
            }
            return item;
        }

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo
        /// </summary>
        public int Folio(OrdenCompraModel objeto) {
            int folio;
            try {
                folio = this.Db.Queryable<OrdenCompraModel>()
                    .WhereIF(objeto.Serie != null, it => it.Serie == objeto.Serie)
                    .Select(it => new { maximus = SqlFunc.AggregateMax(it.Folio) }).Single().maximus + 1;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                folio = 0;
            }

            return folio;
        }
    }
}
