﻿using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarContribuyenteRepository : SqlSugarContext<OrdenCompraProveedorModel>, ISqlContribuyenteRepository {
        public SqlSugarContribuyenteRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        /// <param name="relacion">relacion con el directorio</param>
        public IEnumerable<OrdenCompraProveedorModel> GetList(string relacion) {
            return this.Db.Queryable<OrdenCompraProveedorModel>().Where(it => it.Relacion.Contains(relacion))
                .Where(it => it.Activo == true)
                .Select((dto) => new OrdenCompraProveedorModel {
                    Id = dto.Id,
                    ClaveUsoCFDI = dto.ClaveUsoCFDI,
                    CURP = dto.CURP,
                    Nombre = dto.Nombre,
                    RFC = dto.RFC
                }).OrderBy(it => it.Nombre, OrderByType.Asc).ToList();
        }

        public IEnumerable<OrdenCompraProveedorDetailModel> GetList(string relacion, bool onlyActive) {
            var response = this.Db.Queryable<OrdenCompraProveedorDetailModel>().Where(it => it.Relacion.Contains(relacion)).Where(it => it.Activo == true).Mapper((item, cache) => {
                var contactos = cache.Get(proveedor => {
                    var ids = proveedor.Select(it => it.Id).ToList();
                    return this.Db.Queryable<ContactoProveedorModel>().Where(it => ids.Contains(it.SubId)).Where(it => it.IsActive == true).ToList();
                }).ToList();
                if (contactos != null && contactos.Count > 0) {
                    item.Contactos = new System.ComponentModel.BindingList<ContactoProveedorModel>(contactos.Where(it => it.SubId == item.Id).ToList());
                } else {
                    item.Contactos = new System.ComponentModel.BindingList<ContactoProveedorModel>();
                }
            }).OrderBy(it => it.Nombre, OrderByType.Asc).ToList();
            return response;
        }
    }
}
