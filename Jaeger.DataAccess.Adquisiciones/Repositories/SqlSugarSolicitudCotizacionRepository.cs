﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarSolicitudCotizacionRepository : MySqlSugarContext<SolicitudCotizacionModel>, ISqlSolicitudCotizacionRepository {
        public SqlSugarSolicitudCotizacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sql = @"select _ordcms.* from _ordcms @wcondiciones";
            return this.GetMapper<T1>(sql, conditionals);
        }

        public SolicitudCotizacionDetailModel Save(SolicitudCotizacionDetailModel model) {
            if (model.IdSolicitud == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.Folio = this.Folio(model);
                model.IdSolicitud = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }

        public new SolicitudCotizacionDetailModel GetById(int index) {
            var response = this.Db.Queryable<SolicitudCotizacionDetailModel>().Mapper((model, cache) => {
                var items = cache.Get(order => {
                    var ids = order.Select(it => it.IdSolicitud).ToList();
                    return this.Db.Queryable<SolicitudCotizacionConceptoModel>().Where(it => ids.Contains(it.IdSolicitud)).ToList();
                });
                model.Conceptos = new System.ComponentModel.BindingList<SolicitudCotizacionConceptoModel>(items.Where(it => it.IdSolicitud == model.IdSolicitud).ToList());
            }).Where(it => it.IdSolicitud == index).Single();
            return response;
        }

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo
        /// </summary>
        public int Folio(SolicitudCotizacionModel model) {
            int folio;
            try {
                folio = this.Db.Queryable<SolicitudCotizacionModel>()
                    .WhereIF(model.Serie != null, it => it.Serie == model.Serie)
                    .Select(it => new { maximus = SqlFunc.AggregateMax(it.Folio) }).Single().maximus + 1;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                folio = 0;
            }

            return folio;
        }

        public void Crear() {
            this.CreateTable();
        }
    }
}
