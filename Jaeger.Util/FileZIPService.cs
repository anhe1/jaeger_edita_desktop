﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Jaeger.Util {
    /// <summary>
    /// Zip and Unzip in memory using System.IO.Compression.
    /// </summary>;
    /// <remarks>
    /// Include System.IO.Compression in your project.
    /// </remarks>
    public static class FileZIPService {
        /// <summary>
        /// Zips a string into a zipped byte array.
        /// </summary>
        /// <param name="textToZip">The text to be zipped.</param>
        /// <returns>byte[] representing a zipped stream</returns>
        public static byte[] Zip(string textToZip, string entryName) {
            using (var memoryStream = new MemoryStream()) {
                using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true)) {
                    var demoFile = zipArchive.CreateEntry(entryName);

                    using (var entryStream = demoFile.Open()) {
                        using (var streamWriter = new StreamWriter(entryStream)) {
                            streamWriter.Write(textToZip);
                        }
                    }
                }

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Unzip a zipped byte array into a string.
        /// </summary>
        /// <param name="zippedBuffer">The byte array to be unzipped</param>
        /// <returns>string representing the original stream</returns>
        public static string Unzip(byte[] zippedBuffer) {
            using (var zippedStream = new MemoryStream(zippedBuffer)) {
                using (var archive = new ZipArchive(zippedStream)) {
                    var entry = archive.Entries.FirstOrDefault();

                    if (entry != null) {
                        using (var unzippedEntryStream = entry.Open()) {
                            using (var ms = new MemoryStream()) {
                                unzippedEntryStream.CopyTo(ms);
                                var unzippedArray = ms.ToArray();
                                var d0 = DetectBomBytes(unzippedArray);
                                Encoding utf8WithoutBom = new UTF8Encoding(false);
                                return Encoding.UTF8.GetString(Encoding.Convert(d0, utf8WithoutBom, unzippedArray));
                            }
                        }
                    }

                    return null;
                }
            }
        }

        public static byte[] ReadFully(Stream input) {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream()) {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static Encoding DetectBomBytes(byte[] bomBytes) {
            if (bomBytes == null) {
                throw new ArgumentNullException("Must provide a valid BOM byte array!", "BOMBytes");
            }

            if (bomBytes.Length < 2) {
                return null;
            }

            if (bomBytes[0] == 255 && bomBytes[1] == 254 && ((bomBytes.Length) < 4 || bomBytes[2] != 0 || bomBytes[3] != 0)) {
                return Encoding.Unicode;
            }

            if (bomBytes[0] == 254 && bomBytes[1] == 255) {
                return Encoding.BigEndianUnicode;
            }

            if (bomBytes.Length < 3) {
                return null;
            }

            if (bomBytes[0] == 239 && bomBytes[1] == 187 && bomBytes[2] == 191) {
                return Encoding.UTF8;
            }

            if (bomBytes[0] == 43 && bomBytes[1] == 47 && bomBytes[2] == 118) {
                return Encoding.UTF7;
            }

            if (bomBytes.Length < 4) {
                return null;
            }

            if (bomBytes[0] == 255 && bomBytes[1] == 254 && bomBytes[2] == 0 && bomBytes[3] == 0) {
                return Encoding.UTF32;
            }

            if (bomBytes[0] != 0 || bomBytes[1] != 0 || bomBytes[2] != 254 || bomBytes[3] != 255) {
                return Encoding.ASCII;
            }

            return Encoding.GetEncoding(12001);
        }
    }
}
