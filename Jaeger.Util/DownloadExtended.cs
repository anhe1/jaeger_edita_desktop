﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Jaeger.Util {
    public class DownloadExtended {
        protected virtual void OnProcessDownload(DownloadChanged e) {
            EventHandler<DownloadChanged> onProcessDownload = ProcessDownload;
            if (onProcessDownload != null)
                onProcessDownload(this, e);
        }

        public event EventHandler<DownloadChanged> ProcessDownload;

        public static async Task<BindingList<DescargaElemento>> Run(BindingList<DescargaElemento> listfiles, string carpeta, IProgress<Descarga> proceso) {
            BindingList<DescargaElemento> output = new BindingList<DescargaElemento>();
            int contador = 0;
            await Task.Run(() => {
                Parallel.ForEach<DescargaElemento>(listfiles, (comprobante) => {
                    DescargaElemento results = DescargaNormal(comprobante, carpeta);
                    output.Add(results);
                    contador = contador + 1;
                    proceso.Report(new Descarga((output.Count * 100) / listfiles.Count, listfiles.Count, contador, "Descargando "));
                });
            });

            return output;
        }

        public static void Run2_(BindingList<DescargaElemento> listfiles, string carpeta, IProgress<Descarga> proceso) {
            BindingList<DescargaElemento> output = new BindingList<DescargaElemento>();
            int contador = 0;
            Task.Run(() => {
                Parallel.ForEach<DescargaElemento>(listfiles, (comprobante) => {
                    DescargaElemento results = DescargaNormal(comprobante, carpeta);
                    output.Add(results);
                    contador = contador + 1;
                    proceso.Report(new Descarga((output.Count * 100) / listfiles.Count, listfiles.Count, contador, "Descargando "));
                });
            });
        }

        #region metodos por probar

        public static BindingList<DescargaElemento> RunDownloadSync(BindingList<DescargaElemento> listfiles, string carpeta) {
            BindingList<DescargaElemento> output = new BindingList<DescargaElemento>();
            foreach (var item in listfiles) {
                output.Add(DescargaNormal(item, carpeta));
            }
            return output;
        }

        public BindingList<DescargaElemento> RunDownloadParallelSync(BindingList<DescargaElemento> listfiles, string carpeta) {
            BindingList<DescargaElemento> output = new BindingList<DescargaElemento>();
            Parallel.ForEach<DescargaElemento>(listfiles, (site) => {
                DescargaElemento result = DescargaNormal(site, carpeta);
                this.OnProcessDownload(new DownloadChanged() { Caption = "Descargando: " + output.Count.ToString() });
                output.Add(result);
            });
            return output;
        }

        public static async Task<BindingList<DescargaElemento>> RunDownloadParallelAsyncV2(BindingList<DescargaElemento> listfiles, string carpeta, IProgress<Descarga> proceso) {
            BindingList<DescargaElemento> output = new BindingList<DescargaElemento>();
            int contador = 0;
            await Task.Run(() => {
                Parallel.ForEach<DescargaElemento>(listfiles, (comprobante) => {
                    DescargaElemento results = DescargaNormal(comprobante, carpeta);
                    output.Add(results);
                    contador = contador + 1;
                    proceso.Report(new Descarga((output.Count * 100) / listfiles.Count, listfiles.Count, contador, "Descargando "));
                });
            });

            return output;
        }

        public static async Task<BindingList<DescargaElemento>> RunDownloadAsync(BindingList<DescargaElemento> listfiles, string carpeta, IProgress<Descarga> proceso, CancellationToken cancellationToken) {
            int contador = 0;
            BindingList<DescargaElemento> output = new BindingList<DescargaElemento>();
            foreach (var item in listfiles) {
                DescargaElemento result = await DownloadAsync(item);
                output.Add(result);
                cancellationToken.ThrowIfCancellationRequested();
                proceso.Report(new Descarga((output.Count * 100) / listfiles.Count, listfiles.Count, contador, "Descargando "));
            }
            return output;
        }

        public static async Task<List<DescargaElemento>> RunDownloadParallelAsync(BindingList<DescargaElemento> listfiles, string carpeta, IProgress<Descarga> proceso) {
            BindingList<Task<DescargaElemento>> tasks = new BindingList<Task<DescargaElemento>>();
            foreach (var item in listfiles) {
                tasks.Add(DownloadAsync(item));
            }
            var results = await Task.WhenAll(tasks);
            return new List<DescargaElemento>(results);
        }

        #endregion

        public static DescargaElemento DescargaNormal(DescargaElemento elemento, string carpeta) {
            elemento.Descargado = FileService.DownloadFile(elemento.URL, System.IO.Path.Combine(carpeta, elemento.KeyName));
            return elemento;
        }

        private static async Task<DescargaElemento> DownloadAsync(DescargaElemento elemento) {
            WebClient client = new WebClient();
            await client.DownloadFileTaskAsync(elemento.URL, elemento.KeyName);
            return elemento;
        }
    }

    public partial class DownloadChanged : EventArgs {
        public DownloadChanged() {

        }

        public string Caption {
            get; set;
        }

        public override string ToString() {
            return Caption;
        }
    }

    public partial class Descarga {
        public Descarga() {
            this.Terminado = false;
        }

        public Descarga(string caption) {
            this.Caption = caption;
            this.Terminado = false;
        }

        public Descarga(int completado, int total, int contador, string caption, bool terminado = false) {
            this.Completado = completado;
            this.Contador = contador;
            this.Caption = caption;
            this.Terminado = terminado;
        }

        /// <summary>
        /// obtener o establecer el % completado
        /// </summary>
        public int Completado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el contador de items
        /// </summary>
        public int Contador {
            get; set;
        }

        /// <summary>
        /// obtener o establecer texto
        /// </summary>
        public string Caption {
            get; set;
        }

        /// <summary>
        /// obtener o establecer si el proceso esta terminado
        /// </summary>
        public bool Terminado {
            get; set;
        }

        public override string ToString() {
            return string.Format("Descargando {0} | {1} %", this.Contador, this.Completado);
        }
    }

    public partial class DescargaElemento {
        public string URL {
            get; set;
        }
        public string KeyName {
            get; set;
        }
        public bool Descargado {
            get; set;
        }
    }
}
