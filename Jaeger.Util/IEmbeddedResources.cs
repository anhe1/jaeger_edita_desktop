﻿using System.IO;

namespace Jaeger.Util {
    public interface IEmbeddedResources {
        /// <summary>
        /// obtener recurso desde el ensablado especifico y almacenar en la ruta especifica
        /// </summary>
        /// <param name="assembly">nombre del ensamblado</param>
        /// <param name="nameResource">nombre del recurso</param>
        /// <param name="fileName">nombre del archivo de salida</param>
        /// <returns></returns>
        bool GetResource(string nameResource, string fileName);

        /// <summary>
        /// obtener un recurso 
        /// </summary>
        /// <param name="assembly">nombre del esamblado</param>
        /// <param name="nameResource">nombre del recurso</param>
        /// <returns></returns>
        Stream GetStream(string nameResource);

        /// <summary>
        /// obtener recurso en un array de bytes[]
        /// </summary>
        byte[] GetAsBytes(string resourceName);

        /// <summary>
        /// obtener el contenido de un recurso en formato string, utilizado para archivos de texto
        /// </summary>
        /// <param name="resourceName">nombre del recurso</param>
        /// <returns>cadena string</returns>
        string GetAsString(string resourceName);

        /// <summary>
        /// obtener las rutas y nombres de los rescursos de un ensablado especificado
        /// </summary>
        string[] GetList();
    }
}
