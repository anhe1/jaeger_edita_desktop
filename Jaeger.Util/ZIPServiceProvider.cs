﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Jaeger.Util {
    public class ZIPServiceProvider {
        public bool ExtractZipToDirectory(string zipFilename) {
            bool flag = false;
            Dictionary<string, Stream> keyValues = new Dictionary<string, Stream>();
            try {
                using (ZipArchive zipArchive = ZipFile.Open(zipFilename, ZipArchiveMode.Read)) {

                    foreach (var item in zipArchive.Entries) {
                        keyValues.Add(item.FullName, item.Open());
                    }
                }
                flag = true;
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
            }

            return flag;
        }

        public bool ExtractZipToDirectory1(string zipFilename, string extractionDirectory) {
            bool flag = false;
            Dictionary<string, Stream> keyValues = new Dictionary<string, Stream>();
            try {
                using (ZipArchive zipArchive = ZipFile.Open(zipFilename, ZipArchiveMode.Read)) {
                    foreach (var item in zipArchive.Entries) {
                        keyValues.Add(item.FullName, item.Open());
                    }
                }
                var archivo = new FileStream(zipFilename, FileMode.Open);
                var d0 = ReadStream(archivo);
                var d1 = GetFiles(d0);
                foreach (var item in d1) {
                    string result = Encoding.UTF8.GetString(item.Value);
                    Console.WriteLine(result);
                }
                flag = true;
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
            }

            return flag;
        }

        public static Dictionary<string, byte[]> GetFiles(byte[] zippedFile) {
            using (MemoryStream ms = new MemoryStream(zippedFile))
            using (ZipArchive archive = new ZipArchive(ms, ZipArchiveMode.Read)) {
                return archive.Entries.ToDictionary(x => x.FullName, x => ReadStream(x.Open()));
            }
        }

        private static byte[] ReadStream(Stream stream) {
            using (var ms = new MemoryStream()) {
                stream.CopyTo(ms);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Copies the contents of input to output. Doesn't close either stream.
        /// </summary>
        public static void CopyStream(Stream input, Stream output) {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0) {
                output.Write(buffer, 0, len);
            }
        }
    }

    public interface IZIPServiceProvider {
        IZIPServiceProviderForArchive ForArchive(string fileName);
    }

    public interface IZIPServiceProviderForArchive {
        IZIPServiceProviderExtractIn ExtractIn(string directoryName);
    }

    public interface IZIPServiceProviderExtractIn {
        bool Execute();
    }
}
