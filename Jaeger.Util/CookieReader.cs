﻿/// develop: ANHE 040920192157
/// purpose: clase para recuperar recursos de un ensamblado
using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Jaeger.Util {
    public class CookieReader {
        public const int InternetCookieHttponly = 8192;

        public static string GetCookie(string url) {
            string str;
            int num = 512;
            StringBuilder stringBuilder = new StringBuilder(num);
            if (!CookieReader.InternetGetCookieEx(url, null, stringBuilder, ref num, InternetCookieHttponly, IntPtr.Zero)) {
                if (num >= 0) {
                    stringBuilder = new StringBuilder(num);
                    if (!CookieReader.InternetGetCookieEx(url, null, stringBuilder, ref num, InternetCookieHttponly, IntPtr.Zero)) {
                        str = null;
                        return str;
                    }
                } else {
                    str = null;
                    return str;
                }
            }
            str = stringBuilder.ToString();
            return str;
        }

        [DllImport("wininet.dll", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool InternetGetCookieEx(string url, string cookieName, StringBuilder cookieData, ref int size, int flags, IntPtr pReserved);

        [DllImport("wininet.dll", CharSet = CharSet.Auto, ExactSpelling = false, SetLastError = true)]
        public static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);
    }
}
