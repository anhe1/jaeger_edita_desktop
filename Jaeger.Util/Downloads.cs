﻿using System.Runtime.InteropServices;

namespace Jaeger.Util {
    public class Downloads {
        public static bool File(string url, string nameFile) {
            bool flag = false;
            if (Downloads.Urldownloadtofile(0, url, nameFile, 0, 0) == 0) {
                flag = true;
            }
            return flag;
        }

        [DllImport("urlmon.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint = "URLDownloadToFileW", ExactSpelling = true, SetLastError = true)]
        public static extern int Urldownloadtofile(int pCaller, string szurl, string szfilename, int reserved, int callback);
    }
}
