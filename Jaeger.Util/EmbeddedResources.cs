﻿/// develop: ANHE 040920192157
/// purpose: clase para recuperar recursos de un ensamblado
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Linq;

namespace Jaeger.Util {
    public class EmbeddedResources : IEmbeddedResources {
        private readonly Assembly localAssembly;

        public EmbeddedResources(string assembly) {
            this.localAssembly = Assembly.Load(assembly);
        }

        public bool GetResource(string nameResource, string fileName) {
            using (Stream stream = this.localAssembly.GetManifestResourceStream(nameResource)) {
                if (stream == null)
                    return false;
                FileStream oFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                stream.CopyTo(oFileStream);
                oFileStream.Close();
            }
            return File.Exists(fileName);
        }

        public Stream GetStream(string nameResource) {
            Stream stream = this.localAssembly.GetManifestResourceStream(nameResource);
            return stream;
        }

        public byte[] GetAsBytes(string resourceName) {
            try {
                using (Stream resourceStream = this.localAssembly.GetManifestResourceStream(resourceName)) {
                    byte[] content = new byte[resourceStream.Length];
                    resourceStream.Read(content, 0, content.Length);
                    return content;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// obtener el contenido de un recurso en formato string, utilizado para archivos de texto
        /// </summary>
        /// <param name="resourceName">nombre del recurso</param>
        /// <returns>cadena string</returns>
        public string GetAsString(string resourceName) {
            var contenido = this.GetAsBytes(resourceName);
            if (contenido != null)
                return Encoding.UTF8.GetString(contenido);
            return string.Empty;
        }

        public string[] GetList() {
            return this.localAssembly.GetManifestResourceNames().ToArray();
        }

        #region metodos estaticos

        public static bool GetResource(string assembly, string nameResource, string fileName) {
            EmbeddedResources resource = new EmbeddedResources(assembly);
            return resource.GetResource(nameResource, fileName);
        }

        public static string[] GetList(string assembly) {
            Assembly localAssembly = Assembly.Load(assembly);
            return localAssembly.GetManifestResourceNames().ToArray();
        }

        #endregion
    }
}
