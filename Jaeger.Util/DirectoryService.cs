﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Jaeger.Util {
    public class DirectoryService {
        public void CreateDirectory(string directory) {
            Directory.CreateDirectory(directory);
        }

        public void CreateDirectoryConPermisos(string directory) {
            bool flag;
            SecurityIdentifier securityIdentifier = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            DirectoryInfo directoryInfo = Directory.CreateDirectory(directory);
            DirectorySecurity accessControl = directoryInfo.GetAccessControl();
            AccessRule fileSystemAccessRule = new FileSystemAccessRule(securityIdentifier, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);
            accessControl.ModifyAccessRule(AccessControlModification.Add, fileSystemAccessRule, out flag);
            directoryInfo.SetAccessControl(accessControl);
        }

        /// <summary>
        /// eliminar directorio
        /// </summary>
        /// <param name="directory">ruta del diretorio</param>
        public void Delete(string directory) {
            if (Exists(directory))
                Directory.Delete(directory);
        }

        public void Delete(string directory, bool recursive) {
            Directory.Delete(directory, recursive);
        }

        public async Task DeleteAsync(string directory, bool recursive) {
            await Task.Run(() => Directory.Delete(directory, recursive));
        }

        public void DeleteDirectory(string directory) {
            Directory.Delete(directory);
        }

        public static bool Exists(string directory) {
            return Directory.Exists(directory);
        }

        public string GetCurrentDirectory() {
            return Directory.GetCurrentDirectory();
        }

        public string[] GetDirectories(string directory) {
            return Directory.GetDirectories(directory);
        }

        public string[] GetDirectories(string directory, string searchPattern) {
            return Directory.GetDirectories(directory, searchPattern);
        }

        public string[] GetDirectories(string directory, string searchPattern, SearchOption options) {
            return Directory.GetDirectories(directory, searchPattern, options);
        }

        public async Task<string[]> GetDirectoriesAsync(string directory, string searchPattern, SearchOption options) {
            string[] strArrays = await Task.Run<string[]>(() => Directory.GetDirectories(directory, searchPattern, options));
            return strArrays;
        }

        public string[] GetFiles(string directory) {
            return Directory.GetFiles(directory);
        }

        public string[] GetFiles(string directory, string searchPattern) {
            return Directory.GetFiles(directory, searchPattern);
        }

        public string[] GetFiles(string directory, string searchPattern, SearchOption options) {
            return Directory.GetFiles(directory, searchPattern, options);
        }

        public async Task<string[]> GetFilesAsync(string directory, string searchPattern, SearchOption options) {
            string[] strArrays = await Task.Run<string[]>(() => Directory.GetFiles(directory, searchPattern, options));
            return strArrays;
        }

        public DirectoryInfo GetParent(string directory) {
            return Directory.GetParent(directory);
        }

        public void Move(string directory, string destination) {
            Directory.Move(directory, destination);
        }

        #region metodos estaticos
        /// <summary>
        /// comprobar si es un directorio
        /// </summary>
        public static bool IsDirectory(string path) {
            try {
                var atributes = File.GetAttributes(path);
                return atributes.HasFlag(FileAttributes.Directory);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
        #endregion
    }
}
