﻿using System;
using System.Collections.Generic;
using Jaeger.LockOn.Entities;
using Jaeger.LockOn.Interfaces;

namespace Jaeger.LockOn.Services {
    public class CommandService : ICommandService, IDisposable {
        public IRecienteModel Execute(string[] arguments) {
            var args = ParseCommand.Parse(string.Join(" ", arguments), false);
            foreach (var item in args) {
                ParseCommand.Option commandItem = item;
                string lower = commandItem.Name.ToLower();
                if (lower == "start") {
                    return this.Execute(ParseCommand.Parse(commandItem.Value.ToString()));
                } else if (lower == "local") {

                } else if (lower == "help") {
                    Console.WriteLine("help: No hay!");
                }
            }
            return null;
        }

        private IRecienteModel Execute(List<ParseCommand.Option> args) {
            var result = new RecienteModel();
            foreach (var objeto in args) {
                string lower = objeto.Name.ToLower();
                if (lower == "u") {
                    result.User = objeto.Value;
                } else if (lower == "p") {
                    result.Password = objeto.Value;
                } else if (lower == "e") {
                    result.RFC = objeto.Value;
                }
            }
            return result;
        }

        public void Dispose() {

        }
    }
}
