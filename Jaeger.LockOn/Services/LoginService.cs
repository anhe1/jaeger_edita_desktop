﻿using System;
using Jaeger.Dataaccess.Shatterdome.Repositories;
using Jaeger.DataAccess.Kaiju;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.LockOn.Contracts;
using Jaeger.Domain.LockOn.Entities;
using Jaeger.Domain.Services;
using Jaeger.Domain.Shatterdome.Contracts;
using Jaeger.Domain.Base.Profile;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.DataAccess.Empresa.Repositories;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Aplication.LockOn {
    /// <summary>
    /// servicio de sesion
    /// </summary>
    public class LoginService : ILoginService {
        protected string localFileSynapsis = @"C:\Jaeger\jaeger_striker_eureka.json";
        protected ILocalSinapsisService localSinapsis;
        protected ISqlShatterdomeRepository serverJaeger;
        private SynapsisBasic basic;
        private DataBaseConfiguracion _edita;

        public Configuracion Configuracion {
            get; set;
        }

        public KaijuLogger Piloto {
            get; set;
        }

        public bool Ready {
            get;set;
        }

        public string Message {
            get;set;
        }

        public string Dominio {
            get;set;
        }

        public LoginService() {
            this.Ready = false;
            this.localSinapsis = new LocalSinapsisService();
            this.Message = "";
            this.Load();
            if (this.ConectarEDITA() == false)
                this.Message = this.Message;

        }

        public bool Load() {
            try {
                var contenido = string.Empty;
                if (System.IO.File.Exists(this.localFileSynapsis) == false) {
                    contenido = Properties.Resources.striker_eureka;
                } else {
                    contenido = System.IO.File.ReadAllText(localFileSynapsis, System.Text.Encoding.UTF8);
                }
                this.basic = this.localSinapsis.Load(contenido);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
        
        public bool ExecuteLogin(IRecienteModel usuario) {
            if (this.ConectarEDITA() == false)
                return false;

            // buscamos la empresa
            var confEdita = this.serverJaeger.GetByRFC(usuario.RFC);
            if (confEdita != null) {
                this.Dominio = confEdita.Clave;

                ISqlUserRepository usuarios = new SqlSugarUserRepository(confEdita.Synapsis.BaseDatos.Edita);
                ISqlUserRolRepository perfilRepository = new SqlSugarUserRolRepository(confEdita.Synapsis.BaseDatos.Edita, "SYSLOG");
                ISqlConfiguracionRepository configuracionRepository = new SqlSugarConfiguracionRepository(confEdita.Synapsis.BaseDatos.Edita);
                var piloto = usuarios.GetUserBy(usuario.User, ValidacionService.EsCorreo(usuario.User));
                if (piloto != null) {
                    piloto.Password = "$2a" + piloto.Password.Substring(3, piloto.Password.Length - 3);
                    var success =  BCrypt.Net.BCrypt.Verify(usuario.Password, piloto.Password);
                    if (success) {
                        this.Piloto = new KaijuLogger {
                            Nombre = piloto.Nombre,
                            Correo = piloto.Correo,
                            Id = piloto.IdUser,
                            Clave = piloto.Clave,
                            IdPerfil = piloto.IdUserRol
                        };

                        var perfil = perfilRepository.GetById(this.Piloto.IdPerfil);
                        if (perfil != null) {
                            this.Piloto.Roles.Add(new Domain.Base.Abstractions.Rol(perfil.Nombre.ToLower()));
                        }

                        var data2 = ComprobanteConfig.Json(configuracionRepository.GetByKey("cfdi").Data);
                        Console.WriteLine(data2.Json());
                        // aqui asignamos la información de la configuracion de EDITA
                        this.Configuracion = new Configuracion();
                        Configuracion.Empresa.Clave = confEdita.Clave;
                        Configuracion.Empresa.RFC = confEdita.RFC;
                        Configuracion.Empresa.RazonSocial = data2.General.RazonSocial;
                        Configuracion.Empresa.RegimenFiscal = data2.General.RegimenFiscal;
                        Configuracion.Empresa.DomicilioFiscal = data2.DomicilioFiscal;
                        Configuracion.Amazon = confEdita.Synapsis.Amazon;
                        // proveedores de certificacion
                        Configuracion.ProveedorAutorizado = confEdita.Synapsis.PAC;
                        Configuracion.ProveedorAutorizado.Certificacion.RFC = confEdita.RFC;
                        Configuracion.ProveedorAutorizado.Cancelacion.RFC = confEdita.RFC;
                        Configuracion.ProveedorAutorizado.Validacion.RFC = confEdita.RFC;
                        // informacion de la base de datos
                        Configuracion.RDS = confEdita.Synapsis.BaseDatos;
                        return success;
                    } else {
                        this.Message = "Usuario o contraseña no validos.";
                    }
                } else {
                    this.Message = "Usuario o contraseña no validos.";
                }
            } else {
                if (serverJaeger.Message != null) {
                    this.Message = serverJaeger.Message;
                } else {
                    this.Message = "No se encontro información referente a la empresa solicitada.";
                }
            }
            return false;
        }

        private bool ConectarEDITA() {
            // configuracion para conectar con shatterdome
            this._edita = new DataBaseConfiguracion {
                Database = this.basic.V2.DataBase,
                Charset = this.basic.V2.Charset,
                ForcedWrite = this.basic.V2.ForcedWrite,
                HostName = this.basic.V2.HostName,
                ModoSSL = this.basic.V2.ModoSSL,
                PageSize = this.basic.V2.PageSize,
                Password = this.basic.V2.Password,
                Pooling = this.basic.V2.Pooling,
                PortNumber = this.basic.V2.PortNumber,
                UserID = this.basic.V2.UserID
            };
            this.serverJaeger = new SqlSugarShatterdomeRepository(this._edita);
            if (this.serverJaeger.TestConfig(this._edita) == false) {
                this.Message = this.serverJaeger.Message;
                return false;
            }
            return true;
        }
    }
}
