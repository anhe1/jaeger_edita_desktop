﻿using System;
using System.Collections.Generic;

namespace Jaeger.LockOn.Services {
    public static class ParseCommand {
        public partial class Option {
            public Option() {
            }

            public Option(string name, string value) {
                this.Name = name;
                this.Value = value;
            }

            public string Name { get; set; }

            public string Value { get; set; }
        }

        public static List<Option> Parse(string value, bool testing = false) {
            List<Option> parseCommandLine;
            List<Option> commandLineArgs = new List<Option>();
            if (string.IsNullOrEmpty(value)) {
                parseCommandLine = new List<Option>();
            } else if (value.Contains("/")) {
                string[] separadores = new string[] { "/" };
                string[] strArrays = value.Split(separadores, StringSplitOptions.None);
                for (int i = 0; i < checked(strArrays.Length); i++) {
                    string arg = strArrays[i];
                    if (!string.IsNullOrEmpty(arg)) {
                        if (arg.Contains("=")) {
                            int idx = arg.IndexOf("=");
                            if (idx < checked(arg.Length - 1)) {
                                commandLineArgs.Add(new Option { Name = arg.Substring(0, idx).Trim(), Value = arg.Substring(checked(idx + 1)).Trim() });
                            }
                        }
                    }
                }
                parseCommandLine = commandLineArgs;
            } else {
                parseCommandLine = new List<Option>();
            }
            return parseCommandLine;
        }

        public static List<Option> Parse(string value) {
            List<Option> parseCommandLine;
            List<Option> commandLineArgs = new List<Option>();
            if (string.IsNullOrEmpty(value)) {
                parseCommandLine = new List<Option>();
            } else if (value.Contains("-")) {
                string[] separadores = new string[] { "-" };
                string[] strArrays = value.Split(separadores, StringSplitOptions.None);
                for (int i = 0; i < checked(strArrays.Length); i++) {
                    string arg = strArrays[i];
                    if (!string.IsNullOrEmpty(arg)) {
                        if (arg.Contains(" ")) {
                            int idx = arg.IndexOf(" ");
                            if (idx < checked(arg.Length - 1)) {
                                commandLineArgs.Add(new Option { Name = arg.Substring(0, idx).Trim(), Value = arg.Substring(checked(idx + 1)).Trim() });
                            }
                        }
                    }
                }
                parseCommandLine = commandLineArgs;
            } else {
                parseCommandLine = new List<Option>();
            }
            return parseCommandLine;
        }
    }
}
