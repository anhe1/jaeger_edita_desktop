﻿using Jaeger.LockOn.Interfaces;

namespace Jaeger.LockOn.Services {
    public class LoginBuilder : ILoginBuilder, ILoginLoadBuilder {
        #region declaraciones
        private ILogin _Login;
        #endregion

        public static ILogin Create() {
            return new Login();
        }

        public ILogin Connect() {
            this._Login = this._Login.Connect();
            return this._Login;
        }

        public ILoginLoadBuilder Load() {
            this._Login = this._Login.Load();
            return this;
        }
    }
}
