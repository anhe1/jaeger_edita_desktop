﻿using System;
using System.IO;

namespace Jaeger.LockOn.Services {
    static public class LogginErrorLog {
        public static string FileName;

        /// <summary>
        /// Constructor
        /// </summary>
        static LogginErrorLog() {
            LogginErrorLog.FileName = "C:\\Jaeger\\Jaeger.Log\\formJaeger.log";
        }

        static public bool LogDelete() {
            try {
                File.Delete(LogginErrorLog.FileName);
                return true;
            } catch (Exception e) {
                LogginErrorLog.LogWrite(e.Message);
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static void LogWrite(string mensaje) {
            try {
                if (!File.Exists(LogginErrorLog.FileName)) {
                    File.Create(LogginErrorLog.FileName).Close();
                }
                var streamWriter = File.AppendText(LogginErrorLog.FileName);
                object[] type = new object[] { mensaje, ",", DateTime.Now };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            } catch (Exception e) {
                LogginErrorLog.LogWrite(e.Message);
                Console.WriteLine(e.Message);
            }
        }
    }
}
