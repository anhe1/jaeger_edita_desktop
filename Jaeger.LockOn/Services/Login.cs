﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Profile;
using Jaeger.LockOn.Interfaces;
using Jaeger.LockOn.Entities;
using Jaeger.LockOn.Repositories;
using Jaeger.Domain.Base.Services;

namespace Jaeger.LockOn.Services {
    public class Login : ILogin {
        #region 
        protected ISqlShatterdomeRepository _Server;
        protected LocalDataBase _Local;
        private bool _IsReady = false;
        private readonly string _LocalFileName = @"C:\Jaeger\jaeger_striker_eureka.json";
        private Domain.Empresa.Entities.Configuracion _Configuracion;
        private string _Dominio;
        private string _Message;
        #endregion

        public Login() { }

        public bool IsReady { get { return this._IsReady; } }

        public string Dominio { get { return this._Dominio; } }

        public string Message { get { return this._Message; } }

        public KaijuLogger Piloto { get; set; }

        public Domain.Empresa.Entities.Configuracion Data { get { return this._Configuracion; } }

        public Login Connect() {
            this._IsReady = false;
            this._Server = new SqlSugarShatterdomeRepository(this.GetConfiguration());
            if (this._Server != null) {
                this._IsReady = true;
            } else {
                this._Message = this._Server.Message;
            }
            return this;
        }

        public Login Load() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            try {
                var contenido = this.GetString();
                _Local = JsonConvert.DeserializeObject<LocalDataBase>(HelperCsTripleDes.Decrypt(contenido, "", true), conf);
                if (_Local == null)
                    _Local = JsonConvert.DeserializeObject<LocalDataBase>(contenido, conf);
            } catch (Exception ex) {
                this._Message = ex.StackTrace;
            }
            return this;
        }

        public bool Execute(IRecienteModel usuario) {
            // buscamos la empresa
            var confEdita = this._Server.GetByRFC(usuario.RFC);
            if (confEdita != null) {
                this._Dominio = confEdita.Dominio;
                ISqlUserRepository usuarios = new SqlSugarUserRepository(confEdita.Synapsis.BaseDatos.Edita);
                var piloto = usuarios.GetUserBy(usuario.User, ValidacionService.IsMail(usuario.User));
                if (piloto != null) {
                    piloto.Password = "$2a" + piloto.Password.Substring(3, piloto.Password.Length - 3);
                    var success = DevOne.Security.Cryptography.BCrypt.BCryptHelper.CheckPassword(usuario.Password, piloto.Password);
                    if (success) {
                        this.Piloto = new KaijuLogger {
                            Nombre = piloto.Nombre,
                            Correo = piloto.Correo,
                            Id = piloto.IdUser,
                            Clave = piloto.Clave,
                            IdPerfil = piloto.IdUserRol,
                        };
                        // aqui asignamos la información de la configuracion de EDITA
                        this._Configuracion = new Domain.Empresa.Entities.Configuracion();
                        _Configuracion.Empresa.Clave = confEdita.Clave;
                        _Configuracion.Empresa.RFC = confEdita.RFC;
                        //_Configuracion.Empresa.RazonSocial = data2.General.RazonSocial;
                        //_Configuracion.Empresa.RegimenFiscal = data2.General.RegimenFiscal;
                        //_Configuracion.Empresa.DomicilioFiscal = data2.DomicilioFiscal;
                        _Configuracion.Amazon = confEdita.Synapsis.Amazon;
                        // proveedores de certificacion
                        _Configuracion.ProveedorAutorizado = confEdita.Synapsis.PAC;
                        _Configuracion.ProveedorAutorizado.Certificacion.RFC = confEdita.RFC;
                        _Configuracion.ProveedorAutorizado.Cancelacion.RFC = confEdita.RFC;
                        _Configuracion.ProveedorAutorizado.Validacion.RFC = confEdita.RFC;
                        _Configuracion.RDS = confEdita.Synapsis.BaseDatos;
                        return true;
                    } else {
                        this._Message = "Usuario o contraseña incorrecta.";
                    }
                } else {
                    this._Message = "Usuario o contraseña incorrecta.";
                }
            } else {
                //this._Message = this._Server.Message;
                this._Message = "Empresa no registrada.";
            }
            return false;
        }

        #region metodos privados
        private string GetString() {
            try {
                if (System.IO.File.Exists(this._LocalFileName) == false)
                    return Jaeger.LockOn.Properties.Resources.striker_eureka;
                return System.IO.File.ReadAllText(this._LocalFileName);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return string.Empty;
        }

        private DataBaseConfiguracion GetConfiguration() {
            // configuracion para conectar con shatterdome
            return new DataBaseConfiguracion {
                Database = this._Local.DataBase,
                Charset = this._Local.Charset,
                ForcedWrite = this._Local.ForcedWrite,
                HostName = this._Local.HostName,
                ModoSSL = this._Local.ModoSSL,
                PageSize = this._Local.PageSize,
                Password = this._Local.Password,
                Pooling = this._Local.Pooling,
                PortNumber = this._Local.PortNumber,
                UserID = this._Local.UserID
            };
        }
        #endregion
    }
}
