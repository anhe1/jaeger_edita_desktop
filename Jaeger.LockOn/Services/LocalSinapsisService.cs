﻿using System;
using Jaeger.Domain.LockOn.Entities;
using Jaeger.Domain.Services;
using Newtonsoft.Json;

namespace Jaeger.Aplication.LockOn {
    public class LocalSinapsisService : ILocalSinapsisService {
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
        public SynapsisBasic Load() {
            return null;
        }

        public SynapsisBasic Load(string contenido) {
            try {
                var local = new SynapsisBasic();
                local = JsonConvert.DeserializeObject<SynapsisBasic>(HelperCsTripleDes.Decrypt(contenido, "", true), this.conf);
                if (local == null)
                    local = JsonConvert.DeserializeObject<SynapsisBasic>(contenido, this.conf);
                return local;

            } catch (Exception) {
                return null;
            }
        }

        public bool Save(SynapsisBasic basic) {
            return false;
        }
    }
}
