﻿namespace Jaeger.LockOn.Services.Restore {
    public interface IResponse {
        bool IsSuccess { get; }
        IMessages Message { get; set; }
    }
}
