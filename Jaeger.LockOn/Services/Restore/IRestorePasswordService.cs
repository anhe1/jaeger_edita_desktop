﻿namespace Jaeger.LockOn.Services.Restore {
    public interface IResetPasswordService {
        IResetPasswordService WithMail(string mail);

        IResetPasswordService WithDomain(string domain);

        IResponse Execute();
    }
}
