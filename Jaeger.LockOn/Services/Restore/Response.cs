﻿namespace Jaeger.LockOn.Services.Restore {
    public class Response : IResponse {
        public bool IsSuccess { get; set; }
        public IMessages Message { get; set; }
    }
}
