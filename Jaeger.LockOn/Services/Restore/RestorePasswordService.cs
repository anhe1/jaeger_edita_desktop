﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;

namespace Jaeger.LockOn.Services.Restore {
    /// <summary>
    /// recuperar password de la cuenta de EDITA
    /// </summary>
    public class ResetPasswordService : IResetPasswordService {
        #region declaraciones
        private string _Mail;
        private string _Domain;
        #endregion

        public ResetPasswordService() {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback =
              delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
        }

        public IResetPasswordService WithMail(string mail) {
            this._Mail = mail;
            return this;
        }

        public IResetPasswordService WithDomain(string domain) {
            this._Domain = domain;
            return this;
        }

        public IResponse Execute() {
            var endpoint = string.Format("https://{0}.ipo.com.mx/api/session?email={1}", this._Domain, this._Mail);
            return this.Execute(endpoint);
        }

        private IResponse Execute(string endPoint) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endPoint);
            request.Method = "PUT";
            IResponse value = new Response();
            HttpWebResponse response = null;

            try {
                response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream()) {
                    if (responseStream != null) {
                        using (StreamReader reader = new StreamReader(responseStream)) {
                            var serializer = new JsonSerializer();
                            var jsonTextReader = new JsonTextReader(reader);
                            value = serializer.Deserialize<IResponse>(jsonTextReader);

                        }
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            } finally {
                if (response != null) {
                    ((IDisposable)response).Dispose();
                }
            }
            return value;
        }
    }
}
