﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Jaeger.LockOn.Interfaces;
using Jaeger.LockOn.Entities;

namespace Jaeger.LockOn.Services {
    /// <summary>
    /// repositorio de login reciente version 2.0.1
    /// </summary>
    public class RecienteRepository : IRecienteRepository {
        #region declaraciones
        private readonly JsonSerializerSettings _ConfigJson;
        private readonly string _LocalFileName = "jaeger_lock_on.json";
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public RecienteRepository() {
            this._LocalFileName = Path.Combine(@"C:\Jaeger", this._LocalFileName);
            this._ConfigJson = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            this.Items = new List<IRecienteModel>();
        }

        public RecienteRepository(string fileName) {
            this._LocalFileName = fileName;
            this._ConfigJson = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            this.Items = new List<IRecienteModel>();
        }

        public string Version { get { return Assembly.GetExecutingAssembly().GetName().Version.ToString(); } }

        /// <summary>
        /// obtener o establecer objetos recientes
        /// </summary>
        public List<IRecienteModel> Items { get; set; }

        /// <summary>
        /// agregar usuario
        /// </summary>
        public void Add(string rfc, string userID) {
            try {
                var existe = this.Items.Where(it => it.RFC.ToLower().Trim() == rfc.ToLower().Trim()).FirstOrDefault();
                if (existe == null) {
                    this.Items.Add(new RecienteModel(rfc, userID));
                } else {
                    existe.User = userID;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// cargar archivo local
        /// </summary>
        public IRecienteRepository Load() {
            if (File.Exists(this._LocalFileName)) {
                var d0 = File.ReadAllText(this._LocalFileName);
                try {
                    this.Items = JsonConvert.DeserializeObject<List<RecienteModel>>(d0).ToList<IRecienteModel>()
                        .ToList();
                    if (this.Items == null) {
                        this.Items = new List<IRecienteModel>();
                    }

                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            } else {
                this.Items = new List<IRecienteModel>();
            }
            return this;
        }

        public void Save() {
            try {
                File.WriteAllText(this._LocalFileName, JsonConvert.SerializeObject(this.Items, this._ConfigJson));
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
