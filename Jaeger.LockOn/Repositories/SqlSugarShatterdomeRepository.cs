﻿using System.Linq;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.Services;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.LockOn.Entities;
using Jaeger.LockOn.Interfaces;

namespace Jaeger.LockOn.Repositories {
    public class SqlSugarShatterdomeRepository : MySqlSugarContext<ShatterdomeModel>, ISqlShatterdomeRepository {
        public SqlSugarShatterdomeRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener la configuración de la empresa por su rFC
        /// </summary>
        public ShatterdomeModel GetByRFC(string rfc) {
            try {
                return this.Db.Queryable<ShatterdomeModel>().Where(it => it.Activo == 1).Where(it => it.RFC == rfc).Single();
            } catch (System.Exception ex) {
                try {
                    var d = this.Db.Queryable<ShatterdomeModel>().Where(it => it.RFC == rfc).ToSql();
                    var d1 = this.GetMapper<ShatterdomeModel>(d.Key, d.Value).First();
                    return d1;
                } catch (System.Exception ex2) {
                    LogErrorService.LogWrite(ex.Message);
                    this.Message = ex2.Message;
                }
                LogErrorService.LogWrite(ex.Message);
                this.Message = ex.Message;
                return null;
            }
        }
    }
}
