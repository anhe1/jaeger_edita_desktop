﻿using System;
using System.Linq;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.LockOn.Entities;
using Jaeger.LockOn.Interfaces;

namespace Jaeger.LockOn.Repositories {
    /// <summary>
    /// repositorio de usuarios
    /// </summary>
    public class SqlSugarUserRepository : MySqlSugarContext<UsuarioModel>, ISqlUserRepository {
        public SqlSugarUserRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        ~SqlSugarUserRepository() { this.Db.Close(); }

        public UsuarioModel GetUserBy(string clave, bool correo) {
            try {
                var response = this.Db.Queryable<UsuarioModel>()
                    .Where(it => it.Activo == 1)
                    .WhereIF(correo, it => it.Correo == clave)
                    .WhereIF(correo == false, it => it.Clave == clave.ToUpper().Trim()).Single();
                return response;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                var response = this.Db.Queryable<UsuarioModel>()
                    .WhereIF(correo, it => it.Correo == clave)
                    .WhereIF(correo == false, it => it.Clave == clave.ToUpper().Trim()).ToSql();
                var result = this.GetMapper<UsuarioModel>(response.Key, response.Value).First();
                return result;
            }
        }
    }
}
