﻿namespace Jaeger.LockOn.Interfaces {
    public interface IRecienteModel {
        /// <summary>
        /// obtener o establecer registro federal de contribuyentes
        /// </summary>
        string RFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre del tema utilizado
        /// </summary>
        string Tema {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre del usuario
        /// </summary>
        string User {
            get; set;
        }

        /// <summary>
        /// obtener o establecer password de la cuenta
        /// </summary>
        string Password {
            get; set;
        }
    }
}
