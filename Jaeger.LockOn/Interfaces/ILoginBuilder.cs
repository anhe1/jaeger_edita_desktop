﻿namespace Jaeger.LockOn.Interfaces {
    public interface ILoginBuilder {
        ILoginLoadBuilder Load();
    }

    public interface ILoginLoadBuilder {
        ILogin Connect();
    }
}
