﻿using Jaeger.LockOn.Services;
using Jaeger.Domain.Base.Profile;

namespace Jaeger.LockOn.Interfaces {
    public interface ILogin {
        bool IsReady { get; }

        string Dominio { get; }

        string Message { get; }

        KaijuLogger Piloto { get; set; }

        Domain.Empresa.Entities.Configuracion Data { get; }

        Login Connect();

        Login Load();

        bool Execute(IRecienteModel usuario);
    }
}
