﻿using Jaeger.Domain.LockOn.Entities;

namespace Jaeger.Aplication.LockOn {
    public interface ILocalSinapsisService {
        SynapsisBasic Load();
        SynapsisBasic Load(string contenido);
        bool Save(SynapsisBasic basic);
    }
}
