﻿using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Base.Profile;
using Jaeger.Domain.LockOn.Contracts;

namespace Jaeger.Aplication.LockOn {
    public interface ILoginService {
        KaijuLogger Piloto {
            get; set;
        }

        Configuracion Configuracion {
            get; set;
        }

        bool Ready {
            get;set;
        }

        string Message {
            get; set;
        }

        string Dominio {
            get; set;
        }

        bool ExecuteLogin(IRecienteModel usuario);

        bool Load();
    }
}
