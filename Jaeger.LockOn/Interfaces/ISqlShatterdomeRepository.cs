﻿using Jaeger.Domain.Contracts;
using Jaeger.LockOn.Entities;

namespace Jaeger.LockOn.Interfaces {
    public interface ISqlShatterdomeRepository : IGenericRepository<ShatterdomeModel> {
        /// <summary>
        /// obtener la configuración de la empresa por su rFC
        /// </summary>
        ShatterdomeModel GetByRFC(string rfc);
    }
}
