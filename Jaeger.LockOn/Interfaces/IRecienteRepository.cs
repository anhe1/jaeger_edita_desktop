﻿using System.Collections.Generic;

namespace Jaeger.LockOn.Interfaces {
    /// <summary>
    /// repositorio 
    /// </summary>
    public interface IRecienteRepository {
        /// <summary>
        /// obtener o establecer objetos recientes
        /// </summary>
        List<IRecienteModel> Items { get; set; }

        /// <summary>
        /// agregar usuario
        /// </summary>
        void Add(string rfc, string userID);

        /// <summary>
        /// cargar archivo local
        /// </summary>
        IRecienteRepository Load();

        void Save();
    }
}
