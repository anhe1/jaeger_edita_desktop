﻿using Jaeger.Domain.Contracts;
using Jaeger.LockOn.Entities;

namespace Jaeger.LockOn.Interfaces {
    public interface ISqlUserRepository : IGenericRepository<UsuarioModel> {
        UsuarioModel GetUserBy(string clave, bool correo);
    }
}
