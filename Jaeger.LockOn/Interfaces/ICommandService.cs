﻿using System;

namespace Jaeger.LockOn.Interfaces {
    public interface ICommandService : IDisposable {
        IRecienteModel Execute(string[] args);
    }
}
