﻿using SqlSugar;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.LockOn.Entities {
    /// <summary>
    /// configuracion de empresa
    /// </summary>
    [SugarTable("empr")]
    public class ShatterdomeModel {
        public ShatterdomeModel() { }

        [DataNames("empr_a", "EMPR_A")]
        [SugarColumn(ColumnName = "empr_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public int Activo { get; set; }

        [DataNames("empr_rgf_id")]
        [SugarColumn(ColumnName = "empr_rgf_id", ColumnDescription = "id del regimen fiscal")]
        public int RegimenFiscal { get; set; }

        [DataNames("empr_clv")]
        [SugarColumn(ColumnName = "empr_clv", ColumnDescription = "clave de la empresa")]
        public string Clave { get; set; }

        [DataNames("empr_rfc")]
        [SugarColumn(ColumnName = "empr_rfc", ColumnDescription = "clave del registro federal de contribuyentes")]
        public string RFC { get; set; }

        [DataNames("empr_dom")]
        [SugarColumn(ColumnName = "empr_dom", ColumnDescription = "dominio asigando a la empresa")]
        public string Dominio { get; set; }

        [DataNames("empr_mail")]
        [SugarColumn(ColumnName = "empr_mail", ColumnDescription = "correo electronico de contacto")]
        public string Correo { get; set; }

        /// <summary>
        /// obtener o establecer el nombre o denominacion social de la empresa registrada
        /// </summary>
        [DataNames("empr_nom")]
        [SugarColumn(ColumnName = "empr_nom", ColumnDescription = "nombre o denominacion social de la empresa registrada")]
        public string Nombre { get; set; }

        [DataNames("empr_rep")]
        [SugarColumn(ColumnName = "empr_rep", ColumnDescription = "nombre del representante legal", Length = 255)]
        public string RepresentanteLegal { get; set; }

        /// <summary>
        /// obtener o establecer la configuracion general de la empresa.
        /// </summary>
        [DataNames("empr_conf")]
        [SugarColumn(ColumnName = "empr_conf", ColumnDataType = "TEXT", ColumnDescription = "configuracion general de la empresa.", IsNullable = true)]
        public string Data { get; set; }

        [DataNames("empr_direc")]
        [SugarColumn(ColumnName = "empr_direc", ColumnDataType = "TEXT", ColumnDescription = "direccion fiscal", IsNullable = true)]
        public string DireccionFiscal { get; set; }

        [SugarColumn(IsIgnore = true)]
        public SynapsisModel Synapsis {
            get {
                return SynapsisModel.Json(this.Data);
            }
        }
    }
}
