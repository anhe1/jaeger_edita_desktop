﻿using Newtonsoft.Json;
using Jaeger.LockOn.Interfaces;

namespace Jaeger.LockOn.Entities {
    [JsonObject]
    public class RecienteModel : IRecienteModel {
        public RecienteModel() {
        }

        public RecienteModel(string rfc, string userID) {
            this.RFC = rfc;
            this.User = userID;
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del tema utilizado
        /// </summary>
        [JsonProperty("tema")]
        public string Tema { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("pass")]
        public string Password { get; set; }
    }
}
