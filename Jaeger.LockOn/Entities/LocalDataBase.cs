﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.LockOn.Entities {
    /// <summary>
    /// clase que contiene la configuracion de la base de datos
    /// </summary>
    [JsonObject("conf")]
    public class LocalDataBase  {
        #region declaraciones
        private string _DataBase;
        private string _DataSource;
        private int _PortNumber;
        private string _Charset;
        private bool _Pooling;
        private int _PageSize;
        private int _ModoSSL;
        private bool _ForcedWrite;
        private string _UserID;
        private string _Password;
        #endregion

        public LocalDataBase() {
            this.PortNumber = 4530;
            this._ModoSSL = 0;
        }

        /// <summary>
        /// obtener o establecer el nombre de la base de datos
        /// </summary>
        [DisplayName("Base de Datos"), Description("Nombre de la base de datos.")]
        [DefaultValue("Server")]
        [JsonProperty("database")]
        public string DataBase {
            get {
                return this._DataBase;
            }
            set {
                if (this._DataBase != value) {
                    this._DataBase = value;
                }
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del servidor
        /// </summary>
        [DisplayName("Servidor"), Description("Servidor o Host del servicio de base de datos.")]
        [JsonProperty("host")]
        public string HostName {
            get {
                return this._DataSource;
            }
            set {
                if (this._DataSource != value) {
                    this._DataSource = value;
                }
            }
        }

        /// <summary>
        /// obtener o establecer el numero del puerto del servidor
        /// </summary>
        [DisplayName("Puerto"), Description("Puerto de conexión, default 3306")]
        [JsonProperty("port")]
        public int PortNumber {
            get {
                return this._PortNumber;
            }
            set {
                if (this._PortNumber != value) {
                    this._PortNumber = value;
                }
            }
        }

        [DisplayName("Modo SSL"), Description("Modo SSL aplicable a Mysql (default 0)")]
        [JsonProperty("modoSSL")]
        public int ModoSSL {
            get {
                return this._ModoSSL;
            }
            set {
                this._ModoSSL = value;
            }
        }

        /// <summary>
        /// obtener o establecer el juego de caracteres utilizado
        /// </summary>
        [DisplayName("Charset"), Description("juego de caracteres utilizado")]
        [JsonProperty("charset")]
        public string Charset {
            get {
                return this._Charset;
            }
            set {
                if (this._Charset != value) {
                    this._Charset = value;
                }
            }
        }

        [JsonProperty("pooling")]
        public bool Pooling {
            get {
                return this._Pooling;
            }
            set {
                if (this._Pooling != value) {
                    this._Pooling = value;
                }
            }
        }

        [JsonProperty("pagesize")]
        public int PageSize {
            get {
                return this._PageSize;
            }
            set {
                if (this._PageSize != value) {
                    this._PageSize = value;
                }
            }
        }

        [JsonProperty("forcewrite")]
        public bool ForcedWrite {
            get {
                return this._ForcedWrite;
            }
            set {
                if (this._ForcedWrite != value) {
                    this._ForcedWrite = value;
                }
            }
        }

        /// <summary>
        /// obtener o estalecer el nombre de usuario
        /// </summary>
        [DisplayName("Nombre de usuario"), Description("Nombre de usuario")]
        [JsonProperty("user")]
        public string UserID {
            get {
                return this._UserID;
            }
            set {
                if (this._UserID != value) {
                    this._UserID = value;
                }
            }
        }

        /// <summary>
        /// obtener o establecer el password de la base de datos
        /// </summary>
        [DisplayName("Contraseña"), Description("Contraseña del servicio.")]
        [JsonProperty("pass")]
        public string Password {
            get {
                return this._Password;
            }
            set {
                if (this._Password != value) {
                    this._Password = value;
                }
            }
        }
    }
}
