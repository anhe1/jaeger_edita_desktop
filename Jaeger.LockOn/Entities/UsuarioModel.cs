﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.LockOn.Entities {
    [SugarTable("_user", "usuarios: tabla de usuarios")]
    public class UsuarioModel {

        public UsuarioModel() { }

        [DataNames("_user_id", "USER_ID")]
        [SugarColumn(ColumnName = "_user_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true, IsOnlyIgnoreInsert = true)]
        public int IdUser { get; set; }

        [DataNames("_user_a", "USER_A")]
        [SugarColumn(ColumnName = "_user_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public int Activo { get; set; }

        [DataNames("_user_idroll", "USER_IDROLL")]
        [SugarColumn(ColumnName = "_user_idroll", ColumnDescription = "ID de roll", DefaultValue = "0")]
        public int IdUserRol { get; set; }

        [DataNames("_user_psw", "USER_PSW")]
        [SugarColumn(ColumnName = "_user_psw", ColumnDescription = "password (md5 y base64)", Length = 64)]
        public string Password { get; set; }
        
        [DataNames("_user_nom", "USER_NOM")]
        [SugarColumn(ColumnName = "_user_nom", ColumnDescription = "nombre completo del usuario", Length = 128)]
        public string Nombre { get; set; }

        [DataNames("_user_mail", "USER_MAIL")]
        [SugarColumn(ColumnName = "_user_mail", ColumnDescription = "correo electronico, separados por (;)", IsNullable = true, Length = 128)]
        public string Correo { get; set; }

        [DataNames("_user_clv", "USER_CLV")]
        [SugarColumn(ColumnName = "_user_clv", ColumnDescription = "clave unica de registro en sistema", Length = 14, IsNullable = true)]
        public string Clave { get; set; }
    }
}
