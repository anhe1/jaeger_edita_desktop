﻿using System.Collections.Generic;

namespace Jaeger.Aplication.Helpers.Contracts {
    /// <summary>
    /// clase para busqueda de archivos
    /// </summary>
    public interface ISearchWorker : Base.Contracts.IWorker {
        /// <summary>
        /// obtener o establecer data
        /// </summary>
        Dictionary<string, byte[]> DataSource { get; }

        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, string> Errores { get; }

        /// <summary>
        /// obtener origenes
        /// </summary>
        List<string> Sources { get; }

        ISearchWorker Add(string source);
        ISearchWorker Add(List<string> sources);
        ISearchWorker Execute();
    }
}
