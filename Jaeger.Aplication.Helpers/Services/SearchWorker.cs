﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Helpers;
using Jaeger.Aplication.Helpers.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.Util;

namespace Jaeger.Aplication.Helpers.Services {
    /// <summary>
    /// clase para busqueda de archivos
    /// </summary>
    public class SearchWorker : Base.Abstracts.WorkerBase, IWorker, ISearchWorker {
        #region declaraciones
        protected Dictionary<string, byte[]> _DataSource;
        protected Dictionary<string, string> _Errores;
        protected List<string> _Sources;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public SearchWorker() : base("SearchWorker") {
            this._DataSource = new Dictionary<string, byte[]>();
            this._Sources = new List<string>();
            this._Errores = new Dictionary<string, string>();
        }

        /// <summary>
        /// obtener o establecer diccionario de archivos origen
        /// </summary>
        public Dictionary<string, byte[]> DataSource {
            get {
                return this._DataSource;
            }
        }

        /// <summary>
        /// obtener o establecer origines de los datos
        /// </summary>
        public List<string> Sources {
            get {
                return this._Sources;
            }
        }

        public Dictionary<string, string> Errores {
            get { return this._Errores; }
        }

        #region implementation
        public override bool IsCancelable {
            get { return true; }
        }

        public override bool SupportsDualProgress {
            get { return true; }
        }

        public override object Result {
            get { return this.DataSource; }
        }

        public override void BeginWork() {
            if (_worker != null && _worker.IsAlive)
                throw new ApplicationException($"{this._workerName} is already in progress");

            _cancelled = false;

            ThreadStart ts = delegate {
                try {
                    this.NotifyPrimaryProgress(0, "Iniciando búsqueda ...");
                    this.Execute();
                    this.NotifyPrimaryProgress(true, 100, "Terminado.");
                } catch (UserCancellationException uex) {
                    this.NotifyPrimaryProgress(true, 100, uex);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            };
            this.Thread(ts).Start();
        }

        public override void Cancel() {
            this._cancelled = true;
        }
        #endregion

        #region builder
        public ISearchWorker Source() {
            if (this._DataSource == null)
                this._DataSource = new Dictionary<string, byte[]>();
            if (this._Sources == null)
                this._Sources = new List<string>();
            this._Errores = new Dictionary<string, string>();
            return this;
        }

        /// <summary>
        /// agregar origen
        /// </summary>
        /// <param name="source">origen</param>
        public ISearchWorker Add(string source) {
            // en caso de no estar vacio
            if (!string.IsNullOrEmpty(source)) {
                this._Sources.Add(source);
            }
            return this;
        }

        /// <summary>
        /// agregar lista de origenes
        /// </summary>
        /// <param name="sources">List<string></param>
        public ISearchWorker Add(List<string> sources) {
            foreach (var source in sources) {
                this.Add(source);
            }
            return this;
        }

        /// <summary>
        /// limpieza
        /// </summary>
        public new ISearchWorker Clear() {
            this._DataSource = new Dictionary<string, byte[]>();
            this._Sources = new List<string>();
            this._Errores = new Dictionary<string, string>();
            return this;
        }

        public ISearchWorker Execute() {
            if (this._Sources.Count > 0) {
                var index = 0;
                foreach (var source in this._Sources) {
                    index++;
                    this.NotifyPrimaryProgress(false, (int)this.Progress(index, this._Sources.Count), $"Resolviendo {source}");
                    var data = this.ResolverSource(source);
                    if (data != null) {
                        this._DataSource.AddRange(data);
                    }
                }
            }
            return this;
        }

        public IWorker GetWorker() {
            return this;
        }
        #endregion

        #region metodos para resolver el origen de los archivos
        protected Dictionary<string, byte[]> ResolverSource(string source) {
            this.NotifyPrimaryProgress(false, 0, $"Buscando archivos desde: {source}");
            if (DirectoryService.IsDirectory(source)) {
                if (DirectoryService.Exists(source) == false) { return null; }
                return this.GetSourceFolder(source);
            } else if (Path.GetExtension(source).ToLower() == ".zip") {
                return this.GetZIP(source);
            } else if (Path.GetExtension(source).ToLower() == ".xml" | Path.GetExtension(source).ToLower() == ".pdf") {
                return this.GetFile(source);
            } else {
                //throw new Exception("Tipo de Importador no soportado");
                this._Errores.Add(source, "Tipo de importador no soportado");
                return null;
            }
        }

        protected Dictionary<string, byte[]> GetFile(string fileName) {
            if (!File.Exists(fileName)) {
                throw new ArgumentException("Invalid source catalog: is not a directory");
            }
            var filesResult = new Dictionary<string, byte[]>();
            // solo aceptamos archivos con extension XML y PDF
            if (Path.GetExtension(fileName).ToLower() == ".xml" | Path.GetExtension(fileName).ToLower() == ".pdf") {
                filesResult.Add(fileName, File.ReadAllBytes(fileName));
            }
            return filesResult;
        }

        protected Dictionary<string, byte[]> GetZIP(string fileZIP) {
            if (!File.Exists(fileZIP)) {
                throw new ArgumentException("Invalid source catalog: is not a directory");
            }
            var filesResult = new Dictionary<string, byte[]>();
            filesResult = ZIPServiceProvider.GetFiles(File.ReadAllBytes(fileZIP));
            return filesResult;
        }

        protected Dictionary<string, byte[]> GetSourceFolder(string folderName) {
            if (DirectoryService.Exists(folderName) == false) {
                return null;
            }
            this.NotifySecondaryProgress(false, 0, $"Buscando archivos desde: {folderName}");
            var filesSource = this.GetFiles(folderName, SearchOption.AllDirectories);
            var filesResult = new Dictionary<string, byte[]>();

            if (filesSource != null) {
                foreach (var file in filesSource) {
                    try {
                        this.NotifySecondaryProgress(false, (int)this.Progress(filesResult.Count, filesSource.Length), $"{file}");
                        filesResult.Add(file, File.ReadAllBytes(file));
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return filesResult;
        }

        /// <summary>
        /// Obtener lista de archivos del directorio especifico, se fitran por extensiones PDF y XML
        /// </summary>
        protected string[] GetFiles(string directory, SearchOption options) {
            // filtrar directorios con 
            try {
                return Directory.GetFiles(directory, "*.*", options).Where(s => s.EndsWith(".xml") || s.EndsWith(".pdf")).ToArray();
            } catch (Exception ex) {
                this._Errores.Add(directory, ex.Message);
                return null;
            }
        }
        #endregion
    }
}
