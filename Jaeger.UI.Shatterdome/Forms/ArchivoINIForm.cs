﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Jaeger.UI.Shatterdome.Forms {
    public partial class ArchivoINIForm : Form {
        

        public ArchivoINIForm() {
            InitializeComponent();
        }

        private void ArchivoINIForm_Load(object sender, EventArgs e) {
            
        }

        private void ToolBarButtonAbrir_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { AddExtension = true, DefaultExt = "json", Title = "Abrir archivo de configuración", Filter = "*.json|*.JSON" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                try {
                    this.Tag = openFile.FileName;
                    var contenido = File.ReadAllText(openFile.FileName, Encoding.UTF8);
                    this.textBoxContenido.Text = contenido;
                } catch (Exception ex) {
                    MessageBox.Show(this, ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            try {
                File.WriteAllText((string)this.Tag, this.textBoxContenido.Text);
            } catch (Exception ex) {
                MessageBox.Show(this, ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.Message);
            }
            var saveFile = new SaveFileDialog { AddExtension = true, DefaultExt = "json", Title = "Abrir archivo de configuración", Filter = "*.json|*.JSON", FileName = (string)this.Tag };
            if (saveFile.ShowDialog(this) == DialogResult.OK) {
            }
        }

        private void ToolBarButtonGuardarComo_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog { AddExtension = true, DefaultExt = "json", Title = "Abrir archivo de configuración", Filter = "*.json|*.JSON" };
            if (saveFile.ShowDialog(this) == DialogResult.OK) {
            }
        }

        private void ToolBarButtonSerializar_Click(object sender, EventArgs e) {
            
        }

        private void ToolBarButtonEncriptar_Click(object sender, EventArgs e) {
            textBoxContenido.Text = Encrypt(textBoxContenido.Text, ToolTextBoxPassword.Text, true);
        }

        public static string Encrypt(string toEncrypt, string key, bool useHashing) {
            byte[] bytes;
            byte[] numArray = Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing) {
                MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
            } else {
                bytes = Encoding.UTF8.GetBytes(key);
            }
            TripleDESCryptoServiceProvider tripleDesCryptoServiceProvider = new TripleDESCryptoServiceProvider() {
                Key = bytes,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cryptoTransform = tripleDesCryptoServiceProvider.CreateEncryptor();
            byte[] numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, numArray.Length);
            return Convert.ToBase64String(numArray1, 0, numArray1.Length);
        }

        public static string Decrypt(string toDecrypt, string key, bool useHashing) {
            byte[] bytes;
            string decrypt;
            string str;
            try {
                byte[] numArray = Convert.FromBase64String(toDecrypt);
                if (useHashing) {
                    MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                    bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
                } else {
                    bytes = Encoding.UTF8.GetBytes(key);
                }
                TripleDESCryptoServiceProvider tripleDesCryptoServiceProvider = new TripleDESCryptoServiceProvider() {
                    Key = bytes,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                ICryptoTransform cryptoTransform = tripleDesCryptoServiceProvider.CreateDecryptor();
                byte[] numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, numArray.Length);
                str = Encoding.UTF8.GetString(numArray1);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                decrypt = "";
                return decrypt;
            }
            decrypt = str;
            return decrypt;
        }
    }
}
