﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using Jaeger.Aplication.Shatterdome.Empresas;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Profile;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Shatterdome.Forms {
    public partial class MainForm : Form {
        private readonly Assembly currentAssem = Assembly.GetExecutingAssembly();
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            var local = new LocalSynapsisService();
            if (local.Load() == false) {
                MessageBox.Show(this, "No se encontro el archivo de configuración local.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.TInformacion.Text = string.Format("Server: {0}", ConfigService.Synapsis.Configuracion.HostName);
            this.ToolStripButtonDataBase.Text = string.Format("DB: {0}", ConfigService.Synapsis.Configuracion.Database);

            var d = new KaijuLogger {
                Roles = new List<Rol>() { new Rol { Name = "administrador" } },
                IdPerfil = 1
            };
            var m = this.Menus();
            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Disabled);
            MenuPermissions.Load(m);
            UIMenuUtility.SetPermission(this, d, MenuPermissions);
        }

        private void Menu_Herramientas_GeneradorPassword_Click(object sender, EventArgs e) {
            var generador_form = new GeneradorPasswordForm();
            generador_form.ShowDialog(this);
        }

        private void Menu_Ayuda_AcercaDe_Click(object sender, EventArgs e) {
            var acercaDe_Form = new AboutBoxForm();
            acercaDe_Form.ShowDialog(this);
        }

        private void Menu_Archivo_Configuracion_Click(object sender, EventArgs e) {
            var configuracion_form = new ConfiguracionForm();
            configuracion_form.ShowDialog(this);
        }

        private void Menu_Archivo_Salir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Menu_Shatterdome_Click(object sender, EventArgs e) {
            var item = (ToolStripMenuItem)sender;
            

            if (item.Tag != null) {
                try {
                    var d = this.currentAssem.GetTypes();
                    Type type = this.currentAssem.GetType("Jaeger.UI.Shatterdome.Forms." + item.Tag.ToString(), true);
                    this.Form_Active(type);
                } catch (Exception ex) {
                    MessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            } else {
                MessageBox.Show(this, "No se implemento el formulario requerido.", "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Form_Active(Type type) {
            Form form = (Form)Activator.CreateInstance(type);
            form.MdiParent = this;
            form.Size = this.Size;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        public List<UIMenuElement> Menus() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Id = 1, ParentId = 0, Name = "m_Archivo", Label = "Archivo", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 2, ParentId = 1, Name = "m_arc_Configuracion", Label = "Configuración", Form = "", Rol = "Administrador" },
                new UIMenuElement { Id = 2, ParentId = 1, Name = "m_Empresas", Label = "Empresas", Form = "", Rol = "Administrador" },
                new UIMenuElement { Id = 6, ParentId = 1, Name = "m_arc_Salir", Label = "Salir", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 44, ParentId = 0, Name = "m_Ventana", Label = "Ventana", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 45, ParentId = 0, Name = "m_Ayuda", Label = "Ayuda", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 46, ParentId = 45, Name = "m_Ayuda_AcercaDe", Label = "Acerca de ...", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 46, ParentId = 45, Name = "m_Herramientas", Label = "Herramientas", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 46, ParentId = 45, Name = "m_her_GeneradorPassword", Label = "Password", Default = true, Form = "", Rol = "*" },
                new UIMenuElement { Id = 46, ParentId = 45, Name = "m_her_ArchivoINI", Label = "INI", Default = true, Form = "", Rol = "*" }
                
            };

            return response;
        }
    }
}
