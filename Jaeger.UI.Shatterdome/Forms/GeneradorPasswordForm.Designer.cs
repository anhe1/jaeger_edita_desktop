﻿namespace Jaeger.UI.Shatterdome.Forms {
    partial class GeneradorPasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LongitudMinText = new System.Windows.Forms.TextBox();
            this.LongitudMaxText = new System.Windows.Forms.TextBox();
            this.ExcluirCaracteresText = new System.Windows.Forms.TextBox();
            this.PasswordText = new System.Windows.Forms.TextBox();
            this.GenerarButton = new System.Windows.Forms.Button();
            this.LongitudMinLabel = new System.Windows.Forms.Label();
            this.LongitudMaxLabel = new System.Windows.Forms.Label();
            this.ExcluirCaracteresLabel = new System.Windows.Forms.Label();
            this.SegeridoLabel = new System.Windows.Forms.Label();
            this.MD5Text = new System.Windows.Forms.TextBox();
            this.MD5Label = new System.Windows.Forms.Label();
            this.RepetirCaracteresCheck = new System.Windows.Forms.CheckBox();
            this.CaracteresConsecutivosCheck = new System.Windows.Forms.CheckBox();
            this.ExcluirSimbolosCheck = new System.Windows.Forms.CheckBox();
            this.AvisoLabel = new System.Windows.Forms.Label();
            this.NumCaracteresLabel = new System.Windows.Forms.Label();
            this.ParametrosGroup = new System.Windows.Forms.GroupBox();
            this.CerrarButton = new System.Windows.Forms.Button();
            this.ParametrosGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // LongitudMinText
            // 
            this.LongitudMinText.Location = new System.Drawing.Point(294, 55);
            this.LongitudMinText.Name = "LongitudMinText";
            this.LongitudMinText.Size = new System.Drawing.Size(49, 20);
            this.LongitudMinText.TabIndex = 5;
            this.LongitudMinText.Text = "32";
            // 
            // LongitudMaxText
            // 
            this.LongitudMaxText.Location = new System.Drawing.Point(294, 81);
            this.LongitudMaxText.Name = "LongitudMaxText";
            this.LongitudMaxText.Size = new System.Drawing.Size(49, 20);
            this.LongitudMaxText.TabIndex = 6;
            this.LongitudMaxText.Text = "32";
            // 
            // ExcluirCaracteresText
            // 
            this.ExcluirCaracteresText.Location = new System.Drawing.Point(102, 21);
            this.ExcluirCaracteresText.Name = "ExcluirCaracteresText";
            this.ExcluirCaracteresText.Size = new System.Drawing.Size(241, 20);
            this.ExcluirCaracteresText.TabIndex = 1;
            this.ExcluirCaracteresText.Text = "`~!@#$%^&*()-_=+[]{}\\\\|;:\'\\\",<.>/?";
            // 
            // PasswordText
            // 
            this.PasswordText.Location = new System.Drawing.Point(9, 160);
            this.PasswordText.Multiline = true;
            this.PasswordText.Name = "PasswordText";
            this.PasswordText.ReadOnly = true;
            this.PasswordText.Size = new System.Drawing.Size(334, 40);
            this.PasswordText.TabIndex = 8;
            this.PasswordText.TextChanged += new System.EventHandler(this.Password_TextChanged);
            // 
            // GenerarButton
            // 
            this.GenerarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GenerarButton.Location = new System.Drawing.Point(288, 310);
            this.GenerarButton.Name = "GenerarButton";
            this.GenerarButton.Size = new System.Drawing.Size(75, 23);
            this.GenerarButton.TabIndex = 11;
            this.GenerarButton.Text = "Generar";
            this.GenerarButton.UseVisualStyleBackColor = true;
            this.GenerarButton.Click += new System.EventHandler(this.Generar_Click);
            // 
            // LongitudMinLabel
            // 
            this.LongitudMinLabel.AutoSize = true;
            this.LongitudMinLabel.Location = new System.Drawing.Point(214, 59);
            this.LongitudMinLabel.Name = "LongitudMinLabel";
            this.LongitudMinLabel.Size = new System.Drawing.Size(73, 13);
            this.LongitudMinLabel.TabIndex = 3;
            this.LongitudMinLabel.Text = "Longitud Mín.";
            // 
            // LongitudMaxLabel
            // 
            this.LongitudMaxLabel.AutoSize = true;
            this.LongitudMaxLabel.Location = new System.Drawing.Point(214, 85);
            this.LongitudMaxLabel.Name = "LongitudMaxLabel";
            this.LongitudMaxLabel.Size = new System.Drawing.Size(74, 13);
            this.LongitudMaxLabel.TabIndex = 3;
            this.LongitudMaxLabel.Text = "Longitud Máx.";
            // 
            // ExcluirCaracteresLabel
            // 
            this.ExcluirCaracteresLabel.AutoSize = true;
            this.ExcluirCaracteresLabel.Location = new System.Drawing.Point(6, 24);
            this.ExcluirCaracteresLabel.Name = "ExcluirCaracteresLabel";
            this.ExcluirCaracteresLabel.Size = new System.Drawing.Size(91, 13);
            this.ExcluirCaracteresLabel.TabIndex = 0;
            this.ExcluirCaracteresLabel.Text = "Excluir carácteres";
            // 
            // SegeridoLabel
            // 
            this.SegeridoLabel.AutoSize = true;
            this.SegeridoLabel.Location = new System.Drawing.Point(6, 138);
            this.SegeridoLabel.Name = "SegeridoLabel";
            this.SegeridoLabel.Size = new System.Drawing.Size(52, 13);
            this.SegeridoLabel.TabIndex = 7;
            this.SegeridoLabel.Text = "Sugerido:";
            // 
            // MD5Text
            // 
            this.MD5Text.Location = new System.Drawing.Point(42, 211);
            this.MD5Text.Name = "MD5Text";
            this.MD5Text.ReadOnly = true;
            this.MD5Text.Size = new System.Drawing.Size(223, 20);
            this.MD5Text.TabIndex = 10;
            // 
            // MD5Label
            // 
            this.MD5Label.AutoSize = true;
            this.MD5Label.Location = new System.Drawing.Point(6, 214);
            this.MD5Label.Name = "MD5Label";
            this.MD5Label.Size = new System.Drawing.Size(30, 13);
            this.MD5Label.TabIndex = 9;
            this.MD5Label.Text = "MD5";
            // 
            // RepetirCaracteresCheck
            // 
            this.RepetirCaracteresCheck.AutoSize = true;
            this.RepetirCaracteresCheck.Location = new System.Drawing.Point(9, 57);
            this.RepetirCaracteresCheck.Name = "RepetirCaracteresCheck";
            this.RepetirCaracteresCheck.Size = new System.Drawing.Size(113, 17);
            this.RepetirCaracteresCheck.TabIndex = 2;
            this.RepetirCaracteresCheck.Text = "Repetir carácteres";
            this.RepetirCaracteresCheck.UseVisualStyleBackColor = true;
            // 
            // CaracteresConsecutivosCheck
            // 
            this.CaracteresConsecutivosCheck.AutoSize = true;
            this.CaracteresConsecutivosCheck.Location = new System.Drawing.Point(9, 83);
            this.CaracteresConsecutivosCheck.Name = "CaracteresConsecutivosCheck";
            this.CaracteresConsecutivosCheck.Size = new System.Drawing.Size(143, 17);
            this.CaracteresConsecutivosCheck.TabIndex = 3;
            this.CaracteresConsecutivosCheck.Text = "Caracteres consecutivos";
            this.CaracteresConsecutivosCheck.UseVisualStyleBackColor = true;
            // 
            // ExcluirSimbolosCheck
            // 
            this.ExcluirSimbolosCheck.AutoSize = true;
            this.ExcluirSimbolosCheck.Location = new System.Drawing.Point(9, 106);
            this.ExcluirSimbolosCheck.Name = "ExcluirSimbolosCheck";
            this.ExcluirSimbolosCheck.Size = new System.Drawing.Size(100, 17);
            this.ExcluirSimbolosCheck.TabIndex = 4;
            this.ExcluirSimbolosCheck.Text = "Excluir simbolos";
            this.ExcluirSimbolosCheck.UseVisualStyleBackColor = true;
            // 
            // AvisoLabel
            // 
            this.AvisoLabel.AutoSize = true;
            this.AvisoLabel.Location = new System.Drawing.Point(61, 255);
            this.AvisoLabel.Name = "AvisoLabel";
            this.AvisoLabel.Size = new System.Drawing.Size(10, 13);
            this.AvisoLabel.TabIndex = 3;
            this.AvisoLabel.Text = "-";
            // 
            // NumCaracteresLabel
            // 
            this.NumCaracteresLabel.AutoSize = true;
            this.NumCaracteresLabel.Location = new System.Drawing.Point(271, 214);
            this.NumCaracteresLabel.Name = "NumCaracteresLabel";
            this.NumCaracteresLabel.Size = new System.Drawing.Size(73, 13);
            this.NumCaracteresLabel.TabIndex = 7;
            this.NumCaracteresLabel.Text = "Caracteres (0)";
            // 
            // ParametrosGroup
            // 
            this.ParametrosGroup.Controls.Add(this.ExcluirCaracteresLabel);
            this.ParametrosGroup.Controls.Add(this.ExcluirSimbolosCheck);
            this.ParametrosGroup.Controls.Add(this.LongitudMinText);
            this.ParametrosGroup.Controls.Add(this.CaracteresConsecutivosCheck);
            this.ParametrosGroup.Controls.Add(this.ExcluirCaracteresText);
            this.ParametrosGroup.Controls.Add(this.RepetirCaracteresCheck);
            this.ParametrosGroup.Controls.Add(this.LongitudMaxText);
            this.ParametrosGroup.Controls.Add(this.AvisoLabel);
            this.ParametrosGroup.Controls.Add(this.PasswordText);
            this.ParametrosGroup.Controls.Add(this.MD5Label);
            this.ParametrosGroup.Controls.Add(this.MD5Text);
            this.ParametrosGroup.Controls.Add(this.NumCaracteresLabel);
            this.ParametrosGroup.Controls.Add(this.LongitudMinLabel);
            this.ParametrosGroup.Controls.Add(this.SegeridoLabel);
            this.ParametrosGroup.Controls.Add(this.LongitudMaxLabel);
            this.ParametrosGroup.Location = new System.Drawing.Point(12, 12);
            this.ParametrosGroup.Name = "ParametrosGroup";
            this.ParametrosGroup.Size = new System.Drawing.Size(353, 290);
            this.ParametrosGroup.TabIndex = 12;
            this.ParametrosGroup.TabStop = false;
            this.ParametrosGroup.Text = "Parametros";
            // 
            // CerrarButton
            // 
            this.CerrarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CerrarButton.Location = new System.Drawing.Point(207, 310);
            this.CerrarButton.Name = "CerrarButton";
            this.CerrarButton.Size = new System.Drawing.Size(75, 23);
            this.CerrarButton.TabIndex = 13;
            this.CerrarButton.Text = "Cerrar";
            this.CerrarButton.UseVisualStyleBackColor = true;
            this.CerrarButton.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // GeneradorPasswordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 345);
            this.Controls.Add(this.CerrarButton);
            this.Controls.Add(this.ParametrosGroup);
            this.Controls.Add(this.GenerarButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GeneradorPasswordForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Generador de contraseñas";
            this.Load += new System.EventHandler(this.GeneradorPasswordForm_Load);
            this.ParametrosGroup.ResumeLayout(false);
            this.ParametrosGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox LongitudMinText;
        private System.Windows.Forms.TextBox LongitudMaxText;
        private System.Windows.Forms.TextBox ExcluirCaracteresText;
        private System.Windows.Forms.TextBox PasswordText;
        private System.Windows.Forms.Button GenerarButton;
        private System.Windows.Forms.Label LongitudMinLabel;
        private System.Windows.Forms.Label LongitudMaxLabel;
        private System.Windows.Forms.Label ExcluirCaracteresLabel;
        private System.Windows.Forms.Label SegeridoLabel;
        private System.Windows.Forms.TextBox MD5Text;
        private System.Windows.Forms.Label MD5Label;
        private System.Windows.Forms.CheckBox RepetirCaracteresCheck;
        private System.Windows.Forms.CheckBox CaracteresConsecutivosCheck;
        private System.Windows.Forms.CheckBox ExcluirSimbolosCheck;
        private System.Windows.Forms.Label AvisoLabel;
        private System.Windows.Forms.Label NumCaracteresLabel;
        private System.Windows.Forms.GroupBox ParametrosGroup;
        private System.Windows.Forms.Button CerrarButton;
    }
}