﻿
namespace Jaeger.UI.Shatterdome.Forms {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.m_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_arc_Configuracion = new System.Windows.Forms.ToolStripMenuItem();
            this.m_arc_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Empresas = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Herramientas = new System.Windows.Forms.ToolStripMenuItem();
            this.m_her_ArchivoINI = new System.Windows.Forms.ToolStripMenuItem();
            this.m_her_GeneradorPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Ventana = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Ayuda = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Ayuda_AcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.TInformacion = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolStripButtonDataBase = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Archivo,
            this.m_Empresas,
            this.m_Herramientas,
            this.m_Ventana,
            this.m_Ayuda});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.MdiWindowListItem = this.m_Ventana;
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "Menú";
            // 
            // m_Archivo
            // 
            this.m_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_arc_Configuracion,
            this.m_arc_Salir});
            this.m_Archivo.Name = "m_Archivo";
            this.m_Archivo.Size = new System.Drawing.Size(76, 20);
            this.m_Archivo.Text = "m_Archivo";
            // 
            // m_arc_Configuracion
            // 
            this.m_arc_Configuracion.Name = "m_arc_Configuracion";
            this.m_arc_Configuracion.Size = new System.Drawing.Size(166, 22);
            this.m_arc_Configuracion.Text = "m_Configuracion";
            this.m_arc_Configuracion.Click += new System.EventHandler(this.Menu_Archivo_Configuracion_Click);
            // 
            // m_arc_Salir
            // 
            this.m_arc_Salir.Name = "m_arc_Salir";
            this.m_arc_Salir.Size = new System.Drawing.Size(166, 22);
            this.m_arc_Salir.Text = "m_Salir";
            this.m_arc_Salir.Click += new System.EventHandler(this.Menu_Archivo_Salir_Click);
            // 
            // m_Empresas
            // 
            this.m_Empresas.Name = "m_Empresas";
            this.m_Empresas.Size = new System.Drawing.Size(85, 20);
            this.m_Empresas.Tag = "EmpresasForm";
            this.m_Empresas.Text = "m_Empresas";
            this.m_Empresas.Click += new System.EventHandler(this.Menu_Shatterdome_Click);
            // 
            // m_Herramientas
            // 
            this.m_Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_her_ArchivoINI,
            this.m_her_GeneradorPassword});
            this.m_Herramientas.Name = "m_Herramientas";
            this.m_Herramientas.Size = new System.Drawing.Size(106, 20);
            this.m_Herramientas.Text = "m_Herramientas";
            // 
            // m_her_ArchivoINI
            // 
            this.m_her_ArchivoINI.Name = "m_her_ArchivoINI";
            this.m_her_ArchivoINI.Size = new System.Drawing.Size(195, 22);
            this.m_her_ArchivoINI.Tag = "ArchivoINIForm";
            this.m_her_ArchivoINI.Text = "m_ArchivoINI";
            this.m_her_ArchivoINI.Click += new System.EventHandler(this.Menu_Shatterdome_Click);
            // 
            // m_her_GeneradorPassword
            // 
            this.m_her_GeneradorPassword.Name = "m_her_GeneradorPassword";
            this.m_her_GeneradorPassword.Size = new System.Drawing.Size(195, 22);
            this.m_her_GeneradorPassword.Tag = "GeneradorPasswordForm";
            this.m_her_GeneradorPassword.Text = "m_GeneradorPassword";
            this.m_her_GeneradorPassword.Click += new System.EventHandler(this.Menu_Herramientas_GeneradorPassword_Click);
            // 
            // m_Ventana
            // 
            this.m_Ventana.Name = "m_Ventana";
            this.m_Ventana.Size = new System.Drawing.Size(77, 20);
            this.m_Ventana.Text = "m_Ventana";
            // 
            // m_Ayuda
            // 
            this.m_Ayuda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Ayuda_AcercaDe});
            this.m_Ayuda.Name = "m_Ayuda";
            this.m_Ayuda.Size = new System.Drawing.Size(69, 20);
            this.m_Ayuda.Text = "m_Ayuda";
            // 
            // menu_Ayuda_AcercaDe
            // 
            this.menu_Ayuda_AcercaDe.Name = "menu_Ayuda_AcercaDe";
            this.menu_Ayuda_AcercaDe.Size = new System.Drawing.Size(199, 22);
            this.menu_Ayuda_AcercaDe.Text = "menu_Ayuda_AcercaDe";
            this.menu_Ayuda_AcercaDe.Click += new System.EventHandler(this.Menu_Ayuda_AcercaDe_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TInformacion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // TInformacion
            // 
            this.TInformacion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButtonDataBase});
            this.TInformacion.Image = global::Jaeger.UI.Shatterdome.Properties.Resources.material2tones_database_options_16px;
            this.TInformacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TInformacion.Name = "TInformacion";
            this.TInformacion.Size = new System.Drawing.Size(68, 20);
            this.TInformacion.Text = "Server";
            // 
            // ToolStripButtonDataBase
            // 
            this.ToolStripButtonDataBase.Name = "ToolStripButtonDataBase";
            this.ToolStripButtonDataBase.Size = new System.Drawing.Size(180, 22);
            this.ToolStripButtonDataBase.Text = "toolStripMenuItem1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "EDITA - Empresas (Shatterdome)";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo;
        private System.Windows.Forms.ToolStripMenuItem m_Empresas;
        private System.Windows.Forms.ToolStripMenuItem m_Herramientas;
        private System.Windows.Forms.ToolStripMenuItem m_Ventana;
        private System.Windows.Forms.ToolStripMenuItem m_Ayuda;
        private System.Windows.Forms.ToolStripMenuItem m_arc_Configuracion;
        private System.Windows.Forms.ToolStripMenuItem m_arc_Salir;
        private System.Windows.Forms.ToolStripMenuItem m_her_ArchivoINI;
        private System.Windows.Forms.ToolStripMenuItem m_her_GeneradorPassword;
        private System.Windows.Forms.ToolStripMenuItem menu_Ayuda_AcercaDe;
        private System.Windows.Forms.ToolStripDropDownButton TInformacion;
        private System.Windows.Forms.ToolStripMenuItem ToolStripButtonDataBase;
    }
}