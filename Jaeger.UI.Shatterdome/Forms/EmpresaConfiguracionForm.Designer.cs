﻿namespace Jaeger.UI.Shatterdome.Forms {
    partial class EmpresaConfiguracionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmpresaConfiguracionForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txbClave = new System.Windows.Forms.TextBox();
            this.txbRFC = new System.Windows.Forms.TextBox();
            this.txbNombre = new System.Windows.Forms.TextBox();
            this.txbDominio = new System.Windows.Forms.TextBox();
            this.txbCorreo = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txbContenido = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonIzquierda = new System.Windows.Forms.Button();
            this.buttonLimpiar = new System.Windows.Forms.Button();
            this.buttonDerecha = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.buttonTestRDS = new System.Windows.Forms.Button();
            this.txtRepresentanteLegal = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clave:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(170, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "RFC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Correo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dominio:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nombre:";
            // 
            // txbClave
            // 
            this.txbClave.Location = new System.Drawing.Point(69, 12);
            this.txbClave.MaxLength = 10;
            this.txbClave.Name = "txbClave";
            this.txbClave.Size = new System.Drawing.Size(88, 20);
            this.txbClave.TabIndex = 5;
            // 
            // txbRFC
            // 
            this.txbRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txbRFC.Location = new System.Drawing.Point(213, 12);
            this.txbRFC.MaxLength = 14;
            this.txbRFC.Name = "txbRFC";
            this.txbRFC.Size = new System.Drawing.Size(100, 20);
            this.txbRFC.TabIndex = 6;
            // 
            // txbNombre
            // 
            this.txbNombre.Location = new System.Drawing.Point(69, 38);
            this.txbNombre.MaxLength = 64;
            this.txbNombre.Name = "txbNombre";
            this.txbNombre.Size = new System.Drawing.Size(243, 20);
            this.txbNombre.TabIndex = 7;
            // 
            // txbDominio
            // 
            this.txbDominio.Location = new System.Drawing.Point(69, 64);
            this.txbDominio.MaxLength = 64;
            this.txbDominio.Name = "txbDominio";
            this.txbDominio.Size = new System.Drawing.Size(243, 20);
            this.txbDominio.TabIndex = 8;
            // 
            // txbCorreo
            // 
            this.txbCorreo.Location = new System.Drawing.Point(69, 90);
            this.txbCorreo.MaxLength = 64;
            this.txbCorreo.Name = "txbCorreo";
            this.txbCorreo.Size = new System.Drawing.Size(243, 20);
            this.txbCorreo.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txbContenido);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(327, 481);
            this.panel1.TabIndex = 10;
            // 
            // txbContenido
            // 
            this.txbContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbContenido.Location = new System.Drawing.Point(0, 176);
            this.txbContenido.Multiline = true;
            this.txbContenido.Name = "txbContenido";
            this.txbContenido.Size = new System.Drawing.Size(295, 305);
            this.txbContenido.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonIzquierda);
            this.panel3.Controls.Add(this.buttonLimpiar);
            this.panel3.Controls.Add(this.buttonDerecha);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(295, 176);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(32, 305);
            this.panel3.TabIndex = 15;
            // 
            // buttonIzquierda
            // 
            this.buttonIzquierda.Location = new System.Drawing.Point(0, 123);
            this.buttonIzquierda.Name = "buttonIzquierda";
            this.buttonIzquierda.Size = new System.Drawing.Size(32, 26);
            this.buttonIzquierda.TabIndex = 1;
            this.buttonIzquierda.UseVisualStyleBackColor = true;
            this.buttonIzquierda.Click += new System.EventHandler(this.buttonIzquierda_Click);
            // 
            // buttonLimpiar
            // 
            this.buttonLimpiar.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonLimpiar.Location = new System.Drawing.Point(0, 0);
            this.buttonLimpiar.Name = "buttonLimpiar";
            this.buttonLimpiar.Size = new System.Drawing.Size(32, 26);
            this.buttonLimpiar.TabIndex = 0;
            this.buttonLimpiar.UseVisualStyleBackColor = true;
            this.buttonLimpiar.Click += new System.EventHandler(this.buttonLimpiar_Click);
            // 
            // buttonDerecha
            // 
            this.buttonDerecha.Location = new System.Drawing.Point(0, 76);
            this.buttonDerecha.Name = "buttonDerecha";
            this.buttonDerecha.Size = new System.Drawing.Size(32, 26);
            this.buttonDerecha.TabIndex = 0;
            this.buttonDerecha.UseVisualStyleBackColor = true;
            this.buttonDerecha.Click += new System.EventHandler(this.buttonDerecha_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.buttonTestRDS);
            this.panel2.Controls.Add(this.txbClave);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtRepresentanteLegal);
            this.panel2.Controls.Add(this.txbRFC);
            this.panel2.Controls.Add(this.txbCorreo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txbNombre);
            this.panel2.Controls.Add(this.txbDominio);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(327, 176);
            this.panel2.TabIndex = 13;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(256, 146);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(56, 17);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "Activo";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // buttonTestRDS
            // 
            this.buttonTestRDS.Location = new System.Drawing.Point(131, 142);
            this.buttonTestRDS.Name = "buttonTestRDS";
            this.buttonTestRDS.Size = new System.Drawing.Size(119, 23);
            this.buttonTestRDS.TabIndex = 10;
            this.buttonTestRDS.Text = "Prueba de conexión";
            this.buttonTestRDS.UseVisualStyleBackColor = true;
            this.buttonTestRDS.Click += new System.EventHandler(this.ButtonTestRDS_Click);
            // 
            // txtRepresentanteLegal
            // 
            this.txtRepresentanteLegal.Location = new System.Drawing.Point(82, 116);
            this.txtRepresentanteLegal.MaxLength = 64;
            this.txtRepresentanteLegal.Name = "txtRepresentanteLegal";
            this.txtRepresentanteLegal.Size = new System.Drawing.Size(231, 20);
            this.txtRepresentanteLegal.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Rep. Legal:";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(327, 23);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(581, 481);
            this.propertyGrid1.TabIndex = 11;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 504);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(908, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Información";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(908, 23);
            this.ToolBar.TabIndex = 13;
            this.ToolBar.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_Guardar_Click);
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_Actualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_Cerrar_Click);
            // 
            // EmpresaConfiguracionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 526);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmpresaConfiguracionForm";
            this.Text = "Configuración Empresa";
            this.Load += new System.EventHandler(this.EmpresaConfiguracion_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbClave;
        private System.Windows.Forms.TextBox txbRFC;
        private System.Windows.Forms.TextBox txbNombre;
        private System.Windows.Forms.TextBox txbDominio;
        private System.Windows.Forms.TextBox txbCorreo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Button buttonTestRDS;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TextBox txtRepresentanteLegal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txbContenido;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonIzquierda;
        private System.Windows.Forms.Button buttonLimpiar;
        private System.Windows.Forms.Button buttonDerecha;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}