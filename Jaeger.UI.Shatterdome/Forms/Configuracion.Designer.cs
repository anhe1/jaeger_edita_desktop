﻿
namespace Jaeger.UI.Empresa.Forms {
    partial class Configuracion {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Configuracion));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblApellidoMaterno = new System.Windows.Forms.Label();
            this.txtRegimenCapital = new System.Windows.Forms.TextBox();
            this.lblApellidoPaterno = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblRFC = new System.Windows.Forms.Label();
            this.txtNombreComercial = new System.Windows.Forms.TextBox();
            this.grpDattosUbicacion = new System.Windows.Forms.GroupBox();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.lblReferencia = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.lblLocalidad = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.radLabel4 = new System.Windows.Forms.Label();
            this.txtCiudad = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.lblTipoVialidad = new System.Windows.Forms.Label();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.lblNumInterior = new System.Windows.Forms.Label();
            this.lblNumExterior = new System.Windows.Forms.Label();
            this.txtNumInterior = new System.Windows.Forms.TextBox();
            this.txtNumExterior = new System.Windows.Forms.TextBox();
            this.lblEntidad = new System.Windows.Forms.Label();
            this.lblNombreVialidad = new System.Windows.Forms.Label();
            this.txtCodigoPostal = new System.Windows.Forms.TextBox();
            this.lblColonia = new System.Windows.Forms.Label();
            this.lblMunicipio = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.lblCodigoPostal = new System.Windows.Forms.Label();
            this.txtNombreVialidad = new System.Windows.Forms.TextBox();
            this.txtEntidad = new System.Windows.Forms.TextBox();
            this.txtTipoVialidad = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.bGuardar = new System.Windows.Forms.Button();
            this.bCerrar = new System.Windows.Forms.Button();
            this.bPDF = new System.Windows.Forms.Button();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.txtCIF = new System.Windows.Forms.TextBox();
            this.radLabel1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtRegistroPatronal = new System.Windows.Forms.TextBox();
            this.lblRegimen = new System.Windows.Forms.Label();
            this.Regimen = new System.Windows.Forms.ComboBox();
            this.radLabel3 = new System.Windows.Forms.Label();
            this.radLabel2 = new System.Windows.Forms.Label();
            this.RegimenFiscal = new System.Windows.Forms.ComboBox();
            this.lblRegimenFiscal = new System.Windows.Forms.Label();
            this.txtCURP = new System.Windows.Forms.TextBox();
            this.Encabezado = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpDattosUbicacion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(556, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblApellidoMaterno
            // 
            this.lblApellidoMaterno.Location = new System.Drawing.Point(8, 92);
            this.lblApellidoMaterno.Name = "lblApellidoMaterno";
            this.lblApellidoMaterno.Size = new System.Drawing.Size(103, 18);
            this.lblApellidoMaterno.TabIndex = 12;
            this.lblApellidoMaterno.Text = "Nombre Comercial:";
            // 
            // txtRegimenCapital
            // 
            this.txtRegimenCapital.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegimenCapital.Location = new System.Drawing.Point(303, 39);
            this.txtRegimenCapital.Name = "txtRegimenCapital";
            this.txtRegimenCapital.Size = new System.Drawing.Size(117, 20);
            this.txtRegimenCapital.TabIndex = 3;
            this.txtRegimenCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoPaterno
            // 
            this.lblApellidoPaterno.Location = new System.Drawing.Point(303, 20);
            this.lblApellidoPaterno.Name = "lblApellidoPaterno";
            this.lblApellidoPaterno.Size = new System.Drawing.Size(104, 18);
            this.lblApellidoPaterno.TabIndex = 2;
            this.lblApellidoPaterno.Text = "Régimen de capital:";
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombre.Location = new System.Drawing.Point(8, 39);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(290, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(8, 20);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(157, 18);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Denominación o Razón Social:";
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(8, 66);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 6;
            this.lblRFC.Text = "RFC:";
            // 
            // txtNombreComercial
            // 
            this.txtNombreComercial.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreComercial.Location = new System.Drawing.Point(8, 117);
            this.txtNombreComercial.Name = "txtNombreComercial";
            this.txtNombreComercial.Size = new System.Drawing.Size(330, 20);
            this.txtNombreComercial.TabIndex = 13;
            // 
            // grpDattosUbicacion
            // 
            this.grpDattosUbicacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpDattosUbicacion.Controls.Add(this.txtReferencia);
            this.grpDattosUbicacion.Controls.Add(this.lblReferencia);
            this.grpDattosUbicacion.Controls.Add(this.txtLocalidad);
            this.grpDattosUbicacion.Controls.Add(this.lblLocalidad);
            this.grpDattosUbicacion.Controls.Add(this.txtTelefono);
            this.grpDattosUbicacion.Controls.Add(this.lblTelefono);
            this.grpDattosUbicacion.Controls.Add(this.radLabel4);
            this.grpDattosUbicacion.Controls.Add(this.txtCiudad);
            this.grpDattosUbicacion.Controls.Add(this.txtCorreo);
            this.grpDattosUbicacion.Controls.Add(this.lblTipoVialidad);
            this.grpDattosUbicacion.Controls.Add(this.lblCorreo);
            this.grpDattosUbicacion.Controls.Add(this.lblNumInterior);
            this.grpDattosUbicacion.Controls.Add(this.lblNumExterior);
            this.grpDattosUbicacion.Controls.Add(this.txtNumInterior);
            this.grpDattosUbicacion.Controls.Add(this.txtNumExterior);
            this.grpDattosUbicacion.Controls.Add(this.lblEntidad);
            this.grpDattosUbicacion.Controls.Add(this.lblNombreVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtCodigoPostal);
            this.grpDattosUbicacion.Controls.Add(this.lblColonia);
            this.grpDattosUbicacion.Controls.Add(this.lblMunicipio);
            this.grpDattosUbicacion.Controls.Add(this.txtColonia);
            this.grpDattosUbicacion.Controls.Add(this.lblCodigoPostal);
            this.grpDattosUbicacion.Controls.Add(this.txtNombreVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtEntidad);
            this.grpDattosUbicacion.Controls.Add(this.txtTipoVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtMunicipio);
            this.grpDattosUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDattosUbicacion.Location = new System.Drawing.Point(0, 198);
            this.grpDattosUbicacion.Name = "grpDattosUbicacion";
            this.grpDattosUbicacion.Size = new System.Drawing.Size(556, 180);
            this.grpDattosUbicacion.TabIndex = 2;
            this.grpDattosUbicacion.TabStop = false;
            this.grpDattosUbicacion.Text = "Datos de Ubicación (domicilio fiscal, vigente)";
            // 
            // txtReferencia
            // 
            this.txtReferencia.Location = new System.Drawing.Point(381, 149);
            this.txtReferencia.MaxLength = 40;
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(163, 20);
            this.txtReferencia.TabIndex = 25;
            // 
            // lblReferencia
            // 
            this.lblReferencia.Location = new System.Drawing.Point(314, 150);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(61, 18);
            this.lblReferencia.TabIndex = 24;
            this.lblReferencia.Text = "Referencia:";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocalidad.Location = new System.Drawing.Point(70, 149);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(241, 20);
            this.txtLocalidad.TabIndex = 23;
            // 
            // lblLocalidad
            // 
            this.lblLocalidad.Location = new System.Drawing.Point(8, 150);
            this.lblLocalidad.Name = "lblLocalidad";
            this.lblLocalidad.Size = new System.Drawing.Size(56, 18);
            this.lblLocalidad.TabIndex = 22;
            this.lblLocalidad.Text = "Localidad:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(372, 123);
            this.txtTelefono.MaxLength = 40;
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(172, 20);
            this.txtTelefono.TabIndex = 21;
            // 
            // lblTelefono
            // 
            this.lblTelefono.Location = new System.Drawing.Point(314, 124);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 18);
            this.lblTelefono.TabIndex = 20;
            this.lblTelefono.Text = "Teléfono:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(314, 98);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(44, 18);
            this.radLabel4.TabIndex = 16;
            this.radLabel4.Text = "Ciudad:";
            // 
            // txtCiudad
            // 
            this.txtCiudad.BackColor = System.Drawing.SystemColors.Window;
            this.txtCiudad.Location = new System.Drawing.Point(364, 97);
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.Size = new System.Drawing.Size(180, 20);
            this.txtCiudad.TabIndex = 17;
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.SystemColors.Window;
            this.txtCorreo.Location = new System.Drawing.Point(115, 123);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(196, 20);
            this.txtCorreo.TabIndex = 19;
            // 
            // lblTipoVialidad
            // 
            this.lblTipoVialidad.Location = new System.Drawing.Point(8, 22);
            this.lblTipoVialidad.Name = "lblTipoVialidad";
            this.lblTipoVialidad.Size = new System.Drawing.Size(88, 18);
            this.lblTipoVialidad.TabIndex = 0;
            this.lblTipoVialidad.Text = "Tipo de vialidad:";
            // 
            // lblCorreo
            // 
            this.lblCorreo.Location = new System.Drawing.Point(8, 124);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(101, 18);
            this.lblCorreo.TabIndex = 18;
            this.lblCorreo.Text = "Correo electrónico:";
            // 
            // lblNumInterior
            // 
            this.lblNumInterior.Location = new System.Drawing.Point(202, 46);
            this.lblNumInterior.Name = "lblNumInterior";
            this.lblNumInterior.Size = new System.Drawing.Size(75, 18);
            this.lblNumInterior.TabIndex = 6;
            this.lblNumInterior.Text = "Núm. interior:";
            // 
            // lblNumExterior
            // 
            this.lblNumExterior.Location = new System.Drawing.Point(8, 46);
            this.lblNumExterior.Name = "lblNumExterior";
            this.lblNumExterior.Size = new System.Drawing.Size(77, 18);
            this.lblNumExterior.TabIndex = 4;
            this.lblNumExterior.Text = "Núm. exterior:";
            // 
            // txtNumInterior
            // 
            this.txtNumInterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumInterior.Location = new System.Drawing.Point(283, 45);
            this.txtNumInterior.Name = "txtNumInterior";
            this.txtNumInterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumInterior.TabIndex = 7;
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumExterior.Location = new System.Drawing.Point(91, 45);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumExterior.TabIndex = 5;
            // 
            // lblEntidad
            // 
            this.lblEntidad.Location = new System.Drawing.Point(314, 72);
            this.lblEntidad.Name = "lblEntidad";
            this.lblEntidad.Size = new System.Drawing.Size(101, 18);
            this.lblEntidad.TabIndex = 12;
            this.lblEntidad.Text = "Entidad Federativa:";
            // 
            // lblNombreVialidad
            // 
            this.lblNombreVialidad.Location = new System.Drawing.Point(211, 22);
            this.lblNombreVialidad.Name = "lblNombreVialidad";
            this.lblNombreVialidad.Size = new System.Drawing.Size(119, 18);
            this.lblNombreVialidad.TabIndex = 2;
            this.lblNombreVialidad.Text = "Nombre de la vialidad:";
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodigoPostal.Location = new System.Drawing.Point(474, 45);
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.Size = new System.Drawing.Size(70, 20);
            this.txtCodigoPostal.TabIndex = 9;
            this.txtCodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblColonia
            // 
            this.lblColonia.Location = new System.Drawing.Point(8, 72);
            this.lblColonia.Name = "lblColonia";
            this.lblColonia.Size = new System.Drawing.Size(47, 18);
            this.lblColonia.TabIndex = 10;
            this.lblColonia.Text = "Colonia:";
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Location = new System.Drawing.Point(8, 98);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(126, 18);
            this.lblMunicipio.TabIndex = 14;
            this.lblMunicipio.Text = "Municipio o delegación:";
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.SystemColors.Window;
            this.txtColonia.Location = new System.Drawing.Point(58, 71);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(253, 20);
            this.txtColonia.TabIndex = 11;
            // 
            // lblCodigoPostal
            // 
            this.lblCodigoPostal.Location = new System.Drawing.Point(393, 46);
            this.lblCodigoPostal.Name = "lblCodigoPostal";
            this.lblCodigoPostal.Size = new System.Drawing.Size(78, 18);
            this.lblCodigoPostal.TabIndex = 8;
            this.lblCodigoPostal.Text = "Codigo Postal:";
            // 
            // txtNombreVialidad
            // 
            this.txtNombreVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreVialidad.Location = new System.Drawing.Point(340, 21);
            this.txtNombreVialidad.Name = "txtNombreVialidad";
            this.txtNombreVialidad.Size = new System.Drawing.Size(204, 20);
            this.txtNombreVialidad.TabIndex = 3;
            // 
            // txtEntidad
            // 
            this.txtEntidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtEntidad.Location = new System.Drawing.Point(419, 71);
            this.txtEntidad.Name = "txtEntidad";
            this.txtEntidad.Size = new System.Drawing.Size(125, 20);
            this.txtEntidad.TabIndex = 13;
            // 
            // txtTipoVialidad
            // 
            this.txtTipoVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoVialidad.Location = new System.Drawing.Point(102, 21);
            this.txtTipoVialidad.Name = "txtTipoVialidad";
            this.txtTipoVialidad.Size = new System.Drawing.Size(103, 20);
            this.txtTipoVialidad.TabIndex = 1;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipio.Location = new System.Drawing.Point(140, 97);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(171, 20);
            this.txtMunicipio.TabIndex = 15;
            // 
            // bGuardar
            // 
            this.bGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGuardar.Location = new System.Drawing.Point(388, 389);
            this.bGuardar.Name = "bGuardar";
            this.bGuardar.Size = new System.Drawing.Size(75, 23);
            this.bGuardar.TabIndex = 4;
            this.bGuardar.Text = "Guardar";
            this.bGuardar.Click += new System.EventHandler(this.Button_Guardar_Click);
            // 
            // bCerrar
            // 
            this.bCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCerrar.Location = new System.Drawing.Point(469, 389);
            this.bCerrar.Name = "bCerrar";
            this.bCerrar.Size = new System.Drawing.Size(75, 23);
            this.bCerrar.TabIndex = 5;
            this.bCerrar.Text = "Cerrar";
            this.bCerrar.Click += new System.EventHandler(this.Button_Cerrar_Click);
            // 
            // bPDF
            // 
            this.bPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bPDF.Location = new System.Drawing.Point(307, 389);
            this.bPDF.Name = "bPDF";
            this.bPDF.Size = new System.Drawing.Size(75, 23);
            this.bPDF.TabIndex = 3;
            this.bPDF.Text = "CIF";
            this.bPDF.Click += new System.EventHandler(this.Button_CIF_Click);
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(42, 65);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(92, 20);
            this.txtRFC.TabIndex = 7;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCIF
            // 
            this.txtCIF.BackColor = System.Drawing.SystemColors.Window;
            this.txtCIF.Location = new System.Drawing.Point(447, 91);
            this.txtCIF.Name = "txtCIF";
            this.txtCIF.Size = new System.Drawing.Size(97, 20);
            this.txtCIF.TabIndex = 15;
            this.txtCIF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(137, 66);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(37, 18);
            this.radLabel1.TabIndex = 8;
            this.radLabel1.Text = "CURP:";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.txtRegistroPatronal);
            this.groupBox1.Controls.Add(this.lblRegimen);
            this.groupBox1.Controls.Add(this.Regimen);
            this.groupBox1.Controls.Add(this.txtNombreComercial);
            this.groupBox1.Controls.Add(this.radLabel3);
            this.groupBox1.Controls.Add(this.lblNombre);
            this.groupBox1.Controls.Add(this.txtCIF);
            this.groupBox1.Controls.Add(this.radLabel2);
            this.groupBox1.Controls.Add(this.RegimenFiscal);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.lblRegimenFiscal);
            this.groupBox1.Controls.Add(this.lblApellidoPaterno);
            this.groupBox1.Controls.Add(this.txtRegimenCapital);
            this.groupBox1.Controls.Add(this.lblApellidoMaterno);
            this.groupBox1.Controls.Add(this.txtRFC);
            this.groupBox1.Controls.Add(this.lblRFC);
            this.groupBox1.Controls.Add(this.txtCURP);
            this.groupBox1.Controls.Add(this.radLabel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(556, 148);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información";
            // 
            // txtRegistroPatronal
            // 
            this.txtRegistroPatronal.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegistroPatronal.Location = new System.Drawing.Point(447, 117);
            this.txtRegistroPatronal.Name = "txtRegistroPatronal";
            this.txtRegistroPatronal.Size = new System.Drawing.Size(97, 20);
            this.txtRegistroPatronal.TabIndex = 17;
            // 
            // lblRegimen
            // 
            this.lblRegimen.Location = new System.Drawing.Point(426, 20);
            this.lblRegimen.Name = "lblRegimen";
            this.lblRegimen.Size = new System.Drawing.Size(48, 18);
            this.lblRegimen.TabIndex = 4;
            this.lblRegimen.Text = "Persona:";
            // 
            // Regimen
            // 
            this.Regimen.Location = new System.Drawing.Point(426, 39);
            this.Regimen.Name = "Regimen";
            this.Regimen.Size = new System.Drawing.Size(118, 21);
            this.Regimen.TabIndex = 5;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(349, 118);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(95, 18);
            this.radLabel3.TabIndex = 16;
            this.radLabel3.Text = "Registro Patronal:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(404, 92);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(24, 18);
            this.radLabel2.TabIndex = 14;
            this.radLabel2.Text = "CIF:";
            // 
            // RegimenFiscal
            // 
            this.RegimenFiscal.Location = new System.Drawing.Point(423, 65);
            this.RegimenFiscal.Name = "RegimenFiscal";
            this.RegimenFiscal.Size = new System.Drawing.Size(121, 21);
            this.RegimenFiscal.TabIndex = 18;
            // 
            // lblRegimenFiscal
            // 
            this.lblRegimenFiscal.Location = new System.Drawing.Point(303, 66);
            this.lblRegimenFiscal.Name = "lblRegimenFiscal";
            this.lblRegimenFiscal.Size = new System.Drawing.Size(83, 18);
            this.lblRegimenFiscal.TabIndex = 10;
            this.lblRegimenFiscal.Text = "Régimen Fiscal:";
            // 
            // txtCURP
            // 
            this.txtCURP.BackColor = System.Drawing.SystemColors.Window;
            this.txtCURP.Location = new System.Drawing.Point(180, 65);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.Size = new System.Drawing.Size(120, 20);
            this.txtCURP.TabIndex = 9;
            this.txtCURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Location = new System.Drawing.Point(12, 12);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(108, 18);
            this.Encabezado.TabIndex = 0;
            this.Encabezado.Text = "Información General";
            // 
            // Configuracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 424);
            this.Controls.Add(this.Encabezado);
            this.Controls.Add(this.grpDattosUbicacion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bPDF);
            this.Controls.Add(this.bCerrar);
            this.Controls.Add(this.bGuardar);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Configuracion";
            this.ShowInTaskbar = false;
            this.Text = "Configuración del emisor";
            this.Load += new System.EventHandler(this.Configuracion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpDattosUbicacion.ResumeLayout(false);
            this.grpDattosUbicacion.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblApellidoMaterno;
        private System.Windows.Forms.TextBox txtRegimenCapital;
        private System.Windows.Forms.Label lblApellidoPaterno;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblRFC;
        private System.Windows.Forms.TextBox txtNombreComercial;
        private System.Windows.Forms.GroupBox grpDattosUbicacion;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Label lblTipoVialidad;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.Label lblNumInterior;
        private System.Windows.Forms.Label lblNumExterior;
        private System.Windows.Forms.TextBox txtNumInterior;
        private System.Windows.Forms.TextBox txtNumExterior;
        private System.Windows.Forms.Label lblEntidad;
        private System.Windows.Forms.Label lblNombreVialidad;
        private System.Windows.Forms.TextBox txtCodigoPostal;
        private System.Windows.Forms.Label lblColonia;
        private System.Windows.Forms.Label lblMunicipio;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.Label lblCodigoPostal;
        private System.Windows.Forms.TextBox txtNombreVialidad;
        private System.Windows.Forms.TextBox txtEntidad;
        private System.Windows.Forms.TextBox txtTipoVialidad;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Button bGuardar;
        private System.Windows.Forms.Button bCerrar;
        private System.Windows.Forms.Button bPDF;
        private System.Windows.Forms.TextBox txtRFC;
        private System.Windows.Forms.TextBox txtCIF;
        private System.Windows.Forms.Label radLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCURP;
        private System.Windows.Forms.Label radLabel2;
        private System.Windows.Forms.TextBox txtRegistroPatronal;
        private System.Windows.Forms.Label radLabel3;
        public System.Windows.Forms.ComboBox RegimenFiscal;
        public System.Windows.Forms.Label lblRegimenFiscal;
        public System.Windows.Forms.ComboBox Regimen;
        public System.Windows.Forms.Label lblRegimen;
        private System.Windows.Forms.Label radLabel4;
        private System.Windows.Forms.TextBox txtCiudad;
        public System.Windows.Forms.TextBox txtTelefono;
        public System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.Label Encabezado;
        public System.Windows.Forms.TextBox txtReferencia;
        public System.Windows.Forms.Label lblReferencia;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.Label lblLocalidad;
    }
}