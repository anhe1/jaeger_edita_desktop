﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Shatterdome.Empresas;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Shatterdome.Entities;

namespace Jaeger.UI.Shatterdome.Forms {
    public partial class EmpresaConfiguracionForm : Form {
        protected IShatterdomeService service;
        protected ShatterdomeDetailModel shatterdome;
        private bool correcto = false;

        public EmpresaConfiguracionForm(ShatterdomeDetailModel shatterdome) {
            InitializeComponent();
            this.shatterdome = shatterdome;
        }

        private void EmpresaConfiguracion_Load(object sender, EventArgs e) {
            this.service = new ShatterdomeService();
            this.ToolBar.Actualizar.PerformClick();
        }

        private void CreateBinding() {
            this.checkBox1.DataBindings.Clear();
            this.checkBox1.DataBindings.Add("Checked", this.shatterdome, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbClave.DataBindings.Clear();
            this.txbClave.DataBindings.Add("Text", this.shatterdome, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbRFC.DataBindings.Clear();
            this.txbRFC.DataBindings.Add("Text", this.shatterdome, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbNombre.DataBindings.Clear();
            this.txbNombre.DataBindings.Add("Text", this.shatterdome, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbDominio.DataBindings.Clear();
            this.txbDominio.DataBindings.Add("Text", this.shatterdome, "Dominio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbCorreo.DataBindings.Clear();
            this.txbCorreo.DataBindings.Add("Text", this.shatterdome, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRepresentanteLegal.DataBindings.Clear();
            this.txtRepresentanteLegal.DataBindings.Add("Text", this.shatterdome, "RepresentanteLegal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.propertyGrid1.SelectedObject = this.shatterdome.Synapsis;

        }

        private void ToolBar_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            
        }

        private void ToolBar_Actualizar_Click(object sender, EventArgs e) {
            if (this.shatterdome == null)
                this.shatterdome = new ShatterdomeDetailModel();
            this.CreateBinding();
        }

        private void Guardar() {
            this.service.Save(this.shatterdome);
        }

        private void ToolBar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ButtonTestRDS_Click(object sender, EventArgs e) {
            using (var _espera = new WaitingForm(this.Probar)) {
                _espera.Text = "Comprobando configuración ...";
                _espera.ShowDialog(this);
            }

            if (this.correcto == true) {
                MessageBox.Show(this, "Conexión con la base de datos correcta.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else {
                MessageBox.Show(this, "No es posible conectar con el host remoto.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            this.correcto = false;
        }

        private void buttonLimpiar_Click(object sender, EventArgs e) {

        }

        private void buttonDerecha_Click(object sender, EventArgs e) {
            this.txbContenido.Text = this.shatterdome.Synapsis.Json();
        }

        private void buttonIzquierda_Click(object sender, EventArgs e) {
            this.txbContenido.Text = this.shatterdome.Synapsis.Json();
        }

        private void ToolBarButtonMapeo_Click(object sender, EventArgs e) {
            //using (var fbd = new FolderBrowserDialog()) {
            //    DialogResult result = fbd.ShowDialog();

            //    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath)) {
            //        this.ToolBarButtonMapeo.Tag  = fbd.SelectedPath;
            //        using (var espera = new WaitingForm(this.Mapeo)) {
            //            espera.Text = "creando clases, espere un momento...";
            //            espera.ShowDialog(this);
            //        }
            //        if (this.ToolBarButtonMapeo.Tag != null) {
            //            MessageBox.Show((string)this.ToolBarButtonMapeo.Tag);
            //        }
            //    }
            //}
        }

        private void Mapeo() {
            //string ruta = (string)this.ToolBarButtonMapeo.Tag;
            //if (System.IO.Directory.Exists(ruta)) {
            //    var p = new MantenimientoService();
            //    p.Prueba(this.shatterdome.Synapsis.BaseDatos.Edita, ruta);
            //    this.ToolBarButtonMapeo.Tag = "Listo!";
            //}
            //else {
            //    this.ToolBarButtonMapeo.Tag = "No existe la carpeta de salida";
            //}
        }

        private void Probar() {
            this.correcto = this.service.TestService(this.shatterdome);
        }
    }
}
