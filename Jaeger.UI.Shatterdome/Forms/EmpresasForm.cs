﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Domain.Shatterdome.Entities;
using Jaeger.Aplication.Shatterdome.Empresas;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Shatterdome.Forms {
    public partial class EmpresasForm : Form {
        protected IShatterdomeService service;
        protected BindingList<ShatterdomeDetailModel> data;

        public EmpresasForm() {
            InitializeComponent();
        }

        private void EmpresasForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridData.DataGridCommon();
            this.service = new ShatterdomeService();
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as ShatterdomeDetailModel;
                if (seleccionado != null) {
                    var editar = new EmpresaConfiguracionForm(seleccionado) { MdiParent = this.ParentForm, Text = string.Format("{0} [{1}]", "Empresa: ", seleccionado.RFC) };
                    editar.Show();
                }
            }
        }

        private void ToolBarButtonEliminar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as ShatterdomeDetailModel;
                if (seleccionado != null) {
                    if (MessageBox.Show(this, "¿Esta seguro?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                    this.service.Delete(seleccionado);
                }
            }
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            var nuevo = new EmpresaConfiguracionForm(null) { MdiParent = this.ParentForm, Text = "Nueva empresa" };
            nuevo.Show();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Actualizar)) {
                espera.Text = "Cargando ...";
                espera.ShowDialog(this);
            }
            this.gridData.DataSource = this.data;
        }

        private void Actualizar() {
            this.data = new BindingList<ShatterdomeDetailModel>(this.service.GetList());
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
