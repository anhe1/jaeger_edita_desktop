﻿namespace Jaeger.UI.Shatterdome.Forms {
    partial class EmpresasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmpresasForm));
            this.gridData = new System.Windows.Forms.DataGridView();
            this.gcId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcActivo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gcRegimenFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcClave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcDominio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcCorreo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcFechaNuevo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcCreo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcFechaModifica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcModifica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.AllowUserToAddRows = false;
            this.gridData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcId,
            this.gcActivo,
            this.gcRegimenFiscal,
            this.gcClave,
            this.gcRFC,
            this.gcNombre,
            this.gcDominio,
            this.gcCorreo,
            this.gcFechaNuevo,
            this.gcCreo,
            this.gcFechaModifica,
            this.gcModifica});
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 25);
            this.gridData.Name = "gridData";
            this.gridData.RowHeadersVisible = false;
            this.gridData.Size = new System.Drawing.Size(1009, 489);
            this.gridData.TabIndex = 1;
            // 
            // gcId
            // 
            this.gcId.DataPropertyName = "Id";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "#00000";
            this.gcId.DefaultCellStyle = dataGridViewCellStyle2;
            this.gcId.FillWeight = 50F;
            this.gcId.HeaderText = "ID";
            this.gcId.Name = "gcId";
            this.gcId.ReadOnly = true;
            this.gcId.Width = 50;
            // 
            // gcActivo
            // 
            this.gcActivo.DataPropertyName = "Activo";
            this.gcActivo.FillWeight = 50F;
            this.gcActivo.HeaderText = "A";
            this.gcActivo.Name = "gcActivo";
            this.gcActivo.ReadOnly = true;
            this.gcActivo.Width = 50;
            // 
            // gcRegimenFiscal
            // 
            this.gcRegimenFiscal.DataPropertyName = "RegimenFiscal";
            this.gcRegimenFiscal.FillWeight = 50F;
            this.gcRegimenFiscal.HeaderText = "Regimen Fiscal";
            this.gcRegimenFiscal.Name = "gcRegimenFiscal";
            this.gcRegimenFiscal.ReadOnly = true;
            this.gcRegimenFiscal.Width = 50;
            // 
            // gcClave
            // 
            this.gcClave.DataPropertyName = "Clave";
            this.gcClave.HeaderText = "Clave";
            this.gcClave.Name = "gcClave";
            this.gcClave.ReadOnly = true;
            // 
            // gcRFC
            // 
            this.gcRFC.DataPropertyName = "RFC";
            this.gcRFC.HeaderText = "RFC";
            this.gcRFC.Name = "gcRFC";
            this.gcRFC.ReadOnly = true;
            // 
            // gcNombre
            // 
            this.gcNombre.DataPropertyName = "Nombre";
            this.gcNombre.HeaderText = "Nombre";
            this.gcNombre.Name = "gcNombre";
            this.gcNombre.ReadOnly = true;
            this.gcNombre.Width = 200;
            // 
            // gcDominio
            // 
            this.gcDominio.DataPropertyName = "Dominio";
            this.gcDominio.HeaderText = "Dominio";
            this.gcDominio.Name = "gcDominio";
            this.gcDominio.ReadOnly = true;
            this.gcDominio.Width = 175;
            // 
            // gcCorreo
            // 
            this.gcCorreo.DataPropertyName = "Correo";
            this.gcCorreo.HeaderText = "Correo";
            this.gcCorreo.Name = "gcCorreo";
            this.gcCorreo.ReadOnly = true;
            this.gcCorreo.Width = 175;
            // 
            // gcFechaNuevo
            // 
            this.gcFechaNuevo.DataPropertyName = "FechaNuevo";
            this.gcFechaNuevo.HeaderText = "Fec. Sist.";
            this.gcFechaNuevo.Name = "gcFechaNuevo";
            this.gcFechaNuevo.ReadOnly = true;
            this.gcFechaNuevo.Width = 65;
            // 
            // gcCreo
            // 
            this.gcCreo.DataPropertyName = "Creo";
            this.gcCreo.HeaderText = "Creó";
            this.gcCreo.Name = "gcCreo";
            this.gcCreo.ReadOnly = true;
            this.gcCreo.Width = 60;
            // 
            // gcFechaModifica
            // 
            this.gcFechaModifica.DataPropertyName = "Fec. Mod.";
            this.gcFechaModifica.HeaderText = "Fec. Modifica";
            this.gcFechaModifica.Name = "gcFechaModifica";
            this.gcFechaModifica.ReadOnly = true;
            this.gcFechaModifica.Width = 65;
            // 
            // gcModifica
            // 
            this.gcModifica.DataPropertyName = "Modifica";
            this.gcModifica.HeaderText = "Modifica";
            this.gcModifica.Name = "gcModifica";
            this.gcModifica.ReadOnly = true;
            this.gcModifica.Width = 60;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Empresa";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(1009, 25);
            this.ToolBar.TabIndex = 2;
            this.ToolBar.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonNuevo_Click);
            this.ToolBar.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonEditar_Click);
            this.ToolBar.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonEliminar_Click);
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonActualizar_Click);
            // 
            // EmpresasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 514);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmpresasForm";
            this.ShowInTaskbar = false;
            this.Text = "Empresas Registradas";
            this.Load += new System.EventHandler(this.EmpresasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView gridData;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gcActivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcRegimenFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcClave;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcDominio;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcCorreo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcFechaNuevo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcCreo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcFechaModifica;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcModifica;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}