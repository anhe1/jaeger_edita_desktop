﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Empresa;
using Jaeger.SAT.CedulaFiscal.Entities;
using Jaeger.SAT.CedulaFiscal.Interfaces;
using Jaeger.SAT.CedulaFiscal;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Empresa.Forms {
    public partial class Configuracion : Form {
        ConfiguracionService _Service = new ConfiguracionService();
        Domain.Empresa.Entities.EmpresaDetalle _Configuracion;
        protected internal IResponseCedula _Response;
        protected internal ICedulaFiscal _CedulaFiscal;
        protected internal IService _CedulaFiscalService;
        protected internal IRequestCedula _Request;

        public Configuracion(UIMenuElement element) {
            InitializeComponent();
        }

        private void Configuracion_Load(object sender, EventArgs e) {
            this._Configuracion = this._Service.GetEmpresa();
            this.CreateBinding();
        }

        private void Button_Guardar_Click(object sender, EventArgs e) {
            this._Service.Save(this._Configuracion.Empresa.Build());
            this._Service.Save(this._Configuracion.DomicilioFiscal.Build());
        }

        private void Button_CIF_Click(object sender, EventArgs e) {
            var fileOpen = new OpenFileDialog() { Filter = "*.pdf|*.PDF|*.png|*.PNG|*.jpg|*.JPG" };
            if (fileOpen.ShowDialog(this) == DialogResult.OK) {
                if (Path.GetExtension(fileOpen.FileName) == ".png") {
                    var image = new Bitmap(fileOpen.FileName);
                    if (image != null) {
                        var d0 = new ServiceQR();
                        var url = d0.GetByQR(fileOpen.FileName);
                        if (url.Message.Contains("https://siat.sat.gob.mx/app/qr/faces/pages/mobile/validadorqr.jsf?D1=10&D2=1&D3=19120271393_TESG830215FF9")) {
                            IRequestCedula request = RequestCedula.Create().WithURL(url.Message).Build();
                            this._Response = this._CedulaFiscalService.Execute(request);
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                } else if (Path.GetExtension(fileOpen.FileName) == ".pdf") {
                    var r = PDFExtractorService.GetData(fileOpen.FileName);
                    if (r != null) {
                        if (r.Count > 0) {
                            this._Request = RequestCedula.Create().WithRFC(r["rfc"]).WithId(r["idcif"]).Build();
                            using (var espera = new WaitingForm(this.Buscar)) {
                                espera.Text = "Buscando...";
                                espera.ShowDialog(this);
                            }
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    } else {
                        MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                } else {
                    MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void Button_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Buscar() {
            if (this._CedulaFiscalService == null) {
                this._CedulaFiscalService = new Jaeger.SAT.CedulaFiscal.Service();
            }
            
            this._Response = this._CedulaFiscalService.Execute(this._Request);
            if (this._Response.IsValida) {
                this._CedulaFiscal = this._Response.CedulaFiscal;
                this.Autoridad();
                //this.CreateBinding();
            } else {
                MessageBox.Show(this, this._Response.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void CreateBinding() {
            this.txtRFC.DataBindings.Add("Text", this._Configuracion.Empresa, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCIF.DataBindings.Add("Text", this._Configuracion.Empresa, "CIF", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCURP.DataBindings.Add("Text", this._Configuracion.Empresa, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombre.DataBindings.Add("Text", this._Configuracion.Empresa, "RazonSocial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRegimenCapital.DataBindings.Add("Text", this._Configuracion.Empresa, "SociedadCapital", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombreComercial.DataBindings.Add("Text", this._Configuracion.Empresa, "NombreComercial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRegistroPatronal.DataBindings.Add("Text", this._Configuracion.Empresa, "RegistroPatronal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Regimen.DataBindings.Add("Text", this._Configuracion.Empresa, "TipoPersona", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RegimenFiscal.DataBindings.Add("Text", this._Configuracion.Empresa, "RegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txtTipoVialidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "TipoVialidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombreVialidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "NombreVialidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNumExterior.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "NumExterior", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNumInterior.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "NumInterior", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCodigoPostal.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "CodigoPostal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtColonia.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Colonia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtEntidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "EntidadFederativa", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtMunicipio.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "MunicipioDelegacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCiudad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Ciudad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtTelefono.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCorreo.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtReferencia.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtLocalidad.DataBindings.Add("Text", this._Configuracion.DomicilioFiscal, "Localidad", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Autoridad() {
            if (this._CedulaFiscal != null) {
                if (this._CedulaFiscal.TipoPersona == CedulaFiscal.TipoPersonaEnum.Moral) {
                    this._Configuracion.Empresa.RFC = this._CedulaFiscal.Moral.RFC;
                    this._Configuracion.Empresa.RazonSocial = this._CedulaFiscal.Moral.Nombre;
                    this._Configuracion.Empresa.NombreComercial = this._Configuracion.Empresa.RazonSocial + " " + this._CedulaFiscal.Moral.RegimenCapital;
                    this._Configuracion.Empresa.SociedadCapital = this._CedulaFiscal.Moral.RegimenCapital;
                    this._Configuracion.DomicilioFiscal.CodigoPostal = this._CedulaFiscal.Moral.DomicilioFiscal.CodigoPostal;
                    this._Configuracion.DomicilioFiscal.NombreVialidad = this._CedulaFiscal.Moral.DomicilioFiscal.NombreVialidad;
                    this._Configuracion.DomicilioFiscal.TipoVialidad = this._CedulaFiscal.Moral.DomicilioFiscal.TipoVialidad;
                    this._Configuracion.DomicilioFiscal.NumExterior = this._CedulaFiscal.Moral.DomicilioFiscal.NumExterior;
                    this._Configuracion.DomicilioFiscal.NumInterior = this._CedulaFiscal.Moral.DomicilioFiscal.NumInterior;
                    this._Configuracion.DomicilioFiscal.MunicipioDelegacion = this._CedulaFiscal.Moral.DomicilioFiscal.MunicipioDelegacion;
                    this._Configuracion.DomicilioFiscal.Colonia = this._CedulaFiscal.Moral.DomicilioFiscal.Colonia;
                    this._Configuracion.DomicilioFiscal.Correo = this._CedulaFiscal.Moral.DomicilioFiscal.Correo;
                    this._Configuracion.DomicilioFiscal.EntidadFederativa = this._CedulaFiscal.Moral.DomicilioFiscal.EntidadFederativa;
                    this._Configuracion.Empresa.CIF = this._CedulaFiscal.IdCIF;
                } else if (_Response.CedulaFiscal.TipoPersona == CedulaFiscal.TipoPersonaEnum.Fisica) {
                    
                    
                    
                }
            }
        }

        private bool Verificacion() {
            return true;
        }
    }
}
