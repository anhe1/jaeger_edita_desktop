﻿
namespace Jaeger.UI.Shatterdome.Forms {
    partial class ArchivoINIForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArchivoINIForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolBarButtonArchivo = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolBarButtonAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarButtonGuardar = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarButtonGuardarComo = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBarButtonActualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonEncriptar = new System.Windows.Forms.ToolStripButton();
            this.ToolLabelPassword = new System.Windows.Forms.ToolStripLabel();
            this.ToolTextBoxPassword = new System.Windows.Forms.ToolStripTextBox();
            this.ToolBarButtonCerrar = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxContenido = new System.Windows.Forms.TextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.ToolBarButtonEliminar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonSerializar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonDesSerializar = new System.Windows.Forms.ToolStripButton();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonArchivo,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonEncriptar,
            this.ToolLabelPassword,
            this.ToolTextBoxPassword,
            this.ToolBarButtonCerrar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolBarButtonArchivo
            // 
            this.ToolBarButtonArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonAbrir,
            this.ToolBarButtonGuardar,
            this.ToolBarButtonGuardarComo});
            this.ToolBarButtonArchivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonArchivo.Name = "ToolBarButtonArchivo";
            this.ToolBarButtonArchivo.Size = new System.Drawing.Size(61, 22);
            this.ToolBarButtonArchivo.Text = "Archivo";
            // 
            // ToolBarButtonAbrir
            // 
            this.ToolBarButtonAbrir.Name = "ToolBarButtonAbrir";
            this.ToolBarButtonAbrir.Size = new System.Drawing.Size(162, 22);
            this.ToolBarButtonAbrir.Text = "Abrir";
            this.ToolBarButtonAbrir.Click += new System.EventHandler(this.ToolBarButtonAbrir_Click);
            // 
            // ToolBarButtonGuardar
            // 
            this.ToolBarButtonGuardar.Name = "ToolBarButtonGuardar";
            this.ToolBarButtonGuardar.Size = new System.Drawing.Size(162, 22);
            this.ToolBarButtonGuardar.Text = "Guardar";
            this.ToolBarButtonGuardar.Click += new System.EventHandler(this.ToolBarButtonGuardar_Click);
            // 
            // ToolBarButtonGuardarComo
            // 
            this.ToolBarButtonGuardarComo.Name = "ToolBarButtonGuardarComo";
            this.ToolBarButtonGuardarComo.Size = new System.Drawing.Size(162, 22);
            this.ToolBarButtonGuardarComo.Text = "Guardar como ...";
            this.ToolBarButtonGuardarComo.Click += new System.EventHandler(this.ToolBarButtonGuardarComo_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Size = new System.Drawing.Size(63, 22);
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonEncriptar
            // 
            this.ToolBarButtonEncriptar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonEncriptar.Name = "ToolBarButtonEncriptar";
            this.ToolBarButtonEncriptar.Size = new System.Drawing.Size(58, 22);
            this.ToolBarButtonEncriptar.Text = "Encriptar";
            this.ToolBarButtonEncriptar.Click += new System.EventHandler(this.ToolBarButtonEncriptar_Click);
            // 
            // ToolLabelPassword
            // 
            this.ToolLabelPassword.Name = "ToolLabelPassword";
            this.ToolLabelPassword.Size = new System.Drawing.Size(67, 22);
            this.ToolLabelPassword.Text = "Contraseña";
            // 
            // ToolTextBoxPassword
            // 
            this.ToolTextBoxPassword.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolTextBoxPassword.Name = "ToolTextBoxPassword";
            this.ToolTextBoxPassword.Size = new System.Drawing.Size(100, 25);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Size = new System.Drawing.Size(43, 22);
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ToolStripStatusLabel
            // 
            this.ToolStripStatusLabel.Name = "ToolStripStatusLabel";
            this.ToolStripStatusLabel.Size = new System.Drawing.Size(16, 17);
            this.ToolStripStatusLabel.Text = "...";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.propertyGrid1);
            this.splitContainer1.Size = new System.Drawing.Size(800, 403);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxContenido);
            this.panel1.Controls.Add(this.toolStrip2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(266, 403);
            this.panel1.TabIndex = 2;
            // 
            // textBoxContenido
            // 
            this.textBoxContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxContenido.Location = new System.Drawing.Point(0, 0);
            this.textBoxContenido.Multiline = true;
            this.textBoxContenido.Name = "textBoxContenido";
            this.textBoxContenido.Size = new System.Drawing.Size(242, 403);
            this.textBoxContenido.TabIndex = 1;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonEliminar,
            this.ToolBarButtonSerializar,
            this.ToolBarButtonDesSerializar});
            this.toolStrip2.Location = new System.Drawing.Point(242, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(24, 403);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // ToolBarButtonEliminar
            // 
            this.ToolBarButtonEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolBarButtonEliminar.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonEliminar.Image")));
            this.ToolBarButtonEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonEliminar.Name = "ToolBarButtonEliminar";
            this.ToolBarButtonEliminar.Size = new System.Drawing.Size(21, 20);
            this.ToolBarButtonEliminar.Text = "toolStripButton1";
            // 
            // ToolBarButtonSerializar
            // 
            this.ToolBarButtonSerializar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolBarButtonSerializar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonSerializar.Name = "ToolBarButtonSerializar";
            this.ToolBarButtonSerializar.Size = new System.Drawing.Size(21, 4);
            this.ToolBarButtonSerializar.Text = "toolStripButton2";
            this.ToolBarButtonSerializar.Click += new System.EventHandler(this.ToolBarButtonSerializar_Click);
            // 
            // ToolBarButtonDesSerializar
            // 
            this.ToolBarButtonDesSerializar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolBarButtonDesSerializar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonDesSerializar.Name = "ToolBarButtonDesSerializar";
            this.ToolBarButtonDesSerializar.Size = new System.Drawing.Size(21, 4);
            this.ToolBarButtonDesSerializar.Text = "toolStripButton3";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(530, 403);
            this.propertyGrid1.TabIndex = 0;
            // 
            // ArchivoINIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ArchivoINIForm";
            this.Text = "Archivo INI";
            this.Load += new System.EventHandler(this.ArchivoINIForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel;
        private System.Windows.Forms.ToolStripDropDownButton ToolBarButtonArchivo;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButtonAbrir;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButtonGuardar;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButtonGuardarComo;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ToolStripButton ToolBarButtonActualizar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonCerrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxContenido;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton ToolBarButtonEliminar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonSerializar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonDesSerializar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonEncriptar;
        private System.Windows.Forms.ToolStripLabel ToolLabelPassword;
        private System.Windows.Forms.ToolStripTextBox ToolTextBoxPassword;
    }
}