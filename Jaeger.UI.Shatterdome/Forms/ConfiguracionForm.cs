﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Shatterdome.Empresas;
using Jaeger.Domain.Shatterdome.Entities;

namespace Jaeger.UI.Shatterdome.Forms {
    public partial class ConfiguracionForm : Form {

        private LocalSynapsisService service;

        public ConfiguracionForm() {
            InitializeComponent();
        }

        private void ViewConfiguracion_Load(object sender, EventArgs e) {
            this.service = new LocalSynapsisService();
            if (!this.service.Exists()) {
                MessageBox.Show(this, "No se encontro el archivo de configuración local. Se creo una instancia nueva.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (ConfigService.Synapsis == null) {
                    ConfigService.Synapsis = new ShatterdomeSynapsis();
                }
            }
            this.propertyGrid1.SelectedObject = ConfigService.Synapsis.Configuracion;
        }

        private void ButtonGuardar_Click(object sender, EventArgs e) {
            this.service.Save();
            MessageBox.Show(this, "Es necesario reiniciar para aplicar la configuración", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
