﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Jaeger.Aplication.Shatterdome.Empresas;

namespace Jaeger.UI.Shatterdome.Forms {
    public partial class GeneradorPasswordForm : Form {
        private IPasswordGeneratorService generador;

        public GeneradorPasswordForm() {
            InitializeComponent();
        }

        private void GeneradorPasswordForm_Load(object sender, EventArgs e) {
            this.generador = new PasswordGeneratorService();
        }

        private void Generar_Click(object sender, EventArgs e) {
            AvisoLabel.Text = "-";
            this.generador.ConsecutiveCharacters = CaracteresConsecutivosCheck.Checked;
            this.generador.ExcludeSymbols = ExcluirSimbolosCheck.Checked;
            this.generador.RepeatCharacters = RepetirCaracteresCheck.Checked;
            this.generador.Exclusions = ExcluirCaracteresText.Text;
            this.generador.Minimum = int.Parse(LongitudMinText.Text);
            this.generador.Maximum = int.Parse(LongitudMaxText.Text) + 1;
            this.PasswordText.Text = this.generador.Generate();

            using (MD5 md5Hash = MD5.Create()) {
                this.MD5Text.Text = GetMd5Hash(md5Hash, this.PasswordText.Text);
                if (VerifyMd5Hash(md5Hash, this.PasswordText.Text, this.MD5Text.Text))
                    AvisoLabel.Text = "The hashes are the same.";
                else
                    AvisoLabel.Text = "The hashes are not same.";
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private string GetMd5Hash(MD5 md5Hash, string input) {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private bool VerifyMd5Hash(MD5 md5Hash, string input, string hash) {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
                return true;
            else
                return false;
        }

        private void Password_TextChanged(object sender, EventArgs e) {
            this.NumCaracteresLabel.Text = string.Format("Caracteres: {0}", PasswordText.Text.Length.ToString());
        }
    }
}
