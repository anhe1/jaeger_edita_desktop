﻿using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.Builder {
    public class ProductoModeloBuilder : IProductoModeloBuilder, IProductoModeloAlmacenBuilder, IProductoModeloTipoBuilder, IProductoModeloDescripcionBuilder {
        protected internal ProductoServicioDetailModel _Producto;

        public ProductoModeloBuilder() {
            this._Producto = new ProductoServicioDetailModel();
        }

        public IProductoModeloAlmacenBuilder Almacen(AlmacenEnum almacen) {
            this._Producto.Almacen = almacen;
            return this;
        }

        public IProductoModeloTipoBuilder Tipo(ProdServTipoEnum tipo) {
            this._Producto.Tipo = tipo;
            return this;
        }

        public IProductoModeloDescripcionBuilder AddDescripcion(string descripcion) {
            this._Producto.Nombre = descripcion;
            return this;
        }

        public virtual T Build<T>() where T : class { 
            if (typeof(T) == typeof(ProductoServicioDetailModel)) {
                return this._Producto as T;
            }
            return null;
        }
    }
}
