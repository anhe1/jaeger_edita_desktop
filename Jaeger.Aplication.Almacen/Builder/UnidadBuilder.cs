﻿using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Aplication.Almacen.Builder {
    public class UnidadBuilder : IUnidadBuilder, IUnidadBuild {
        protected UnidadModel _UnidadModel;

        public UnidadBuilder() {
            this._UnidadModel = new UnidadModel() { SetModified = true };
        }
        public UnidadModel Build() {
            return this._UnidadModel;
        }
    }
}
