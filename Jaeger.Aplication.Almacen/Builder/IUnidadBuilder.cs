﻿using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Aplication.Almacen.Builder {
    public interface IUnidadBuilder : IUnidadBuild {
    }

    public interface IUnidadBuild {
        UnidadModel Build();
    }
}
