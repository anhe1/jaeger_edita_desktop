﻿using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Aplication.Almacen.Builder {
    public class AlmacenBuilder : IAlmacenBuilder, IAlmacenDescripcionBuilder {
        protected internal AlmacenModel _Alamcen;

        public AlmacenBuilder() {
            this._Alamcen = new AlmacenModel();
        }

        public IAlmacenDescripcionBuilder Add(string descripcion) {
            this._Alamcen.Descripcion = descripcion;
            return this;
        }

        public IAlmacenModel Build() {
            return this._Alamcen;
        }
    }
}
