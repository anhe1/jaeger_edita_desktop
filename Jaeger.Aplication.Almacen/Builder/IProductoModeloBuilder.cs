﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.Builder {
    public interface IProductoModeloBuilder {
        IProductoModeloAlmacenBuilder Almacen(AlmacenEnum Almacen);
    }

    public interface IProductoModeloAlmacenBuilder {
        IProductoModeloTipoBuilder Tipo(ProdServTipoEnum tipo);
    }

    public interface IProductoModeloTipoBuilder {
        IProductoModeloDescripcionBuilder AddDescripcion(string descripcion);
    }

    public interface IProductoModeloDescripcionBuilder {
        T Build<T>() where T : class;
    }
}
