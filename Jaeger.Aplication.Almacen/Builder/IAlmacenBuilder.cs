﻿using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Aplication.Almacen.Builder {
    public interface IAlmacenBuilder {
        IAlmacenDescripcionBuilder Add(string descripcion);
    }

    public interface IAlmacenDescripcionBuilder {
        IAlmacenModel Build();
    }
}
