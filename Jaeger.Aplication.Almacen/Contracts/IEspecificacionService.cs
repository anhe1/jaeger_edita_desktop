﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.Contracts {
    public interface IEspecificacionService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        EspecificacionModel Save(EspecificacionModel model);

        BindingList<EspecificacionModel> Save(BindingList<EspecificacionModel> models);
    }
}