﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.Contracts {
    /// <summary>
    /// servicio de clasificacion de Bienes Productos y Servicios (BPS)
    /// </summary>
    public interface IClasificacionService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
