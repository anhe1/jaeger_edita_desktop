﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.Contracts {
    /// <summary>
    /// servicio de categorias de productos para tienda web
    /// </summary>
    public interface ICategoriasService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
