﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Aplication.Almacen.Contracts {
    public interface IModeloService : IAlmacenService {
        ModeloDetailModel GetNew();

        ModeloDetailModel GetById(int index);
        
        ModeloDetailModel Save(ModeloDetailModel modelo);
        
        BindingList<ModeloDetailModel> GetList(bool onlyActive);
    }
}
