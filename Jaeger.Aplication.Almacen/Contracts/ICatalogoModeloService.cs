﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.Contracts {
    /// <summary>
    /// servicio para operaciones de modelos
    /// </summary>
    public interface ICatalogoModeloService {
        /// <summary>
        /// almacenar modelo
        /// </summary>
        ModeloModel Save(ModeloModel model);

        /// <summary>
        /// lista generica
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
