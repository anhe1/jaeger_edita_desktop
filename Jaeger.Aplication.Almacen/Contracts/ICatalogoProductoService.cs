﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Aplication.Almacen.Contracts {
    /// <summary>
    /// servicio para administrar el catalogo de productos y servicios en facturacion, se utiliza el repositorio de modelos
    /// </summary>
    public interface ICatalogoProductoService : IAlmacenService {

        /// <summary>
        /// clasificacion de productos y servicios (BPS)
        /// </summary>
        List<IClasificacionSingle> GetClasificacion();

        /// <summary>
        /// Catalogo de unidades del almacen, segun el tipo de almacen
        /// </summary>
        BindingList<UnidadModel> GetUnidades();

        /// <summary>
        /// lista generica por tipo de objeto y condicionales
        /// </summary>
        /// <typeparam name="T1">clase modelo</typeparam>
        /// <param name="conditionals">condicionales</param>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        ProductoServicioModel Save(ProductoServicioModel model);

        ProductoServicioDetailModel Save(ProductoServicioDetailModel model);

        ModeloModel Save(ModeloModel model);
        
        ModeloDetailModel Save(ModeloDetailModel model);
        
        MedioModel Save(MedioModel medio, string fileName, string keyName);

        /// <summary>
        /// Autorizar modelos
        /// </summary>
        /// <param name="models">lista de modelos</param>
        List<ModeloDetailModel> Autoriza(List<ModeloDetailModel> models);

        /// <summary>
        /// obtener el catalogo de productos de facturacion
        /// </summary>
        BindingList<ModeloDetailModel> GetListConceptos();
    }
}
