﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.Contracts {
    /// <summary>
    /// Catalogo de unidades
    /// </summary>
    public interface IUnidadService : IAlmacenService {
        UnidadModel Save(UnidadModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        BindingList<UnidadModel> Save(BindingList<UnidadModel> items);
    }
}
