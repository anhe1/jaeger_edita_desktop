﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.Contracts {
    public interface IAlmacenService {
        AlmacenEnum Almacen { get; set; }
        List<IAlmacenModel> GetAlmacenes();
    }
}
