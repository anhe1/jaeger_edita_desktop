﻿using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Builder;

namespace Jaeger.Aplication.Almacen.Services {
    public class MediosService : IMediosService {
        public MediosService() { }

        #region metodos estaticos
        public static IMedioQueryBuilder Query() {
            return new MedioQueryBuilder();
        }
        #endregion
    }
}
