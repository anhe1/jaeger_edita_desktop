﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.DataAccess.Almacen.Repositories;

namespace Jaeger.Aplication.Almacen.Services {
    /// <summary>
    /// catalogo de productos y servicios (BPS)
    /// </summary>
    public class CatalogoProductoService : AlmacenService, ICatalogoProductoService {
        #region declaraciones
        protected ISqlProdServRepository productoRepository;
        protected ISqlUnidadRepository unidadRepository;
        protected ISqlModelosRepository modelosRepository;
        protected ISqlContribuyenteRepository contribuyenteRepository;
        protected Base.Contracts.IEditaBucketService s3;
        protected ISqlMediosRepository mediosRepository;
        protected internal ISqlClasificacionRepository clasificacionRepository;
        #endregion

        public CatalogoProductoService(AlmacenEnum almacen) : base(almacen) {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.productoRepository = new SqlSugarProdServRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.unidadRepository = new SqlSugarUnidadRepository(ConfigService.Synapsis.RDS.Edita);
            this.modelosRepository = new SqlSugarModelosRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.contribuyenteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.clasificacionRepository = new SqlSugarClasificacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.s3 = new Base.Services.EditaBucketService();
        }

        /// <summary>
        /// clasificacion de productos y servicios (BPS)
        /// </summary>
        public virtual List<IClasificacionSingle> GetClasificacion() {
            var query = Query().ByAlmacen(this.Almacen).Activo(true);
            return this.clasificacionRepository.GetList<ClasificacionSingle>(query.Build()).ToList<IClasificacionSingle>();
        }

        /// <summary>
        /// Catalogo de unidades del almacen, segun el tipo de almacen
        /// </summary>
        public BindingList<UnidadModel> GetUnidades() {
            var query = UnidadService.Query().ByAlmacen(this.Almacen).Activo().Build();
            return new BindingList<UnidadModel>(this.GetList<UnidadModel>(query).ToList());
        }

        /// <summary>
        /// lista generica por tipo de objeto y condicionales
        /// </summary>
        /// <typeparam name="T1">clase modelo</typeparam>
        /// <param name="conditionals">condicionales</param>
        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ProductoServicioDetailModel)) {
                var dataResult = new List<ProductoServicioDetailModel>(this.productoRepository.GetList<ProductoServicioDetailModel>(conditionals));
                if (dataResult != null) {
                    var activo = true;
                    // determinar si se encuentra el campo de registro activo
                    var condicion = conditionals.Where(it => it.FieldName.ToLower().EndsWith("_a")).FirstOrDefault();
                    if (condicion != null) {
                        activo = condicion.FieldValue == "1";
                    }
                    // lista de indices de productos
                    var lIdProductos = dataResult.Select(it => it.IdProducto).ToList();
                    // consulta de modelos relacionados a la lista de productos
                    var queryModelos = new ModelosQueryBuilder().ByIdAlmacen(this.Almacen).Activo(activo).IdProducto(lIdProductos.ToArray());
                    var dataModelos = this.modelosRepository.GetList<ModeloDetailModel>(queryModelos.Build()).ToList();
                    // asignar la lista de modelos a catalogo de productos
                    if (dataModelos != null) {
                        for (int i = 0; i < dataResult.Count; i++) {
                            dataResult[i].Modelos = new BindingList<ModeloDetailModel>(dataModelos.Where(it => it.IdProducto == dataResult[i].IdProducto).ToList());
                        }
                    }
                }
                return dataResult as List<T1>;
            } else if (typeof(T1) == typeof(UnidadModel)) {
                return this.unidadRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(ProductoModeloExistenciaModel) | typeof(T1) == typeof(ProductoModeloExistencia)) {
                return this.productoRepository.GetList<T1>(conditionals) as List<T1>;
            } else if (typeof(T1) == typeof(ClasificacionSingle) || typeof(T1) == typeof(ClasificacionProductoModel)) {
                return this.clasificacionRepository.GetList<T1>(conditionals);
            }
            return this.modelosRepository.GetList<T1>(conditionals);
        }

        public ProductoServicioDetailModel Save(ProductoServicioDetailModel model) {
            if (model.IdProducto == 0)
                model.Creo = ConfigService.Piloto.Clave;
            model.IdAlmacen = (int)this.Almacen;
            for (int i = 0; i < model.Modelos.Count; i++) {
                model.Modelos[i].IdProducto = model.IdProducto;
                model.Modelos[i].IdCategoria = model.IdCategoria;
                model.Modelos[i].IdAlmacen = model.IdAlmacen;
                if (model.Modelos[i].IdModelo == 0) {
                    model.Modelos[i].Creo = ConfigService.Piloto.Clave;
                } else {
                    model.Modelos[i].Modifica = ConfigService.Piloto.Clave;
                }
                for (int i2 = 0; i2 < model.Modelos[i].Proveedores.Count; i2++) {
                    if (model.Modelos[i].Proveedores[i].Id == 0)
                        model.Modelos[i].Proveedores[i].Creo = ConfigService.Piloto.Clave;
                    else
                        model.Modelos[i].Proveedores[i].Modifico = ConfigService.Piloto.Clave;
                }
            }
            return this.productoRepository.Save(model);
        }

        public ProductoServicioModel Save(ProductoServicioModel model) {
            if (model.IdProducto == 0)
                model.Creo = ConfigService.Piloto.Clave;
            model.IdAlmacen = (int)this.Almacen;
            return this.productoRepository.Save(model);
        }

        public virtual ModeloModel Save(ModeloModel model) {
            return this.modelosRepository.Save(model);
        }

        public virtual ModeloDetailModel Save(ModeloDetailModel model) {
            return this.modelosRepository.Save(model);
        }

        public MedioModel Save(MedioModel medio, string fileName, string keyName) {
            var infoFile = new System.IO.FileInfo(fileName);
            if (infoFile.Exists) {
                medio.URL = this.s3.Upload(fileName, keyName);
                if (medio.URL != null) {
                    return medio;
                }
            }
            return medio;
        }

        /// <summary>
        /// Autorizar modelos
        /// </summary>
        /// <param name="models">lista de modelos</param>
        public List<ModeloDetailModel> Autoriza(List<ModeloDetailModel> models) {
            for (int i = 0; i < models.Count; i++) {
                models[i].Modifica = ConfigService.Piloto.Clave;
                models[i].FechaModifica = DateTime.Now;
                this.modelosRepository.Autoriza(models[i]);
            }
            return models;
        }

        /// <summary>
        /// obtener el catalogo de productos de facturacion
        /// </summary>
        public BindingList<ModeloDetailModel> GetListConceptos() {
            return new BindingList<ModeloDetailModel>(this.modelosRepository.GetList(AlmacenEnum.PT, 0, true).ToList());
        }

        #region metodos estaticos
        /// <summary>
        /// obtener lista de tipos de producto de la Enumeracion ProdServTipoEnum
        /// </summary>
        public static List<ProductoServicioTipoModel> GetTipos() {
            return ((ProdServTipoEnum[])Enum.GetValues(typeof(ProdServTipoEnum))).Select(c => new ProductoServicioTipoModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// lista de factores de IVA
        /// </summary>
        public static List<FactorIVAModel> FactorIVA() {
            return ((ImpuestoTipoFactorEnum[])Enum.GetValues(typeof(ImpuestoTipoFactorEnum))).Select(c => new FactorIVAModel((int)c, c.ToString())).ToList();
        }

        public static IProductosQueryBuilder Query() {
            return new ProductosQueryBuilder();
        }
        #endregion
    }
}
