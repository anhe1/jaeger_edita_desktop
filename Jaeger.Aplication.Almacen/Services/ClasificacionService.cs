﻿using System.Collections.Generic;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Almacen.Repositories;

namespace Jaeger.Aplication.Almacen.Services {
    /// <summary>
    /// servicio de categorias o clasifiacion general
    /// </summary>
    public class ClasificacionService : IClasificacionService {
        #region declaraciones
        protected internal ISqlClasificacionRepository categoriaRepository;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ClasificacionService() {
            this.categoriaRepository = new SqlSugarClasificacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.categoriaRepository.GetList<T1>(conditionals);
        }

        /// <summary>
        /// creacion de consulta para clasificacion
        /// </summary>
        public static IClasificacionQueryBuilder Query() {
            return new ClasificacionQueryBuilder();
        }
    }
}
