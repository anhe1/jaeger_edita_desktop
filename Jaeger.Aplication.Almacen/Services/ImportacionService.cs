﻿using System.Collections.Generic;
using System.Linq;
using MiniExcelLibs;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Aplication.Almacen.Services {
    public class ImportacionService {
        public ImportacionService() { }

        public List<ProductoModeloLayout> Importar(string path) {
            if (!System.IO.File.Exists(path)) {
                return new List<ProductoModeloLayout>();
            }

            var rows = MiniExcel.Query<ProductoModeloLayout>(path);
            return rows.ToList();
        }
    }
}
