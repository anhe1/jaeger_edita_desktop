﻿using System.Collections.Generic;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Almacen.Repositories;

namespace Jaeger.Aplication.Almacen.Services {
    /// <summary>
    /// servicio para operaciones de modelos
    /// </summary>
    public class CatalogoModeloService : ICatalogoModeloService {
        protected ISqlModelosRepository modelosRepository;

        public CatalogoModeloService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.modelosRepository = new SqlSugarModelosRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// almacenar modelo
        /// </summary>
        public ModeloModel Save(ModeloModel model) {
            return this.modelosRepository.Save(model);
        }

        /// <summary>
        /// lista generica
        /// </summary>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.modelosRepository.GetList<T1>(conditionals);
        }

        #region metodos estaticos
            /// <summary>
            /// consulta para productos - modelos
            /// </summary>
        public static IModelosQueryBuilder Query() {
            return new ModelosQueryBuilder();
        }
        #endregion
    }
}
