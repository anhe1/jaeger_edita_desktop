﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.DataAccess.Almacen.Repositories;

namespace Jaeger.Aplication.Almacen.Services {
    public class UnidadService : AlmacenService, IUnidadService {
        #region declaraciones
        protected internal ISqlUnidadRepository unidadRepository;
        #endregion

        public UnidadService(AlmacenEnum almacen) : base(almacen) {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.unidadRepository = new SqlSugarUnidadRepository(ConfigService.Synapsis.RDS.Edita);
        }

        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.unidadRepository.GetList<T1>(conditionals);
        }

        public virtual UnidadModel Save(UnidadModel model) {
            return this.unidadRepository.Save(model);
        }

        public virtual BindingList<UnidadModel> Save(BindingList<UnidadModel> items) {
            for (int i = 0; i < items.Count; i++) {
                if (items[i].IdUnidad == 0 && items[i].SetModified) {
                    items[i].Creo = ConfigService.Piloto.Clave;
                    items[i].FechaNuevo = DateTime.Now;
                    items[i].IdUnidad = this.unidadRepository.Insert(items[i]);
                } else if (items[i].SetModified) {
                    this.unidadRepository.Update(items[i]);
                }

            }
            return items;
        }

        #region metodos estaticos
        public static IUnidadQueryBuilder Query() {
            return new UnidadQueryBuilder();
        }

        public static Builder.IUnidadBuilder Create() {
            return new Builder.UnidadBuilder();
        }
        #endregion
    }
}
