﻿using System.Collections.Generic;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.Services {
    /// <summary>
    /// servicio de categorias de productos para tienda web
    /// </summary>
    public class CategoriasService : ICategoriasService {
        protected internal ISqlCategoriaRepository categoriasRepository;

        public CategoriasService() {
            //this.categoriasRepository = new SqlSugarca
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.categoriasRepository.GetList<T1>(conditionals);
        }

        #region metodos estaticos
        public static ICategoriaQueryBuilder Query() {
            return new CategoriaQueryBuilder();
        }
        #endregion 
    }
}
