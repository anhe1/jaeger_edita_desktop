﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.DataAccess.Almacen.Repositories;

namespace Jaeger.Aplication.Almacen.Services {
    public class AlmacenService : IAlmacenService {
        protected internal ISqlAlmacenRepository _AlmacenRepository;
        public AlmacenService(AlmacenEnum almacen) {
            this.Almacen = almacen;
            this._AlmacenRepository = new SqlSugarAlmacenRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public AlmacenEnum Almacen { get; set; }

        public List<IAlmacenModel> GetAlmacenes() {
            return this._AlmacenRepository.GetList(new List<Domain.Base.Builder.IConditional>()).ToList();
        }

        #region metodos estaticos
        public static List<DepartamentoModel> GetDepartamentos() {
            return new List<DepartamentoModel> {
                new DepartamentoModel{ IdDepartamento = 1, Descripcion = "Almacén Gral"},
                new DepartamentoModel{ IdDepartamento = 2, Descripcion = "Almacén MP"},
                new DepartamentoModel{ IdDepartamento = 3, Descripcion = "Almacén PT"},
                new DepartamentoModel{ IdDepartamento = 4, Descripcion = "Almacén DP"},
                new DepartamentoModel{ IdDepartamento = 5, Descripcion = "Administración"},
                new DepartamentoModel{ IdDepartamento = 7, Descripcion = "Ventas"},
                new DepartamentoModel{ IdDepartamento = 8, Descripcion = "Cotizaciones"},
                new DepartamentoModel{ IdDepartamento = 9, Descripcion = "PrePrensa"},
                new DepartamentoModel{ IdDepartamento = 10, Descripcion = "Prensa"},
                new DepartamentoModel{ IdDepartamento = 11, Descripcion = "PosPrensa"},
                new DepartamentoModel{ IdDepartamento = 12, Descripcion = "Sistemas"},
                new DepartamentoModel{ IdDepartamento = 13, Descripcion = "Vigilancia"},
                new DepartamentoModel{ IdDepartamento = 14, Descripcion = "Distribución"},
            };
        }

        public static DepartamentoModel GetDepartamento(int idDepartamento) {
            var departamentos = GetDepartamentos();
            try {
                var d0 = departamentos.Where(it => it.IdDepartamento == idDepartamento).FirstOrDefault();
                if (d0 != null) {
                    return d0;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new DepartamentoModel();
        }
        #endregion
    }
}
