﻿using Jaeger.Domain.Almacen.Builder;

namespace Jaeger.Aplication.Almacen.Services {
    public class ExistenciasServices {
        #region metodos estaticos
        /// <summary>
        /// consulta de existencias
        /// </summary>
        public static IExistenciasQueryBuilder Query() {
            return new ExistenciasQueryBuilder();
        }
        #endregion
    }
}
