﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.DataAccess.Almacen.Repositories;

namespace Jaeger.Aplication.Almacen.Services {
    public class EspecificacionService : IEspecificacionService {
        protected ISqlEspecificacionRepository especificacionRepository;

        public EspecificacionService() { this.OnLoad(); }

        public virtual void OnLoad() {
            this.especificacionRepository = new SqlSugarEspecificacionRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.especificacionRepository.GetList<T1>(conditionals).ToList();
        }

        public EspecificacionModel Save(EspecificacionModel model) {
            return this.especificacionRepository.Save(model);
        }

        public BindingList<EspecificacionModel> Save(BindingList<EspecificacionModel> models) {
            for (int i = 0; i < models.Count; i++) {
                if (models[i].SetModified)
                    models[i] = this.especificacionRepository.Save(models[i]);
            }
            return models;
        }

        public static IEspecificacionQueryBuilder Query() {
            return new EspecificacionQueryBuilder();
        }
    }
}
