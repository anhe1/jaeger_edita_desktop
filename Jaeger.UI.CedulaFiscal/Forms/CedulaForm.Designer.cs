﻿namespace Jaeger.UI.CedulaFiscal.Forms {
    partial class CedulaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CedulaForm));
            this.picHeader = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new Telerik.WinControls.UI.RadButton();
            this.tabControl1 = new Telerik.WinControls.UI.RadPageView();
            this.tabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.btnExaminar = new Telerik.WinControls.UI.RadButton();
            this.txtPath = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.tabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.btnBuscar = new Telerik.WinControls.UI.RadButton();
            this.txtCURP = new Telerik.WinControls.UI.RadTextBox();
            this.lblIdConstancia = new Telerik.WinControls.UI.RadLabel();
            this.txtRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.tabControl2 = new Telerik.WinControls.UI.RadPageView();
            this.PagePersonaFisica = new Telerik.WinControls.UI.RadPageViewPage();
            this.PersonaFisica = new Jaeger.UI.CedulaFiscal.Forms.CedulaDatosPersonaFisicaControl();
            this.PagePersonalMoral = new Telerik.WinControls.UI.RadPageViewPage();
            this.PersonaMoral = new Jaeger.UI.CedulaFiscal.Forms.CedulaDatosPersonaMoralControl();
            this.btnCopiar = new Telerik.WinControls.UI.RadButton();
            this.lblInformacion = new Telerik.WinControls.UI.RadLabel();
            this.gRegimenes = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.picHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExaminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIdConstancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl2)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.PagePersonaFisica.SuspendLayout();
            this.PagePersonalMoral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCopiar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInformacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRegimenes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRegimenes.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // picHeader
            // 
            this.picHeader.BackColor = System.Drawing.Color.White;
            this.picHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.picHeader.Location = new System.Drawing.Point(0, 0);
            this.picHeader.Name = "picHeader";
            this.picHeader.Size = new System.Drawing.Size(611, 40);
            this.picHeader.TabIndex = 0;
            this.picHeader.TabStop = false;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Location = new System.Drawing.Point(524, 516);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 2;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.DefaultPage = this.tabPage1;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedPage = this.tabPage1;
            this.tabControl1.Size = new System.Drawing.Size(611, 90);
            this.tabControl1.TabIndex = 3;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnExaminar);
            this.tabPage1.Controls.Add(this.txtPath);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.ItemSize = new System.Drawing.SizeF(72F, 28F);
            this.tabPage1.Location = new System.Drawing.Point(10, 37);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(590, 42);
            this.tabPage1.Text = "Por archivo";
            // 
            // btnExaminar
            // 
            this.btnExaminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExaminar.Location = new System.Drawing.Point(510, 14);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(75, 23);
            this.btnExaminar.TabIndex = 2;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.Click += new System.EventHandler(this.BtnExaminar_Click);
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.Location = new System.Drawing.Point(60, 15);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(444, 20);
            this.txtPath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Archivo:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnBuscar);
            this.tabPage2.Controls.Add(this.txtCURP);
            this.tabPage2.Controls.Add(this.lblIdConstancia);
            this.tabPage2.Controls.Add(this.txtRFC);
            this.tabPage2.Controls.Add(this.lblRFC);
            this.tabPage2.ItemSize = new System.Drawing.SizeF(56F, 28F);
            this.tabPage2.Location = new System.Drawing.Point(10, 37);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(600, 42);
            this.tabPage2.Text = "Por RFC";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(488, 16);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 23;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // txtCURP
            // 
            this.txtCURP.BackColor = System.Drawing.SystemColors.Window;
            this.txtCURP.Location = new System.Drawing.Point(261, 17);
            this.txtCURP.MaxLength = 11;
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.Size = new System.Drawing.Size(124, 20);
            this.txtCURP.TabIndex = 21;
            this.txtCURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblIdConstancia
            // 
            this.lblIdConstancia.Location = new System.Drawing.Point(165, 18);
            this.lblIdConstancia.Name = "lblIdConstancia";
            this.lblIdConstancia.Size = new System.Drawing.Size(92, 18);
            this.lblIdConstancia.TabIndex = 22;
            this.lblIdConstancia.Text = "Id de Constancia:";
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(49, 17);
            this.txtRFC.MaxLength = 14;
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(113, 20);
            this.txtRFC.TabIndex = 19;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(15, 18);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 20;
            this.lblRFC.Text = "RFC:";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.PagePersonaFisica);
            this.tabControl2.Controls.Add(this.PagePersonalMoral);
            this.tabControl2.DefaultPage = this.PagePersonaFisica;
            this.tabControl2.Location = new System.Drawing.Point(4, 126);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedPage = this.PagePersonaFisica;
            this.tabControl2.Size = new System.Drawing.Size(606, 308);
            this.tabControl2.TabIndex = 4;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl2.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl2.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Left;
            // 
            // PagePersonaFisica
            // 
            this.PagePersonaFisica.Controls.Add(this.PersonaFisica);
            this.PagePersonaFisica.ItemSize = new System.Drawing.SizeF(28F, 86F);
            this.PagePersonaFisica.Location = new System.Drawing.Point(37, 10);
            this.PagePersonaFisica.Name = "PagePersonaFisica";
            this.PagePersonaFisica.Padding = new System.Windows.Forms.Padding(3);
            this.PagePersonaFisica.Size = new System.Drawing.Size(558, 287);
            this.PagePersonaFisica.Text = "Persona Fisica";
            // 
            // PersonaFisica
            // 
            this.PersonaFisica.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersonaFisica.Location = new System.Drawing.Point(3, 3);
            this.PersonaFisica.Name = "PersonaFisica";
            this.PersonaFisica.Size = new System.Drawing.Size(552, 281);
            this.PersonaFisica.TabIndex = 0;
            // 
            // PagePersonalMoral
            // 
            this.PagePersonalMoral.Controls.Add(this.PersonaMoral);
            this.PagePersonalMoral.ItemSize = new System.Drawing.SizeF(28F, 88F);
            this.PagePersonalMoral.Location = new System.Drawing.Point(37, 10);
            this.PagePersonalMoral.Name = "PagePersonalMoral";
            this.PagePersonalMoral.Padding = new System.Windows.Forms.Padding(3);
            this.PagePersonalMoral.Size = new System.Drawing.Size(558, 287);
            this.PagePersonalMoral.Text = "Persona Moral";
            // 
            // PersonaMoral
            // 
            this.PersonaMoral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersonaMoral.Location = new System.Drawing.Point(3, 3);
            this.PersonaMoral.Name = "PersonaMoral";
            this.PersonaMoral.Size = new System.Drawing.Size(552, 281);
            this.PersonaMoral.TabIndex = 0;
            // 
            // btnCopiar
            // 
            this.btnCopiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopiar.Enabled = false;
            this.btnCopiar.Location = new System.Drawing.Point(443, 516);
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(75, 23);
            this.btnCopiar.TabIndex = 2;
            this.btnCopiar.Text = "Copiar";
            this.btnCopiar.Click += new System.EventHandler(this.BtnCopiar_Click);
            // 
            // lblInformacion
            // 
            this.lblInformacion.BackColor = System.Drawing.Color.White;
            this.lblInformacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacion.Location = new System.Drawing.Point(12, 9);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(314, 18);
            this.lblInformacion.TabIndex = 47;
            this.lblInformacion.Text = "Información de la Cedula de Indentificación Fiscal";
            // 
            // gRegimenes
            // 
            this.gRegimenes.Location = new System.Drawing.Point(37, 433);
            // 
            // 
            // 
            this.gRegimenes.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 380;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.FieldName = "FechaAlta";
            gridViewTextBoxColumn3.FormatString = "{0:d}";
            gridViewTextBoxColumn3.HeaderText = "Fec. de Alta";
            gridViewTextBoxColumn3.Name = "FechaAlta";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 85;
            this.gRegimenes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.gRegimenes.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gRegimenes.Name = "gRegimenes";
            this.gRegimenes.ShowGroupPanel = false;
            this.gRegimenes.Size = new System.Drawing.Size(563, 72);
            this.gRegimenes.TabIndex = 48;
            // 
            // CedulaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCerrar;
            this.ClientSize = new System.Drawing.Size(611, 551);
            this.Controls.Add(this.gRegimenes);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnCopiar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.picHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CedulaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cedula de Identificación Fiscal";
            this.Load += new System.EventHandler(this.CedulaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExaminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIdConstancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl2)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.PagePersonaFisica.ResumeLayout(false);
            this.PagePersonalMoral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCopiar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInformacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRegimenes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRegimenes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picHeader;
        private Telerik.WinControls.UI.RadButton btnCerrar;
        private Telerik.WinControls.UI.RadPageView tabControl1;
        private Telerik.WinControls.UI.RadPageViewPage tabPage1;
        private Telerik.WinControls.UI.RadButton btnExaminar;
        private Telerik.WinControls.UI.RadTextBox txtPath;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadPageViewPage tabPage2;
        private Telerik.WinControls.UI.RadPageView tabControl2;
        private Telerik.WinControls.UI.RadPageViewPage PagePersonaFisica;
        private CedulaDatosPersonaFisicaControl PersonaFisica;
        private Telerik.WinControls.UI.RadPageViewPage PagePersonalMoral;
        private CedulaDatosPersonaMoralControl PersonaMoral;
        private Telerik.WinControls.UI.RadButton btnBuscar;
        private Telerik.WinControls.UI.RadTextBox txtCURP;
        private Telerik.WinControls.UI.RadLabel lblIdConstancia;
        private Telerik.WinControls.UI.RadTextBox txtRFC;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        public Telerik.WinControls.UI.RadButton btnCopiar;
        private Telerik.WinControls.UI.RadLabel lblInformacion;
        internal Telerik.WinControls.UI.RadGridView gRegimenes;
    }
}