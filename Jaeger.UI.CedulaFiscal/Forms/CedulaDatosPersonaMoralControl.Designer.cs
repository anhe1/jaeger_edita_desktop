﻿namespace Jaeger.UI.CedulaFiscal.Forms {
    partial class CedulaDatosPersonaMoralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.grpDatosIdentificacion = new Telerik.WinControls.UI.RadGroupBox();
            this.txtFecCambio = new Telerik.WinControls.UI.RadTextBox();
            this.lblFecCambio = new Telerik.WinControls.UI.RadLabel();
            this.txtSituacion = new Telerik.WinControls.UI.RadTextBox();
            this.lblSituacion = new Telerik.WinControls.UI.RadLabel();
            this.txtFecInicio = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.txtFecConstitucion = new Telerik.WinControls.UI.RadTextBox();
            this.lblFecNacimiento = new Telerik.WinControls.UI.RadLabel();
            this.txtNombreComercial = new Telerik.WinControls.UI.RadTextBox();
            this.lblApellidoMaterno = new Telerik.WinControls.UI.RadLabel();
            this.txtRegimenCapital = new Telerik.WinControls.UI.RadTextBox();
            this.lblApellidoPaterno = new Telerik.WinControls.UI.RadLabel();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.txtRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.DatosUbicacion = new Jaeger.UI.CedulaFiscal.Forms.CedulaDatosUbicacionControl();
            ((System.ComponentModel.ISupportInitialize)(this.grpDatosIdentificacion)).BeginInit();
            this.grpDatosIdentificacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSituacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSituacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecConstitucion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoMaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegimenCapital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoPaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDatosIdentificacion
            // 
            this.grpDatosIdentificacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpDatosIdentificacion.Controls.Add(this.txtFecCambio);
            this.grpDatosIdentificacion.Controls.Add(this.lblFecCambio);
            this.grpDatosIdentificacion.Controls.Add(this.txtSituacion);
            this.grpDatosIdentificacion.Controls.Add(this.lblSituacion);
            this.grpDatosIdentificacion.Controls.Add(this.txtFecInicio);
            this.grpDatosIdentificacion.Controls.Add(this.label6);
            this.grpDatosIdentificacion.Controls.Add(this.txtFecConstitucion);
            this.grpDatosIdentificacion.Controls.Add(this.lblFecNacimiento);
            this.grpDatosIdentificacion.Controls.Add(this.txtNombreComercial);
            this.grpDatosIdentificacion.Controls.Add(this.lblApellidoMaterno);
            this.grpDatosIdentificacion.Controls.Add(this.txtRegimenCapital);
            this.grpDatosIdentificacion.Controls.Add(this.lblApellidoPaterno);
            this.grpDatosIdentificacion.Controls.Add(this.txtNombre);
            this.grpDatosIdentificacion.Controls.Add(this.lblNombre);
            this.grpDatosIdentificacion.Controls.Add(this.txtRFC);
            this.grpDatosIdentificacion.Controls.Add(this.lblRFC);
            this.grpDatosIdentificacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDatosIdentificacion.HeaderText = "Datos de Identificación";
            this.grpDatosIdentificacion.Location = new System.Drawing.Point(0, 0);
            this.grpDatosIdentificacion.Name = "grpDatosIdentificacion";
            this.grpDatosIdentificacion.Size = new System.Drawing.Size(550, 131);
            this.grpDatosIdentificacion.TabIndex = 5;
            this.grpDatosIdentificacion.TabStop = false;
            this.grpDatosIdentificacion.Text = "Datos de Identificación";
            // 
            // txtFecCambio
            // 
            this.txtFecCambio.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecCambio.Location = new System.Drawing.Point(340, 101);
            this.txtFecCambio.Name = "txtFecCambio";
            this.txtFecCambio.NullText = "00/00/0000";
            this.txtFecCambio.ReadOnly = true;
            this.txtFecCambio.Size = new System.Drawing.Size(159, 20);
            this.txtFecCambio.TabIndex = 27;
            this.txtFecCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFecCambio
            // 
            this.lblFecCambio.Location = new System.Drawing.Point(337, 85);
            this.lblFecCambio.Name = "lblFecCambio";
            this.lblFecCambio.Size = new System.Drawing.Size(185, 18);
            this.lblFecCambio.TabIndex = 28;
            this.lblFecCambio.Text = "Fec. del último cambio de situación:";
            // 
            // txtSituacion
            // 
            this.txtSituacion.BackColor = System.Drawing.SystemColors.Window;
            this.txtSituacion.Location = new System.Drawing.Point(442, 23);
            this.txtSituacion.Name = "txtSituacion";
            this.txtSituacion.NullText = "XXXXXX";
            this.txtSituacion.ReadOnly = true;
            this.txtSituacion.Size = new System.Drawing.Size(100, 20);
            this.txtSituacion.TabIndex = 25;
            this.txtSituacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSituacion
            // 
            this.lblSituacion.Location = new System.Drawing.Point(385, 24);
            this.lblSituacion.Name = "lblSituacion";
            this.lblSituacion.Size = new System.Drawing.Size(54, 18);
            this.lblSituacion.TabIndex = 26;
            this.lblSituacion.Text = "Situación:";
            // 
            // txtFecInicio
            // 
            this.txtFecInicio.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecInicio.Location = new System.Drawing.Point(175, 101);
            this.txtFecInicio.Name = "txtFecInicio";
            this.txtFecInicio.NullText = "00/00/0000";
            this.txtFecInicio.ReadOnly = true;
            this.txtFecInicio.Size = new System.Drawing.Size(159, 20);
            this.txtFecInicio.TabIndex = 23;
            this.txtFecInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(172, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 18);
            this.label6.TabIndex = 24;
            this.label6.Text = "Fec. de Inicio de operaciones:";
            // 
            // txtFecConstitucion
            // 
            this.txtFecConstitucion.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecConstitucion.Location = new System.Drawing.Point(10, 101);
            this.txtFecConstitucion.Name = "txtFecConstitucion";
            this.txtFecConstitucion.NullText = "00/00/0000";
            this.txtFecConstitucion.ReadOnly = true;
            this.txtFecConstitucion.Size = new System.Drawing.Size(159, 20);
            this.txtFecConstitucion.TabIndex = 23;
            this.txtFecConstitucion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFecNacimiento
            // 
            this.lblFecNacimiento.Location = new System.Drawing.Point(7, 85);
            this.lblFecNacimiento.Name = "lblFecNacimiento";
            this.lblFecNacimiento.Size = new System.Drawing.Size(117, 18);
            this.lblFecNacimiento.TabIndex = 24;
            this.lblFecNacimiento.Text = "Fecha de constitución:";
            // 
            // txtNombreComercial
            // 
            this.txtNombreComercial.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreComercial.Location = new System.Drawing.Point(360, 62);
            this.txtNombreComercial.Name = "txtNombreComercial";
            this.txtNombreComercial.NullText = "Nombre Comercial";
            this.txtNombreComercial.ReadOnly = true;
            this.txtNombreComercial.Size = new System.Drawing.Size(187, 20);
            this.txtNombreComercial.TabIndex = 21;
            this.txtNombreComercial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoMaterno
            // 
            this.lblApellidoMaterno.Location = new System.Drawing.Point(357, 46);
            this.lblApellidoMaterno.Name = "lblApellidoMaterno";
            this.lblApellidoMaterno.Size = new System.Drawing.Size(103, 18);
            this.lblApellidoMaterno.TabIndex = 22;
            this.lblApellidoMaterno.Text = "Nombre Comercial:";
            // 
            // txtRegimenCapital
            // 
            this.txtRegimenCapital.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegimenCapital.Location = new System.Drawing.Point(210, 62);
            this.txtRegimenCapital.Name = "txtRegimenCapital";
            this.txtRegimenCapital.NullText = "Régimen de capital";
            this.txtRegimenCapital.ReadOnly = true;
            this.txtRegimenCapital.Size = new System.Drawing.Size(144, 20);
            this.txtRegimenCapital.TabIndex = 21;
            this.txtRegimenCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoPaterno
            // 
            this.lblApellidoPaterno.Location = new System.Drawing.Point(207, 46);
            this.lblApellidoPaterno.Name = "lblApellidoPaterno";
            this.lblApellidoPaterno.Size = new System.Drawing.Size(104, 18);
            this.lblApellidoPaterno.TabIndex = 22;
            this.lblApellidoPaterno.Text = "Régimen de capital:";
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombre.Location = new System.Drawing.Point(8, 62);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.NullText = "Denominación o Razón Social";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(196, 20);
            this.txtNombre.TabIndex = 19;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(8, 46);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(157, 18);
            this.lblNombre.TabIndex = 20;
            this.lblNombre.Text = "Denominación o Razón Social:";
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(46, 23);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.ReadOnly = true;
            this.txtRFC.Size = new System.Drawing.Size(113, 20);
            this.txtRFC.TabIndex = 15;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(8, 24);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 16;
            this.lblRFC.Text = "RFC:";
            // 
            // DatosUbicacion
            // 
            this.DatosUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.DatosUbicacion.Location = new System.Drawing.Point(0, 131);
            this.DatosUbicacion.Name = "DatosUbicacion";
            this.DatosUbicacion.Size = new System.Drawing.Size(550, 149);
            this.DatosUbicacion.TabIndex = 6;
            // 
            // CedulaDatosPersonaMoralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DatosUbicacion);
            this.Controls.Add(this.grpDatosIdentificacion);
            this.Name = "CedulaDatosPersonaMoralControl";
            this.Size = new System.Drawing.Size(550, 280);
            this.Load += new System.EventHandler(this.CedulaDatosPersonaMoralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpDatosIdentificacion)).EndInit();
            this.grpDatosIdentificacion.ResumeLayout(false);
            this.grpDatosIdentificacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSituacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSituacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecConstitucion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoMaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegimenCapital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoPaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox grpDatosIdentificacion;
        private Telerik.WinControls.UI.RadTextBox txtFecCambio;
        private Telerik.WinControls.UI.RadLabel lblFecCambio;
        private Telerik.WinControls.UI.RadTextBox txtSituacion;
        private Telerik.WinControls.UI.RadLabel lblSituacion;
        private Telerik.WinControls.UI.RadTextBox txtFecInicio;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadTextBox txtFecConstitucion;
        private Telerik.WinControls.UI.RadLabel lblFecNacimiento;
        private Telerik.WinControls.UI.RadTextBox txtNombreComercial;
        private Telerik.WinControls.UI.RadLabel lblApellidoMaterno;
        private Telerik.WinControls.UI.RadTextBox txtRegimenCapital;
        private Telerik.WinControls.UI.RadLabel lblApellidoPaterno;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private Telerik.WinControls.UI.RadTextBox txtRFC;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        private CedulaDatosUbicacionControl DatosUbicacion;
    }
}
