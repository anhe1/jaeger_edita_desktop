﻿using System;
using System.Windows.Forms;
using Jaeger.SAT.CIF.Interfaces;

namespace Jaeger.UI.CedulaFiscal.Forms {
    public partial class CedulaDatosUbicacionControl : UserControl {
        public CedulaDatosUbicacionControl() {
            InitializeComponent();
        }

        private void CedulaDatosUbicacionControl_Load(object sender, EventArgs e) {

        }

        public void Cargar(IDomicilioFiscal persona) {
            this.txtTipoVialidad.Text = persona.TipoVialidad;
            this.txtNombreVialidad.Text = persona.NombreVialidad;
            this.txtNumExterior.Text = persona.NumExterior;
            this.txtNumInterior.Text = persona.NumInterior;
            this.txtCodigoPostal.Text = persona.CodigoPostal;
            this.txtColonia.Text = persona.Colonia;
            this.txtEntidad.Text = persona.EntidadFederativa;
            this.txtMunicipio.Text = persona.MunicipioDelegacion;
            this.txtAl.Text = persona.Al;
            this.txtCorreo.Text = persona.Correo;
        }
    }
}
