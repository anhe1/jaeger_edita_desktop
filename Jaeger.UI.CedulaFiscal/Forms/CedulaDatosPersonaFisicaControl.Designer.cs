﻿namespace Jaeger.UI.CedulaFiscal.Forms {
    partial class CedulaDatosPersonaFisicaControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.grpDatosIdentificacion = new Telerik.WinControls.UI.RadGroupBox();
            this.txtFecCambio = new Telerik.WinControls.UI.RadTextBox();
            this.lblFecCambio = new Telerik.WinControls.UI.RadLabel();
            this.txtSituacion = new Telerik.WinControls.UI.RadTextBox();
            this.lblSituacion = new Telerik.WinControls.UI.RadLabel();
            this.txtFecInicio = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.txtFecNacimiento = new Telerik.WinControls.UI.RadTextBox();
            this.lblFecNacimiento = new Telerik.WinControls.UI.RadLabel();
            this.txtApellidoMaterno = new Telerik.WinControls.UI.RadTextBox();
            this.lblApellidoMaterno = new Telerik.WinControls.UI.RadLabel();
            this.txtApellidoPaterno = new Telerik.WinControls.UI.RadTextBox();
            this.lblApellidoPaterno = new Telerik.WinControls.UI.RadLabel();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.txtCURP = new Telerik.WinControls.UI.RadTextBox();
            this.lblCURP = new Telerik.WinControls.UI.RadLabel();
            this.txtRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.DatosUbicacion = new Jaeger.UI.CedulaFiscal.Forms.CedulaDatosUbicacionControl();
            ((System.ComponentModel.ISupportInitialize)(this.grpDatosIdentificacion)).BeginInit();
            this.grpDatosIdentificacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSituacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSituacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoMaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoMaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoPaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoPaterno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDatosIdentificacion
            // 
            this.grpDatosIdentificacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpDatosIdentificacion.Controls.Add(this.txtFecCambio);
            this.grpDatosIdentificacion.Controls.Add(this.lblFecCambio);
            this.grpDatosIdentificacion.Controls.Add(this.txtSituacion);
            this.grpDatosIdentificacion.Controls.Add(this.lblSituacion);
            this.grpDatosIdentificacion.Controls.Add(this.txtFecInicio);
            this.grpDatosIdentificacion.Controls.Add(this.label6);
            this.grpDatosIdentificacion.Controls.Add(this.txtFecNacimiento);
            this.grpDatosIdentificacion.Controls.Add(this.lblFecNacimiento);
            this.grpDatosIdentificacion.Controls.Add(this.txtApellidoMaterno);
            this.grpDatosIdentificacion.Controls.Add(this.lblApellidoMaterno);
            this.grpDatosIdentificacion.Controls.Add(this.txtApellidoPaterno);
            this.grpDatosIdentificacion.Controls.Add(this.lblApellidoPaterno);
            this.grpDatosIdentificacion.Controls.Add(this.txtNombre);
            this.grpDatosIdentificacion.Controls.Add(this.lblNombre);
            this.grpDatosIdentificacion.Controls.Add(this.txtCURP);
            this.grpDatosIdentificacion.Controls.Add(this.lblCURP);
            this.grpDatosIdentificacion.Controls.Add(this.txtRFC);
            this.grpDatosIdentificacion.Controls.Add(this.lblRFC);
            this.grpDatosIdentificacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDatosIdentificacion.HeaderText = "Datos de Identificación";
            this.grpDatosIdentificacion.Location = new System.Drawing.Point(0, 0);
            this.grpDatosIdentificacion.Name = "grpDatosIdentificacion";
            this.grpDatosIdentificacion.Size = new System.Drawing.Size(550, 131);
            this.grpDatosIdentificacion.TabIndex = 4;
            this.grpDatosIdentificacion.TabStop = false;
            this.grpDatosIdentificacion.Text = "Datos de Identificación";
            // 
            // txtFecCambio
            // 
            this.txtFecCambio.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecCambio.Location = new System.Drawing.Point(340, 101);
            this.txtFecCambio.Name = "txtFecCambio";
            this.txtFecCambio.NullText = "00/00/0000";
            this.txtFecCambio.ReadOnly = true;
            this.txtFecCambio.Size = new System.Drawing.Size(159, 20);
            this.txtFecCambio.TabIndex = 27;
            this.txtFecCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFecCambio
            // 
            this.lblFecCambio.Location = new System.Drawing.Point(337, 85);
            this.lblFecCambio.Name = "lblFecCambio";
            this.lblFecCambio.Size = new System.Drawing.Size(185, 18);
            this.lblFecCambio.TabIndex = 28;
            this.lblFecCambio.Text = "Fec. del último cambio de situación:";
            // 
            // txtSituacion
            // 
            this.txtSituacion.BackColor = System.Drawing.SystemColors.Window;
            this.txtSituacion.Location = new System.Drawing.Point(442, 23);
            this.txtSituacion.Name = "txtSituacion";
            this.txtSituacion.NullText = "XXXXXX";
            this.txtSituacion.ReadOnly = true;
            this.txtSituacion.Size = new System.Drawing.Size(100, 20);
            this.txtSituacion.TabIndex = 25;
            this.txtSituacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSituacion
            // 
            this.lblSituacion.Location = new System.Drawing.Point(385, 24);
            this.lblSituacion.Name = "lblSituacion";
            this.lblSituacion.Size = new System.Drawing.Size(54, 18);
            this.lblSituacion.TabIndex = 26;
            this.lblSituacion.Text = "Situación:";
            // 
            // txtFecInicio
            // 
            this.txtFecInicio.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecInicio.Location = new System.Drawing.Point(175, 101);
            this.txtFecInicio.Name = "txtFecInicio";
            this.txtFecInicio.NullText = "00/00/0000";
            this.txtFecInicio.ReadOnly = true;
            this.txtFecInicio.Size = new System.Drawing.Size(159, 20);
            this.txtFecInicio.TabIndex = 23;
            this.txtFecInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(172, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 18);
            this.label6.TabIndex = 24;
            this.label6.Text = "Fec. de Inicio de operaciones:";
            // 
            // txtFecNacimiento
            // 
            this.txtFecNacimiento.BackColor = System.Drawing.SystemColors.Window;
            this.txtFecNacimiento.Location = new System.Drawing.Point(10, 101);
            this.txtFecNacimiento.Name = "txtFecNacimiento";
            this.txtFecNacimiento.NullText = "00/00/0000";
            this.txtFecNacimiento.ReadOnly = true;
            this.txtFecNacimiento.Size = new System.Drawing.Size(159, 20);
            this.txtFecNacimiento.TabIndex = 23;
            this.txtFecNacimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFecNacimiento
            // 
            this.lblFecNacimiento.Location = new System.Drawing.Point(8, 85);
            this.lblFecNacimiento.Name = "lblFecNacimiento";
            this.lblFecNacimiento.Size = new System.Drawing.Size(114, 18);
            this.lblFecNacimiento.TabIndex = 24;
            this.lblFecNacimiento.Text = "Fecha de Nacimiento:";
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.BackColor = System.Drawing.SystemColors.Window;
            this.txtApellidoMaterno.Location = new System.Drawing.Point(371, 62);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.ReadOnly = true;
            this.txtApellidoMaterno.Size = new System.Drawing.Size(175, 20);
            this.txtApellidoMaterno.TabIndex = 21;
            this.txtApellidoMaterno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoMaterno
            // 
            this.lblApellidoMaterno.Location = new System.Drawing.Point(370, 46);
            this.lblApellidoMaterno.Name = "lblApellidoMaterno";
            this.lblApellidoMaterno.Size = new System.Drawing.Size(96, 18);
            this.lblApellidoMaterno.TabIndex = 22;
            this.lblApellidoMaterno.Text = "Apellido Materno:";
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.BackColor = System.Drawing.SystemColors.Window;
            this.txtApellidoPaterno.Location = new System.Drawing.Point(189, 62);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.ReadOnly = true;
            this.txtApellidoPaterno.Size = new System.Drawing.Size(175, 20);
            this.txtApellidoPaterno.TabIndex = 21;
            this.txtApellidoPaterno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblApellidoPaterno
            // 
            this.lblApellidoPaterno.Location = new System.Drawing.Point(189, 46);
            this.lblApellidoPaterno.Name = "lblApellidoPaterno";
            this.lblApellidoPaterno.Size = new System.Drawing.Size(92, 18);
            this.lblApellidoPaterno.TabIndex = 22;
            this.lblApellidoPaterno.Text = "Apellido Paterno:";
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombre.Location = new System.Drawing.Point(8, 62);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(175, 20);
            this.txtNombre.TabIndex = 19;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(8, 46);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(48, 18);
            this.lblNombre.TabIndex = 20;
            this.lblNombre.Text = "Nombre";
            // 
            // txtCURP
            // 
            this.txtCURP.BackColor = System.Drawing.SystemColors.Window;
            this.txtCURP.Location = new System.Drawing.Point(205, 23);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.ReadOnly = true;
            this.txtCURP.Size = new System.Drawing.Size(159, 20);
            this.txtCURP.TabIndex = 17;
            this.txtCURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCURP
            // 
            this.lblCURP.Location = new System.Drawing.Point(162, 24);
            this.lblCURP.Name = "lblCURP";
            this.lblCURP.Size = new System.Drawing.Size(37, 18);
            this.lblCURP.TabIndex = 18;
            this.lblCURP.Text = "CURP:";
            // 
            // txtRFC
            // 
            this.txtRFC.BackColor = System.Drawing.SystemColors.Window;
            this.txtRFC.Location = new System.Drawing.Point(46, 23);
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.ReadOnly = true;
            this.txtRFC.Size = new System.Drawing.Size(113, 20);
            this.txtRFC.TabIndex = 15;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(8, 24);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 16;
            this.lblRFC.Text = "RFC:";
            // 
            // DatosUbicacion
            // 
            this.DatosUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.DatosUbicacion.Location = new System.Drawing.Point(0, 131);
            this.DatosUbicacion.Name = "DatosUbicacion";
            this.DatosUbicacion.Size = new System.Drawing.Size(550, 149);
            this.DatosUbicacion.TabIndex = 5;
            // 
            // CedulaDatosPersonaFisicaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DatosUbicacion);
            this.Controls.Add(this.grpDatosIdentificacion);
            this.Name = "CedulaDatosPersonaFisicaControl";
            this.Size = new System.Drawing.Size(550, 280);
            this.Load += new System.EventHandler(this.CedulaDatosPersonaFisicaControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpDatosIdentificacion)).EndInit();
            this.grpDatosIdentificacion.ResumeLayout(false);
            this.grpDatosIdentificacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSituacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSituacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFecNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoMaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoMaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApellidoPaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblApellidoPaterno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox grpDatosIdentificacion;
        private Telerik.WinControls.UI.RadTextBox txtFecCambio;
        private Telerik.WinControls.UI.RadLabel lblFecCambio;
        private Telerik.WinControls.UI.RadTextBox txtSituacion;
        private Telerik.WinControls.UI.RadLabel lblSituacion;
        private Telerik.WinControls.UI.RadTextBox txtFecInicio;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadTextBox txtFecNacimiento;
        private Telerik.WinControls.UI.RadLabel lblFecNacimiento;
        private Telerik.WinControls.UI.RadTextBox txtApellidoMaterno;
        private Telerik.WinControls.UI.RadLabel lblApellidoMaterno;
        private Telerik.WinControls.UI.RadTextBox txtApellidoPaterno;
        private Telerik.WinControls.UI.RadLabel lblApellidoPaterno;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private Telerik.WinControls.UI.RadTextBox txtCURP;
        private Telerik.WinControls.UI.RadLabel lblCURP;
        private Telerik.WinControls.UI.RadTextBox txtRFC;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        private CedulaDatosUbicacionControl DatosUbicacion;
    }
}
