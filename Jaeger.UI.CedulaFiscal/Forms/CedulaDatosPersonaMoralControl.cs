﻿using System;
using System.Windows.Forms;
using Jaeger.SAT.CIF.Interfaces;

namespace Jaeger.UI.CedulaFiscal.Forms {
    public partial class CedulaDatosPersonaMoralControl : UserControl {
        public CedulaDatosPersonaMoralControl() {
            InitializeComponent();
        }

        private void CedulaDatosPersonaMoralControl_Load(object sender, EventArgs e) {

        }

        public void Cargar(IPersonaMoral persona) {
            this.txtRFC.Text = persona.RFC;
            this.txtSituacion.Text = persona.Situacion;
            this.txtNombre.Text = persona.Nombre;
            this.txtRegimenCapital.Text = persona.RegimenCapital;
            this.txtNombreComercial.Text = "";
            this.txtFecConstitucion.Text = persona.FechaConstitucion.Value.ToString("dd-MM-yyyy");
            this.txtFecInicio.Text = persona.FechaInicio.Value.ToString("dd-MM-yyyy");
            this.txtFecCambio.Text = persona.FechaUltimoCambio.Value.ToString("dd-MM-yyyy");
            this.DatosUbicacion.Cargar(persona.DomicilioFiscal);
        }
    }
}
