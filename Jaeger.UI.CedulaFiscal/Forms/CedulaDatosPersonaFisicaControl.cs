﻿using System;
using System.Windows.Forms;
using Jaeger.SAT.CIF.Interfaces;

namespace Jaeger.UI.CedulaFiscal.Forms {
    public partial class CedulaDatosPersonaFisicaControl : UserControl {
        public CedulaDatosPersonaFisicaControl() {
            InitializeComponent();
        }

        private void CedulaDatosPersonaFisicaControl_Load(object sender, EventArgs e) {

        }

        public void Cargar(IPersonaFisica personaFisica) {
            this.txtRFC.Text = personaFisica.RFC;
            this.txtCURP.Text = personaFisica.CURP;
            this.txtSituacion.Text = personaFisica.Situacion;
            this.txtNombre.Text = personaFisica.Nombre;
            this.txtApellidoPaterno.Text = personaFisica.PrimerApellido;
            this.txtApellidoMaterno.Text = personaFisica.SegundoApellido;
            this.txtFecNacimiento.Text = personaFisica.FechaNacimiento.Value.ToString();
            this.txtFecInicio.Text = personaFisica.FechaInicio.Value.ToString();
            this.txtFecCambio.Text = personaFisica.FechaUltimoCambio.Value.ToString();
            this.DatosUbicacion.Cargar(personaFisica.DomicilioFiscal);
        }
    }
}
