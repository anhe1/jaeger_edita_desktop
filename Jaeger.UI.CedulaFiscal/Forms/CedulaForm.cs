﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.SAT.CIF.Interfaces;
using Jaeger.SAT.CIF.Entities;
using Jaeger.SAT.CIF.Services;
using Jaeger.SAT.CIF.Helpers;

namespace Jaeger.UI.CedulaFiscal.Forms {
    public partial class CedulaForm : RadForm {
        #region declaraciones
        protected internal IQueryService service;
        protected internal IResponse responseCedula;
        protected internal ICedulaFiscal cedulaFiscal;
        protected internal bool IsOk = false;
        #endregion

        #region eventos
        public event EventHandler<ICedulaFiscal> Copied;
        public void OnCopied(ICedulaFiscal cedula) {
            if (this.Copied != null) {
                this.Copied(this, cedula);
            }
        }
        #endregion

        public CedulaForm() {
            InitializeComponent();
        }

        public ICedulaFiscal ShowForm(IWin32Window parent) {
            this.btnCopiar.Enabled = true;
            this.ShowDialog(parent);
            if (this.IsOk)
                return cedulaFiscal;
            return null;
        }

        private void CedulaForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.service = new QueryService();
        }

        private void BtnExaminar_Click(object sender, EventArgs e) {
            var fileOpen = new OpenFileDialog() { Filter = "*.pdf|*.PDF|*.png|*.PNG|*.jpg|*.JPG" };
            if (fileOpen.ShowDialog(this) == DialogResult.OK) {
                this.txtPath.Text = fileOpen.FileName;
                if (Path.GetExtension(this.txtPath.Text) == ".png") {
                    var image = new Bitmap(this.txtPath.Text);
                    if (image != null) {
                        var d0 = new ServiceQR();
                        var url = d0.GetByQR(this.txtPath.Text);
                        if (url.Message.Contains("https://siat.sat.gob.mx/app/qr/faces/pages/mobile/validadorqr.jsf?D1=10&D2=1&D3=19120271393_TESG830215FF9")) {
                            IRequest request = Request.Create().AddURL(url.Message).Build();
                            this.responseCedula = this.service.Execute(request);
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                } else if (Path.GetExtension(this.txtPath.Text) == ".pdf") {
                    var r = PDFExtractor.GetData(this.txtPath.Text);
                    if (r != null) {
                        if (r.Count > 0) {
                            if (r["rfc"] != null) { this.txtRFC.Text = r["rfc"]; }
                            if (r["idcif"] != null) { this.txtCURP.Text = r["idcif"]; }
                            BtnBuscar_Click(sender, e);
                        } else {
                            MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    } else {
                        MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                } else {
                    MessageBox.Show(this, "No se puede obtener alguna cédula de identificación fiscal de la imagen seleccionada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Buscar)) {
                espera.Text = "Buscando...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void BtnCopiar_Click(object sender, EventArgs e) {
            if (this.cedulaFiscal != null) {
                if (this.cedulaFiscal.TipoPersona == SAT.CIF.Entities.CedulaFiscal.TipoPersonaEnum.Fisica) {
                    Clipboard.SetText(this.cedulaFiscal.Fisica.ToString());
                } else {
                    Clipboard.SetText(this.cedulaFiscal.Moral.ToString());
                }
            }
            this.OnCopied(this.cedulaFiscal);
            this.IsOk = true;
            
        }

        private void BtnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region metodos privados
        private void Buscar() {
            IRequest request = Request.Create().AddRFC(this.txtRFC.Text).AddId(this.txtCURP.Text).Build();
            this.responseCedula = this.service.Execute(request);
            if (this.responseCedula.IsValida) {
                this.cedulaFiscal = this.responseCedula.CedulaFiscal;
            } else {
                MessageBox.Show(this, this.responseCedula.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void CreateBinding() {
            if (this.cedulaFiscal != null) {
                if (this.cedulaFiscal.TipoPersona == SAT.CIF.Entities.CedulaFiscal.TipoPersonaEnum.Moral) {
                    this.PersonaMoral.Cargar(this.cedulaFiscal.Moral);
                    tabControl2.SelectedPage = this.PagePersonalMoral;
                    this.gRegimenes.DataSource = this.cedulaFiscal.Moral.Regimenes;
                } else if (responseCedula.CedulaFiscal.TipoPersona == SAT.CIF.Entities.CedulaFiscal.TipoPersonaEnum.Fisica) {
                    this.PersonaFisica.Cargar(this.cedulaFiscal.Fisica);
                    tabControl2.SelectedPage = this.PagePersonaFisica;
                    this.gRegimenes.DataSource = this.cedulaFiscal.Fisica.Regimenes;
                }
            }
        }
        #endregion
    }
}
