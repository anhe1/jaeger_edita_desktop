﻿namespace Jaeger.UI.Forms {
    partial class ProdServRepositoryForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IncluirIVATrasladado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IncluirIepsTrasladado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Complemento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estimulo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PalabrasSimilares = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VigenciaIni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VigenciaFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolBarButtonImportar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonExportar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonActualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonCerrar = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonImportar,
            this.ToolBarButtonExportar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonCerrar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1103, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.Descripcion,
            this.IncluirIVATrasladado,
            this.IncluirIepsTrasladado,
            this.Complemento,
            this.Estimulo,
            this.PalabrasSimilares,
            this.VigenciaIni,
            this.VigenciaFin});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1103, 425);
            this.dataGridView1.TabIndex = 1;
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            this.Clave.ReadOnly = true;
            this.Clave.Width = 70;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Width = 185;
            // 
            // IncluirIVATrasladado
            // 
            this.IncluirIVATrasladado.DataPropertyName = "IncluirIVATrasladado";
            this.IncluirIVATrasladado.HeaderText = "Incluir IVA Trasladado";
            this.IncluirIVATrasladado.Name = "IncluirIVATrasladado";
            // 
            // IncluirIepsTrasladado
            // 
            this.IncluirIepsTrasladado.DataPropertyName = "IncluirIepsTrasladado";
            this.IncluirIepsTrasladado.HeaderText = "Incluir IEPS Trasladado";
            this.IncluirIepsTrasladado.Name = "IncluirIepsTrasladado";
            // 
            // Complemento
            // 
            this.Complemento.DataPropertyName = "Complemento";
            this.Complemento.HeaderText = "Complemento";
            this.Complemento.Name = "Complemento";
            // 
            // Estimulo
            // 
            this.Estimulo.DataPropertyName = "Estimulo";
            this.Estimulo.HeaderText = "Estimulo";
            this.Estimulo.Name = "Estimulo";
            this.Estimulo.Width = 50;
            // 
            // PalabrasSimilares
            // 
            this.PalabrasSimilares.DataPropertyName = "PalabrasSimilares";
            this.PalabrasSimilares.HeaderText = "Palabras Similares";
            this.PalabrasSimilares.Name = "PalabrasSimilares";
            this.PalabrasSimilares.Width = 150;
            // 
            // VigenciaIni
            // 
            this.VigenciaIni.DataPropertyName = "VigenciaIni";
            this.VigenciaIni.HeaderText = "Ini. Vigencia";
            this.VigenciaIni.Name = "VigenciaIni";
            this.VigenciaIni.Width = 85;
            // 
            // VigenciaFin
            // 
            this.VigenciaFin.DataPropertyName = "VigenciaFin";
            this.VigenciaFin.HeaderText = "Fin Vigencia";
            this.VigenciaFin.Name = "VigenciaFin";
            this.VigenciaFin.Width = 85;
            // 
            // ToolBarButtonImportar
            // 
            this.ToolBarButtonImportar.Image = global::Jaeger.UI.Properties.Resources.color_xls_16px;
            this.ToolBarButtonImportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonImportar.Name = "ToolBarButtonImportar";
            this.ToolBarButtonImportar.Size = new System.Drawing.Size(73, 22);
            this.ToolBarButtonImportar.Text = "Importar";
            this.ToolBarButtonImportar.Click += new System.EventHandler(this.ToolBarButtonImportar_Click);
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.color_json_16px;
            this.ToolBarButtonExportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Size = new System.Drawing.Size(71, 22);
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.Click += new System.EventHandler(this.ToolBarButtonExportar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.color_refresh_16px;
            this.ToolBarButtonActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Size = new System.Drawing.Size(79, 22);
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.color_close_window_16px;
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Size = new System.Drawing.Size(59, 22);
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // ProdServRepositoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ProdServRepositoryForm";
            this.Text = "ProdServRepositoryForm";
            this.Load += new System.EventHandler(this.ProdServRepositoryForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripButton ToolBarButtonActualizar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonImportar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonExportar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonCerrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn IncluirIVATrasladado;
        private System.Windows.Forms.DataGridViewTextBoxColumn IncluirIepsTrasladado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Complemento;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Estimulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PalabrasSimilares;
        private System.Windows.Forms.DataGridViewTextBoxColumn VigenciaIni;
        private System.Windows.Forms.DataGridViewTextBoxColumn VigenciaFin;
    }
}