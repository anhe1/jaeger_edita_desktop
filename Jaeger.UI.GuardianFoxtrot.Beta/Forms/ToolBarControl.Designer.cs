﻿namespace Jaeger.UI.Forms {
    partial class ToolBarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.ToolBarButtonImportar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonExportar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonActualizar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonHerramientas = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolBarButtonCerrar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonCrearTabla = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonImportar,
            this.ToolBarButtonExportar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonCerrar});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(1054, 25);
            this.ToolBar.TabIndex = 1;
            this.ToolBar.Text = "toolStrip1";
            // 
            // ToolBarButtonImportar
            // 
            this.ToolBarButtonImportar.Image = global::Jaeger.UI.Properties.Resources.color_xls_16px;
            this.ToolBarButtonImportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonImportar.Name = "ToolBarButtonImportar";
            this.ToolBarButtonImportar.Size = new System.Drawing.Size(73, 22);
            this.ToolBarButtonImportar.Text = "Importar";
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.Image = global::Jaeger.UI.Properties.Resources.color_json_16px;
            this.ToolBarButtonExportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Size = new System.Drawing.Size(71, 22);
            this.ToolBarButtonExportar.Text = "Exportar";
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Properties.Resources.color_refresh_16px;
            this.ToolBarButtonActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Size = new System.Drawing.Size(79, 22);
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonCrearTabla});
            this.ToolBarButtonHerramientas.Image = global::Jaeger.UI.Properties.Resources.color_toolbox_16px;
            this.ToolBarButtonHerramientas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Size = new System.Drawing.Size(107, 22);
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Properties.Resources.color_close_window_16px;
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Size = new System.Drawing.Size(59, 22);
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // ToolBarButtonCrearTabla
            // 
            this.ToolBarButtonCrearTabla.Name = "ToolBarButtonCrearTabla";
            this.ToolBarButtonCrearTabla.Size = new System.Drawing.Size(180, 22);
            this.ToolBarButtonCrearTabla.Text = "Crear Tabla";
            this.ToolBarButtonCrearTabla.Click += new System.EventHandler(this.ToolBarButtonCrearTabla_Click);
            // 
            // ToolBarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolBar);
            this.Name = "ToolBarControl";
            this.Size = new System.Drawing.Size(1054, 25);
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolBar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonImportar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonExportar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonActualizar;
        private System.Windows.Forms.ToolStripButton ToolBarButtonCerrar;
        private System.Windows.Forms.ToolStripDropDownButton ToolBarButtonHerramientas;
        private System.Windows.Forms.ToolStripMenuItem ToolBarButtonCrearTabla;
    }
}
