﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class ToolBarControl : UserControl {
        public event EventHandler<EventArgs> ButtonActualizar_Click;
        public event EventHandler<EventArgs> ButtonCrearTabla_Click;
        public event EventHandler<EventArgs> ButtonCerrar_Click;

        public void OnButtonActualizarClick(object sender, EventArgs e) {
            if (this.ButtonActualizar_Click != null) {
                this.ButtonActualizar_Click(sender, e);
            }
        }

        public void OnButtonCerrarClick(object sender, EventArgs e) {
            if (this.ButtonCerrar_Click != null) {
                this.ButtonCerrar_Click(sender, e);
            }
        }

        public void OnButtonCrearTablaClick(object sender, EventArgs e) {
            if (this.ButtonCrearTabla_Click != null) {
                this.ButtonCrearTabla_Click(sender, e);
            }
        }

        public ToolBarControl() {
            InitializeComponent();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.OnButtonCerrarClick(sender, e);
        }

        private void ToolBarButtonCrearTabla_Click(object sender, EventArgs e) {
            this.OnButtonCrearTablaClick(sender, e);
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            this.ButtonActualizar_Click(sender, e);
        }
    }
}
