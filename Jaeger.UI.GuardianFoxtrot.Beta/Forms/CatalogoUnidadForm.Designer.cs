﻿namespace Jaeger.UI.Forms {
    partial class CatalogoUnidadForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IncluirIVATrasladado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IncluirIepsTrasladado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Complemento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estimulo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PalabrasSimilares = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VigenciaIni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VigenciaFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolBar = new Jaeger.UI.Forms.ToolBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.Descripcion,
            this.IncluirIVATrasladado,
            this.IncluirIepsTrasladado,
            this.Complemento,
            this.Estimulo,
            this.PalabrasSimilares,
            this.VigenciaIni,
            this.VigenciaFin});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 425);
            this.dataGridView1.TabIndex = 2;
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            this.Clave.ReadOnly = true;
            this.Clave.Width = 70;
            // 
            // Descripcion
            // 
            this.Descripcion.DataPropertyName = "Descripcion";
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Width = 185;
            // 
            // IncluirIVATrasladado
            // 
            this.IncluirIVATrasladado.DataPropertyName = "IncluirIVATrasladado";
            this.IncluirIVATrasladado.HeaderText = "Incluir IVA Trasladado";
            this.IncluirIVATrasladado.Name = "IncluirIVATrasladado";
            // 
            // IncluirIepsTrasladado
            // 
            this.IncluirIepsTrasladado.DataPropertyName = "IncluirIepsTrasladado";
            this.IncluirIepsTrasladado.HeaderText = "Incluir IEPS Trasladado";
            this.IncluirIepsTrasladado.Name = "IncluirIepsTrasladado";
            // 
            // Complemento
            // 
            this.Complemento.DataPropertyName = "Complemento";
            this.Complemento.HeaderText = "Complemento";
            this.Complemento.Name = "Complemento";
            // 
            // Estimulo
            // 
            this.Estimulo.DataPropertyName = "Estimulo";
            this.Estimulo.HeaderText = "Estimulo";
            this.Estimulo.Name = "Estimulo";
            this.Estimulo.Width = 50;
            // 
            // PalabrasSimilares
            // 
            this.PalabrasSimilares.DataPropertyName = "PalabrasSimilares";
            this.PalabrasSimilares.HeaderText = "Palabras Similares";
            this.PalabrasSimilares.Name = "PalabrasSimilares";
            this.PalabrasSimilares.Width = 150;
            // 
            // VigenciaIni
            // 
            this.VigenciaIni.DataPropertyName = "VigenciaIni";
            this.VigenciaIni.HeaderText = "Ini. Vigencia";
            this.VigenciaIni.Name = "VigenciaIni";
            this.VigenciaIni.Width = 85;
            // 
            // VigenciaFin
            // 
            this.VigenciaFin.DataPropertyName = "VigenciaFin";
            this.VigenciaFin.HeaderText = "Fin Vigencia";
            this.VigenciaFin.Name = "VigenciaFin";
            this.VigenciaFin.Width = 85;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonActualizar_Click);
            this.ToolBar.ButtonCrearTabla_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonCrearTabla_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonCerrar_Click);
            // 
            // CatalogoUnidadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ToolBar);
            this.Name = "CatalogoUnidadForm";
            this.Text = "CatalogoUnidadForm";
            this.Load += new System.EventHandler(this.CatalogoUnidadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ToolBarControl ToolBar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn IncluirIVATrasladado;
        private System.Windows.Forms.DataGridViewTextBoxColumn IncluirIepsTrasladado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Complemento;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Estimulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PalabrasSimilares;
        private System.Windows.Forms.DataGridViewTextBoxColumn VigenciaIni;
        private System.Windows.Forms.DataGridViewTextBoxColumn VigenciaFin;
    }
}