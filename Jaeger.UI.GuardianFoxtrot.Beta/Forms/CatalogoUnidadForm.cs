﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Contracts;
using Jaeger.Aplication.Services;
using Jaeger.Catalogos.Entities;

namespace Jaeger.UI.Forms {
    public partial class CatalogoUnidadForm : Form {
        protected ICatalogoUnidadService service;
        private BindingList<ClaveUnidadModel> unidades;
        public CatalogoUnidadForm() {
            InitializeComponent();
        }

        private void CatalogoUnidadForm_Load(object sender, EventArgs e) {
            this.service = new CatalogoUnidadService();
            
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBar_ButtonCrearTabla_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.CrearTabla)) {
                espera.Text = "Creando tabla";
                espera.ShowDialog(this);
            }
        }

        private void CrearTabla() {
            this.service.Crear();
        }

        private void Actualizar() {
            this.unidades = this.service.GetList();
        }
    }
}
