﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms { 
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            ConfigService.Synapsis = new Configuracion() { RDS = new BaseDatos { Edita = new DataBaseConfiguracion { Database = @"C:\Jaeger\Jaeger.Catalogos\Catalogos.sqlite" } } };
            ConfigService.Piloto = new Domain.Base.Profile.KaijuLogger();
            ConfigService.Piloto.Roles.Add(new Rol());
            (ConfigService.Piloto.Roles[0] as Rol).Name = "desarrollador";
            ConfigService.Menus = this.Menus();
            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            MenuPermissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);
        }

        private void Menu_Archivo_Salir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Menu_AcercaDe_Click(object sender, EventArgs e) {
            var acercaDe_Form = new AboutBoxForm();
            acercaDe_Form.ShowDialog(this);
        }

        private void Menu_striker_beta_Click(object sender, EventArgs e) {
            var item = (ToolStripMenuItem)sender;
            var localAssembly = Assembly.Load("Jaeger.UI.Beta.GuardianFoxtrot");

            if (item.Tag != null) {
                try {
                    var d = localAssembly.GetTypes();
                    Type type = localAssembly.GetType("Jaeger.UI." + item.Tag.ToString(), true);
                    this.Form_Active(type);
                } catch (Exception ex) {
                    MessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            } else {
                MessageBox.Show(this, "No se implemento el formulario requerido.", "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Form_Active(Type type) {
            Form form = (Form)Activator.CreateInstance(type);
            form.MdiParent = this;
            form.Size = this.Size;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        public List<UIMenuElement> Menus() {
            var response = new List<UIMenuElement>();
            response.Add(new UIMenuElement { Name = "menu_Archivo", Label = "Archivo" });
            response.Add(new UIMenuElement { Name = "menu_Archivo_DBAbrir", Label = "Abrir Base de Datos" });
            response.Add(new UIMenuElement { Name = "menu_Archivo_Salir", Label = "Salir", Rol = "*" });
            response.Add(new UIMenuElement { Name = "menu_Catalogos", Label = "Catálogos" });
            response.Add(new UIMenuElement { Name = "menu_Catalogo_Aduana", Label = "Catálogo de Aduanas" });
            response.Add(new UIMenuElement { Name = "menu_Catalogo_ClaveUnidad", Label = "Catálogo de Unidad" });
            response.Add(new UIMenuElement { Name = "menu_Herramientas", Label = "Herramientas" });
            response.Add(new UIMenuElement { Name = "menu_Herramientas_Repositorio", Label = "Repositorio" });
            response.Add(new UIMenuElement { Name = "menu_Ventana", Label = "Ventana", Rol = "*" });
            response.Add(new UIMenuElement { Name = "menu_Ayuda", Label = "Ayuda", Rol = "*" });
            return response;
        }

        private void menu_Herramientas_CrearDB_Click(object sender, EventArgs e) {

        }
    }
}
