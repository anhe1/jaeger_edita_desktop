﻿namespace Jaeger.UI.Forms {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Archivo_DBAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Archivo_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Catalogos = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Herramientas = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Herramientas_CrearDB = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Herramientas_Repositorio = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Ventana = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Ayuda = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_AcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Catalogo_Aduana = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Catalogo_ClaveUnidad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Archivo,
            this.menu_Catalogos,
            this.menu_Herramientas,
            this.menu_Ventana,
            this.menu_Ayuda});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.menu_Ventana;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu_Archivo
            // 
            this.menu_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Archivo_DBAbrir,
            this.menu_Archivo_Salir});
            this.menu_Archivo.Name = "menu_Archivo";
            this.menu_Archivo.Size = new System.Drawing.Size(96, 20);
            this.menu_Archivo.Text = "menu_Archivo";
            // 
            // menu_Archivo_DBAbrir
            // 
            this.menu_Archivo_DBAbrir.Name = "menu_Archivo_DBAbrir";
            this.menu_Archivo_DBAbrir.Size = new System.Drawing.Size(197, 22);
            this.menu_Archivo_DBAbrir.Text = "menu_Archivo_DBAbrir";
            // 
            // menu_Archivo_Salir
            // 
            this.menu_Archivo_Salir.Name = "menu_Archivo_Salir";
            this.menu_Archivo_Salir.Size = new System.Drawing.Size(197, 22);
            this.menu_Archivo_Salir.Text = "menu_Archivo_Salir";
            this.menu_Archivo_Salir.Click += new System.EventHandler(this.Menu_Archivo_Salir_Click);
            // 
            // menu_Catalogos
            // 
            this.menu_Catalogos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Catalogo_Aduana,
            this.menu_Catalogo_ClaveUnidad});
            this.menu_Catalogos.Name = "menu_Catalogos";
            this.menu_Catalogos.Size = new System.Drawing.Size(108, 20);
            this.menu_Catalogos.Text = "menu_Catalogos";
            // 
            // menu_Herramientas
            // 
            this.menu_Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Herramientas_CrearDB,
            this.menu_Herramientas_Repositorio});
            this.menu_Herramientas.Name = "menu_Herramientas";
            this.menu_Herramientas.Size = new System.Drawing.Size(126, 20);
            this.menu_Herramientas.Text = "menu_Herramientas";
            // 
            // menu_Herramientas_CrearDB
            // 
            this.menu_Herramientas_CrearDB.Name = "menu_Herramientas_CrearDB";
            this.menu_Herramientas_CrearDB.Size = new System.Drawing.Size(170, 22);
            this.menu_Herramientas_CrearDB.Text = "menu_CrearDB";
            this.menu_Herramientas_CrearDB.Click += new System.EventHandler(this.menu_Herramientas_CrearDB_Click);
            // 
            // menu_Herramientas_Repositorio
            // 
            this.menu_Herramientas_Repositorio.Name = "menu_Herramientas_Repositorio";
            this.menu_Herramientas_Repositorio.Size = new System.Drawing.Size(170, 22);
            this.menu_Herramientas_Repositorio.Tag = "Forms.ProdServRepositoryForm";
            this.menu_Herramientas_Repositorio.Text = "menu_Repositorio";
            this.menu_Herramientas_Repositorio.Click += new System.EventHandler(this.Menu_striker_beta_Click);
            // 
            // menu_Ventana
            // 
            this.menu_Ventana.Name = "menu_Ventana";
            this.menu_Ventana.Size = new System.Drawing.Size(97, 20);
            this.menu_Ventana.Text = "menu_Ventana";
            // 
            // menu_Ayuda
            // 
            this.menu_Ayuda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_AcercaDe});
            this.menu_Ayuda.Name = "menu_Ayuda";
            this.menu_Ayuda.Size = new System.Drawing.Size(89, 20);
            this.menu_Ayuda.Text = "menu_Ayuda";
            // 
            // menu_AcercaDe
            // 
            this.menu_AcercaDe.Name = "menu_AcercaDe";
            this.menu_AcercaDe.Size = new System.Drawing.Size(160, 22);
            this.menu_AcercaDe.Text = "menu_AcercaDe";
            this.menu_AcercaDe.Click += new System.EventHandler(this.Menu_AcercaDe_Click);
            // 
            // menu_Catalogo_Aduana
            // 
            this.menu_Catalogo_Aduana.Name = "menu_Catalogo_Aduana";
            this.menu_Catalogo_Aduana.Size = new System.Drawing.Size(177, 22);
            this.menu_Catalogo_Aduana.Tag = "Forms.CatalogoAduanaForm";
            this.menu_Catalogo_Aduana.Text = "menu_Aduana";
            // 
            // menu_Catalogo_ClaveUnidad
            // 
            this.menu_Catalogo_ClaveUnidad.Name = "menu_Catalogo_ClaveUnidad";
            this.menu_Catalogo_ClaveUnidad.Size = new System.Drawing.Size(177, 22);
            this.menu_Catalogo_ClaveUnidad.Tag = "Forms.CatalogoUnidadForm";
            this.menu_Catalogo_ClaveUnidad.Text = "menu_ClaveUnidad";
            this.menu_Catalogo_ClaveUnidad.Click += new System.EventHandler(this.Menu_striker_beta_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "SAT - Catálogos";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo_Salir;
        private System.Windows.Forms.ToolStripMenuItem menu_Catalogos;
        private System.Windows.Forms.ToolStripMenuItem menu_Herramientas;
        private System.Windows.Forms.ToolStripMenuItem menu_Ventana;
        private System.Windows.Forms.ToolStripMenuItem menu_Ayuda;
        private System.Windows.Forms.ToolStripMenuItem menu_AcercaDe;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo_DBAbrir;
        private System.Windows.Forms.ToolStripMenuItem menu_Herramientas_Repositorio;
        private System.Windows.Forms.ToolStripMenuItem menu_Herramientas_CrearDB;
        private System.Windows.Forms.ToolStripMenuItem menu_Catalogo_Aduana;
        private System.Windows.Forms.ToolStripMenuItem menu_Catalogo_ClaveUnidad;
    }
}