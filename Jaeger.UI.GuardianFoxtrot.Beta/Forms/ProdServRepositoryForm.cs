﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Contracts;
using Jaeger.Aplication.Services;
using Jaeger.Catalogos.Entities;

namespace Jaeger.UI.Forms {
    public partial class ProdServRepositoryForm : Form {
        protected ICatalogoService service;
        private BindingList<ClaveProdServModel> productos;
        public ProdServRepositoryForm() {
            InitializeComponent();
        }

        private void ProdServRepositoryForm_Load(object sender, EventArgs e) {
            this.dataGridView1.DataGridCommon();
            this.service = new CatalogoService();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.dataGridView1.DataSource = this.productos;
        }

        private void Consultar() {
            this.productos = this.service.GetList();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonImportar_Click(object sender, EventArgs e) {
            //var d = new Catalogos.Repositories.ProdServsCatalogo();
            //d.Load();
            //this.productos = new BindingList<ClaveProdServModel>();
            //foreach (var item in d.Items) {
            //    this.productos.Add(new ClaveProdServModel { Clave = item.Clave, Complemento = item.Complemento, Descripcion = item.Descripcion, Estimulo = item.Estimulo, IncluirIepsTrasladado = item.IncluirIepsTrasladado, IncluirIvaTrasladado = item.IncluirIvaTrasladado, PalabrasSimilares = item.PalabrasSimilares, VigenciaFin = item.VigenciaFin, VigenciaIni = item.VigenciaIni });
            //}
            //this.service.Save(this.productos.ToList());
            var openFile = new OpenFileDialog() { DefaultExt = "xls", Filter = "*.xls|*.XLS" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                var d = new CatalogoExcelService();
                d.ExcelFile = openFile.FileName;
                d.Reader();
                this.service.Save(d.Datos("c_ClaveProdServ"));
                //this.dataGridView1.DataSource = d.Datos("c_ClaveProdServ");
            }
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {
            this.service.Crear();
        }
    }
}
