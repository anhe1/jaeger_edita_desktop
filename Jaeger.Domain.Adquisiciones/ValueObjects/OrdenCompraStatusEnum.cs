using System.ComponentModel;

namespace Jaeger.Domain.Adquisiciones.ValueObjects {
    public enum OrdenCompraStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Requerido")]
        Requerida = 1,
        [Description("Autorizado")]
        Autorizada = 2,
        [Description("Ingresada")]
        Ingresada = 3
    }
}