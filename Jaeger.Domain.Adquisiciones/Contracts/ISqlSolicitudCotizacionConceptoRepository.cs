﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Adquisiciones.Contracts {
    public interface ISqlSolicitudCotizacionConceptoRepository : IGenericRepository<SolicitudCotizacionConceptoModel> {
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();

        SolicitudCotizacionConceptoModel Save(SolicitudCotizacionConceptoModel model);

        bool CreateTable();
    }
}