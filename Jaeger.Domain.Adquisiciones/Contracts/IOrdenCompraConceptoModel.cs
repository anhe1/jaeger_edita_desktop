using System;

namespace Jaeger.Domain.Adquisiciones.Interfces {
    public interface IOrdenCompraConcepto {
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con la orden de compra
        /// </summary>
        int SubId { get; set; }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece
        /// </summary>
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establcer el tipo de producto (producto o servicio)
        /// </summary>
        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer el indice del producto o servicio
        /// </summary>
        int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer el numero de orden relacionado al producto
        /// </summary>
        int OrdenProduccion { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad de decimales a utilizar para el unitario
        /// </summary>
        int Decimales { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad de bienes o servicios del tipo particular definido por el presente concepto.
        /// <xs:fractionDigits value="6"/>
        ///<xs:minInclusive value = "0.000001" />
        /// </summary>
        decimal Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer el valor unitario del modelo
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el unitario base del catalogo de productos
        /// </summary>
        decimal UnitarioBase { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        string FactorTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        decimal? ValorTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS trasladado, puede ser TASA � CUOTA
        /// </summary>
        string FactorTrasladoIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor % del impuesto IEPS, puede ser valor FIJO o RANGO, si el factor es TASA es fijo, si el factor es CUOTA puede ser rango
        /// </summary>
        decimal? ValorTrasladoIEPS { get; set; }

        ///// <summary>
        ///// obtener o establecer el factor del impuesto retenido IVA, puese ser TASA
        ///// </summary>
        //string FactorRetencionIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA, puede ser un rango cuando el factor es TASA
        /// </summary>
        decimal? ValorRetencionIVA { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS que puede ser TASA � CUOTA
        /// </summary>
        string FactorRetencionIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS, puede ser FIJO si al factor es TASA � RANGO si el factor es CUOTA
        /// </summary>
        decimal? ValorRetencionIEPS { get; set; }

        ///// <summary>
        ///// obtener o establecer el factor del impuesto retenido ISR, es este caso siempre es TASA
        ///// </summary>
        //string FactorRetencionISR { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido ISR, si el factor es TASA el valor puede ser un RANGO
        /// </summary>
        decimal? ValorRetencionISR { get; set; }

        /// <summary>
        /// obtener o establecer el importe de los descuentos aplicables al concepto. No se permiten valores negativos.
        /// </summary>
        decimal Descuento { get; set; }

        /// <summary>
        /// obtener o establecer el importe total de los bienes o servicios del presente concepto. Debe ser equivalente al resultado de multiplicar la cantidad por el valor unitario expresado 
        /// en el concepto. No se permiten valores negativos. 
        /// </summary>
        decimal Importe { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operaci�n del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,20}"
        /// </summary>
        string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.
        /// </summary>
        string ClaveUnidad { get; set; }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del cat�logo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        string ClaveProdServ { get; set; }

        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer la descripci�n del bien o servicio cubierto por el presente concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,1000}"
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer el n�mero de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operaci�n del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del est�ndar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,100}"
        string NoIdentificacion { get; set; }

        string Especificacion { get; set; }

        /// <summary>
        /// Desc:marca del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        string Marca { get; set; }

        DateTime? FechaRequerida { get; set; }

        DateTime FechaNuevo { get; set; }

        DateTime? FechaModifica { get; set; }

        string Creo { get; set; }

        string Modifica { get; set; }
    }
}