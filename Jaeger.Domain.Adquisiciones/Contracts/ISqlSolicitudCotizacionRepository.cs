﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Adquisiciones.Contracts {
    public interface ISqlSolicitudCotizacionRepository : IGenericRepository<SolicitudCotizacionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        new SolicitudCotizacionDetailModel GetById(int index);

        SolicitudCotizacionDetailModel Save(SolicitudCotizacionDetailModel model);

        void Crear();
    }
}