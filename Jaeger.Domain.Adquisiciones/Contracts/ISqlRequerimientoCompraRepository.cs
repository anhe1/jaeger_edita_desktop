﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.Domain.Adquisiciones.Contracts {
    /// <summary>
    /// repositorio de requerimientos de compra de producto o servicios
    /// </summary>
    public interface ISqlRequerimientoCompraRepository : IGenericRepository<RequerimientoCompraModel> {
        /// <summary>
        /// almacenar requerimiento de compra
        /// </summary>
        RequerimientoCompraDetailModel Save(RequerimientoCompraDetailModel requerimiento);

        /// <summary>
        /// cancelar requerimiento de compra
        /// </summary>
        bool Cancelar(int index, string usuario);

        /// <summary>
        /// crear tablas necesarias
        /// </summary>
        bool CrearTablas();
    }
}