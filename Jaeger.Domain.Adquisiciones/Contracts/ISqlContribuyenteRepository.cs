﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.Domain.Adquisiciones.Contracts {
    public interface ISqlContribuyenteRepository : IGenericRepository<OrdenCompraProveedorModel> {
        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        /// <param name="relacion">relacion con el directorio</param>
        IEnumerable<OrdenCompraProveedorModel> GetList(string relacion);

        IEnumerable<OrdenCompraProveedorDetailModel> GetList(string relacion, bool onlyActive);
    }
}
