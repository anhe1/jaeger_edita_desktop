using System;
using Jaeger.Domain.Adquisiciones.ValueObjects;

namespace Jaeger.Domain.Adquisiciones.Interfces {
    public interface IOrdenCompra {
        int Id { get; set; }

        string Version { get; set; }

        int IdStatus { get; set; }

        int Folio { get; set; }

        string Serie { get; set; }

        int IdProveedor { get; set; }

        string ReceptorNombre { get; set; }

        string ReceptorRFC { get; set; }

        string Contacto { get; set; }

        string Correo { get; set; }

        DateTime FechaEmision { get; set; }

        DateTime? FechaAutoriza { get; set; }

        DateTime? FechaRequerida { get; set; }

        decimal SubTotal { get; set; }

        decimal Descuento { get; set; }
    }
}