using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Adquisiciones.Contracts {
    public interface ISqlOrdenCompraRepository : IGenericRepository<OrdenCompraModel> {
        /// <summary>
        /// listado de ordenes de compra por año y mes
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <returns></returns>
        IEnumerable<OrdenCompraDetailModel> GetList(int month, int year, int index = 0);

        IEnumerable<OrdenCompraSingleModel> GetList(int month, int year);

        /// <summary>
        /// listado simple de ordenes de compra
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        IEnumerable<OrdenCompraSingleModel> GetList(List<Conditional> conditionals);

        OrdenCompraDetailModel GetByFolio(int folio);

        new OrdenCompraDetailModel GetById(int index);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        OrdenCompraDetailModel Save(OrdenCompraDetailModel item);

        /// <summary>
        /// cancelar una orden de compra
        /// </summary>
        bool Cancelar(OrdenCompraModel item, string usuario);

        /// <summary>
        /// listado de ordenes de compra por año y mes
        /// </summary>
        /// <param name="mes">mes</param>
        /// <param name="anio">año</param>
        /// <returns></returns>
        //IEnumerable<OrdenCompraSingle> GetList(MonthsOfYearEnum mes, int anio);

        OrdenCompraConceptoDetailModel Save(OrdenCompraConceptoDetailModel item);

        bool CrearTablas();
    }
}