﻿using System;
using SqlSugar;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// concepto de orden de compra 
    /// </summary>
    [SugarTable("_ordcns", "proveedor: orden de compra")]
    public class SolicitudCotizacionConceptoModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int subIndex;
        private int almacenInt;
        private int tipoInt;
        private int idProducto;
        private int idModelo;
        private int idUnidad;
        private decimal cantidad;
        private string unidad;
        private string descripcion;
        private string especificacion;
        private string marca;
        private string nota;
        private string creo;
        private string modifica;
        private DateTime? fechaRequerida;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string producto;
        #endregion

        public SolicitudCotizacionConceptoModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        #region propiedades
        [SugarColumn(ColumnName = "_ordcns_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdConcepto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_a", ColumnDescription = "obtener o establecer si el registro se encuentra activo", Length = 1, IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la orden de compra
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_sbid", ColumnDescription = "obtener o establecer el indice de relacion con la orden de compra")]
        public int IdSolicitud {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_alm_id", ColumnDescription = "obtener o establecer el almacen al que pertenece")]
        public int IdAlmacen {
            get {
                return this.almacenInt;
            }
            set {
                this.almacenInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el tipo de producto (producto o servicio)
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_tipo", ColumnDescription = "obtener o establcer el tipo de producto (producto o servicio)")]
        public int IdTipo {
            get {
                return this.tipoInt;
            }
            set {
                this.tipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del producto o servicio
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_prd_id", ColumnDescription = "obtener o establecer el indice del producto o servicio")]
        public int IdProducto {
            get {
                return this.idProducto;
            }
            set {
                this.idProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_mdl_id", ColumnDescription = "obtener o establecer el indice del modelo")]
        public int IdModelo {
            get {
                return this.idModelo;
            }
            set {
                this.idModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_undd_id", ColumnDescription = "obtener o establecer la unidad de almacen")]
        public int IdUnidad {
            get {
                return this.idUnidad;
            }
            set {
                this.idUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de bienes o servicios del tipo particular definido por el presente concepto.
        /// <xs:fractionDigits value="6"/>
        ///<xs:minInclusive value = "0.000001" />
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_cant", ColumnDescription = "obtener o establecer la cantidad requerida del producto o servicio", Length = 14, DecimalDigits = 4)]
        public decimal Cantidad {
            get {
                return this.cantidad;
            }
            set {
                this.cantidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_undd", ColumnDescription = "obtener o establecer la unidad del almacen", Length = 20, IsNullable = true)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del bien o servicio cubierto por el presente concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,1000}"
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_prdct", ColumnDescription = "obtener o establecer la descripción del bien o servicio cubierto por el presente concepto.", Length = 200, IsNullable = true)]
        public string Producto {
            get {
                return this.producto;
            }
            set {
                this.producto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del bien o servicio cubierto por el presente concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,1000}"
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_dscrc", ColumnDescription = "obtener o establecer la descripción del bien o servicio cubierto por el presente concepto.", Length = 1000, IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer especificacion del producto o servicio
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_espc", ColumnDescription = "alm:|mp,pt| desc: espcificaciones", IsNullable = true, Length = 128)]
        public string Especificacion {
            get {
                return this.especificacion;
            }
            set {
                this.especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre de la marca o fabricante
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_mrc", ColumnDescription = "alm:|mp,pt| desc: marca o fabricante", Length = 128, IsNullable = true)]
        public string Marca {
            get {
                return this.marca;
            }
            set {
                this.marca = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones del concepto
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_nota", ColumnDescription = "alm:|mp,pt| observaciones", Length = 128, IsNullable = true)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de requerimiento
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_fecreq", ColumnDescription = "obtener o establecer la fecha de requerimiento", IsNullable = true)]
        public DateTime? FechaRequerida {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRequerida >= firstGoodDate)
                    return this.fechaRequerida;
                return null;
            }
            set {
                this.fechaRequerida = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true, ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcns_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsOnlyIgnoreUpdate = true, IsNullable = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_fm", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_ordcns_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
