﻿using SqlSugar;
using System.ComponentModel;

namespace Jaeger.Domain.Adquisiciones.Entities {
    [SugarTable("_ordcms", "proveedor: solicitud de cotizacion")]
    public class SolicitudCotizacionDetailModel : SolicitudCotizacionModel {
        private BindingList<SolicitudCotizacionConceptoModel> _Conceptos;
        public SolicitudCotizacionDetailModel() : base() {
            this._Conceptos = new BindingList<SolicitudCotizacionConceptoModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get { return this.IdSolicitud == 0; }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<SolicitudCotizacionConceptoModel> Conceptos {
            get { return this._Conceptos; }
            set {
                if (this._Conceptos != null) {
                    this.Conceptos.AddingNew -= new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this.Conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {

        }
    }
}
