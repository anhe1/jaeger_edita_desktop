﻿using System;
using Jaeger.Domain.Adquisiciones.ValueObjects;
using SqlSugar;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// clase para la vista de ordenes de compra
    /// </summary>
    [SugarTable("_ordcmp", "proveedor: orden de compra")]
    public class OrdenCompraSingleModel : OrdenCompraModel {

        public OrdenCompraSingleModel() : base() {
            
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public OrdenCompraStatusEnum Status {
            get {
                if (this.IdStatus == 0)
                    return OrdenCompraStatusEnum.Cancelado;
                else if (this.IdStatus == 1)
                    return OrdenCompraStatusEnum.Requerida;
                else if (this.IdStatus == 2)
                    return OrdenCompraStatusEnum.Autorizada;
                else if (this.IdStatus == 3)
                    return OrdenCompraStatusEnum.Ingresada;
                else
                    return OrdenCompraStatusEnum.Requerida;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }
    }
}