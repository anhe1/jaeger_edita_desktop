﻿namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraStatusModel : Base.Abstractions.BaseSingleModel {
        public OrdenCompraStatusModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}