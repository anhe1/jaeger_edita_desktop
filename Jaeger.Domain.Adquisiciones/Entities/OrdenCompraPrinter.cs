﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraPrinter : OrdenCompraDetailModel {

        public OrdenCompraPrinter() : base() {

        }

        public OrdenCompraPrinter(OrdenCompraDetailModel model) : base() {
            MapperClassExtensions.MatchAndMap<OrdenCompraDetailModel, OrdenCompraPrinter>(model, this);

            //Acumulado = model.Acumulado;
            //Autoriza = model.Autoriza;
            //Cancela = model.Cancela;
            //ClaveFormaPago = model.ClaveFormaPago;
            //ClaveMetodoPago = model.ClaveMetodoPago;
            //ClaveUsoCFDI = model.ClaveUsoCFDI;
            //CondicionPago = model.CondicionPago;
            //Contacto = model.Contacto;
            //Correo = model.Correo;
            //Creo = model.Creo;
            //Decimales = model.Decimales;
            //Descuento = model.Descuento;
            //EntregarEn = model.EntregarEn;
            //FechaAutoriza = model.FechaAutoriza;
            //FechaCancela = model.FechaCancela;
            //FechaEmision = model.FechaEmision;
            //FechaModifica = model.FechaModifica;
            //FechaRecepcion = model.FechaRecepcion;
            //FechaRequerida = model.FechaRequerida;
            //Folio = model.Folio;
            //Id = model.Id;
            //IdProveedor = model.IdProveedor;
            //IdStatus = model.IdStatus;
            //MetodoEnvio = model.MetodoEnvio;
            //Modifico = model.Modifico;
            //Nota = model.Nota;
            //OrdenRelacionada = model.OrdenRelacionada;
            //ReceptorNombre = model.ReceptorNombre;
            //ReceptorRFC = model.ReceptorRFC;
            //RetencionIEPS = model.RetencionIEPS;
            //RetencionISR = model.RetencionISR;
            //RetencionIVA = model.RetencionIVA;
            //Serie = model.Serie;
            //Status = model.Status;
            //SubTotal = model.SubTotal;
            //TrasladoIEPS = model.TrasladoIEPS;
            //TrasladoIVA = model.TrasladoIVA;
            //Version = model.Version;
            //Total = model.Total;
            //// aqui para no modificar los subtotales e impuestos
            //Conceptos = model.Conceptos;
            this.Configuracion = new OrdenCompraConfig();
        }

        public OrdenCompraConfig Configuracion { get; set; }

        public string FolioToString() {
            return this.Folio.ToString("#0000");
        }

        public string Concepto {
            get {
                return this.Configuracion.Concepto.Replace("@folio", this.FolioToString());
            }
        }

        public string LeyendaPiePagina() {
            return this.Configuracion.LeyendaPiePagina;
        }

        public string TotalEnLetra {
            get; set;
        }

        public string[] QrText() {
            return new string[] { "||OC|", this.Version, "|", this.FolioToString(), "|", this.FechaEmision.ToShortDateString(), "|receptor=", this.ReceptorRFC, "|total=",
                this.Total.ToString(),"|Articulos=", this.Conceptos.Count.ToString(),"|", "creo=", this.Creo, "||" };
        }

        public string KeyName() {
            return string.Format("OC-{0}-{1}-{2}-{3}.pdf", this.Serie, this.FolioToString(), this.ReceptorRFC, this.FechaEmision.ToString("yyyyMMddHHmmss"));
        }
    }
}
