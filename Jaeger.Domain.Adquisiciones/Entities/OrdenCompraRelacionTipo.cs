﻿namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraRelacionTipo : Base.Abstractions.BaseSingleTipoModel {
        public OrdenCompraRelacionTipo(string id, string descripcion) : base(id, descripcion) {
        }
    }
}