﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraRel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private int idProveedor;
        private int folio;
        private int statusInt;
        private decimal total;
        private string version;
        private string serie;
        private string creo;
        private string rFC;
        private string proveedor;
        private string contacto;
        private DateTime fechaEmision;
        #endregion

        #region constructor
        public OrdenCompraRel() {
            this.Version = "2.0";
            this.FechaEmision = DateTime.Now;
        }
        #endregion

        #region propiedades
        /// <summary>
        /// obtener o establecer el indice de la orden de compra
        /// </summary>
        [DataNames("_ordcmp_id")]
        [SugarColumn(ColumnName = "_ordcmp_id", ColumnDescription = "indice", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del proveedor
        /// </summary>
        [DataNames("_ordcmp_drctr_id")]
        [SugarColumn(ColumnName = "_ordcmp_drctr_id", ColumnDescription = "indice del directorio de proveedores", IsNullable = false, Length = 11)]
        public int IdProveedor {
            get {
                return this.idProveedor;
            }
            set {
                this.idProveedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de la orden de compra
        /// </summary>
        [DataNames("_ordcmp_folio")]
        [SugarColumn(ColumnName = "_ordcmp_folio", ColumnDescription = "folio de la orden de compra", IsNullable = false, Length = 11)]
        public int Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_stts")]
        [SugarColumn(ColumnName = "_ordcmp_stts", ColumnDescription = "estado del comprobante", IsNullable = false, Length = 1)]
        public int IdStatus {
            get {
                return this.statusInt;
            }
            set {
                this.statusInt = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_ttl")]
        [SugarColumn(ColumnName = "_ordcmp_ttl", ColumnDescription = "total de la orden de compra", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return Math.Round(this.total, 4);
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_fn")]
        [SugarColumn(ColumnName = "_ordcmp_fn", ColumnDescription = "fecha del sistema", IsNullable = false)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_ver")]
        [SugarColumn(ColumnName = "_ordcmp_ver", ColumnDescription = "version del comprobante", IsNullable = false, Length = 3)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_sre")]
        [SugarColumn(ColumnName = "_ordcmp_sre", ColumnDescription = "serie", IsNullable = true, Length = 10)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_usr_n")]
        [SugarColumn(ColumnName = "_ordcmp_usr_n", ColumnDescription = "clave del usuario", IsNullable = false, Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del proveedor
        /// </summary>
        [DataNames("_ordcmp_rfc")]
        [SugarColumn(ColumnName = "_ordcmp_rfc", ColumnDescription = "rfc del proveedor", IsNullable = true, Length = 14)]
        public string ReceptorRFC {
            get {
                return this.rFC;
            }
            set {
                this.rFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del proveedor
        /// </summary>
        [DataNames("_ordcmp_nom")]
        [SugarColumn(ColumnName = "_ordcmp_nom", ColumnDescription = "nombre del proveedor", IsNullable = true, Length = 128)]
        public string ReceptorNombre {
            get {
                return this.proveedor;
            }
            set {
                this.proveedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto del proveedor
        /// </summary>
        [DataNames("_ordcmp_cntc")]
        [SugarColumn(ColumnName = "_ordcmp_cntc", ColumnDescription = "nombre del contacto del proveedor", IsNullable = true, Length = 128)]
        public string Contacto {
            get {
                return this.contacto;
            }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}