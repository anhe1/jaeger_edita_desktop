﻿using System;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Adquisiciones.Interfces;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// almacen de materia prima: ordenes de compra
    /// </summary>
    [SugarTable("_ordcmp", "proveedor: orden de compra")]
    public class OrdenCompraModel : BasePropertyChangeImplementation, Base.Contracts.IEntityBase, IOrdenCompra {

        #region declaraciones
        private int index;
        private int idProveedor;
        private int folio;
        private int decimales;
        private int statusInt;
        private decimal subTotal;
        private decimal descuento;
        private decimal trasladoIVA;
        private decimal trasladoIEPS;
        private decimal retencionIEPS;
        private decimal retencionISR;
        private decimal total;
        private DateTime? fechaAutorizaField;
        private DateTime? fechaRecepcionField;
        private DateTime? fechaCancelaField;
        private DateTime? fechaRequerida;
        private DateTime? fechaModificaField;
        private string version;
        private string claveUsoCFDI;
        private string claveFormaPago;
        private string claveMetodoPago;
        private string serie;
        private string creo;
        private string modifico;
        private string cancela;
        private string autoriza;
        private string rFC;
        private string proveedor;
        private string contacto;
        private string correo;
        private string nota;
        private string metodoEnvio;
        private string condicionPago;
        private OrdenCompraRelacionado ordenCompraRelacionado;
        private string entregarEnText;
        private decimal retencionIVA;
        private DateTime fechaEmision;
        private decimal acumulado;
        #endregion

        #region constructor
        public OrdenCompraModel() {
            this.Version = "2.0";
            this.FechaEmision = DateTime.Now;
            this.ClaveUsoCFDI = "P01";
            this.ClaveFormaPago = "99";
            //this.EntregarEn = new DomicilioFiscal();
            this.OrdenRelacionada = new OrdenCompraRelacionado();
        }
        #endregion

        #region propiedades
        /// <summary>
        /// obtener o establecer el indice de la orden de compra
        /// </summary>
        [DataNames("_ordcmp_id")]
        [SugarColumn(ColumnName = "_ordcmp_id", ColumnDescription = "indice", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer version del documento
        /// </summary>
        [DataNames("_ordcmp_ver")]
        [SugarColumn(ColumnName = "_ordcmp_ver", ColumnDescription = "version del comprobante", IsNullable = false, Length = 3)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_stts")]
        [SugarColumn(ColumnName = "_ordcmp_stts", ColumnDescription = "estado del comprobante", IsNullable = false, Length = 1)]
        public int IdStatus {
            get {
                return this.statusInt;
            }
            set {
                this.statusInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de la orden de compra
        /// </summary>
        [DataNames("_ordcmp_folio")]
        [SugarColumn(ColumnName = "_ordcmp_folio", ColumnDescription = "folio de la orden de compra", IsNullable = false, Length = 11)]
        public int Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_sre")]
        [SugarColumn(ColumnName = "_ordcmp_sre", ColumnDescription = "serie", IsNullable = true, Length = 10)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del proveedor
        /// </summary>
        [DataNames("_ordcmp_drctr_id")]
        [SugarColumn(ColumnName = "_ordcmp_drctr_id", ColumnDescription = "indice del directorio de proveedores", IsNullable = false, Length = 11)]
        public int IdProveedor {
            get {
                return this.idProveedor;
            }
            set {
                this.idProveedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del proveedor
        /// </summary>
        [DataNames("_ordcmp_nom")]
        [SugarColumn(ColumnName = "_ordcmp_nom", ColumnDescription = "nombre del proveedor", IsNullable = true, Length = 128)]
        public string ReceptorNombre {
            get {
                return this.proveedor;
            }
            set {
                this.proveedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del proveedor
        /// </summary>
        [DataNames("_ordcmp_rfc")]
        [SugarColumn(ColumnName = "_ordcmp_rfc", ColumnDescription = "rfc del proveedor", IsNullable = true, Length = 14)]
        public string ReceptorRFC {
            get {
                return this.rFC;
            }
            set {
                this.rFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto del proveedor
        /// </summary>
        [DataNames("_ordcmp_cntc")]
        [SugarColumn(ColumnName = "_ordcmp_cntc", ColumnDescription = "nombre del contacto del proveedor", IsNullable = true, Length = 128)]
        public string Contacto {
            get {
                return this.contacto;
            }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el correo del contacto con el proveedor
        /// </summary>
        [DataNames("_ordcmp_cmail")]
        [SugarColumn(ColumnName = "_ordcmp_cmail", ColumnDescription = "correo del contacto", IsNullable = true, Length = 128)]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_fn")]
        [SugarColumn(ColumnName = "_ordcmp_fn", ColumnDescription = "fecha del sistema", IsNullable = false)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_fa")]
        [SugarColumn(ColumnName = "_ordcmp_fa", ColumnDescription = "fecha de autorizacion", IsNullable = true)]
        public DateTime? FechaAutoriza {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaAutorizaField >= firstGoodDate)
                    return this.fechaAutorizaField;
                return null;
            }
            set {
                this.fechaAutorizaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que autoriza
        /// </summary>
        [DataNames("_ordcmp_usr_a")]
        [SugarColumn(ColumnName = "_ordcmp_usr_a", ColumnDescription = "clave del usuario que autoriza", IsNullable = true, Length = 10)]
        public string Autoriza {
            get {
                return this.autoriza;
            }
            set {
                this.autoriza = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_fr")]
        [SugarColumn(ColumnName = "_ordcmp_fr", ColumnDescription = "fecha de recepcion", IsNullable = true)]
        public DateTime? FechaRecepcion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRecepcionField >= firstGoodDate)
                    return this.fechaRecepcionField;
                return null;
            }
            set {
                this.fechaRecepcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha requerida
        /// </summary>
        [DataNames("_ordcmp_freq")]
        [SugarColumn(ColumnName = "_ordcmp_freq", ColumnDescription = "fecha requerida", IsNullable = true)]
        public DateTime? FechaRequerida {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRequerida >= firstGoodDate)
                    return this.fechaRequerida;
                return null;
            }
            set {
                this.fechaRequerida = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de decimales
        /// </summary>
        [DataNames("_ordcmp_prec")]
        [SugarColumn(ColumnDataType = "TINYINT", ColumnName = "_ordcmp_prec", ColumnDescription = "numero de decimales", Length = 2)]
        public int Decimales {
            get {
                return this.decimales;
            }
            set {
                this.decimales = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subtotal de la orden de compra
        /// </summary>
        [DataNames("_ordcmp_sbttl")]
        [SugarColumn(ColumnName = "_ordcmp_sbttl", ColumnDescription = "subtotal", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get {
                return this.subTotal;
            }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del descuento
        /// </summary>
        [DataNames("_ordcmp_desc")]
        [SugarColumn(ColumnName = "_ordcmp_desc", ColumnDescription = "monto del descuento", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get {
                return Math.Round(this.descuento, 4);
            }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe total del impuesto traslado IVA
        /// </summary>
        [DataNames("_ordcmp_trsiva")]
        [SugarColumn(ColumnName = "_ordcmp_trsiva", ColumnDescription = "total del impuesto trasladado IVA", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get {
                return Math.Round(this.trasladoIVA, 4);
            }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del impuesto traslado IEPS
        /// </summary>
        [DataNames("_ordcmp_trsieps")]
        [SugarColumn(ColumnName = "_ordcmp_trsieps", ColumnDescription = "total del impuesto trasladado IEPS", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get {
                return this.trasladoIEPS;
            }
            set {
                this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del impuesto retenido IVA
        /// </summary>
        [DataNames("_ordcmp_retiva")]
        [SugarColumn(ColumnName = "_ordcmp_retiva", ColumnDescription = "total del impuesto retenido IVA", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get {
                return Math.Round(this.retencionIVA, 4);
            }
            set {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del impuesto retenido IEPS
        /// </summary>
        [DataNames("_ordcmp_retieps")]
        [SugarColumn(ColumnName = "_ordcmp_retieps", ColumnDescription = "total del impuesto retenido IEPS", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get {
                return this.retencionIEPS;
            }
            set {
                this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_retisr")]
        [SugarColumn(ColumnName = "_ordcmp_retisr", ColumnDescription = "total del impuesto retenido ISR", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get {
                return Math.Round(this.retencionISR, 4);
            }
            set {
                this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_ttl")]
        [SugarColumn(ColumnName = "_ordcmp_ttl", ColumnDescription = "total de la orden de compra", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return Math.Round(this.total, 4);
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_acu")]
        [SugarColumn(ColumnName = "_ordcmp_acu", ColumnDescription = "total acumulado pagado de la orden de compra", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_usocfdi")]
        [SugarColumn(ColumnName = "_ordcmp_usocfdi", ColumnDescription = "clave de uso de CFDI", IsNullable = true, Length = 3)]
        public string ClaveUsoCFDI {
            get {
                return this.claveUsoCFDI;
            }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_frmpg")]
        [SugarColumn(ColumnName = "_ordcmp_frmpg", ColumnDescription = "clave de la forma de pago", IsNullable = true, Length = 3)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_mtdpg")]
        [SugarColumn(ColumnName = "_ordcmp_mtdpg", ColumnDescription = "clave del metodo de pago", IsNullable = true, Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPago;
            }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_fc")]
        [SugarColumn(ColumnName = "_ordcmp_fc", ColumnDescription = "fecha de cancelacion", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancelaField >= firstGoodDate)
                    return this.fechaCancelaField;
                return null;
            }
            set {
                this.fechaCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que cancelo
        /// </summary>
        [DataNames("_ordcmp_usr_c")]
        [SugarColumn(ColumnName = "_ordcmp_usr_c", ColumnDescription = "clave del usuario que cancelo", IsNullable = true, Length = 10)]
        public string Cancela {
            get {
                return this.cancela;
            }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_nota")]
        [SugarColumn(ColumnName = "_ordcmp_nota", ColumnDescription = "notas", IsNullable = true, Length = 128)]
        public string Nota {
            get {
                return this.nota;
            }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer metodo de envio
        /// </summary>
        [DataNames("_ordcmp_mtdenv")]
        [SugarColumn(ColumnName = "_ordcmp_mtdenv", ColumnDescription = "metodo del envio", IsNullable = true, Length = 128)]
        public string MetodoEnvio {
            get {
                return this.metodoEnvio;
            }
            set {
                this.metodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las condiciones de pago
        /// </summary>
        [DataNames("_ordcmp_cndpg")]
        [SugarColumn(ColumnName = "_ordcmp_cndpg", ColumnDescription = "condiciones de pago", IsNullable = true, Length = 128)]
        public string CondicionPago {
            get {
                return this.condicionPago;
            }
            set {
                this.condicionPago = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_usr_n")]
        [SugarColumn(ColumnName = "_ordcmp_usr_n", ColumnDescription = "clave del usuario", IsNullable = false, Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcmp_fm")]
        [SugarColumn(ColumnName = "_ordcmp_fm", ColumnDescription = "fecha de modificacion", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate)
                    return this.fechaModificaField;
                return null;
            }
            set {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifico el registro
        /// </summary>
        [DataNames("_ordcmp_usr_m")]
        [SugarColumn(ColumnName = "_ordcmp_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", IsNullable = true, Length = 10)]
        public string Modifica {
            get {
                return this.modifico;
            }
            set {
                this.modifico = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// ordenes de compra relacionadas
        /// </summary>
        [DataNames("_ordcmp_rela")]
        [SugarColumn(ColumnDataType = "TEXT", ColumnName = "_ordcmp_rela", ColumnDescription = "ordenes de compra relacionados (json)", IsNullable = true)]
        public string OrdenRelacionadaJson {
            get {
                return JsonConvert.SerializeObject(this.ordenCompraRelacionado, Formatting.None);
            }
            set {
                if (value != null)
                    this.ordenCompraRelacionado = JsonConvert.DeserializeObject<OrdenCompraRelacionado>(value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// direccion de entrega
        /// </summary>
        [DataNames("_ordcmp_dent")]
        [SugarColumn(ColumnName = "_ordcmp_dent", ColumnDescription = "direccion de entrega", ColumnDataType = "TEXT", IsNullable = true)]
        public string EntregarEnText {
            get {
                return this.entregarEnText;
            }
            set {
                this.entregarEnText = value;
                this.OnPropertyChanged();
            }
        }

        //[SugarColumn(IsIgnore = true)]
        //public DomicilioFiscal EntregarEn {
        //    get {
        //        try {
        //            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
        //            return JsonConvert.DeserializeObject<DomicilioFiscal>(this.EntregarEnText, conf);
        //        }
        //        catch (Exception) {
        //            return new DomicilioFiscal();
        //        }
        //    }
        //    set {
        //        var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
        //        this.EntregarEnText = JsonConvert.SerializeObject(value, conf);
        //    }
        //}

        //public string Json(Formatting objFormat = 0) {
        //    // configuracion json para la serializacion
        //    var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
        //    return JsonConvert.SerializeObject(this, objFormat, conf);
        //}

        //public static DomicilioFiscal Json(string inputString) {
        //    try {
        //        var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
        //        return JsonConvert.DeserializeObject<DomicilioFiscal>(inputString, conf);
        //    }
        //    catch (Exception ex) {
        //        Console.WriteLine(ex.Message);
        //        return null;
        //    }
        //}
        #endregion

        #region propiedades ignoradas
        /// <summary>
        /// columna ignorada porque se utiliza la fecha de emision
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el saldo de la orden de compra
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal Saldo {
            get {
                return this.Total - this.Acumulado;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public OrdenCompraRelacionado OrdenRelacionada {
            get {
                if (this.ordenCompraRelacionado == null)
                    this.ordenCompraRelacionado = new OrdenCompraRelacionado();
                return this.ordenCompraRelacionado;
            }
            set {
                this.ordenCompraRelacionado = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}