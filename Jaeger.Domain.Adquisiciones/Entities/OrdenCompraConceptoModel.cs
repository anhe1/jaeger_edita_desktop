using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// concepto de orden de compra 
    /// </summary>
    [JsonObject("prd")]
    [SugarTable("_ordcnp", "proveedor: orden de compra")]
    public class OrdenCompraConceptoModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int subIndex;
        private int almacenInt;
        private int tipoInt;
        private int idProducto;
        private int idModelo;
        private int idUnidad;
        private int ordenProduccion;
        private int decimales;
        private decimal cantidad;
        private decimal unitario;
        private decimal unitarioBase;
        private string factorTrasladoIVA;
        private decimal? valorTrasladoIVA;
        private string factorTrasladoIEPS;
        private decimal? valorTrasladoIEPS;
        private decimal? valorRetencionIVA;
        private string factorRetencionIEPS;
        private decimal? valorRetencionIEPS;
        private decimal? valorRetencionISR;
        private decimal descuento;
        private decimal importe;
        private decimal subTotal;
        private string unidad;
        private string claveUnidad;
        private string claveProdServ;
        private string nombre;
        private string descripcion;
        private string noIdentificacion;
        private string especificacion;
        private string marca;
        private string nota;
        private string creo;
        private string modifica;
        private DateTime? fechaRequerida;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private BindingList<OrdenCompraConceptoImpuesto> impuestos;
        #endregion

        public OrdenCompraConceptoModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
            this.impuestos = new BindingList<OrdenCompraConceptoImpuesto>() { RaiseListChangedEvents = true };
            this.impuestos.AddingNew += new AddingNewEventHandler(this.Impuestos_AddingNew);
            this.impuestos.ListChanged += new ListChangedEventHandler(this.Impuestos_ListChanged);
        }

        [SugarColumn(ColumnName = "_ordcnp_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_a", ColumnDescription = "obtener o establecer si el registro se encuentra activo", Length = 1, IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la orden de compra
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_sbid", ColumnDescription = "obtener o establecer el indice de relacion con la orden de compra")]
        public int SubId {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_alm_id", ColumnDescription = "obtener o establecer el almacen al que pertenece")]
        public int IdAlmacen {
            get {
                return this.almacenInt;
            }
            set {
                this.almacenInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el tipo de producto (producto o servicio)
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_tipo", ColumnDescription = "obtener o establcer el tipo de producto (producto o servicio)")]
        public int TipoInt {
            get {
                return this.tipoInt;
            }
            set {
                this.tipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del producto o servicio
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_prd_id", ColumnDescription = "obtener o establecer el indice del producto o servicio")]
        public int IdProducto {
            get {
                return this.idProducto;
            }
            set {
                this.idProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_mdl_id", ColumnDescription = "obtener o establecer el indice del modelo")]
        public int IdModelo {
            get {
                return this.idModelo;
            }
            set {
                this.idModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_undd_id", ColumnDescription = "obtener o establecer la unidad de almacen")]
        public int IdUnidad {
            get {
                return this.idUnidad;
            }
            set {
                this.idUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden relacionado al producto
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_op", ColumnDescription = "obtener o establecer el numero de orden relacionado al producto")]
        public int OrdenProduccion {
            get {
                return this.ordenProduccion;
            }
            set {
                this.ordenProduccion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de decimales a utilizar para el unitario
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_dec", ColumnDescription = "obtener o establecer la cantidad de decimales a utilizar para el unitario", Length = 1)]
        public int Decimales {
            get {
                return this.decimales;
            }
            set {
                this.decimales = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de bienes o servicios del tipo particular definido por el presente concepto.
        /// <xs:fractionDigits value="6"/>
        ///<xs:minInclusive value = "0.000001" />
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_cant", ColumnDescription = "obtener o establecer la cantidad requerida del producto o servicio", Length = 14, DecimalDigits = 4)]
        public decimal Cantidad {
            get {
                return this.cantidad;
            }
            set {
                this.cantidad = value;
                this.Unitario = this.Unitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor unitario del modelo
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_unt", ColumnDescription = "obtener o establecer el valor unitario del modelo", Length = 14, DecimalDigits = 4)]
        public decimal Unitario {
            get {
                return this.unitario;
            }
            set {
                this.unitario = value;
                this.Importe = (this.Cantidad * this.Unitario);
                this.SubTotal = (this.Importe - this.Descuento);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el unitario base del catalogo de productos
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_untb", ColumnDescription = "obtener o establecer el unitario base del catalogo de productos", Length = 14, DecimalDigits = 4)]
        public decimal UnitarioBase {
            get {
                return this.unitarioBase;
            }
            set {
                this.unitarioBase = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_trsivaf", ColumnDescription = "obtener o establecer el factor del impuesto trasladado IVA", IsNullable = true, Length = 6)]
        public string FactorTrasladoIVA {
            get {
                if (this.factorTrasladoIVA == "N/A")
                    return null;
                return this.factorTrasladoIVA;
            }
            set {
                if (value == "N/A")
                    this.ValorTrasladoIVA = null;
                this.factorTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_trsiva", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto trasladado IVA", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorTrasladoIVA {
            get {
                return this.valorTrasladoIVA;
            }
            set {
                this.valorTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS trasladado, puede ser TASA � CUOTA
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_trsiepsf", ColumnDescription = "obtener o establecer el factor del impuesto trasladado IEPS", IsNullable = true, Length = 6)]
        public string FactorTrasladoIEPS {
            get {
                if (this.factorTrasladoIEPS == "N/A")
                    return null;
                return this.factorTrasladoIEPS;
            }
            set {
                if (value == "N/A")
                    this.valorTrasladoIEPS = null;
                this.factorTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor % del impuesto IEPS, puede ser valor FIJO o RANGO, si el factor es TASA es fijo, si el factor es CUOTA puede ser rango
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_trsieps", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto trasladado IEPS", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorTrasladoIEPS {
            get {
                return this.valorTrasladoIEPS;
            }
            set {
                this.valorTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA, puede ser un rango cuando el factor es TASA
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_retiva", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto retenido IVA", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionIVA {
            get {
                return this.valorRetencionIVA;
            }
            set {
                this.valorRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS que puede ser TASA � CUOTA
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_retiepsf", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el factor del impuesto retenido IEPS", IsNullable = true, Length = 6)]
        public string FactorRetencionIEPS {
            get {
                if (this.factorRetencionIEPS == "N/A")
                    return null;
                return this.factorRetencionIEPS;
            }
            set {
                if (value == "N/A")
                    this.ValorRetencionIEPS = null;
                this.factorRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS, puede ser FIJO si al factor es TASA � RANGO si el factor es CUOTA
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_retieps", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto retenido IEPS", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionIEPS {
            get {
                return this.valorRetencionIEPS;
            }
            set {
                this.valorRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido ISR, si el factor es TASA el valor puede ser un RANGO
        /// </summary>
        [SugarColumn(IsIgnore = true, ColumnName = "_ordcnp_retisr", ColumnDescription = "alm:|pt,tw| desc: obtener o establecer el valor del impuesto retenido ISR", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionISR {
            get {
                return valorRetencionISR;
            }
            set {
                this.valorRetencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe de los descuentos aplicables al concepto. No se permiten valores negativos.
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_desc", ColumnDescription = "obtener o establecer el importe del descuento aplicable", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get {
                return this.descuento;
            }
            set {
                this.descuento = value;
                this.Unitario = this.Unitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe total de los bienes o servicios del presente concepto. Debe ser equivalente al resultado de multiplicar la cantidad por el valor unitario expresado 
        /// en el concepto. No se permiten valores negativos. 
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_impr", ColumnDescription = "obtener o establecer el importe", Length = 14, DecimalDigits = 4)]
        public decimal Importe {
            get {
                return this.importe;
            }
            set {
                this.importe = value;
                foreach (OrdenCompraConceptoImpuesto item in this.impuestos) {
                    item.Base = value;
                }
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal SubTotal {
            get {
                return this.subTotal;
            }
            set {
                this.subTotal = value;
                //foreach (var item in this.Impuestos) {
                //    item.Base = this.SubTotal;
                //}
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operaci�n del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,20}"
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_undd", ColumnDescription = "obtener o establecer la unidad del almacen", Length = 20, IsNullable = true)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_clvund", ColumnDescription = "obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripci�n del concepto.", Length = 3, IsNullable = true)]
        public string ClaveUnidad {
            get {
                return this.claveUnidad;
            }
            set {
                this.claveUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del cat�logo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_clvprd", ColumnDescription = "obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del cat�logo de productos y servicios, ", Length = 8, IsNullable = true)]
        public string ClaveProdServ {
            get {
                return this.claveProdServ;
            }
            set {
                this.claveProdServ = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_nom", Length = 255)]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripci�n del bien o servicio cubierto por el presente concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,1000}"
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_dscrc", ColumnDescription = "obtener o establecer la descripci�n del bien o servicio cubierto por el presente concepto.", Length = 1000, IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el n�mero de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operaci�n del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del est�ndar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |�|�|!|&quot;|%|&amp;|'|�|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|�|�|�|�|�|�|�|�|�|�|�|�){1,100}"
        [SugarColumn(ColumnName = "_ordcnp_sku", ColumnDescription = "obtener o establecer el n�mero de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operaci�n del emisor,", Length = 100, IsNullable = true)]
        public string NoIdentificacion {
            get {
                return this.noIdentificacion;
            }
            set {
                this.noIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer especificacion del producto o servicio
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_espc", ColumnDescription = "alm:|mp,pt| desc: espcificaciones", IsNullable = true, Length = 128)]
        public string Especificacion {
            get {
                return this.especificacion;
            }
            set {
                this.especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre de la marca o fabricante
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_mrc", ColumnDescription = "alm:|mp,pt| desc: marca o fabricante", Length = 128, IsNullable = true)]
        public string Marca {
            get {
                return this.marca;
            }
            set {
                this.marca = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones del concepto
        /// </summary>
        [SugarColumn(ColumnName = "_ordcnp_nota", ColumnDescription = "alm:|mp,pt| observaciones", Length = 128, IsNullable = true)]
        public string Nota {
            get { return this.nota; }
            set { this.nota = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_fecreq", ColumnDescription = "obtener o establecer la fecha de requerimiento", IsNullable = true)]
        public DateTime? FechaRequerida {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRequerida >= firstGoodDate)
                    return this.fechaRequerida;
                return null;
            }
            set {
                this.fechaRequerida = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true, ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_fm", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsOnlyIgnoreUpdate = true, IsNullable = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_ordcnp_impto", ColumnDescription = "", ColumnDataType = "TEXT")]
        public string JImpuestos {
            get {
                return this.JsonImpuestos();
            }
            set {
                this.Impuestos = Json(value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de impuestos aplicables al concepto.
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<OrdenCompraConceptoImpuesto> Impuestos {
            get {
                return this.impuestos;
            }
            set {
                if (this.impuestos != null) {
                    this.impuestos.ListChanged -= new ListChangedEventHandler(Impuestos_ListChanged);
                    this.impuestos.AddingNew -= new AddingNewEventHandler(Impuestos_AddingNew);
                }
                this.impuestos = value;
                if (this.impuestos != null) {
                    this.impuestos.ListChanged += new ListChangedEventHandler(Impuestos_ListChanged);
                    this.impuestos.AddingNew += new AddingNewEventHandler(Impuestos_AddingNew);
                }
                this.OnPropertyChanged();
            }
        }

        #region metodos privados

        private void Impuestos_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new OrdenCompraConceptoImpuesto() { Base = this.Importe };
        }

        private void Impuestos_ListChanged(object sender, ListChangedEventArgs e) {
            this.ValorTrasladoIVA = this.Impuestos.Where((OrdenCompraConceptoImpuesto p) => p.Tipo == ImpuestoTipoEnum.Traslado && p.Impuesto == ImpuestoEnum.IVA).Sum((OrdenCompraConceptoImpuesto p) => p.Importe);
            this.ValorTrasladoIEPS = this.Impuestos.Where((OrdenCompraConceptoImpuesto p) => p.Tipo == ImpuestoTipoEnum.Traslado && p.Impuesto == ImpuestoEnum.IEPS).Sum((OrdenCompraConceptoImpuesto p) => p.Importe);
            this.ValorRetencionIVA = this.Impuestos.Where((OrdenCompraConceptoImpuesto p) => p.Tipo == ImpuestoTipoEnum.Retencion && p.Impuesto == ImpuestoEnum.IVA).Sum((OrdenCompraConceptoImpuesto p) => p.Importe);
            this.ValorRetencionIEPS = this.Impuestos.Where((OrdenCompraConceptoImpuesto p) => p.Tipo == ImpuestoTipoEnum.Retencion && p.Impuesto == ImpuestoEnum.IEPS).Sum((OrdenCompraConceptoImpuesto p) => p.Importe);
            this.ValorRetencionISR = this.Impuestos.Where((OrdenCompraConceptoImpuesto p) => p.Tipo == ImpuestoTipoEnum.Retencion && p.Impuesto == ImpuestoEnum.ISR).Sum((OrdenCompraConceptoImpuesto p) => p.Importe);
        }

        private string JsonImpuestos() {
            if (Impuestos != null) {
                if (this.Impuestos.Count > 0)
                    return JsonConvert.SerializeObject(this.Impuestos);
            }
            return string.Empty;
        }
        #endregion

        #region miembros estaticos
        public static BindingList<OrdenCompraConceptoImpuesto> Json(string value) {
            if (!(string.IsNullOrEmpty(value))) {
                return JsonConvert.DeserializeObject<BindingList<OrdenCompraConceptoImpuesto>>(value);
            } else {
                return new BindingList<OrdenCompraConceptoImpuesto>();
            }
        }
        #endregion
    }
}