﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Adquisiciones.Entities {
    public class SolicitudCotizacionPrinter : SolicitudCotizacionDetailModel {
        public SolicitudCotizacionPrinter() : base() {

        }

        public SolicitudCotizacionPrinter(SolicitudCotizacionDetailModel model) : base() {
            MapperClassExtensions.MatchAndMap<SolicitudCotizacionDetailModel, SolicitudCotizacionPrinter>(model, this);
        }

        public string Concepto {
            get { return "Nos es grato dirigirnos a usted a fin de solicitarle la presentación de una cotización para los articulos"; }
        }

        public string LeyendaPiePagina {
            get { return ""; }
        }

        public string[] QrText {
            get {
                return new string[] { "||SOLICITUD|FOLIO=", this.Folio.ToString("#00000"), "|FECHA=", this.FechaEmision.ToString("dd-mm-yyyy"), "||" };
            }
        }
    }
}
