﻿using System;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// concepto de orden de compra 
    /// </summary>
    [JsonObject("prd")]
    [SugarTable("_ordcnp", "catalogo de ordenes de compra")]
    public class OrdenCompraConceptoDetailModel : OrdenCompraConceptoModel {

        public OrdenCompraConceptoDetailModel(): base() {

        }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public AlmacenEnum Almacen {
            get {
                return (AlmacenEnum)this.IdAlmacen;
            }
            set {
                this.IdAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el tipo de producto (producto o servicio)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ProdServTipoEnum Tipo {
            get {
                return (ProdServTipoEnum)Enum.Parse(typeof(ProdServTipoEnum), this.TipoInt.ToString());
            }
            set {
                this.TipoInt = (int)value;
                this.OnPropertyChanged();
            }
        }
    }
}