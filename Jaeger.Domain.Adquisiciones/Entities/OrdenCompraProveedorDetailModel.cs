﻿using SqlSugar;

namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraProveedorDetailModel : OrdenCompraProveedorModel {
        private System.ComponentModel.BindingList<ContactoProveedorModel> contactos;

        public OrdenCompraProveedorDetailModel() : base() {

        }

        [SugarColumn(IsIgnore = true)]
        public System.ComponentModel.BindingList<ContactoProveedorModel> Contactos {
            get {
                return this.contactos;
            }
            set {
                this.contactos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
