﻿using System;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Adquisiciones.ValueObjects;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// almacen de materia prima: ordenes de compra
    /// </summary>
    [SugarTable("_ordcmp", "proveedor: orden de compra")]
    public class OrdenCompraDetailModel : OrdenCompraModel {
        private BindingList<OrdenCompraConceptoDetailModel> conceptos;

        public OrdenCompraDetailModel() {
            this.Status = OrdenCompraStatusEnum.Requerida;
            this.Version = "2.0";
            this.FechaEmision = DateTime.Now;
            this.ClaveUsoCFDI = "P01";
            this.ClaveFormaPago = "99";
            this.conceptos = new BindingList<OrdenCompraConceptoDetailModel>() { RaiseListChangedEvents = true };
            this.conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
            this.conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public OrdenCompraStatusEnum Status {
            get {
                if (this.IdStatus == 0)
                    return OrdenCompraStatusEnum.Cancelado;
                else if (this.IdStatus == 1)
                    return OrdenCompraStatusEnum.Requerida;
                else if (this.IdStatus == 2)
                    return OrdenCompraStatusEnum.Autorizada;
                else if (this.IdStatus == 3)
                    return OrdenCompraStatusEnum.Ingresada;
                else
                    return OrdenCompraStatusEnum.Requerida;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get {
                return this.Status == OrdenCompraStatusEnum.Requerida;
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de decimales
        /// </summary>
        [DataNames("_ordcmp_prec")]
        [SugarColumn(ColumnDataType = "TINYINT", ColumnName = "_ordcmp_prec", ColumnDescription = "numero de decimales", Length = 2)]
        public new int Decimales {
            get {
                return base.Decimales;
            }
            set {
                base.Decimales = value;
                this.Conceptos = new BindingList<OrdenCompraConceptoDetailModel>(this.Conceptos.Select(c => { c.Decimales = this.Decimales; return c; }).ToList<OrdenCompraConceptoDetailModel>());
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<OrdenCompraConceptoDetailModel> Conceptos {
            get {
                return this.conceptos;
            }
            set {
                if (this.conceptos != null) {
                    this.Conceptos.AddingNew -= new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.conceptos = value;
                if (this.conceptos != null) {
                    this.Conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        #region metodos privados
        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            //e.NewObject = new OrdenCompraConceptoDetailModel { SubId = this.Id };
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true).Sum((OrdenCompraConceptoDetailModel p) => p.Importe);
            this.Descuento = this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true).Sum((OrdenCompraConceptoDetailModel p) => p.Descuento);
            this.TrasladoIVA = (decimal)this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true && p.ValorTrasladoIVA != null).Sum((OrdenCompraConceptoDetailModel p) => p.ValorTrasladoIVA);
            this.TrasladoIEPS = (decimal)this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true && p.ValorTrasladoIEPS != null).Sum((OrdenCompraConceptoDetailModel p) => p.ValorTrasladoIEPS);
            this.RetencionIVA = (decimal)this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true).Sum((OrdenCompraConceptoDetailModel p) => p.ValorRetencionIVA);
            this.RetencionISR = (decimal)this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true).Sum((OrdenCompraConceptoDetailModel p) => p.ValorRetencionISR);
            this.RetencionIEPS = (decimal)this.Conceptos.Where((OrdenCompraConceptoDetailModel p) => p.Activo == true).Sum((OrdenCompraConceptoDetailModel p) => p.ValorRetencionIEPS);
            this.Total = (this.SubTotal - this.Descuento) + (this.TrasladoIVA + this.TrasladoIEPS) - (this.RetencionIVA + this.RetencionISR + this.RetencionIEPS);
        }
        #endregion

        public bool Autorizacion() {
            bool response = false;
            for (int i = 0; i < this.conceptos.Count; i++) {
                if (this.conceptos[i].Unitario > this.conceptos[i].UnitarioBase) {
                    response = true;
                    break;
                }
            }
            return response;
        }

        public void Clonar() {
            this.Id = 0;
            this.Folio = 0;
            this.Status = OrdenCompraStatusEnum.Requerida;
            this.Modifica = null;
            this.FechaEmision = DateTime.Now;
            this.FechaModifica = null;
            this.FechaAutoriza = null;
            this.FechaCancela = null;
            this.FechaRecepcion = null;
            this.Cancela = null;
            this.Autoriza = null;
            var d = new BindingList<OrdenCompraConceptoDetailModel>();
            for (int i = 0; i < this.Conceptos.Count; i++) {
                this.Conceptos[i].Id = 0;
                this.Conceptos[i].FechaNuevo = DateTime.Now;
                this.Conceptos[i].Modifica = null;
                this.Conceptos[i].FechaModifica = null;
                if (this.Conceptos[i].Activo == true) {
                    this.Conceptos[i].Unitario = this.Conceptos[i].Unitario;
                    d.Add(this.Conceptos[i]);
                }
            }
            this.Conceptos = d;
        }
    }
}