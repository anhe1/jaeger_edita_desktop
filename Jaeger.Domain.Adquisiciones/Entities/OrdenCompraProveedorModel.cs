﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// clase que representa a un contribuyente
    /// </summary>
    [SugarTable("_drctr", "directorio proveedores y clientes")]
    public class OrdenCompraProveedorModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int idDescuento;
        private int diasEntrega;
        private bool extranjero;
        private decimal credito;
        private decimal factorDescuento;
        private decimal factorIvaPactado;
        private string clave;
        private string curp;
        private string rfc;
        private string nombre;
        private string telefono;
        private string correo;
        private string sitioweb;
        private string relacion;
        private string nota;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModificaField;
        private string regimen;
        private string regimenFiscal;
        private string numRegIdTrib;
        private string residenciaFiscal;
        private string claveUsoCFDI;
        #endregion

        public OrdenCompraProveedorModel() {
            this.Activo = true;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id")]
        [SugarColumn(ColumnName = "_drctr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("_drctr_a")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer id de catalogo de descuentos asignado cuando es cliente
        /// </summary>
        [DataNames("_drctr_dscnt_id")]
        [SugarColumn(ColumnName = "_drctr_dscnt_id", ColumnDescription = "id de catalogo de descuentos asignado cuando es cliente", IsNullable = true, Length = 11)]
        public int IdDescuento {
            get { return this.idDescuento; }
            set {
                this.idDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de entrega
        /// </summary>
        [DataNames("_drctr_dsp")]
        [SugarColumn(ColumnName = "_drctr_dsp", ColumnDescription = "dias pactados de entrega", IsNullable = true)]
        public int DiasEntrega {
            get { return this.diasEntrega; }
            set {
                this.diasEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer limite de credito
        /// </summary>
        [DataNames("_drctr_crd")]
        [SugarColumn(ColumnName = "_drctr_crd", ColumnDescription = "importe del limite de credito", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal Credito {
            get { return this.credito; }
            set {
                this.credito = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor del descuento pactado
        /// </summary>
        [DataNames("_drctr_pctd")]
        [SugarColumn(ColumnName = "_drctr_pctd", ColumnDescription = "factor del descuento pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal FactorDescuento {
            get { return this.factorDescuento; }
            set {
                this.factorDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor del IVA pactado
        /// </summary>
        [DataNames("_drctr_iva")]
        [SugarColumn(ColumnName = "_drctr_iva", ColumnDescription = "factor del IVA pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal FactorIvaPactado {
            get { return this.factorIvaPactado; }
            set {
                this.factorIvaPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)
        /// </summary>
        [DataNames("_drctr_extrnjr")]
        [SugarColumn(ColumnName = "_drctr_extrnjr", ColumnDescription = "Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)", IsNullable = true, Length = 1)]
        public bool Extranjero {
            get {
                return this.extranjero;
            }
            set {
                this.extranjero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        [DataNames("_drctr_clv")]
        [SugarColumn(ColumnName = "_drctr_clv", ColumnDescription = "clave del usuario", IsNullable = false, Length = 14)]
        public string Clave {
            get { return this.clave; }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        [DataNames("_drctr_curp")]
        [SugarColumn(ColumnName = "_drctr_curp", ColumnDescription = "Clave Unica de Registro de Poblacion (CURP)", Length = 18, IsNullable = false)]
        public string CURP {
            get { return this.curp; }
            set {
                this.curp = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("_drctr_rfc")]
        [SugarColumn(ColumnName = "_drctr_rfc", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsNullable = false)]
        public string RFC {
            get { return this.rfc; }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("_drctr_nom")]
        [SugarColumn(ColumnName = "_drctr_nom", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public string Nombre {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero telefonico de contacto
        /// </summary>
        [DataNames("_drctr_tel")]
        [SugarColumn(ColumnName = "_drctr_tel", ColumnDescription = "telefono", IsNullable = false, Length = 64)]
        public string Telefono {
            get { return this.telefono; }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estabelcer direccion de correo electronico
        /// </summary>
        [DataNames("_drctr_mail")]
        [SugarColumn(ColumnName = "_drctr_mail", ColumnDescription = "correo electronico", IsNullable = false, Length = 128)]
        public string Correo {
            get { return this.correo; }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer direccion del sitio web
        /// </summary>
        [DataNames("_drctr_web")]
        [SugarColumn(ColumnName = "_drctr_web", ColumnDescription = "sitio web", IsNullable = false, Length = 64)]
        public string SitioWEB {
            get { return this.sitioweb; }
            set {
                this.sitioweb = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las etiquetas de las relaciones comerciales
        /// </summary>
        [DataNames("_drctr_rlcn")]
        [SugarColumn(ColumnName = "_drctr_rlcn", ColumnDescription = "etiquetas de relacion del directorio", IsNullable = false, Length = 64)]
        public string Relacion {
            get { return this.relacion; }
            set {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// observaciones
        /// </summary>
        [DataNames("_drctr_nts")]
        [SugarColumn(ColumnName = "_drctr_nts", ColumnDescription = "observaciones", IsNullable = false, Length = 255)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("_drctr_usr_n")]
        [SugarColumn(ColumnName = "_drctr_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = false, IsOnlyIgnoreUpdate = true, Length = 10)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_drctr_usr_m")]
        [SugarColumn(ColumnName = "_drctr_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = false, IsOnlyIgnoreInsert = true, Length = 10)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_drctr_fn")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_drctr_fm")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate) {
                    return this.fechaModificaField;
                }
                return null;
            }
            set {
                this.fechaModificaField = value;
            }
        }

        /// <summary>
        /// obtener o establecer regimen fiscal (Fisica o Moral)
        /// </summary>
        [DataNames("_drctr_rgf")]
        [SugarColumn(ColumnName = "_drctr_rgf", ColumnDescription = "regimen fiscal Fisica, Moral", Length = 64, IsNullable = false)]
        public string Regimen {
            get { return this.regimen; }
            set {
                this.regimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        [DataNames("_drctr_rgfsc")]
        [SugarColumn(ColumnName = "_drctr_rgfsc", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = false)]
        public string RegimenFiscal {
            get { return this.regimenFiscal; }
            set {
                this.regimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer número de registro de identidad fiscal
        /// </summary>
        [DataNames("_drctr_nmreg")]
        [SugarColumn(ColumnName = "_drctr_nmreg", ColumnDescription = "número de registro de identidad fiscal", Length = 40, IsNullable = true)]
        public string NumRegIdTrib {
            get { return this.numRegIdTrib; }
            set {
                this.numRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del país de residencia para efectos fiscales del receptor del comprobante
        /// </summary>
        [DataNames("_drctr_resfis")]
        [SugarColumn(ColumnName = "_drctr_resfis", ColumnDescription = "clave del país de residencia para efectos fiscales del receptor del comprobante", Length = 20, IsNullable = false)]
        public string ResidenciaFiscal {
            get { return this.residenciaFiscal; }
            set {
                this.residenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer uso de CFDI predeterminado
        /// </summary>
        [DataNames("_drctr_usocfdi")]
        [SugarColumn(ColumnName = "_drctr_usocfdi", ColumnDescription = "clave de uso de cfdi por default", Length = 4, IsNullable = true)]
        public string ClaveUsoCFDI {
            get { return this.claveUsoCFDI; }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }
    }
}
