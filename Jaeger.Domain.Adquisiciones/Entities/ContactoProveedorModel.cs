﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Adquisiciones.Entities {
    [SugarTable("_dcntc", "directorio: contactos del directorio")]
    public class ContactoProveedorModel : BasePropertyChangeImplementation {
        private int index;
        private bool activo;
        private int subIndex;
        private string nombre;
        private string creo;
        private string modifica;
        private string correo;
        private DateTime fechaNuevo;
        private DateTime? dateChangeDate;
        private string telefono;

        public ContactoProveedorModel() {
            this.IsActive = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        [DataNames("_dcntc_id")]
        [SugarColumn(ColumnName = "_dcntc_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_dcntc_a")]
        [SugarColumn(ColumnName = "_dcntc_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool IsActive {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el directorio
        /// </summary>
        [DataNames("_dcntc_drctr_id")]
        [SugarColumn(ColumnName = "_dcntc_drctr_id", ColumnDescription = "indice de relacion con el directorio")]
        public int SubId {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del contacto
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_nom", ColumnDescription = "nombre del contacto")]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// trato sugerido
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_sug")]
        public string TratoSugerido {
            get; set;
        }

        /// <summary>
        /// puesto
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_pst")]
        public string Puesto {
            get; set;
        }

        /// <summary>
        /// detalles
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_dtlls")]
        public string Detalles {
            get; set;
        }

        /// <summary>
        /// tipo de telefono
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_ttlfn")]
        public string TelefonoTipo {
            get; set;
        }

        /// <summary>
        /// tipo de correo electronico
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_tmail")]
        public string CorreoTipo {
            get; set;
        }

        /// <summary>
        /// telefono
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_tlfn")]
        public string Telefono {
            get {
                return this.telefono;
            }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// correo electronico
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_mail")]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha especial
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string FechaEspecial {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_usr_n")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_usr_m")]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_fn")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_dcntc_fm")]
        public DateTime? FechaMod {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.dateChangeDate >= firstGoodDate)
                    return this.dateChangeDate.Value;
                return null;
            }
            set {
                this.dateChangeDate = value;
                this.OnPropertyChanged();
            }
        }
    }
}
