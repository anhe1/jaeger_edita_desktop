﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Adquisiciones.Entities {
    [SugarTable("_ordcms", "proveedor: solicitud de cotizacion")]
    public class SolicitudCotizacionModel : Base.Abstractions.BasePropertyChangeImplementation, Base.Contracts.IEntityBase {
        #region declaraciones
        private int index;
        private int idProveedor;
        private int folio;
        private int _IdStatus;
        private DateTime? fechaRecepcionField;
        private DateTime? fechaRequerida;
        private DateTime? fechaModificaField;
        private string version;
        private string serie;
        private string creo;
        private string modifico;
        private string rFC;
        private string proveedor;
        private string contacto;
        private string correo;
        private string nota;
        private string metodoEnvio;
        private string entregarEnText;
        private DateTime fechaEmision;
        #endregion

        #region constructor
        public SolicitudCotizacionModel() {
            this.Version = "1.0";
            this.FechaEmision = DateTime.Now;
        }
        #endregion

        #region propiedades
        /// <summary>
        /// obtener o establecer el indice de la orden de compra
        /// </summary>
        [DataNames("_ordcms_id")]
        [SugarColumn(ColumnName = "_ordcms_id", ColumnDescription = "indice", IsPrimaryKey = true, IsIdentity = true)]
        public int IdSolicitud {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer version de la solicitud de cotizacion a proveedor
        /// </summary>
        [DataNames("_ordcms_ver")]
        [SugarColumn(ColumnName = "_ordcms_ver", ColumnDescription = "version del comprobante", IsNullable = false, Length = 3)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de la orden de compra
        /// </summary>
        [DataNames("_ordcms_folio")]
        [SugarColumn(ColumnName = "_ordcms_folio", ColumnDescription = "folio de la orden de compra", IsNullable = false, Length = 11)]
        public int Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_sre")]
        [SugarColumn(ColumnName = "_ordcms_sre", ColumnDescription = "serie", IsNullable = true, Length = 10)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_stts")]
        [SugarColumn(ColumnName = "_ordcms_stts", ColumnDescription = "estado del comprobante", IsNullable = false, Length = 1)]
        public int IdStatus {
            get {
                return this._IdStatus;
            }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del proveedor
        /// </summary>
        [DataNames("_ordcms_drctr_id")]
        [SugarColumn(ColumnName = "_ordcms_drctr_id", ColumnDescription = "indice del directorio de proveedores", IsNullable = false, Length = 11)]
        public int IdProveedor {
            get {
                return this.idProveedor;
            }
            set {
                this.idProveedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del proveedor
        /// </summary>
        [DataNames("_ordcms_nom")]
        [SugarColumn(ColumnName = "_ordcms_nom", ColumnDescription = "nombre del proveedor", IsNullable = true, Length = 128)]
        public string ReceptorNombre {
            get {
                return this.proveedor;
            }
            set {
                this.proveedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del proveedor
        /// </summary>
        [DataNames("_ordcms_rfc")]
        [SugarColumn(ColumnName = "_ordcms_rfc", ColumnDescription = "rfc del proveedor", IsNullable = true, Length = 14)]
        public string ReceptorRFC {
            get {
                return this.rFC;
            }
            set {
                this.rFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto del proveedor
        /// </summary>
        [DataNames("_ordcms_cntc")]
        [SugarColumn(ColumnName = "_ordcms_cntc", ColumnDescription = "nombre del contacto del proveedor", IsNullable = true, Length = 128)]
        public string Contacto {
            get {
                return this.contacto;
            }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el correo del contacto con el proveedor
        /// </summary>
        [DataNames("_ordcms_cmail")]
        [SugarColumn(ColumnName = "_ordcms_cmail", ColumnDescription = "correo del contacto", IsNullable = true, Length = 128)]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_fn")]
        [SugarColumn(ColumnName = "_ordcms_fn", ColumnDescription = "fecha del sistema", IsNullable = false)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_fecord")]
        [SugarColumn(ColumnName = "_ordcms_fecord", ColumnDescription = "fecha de recepcion", IsNullable = true)]
        public DateTime? FechaOrdenCompra {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRecepcionField >= firstGoodDate)
                    return this.fechaRecepcionField;
                return null;
            }
            set {
                this.fechaRecepcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha requerida
        /// </summary>
        [DataNames("_ordcms_freq")]
        [SugarColumn(ColumnName = "_ordcms_freq", ColumnDescription = "fecha requerida", IsNullable = true)]
        public DateTime? FechaRequerida {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRequerida >= firstGoodDate)
                    return this.fechaRequerida;
                return null;
            }
            set {
                this.fechaRequerida = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_nota")]
        [SugarColumn(ColumnName = "_ordcms_nota", ColumnDescription = "notas", IsNullable = true, Length = 128)]
        public string Nota {
            get {
                return this.nota;
            }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer metodo de envio
        /// </summary>
        [DataNames("_ordcms_mtdenv")]
        [SugarColumn(ColumnName = "_ordcms_mtdenv", ColumnDescription = "metodo del envio", IsNullable = true, Length = 128)]
        public string MetodoEnvio {
            get {
                return this.metodoEnvio;
            }
            set {
                this.metodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// direccion de entrega
        /// </summary>
        [DataNames("_ordcms_dent")]
        [SugarColumn(ColumnName = "_ordcms_dent", ColumnDescription = "direccion de entrega", ColumnDataType = "TEXT", IsNullable = true)]
        public string EntregarEnText {
            get {
                return this.entregarEnText;
            }
            set {
                this.entregarEnText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// columna ignorada porque se utiliza la fecha de emision
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_usr_n")]
        [SugarColumn(ColumnName = "_ordcms_usr_n", ColumnDescription = "clave del usuario", IsNullable = false, Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordcms_fm")]
        [SugarColumn(ColumnName = "_ordcms_fm", ColumnDescription = "fecha de modificacion", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate)
                    return this.fechaModificaField;
                return null;
            }
            set {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifico el registro
        /// </summary>
        [DataNames("_ordcms_usr_m")]
        [SugarColumn(ColumnName = "_ordcms_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", IsNullable = true, Length = 10)]
        public string Modifica {
            get {
                return this.modifico;
            }
            set {
                this.modifico = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
