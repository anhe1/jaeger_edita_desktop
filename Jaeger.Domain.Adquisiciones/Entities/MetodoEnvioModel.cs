﻿namespace Jaeger.Domain.Adquisiciones.Entities {
    public class MetodoEnvioModel : Base.Abstractions.BaseSingleModel {
        private decimal _Costo;
        public MetodoEnvioModel(int id, string descripcion) : base(id, descripcion) {
            this._Costo = 0;
        }

        public MetodoEnvioModel(int id, string descripcion, decimal costo) : base(id, descripcion) {
            this._Costo = costo;
        }

        public decimal Costo {
            get { return _Costo; }
            set { this._Costo = value; }
        }
    }
}