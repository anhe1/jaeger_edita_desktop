using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Adquisiciones.Entities {
    /// <summary>
    /// Orden de compra relacionada
    /// </summary>
    public class OrdenCompraRelacionado : BasePropertyChangeImplementation {
        private string clave;
        private string descripcion;
        private BindingList<OrdenCompraRel> relacionado;

        public OrdenCompraRelacionado() {
            this.Relacionado = new BindingList<OrdenCompraRel>();
        }

        /// <summary>
        /// obtener o establecer la clave
        /// </summary>
        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la causa o motivo de la relaci�n con la orden de compra original
        /// </summary>
        [JsonProperty("desc")]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de documentos relacionados
        /// </summary>
        [JsonProperty("doctos")]
        public BindingList<OrdenCompraRel> Relacionado {
            get {
                return this.relacionado;
            }
            set {
                this.relacionado = value;
                this.OnPropertyChanged();
            }
        }
    }
}