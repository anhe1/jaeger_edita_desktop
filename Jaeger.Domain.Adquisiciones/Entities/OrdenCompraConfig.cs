﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraConfig {

        public OrdenCompraConfig() {
            this.Concepto = "Por medio de la presente solicitamos su apreciable atención para atender a la brevedad posible el requerimiento @folio que a continuación le describimos.";
            this.LeyendaPiePagina = "Mucho agradecería nos envíe por correo electrónico, a la brevedad posible la factura correspondiente debiendo anotar las órdenes de trabajo o el número de órden de compra correspondientes a cada partida (Indispensable).";
        }

        [JsonIgnore]
        [Browsable(false)]
        public int Id { get; set; }

        [DisplayName("Concepto"), Description("Concepto que aparece en la versión impresa del comprobante.")]
        public string Concepto { get; set; }

        [DisplayName("Leyenda pie de página"), Description("Leyenda que se mostrará al pie de la página de la orden de compra.")]
        public string LeyendaPiePagina { get; set; }

        [DisplayName("Importe en letra"), Description("Incluir el importe con letra.")]
        [JsonProperty("importeLetra")]
        public bool IncluirImporteLetra { get; set; }

        [DisplayName("Domicilios de Entrega")]
        public BindingList<DomicilioFiscal> Domicilios { get; set; }

        public string Json() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, Formatting.None, conf);
        }

        public static OrdenCompraConfig Json(string inputString) {
            return JsonConvert.DeserializeObject<OrdenCompraConfig>(inputString);
        }
    }
}
