﻿using SqlSugar;

namespace Jaeger.Domain.Adquisiciones.Entities {
    [SugarTable("_ordcms", "proveedor: solicitud de cotizacion")]
    public class SolicitudCotizacionSingleModel : SolicitudCotizacionModel {
        public SolicitudCotizacionSingleModel() : base() {

        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
