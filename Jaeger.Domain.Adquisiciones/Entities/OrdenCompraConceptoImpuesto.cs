﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Adquisiciones.Entities {
    public class OrdenCompraConceptoImpuesto : BasePropertyChangeImplementation {
        private ImpuestoTipoEnum tipoField;
        private decimal baseField;
        private ImpuestoEnum impuestoField;
        private ImpuestoTipoFactorEnum tipoFactorField;
        private decimal tasaOCuotaField;
        private decimal importeField;

        public OrdenCompraConceptoImpuesto() {
            this.tipoField = ImpuestoTipoEnum.Traslado;
            this.impuestoField = ImpuestoEnum.IVA;
        }

        [JsonProperty("tipo")]
        public ImpuestoTipoEnum Tipo {
            get {
                return this.tipoField;
            }
            set {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("base")]
        public decimal Base {
            get {
                return this.baseField;
            }
            set {
                this.baseField = value;
                this.CalcularImporte();
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("impuesto")]
        public ImpuestoEnum Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipoFactor")]
        public ImpuestoTipoFactorEnum TipoFactor {
            get {
                return this.tipoFactorField;
            }
            set {
                this.tipoFactorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tasaOCuota")]
        public decimal TasaOCuota {
            get {
                return this.tasaOCuotaField;
            }
            set {
                this.tasaOCuotaField = value;
                this.CalcularImporte();
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return Math.Round(this.importeField, 4);
            }
            set {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }

        public void CalcularImporte() {
            this.Importe = this.Base * this.TasaOCuota;
        }
    }
}
