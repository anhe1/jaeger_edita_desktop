﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Tienda.Services {
    /// <summary>
    /// Catalogo de Prod
    /// </summary>
    public class CatalogoProductoService : Almacen.Services.CatalogoProductoService, Almacen.Contracts.IAlmacenService, Almacen.Contracts.ICatalogoProductoService, ICatalogoProductosService {
        protected internal Domain.Almacen.Contracts.ISqlProdServXRepository sqlProdServXRepository;
        protected ISqlModeloEspecificacionRepository modeloEspecificacionRepository;
        protected ISqlModeloExistenciaRepository modeloExistenciaRepository;
        protected internal ISqlPublicacionRepository publicacionRepository;

        public CatalogoProductoService(AlmacenEnum almacen) : base(almacen) {

        }

        public virtual T1 GetById<T1>(int id) where T1 : class, new() {
            throw new System.NotImplementedException();
        }

        public virtual bool Remover<T1>(T1 model) where T1 : class {
            throw new System.NotImplementedException();
        }

        public virtual T1 Save<T1>(T1 model) where T1 : class, new() {
            if (typeof(T1) == typeof(ProductoServicioXDetailModel)) {
                var d1 = this.Save1(model as ProductoServicioXDetailModel);
                return d1 as T1;
            } else if (typeof(T1) == typeof(ModeloXDetailModel)) {
                var d2 = this.Save1(model as ModeloXDetailModel);
                return d2 as T1;
            } else if (typeof(T1) == typeof(MedioModel)) {

            } else if (typeof(T1) == typeof(ModeloYEspecificacionModel)) {
                var d4 = this.Save1(model as ModeloYEspecificacionModel);
                return d4 as T1;
            }
            return null;
        }

        public virtual BindingList<EspecificacionModel> GetEspecificaciones() {
            throw new System.NotImplementedException();
        }

        public virtual List<VisibilidadModel> GetVisibilidad() {
            return Domain.Base.Services.EnumerationExtension.GetEnumToClass<VisibilidadModel, VisibilidadEnum>().ToList();
        }

        public virtual BindingList<ProductoServicioXDetailModel> Save(List<ProductoServicioXDetailModel> items) {
            throw new System.NotImplementedException();
        }

        public virtual List<ModeloXDetailModel> Save(List<ModeloXDetailModel> items) {
            throw new System.NotImplementedException();
        }

        public ProductoServicioXDetailModel Save1(ProductoServicioXDetailModel ProductoServicioModel) {
            if (ProductoServicioModel.Modelos != null) {
                if (ProductoServicioModel.Modelos.Count > 0) {
                    ProductoServicioModel.Modelos.ToList().ForEach(it => it.IdCategoria = ProductoServicioModel.IdCategoria);
                    foreach (var item in ProductoServicioModel.Modelos) {
                        this.Save1(item);
                    }
                }
            }
            return this.sqlProdServXRepository.Save(ProductoServicioModel);
        }

        public ModeloXDetailModel Save1(ModeloXDetailModel model) {
            for (int i = 0; i < model.Medios.Count; i++) {
                model.Medios[i].IdModelo = model.IdModelo;
                model.Medios[i] = this.mediosRepository.Save(model.Medios[i]);
            }
            var d0 = this.modelosRepository.Save(model as ModeloXDetailModel);
            d0.IdModelo = model.IdModelo;

            if (model.Disponibles != null) {
                for (int i = 0; i < model.Disponibles.Count; i++) {
                    model.Disponibles[i].IdProducto = model.IdProducto;
                    this.modeloEspecificacionRepository.Save(model.Disponibles[i]);
                    this.modeloExistenciaRepository.Save(model.Disponibles[i].GetExistenciaModel());
                }
            }

            if (model.Publicacion != null) {
                model.Publicacion.IdModelo = model.IdModelo;
                this.publicacionRepository.Saveable(model.Publicacion);
            }
            return model;
        }

        public ModeloYEspecificacionModel Save1(ModeloYEspecificacionModel seleccionado) {
            return this.modeloEspecificacionRepository.Save(seleccionado);
        }

        public List<IClasificacionSingle> GetCategorias() {
            throw new System.NotImplementedException();
        }
    }
}
