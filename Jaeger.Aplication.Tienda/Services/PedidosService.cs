﻿using System.Collections.Generic;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Tienda.Builder;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.Tienda.Services {

    public class PedidosService : PedidoService, IPedidosService {

        public PedidosService() : base() { }

        public List<PedidoClientePrinter> GetRemisiones(List<PedidoClientePrinter> pedidos) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.pedidoClienteRepository.GetList<T1>(conditionals);
        }

        public static IPedidoQueryBuilder Query() {
            return new PedidoQueryBuilder();
        }
    }
}
