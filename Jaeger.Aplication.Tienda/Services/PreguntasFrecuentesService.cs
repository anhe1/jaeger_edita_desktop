﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.DataAccess.Tienda.Repositories;

namespace Jaeger.Aplication.Tienda.Services {
    public class PreguntasFrecuentesService : IPreguntasFrecuentesService {
        protected internal ISqlFAQRepository sqlRepository;

        public PreguntasFrecuentesService() {
            this.sqlRepository = new SqlSugarFAQRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public virtual BindingList<PreguntaFrecuenteModel> GetList(bool onlyActive) {
            if (!onlyActive) {
                return new BindingList<PreguntaFrecuenteModel>(this.sqlRepository.GetList().ToList());
            }
            return new BindingList<PreguntaFrecuenteModel>(this.sqlRepository.GetList<PreguntaFrecuenteModel>(
                new List<IConditional> { new Conditional("TWFAQ_A", "1") })
                .ToList());
        }

        public virtual PreguntaFrecuenteModel Save(PreguntaFrecuenteModel model) {
            return this.sqlRepository.Save(model);
        }
    }
}
