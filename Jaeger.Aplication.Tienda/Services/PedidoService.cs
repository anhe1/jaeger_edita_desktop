﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.DataAccess.Tienda.Repositories;

namespace Jaeger.Aplication.Tienda.Services {
    public class PedidoService : IPedidoService {
        protected ISqlPedidoClienteRepository pedidoClienteRepository;
        protected ISqlPedidoClientePartidaRepository partidaRepository;
        protected Domain.Contribuyentes.Contracts.ISqlContribuyenteRepository contribuyenteRepository;
        protected Domain.Contribuyentes.Contracts.ISqlDomicilioRepository direccionRepository;

        public PedidoService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.pedidoClienteRepository = new SqlSugarPedidoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.contribuyenteRepository = new DataAccess.Contribuyentes.Repositories.SqlContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public bool Cancelar(IPedidoClienteStatusDetailModel model) {
            if (this.pedidoClienteRepository.Cancelar(model)) {
                return true;
            }
            return false;
        }

        public DomicilioFiscalDetailModel GetEmbarque(int index) {
            return this.direccionRepository.GetList<DomicilioFiscalDetailModel>(new List<IConditional> { new Conditional("DRCCN_DRCTR_ID", index.ToString()) }).FirstOrDefault();
        }

        public List<MetodoEnvioModel> GetEnvio() {
            return GetMetodosEnvio();
        }

        public BindingList<PedidoClientePartidaDetailModel> GetPartidas(int idPedido) {
            return new BindingList<PedidoClientePartidaDetailModel>(this.partidaRepository.GetList(new List<IConditional> { new Conditional("PDCLP_PDCLN_ID", idPedido.ToString()) }).ToList());
        }

        public PedidoClienteDetailModel GetPedido(int index) {
            var _response = this.pedidoClienteRepository.GetPedido(index);
            if (_response != null) {
                _response.Partidas = this.GetPartidas(index);
                _response.Autorizaciones = new BindingList<IPedidoClienteStatusModel>(this.pedidoClienteRepository.GetList<PedidoClienteStatusModel>(new List<IConditional> { new Conditional("PDCLS_PDCLN_ID", index.ToString()) }).ToList<IPedidoClienteStatusModel>());
                var d0 = new BindingList<IPedidoClienteRelacionadoModel>(this.pedidoClienteRepository.GetList<PedidoClienteRelacionadoModel>(new List<IConditional> { new Conditional("PDCLR_PDCLN_ID", index.ToString()) }).ToList<IPedidoClienteRelacionadoModel>());
            }
            return _response;
        }

        public PedidoClientePrinter GetPrinter(int index) {
            throw new System.NotImplementedException();
        }

        public List<RemisionDetailModel> GetRemisiones(int idPedido) {
            throw new System.NotImplementedException();
        }

        public PedidoClienteDetailModel Save(PedidoClienteDetailModel comprobante) {
            throw new System.NotImplementedException();
        }

        public List<ContribuyenteDetailModel> GetClientes() {
            var query = new Domain.Contribuyentes.Builder.ContribuyenteQueryBuilder();
            query.WithRelacionComercial(TipoRelacionComericalEnum.Cliente);
            return this.contribuyenteRepository.GetList<ContribuyenteDetailModel>(query.Build()).ToList();
        }

        #region metodos estaticos
        /// <summary>
        /// catalogo de metodos de envio
        /// </summary>
        public static List<MetodoEnvioModel> GetMetodosEnvio() {
            return new List<MetodoEnvioModel> {
                new MetodoEnvioModel(1, "01-Paquetería Interna"),
                new MetodoEnvioModel(2, "02-FEDEX"),
                new MetodoEnvioModel(3, "03-DHL")
            };
        }

        /// <summary>
        /// obtener lista de tamaños disponibles
        /// </summary>
        public static List<StatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<StatusModel, PedidoClienteStatusEnum>().ToList();
        }

        /// <summary>
        /// catalogo de tipos de cancelacion del pedido
        /// </summary>
        public static List<PedidoCancelacionTipoModel> GetCancelacionTipo() {
            return EnumerationExtension.GetEnumToClass<PedidoCancelacionTipoModel, PedidoCancelacionTipoEnum>().ToList();
        }

        /// <summary>
        /// obtener lista impuesto traslado IVA
        /// </summary>
        public static List<ImpuestoModel> GetFactorTrasladoIVA() {
            return EnumerationExtension.GetEnumToClass<ImpuestoModel, ImpuestoTipoFactorEnum>().ToList();
        }

        public static List<Domain.Base.Contracts.ISingleModel> GetOrignenes() {
            return EnumerationExtension.GetEnumToClass<SingleModel, PedigoOrigenEnum>().ToList<Domain.Base.Contracts.ISingleModel>();
        }

        public static List<FormaPagoModel> GetFormaPagos() {
            return EnumerationExtension.GetEnumToClass<FormaPagoModel, FormaPagoEnum>().ToList();
        }
        #endregion
    }
}
