﻿using Jaeger.Domain.Almacen.Builder;

namespace Jaeger.Aplication.Tienda.Services {
    public class CatalogoModeloService : Aplication.Almacen.Services.CatalogoModeloService, Almacen.Contracts.ICatalogoModeloService {
        #region metodos estaticos
        /// <summary>
        /// consulta para productos - modelos
        /// </summary>
        public new static IModelosQueryBuilder Query() {
            return new Domain.Tienda.Builder.ModelosQueryBuilder();
        }
        #endregion
    }
}
