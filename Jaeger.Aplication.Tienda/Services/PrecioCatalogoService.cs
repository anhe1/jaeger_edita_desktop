﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.DataAccess.Almacen.Repositories;
using Jaeger.DataAccess.Tienda.Repositories;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.ValueObjects;

namespace Jaeger.Aplication.Tienda.Services {
    public class PrecioCatalogoService : IPrecioCatalogoService {
        protected ISqlPrecioRepository precioRepository;
        protected ISqlPrecioPartidaRepository precioPartidaRepository;
        protected ISqlEspecificacionRepository especificacionRepository;

        public PrecioCatalogoService() {
            this.precioRepository = new SqlSugarPrecioRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.precioPartidaRepository = new SqlSugarPrecioPartidaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.especificacionRepository = new SqlSugarEspecificacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public virtual BindingList<PrecioDetailModel> GetList(bool onlyActive = true) {
            var condicion = new List<IConditional>();
            if (onlyActive) {
                condicion.Add(new Conditional("CTPRC_A", "1"));
            }
            var _response = new BindingList<PrecioDetailModel>(this.precioRepository.GetList(condicion).ToList());
            if (_response != null) {
                var _ids = _response.Select(it => it.IdPrecio).ToList();
                if (_ids.Count > 0) {
                    var _condicion1 = new List<IConditional>() { new Conditional("CTPRCP_CTPRC_ID", string.Join(",", _ids.ToArray()), ConditionalTypeEnum.In) };
                    if (onlyActive) {
                        _condicion1.Add(new Conditional("CTPRCP_A", "1"));
                    }

                    var _partidas = this.precioPartidaRepository.GetList(_condicion1);

                    if (_partidas != null) {
                        for (int i = 0; i < _response.Count; i++) {
                            _response[i].Items = new BindingList<PrecioPartidaModel>(_partidas.Where(it => it.IdPrecio == _response[i].IdPrecio).ToList());
                        }
                    }
                } else {
                    for (int i = 0; i < _response.Count; i++) {
                        _response[i].Items = new BindingList<PrecioPartidaModel>();
                    }
                }
            }

            return _response;
        }

        public virtual List<LPrecioModel> GetPrecio(int index) {
            return this.precioRepository.GetList(index, 0).ToList();
        }

        public List<EspecificacionModel> GetTamanios(bool onlyActive = true) {
            var condicion = new List<IConditional>();
            if (onlyActive) {
                condicion.Add(new Conditional("CTESPC_A", "1"));
            }
            return this.especificacionRepository.GetList<EspecificacionModel>(condicion).ToList();
        }

        /// <summary>
        /// obtener listado de prioridad del catalogo de precios
        /// </summary>
        public virtual List<PrioridadModel> GetPrioridad() {
            List<PrioridadModel> enums = ((CatalogoPrioridadEnum[])Enum.GetValues(typeof(CatalogoPrioridadEnum))).Select(c => new
            PrioridadModel() {
                Id = (int)c,
                Activo = 1,
                Descripcion = c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description
            }).ToList();
            return enums;
        }

        public virtual PrecioDetailModel Save(PrecioDetailModel model) {
            for (int i = 0; i < model.Items.Count; i++) {
                model.Items[i].IdPrecio = model.IdPrecio;
                model.Items[i] = this.Save(model.Items[i]);
            }
            return model;
        }

        public virtual PrecioPartidaModel Save(PrecioPartidaModel model) {
            return this.precioPartidaRepository.Save(model);
        }
    }
}
