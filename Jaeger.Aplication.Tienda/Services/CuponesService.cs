﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.DataAccess.Tienda.Repositories;

namespace Jaeger.Aplication.Tienda.Services {
    public class CuponesService : ICuponesService {
        protected internal ISqlCuponRepository cuponRepository;

        public CuponesService() { 
            this.cuponRepository = new SqlSugarCuponRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public virtual BindingList<CuponModel> GetList(bool onlyActive) {
            return new BindingList<CuponModel>(this.cuponRepository.GetList<CuponModel>(new List<IConditional> { new Conditional("TWCPN_A", "1") }).ToList());
        }

        public CuponModel Save(CuponModel model) {
            return this.cuponRepository.Save(model);
        }
    }
}
