﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Tienda.Contracts {
    /// <summary>
    /// Servicio: Catalogo de Productos
    /// </summary>
    public interface ICatalogoProductosService : Almacen.PT.Contracts.IProductosCatalogoService {
        AlmacenEnum Almacen { get; set; }
        List<IAlmacenModel> GetAlmacenes();

        T1 GetById<T1>(int id) where T1 : class, new();

        T1 Save<T1>(T1 model) where T1 : class, new();

        bool Remover<T1>(T1 model) where T1 : class;

        MedioModel Save(MedioModel medio, string fileName, string keyName);

        BindingList<UnidadModel> GetUnidades();

        /// <summary>
        /// obtener lista de visibilidad para la tienda web
        /// </summary>
        List<VisibilidadModel> GetVisibilidad();

        BindingList<EspecificacionModel> GetEspecificaciones();

        List<IClasificacionSingle> GetClasificacion();

        BindingList<ProductoServicioXDetailModel> Save(List<ProductoServicioXDetailModel> items);

        List<ModeloXDetailModel> Save(List<ModeloXDetailModel> items);
    }
}