﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface IPedidoService {
        PedidoClienteDetailModel GetPedido(int index);

        BindingList<PedidoClientePartidaDetailModel> GetPartidas(int idPedido);

        PedidoClienteDetailModel Save(PedidoClienteDetailModel comprobante);

        DomicilioFiscalDetailModel GetEmbarque(int index);

        bool Cancelar(IPedidoClienteStatusDetailModel model);

        PedidoClientePrinter GetPrinter(int index);

        List<ContribuyenteDetailModel> GetClientes();

        List<MetodoEnvioModel> GetEnvio();

        /// <summary>
        /// listado de remisiones relacionadas al pedido
        /// </summary>
        List<RemisionDetailModel> GetRemisiones(int idPedido);
    }
}