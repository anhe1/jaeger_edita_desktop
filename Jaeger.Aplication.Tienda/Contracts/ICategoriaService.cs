﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface ICategoriaService : Almacen.Contracts.ICategoriasService {
        BindingList<CategoriaModel> GetList();
    }
}