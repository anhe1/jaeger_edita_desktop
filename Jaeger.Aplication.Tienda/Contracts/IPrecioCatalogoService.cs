﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface IPrecioCatalogoService {
        /// <summary>
        /// obtener listado de prioridad del catalogo de precios
        /// </summary>
        List<PrioridadModel> GetPrioridad();

        BindingList<PrecioDetailModel> GetList(bool onlyActive = true);

        List<LPrecioModel> GetPrecio(int index);

        List<EspecificacionModel> GetTamanios(bool onlyActive = true);

        PrecioPartidaModel Save(PrecioPartidaModel model);

        PrecioDetailModel Save(PrecioDetailModel model);
    }
}