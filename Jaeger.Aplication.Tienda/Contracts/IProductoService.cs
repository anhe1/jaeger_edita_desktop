﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface IProductoService {
        AlmacenEnum Almacen { get; set; }

        T1 GetById<T1>(int id) where T1 : class, new();

        T1 Save<T1>(T1 model) where T1 : class, new();

        bool Remover<T1>(T1 model) where T1 : class;

        MedioModel Save(MedioModel medio, string fileName, string keyName);

        BindingList<UnidadModel> GetUnidades();
    }
}