﻿using System.ComponentModel;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface ICuponesService {
        BindingList<CuponModel> GetList(bool onlyActive);

        CuponModel Save(CuponModel model);
    }
}