﻿using System.ComponentModel;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface IPreguntasFrecuentesService {
        PreguntaFrecuenteModel Save(PreguntaFrecuenteModel model);

        BindingList<PreguntaFrecuenteModel> GetList(bool onlyActive);
    }
}