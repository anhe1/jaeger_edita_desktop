﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.Tienda.Contracts {
    public interface IPedidosService : IPedidoService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        List<PedidoClientePrinter> GetRemisiones(List<PedidoClientePrinter> pedidos);
    }
}