﻿using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.Tienda.Builder {
    public class ProductoModeloBuilder : Almacen.Builder.ProductoModeloBuilder, Almacen.Builder.IProductoModeloBuilder, IProductoModeloBuilder {
        public ProductoModeloBuilder() { }

        public override T Build<T>() {
            if (typeof(T) == typeof(ProductoServicioXDetailModel)) {
                var d0 = new ProductoServicioXDetailModel {
                    Almacen = this._Producto.Almacen,
                    Nombre = this._Producto.Nombre
                };
                return d0 as T;
            } else if (typeof(T) == typeof(ModeloXDetailModel)) {
                var d1 = new ModeloXDetailModel { Almacen = this._Producto.Almacen, FactorTrasladoIVA = "Tasa", ValorTrasladoIVA = new decimal(0.16), Tipo = ProdServTipoEnum.Producto };
                return d1 as T;
            } else if (typeof(T) == typeof(MedioModel)) {
                var d3 = new MedioModel();
                return d3 as T;
            }
            return base.Build<T>();
        }
    }
}
