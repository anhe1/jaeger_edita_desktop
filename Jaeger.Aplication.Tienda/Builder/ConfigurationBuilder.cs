﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Aplication.Tienda.Builder {
    public class ConfigurationBuilder : Domain.Empresa.Builder.ConfigurationBuilder, Domain.Empresa.Builder.IConfigurationBuilder, Domain.Empresa.Builder.IConfigurationBuild {
        private Domain.Tienda.Contracts.IConfiguration _Configuration;

        public ConfigurationBuilder() : base() {
            this._Configuration = new Domain.Tienda.Entities.Configuration();
        }

        public override List<IParametroModel> Build(IConfiguration configuration) {
            this._Configuration = (Domain.Tienda.Contracts.IConfiguration)configuration;
            var parameters = new List<IParametroModel> {
                new ParametroModel().WithKey(Domain.Empresa.Enums.ConfigKeyEnum.ConfiguracionBD).WithGroup(Domain.Empresa.Enums.ConfigGroupEnum.CPLite).With(this._Configuration.DataBase.Json()),
            };
            return parameters;
        }

        public override IConfiguration Build(List<IParametroModel> parametros) {
            if (parametros.Count > 0) {
                foreach (var parameter in parametros) {
                    switch (parameter.Key) {
                        case Domain.Empresa.Enums.ConfigKeyEnum.ConfiguracionBD:
                            this._Configuration.DataBase = GetDataDB(parameter.Data);
                            break;
                        default:
                            break;
                    }
                }
            } else {
                return null;
            }
            return this._Configuration;
        }

        private Domain.DataBase.Contracts.IDataBaseConfiguracion GetDataDB(string data) {
            if (!string.IsNullOrEmpty(data)) {
                var db = new Domain.DataBase.Entities.DataBaseConfiguracion().WithString(data);
                return db;
            }
            return null;
        }
    }
}
