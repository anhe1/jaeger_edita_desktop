﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using SqlSugar;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Services;

namespace Jaeger.DataAccess.Abstractions {
    public abstract class SqlSugarContext<T> : Repository, IGenericRepository<T> where T : class, new() {
        private readonly SqlSugarClient dbase;

        public SqlSugarContext(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.User = "SYSDBA";
            this.dbase = new SqlSugarClient(new ConnectionConfig() {
                ConnectionString = this.StringBuilder(configuracion).ToString(), //Master Connection
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute,
                SlaveConnectionConfigs = new List<SlaveConnectionConfig>() {
                     new SlaveConnectionConfig() { HitRate = 10, ConnectionString = this.StringBuilder(configuracion).ToString() } ,
                     new SlaveConnectionConfig() { HitRate = 10, ConnectionString = this.StringBuilder(configuracion).ToString() }
                },
                AopEvents = new AopEvents() {
                    OnLogExecuting = (sql, p) => {
                        Console.WriteLine(sql);
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                        LogErrorService.LogWrite(string.Concat("OnLogExecuted SQL: ", sql));
                    },
                    OnLogExecuted = (sql, p) => {
                        Console.WriteLine(string.Concat("OnLogExecuted SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    },
                    OnError = (exp) => {
                        Console.WriteLine(exp.Message);
                        LogErrorService.LogWrite("SqlSugarContext:exp: " + exp.Sql);
                    },
                    OnDiffLogEvent = (e) => {
                        LogErrorService.LogWrite("SqlSugarContext: " + e.Sql);
                    }
                }
            });
        }

        ~SqlSugarContext() {
            if (this.dbase != null) {
                try {
                    this.dbase.Close();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public SqlSugarClient Db {
            get {
                return this.dbase;
            }
        }

        public SimpleClient<T> CurrentDb {
            get {
                return new SimpleClient<T>(Db);
            }
        }

        public virtual T GetById(int id) {
            this.IsError = false;
            return CurrentDb.GetById(id);
        }

        public virtual IEnumerable<T> GetList() {
            this.IsError = false;
            return CurrentDb.GetList();
        }

        public virtual bool Delete(int id) {
            this.IsError = false;
            return CurrentDb.DeleteById(id);
        }

        public virtual int Insert(T objeto) {
            try {
                this.IsError = false;
                var response = this.CurrentDb.InsertReturnIdentity(objeto);
                return response;
            } catch (SqlSugarException ex) {
                this.LogError(ex);
                return 0;
            }
        }

        public virtual int Update(T objeto) {
            this.IsError = false;
            return this.CurrentDb.AsUpdateable(objeto).ExecuteCommand();
        }

        public virtual bool CreateTable() {
            try {
                this.IsError = false;
                this.Db.CodeFirst.InitTables<T>();
                return true;
            } catch (SqlSugarException ex) {
                this.LogError(ex);
            }
            return false;
        }

        public virtual void GenerationClass(string classProjectName, string classPath, string classNamespace, string classDirectory) {
            this.IsError = false;
            this.Db.DbFirst.IsCreateAttribute().CreateClassFile(classDirectory, classNamespace);
        }

        public string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }

        private void LogError(SqlSugarException ex) {
            LogErrorService.LogWrite("Create Table: Sugar: " + ex.Message);
            Console.WriteLine(ex.Message);
            this.Message = ex.Message;
            this.IsError = true;
        }
    }
}
