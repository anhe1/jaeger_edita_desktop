﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Contracts;

namespace Jaeger.DataAccess.Abstractions {
    /// <summary>
    /// Version Anterior
    /// </summary>
    public abstract class Repository2Master : Repository, IRepositoryMaster {

        /// <summary>
        /// lista de parametros al pasar comando sql en modo texto
        /// </summary>
        protected List<MySqlParameter> parameters;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public Repository2Master(DataBaseConfiguracion configuracion)
            : base(configuracion) {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected int ExecuteNoQuery(MySqlCommand sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;
                return sqlCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// ejecutar sentencia SQL en modo texto, es necesario incluir los parametros en propiedad "parametros"
        /// </summary>
        /// <param name="sqlcommand"></param>
        /// <returns></returns>
        protected int ExecuteScalar(string sqlcommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new MySqlCommand()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlcommand;
                    comando.Parameters.AddRange(this.parameters.ToArray());
                    int indice = 0;
                    try {
                        indice = Convert.ToInt32(comando.ExecuteScalar());
                    } catch (Exception ex) {
                        var fbex = ex as MySqlException;
                        if (fbex != null)
                            Console.WriteLine("MySql: " + fbex.Message);
                        else
                            Console.WriteLine("Exception: " + ex.Message);
                        indice = 0;
                    }
                    return indice;
                }
            }
        }

        protected int ExecuteScalar(MySqlCommand sqlcommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlcommand.Connection = connection;

                if (this.parameters != null)
                    sqlcommand.Parameters.AddRange(this.parameters.ToArray());

                int indice = 0;
                try {
                    indice = Convert.ToInt32(sqlcommand.ExecuteScalar());
                } catch (Exception ex) {
                    var sqlEx = ex as MySqlException;
                    if (sqlEx != null)
                        Console.WriteLine("MySql: " + sqlEx.Message);
                    else
                        Console.WriteLine("Exception: " + ex.Message);
                    indice = 0;
                }

                return indice;
            }
        }

        /// <summary>
        /// ejecutar comando MySqlCommand, este debe incluir los parametros
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader1(MySqlCommand sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;

                var reader = sqlCommand.ExecuteReader();
                using (var table = new DataTable()) {

                    table.Load(reader);
                    reader.Dispose();
                    return table;
                }
            }
        }

        protected DataTable ExecuteReader(MySqlCommand sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;
                using (var adapter = new MySqlDataAdapter(sqlCommand)) {
                    using (var table = new DataTable()) {
                        adapter.Fill(table);
                        return table;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader(string sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new MySqlCommand()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlCommand;
                    if (this.parameters != null)
                        comando.Parameters.AddRange(this.parameters.ToArray());
                    return this.ExecuteReader(comando);
                }
            }
        }

        /// <summary>
        /// ejecutar transaccion 
        /// </summary>
        /// <param name="sqlCommand">objeto MySqlCommand</param>
        /// <returns></returns>
        protected int ExecuteTransaction(MySqlCommand sqlCommand) {
            int response = 0;
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var transaction = connection.BeginTransaction()) {
                    sqlCommand.Connection = connection;
                    sqlCommand.Transaction = transaction;
                    response = sqlCommand.ExecuteNonQuery();
                    try {
                        transaction.Commit();
                    } catch (Exception ex) {
                        var fbex = ex as MySqlException;
                        if (fbex != null)
                            Console.WriteLine("firebird: " + fbex.Message);
                        response = 0;
                    }
                }
            }
            return response;
        }

        protected int GetIndex(string field, string tablename) {
            int lNewIndex = 0;
            try {
                lNewIndex = this.ExecuteScalar(new MySqlCommand { CommandText = string.Concat("select max(", field, ") from ", tablename) });
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                lNewIndex = -1;
            }
            return lNewIndex + 1;
        }
    }
}
