﻿using MySql.Data.MySqlClient;

namespace Jaeger.DataAccess.Abstractions {
    /// <summary>
    /// clase abstracta para del repositorio
    /// </summary>
    public abstract class Repository {
        private readonly string connectionString;
        private readonly Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public Repository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) {
            this.configuracion = configuracion;
            this.connectionString = this.StringBuilder(configuracion).ToString();
        }

        public string User {
            get; set;
        }

        public string Message {
            get; set;
        }

        public bool IsError {
            get; set;
        }

        /// <summary>
        /// obtener conexion valida a la base de datos
        /// </summary>
        /// <returns>retorna un objeto FbConnection</returns>
        protected MySqlConnection GetConnection() {
            return new MySqlConnection(this.connectionString);
        }

        /// <summary>
        /// generador de cadena de conexion de la base de datos
        /// </summary>
        /// <param name="configuracion">objeto de configuracion de la base de datos</param>
        /// <returns>retorna un objeto FbConnectionStringBuilder</returns>
        public MySqlConnectionStringBuilder StringBuilder(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) {
            int puerto = 4530;
            if (uint.Parse(configuracion.PortNumber.ToString()) != 0)
                puerto = configuracion.PortNumber;
            var builder = new MySqlConnectionStringBuilder {
                UserID = configuracion.UserID,
                Password = configuracion.Password,
                Server = configuracion.HostName,
                Database = configuracion.Database,
                Port = uint.Parse(puerto.ToString()),
                SslMode = MySqlSslMode.None,// (MySqlSslMode)configuracion.ModoSSL,
                Pooling = configuracion.Pooling,
                ConnectionLifeTime = 240, //configuracion.ConnectionLifeTime,
                ConnectionTimeout = 240, //configuracion.ConnectionTimeout,
                CharacterSet = configuracion.Charset,
                DefaultCommandTimeout = 120
            };
            return builder;
        }
    }
}
