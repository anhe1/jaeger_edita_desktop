﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using SqlSugar;
using Jaeger.Domain.Contracts;
using Jaeger.DataAccess.Services;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Abstractions {
    public abstract class MySqlSugarContext<T> : RepositoryMaster<T>, IGenericRepository<T> where T : class, new() {
        private readonly SqlSugarClient dbase;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de base de datos</param>
        public MySqlSugarContext(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
            this.dbase = new SqlSugarClient(new ConnectionConfig() {
                ConnectionString = this.StringBuilder(configuracion).ToString(), //Master Connection
                DbType = SqlSugar.DbType.MySql,
                IsAutoCloseConnection = false,
                InitKeyType = InitKeyType.Attribute,
                SlaveConnectionConfigs = new List<SlaveConnectionConfig>() {
                     new SlaveConnectionConfig() { HitRate = 1, ConnectionString = this.StringBuilder(configuracion).ToString() } ,
                     new SlaveConnectionConfig() { HitRate = 1, ConnectionString = this.StringBuilder(configuracion).ToString() }
                },
                AopEvents = new AopEvents() {
                    OnLogExecuting = (sql, p) => {
                        Console.WriteLine(sql);
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                        LogErrorService.LogWrite(string.Concat("OnLogExecuted SQL: ", sql));
                    },
                    OnLogExecuted = (sql, p) => {
                        Console.WriteLine(string.Concat("OnLogExecuted SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    },
                    OnError = (exp) => {
                        Console.WriteLine(exp.Message);
                        var pendejo = exp.Data;
                        LogErrorService.LogWrite("OnError: " + exp.Message);
                    },
                    OnDiffLogEvent = (e) => {
                        LogErrorService.LogWrite("OnDiffLogEvent: " + e.ToString());
                    }
                }
            });
        }

        ~MySqlSugarContext() {
            if (this.Db != null) {
                try {
                    this.Db.Close();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        #region SqlSugar
        public SqlSugarClient Db {
            get {
                return this.dbase;
            }
        }

        public SimpleClient<T> CurrentDb {
            get {
                return new SimpleClient<T>(Db);
            }
        }

        public virtual T GetById(int id) {
            return CurrentDb.GetById(id);
        }

        public virtual IEnumerable<T> GetList() {
            return CurrentDb.GetList();
        }

        public virtual bool Delete(int id) {
            return CurrentDb.DeleteById(id);
        }

        public virtual int Insert(T objeto) {
            return this.CurrentDb.InsertReturnIdentity(objeto);
        }

        public virtual int Update(T objeto) {
            return this.CurrentDb.AsUpdateable(objeto).ExecuteCommand();
        }

        public virtual bool CreateTable() {
            try {
                this.Db.CodeFirst.InitTables<T>();
                return true;
            } catch (SqlSugarException ex) {
                LogErrorService.LogWrite("Create Table: Sugar: " + ex.Message);
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// funciones como extract no funcionan porque la condicional no lo transforma como funcion necesito alternativa
        /// </summary>
        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<T1>().Where(_conditionalList).ToList();
        }

        public virtual void GenerationClass(string classProjectName, string classPath, string classNamespace, string classDirectory) {
            this.Db.DbFirst.IsCreateAttribute().CreateClassFile(classDirectory, classNamespace);
        }

        public new string Message { get; set; }
        #endregion

        public string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            var provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }

        protected MySqlCommand GetCommand(string sql, List<SugarParameter> parameters) {
            var sqlCommand = new MySqlCommand {
                CommandText = sql
            };

            sqlCommand = this.SetParameters(sqlCommand, parameters);
            LogErrorService.LogWrite(sql);
            return sqlCommand;
        }

        private MySqlCommand SetParameters(MySqlCommand sqlCommand, List<SugarParameter> parameters = null) {
            if (parameters == null) { return sqlCommand; }

            foreach (var item in parameters) {
                Console.WriteLine(item.ParameterName + " = " + item.Value);
                var e = sqlCommand.Parameters.Contains(item.ParameterName);
                if (e == false) {
                    sqlCommand.Parameters.AddWithValue(item.ParameterName, item.Value);
                }
            }
            return sqlCommand;
        }

        protected int Execute(IInsertable<T> result) {
            try {
                return result.ExecuteReturnIdentity();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
                var r = result.ToSql();
                return this.ExecuteScalar(this.GetCommand(r.Key, r.Value));
            }
        }

        protected int Execute<T1>(IInsertable<T1> d) {
            try {
                return d.ExecuteReturnIdentity();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
                var r = d.ToSql();
                return this.ExecuteScalar(this.GetCommand(r.Key, r.Value));
            }
        }

        /// <summary>
        /// actualizar
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        protected int Execute(IUpdateable<T> result) {
            try {
                return result.ExecuteCommand();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
                var r = result.ToSql();
                return this.ExecuteNoQuery(this.GetCommand(r.Key, r.Value));
            }
        }

        /// <summary>
        /// insertar registro utilizando los dos metodos
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="d"></param>
        /// <returns></returns>
        protected int Insert<T1>(IInsertable<T1> d) where T1 : class, new() {
            try {
                return d.ExecuteReturnIdentity();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
                var r = d.ToSql();
                return this.ExecuteScalar(this.GetCommand(r.Key, r.Value));
            }
        }

        protected int Execute<T1>(IUpdateable<T1> result) where T1 : class, new() {
            try {
                return result.ExecuteCommand();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
                var r = result.ToSql();
                return this.ExecuteNoQuery(this.GetCommand(r.Key, r.Value));
            }
        }

        protected int ExecuteScalar(string sql, List<SugarParameter> parameters) {
            var sqlCommand = new MySqlCommand(sql);
            foreach (var item in parameters) {
                sqlCommand.Parameters.Add(new MySqlParameter(item.ParameterName, item.Value));
            }
            return this.ExecuteScalar(sqlCommand);
        }

        protected int ExecuteTransaction(string sql, List<SugarParameter> parameters) {
            var sqlCommand = new MySqlCommand(sql);
            sqlCommand.Parameters.AddRange(Parameters(parameters));
            return this.ExecuteTransaction(sqlCommand);
        }

        protected int ExecuteTransaction(string sql) {
            return this.ExecuteTransaction(new MySqlCommand(sql));
        }

        protected IEnumerable<T1> GetMapper<T1>(string sqlcommand, List<SugarParameter> parameters) where T1 : class, new() {
            var tabla = this.ExecuteReader(this.GetCommand(sqlcommand, parameters));
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(string sqlcommand, List<SqlParameter> parameters = null) where T1 : class, new() {
            if (parameters != null) {
                var _parameters = new List<SugarParameter>();
                foreach (var item in parameters) {
                    _parameters.Add(new SugarParameter(item.ParameterName, item.Value));
                }
                var tabla = this.ExecuteReader(this.GetCommand(sqlcommand, _parameters));
                var mapper = new DataNamesMapper<T1>();
                return mapper.Map(tabla).ToList();
            }
            var tabla1 = this.ExecuteReader(this.GetCommand(sqlcommand, new List<SugarParameter>()));
            var mapper1 = new DataNamesMapper<T1>();
            return mapper1.Map(tabla1).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(string sqlCommand, List<IConditional> conditionals) where T1 : class, new() {
            KeyValuePair<string, SugarParameter[]> where = new KeyValuePair<string, SugarParameter[]>();
            if (conditionals != null) {
                if (conditionals.Count > 0) {
                    where = MySql.Services.ExpressionTool.ConditionalToSql(conditionals);
                    if (sqlCommand.Contains("@wcondiciones")) {
                        sqlCommand = sqlCommand.Replace("@wcondiciones", " where " + where.Key);
                    } else {
                        sqlCommand = sqlCommand.Replace("@condiciones", " and " + where.Key);
                    }
                }
            }
            sqlCommand = sqlCommand.Replace("@wcondiciones", "");
            sqlCommand = sqlCommand.Replace("@condiciones", "");
            var d1 = this.Db.Ado.GetDataTable(sqlCommand, where.Value);
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(d1).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.Db.Queryable<T1>().Where(this.Convierte(conditionals)).ToList();
        }

        public List<IConditionalModel> Convierte(List<IConditional> conditional) {
            var _condiciones = new List<IConditionalModel>();
            if (conditional != null) {
                foreach (var item in conditional) {
                    _condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return _condiciones;
        }

        protected MySqlParameter[] Parameters(List<SugarParameter> parameters) {
            var Parameters = new List<MySqlParameter>();
            foreach (var item in parameters) {
                Parameters.Add(new MySqlParameter(item.ParameterName, item.Value));
            }
            return Parameters.ToArray();
        }
    }
}
