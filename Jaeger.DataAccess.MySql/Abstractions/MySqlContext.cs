﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using Jaeger.DataAccess.Entities;
using Jaeger.DataAccess.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Abstractions {
    /// <summary>
    /// Version Anterior
    /// </summary>
    public class MySqlContext : Repository, IRepositoryMaster {
        protected int reads;
        protected int writes;
        private MySqlConnection connectionRead;
        private MySqlConnection connectionWrite;

        public DataBaseConfiguracion Configuracion;

        public MySqlContext(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.reads = 0;
            this.writes = 0;
        }

        public new MessageError Message { get; set; }

        public int GetIndex(string oField, string oTable) {
            this.Message = null;
            int lNewIndex = 0;
            try {
                lNewIndex = this.ExecuteScalar(new MySqlCommand { CommandText = string.Concat("select max(", oField, ") from ", oTable) });
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                this.Message = new MessageError(ex.Message, 0, ex.Source);
                lNewIndex = -1;
            }
            return lNewIndex + 1;
        }

        public bool Execute(MySqlCommand sqlCommand) {
            bool result = false;
            sqlCommand = ExecuteCommand(sqlCommand);
            try {
                result = (sqlCommand.ExecuteNonQuery() > 0 ? true : false);
            }
            catch (Exception ex) {
                result = false;
                if ((ex.ToString().Contains("duplicate") ? true : ex.ToString().Contains("duplicado")))
                    this.Message = new MessageError("Ya existe!", 0, "");
                else
                    this.Message = new MessageError(ex.Message);
            }
            return result;
        }

        public int ExecuteScalar(MySqlCommand sqlCommand) {
            int response = 0;
            try {
                sqlCommand = this.ExecuteCommand(sqlCommand);
                try {
                    response = Convert.ToInt32(sqlCommand.ExecuteScalar());
                }
                catch (Exception ex) {
                    this.Message = new MessageError("ExecuteScalar: " + ex.Message);
                    response = 0;
                }
                finally {
                    sqlCommand.Dispose();
                }
            }
            catch (MySqlException ex) {
                this.Message = new MessageError("ExecuteScalar: " + ex.Message);
                response = 0;
            }
            return response;
        }

        public int ExecuteScalarTransaccion(MySqlCommand sqlCommand) {
            int intResponse = 0;
            MySqlTransaction sqlTransaccion = null;
            sqlCommand = this.ExecuteCommand(sqlCommand);
            sqlTransaccion = sqlCommand.Connection.BeginTransaction();
            sqlCommand.Transaction = sqlTransaccion;
            try {
                intResponse = Convert.ToInt32(sqlCommand.ExecuteScalar());
                sqlTransaccion.Commit();
            }
            catch (Exception ex) {
                this.Message = new MessageError("ExecuteScalarTransaccion: " + ex.Message);
                sqlTransaccion.Rollback();
                return 0;
            }
            return intResponse;
        }

        public bool ExecuteTransaccion(MySqlCommand sqlCommand) {
            bool response = false;

            sqlCommand = this.ExecuteCommand(sqlCommand);
            if (sqlCommand.CommandText.ToLower().StartsWith("select"))
                sqlCommand.Transaction = this.connectionRead.BeginTransaction();
            else
                sqlCommand.Transaction = this.connectionWrite.BeginTransaction();

            try {
                response = (sqlCommand.ExecuteNonQuery() > 0 ? true : false);
                sqlCommand.Transaction.Commit();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                this.Message = new MessageError("ExecuteTransaccion: " + ex.Message);
                sqlCommand.Transaction.Rollback();
                response = false;
            }
            return response;
        }

        public DataSet GetDataSet(string sqlString) {
            var dataset = new DataSet();
            try {
                var oDataAdapter = new MySqlDataAdapter(sqlString, this.connectionRead);
                if (oDataAdapter.SelectCommand.CommandTimeout == 30) {
                    oDataAdapter.SelectCommand.CommandTimeout = 0;
                }
                oDataAdapter.Fill(dataset);
            }
            catch (MySqlException ex) {
                this.Message = new MessageError("GetDataSet: " + ex.Message);
                dataset = null;
            }
            return dataset;
        }

        public DataTable ExecuteReader(string sqlString) {
            try {
                return ExecuteReader(new MySqlCommand { CommandText = sqlString });
            }
            catch (MySqlException ex) {
                this.Message = new MessageError(ex.Message);
            }
            return null;
        }

        public DataTable ExecuteReader(MySqlCommand sqlCommand) {
            var oDataTable = new DataTable();

            try {
                sqlCommand = this.ExecuteCommand(sqlCommand);
                var oDataAdapter = new MySqlDataAdapter(sqlCommand);
                try {
                    oDataAdapter.Fill(oDataTable);
                }
                catch (Exception ex) {
                    this.Message = new MessageError(ex.Message);
                    Console.WriteLine("GetDataTable" + ex.Message);
                }
            }
            catch (MySqlException ex) {
                this.Message = new MessageError(ex.Message);
            }
            return oDataTable;
        }

        private MySqlCommand ExecuteCommand(MySqlCommand sqlCommand) {
            this.Message = null;
            if (sqlCommand.CommandText.ToLower().StartsWith("select")) {
                if (this.connectionRead == null | this.reads >= 500) {
                    this.connectionRead = this.GetConnection();
                    this.connectionRead.Open();
                    this.reads = 0;
                }
                sqlCommand.Connection = this.connectionRead;
                this.reads++;
            }
            else {
                if (this.connectionWrite == null | this.writes >= 500) {
                    this.connectionWrite = this.GetConnection();
                    this.connectionWrite.Open();
                    this.writes = 0;
                }
                sqlCommand.Connection = this.connectionWrite;
                this.writes++;
            }
            return sqlCommand;
        }
    }
}
