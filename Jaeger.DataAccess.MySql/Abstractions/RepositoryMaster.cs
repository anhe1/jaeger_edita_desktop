﻿using System;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Jaeger.Domain.Services.Mapping;
using Jaeger.DataAccess.Contracts;
using Jaeger.DataAccess.Services;

namespace Jaeger.DataAccess.Abstractions {
    public abstract class RepositoryMaster<T> : Repository, IRepositoryMaster where T : class, new() {

        /// <summary>
        /// lista de parametros al pasar comando sql en modo texto
        /// </summary>
        protected List<MySqlParameter> parameters;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public RepositoryMaster(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion)
            : base(configuracion) {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected int ExecuteNoQuery(DbCommand sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;
                return sqlCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// ejecutar sentencia SQL en modo texto, es necesario incluir los parametros en propiedad "parametros"
        /// </summary>
        /// <param name="sqlcommand"></param>
        /// <returns></returns>
        protected int ExecuteScalar(string sqlcommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new MySqlCommand()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlcommand;
                    comando.Parameters.AddRange(this.parameters.ToArray());
                    int indice = 0;
                    try {
                        indice = Convert.ToInt32(comando.ExecuteScalar());
                    } catch (Exception ex) {
                        var fbex = ex as MySqlException;
                        if (fbex != null)
                            Console.WriteLine("MySql: " + fbex.Message);
                        else
                            Console.WriteLine("Exception: " + ex.Message);
                        LogErrorService.LogWrite(ex.Message);
                        indice = 0;
                    }
                    return indice;
                }
            }
        }

        protected int ExecuteScalar(DbCommand sqlcommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlcommand.Connection = connection;

                if (this.parameters != null)
                    sqlcommand.Parameters.AddRange(this.parameters.ToArray());

                int indice = 0;
                try {
                    indice = Convert.ToInt32(sqlcommand.ExecuteScalar());
                } catch (Exception ex) {
                    var sqlEx = ex as MySqlException;
                    if (sqlEx != null)
                        Console.WriteLine("MySql: " + sqlEx.Message);
                    else
                        Console.WriteLine("Exception: " + ex.Message);
                    LogErrorService.LogWrite(ex.Message);
                    indice = 0;
                }

                return indice;
            }
        }

        protected DataTable ExecuteReader(DbCommand sqlCommand) {
            var command = new MySqlCommand {
                CommandText = sqlCommand.CommandText,
                CommandTimeout = 320
            };

            foreach (var item in sqlCommand.Parameters) {
                command.Parameters.Add(item);
            }

            using (var connection = this.GetConnection()) {
                connection.Open();
                command.Connection = connection;
                using (var adapter = new MySqlDataAdapter(command)) {
                    using (var table = new DataTable()) {
                        adapter.Fill(table);
                        return table;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader(string sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new MySqlCommand()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlCommand;
                    if (this.parameters != null)
                        comando.Parameters.AddRange(this.parameters.ToArray());
                    return this.ExecuteReader(comando);
                }
            }
        }

        /// <summary>
        /// ejecutar transaccion 
        /// </summary>
        /// <param name="sqlCommand">objeto MySqlCommand</param>
        /// <returns></returns>
        protected int ExecuteTransaction(DbCommand sqlCommand) {
            int response = 0;
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var transaction = connection.BeginTransaction()) {
                    sqlCommand.Connection = connection;
                    sqlCommand.Transaction = transaction;
                    response = sqlCommand.ExecuteNonQuery();
                    try {
                        transaction.Commit();
                    } catch (Exception ex) {
                        var fbex = ex as MySqlException;
                        if (fbex != null)
                            Console.WriteLine("firebird: " + fbex.Message);
                        LogErrorService.LogWrite(ex.Message);
                        response = 0;
                    }
                }
            }
            return response;
        }

        protected int GetIndex(string field, string tablename) {
            int lNewIndex = 0;
            try {
                lNewIndex = this.ExecuteScalar(new MySqlCommand { CommandText = string.Concat("select max(", field, ") from ", tablename) });
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
                lNewIndex = -1;
            }
            return lNewIndex + 1;
        }

        /// <summary>
        /// mapper
        /// </summary>
        /// <typeparam name="T1">clase</typeparam>
        /// <param name="dataTable">data table</param>
        /// <returns>Ienumerable class</returns>
        protected IEnumerable<T1> GetMapper<T1>(DataTable dataTable) where T1 : class, new() {
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(dataTable).ToList();
        }

        /// <summary>
        /// mapper
        /// </summary>
        /// <typeparam name="T1">type class</typeparam>
        /// <param name="stringCommand">cadena sql</param>
        /// <returns>IEnumerable class</returns>
        protected IEnumerable<T1> GetMapper<T1>(string stringCommand) where T1 : class, new() {
            var tabla = this.ExecuteReader(stringCommand);
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }
    }
}
