﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.Services;

namespace Jaeger.DataAccess.MySql.Services {
    public class SqlSugarMaintenance : Repository {
        public partial class Tabla {
            public string Nombre {
                get; set;
            }

            public string Descripcion {
                get; set;
            }
        }

        public SqlSugarMaintenance(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.Configuracion = configuracion;
            //this.Message = new MessageError();
            this.dbase = new SqlSugarClient(new ConnectionConfig() {
                ConnectionString = this.StringBuilderToString(),
                DbType = DbType.MySql,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute,
                AopEvents = new AopEvents() {
                    OnLogExecuting = (sql, p) => {
                        Console.WriteLine(string.Concat("Executing SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    }
                }
            });
        }

        public SqlSugarClient dbase {
            get; set;
        }

        public DataBaseConfiguracion Configuracion {
            get; set;
        }

        /// <summary>
        /// crear mapeo de clases de la base
        /// </summary>
        /// <param name="output">ruta donde se almacenan los archivos resultantes</param>
        public void CreateClass(string output) {
            foreach (var item in this.dbase.DbMaintenance.GetTableInfoList()) {
                string entityName = item.Name.ToUpper();/*Format class name*/
                dbase.MappingTables.Add(entityName, item.Name);
                foreach (var col in dbase.DbMaintenance.GetColumnInfosByTableName(item.Name)) {
                    dbase.MappingColumns.Add(col.DbColumnName.ToUpper() /*Format class property name*/, col.DbColumnName, entityName);
                }
            }
            dbase.DbFirst.SettingClassTemplate(old => { return old; })
                       .SettingNamespaceTemplate(old => { return old; })
                       .SettingPropertyDescriptionTemplate(old => {
                           return @"        /// <summary>
        /// obtener o establecer {PropertyDescription} Default_New:{DefaultValue} Nullable_New:{IsNullable}
        /// </summary>";
                       })
                        .SettingPropertyTemplate(old => { return @"
        [DataNames(""{PropertyName}"")] {SugarColumn}ColumnDescription 
        public {PropertyType} {PropertyName} {get { return this._{PropertyName}; } set { this._{PropertyName} = value; this.OnPropertyChanged(); }}"; })
                        .SettingConstructorTemplate(old => { return old; }).IsCreateAttribute().CreateClassFile(output, "Models");
        }

        public List<Tabla> GetTablesInfo() {
            List<Tabla> lista = new List<Tabla>();
            var tables = this.dbase.DbMaintenance.GetTableInfoList();
            foreach (var item in tables) {
                lista.Add(new Tabla { Nombre = item.Name, Descripcion = item.Description });
            }
            return lista;
        }

        public bool ConnectionOpen() {
            MySqlConnection conexion = new MySqlConnection();
            try {
                conexion.ConnectionString = this.StringBuilderToString();
                conexion.Open();
                conexion.Close();
                return true;
            } catch (Exception ex) {
                var exception = ex as MySqlException;
                if (exception != null) {
                    this.Message = ex.Message;
                }
                LogErrorService.LogWrite(ex.Message);
                Console.WriteLine(ex.Message);
            } finally {
                conexion.Close();
            }

            return false;
        }

        private string StringBuilderToString() {
            MySqlConnectionStringBuilder cs = new MySqlConnectionStringBuilder() {
                Pooling = false,
                Database = this.Configuracion.Database,
                Server = this.Configuracion.HostName,
                UserID = this.Configuracion.UserID,
                Port = uint.Parse(this.Configuracion.PortNumber.ToString()),
                Password = this.Configuracion.Password,
            };
            return cs.ConnectionString;
        }
    }
}
