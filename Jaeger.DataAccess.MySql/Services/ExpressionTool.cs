﻿using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Jaeger.DataAccess.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.MySql.Services {
    public static class ExpressionTool {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand">si contiene @wcondiciones se agrega WHERE sino AND (@condiciones)</param>
        public static string Where(string sqlCommand, List<IConditional> conditionals) {
            if (conditionals != null) {
                if (conditionals.Count > 0) {
                    var where = ExpressionTool.ConditionalModelToSql(conditionals);
                    if (sqlCommand.Contains("@wcondiciones")) {
                        sqlCommand = sqlCommand.Replace("@wcondiciones", " WHERE " + where.Key);
                    } else {
                        sqlCommand = sqlCommand.Replace("@condiciones", " AND " + where.Key);
                    }
                    //sqlCommand.Parameters.AddRange(where.Value);
                } else {
                    sqlCommand = sqlCommand.Replace("@condiciones", "");
                    sqlCommand = sqlCommand.Replace("@wcondiciones", "");
                }
            }

            return sqlCommand;
        }

        //public static MySqlCommand Where(MySqlCommand sqlCommand, List<IConditional> conditionals) {
        //    if (conditionals != null) {
        //        if (conditionals.Count > 0) {
        //            var where = ExpressionTool.ConditionalModelToSql(conditionals);
        //            if (sqlCommand.CommandText.Contains("@wcondiciones")) {
        //                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", " WHERE " + where.Key);
        //            } else {
        //                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", " AND " + where.Key);
        //            }
        //            //sqlCommand.Parameters.AddRange(where.Value);
        //        } else {
        //            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
        //            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "");
        //        }
        //    }

        //    return sqlCommand;
        //}

        public static KeyValuePair<string, MySqlParameter[]> ConditionalModelToSql(List<IConditional> models) {
            StringBuilder builder = new StringBuilder();
            var parameters = new List<MySqlParameter>();
            foreach (var item in models) {
                var index = models.IndexOf(item);
                var type = index == 0 ? "" : "AND";
                string temp = " {0} {1} {2} {3}  ";
                // en caso de enviar alguna funcion se remueven los espacios para crear el nombre del parametro
                string parameterName = string.Format("{0}Conditional{1}{2}", "@", item.FieldName.Replace(" ", "").Trim(), index);
                if (parameterName.Contains(".")) {
                    parameterName = parameterName.Replace(".", "_");
                }
                if (parameterName.Contains("[")) {
                    parameterName = parameterName.Replace("[", "_");
                }
                if (parameterName.Contains("]")) {
                    parameterName = parameterName.Replace("]", "_");
                }
                if (parameterName.Contains("(")) {
                    parameterName = parameterName.Replace("(", "");
                }
                if (parameterName.Contains(")")) {
                    parameterName = parameterName.Replace(")", "");
                }

                switch (item.ConditionalType) {
                    case ConditionalTypeEnum.Equal:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.Like:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.GreaterThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.GreaterThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">=", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<=", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.In:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue1 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "IN", inValue1);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NotIn:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue2 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "NOT IN", inValue2);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LikeLeft:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.NoLike:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " NOT LIKE", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.LikeRight:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, "%" + item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NoEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNullOrEmpty:
                        builder.AppendFormat(" {0} (({1}) OR ({2})) ", type, item.FieldName.ToSqlFilter() + " IS NULL ", item.FieldName.ToSqlFilter() + " = '' ");
                        parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNot:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " IS NOT ", "NULL");
                        } else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                            parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        }
                        break;
                    case ConditionalTypeEnum.EqualNull:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "  IS ", " NULL ");
                        } else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                            parameters.Add(new MySqlParameter(parameterName, item.FieldValue));
                        }
                        break;
                    default:
                        break;
                }
            }
            return new KeyValuePair<string, MySqlParameter[]>(builder.ToString(), parameters.ToArray());
        }

        public static KeyValuePair<string, SqlSugar.SugarParameter[]> ConditionalToSql(List<Conditional> models) {
            StringBuilder builder = new StringBuilder();
            var parameters = new List<SqlSugar.SugarParameter>();
            foreach (var item in models) {
                var index = models.IndexOf(item);
                var type = index == 0 ? "" : "AND";
                string temp = " {0} {1} {2} {3}  ";
                // en caso de enviar alguna funcion se remueven los espacios para crear el nombre del parametro
                string parameterName = string.Format("{0}Conditional{1}{2}", "@", item.FieldName.Replace(" ", "").Trim(), index);
                if (parameterName.Contains(".")) {
                    parameterName = parameterName.Replace(".", "_");
                }
                if (parameterName.Contains("[")) {
                    parameterName = parameterName.Replace("[", "_");
                }
                if (parameterName.Contains("]")) {
                    parameterName = parameterName.Replace("]", "_");
                }
                if (parameterName.Contains("(")) {
                    parameterName = parameterName.Replace("(", "");
                }
                if (parameterName.Contains(")")) {
                    parameterName = parameterName.Replace(")", "");
                }

                switch (item.ConditionalType) {
                    case ConditionalTypeEnum.Equal:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.Like:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.GreaterThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.GreaterThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">=", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<=", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.In:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue1 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "IN", inValue1);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NotIn:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue2 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "NOT IN", inValue2);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LikeLeft:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.NoLike:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " NOT LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.LikeRight:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, "%" + item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NoEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNullOrEmpty:
                        builder.AppendFormat(" {0} (({1}) OR ({2})) ", type, item.FieldName.ToSqlFilter() + " IS NULL ", item.FieldName.ToSqlFilter() + " = '' ");
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNot:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " IS NOT ", "NULL");
                        } else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                            parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        }
                        break;
                    case ConditionalTypeEnum.EqualNull:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "  IS ", " NULL ");
                        } else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                            parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        }
                        break;
                    default:
                        break;
                }
            }
            return new KeyValuePair<string, SqlSugar.SugarParameter[]>(builder.ToString(), parameters.ToArray());
        }

        public static KeyValuePair<string, SqlSugar.SugarParameter[]> ConditionalToSql(List<IConditional> models) {
            StringBuilder builder = new StringBuilder();
            var parameters = new List<SqlSugar.SugarParameter>();
            foreach (var item in models) {
                var index = models.IndexOf(item);
                var type = index == 0 ? "" : "AND";
                string temp = " {0} {1} {2} {3}  ";
                // en caso de enviar alguna funcion se remueven los espacios para crear el nombre del parametro
                string parameterName = string.Format("{0}Conditional{1}{2}", "@", item.FieldName.Replace(" ", "").Trim(), index);
                if (parameterName.Contains(".")) {
                    parameterName = parameterName.Replace(".", "_");
                }
                if (parameterName.Contains("[")) {
                    parameterName = parameterName.Replace("[", "_");
                }
                if (parameterName.Contains("]")) {
                    parameterName = parameterName.Replace("]", "_");
                }
                if (parameterName.Contains("(")) {
                    parameterName = parameterName.Replace("(", "");
                }
                if (parameterName.Contains(")")) {
                    parameterName = parameterName.Replace(")", "");
                }

                switch (item.ConditionalType) {
                    case ConditionalTypeEnum.Equal:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.Like:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.GreaterThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.GreaterThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), ">=", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThan:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LessThanOrEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<=", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.In:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue1 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "IN", inValue1);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NotIn:
                        if (item.FieldValue == null)
                            item.FieldValue = string.Empty;
                        var inValue2 = ("(" + item.FieldValue.Split(',').ToJoinSqlInVals() + ")");
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "NOT IN", inValue2);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.LikeLeft:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.NoLike:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " NOT LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, "%" + item.FieldValue + "%"));
                        break;
                    case ConditionalTypeEnum.LikeRight:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "LIKE", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, "%" + item.FieldValue));
                        break;
                    case ConditionalTypeEnum.NoEqual:
                        builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNullOrEmpty:
                        builder.AppendFormat(" {0} (({1}) OR ({2})) ", type, item.FieldName.ToSqlFilter() + " IS NULL ", item.FieldName.ToSqlFilter() + " = '' ");
                        parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        break;
                    case ConditionalTypeEnum.IsNot:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), " IS NOT ", "NULL");
                        } else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "<>", parameterName);
                            parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        }
                        break;
                    case ConditionalTypeEnum.EqualNull:
                        if (item.FieldValue == null) {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "  IS ", " NULL ");
                        } else {
                            builder.AppendFormat(temp, type, item.FieldName.ToSqlFilter(), "=", parameterName);
                            parameters.Add(new SqlSugar.SugarParameter(parameterName, item.FieldValue));
                        }
                        break;
                    default:
                        break;
                }
            }
            return new KeyValuePair<string, SqlSugar.SugarParameter[]>(builder.ToString(), parameters.ToArray());
        }
    }
}
