﻿// ¿Cúales son las tareas de este servicio?
// 1. Proporcionar la información de la configuración de cada uno de los servicios disponibles basado en los grupos y parametros.
// 2. Almacenar la información de los ajustes en grupos y parametros.
// 3. Proporcionar la información de la empresa.
// 4. Proporcionar la información de la configuración de la empresa.
// 5. Almacenar la información de la configuración de la empresa.
// 6. Validar la información de la configuración de la empresa.
// 7. Proporcionar la información de la configuración de la nómina.
// ** Cada sub sub sistema puede acceder a la configuración a través de una clase heredada.
// ** Actualmente se utiliza la versión acterior de configuración donde se utiliza un formato json que originalmente es solo para la información de CFDi
using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Aplication.Empresa.Service {
    /// <summary>
    /// Servicio para parametros de configuracion de la aplicacion
    /// </summary>
    public class ConfigurationService : Abstracts.ConfigurationService, Contracts.IConfigurationService, Contracts.IEmpresaService {
        /// <summary>
        /// constructor
        /// </summary>
        public ConfigurationService() { this.OnLoad(); }

        /// <summary>
        /// sobre carga para repositorios
        /// </summary>
        protected override void OnLoad() {
            // repositorio de parametros de configuración
            this._ParametrosRepository = new DataAccess.Empresa.Repositories.SqlParametroRepository(ConfigService.Synapsis.RDS.Edita);
            // repositorio utilizado para tabla _conf
            this._ConfiguracionRepository = new DataAccess.Empresa.Repositories.SqlSugarConfiguracionRepository(ConfigService.Synapsis.RDS.Edita);
        }

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="configGroup">ConfigGroupEnum</param>
        /// <returns>lista de parametros</returns>
        public override List<IParametroModel> Get(ConfigGroupEnum configGroup) {
            return this._ParametrosRepository.Get(configGroup).ToList<IParametroModel>();
        }

        /// <summary>
        /// almacenar lista de parametros
        /// </summary>
        /// <param name="parametros">List<IParametro></param>
        public override void Set(List<IParametroModel> parametros) {
            this._ParametrosRepository.Save(parametros);
        }

        /// <summary>
        /// obtener informacion de la empresa
        /// </summary>
        /// <returns>IEmpresaGeneral</returns>
        public virtual EmpresaDetalle GetEmpresa1() {
            var parametros = this._ParametrosRepository.Get(new ConfigGroupEnum[] { ConfigGroupEnum.Contribuyente, ConfigGroupEnum.DomicilioFiscal }).ToList();
            var data = this.GetConfiguracion(ConfiguracionKeyEnum.cfdi);
            var cfdi = Domain.Empresa.Entities.ComprobanteConfig.Json(data.Data);
            var empresa = new EmpresaDetalle();


            empresa.Empresa.Build(parametros);
            empresa.DomicilioFiscal.Build(parametros);

            return empresa;
        }

        /// <summary>
        /// obtener informacion de la empresa
        /// </summary>
        /// <returns>IEmpresaGeneral</returns>
        public virtual EmpresaGeneral GetEmpresa() {
            var parametros = this._ParametrosRepository.Get(new ConfigGroupEnum[] { ConfigGroupEnum.Contribuyente, ConfigGroupEnum.DomicilioFiscal }).ToList();
            var data = this.GetConfiguracion(ConfiguracionKeyEnum.cfdi);
            var cfdi = Domain.Empresa.Entities.ComprobanteConfig.Json(data.Data);
            var empresa = new EmpresaDetalle();
            var response = new EmpresaGeneral();

            if (parametros != null) {
                empresa.Empresa.Build(parametros);
                empresa.DomicilioFiscal.Build(parametros);
                response.Clave = cfdi.General.Clave;
                response.RazonSocial = empresa.Empresa.RazonSocial;
                response.NombreComercial = empresa.Empresa.NombreComercial;
                response.RFC = empresa.Empresa.RFC;
                response.RegistroPatronal = empresa.Empresa.RegistroPatronal;
                response.DomicilioFiscal = new DomicilioFiscal {
                    Calle = empresa.DomicilioFiscal.NombreVialidad,
                    Ciudad = empresa.DomicilioFiscal.Ciudad,
                    CodigoPostal = empresa.DomicilioFiscal.CodigoPostal,
                    Colonia = empresa.DomicilioFiscal.Colonia,
                    Delegacion = empresa.DomicilioFiscal.MunicipioDelegacion,
                    Estado = empresa.DomicilioFiscal.EntidadFederativa,
                    NoExterior = empresa.DomicilioFiscal.NumExterior,
                    NoInterior = empresa.DomicilioFiscal.NumInterior,
                    Referencia = empresa.DomicilioFiscal.Referencia,
                    Localidad = empresa.DomicilioFiscal.Localidad,
                };
            }

            #region informacion de la empresa
            if (string.IsNullOrEmpty(response.Clave))
                response.Clave = cfdi.General.Clave;

            if (string.IsNullOrEmpty(response.RazonSocial))
                response.RazonSocial = cfdi.General.RazonSocial;

            if (string.IsNullOrEmpty(response.NombreComercial))
                response.NombreComercial = cfdi.General.NombreComercial;

            if (string.IsNullOrEmpty(response.RFC))
                response.RFC = cfdi.General.RFC;

            if (string.IsNullOrEmpty(response.RegistroPatronal))
                response.RegistroPatronal = cfdi.General.RegistroPatronal;

            if (string.IsNullOrEmpty(response.RegimenFiscal))
                response.RegimenFiscal = cfdi.General.RegimenFiscal;
            #endregion

            #region domicilio discal
            if (string.IsNullOrEmpty(response.DomicilioFiscal.CodigoDePais))
                response.DomicilioFiscal.CodigoDePais = cfdi.DomicilioFiscal.CodigoDePais;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Pais))
                response.DomicilioFiscal.Pais = cfdi.DomicilioFiscal.Pais;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Calle))
                response.DomicilioFiscal.Calle = cfdi.DomicilioFiscal.Calle;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Ciudad))
                response.DomicilioFiscal.Ciudad = cfdi.DomicilioFiscal.Ciudad;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.CodigoPostal))
                response.DomicilioFiscal.CodigoPostal = cfdi.DomicilioFiscal.CodigoPostal;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Colonia))
                response.DomicilioFiscal.Colonia = cfdi.DomicilioFiscal.Colonia;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Delegacion))
                response.DomicilioFiscal.Delegacion = cfdi.DomicilioFiscal.Delegacion;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Estado))
                response.DomicilioFiscal.Estado = cfdi.DomicilioFiscal.Estado;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Localidad))
                response.DomicilioFiscal.Localidad = cfdi.DomicilioFiscal.Localidad;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.Referencia))
                response.DomicilioFiscal.Referencia = cfdi.DomicilioFiscal.Referencia;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.NoExterior))
                response.DomicilioFiscal.NoExterior = cfdi.DomicilioFiscal.NoExterior;

            if (string.IsNullOrEmpty(response.DomicilioFiscal.NoInterior))
                response.DomicilioFiscal.NoInterior = cfdi.DomicilioFiscal.NoInterior;
            #endregion

            return response;
        }

        #region configuraciones de la tabla _conf (anterior)
        public EmpresaConfiguracionModel GetConfiguracion(Domain.Base.ValueObjects.ConfiguracionKeyEnum configuracionKey) {
            return this._ConfiguracionRepository.GetByKey(configuracionKey.ToString());
        }

        public EmpresaConfiguracionModel SetConfiguracion(EmpresaConfiguracionModel data) {
            return this._ConfiguracionRepository.Save(data);
        }
        #endregion

        #region metodos estaticos
        public static bool IsValid(IDataBaseConfiguracion dataBase) {
            if (dataBase == null)
                return false;

            if (string.IsNullOrEmpty(dataBase.Database))
                return false;

            if (string.IsNullOrEmpty(dataBase.HostName))
                return false;

            if (string.IsNullOrEmpty(dataBase.Password))
                return false;
            return true;
        }
        #endregion
    }
}
