﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.DataAccess.Kaiju;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Empresa.Service {
    /// <summary>
    /// servicio para control de usuarios
    /// </summary>
    public class UserService : Contracts.IUserService {
        #region declaraciones
        protected ISqlUserRepository repository;
        protected ISqlUserRolRepository profile;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public UserService() {
            this.repository = new SqlSugarUserRepository(ConfigService.Synapsis.RDS.Edita);
            this.profile = new SqlSugarUserRolRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// buscar una clave de usuario
        /// </summary>
        /// <param name="clave"></param>
        /// <returns>indice</returns>
        public int Search(string clave) {
            return this.repository.Search(clave);
        }

        /// <summary>
        /// obtener 
        /// </summary>
        public UserModel GetUser(int index) {
            return this.repository.GetById(index);
        }

        /// <summary>
        /// desactivar el usuario
        /// </summary>
        /// <param name="item">IUser</param>
        /// <returns>verdadero</returns>
        public bool Desactive(UserModel item) {
            return this.repository.Delete(item.IdUser);
        }

        /// <summary>
        /// guardar objeto usuario
        /// </summary>
        /// <param name="usuario">usuario</param>
        /// <returns>IUser</returns>
        public UserModel Save(UserModel usuario) {
            return this.repository.Save(usuario);
        }

        /// <summary>
        /// almacenar Rol de Usuario
        /// </summary>
        /// <param name="perfil">IUserRol</param>
        /// <returns>IUserRol</returns>
        public UserRolDetailModel Save(UserRolDetailModel perfil) {
            if (perfil.IdUserRol == 0) {
                perfil.Creo = ConfigService.Piloto.Clave;
                perfil.FechaNuevo = DateTime.Now;
                perfil.IdUserRol = this.profile.Insert(perfil);
            } else {
                perfil.Modifica = ConfigService.Piloto.Clave;
                perfil.FechaModifica = DateTime.Now;
                this.profile.Update(perfil);
            }
            return perfil;
        }

        /// <summary>
        /// obtener lista de usuarios
        /// </summary>
        /// <returns>List</returns>
        public BindingList<UserModel> GetUsuarios() {
            return new BindingList<UserModel>(this.repository.GetList().ToList());
        }

        /// <summary>
        /// obtener listado de perfiles
        /// </summary>
        /// <returns>List</returns>
        public BindingList<UserRolDetailModel> GetPerfiles() {
            //return new BindingList<UserRolDetailModel>(this.profile.GetList().ToList());
            return null;
        }
    }
}
