﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Aplication.Empresa.Abstracts {
    public abstract class ConfigurationService {
        #region declaraciones
        /// <summary>
        /// obtener o establecer repositorio de parametros de configuracion
        /// </summary>
        protected internal ISqlParametroRepository _ParametrosRepository { get; protected set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        protected internal ISqlConfiguracionRepository _ConfiguracionRepository { get; protected set; }
        #endregion

        /// <summary>
        /// sobre carga para repositorios
        /// </summary>
        protected abstract void OnLoad();

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="configGroup">ConfigGroupEnum</param>
        /// <returns>lista de parametros</returns>
        public abstract List<IParametroModel> Get(ConfigGroupEnum configGroup);

        /// <summary>
        /// almacenar lista de parametros
        /// </summary>
        /// <param name="parametros">List<IParametro></param>
        public abstract void Set(List<IParametroModel> parametros);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="empresa"></param>
        /// <returns></returns>
        public static EmpresaGeneral IsTesting(EmpresaGeneral empresa) {
            empresa.RegimenFiscal = "601";
            empresa.RazonSocial = "Impresores Profesionales SA de CV";
            empresa.NombreComercial = "Impresores Profesionales";
            empresa.RFC = "IPR981125PN9";
            empresa.DomicilioFiscal.CodigoPostal = "08100";
            return empresa;
        }
    }
}
