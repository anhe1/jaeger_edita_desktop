﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Aplication.Empresa.Contracts {
    /// <summary>
    /// Servicio para parametros de configuracion de la aplicacion
    /// </summary>
    public interface IConfigurationService : Base.IBaseService {
        /// <summary>
        /// almacenar lista de parametros
        /// </summary>
        /// <param name="parametros">List<IParametro></param>
        void Set(List<IParametroModel> parametros);

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="configGroup">ConfigGroupEnum</param>
        /// <returns>lista de parametros</returns>
        List<IParametroModel> Get(ConfigGroupEnum configGroup);
    }
}
