﻿using System.ComponentModel;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Aplication.Empresa.Contracts {
    /// <summary>
    /// interface para el servicio de usuarios
    /// </summary>
    public interface IUserService {
        /// <summary>
        /// buscar una clave de usuario
        /// </summary>
        /// <param name="clave"></param>
        /// <returns>indice</returns>
        int Search(string clave);

        /// <summary>
        /// desactivar el usuario
        /// </summary>
        /// <param name="item">IUser</param>
        /// <returns>verdadero</returns>
        bool Desactive(UserModel item);

        /// <summary>
        /// guardar objeto usuario
        /// </summary>
        UserModel Save(UserModel usuario);

        /// <summary>
        /// almacenar Rol de Usuario
        /// </summary>
        /// <param name="perfil">IUserRol</param>
        /// <returns>IUserRol</returns>
        UserRolDetailModel Save(UserRolDetailModel perfil);

        /// <summary>
        /// obtener usuario
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>IUsuario</returns>
        UserModel GetUser(int index);

        /// <summary>
        /// obtener lista de usuarios
        /// </summary>
        /// <returns>List</returns>
        BindingList<UserModel> GetUsuarios();

        /// <summary>
        /// obtener lista de perfiles
        /// </summary>
        /// <returns>List</returns>
        BindingList<UserRolDetailModel> GetPerfiles();
    }
}
