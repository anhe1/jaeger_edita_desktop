﻿using Jaeger.Aplication.Base;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Aplication.Empresa.Contracts {
    /// <summary>
    /// 
    /// </summary>
    public interface IEmpresaService : IConfigurationService, IBaseService {
        EmpresaGeneral GetEmpresa();
        EmpresaDetalle GetEmpresa1();
    }
}
