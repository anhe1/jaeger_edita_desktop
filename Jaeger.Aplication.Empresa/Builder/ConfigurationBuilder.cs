﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Aplication.Empresa.Builder {
    public class ConfigurationBuilder : IConfigurationBuilder, IConfiguracionBuild {
        protected internal EmpresaDetalle _Empresa;

        public ConfigurationBuilder() {
            this._Empresa = new EmpresaDetalle();
        }

        public EmpresaDetalle Build(List<IParametroModel> parametros) {
            foreach (var item in parametros) {
                switch (item.Key) {
                    case ConfigKeyEnum.RFC:
                        this._Empresa.Empresa.RFC = item.Data;
                        break;
                    case ConfigKeyEnum.CURP:
                        this._Empresa.Empresa.CURP = item.Data;
                        break;
                    case ConfigKeyEnum.RazonSocial:
                        this._Empresa.Empresa.RazonSocial = item.Data;
                        break;
                    case ConfigKeyEnum.NombreComercial:
                        this._Empresa.Empresa.NombreComercial = item.Data;
                        break;
                    case ConfigKeyEnum.SociedadCapital:
                        this._Empresa.Empresa.SociedadCapital = item.Data;
                        break;
                    case ConfigKeyEnum.RegimenFiscal:
                        this._Empresa.Empresa.RegimenFiscal = item.Data;
                        break;
                    case ConfigKeyEnum.RegistroPatronal:
                        this._Empresa.Empresa.RegistroPatronal = item.Data;
                        break;
                    case ConfigKeyEnum.CIF:
                        this._Empresa.Empresa.CIF = item.Data;
                        break;
                    case ConfigKeyEnum.CIFUrl:
                        this._Empresa.Empresa.CIFUrl = item.Data;
                        break;
                    case ConfigKeyEnum.TipoPersona:
                        this._Empresa.Empresa.TipoPersona = item.Data;
                        break;
                    case ConfigKeyEnum.Dominio:
                        this._Empresa.Empresa.Dominio = item.Data;
                        break;
                    default:
                        break;
                }
            }
            this._Empresa.DomicilioFiscal = this.Build1(parametros);
            return this._Empresa;
        }

        public List<IParametroModel> Build(EmpresaDetalle empresa) {
            var d0 = new List<IParametroModel> {
                new ParametroModel(ConfigKeyEnum.RFC, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.RFC),
                new ParametroModel(ConfigKeyEnum.CURP, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.CURP),
                new ParametroModel(ConfigKeyEnum.RazonSocial, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.RazonSocial),
                new ParametroModel(ConfigKeyEnum.NombreComercial, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.NombreComercial),
                new ParametroModel(ConfigKeyEnum.SociedadCapital, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.SociedadCapital),
                new ParametroModel(ConfigKeyEnum.RegimenFiscal, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.RegimenFiscal),
                new ParametroModel(ConfigKeyEnum.RegistroPatronal, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.RegistroPatronal),
                new ParametroModel(ConfigKeyEnum.CIF, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.CIF),
                new ParametroModel(ConfigKeyEnum.CIFUrl, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.CIFUrl),
                new ParametroModel(ConfigKeyEnum.TipoPersona, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.TipoPersona),
                new ParametroModel(ConfigKeyEnum.Dominio, ConfigGroupEnum.Contribuyente, this._Empresa.Empresa.Dominio)
            };
            return d0;
        }

        private DomicilioFiscalConfiguracion Build1(List<IParametroModel> parametros) {
            var configuration = new DomicilioFiscalConfiguracion();
            foreach (var item in parametros) {
                switch (item.Key) {
                    case ConfigKeyEnum.CodigoPostal:
                        configuration.CodigoPostal = item.Data;
                        break;
                    case ConfigKeyEnum.Colonia:
                        configuration.Colonia = item.Data;
                        break;
                    case ConfigKeyEnum.NumExterior:
                        configuration.NumExterior = item.Data;
                        break;
                    case ConfigKeyEnum.NumInterior:
                        configuration.NumInterior = item.Data;
                        break;
                    case ConfigKeyEnum.MunicipioDelegacion:
                        configuration.MunicipioDelegacion = item.Data;
                        break;
                    case ConfigKeyEnum.TipoVialidad:
                        configuration.TipoVialidad = item.Data;
                        break;
                    case ConfigKeyEnum.Calle:
                        configuration.NombreVialidad = item.Data;
                        break;
                    case ConfigKeyEnum.Estado:
                        configuration.EntidadFederativa = item.Data;
                        break;
                    case ConfigKeyEnum.Referencia:
                        configuration.Referencia = item.Data;
                        break;
                    case ConfigKeyEnum.Localidad:
                        configuration.Localidad = item.Data;
                        break;
                    case ConfigKeyEnum.CorreoElectronico:
                        configuration.Correo = item.Data;
                        break;
                    case ConfigKeyEnum.Telefono:
                        configuration.Telefono = item.Data;
                        break;
                    case ConfigKeyEnum.Ciudad:
                        configuration.Ciudad = item.Data;
                        break;
                    default:
                        break;
                }
            }
            return configuration;
        }

        private List<IParametroModel> Build1() {
            var parameters = new List<IParametroModel> {
                new ParametroModel(ConfigKeyEnum.CodigoPostal, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.CodigoPostal),
                new ParametroModel(ConfigKeyEnum.Colonia, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.Colonia),
                new ParametroModel(ConfigKeyEnum.NumExterior, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.NumExterior),
                new ParametroModel(ConfigKeyEnum.NumInterior, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.NumInterior),
                new ParametroModel(ConfigKeyEnum.MunicipioDelegacion, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.MunicipioDelegacion),
                new ParametroModel(ConfigKeyEnum.TipoVialidad, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.TipoVialidad),
                new ParametroModel(ConfigKeyEnum.Calle, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.NombreVialidad),
                new ParametroModel(ConfigKeyEnum.Estado, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.EntidadFederativa),
                new ParametroModel(ConfigKeyEnum.Referencia, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.Referencia),
                new ParametroModel(ConfigKeyEnum.Localidad, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.Localidad),
                new ParametroModel(ConfigKeyEnum.Ciudad, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.Ciudad),
                new ParametroModel(ConfigKeyEnum.Telefono, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.Telefono),
                new ParametroModel(ConfigKeyEnum.CorreoElectronico, ConfigGroupEnum.DomicilioFiscal, this._Empresa.DomicilioFiscal.Correo),
            };
            return parameters;
        }
    }
}
