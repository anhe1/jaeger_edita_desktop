﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Aplication.Empresa.Builder {
    public interface IConfigurationBuilder {
        EmpresaDetalle Build(List<IParametroModel> parametros);
    }

    public interface IConfiguracionBuild {
        List<IParametroModel> Build(EmpresaDetalle empresa);
    }
}
