﻿using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    public class DevolucionBuilder : IDevolucionBuilder, IDevolucionIdDirectorioBuilder, IDevolucionConceptoBuilder , IDevolucionBuild {
        protected DevolucionDetailModel _Devolucion;

        public DevolucionBuilder() { this._Devolucion = new DevolucionDetailModel(); }

        public IDevolucionIdDirectorioBuilder IdDirectorio(int idDirectorio) {
            this._Devolucion.IdDirectorio = idDirectorio;
            return this;
        }

        public IDevolucionIdDirectorioBuilder Add(ValeAlmacenConceptoModel item) {
            if (this._Devolucion.Conceptos == null) { this._Devolucion.Conceptos = new System.ComponentModel.BindingList<ValeAlmacenConceptoModel>(); }
            this._Devolucion.Conceptos.Add(item);
            return this;
        }

        public DevolucionDetailModel Build() {
            return this._Devolucion;
        }
    }
}
