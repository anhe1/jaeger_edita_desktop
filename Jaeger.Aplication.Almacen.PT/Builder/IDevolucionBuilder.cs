﻿using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    public interface IDevolucionBuilder {
        DevolucionDetailModel Build();
        IDevolucionIdDirectorioBuilder IdDirectorio(int idDirectorio);
    }

    public interface IDevolucionIdDirectorioBuilder : IDevolucionBuild {
        IDevolucionIdDirectorioBuilder Add(ValeAlmacenConceptoModel item);
    }

    public interface IDevolucionConceptoBuilder : IDevolucionBuild {

    }

    public interface IDevolucionBuild {
        DevolucionDetailModel Build();
    }
}
