﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    public class RemisionBuilder : IRemisionBuilder {
        private readonly IRemisionDetailModel _Remision;

        public RemisionBuilder() {
            this._Remision = new RemisionDetailModel {
                Status = RemisionStatusEnum.Pendiente,
                ClaveMoneda = "MXN",
                EmisorRFC = ConfigService.Synapsis.Empresa.RFC,
                Creo = ConfigService.Piloto.Clave,
                FechaEmision = DateTime.Now
            };
        }

        public IRemisionDetailModel Build() {
            return this._Remision;
        }

        public IRemisionBuilder WithStatus(RemisionStatusEnum status) {
            this._Remision.Status = status;
            return this;
        }

        public IRemisionBuilder AddConcepto(IRemisionConceptoDetailModel concepto) {
            this._Remision.Conceptos.Add(concepto as RemisionConceptoDetailModel);
            return this;
        }

        public IRemisionBuilder Clone(IRemisionDetailModel model) {
            if (model != null) {
                for (int i = 0; i < model.Conceptos.Count; i++) {
                    var row = new RemisionConceptoDetailModel();
                    MapperClassExtensions.MatchAndMap<RemisionConceptoDetailModel, RemisionConceptoDetailModel>(model.Conceptos[i], row);
                    if (row != null) {
                        row.IdRemision = 0;
                        row.IdMovimiento = 0;
                        row.FechaNuevo = DateTime.Now;
                        row.FechaModifica = null;
                        row.Modifica = null;
                        row.Creo = null;
                        this.AddConcepto(row);
                    }
                }
            }
            return this;
        }

        #region metodos estaticos
        /// <summary>
        /// obtener listado de status de remisiones
        /// </summary>
        public static List<StatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<StatusModel, RemisionStatusEnum>().ToList();
        }

        public static IRemisionDetailModel SetStatus(IRemisionDetailModel remision, IRemisionStatusDetailModel status) {
            remision.IdStatus = status.IdStatusB;
            remision.FechaCancela = status.FechaNuevo;
            if (remision.Autorizaciones == null)
                remision.Autorizaciones = new BindingList<RemisionStatusModel>();
                remision.Autorizaciones.Add(status as RemisionStatusModel);
            if (status.Relaciones != null) {
                if (remision.RemisionRelacionada == null)
                    remision.RemisionRelacionada = new BindingList<RemisionRelacionadaModel>();
                for (int i = 0; i < status.Relaciones.Count; i++) {
                    status.Relaciones[i].Relacion = status.CvMotivo;
                    //e.Relaciones[i].IdTipoRelacion = status.User;
                    remision.RemisionRelacionada.Add(status.Relaciones[i] as RemisionRelacionadaModel);
                }
            }

            return remision;
        }

        /// <summary>
        /// Control de status
        /// </summary>
        public static RemisionStatusEnum ControlStatus(RemisionDetailModel model) {
            if (model.Status == RemisionStatusEnum.Pendiente && model.FechaEntrega is null && model.FechaCobranza is null) {
                return RemisionStatusEnum.En_Ruta;
            } else if (model.Status == RemisionStatusEnum.En_Ruta && model.FechaEntrega != null && model.FechaCobranza is null) {
                return RemisionStatusEnum.Entregado;
            } else if (model.Status == RemisionStatusEnum.Entregado && model.FechaEntrega != null && model.FechaCobranza != null) {
                return RemisionStatusEnum.Por_Cobrar;
            } else if (model.Status == RemisionStatusEnum.Pendiente && model.FechaEntrega != null && model.FechaCobranza is null) {
                return RemisionStatusEnum.Entregado;
            }
            return model.Status;
        }

        /// <summary>
        /// crear concepto de remision
        /// </summary>
        public static IRemisionConceptoDetailModel Create(ProductoXModelo e) {
            return new RemisionConceptoDetailModel {
                IdAlmacen = 0,
                IdProducto = e.IdProducto,
                Identificador = e.Identificador,
                IdModelo = e.IdModelo,
                Activo = true,
                Catalogo = e.Categoria,
                Especificacion = e.Especificacion,
                IdEspecificacion = e.IdEspecificacion,
                IdUnidad = e.IdUnidad,
                Marca = e.Marca,
                Modelo = e.Descripcion,
                Producto = e.Nombre,
                Tamanio = e.Variante,
                TasaIVA = e.TasaIVA,
                Unidad = e.Unidad,
                UnidadFactor = e.FactorUnidad
            };
        }
        #endregion
    }
}
