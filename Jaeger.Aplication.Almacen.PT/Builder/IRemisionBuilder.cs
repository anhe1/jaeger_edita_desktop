﻿using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.ValueObjects;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    public interface IRemisionBuilder {
        IRemisionBuilder WithStatus(RemisionStatusEnum status);
        IRemisionBuilder AddConcepto(IRemisionConceptoDetailModel concepto);
        IRemisionBuilder Clone(IRemisionDetailModel model);
        IRemisionDetailModel Build();
    }
}
