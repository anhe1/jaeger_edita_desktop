﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    public class ConfigurationBuilder : IConfigurationBuilder, IConfigurarionBuild {
        protected internal IConfiguration _Configuration;
        protected internal string Pagare = @"Debo(mos) y pagaré(mos) incondicionalmente por este pagaré a la órden de @Empresa en la ciudad de méxico, d.f. o en la que el tenedor elija el día la cantidad de $ @Total @TotalEnLetra , cantidad correspondiente al importe de mercancías y/o servicios que he recibido de conformidad con la remisión no. @folio, siendo este pagaré mercantil en los artículos 170, 173 y 174 en su parte final de la ley general de títulos y operaciones de crédito y de conformidad con el artículo 11 y demás relativos. además de la ley ya citada me obligo incondicionalmente a pagar el importe de este pagaré (aún cuando sea aceptado en mi nombre y representación por empleado o dependencia de mi negocio). el presente pagaré es sin protesto, y en caso de incumplimiento por parte del suscriptor al pago del mismo, devengará intereses moratorios pagaderos juntamente con la suerte principal, a una tasa del 5% mensual, calculados desde el momento en que se dé el incumplimiento hasta la fecha de su pago total.";
        protected internal string Leyenda1 = "Realice su pago en una de nuestras cuentas Alejandro Reyes Liñan Santander Cta. 18131402 Suc. 015 Clabe: 014180606204545338. Anotando en la referencia alfanumérica su clave de cliente @ReceptorClave y su número (s) de remisión. Comuníquese con nosotros a los teléfonos 5115-5320 al 27 ext. 114 o mándenos un correo adm.cobranza@ipo.com.mx ";
        protected internal string Leyenda2 = "El Cliente firma de conformidad con los materiales descritos en esta remisión, cuenta con cinco días para cualquier aclaración. En mercancía de temporada no se aceptan devoluciones.";

        public ConfigurationBuilder() {
            this._Configuration = new Configuration();
        }

        public IConfigurarionBuild AddPagare(string pagare) {
            this._Configuration.Pagare = pagare;
            return this;
        }

        public IConfiguration Build(List<IParametroModel> parametros) {
            if (parametros != null) {
                var configuracion = parametros.Where(it => it.Key == ConfigKeyEnum.Configuracion).FirstOrDefault();
                var pagare = parametros.Where(it => it.Key == ConfigKeyEnum.Pagare).FirstOrDefault();
                var leyenda = parametros.Where(it => it.Key == ConfigKeyEnum.Leyenda).FirstOrDefault();
                var expresion = parametros.Where(it => it.Key == ConfigKeyEnum.Expresion).FirstOrDefault();
                var folder = parametros.Where(it => it.Key == ConfigKeyEnum.Folder).FirstOrDefault();
                var templete = parametros.Where(it => it.Key == ConfigKeyEnum.Templete).FirstOrDefault();

                if (configuracion != null)
                    SetData(configuracion.Data);
                if (pagare != null)
                    this._Configuration.Pagare = configuracion.Data;
                if (leyenda != null)
                    this._Configuration.Leyenda1 = configuracion.Data;
                if (expresion != null)
                    this._Configuration.Leyenda2 = configuracion.Data;
                if (folder != null)
                    this._Configuration.Folder = configuracion.Data;
                if (templete != null)
                    this._Configuration.Templete = configuracion.Data;

            } else {
                this.Default();
            }
            return this._Configuration;
        }

        public IConfiguration Build() {
            return this._Configuration;
        }

        public List<IParametroModel> Build(IConfiguration configuration) {
            var parametros = new List<IParametroModel>();
            var configuracion = new ParametroModel().WithKey(ConfigKeyEnum.Configuracion).WithGroup(ConfigGroupEnum.AlmacenPT).With(GetData(configuration));
            var pagare = new ParametroModel().WithKey(ConfigKeyEnum.Pagare).WithGroup(ConfigGroupEnum.AlmacenPT).With(configuration.Pagare);
            var leyenda1 = new ParametroModel().WithKey(ConfigKeyEnum.Leyenda).WithGroup(ConfigGroupEnum.AlmacenPT).With(configuration.Leyenda1);
            var leyenda2 = new ParametroModel().WithKey(ConfigKeyEnum.Expresion).WithGroup(ConfigGroupEnum.AlmacenPT).With(configuration.Leyenda2);
            var folder = new ParametroModel().WithKey(ConfigKeyEnum.Folder).WithGroup(ConfigGroupEnum.AlmacenPT).With(configuration.Folder);
            var templete = new ParametroModel().WithKey(ConfigKeyEnum.Templete).WithGroup(ConfigGroupEnum.AlmacenPT).With(configuration.Templete);
            parametros.Add(configuracion);
            parametros.Add(pagare);
            parametros.Add(leyenda1);
            parametros.Add(leyenda2);
            parametros.Add(folder);
            parametros.Add(templete);
            return parametros;
        }

        public IConfigurarionBuild Default() {
            this._Configuration = new Configuration {
                Pagare = this.Pagare,
                Leyenda1 = this.Leyenda1,
                Leyenda2 = this.Leyenda2
            };
            return this;
        }

        private string GetData(IConfiguration configuration) {
            var salida = new StringBuilder();
            salida.Append(configuration.ShowTotalConLetra ? "1" : "0");
            salida.Append(configuration.ShowPagare ? "1" : "0");
            return salida.ToString();
        }

        private void SetData(string parametro) {
            for (int i = 0; i < parametro.Length; i++) {
                bool flag = parametro[i] == '1';
                switch (i) {
                    case 0:
                        this._Configuration.ShowPagare = flag;
                        break;
                    case 1:
                        this._Configuration.ShowTotalConLetra = flag;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
