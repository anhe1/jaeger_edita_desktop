﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    public class RemisionPrinterBuilder  : IRemisionPrinterBuilder, IRemisionPrinterConfigurationBuilder {
        protected internal IConfiguration _Configuration;
        public IRemisionPrinterConfigurationBuilder Add(IConfiguration configuration) {
            this._Configuration = configuration;
            return this;
        }

        public RemisionDetailPrinter Build(IRemisionDetailModel model) {
            var printer = new RemisionDetailPrinter((RemisionDetailModel)model);
            if (_Configuration.Pagare != null) {
                var data = _Configuration.Pagare.Replace("@Empresa", ConfigService.Synapsis.Empresa.RazonSocial)
                    .Replace("@TotalEnLetra", printer.TotalEnLetra)
                    .Replace("@Total", printer.GTotal.ToString("0.00"))
                    .Replace("@Folio", printer.Folio.ToString());
                printer.Pagare = data;
                printer.ShowPagare = this._Configuration.ShowPagare;
            } else {
                printer.ShowPagare = false;
            }

            printer.Leyenda1 = _Configuration.Leyenda1;
            if (printer.Leyenda1 != null) {
                printer.Leyenda1 = printer.Leyenda1.Replace("@ReceptorClave", printer.ReceptorClave);
            }
            return printer;
        }
    }
}
