﻿using System;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    /// <summary>
    /// clase builder para crear documento de almacén
    /// </summary>
    public class ValeAlmacenBuilder : IValeAlmacenBuilder, IValeAlmacenBuild, IValeAlmacenIdBuilder, IValeAlmacenReceptorBuilder, IValeAlmacenEfectoBuilder, IValeAlmacenStatusBuilder {
        protected internal IValeAlmacenDetailModel _ValeAlmacen;
        public ValeAlmacenBuilder() {
            this._ValeAlmacen =
                new ValeAlmacenDetailModel {
                    Status = ValeAlmacenStatusEnum.Emitido,
                    Creo = ConfigService.Piloto.Clave,
                    FechaNuevo = DateTime.Now,
                    FechaEmision = DateTime.Now,
                };
        }

        public IValeAlmacenDetailModel Build() {
            return this._ValeAlmacen;
        }

        public IValeAlmacenStatusBuilder Tipo(TipoMovimientoEnum tipo) {
            this._ValeAlmacen.TipoMovimiento = tipo;
            return this;
        }

        public IValeAlmacenStatusBuilder Movimiento(TipoComprobanteEnum tipo) {
            this._ValeAlmacen.TipoComprobante = tipo;
            return this;
        }

        public IValeAlmacenReceptorBuilder WithIdDirectorio(int idReceptor) {
            this._ValeAlmacen.IdDirectorio = idReceptor;
            return this;
        }

        public IValeAlmacenReceptorBuilder WithStatus(ValeAlmacenStatusEnum status) {
            this._ValeAlmacen.IdStatus = (int)status;
            return this;
        }

        public IValeAlmacenBuild Almacen(AlmacenEnum almacen) {
            this._ValeAlmacen.IdTipoAlmacen = (int)almacen;
            return this;
        }

        public static ValeAlmacenConceptoModel Build(ProductoXModelo e) {
            return new ValeAlmacenConceptoModel {
                IdProducto = e.IdProducto,
                Identificador = e.Identificador,
                IdModelo = e.IdModelo,
                Activo = e.Activo,
                Catalogo = e.Categoria,
                Especificacion = e.Especificacion,
                IdEspecificacion = e.IdEspecificacion,
                IdUnidad = e.IdUnidad,
                Marca = e.Marca,
                Modelo = e.Descripcion,
                Producto = e.Nombre,
                Tamanio = e.Variante,
                TasaIVA = e.TasaIVA,
                Unidad = e.Unidad,
                UnidadFactor = e.FactorUnidad, 
                Unitario = e.Unitario,
            };
        }
    }
}
