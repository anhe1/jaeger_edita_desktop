﻿using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    /// <summary>
    /// clase builder para crear documento de almacén
    /// </summary>
    public interface IValeAlmacenBuilder {
        IValeAlmacenBuild Almacen(AlmacenEnum almacen);
    }

    public interface IValeAlmacenIdBuilder {
        IValeAlmacenStatusBuilder Movimiento(TipoComprobanteEnum tipo);
    }

    public interface IValeAlmacenStatusBuilder : IValeAlmacenBuild {
        IValeAlmacenReceptorBuilder WithStatus(ValeAlmacenStatusEnum status);
    }

    public interface IValeAlmacenEfectoBuilder : IValeAlmacenBuild {
        IValeAlmacenReceptorBuilder WithIdDirectorio(int idReceptor);
    }

    public interface IValeAlmacenReceptorBuilder {
        IValeAlmacenStatusBuilder Tipo(TipoMovimientoEnum tipo);
    }

    public interface IValeAlmacenBuild {
        IValeAlmacenDetailModel Build();
    }
}
