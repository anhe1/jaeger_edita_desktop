﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    /// <summary>
    /// builder de configuracion 
    /// </summary>
    public interface IConfigurationBuilder {
        IConfigurarionBuild AddPagare(string pagare);
        IConfiguration Build(List<IParametroModel> parametros);

        IConfigurarionBuild Default();
    }

    public interface IConfigurarionBuild {
        IConfiguration Build();

        List<IParametroModel> Build(IConfiguration configuration);
    }
}
