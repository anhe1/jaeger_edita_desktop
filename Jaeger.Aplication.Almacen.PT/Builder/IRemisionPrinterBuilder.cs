﻿using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Aplication.Almacen.PT.Builder {
    /// <summary>
    /// builder Printer
    /// </summary>
    public interface IRemisionPrinterBuilder {
        IRemisionPrinterConfigurationBuilder Add(IConfiguration configuration);
    }

    public interface IRemisionPrinterConfigurationBuilder {
        RemisionDetailPrinter Build(IRemisionDetailModel model);
    }
}
