﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Aplication.Almacen.PT.Contracts {
    public interface IDevolucionService {
        DevolucionDetailModel GetNew();

        DevolucionDetailModel GetById(int index);

        DevolucionDetailPrinterModel GetPrinter(int index);

        BindingList<ValeAlmacenConceptoModel> GetConceptos(int idVale);

        DevolucionDetailModel Save(DevolucionDetailModel vale);
    }
}