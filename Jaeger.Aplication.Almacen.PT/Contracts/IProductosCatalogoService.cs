﻿using Jaeger.Domain.Base.Builder;
using System.Collections.Generic;

namespace Jaeger.Aplication.Almacen.PT.Contracts {
    public interface IProductosCatalogoService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
