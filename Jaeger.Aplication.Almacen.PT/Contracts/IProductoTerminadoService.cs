﻿using Jaeger.Aplication.Almacen.Contracts;

namespace Jaeger.Aplication.Almacen.PT.Contracts {
    /// <summary>
    /// catalogo de productos y servicios de producto terminado
    /// </summary>
    public interface IProductoTerminadoService : ICatalogoProductoService {

    }
}
