﻿using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Aplication.Almacen.PT.Contracts {
    public interface IRemisionService {
        RemisionDetailModel GetById(int id);
        
        RemisionDetailModel Save(RemisionDetailModel model);

        IRemisionDetailModel Checked(IRemisionDetailModel model);

        bool Update(RemisionDetailModel remision);

        RemisionDetailModel UpdatePDF(RemisionDetailModel model);

        RemisionDetailPrinter GetPrinter(int index);

        RemisionDetailModel Certificar(RemisionDetailModel model);
    }
}
