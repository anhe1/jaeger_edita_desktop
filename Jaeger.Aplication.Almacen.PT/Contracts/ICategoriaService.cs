﻿namespace Jaeger.Aplication.Almacen.PT.Contracts {
    /// <summary>
    /// catalogo de categorias del almacen
    /// </summary>
    public interface ICategoriaService : Almacen.Contracts.IClasificacionService {

    }
}
