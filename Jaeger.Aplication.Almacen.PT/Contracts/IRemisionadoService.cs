﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.PT.Contracts {
    public interface IRemisionadoService : IRemisionService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        RemisionDetailModel FechaEntrega(RemisionDetailModel remision);

        RemisionDetailModel FechaCobranza(RemisionDetailModel remision);
    }
}
