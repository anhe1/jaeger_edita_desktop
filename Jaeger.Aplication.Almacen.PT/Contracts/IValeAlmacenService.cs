﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.PT.Contracts {
    public interface IValeAlmacenService : Almacen.Contracts.IAlmacenService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        IValeAlmacenDetailModel GetComprobante(int index);

        ValeAlmacenPrinter GetPrinter(int index);

        IValeAlmacenDetailModel Save(IValeAlmacenDetailModel model);

        IValeAlmacenDetailModel UpdatePDF(IValeAlmacenDetailModel model);
    }
}
