﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Base.Services;
using Jaeger.DataAccess.Almacen.PT.Repositories;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Almacen.PT.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Almacen.PT.Services {
    public class DevolucionService : Contracts.IDevolucionService {
        #region declaraciones
        protected ISqlValeAlmacenRepository valeAlmacenRepository;
        protected ISqlMovimientoAlmacenPT movimientoAlmacenPT;
        #endregion

        public DevolucionService() {
            this.valeAlmacenRepository = new SqlSugarValeAlmacenRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
            this.movimientoAlmacenPT = new SqlSugarMovimientoAlmacenRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public virtual DevolucionDetailModel GetNew() {
            return new DevolucionDetailModel() { FechaNuevo = DateTime.Now, Creo = ConfigService.Piloto.Clave, IdTipoMovimiento = 1, TipoComprobante = TipoComprobanteEnum.Devolucion, IdDepartamento = 8 };
        }

        public virtual DevolucionDetailModel GetById(int index) {
            //var _response = this.valeAlmacenRepository.GetVale(index);
            //if (_response != null) {
            //    var response = new DevolucionDetailModel(_response);
            //    response.Conceptos = this.GetConceptos(index);
            //return response;
            //}
            return null;
        }

        public virtual DevolucionDetailPrinterModel GetPrinter(int index) {
            var devolucion = this.GetById(index);
            if (devolucion != null) {
                var printer = new DevolucionDetailPrinterModel(devolucion);
                return printer;
            }
            return null;
        }

        public virtual DevolucionDetailModel Save(DevolucionDetailModel vale) {
            //if (vale.IsEditable) {
            //    vale.IdDocumento = this.valeAlmacenRepository.CreateGuid(new string[] { vale.GetOriginalString });
            //}
            //vale = this.valeAlmacenRepository.Save(vale);
            //if (vale.Conceptos != null) {
            //    for (int i = 0; i < vale.Conceptos.Count; i++) {
            //        vale.Conceptos[i].IdComprobante = vale.IdComprobante;
            //        vale.Conceptos[i].IdAlmacen = vale.IdAlmacen;
            //        vale.Conceptos[i].IdTipo = vale.IdTipoComprobante;
            //        vale.Conceptos[i].IdDepartamento = vale.IdDepartamento;
            //        //vale.Conceptos[i].IdPedido = vale.IdPedido;
            //        vale.Conceptos[i].IdCliente = vale.IdDirectorio;
            //        vale.Conceptos[i].IdEfecto = vale.IdTipoMovimiento;
            //        vale.Conceptos[i] = this.movimientoAlmacenPT.Save(vale.Conceptos[i]);
            //    }
            //}
            return vale;
        }

        public virtual BindingList<ValeAlmacenConceptoModel> GetConceptos(int idVale) {
            return new BindingList<ValeAlmacenConceptoModel>(this.movimientoAlmacenPT.GetList<ValeAlmacenConceptoModel>(
                    new List<IConditional> {
                        new Conditional("MVAPT_A", "1"),
                        new Conditional("MVAPT_ALMPT_ID", idVale.ToString())
                }).ToList());
        }

        #region metodos estaticos
        public static List<StatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<StatusModel, DevolucionPTStatusEnum>().ToList();
        }

        public static BindingList<TipoDevolucionModel> GetTiposDevolucion() {
            return new BindingList<TipoDevolucionModel>() {
                new TipoDevolucionModel(0, "No definido"),
                new TipoDevolucionModel(1, "Devolución Cliente"),
                new TipoDevolucionModel(2, "Solicitud de vendedor"),
                new TipoDevolucionModel(3, "Mercancía no pedida"),
                new TipoDevolucionModel(4, "Mercancía pedida por error"),
                new TipoDevolucionModel(5, "Cambio de la mercancía")
            };
        }

        public static IDevolucionBuilder Create() {
            return new DevolucionBuilder();
        }
        #endregion
    }
}
