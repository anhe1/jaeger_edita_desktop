﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Builder;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Almacen.PT.Builder;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Almacen.PT.Services {
    public class ValeAlmacenService : Almacen.Services.AlmacenService, Almacen.Contracts.IAlmacenService, IValeAlmacenService {
        #region declaraciones
        protected ISqlValeAlmacenRepository almacenRepository;
        protected ISqlValeRelacionRepository relacionRepository;
        protected ISqlValeStatusRepository statusRepository;
        protected ISqlMovimientoAlmacenPT movimientoAlmacen;
        #endregion

        public ValeAlmacenService() : base(AlmacenEnum.PT) {
            this.almacenRepository = new DataAccess.Almacen.PT.Repositories.SqlSugarValeAlmacenRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.relacionRepository = new DataAccess.Almacen.PT.Repositories.SqlSugarValeRelacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.statusRepository = new DataAccess.Almacen.PT.Repositories.SqlSugarValeStatusRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.almacenRepository.GetList<T1>(conditionals).ToList();
        }

        public IValeAlmacenDetailModel GetComprobante(int index) {
            return this.almacenRepository.GetById(index);
        }

        public ValeAlmacenPrinter GetPrinter(int index) {
            var d1 = this.GetComprobante(index);
            var d0 = new ValeAlmacenPrinter(d1 as ValeAlmacenDetailModel);
            return d0;
        }

        public IValeAlmacenDetailModel Save(IValeAlmacenDetailModel model) {
            // las partidas se almacen con el comprobante
            model = this.almacenRepository.Save(model);

            if (model.Autorizacion != null) {
                for (int i = 0; i < model.Autorizacion.Count; i++) {
                    model.Autorizacion[i].IdComprobante = model.IdComprobante;
                    this.statusRepository.Save(model.Autorizacion[i]);
                }
            }

            if (model.Relaciones != null) {
                for (int i = 0; i < model.Relaciones.Count; i++) {
                    model.Relaciones[i].IdComprobante = model.IdComprobante;
                    this.relacionRepository.Save(model.Relaciones[i]);
                }
            }

            return model;
        }

        public IValeAlmacenDetailModel UpdatePDF(IValeAlmacenDetailModel model) {
            return model;
        }

        #region metodos estaticos
        /// <summary>
        /// obtener listado de status
        /// </summary>
        public static List<StatusModel> GetStatus() {
            return ((ValeAlmacenStatusEnum[])Enum.GetValues(typeof(ValeAlmacenStatusEnum))).Select(c => new StatusModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// obtener listado de tipos de operacion del almacen de producto terminado
        /// </summary>
        public static List<DocumentoAlmacenDetailModel> TipoOperacion() {
            return new List<DocumentoAlmacenDetailModel> {
                new DocumentoAlmacenDetailModel{ IdDocumento = (int)TipoComprobanteEnum.EntradaProducto,  
                    Descripcion = "Vale de Entrada", TipoComprobante = TipoComprobanteEnum.EntradaProducto, TipoMovimiento = TipoMovimientoEnum.Ingreso, IsReadOnly = true, FormatoImpreso = "ValeAlmacenPT15Reporte.rdlc", ReceptorTipos = new BindingList<ItemSelectedModel>{ new ItemSelectedModel { Id = 1, Name = "Cliente", Selected = true } } },
                new DocumentoAlmacenDetailModel{ IdDocumento = (int)TipoComprobanteEnum.SalidaProducto,
                    Descripcion = "Vale de Salida", TipoComprobante = TipoComprobanteEnum.SalidaProducto, TipoMovimiento = TipoMovimientoEnum.Egreso, IsReadOnly = true, FormatoImpreso = "ValeAlmacenPT15Reporte.rdlc", ReceptorTipos = new BindingList<ItemSelectedModel>{ new ItemSelectedModel { Id = 4, Name = "Vendedor", Selected = true } }  },
                new DocumentoAlmacenDetailModel{ IdDocumento = (int)TipoComprobanteEnum.Devolucion,
                    Descripcion = "Devolución de Producto", TipoComprobante =  TipoComprobanteEnum.Devolucion, TipoMovimiento = TipoMovimientoEnum.Ingreso, IsReadOnly = true, ConValores = true, FormatoImpreso = "ValeAlmacenPT15Reporte.rdlc", ClaveMotivo = true, ReceptorTipos = new BindingList<ItemSelectedModel>{ new ItemSelectedModel { Id = 1, Name = "Cliente", Selected = true } }  },
                new DocumentoAlmacenDetailModel{IdDocumento =(int) TipoComprobanteEnum.RemisionInterna,   
                    Descripcion = "Remisión Interna", TipoComprobante = TipoComprobanteEnum.RemisionInterna, TipoMovimiento = TipoMovimientoEnum.Ingreso, IsReadOnly = true, FormatoImpreso = "ValeAlmacen15Reporte.rdlc", ReceptorTipos = new BindingList<ItemSelectedModel>{ new ItemSelectedModel { Id = 1, Name = "Cliente", Selected = true } }  }
            };
        }

        public static DocumentoAlmacenDetailModel TipoOperacion(int IdDocumento) {
            var d0 = TipoOperacion();
            try {
                var d1 = d0.Where(it => it.IdDocumento == IdDocumento).FirstOrDefault();
                if (d1 != null) {
                    return d1;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new DocumentoAlmacenDetailModel();
        }

        /// <summary>
        /// listado de tipos de relacion con otros vales
        /// </summary>
        public static List<DocumentoTipoRelacion> GetTipoRelacion() {
            return EnumerationExtension.GetEnumToClass<DocumentoTipoRelacion, ValeAlmacenTipoRelacionEnum>().ToList();
        }

        /// <summary>
        /// lista de tipos de devolucion
        /// </summary>
        public static List<TipoDevolucionModel> GetTipoDevolucion() {
            return EnumerationExtension.GetEnumToClass<TipoDevolucionModel, TipoDevolucionEnum>().ToList();
        }

        public static TipoDevolucionModel GetTipoDevolucion(int idDevolucion) {
            var d0 = GetTipoDevolucion();
            try {
                var d1 = d0.Where(it => it.Id == idDevolucion).FirstOrDefault();
                if (d1 != null) {
                    return d1;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new TipoDevolucionModel(0, "Desconocido");
        }

        /// <summary>
        /// clase builder para crear documento de almacén
        /// </summary>
        public static IValeAlmacenBuilder Create() {
            return new ValeAlmacenBuilder();
        }

        /// <summary>
        /// builder para crear consulta del almacen
        /// </summary>
        public static IValeAlmacenQueryBuilder Query() {
            return new ValeAlmacenQueryBuilder();
        }
        #endregion
    }
}
