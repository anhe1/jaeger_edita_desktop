﻿using System.Collections.Generic;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.PT.Services {
    public partial class RemisionadoService : RemisionService, IRemisionadoService {
        public RemisionadoService() : base() { }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(Domain.Almacen.PT.Entities.RemisionReceptorModel))
                return this.remisionRepository.GetListR<T1>(conditionals);
            return this.remisionRepository.GetList<T1>(conditionals);
        }
    }
}
