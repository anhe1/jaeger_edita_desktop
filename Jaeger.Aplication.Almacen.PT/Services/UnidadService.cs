﻿using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.PT.Services {
    public class UnidadService : Almacen.Services.UnidadService, IUnidadService {

        public UnidadService() : base(AlmacenEnum.PT) {
            
        }
    }
}
