﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Almacen.PT.Builder;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Almacen.PT.Builder;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Almacen.PT.Services {
    public class RemisionService : IRemisionService {
        #region declaraciones
        protected ISqlRemisionadoPTRepository remisionRepository;
        protected ISqlRemisionadoRRepository relacionesRepository;
        protected ISqlMovimientoAlmacenPT movimientoAlmacenPT;
        protected ISqlRemisionStatusRepository statusRepository;
        protected IEditaBucketService s3;
        protected internal IConfiguration _Configuration;
        #endregion

        public RemisionService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.s3 = new EditaBucketService {
                Folder = "remision"
            };
            this.remisionRepository = new SqlRemisionadoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.relacionesRepository = new SqlSugarRemisionadoRRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.movimientoAlmacenPT = new SqlSugarMovimientoAlmacenRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.statusRepository = new SqlSugarRemisionStatusRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._Configuration = Configuration().Default().Build();
            this._Configuration.ShowPagare = false;
        }

        public virtual RemisionDetailModel GetById(int id) {
            var remision = this.remisionRepository.GetById(id);
            if (remision != null) {
                remision.Conceptos = this.GetConceptos(id);
                remision.Autorizaciones = this.GetStatus(id);
                remision.RemisionRelacionada = this.GetRelaciones(id);
            }
            return remision;
        }

        public virtual BindingList<RemisionConceptoDetailModel> GetConceptos(int idRemision) {
            return new BindingList<RemisionConceptoDetailModel>(this.movimientoAlmacenPT.GetList<RemisionConceptoDetailModel>(RemisionService.Query().GetConceptos(idRemision).Build()).ToList());
            //return new BindingList<RemisionConceptoDetailModel>(this.movimientoAlmacenPT.GetList<RemisionConceptoDetailModel>(
            //    new List<IConditional> {
            //        new Conditional("MVAPT_RMSN_ID", idRemision.ToString()),
            //        new Conditional("MVAPT_A", "1"),
            //        new Conditional("MVAPT_DOC_ID", "26")
            //    }).ToList());
        }

        public virtual BindingList<RemisionRelacionadaModel> GetRelaciones(int idRemision) {
            return new BindingList<RemisionRelacionadaModel>(this.relacionesRepository.GetList<RemisionRelacionadaModel>(
                new List<IConditional> {
                    new Conditional("RMSNR_RMSN_ID", idRemision.ToString())
                }).ToList());
        }

        public virtual RemisionDetailPrinter GetPrinter(int index) {
            var remision = this.GetById(index);
            if (remision.IsEditable) {
                remision = this.Certificar(remision);
            }

            return RemisionService.Printer().Add(_Configuration).Build(remision);
        }

        public virtual RemisionDetailModel Save(RemisionDetailModel remision) {
            remision = this.remisionRepository.Save(remision);
            if (remision.IdRemision > 0) {
                if (remision.Conceptos != null) {
                    for (int i = 0; i < remision.Conceptos.Count; i++) {
                        remision.Conceptos[i].IdCliente = remision.IdCliente;
                        remision.Conceptos[i].IdRemision = remision.IdRemision;
                        this.movimientoAlmacenPT.Save(remision.Conceptos[i]);
                    }
                }

                if (remision.Autorizaciones != null) {
                    for (int i = 0; i < remision.Autorizaciones.Count; i++) {
                        remision.Autorizaciones[i].IdRemision = remision.IdRemision;
                        if (remision.Autorizaciones[i].IdAuto == 0) {
                            remision.Autorizaciones[i] = this.statusRepository.Save(remision.Autorizaciones[i]);
                        }
                    }
                }

                if (remision.RemisionRelacionada != null) {
                    for (int i = 0; i < remision.RemisionRelacionada.Count; i++) {
                        remision.RemisionRelacionada[i].IdRemision = remision.IdRemision;
                        //remision.RemisionRelacionada[i] =
                        this.relacionesRepository.Saveable(remision.RemisionRelacionada[i]);
                    }
                }
            }
            return remision;
        }

        public virtual RemisionDetailModel Certificar(RemisionDetailModel model) {
            model.IdDocumento = this.remisionRepository.CreateGuid(new string[] { model.GetOriginalString });
            model.Status = RemisionBuilder.ControlStatus(model);
            // afectar existencias
            if (this.movimientoAlmacenPT.Existencia(new RemisionQueryBuilder().SetStock(model.IdRemision).Build())) {
                Console.WriteLine("No se afectaron las existencias");
            }
            return this.Save(model);
        }

        /// <summary>
        /// solo actualizar los datos generales del comprobante
        /// </summary>
        public virtual bool Update(RemisionDetailModel remision) {
            return this.remisionRepository.Update(remision) > 0;
        }

        public virtual RemisionDetailModel UpdatePDF(RemisionDetailModel model) {
            this.s3.Folder = "remision";
            model.UrlFilePDF = this.s3.Upload((string)model.Tag, model.KeyName + ".pdf");
            this.remisionRepository.UploadPDF(model);
            return model;
        }

        /// <summary>
        /// historial de cambios de status
        /// </summary>
        public virtual BindingList<RemisionStatusModel> GetStatus(int idRemision) {
            return new BindingList<RemisionStatusModel>(this.statusRepository.GetList<RemisionStatusModel>(new List<IConditional> {
                new Conditional("RMSNS_RMSN_ID", idRemision.ToString())
            }).ToList());
        }

        public virtual int DiasCredito(int idCliente) {
            return -1;
        }

        public virtual RemisionDetailModel FechaEntrega(RemisionDetailModel remision) {
            remision.Status = RemisionBuilder.ControlStatus(remision);
            if (remision.Status == RemisionStatusEnum.Entregado) {
                var d = this.DiasCredito(remision.IdCliente);
                if (d > 0) {
                    remision.FechaVence = remision.FechaEntrega.Value.AddDays(d);
                } else {
                    remision.FechaVence = remision.FechaEntrega.Value.AddDays(1);
                }
                this.Update(remision);
            }
            return remision;
        }

        public virtual RemisionDetailModel FechaCobranza(RemisionDetailModel remision) {
            remision.Status = RemisionBuilder.ControlStatus(remision);
            if (remision.Status == RemisionStatusEnum.Por_Cobrar) {
                var diasCredito = 0;
                if (remision.FechaVence != null && remision.FechaVence.Value != null) {
                    diasCredito = remision.FechaVence.Value.Subtract(remision.FechaEntrega.Value).Days;
                } else {
                    var d = this.DiasCredito(remision.IdCliente);
                    if (d > 0) {
                        remision.FechaVence = remision.FechaEntrega.Value.AddDays(d);
                    } else {
                        remision.FechaVence = remision.FechaEntrega.Value.AddDays(1);
                    }
                }
                // si el factor pactado es mayor de cero
                if (remision.FactorPactado > 0) {
                    if (diasCredito == 0) {
                        diasCredito = 1;
                    }
                    // si los dias de credito son mayores a los dias transcurridos
                    if (diasCredito >= remision.DiasTranscurridos) {
                        remision.DescuentoFactor = remision.FactorPactado;
                        remision.DescuentoTipo = "Autorizado";
                        remision.PorCobrar = remision.GTotal * remision.DescuentoFactor;
                        remision.DescuentoTotal = remision.GTotal - remision.PorCobrar;
                    }
                } else {
                    remision.DescuentoFactor = 0;
                    remision.DescuentoTipo = "Sin Descuento";
                    remision.DescuentoTotal = 0;
                    remision.PorCobrar = remision.Total;
                    remision.PorCobrarPactado = remision.Total;
                }
                this.Update(remision);
            }
            return remision;
        }

        /// <summary>
        /// obtener listado para el estado de cuenta por cliente o vendedor
        /// </summary>
        /// <param name="idVendedor">si es mayor de cero entonces vendedor</param>
        /// <param name="idCliente">si es mayor de cero entonces cliente</param>
        public virtual List<RemisionSingleModel> GetEdoCuenta(int idVendedor, int idCliente = 0) {
            var condiciones = new List<IConditional> {
                new Conditional("RMSN_STTS_ID", "4")
            };

            if (idCliente > 0)
                condiciones.Add(new Conditional("RMSN_DRCTR_ID", idCliente.ToString()));

            if (idVendedor > 0)
                condiciones.Add(new Conditional("RMSN_VNDDR_ID", idVendedor.ToString()));

            return this.remisionRepository.GetList<RemisionSingleModel>(condiciones).ToList();
        }

        public virtual IRemisionDetailModel Checked(IRemisionDetailModel remision) {
            if (remision.Conceptos != null) {
                if (remision.Conceptos.Count() > 0) {
                    for (int i = 0; i < remision.Conceptos.Count; i++) {

                    }
                }
            }
            return remision;
        }

        #region metodos estaticos
        /// <summary>
        /// obtener listado de status de remisiones
        /// </summary>
        public static List<StatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<StatusModel, RemisionStatusEnum>().ToList();
        }

        /// <summary>
        /// listado de metodos de envio
        /// </summary>
        public static List<RemisionMetodoEnvioModel> GetMetodoEnvio() {
            return new List<RemisionMetodoEnvioModel>() {
                new RemisionMetodoEnvioModel(0, "Recoger en sucursal", 0),
                new RemisionMetodoEnvioModel(1, "Paquetería Interna"),
                new RemisionMetodoEnvioModel(2, "Paquetería Externa"),
                new RemisionMetodoEnvioModel(3, "FEDEX")
            };
        }

        /// <summary>
        /// listado de condiciones de pago
        /// </summary>
        public static List<RemisionCondicionPagoModel> GetCondicionPago() {
            return EnumerationExtension.GetEnumToClass<RemisionCondicionPagoModel, RemisionCondicionesPagoEnum>().ToList();
        }

        public static RemisionCondicionPagoModel GetCondicionPago(int id) {
            var lista = GetCondicionPago();
            return lista.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// listado de tipos de relacion con otros comprobantes
        /// </summary>
        public static List<RemisionRelacionModel> GetTipoRelacion() {
            return EnumerationExtension.GetEnumToClass<RemisionRelacionModel, RemisionTipoRelacionEnum>().ToList();
        }

        public static List<IRemisionTempleteModel> GetRemisionTemplete() {
            return new List<IRemisionTempleteModel> {
                new RemisionTempleteModel("Simple", "", "RemisionSimpleV20.rdlc"),
                new RemisionTempleteModel("Simple con importes", "", "RemisionImportesV20.rdlc"),
                new RemisionTempleteModel("Con Pagaré", "", "RemisionPagareV201.rdlc"),
            };
        }
        #endregion

        #region builder
        public static IConfigurationBuilder Configuration() {
            return new ConfigurationBuilder();
        }

        /// <summary>
        /// builder para la creacion del objeto printer para la remision
        /// </summary>
        public static IRemisionPrinterBuilder Printer() {
            return new RemisionPrinterBuilder();
        }

        /// <summary>
        /// builder para creacion de objeto remision
        /// </summary>
        public static IRemisionBuilder Create() {
            return new RemisionBuilder();
        }

        public static IRemisionQueryBuilder Query() {
            return new RemisionQueryBuilder();
        }
        #endregion
    }
}
