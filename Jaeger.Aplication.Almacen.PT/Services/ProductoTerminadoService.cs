﻿using System.Collections.Generic;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.DataAccess.Almacen.PT.Repositories;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Almacen.PT.Services {
    /// <summary>
    /// catalogo de productos del almacen de producto terminado
    /// </summary>
    public class ProductoTerminadoService : Almacen.Services.CatalogoProductoService, IProductoTerminadoService, ICatalogoProductoService {
        protected internal ISqlCategoriaRepository categoriaRepository;

        public ProductoTerminadoService() : base(Domain.Base.ValueObjects.AlmacenEnum.PT) {
            this.categoriaRepository = new SqlSugarCategoriaRepository(ConfigService.Synapsis.RDS.Edita);
        }

        public override List<IClasificacionSingle> GetClasificacion() {
            throw new System.NotImplementedException();
        }
    }
}
