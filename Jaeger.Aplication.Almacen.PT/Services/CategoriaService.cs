﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.PT.Services {
    public class CategoriaService : Contracts.ICategoriaService {
        protected internal ISqlCategoriaRepository categoriaRepository;

        public CategoriaService() {
            
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.categoriaRepository.GetList<T1>(conditionals);
        }
    }
}
