﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Jaeger.Repositorio.Contracts;
using Jaeger.Repositorio.Entities;

namespace Jaeger.Repositorio.Abstractions {
    public class TableResultHtmlBuilder : HtmlToTable {
        public TableResultHtmlBuilder() {
            tableName = "ctl00_MainContent_tblResult";
            dTable = new DataTable();
            _Result = new List<IComprobanteFiscal>();
        }

        public TableResultHtmlBuilder WithHtml(string strHtml) {
            strHtml = Regex.Replace(strHtml, "</?(div)[^>]*>", string.Empty, RegexOptions.IgnoreCase);
            strHtml = Regex.Replace(strHtml, "<script[^>]*>[\\s\\S]*?</script>", string.Empty, RegexOptions.IgnoreCase);
            htmlString = strHtml;
            return this;
        }

        public List<IComprobanteFiscal> Registros {
            get { return _Result; }
        }

        protected internal override void CreateRows(HtmlNode table) {
            var filas = new List<List<string>>();
            foreach (HtmlNode childNode in table.ChildNodes) {
                if (childNode.Name.ToUpper() == "TR") {
                    List<string> fila = new List<string>();
                    IComprobanteFiscal registro = new ComprobanteFiscalResponse();
                    int columna = 0;
                    foreach (HtmlNode htmlNode in childNode.ChildNodes) {
                        if (htmlNode.Name.ToUpper() == "TD") {
                            if (htmlNode.OuterHtml.Contains("BtnDescarga")) {
                                var acciones = new RegistroAccion();
                                acciones.WithUrlXml(GetElementURL(htmlNode, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnDescarga"));
                                acciones.WithUrlDetalle(GetElementURL(htmlNode, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnVerDetalle"));
                                acciones.WithUrlPdf(GetElementURL(htmlNode, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnRI"));
                                acciones.WithUrlAcuseFinal(GetElementURL(htmlNode, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnRecuperaAcuse"));
                                registro.WithAcciones(acciones);
                                fila.Add(acciones.ToString());
                            } else {
                                string data = htmlNode.InnerText.Replace("\r", "").Replace("\n", "").Trim();
                                if (Headers[columna].ToLower() == "Folio Fiscal".ToLower()) {
                                    registro.WithFolioFiscal(data);
                                } else if (Headers[columna].ToLower() == "RFC Emisor".ToLower()) {
                                    registro.WithEmisorRFC(data);
                                } else if (Headers[columna].ToLower() == "Nombre o Razón Social del Emisor".ToLower()) {
                                    registro.Emisor = data;
                                } else if (Headers[columna].ToLower() == "RFC Receptor".ToLower()) {
                                    registro.ReceptorRFC = data;
                                } else if (Headers[columna].ToLower() == "Nombre o Razón Social del Receptor".ToLower()) {
                                    registro.Receptor = data;
                                } else if (Headers[columna].ToLower() == "Fecha de Emisión".ToLower()) {
                                    registro = registro.WithFechaEmision(data);
                                } else if (Headers[columna].ToLower() == "Fecha de Certificación".ToLower()) {
                                    registro = registro.WithFechaCertificacion(data);
                                } else if (Headers[columna].ToLower() == "PAC que Certificó".ToLower()) {
                                    registro.PacCertifica = data;
                                } else if (Headers[columna].ToLower() == "Total".ToLower()) {
                                    registro.WithTotal(data);
                                } else if (Headers[columna].ToLower() == "Efecto del Comprobante".ToLower()) {
                                    registro.Efecto = data;
                                } else if (Headers[columna].ToLower() == "Estatus de cancelación".ToLower()) {
                                    registro.EstatusCancelacion = data;
                                } else if (Headers[columna].ToLower() == "Estado del Comprobante".ToLower()) {
                                    registro.EstadoComprobante = data;
                                } else if (Headers[columna].ToLower() == "Estatus de Proceso de Cancelación".ToLower()) {
                                    registro.EstatusProcesoCancelacion = data;
                                } else if (Headers[columna].ToLower() == "Fecha de Proceso de Cancelación".ToLower()) {
                                    registro.WithFechaProcesoCancelaion(data);
                                } else if (Headers[columna].ToLower() == "RFC a cuenta de terceros".ToLower()) {
                                    registro.CuentaTercerosRFC = data;
                                } else if (Headers[columna].ToLower() == "Motivo".ToLower()) {
                                    registro.Motivo = data;
                                } else if (Headers[columna].ToLower() == "Folio de Sustitución".ToLower()) {
                                    registro.Motivo = data;
                                }
                                fila.Add(data);
                            }
                            columna++;
                        }
                    }
                    if (fila.Count == dTable.Columns.Count) {
                        _Result.Add(registro);
                        filas.Add(fila);
                    }
                }
            }

            foreach (var item in filas) {
                DataRow dataRow = dTable.NewRow();
                int num = 0;
                if (item.Count != dTable.Columns.Count) {
                    throw new Exception("No coincide el número de Valores con los encabezados encontrados.");
                }
                foreach (string str1 in item) {
                    int num1 = num;
                    num = num1 + 1;
                    dataRow[num1] = str1;
                }
                dTable.Rows.Add(dataRow);
            }

        }

        private string GetElementURL(HtmlNode element, string url, string byName) {
            string _result = "";
            var localHtml = new HtmlDocument();
            localHtml.LoadHtml(element.OuterHtml);
            try {
                var _elementById = localHtml.GetElementbyId(byName);
                if (_elementById != null) {
                    var _file = Regex.Match(_elementById.OuterHtml, "'.*?'").Value.Replace("'", "");
                    _result = string.Format(url, _file);
                }
            } catch (Exception ex) {
                Services.LogErrorService.LogWrite(ex.StackTrace);
                Console.WriteLine("GetElementURL: " + ex.Message);
            }

            return _result;
        }
    }
}
