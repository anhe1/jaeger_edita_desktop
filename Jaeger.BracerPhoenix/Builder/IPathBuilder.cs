﻿using Jaeger.Repositorio.Contracts;

namespace Jaeger.Repositorio.Builder {
    /// <summary>
    /// Builder para creacion de rutas del repositorio
    /// </summary>
    public interface IPathBuilder {
        IPathRootBuilder AddRoot(string path);
    }

    public interface IPathRootBuilder {
        IPathCoomprobanteBuilder AddRFC(string rfc);
    }

    public interface IPathCoomprobanteBuilder {
        IPathDirectoryBuilder WithComprobante(IComprobanteFiscal comprobanteFiscal);
    }

    public interface IPathTempleteBuilder {
        IPathKeyNameBuilder AddKeyName(string extension = "xml");
    }

    public interface IPathDirectoryBuilder {
        IPathCoomprobanteBuilder AddPrefix(string prefix = "CFDI-");
        IPathDirectoryBuilder AddTipo();
        IPathDirectoryBuilder AddYear();
        IPathDirectoryBuilder AddMonth();
        IPathDirectoryBuilder AddDay();
        IPathKeyNameBuilder AddKeyName(string extension = "xml");
        /// <summary>
        /// Repositorio/RFC/Tipo/Year/Month/Day/CFD-
        /// </summary>
        IPathTempleteBuilder Templete();
    }

    public interface IPathKeyNameBuilder {
        IPathDirectoryBuilder Create();

        string Build();
    }
}
