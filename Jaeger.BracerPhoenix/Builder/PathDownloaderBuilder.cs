﻿using Jaeger.Repositorio.Contracts;
using System.Collections.Generic;
using System.IO;

namespace Jaeger.Repositorio.Builder {
    /// <summary>
    /// Builder para creacion de rutas del repositorio
    /// </summary>
    public class PathDownloaderBuilder : IPathBuilder, IPathRootBuilder, IPathCoomprobanteBuilder, IPathDirectoryBuilder, IPathKeyNameBuilder, IPathTempleteBuilder {
        #region declaraciones
        protected internal Repositorio.Entities.ConfiguracionDetail _Configuration;
        protected internal IComprobanteFiscal _ComprobanteFiscal;
        protected internal List<string> _Partes;
        protected internal string _RFC;
        protected internal string _DirectoryRoot;
        protected internal string _FileName;
        protected internal string _Prefix = "CFDI-";
        #endregion

        public PathDownloaderBuilder() {
            this._Partes = new List<string>();
        }

        public string Build() {
            var d0 = Path.Combine(this._Partes.ToArray());
            var d1 = Path.Combine(d0, this._FileName);
            return d1;
        }

        public IPathRootBuilder AddRoot(string path) {
            this._DirectoryRoot = path;
            this._Partes.Add(path);
            return this;
        }

        public IPathCoomprobanteBuilder AddRFC(string rfc) {
            this._RFC = rfc.ToUpper();
            this._Partes.Add(this._RFC);
            return this;
        }

        public IPathCoomprobanteBuilder AddPrefix(string prefix = "CFDI-") {
            this._Prefix = prefix;
            return this;
        }

        public IPathDirectoryBuilder AddTipo() {
            if (this._RFC == this._ComprobanteFiscal.EmisorRFC) {
                this._Partes.Add("Emitidos");
            } else if (this._RFC == this._ComprobanteFiscal.ReceptorRFC) {
                this._Partes.Add("Recibidos");
            } else {

            }
            return this;
        }

        public IPathDirectoryBuilder AddYear() {
            this._Partes.Add(this._ComprobanteFiscal.FechaEmision.Year.ToString("00"));
            return this;
        }

        public IPathDirectoryBuilder AddMonth() {
            this._Partes.Add(this._ComprobanteFiscal.FechaEmision.Month.ToString("00"));
            return this;
        }

        public IPathDirectoryBuilder AddDay() {
            this._Partes.Add(this._ComprobanteFiscal.FechaEmision.Day.ToString("00"));
            return this;
        }

        public IPathKeyNameBuilder AddKeyName(string extension = ".xml") {
            if (!extension.StartsWith(".")) { extension = "." + extension; }
            this._FileName = this._Prefix + Path.GetFileNameWithoutExtension(this._ComprobanteFiscal.KeyName) + extension;
            return this;
        }

        public IPathDirectoryBuilder WithComprobante(IComprobanteFiscal comprobanteFiscal) {
            this._Partes = new List<string>();
            this._ComprobanteFiscal = comprobanteFiscal;
            return this;
        }

        public IPathTempleteBuilder Templete() {
            this._Partes = new List<string> {
                this._DirectoryRoot,
                this._RFC
            };
            this.AddTipo().AddYear().AddMonth().AddDay();
            return this;
        }

        public IPathDirectoryBuilder Create() {
            return this;
        }

    }
}
