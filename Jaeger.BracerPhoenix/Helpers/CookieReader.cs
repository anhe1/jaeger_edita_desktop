﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Jaeger.Repositorio.Services {
    public class CookieReader {
        public const int InternetCookieHttponly = 8192;

        public static string GetCookie(string url) {
            int num = 512;
            StringBuilder stringBuilder = new StringBuilder(num);
            if (!InternetGetCookieEx(url, null, stringBuilder, ref num, InternetCookieHttponly, IntPtr.Zero)) {
                if (num >= 0) {
                    stringBuilder = new StringBuilder(num);
                    if (!InternetGetCookieEx(url, null, stringBuilder, ref num, InternetCookieHttponly, IntPtr.Zero)) {
                        return null;
                    }
                } else {
                    return null;
                }
            }
            return stringBuilder.ToString();
        }

        [DllImport("wininet.dll", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool InternetGetCookieEx(string url, string cookieName, StringBuilder cookieData, ref int size, int flags, IntPtr pReserved);
    }
}
