﻿// develop: anhe
// purpose: consulta y descarga de comprobantes fiscales con el metodo tradicional
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Jaeger.Repositorio.Services;

namespace Jaeger.Repositorio.V4 {
    public class DescargaXml4SAT {

        private string _currentUrl;
        private bool _supplierStructure;
        private WebBrowser webSAT;

        private int MaximoDeDescargasSAT = 495;
        private bool SeProcesoWB_SAT = false;
        private int Intentos = 0;

        private string gUsuario = "";
        private string gPassword = "";
        private DateTime gFechaIni;
        private DateTime gFechaFin;
        private string gTipoER = "";
        private string gEstatus = "";
        private string gRutaDescargaXML = "";
        private string gEmisor = "";
        private string gCaptcha = "";
        public bool gConexion = true;

        private TextBox gtxtLog = null;

        public DescargaXml4SAT(TextBox txtLog) {
            this._supplierStructure = true;
            gtxtLog = txtLog;
        }

        //public string Controller_DescargaXmlSat(WebBrowser _wsSat, string Usuario, string Password, DateTime FechaIni, DateTime FechaFin, string TipoER, string Estatus, string RutaDescargaXML, string Emisor, string captcha, bool Conexion) {
        //    gUsuario = Usuario;
        //    gPassword = Password;
        //    gFechaIni = FechaIni;
        //    gFechaFin = FechaFin;
        //    gTipoER = TipoER;
        //    gEstatus = Estatus;
        //    gRutaDescargaXML = RutaDescargaXML;
        //    gEmisor = Emisor;
        //    gCaptcha = captcha;
        //    webSAT = _wsSat;
        //    gConexion = Conexion;

        //    webSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.wbSAT_DocumentCompleted);

        //    this.Authenticate();

        //    return "OK";
        //}

        private void Authenticate() {
            try {

                this.Message("Ultima Revision... 2022-06-27");
                this.Message(string.Format(Properties.Resources.Message_IniciarSession, gUsuario.Trim().ToUpper()));

                // oPCION 1: PARA PASARLE EL RFC, CIEC Y CAPTCHA 
                // string postData = string.Format("&Ecom_User_ID={0}&Ecom_Password={1}&userCaptcha={2}&submit=Enviar", gUsuario, gPassword, gCaptcha);
                // System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                // byte[] bytes = encoding.GetBytes(postData);
                // _wbSat.Navigate(_wbSat.Url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");

                // oPCION 2:  PARA PASARLE EL RFC, CIEC Y CAPTCHA 
                this.webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", gUsuario);
                this.webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", gPassword);
                this.webSAT.Document.GetElementById("UserCaptcha").SetAttribute("value", gCaptcha); // sep 2019 cambio para recuperar el captcha "jcaptcha" por "UserCaptcha"
                                                                                                    //this._wbSat.Document.GetElementById("submit").InvokeMember("click");             // si funciona, pero sale ventana emergente (Desbordamiento de pila)
                this.webSAT.Document.GetElementById("submit").Focus();
                SendKeys.Send("{ENTER}");                                                          // con este similar no, todo ok
                this.webSAT.Document.GetElementById("submit").Focus();
                SendKeys.Send("{ENTER}");                                                          // con este similar no, todo ok


                #region ESPERA A QUE SE CARGUE LA PAGINA WEB Y SE PROCESE EL METODOD wbSAT_DocumentCompleted
                SeProcesoWB_SAT = false;

                Intentos = 0;
                while (true) {
                    Application.DoEvents();
                    if (!this.webSAT.IsBusy) {
                        //      break;
                        //_wbSat.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");

                        if (SeProcesoWB_SAT) {
                            break;
                        }

                        if (Intentos > 100000) {
                            this.Message("CONTRASEÑA CIEC ..... ");
                            //  this.Message("AVISO IMPORTANTE: CONTRAEÑA CIEC INCORRECTA  " + Intentos.ToString());
                            break;
                        }
                        Intentos = Intentos + 1;
                    }
                }

                // this.Message("Contador de Intentos = " + Intentos.ToString());

                //this._wbSat.Dispose();
                #endregion

            } catch (Exception exception) {
                throw new Exception(exception.Message);
            }
        }

        private void wbSAT_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {

            DateTime FI = gFechaIni;
            DateTime FF = gFechaFin;
            string TipoER = gTipoER.Trim();

            bool flag;
            bool flag1;

            if (this.webSAT.Document != null) {
                //MessageBox.Show(this._wbSat.Url.ToString());
                //                                       1         2         3       3 4         5
                //                              123456789.123456789.123456789.123456789.123456789.
                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                || this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")
                ) {
                    this.Message(string.Format(Properties.Resources.Message_Error_Autenticado, gUsuario));

                    if (TipoER == "Recibidos")  // XML Recibidos
                    {
                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                    }


                    if (TipoER == "Emitidos") // XML Enviados
                    {
                        this.webSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                    }
                }

                if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app/login?sid=1&sid=1")) {
                    //this.txtPassword.Focus();
                    //this.txtPassword.SelectionStart = 0;
                    //this.txtPassword.SelectionLength = this.txtPassword.Text.Length;
                    this.Message(Properties.Resources.Message_Error_ClaveCIEC);
                    webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", gUsuario.Trim().ToUpper());
                    webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", gPassword.Trim());
                    webSAT.Document.GetElementById("jcaptcha").SetAttribute("value", gCaptcha);
                    webSAT.Document.GetElementById("submit").InvokeMember("click");
                }

                flag = (this.webSAT.Url != new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx") ? true : !(this.webSAT.Url != new Uri(this._currentUrl)));
                if (!flag) {
                    #region Documentos Recibidos
                    for (DateTime Fecha = FI; Fecha <= FF; Fecha = Fecha.AddDays(1)) {
                        this.Message(string.Format("Procesando documentos recibidos de la fecha  " + Fecha.ToString("dd/MM/yyyy")));

                        int VecesQueSeHaLLamdoADocumentos = 0;
                        int TotalArchivosDescargados = 0;
                        DateTime UltimaFechaEmision = DateTime.Now;
                        do {
                            TotalArchivosDescargados = 0;
                            VecesQueSeHaLLamdoADocumentos = VecesQueSeHaLLamdoADocumentos + 1;
                            this.GetReceivedDocuments(Fecha.Year, Fecha.Month, Fecha.Day, ref TotalArchivosDescargados, ref UltimaFechaEmision, VecesQueSeHaLLamdoADocumentos);
                        }
                        while (TotalArchivosDescargados >= MaximoDeDescargasSAT);
                    }
                    #endregion

                    this.FinishSession();
                }

                flag1 = (this.webSAT.Url != new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx") ? true : !(this.webSAT.Url != new Uri(this._currentUrl)));
                if (!flag1) {
                    #region Documentos Enviados
                    for (DateTime Fecha = FI; Fecha <= FF; Fecha = Fecha.AddDays(1)) {
                        this.Message(string.Format("Procesando documentos enviados de la fecha  ." + Fecha.ToString("dd/MM/yyyy")));

                        int VecesQueSeHaLLamdoADocumentos = 0;
                        int TotalArchivosDescargados = 0;
                        DateTime UltimaFechaEmision = DateTime.Now;
                        do {
                            TotalArchivosDescargados = 0;
                            VecesQueSeHaLLamdoADocumentos = VecesQueSeHaLLamdoADocumentos + 1;
                            this.GetSendedDocuments(Fecha.Year, Fecha.Month, Fecha.Day, ref TotalArchivosDescargados, ref UltimaFechaEmision, VecesQueSeHaLLamdoADocumentos);
                        }
                        while (TotalArchivosDescargados >= MaximoDeDescargasSAT);
                    }
                    #endregion

                    this.FinishSession();
                }

                int timeout = 0;
                while (true) {
                    Application.DoEvents();
                    if (!this.webSAT.IsBusy) {
                        break;
                    }
                    timeout = timeout + 1;
                    if (timeout > 40000)
                        break;

                }
                this._currentUrl = this.webSAT.Url.ToString();
            }
        }

        private void GetReceivedDocuments(int year, int month, int day, ref int TotalArchivosDescargados, ref DateTime UltimaFechaEmision, int VecesQueSeHaLLamadoAEsteMetodo) {
            bool flag1;
            bool flag2;
            try {
                if (this.webSAT.Document != null) {
                    #region Captura los datos de la pagina web
                    #region Selecciona el RdoFechas con ub CLICK
                    HtmlElement elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_RdoFechas");
                    if (elementById != null) {
                        elementById.InvokeMember("click");
                    }

                    while (true) {
                        Application.DoEvents();
                        HtmlElement htmlElement = this.webSAT.Document.GetElementById("DdlAnio");
                        flag1 = (htmlElement == null ? true : !(htmlElement.GetAttribute("disabled") != "disabled"));
                        if (!flag1) {
                            break;
                        }
                    }
                    #endregion

                    #region Captura la FECHA del día
                    HtmlElement elementById1 = this.webSAT.Document.GetElementById("DdlAnio");
                    if (elementById1 != null) {
                        elementById1.SetAttribute("value", year.ToString(CultureInfo.InvariantCulture));
                    }
                    HtmlElement htmlElement1 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMes");
                    if (htmlElement1 != null) {
                        htmlElement1.SetAttribute("value", month.ToString("0"));
                    }
                    HtmlElement elementById2 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlDia");
                    if (elementById2 != null) {
                        elementById2.SetAttribute("value", day.ToString("00"));
                    }
                    #endregion
                    // 

                    if (VecesQueSeHaLLamadoAEsteMetodo > 1) {
                        #region Set atributo de la Hora Inicial
                        HtmlElement elementById4 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");
                        if (elementById4 != null) {
                            elementById4.SetAttribute("value", UltimaFechaEmision.Hour.ToString());
                        }
                        HtmlElement elementById5 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");
                        if (elementById5 != null) {
                            elementById5.SetAttribute("value", UltimaFechaEmision.Minute.ToString());
                        }
                        HtmlElement elementById6 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");
                        if (elementById6 != null) {
                            elementById6.SetAttribute("value", UltimaFechaEmision.Second.ToString());
                        }
                        #endregion

                        //if (false) {
                        //    #region Set atributo de la Hora Final
                        //    HtmlElement elementById7 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");
                        //    if (elementById7 != null) {
                        //        elementById7.SetAttribute("value", "14");
                        //    }
                        //    HtmlElement elementById8 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");
                        //    if (elementById8 != null) {
                        //        elementById8.SetAttribute("value", "59");
                        //    }
                        //    HtmlElement elementById9 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");
                        //    if (elementById9 != null) {
                        //        elementById9.SetAttribute("value", "59");
                        //    }
                        //    #endregion
                        //}
                    }

                    if (gEstatus.Trim() != "Todos") {
                        #region Set atributo de Estatus
                        HtmlElement elementById10 = this.webSAT.Document.GetElementById("ctl00_MainContent_DdlEstadoComprobante");
                        if (elementById10 != null) {
                            if (gEstatus.Trim() == "Cancelados")
                                elementById10.SetAttribute("value", "0");
                            if (gEstatus.Trim() == "Vigentes")
                                elementById10.SetAttribute("value", "1");
                        }
                        #endregion
                    }

                    #region Se le da CLICK al btnBusqueda
                    HtmlElement htmlElement2 = this.webSAT.Document.GetElementById("ctl00_MainContent_BtnBusqueda");
                    if (htmlElement2 != null) {
                        htmlElement2.InvokeMember("click");
                    }

                    int num = 0;
                    while (true) {
                        Application.DoEvents();
                        HtmlElement elementById3 = this.webSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
                        if (!(elementById3 == null)) {
                            flag2 = (elementById3.Style != "display: none;" ? true : num <= 100000);
                            if (!flag2) {
                                break;
                            }
                            num++;
                        }
                    }
                    #endregion
                    #endregion

                    this.ProcessResult(ref TotalArchivosDescargados, ref UltimaFechaEmision, "Recibidos");
                }
            } catch (Exception exception) {
                this.Message("ERROR EN EL METODO GetRecivedDocuments: " + exception.Message);

                MessageBox.Show(exception.Message);
            }
        }

        private void GetSendedDocuments(int year, int month, int day, ref int TotalArchivosDescargados, ref DateTime UltimaFechaEmision, int VecesQueSeHaLLamadoAEsteMetodo) {
            HtmlElement elementById;
            bool flag1;
            bool flag2;
            bool flag3;
            bool flag4;
            try {
                if (this.webSAT.Document != null) {
                    #region Selecciona el RdoFechas con un Click
                    HtmlElement htmlElement = this.webSAT.Document.GetElementById("ctl00_MainContent_RdoFechas");
                    if (htmlElement != null) {
                        htmlElement.InvokeMember("click");
                    }
                    while (true) {
                        Application.DoEvents();
                        HtmlElement elementById1 = this.webSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
                        flag1 = (elementById1 == null ? true : !(elementById1.GetAttribute("disabled") != "disabled"));
                        if (!flag1) {
                            break;
                        }
                    }
                    #endregion

                    #region Capturamos la FECHA
                    DateTime dateTime = new DateTime(year, month, day);
                    HtmlElement htmlElement1 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_Calendario_text");
                    if (htmlElement1 != null) {
                        htmlElement1.SetAttribute("value", dateTime.ToString("dd/MM/yyyy"));
                    }
                    HtmlElement elementById2 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_BtnFecha2");
                    if (elementById2 != null) {
                        elementById2.InvokeMember("click");
                    }

                    while (true) {
                        Application.DoEvents();
                        elementById = this.webSAT.Document.GetElementById("datepicker");
                        flag2 = (elementById == null ? true : !elementById.Style.Contains("visibility: visible"));
                        if (!flag2) {
                            break;
                        }
                    }
                    #endregion

                    if (VecesQueSeHaLLamadoAEsteMetodo > 1) {
                        #region Set atributo de la Hora Inicial
                        HtmlElement elementById4 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlHora");
                        if (elementById4 != null) {
                            elementById4.SetAttribute("value", UltimaFechaEmision.Hour.ToString());
                        }
                        HtmlElement elementById5 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlMinuto");
                        if (elementById5 != null) {
                            elementById5.SetAttribute("value", UltimaFechaEmision.Minute.ToString());
                        }
                        HtmlElement elementById6 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlSegundo");
                        if (elementById6 != null) {
                            elementById6.SetAttribute("value", UltimaFechaEmision.Second.ToString());
                        }
                        #endregion

                        //if (false) {
                        //    #region Set atributo de la Hora Final
                        //    HtmlElement elementById7 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlHora");
                        //    if (elementById7 != null) {
                        //        elementById7.SetAttribute("value", "14");
                        //    }
                        //    HtmlElement elementById8 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlMinuto");
                        //    if (elementById8 != null) {
                        //        elementById8.SetAttribute("value", "59");
                        //    }
                        //    HtmlElement elementById9 = this.webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlSegundo");
                        //    if (elementById9 != null) {
                        //        elementById9.SetAttribute("value", "59");
                        //    }
                        //    #endregion
                        //}
                    }

                    if (gEstatus.Trim() != "Todos") {
                        #region Set atributo de Estatus
                        HtmlElement elementById10 = this.webSAT.Document.GetElementById("ctl00_MainContent_DdlEstadoComprobante");
                        if (elementById10 != null) {
                            if (gEstatus.Trim() == "Cancelados")
                                elementById10.SetAttribute("value", "0");
                            if (gEstatus.Trim() == "Vigentes")
                                elementById10.SetAttribute("value", "1");
                        }
                        #endregion
                    }


                    IEnumerable<HtmlElement> htmlElements = ElementsByClass(this.webSAT.Document, "dpDayHighlightTD");
                    if (htmlElements != null) {
                        htmlElements.First<HtmlElement>().InvokeMember("click");
                    }
                    while (true) {
                        Application.DoEvents();
                        elementById = this.webSAT.Document.GetElementById("datepicker");
                        flag3 = (elementById == null ? true : elementById.Style.Contains("visibility: visible"));
                        if (!flag3) {
                            break;
                        }
                    }

                    #region Dar CLICK en BUSQUEDA
                    HtmlElement htmlElement2 = this.webSAT.Document.GetElementById("ctl00_MainContent_BtnBusqueda");
                    if (htmlElement2 != null) {
                        htmlElement2.InvokeMember("click");
                    }
                    int num = 0;
                    while (true) {
                        Application.DoEvents();
                        HtmlElement elementById3 = this.webSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
                        if (!(elementById3 == null)) {
                            flag4 = (elementById3.Style != "display: none;" ? true : num <= 100000);
                            if (!flag4) {
                                break;
                            }
                            num++;
                        }
                    }
                    #endregion

                    this.ProcessResult(ref TotalArchivosDescargados, ref UltimaFechaEmision, "Emitidos");
                }
            } catch (Exception exception) {
                this.Message("ERROR EN EL METODO GetSendedDocuments: " + exception.Message);
                MessageBox.Show(exception.Message);
            }
        }

        private void ProcessResult(ref int TotArchivosDescargados, ref DateTime UltimaFechaEmision, String TipoER) {
            DateTime dtFechaEmision;
            string flag_Enviar_o_Recibir_CFDI;
            bool flag;
            bool Recibidos = true;
            if (TipoER == "Emitidos") {
                Recibidos = false;
            }

            try {
                Regex regex = new Regex("'.*?'");
                if (!(this.webSAT.Document == null)) {
                    // se modifico para que busque la tabla especifica

                    //HtmlElementCollection elementTablas = this._wbSat.Document.GetElementsByTagName("table");
                    //foreach (HtmlElement elementTabla in elementTablas)
                    //{

                    // hace la busqueda por una tabla
                    HtmlElement elementTabla = this.webSAT.Document.GetElementById("ctl00_MainContent_tblResult");

                    //Obtiene los Tr principales antes tenia el problema con las subtablas donde esta el boton
                    HtmlElementCollection elementBtnDescargatr = elementTabla.GetElementsByTagName("tbody")[0].Children;

                    //foreach (HtmlElement elementRenglon in elementTabla.GetElementsByTagName("tr"))
                    foreach (HtmlElement elementRenglon in elementBtnDescargatr) {
                        // HtmlElementCollection elementBtnDescarga = elementRenglon.GetElementsByTagName("img").GetElementsByName("BtnDescarga");
                        //ya no es una imagen es un span
                        HtmlElementCollection elementBtnDescarga = elementRenglon.GetElementsByTagName("span").GetElementsByName("BtnDescarga");

                        if (elementBtnDescarga.Count == 0) {
                            #region XML Cancelados
                            IEnumerable<HtmlElement> htmlElements = elementRenglon.GetElementsByTagName("span").Cast<HtmlElement>();
                            IEnumerable<HtmlElement> htmlElements1 = htmlElements.Where<HtmlElement>((HtmlElement span) => {
                                bool innerText = span.InnerText != "";
                                return innerText;
                            });

                            string DatosDelRenglon = htmlElements1.Aggregate<HtmlElement, string>("", (string current, HtmlElement span) => {
                                string str = string.Concat(current, span.InnerText, "|");
                                return str;
                            });


                            char[] chrArray = new char[] { '|' };
                            // 0 UUID 1 RFC Emisor 2 Nombre Emisor 3 RFC Receptor 4 Nombre Receptor 5 Fecha Emision 6 Fecha Timbrado 7 PAC 8 Total 9 Tipo CFDI 10 Estatus 11 Fecha Cancelacion
                            string[] aDatosDelRenglon = DatosDelRenglon.Split(chrArray);
                            try {
                                if (aDatosDelRenglon[10] == "Cancelado") {
                                    String FechaCancelacion = aDatosDelRenglon[11];
                                    String FechaEmision = aDatosDelRenglon[05];


                                    DateTime.TryParse(aDatosDelRenglon[05], out dtFechaEmision);

                                    #region Forma la Estructura del Directorio a Descargar
                                    string[] EstructuraDelDirectorio = new string[] { gRutaDescargaXML.Trim(), gUsuario.ToString().Trim(), null, null, null, null };
                                    string[] strArrays1 = EstructuraDelDirectorio;

                                    flag_Enviar_o_Recibir_CFDI = (Recibidos ? "Received" : "Sended");
                                    strArrays1[2] = flag_Enviar_o_Recibir_CFDI;

                                    int yearEmision = dtFechaEmision.Year;
                                    EstructuraDelDirectorio[3] = yearEmision.ToString("0000");

                                    int MonthEmision = dtFechaEmision.Month;
                                    EstructuraDelDirectorio[4] = MonthEmision.ToString("00");

                                    int DayEmision = dtFechaEmision.Day;
                                    EstructuraDelDirectorio[5] = DayEmision.ToString("00");

                                    string RutaDelDirectorioDescarga = Path.Combine(EstructuraDelDirectorio);
                                    if (!Directory.Exists(RutaDelDirectorioDescarga)) {
                                        Directory.CreateDirectory(RutaDelDirectorioDescarga);
                                    }
                                    #endregion

                                    string ArchivoADescargar = Path.Combine(RutaDelDirectorioDescarga, string.Format("{0}_{1}_{2}.txt", aDatosDelRenglon[1], aDatosDelRenglon[3], aDatosDelRenglon[0] + "_CANCELADO"));
                                    this.DownloadFileCancelado(aDatosDelRenglon, ArchivoADescargar, RutaDelDirectorioDescarga, (Recibidos ? "Recibido" : "Emitido"));

                                    TotArchivosDescargados = TotArchivosDescargados + 1;
                                    UltimaFechaEmision = dtFechaEmision;
                                    //  this.lblStatus.Text = "(" + TotArchivosDescargados.ToString() + " | " + dtFechaEmision.ToString("dd/MM/yyyy HH:mm:ss") + ")";
                                    //utilerias.EscribeArchSal(gRutaDescargaXML.Trim() + "LOG.TXT", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " " + "(" + TotArchivosDescargados.ToString() + " | " + dtFechaEmision.ToString("dd/MM/yyyy HH:mm:ss") + ")");

                                    flag = (!this._supplierStructure ? true : !Recibidos);
                                    if (!flag) {
                                        #region Forma la Estructura del Directorio a Descargar el archivo
                                        EstructuraDelDirectorio = new string[] { gRutaDescargaXML, gUsuario.ToString().Trim(), null, null, null, null, null };
                                        string[] strArrays2 = EstructuraDelDirectorio;

                                        strArrays2[2] = (Recibidos ? "Received" : "Sended");

                                        EstructuraDelDirectorio[3] = aDatosDelRenglon[1];
                                        yearEmision = dtFechaEmision.Year;

                                        EstructuraDelDirectorio[4] = yearEmision.ToString("0000");
                                        yearEmision = dtFechaEmision.Month;

                                        EstructuraDelDirectorio[5] = yearEmision.ToString("00");
                                        yearEmision = dtFechaEmision.Day;

                                        EstructuraDelDirectorio[6] = yearEmision.ToString("00");

                                        RutaDelDirectorioDescarga = Path.Combine(EstructuraDelDirectorio);
                                        if (!Directory.Exists(RutaDelDirectorioDescarga)) {
                                            Directory.CreateDirectory(RutaDelDirectorioDescarga);
                                        }
                                        #endregion
                                        ArchivoADescargar = Path.Combine(RutaDelDirectorioDescarga, string.Format("{0}_{1}_{2}.txt", aDatosDelRenglon[1], aDatosDelRenglon[3], aDatosDelRenglon[0] + "_CANCELADO"));
                                        this.DownloadFileCancelado(aDatosDelRenglon, ArchivoADescargar, RutaDelDirectorioDescarga, (Recibidos ? "Recibido" : "Emitido"));
                                    }
                                }
                            } catch {

                            }
                            #endregion
                        }

                        if (elementBtnDescarga.Count == 1) {
                            #region DESCARGA XML 
                            string urlFileDownload = string.Format("https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", regex.Match(elementBtnDescarga[0].OuterHtml).Value.Replace("'", ""));

                            #region Obtien los Datos del  Renglon de la Tabla HTML de la pagina web
                            IEnumerable<HtmlElement> htmlElements = elementRenglon.GetElementsByTagName("span").Cast<HtmlElement>();
                            IEnumerable<HtmlElement> htmlElements1 = htmlElements.Where<HtmlElement>((HtmlElement span) => {
                                bool innerText = span.InnerText != "";
                                return innerText;
                            });

                            string DatosDelRenglon = htmlElements1.Aggregate<HtmlElement, string>("", (string current, HtmlElement span) => {
                                string str = string.Concat(current, span.InnerText, "|");
                                return str;
                            });

                            #endregion

                            #region Extracta cada uno de los datos del Renglon
                            char[] chrArray = new char[] { '|' };
                            // ANTES --- 0 UUID 1 RFC Emisor 2 Nombre Emisor 3 RFC Receptor 4 Nombre Receptor 5 Fecha Emision 6 Fecha Timbrado  7 PAC  8 Total  9 Tipo CFDI 10 Estatus
                            // ANTES --- 3 UUID 4 RFC Emisor 5 Nombre Emisor 6 RFC Receptor 7 Nombre Receptor 8 Fecha Emision 9 Fecha Timbrado 10 PAC 11 Total 12 Tipo CFDI 14 Estatus

                            // AHORA --- 5 UUID 6 RFC Emisor 7 Nombre Emisor 8 RFC Receptor 9 Nombre Receptor 10 Fecha Emision 11 Fecha Timbrado 
                            //           12 PAC 13 Total 14 Tipo CFDI 15 Estatus de Cancelacion 16 Estatus del CFDI 17 Estatus de Proceso de Cancelación 18 Fecha de Cancelacion (20 elementos)


                            string[] aDatosDelRenglon = DatosDelRenglon.Split(chrArray);

                            int Base = 100;

                            if (aDatosDelRenglon.Length == 20)
                                Base = 0;
                            if (aDatosDelRenglon.Length == 19)
                                Base = 1;
                            if (aDatosDelRenglon.Length == 18)
                                Base = 2;
                            if (aDatosDelRenglon.Length == 17)
                                Base = 2;

                            if (Base == 100) {
                                this.Message("ERROR No se puede descargar Longitud del arreglo es de : " + aDatosDelRenglon.Length.ToString());
                                this.Message("ERROR No se puede descargar Longitud del arreglo es de : " + aDatosDelRenglon.Length.ToString());
                                return;
                            }

                            String UUID = aDatosDelRenglon[5 - Base].Trim();
                            String eRFC = aDatosDelRenglon[6 - Base].Trim();
                            String eNombre = aDatosDelRenglon[7 - Base].Trim();
                            String rRFC = aDatosDelRenglon[8 - Base].Trim();
                            String rNombre = aDatosDelRenglon[9 - Base].Trim();
                            String Fecha_Emision = aDatosDelRenglon[10 - Base].Trim();
                            String Fecha_Timbrado = aDatosDelRenglon[11 - Base].Trim();
                            String pRFC = aDatosDelRenglon[12 - Base].Trim();
                            String Total = aDatosDelRenglon[13 - Base].Trim();
                            String TipoCFDI = aDatosDelRenglon[14 - Base].Trim();
                            String Estatus = aDatosDelRenglon[16 - Base].Trim();
                            String Fecha_Cancelacion = aDatosDelRenglon[18 - Base].Trim();

                            if (aDatosDelRenglon.Length == 17)
                                Estatus = aDatosDelRenglon[13].Trim();

                            #endregion


                            DateTime.TryParse(Fecha_Emision, out dtFechaEmision);

                            #region Forma la Estructura del Directorio a Descargar
                            string[] EstructuraDelDirectorio = new string[] { gRutaDescargaXML.Trim(), gUsuario.ToString().Trim(), null, null, null, null };
                            string[] strArrays1 = EstructuraDelDirectorio;

                            flag_Enviar_o_Recibir_CFDI = (Recibidos ? "Received" : "Sended");
                            strArrays1[2] = flag_Enviar_o_Recibir_CFDI;

                            int yearEmision = dtFechaEmision.Year;
                            EstructuraDelDirectorio[3] = yearEmision.ToString("0000");

                            int MonthEmision = dtFechaEmision.Month;
                            EstructuraDelDirectorio[4] = MonthEmision.ToString("00");

                            int DayEmision = dtFechaEmision.Day;
                            EstructuraDelDirectorio[5] = DayEmision.ToString("00");

                            string RutaDelDirectorioDescarga = Path.Combine(EstructuraDelDirectorio);
                            if (!Directory.Exists(RutaDelDirectorioDescarga)) {
                                Directory.CreateDirectory(RutaDelDirectorioDescarga);
                            }
                            #endregion

                            string ArchivoADescargar = "";
                            String xmlFile = "";
                            if (Estatus != "Cancelado") {
                                #region VIGENTE
                                ArchivoADescargar = Path.Combine(RutaDelDirectorioDescarga, string.Format("{0}_{1}_{2}.xml", eRFC, rRFC, UUID));
                                xmlFile = this.DownloadFile(urlFileDownload, ArchivoADescargar, RutaDelDirectorioDescarga, aDatosDelRenglon, (Recibidos ? "Recibido" : "Emitido"));

                                flag = (!this._supplierStructure ? true : !Recibidos);
                                if (!flag) {
                                    #region Forma la Estructura del Directorio a Descargar el archivo
                                    EstructuraDelDirectorio = new string[] { gRutaDescargaXML, gUsuario.ToString().Trim(), null, null, null, null, null };
                                    string[] strArrays2 = EstructuraDelDirectorio;

                                    strArrays2[2] = (Recibidos ? "Received" : "Sended");

                                    EstructuraDelDirectorio[3] = eRFC;
                                    yearEmision = dtFechaEmision.Year;

                                    EstructuraDelDirectorio[4] = yearEmision.ToString("0000");
                                    yearEmision = dtFechaEmision.Month;

                                    EstructuraDelDirectorio[5] = yearEmision.ToString("00");
                                    yearEmision = dtFechaEmision.Day;

                                    EstructuraDelDirectorio[6] = yearEmision.ToString("00");

                                    RutaDelDirectorioDescarga = Path.Combine(EstructuraDelDirectorio);
                                    if (!Directory.Exists(RutaDelDirectorioDescarga)) {
                                        Directory.CreateDirectory(RutaDelDirectorioDescarga);
                                    }
                                    #endregion

                                    ArchivoADescargar = Path.Combine(RutaDelDirectorioDescarga, string.Format("{0}_{1}_{2}.xml", eRFC, rRFC, UUID));
                                    this.DownloadFile(urlFileDownload, ArchivoADescargar, RutaDelDirectorioDescarga, aDatosDelRenglon, (Recibidos ? "Recibido" : "Emitido"));
                                }
                                #endregion

                            } else {
                                #region CANCELADO
                                String FechaCancelacion = "";
                                try {
                                    FechaCancelacion = Fecha_Cancelacion;
                                    if (FechaCancelacion.Length == 0)
                                        FechaCancelacion = Fecha_Emision;
                                } catch {
                                    FechaCancelacion = Fecha_Emision;
                                }

                                String FechaEmision = Fecha_Emision;

                                DateTime.TryParse(Fecha_Emision, out dtFechaEmision);

                                #region Forma la Estructura del Directorio a Descargar
                                EstructuraDelDirectorio = new string[] { gRutaDescargaXML.Trim(), gUsuario.ToString().Trim(), null, null, null, null };
                                strArrays1 = EstructuraDelDirectorio;

                                flag_Enviar_o_Recibir_CFDI = (Recibidos ? "Received" : "Sended");
                                strArrays1[2] = flag_Enviar_o_Recibir_CFDI;

                                yearEmision = dtFechaEmision.Year;
                                EstructuraDelDirectorio[3] = yearEmision.ToString("0000");

                                MonthEmision = dtFechaEmision.Month;
                                EstructuraDelDirectorio[4] = MonthEmision.ToString("00");

                                DayEmision = dtFechaEmision.Day;
                                EstructuraDelDirectorio[5] = DayEmision.ToString("00");

                                RutaDelDirectorioDescarga = Path.Combine(EstructuraDelDirectorio);
                                if (!Directory.Exists(RutaDelDirectorioDescarga)) {
                                    Directory.CreateDirectory(RutaDelDirectorioDescarga);
                                }
                                #endregion

                                ArchivoADescargar = Path.Combine(RutaDelDirectorioDescarga, string.Format("{0}_{1}_{2}.txt", eRFC, rRFC, UUID + "_CANCELADO"));
                                this.DownloadFileCancelado(aDatosDelRenglon, ArchivoADescargar, RutaDelDirectorioDescarga, (Recibidos ? "Recibido" : "Emitido"));
                                flag = (!this._supplierStructure ? true : !Recibidos);
                                if (!flag) {
                                    #region Forma la Estructura del Directorio a Descargar el archivo
                                    EstructuraDelDirectorio = new string[] { gRutaDescargaXML, gUsuario.ToString().Trim(), null, null, null, null, null };
                                    string[] strArrays2 = EstructuraDelDirectorio;

                                    strArrays2[2] = (Recibidos ? "Received" : "Sended");

                                    EstructuraDelDirectorio[3] = eRFC;
                                    yearEmision = dtFechaEmision.Year;

                                    EstructuraDelDirectorio[4] = yearEmision.ToString("0000");
                                    yearEmision = dtFechaEmision.Month;

                                    EstructuraDelDirectorio[5] = yearEmision.ToString("00");
                                    yearEmision = dtFechaEmision.Day;

                                    EstructuraDelDirectorio[6] = yearEmision.ToString("00");

                                    RutaDelDirectorioDescarga = Path.Combine(EstructuraDelDirectorio);
                                    if (!Directory.Exists(RutaDelDirectorioDescarga)) {
                                        Directory.CreateDirectory(RutaDelDirectorioDescarga);
                                    }
                                    #endregion
                                    ArchivoADescargar = Path.Combine(RutaDelDirectorioDescarga, string.Format("{0}_{1}_{2}.txt", eRFC, rRFC, UUID + "_CANCELADO"));
                                    this.DownloadFileCancelado(aDatosDelRenglon, ArchivoADescargar, RutaDelDirectorioDescarga, (Recibidos ? "Recibido" : "Emitido"));
                                }

                                TotArchivosDescargados = TotArchivosDescargados + 1;
                                UltimaFechaEmision = dtFechaEmision;
                                //  this.lblStatus.Text = "(" + TotArchivosDescargados.ToString() + " | " + dtFechaEmision.ToString("dd/MM/yyyy HH:mm:ss") + ")";
                                //utilerias.EscribeArchSal(gRutaDescargaXML.Trim() + "LOG.TXT", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " " + "(" + TotArchivosDescargados.ToString() + " | " + dtFechaEmision.ToString("dd/MM/yyyy HH:mm:ss") + ")");

                                #endregion

                            }

                            TotArchivosDescargados = TotArchivosDescargados + 1;
                            UltimaFechaEmision = dtFechaEmision;
                            #endregion
                        }

                        if (TotArchivosDescargados >= MaximoDeDescargasSAT)
                            return;
                    }
                    //}
                } else {
                    return;
                }
            } catch (Exception ex) {
                this.Message(string.Format(Properties.Resources.Message_Error_Procesando, ex.Message));
                throw new Exception(ex.Message);
            }
        }

        private string DownloadFile(string address, string fileName, string RutaXML, string[] Datos, string Sentido) {
            Intentos = 0;
            try {
                string cookie = CookieReaderService.GetCookie(this.webSAT.Url.ToString());
                WebClient webClient = new WebClient();

                webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                webClient.DownloadFile(address, fileName);

                string UUID = Datos[3].Trim();
                string eRFC = Datos[4].Trim();
                string eNombre = Datos[5].Trim();
                string rRFC = Datos[6].Trim();
                string rNombre = Datos[7].Trim();
                string Fecha_Emision = Datos[8].Trim();
                string Fecha_Timbrado = Datos[9].Trim();
                string pRFC = Datos[10].Trim();
                string TipoCFDI = Datos[12].Trim();
                string Estatus = Datos[14].Trim();

                string NewFileName = RutaXML + @"\" + TipoCFDI + "_" + eRFC + "_" + rRFC + "_" + UUID + ".xml";

                if (File.Exists(NewFileName))
                    File.Delete(NewFileName);

                File.Copy(fileName, NewFileName);
                File.Delete(fileName);

                this.Message(string.Format("Creando el archivo {0}.\n", NewFileName));

                // }

                return NewFileName;
            } catch (Exception exception) {
                this.Message("ERROR AL TRATAR DE DESCARGAR EL XML: " + exception.Message);
                return "ERROR " + exception.Message;
            }

        }

        private void DownloadFileCancelado(string[] Datos, string fileName, String RutaXML, String Sentido) {
            Intentos = 0;

            try {

                if (File.Exists(fileName)) {
                    this.Message(string.Format("El archivo {0} ya existe.\n", fileName));
                } else {

                    string TipoCFDI = Datos[12].Trim().Substring(0, 1).ToUpper();
                    string eRFC = Datos[4].Trim(); // eRFC
                    string rRFC = Datos[6].Trim(); // rRFC
                    string UUID = Datos[3].Trim(); // rRFC
                    string Total = Datos[11].Trim();

                    string FechaCancelacion = "";
                    if (Datos[16].Trim().Length == 0)
                        FechaCancelacion = Datos[08].Trim();
                    else
                        FechaCancelacion = Datos[16].Trim();

                    Total = Total.Replace("$", "");
                    Total = Total.Replace(",", "");


                    string newFileName = RutaXML + @"\" + TipoCFDI + "_" + eRFC + "_" + rRFC + "_" + UUID + ".txt";

                    if (File.Exists(newFileName))
                        File.Delete(newFileName);

                    // 0 UUID 1 RFC Emisor 2 Nombre Emisor 3 RFC Receptor 4 Nombre Receptor 5 Fecha Emision 6 Fecha Timbrado 7 PAC 8 Total 9 Tipo CFDI 10 Estatus 11 Fecha Cancelacion
                    EscribeArchSal(newFileName, "UUID....................." + Datos[3].Trim());
                    EscribeArchSal(newFileName, "RFC del Emisor..........." + Datos[4].Trim());
                    EscribeArchSal(newFileName, "Nombre del Emisor........" + Datos[5].Trim());
                    EscribeArchSal(newFileName, "RFC del Receptor........." + Datos[6].Trim());
                    EscribeArchSal(newFileName, "Nombre del Receptor......" + Datos[7].Trim());
                    EscribeArchSal(newFileName, "Fecha de Emision ........" + Datos[8].Trim());
                    EscribeArchSal(newFileName, "Fecha de Timbrado........" + Datos[9].Trim());
                    EscribeArchSal(newFileName, "Fecha de Cancelación....." + FechaCancelacion);
                    EscribeArchSal(newFileName, "PAC......................" + Datos[10].Trim());
                    EscribeArchSal(newFileName, "Total del CFDI..........." + Datos[11].Trim());
                    EscribeArchSal(newFileName, "Tipo de CFDI............." + Datos[12].Trim());
                    EscribeArchSal(newFileName, "Estatus del CFDI........." + Datos[14].Trim());

                    this.Message(string.Format("Creando el archivo {0}.\n", fileName));
                }
            } catch (Exception exception) {
                this.Message("ERROR AL TRATAR DE DESCARGAR EL XML CANCELADO: " + exception.Message);
                throw new Exception(exception.Message);
            }
        }

        private static IEnumerable<HtmlElement> ElementsByClass(HtmlDocument doc, string className) {
            IEnumerable<HtmlElement> htmlElements = doc.All.Cast<HtmlElement>().Where<HtmlElement>((HtmlElement e) => {
                bool attribute = e.GetAttribute("className") == className;
                return attribute;
            });
            return htmlElements;
        }

        private void FinishSession() {
            try {
                this.Message(string.Format(Properties.Resources.Message_CerrarSession, gUsuario.Trim()));
                this.webSAT.Navigate(new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/logout.aspx?salir=y"));
                this.Message("Proceso terminado.");
                SeProcesoWB_SAT = true;

            } catch (Exception exception) {
                this.Message("ERROR AL TRATAR DE TERMINAR LA SECION: " + exception.Message);
                throw new Exception(exception.Message);
            }
        }

        private void Message(string message) {
            try {
                //this.lblStatus.Text = message;
                gtxtLog.AppendText(string.Concat(message, "\n\r\n\r"));
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        public void EscribeArchSal(string ArchSal, string Mensaje) {
            try {
                FileStream Archivo = new FileStream(ArchSal, FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(Archivo);
                sw.WriteLine(Mensaje);
                sw.Close();
                Archivo.Close();
            } catch (Exception err) {
                string Error = err.Message;
            }
        }
    }
}
