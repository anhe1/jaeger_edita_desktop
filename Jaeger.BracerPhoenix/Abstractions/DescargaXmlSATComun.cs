﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using HtmlAgilityPack;
using Jaeger.Util.Services;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.Repositorio.Services;
using Jaeger.Repositorio.Contracts;

namespace Jaeger.Repositorio.Abstractions {
    public abstract class DescargaXmlSATComun : BasePropertyChangeImplementation {
        private ConfiguracionDetail _Configuracion;
        private List<ComprobanteFiscalResponse> _DescargaResultados;
        private List<DescargaFechas500> listFechasEmi500 = new List<DescargaFechas500>();
        private List<DescargaFechas500> listFechasRec500 = new List<DescargaFechas500>();
        private int _MaximoDescargaSAT = 495;
        private bool _DescargaParalela;

        public WebBrowser _WebSAT;
        public TextBox LbMensaje = null;
        public TextBox LbError = null;
        public bool ModoDescarga = false;
        public string ErrorLog;
        public bool Truco = false;
        public bool TrucoHide = false;
        public bool Cerrando = false;
        public int IndexLapsoEmitido = 0;
        public int IndexDiaEmitido = 0;
        public int IndexLapsoRecibido = 0;
        public int IndexDiaRecibido = 0;

        public Timer TimerLogin = new Timer() { Interval = 1000, Enabled = false };
        public Timer TimerClose = new Timer() { Interval = 1000, Enabled = false };
        public Timer TimerError = new Timer() { Interval = 2000, Enabled = false };
        public Timer TimerCloseAlert = new Timer() { Interval = 500, Enabled = false };
        public Timer TimerFiltrarRecibidas = new Timer() { Enabled = false, Interval = 500 };
        public Timer TimerFiltrarEmitidas = new Timer() { Enabled = false, Interval = 500 };
        public Timer TimerRecopilarRecibidas = new Timer() { Enabled = false, Interval = 1000 };
        public Timer TimerRecopilarEmitidas = new Timer() { Enabled = false, Interval = 1000 };
        public Timer TimerEvitarCierre = new Timer() { Enabled = false, Interval = 15000 };

        public List<DescargaLapso> ListLapsosEmitidos = new List<DescargaLapso>();
        public List<DescargaLapso> ListLapsosEmitidosNoDescargables = new List<DescargaLapso>();

        public List<DescargaLapso> ListLapsosRecibidos = new List<DescargaLapso>();
        public List<DescargaLapso> ListLapsosRecibidosNoDescargables = new List<DescargaLapso>();

        protected internal string UrlAutentificacion = "https://portalcfdi.facturaelectronica.sat.gob.mx/";
        public string UrlEmitidas = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx";
        public string UrlRecibidas = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx";
        public string UrlOculta = "https://portalcfdi.facturaelectronica.sat.gob.mx/Consulta.aspx?oculta=1";
        public string UrlError1 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATx509Custom&sid=0&option=credential&sid=0";
        public string UrlError2 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=";
        public string UrlRedir = "https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa=";
        public string UrlFailCaptcha = "https://cfdiau.sat.gob.mx/nidp/app/login?sid=0&sid=0";
        public string _Version = "3.0";

        /// <summary>
        /// construnctor
        /// </summary>
        public DescargaXmlSATComun() {
            _DescargaResultados = new List<ComprobanteFiscalResponse>();
        }

        #region propiedades
        public string Version {
            get { return this._Version; }
            protected set { this._Version = value; }
        }

        public ConfiguracionDetail Configuracion {
            get {
                return _Configuracion;
            }
            set {
                _Configuracion = value;
                OnPropertyChanged();
            }
        }

        public List<ComprobanteFiscalResponse> Resultados {
            get {
                return _DescargaResultados;
            }
            set {
                _DescargaResultados = value;
            }
        }

        public List<DescargaFechas500> ListaFechasEmitidos {
            get {
                return listFechasEmi500;
            }
            set {
                listFechasEmi500 = value;
                OnPropertyChanged();
            }
        }

        public List<DescargaFechas500> ListaFechasRecibidos {
            get {
                return listFechasRec500;
            }
            set {
                listFechasRec500 = value;
                OnPropertyChanged();
            }
        }

        public int MaximoDescargaSAT {
            get {
                return _MaximoDescargaSAT;
            }
            set {
                _MaximoDescargaSAT = value;
            }
        }

        public long Total {
            get {
                return _DescargaResultados.Count;
            }
        }

        public bool DescargaParalela {
            get {
                return _DescargaParalela;
            }
            set {
                _DescargaParalela = value;
            }
        }

        public int Cuantos {
            get {
                return _DescargaResultados.Count;
            }
        }

        #endregion

        #region  eventos

        /// <summary>
        /// al iniciar el proceso de consulta
        /// </summary>
        public event EventHandler<DescargaMasivaStartProcess> StartProcess;

        /// <summary>
        /// progreso de la descarga
        /// </summary>
        public event EventHandler<DescargaMasivaProgreso> ProgressChanged;

        /// <summary>
        /// al terminar el proceso 
        /// </summary>
        public event EventHandler<DescargaMasivaCompletedProcess> CompletedProcess;

        public void OnStartProcess(DescargaMasivaStartProcess e) {
            if (StartProcess != null)
                StartProcess(this, e);
        }

        public void OnProgressChanged(DescargaMasivaProgreso e) {
            if (ProgressChanged != null)
                ProgressChanged(this, e);
        }

        public void OnCompletedProcess(DescargaMasivaCompletedProcess e) {
            if (CompletedProcess != null)
                CompletedProcess(this, e);
        }

        #endregion

        #region metodos privados

        /// <summary>
        /// obtener url de descarga
        /// </summary>
        public string GetElementURL(HtmlElement element, string url, string byName) {
            string result = string.Empty;
            HtmlElementCollection elementBtnDescarga = element.GetElementsByTagName("span").GetElementsByName(byName);
            Regex regex = new Regex("'.*?'");
            if (elementBtnDescarga.Count == 1) {
                result = string.Format(url, regex.Match(elementBtnDescarga[0].OuterHtml).Value.Replace("'", ""));
            } else {
            }
            return result;
        }

        public string GetElementURL(HtmlNode element, string url, string byName) {
            string _result = "";
            var _doc = new HtmlAgilityPack.HtmlDocument();
            _doc.LoadHtml(element.OuterHtml);
            try {
                var _elementById = _doc.GetElementbyId(byName);
                if (_elementById != null) {
                    var _file = Regex.Match(_elementById.OuterHtml, "'.*?'").Value.Replace("'", "");
                    _result = string.Format(url, _file);
                }
            } catch (Exception ex) {
                LogErrorService.LogWrite("GetElementURL: \r\n" + ex.StackTrace);
                Console.WriteLine("GetElementURL: " + ex.Message);
            }

            return _result;
        }

        /// <summary>
        /// reporte de progreso de la descarga
        /// </summary>
        public virtual void Reporte_ProgressChanged(object sender, DescargaMasivaProgreso e) {
            LbMensaje.Text = string.Format("{0} {1} de {2} ({3}%)", e.Data, e.Contador, e.Total, e.Completado);
            OnProgressChanged(e);
        }

        private void TimerEvitarCierre_Tick(object sender, EventArgs e) {
            if (ModoDescarga == true) {
                try {
                    GoUrl(UrlOculta);
                    Console.WriteLine("TimerEvitarCierre: Evitando cierre...");
                } catch (Exception ex) {
                    LogErrorService.LogWrite(ex.Message);
                    Console.WriteLine("TimerEvitarCierre: Evitando cierre... error: " + ex.Message);
                }
            }
        }

        #endregion

        #region metodos estaticos
        /// <summary>
        /// crear la estructura del directorio donde se alacenara el archivo descargado
        /// </summary>
        public string CrearEstructuraDirectorioDescarga(IComprobanteFiscal item) {
            return Path.Combine(new string[] {
                _Configuracion.CarpetaDescarga.Trim(),
                _Configuracion.RFC.ToString().Trim(),
                _Configuracion.TipoText,
                item.FechaEmision.Year.ToString("0000"),
                item.FechaEmision.Month.ToString("00"),
                item.FechaEmision.Day.ToString("00") 
            });
        }

        public static bool GetLimiteDescargaSAT(string content) {
            bool flag = false;
            if (content.ToLower().Contains("ya no puedes descargar")) {
                flag = true;
            }
            return flag;
        }

        public static bool GetValidXml(string content) {
            bool flag = false;
            try {
                if (content.Contains("cfdi") | content.Contains("retenciones")) {
                    return true;
                }
            } catch (Exception ex) {
                Console.WriteLine("GetValidXML: " + ex.Message);
            }
            return flag;
        }

        [DllImport("user32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        #endregion

        #region metodos publicos

        /// <summary>
        /// pantalla del selector de consulta de emitidas o recibidas
        /// </summary>
        public void ClickEnSelector(DescargaTipoEnum tipo) {
            IEnumerator enumerator = null;
            string str = "";

            if (tipo == DescargaTipoEnum.Emitidos) {
                str = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx";
            } else if (tipo == DescargaTipoEnum.Recibidos) {
                str = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx";
            }
            try {
                try {
                    enumerator = _WebSAT.Document.Links.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        HtmlElement current = (HtmlElement)enumerator.Current;
                        if (current.GetAttribute("href") != str) {
                            continue;
                        }
                        current.InvokeMember("click");
                        break;
                    }
                } finally {
                    if (enumerator is IDisposable) {
                        (enumerator as IDisposable).Dispose();
                    }
                }
            } catch (Exception ex) {
                LogErrorService.LogWrite("AuthenticateComun: \r\n" + ex.StackTrace);
                Console.WriteLine("ClickSelector: " + ex.Message);
            }
        }

        public void LlenarLista() {
            DateTime dateTime = Configuracion.FechaInicial.AddDays(-1);
            DateTime f2 = Configuracion.FechaFinal;
            f2 = f2.AddDays(1);

            while (dateTime < f2) {
                dateTime = dateTime.AddDays(1);
                DescargaFechas500 fecha500 = new DescargaFechas500() {
                    Fecha = dateTime
                };

                if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                    ListaFechasRecibidos.Add(fecha500);
                }
                if (Configuracion.Tipo != DescargaTipoEnum.Emitidos) {
                    continue;
                }
                ListaFechasEmitidos.Add(fecha500);
            }
        }

        public void GoUrl(string url) {
            Application.DoEvents();
            try {
                _WebSAT.Navigate(url, "_self", null, "User-Agent: Luke's Web Browser");
            } catch (Exception exception1) {
                Console.WriteLine(exception1.Message);
                Exception exception = exception1;
                MessageBox.Show("GoURL: " + exception.Message, "Navigate Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public HtmlElement GetElementHtmlById(string id, ref WebBrowser webBrowser1) {
            HtmlElement elementById = null;
            try {
                elementById = webBrowser1.Document.GetElementById(id);
            } catch (Exception exception) {
                Console.WriteLine("GetElementHtmlById: " + exception.Message);
            }
            return elementById;
        }

        public virtual void AuthenticateC() {
            try {
                //this.TimerCloseAlert.Enabled = true;
                LbMensaje.Text = string.Format("Iniciando sesion del usuario {0}.", Configuracion.RFC.Trim().ToUpper());

                string postData = string.Format("&Ecom_User_ID={0}&Ecom_Password={1}&userCaptcha={2}&submit=Enviar", Configuracion.RFC, Configuracion.CIEC, Configuracion.Captcha);
                LbMensaje.Text = "Datos a posterar en la url  " + postData;

                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                byte[] bytes = encoding.GetBytes(postData);

                LbMensaje.Text = "Navegando a la url " + _WebSAT.Url.ToString();
                _WebSAT.Navigate(_WebSAT.Url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");

                #region ESPERA A QUE SE CARGUE LA PAGINA WEB Y SE PROCESE EL METODOD wbSAT_DocumentCompleted

                bool seProcesoWBSat = false;

                int intentos = 0;
                while (true) {
                    Application.DoEvents();
                    if (!_WebSAT.IsBusy) {
                        if (seProcesoWBSat) {
                            break;
                        } else {
                            if (this._WebSAT.Document != null && this._WebSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/app/login?id=SATUPCFDiCon&sid=0&option=credential&sid=0")) {
                                if (this._WebSAT.Document.Body.InnerText.Contains("Captcha no v")) {
                                    LbError.Text = "Captcha no válido ";
                                    break;
                                }
                            }
                        }

                        if (intentos > 100000) {
                            LbError.Text = "CONTRASEÑA CIEC ..... ";
                            LbMensaje.Text = "AVISO IMPORTANTE: CONTRAEÑA CIEC INCORRECTA  " + intentos.ToString();
                            break;
                        }
                        intentos++;
                    }
                }
                #endregion
            } catch (Exception ex) {
                Services.LogErrorService.LogWrite("AuthenticateComun: \r\n" + ex.StackTrace);
                Console.WriteLine("AuthenticateComun: " + ex.Message);
            }
        }

        public bool EstaEnSelector() {
            bool flag = false;
            if (new Regex("Seleccione la opción deseada").Matches(_WebSAT.Document.Body.InnerHtml).Count > 0) {
                flag = true;
            }
            return flag;
        }

        public bool ErrroDeAutentificacion() {
            bool flag = false;
            try {
                Regex regex = new Regex("Identity Provider");
                string innerHtml = "";
                if (_WebSAT.Document.Body != null) {
                    if (_WebSAT.Document.Body.InnerHtml != null) {
                        innerHtml = _WebSAT.Document.Body.InnerHtml;
                    }
                    if (regex.Matches(innerHtml).Count > 0) {
                        flag = true;
                    }
                }
            } catch (Exception exception) {
                LogErrorService.LogWrite(exception.StackTrace);
                if (exception.Message.Contains("No se puede obtener acceso al objeto desechado")) {
                    this.TimerError.Stop();
                }
                Console.WriteLine("ErrorAutenticacion: " + exception.Message);
            }
            return flag;
        }

        public bool ErrroDeCredenciales() {
            bool flag = false;
            try {
                if (new Regex("Unable to complete request at this time").Matches(_WebSAT.Document.Body.InnerHtml).Count > 0) {
                    flag = true;
                }
            } catch (Exception exception) {
                LogErrorService.LogWrite(exception.StackTrace);
                Console.WriteLine("ErrorCredenciales: " + exception.Message);
            }
            return flag;
        }

        public bool ExisteUpdateProgres() {
            bool flag = false;
            HtmlElement elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
            if (elementById != null) {
                flag = elementById.Style != "display: none;" ? true : false;
            }
            return flag;
        }

        public int GetCuantosXML() {
            int num = 0;
            var elementsByTagName = _WebSAT.Document.GetElementsByTagName("table");
            try {
                foreach (HtmlElement current in elementsByTagName) {
                    foreach (HtmlElement item in current.GetElementsByTagName("tr")) {
                        if (item.GetElementsByTagName("span").GetElementsByName("BtnDescarga").Count != 1) {
                            continue;
                        }
                        num++;
                    }
                }
            } finally {

            }
            return num;
        }

        public int GetCuantosXML1() {
            IEnumerator enumerator = null;
            IEnumerator enumerator1 = null;
            int num = 0;
            var elementsByTagName = _WebSAT.Document.GetElementsByTagName("table");
            try {
                enumerator = elementsByTagName.GetEnumerator();
                while (enumerator.MoveNext()) {
                    var current = (HtmlElement)enumerator.Current;
                    try {
                        enumerator1 = current.GetElementsByTagName("tr").GetEnumerator();
                        while (enumerator1.MoveNext()) {
                            if (((HtmlElement)enumerator1.Current).GetElementsByTagName("span").GetElementsByName("BtnDescarga").Count != 1) {
                                continue;
                            }
                            num = checked(num + 1);
                        }
                    } finally {
                        if (enumerator1 is IDisposable) {
                            (enumerator1 as IDisposable).Dispose();
                        }
                    }
                }
            } finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            return num;
        }

        public int GetCuantosXML2() {
            int num = 0;
            string innerHtml = "";
            try {
                innerHtml = GetElementHtmlById("ctl00_MainContent_UpnlResultados", ref _WebSAT).InnerHtml;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                innerHtml = "";
                num = 0;
            }

            if (innerHtml != "") {
                Regex regx = new Regex("'.*?'");
                try {
                    num = regx.Matches(innerHtml).Count;
                } catch (Exception ex) {
                    num = 0;
                    LogErrorService.LogWrite(ex.StackTrace);
                    Console.WriteLine("GetCuantosXML: " + ex.Message);
                }
            }

            return num;
        }

        public bool GetLimiteDe500() {
            bool flag = false;
            try {
                HtmlElement elementHtmlById = GetElementHtmlById("ctl00_MainContent_PnlLimiteRegistros", ref _WebSAT);
                if (elementHtmlById == null) {
                    flag = false;
                } else {
                    //string innerHtml = elementHtmlById.InnerHtml;
                    flag = true;
                }
            } catch (Exception exception) {
                LogErrorService.LogWrite(exception.StackTrace);
                Console.WriteLine("GetLimite500: " + exception.Message);
                flag = false;
            }
            return flag;
        }

        public bool GetNavegadorCerrado() {
            bool flag = false;
            Regex regex = new Regex("cerrar completamente su navegador.");
            try {
                if (_WebSAT.Document.Body == null)
                    return true;

                if (regex.Matches(_WebSAT.Document.Body.InnerHtml).Count > 0) {
                    flag = true;
                }
            } catch (Exception ex) {
                Console.WriteLine("GeNavegadirCerrado: " + ex.Message);
                flag = false;
            }
            return flag;
        }

        public string[] GetTiempoInteger(int tiempo) {
            long segundos = 3600;
            long minutos = 60;
            long num2 = checked((int)Math.Round(Math.Truncate(tiempo / (double)segundos)));
            long num3 = checked(tiempo - checked(num2 * segundos));
            long num4 = checked((int)Math.Round(Math.Truncate(num3 / (double)minutos)));
            long num5 = checked(num3 - checked(num4 * minutos));
            string[] devuelto = { null, num2.ToString(), num4.ToString(), num5.ToString() };
            return devuelto;
        }

        public bool UrlLoad(string temp) {
            bool flag = false;
            if (_WebSAT.Url.ToString().Contains(temp)) {
                flag = true;
            }
            return flag;
        }

        public bool UrlErroLogin(string url) {
            bool flag = false;
            if (!(url.Contains("SATx509Custom") & url != UrlError1)) {
                flag = false;
            } else {
                url = url.Substring(0, 62);
                int integer = 0;
                integer = Convert.ToInt32(url.Split('=')[2]);

                if (integer >= 1) {
                    flag = true;
                }
            }
            return flag;
        }

        public bool CredencialesNoValidas() {
            var flag = false;
            var regex = new Regex("El RFC o contrase");
            long count = 0;
            try {
                string innerHtml = _WebSAT.Document.Body.InnerHtml;
                count = regex.Matches(innerHtml).Count;
            } catch (Exception exception) {
                Console.WriteLine("CredencialesNoValidas: " + exception.Message);
                count = 0;
            }
            if (count > 0) {
                flag = true;
            }
            return flag;
        }

        // es probable desaparecer porque solo se usa para la ventana
        public void LoginScrapSAT() {
            if (this._WebSAT.ReadyState == WebBrowserReadyState.Complete & (UrlLoad(UrlError2) | _WebSAT.Url.ToString() == UrlFailCaptcha)) {
                try {
                    _WebSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", Configuracion.RFC);
                    _WebSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", Configuracion.CIEC);
                } catch (Exception ex) {
                    Console.WriteLine("LoginScraptSAT: " + ex.Message);
                }
            }
        }

        public static bool WriteFileFromB64(string base64, string directorio, string archivo) {
            bool result = false;
            byte[] numArray;

            if (base64 == null ? true : base64.Length == 0) {
                numArray = null;
            } else {
                try {
                    numArray = Convert.FromBase64String(base64);
                } catch (FormatException fex) {
                    LogErrorService.LogWrite(fex.StackTrace);
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64, Environment.NewLine), fex);
                }
            }

            if (numArray != null) {
                if (!Directory.Exists(directorio)) {
                    Directory.CreateDirectory(directorio);
                }

                try {
                    using (FileStream fileStream = File.Create(Path.Combine(directorio, archivo))) {
                        fileStream.Write(numArray, 0, checked(numArray.Length));
                        fileStream.Close();
                        result = true;
                    }
                } catch (Exception ex) {
                    LogErrorService.LogWrite(ex.StackTrace);
                    Console.WriteLine("WriteFileFromB64: " + ex.Message);
                }
            }

            return result;
        }

        public bool HayDiasPorRevisar(List<DescargaFechas500> objetos) {
            List<DescargaFechas500>.Enumerator enumerator = new List<DescargaFechas500>.Enumerator();
            bool flag = false;
            try {
                enumerator = objetos.GetEnumerator();
                while (enumerator.MoveNext()) {
                    if (!(enumerator.Current.Status == DescargaLapsoStatusEnum.Pendiente)) {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
            } finally {
                ((IDisposable)enumerator).Dispose();
            }
            return flag;
        }

        public bool HayRangosPorRevisar(List<DescargaLapso> objetos) {
            List<DescargaLapso>.Enumerator enumerator = new List<DescargaLapso>.Enumerator();
            bool flag = false;
            try {
                enumerator = objetos.GetEnumerator();
                while (enumerator.MoveNext()) {
                    if (!(enumerator.Current.Status == DescargaLapsoStatusEnum.Pendiente)) {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
            } finally {
                ((IDisposable)enumerator).Dispose();
            }
            return flag;
        }

        public void CrearLapsosEmitidos(int i) {
            if (ListLapsosEmitidos[i].Status == DescargaLapsoStatusEnum.X) {
                if (ListLapsosEmitidos[i].SegundoFinal > ListLapsosEmitidos[i].SegundoInicial) {
                    LbMensaje.Text = "Dividiendo Lapsos ...";
                    DescargaLapso lapso = new DescargaLapso();
                    DescargaLapso objectValue = new DescargaLapso();
                    int integer = 0;
                    int num = 0;
                    long num1 = checked((long)Math.Truncate((double)checked(ListLapsosEmitidos[i].SegundoFinal - ListLapsosEmitidos[i].SegundoInicial) / 2));
                    integer = ListLapsosEmitidos[i].SegundoInicial + (int)num1;
                    num = checked(integer + 1);
                    lapso.Fecha = ListLapsosEmitidos[i].Fecha;
                    lapso.SegundoInicial = ListLapsosEmitidos[i].SegundoInicial;
                    lapso.SegundoFinal = integer;
                    lapso.Status = DescargaLapsoStatusEnum.Pendiente;
                    objectValue.Fecha = ListLapsosEmitidos[i].Fecha;
                    objectValue.SegundoInicial = num;
                    objectValue.SegundoFinal = ListLapsosEmitidos[i].SegundoFinal;
                    objectValue.Status = DescargaLapsoStatusEnum.Pendiente;
                    ListLapsosEmitidos.Add(lapso);
                    ListLapsosEmitidos.Add(objectValue);
                    IndexLapsoEmitido = i;
                    return;
                }
                ListLapsosEmitidos[i].Status = DescargaLapsoStatusEnum.B;
                ListLapsosEmitidosNoDescargables.Add(ListLapsosEmitidos[i]);
            }
        }

        public void CrearLapsosRecibidos(int i) {
            if (ListLapsosRecibidos[i].Status == DescargaLapsoStatusEnum.X) {
                if (ListLapsosRecibidos[i].SegundoFinal > ListLapsosRecibidos[i].SegundoInicial) {
                    LbMensaje.Text = "Dividiendo Lapsos ...";
                    DescargaLapso lapso = new DescargaLapso();
                    DescargaLapso lapso2 = new DescargaLapso();
                    int integer = 0;
                    int num = 0;
                    long num1 = checked((long)Math.Round((double)checked(ListLapsosRecibidos[i].SegundoFinal - ListLapsosRecibidos[i].SegundoInicial) / 2));
                    integer = ListLapsosRecibidos[i].SegundoInicial + (int)num1;
                    num = checked(integer + 1);
                    lapso.Fecha = ListLapsosRecibidos[i].Fecha;
                    lapso.SegundoInicial = ListLapsosRecibidos[i].SegundoInicial;
                    lapso.SegundoFinal = integer;
                    lapso.Status = DescargaLapsoStatusEnum.Pendiente;
                    lapso2.Fecha = ListLapsosRecibidos[i].Fecha;
                    lapso2.SegundoInicial = num;
                    lapso2.SegundoFinal = ListLapsosRecibidos[i].SegundoFinal;
                    lapso2.Status = DescargaLapsoStatusEnum.Pendiente;
                    ListLapsosRecibidos.Add(lapso);
                    ListLapsosRecibidos.Add(lapso2);
                    IndexLapsoRecibido = i;
                    Console.WriteLine("Lapso1 -> " + lapso.ToString());
                    Console.WriteLine("Lapso2 -> " + lapso2.ToString());
                    return;
                }
                ListLapsosRecibidos[i].Status = DescargaLapsoStatusEnum.B;
                ListLapsosRecibidosNoDescargables.Add(ListLapsosRecibidos[i]);
            }
        }

        public void IterarDiasEmitidos() {
            if (ListaFechasEmitidos[IndexDiaEmitido].Status == DescargaLapsoStatusEnum.Pendiente) {
                ListaFechasEmitidos[IndexDiaEmitido].Status = DescargaLapsoStatusEnum.Ok;
                ListLapsosEmitidos.Clear();
                IndexLapsoEmitido = 0;
                DescargaLapso lapso = new DescargaLapso() {
                    Fecha = Convert.ToDateTime(ListaFechasEmitidos[IndexDiaEmitido].Fecha),
                    SegundoInicial = 0,
                    SegundoFinal = 86399,
                    Status = DescargaLapsoStatusEnum.Pendiente
                };
                ListLapsosEmitidos.Add(lapso);
            }
        }

        public void IterarDiasRecibidos() {
            if (ListaFechasRecibidos[IndexDiaRecibido].Status == DescargaLapsoStatusEnum.Pendiente) {
                ListaFechasRecibidos[IndexDiaRecibido].Status = DescargaLapsoStatusEnum.Ok;
                ListLapsosRecibidos.Clear();
                IndexLapsoRecibido = 0;
                DescargaLapso lapso = new DescargaLapso() {
                    Fecha = Convert.ToDateTime(ListaFechasRecibidos[IndexDiaRecibido].Fecha),
                    SegundoInicial = 0,
                    SegundoFinal = 86399,
                    Status = DescargaLapsoStatusEnum.Pendiente
                };
                ListLapsosRecibidos.Add(lapso);
            }
        }

        public virtual void ProcessResult(ref int totArchivosDescargados, ref DateTime ultimaFechaEmision, ref WebBrowser webSAT) {
            var temporal = new TableResultHtmlBuilder().WithHtml(webSAT.Document.Body.OuterHtml);
            temporal.Build();
            for (int i = 0; i < temporal.Registros.Count; i++) {
                temporal.Registros[i].Tipo = _Configuracion.TipoText;
                temporal.Registros[i].PathFull = CrearEstructuraDirectorioDescarga(temporal.Registros[i] as ComprobanteFiscalResponse);
                this._DescargaResultados.Add(temporal.Registros[i] as ComprobanteFiscalResponse);
            }
            totArchivosDescargados = temporal.Registros.Count;
            return;

            //try {
            //    if (!(webSAT.Document == null)) {
            //        // hace la busqueda por una tabla
            //        HtmlElement elementTabla = webSAT.Document.GetElementById("ctl00_MainContent_tblResult");

            //        //Obtiene los Tr principales antes tenia el problema con las subtablas donde esta el boton
            //        HtmlElementCollection elementBtnDescargatr = elementTabla.GetElementsByTagName("tbody")[0].Children;

            //        foreach (HtmlElement elementRenglon in elementBtnDescargatr) {
            //            //ya no es una imagen es un span
            //            string urlFileDownload = GetElementURL(elementRenglon, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnDescarga");
            //            string urlPdfDownload = GetElementURL(elementRenglon, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnRI");
            //            string urlRecuperaAcuseFinal = GetElementURL(elementRenglon, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnRecuperaAcuse");

            //            IEnumerable<HtmlElement> htmlElementos = elementRenglon.GetElementsByTagName("span").Cast<HtmlElement>();
            //            IEnumerable<HtmlElement> htmlElementos1 = htmlElementos.Where((span) => {
            //                bool innerText = span.InnerText != "";
            //                return innerText;
            //            });

            //            string datosDelRenglonTest = htmlElementos1.Aggregate("", (current, span) => {
            //                string str = string.Concat(current, span.InnerText, "|");
            //                return str;
            //            });
            //            File.WriteAllText(@"C:\Jaeger\Jaeger.Log\DescargaTabla_" + _Configuracion.TipoText + ".log", datosDelRenglonTest + "\r\n");
            //            // sino son los encabezados
            //            if (!datosDelRenglonTest.StartsWith("  Acciones")) {
            //                char[] chrArray = new char[] { '|' };
            //                string[] aDatosDelRenglon = datosDelRenglonTest.Split(chrArray);
            //                // reparamos las posiciones para obtener la tabla correcta
            //                if (aDatosDelRenglon.Length == 16) {
            //                    datosDelRenglonTest = "||" + datosDelRenglonTest.Replace('$', ' ');
            //                } else if (aDatosDelRenglon.Length == 15) {
            //                    datosDelRenglonTest = "|||" + datosDelRenglonTest.Replace('$', ' ');
            //                } else if (aDatosDelRenglon.Length == 14) {
            //                    datosDelRenglonTest = "||||" + datosDelRenglonTest.Replace('$', ' ');
            //                } else if (aDatosDelRenglon.Length == 18) {

            //                } else {
            //                    File.WriteAllText(@"C:\Jaeger\Jaeger.Log\DescargaTablaNoIdent_" + _Configuracion.TipoText + ".log", DateTime.Now.ToString() + "+" + datosDelRenglonTest + "\r\n");
            //                }

            //                aDatosDelRenglon = datosDelRenglonTest.Split(chrArray);
            //                ComprobanteFiscalResponse vigente = new ComprobanteFiscalResponse();
            //                vigente.Tipo = _Configuracion.TipoText;
            //                vigente.FolioFiscal = aDatosDelRenglon[3]; // 3 UUID
            //                vigente.EmisorRFC = aDatosDelRenglon[4]; // 4 RFC Emisor 
            //                vigente.Emisor = aDatosDelRenglon[5]; // 5 Nombre Emisor
            //                vigente.ReceptorRFC = aDatosDelRenglon[6]; // 6 RFC Receptor
            //                vigente.Receptor = aDatosDelRenglon[7]; // 7 Nombre Receptor
            //                vigente.FechaEmision = ParseConvert.ConvertDateTime(aDatosDelRenglon[8]); // 8 Fecha Emision
            //                vigente.FechaCertifica = ParseConvert.ConvertDateTime(aDatosDelRenglon[9]); // 9 Fecha Timbrado
            //                vigente.PacCertifica = aDatosDelRenglon[10]; // 10 PAC 11 
            //                vigente.Total = ParseConvert.ConvertDecimal(aDatosDelRenglon[11].Replace('$', ' ')); // 11 Total
            //                vigente.Efecto = aDatosDelRenglon[12]; // 12 Tipo CFDI
            //                vigente.EstatusCancelacion = aDatosDelRenglon[13];
            //                vigente.EstadoComprobante = aDatosDelRenglon[14]; // 14 Estatus
            //                vigente.EstatusProcesoCancelacion = aDatosDelRenglon[15];
            //                vigente.FechaProcesoCancelacion = ParseConvert.ConvertDateTime(aDatosDelRenglon[16]);
            //                vigente.PathXML = CrearEstructuraDirectorioDescarga(vigente);
            //                var accion = new RegistroAccion {
            //                    UrlXML = urlFileDownload,
            //                    UrlPDF = urlPdfDownload,
            //                    UrlAcuseFinal = urlRecuperaAcuseFinal
            //                };
            //                vigente.WithAcciones(accion);
            //                _DescargaResultados.Add(vigente);
            //                totArchivosDescargados = totArchivosDescargados + 1;
            //            }

            //            if (totArchivosDescargados >= MaximoDescargaSAT)
            //                return;
            //        }
            //    } else {
            //        return;
            //    }
            //} catch (Exception ex) {
            //    LogErrorService.LogWrite(ex.StackTrace);
            //    Console.WriteLine("ProcessResult: " + ex.Message);
            //    ErrorLog = "ProcessResult: " + ex.Message;
            //}
        }

        public virtual void ProcessResult(ref int totArchivosDescargados, ref WebBrowser webSAT) {
            var temporal = new TableResultHtmlBuilder().WithHtml(webSAT.Document.Body.OuterHtml);
            temporal.Build();
            for (int i = 0; i < temporal.Registros.Count; i++) {
                temporal.Registros[i].Tipo = _Configuracion.TipoText;
                this._DescargaResultados.Add(temporal.Registros[i] as ComprobanteFiscalResponse);
            }
            totArchivosDescargados = temporal.Registros.Count;
            return;

            //var elementTabla = webSAT.Document.GetElementById("ctl00_MainContent_tblResult");
            //HtmlAgilityPack.HtmlDocument _doc = new HtmlAgilityPack.HtmlDocument();
            //_doc.LoadHtml(elementTabla.OuterHtml);
            //var columnas = (this.Configuracion.Tipo == DescargaTipoEnum.Emitidos ? 18 : 16);
            //try {
            //    int num = -1;
            //    foreach (HtmlNode array in _doc.DocumentNode.Descendants("tr").ToArray()) {
            //        if (array.HasChildNodes && array.ChildNodes != null && array.ChildNodes.Count.Equals(columnas)) {
            //            var vigente = new ComprobanteFiscalResponse();
            //            num = 0;
            //            foreach (HtmlNode htmlNode1 in from x in array.ChildNodes where x.Name == "td" select x) {
            //                if (htmlNode1.Name == "td") {
            //                    switch (num) {
            //                        case 0: {
            //                                var acciones = new RegistroAccion();
            //                                acciones.WithUrlXml(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnDescarga"));
            //                                acciones.WithUrlDetalle(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnVerDetalle"));
            //                                acciones.WithUrlPdf(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnRI"));
            //                                acciones.WithUrlAcuseFinal(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnRecuperaAcuse"));
            //                                vigente.WithAcciones(acciones);
            //                                break;
            //                            }
            //                        case 1: { // folio fiscal
            //                                vigente.FolioFiscal = htmlNode1.InnerText;
            //                                break;
            //                            }
            //                        case 2: { // RFC Emisor
            //                                vigente.EmisorRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
            //                                break;
            //                            }
            //                        case 3: { // Nombre o Razon Social del Emisor
            //                                vigente.Emisor = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
            //                                break;
            //                            }
            //                        case 4: { // RFC Receptor
            //                                vigente.ReceptorRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
            //                                break;
            //                            }
            //                        case 5: { // Nombre o Razon Social del Receptor
            //                                vigente.Receptor = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
            //                                break;
            //                            }
            //                        case 6: { // Fecha de Emisión
            //                                vigente.FechaEmision = Convert.ToDateTime(htmlNode1.InnerText.Replace("T", " ").Replace("&#58;", ":"));
            //                                break;
            //                            }
            //                        case 7: { // Fecha de Certificación
            //                                vigente.FechaCertifica = Convert.ToDateTime(htmlNode1.InnerText.Replace("T", " ").Replace("&#58;", ":"));
            //                                break;
            //                            }
            //                        case 8: { // PAC que Certifico
            //                                vigente.PacCertifica = htmlNode1.InnerText;
            //                                break;
            //                            }
            //                        case 9: { // Total
            //                                vigente.Total = Convert.ToDecimal(htmlNode1.InnerText.Replace("$", "").Replace("&#36;", "").Replace(",", ""));
            //                                break;
            //                            }
            //                        case 10: { // Efecto del Comprobante
            //                                vigente.Efecto = htmlNode1.InnerText;
            //                                break;
            //                            }
            //                        case 11: { // Estatus de cancelacion
            //                                vigente.EstatusCancelacion = htmlNode1.InnerText.Trim();
            //                                break;
            //                            }
            //                        case 12: { // Estado del Comprobante
            //                                vigente.EstadoComprobante = htmlNode1.InnerText.Trim();
            //                                break;
            //                            }
            //                        case 13: { // Estatus del Proceso de Cancelación
            //                                vigente.EstatusProcesoCancelacion = htmlNode1.InnerText.Trim();
            //                                break;
            //                            }
            //                        case 14: { // Fecha de Proceso de Cancelación
            //                                vigente.FechaProcesoCancelacion = ParseConvert.ConvertDateTime(htmlNode1.InnerText);
            //                                break;
            //                            }
            //                        case 15: { // RFC a cuenta de terceros
            //                                vigente.CuentaTercerosRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
            //                                break;
            //                            }
            //                        case 16: { // Motivo de cancelacion
            //                                vigente.Motivo = htmlNode1.InnerText;
            //                                break;
            //                            }
            //                        case 17: { // Folio de Sustitucion
            //                                vigente.FolioSustitucion = htmlNode1.InnerText;
            //                                break;
            //                            }
            //                        default: {
            //                                //goto Label1;
            //                                LogErrorService.LogWrite("[ProcessResult]: No coincide con ningun numero de columna." + Environment.NewLine + htmlNode1.OuterHtml);
            //                                Console.WriteLine("ProcessResult: no coincide con ningun numero de columna.");
            //                                break;
            //                            }
            //                    }
            //                    LogErrorService.LogWrite("[ProcessResult]: " + Environment.NewLine + htmlNode1.OuterHtml);
            //                    num++;
            //                }
            //            }
            //            if (!string.IsNullOrEmpty(vigente.FolioFiscal)) {
            //                vigente.PathXML = CrearEstructuraDirectorioDescarga(vigente);
            //                vigente.Tipo = _Configuracion.TipoText;
            //                _DescargaResultados.Add(vigente);
            //                totArchivosDescargados++;
            //            }
            //        }
            //    }
            //} catch (Exception ex) {
            //    ErrorLog = string.Format("ProcessResult: {0}", ex.Message);
            //    Console.WriteLine("ProcessResult: " + ErrorLog);
            //}
        }

        /// <summary>
        /// procedimiento para la descarga
        /// </summary>
        public virtual async void ProcessDownloads() {
            TimerEvitarCierre.Tick += new EventHandler(TimerEvitarCierre_Tick);
            ModoDescarga = true;
            int contador = 0;
            TimerEvitarCierre.Enabled = true;
            Progress<DescargaMasivaProgreso> reporte = new Progress<DescargaMasivaProgreso>();
            reporte.ProgressChanged += Reporte_ProgressChanged;
            Resultados = await DescargaService.Run(Resultados, _WebSAT.Url.ToString(), reporte);
            contador = 0;
            LbMensaje.Text = "Verificando archivos ...";

            foreach (ComprobanteFiscalResponse item in Resultados) {
                if (item.XmlContentB64 != null && item.EstadoComprobante.ToLower() != "cancelado") {
                    if (item.XmlContentB64 != "") {
                        string content = "";
                        try {
                            // validamos el xml
                            content = System.Text.Encoding.UTF8.GetString(FilesExtendedService.FromBase64(item.XmlContentB64));
                        } catch (Exception ex) {
                            LbError.Text = "ProcessDownloads: " + ex.Message;
                            Console.WriteLine("ProcessDownloads: " + ex.Message);
                            content = "";
                        }

                        if (string.IsNullOrEmpty(item.PathXML)) {
                            item.PathXML = CrearEstructuraDirectorioDescarga(item);

                        }

                        if (GetLimiteDescargaSAT(content)) {
                            item.Result = "Limite";
                            item.XmlContentB64 = null;
                        } else if (!GetValidXml(content)) {
                            if (content.Contains("error al intentar descargar el archivo")) {
                                item.Result = "NoDisponible";
                            } else {
                                item.Result = "NoValido";
                            }
                            WriteFileFromB64(item.XmlContentB64, item.PathXML, item.KeyName1() + ".html");
                            item.XmlContentB64 = null;
                        } else {
                            WriteFileFromB64(item.XmlContentB64, item.PathXML, item.KeyNameXml());
                            item.Result = "Descargado";
                        }
                    }
                }

                // descargar archivos adicionales
                if (item.PdfRepresentacionImpresaB64 != null) {
                    // en el caso de la representacion vamos omitir guardarla en el repositorio
                    if (WriteFileFromB64(item.PdfRepresentacionImpresaB64, item.PathXML, item.KeyNamePdf())) {
                        item.PdfRepresentacionImpresaB64 = null;
                    }
                }

                // acuse final de la cancelacion, este archivo si lo guardamos en la base
                if (item.PdfAcuseFinalB64 != null) {
                    WriteFileFromB64(item.PdfAcuseFinalB64, item.PathXML, item.KeyNameAcuseFinal());
                }
                contador = contador + 1;
                LbMensaje.Text = string.Concat("Verificando ... ", contador);
                Application.DoEvents();
            }

            // cerrar la pagina
            try {
                GetElementHtmlById("anchorClose", ref _WebSAT).InvokeMember("click");
            } catch (Exception exception3) {
                Console.WriteLine("ProcessDownloads" + exception3.Message);
            }

            Cerrando = true;
            TimerClose.Enabled = true;
            TimerEvitarCierre.Enabled = false;
            TimerEvitarCierre.Stop();
            ModoDescarga = false;
            OnProgressChanged(new DescargaMasivaProgreso(100, Resultados.Count, Resultados.Count, "Terminado"));
            OnCompletedProcess(new DescargaMasivaCompletedProcess("Descarga Terminada", ""));
        }
        #endregion
    }
}
