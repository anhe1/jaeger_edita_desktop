﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HtmlAgilityPack;
using Jaeger.Repositorio.Entities;

namespace Jaeger.Repositorio.Abstractions {
    public abstract class MasivoCommon {
        #region declaraciones
        protected internal WebBrowser webSAT;
        protected internal bool Cerrando = false;
        protected internal bool TrucoHide = false;
        protected internal bool ModoDescarga = false;

        protected internal readonly string UrlAutentificacion = "https://portalcfdi.facturaelectronica.sat.gob.mx/";
        protected internal readonly string UrlEmitidas = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx";
        protected internal readonly string UrlRecibidas = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx";
        protected internal readonly string UrlOculta = "https://portalcfdi.facturaelectronica.sat.gob.mx/Consulta.aspx?oculta=1";
        protected internal readonly string UrlError1 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATx509Custom&sid=0&option=credential&sid=0";
        protected internal readonly string UrlError2 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=";
        protected internal readonly string UrlRedir = "https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa=";
        protected internal readonly string UrlFailCaptcha = "https://cfdiau.sat.gob.mx/nidp/app/login?sid=0&sid=0";

        protected internal Timer TimerEvitarCierre = new Timer() { Enabled = false, Interval = 15000 };
        protected internal Timer TimerClose = new Timer() { Interval = 1000, Enabled = false };
        #endregion

        #region eventos
        public event EventHandler<DescargaMasivaProgreso> ProgressChanged;

        public void OnProgressChanged(DescargaMasivaProgreso e) {
            if (ProgressChanged != null)
                ProgressChanged(this, e);
        }
        #endregion

        #region propiedades
        public virtual string Version {
            get { return "3.0"; }
        }

        public ConfiguracionDetail Configuracion { get; set; }

        public List<ComprobanteFiscalResponse> Resultados { get; set; }
        #endregion

        #region metodos
        protected HtmlElement GetElementHtmlById(string id) {
            HtmlElement elementById = null;
            try {
                elementById = this.webSAT.Document.GetElementById(id);
            } catch (Exception exception) {
                elementById = null;
                Console.WriteLine("GetElementHtmlById: " + exception.Message);
            }
            return elementById;
        }

        protected string GetElementURL(HtmlNode element, string url, string byName) {
            string _result = "";
            var _doc = new HtmlAgilityPack.HtmlDocument();
            _doc.LoadHtml(element.OuterHtml);
            try {
                var _elementById = _doc.GetElementbyId(byName);
                if (_elementById != null) {
                    var _file = Regex.Match(_elementById.OuterHtml, "'.*?'").Value.Replace("'", "");
                    _result = string.Format(url, _file);
                }
            } catch (Exception ex) {
                Console.WriteLine("GetElementURL: " + ex.Message);
            }

            return _result;
        }

        protected bool UrlErroLogin(string url) {
            bool flag = false;
            if (!(url.Contains("SATx509Custom") & url != UrlError1)) {
                flag = false;
            } else {
                url = url.Substring(0, 62);
                int integer = Convert.ToInt32(url.Split('=')[2]);

                if (integer >= 1) {
                    flag = true;
                }
            }
            return flag;
        }

        protected bool UrlLoad(string temp) {
            bool flag = false;
            if (webSAT.Url.ToString().Contains(temp)) {
                flag = true;
            }
            return flag;
        }

        protected void GoUrl(string url) {
            Application.DoEvents();
            try {
                webSAT.Navigate(url, "_self", null, "User-Agent: Luke's Web Browser");
            } catch (Exception exception1) {
                Console.WriteLine(exception1.Message);
                Exception exception = exception1;
                MessageBox.Show("GoURL: " + exception.Message, "Navigate Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected void LoginScrapSAT() {
            if (webSAT.ReadyState == WebBrowserReadyState.Complete & (UrlLoad(UrlError2) | webSAT.Url.ToString() == UrlFailCaptcha)) {
                try {
                    webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", Configuracion.RFC);
                    webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", Configuracion.CIEC);
                } catch (Exception ex) {
                    Console.WriteLine("LoginScraptSAT: " + ex.Message);
                }
            }
        }

        protected bool EstaEnSelector() {
            bool flag = false;
            if (new Regex("Seleccione la opción deseada").Matches(webSAT.Document.Body.InnerHtml).Count > 0) {
                flag = true;
            }
            return flag;
        }

        public static bool GetLimiteDescargaSAT(string content) {
            bool flag = false;

            string mensaje = "Ya no puedes descargar";

            if (content.Contains(mensaje)) {
                flag = true;
            }
            return flag;
        }

        public static bool GetValidXml(string content) {
            bool flag = false;
            try {
                if (content.Contains("cfdi") | content.Contains("retenciones")) {
                    return true;
                }
            } catch (Exception ex) {
                Console.WriteLine("GetValidXML: " + ex.Message);
            }
            return flag;
        }

        public static bool WriteFileFromB64(string base64, string directorio, string archivo) {
            bool result = false;
            byte[] numArray;

            if (base64 == null ? true : base64.Length == 0) {
                numArray = null;
            } else {
                try {
                    numArray = Convert.FromBase64String(base64);
                } catch (FormatException fex) {
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64, Environment.NewLine), fex);
                }
            }

            if (numArray != null) {
                if (!Directory.Exists(directorio)) {
                    Directory.CreateDirectory(directorio);
                }

                try {
                    using (FileStream fileStream = File.Create(Path.Combine(directorio, archivo))) {
                        fileStream.Write(numArray, 0, checked(numArray.Length));
                        fileStream.Close();
                        result = true;
                    }
                } catch (Exception ex) {
                    Console.WriteLine("WriteFileFromB64: " + ex.Message);
                }
            }

            return result;
        }

        public string CrearEstructuraDirectorioDescarga(ComprobanteFiscalResponse objeto) {
            var estructuraDelDirectorio = new string[] {
                Configuracion.CarpetaDescarga.Trim(),
                Configuracion.RFC.ToString().Trim(),
                Configuracion.TipoText,
                objeto.FechaEmision.Year.ToString("0000"),
                objeto.FechaEmision.Month.ToString("00"),
                objeto.FechaEmision.Day.ToString("00") };

            var rutaDelDirectorioDescarga = Path.Combine(estructuraDelDirectorio);
            return rutaDelDirectorioDescarga;
        }

        /// <summary>
        /// reporte de progreso de la descarga
        /// </summary>
        public virtual void Reporte_ProgressChanged(object sender, DescargaMasivaProgreso e) {
            Console.WriteLine(string.Format("{0} {1} de {2} ({3}%)", e.Data, e.Contador, e.Total, e.Completado));
            OnProgressChanged(e);
        }
        #endregion
    }
}
