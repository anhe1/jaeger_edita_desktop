﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using HtmlAgilityPack;
using Jaeger.Repositorio.Contracts;
using Jaeger.Repositorio.Services;

namespace Jaeger.Repositorio.Abstractions {
    public abstract class HtmlToTable {
        #region declaraciones
        protected DataTable dTable = new DataTable();
        protected internal HtmlDocument htmlDocument;
        protected internal string tableName;
        protected internal string htmlString;
        protected internal List<string> Headers = new List<string>();
        protected internal List<IComprobanteFiscal> _Result;
        #endregion

        public virtual DataTable Build() {
            return this.GetDataTable();
        }

        protected internal DataTable GetDataTable() {
            this.GetHtmlDocument(this.htmlString);
            if (htmlDocument == null) {
                //throw new Exception("No se ha obtenido el documento HTML.");
                LogErrorService.LogWrite("No se ha obtenido el documento HTML.");
            }
            HtmlNode htmlTable = GetHtmlTable();
            if (htmlTable == null) {
                //throw new Exception("No se ha obtenido la tabla indicada.");
                LogErrorService.LogWrite("No se ha obtenido la tabla indicada.");
                LogErrorService.LogWrite(this.htmlString);
                return new DataTable();
            }
            CreateColumns(htmlTable);
            CreateRows(htmlTable);
            return dTable;
        }

        protected internal abstract void CreateRows(HtmlNode table);

        protected internal void CreateHeaders(List<string> lstHeaders) {
            foreach (string lstHeader in lstHeaders) {
                dTable.Columns.Add(lstHeader);
            }
        }

        protected internal void CreateColumns(HtmlNode table) {
            if (table == null) { return; }
            foreach (HtmlNode childNode in table.ChildNodes) {
                if (childNode.Name.ToUpper() == "TR") {
                    foreach (HtmlNode htmlNode in childNode.ChildNodes) {
                        if (htmlNode.Name.ToUpper() == "TH") {
                            string value = htmlNode.InnerText
                                .Replace("\r", "")
                                .Replace("\n", "")
                                .Replace("&oacute;", "ó")
                                .Replace("&nbsp;", "").Trim();
                            Headers.Add(value);
                        }
                    }
                }
            }
            this.CreateHeaders(Headers);
        }

        protected internal HtmlDocument GetHtmlDocument(string strHtml) {
            htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(strHtml);

            return htmlDocument;
        }

        protected internal HtmlNode GetHtmlTable() {
            var table = this.htmlDocument.GetElementbyId(this.tableName);
            if (table == null) { return null; }
            HtmlNode table1;
            if (table.ChildNodes.FirstOrDefault().Name == "tbody") {
                table1 = table.GetElementsByTagName("tbody").FirstOrDefault();
            } else {
                table1 = table;
            }
            return table1;
        }
    }
}
