﻿using Jaeger.Repositorio.Abstractions;

namespace Jaeger.Repositorio.Entities {
    public class RegistroAccion : IRegistroAccion {
        public RegistroAccion() { }

        #region build
        public IRegistroAccion WithUrlXml(string url) {
            UrlXML = url;
            return this;
        }

        public IRegistroAccion WithUrlPdf(string url) {
            UrlPDF = url;
            return this;
        }

        public IRegistroAccion WithUrlAcusePdf(string url) {
            UrlAcusePdf = url;
            return this;
        }

        public IRegistroAccion WithUrlDetalle(string url) {
            UrlDetalle = url;
            return this;
        }

        public IRegistroAccion WithUrlAcuseFinal(string url) {
            UrlAcuseFinal = url;
            return this;
        }
        #endregion

        #region propiedades
        /// <summary>
        /// obtener o establecer url de descarga para el archivo xml
        /// </summary>
        public string UrlXML { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo pdf
        /// </summary>
        public string UrlPDF { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo del acuse pdf
        /// </summary>
        public string UrlAcusePdf { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo pdf de detalles
        /// </summary>
        public string UrlDetalle { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo del acuse final de la cancelacion
        /// </summary>
        public string UrlAcuseFinal { get; set; }

        public override string ToString() {
            return string.Format("xml={0}|pdf={1}|acuse={2}|acusefinal{3}|detalle={4}", UrlXML, UrlPDF, UrlAcusePdf, UrlAcuseFinal, UrlDetalle);
        }
        #endregion
    }
}
