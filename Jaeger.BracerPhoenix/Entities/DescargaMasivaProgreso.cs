﻿namespace Jaeger.Repositorio.Entities {
    public class DescargaMasivaProgreso {
        public DescargaMasivaProgreso(int completado, int total, int contador, string caption) {
            Completado = completado;
            Contador = contador;
            Data = caption;
            Total = total;
        }

        /// <summary>
        /// obtener o establecer el % completado
        /// </summary>
        public int Completado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el contador de items
        /// </summary>
        public int Contador {
            get; set;
        }

        public int Total {
            get; set;
        }

        /// <summary>
        /// obtener o establecer texto
        /// </summary>
        public string Data {
            get; set;
        }
    }
}
