﻿using System.ComponentModel;

namespace Jaeger.Repositorio.Entities {
    public class DescargaProcess {
        public DescargaProcess() {
            Complete = 0;
            Downloaded = new BindingList<ComprobanteFiscalResponse>();
        }

        public int Complete {
            get; set;
        }
        public BindingList<ComprobanteFiscalResponse> Downloaded {
            get; set;
        }
    }
}
