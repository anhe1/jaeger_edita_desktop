﻿using System;
using Jaeger.Repositorio.ValueObjects;

namespace Jaeger.Repositorio.Entities {
    public class DescargaFechas500 {
        public DescargaFechas500() {
            Status = DescargaLapsoStatusEnum.Pendiente;
            Total = 0;
        }

        public DateTime Fecha {
            get; set;
        }

        public DescargaLapsoStatusEnum Status {
            get; set;
        }

        public int Total {
            get; set;
        }
    }
}
