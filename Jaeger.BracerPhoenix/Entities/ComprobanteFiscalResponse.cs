﻿/// develop: anhe 261120180043
/// purpose: representa fila de la tabla de comprobantes del SAT
using Jaeger.Repositorio.Abstractions;
using Jaeger.Repositorio.Contracts;
using System;

namespace Jaeger.Repositorio.Entities {
    public class ComprobanteFiscalResponse : Contracts.IComprobanteFiscal {
        private DateTime? _FechaProcesoCancelacion;
        private DateTime? fechaValidacion;
        private DateTime? fechaNuevo;

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalResponse() {
            FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el folio fiscal (uuid) del comprobante
        /// </summary>
        public string FolioFiscal { get; set; }

        /// <summary>
        /// obtener o establecer el rfc del emisor del comprobante
        /// </summary>
        public string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Emisor
        /// </summary>
        public string Emisor { get; set; }

        /// <summary>
        /// obtener o establacer el RFC del receptor del comprobante
        /// </summary>
        public string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el Nombre o Razón Social del Receptor
        /// </summary>
        public string Receptor { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante
        /// </summary>
        public DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de certificación del comprobante
        /// </summary>
        public DateTime FechaCertifica { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del PAC que Certificó
        /// </summary>
        public string PacCertifica { get; set; }

        /// <summary>
        /// obtener o establecer valor total del comprobante
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Efecto { get; set; }

        /// <summary>
        /// obtener o establecer leyenda de Estatus de cancelación (si es posible cancelar con o sin aceptación)
        /// </summary>
        public string EstatusCancelacion { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante Cancelado o Vigente
        /// </summary>
        public string EstadoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer Estatus de Proceso de Cancelación
        /// </summary>
        public string EstatusProcesoCancelacion { get; set; }

        /// <summary>
        /// obtener o establecer Fecha de Proceso de Cancelación
        /// </summary>
        public DateTime? FechaProcesoCancelacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (_FechaProcesoCancelacion >= firstGoodDate)
                    return _FechaProcesoCancelacion;
                else
                    return null;
            }
            set {
                _FechaProcesoCancelacion = value;
            }
        }

        /// <summary>
        /// obtener o establecer RFC a cuenta de terceros
        /// </summary>
        public string CuentaTercerosRFC { get; set; }

        /// <summary>
        /// obtener o establecer el motivo de la cancelacion
        /// </summary>
        public string Motivo { get; set; }

        /// <summary>
        /// obtener o establecer folio de sustitucion del comprobante
        /// </summary>
        public string FolioSustitucion { get; set; }

        /// <summary>
        /// obtener o establecer si el comprobante fue descargado
        /// </summary>
        public bool Descargado { get; set; }

        /// <summary>
        /// obtener o establecer ruta completa del archivo
        /// </summary>
        public string PathFull { get; set; }

        /// <summary>
        /// obtener o establecer el tipo del comprobante (Emitido o Recibido)
        /// </summary>
        public string Tipo { get; set; }

        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaValidacion >= firstGoodDate)
                    return fechaValidacion;
                else
                    return null;
            }
            set {
                fechaValidacion = value;
            }
        }

        public DateTime? FechaNuevo {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaNuevo > firstGoodDate)
                    return fechaNuevo;
                return null;
            }
            set {
                fechaNuevo = value;
            }
        }

        /// <summary>
        /// obtener o establecer URL de descarga del archivo XML del comprobante
        /// </summary>
        //public string UrlXML { get; set; }

        /// <summary>
        /// obtener o establecer URL de descarga de la representacion impresa (PDF) del comprobante
        /// </summary>
        //public string UrlPDF { get; set; }

        /// <summary>
        /// obtener o establecer URL de descarga del detalle del comprobante (PDF)
        /// </summary>
        //public string UrlDetalle { get; set; }

        /// <summary>
        /// obtener o establecer url para recuperar la representación impresa (pdf) del acuse de solicitud  de cancelacion del comprobante
        /// </summary>
        //public string UrlRecuperarAcuse { get; set; }

        /// <summary>
        /// obtener o establecer url para recuperar la representación impresa el Acuse del status final de la cancelacion del comprobante
        /// </summary>
        //public string UrlRecuperaAcuseFinal { get; set; }

        /// <summary>
        /// obtener o establacer la ruta del archivo XML
        /// </summary>
        public string PathXML { get; set; }

        /// <summary>
        /// obtener o establecer la ruta del archivo PDF
        /// </summary>
        public string PathPDF { get; set; }

        /// <summary>
        /// obtener o establecer le resultado de la descarga
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// obtener o establecer el contenido del comprobante XML en base64
        /// </summary>
        public string XmlContentB64 { get; set; }

        /// <summary>
        /// obtener o establecer el contenido de la representacion impresa del comprobante en base64
        /// </summary>
        public string PdfRepresentacionImpresaB64 { get; set; }

        /// <summary>
        /// obtener o establecer el contenido del acuse final de cancelacion del comprobante en base64
        /// </summary>
        public string PdfAcuseB64 { get; set; }

        /// <summary>
        /// obtener o establecer el contenido del acuse de canclacion del comprobante en base64
        /// </summary>
        public string PdfAcuseFinalB64 { get; set; }

        ///// <summary>
        ///// obtener o establecer la bandera que identifica el status de la consilicacion (0 = Sin Comprobar, 1 = Comprobado 2 = No Encontrado)
        ///// </summary>
        //public EnumDescargaConsilia Comprobado { get; set; }

        public string ComprobadoText { get; set; }

        public string KeyName {
            get {
                return string.Concat(EmisorRFC, "-", ReceptorRFC, "-", FolioFiscal, "-", FechaEmision.ToString("yyyyMMddHHmmss"));
            }
        }
        private IRegistroAccion _Acciones;
        public IRegistroAccion Acciones {
            get { return this._Acciones; }
        }

        /// <summary>
        /// obtener 
        /// </summary>
        public string KeyName1() {
            return string.Concat(EmisorRFC, "-", ReceptorRFC, "-", FolioFiscal, "-", FechaEmision.ToString("yyyyMMddHHmmss"));
        }

        public string KeyNameXml() {
            return string.Concat(KeyName1(), ".xml");
        }

        public string KeyNamePdf() {
            return string.Concat(KeyName1(), ".pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representación impresa del acuse de cancelacion, en este caso solo para los comprobantes emitidos.
        /// </summary>
        public string KeyNameAcuse() {
            return string.Concat(KeyName1(), "-Acuse.pdf");
        }

        /// <summary>
        /// obtener el nombre del archivo de la representacion impresa del acuse final de la cancelacion del comprobante.
        /// </summary>
        public string KeyNameAcuseFinal() {
            return string.Concat(KeyName1(), "-AcuseFinal.pdf");
        }

        #region build
        public IComprobanteFiscal WithAcciones(IRegistroAccion acciones) {
            this._Acciones = acciones;
            return this;
        }

        public IComprobanteFiscal WithFolioFiscal(string uuid) {
            this.FolioFiscal = uuid;
            return this;
        }

        public IComprobanteFiscal WithEmisorRFC(string rfc) {
            this.EmisorRFC = rfc;
            return this;
        }

        public IComprobanteFiscal WithEmisor(string emisor) {
            this.Emisor = emisor;
            return this;
        }

        public IComprobanteFiscal WithFechaEmision(string fecha) {
            if (!string.IsNullOrEmpty(fecha)) {
                this.FechaEmision = DateTime.Parse(fecha);
            }
            return this;
        }

        public IComprobanteFiscal WithFechaCertificacion(string fecha) {
            if (!string.IsNullOrEmpty(fecha)) {
                this.FechaCertifica = DateTime.Parse(fecha);
            }
            return this;
        }

        public IComprobanteFiscal WithFechaProcesoCancelaion(string fecha) {
            if (!string.IsNullOrEmpty(fecha)) {
                this._FechaProcesoCancelacion = DateTime.Parse(fecha);
            }
            return this;
        }

        public IComprobanteFiscal WithFechaProcesoCancelaion(DateTime fecha) {
            this._FechaProcesoCancelacion = fecha;
            return this;
        }

        public IComprobanteFiscal WithTotal(string total) {
            try {
                this.Total = decimal.Parse(total.Replace("$", "").Trim());
            } catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }
            return this;
        }
        #endregion
    }
}