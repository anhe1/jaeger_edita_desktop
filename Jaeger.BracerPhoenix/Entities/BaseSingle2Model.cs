﻿/// develop: anhe 241120180031
/// purpose: configuración de la descarga masiva de comprobantes

namespace Jaeger.Repositorio.Entities {
    public class BaseSingle2Model : BasePropertyChangeImplementation {
        /// <summary>
        /// constructor
        /// </summary>
        public BaseSingle2Model() {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">indice del elemento</param>
        /// <param name="descripcion">descripcion del elemento</param>
        public BaseSingle2Model(string id, string descripcion) {
            this.Id = id;
            this.Descripcion = descripcion;
        }

        /// <summary>
        /// obtener o establecer el indice del elemento
        /// </summary>
        public string Id {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la descripcion del elemento
        /// </summary>
        public string Descripcion {
            get; set;
        }
    }
}
