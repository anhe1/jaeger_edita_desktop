﻿/// develop: anhe 241120180031
/// purpose: configuración de la descarga masiva de comprobantes
using System;
using Jaeger.Repositorio.ValueObjects;

namespace Jaeger.Repositorio.Entities {
    public class Configuracion : BasePropertyChangeImplementation {
        #region declaraciones
        private string rfcField;
        private string ciecField;
        private DescargaTipoEnum tipoField;
        private DateTime fechaField;
        private DateTime fechaInicialField;
        private DateTime fechaFinalField;
        private string carpetaDescargaField;
        private bool reporteField;
        private EstadosEnum statusField;
        private string rfcbuscarField;
        private string complementoField;
        private string captchaField;
        private bool filtrorfcField;
        private bool separarField;
        private bool agruparField;
        private bool mismodiaField;
        private bool renombrarArchivos;
        private bool soloConsultaField;
        #endregion

        public Configuracion() {
            fechaField = DateTime.Now;
            FechaInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            FechaFinal = DateTime.Now;
            rfcField = "";
            rfcbuscarField = "";
            tipoField = DescargaTipoEnum.Recibidos;
            statusField = EstadosEnum.Todos;
            this.CarpetaDescarga = @"C:\CFDI";
        }

        /// <summary>
        /// obtener o establecer el RFC del contribuyente a procesar.
        /// </summary>
        public string RFC {
            get {
                return rfcField;
            }
            set {
                rfcField = value.ToUpper().Trim();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clase CIEC del contribuyente
        /// </summary>
        public string CIEC {
            get {
                return ciecField;
            }
            set {
                ciecField = value.Trim();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de descaga a realizar
        /// </summary>
        public DescargaTipoEnum Tipo {
            get {
                return tipoField;
            }
            set {
                tipoField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del lote a descargar para control interno
        /// </summary>
        public DateTime Fecha {
            get {
                return fechaField;
            }
            set {
                fechaField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha inicial de la consulta
        /// </summary>
        public DateTime FechaInicial {
            get {
                return fechaInicialField;
            }
            set {
                fechaInicialField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha final de la consulta
        /// </summary>
        public DateTime FechaFinal {
            get {
                return fechaFinalField;
            }
            set {
                fechaFinalField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la carpeta de descarga
        /// </summary>
        public string CarpetaDescarga {
            get {
                return carpetaDescargaField;
            }
            set {
                carpetaDescargaField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el usuario requiere visualizar el reporte de la descarga
        /// </summary>
        public bool Reporte {
            get {
                return reporteField;
            }
            set {
                reporteField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el status de la busqueda
        /// </summary>
        public EstadosEnum Status {
            get {
                return statusField;
            }
            set {
                statusField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si se buscar un RFC del emisor o receptor
        /// </summary>
        public string BuscarPorRFC {
            get {
                return rfcbuscarField;
            }
            set {
                rfcbuscarField = value.Trim().ToUpper();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el tipo de complemento a buscar
        /// </summary>
        public string Complemento {
            get {
                return complementoField;
            }
            set {
                complementoField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtner o establacer el captcha a utilizar durante la descarga
        /// </summary>
        public string Captcha {
            get {
                return captchaField;
            }
            set {
                captchaField = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer si se buscara por captcha
        /// </summary>
        public bool FiltrarPorRFC {
            get {
                return filtrorfcField;
            }
            set {
                filtrorfcField = value;
                OnPropertyChanged();
            }
        }

        public bool Separar {
            get {
                return separarField;
            }
            set {
                separarField = value;
                OnPropertyChanged();
            }
        }

        public bool Agrupar {
            get {
                return agruparField;
            }
            set {
                agruparField = value;
                OnPropertyChanged();
            }
        }

        public bool Mismodia {
            get {
                return mismodiaField;
            }
            set {
                mismodiaField = value;
                OnPropertyChanged();
            }
        }

        public bool Renombrar {
            get {
                return renombrarArchivos;
            }
            set {
                renombrarArchivos = value;
                OnPropertyChanged();
            }
        }

        public bool SoloConsulta {
            get {
                return soloConsultaField;
            }
            set {
                soloConsultaField = value;
                OnPropertyChanged();
            }
        }
    }
}
