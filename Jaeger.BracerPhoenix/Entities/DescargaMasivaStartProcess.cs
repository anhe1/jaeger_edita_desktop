using System;

namespace Jaeger.Repositorio.Entities {
    public class DescargaMasivaStartProcess : EventArgs {
        public DescargaMasivaStartProcess() {
        }

        public string Data {
            get; set;
        }
    }
}