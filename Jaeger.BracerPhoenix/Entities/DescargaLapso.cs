﻿using System;
using Jaeger.Repositorio.ValueObjects;

namespace Jaeger.Repositorio.Entities {
    public class DescargaLapso {
        public DateTime Fecha {
            get; set;
        }
        public int SegundoInicial {
            get; set;
        }
        public int SegundoFinal {
            get; set;
        }
        public DescargaLapsoStatusEnum Status {
            get; set;
        }
        public int Cuantos {
            get; set;
        }

        public override string ToString() {
            return string.Format("Lapso: Fecha: {0} Seg. Inicial: {1} Seg. Final: {2} Status: {3} Cuantos: {4}", Fecha.ToShortDateString(), SegundoInicial.ToString(), SegundoFinal.ToString(), Enum.GetName(typeof(DescargaLapsoStatusEnum), Status), Cuantos);
        }
    }
}
