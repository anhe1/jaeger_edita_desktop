﻿/// develop: anhe 241120180031
/// purpose: configuración de la descarga masiva de comprobantes
using System;
using Jaeger.Repositorio.ValueObjects;

namespace Jaeger.Repositorio.Entities {

    public class ConfiguracionDetail : Configuracion {
        public ConfiguracionDetail() : base() {
            
            this.CarpetaDescarga = @"C:\CFDI";
        }

        public int Estado {
            get {
                return (int)this.Status;
            }
            set {
                this.Status = (EstadosEnum)value;
                this.OnPropertyChanged();
            }
        }

        public string TipoText {
            get {
                return Enum.GetName(typeof(DescargaTipoEnum), Tipo);
            }
            set {
                this.Tipo = (DescargaTipoEnum)Enum.Parse(typeof(DescargaTipoEnum), value);
                OnPropertyChanged();
            }
        }
    }
}
