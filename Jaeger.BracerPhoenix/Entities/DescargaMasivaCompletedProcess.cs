﻿using System;

namespace Jaeger.Repositorio.Entities {
    public class DescargaMasivaCompletedProcess : EventArgs {
        public DescargaMasivaCompletedProcess(string mensaje, string logError, bool isError = false) {
            Data = mensaje;
            LogError = logError;
        }

        public string Data {
            get; set;
        }

        public string LogError {
            get; set;
        }

        public bool IsError {
            get; set;
        }
    }
}
