﻿// develop: anhe
// purpose: consulta y descarga de comprobantes fiscales con el metodo tradicional
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using System.ComponentModel;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.Repositorio.Abstractions;

namespace Jaeger.Repositorio.V3 {
    public class DescargaXml1SAT : DescargaXmlSATComun, IDescargaMasiva {
        private TextBox gtxtLog = null;
        private TextBox gtxtStatus = null;
        private string currentURL;

        private bool seProcesoWBSat = false;
        private int intentos = 0;
        
        private BackgroundWorker procesar = new BackgroundWorker();

        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml1SAT(TextBox txtLog, TextBox txtStatus) {
            gtxtLog = txtLog;
            gtxtStatus = txtStatus;
            procesar.DoWork += Procesar_DoWork;
        }

        public new string Version {
            get { return "3.0"; }
        }

        private void Procesar_DoWork(object sender, DoWorkEventArgs e) {
            throw new NotImplementedException();
        }

        #region metodos publicos

        public void Consultar(WebBrowser _wsSat2, ConfiguracionDetail conf) {
            Configuracion = conf;
            Resultados = new List<ComprobanteFiscalResponse>();
            _WebSAT = _wsSat2;

            _WebSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(WebBrowser_DocumentCompleted);

            Authenticate();
        }

        public void Reset() {
            TimerLogin.Enabled = false;
            TimerClose.Enabled = false;
            TimerError.Enabled = false;
            TimerCloseAlert.Enabled = false;
            TimerFiltrarRecibidas.Enabled = false;
            TimerFiltrarEmitidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = false;
            TimerRecopilarEmitidas.Enabled = false;
            TimerEvitarCierre.Enabled = false;
        }

        #endregion

        #region metodos privados

        private void Authenticate() {
            try {
                Message(string.Format("Iniciando sesion del usuario {0}.", Configuracion.RFC.Trim().ToUpper()));

                string postData = string.Format("&Ecom_User_ID={0}&Ecom_Password={1}&jcaptcha={2}&submit=Enviar", Configuracion.RFC, Configuracion.CIEC, Configuracion.Captcha);
                MessageStatus("Datos a posterar en la url  " + postData);

                var encoding = System.Text.Encoding.UTF8;
                byte[] bytes = encoding.GetBytes(postData);

                MessageStatus("Navegando a la url " + _WebSAT.Url.ToString());
                _WebSAT.Navigate(_WebSAT.Url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");

                #region ESPERA A QUE SE CARGUE LA PAGINA WEB Y SE PROCESE EL METODOD wbSAT_DocumentCompleted
                seProcesoWBSat = false;

                intentos = 0;
                while (true) {
                    Application.DoEvents();
                    if (!_WebSAT.IsBusy) {
                        if (seProcesoWBSat) {
                            break;
                        }

                        if (intentos > 100000) {
                            Message("CONTRASEÑA CIEC ..... ");
                            MessageStatus("AVISO IMPORTANTE: CONTRAEÑA CIEC INCORRECTA  " + intentos.ToString());
                            break;
                        }
                        intentos += 1;
                    }
                }
                #endregion

            } catch (Exception exception) {
                throw new Exception(exception.Message);
            }
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {

            DateTime FI = Configuracion.FechaInicial;
            DateTime FF = Configuracion.FechaFinal;

            bool flag;
            bool flag1;

            if (_WebSAT.Document != null) {
                //if (this.webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0"))
                if (_WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                 || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                 || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                 || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")) {

                    MessageStatus(string.Format("Usuario {0} Autentificado correctamente.", Configuracion.RFC));

                    if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                        _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                    }

                    if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                        _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                    }
                }

                if (_WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app/login?sid=1&sid=1")) {
                    MessageStatus("Clave CIEC incorrecta, favor de verificar RFC y Clave.");
                    _WebSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", Configuracion.RFC.Trim().ToUpper());
                    _WebSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", Configuracion.CIEC.Trim());
                    _WebSAT.Document.GetElementById("jcaptcha").SetAttribute("value", Configuracion.Captcha);
                    _WebSAT.Document.GetElementById("submit").InvokeMember("click");
                }

                flag = _WebSAT.Url != new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx") ? true : !(_WebSAT.Url != new Uri(currentURL));
                if (!flag) {
                    #region Documentos Recibidos
                    for (DateTime fecha = FI; fecha <= FF; fecha = fecha.AddDays(1)) {
                        MessageStatus(string.Format("Procesando documentos recibidos de la fecha " + fecha.ToString("dd/MM/yyyy")));

                        int vecesQueSeHaLLamdoADocumentos = 0;
                        int totalArchivosDescargados = 0;
                        DateTime ultimaFechaEmision = DateTime.Now;
                        do {
                            totalArchivosDescargados = 0;
                            vecesQueSeHaLLamdoADocumentos = vecesQueSeHaLLamdoADocumentos + 1;
                            GetReceivedDocuments(fecha.Year, fecha.Month, fecha.Day, ref totalArchivosDescargados, ref ultimaFechaEmision, vecesQueSeHaLLamdoADocumentos);
                        }
                        while (totalArchivosDescargados >= MaximoDescargaSAT);
                    }
                    #endregion

                    FinishSession();
                }

                flag1 = _WebSAT.Url != new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx") ? true : !(_WebSAT.Url != new Uri(currentURL));
                if (!flag1) {
                    #region Documentos Enviados
                    for (DateTime fecha = FI; fecha <= FF; fecha = fecha.AddDays(1)) {
                        MessageStatus(string.Format("Procesando documentos enviados de la fecha." + fecha.ToString("dd/MM/yyyy")));

                        int vecesQueSeHaLLamdoADocumentos = 0;
                        int totalArchivosDescargados = 0;
                        DateTime ultimaFechaEmision = DateTime.Now;
                        do {
                            totalArchivosDescargados = 0;
                            vecesQueSeHaLLamdoADocumentos = vecesQueSeHaLLamdoADocumentos + 1;
                            GetSendedDocuments(fecha.Year, fecha.Month, fecha.Day, ref totalArchivosDescargados, ref ultimaFechaEmision, vecesQueSeHaLLamdoADocumentos);
                        }
                        while (totalArchivosDescargados >= MaximoDescargaSAT);
                    }
                    #endregion

                    FinishSession();
                }

                int timeout = 0;
                while (true) {
                    Application.DoEvents();
                    if (!_WebSAT.IsBusy) {
                        break;
                    }
                    timeout = timeout + 1;
                    if (timeout > 40000)
                        break;

                }
                currentURL = _WebSAT.Url.ToString();
            }
        }

        /// <summary>
        /// metodo para insertar consulta de los comprobantes emitidos
        /// </summary>
        private void GetReceivedDocuments(int year, int month, int day, ref int totalArchivosDescargados, ref DateTime ultimaFechaEmision, int vecesQueSeHaLLamadoAEsteMetodo) {
            bool flag1;
            bool flag2;
            try {
                if (_WebSAT.Document != null) {
                    #region Captura los datos de la pagina web
                    #region Selecciona el RdoFechas con ub CLICK
                    HtmlElement elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_RdoFechas");
                    if (elementById != null) {
                        elementById.InvokeMember("click");
                    }

                    while (true) {
                        Application.DoEvents();
                        HtmlElement htmlElement = _WebSAT.Document.GetElementById("DdlAnio");
                        flag1 = htmlElement == null ? true : !(htmlElement.GetAttribute("disabled") != "disabled");
                        if (!flag1) {
                            break;
                        }
                    }
                    #endregion

                    #region Captura la FECHA del día
                    HtmlElement elementById1 = _WebSAT.Document.GetElementById("DdlAnio");
                    if (elementById1 != null) {
                        elementById1.SetAttribute("value", year.ToString(CultureInfo.InvariantCulture));
                    }
                    HtmlElement htmlElement1 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMes");
                    if (htmlElement1 != null) {
                        htmlElement1.SetAttribute("value", month.ToString("0"));
                    }
                    HtmlElement elementById2 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlDia");
                    if (elementById2 != null) {
                        elementById2.SetAttribute("value", day.ToString("00"));
                    }
                    #endregion
                    // 

                    if (vecesQueSeHaLLamadoAEsteMetodo > 1) {
                        #region Set atributo de la Hora Inicial
                        HtmlElement elementById4 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");
                        if (elementById4 != null) {
                            elementById4.SetAttribute("value", ultimaFechaEmision.Hour.ToString());
                        }
                        HtmlElement elementById5 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");
                        if (elementById5 != null) {
                            elementById5.SetAttribute("value", ultimaFechaEmision.Minute.ToString());
                        }
                        HtmlElement elementById6 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");
                        if (elementById6 != null) {
                            elementById6.SetAttribute("value", ultimaFechaEmision.Second.ToString());
                        }
                        #endregion

                        //if (false) {
                        //    #region Set atributo de la Hora Final
                        //    HtmlElement elementById7 = webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");
                        //    if (elementById7 != null) {
                        //        elementById7.SetAttribute("value", "14");
                        //    }
                        //    HtmlElement elementById8 = webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");
                        //    if (elementById8 != null) {
                        //        elementById8.SetAttribute("value", "59");
                        //    }
                        //    HtmlElement elementById9 = webSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");
                        //    if (elementById9 != null) {
                        //        elementById9.SetAttribute("value", "59");
                        //    }
                        //    #endregion
                        //}
                    }

                    //if (gEstatus.Trim() != "Todos")
                    if (Configuracion.Status != EstadosEnum.Todos) {
                        #region Set atributo de Estatus
                        HtmlElement elementById10 = _WebSAT.Document.GetElementById("ctl00_MainContent_DdlEstadoComprobante");
                        if (elementById10 != null) {
                            //if (gEstatus.Trim() == "Cancelados") elementById10.SetAttribute("value", "0");
                            //if (gEstatus.Trim() == "Vigentes") elementById10.SetAttribute("value", "1");
                            if (Configuracion.Status == EstadosEnum.Cancelado)
                                elementById10.SetAttribute("value", "0");
                            if (Configuracion.Status == EstadosEnum.Vigente)
                                elementById10.SetAttribute("value", "1");
                        }
                        #endregion
                    }

                    #region Se le da CLICK al btnBusqueda
                    HtmlElement htmlElement2 = _WebSAT.Document.GetElementById("ctl00_MainContent_BtnBusqueda");
                    if (htmlElement2 != null) {
                        htmlElement2.InvokeMember("click");
                    }

                    int num = 0;
                    while (true) {
                        Application.DoEvents();
                        HtmlElement elementById3 = _WebSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
                        if (!(elementById3 == null)) {
                            flag2 = elementById3.Style != "display: none;" ? true : num <= 100000;
                            if (!flag2) {
                                break;
                            }
                            num++;
                        }
                    }
                    #endregion
                    #endregion

                    ProcessResult(ref totalArchivosDescargados, ref ultimaFechaEmision, ref _WebSAT);
                }
            } catch (Exception exception) {
                Message("ERROR EN EL METODO GetRecivedDocuments: " + exception.Message);

                MessageBox.Show(exception.Message);
            }
        }

        private void GetSendedDocuments(int year, int month, int day, ref int TotalArchivosDescargados, ref DateTime UltimaFechaEmision, int VecesQueSeHaLLamadoAEsteMetodo) {
            HtmlElement elementById;
            bool flag1;
            bool flag2;
            bool flag3;
            bool flag4;
            try {
                if (_WebSAT.Document != null) {
                    #region Selecciona el RdoFechas con un Click
                    HtmlElement htmlElement = _WebSAT.Document.GetElementById("ctl00_MainContent_RdoFechas");
                    if (htmlElement != null) {
                        htmlElement.InvokeMember("click");
                    }
                    while (true) {
                        Application.DoEvents();
                        HtmlElement elementById1 = _WebSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
                        flag1 = elementById1 == null ? true : !(elementById1.GetAttribute("disabled") != "disabled");
                        if (!flag1) {
                            break;
                        }
                    }
                    #endregion

                    #region Capturamos la FECHA
                    DateTime dateTime = new DateTime(year, month, day);
                    HtmlElement htmlElement1 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_Calendario_text");
                    if (htmlElement1 != null) {
                        htmlElement1.SetAttribute("value", dateTime.ToString("dd/MM/yyyy"));
                    }
                    HtmlElement elementById2 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_BtnFecha2");
                    if (elementById2 != null) {
                        elementById2.InvokeMember("click");
                    }

                    while (true) {
                        Application.DoEvents();
                        elementById = _WebSAT.Document.GetElementById("datepicker");
                        flag2 = elementById == null ? true : !elementById.Style.Contains("visibility: visible");
                        if (!flag2) {
                            break;
                        }
                    }
                    #endregion

                    if (VecesQueSeHaLLamadoAEsteMetodo > 1) {
                        #region Set atributo de la Hora Inicial
                        HtmlElement elementById4 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlHora");
                        if (elementById4 != null) {
                            elementById4.SetAttribute("value", UltimaFechaEmision.Hour.ToString());
                        }
                        HtmlElement elementById5 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlMinuto");
                        if (elementById5 != null) {
                            elementById5.SetAttribute("value", UltimaFechaEmision.Minute.ToString());
                        }
                        HtmlElement elementById6 = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlSegundo");
                        if (elementById6 != null) {
                            elementById6.SetAttribute("value", UltimaFechaEmision.Second.ToString());
                        }
                        #endregion

                        //if (false) {
                        //    #region Set atributo de la Hora Final
                        //    HtmlElement elementById7 = webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlHora");
                        //    if (elementById7 != null) {
                        //        elementById7.SetAttribute("value", "14");
                        //    }
                        //    HtmlElement elementById8 = webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlMinuto");
                        //    if (elementById8 != null) {
                        //        elementById8.SetAttribute("value", "59");
                        //    }
                        //    HtmlElement elementById9 = webSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlSegundo");
                        //    if (elementById9 != null) {
                        //        elementById9.SetAttribute("value", "59");
                        //    }
                        //    #endregion
                        //}
                    }

                    //if (gEstatus.Trim() != "Todos")
                    if (Configuracion.Status != EstadosEnum.Todos) {
                        #region Set atributo de Estatus
                        HtmlElement elementById10 = _WebSAT.Document.GetElementById("ctl00_MainContent_DdlEstadoComprobante");
                        if (elementById10 != null) {
                            //if (gEstatus.Trim() == "Cancelados") elementById10.SetAttribute("value", "0");
                            //if (gEstatus.Trim() == "Vigentes") elementById10.SetAttribute("value", "1");
                            if (Configuracion.Status == EstadosEnum.Cancelado)
                                elementById10.SetAttribute("value", "0");
                            if (Configuracion.Status == EstadosEnum.Vigente)
                                elementById10.SetAttribute("value", "1");
                        }
                        #endregion
                    }


                    IEnumerable<HtmlElement> htmlElements = ElementsByClass(_WebSAT.Document, "dpDayHighlightTD");
                    if (htmlElements != null) {
                        htmlElements.First().InvokeMember("click");
                    }
                    while (true) {
                        Application.DoEvents();
                        elementById = _WebSAT.Document.GetElementById("datepicker");
                        flag3 = elementById == null ? true : elementById.Style.Contains("visibility: visible");
                        if (!flag3) {
                            break;
                        }
                    }

                    #region Dar CLICK en BUSQUEDA
                    HtmlElement htmlElement2 = _WebSAT.Document.GetElementById("ctl00_MainContent_BtnBusqueda");
                    if (htmlElement2 != null) {
                        htmlElement2.InvokeMember("click");
                    }
                    int num = 0;
                    while (true) {
                        Application.DoEvents();
                        HtmlElement elementById3 = _WebSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
                        if (!(elementById3 == null)) {
                            flag4 = elementById3.Style != "display: none;" ? true : num <= 100000;
                            if (!flag4) {
                                break;
                            }
                            num++;
                        }
                    }
                    #endregion

                    ProcessResult(ref TotalArchivosDescargados, ref UltimaFechaEmision, ref _WebSAT);
                }
            } catch (Exception exception) {
                Message("ERROR EN EL METODO GetSendedDocuments: " + exception.Message);
                MessageBox.Show(exception.Message);
            }
        }

        private static IEnumerable<HtmlElement> ElementsByClass(HtmlDocument doc, string className) {
            IEnumerable<HtmlElement> htmlElements = doc.All.Cast<HtmlElement>().Where((e) => {
                bool attribute = e.GetAttribute("className") == className;
                return attribute;
            });
            return htmlElements;
        }

        private void FinishSession() {
            try {
                MessageStatus(string.Format("Cerrar sesion usuario {0}.", Configuracion.RFC.Trim()));
                _WebSAT.Navigate(new Uri("https://portalcfdi.facturaelectronica.sat.gob.mx/logout.aspx?salir=y"));
                Message("Proceso terminado.");
                seProcesoWBSat = true;
            } catch (Exception exception) {
                Message("ERROR AL TRATAR DE TERMINAR LA SECION: " + exception.Message);
                throw new Exception(exception.Message);
            }
        }

        private void Message(string message) {
            try {
                gtxtLog.AppendText(string.Concat(message, "\n"));
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        /// unicamente para establecer 
        /// </summary>
        private void MessageStatus(string menssage) {
            gtxtStatus.Text = menssage;
        }

        #endregion
    }
}
