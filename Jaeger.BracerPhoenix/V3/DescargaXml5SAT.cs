﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using HtmlAgilityPack;
using Jaeger.Repositorio.Abstractions;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.Services;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.Util.Services;

namespace Jaeger.Repositorio.V3 {
    public class DescargaXml5SAT : MasivoCommon {
        
        public DescargaXml5SAT() { 
            this.Configuracion = new ConfiguracionDetail { Tipo = DescargaTipoEnum.Emitidos }; 
        }

        public void Descargar(WebBrowser _wsSat2) {
            this.webSAT = _wsSat2;
            webSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WbSAT_DocumentCompleted);
        }

        public void Procesar() {
            this.Resultados = this.ProcessResult();
            this.ProcessDownloads();
        }

        private void WbSAT_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            if (this.webSAT.Document != null && !Cerrando) {
                if (ModoDescarga) {
                    return;
                } else
                if (!UrlErroLogin(webSAT.Url.ToString())) {
                    if (UrlLoad(UrlRedir)) {
                        GoUrl(UrlAutentificacion);
                    }
                    if (UrlLoad(UrlError2)) {
                        LoginScrapSAT();
                    }
                    if (webSAT.Url.ToString().Contains("https://portalcfdi.facturaelectronica.sat.gob.mx") & EstaEnSelector() && !TrucoHide) {
                        GoUrl(UrlOculta);
                        TrucoHide = true;
                    }

                    if (webSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa=")) {
                        GoUrl(UrlAutentificacion);
                        return;
                    } else if (webSAT.Url.ToString() == UrlFailCaptcha) {
                        LoginScrapSAT();

                    }

                    if (webSAT.Url == new Uri(this.UrlRecibidas)) {
                        this.Configuracion.Tipo = DescargaTipoEnum.Recibidos;
                    }

                    if (webSAT.Url == new Uri(this.UrlEmitidas)) {
                        this.Configuracion.Tipo = DescargaTipoEnum.Emitidos;
                    }

                    if (webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                    || webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                    || webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                    || webSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")
                    ) {
                        GoUrl(UrlOculta);
                    }
                } else {
                    GoUrl(UrlError1);
                    return;
                }
            }
        }

        public virtual List<ComprobanteFiscalResponse> ProcessResult() {
            var response = new List<ComprobanteFiscalResponse>();
            var elementTabla = webSAT.Document.GetElementById("ctl00_MainContent_tblResult");
            var _doc = new HtmlAgilityPack.HtmlDocument();
            _doc.LoadHtml(elementTabla.OuterHtml);
            var columnas = (this.Configuracion.Tipo == DescargaTipoEnum.Emitidos ? 18 : 16);
            try {
                int num = -1;
                foreach (HtmlNode array in _doc.DocumentNode.Descendants("tr").ToArray()) {
                    if (array.HasChildNodes && array.ChildNodes != null && array.ChildNodes.Count.Equals(columnas)) {
                        var vigente = new ComprobanteFiscalResponse();
                        num = 0;
                        foreach (HtmlNode htmlNode1 in from x in array.ChildNodes where x.Name == "td" select x) {
                            if (htmlNode1.Name == "td") {
                                switch (num) {
                                    case 0: {
                                            var acciones = new RegistroAccion();
                                            acciones.WithUrlXml(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnDescarga"));
                                            acciones.WithUrlDetalle(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnVerDetalle"));
                                            acciones.WithUrlPdf(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnRI"));
                                            acciones.WithUrlAcuseFinal(GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnRecuperaAcuse"));
                                            vigente.WithAcciones(acciones);
                                            break;
                                        }
                                    case 1: { // folio fiscal
                                            vigente.FolioFiscal = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 2: { // RFC Emisor
                                            vigente.EmisorRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 3: { // Nombre o Razon Social del Emisor
                                            vigente.Emisor = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 4: { // RFC Receptor
                                            vigente.ReceptorRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 5: { // Nombre o Razon Social del Receptor
                                            vigente.Receptor = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 6: { // Fecha de Emisión
                                            vigente.FechaEmision = Convert.ToDateTime(htmlNode1.InnerText.Replace("T", " ").Replace("&#58;", ":"));
                                            break;
                                        }
                                    case 7: { // Fecha de Certificación
                                            vigente.FechaCertifica = Convert.ToDateTime(htmlNode1.InnerText.Replace("T", " ").Replace("&#58;", ":"));
                                            break;
                                        }
                                    case 8: { // PAC que Certifico
                                            vigente.PacCertifica = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 9: { // Total
                                            vigente.Total = Convert.ToDecimal(htmlNode1.InnerText.Replace("$", "").Replace("&#36;", "").Replace(",", ""));
                                            break;
                                        }
                                    case 10: { // Efecto del Comprobante
                                            vigente.Efecto = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 11: { // Estatus de cancelacion
                                            vigente.EstatusCancelacion = htmlNode1.InnerText.Trim();
                                            break;
                                        }
                                    case 12: { // Estado del Comprobante
                                            vigente.EstadoComprobante = htmlNode1.InnerText.Trim();
                                            break;
                                        }
                                    case 13: { // Estatus del Proceso de Cancelación
                                            vigente.EstatusProcesoCancelacion = htmlNode1.InnerText.Trim();
                                            break;
                                        }
                                    case 14: { // Fecha de Proceso de Cancelación
                                            vigente.FechaProcesoCancelacion = ParseConvert.ConvertDateTime(htmlNode1.InnerText);
                                            break;
                                        }
                                    case 15: { // RFC a cuenta de terceros
                                            vigente.CuentaTercerosRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 16: { // Motivo de cancelacion
                                            vigente.Motivo = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 17: { // Folio de Sustitucion
                                            vigente.FolioSustitucion = htmlNode1.InnerText;
                                            break;
                                        }
                                    default: {
                                            //goto Label1;
                                            LogErrorService.LogWrite("[ProcessResult]: No coincide con ningun numero de columna." + Environment.NewLine + htmlNode1.OuterHtml);
                                            Console.WriteLine("ProcessResult: no coincide con ningun numero de columna.");
                                            break;
                                        }
                                }
                                LogErrorService.LogWrite("[ProcessResult]: " + Environment.NewLine + htmlNode1.OuterHtml);
                                num++;
                            }
                        }
                        if (!string.IsNullOrEmpty(vigente.FolioFiscal)) {
                            vigente.PathXML = CrearEstructuraDirectorioDescarga(vigente);
                            vigente.Tipo = Configuracion.TipoText;
                            response.Add(vigente);
                        }
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine("ProcessResult: " + ex.Message);
            }
            return response;
        }

        public virtual async void ProcessDownloads() {
            ModoDescarga = true;
            int contador = 0;
            TimerEvitarCierre.Enabled = true;
            Progress<DescargaMasivaProgreso> reporte = new Progress<DescargaMasivaProgreso>();
            reporte.ProgressChanged += Reporte_ProgressChanged;
            Resultados = await DescargaService.Run(Resultados, webSAT.Url.ToString(), reporte);
            contador = 0;

            foreach (ComprobanteFiscalResponse item in Resultados) {
                if (item.XmlContentB64 != null && item.EstadoComprobante != "Cancelado") {
                    if (item.XmlContentB64 != "") {
                        string content = "";
                        try {
                            // validamos el xml
                            content = System.Text.Encoding.UTF8.GetString(FilesExtendedService.FromBase64(item.XmlContentB64));
                        } catch (Exception ex) {
                            Console.WriteLine("ProcessDownloads: " + ex.Message);
                            content = "";
                        }

                        if (GetLimiteDescargaSAT(content)) {
                            item.Result = "Limite";
                            item.XmlContentB64 = null;
                        } else if (!GetValidXml(content)) {
                            if (content.Contains("error al intentar descargar el archivo")) {
                                item.Result = "NoDisponible";
                            } else {
                                item.Result = "NoValido";
                            }
                            WriteFileFromB64(item.XmlContentB64, item.PathXML, item.KeyName1() + ".html");
                            item.XmlContentB64 = null;
                        } else {
                            WriteFileFromB64(item.XmlContentB64, item.PathXML, item.KeyNameXml());
                            item.Result = "Descargado";
                        }
                    }
                }

                // descargar archivos adicionales
                if (item.PdfRepresentacionImpresaB64 != null) {
                    // en el caso de la representacion vamos omitir guardarla en el repositorio
                    if (WriteFileFromB64(item.PdfRepresentacionImpresaB64, item.PathXML, item.KeyNamePdf())) {
                        item.PdfRepresentacionImpresaB64 = null;
                    }
                }

                // acuse final de la cancelacion, este archivo si lo guardamos en la base
                if (item.PdfAcuseFinalB64 != null) {
                    WriteFileFromB64(item.PdfAcuseFinalB64, item.PathXML, item.KeyNameAcuseFinal());
                }
                contador++;
                Application.DoEvents();
            }

            // cerrar la pagina
            try {
                GetElementHtmlById("anchorClose").InvokeMember("click");
            } catch (Exception exception3) {
                Console.WriteLine("ProcessDownloads" + exception3.Message);
            }

            Cerrando = true;
            TimerClose.Enabled = true;
            TimerEvitarCierre.Enabled = false;
            ModoDescarga = false;
            OnProgressChanged(new DescargaMasivaProgreso(100, Resultados.Count, Resultados.Count, "Terminado"));
        }
    }
}
