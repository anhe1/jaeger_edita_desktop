﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using HtmlAgilityPack;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.Repositorio.Services;
using Jaeger.Util.Services;

namespace Jaeger.Repositorio.V3 {
    public abstract class DescargaXmlSATComun : BasePropertyChangeImplementation {
        private ConfiguracionDetail configuracion;
        private List<DescargaResponse> descargaResultados;
        private List<DescargaFechas500> listFechasEmi500 = new List<DescargaFechas500>();
        private List<DescargaFechas500> listFechasRec500 = new List<DescargaFechas500>();
        private int maximoDescargaSAT = 495;
        private bool descargaParalela;

        public WebBrowser webSAT;
        public TextBox LbMensaje = null;
        public TextBox LbError = null;
        public bool ModoDescarga = false;
        public string ErrorLog;
        public bool Truco = false;
        public bool TrucoHide = false;
        public bool Cerrando = false;
        public int IndexLapsoEmitido = 0;
        public int IndexDiaEmitido = 0;
        public int IndexLapsoRecibido = 0;
        public int IndexDiaRecibido = 0;

        public Timer TimerLogin = new Timer() { Interval = 1000, Enabled = false };
        public Timer TimerClose = new Timer() { Interval = 1000, Enabled = false };
        public Timer TimerError = new Timer() { Interval = 2000, Enabled = false };
        public Timer TimerCloseAlert = new Timer() { Interval = 500, Enabled = false };
        public Timer TimerFiltrarRecibidas = new Timer() { Enabled = false, Interval = 500 };
        public Timer TimerFiltrarEmitidas = new Timer() { Enabled = false, Interval = 500 };
        public Timer TimerRecopilarRecibidas = new Timer() { Enabled = false, Interval = 1000 };
        public Timer TimerRecopilarEmitidas = new Timer() { Enabled = false, Interval = 1000 };
        public Timer TimerEvitarCierre = new Timer() { Enabled = false, Interval = 15000 };

        public List<DescargaLapso> ListLapsosEmitidos = new List<DescargaLapso>();
        public List<DescargaLapso> ListLapsosEmitidosNoDescargables = new List<DescargaLapso>();

        public List<DescargaLapso> ListLapsosRecibidos = new List<DescargaLapso>();
        public List<DescargaLapso> ListLapsosRecibidosNoDescargables = new List<DescargaLapso>();

        public string UrlAutentificacion = "https://portalcfdi.facturaelectronica.sat.gob.mx/";
        public string UrlEmitidas = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx";
        public string UrlRecibidas = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx";
        public string UrlOculta = "https://portalcfdi.facturaelectronica.sat.gob.mx/Consulta.aspx?oculta=1";
        public string UrlError1 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATx509Custom&sid=0&option=credential&sid=0";
        public string UrlError2 = "https://cfdiau.sat.gob.mx/nidp/wsfed/ep?id=SATUPCFDiCon&sid=";
        public string UrlRedir = "https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa=";
        public string UrlFailCaptcha = "https://cfdiau.sat.gob.mx/nidp/app/login?sid=0&sid=0";

        /// <summary>
        /// construnctor
        /// </summary>
        public DescargaXmlSATComun() {
            this.descargaResultados = new List<DescargaResponse>();
        }

        #region propiedades

        public ConfiguracionDetail Configuracion {
            get {
                return this.configuracion;
            }
            set {
                this.configuracion = value;
                this.OnPropertyChanged();
            }
        }

        public List<DescargaResponse> Resultados {
            get {
                return this.descargaResultados;
            }
            set {
                this.descargaResultados = value;
            }
        }

        public List<DescargaFechas500> ListaFechasEmitidos {
            get {
                return listFechasEmi500;
            }
            set {
                listFechasEmi500 = value;
                this.OnPropertyChanged();
            }
        }

        public List<DescargaFechas500> ListaFechasRecibidos {
            get {
                return listFechasRec500;
            }
            set {
                this.listFechasRec500 = value;
                this.OnPropertyChanged();
            }
        }

        public int MaximoDescargaSAT {
            get {
                return this.maximoDescargaSAT;
            }
            set {
                this.maximoDescargaSAT = value;
            }
        }

        public long Total {
            get {
                return this.descargaResultados.Count;
            }
        }

        public bool DescargaParalela {
            get {
                return this.descargaParalela;
            }
            set {
                this.descargaParalela = value;
            }
        }

        public int Cuantos {
            get {
                return this.descargaResultados.Count;
            }
        }

        #endregion

        #region  eventos

        /// <summary>
        /// al iniciar el proceso de consulta
        /// </summary>
        public event EventHandler<DescargaMasivaStartProcess> StartProcess;

        /// <summary>
        /// progreso de la descarga
        /// </summary>
        public event EventHandler<DescargaMasivaProgreso> ProgressChanged;

        /// <summary>
        /// al terminar el proceso 
        /// </summary>
        public event EventHandler<DescargaMasivaCompletedProcess> CompletedProcess;

        public void OnStartProcess(DescargaMasivaStartProcess e) {
            if (this.StartProcess != null)
                this.StartProcess(this, e);
        }

        public void OnProgressChanged(DescargaMasivaProgreso e) {
            if (this.ProgressChanged != null)
                this.ProgressChanged(this, e);
        }

        public void OnCompletedProcess(DescargaMasivaCompletedProcess e) {
            if (this.CompletedProcess != null)
                this.CompletedProcess(this, e);
        }

        #endregion

        #region metodos privados

        /// <summary>
        /// obtener url de descarga
        /// </summary>
        private string GetElementURL(HtmlElement element, string url, string byName) {
            string result = "";
            HtmlElementCollection elementBtnDescarga = element.GetElementsByTagName("span").GetElementsByName(byName);
            Regex regex = new Regex("'.*?'");
            if (elementBtnDescarga.Count == 1) {
                result = string.Format(url, regex.Match(elementBtnDescarga[0].OuterHtml).Value.Replace("'", ""));
            }
            else {
            }
            return result;
        }

        private string GetElementURL(HtmlAgilityPack.HtmlNode element, string url, string byName) {
            string result = "";
            HtmlAgilityPack.HtmlDocument _doc = new HtmlAgilityPack.HtmlDocument();
            _doc.LoadHtml(element.OuterHtml);
            try {
                var _node = _doc.GetElementbyId(byName);
                if (_node != null)
                if (_node.OuterHtml != null)
                result = string.Format(url, Regex.Match(_node.OuterHtml, "'.*?'").Value.Replace("'", ""));
            }
            catch (Exception ex) {
                Console.WriteLine("GetElementURL: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// reporte de progreso de la descarga
        /// </summary>
        private void Reporte_ProgressChanged(object sender, DescargaMasivaProgreso e) {
            this.LbMensaje.Text = string.Format("{0} {1} de {2} ({3}%)", e.Data, e.Contador, e.Total, e.Completado);
            this.OnProgressChanged(e);
        }

        private void TimerEvitarCierre_Tick(object sender, EventArgs e) {
            if (this.ModoDescarga == true) {
                try {
                    this.GoUrl(this.UrlOculta);
                    Console.WriteLine("TimerEvitarCierre: Evitando cierre...");
                }
                catch (Exception ex) {
                    Console.WriteLine("TimerEvitarCierre: Evitando cierre... error: " + ex.Message);
                }
            }
        }

        #endregion

        #region metodos estaticos

        /// <summary>
        /// crear la estructura del directorio donde se alacenara el archivo descargado
        /// </summary>
        public string CrearEstructuraDirectorioDescarga(DescargaResponse objeto) {
            string[] estructuraDelDirectorio = new string[] { configuracion.CarpetaDescarga.Trim(), configuracion.RFC.ToString().Trim(), this.configuracion.TipoText, objeto.FechaEmision.Year.ToString("0000"), objeto.FechaEmision.Month.ToString("00"), objeto.FechaEmision.Day.ToString("00") };
            string rutaDelDirectorioDescarga = Path.Combine(estructuraDelDirectorio);
            return rutaDelDirectorioDescarga;
        }

        public static bool GetLimiteDescargaSAT(string content) {
            bool flag = false;

            string mensaje = "Ya no puedes descargar";

            if (content.Contains(mensaje)) {
                flag = true;
            }
            return flag;
        }

        public static bool GetValidXml(string content) {
            bool flag = false;
            try {
                if (content.Contains("cfdi") | content.Contains("retenciones")) {
                    return true;
                }
            }
            catch (Exception ex) {
                Console.WriteLine("GetValidXML: " + ex.Message);
            }
            return flag;
        }

        public static string ExtraerReferencia(string path) {
            string str = "";
            using (StreamReader streamReader = new StreamReader(path)) {
                while (!streamReader.EndOfStream) {
                    string str1 = streamReader.ReadLine();
                    if (!str1.Contains("referencia:")) {
                        continue;
                    }
                    str = str1;
                    str = str.Trim();
                    str = str.Substring(67, 37);
                    str = str.Trim();
                    return str;
                }
            }
            str = str.Trim();
            str = str.Substring(67, 37);
            str = str.Trim();
            return str;
        }

        [DllImport("user32.dll", CharSet = CharSet.None, ExactSpelling = false)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        #endregion

        #region metodos publicos

        /// <summary>
        /// pantalla del selector de consulta de emitidas o recibidas
        /// </summary>
        public void ClickEnSelector(DescargaTipoEnum tipo) {
            IEnumerator enumerator = null;
            string str = "";

            if (tipo == DescargaTipoEnum.Emitidos) {
                str = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx";
            }
            else if (tipo == DescargaTipoEnum.Recibidos) {
                str = "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx";
            }
            try {
                try {
                    enumerator = this.webSAT.Document.Links.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        HtmlElement current = (HtmlElement)enumerator.Current;
                        if (current.GetAttribute("href") != str) {
                            continue;
                        }
                        current.InvokeMember("click");
                        break;
                    }
                }
                finally {
                    if (enumerator is IDisposable) {
                        (enumerator as IDisposable).Dispose();
                    }
                }
            }
            catch (Exception exception) {
                Console.WriteLine("ClickSelector: " + exception.Message);
            }
        }

        public void LlenarLista() {
            DateTime dateTime = this.Configuracion.FechaInicial.AddDays(-1);
            DateTime f2 = this.Configuracion.FechaFinal;
            f2 = f2.AddDays(1);

            while (dateTime < f2) {
                dateTime = dateTime.AddDays(1);
                DescargaFechas500 fecha500 = new DescargaFechas500() {
                    Fecha = dateTime
                };

                if (this.Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                    this.ListaFechasRecibidos.Add(fecha500);
                }
                if (this.Configuracion.Tipo != DescargaTipoEnum.Emitidos) {
                    continue;
                }
                this.ListaFechasEmitidos.Add(fecha500);
            }
        }

        public void DownloadFileCancelado(DescargaResponse objeto) {
            try {
                if (File.Exists(System.IO.Path.Combine(objeto.PathXML, objeto.KeyName() + ".txt"))) {
                    //this.Message(string.Format("El archivo {0} ya existe.", objeto.KeyName()));
                }
                else {
                    string NewFileName = System.IO.Path.Combine(objeto.PathXML, objeto.KeyName() + ".txt");

                    if (File.Exists(NewFileName))
                        File.Delete(NewFileName);

                    // 0 UUID 1 RFC Emisor 2 Nombre Emisor 3 RFC Receptor 4 Nombre Receptor 5 Fecha Emision 6 Fecha Timbrado 7 PAC 8 Total 9 Tipo CFDI 10 Estatus 11 Fecha Cancelacion
                    string[] EscribeArchSal = new string[]
                    {
                        "UUID.....................", objeto.IdDocumento,
                        "RFC del Emisor...........", objeto.EmisorRFC,
                        "Nombre del Emisor........", objeto.Emisor,
                        "RFC del Receptor.........", objeto.ReceptorRFC,
                        "Nombre del Receptor......", objeto.Receptor,
                        "Fecha de Emision ........", objeto.FechaEmision.ToShortDateString(),
                        "Fecha de Timbrado........", objeto.FechaCerfificacion.ToShortDateString(),
                        "Fecha de Cancelación.....", objeto.FechaStatusCancela.ToString(),
                        "PAC......................", objeto.PACCertifica,
                        "Total del CFDI...........", objeto.Total.ToString(),
                        "Tipo de CFDI.............", objeto.Efecto,
                        "Estatus del CFDI.........", objeto.Estado
                    };

                    FilesExtendedService.WriteFileText(NewFileName, string.Join("\r\n", EscribeArchSal));
                    //this.Message(string.Format("Creando el archivo {0}.\n", objeto.KeyName()));
                }
            }
            catch (Exception ex) {
                Console.WriteLine("DownloadFileCancelado: " + ex.Message);
                //this.Message("ERROR AL TRATAR DE DESCARGAR EL XML CANCELADO: " + exception.Message);
                //throw new Exception(exception.Message);
            }
        }

        public void GoUrl(string url) {
            Application.DoEvents();
            try {
                this.webSAT.Navigate(url, "_self", null, "User-Agent: Luke's Web Browser");
            }
            catch (Exception exception1) {
                Console.WriteLine(exception1.Message);
                Exception exception = exception1;
                MessageBox.Show("GoURL: " + exception.Message, "Navigate Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public HtmlElement GetElementHtmlById(string id, ref WebBrowser webBrowser1) {
            HtmlElement elementById = null;
            try {
                elementById = webBrowser1.Document.GetElementById(id);
            }
            catch (Exception exception) {
                Console.WriteLine("GetElementHtmlById: " + exception.Message);
            }
            return elementById;
        }

        public void AuthenticateComun() {
            try {
                this.TimerCloseAlert.Enabled = true;
                this.LbMensaje.Text = string.Format("Iniciando sesion del usuario {0}.", this.Configuracion.RFC.Trim().ToUpper());

                string postData = string.Format("&Ecom_User_ID={0}&Ecom_Password={1}&userCaptcha={2}&submit=Enviar", this.Configuracion.RFC, this.Configuracion.CIEC, this.Configuracion.Captcha);
                this.LbMensaje.Text = "Datos a posterar en la url  " + postData;

                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                byte[] bytes = encoding.GetBytes(postData);

                this.LbMensaje.Text = "Navegando a la url " + this.webSAT.Url.ToString();
                this.webSAT.Navigate(this.webSAT.Url, string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");

                #region ESPERA A QUE SE CARGUE LA PAGINA WEB Y SE PROCESE EL METODOD wbSAT_DocumentCompleted

                bool seProcesoWBSat = false;

                int intentos = 0;
                while (true) {
                    Application.DoEvents();
                    if (!this.webSAT.IsBusy) {
                        if (seProcesoWBSat) {
                            break;
                        }

                        if (intentos > 100000) {
                            this.LbError.Text = "CONTRASEÑA CIEC ..... ";
                            this.LbMensaje.Text = "AVISO IMPORTANTE: CONTRAEÑA CIEC INCORRECTA  " + intentos.ToString();
                            LogErrorService.LogWrite(this.webSAT.Url.ToString());
                            LogErrorService.LogWrite(this.webSAT.Document.Body.InnerHtml.ToString());
                            break;
                        }
                        intentos = intentos + 1;
                    }
                }

                #endregion
            }
            catch (Exception ex) {
                LogErrorService.LogWrite("AuthenticateComun");
                LogErrorService.LogWrite(this.webSAT.Url.ToString());
                LogErrorService.LogWrite(this.webSAT.Document.Body.InnerHtml.ToString());
                Console.WriteLine("AuthenticateComun: " + ex.Message);
            }
        }

        public bool EstaEnSelector() {
            bool flag = false;
            if ((new Regex("Seleccione la opción deseada")).Matches(this.webSAT.Document.Body.InnerHtml).Count > 0) {
                flag = true;
            }
            return flag;
        }

        public bool ErrroDeAutentificacion() {
            bool flag = false;
            try {
                Regex regex = new Regex("Identity Provider");
                string innerHtml = "";
                if (this.webSAT.Document.Body.InnerHtml != null) {
                    innerHtml = this.webSAT.Document.Body.InnerHtml;
                }
                if (regex.Matches(innerHtml).Count > 0) {
                    flag = true;
                }
            }
            catch (Exception exception) {
                Console.WriteLine("ErrorAutenticacion: " + exception.Message);
            }
            return flag;
        }

        public bool ErrroDeCredenciales() {
            bool flag = false;
            try {
                if ((new Regex("Unable to complete request at this time")).Matches(this.webSAT.Document.Body.InnerHtml).Count > 0) {
                    flag = true;
                }
            }
            catch (Exception exception) {
                Console.WriteLine("ErrorCredenciales: " + exception.Message);
            }
            return flag;
        }

        public bool ExisteUpdateProgres() {
            bool flag = false;
            HtmlElement elementById = this.webSAT.Document.GetElementById("ctl00_MainContent_UpdateProgress1");
            if (elementById != null) {
                flag = (elementById.Style != "display: none;" ? true : false);
            }
            return flag;
        }

        public int GetCuantosXML() {
            IEnumerator enumerator = null;
            IEnumerator enumerator1 = null;
            int num = 0;
            HtmlElementCollection elementsByTagName = this.webSAT.Document.GetElementsByTagName("table");
            try {
                enumerator = elementsByTagName.GetEnumerator();
                while (enumerator.MoveNext()) {
                    HtmlElement current = (HtmlElement)enumerator.Current;
                    try {
                        enumerator1 = current.GetElementsByTagName("tr").GetEnumerator();
                        while (enumerator1.MoveNext()) {
                            if (((HtmlElement)enumerator1.Current).GetElementsByTagName("span").GetElementsByName("BtnDescarga").Count != 1) {
                                continue;
                            }
                            num = checked(num + 1);
                        }
                    }
                    finally {
                        if (enumerator1 is IDisposable) {
                            (enumerator1 as IDisposable).Dispose();
                        }
                    }
                }
            }
            finally {
                if (enumerator is IDisposable) {
                    (enumerator as IDisposable).Dispose();
                }
            }
            return num;
        }

        public int GetCuantosXML2() {
            int num = 0;
            string innerHtml = "";
            try {
                innerHtml = this.GetElementHtmlById("ctl00_MainContent_UpnlResultados", ref this.webSAT).InnerHtml;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                innerHtml = "";
                num = 0;
            }

            if (innerHtml != "") {
                Regex regx = new Regex("'.*?'");
                try {
                    num = regx.Matches(innerHtml).Count;
                }
                catch (Exception ex) {
                    num = 0;
                    Console.WriteLine("GetCuantosXML: " + ex.Message);
                }
            }

            return num;
        }

        public bool GetLimiteDe500() {
            bool flag = false;
            try {
                HtmlElement elementHtmlById = this.GetElementHtmlById("ctl00_MainContent_PnlLimiteRegistros", ref this.webSAT);
                if (elementHtmlById == null) {
                    flag = false;
                }
                else {
                    //string innerHtml = elementHtmlById.InnerHtml;
                    flag = true;
                }
            }
            catch (Exception exception) {
                Console.WriteLine("GetLimite500: " + exception.Message);
                flag = false;
            }
            return flag;
        }

        public bool GetNavegadorCerrado() {
            bool flag = false;
            Regex regex = new Regex("cerrar completamente su navegador.");
            try {
                if (this.webSAT.Document.Body == null)
                    return true;

                if (regex.Matches(this.webSAT.Document.Body.InnerHtml).Count > 0) {
                    flag = true;
                }
            }
            catch (Exception ex) {
                Console.WriteLine("GeNavegadirCerrado: " + ex.Message);
                flag = false;
            }
            return flag;
        }

        public string[] GetTiempoInteger(int tiempo) {
            long integer = tiempo;
            long num = 3600;
            long num1 = 60;
            long num2 = 0;
            num2 = (checked((int)Math.Round(Math.Truncate((double)integer / (double)num))));
            long num3 = checked(integer - checked(num2 * num));
            long num4 = 0;
            num4 = (checked((int)Math.Round(Math.Truncate((double)num3 / (double)num1))));
            long num5 = checked(num3 - checked(num4 * num1));
            long num6 = 0;
            num6 = num5;
            string[] devuelto = { null, num2.ToString(), num4.ToString(), num6.ToString() };
            return devuelto;
        }

        public bool UrlLoad(string temp) {
            bool flag = false;
            if (this.webSAT.Url.ToString().Contains(temp)) {
                flag = true;
            }
            return flag;
        }

        public bool UrlErroLogin(string url) {
            bool flag = false;
            if (!(url.Contains("SATx509Custom") & url != this.UrlError1)) {
                flag = false;
            }
            else {
                url = url.Substring(0, 62);
                int integer = 0;
                integer = Convert.ToInt32(url.Split('=')[2]);

                if (integer >= 1) {
                    flag = true;
                }
            }
            return flag;
        }

        public bool CredencialesNoValidas() {
            bool flag = false;
            Regex regex = new Regex("El RFC o contrase");
            long count = 0;
            try {
                string innerHtml = this.webSAT.Document.Body.InnerHtml;
                count = regex.Matches(innerHtml).Count;
            }
            catch (Exception exception) {
                Console.WriteLine("CredencialesNoValidas: " + exception.Message);
                count = 0;
            }
            if (count > 0) {
                flag = true;
            }
            return flag;
        }

        // es probable desaparecer porque solo se usa para la ventana
        public void LoginScrapSAT() {
            if (this.webSAT.ReadyState == WebBrowserReadyState.Complete & (this.UrlLoad(this.UrlError2) | this.webSAT.Url.ToString() == this.UrlFailCaptcha)) {
                try {
                    this.webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_User_ID")[0].SetAttribute("value", this.Configuracion.RFC);
                    this.webSAT.Document.GetElementsByTagName("input").GetElementsByName("Ecom_Password")[0].SetAttribute("value", this.Configuracion.CIEC);
                }
                catch (Exception ex) {
                    Console.WriteLine("LoginScraptSAT: " + ex.Message);
                }
            }
        }

        public static bool WriteFileFromB64(string base64, string directorio, string archivo) {
            bool result = false;
            byte[] numArray;

            if ((base64 == null ? true : base64.Length == 0)) {
                numArray = null;
            }
            else {
                try {
                    numArray = Convert.FromBase64String(base64);
                }
                catch (FormatException fex) {
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64, Environment.NewLine), fex);
                }
            }

            if (numArray != null) {
                if (!Directory.Exists(directorio)) {
                    Directory.CreateDirectory(directorio);
                }

                try {
                    using (FileStream fileStream = File.Create(Path.Combine(directorio, archivo))) {
                        fileStream.Write(numArray, 0, checked(numArray.Length));
                        fileStream.Close();
                        result = true;
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine("WriteFileFromB64: " + ex.Message);
                }
            }

            return result;
        }

        public bool HayDiasPorRevisar(List<DescargaFechas500> objetos) {
            List<DescargaFechas500>.Enumerator enumerator = new List<DescargaFechas500>.Enumerator();
            bool flag = false;
            try {
                enumerator = objetos.GetEnumerator();
                while (enumerator.MoveNext()) {
                    if (!(enumerator.Current.Status == DescargaLapsoStatusEnum.Pendiente)) {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            return flag;
        }

        public bool HayRangosPorRevisar(List<DescargaLapso> objetos) {
            List<DescargaLapso>.Enumerator enumerator = new List<DescargaLapso>.Enumerator();
            bool flag = false;
            try {
                enumerator = objetos.GetEnumerator();
                while (enumerator.MoveNext()) {
                    if (!(enumerator.Current.Status == DescargaLapsoStatusEnum.Pendiente)) {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
            }
            finally {
                ((IDisposable)enumerator).Dispose();
            }
            return flag;
        }

        public void CrearLapsosEmitidos(int i) {
            if (this.ListLapsosEmitidos[i].Status == DescargaLapsoStatusEnum.X) {
                if (this.ListLapsosEmitidos[i].SegundoFinal > this.ListLapsosEmitidos[i].SegundoInicial) {
                    this.LbMensaje.Text = "Dividiendo Lapsos ...";
                    DescargaLapso lapso = new DescargaLapso();
                    DescargaLapso objectValue = new DescargaLapso();
                    int integer = 0;
                    int num = 0;
                    long num1 = checked((long)Math.Truncate((double)((checked(this.ListLapsosEmitidos[i].SegundoFinal - this.ListLapsosEmitidos[i].SegundoInicial))) / 2));
                    integer = this.ListLapsosEmitidos[i].SegundoInicial + (int)num1;
                    num = checked(integer + 1);
                    lapso.Fecha = this.ListLapsosEmitidos[i].Fecha;
                    lapso.SegundoInicial = this.ListLapsosEmitidos[i].SegundoInicial;
                    lapso.SegundoFinal = integer;
                    lapso.Status = DescargaLapsoStatusEnum.Pendiente;
                    objectValue.Fecha = this.ListLapsosEmitidos[i].Fecha;
                    objectValue.SegundoInicial = num;
                    objectValue.SegundoFinal = this.ListLapsosEmitidos[i].SegundoFinal;
                    objectValue.Status = DescargaLapsoStatusEnum.Pendiente;
                    this.ListLapsosEmitidos.Add(lapso);
                    this.ListLapsosEmitidos.Add(objectValue);
                    this.IndexLapsoEmitido = i;
                    return;
                }
                this.ListLapsosEmitidos[i].Status = DescargaLapsoStatusEnum.B;
                this.ListLapsosEmitidosNoDescargables.Add(this.ListLapsosEmitidos[i]);
            }
        }

        public void CrearLapsosRecibidos(int i) {
            if (this.ListLapsosRecibidos[i].Status == DescargaLapsoStatusEnum.X) {
                if (this.ListLapsosRecibidos[i].SegundoFinal > this.ListLapsosRecibidos[i].SegundoInicial) {
                    this.LbMensaje.Text = "Dividiendo Lapsos ...";
                    DescargaLapso lapso = new DescargaLapso();
                    DescargaLapso lapso2 = new DescargaLapso();
                    int integer = 0;
                    int num = 0;
                    long num1 = checked((long)Math.Round((double)((checked(this.ListLapsosRecibidos[i].SegundoFinal - this.ListLapsosRecibidos[i].SegundoInicial))) / 2));
                    integer = this.ListLapsosRecibidos[i].SegundoInicial + (int)num1;
                    num = checked(integer + 1);
                    lapso.Fecha = this.ListLapsosRecibidos[i].Fecha;
                    lapso.SegundoInicial = this.ListLapsosRecibidos[i].SegundoInicial;
                    lapso.SegundoFinal = integer;
                    lapso.Status = DescargaLapsoStatusEnum.Pendiente;
                    lapso2.Fecha = this.ListLapsosRecibidos[i].Fecha;
                    lapso2.SegundoInicial = num;
                    lapso2.SegundoFinal = this.ListLapsosRecibidos[i].SegundoFinal;
                    lapso2.Status = DescargaLapsoStatusEnum.Pendiente;
                    this.ListLapsosRecibidos.Add(lapso);
                    this.ListLapsosRecibidos.Add(lapso2);
                    this.IndexLapsoRecibido = i;
                    Console.WriteLine("Lapso1 -> " + lapso.ToString());
                    Console.WriteLine("Lapso2 -> " + lapso2.ToString());
                    return;
                }
                this.ListLapsosRecibidos[i].Status = DescargaLapsoStatusEnum.B;
                this.ListLapsosRecibidosNoDescargables.Add(this.ListLapsosRecibidos[i]);
            }
        }

        public void IterarDiasEmitidos() {
            if (this.ListaFechasEmitidos[this.IndexDiaEmitido].Status == DescargaLapsoStatusEnum.Pendiente) {
                this.ListaFechasEmitidos[this.IndexDiaEmitido].Status = DescargaLapsoStatusEnum.Ok;
                this.ListLapsosEmitidos.Clear();
                this.IndexLapsoEmitido = 0;
                DescargaLapso lapso = new DescargaLapso() {
                    Fecha = ParseConvert.ConvertDateTime(this.ListaFechasEmitidos[this.IndexDiaEmitido].Fecha),
                    SegundoInicial = 0,
                    SegundoFinal = 86399,
                    Status = DescargaLapsoStatusEnum.Pendiente
                };
                this.ListLapsosEmitidos.Add(lapso);
            }
        }

        public void IterarDiasRecibidos() {
            if (this.ListaFechasRecibidos[this.IndexDiaRecibido].Status == DescargaLapsoStatusEnum.Pendiente) {
                this.ListaFechasRecibidos[this.IndexDiaRecibido].Status = DescargaLapsoStatusEnum.Ok;
                this.ListLapsosRecibidos.Clear();
                this.IndexLapsoRecibido = 0;
                DescargaLapso lapso = new DescargaLapso() {
                    Fecha = ParseConvert.ConvertDateTime(this.ListaFechasRecibidos[this.IndexDiaRecibido].Fecha),
                    SegundoInicial = 0,
                    SegundoFinal = 86399,
                    Status = DescargaLapsoStatusEnum.Pendiente
                };
                this.ListLapsosRecibidos.Add(lapso);
            }
        }

        public void ProcessResult(ref int totArchivosDescargados, ref DateTime ultimaFechaEmision, ref WebBrowser webSAT) {
            try {
                if (!(webSAT.Document == null)) {
                    // hace la busqueda por una tabla
                    HtmlElement elementTabla = webSAT.Document.GetElementById("ctl00_MainContent_tblResult");

                    //Obtiene los Tr principales antes tenia el problema con las subtablas donde esta el boton
                    HtmlElementCollection elementBtnDescargatr = elementTabla.GetElementsByTagName("tbody")[0].Children;

                    foreach (HtmlElement elementRenglon in elementBtnDescargatr) {
                        //ya no es una imagen es un span
                        string urlFileDownload = this.GetElementURL(elementRenglon, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnDescarga");
                        string urlPdfDownload = this.GetElementURL(elementRenglon, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnRI");
                        string urlRecuperaAcuseFinal = this.GetElementURL(elementRenglon, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnRecuperaAcuse");

                        IEnumerable<HtmlElement> htmlElementos = elementRenglon.GetElementsByTagName("span").Cast<HtmlElement>();
                        IEnumerable<HtmlElement> htmlElementos1 = htmlElementos.Where<HtmlElement>((HtmlElement span) => {
                            bool innerText = span.InnerText != "";
                            return innerText;
                        });

                        string datosDelRenglonTest = htmlElementos1.Aggregate<HtmlElement, string>("", (string current, HtmlElement span) => {
                            string str = string.Concat(current, span.InnerText, "|");
                            return str;
                        });
                        FilesExtendedService.WriteFileText(@"C:\Jaeger\Jaeger.Log\DescargaTabla_" + this.configuracion.TipoText + ".log", datosDelRenglonTest + "\r\n", true);
                        // sino son los encabezados
                        if (!datosDelRenglonTest.StartsWith("  Acciones")) {
                            char[] chrArray = new char[] { '|' };
                            string[] aDatosDelRenglon = datosDelRenglonTest.Split(chrArray);
                            // reparamos las posiciones para obtener la tabla correcta
                            if (aDatosDelRenglon.Length == 16) {
                                datosDelRenglonTest = "||" + datosDelRenglonTest.Replace('$', ' ');
                            }
                            else if (aDatosDelRenglon.Length == 15) {
                                datosDelRenglonTest = "|||" + datosDelRenglonTest.Replace('$', ' ');
                            }
                            else if (aDatosDelRenglon.Length == 14) {
                                datosDelRenglonTest = "||||" + datosDelRenglonTest.Replace('$', ' ');
                            }
                            else if (aDatosDelRenglon.Length == 18) {

                            }
                            else {
                                FilesExtendedService.WriteFileText(@"C:\Jaeger\Jaeger.Log\DescargaTablaNoIdent_" + this.configuracion.TipoText + ".log", DateTime.Now.ToString() + "+" + datosDelRenglonTest + "\r\n", true);
                            }

                            aDatosDelRenglon = datosDelRenglonTest.Split(chrArray);
                            var vigente = new DescargaResponse();
                            vigente.Tipo = this.configuracion.TipoText;
                            vigente.IdDocumento = aDatosDelRenglon[3]; // 3 UUID
                            vigente.EmisorRFC = aDatosDelRenglon[4]; // 4 RFC Emisor 
                            vigente.Emisor = aDatosDelRenglon[5]; // 5 Nombre Emisor
                            vigente.ReceptorRFC = aDatosDelRenglon[6]; // 6 RFC Receptor
                            vigente.Receptor = aDatosDelRenglon[7]; // 7 Nombre Receptor
                            vigente.FechaEmision = ParseConvert.ConvertDateTime(aDatosDelRenglon[8]); // 8 Fecha Emision
                            vigente.FechaCerfificacion = ParseConvert.ConvertDateTime(aDatosDelRenglon[9]); // 9 Fecha Timbrado
                            vigente.PACCertifica = aDatosDelRenglon[10]; // 10 PAC 11 
                            vigente.Total = Convert.ToDecimal(aDatosDelRenglon[11].Replace('$', ' ')); // 11 Total
                            vigente.Efecto = aDatosDelRenglon[12]; // 12 Tipo CFDI
                            vigente.StatusCancelacion = aDatosDelRenglon[13];
                            vigente.Estado = aDatosDelRenglon[14]; // 14 Estatus
                            vigente.StatusProcesoCancelar = aDatosDelRenglon[15];
                            vigente.FechaStatusCancela = ParseConvert.ConvertDateTime(aDatosDelRenglon[16]);
                            vigente.PathXML = this.CrearEstructuraDirectorioDescarga(vigente);
                            vigente.PathPDF = vigente.PathXML;
                            vigente.UrlXML = urlFileDownload;
                            vigente.UrlPDF = urlPdfDownload;
                            vigente.UrlRecuperaAcuseFinal = urlRecuperaAcuseFinal;

                            this.descargaResultados.Add(vigente);
                            totArchivosDescargados = totArchivosDescargados + 1;
                        }

                        if (totArchivosDescargados >= MaximoDescargaSAT)
                            return;
                    }
                }
                else {
                    return;
                }
            }
            catch (Exception ex) {
                Console.WriteLine("ProcessResult: " + ex.Message);
                this.ErrorLog = "ProcessResult: " + ex.Message;
            }
        }

        public void ProcessResult(ref int totArchivosDescargados, ref WebBrowser webSAT) {
            HtmlElement elementTabla = webSAT.Document.GetElementById("ctl00_MainContent_tblResult");
            HtmlAgilityPack.HtmlDocument _doc = new HtmlAgilityPack.HtmlDocument();
            _doc.LoadHtml(elementTabla.OuterHtml);

            try {
                int num = -1;
                foreach (HtmlNode array in _doc.DocumentNode.Descendants("tr").ToArray<HtmlNode>()) {
                    if ((!array.HasChildNodes || array.ChildNodes == null ? false : array.ChildNodes.Count.Equals(15))) {
                        var vigente = new DescargaResponse();
                        num = 0;
                        Label1:
                        foreach (HtmlNode htmlNode1 in from x in array.ChildNodes where x.Name == "td" select x) {
                            if (htmlNode1.Name == "td") {
                                switch (num) {
                                    case 0: {
                                            vigente.UrlXML = this.GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnDescarga");
                                            vigente.UrlDetalle = this.GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnVerDetalle");
                                            vigente.UrlPDF = this.GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/RepresentacionImpresa.aspx?Datos={0}", "BtnRI");
                                            vigente.UrlRecuperaAcuseFinal = this.GetElementURL(htmlNode1, "https://portalcfdi.facturaelectronica.sat.gob.mx/{0}", "BtnRecuperaAcuse");
                                            break;
                                        }
                                    case 1: {
                                            vigente.IdDocumento = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 2: {
                                            vigente.EmisorRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 3: {
                                            vigente.Emisor = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 4: {
                                            vigente.ReceptorRFC = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 5: {
                                            vigente.Receptor = htmlNode1.InnerText.Replace("&amp;", "&").Replace("&#65533;", "Ñ");
                                            break;
                                        }
                                    case 6: {
                                            vigente.FechaEmision = ParseConvert.ConvertDateTime(htmlNode1.InnerText.Replace("T", " ").Replace("&#58;", ":"));
                                            break;
                                        }
                                    case 7: {
                                            vigente.FechaCerfificacion = ParseConvert.ConvertDateTime(htmlNode1.InnerText.Replace("T", " ").Replace("&#58;", ":"));
                                            break;
                                        }
                                    case 8: {
                                            vigente.PACCertifica = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 9: {
                                            vigente.Total = Convert.ToDecimal(htmlNode1.InnerText.Replace("$", "").Replace("&#36;", "").Replace(",", ""));
                                            break;
                                        }
                                    case 10: {
                                            vigente.Efecto = htmlNode1.InnerText;
                                            break;
                                        }
                                    case 11: {
                                            vigente.StatusCancelacion = htmlNode1.InnerText.Trim();
                                            break;
                                        }
                                    case 12: {
                                            vigente.Estado = htmlNode1.InnerText.Trim();
                                            break;
                                        }
                                    case 13: {
                                            vigente.StatusProcesoCancelar = htmlNode1.InnerText.Trim();
                                            break;
                                        }
                                    case 14: {
                                            vigente.FechaStatusCancela = ParseConvert.ConvertDateTime(htmlNode1.InnerText);
                                            break;
                                        }
                                    default: {
                                            //goto Label1;
                                            Console.WriteLine("ProcessResult: ");
                                            break;
                                        }
                                }
                                num++;
                            }
                        }
                        if (!string.IsNullOrEmpty(vigente.IdDocumento)) {
                            vigente.PathXML = this.CrearEstructuraDirectorioDescarga(vigente);
                            vigente.Tipo = this.configuracion.TipoText;
                            this.descargaResultados.Add(vigente);
                            totArchivosDescargados++;
                        }
                    }
                }
            }
            catch (Exception ex) {
                this.ErrorLog = string.Format("ProcessResult: {0}", ex.Message);
                Console.WriteLine("ProcessResult: " + this.ErrorLog);
            }
        }

        /// <summary>
        /// procedimiento para la descarga
        /// </summary>
        public async void ProcessDownloads() {
            this.TimerEvitarCierre.Tick += new EventHandler(this.TimerEvitarCierre_Tick);
            ModoDescarga = true;
            int contador = 0;
            this.TimerEvitarCierre.Enabled = true;
            Progress<DescargaMasivaProgreso> reporte = new Progress<DescargaMasivaProgreso>();
            reporte.ProgressChanged += Reporte_ProgressChanged;
            this.Resultados = await DescargaService.Run(this.Resultados, this.webSAT.Url.ToString(), reporte);
            contador = 0;
            this.LbMensaje.Text = "Verificando archivos ...";

            foreach (DescargaResponse item in this.Resultados) {
                if (item.XmlContentB64 != null && item.Estado != "Cancelado") {
                    if (item.XmlContentB64 != "") {
                        string content = "";
                        try {
                            // validamos el xml
                            content = System.Text.Encoding.UTF8.GetString(FilesExtendedService.FromBase64(item.XmlContentB64));
                        }
                        catch (Exception ex) {
                            this.LbError.Text = "ProcessDownloads: " + ex.Message;
                            Console.WriteLine("ProcessDownloads: " + ex.Message);
                            content = "";
                        }

                        if (GetLimiteDescargaSAT(content)) {
                            item.Result = "Limite";
                            item.XmlContentB64 = null;
                        }
                        else if (!GetValidXml(content)) {
                            if (content.Contains("error al intentar descargar el archivo")) {
                                item.Result = "NoDisponible";
                            }
                            else {
                                item.Result = "NoValido";
                            }
                            WriteFileFromB64(item.XmlContentB64, item.PathXML, item.KeyName() + ".html");
                            item.XmlContentB64 = null;
                        }
                        else {
                            WriteFileFromB64(item.XmlContentB64, item.PathXML, item.KeyNameXml());
                            item.Result = "Descargado";
                        }
                    }
                }

                // descargar archivos adicionales
                if (item.PdfRepresentacionImpresaB64 != null) {
                    // en el caso de la representacion vamos omitir guardarla en el repositorio
                    if (WriteFileFromB64(item.PdfRepresentacionImpresaB64, item.PathXML, item.KeyNamePdf())) {
                        item.PdfRepresentacionImpresaB64 = null;
                    }
                }

                // acuse final de la cancelacion, este archivo si lo guardamos en la base
                if (item.PdfAcuseFinalB64 != null) {
                    WriteFileFromB64(item.PdfAcuseFinalB64, item.PathXML, item.KeyNameAcuseFinal());
                }
                contador = contador + 1;
                this.LbMensaje.Text = string.Concat("Verificando ... ", contador);
                Application.DoEvents();
            }

            // cerrar la pagina
            try {
                this.GetElementHtmlById("anchorClose", ref this.webSAT).InvokeMember("click");
            }
            catch (Exception exception3) {
                Console.WriteLine("ProcessDownloads" + exception3.Message);
            }

            this.Cerrando = true;
            this.TimerClose.Enabled = true;
            this.TimerEvitarCierre.Enabled = false;
            this.ModoDescarga = false;
            this.OnProgressChanged(new DescargaMasivaProgreso(100, this.Resultados.Count, this.Resultados.Count, "Terminado"));
            this.OnCompletedProcess(new DescargaMasivaCompletedProcess("Descarga Terminada", ""));
        }
        #endregion
    }
}
