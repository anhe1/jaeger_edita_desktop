﻿// develop:
// purpose: clase para descarga masiva de comprobantes, para emitidos y recibidos;
using System;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.Repositorio.Abstractions;

namespace Jaeger.Repositorio.V3 {
    public class DescargaXml3SAT : DescargaXmlSATComun, IDescargaMasiva {
        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml3SAT(TextBox txtLog, TextBox txtStatus) {
            LbMensaje = txtLog;
            LbError = txtStatus;
            InicializeComponents();
            ModoDescarga = false;
        }

        public new string Version {
            get { return "3.0"; }
        }

        #region metodos publicos

        public void Consultar(WebBrowser _wsSat2, ConfiguracionDetail conf) {
            Configuracion = conf;
            _WebSAT = _wsSat2;
            _WebSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(WebBrowser_DocumentCompleted);
            Configuracion.Tipo = DescargaTipoEnum.Emitidos;
            LlenarLista();
            IterarDiasEmitidos();

            TimerError.Enabled = true;
            Authenticate();
        }

        public void Reset() {
            TimerLogin.Enabled = false;
            TimerClose.Enabled = false;
            TimerError.Enabled = false;
            TimerCloseAlert.Enabled = false;
            TimerFiltrarRecibidas.Enabled = false;
            TimerFiltrarEmitidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = false;
            TimerRecopilarEmitidas.Enabled = false;
            TimerEvitarCierre.Enabled = false;
        }

        #endregion

        #region metodos privados

        private void InicializeComponents() {
            TimerClose.Tick += new EventHandler(TimerClose_Tick);

            TimerError.Tick += new EventHandler(TimerError_Tick);
            TimerFiltrarEmitidas.Tick += new EventHandler(TimerFiltrarEmitidas_Tick);
            TimerFiltrarRecibidas.Tick += new EventHandler(TimerFiltrarRecibidas_Tick);
            TimerLogin.Tick += new EventHandler(TimerLogin_Tick);
            TimerRecopilarEmitidas.Tick += new EventHandler(TimerRecopilarEmitidas_Tick);
            TimerRecopilarRecibidas.Tick += new EventHandler(TimerRecopilarRecibidas_Tick);

        }

        private void ContinuarRevisandoEmitidos() {
            if (HayDiasPorRevisar(ListaFechasEmitidos)) {
                IndexDiaEmitido = checked(IndexDiaEmitido + 1);
                IterarDiasEmitidos();
                TimerFiltrarEmitidas.Enabled = true;
                return;
            }

            LbError.Text = "";
            LbMensaje.Text = "Descargar Recibidos";
            TimerFiltrarEmitidas.Enabled = false;
            TimerRecopilarEmitidas.Enabled = false;
            ErrorLog = "";
            Cerrando = false;
            Configuracion.Tipo = DescargaTipoEnum.Recibidos;
            IterarDiasRecibidos();
            GoUrl(UrlOculta);
        }

        private void ContinuarRevisandoRecibidos() {
            if (HayDiasPorRevisar(ListaFechasRecibidos)) {
                IndexDiaRecibido = checked(IndexDiaRecibido + 1);
                IterarDiasRecibidos();
                TimerFiltrarRecibidas.Enabled = true;
                return;
            }

            LbMensaje.Text = "Iniciar Descarga...";
            LbError.Text = "";
            TimerFiltrarRecibidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = false;
            ErrorLog = "";
            IniciarDescarga();
        }

        private void IniciarDescarga() {
            OnProgressChanged(new DescargaMasivaProgreso(0, Resultados.Count((p) => p.Result == ""), 0, string.Concat(Resultados.Count((p) => p.Result == ""), " archivos para descargar")));
            ProcessDownloads();
            OnProgressChanged(new DescargaMasivaProgreso(100, Resultados.Count((p) => p.Result == "Completado"), 0, "Descarga terminada."));
        }

        private void NextLapsoEmitido() {
            int count = checked(ListLapsosEmitidos.Count - 1);
            int num = 0;
            while (num <= count) {
                if (!(ListLapsosEmitidos[num].Status == DescargaLapsoStatusEnum.Pendiente)) {
                    num = checked(num + 1);
                } else {
                    IndexLapsoEmitido = num;
                    break;
                }
            }
            TimerRecopilarEmitidas.Enabled = false;
            TimerFiltrarEmitidas.Enabled = true;
        }

        private void NextLapsoRecibido() {
            int count = checked(ListLapsosRecibidos.Count - 1);
            int num = 0;
            while (num <= count) {
                if (!(ListLapsosRecibidos[num].Status == DescargaLapsoStatusEnum.Pendiente)) {
                    num = checked(num + 1);
                } else {
                    IndexLapsoRecibido = num;
                    break;
                }
            }
            TimerFiltrarRecibidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = true;
        }

        private void CloseDescarga() {
            TimerLogin.Enabled = false;
            TimerFiltrarEmitidas.Enabled = false;
            TimerRecopilarEmitidas.Enabled = false;
            TimerClose.Enabled = false;

            Cerrando = false;
            MessageBox.Show("El RFC o contraseña son incorrectos. Verifique su información e inténtelo de nuevo.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void TimerClose_Tick(object sender, EventArgs e) {
            LbMensaje.Text = "Cerrando sesión ...";

            if (GetNavegadorCerrado()) {
                try {
                    _WebSAT.Document.ExecCommand("ClearAuthenticationCache", false, null);
                } catch (Exception ex) {
                    Console.WriteLine("TimerClose: " + ex.Message);
                }
                TimerClose.Enabled = false;
                LbMensaje.Text = "Sesión Terminada ...";
                OnCompletedProcess(new DescargaMasivaCompletedProcess("El proceso concluyo satisfactoriamente!", ""));
                Cerrando = false;
            }
        }

        private void TimerError_Tick(object sender, EventArgs e) {
            if (ErrroDeAutentificacion()) {
                TimerError.Enabled = false;
                GoUrl(UrlAutentificacion);
            }
        }

        private void TimerFiltrarEmitidas_Tick(object sender, EventArgs e) {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null) {
                enabled = elementById.Enabled;
            }
            if (!ExisteUpdateProgres() & enabled) {
                TimerFiltrarEmitidas.Enabled = false;
                GetElementHtmlById("ctl00_MainContent_CldFechaInicial2_Calendario_text", ref _WebSAT).InnerText = ListLapsosEmitidos[IndexLapsoEmitido].Fecha.ToShortDateString();
                GetElementHtmlById("ctl00_MainContent_CldFechaFinal2_Calendario_text", ref _WebSAT).InnerText = ListLapsosEmitidos[IndexLapsoEmitido].Fecha.ToShortDateString();
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlHora");
                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosEmitidos[IndexLapsoEmitido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlMinuto");
                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosEmitidos[IndexLapsoEmitido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaInicial2_DdlSegundo");
                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosEmitidos[IndexLapsoEmitido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlHora");
                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosEmitidos[IndexLapsoEmitido].SegundoFinal)[1]);

                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlMinuto");
                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosEmitidos[IndexLapsoEmitido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFechaFinal2_DdlSegundo");
                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosEmitidos[IndexLapsoEmitido].SegundoFinal)[3]);
                }
                if (Configuracion.FiltrarPorRFC == true) {
                    GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref _WebSAT).SetAttribute("value", Configuracion.BuscarPorRFC);
                }
                GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref _WebSAT).SetAttribute("value", Configuracion.Status.ToString());
                GetElementHtmlById("ddlComplementos", ref _WebSAT).SetAttribute("value", Configuracion.Complemento.ToString());
                GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref _WebSAT).InvokeMember("click");
                LbMensaje.Text = string.Concat("Emitidas día ", ListLapsosEmitidos[IndexLapsoEmitido].Fecha, "...");
                TimerRecopilarEmitidas.Enabled = true;
            }
        }

        private void TimerFiltrarRecibidas_Tick(object sender, EventArgs e) {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null) {
                enabled = elementById.Enabled;
            }
            if (!ExisteUpdateProgres() & enabled) {
                TimerFiltrarRecibidas.Enabled = false;
                // introducir los datos de la fecha
                GetElementHtmlById("DdlAnio", ref _WebSAT).SetAttribute("value", ListaFechasRecibidos[IndexDiaRecibido].Fecha.Year.ToString(System.Globalization.CultureInfo.InvariantCulture));
                GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref _WebSAT).SetAttribute("value", ListaFechasRecibidos[IndexDiaRecibido].Fecha.Month.ToString("0"));
                GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref _WebSAT).InvokeMember("Change");
                GetElementHtmlById("ctl00_MainContent_CldFecha_DdlDia", ref _WebSAT).SetAttribute("value", ListaFechasRecibidos[IndexDiaRecibido].Fecha.Day.ToString("00"));
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoFinal)[1]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoFinal)[3]);
                }

                if (Configuracion.FiltrarPorRFC == true) {
                    GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref _WebSAT).SetAttribute("value", Configuracion.BuscarPorRFC);
                }

                GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref _WebSAT).SetAttribute("value", Configuracion.Status.ToString());
                GetElementHtmlById("ddlComplementos", ref _WebSAT).SetAttribute("value", Configuracion.Complemento);
                GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref _WebSAT).InvokeMember("click");
                LbMensaje.Text = string.Concat("Filtrando día ", ListLapsosRecibidos[IndexLapsoRecibido].Fecha.ToShortDateString(), "...");
                TimerRecopilarRecibidas.Enabled = true;
            }
        }

        private void TimerLogin_Tick(object sender, EventArgs e) {
            if (CredencialesNoValidas() | ErrroDeCredenciales()) {
                CloseDescarga();
            }
        }

        private void TimerRecopilarEmitidas_Tick(object sender, EventArgs e) {
            if (!ExisteUpdateProgres() & _WebSAT.ReadyState == WebBrowserReadyState.Complete & !_WebSAT.IsBusy) {
                TimerRecopilarEmitidas.Enabled = false;
                LbMensaje.Text = "Recopilando XML ...";
                if (GetCuantosXML() <= 0) {
                    ListLapsosEmitidos[IndexLapsoEmitido].Status = DescargaLapsoStatusEnum.Vacio;
                } else if (!GetLimiteDe500()) {
                    ListLapsosEmitidos[IndexLapsoEmitido].Status = DescargaLapsoStatusEnum.Ok;
                    DateTime ahora = DateTime.Now;
                    int countXML = 0;
                    ProcessResult(ref countXML, ref ahora, ref _WebSAT);
                    ListaFechasEmitidos[IndexDiaEmitido].Total = ListaFechasEmitidos[IndexDiaEmitido].Total + countXML;
                    ListLapsosEmitidos[IndexLapsoEmitido].Cuantos = countXML;
                    LbMensaje.Text = string.Concat("Recopilando ", countXML, " XML");
                    LbError.Text = "";
                    LbError.Text = LbMensaje.Text;
                } else {
                    LbMensaje.Text = "Creando Lapsos ...";
                    ListLapsosEmitidos[IndexLapsoEmitido].Status = DescargaLapsoStatusEnum.X;
                    CrearLapsosEmitidos(IndexLapsoEmitido);
                }
                if (HayRangosPorRevisar(ListLapsosEmitidos)) {
                    NextLapsoEmitido();
                    return;
                }
                ContinuarRevisandoEmitidos();
            }
        }

        private void TimerRecopilarRecibidas_Tick(object sender, EventArgs e) {
            if (!ExisteUpdateProgres() & _WebSAT.ReadyState == WebBrowserReadyState.Complete & !_WebSAT.IsBusy) {
                TimerRecopilarRecibidas.Enabled = false;
                LbMensaje.Text = "Recopilando XML ...";
                if (GetCuantosXML() <= 0) {
                    ListLapsosRecibidos[IndexLapsoRecibido].Status = DescargaLapsoStatusEnum.Vacio;
                } else if (!GetLimiteDe500()) {
                    ListLapsosRecibidos[IndexLapsoRecibido].Status = DescargaLapsoStatusEnum.Ok;
                    DateTime ahora = DateTime.Now;
                    int countXML = 0;
                    ProcessResult(ref countXML, ref ahora, ref _WebSAT);
                    ListaFechasRecibidos[IndexDiaRecibido].Total = ListaFechasRecibidos[IndexDiaRecibido].Total + countXML;
                    ListLapsosRecibidos[IndexLapsoRecibido].Cuantos = countXML;
                    LbMensaje.Text = string.Concat("Recopilando ", countXML, " XML");
                    LbError.Text = "";
                    LbError.Text = LbMensaje.Text;
                } else {
                    LbMensaje.Text = "Creando Lapsos ...";
                    ListLapsosRecibidos[IndexLapsoRecibido].Status = DescargaLapsoStatusEnum.X;
                    CrearLapsosRecibidos(IndexLapsoRecibido);
                }
                if (HayRangosPorRevisar(ListLapsosRecibidos)) {
                    NextLapsoRecibido();
                    return;
                }
                ContinuarRevisandoRecibidos();
            }
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            if (_WebSAT.Document != null && !Cerrando) {
                if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                    try {

                        if (ModoDescarga) {
                            return;
                        } else {
                            LbMensaje.Text = "Ingresando ...";
                            if (!UrlErroLogin(_WebSAT.Url.ToString())) {
                                if (UrlLoad(UrlRedir)) {
                                    GoUrl(UrlAutentificacion);
                                }
                                if (UrlLoad(UrlError2)) {
                                    LoginScrapSAT();
                                    TimerLogin.Enabled = true;
                                }
                                if (_WebSAT.Url.ToString().Contains("https://portalcfdi.facturaelectronica.sat.gob.mx") & EstaEnSelector() && !TrucoHide) {
                                    GoUrl(UrlOculta);
                                    TrucoHide = true;
                                }
                                if (_WebSAT.Url.ToString() == UrlOculta & EstaEnSelector()) {
                                    TrucoHide = false;
                                    if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                                        ClickEnSelector(DescargaTipoEnum.Emitidos);
                                    }
                                }
                                if (_WebSAT.Url.ToString() == UrlEmitidas & Configuracion.Tipo == DescargaTipoEnum.Emitidos | _WebSAT.Url.ToString() == UrlRecibidas) {
                                    GetElementHtmlById("ctl00_MainContent_RdoFechas", ref _WebSAT).InvokeMember("click");
                                    TimerLogin.Enabled = false;
                                    if (!Truco) {
                                        TimerFiltrarEmitidas.Enabled = true;
                                    }
                                }
                                if (_WebSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa=")) {
                                    GoUrl(UrlAutentificacion);
                                    return;
                                } else if (_WebSAT.Url.ToString() == UrlFailCaptcha) {
                                    LoginScrapSAT();
                                    TimerLogin.Enabled = true;
                                }

                                if (_WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")) {
                                    if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                                        _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                    }

                                    if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                                        _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                    }
                                }

                                if (_WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                                || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                                || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                                || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")
                                ) {
                                    if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                                        _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                    }

                                    if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                                        _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                    }
                                }
                            } else {
                                GoUrl(UrlError1);
                                return;
                            }
                        }
                    } catch (Exception exception) {
                        MessageBox.Show(exception.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                    if (ModoDescarga) {
                        return;
                    }
                    if (_WebSAT.Url.ToString() == UrlOculta & EstaEnSelector()) {
                        TrucoHide = false;
                        ClickEnSelector(Configuracion.Tipo);
                    }
                    if (_WebSAT.Url.ToString() == UrlRecibidas & Configuracion.Tipo == DescargaTipoEnum.Recibidos | _WebSAT.Url.ToString() == UrlRecibidas) {
                        GetElementHtmlById("ctl00_MainContent_RdoFechas", ref _WebSAT).InvokeMember("click");
                        TimerLogin.Enabled = false;
                        if (!Truco) {
                            TimerFiltrarRecibidas.Enabled = true;
                        }
                    }
                }
            }
            while (true) {
                Application.DoEvents();
                if (!_WebSAT.IsBusy) {
                    break;
                }
                if (_WebSAT.ReadyState == WebBrowserReadyState.Loading) {
                    Application.DoEvents();
                }
            }
        }

        private void Authenticate() {
            try {
                OnStartProcess(new DescargaMasivaStartProcess());
                base.AuthenticateC();
            } catch (Exception ex) {
                OnCompletedProcess(new DescargaMasivaCompletedProcess("Autenticación", ex.Message, true));
                LbError.Text = ex.Message;
            }
        }
        #endregion
    }
}
