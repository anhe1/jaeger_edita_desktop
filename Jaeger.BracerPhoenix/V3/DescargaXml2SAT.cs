﻿// develop: anhe
// purpose: descarga masiva de comprobantes fiscales recibidos
using System;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.Interface;
using Jaeger.Repositorio.ValueObjects;
using Jaeger.Repositorio.Abstractions;

namespace Jaeger.Repositorio.V3 {
    public class DescargaXml2SAT : DescargaXmlSATComun, IDescargaMasiva {

        private readonly Timer timerCloseAlert = new Timer() { Interval = 500, Enabled = false };

        /// <summary>
        /// constructor
        /// </summary>
        public DescargaXml2SAT(TextBox txtLog, TextBox txtStatus) {
            LbError = txtLog;
            LbMensaje = txtStatus;
            InicializeComponents();
            ModoDescarga = false;
        }

        public new string Version {
            get { return "3.0"; }
        }

        #region metodos publicos

        public void Consultar(WebBrowser wsSat2, ConfiguracionDetail conf) {
            _WebSAT = wsSat2;
            Configuracion = conf;
            _WebSAT.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(WebBrowser_DocumentCompleted);
            ListLapsosRecibidosNoDescargables.Clear();
            LlenarLista();
            IterarDiasRecibidos();
            TimerError.Enabled = true;
            ModoDescarga = false;
            Authenticate();
        }

        public void Reset() {
            TimerLogin.Enabled = false;
            TimerClose.Enabled = false;
            TimerError.Enabled = false;
            TimerCloseAlert.Enabled = false;
            TimerFiltrarRecibidas.Enabled = false;
            TimerFiltrarEmitidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = false;
            TimerRecopilarEmitidas.Enabled = false;
            TimerEvitarCierre.Enabled = false;
        }

        #endregion

        #region metodos privados

        public void InicializeComponents() {
            // establecer los eventos de los objetos timer's
            TimerClose.Tick += new EventHandler(TimerClose_Tick);
            TimerError.Tick += new EventHandler(TimerError_Tick);
            TimerRecopilarRecibidas.Tick += new EventHandler(TimerRecopilarRecibidas_Tick);
            TimerLogin.Tick += new EventHandler(TimerLogin_Tick);
            timerCloseAlert.Tick += new EventHandler(TimerCloseAlert_Tick);
            TimerFiltrarRecibidas.Tick += new EventHandler(TimerFiltrarRecibidas_Tick);
        }

        private void Authenticate() {
            try {
                OnStartProcess(new DescargaMasivaStartProcess());
                base.AuthenticateC();
            } catch (Exception ex) {
                OnCompletedProcess(new DescargaMasivaCompletedProcess("Autenticación", ex.Message, true));
                LbError.Text = ex.Message;
            }
        }

        private void ContinuarRevisando() {
            if (HayDiasPorRevisar(ListaFechasRecibidos)) {
                IndexDiaRecibido = checked(IndexDiaRecibido + 1);
                IterarDiasRecibidos();
                TimerFiltrarRecibidas.Enabled = true;
                return;
            }

            LbMensaje.Text = "Iniciando descarga";
            LbError.Text = "";
            ErrorLog = "";
            TimerFiltrarRecibidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = false;
            Cerrando = false;
            IniciarDescarga();
        }

        private void NextRangos() {
            int count = checked(ListLapsosRecibidos.Count - 1);
            int num = 0;
            while (num <= count) {
                if (!(ListLapsosRecibidos[num].Status == DescargaLapsoStatusEnum.Pendiente)) {
                    num = checked(num + 1);
                } else {
                    IndexLapsoRecibido = num;
                    break;
                }
            }
            TimerRecopilarRecibidas.Enabled = false;
            TimerFiltrarRecibidas.Enabled = true;
        }

        private void IniciarDescarga() {
            OnProgressChanged(new DescargaMasivaProgreso(0, Resultados.Count((p) => p.Result == ""), 0, string.Concat(Resultados.Count((p) => p.Result == ""), " archivos para descargar")));
            ProcessDownloads();
            OnProgressChanged(new DescargaMasivaProgreso(100, Resultados.Count((p) => p.Result == "Completado"), 0, "Descarga terminada."));
        }

        private void CloseDescarga() {
            TimerLogin.Enabled = false;
            TimerFiltrarRecibidas.Enabled = false;
            TimerRecopilarRecibidas.Enabled = false;
            TimerClose.Enabled = false;
            Cerrando = false;
            LbMensaje.Text = "Error en la sesion";
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
            if (_WebSAT.Document != null && !Cerrando) {
                try {
                    if (ModoDescarga) {
                        return;
                    } else {
                        LbMensaje.Text = "Ingresando ...";
                        if (!UrlErroLogin(_WebSAT.Url.ToString())) {
                            if (UrlLoad(UrlRedir)) {
                                GoUrl(UrlAutentificacion);
                            }

                            if (UrlLoad(UrlError2)) {
                                LoginScrapSAT();
                                TimerLogin.Enabled = true;
                            }

                            if (_WebSAT.Url.ToString().Contains("https://portalcfdi.facturaelectronica.sat.gob.mx") & EstaEnSelector() && !TrucoHide) {
                                GoUrl(UrlOculta);
                                TrucoHide = true;
                            }

                            if (_WebSAT.Url.ToString() == UrlOculta & EstaEnSelector()) {
                                TrucoHide = false;
                                ClickEnSelector(Configuracion.Tipo);
                                if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                                    ClickEnSelector(DescargaTipoEnum.Recibidos);
                                }
                            }

                            if (_WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")) {
                                if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                                    _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                }

                                if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                                    _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                }
                            }

                            if (_WebSAT.Url.ToString() == UrlRecibidas && Configuracion.Tipo == DescargaTipoEnum.Recibidos | _WebSAT.Url.ToString() == UrlRecibidas) {
                                GetElementHtmlById("ctl00_MainContent_RdoFechas", ref _WebSAT).InvokeMember("click");
                                TimerLogin.Enabled = false;
                                if (!Truco) {
                                    TimerFiltrarRecibidas.Enabled = true;
                                }
                            }

                            if (_WebSAT.Url.ToString().Contains("https://cfdiau.sat.gob.mx/nidp/wsfed_redir_cont_portalcfdi.jsp?wa=")) {
                                GoUrl(UrlAutentificacion);
                                return;
                            } else if (_WebSAT.Url.ToString() == UrlFailCaptcha) {
                                LoginScrapSAT();
                                TimerLogin.Enabled = true;
                            }

                            if (_WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=en_US")
                                || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_MX")
                                || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/portal?locale=es_ES")
                                || _WebSAT.Url == new Uri("https://cfdiau.sat.gob.mx/nidp/app?sid=0")
                                ) {
                                if (Configuracion.Tipo == DescargaTipoEnum.Recibidos) {
                                    _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx");
                                }

                                if (Configuracion.Tipo == DescargaTipoEnum.Emitidos) {
                                    _WebSAT.Navigate("https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaEmisor.aspx");
                                }
                            }
                        } else {
                            GoUrl(UrlError1);
                            return;
                        }
                    }
                } catch (Exception exception) {
                    Console.WriteLine("webBrowserCompleted: " + exception.Message);
                    LbError.Text = "webBrowserCompleted: " + exception.Message;
                }
            }
            while (true) {
                Application.DoEvents();
                if (!_WebSAT.IsBusy) {
                    break;
                }
                if (_WebSAT.ReadyState == WebBrowserReadyState.Loading) {
                    Application.DoEvents();
                }
            }
        }

        #region timers

        private void TimerLogin_Tick(object sender, EventArgs e) {
            if (CredencialesNoValidas() | ErrroDeCredenciales()) {
                CloseDescarga();
            }
        }

        private void TimerFiltrarRecibidas_Tick(object sender, EventArgs e) {
            bool enabled = false;
            HtmlElement elementById = null;
            elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_TxtRfcReceptor");
            if (elementById != null) {
                enabled = elementById.Enabled;
            }
            if (!ExisteUpdateProgres() & enabled) {
                TimerFiltrarRecibidas.Enabled = false;
                // introducir los datos de la fecha
                GetElementHtmlById("DdlAnio", ref _WebSAT).SetAttribute("value", ListaFechasRecibidos[IndexDiaRecibido].Fecha.Year.ToString(System.Globalization.CultureInfo.InvariantCulture));
                GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref _WebSAT).SetAttribute("value", ListaFechasRecibidos[IndexDiaRecibido].Fecha.Month.ToString("0"));
                GetElementHtmlById("ctl00_MainContent_CldFecha_DdlMes", ref _WebSAT).InvokeMember("Change");
                GetElementHtmlById("ctl00_MainContent_CldFecha_DdlDia", ref _WebSAT).SetAttribute("value", ListaFechasRecibidos[IndexDiaRecibido].Fecha.Day.ToString("00"));
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHora");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoInicial)[1]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinuto");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoInicial)[2]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundo");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoInicial)[3]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlHoraFin");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoFinal)[1]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlMinutoFin");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoFinal)[2]);
                }
                elementById = null;
                elementById = _WebSAT.Document.GetElementById("ctl00_MainContent_CldFecha_DdlSegundoFin");

                if (elementById != null) {
                    elementById.SetAttribute("value", GetTiempoInteger(ListLapsosRecibidos[IndexLapsoRecibido].SegundoFinal)[3]);
                }

                if (Configuracion.FiltrarPorRFC == true) {
                    GetElementHtmlById("ctl00_MainContent_TxtRfcReceptor", ref _WebSAT).SetAttribute("value", Configuracion.BuscarPorRFC);
                }

                if (Configuracion.Status == EstadosEnum.Cancelado)
                    GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref _WebSAT).SetAttribute("value", "0");
                if (Configuracion.Status == EstadosEnum.Vigente)
                    GetElementHtmlById("ctl00_MainContent_DdlEstadoComprobante", ref _WebSAT).SetAttribute("value", "1");

                GetElementHtmlById("ddlComplementos", ref _WebSAT).SetAttribute("value", Configuracion.Complemento);
                GetElementHtmlById("ctl00_MainContent_BtnBusqueda", ref _WebSAT).InvokeMember("click");
                LbMensaje.Text = string.Concat("Filtrando día ", ListLapsosRecibidos[IndexLapsoRecibido].Fecha.ToShortDateString(), "...");
                TimerRecopilarRecibidas.Enabled = true;
            }
        }

        private void TimerRecopilarRecibidas_Tick(object sender, EventArgs e) {
            if (!ExisteUpdateProgres() & _WebSAT.ReadyState == WebBrowserReadyState.Complete & !_WebSAT.IsBusy) {
                TimerRecopilarRecibidas.Enabled = false;
                LbMensaje.Text = "Recopilando XML ...";

                if (GetCuantosXML2() <= 0) {
                    ListLapsosRecibidos[IndexLapsoRecibido].Status = DescargaLapsoStatusEnum.Vacio;
                    LbMensaje.Text = "Periodo vacío";
                } else if (!GetLimiteDe500()) {
                    ListLapsosRecibidos[IndexLapsoRecibido].Status = DescargaLapsoStatusEnum.Ok;
                    int countXml = 0;
                    ProcessResult(ref countXml, ref _WebSAT);
                    ListaFechasRecibidos[IndexDiaRecibido].Total = ListaFechasRecibidos[IndexDiaRecibido].Total + countXml;
                    ListLapsosRecibidos[IndexLapsoRecibido].Cuantos = countXml;
                    LbMensaje.Text = string.Format("Disponibles: {0} xml's; Recopilados: {1}", countXml, Resultados.Count);
                    LbError.Text = "";
                } else {
                    LbMensaje.Text = "Creando Lapsos ...";
                    ListLapsosRecibidos[IndexLapsoRecibido].Status = DescargaLapsoStatusEnum.X;
                    CrearLapsosRecibidos(IndexLapsoRecibido);
                }

                if (HayRangosPorRevisar(ListLapsosRecibidos)) {
                    NextRangos();
                    return;
                }
                ContinuarRevisando();
            }
        }

        private void TimerClose_Tick(object sender, EventArgs e) {
            LbMensaje.Text = "Cerrando sesión ...";

            if (GetNavegadorCerrado()) {
                try {
                    _WebSAT.Document.ExecCommand("ClearAuthenticationCache", false, null);
                    OnCompletedProcess(new DescargaMasivaCompletedProcess("El proceso concluyo satisfactoriamente!", ""));
                } catch (Exception ex) {
                    Console.WriteLine("TimerClose: " + ex.Message);
                    LbError.Text = "TimerClose: " + ex.Message;
                }
                TimerClose.Enabled = false;
                timerCloseAlert.Enabled = false;
                TimerError.Enabled = false;
                TimerFiltrarRecibidas.Enabled = false;
                TimerLogin.Enabled = false;
                LbMensaje.Text = "Sesión Terminada ...";
                Cerrando = false;
            }
        }

        private void TimerCloseAlert_Tick(object sender, EventArgs e) {
            if ((long)FindWindow(null, "Mensaje de página web") != 0) {
                SendKeys.SendWait("{Enter}");
                timerCloseAlert.Enabled = false;
            }
        }

        private void TimerError_Tick(object sender, EventArgs e) {
            if (ErrroDeAutentificacion()) {
                TimerError.Enabled = false;
                GoUrl(UrlAutentificacion);
            }
        }

        #endregion

        #endregion
    }
}
