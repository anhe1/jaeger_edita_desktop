﻿// develop:
// purpose: clase de funciones comunes para archivos
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace Jaeger.Repositorio.Services {
    public class FilesExtendedService {
        public static void WriteFileText(string archivo, string texto) {
            using (StreamWriter streamWriter = new StreamWriter(archivo, false)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteFileText(string archivo, string texto, bool append) {
            using (StreamWriter streamWriter = new StreamWriter(archivo, append)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteFileText(string archivo, string texto, bool append, Encoding encoding) {
            using (StreamWriter streamWriter = new StreamWriter(archivo, append, encoding)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteFileText(Queue queTuplas, string nombre, bool agregarDatos) {
            IEnumerator enumerator = null;
            try {
                using (StreamWriter streamWriter = new StreamWriter(nombre, agregarDatos)) {
                    try {
                        enumerator = queTuplas.GetEnumerator();
                        while (enumerator.MoveNext()) {
                            string queTupla = enumerator.Current.ToString();
                            streamWriter.Write(string.Concat(queTupla, "\r\n"));
                        }
                    } finally {
                        if (enumerator is IDisposable) {
                            (enumerator as IDisposable).Dispose();
                        }
                    }
                    streamWriter.Dispose();
                }
            } catch (Exception exception) {
                throw exception;
            }
        }

        public static byte[] FromBase64(string base64Encoded) {
            byte[] numArray;
            if (base64Encoded == null || base64Encoded.Length == 0) {
                numArray = null;
            } else {
                try {
                    numArray = Convert.FromBase64String(base64Encoded);
                } catch (FormatException fex) {
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64Encoded, Environment.NewLine), fex);
                }
            }
            return numArray;
        }

        public static string ReadFileB64(FileInfo archivo) {
            string base64String;
            try {
                if (!archivo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo.Name));
                }
                FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray, 0, length);
                fileStream.Close();
                base64String = Convert.ToBase64String(numArray);
            } catch (Exception exception) {
                base64String = "";
                Console.WriteLine(exception.Message);
            }
            return base64String;
        }
    }
}
