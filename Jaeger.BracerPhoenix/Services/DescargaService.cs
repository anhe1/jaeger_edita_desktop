﻿using System;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Jaeger.Repositorio.Entities;

namespace Jaeger.Repositorio.Services {
    public class DescargaService {
        public static bool DownloadFile(string address, string fileName, string urlweb) {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            bool blnResult;
            try {
                string cookie = CookieReader.GetCookie(urlweb);
                WebClient webClient = new WebClient();
                if (!File.Exists(fileName)) {
                    webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                    Stream fileStream = new FileStream(fileName, FileMode.Create);
                    fileStream.Write(webClient.DownloadData(address), 0, webClient.DownloadData(address).Length);
                    fileStream.Flush();
                    fileStream.Close();
                    blnResult = true;
                } else {
                    blnResult = true;
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                blnResult = false;
            }
            return blnResult;
        }

        public static string DownloadFile(string address, string urlweb) {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            string base64String = "";
            if (address.ValidaURL()) {
                byte[] downloadFile = null;
                try {
                    string cookie = CookieReader.GetCookie(urlweb);
                    WebClient webClient = new WebClient();
                    webClient.Headers.Add(HttpRequestHeader.Cookie, cookie);
                    downloadFile = webClient.DownloadData(address);
                } catch (Exception ex) {
                    Console.WriteLine("DownloadFile: " + ex.Message);
                    downloadFile = null;
                } finally {
                    if (downloadFile != null) {
                        base64String = Convert.ToBase64String(downloadFile);
                    }
                }

            }
            return base64String;
        }

        public static bool WriteFileFromB64(string base64, string archivo) {
            bool result = false;
            byte[] numArray;

            if (base64 == null || base64.Length == 0) {
                numArray = null;
            } else {
                try {
                    numArray = Convert.FromBase64String(base64);
                } catch (FormatException fex) {
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64, Environment.NewLine), fex);
                }
            }

            if (numArray != null) {
                string directorio = Path.GetDirectoryName(archivo);

                if (!Directory.Exists(directorio)) {
                    Directory.CreateDirectory(directorio);
                }

                try {
                    using (FileStream fileStream = File.Create(archivo)) {
                        fileStream.Write(numArray, 0, checked(numArray.Length));
                        fileStream.Close();
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }

            return result;
        }

        public static async Task<List<ComprobanteFiscalResponse>> Run(List<ComprobanteFiscalResponse> listfiles, string urlWeb, IProgress<DescargaMasivaProgreso> proceso) {
            List<ComprobanteFiscalResponse> output = new List<ComprobanteFiscalResponse>();
            int contador = 0;
            await Task.Run(() => {
                Parallel.ForEach(listfiles, (comprobante) => {
                    ComprobanteFiscalResponse results = Execute(comprobante, urlWeb);
                    output.Add(results);
                    contador += 1;
                    proceso.Report(new DescargaMasivaProgreso(output.Count * 100 / listfiles.Count, listfiles.Count, contador, "Descargando "));
                });
            });

            return output;
        }

        public static ComprobanteFiscalResponse Execute(ComprobanteFiscalResponse item, string urlWeb) {
            if (item.EstadoComprobante.ToLower() == "cancelado") return item;

            item.XmlContentB64 = DownloadFile(item.Acciones.UrlXML, urlWeb);
            item.PdfRepresentacionImpresaB64 = DownloadFile(item.Acciones.UrlPDF, urlWeb);
            item.PdfAcuseB64 = DownloadFile(item.Acciones.UrlAcusePdf, urlWeb);
            item.PdfAcuseFinalB64 = DownloadFile(item.Acciones.UrlAcuseFinal, urlWeb);
            return item;
        }

        //public static ComprobanteFiscalResponse Des_Original(ComprobanteFiscalResponse item, string urlweb) {
        //    item.XmlContentB64 = DownloadFile(item.UrlXML, urlweb);
        //    item.PdfRepresentacionImpresaB64 = DownloadFile(item.UrlPDF, urlweb);
        //    item.PdfAcuseB64 = DownloadFile(item.PdfAcuseB64, urlweb);
        //    item.PdfAcuseFinalB64 = DownloadFile(item.UrlRecuperaAcuseFinal, urlweb);
        //    return item;
        //}
    }
}
