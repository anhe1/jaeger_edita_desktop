﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Repositorio.Entities;
using Jaeger.Repositorio.ValueObjects;

namespace Jaeger.Repositorio.Services {
    public static class CommonService {
        public static List<BaseSingle2Model> GetTipoComprobante() {
            return ((DescargaTipoEnum[])Enum.GetValues(typeof(DescargaTipoEnum))).Select(c => new BaseSingle2Model(c.ToString(), c.ToString())).ToList();
        }

        public static List<BaseSingle2Model> GetEstado() {
            return ((EstadosEnum[])Enum.GetValues(typeof(EstadosEnum))).Select(c => new BaseSingle2Model(c.ToString(), c.ToString())).ToList();
        }

        public static List<BaseSingle2Model> GetComplementos() {
            return new List<BaseSingle2Model>(new BaseSingle2Model[] {
                new BaseSingle2Model("-1", "Todos"),
                new BaseSingle2Model("8", "Estándar"),
                new BaseSingle2Model("4294967296", "Acreditamiento de IEPS"),
                new BaseSingle2Model("8388608", "Aerolíneas"),
                new BaseSingle2Model("1073741824", "Certificado de Destrucción"),
                new BaseSingle2Model("17179869184", "Comercio Exterior"),
                new BaseSingle2Model("274877906944", "Comercio Exterior 1.1"),
                new BaseSingle2Model("4", "Compra Venta de Divisas"),
                new BaseSingle2Model("16777216", "Consumo de Combustibles"),
                new BaseSingle2Model("64", "Donatarias"),
                new BaseSingle2Model("256", "Estado De Cuenta Bancario"),
                new BaseSingle2Model("8589934592", "Estado de cuenta de combustibles de monederos electrónicos."),
                new BaseSingle2Model("68719476736", "INE 1.1"),
                new BaseSingle2Model("1024", "Instituciones Educativas Privadas (Pago de colegiatura)"),
                new BaseSingle2Model("4096", "Leyendas Fiscales"),
                new BaseSingle2Model("524288", "Mis Cuentas"),
                new BaseSingle2Model("67108864", "Notarios Públicos"),
                new BaseSingle2Model("536870912", "Obras de artes y antiguedades"),
                new BaseSingle2Model("2048", "Otros Derechos e Impuestos"),
                new BaseSingle2Model("4194304", "Pago en Especie"),
                new BaseSingle2Model("8192", "Persona Física Integrante de Coordinado"),
                new BaseSingle2Model("549755813888", "Recepción de Pagos"),
                new BaseSingle2Model("128", "Recibo de donativo"),
                new BaseSingle2Model("1048576", "Recibo de Pago de Salarios"),
                new BaseSingle2Model("137438953472", "Recibo de Pago de Salarios 1.2"),
                new BaseSingle2Model("32", "Sector de Ventas al Detalle (Detallista)"),
                new BaseSingle2Model("268435456", "Servicios de construcción"),
                new BaseSingle2Model("16384", "SPEI de Tercero a Tercero"),
                new BaseSingle2Model("2147483648", "Sustitución y renovación vehicular"),
                new BaseSingle2Model("32768", "Terceros"),
                new BaseSingle2Model("65536", "Terceros"),
                new BaseSingle2Model("2199023255552", "Timbre Fiscal Digital"),
                new BaseSingle2Model("16", "Turista o Pasajero Extranjero"),
                new BaseSingle2Model("33554432", "Vales de Despensa"),
                new BaseSingle2Model("134217728", "Vehículo Usado"),
                new BaseSingle2Model("2097152", "Venta de Vehiculos")});
        }
    }
}
