﻿using System.Runtime.InteropServices;

namespace Jaeger.Repositorio.Services {
    public class InternetService {
        [DllImport("wininet.dll", CharSet = CharSet.None, ExactSpelling = false)]
        private static extern bool InternetGetConnectedState(out int description, int reservedValue);

        public static bool IsConnectedToInternet() {
            int num;
            bool flag = InternetGetConnectedState(out num, 0);
            return flag;
        }
    }
}
