﻿using System.Text.RegularExpressions;

namespace Jaeger.Repositorio.Services {
    public class ValidaService {
        public static bool RFC(string pRFC) {
            if (pRFC == null)
                return false;
            return (!Regex.IsMatch(pRFC, "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$") ? false : true);
        }

        /// <summary>
        /// validar url
        /// </summary>
        /// <param name="pURL">URL</param>
        /// <returns>verdadero si coincide con la expresion regular</returns>
        public static bool URL(string pURL) {
            if (pURL == null)
                return false;
            else
                if ((new Regex("^(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", RegexOptions.IgnoreCase)).Match(pURL).Captures.Count == 0)
                return false;
            return true;
        }

        /// <summary>
        /// validar formato de folio fiscal (uuid)
        /// </summary>
        /// <param name="pUUID">indentificador unico universal</param>
        /// <returns>verdadero si coincide con la expresion regular</returns>
        public static bool UUID(string pUUID) {
            if (pUUID != null) {
                return (!Regex.IsMatch(pUUID, "[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}") ? false : true);
            }
            return false;
        }

        /// <summary>
        /// validar cadeena base 64
        /// </summary>
        /// <param name="pBase64">cadena base 64</param>
        /// <returns></returns>
        public static bool IsBase64String(string pBase64) {
            if (pBase64 != null) {
                pBase64 = pBase64.Trim();
                return (pBase64.Length % 4 == 0) && Regex.IsMatch(pBase64, @"^[-A-Za-z0-9+=]{1,50}|=[^=]|={3,}$", RegexOptions.None);
            }
            return false;
        }
    }
}
