﻿using System.Text.RegularExpressions;

namespace Jaeger.Repositorio.Services {
    public static class Extensions {
        public static bool ValidaURL(this string urlWeb) {
            if (urlWeb == null) {
                return false;
            } else {
                if (new Regex("^(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", RegexOptions.IgnoreCase).Match(urlWeb).Captures.Count == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}
