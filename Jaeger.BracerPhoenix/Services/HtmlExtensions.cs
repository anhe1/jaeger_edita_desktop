﻿using System.Collections.Generic;

namespace Jaeger.Repositorio.Services {
    public static class HtmlExtensions {

        public static IEnumerable<HtmlAgilityPack.HtmlNode> GetElementsByTagName(this HtmlAgilityPack.HtmlNode parent, string name) {
            return parent.Descendants(name);
        }
    }
}
