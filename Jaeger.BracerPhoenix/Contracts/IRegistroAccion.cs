﻿namespace Jaeger.Repositorio.Abstractions {
    public interface IRegistroAccion {
        /// <summary>
        /// obtener o establecer url de descarga para el archivo xml
        /// </summary>
        string UrlXML { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo pdf
        /// </summary>
        string UrlPDF { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo del acuse pdf
        /// </summary>
        string UrlAcusePdf { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo pdf de detalles
        /// </summary>
        string UrlDetalle { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga para el archivo del acuse final de la cancelacion
        /// </summary>
        string UrlAcuseFinal { get; set; }
    }
}
