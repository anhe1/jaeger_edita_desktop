﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Jaeger.Repositorio.Entities;

namespace Jaeger.Repositorio.Interface {
    public interface IDescargaMasiva {

        #region propiedades
        ConfiguracionDetail Configuracion {
            get; set;
        }
        List<ComprobanteFiscalResponse> Resultados {
            get; set;
        }
        int Cuantos {
            get;
        }

        string Version {
            get;
        }

        #endregion

        #region metodos publicos 

        void Consultar(WebBrowser _wsSat2, ConfiguracionDetail conf);

        void Reset();

        #endregion

        #region eventos de la clase

        /// <summary>
        /// al iniciar el proceso de consulta
        /// </summary>
        event EventHandler<DescargaMasivaStartProcess> StartProcess;

        /// <summary>
        /// progreso de la descarga
        /// </summary>
        event EventHandler<DescargaMasivaProgreso> ProgressChanged;

        /// <summary>
        /// al terminar el proceso 
        /// </summary>
        event EventHandler<DescargaMasivaCompletedProcess> CompletedProcess;

        #endregion
    }
}
