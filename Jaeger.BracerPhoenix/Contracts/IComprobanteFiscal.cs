﻿using Jaeger.Repositorio.Abstractions;
using System;

namespace Jaeger.Repositorio.Contracts {
    public interface IComprobanteFiscal {
        #region propiedades
        /// <summary>
        /// obtener o establecer folio fiscal
        /// </summary>
        string FolioFiscal { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del emisor
        /// </summary>
        string Emisor { get; set; }

        /// <summary>
        /// obtener o establecer rfc del receptor
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        string Receptor { get; set; }

        /// <summary>
        /// obtener o establecer fecha de emision del comprobante
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer fecha de certificacion
        /// </summary>
        DateTime FechaCertifica { get; set; }

        /// <summary>
        /// obtener o establecer rfc del PAC que certifico
        /// </summary>
        string PacCertifica { get; set; }

        /// <summary>
        /// obtener o establecer el total
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer efecto del comprobante
        /// </summary>
        string Efecto { get; set; }

        /// <summary>
        /// obtener o establecer status de la cancelacion
        /// </summary>
        string EstatusCancelacion { get; set; }

        /// <summary>
        /// obtener o establecer status del comprobante
        /// </summary>
        string EstadoComprobante { get; set; }

        /// <summary>
        /// Estatus de Proceso de Cancelación
        /// </summary>
        string EstatusProcesoCancelacion { get; set; }
        /// <summary>
        /// Fecha de Proceso de Cancelación
        /// </summary>
        DateTime? FechaProcesoCancelacion { get; }

        /// <summary>
        /// obtener o establecer RFC a cuenta de terceros
        /// </summary>
        string CuentaTercerosRFC { get; set; }

        /// <summary>
        /// obtener o establecer motivo de cancelacion
        /// </summary>
        string Motivo { get; set; }

        /// <summary>
        /// obtener o establecer folio de sustitucion
        /// </summary>
        string FolioSustitucion { get; set; }

        /// <summary>
        /// obtener o establecer si el comprobante fue descargado
        /// </summary>
        bool Descargado { get; set; }

        /// <summary>
        /// obtener o establecer ruta completa del archivo
        /// </summary>
        string PathFull { get; set; }

        /// <summary>
        /// obtener o establecer el tipo del comprobante (Emitido o Recibido)
        /// </summary>
        string Tipo { get; set; }

        IRegistroAccion Acciones { get; }

        /// <summary>
        /// obtener o establecer nombre del archivo
        /// </summary>
        string KeyName { get; }
        #endregion

        #region builder
        IComprobanteFiscal WithAcciones(IRegistroAccion acciones);

        IComprobanteFiscal WithFolioFiscal(string uuid);

        IComprobanteFiscal WithEmisorRFC(string rfc);

        IComprobanteFiscal WithEmisor(string emisor);

        IComprobanteFiscal WithFechaEmision(string fecha);

        IComprobanteFiscal WithFechaCertificacion(string fecha);

        IComprobanteFiscal WithFechaProcesoCancelaion(string fecha);

        IComprobanteFiscal WithFechaProcesoCancelaion(DateTime fecha);

        IComprobanteFiscal WithTotal(string total);
        #endregion
    }
}
