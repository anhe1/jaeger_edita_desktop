﻿namespace Jaeger.Repositorio.ValueObjects {
    public enum DescargaStatusEnum {
        Pendiente,
        Descargado,
        Error
    }
}
