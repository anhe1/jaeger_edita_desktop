﻿namespace Jaeger.Repositorio.ValueObjects {
    public enum EstadosEnum {
        Todos,
        Vigente,
        Cancelado
    }
}
