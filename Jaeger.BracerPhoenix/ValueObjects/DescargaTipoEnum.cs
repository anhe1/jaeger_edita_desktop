﻿/// <summary>
/// declara el tipo de sub tipo comprobantes
/// </summary>
namespace Jaeger.Repositorio.ValueObjects {
    public enum DescargaTipoEnum {
        Todas,
        Emitidos,
        Recibidos,
        RecurperarDescargaCFDI,
        SolucitudesCancelacion
    }
}
