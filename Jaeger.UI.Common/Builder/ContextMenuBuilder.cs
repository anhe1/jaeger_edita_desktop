﻿using System.Linq;
using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Builder {
    public class ContextMenuBuilder : IContextMenuBuilder, IContextMenuItemsBuilder, IContextMenuTempleteBuilder, IContextMenuBuild {
        #region declaraciones
        protected internal RadContextMenu _ContextMenu;
        #endregion

        public ContextMenuBuilder() {
            this._ContextMenu = new RadContextMenu();
        }

        public RadContextMenu Build() {
            return this._ContextMenu;
        }

        public IContextMenuItemsBuilder AddMenuContext(RadContextMenu menu) {
            this._ContextMenu = menu;
            return this;
        }

        public IContextMenuItemsBuilder Items() {
            return this;
        }

        public IContextMenuItemsBuilder AddItem(RadMenuItem menuItem) {
            this._ContextMenu.Items.Add(menuItem);
            return this;
        }

        public IContextMenuItemsBuilder AddCopiar() {
            this._ContextMenu.Items.Add(new RadMenuItem { Text = "Copiar", Name = "Copiar" });
            return this;
        }

        public IContextMenuItemsBuilder AddSeleccionMultiple() {
            this._ContextMenu.Items.Add(new RadMenuItem { Text = "Selección múltiple", Name = "SeleccionMultiple" });
            return this;
        }

        public IContextMenuItemsBuilder AddEditar() {
            this._ContextMenu.Items.Add(new RadMenuItem { Text = "Editar", Name = "Editar" });
            return this;
        }

        public IContextMenuItemsBuilder AddCancelar() {
            this._ContextMenu.Items.Add(new RadMenuItem { Text = "Cancelar", Name = "Cancelar", Visibility = ElementVisibility.Collapsed });
            return this;
        }

        public IContextMenuItemsBuilder AddImprimir() {
            this._ContextMenu.Items.Add(new RadMenuItem { Text = "Imprimir", Name = "Imprimir" });
            return this;
        }

        public IContextMenuItemsBuilder AddDuplicar() {
            this._ContextMenu.Items.Add(new RadMenuItem { Text = "Duplicar", Name = "Duplicar" });
            return this;
        }

        public RadMenuItem Get() {
            return this._ContextMenu.Items[this._ContextMenu.Items.Count - 1] as RadMenuItem;
        }

        public IContextMenuTempleteBuilder Templete() {
            this._ContextMenu.Items.Clear();
            return this;
        }

        public IContextMenuItemsBuilder Master() {
            this.AddCopiar().AddSeleccionMultiple().AddEditar().AddCancelar().AddImprimir().AddDuplicar();
            return this;
        }

        protected internal void Agregar(RadMenuItem item) {
            try {
                var si = this._ContextMenu.Items.Where(x => x.Name == item.Name).FirstOrDefault();
                if (si == null) {
                    this._ContextMenu.Items.Add(item);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
