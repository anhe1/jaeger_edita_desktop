﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Builder {
    public interface IGridViewBuilder : IGridViewTempleteBuild, IDisposable {
        IGridViewTempleteBuild Template();
        GridViewDataColumn[] Build();
    }

    public interface IGridViewTempleteBuild : IGridViewColumnsBuild {
        IGridViewTempleteBuild WithCaption(string caption);
        RadGridView GridCommon(RadGridView gridView);

        void PrintColumns();
    }

    public interface IGridViewColumnsBuild {
        //IGridViewBuilder AddFechaNuevo();
        //IGridViewBuilder AddCreo();
        //IGridViewBuilder AddModifica();
        //IGridViewBuilder AddFechaModifica();
    }
}
