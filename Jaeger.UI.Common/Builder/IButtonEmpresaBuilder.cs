﻿namespace Jaeger.UI.Common.Builder {
    public interface IButtonEmpresaBuilder {
        ButtonEmpresaBuilder Version(string version);
        ButtonEmpresaBuilder Usuario(string usuario);
        ButtonEmpresaBuilder Empresa(string empresa);

        ButtonEmpresaBuilder DataBase(string database);
    }
}
