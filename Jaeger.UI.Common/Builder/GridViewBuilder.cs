﻿using System;
using System.Drawing;
using System.Collections.Generic;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Builder {

    /// <summary>
    /// clase abstracta para builder de las vistas del Grid de telerik
    /// </summary>
    public abstract class GridViewBuilder : IGridViewBuilder {
        #region declaraciones
        protected internal List<GridViewDataColumn> _Columns;
        protected internal GridViewTemplate _Templete;
        protected internal string FormatStringMoney = "{0:N2}";
        protected internal string FormatStringDate = "{0:dd MMM yy}";
        protected internal string FormatStringP = "{0:P0}";
        protected internal string FormatStringNumber = "{0:N2}";
        protected internal string FormarStringFolio = "{0:00000#}";
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public GridViewBuilder() {
            _Columns = new List<GridViewDataColumn>();
        }

        /// <summary>
        /// Build
        /// </summary>
        /// <returns>array de columnas (GridViewDataColumn)</returns>
        public virtual GridViewDataColumn[] Build() {
            return _Columns.ToArray();
        }

        public virtual IGridViewTempleteBuild Set(GridViewTemplate template) {
            this._Templete = template;
            return this;
        }

        public virtual IGridViewTempleteBuild Template() {
            this._Templete = new GridViewTemplate();
            return this;
        }

        public virtual IGridViewTempleteBuild WithCaption(string caption) {
            if (!string.IsNullOrEmpty(caption)) this._Templete.Caption = caption;
            _Templete.Caption = caption;
            return this;
        }

        protected virtual GridViewTextBoxColumn GetColumnID() {
            return new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "ID",
                Name = "Id",
                Width = 55,
                IsVisible = false,
                ReadOnly = true,
                FormatString = "{0:00000#}",
                TextAlignment = ContentAlignment.MiddleCenter,
            };
        }

        protected virtual GridViewCheckBoxColumn GetColumnIsActive() {
            return new GridViewCheckBoxColumn {
                DataType = typeof(bool),
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo",
                Width = 75,
                IsVisible = false,
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
            };
        }

        protected virtual GridViewTextBoxColumn GetColumn() {
            return new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Column",
                HeaderText = "Column",
                Name = "Column",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 75,
                ReadOnly = true
            };
        }

        public void Dispose() {
            GC.Collect();
        }

        public virtual RadGridView GridCommon(RadGridView gridView) {
            try {
                RadGridView radGridView = gridView;
                radGridView.AutoGenerateColumns = false;
                radGridView.AllowAddNewRow = false;
                radGridView.AllowEditRow = false;
                radGridView.AllowDeleteRow = false;
                radGridView.EnableFiltering = true;
                radGridView.ShowGroupPanel = false;
                radGridView.ShowFilteringRow = false;
                radGridView.EnableAlternatingRowColor = true;
                radGridView.AutoSizeRows = false;
                radGridView.AllowRowResize = false;
                radGridView = null;
                return gridView;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return gridView;
            }
        }

        public void PrintColumns() {
            foreach (var item in this._Columns) {
                Console.WriteLine(item.Name);
            }
        }

        protected void Agregate(GridViewDataColumn column) {
            try {
                var exist = this._Columns.Find(x => x.Name == column.Name);
                if (exist == null) {
                    this._Columns.Add(column);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Columna duplicada: " + column.Name);
            }
        }
    }
}
