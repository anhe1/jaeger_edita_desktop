﻿using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Builder {
    public class ButtonEmpresaBuilder : IButtonEmpresaBuilder {
        protected internal RadSplitButtonElement button = new RadSplitButtonElement {
            ArrowButtonMinSize = new Size(12, 12),
            DefaultItem = null,
            DropDownDirection = RadDirection.Down,
            ExpandArrowButton = false,
            Name = "BEmpresa",
            Image = Properties.Resources.administrator_male_16px,
            TextImageRelation = TextImageRelation.ImageBeforeText
        };

        public ButtonEmpresaBuilder Version(string version) {
            this.button.Items.Add(new RadMenuItem {
                Name = "Version",
                Text = "Version: " + version
            });
            return this;
        }

        public ButtonEmpresaBuilder Usuario(string usuario) {
            this.button.Items.Add(new RadMenuItem {
                Name = "Usuario",
                Text = "Usuario: " + usuario,
                Image = Properties.Resources.user_16,
                TextImageRelation = TextImageRelation.ImageBeforeText
            });
            return this;
        }

        public ButtonEmpresaBuilder Empresa(string empresa) {
            if (empresa == null) { empresa = "Desconocida"; }
            this.button.Text = empresa.ToUpper();
            this.button.Items.Add(new RadMenuItem {
                Name = "Empresa",
                Text = "Empresa: " + empresa.ToUpper()
            }); ;
            return this;
        }

        public ButtonEmpresaBuilder DataBase(string database) {
            this.button.Items.Add(new RadMenuItem {
                Name = "DataBase",
                Text = "DataBase: " + database,
                //Image = Properties.Resources.user_16,
                TextImageRelation = TextImageRelation.ImageBeforeText
            });
            return this;
        }

        public RadSplitButtonElement Build() {
            return button;
        }
    }
}
