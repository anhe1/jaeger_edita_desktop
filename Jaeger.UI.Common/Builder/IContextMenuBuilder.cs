﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Builder {
    public interface IContextMenuBuilder {
        IContextMenuItemsBuilder Items();

        IContextMenuTempleteBuilder Templete();
    }

    public interface IContextMenuTempleteBuilder {
        IContextMenuItemsBuilder Master();
    }

    public interface IContextMenuItemsBuilder {

        IContextMenuItemsBuilder AddItem(RadMenuItem menuItem);

        IContextMenuItemsBuilder AddCopiar();

        IContextMenuItemsBuilder AddSeleccionMultiple();

        IContextMenuItemsBuilder AddEditar();

        IContextMenuItemsBuilder AddCancelar();

        IContextMenuItemsBuilder AddImprimir();

        IContextMenuItemsBuilder AddDuplicar();

        RadMenuItem Get();
    }

    public interface IContextMenuBuild {
        RadContextMenu Build();
    }
}
