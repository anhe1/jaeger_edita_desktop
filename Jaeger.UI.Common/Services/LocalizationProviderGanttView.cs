﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    class LocalizationProviderGanttView : GanttViewLocalizationProvider {
        public override string GetLocalizedString(string id) {
            switch (id) {
                case GanttViewStringId.ContextMenuAdd:
                    return "&Add";
                case GanttViewStringId.ContextMenuAddChild:
                    return "Add &Child";
                case GanttViewStringId.ContextMenuAddSibling:
                    return "Add &Sibling";
                case GanttViewStringId.ContextMenuDelete:
                    return "&Delete";
                case GanttViewStringId.ContextMenuProgress:
                    return "&Progress";
                case "TimelineWeek":
                    return "Week";
            }
            return string.Empty;
        }
    }
}
