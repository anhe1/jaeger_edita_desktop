﻿using Telerik.WinControls.UI.Localization;

namespace Jaeger.UI.Common.Services {
    public class LocalizationProviderCustomScheduler : RadSchedulerLocalizationProvider {
        public override string GetLocalizedString(string id) {
            switch (id) {
                case RadSchedulerStringId.NextAppointment:
                    return "Próxima cita";
                case RadSchedulerStringId.PreviousAppointment:
                    return "Cita previa";
                case RadSchedulerStringId.AppointmentDialogTitle:
                    return "Editar cita";
                case RadSchedulerStringId.AppointmentDialogSubject:
                    return "Asunto:";
                case RadSchedulerStringId.AppointmentDialogLocation:
                    return "Ubicación:";
                case RadSchedulerStringId.AppointmentDialogBackground:
                    return "Fondo:";
                case RadSchedulerStringId.AppointmentDialogDescription:
                    return "Descripción:";
                case RadSchedulerStringId.AppointmentDialogStartTime:
                    return "Inicia:";
                case RadSchedulerStringId.AppointmentDialogEndTime:
                    return "Termina:";
                case RadSchedulerStringId.AppointmentDialogAllDay:
                    return "Evento de todo el día";
                case RadSchedulerStringId.AppointmentDialogResource:
                    return "Resource:";
                case RadSchedulerStringId.AppointmentDialogStatus:
                    return "Show time as:";
                case RadSchedulerStringId.AppointmentDialogOK:
                    return "OK";
                case RadSchedulerStringId.AppointmentDialogCancel:
                    return "Cancel";
                case RadSchedulerStringId.AppointmentDialogDelete:
                    return "Delete";
                case RadSchedulerStringId.AppointmentDialogRecurrence:
                    return "Recurrence";
                case RadSchedulerStringId.OpenRecurringDialogTitle:
                    return "Open Recurring Item";
                case RadSchedulerStringId.DeleteRecurrenceDialogOK:
                    return "OK";
                case RadSchedulerStringId.OpenRecurringDialogOK:
                    return "OK";
                case RadSchedulerStringId.DeleteRecurrenceDialogCancel:
                    return "Cancelar";
                case RadSchedulerStringId.OpenRecurringDialogCancel:
                    return "Cancelar";
                case RadSchedulerStringId.OpenRecurringDialogLabel:
                    return "\"{0}\" is a recurring\nappointment. Do you want to open\nonly this occurrence or the series?";
                case RadSchedulerStringId.OpenRecurringDialogRadioOccurrence:
                    return "Open this occurrence.";
                case RadSchedulerStringId.OpenRecurringDialogRadioSeries:
                    return "Open the series.";
                case RadSchedulerStringId.DeleteRecurrenceDialogTitle:
                    return "Confirm Delete";
                case RadSchedulerStringId.DeleteRecurrenceDialogRadioOccurrence:
                    return "Delete this occurrence.";
                case RadSchedulerStringId.DeleteRecurrenceDialogRadioSeries:
                    return "Delete the series.";
                case RadSchedulerStringId.DeleteRecurrenceDialogLabel:
                    return "Do you want to delete all occurrences of the recurring appointment \"{0}\", or just this one?";
                case RadSchedulerStringId.RecurrenceDragDropCreateExceptionDialogText:
                    return "You changed the date of a single occurrence of a recurring appointment. To change all the dates, open the series.\nDo you want to change just this one?";
                case RadSchedulerStringId.RecurrenceDragDropValidationSameDateText:
                    return "Two occurrences of the same series cannot occur on the same day.";
                case RadSchedulerStringId.RecurrenceDragDropValidationSkipOccurrenceText:
                    return "Cannot reschedule an occurrence of a recurring appointment if it skips over a later occurrence of the same appointment.";
                case RadSchedulerStringId.RecurrenceDialogMessageBoxText:
                    return "Start date should be before EndBy date.";
                case RadSchedulerStringId.RecurrenceDialogMessageBoxWrongRecurrenceRuleText:
                    return "The recurrence pattern is not valid.";
                case RadSchedulerStringId.RecurrenceDialogMessageBoxTitle:
                    return "Validation error";
                case RadSchedulerStringId.RecurrenceDialogTitle:
                    return "Edit Recurrence";
                case RadSchedulerStringId.RecurrenceDialogAppointmentTimeGroup:
                    return "Appointment time";
                case RadSchedulerStringId.RecurrenceDialogDuration:
                    return "Duration:";
                case RadSchedulerStringId.RecurrenceDialogAppointmentEnd:
                    return "End:";
                case RadSchedulerStringId.RecurrenceDialogAppointmentStart:
                    return "Start:";
                case RadSchedulerStringId.RecurrenceDialogRecurrenceGroup:
                    return "Recurrence pattern";
                case RadSchedulerStringId.RecurrenceDialogRangeGroup:
                    return "Range of recurrence";
                case RadSchedulerStringId.RecurrenceDialogOccurrences:
                    return "occurrences";
                case RadSchedulerStringId.RecurrenceDialogRecurrenceStart:
                    return "Start:";
                case RadSchedulerStringId.RecurrenceDialogYearly:
                    return "Yearly";
                case RadSchedulerStringId.RecurrenceDialogHourly:
                    return "Hourly";
                case RadSchedulerStringId.RecurrenceDialogMonthly:
                    return "Monthly";
                case RadSchedulerStringId.RecurrenceDialogWeekly:
                    return "Weekly";
                case RadSchedulerStringId.RecurrenceDialogDaily:
                    return "Daily";
                case RadSchedulerStringId.RecurrenceDialogEndBy:
                    return "End by:";
                case RadSchedulerStringId.RecurrenceDialogEndAfter:
                    return "End after:";
                case RadSchedulerStringId.RecurrenceDialogNoEndDate:
                    return "No end date";
                case RadSchedulerStringId.RecurrenceDialogAllDay:
                    return "All day event";
                case RadSchedulerStringId.RecurrenceDialogDurationDropDown1Day:
                    return "1 day";
                case RadSchedulerStringId.RecurrenceDialogDurationDropDown2Days:
                    return "2 days";
                case RadSchedulerStringId.RecurrenceDialogDurationDropDown3Days:
                    return "3 days";
                case RadSchedulerStringId.RecurrenceDialogDurationDropDown4Days:
                    return "4 days";
                case RadSchedulerStringId.RecurrenceDialogDurationDropDown1Week:
                    return "1 week";
                case RadSchedulerStringId.RecurrenceDialogDurationDropDown2Weeks:
                    return "2 weeks";
                case RadSchedulerStringId.RecurrenceDialogOK:
                    return "OK";
                case RadSchedulerStringId.RecurrenceDialogCancel:
                    return "Cancel";
                case RadSchedulerStringId.RecurrenceDialogRemoveRecurrence:
                    return "Remove Recurrence";
                case RadSchedulerStringId.HourlyRecurrenceEvery:
                    return "Every";
                case RadSchedulerStringId.HourlyRecurrenceHours:
                    return "hour(s)";
                case RadSchedulerStringId.DailyRecurrenceEveryDay:
                    return "Every";
                case RadSchedulerStringId.DailyRecurrenceEveryWeekday:
                    return "Every weekday";
                case RadSchedulerStringId.DailyRecurrenceDays:
                    return "day(s)";
                case RadSchedulerStringId.WeeklyRecurrenceRecurEvery:
                    return "Recur every";
                case RadSchedulerStringId.WeeklyRecurrenceWeeksOn:
                    return "week(s) on:";
                case RadSchedulerStringId.WeeklyRecurrenceSunday:
                    return "Domingo";
                case RadSchedulerStringId.WeeklyRecurrenceMonday:
                    return "Monday";
                case RadSchedulerStringId.WeeklyRecurrenceTuesday:
                    return "Tuesday";
                case RadSchedulerStringId.WeeklyRecurrenceWednesday:
                    return "Wednesday";
                case RadSchedulerStringId.WeeklyRecurrenceThursday:
                    return "Thursday";
                case RadSchedulerStringId.WeeklyRecurrenceFriday:
                    return "Friday";
                case RadSchedulerStringId.WeeklyRecurrenceSaturday:
                    return "Saturday";
                case RadSchedulerStringId.WeeklyRecurrenceDay:
                    return "Day";
                case RadSchedulerStringId.WeeklyRecurrenceWeekday:
                    return "Weekday";
                case RadSchedulerStringId.WeeklyRecurrenceWeekendDay:
                    return "Weekend day";
                case RadSchedulerStringId.MonthlyRecurrenceDay:
                    return "Day";
                case RadSchedulerStringId.MonthlyRecurrenceWeek:
                    return "The";
                case RadSchedulerStringId.MonthlyRecurrenceDayOfMonth:
                    return "of every";
                case RadSchedulerStringId.MonthlyRecurrenceMonths:
                    return "month(s)";
                case RadSchedulerStringId.MonthlyRecurrenceWeekOfMonth:
                    return "of every";
                case RadSchedulerStringId.MonthlyRecurrenceFirst:
                    return "First";
                case RadSchedulerStringId.MonthlyRecurrenceSecond:
                    return "Second";
                case RadSchedulerStringId.MonthlyRecurrenceThird:
                    return "Third";
                case RadSchedulerStringId.MonthlyRecurrenceFourth:
                    return "Fourth";
                case RadSchedulerStringId.MonthlyRecurrenceLast:
                    return "Last";
                case RadSchedulerStringId.YearlyRecurrenceDayOfMonth:
                    return "Every";
                case RadSchedulerStringId.YearlyRecurrenceWeekOfMonth:
                    return "The";
                case RadSchedulerStringId.YearlyRecurrenceOfMonth:
                    return "de";
                case RadSchedulerStringId.YearlyRecurrenceJanuary:
                    return "Enero";
                case RadSchedulerStringId.YearlyRecurrenceFebruary:
                    return "Febrero";
                case RadSchedulerStringId.YearlyRecurrenceMarch:
                    return "Marzo";
                case RadSchedulerStringId.YearlyRecurrenceApril:
                    return "Abril";
                case RadSchedulerStringId.YearlyRecurrenceMay:
                    return "Mayo";
                case RadSchedulerStringId.YearlyRecurrenceJune:
                    return "Junio";
                case RadSchedulerStringId.YearlyRecurrenceJuly:
                    return "Julio";
                case RadSchedulerStringId.YearlyRecurrenceAugust:
                    return "Agosto";
                case RadSchedulerStringId.YearlyRecurrenceSeptember:
                    return "Septiembre";
                case RadSchedulerStringId.YearlyRecurrenceOctober:
                    return "Octubre";
                case RadSchedulerStringId.YearlyRecurrenceNovember:
                    return "Noviembre";
                case RadSchedulerStringId.YearlyRecurrenceDecember:
                    return "Diciembre";
                case RadSchedulerStringId.BackgroundNone:
                    return "None";
                case RadSchedulerStringId.BackgroundImportant:
                    return "Important";
                case RadSchedulerStringId.BackgroundBusiness:
                    return "Negocios";
                case RadSchedulerStringId.BackgroundPersonal:
                    return "Personal";
                case RadSchedulerStringId.BackgroundVacation:
                    return "Vacation";
                case RadSchedulerStringId.BackgroundMustAttend:
                    return "Must Attend";
                case RadSchedulerStringId.BackgroundTravelRequired:
                    return "Travel Required";
                case RadSchedulerStringId.BackgroundNeedsPreparation:
                    return "Needs Preparation";
                case RadSchedulerStringId.BackgroundBirthday:
                    return "Birthday";
                case RadSchedulerStringId.BackgroundAnniversary:
                    return "Anniversary";
                case RadSchedulerStringId.BackgroundPhoneCall:
                    return "Phone Call";
                case RadSchedulerStringId.StatusBusy:
                    return "Busy";
                case RadSchedulerStringId.StatusFree:
                    return "Free";
                case RadSchedulerStringId.StatusTentative:
                    return "Tentative";
                case RadSchedulerStringId.StatusUnavailable:
                    return "Unavailable";
                case RadSchedulerStringId.ReminderNone:
                    return "None";
                case RadSchedulerStringId.ReminderOneMinute:
                    return "1 minute";
                case RadSchedulerStringId.ReminderMinutes:
                    return " minutes";
                case RadSchedulerStringId.ReminderOneSecond:
                    return "1 second";
                case RadSchedulerStringId.ReminderSeconds:
                    return " seconds";
                case RadSchedulerStringId.ReminderDays:
                    return " days";
                case RadSchedulerStringId.ReminderWeeks:
                    return " weeks";
                case RadSchedulerStringId.ReminderHours:
                    return " hours";
                case RadSchedulerStringId.ReminderZeroMinutes:
                    return "0 minutes";
                case RadSchedulerStringId.ReminderFiveMinutes:
                    return "5 minutes";
                case RadSchedulerStringId.ReminderTenMinutes:
                    return "10 minutes";
                case RadSchedulerStringId.ReminderFifteenMinutes:
                    return "15 minutes";
                case RadSchedulerStringId.ReminderThirtyMinutes:
                    return "30 minutes";
                case RadSchedulerStringId.ReminderOneHour:
                    return "1 hour";
                case RadSchedulerStringId.ReminderTwoHours:
                    return "2 hours";
                case RadSchedulerStringId.ReminderThreeHours:
                    return "3 hours";
                case RadSchedulerStringId.ReminderFourHours:
                    return "4 hours";
                case RadSchedulerStringId.ReminderFiveHours:
                    return "5 hours";
                case RadSchedulerStringId.ReminderSixHours:
                    return "6 hours";
                case RadSchedulerStringId.ReminderSevenHours:
                    return "7 hours";
                case RadSchedulerStringId.ReminderEightHours:
                    return "8 hours";
                case RadSchedulerStringId.ReminderNineHours:
                    return "9 hours";
                case RadSchedulerStringId.ReminderTenHours:
                    return "10 hours";
                case RadSchedulerStringId.ReminderElevenHours:
                    return "11 hours";
                case RadSchedulerStringId.ReminderTwelveHours:
                    return "12 hours";
                case RadSchedulerStringId.ReminderEighteenHours:
                    return "18 hours";
                case RadSchedulerStringId.ReminderOneDay:
                    return "1 day";
                case RadSchedulerStringId.ReminderTwoDays:
                    return "2 days";
                case RadSchedulerStringId.ReminderThreeDays:
                    return "3 days";
                case RadSchedulerStringId.ReminderFourDays:
                    return "4 days";
                case RadSchedulerStringId.ReminderOneWeek:
                    return "1 week";
                case RadSchedulerStringId.ReminderTwoWeeks:
                    return "2 weeks";
                case RadSchedulerStringId.Reminder:
                    return "Reminder";
                case RadSchedulerStringId.ContextMenuNewAppointment:
                    return "New Appointment";
                case RadSchedulerStringId.ContextMenuEditAppointment:
                    return "Edit Appointment";
                case RadSchedulerStringId.ContextMenuNewRecurringAppointment:
                    return "New Recurring Appointment";
                case RadSchedulerStringId.ContextMenu60Minutes:
                    return "60 Minutes";
                case RadSchedulerStringId.ContextMenu30Minutes:
                    return "30 Minutes";
                case RadSchedulerStringId.ContextMenu15Minutes:
                    return "15 Minutes";
                case RadSchedulerStringId.ContextMenu10Minutes:
                    return "10 Minutes";
                case RadSchedulerStringId.ContextMenu6Minutes:
                    return "6 Minutes";
                case RadSchedulerStringId.ContextMenu5Minutes:
                    return "5 Minutes";
                case RadSchedulerStringId.ContextMenuNavigateToNextView:
                    return "Next View";
                case RadSchedulerStringId.ContextMenuNavigateToPreviousView:
                    return "Previous View";
                case RadSchedulerStringId.ContextMenuTimescales:
                    return "Time Scales";
                case RadSchedulerStringId.ContextMenuTimescalesYear:
                    return "Year";
                case RadSchedulerStringId.ContextMenuTimescalesMonth:
                    return "Month";
                case RadSchedulerStringId.ContextMenuTimescalesWeek:
                    return "Week";
                case RadSchedulerStringId.ContextMenuTimescalesDay:
                    return "Day";
                case RadSchedulerStringId.ContextMenuTimescalesHour:
                    return "Hour";
                case RadSchedulerStringId.ContextMenuTimescalesHalfHour:
                    return "30 minutes";
                case RadSchedulerStringId.ContextMenuTimescalesFifteenMinutes:
                    return "15 minutes";
                case RadSchedulerStringId.ErrorProviderWrongAppointmentDates:
                    return "Appointment end time is less or equal to start time!";
                case RadSchedulerStringId.ErrorProviderWrongExceptionDuration:
                    return "Recurrence interval must be greater or equal to appointment duration!";
                case RadSchedulerStringId.ErrorProviderExceptionSameDate:
                    return "Two occurrences of the same series cannot occur on the same day.";
                case RadSchedulerStringId.ErrorProviderExceptionSkipOverDate:
                    return "Recurrence exception cannot skip over a later occurrence of the same appointment.";
                case RadSchedulerStringId.TimeZoneLocal:
                    return "Local";
            }
            return string.Empty;
        }
    }
}