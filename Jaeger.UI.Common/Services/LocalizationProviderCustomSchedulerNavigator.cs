﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public class LocalizationProviderCustomSchedulerNavigator : SchedulerNavigatorLocalizationProvider {
        public override string GetLocalizedString(string id) {
            switch (id) {
                case SchedulerNavigatorStringId.DayViewButtonCaption: {
                        return "Vista del día";
                    }
                case SchedulerNavigatorStringId.WeekViewButtonCaption: {
                        return "Vista de la semana";
                    }
                case SchedulerNavigatorStringId.MonthViewButtonCaption: {
                        return "Vista de mes";
                    }
                case SchedulerNavigatorStringId.TimelineViewButtonCaption: {
                        return "Vista cronológica";
                    }
                case SchedulerNavigatorStringId.ShowWeekendCheckboxCaption: {
                        return "Mostrar fin de semana";
                    }
                case SchedulerNavigatorStringId.TodayButtonCaptionToday: {
                        return "Hoy";
                    }
                case SchedulerNavigatorStringId.TodayButtonCaptionThisWeek: {
                        return "Esta semana";
                    }
                case SchedulerNavigatorStringId.TodayButtonCaptionThisMonth: {
                        return "Este mes";
                    }
                case SchedulerNavigatorStringId.SearchInAppointments: {
                        return "Buscar";
                    }
            }
            return string.Empty;
        }
    }
}