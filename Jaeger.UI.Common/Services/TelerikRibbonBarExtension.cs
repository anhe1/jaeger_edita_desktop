﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class TelerikRibbonBarExtension {
        public static void SetDefaultTab(this RadRibbonBar MenuRibbonBar) {
            // set tab active
            for (int i = 0; i < MenuRibbonBar.CommandTabs.Count; i++) {
                if (MenuRibbonBar.CommandTabs[i].Enabled == true) {
                    var selected = MenuRibbonBar.CommandTabs[i] as RibbonTab;
                    if (selected.Visibility == ElementVisibility.Visible) {
                        selected.IsSelected = true;
                        break;
                    } else {
                        selected.IsSelected = false;
                    }
                }
            }
        }
    }
}
