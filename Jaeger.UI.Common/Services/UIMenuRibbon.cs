﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    /// <summary>
    /// Comun para barra
    /// </summary>
    public static class UIMenuRibbon {
        public static RadSplitButtonElement BEmpresa(string rfc, string clave, string ver) {
            var version = Version;
            version.Text = string.Format("Version: {0}", ver);
            var usuario = Usuario;
            usuario.Text = string.Format("Usuario: {0}", clave);
            var empresa = Empresa;
            empresa.Text = string.Format("Empresa: {0}", rfc);

            var d1 = new RadSplitButtonElement {
                ArrowButtonMinSize = new System.Drawing.Size(12, 12),
                DefaultItem = null,
                DropDownDirection = RadDirection.Down,
                ExpandArrowButton = false,
                Name = "BEmpresa",
                Text = rfc,
                Image = UI.Common.Properties.Resources.administrator_male_16px,
                TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
            };
            d1.Items.AddRange(new Telerik.WinControls.RadItem[] {
                    empresa,
                    usuario,
                    version
                });
            return d1;
        }

        public static RadMenuItem Usuario {
            get {
                return new RadMenuItem {
                    Name = "Usuario",
                    Text = "Usuario: {0}",
                    Image = Properties.Resources.user_16,
                    TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
                };
            }
        }

        public static RadMenuItem Empresa {
            get {
                return new RadMenuItem {
                    Name = "Empresa",
                    Text = "Empresa: {0}"
                };
            }
        }

        public static RadMenuItem Version {
            get {
                return new RadMenuItem {
                    Name = "Version",
                    Text = "Version: {0}"
                };
            }
        }
    }
}
