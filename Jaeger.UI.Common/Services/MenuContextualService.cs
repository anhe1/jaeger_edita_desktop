﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    /// <summary>
    /// comunes
    /// </summary>
    public static class MenuContextualService {

        public static RadMenuItem Nuevo {
            get { return new RadMenuItem { Text = "Nuevo" }; }
        }

        public static RadMenuItem Editar {
            get { return new RadMenuItem { Text = "Editar" }; }
        }

        public static RadMenuItem Copiar {
            get { return new RadMenuItem { Text = "Copiar" }; }
        }

        public static RadMenuItem Remover {
            get { return new RadMenuItem { Text = "Remover" }; }
        }

        public static RadMenuItem Cancelar {
            get { return new RadMenuItem { Text = "Cancelar" }; }
        }

        public static RadMenuItem Imprimir {
            get { return new RadMenuItem { Text = "Imprimir" }; }
        }

        public static RadMenuItem SeleccionMultiple {
            get { return new RadMenuItem { Text = "Selección múltiple" }; }
        }

        public static RadMenuItem Descargar {
            get { return new RadMenuItem { Text = "Descargar" }; }
        }

        public static RadMenuItem Cargar {
            get { return new RadMenuItem { Text = "Cargar" }; }
        }
    }
}
