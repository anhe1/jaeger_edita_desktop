﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public class ChartPrinter : IPrintable {
        private RadChartView chart;

        public ChartPrinter(RadChartView chart) {
            this.chart = chart;
        }

        public int BeginPrint(RadPrintDocument sender, PrintEventArgs args) {
            return 1;
        }

        public bool EndPrint(RadPrintDocument sender, PrintEventArgs args) {
            return false;
        }

        public Form GetSettingsDialog(RadPrintDocument document) {
            return null;
        }

        public bool PrintPage(int pageNumber, RadPrintDocument sender, PrintPageEventArgs args) {
            float scale = Math.Min((float)args.MarginBounds.Width / chart.Size.Width, (float)args.MarginBounds.Height / chart.Size.Height);
            Bitmap chartBitmap = this.chart.RootElement.GetAsBitmap(Brushes.Transparent, 0, new SizeF(1, 1));
            Matrix saveMatrix = args.Graphics.Transform;
            args.Graphics.ScaleTransform(scale, scale);
            args.Graphics.DrawImage(chartBitmap, args.MarginBounds.Location);
            args.Graphics.Transform = saveMatrix;

            return false;
        }
    }
}