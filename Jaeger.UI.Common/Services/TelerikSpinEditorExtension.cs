﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class TelerikSpinEditorExtension {
        public static void SetEditable(this RadSpinEditor sender, bool enabled) {
            sender.ReadOnly = !enabled;
            sender.ShowUpDownButtons = enabled;
        }
    }
}
