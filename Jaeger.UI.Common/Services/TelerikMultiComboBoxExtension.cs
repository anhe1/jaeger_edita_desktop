﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class TelerikMultiComboBoxExtension {
        public static void SetEditable(this RadMultiColumnComboBox combo, bool enabled) {
            if (enabled) {
                combo.SelectedIndex = -1;
                combo.AutoSizeDropDownToBestFit = true;
                if (combo.DataSource != null)
                    combo.SelectedValue = null;
            } else {
                combo.DropDownOpening += EditorControl_DropDownOpening;
                combo.EditorControl.CurrentRowChanging += EditorControl_CurrentRowChanging;
                combo.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                combo.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            }
        }

        public static void Filtering(this RadMultiColumnComboBox combo, bool enable) {
            if (enable) {
                if (!combo.EditorControl.EnableFiltering) {
                    combo.DropDownClosing += RadMultiColumnComboBoxDropDownClosing;

                    var multiColumnComboBoxElement = combo.MultiColumnComboBoxElement;
                    multiColumnComboBoxElement.EditorControl.EnableFiltering = true;
                    multiColumnComboBoxElement.EditorControl.ShowFilteringRow = true;
                    multiColumnComboBoxElement.EditorControl.FilterChanging +=
                        RadMultiColumnComboBoxEditorControlFilterChanging;
                }
            } else {
                if (combo.EditorControl.EnableFiltering) {
                    combo.DropDownClosing -= RadMultiColumnComboBoxDropDownClosing;

                    var multiColumnComboBoxElement = combo.MultiColumnComboBoxElement;
                    multiColumnComboBoxElement.EditorControl.EnableFiltering = false;
                    multiColumnComboBoxElement.EditorControl.ShowFilteringRow = false;
                    multiColumnComboBoxElement.EditorControl.FilterChanging -=
                        RadMultiColumnComboBoxEditorControlFilterChanging;
                }
            }
        }

        private static void RadMultiColumnComboBoxEditorControlFilterChanging(object sender, GridViewCollectionChangingEventArgs e) {
            var multiColumnComboGridView = sender as MultiColumnComboGridView;
            if (multiColumnComboGridView == null)
                return;
            if (multiColumnComboGridView.Parent == null)
                return;
            var radPopupControlBase = multiColumnComboGridView.Parent as MultiColumnComboPopupForm;
            if (radPopupControlBase == null)
                return;
            if (!radPopupControlBase.IsDisplayed)
                return;
            if (e.OldItems == null)
                return;
            if (e.OldItems.Count == 0)
                return;
            var filterDescriptor = e.OldItems[0] as FilterDescriptor;
            if (filterDescriptor == null)
                return;
            var grid = multiColumnComboGridView;
            var currentRow = grid.CurrentRow;
            if (currentRow.RowElementType != typeof(GridFilterRowElement))
                return;
            var value = currentRow.Cells[filterDescriptor.PropertyName].Value ?? string.Empty;
            if (value == null)
                return;
            string activeEditorString = null;
            if (grid.ActiveEditor != null) {
                var activeEditorValue = grid.ActiveEditor.Value ?? string.Empty;
                activeEditorString = activeEditorValue.ToString();
            }
            if (e.NewValue == null && value.ToString() != string.Empty &&
                !(activeEditorString != null && activeEditorString == string.Empty &&
                  grid.CurrentColumn.Name == filterDescriptor.PropertyName))
                e.Cancel = true;
        }

        public static void EditorControl_DropDownOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        private static void EditorControl_CurrentRowChanging(object sender, CurrentRowChangingEventArgs e) {
            e.Cancel = true;
        }

        private static void RadMultiColumnComboBoxDropDownClosing(object sender, RadPopupClosingEventArgs args) {
            var radMultiColumnComboBox = sender as RadMultiColumnComboBox;
            if (radMultiColumnComboBox == null)
                return;
            var popupForm = radMultiColumnComboBox.MultiColumnComboBoxElement.PopupForm;
            var editorControl = radMultiColumnComboBox.MultiColumnComboBoxElement.EditorControl;
            MultiColumnComboBoxDropDownClosingHandler(popupForm, editorControl, args);
        }

        private static void MultiColumnComboBoxDropDownClosingHandler(RadPopupControlBase popupForm, RadGridView editorControl, RadPopupClosingEventArgs args) {
            var mousePosition = Control.MousePosition;
            var xIn = mousePosition.X >= popupForm.Location.X &&
                      mousePosition.X <= popupForm.Location.X + popupForm.Size.Width;
            var yIn = mousePosition.Y >= popupForm.Location.Y &&
                      mousePosition.Y <= popupForm.Location.Y + popupForm.Size.Height;
            if (editorControl.CurrentRow.Index == -1 && xIn && yIn) {
                args.Cancel = true;
            } else {
                if (editorControl.ActiveEditor != null) {
                    editorControl.ActiveEditor.EndEdit();
                }
                editorControl.FilterChanging -= RadMultiColumnComboBoxEditorControlFilterChanging;
                foreach (var column in editorControl.Columns) {
                    column.FilterDescriptor = null;
                }
                editorControl.FilterChanging += RadMultiColumnComboBoxEditorControlFilterChanging;
            }
        }
    }
}
