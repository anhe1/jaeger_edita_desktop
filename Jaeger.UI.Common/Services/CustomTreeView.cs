﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public class CustomTreeView : RadTreeView {
        protected override RadTreeViewElement CreateTreeViewElement() {
            return new CustomTreeViewElement();
        }

        public override string ThemeClassName {
            get {
                return typeof(RadTreeView).FullName;
            }
            set {
                base.ThemeClassName = value;
            }
        }
    }
}
