﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public class RowDetailsViewDefinition : TableViewDefinition {
        public override IRowView CreateViewUIElement(GridViewInfo viewInfo) {
            GridTableElement tableElement = (GridTableElement)base.CreateViewUIElement(viewInfo);
            tableElement.ViewElement.RowLayout = CreateRowLayout();
            return tableElement;
        }

        public override IGridRowLayout CreateRowLayout() {
            return new RowDetailsLayout();
        }
    }
}
