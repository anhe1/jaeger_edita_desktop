﻿using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace Jaeger.UI.Common.Services {

    public class CustomDragDropService : TreeViewDragDropService {
        private RadTreeViewElement owner;
        private List<RadTreeNode> draggedNodes;

        public CustomDragDropService(RadTreeViewElement owner) : base(owner) {
            this.owner = owner;
        }

        //Save the dragged nodes
        protected override void PerformStart() {
            base.PerformStart();

            draggedNodes = new List<RadTreeNode>();
            foreach (RadTreeNode selectedNode in this.owner.SelectedNodes) {
                draggedNodes.Add(selectedNode);
            }
        }

        //Clean the saved nodes
        protected override void PerformStop() {
            base.PerformStop();
            draggedNodes.Clear();
        }

        //If tree element or node element is hovered, allow drop
        protected override void OnPreviewDragOver(RadDragOverEventArgs e) {
            base.OnPreviewDragOver(e);

            RadTreeViewElement targetElement = e.HitTarget as RadTreeViewElement;
            if (targetElement != null && targetElement.Equals(this.owner)) {
                e.CanDrop = true;
            } else if (e.HitTarget is TreeNodeElement) {
                e.CanDrop = true;

                foreach (RadTreeNode draggedNode in draggedNodes) {
                    RadTreeNode targetNode = (e.HitTarget as TreeNodeElement).Data.Parent;
                    if (draggedNode == targetNode) {
                        //prevent dragging of a parent node over some of its child nodes
                        e.CanDrop = false;
                        break;
                    }
                }
            }
        }

        //Create copies of the selected node(s) and add them to the hovered node/tree
        protected override void OnPreviewDragDrop(RadDropEventArgs e) {
            TreeNodeElement targetNodeElement = e.HitTarget as TreeNodeElement;
            RadTreeViewElement targetTreeView = targetNodeElement == null ? e.HitTarget as RadTreeViewElement
                                                : targetNodeElement.TreeViewElement;

            if (targetTreeView == null) {
                return;
            }

            targetTreeView.BeginUpdate();

            foreach (RadTreeNode node in draggedNodes) {
                DataRowView rowView = node.DataBoundItem as DataRowView;
                DataRow row = rowView.Row;
                DataTable dt = targetTreeView.DataSource as DataTable;
                if (dt == null) {
                    return;
                }

                //save expanded state and vertical scrollbar value                   

                if (targetNodeElement != null) {
                    if (CanShowDropHint(Cursor.Position, targetNodeElement)) {
                        //change node' parent
                        RadTreeNode nodeOver = targetNodeElement.Data.Parent;

                        if (nodeOver == null) {
                            nodeOver = targetNodeElement.Data;
                        }

                        DataRowView targetRowView = (DataRowView)nodeOver.DataBoundItem;
                        DataRow targetRow = targetRowView.Row;
                        if (row["ParentId"] != targetRow["ParentId"]) {
                            row["ParentId"] = targetRow["Id"];
                        }

                        DataRow rowToInsert = dt.NewRow();
                        rowToInsert["ParentId"] = row["ParentId"];
                        rowToInsert["Id"] = row["Id"];
                        rowToInsert["Title"] = row["Title"];
                        dt.Rows.Remove(row);
                        int targetIndex = GetTargetIndex(dt, targetNodeElement);

                        DropPosition position = this.GetDropPosition(e.DropLocation, targetNodeElement);
                        if (position == DropPosition.AfterNode) {
                            targetIndex++;
                        }
                        dt.Rows.InsertAt(rowToInsert, targetIndex);
                    } else {
                        RadTreeNode nodeOver = targetNodeElement.Data;
                        DataRowView targetRowView = (DataRowView)nodeOver.DataBoundItem;
                        DataRow targetRow = targetRowView.Row;
                        row["ParentId"] = targetRow["Id"];
                    }
                } else {
                    //make the node "root node"
                    row["ParentId"] = null;
                }

                object ds = targetTreeView.DataSource;
                targetTreeView.DataSource = null;
                targetTreeView.DataSource = ds;

                targetTreeView.Update(RadTreeViewElement.UpdateActions.ItemAdded);
                //restore expanded state and vertical scrollbar value
            }
            targetTreeView.EndUpdate();
        }

        private int GetTargetIndex(DataTable dt, TreeNodeElement targetNodeElement) {
            int index = 0;
            DataRowView targetRowView = (DataRowView)targetNodeElement.Data.DataBoundItem;
            DataRow targetRow = targetRowView.Row;
            index = dt.Rows.IndexOf(targetRow);

            return index;
        }

        private bool CanShowDropHint(Point point, TreeNodeElement nodeElement) {
            if (nodeElement == null) {
                return false;
            }

            Point client = nodeElement.PointFromScreen(point);
            int part = nodeElement.Size.Height / 3;
            return client.Y < part || client.Y > part * 2;
        }
    }
}
