﻿using Microsoft.Reporting.WinForms;
using System;
using System.IO;

namespace Jaeger.UI.Common.Services {
    public class HelperReportToPDF {
        private ReportViewer Viewer;
        private string pathLogo;
        private string base64String;

        public HelperReportToPDF() {
            this.Viewer = new ReportViewer();
            this.Viewer.ProcessingMode = ProcessingMode.Local;
        }

        public string PathLogo {
            get {
                return this.pathLogo;
            }
            set {
                this.pathLogo = value;
            }
        }

        public string LogoB64 {
            get; set;
        }

        public string LocalFileName {
            get; set;
        }

        public Stream ReportDefinition {
            set {
                this.Viewer.LocalReport.LoadReportDefinition(value);
            }
        }

        public void SetDisplayName(string name) {
            this.Viewer.LocalReport.DisplayName = name;
        }

        public void SetParameter(string nameParameter, string value) {
            this.Viewer.LocalReport.SetParameters(new ReportParameter(nameParameter, value));
        }

        public void SetDataSource(string name, System.Data.DataTable data) {
            this.Viewer.LocalReport.DataSources.Add(new ReportDataSource(name, data));
        }

        public void Procesar() {

            if (File.Exists(this.PathLogo)) {
                byte[] _logo = File.ReadAllBytes(this.PathLogo);
                this.LogoB64 = Convert.ToBase64String(_logo);
            } else {
                this.LogoB64 = this.ImageToBase64();
            }

            Warning[] warnings;
            string[] streamids;
            string mimeType = "application/pdf";
            string encoding;
            string filenameExtension = "pdf";

            try {
                this.Viewer.LocalReport.SetParameters(new ReportParameter("Logotipo", this.LogoB64));
            } catch (Exception ex) {
                foreach (var item in this.Viewer.LocalReport.GetParameters()) {
                    if (item.Name.ToLower() == "logotipo") {
                        this.Viewer.LocalReport.SetParameters(new ReportParameter("Logotipo", this.ImageToBase64()));
                        break;
                    }
                }
                    Console.WriteLine(ex.Message);
            }

            using (this.Viewer) {
                Viewer.RefreshReport();
                
                byte[] bytes = Viewer.LocalReport.Render(
                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                     out streamids, out warnings);

                using (var fs = new FileStream(this.LocalFileName, FileMode.Create)) {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }

        private string ImageToBase64() {
            using (System.Drawing.Image image = Properties.Resources.logo_tipo) {
                using (MemoryStream m = new MemoryStream()) {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }
    }
}
