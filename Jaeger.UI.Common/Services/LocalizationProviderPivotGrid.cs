﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public class LocalizationProviderPivotGrid : PivotGridLocalizationProvider {
        public override string GetLocalizedString(string id) {
            switch (id) {
                case PivotStringId.GrandTotal:
                    return "Gran Total";
                case PivotStringId.Values:
                    return "Valores";
                case PivotStringId.TotalP0:
                    return "Total {0}";
                case PivotStringId.Product:
                    return "Producto";
                case PivotStringId.StdDevP:
                    return "StdDevP";
                case PivotStringId.Min:
                    return "Min";
                case PivotStringId.Count:
                    return "Count";
                case PivotStringId.StdDev:
                    return "StdDev";
                case PivotStringId.Sum:
                    return "Suma";
                case PivotStringId.Average:
                    return "Promedio";
                case PivotStringId.Var:
                    return "Var";
                case PivotStringId.VarP:
                    return "VarP";
                case PivotStringId.GroupP0AggregateP1:
                    return "{0} {1}";
                case PivotStringId.YearGroupField:
                    return "Año";
                case PivotStringId.MonthGroupField:
                    return "Mes";
                case PivotStringId.QuarterGroupField:
                    return "Trimestre";
                case PivotStringId.WeekGroupField:
                    return "Semana";
                case PivotStringId.DayGroupField:
                    return "Día";
                case PivotStringId.HourGroupField:
                    return "Hora";
                case PivotStringId.MinuteGroupField:
                    return "Minuto";
                case PivotStringId.SecondsGroupField:
                    return "Segundo";
                case PivotStringId.P0Total:
                    return "{0} Total";
                case PivotStringId.PivotAggregateP0ofP1:
                    return "{0} de {1}";
                case PivotStringId.ExpandCollapseMenuItem:
                    return "Expandir";
                case PivotStringId.CollapseAllMenuItems:
                    return "Desplegar todo";
                case PivotStringId.ExpandAllMenuItems:
                    return "Expandir todo";
                case PivotStringId.CopyMenuItem:
                    return "Copiar";
                case PivotStringId.HideMenuItem:
                    return "Ocultar";
                case PivotStringId.SortMenuItem:
                    return "Ordenar";
                case PivotStringId.BestFitMenuItem:
                    return "Ajustar anchos";
                case PivotStringId.ReloadDataMenuItem:
                    return "Cargar de nuevo";
                case PivotStringId.FieldListMenuItem:
                    return "Mostrar lista de campos";
                case PivotStringId.SortAZMenuItem:
                    return "&Sort A-Z";
                case PivotStringId.SortZAMenuItem:
                    return "S&ort Z-A";
                case PivotStringId.SortOptionsMenuItem:
                    return "&More Sort Options ...";
                case PivotStringId.ClearFilterMenuItem:
                    return "&Clear Filter";
                case PivotStringId.LabelFilterMenuItem:
                    return "&Label Filter";
                case PivotStringId.ValueFilterMenuItem:
                    return "&Value Filter";
                case PivotStringId.AllNode:
                    return "(Select All)";
                case PivotStringId.FilterMenuItemEqual:
                    return "&Equals...";
                case PivotStringId.FilterMenuItemDoesNotEquals:
                    return "Does &Not Equal...";
                case PivotStringId.FilterMenuItemBeginsWith:
                    return "Begins W&ith...";
                case PivotStringId.FilterMenuItemDoesNotBeginWith:
                    return "Does No&t Begin With...";
                case PivotStringId.FIlterMenuItemEndsWith:
                    return "Ends Wi&th...";
                case PivotStringId.FilterMenuItemDoesNotEndsWith:
                    return "Does Not End Wit&h...";
                case PivotStringId.FilterMenuItemContains:
                    return "Cont&ains...";
                case PivotStringId.FilterMenuItemDoesNotContain:
                    return "&Does Not Contain...";
                case PivotStringId.FilterMenuItemGreaterThan:
                    return "&Greater Than...";
                case PivotStringId.FilterMenuItemGreaterThanOrEqualTo:
                    return "Greater Than &Or Equal To...";
                case PivotStringId.FilterMenuItemLessThan:
                    return "&Less Than...";
                case PivotStringId.FilterMenuItemLessThanOrEqualTo:
                    return "Less Than Or E&qual To...";
                case PivotStringId.FilterMenuItemBetween:
                    return "Bet&ween...";
                case PivotStringId.FilterMenuItemNotBetween:
                    return "Not &Between...";
                case PivotStringId.TopTenItem:
                    return "&Top 10...";
                case PivotStringId.AllNodeSelectAllSearchResult:
                    return "(Select All Search Result)";
                case PivotStringId.FilterMenuAvailableFilters:
                    return "&Available Filters";
                case PivotStringId.CheckBoxMenuItem:
                    return "Select Multiple Items";
                case PivotStringId.CalculationOptionsDialogNoCalculation:
                    return "No Calculations";
                case PivotStringId.CalculationOptionsDialogPrevious:
                    return "(previous)";
                case PivotStringId.CalculationOptionsDialogNext:
                    return "(next)";
                case PivotStringId.CalculationOptionsDialogGrandTotal:
                    return "% of Grand Total";
                case PivotStringId.CalculationOptionsDialogColumnTotal:
                    return "% of Column Total";
                case PivotStringId.CalculationOptionsDialogRowTotal:
                    return "% of Row Total";
                case PivotStringId.CalculationOptionsDialogOf:
                    return "% Of";
                case PivotStringId.CalculationOptionsDialogDifferenceFrom:
                    return "Difference From";
                case PivotStringId.CalculationOptionsDialogPercentDifferenceFrom:
                    return "% Difference From";
                case PivotStringId.CalculationOptionsDialogRunningTotalIn:
                    return "Running Total In";
                case PivotStringId.CalculationOptionsDialogPercentRunningTotalIn:
                    return "% Running Total In";
                case PivotStringId.CalculationOptionsDialogRankSmallestToLargest:
                    return "Rank Smallest to Largest";
                case PivotStringId.CalculationOptionsDialogRankLargestToSmallest:
                    return "Rank Largest to Smallest";
                case PivotStringId.CalculationOptionsDialogIndex:
                    return "Index";
                case PivotStringId.CalculationOptionsDialogShowValueAs:
                    return "Show value as ({0})";
                case PivotStringId.LabelFilterOptionsDialogEquals:
                    return "equals";
                case PivotStringId.LabelFilterOptionsDialogDoesNotEqual:
                    return "does not equal";
                case PivotStringId.LabelFilterOptionsDialogIsGreaterThen:
                    return "is greater than";
                case PivotStringId.LabelFilterOptionsDialogIsGreaterThanOrEqualTo:
                    return "is greater than or equal to";
                case PivotStringId.LabelFilterOptionsDialogIsLessThan:
                    return "is less than";
                case PivotStringId.LabelFilterOptionsDialogIsLessThanOrEqualTo:
                    return "is less than or equal to";
                case PivotStringId.LabelFilterOptionsDialogBeginsWith:
                    return "begins with";
                case PivotStringId.LabelFilterOptionsDialogDoesNotBeginWith:
                    return "does not begin with";
                case PivotStringId.LabelFilterOptionsDialogEndsWith:
                    return "ends with";
                case PivotStringId.LabelFilterOptionsDialogDoesNotEndsWith:
                    return "does not end with";
                case PivotStringId.LabelFilterOptionsDialogContains:
                    return "contains";
                case PivotStringId.LabelFilterOptionsDialogDoesNotContain:
                    return "does not contain";
                case PivotStringId.LabelFilterOptionsDialogIsBetween:
                    return "is between";
                case PivotStringId.LabelFilterOptionsDialogIsNotBetween:
                    return "is not between";
                case PivotStringId.LabelFilterOptionsDialogLabelFilter:
                    return "Label Filter ({0})";
                case PivotStringId.NumberFormatOptionsDialogCustomFormat:
                    return "Custom format";
                case PivotStringId.NumberFormatOptionsDialogFixedPoint:
                    return "Fixed-point with 2 decimal digits";
                case PivotStringId.NumberFormatOptionsDialogPrefixedCurrency:
                    return "$ prefixed currency with 2 decimal digits";
                case PivotStringId.NumberFormatOptionsDialogPostfixedCurrency:
                    return "€ postfixed currency with 2 decimal digits";
                case PivotStringId.NumberFormatOptionsDialogPostfixedTemperatureC:
                    return "°C postfixed temperature with 2 decimal digits";
                case PivotStringId.NumberFormatOptionsDialogPostfixedTemperatureF:
                    return "°F postfixed temperature with 2 decimal digits";
                case PivotStringId.NumberFormatOptionsDialogExponential:
                    return "Exponential (scientific)";
                case PivotStringId.NumberFormatOptionsDialogFormatOptions:
                    return "Format Options";
                case PivotStringId.NumberFormatOptionsDialogFormatOptionsDescription:
                    return "Format Options ({0})";
                case PivotStringId.SortOptionsDialogSortOptions:
                    return "Sort Options ({0})";
                case PivotStringId.Top10FilterOptionsDialogTop:
                    return "Top";
                case PivotStringId.Top10FilterOptionsDialogBottom:
                    return "Bottom";
                case PivotStringId.Top10FilterOptionsDialogItems:
                    return "Items";
                case PivotStringId.Top10FilterOptionsDialogPercent:
                    return "Percent";
                case PivotStringId.Top10FilterOptionsDialogTop10:
                    return "Top10 Filter ({0})";
                case PivotStringId.ValueFilter:
                    return "Value Filter ({0})";
                case PivotStringId.AggregateOptionsDialogGroupBoxText:
                    return "Summarize Values By";
                case PivotStringId.AggregateOptionsDialogLabelCustomName:
                    return "Custom Name:";
                case PivotStringId.AggregateOptionsDialogLabelDescription:
                    return "Choose the type of calculation that you want to use to summarize data from the selected field.";
                case PivotStringId.AggregateOptionsDialogLabelField:
                    return "FieLabelld Name";
                case PivotStringId.AggregateOptionsDialogLabelSourceName:
                    return "Source Name:";
                case PivotStringId.AggregateOptionsDialogText:
                    return "AggregateOptionsDialog";
                case PivotStringId.DialogButtonCancel:
                    return "Cancel";
                case PivotStringId.DialogButtonOK:
                    return "OK";
                case PivotStringId.CalculationOptionsDialogText:
                    return "CalculationOptionsDialog";
                case PivotStringId.CalculationOptionsDialogLabelBaseItem:
                    return "Base Item:";
                case PivotStringId.CalculationOptionsDialogLabelBaseField:
                    return "Base Field:";
                case PivotStringId.CalculationOptionsDialogGroupBoxText:
                    return "Show Value As";
                case PivotStringId.LabelFilterOptionsDialogGroupBoxText:
                    return "Show items for which the label";
                case PivotStringId.LabelFilterOptionsDialogText:
                    return "LabelFilterOptionsDialog";
                case PivotStringId.LabelFilterOptionsDialogLabelAnd:
                    return "and";
                case PivotStringId.NumberFormatOptionsDialogFormat:
                    return "Format";
                case PivotStringId.NumberFormatOptionsDialogLabelDescription:
                    return "The format should identify the measurement type of the value. ($, ¥, €, kg., lb.," + " m.) The format would be used for general computations such as Sum, Average, Min" + ", Max and others.";
                case PivotStringId.NumberFormatOptionsDialogText:
                    return "NumberFormatOptionsDialog";
                case PivotStringId.NumberFormatOptionsDialogGroupBoxText:
                    return "General Format";
                case PivotStringId.SortOptionsDialogAscending:
                    return "Sort Ascending (A-Z) by:";
                case PivotStringId.SortOptionsDialogDescending:
                    return "Sort Descending (Z-A) by:";
                case PivotStringId.SortOptionsDialogGroupBoxText:
                    return "Sort options";
                case PivotStringId.SortOptionsDialogText:
                    return "SortOptionsDialog";
                case PivotStringId.Top10FilterOptionsDialogGroupBoxText:
                    return "Show";
                case PivotStringId.Top10FilterOptionsDialogLabelBy:
                    return "by";
                case PivotStringId.Top10FilterOptionsDialogText:
                    return "Top10FilterOptionsDialog";
                case PivotStringId.ValueFilterOptionsDialogGroupBox:
                    return "Show items for which";
                case PivotStringId.ValueFilterOptionsDialogText:
                    return "ValueFilterOptionsDialog";
                case PivotStringId.DragDataItemsHere:
                    return "Drag data items here";
                case PivotStringId.DragColumnItemsHere:
                    return "arrastre los elementos de la columna aquí";
                case PivotStringId.DragItemsHere:
                    return "Arrastra elementos aquí";
                case PivotStringId.DragFilterItemsHere:
                    return "Arrastre los elementos del filtro aquí";
                case PivotStringId.DragRowItemsHere:
                    return "Drag row items here";
                case PivotStringId.ResultItemFormat:
                    return "Key: {0}; Aggregates: {1}";
                case PivotStringId.Error:
                    return "Error";
                case PivotStringId.KpiSchemaElementValidatorError:
                    return "Should have at least one KPI member defined (Goal, Status, Treand, Value)";
                case PivotStringId.SchemaElementValidatorMissingPropertyFormat:
                    return "Required property is missing: {0}";
                case PivotStringId.AdomdCellInfoToStringFormat:
                    return "Ordinal: {0} | Value: {1}";
                case PivotStringId.Aggregates:
                    return "Aggregates";
                case PivotStringId.FilterMenuTextBoxItemNullText:
                    return "Search...";
                case PivotStringId.FieldChooserFormButtonAdd:
                    return "Add to";
                case PivotStringId.FieldChooserFormFields:
                    return "Fields:";
                case PivotStringId.FieldChooserFormText:
                    return "Selector de campo";
                case PivotStringId.FieldChooserFormColumnArea:
                    return "Column Area";
                case PivotStringId.FieldChooserFormDataArea:
                    return "Data Area";
                case PivotStringId.FieldChooserFormFilterArea:
                    return "Filter Area";
                case PivotStringId.FieldChooserFormRowArea:
                    return "Row Area";
                case PivotStringId.FieldListlabelChooseFields:
                    return "Choose fields:";
                case PivotStringId.FieldListButtonUpdate:
                    return "Update";
                case PivotStringId.FieldListCheckBoxDeferUpdate:
                    return "Defer Layout Update";
                case PivotStringId.FieldListLabelDrag:
                    return "Drag fields between areas below:";
                case PivotStringId.FieldListLabelRowLabels:
                    return "Row Labels";
                case PivotStringId.FieldListLabelColumnLabels:
                    return "Column Labels";
                case PivotStringId.FieldListLabelReportFilter:
                    return "Report Filter";
                case PivotStringId.None:
                    return "None";
                case PivotStringId.PrintSettingsFitWidth:
                    return "Fit width";
                case PivotStringId.PrintSettingsFitHeight:
                    return "Fit height";
                case PivotStringId.PrintSettingsCompact:
                    return "Comapact";
                case PivotStringId.PrintSettingsTabular:
                    return "Tabular";
                case PivotStringId.PrintSettingsFitAll:
                    return "Fit all";
                case PivotStringId.PrintSettingsPrintOrder:
                    return "Print order";
                case PivotStringId.PrintSettingsThenOver:
                    return "Down, then over";
                case PivotStringId.PrintSettingsThenDown:
                    return "Over, then down";
                case PivotStringId.PrintSettingsFontsAndColors:
                    return "Fonts and colors";
                case PivotStringId.PrintSettingsBackground:
                    return "Background";
                case PivotStringId.PrintSettingsNone:
                    return "(none)";
                case PivotStringId.PrintSettingsFont:
                    return "Font";
                case PivotStringId.PrintSettingsGrantTotal:
                    return "GrandTotal cells:";
                case PivotStringId.PrintSettingsDescriptors:
                    return "Aggregate/group descriptors:";
                case PivotStringId.PrintSettingsSubTotal:
                    return "SubTotal cells:";
                case PivotStringId.PrintSettingsHeaderCells:
                    return "Column/row header cells:";
                case PivotStringId.PrintSettingsDataCells:
                    return "Data cells:";
                case PivotStringId.PrintSettingsGridLinesColor:
                    return "Grid lines color:";
                case PivotStringId.PrintSettingsSettings:
                    return "Settings";
                case PivotStringId.PrintSettingsLayuotType:
                    return "Layout Type:";
                case PivotStringId.PrintSettingsScaleMode:
                    return "Scale mode:";
                case PivotStringId.PrintSettingsPrintSelectionOnly:
                    return "Print selection only";
                case PivotStringId.PrintSettingsShowGridLines:
                    return "Show grid lines";
                case PivotStringId.CollapseMenuItem:
                    return "Collapse";
                case PivotStringId.CalcualtedFields:
                    return "Calculated Fields";
                case PivotStringId.Max:
                    return "Max";
                case PivotStringId.NullValue:
                    return "(blank)";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }
}