﻿using System.IO;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Services {
    public static class Extensions {
        public static string DialogFolderOpen(this string oPahInitial) {
            FolderBrowserDialog folder = new FolderBrowserDialog() { Description = "Selecciona la carpeta de trabajo para" };
            if (folder.ShowDialog() != DialogResult.OK)
                return oPahInitial;

            if (Directory.Exists(folder.SelectedPath) == false)
                return oPahInitial;
            return folder.SelectedPath;
        }

        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        public static void TextBoxOnlyNumbers_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }
    }
}
