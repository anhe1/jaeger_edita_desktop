﻿using System;

namespace Jaeger.UI.Common.Services {
    /// <summary>
    /// Specifies ellipsis format and alignment.
    /// </summary>
    [Flags]
	public enum EllipsisFormat {
		/// <summary>
		/// Text is not modified.
		/// </summary>
		None = 0,
		/// <summary>
		/// Text is trimmed at the end of the string. An ellipsis (...) is drawn in place of remaining text.
		/// </summary>
		End = 1,
		/// <summary>
		/// Text is trimmed at the begining of the string. An ellipsis (...) is drawn in place of remaining text. 
		/// </summary>
		Start = 2,
		/// <summary>
		/// Text is trimmed in the middle of the string. An ellipsis (...) is drawn in place of remaining text.
		/// </summary>
		Middle = 3,
		/// <summary>
		/// Preserve as much as possible of the drive and filename information. Must be combined with alignment information.
		/// </summary>
		Path = 4,
		/// <summary>
		/// Text is trimmed at a word boundary. Must be combined with alignment information.
		/// </summary>
		Word = 8
	}
}
