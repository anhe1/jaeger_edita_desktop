﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace Jaeger.UI.Common.Services {
    public static class TelerikDropDownListExtension {
        public static void SetEditable(this RadDropDownList combo, bool enabled) {
            if (enabled) {
                combo.SelectedIndex = -1;
                combo.SelectedValue = null;
                combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = !enabled;
                combo.ReadOnly = !enabled;
                combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Visible;
                combo.PopupOpening -= Combo_PopupOpening;
            } else {
                combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = !enabled;
                combo.ReadOnly = !enabled;
                combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                combo.PopupOpening += Combo_PopupOpening;
            }
        }

        public static void SetEditable(this CommandBarDropDownList combo, bool enabled) {
            if (enabled) {
                combo.SelectedIndex = -1;
                if (combo.DataSource != null) {
                    combo.SelectedValue = null;
                    combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = false;
                    combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Visible;
                }
            } else {
                combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            }
        }

        /// <summary>
        /// Note : The value of 'SelectNextOnDoubleClick' is set to 'False' in all cases.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="readOnly"></param>
        public static void SetReadOnly(this CommandBarDropDownList ddl, bool readOnly) {
            ddl.DropDownListElement.ArrowButton.Enabled = !readOnly; // Helps the user identify that the control is ReadOnly
            ddl.SelectNextOnDoubleClick = false;
            ddl.Popup.Enabled = !readOnly;

            // For this mode, prevent modification with the keyboard
            if (ddl.DropDownStyle == RadDropDownStyle.DropDown) {
                ddl.DropDownListElement.TextBox.TextBoxItem.ReadOnly = readOnly;
                ddl.EnableMouseWheel = !readOnly;
            }
        }

        private static void Combo_PopupOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }
    }
}
