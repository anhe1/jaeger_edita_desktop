﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using System.Windows.Forms;
using Telerik.WinControls.Layouts;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class TelerikTextBoxExtension {
        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        public static void TxbFolio_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }

        public static void CrearBoton(this RadTextBox textbox, RadButtonElement boton, string caption = "...") {
            boton.Text = caption;
            StackLayoutElement stackPanel = new StackLayoutElement();
            stackPanel.Orientation = Orientation.Horizontal;
            stackPanel.Margin = new Padding(1, 0, 1, 0);
            stackPanel.Children.Add(boton);

            RadTextBoxItem tbItem = textbox.TextBoxElement.TextBoxItem;
            textbox.TextBoxElement.Children.Remove(tbItem);
            DockLayoutPanel dockPanel = new DockLayoutPanel();
            dockPanel.Children.Add(stackPanel);
            dockPanel.Children.Add(tbItem);
            DockLayoutPanel.SetDock(tbItem, Telerik.WinControls.Layouts.Dock.Left);
            DockLayoutPanel.SetDock(stackPanel, Telerik.WinControls.Layouts.Dock.Right);
            textbox.TextBoxElement.Children.Add(dockPanel);
            textbox.TextBoxElement.Padding = new Padding(1, 1, 1, 1);
            tbItem.Margin = new Padding(0, 1, 0, 0);
            boton.Padding = new Padding(2, 0, 2, -2);
        }
    }
}
