﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Common.Services {
    public class RowDetailsRowElement : GridDataRowElement {
        protected override Type ThemeEffectiveType {
            get {
                return typeof(GridDataRowElement);
            }
        }

        public override void UpdateInfo() {
            if (!this.RowInfo.IsCurrent) {
                this.RowInfo.Height = TableElement.RowHeight;
            } else {
                this.RowInfo.Height = ((RowDetailsGrid)this.GridControl).DetailsRowHeight;
            }
            base.UpdateInfo();
        }
    }
}
