﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace Jaeger.UI.Common.Services {
    public static class TelerikDateTimeExtension {
        /// <summary>
        /// establecer las propiedades de solo lectura para un objeto RadDateTimePicker
        /// </summary>
        public static void SetEditable(this RadDateTimePicker datePicker, bool enabled) {
            if (enabled == false) {
                datePicker.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                datePicker.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Collapsed;
            } else {
                datePicker.SetToNullValue();
                datePicker.NullableValue = null;
            }
            datePicker.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
        }
    }
}
