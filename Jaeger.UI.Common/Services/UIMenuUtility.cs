﻿using System.Threading;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Common.Services {
    public class UIMenuUtility {
        public static void SetPermission(RadRibbonForm mainForm, UsuarioBase clientInfo, UIMenuItemPermission menuPermissions) {
            var mainMenu = mainForm.RibbonBar;
            SetPermission(clientInfo, ref mainMenu, menuPermissions);
            mainForm.Invalidate();
            mainForm.Refresh();
            mainMenu.Refresh();
        }

        public static void SetPermission(UsuarioBase user, ref RadRibbonBar menuStrip, UIMenuItemPermission itemPermission) {
            Thread.Sleep(1000);
            for (int i = 0; i < menuStrip.CommandTabs.Count; i++) {
                // 1. get Menu Item
                var ribbonTab = menuStrip.CommandTabs[i] as RibbonTab;
                // 2. Set permission
                bool hasPermission = itemPermission.HasPermission(ribbonTab.Name.ToLower(), user);
                if (itemPermission.CurrentItem != null) {
                    ribbonTab.ToolTipText = itemPermission.CurrentItem.ToolTipText;
                    ribbonTab.Text = itemPermission.CurrentItem.Label;
                }

                if (hasPermission) {
                    ribbonTab.Visibility = ElementVisibility.Visible;
                    ribbonTab.Enabled = true;
                } else {
                    switch (itemPermission.DeniedAction) {
                        case UIPermissionEnum.Disabled:
                            ribbonTab.Enabled = false;
                            break;
                        case UIPermissionEnum.Invisible:
                            ribbonTab.Visibility = ElementVisibility.Collapsed;
                            break;
                    }

                }
                SetTab(user, ref ribbonTab, itemPermission);
            }
        }

        private static void SetTab(UsuarioBase user, ref RibbonTab tab, UIMenuItemPermission itemPermission) {
            if (tab.Items.Count > 0) {
                // recorrer grupos
                foreach (var item in tab.Items)
                    // si es un grupo dentro del tab
                    if (item is RadRibbonBarGroup) {
                        SetMenuItem(user, item, itemPermission);
                        var itemG = item as RadRibbonBarGroup;
                        SetGroups(user, ref itemG, itemPermission);
                    }
            }
        }

        private static void SetGroups(UsuarioBase user, ref RadRibbonBarGroup barGroup, UIMenuItemPermission itemPermission) {
            foreach (var item in barGroup.Items) {
                if (item is RadButtonElement) {
                    SetMenuItem(user, item, itemPermission);
                } else if (item is RadRibbonBarButtonGroup) {
                    var _barButtonGroup = item as RadRibbonBarButtonGroup;
                    SetMenuItem(user, item, itemPermission);
                    SetBarButtonGroup(user, ref _barButtonGroup, itemPermission);
                } else if (item is RadDropDownButtonElement) {
                    var _dropDownButtonElement = item as RadDropDownButtonElement;
                    SetMenuItem(user, item, itemPermission);
                    SetDropDownButtonElement(user, ref _dropDownButtonElement, itemPermission);
                }
            }
        }

        private static void SetBarButtonGroup(UsuarioBase user, ref RadRibbonBarButtonGroup buttonGroup, UIMenuItemPermission itemPermission) {
            foreach (var item in buttonGroup.Items) {
                if (item is RadButtonElement) {
                    SetMenuItem(user, item, itemPermission);
                } else if (item is RadDropDownButtonElement) {
                    var _dropDownButtonElement = item as RadDropDownButtonElement;
                    SetDropDownButtonElement(user, ref _dropDownButtonElement, itemPermission);
                }
            }
        }

        private static void SetDropDownButtonElement(UsuarioBase user, ref RadDropDownButtonElement button, UIMenuItemPermission itemPermission) {
            SetMenuItem(user, button, itemPermission);
            foreach (var item in button.Items) {
                if (item is RadMenuItem) {
                    var item2 = item as RadMenuItem;
                    SetMenuItem(user, item, itemPermission);
                }
            }
        }

        private static void SetMenuItem(UsuarioBase user, RadItem item, UIMenuItemPermission itemPermission) {
            bool _default = false;
            bool hasPermission = itemPermission.HasPermission(item.Name.ToLower(), user);
            // para las etiquetas
            if (itemPermission.CurrentItem != null) {
                item.Text = itemPermission.CurrentItem.Label;
                item.ToolTipText = itemPermission.CurrentItem.ToolTipText;
                item.Tag = itemPermission.CurrentItem.Form;
                _default = itemPermission.CurrentItem.Default;
            }

            if (hasPermission) {
                item.Visibility = ElementVisibility.Visible;
                item.Enabled = true;
            } else {
                if (_default == false)
                    switch (itemPermission.DeniedAction) {
                        case UIPermissionEnum.Disabled:
                            item.Enabled = false;
                            break;
                        case UIPermissionEnum.Invisible:
                            item.Visibility = ElementVisibility.Collapsed;
                            break;
                    }
            }
        }
    }
}