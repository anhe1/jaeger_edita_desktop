﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Data;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.Layouts;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class HelperTelerikExtension {
        /// <summary>
        /// Aplicar propiedades basicas al grid de telerik
        /// </summary>
        /// <param name="gridView">referencia al grid</param>
        public static bool TelerikGridCommon(this RadGridView gridView) {
            try {
                RadGridView radGridView = gridView;
                radGridView.AutoGenerateColumns = false;
                radGridView.AllowAddNewRow = false;
                radGridView.AllowEditRow = false;
                radGridView.AllowDeleteRow = false;
                radGridView.EnableFiltering = true;
                radGridView.ShowGroupPanel = false;
                radGridView.ShowFilteringRow = false;
                radGridView.EnableAlternatingRowColor = true;
                radGridView.AutoSizeRows = false;
                radGridView.AllowRowResize = false;
                radGridView = null;
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public static bool TelerikTemplateCommon1(this GridViewTemplate gridView) {
            GridViewTemplate gridViewTemplate = gridView;
            gridViewTemplate.AllowAddNewRow = false;
            gridViewTemplate.AllowEditRow = false;
            gridViewTemplate.AllowDeleteRow = false;
            gridViewTemplate.EnableFiltering = true;
            gridViewTemplate.ShowFilteringRow = false;
            gridViewTemplate.EnableAlternatingRowColor = true;
            gridViewTemplate.AllowRowResize = false;
            gridViewTemplate = null;
            return true;
        }

        /// <summary>
        /// con esta funcion agregamos fila de sumas de totales, en base al tipo de dato en el gridview
        /// </summary>
        /// <param name="gridView">referencia al grid</param>
        /// <param name="blnClear">para definir si debemos agregar o quitar la funcion de auto suma</param>
        public static bool GridAutoSum(ref RadGridView gridView, bool blnClear = false) {
            if (blnClear) {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            } else {
                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
                foreach (var item in gridView.Columns) {
                    if (item.DataType == typeof(double)) {
                        GridViewSummaryItem summaryItem = new GridViewSummaryItem();
                        summaryItem.Name = item.Name;
                        summaryItem.FormatString = item.FormatString;
                        summaryItem.Aggregate = GridAggregateFunction.Sum;
                        summaryRowItem.Add(summaryItem);
                    }
                }
                gridView.SummaryRowsTop.Add(summaryRowItem);
                gridView.SummaryRowsBottom.Add(summaryRowItem);
            }
            return true;
        }

        public static void TelerikGridAutoSum(this RadGridView gridView, bool clear = false) {
            if (clear == true) {
                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
                foreach (GridViewDataColumn item in gridView.Columns) {
                    if (item.DataType == typeof(double) | item.DataType == typeof(decimal)) {
                        var summaryItem = new GridViewSummaryItem();
                        summaryItem.Name = item.Name;
                        summaryItem.FormatString = item.FormatString;
                        summaryItem.Aggregate = GridAggregateFunction.Sum;
                        summaryRowItem.Add(summaryItem);
                    }
                }
                gridView.SummaryRowsTop.Add(summaryRowItem);
                gridView.SummaryRowsBottom.Add(summaryRowItem);
                gridView.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
            } else {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            }
        }

        public static void GridAutoSum(this GridViewTemplate gridView, bool clear = false) {
            if (clear == true) {
                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
                foreach (GridViewDataColumn item in gridView.Columns) {
                    if (item.DataType == typeof(double) | item.DataType == typeof(decimal)) {
                        var summaryItem = new GridViewSummaryItem {
                            Name = item.Name,
                            FormatString = item.FormatString,
                            Aggregate = GridAggregateFunction.Sum
                        };
                        summaryRowItem.Add(summaryItem);
                    }
                }
                gridView.SummaryRowsTop.Add(summaryRowItem);
                gridView.SummaryRowsBottom.Add(summaryRowItem);
            } else {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            }
        }

        public static void TelerikGridClearGrouping(this RadGridView gridView) {
            gridView.GroupDescriptors.Clear();
        }

        public static void GriGrouping(this RadGridView gridView, ToggleState state) {
            gridView.ShowGroupPanel = state != ToggleState.On;
            if (gridView.ShowGroupPanel == false) {
                gridView.GroupDescriptors.Clear();
            } else {
                gridView.ShowGroupPanel = true;
            }
        }

        public static void GriGrouping(this RadGridView gridView, bool state) {
            gridView.ShowGroupPanel = state;
            if (gridView.ShowGroupPanel == false) {
                gridView.GroupDescriptors.Clear();
            } else {
                gridView.ShowGroupPanel = true;
            }
        }

        /// <summary>
        /// Aplicar archivo layout 
        /// </summary>
        /// <param name="gridView">referencia al grid</param>
        /// <param name="fileLayOut">nombre del archivo del layout</param>
        /// <returns>retornamos verdadero si fue posible aplicar el layout</returns>
        public static void TelerikGridLayOut(this RadGridView gridView, string fileLayOut) {
            if (!File.Exists(fileLayOut)) {
                //return false;
            } else {
                try {
                    gridView.LoadLayout(string.Concat(fileLayOut));
                    //return true;
                } catch (Exception e) {
                    Console.WriteLine("TelerikExtension_Error: " + e.Message);
                    //return false;
                }
            }
        }

        public static bool TelerikGridContextMenuOpening(this RadGridView gridView, ContextMenuOpeningEventArgs e, RadContextMenu contextMenu) {
            if (e.ContextMenuProvider is GridHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridRowHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridFilterCellElement) {
            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (gridView.CurrentRow.ViewInfo.ViewTemplate == gridView.MasterTemplate) {
                    e.ContextMenu = contextMenu.DropDown;
                }
            }
            return false;
        }

        /// <summary>
        /// devuelve el valor de una columna en la fila seleccionada de un grid, siempre el valor del nivel 0 en las filas
        /// </summary>
        /// <param name="row">currentrow (GridViewRowInfo)</param>
        /// <param name="column">nombre de la columna</param>
        public static object TelerikGridRowValue(this GridViewRowInfo row, string column) {
            object telerikGridRowValue;
            if (row == null) {
                telerikGridRowValue = -1;
            } else {
                GridViewDataRowInfo corriente = row as GridViewDataRowInfo;
                if (corriente == null) {
                    telerikGridRowValue = -1;
                } else {
                    telerikGridRowValue = (corriente.HierarchyLevel != 0 ? corriente.ViewInfo.ParentRow.Cells[column].Value : corriente.Cells[column].Value);
                }
            }
            return telerikGridRowValue;
        }

        public static void TelerikGridRowCellValue(this GridViewRowInfo row, string column, object value) {
            if ((row != null)) {
                GridViewDataRowInfo corriente = row as GridViewDataRowInfo;
                if ((corriente != null)) {
                    if (corriente.HierarchyLevel == 0) {
                        corriente.Cells[column].Value = value;
                    } else {
                        corriente.ViewInfo.ParentRow.Cells[column].Value = value;
                    }
                }
            }
        }

        /// <summary>
        /// con este metodo refresacamos la vista de la fila seleccionada, principalmente utilizado las vistas de templetes
        /// </summary>
        /// <param name="row">GridViewHierarchyRowInfo</param>
        public static void TelerikRowRefresh(this GridViewHierarchyRowInfo row) {
            foreach (var item in row.Views) { item.Refresh(); }
        }

        /// <summary>
        /// con este metodo refresacamos la vista de la fila seleccionada, principalmente utilizado las vistas de templetes
        /// </summary>
        /// <param name="row">GridViewHierarchyRowInfo</param>
        public static void TelerikRowRefresh(this GridViewRowInfo row) {
            foreach (var item in (row as GridViewHierarchyRowInfo).Views) { item.Refresh(); row.IsExpanded = false; }
        }

        public static void CrearBoton(this RadTextBox textbox, RadButtonElement boton, string caption = "...") {
            boton.Text = caption;
            StackLayoutElement stackPanel = new StackLayoutElement();
            stackPanel.Orientation = Orientation.Horizontal;
            stackPanel.Margin = new Padding(1, 0, 1, 0);
            stackPanel.Children.Add(boton);

            RadTextBoxItem tbItem = textbox.TextBoxElement.TextBoxItem;
            textbox.TextBoxElement.Children.Remove(tbItem);
            DockLayoutPanel dockPanel = new DockLayoutPanel();
            dockPanel.Children.Add(stackPanel);
            dockPanel.Children.Add(tbItem);
            DockLayoutPanel.SetDock(tbItem, Telerik.WinControls.Layouts.Dock.Left);
            DockLayoutPanel.SetDock(stackPanel, Telerik.WinControls.Layouts.Dock.Right);
            textbox.TextBoxElement.Children.Add(dockPanel);
            textbox.TextBoxElement.Padding = new Padding(1, 1, 1, 1);
            tbItem.Margin = new Padding(0, 1, 0, 0);
            boton.Padding = new Padding(2, 0, 2, -2);
        }

        public static void DialogoAbrirArchivo(this RadTextBox textbox, string filtro = "", string ruta = "", string titulo = "Selecciona archivo") {
            OpenFileDialog dialogoAbrirArchivo = new OpenFileDialog() {
                Title = titulo,
                Multiselect = false,
                InitialDirectory = ruta,
                Filter = filtro
            };

            string temporal = textbox.Text;
            dialogoAbrirArchivo.FileName = textbox.Text;

            if (dialogoAbrirArchivo.ShowDialog() != DialogResult.OK) {
                textbox.Text = temporal;
            }

            if (File.Exists(dialogoAbrirArchivo.FileName)) {
                textbox.Text = dialogoAbrirArchivo.FileName;
            } else {
                textbox.Text = temporal;
            }
        }

        public static void DialogOpenFile(ref RadTextBox objTextBox, string strFilter = "", string startPath = "", string title = "Selecciona archivo") {
            OpenFileDialog openFileDialog1 = new OpenFileDialog() {
                Title = title,
                Multiselect = false,
                InitialDirectory = startPath == "" ? Convert.ToString(Environment.SpecialFolder.MyDocuments) : startPath,
                Filter = strFilter
            };

            OpenFileDialog openFileDialog = openFileDialog1;
            string strTemp = objTextBox.Text;
            openFileDialog.FileName = objTextBox.Text;
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                objTextBox.Text = strTemp;
            }
            if (!File.Exists(openFileDialog.FileName)) {
                objTextBox.Text = strTemp;
            } else {
                objTextBox.Text = openFileDialog.FileName;
            }
        }

        public static object ReturnRowSelected(this RadGridView radGrid) {
            if (radGrid.CurrentRow != null) {
                try {
                    var seleccionado = radGrid.CurrentRow.DataBoundItem as object;
                    return seleccionado;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// activar fila de filtros
        /// </summary>
        /// <param name="gridDataView">referencia del data grid view</param>
        /// <param name="toggleState"></param>
        public static void ActivateFilterRow(this RadGridView gridDataView, ToggleState toggleState) {
            gridDataView.ShowFilteringRow = toggleState != ToggleState.On;
            if (gridDataView.ShowFilteringRow == false)
                gridDataView.FilterDescriptors.Clear();
        }

        public static void SetDefaultTab(this RadRibbonBar MenuRibbonBar) {
            // set tab active
            for (int i = 0; i < MenuRibbonBar.CommandTabs.Count; i++) {
                if (MenuRibbonBar.CommandTabs[i].Enabled == true) {
                    var selected = MenuRibbonBar.CommandTabs[i] as RibbonTab;
                    if (selected.Visibility == ElementVisibility.Visible) {
                        selected.IsSelected = true;
                        break;
                    } else {
                        selected.IsSelected = false;
                    }
                }
            }
        }

        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        public static void TxbFolio_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }

        public static void SetEditable(this RadSpinEditor sender, bool enabled) {
            sender.ReadOnly = !enabled;
            sender.ShowUpDownButtons = enabled;
        }

        /// <summary>
        /// establecer las propiedades de solo lectura para un objeto RadDateTimePicker
        /// </summary>
        public static void SetEditable(this RadDateTimePicker datePicker, bool enabled) {
            if (enabled == false) {
                datePicker.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                datePicker.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Collapsed;
            } else {
                datePicker.SetToNullValue();
                datePicker.NullableValue = null;
            }
            datePicker.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
        }

        public static void SetEditable(this RadMultiColumnComboBox combo, bool enabled) {
            if (enabled) {
                combo.SelectedIndex = -1;
                combo.AutoSizeDropDownToBestFit = true;
                if (combo.DataSource != null)
                    combo.SelectedValue = null;
            } else {
                combo.DropDownOpening += EditorControl_DropDownOpening;
                combo.EditorControl.CurrentRowChanging += EditorControl_CurrentRowChanging;
                combo.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                combo.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            }
        }

        private static void EditorControl_CurrentRowChanging(object sender, CurrentRowChangingEventArgs e) {
            e.Cancel = true;
        }

        public static void SetEditable(this CommandBarDropDownList combo, bool enabled) {
            if (enabled) {
                combo.SelectedIndex = -1;
                if (combo.DataSource != null) {
                    combo.SelectedValue = null;
                    combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = false;
                    combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Visible;
                }
            } else {
                combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            }
        }

        /// <summary>
        /// Note : The value of 'SelectNextOnDoubleClick' is set to 'False' in all cases.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="readOnly"></param>
        public static void SetReadOnly(this CommandBarDropDownList ddl, bool readOnly) {
            ddl.DropDownListElement.ArrowButton.Enabled = !readOnly; // Helps the user identify that the control is ReadOnly
            ddl.SelectNextOnDoubleClick = false;
            ddl.Popup.Enabled = !readOnly;

            // For this mode, prevent modification with the keyboard
            if (ddl.DropDownStyle == RadDropDownStyle.DropDown) {
                ddl.DropDownListElement.TextBox.TextBoxItem.ReadOnly = readOnly;
                ddl.EnableMouseWheel = !readOnly;
            }
        }

        public static void EditorControl_DropDownOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        public static void SetEditable(this RadDropDownList combo, bool enabled) {
            if (enabled) {
                combo.SelectedIndex = -1;
                combo.SelectedValue = null;
                combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = !enabled;
                combo.ReadOnly = !enabled;
                combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Visible;
                combo.PopupOpening -= Combo_PopupOpening;
            } else {
                combo.DropDownListElement.TextBox.TextBoxItem.ReadOnly = !enabled;
                combo.ReadOnly = !enabled;
                combo.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                combo.PopupOpening += Combo_PopupOpening;
            }
        }

        private static void Combo_PopupOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        public static void Filtering(this RadMultiColumnComboBox combo, bool enable) {
            if (enable) {
                if (!combo.EditorControl.EnableFiltering) {
                    combo.DropDownClosing += RadMultiColumnComboBoxDropDownClosing;

                    var multiColumnComboBoxElement = combo.MultiColumnComboBoxElement;
                    multiColumnComboBoxElement.EditorControl.EnableFiltering = true;
                    multiColumnComboBoxElement.EditorControl.ShowFilteringRow = true;
                    multiColumnComboBoxElement.EditorControl.FilterChanging +=
                        RadMultiColumnComboBoxEditorControlFilterChanging;
                }
            } else {
                if (combo.EditorControl.EnableFiltering) {
                    combo.DropDownClosing -= RadMultiColumnComboBoxDropDownClosing;

                    var multiColumnComboBoxElement = combo.MultiColumnComboBoxElement;
                    multiColumnComboBoxElement.EditorControl.EnableFiltering = false;
                    multiColumnComboBoxElement.EditorControl.ShowFilteringRow = false;
                    multiColumnComboBoxElement.EditorControl.FilterChanging -=
                        RadMultiColumnComboBoxEditorControlFilterChanging;
                }
            }
        }

        private static void RadMultiColumnComboBoxEditorControlFilterChanging(object sender, GridViewCollectionChangingEventArgs e) {
            var multiColumnComboGridView = sender as MultiColumnComboGridView;
            if (multiColumnComboGridView == null)
                return;
            if (multiColumnComboGridView.Parent == null)
                return;
            var radPopupControlBase = multiColumnComboGridView.Parent as MultiColumnComboPopupForm;
            if (radPopupControlBase == null)
                return;
            if (!radPopupControlBase.IsDisplayed)
                return;
            if (e.OldItems == null)
                return;
            if (e.OldItems.Count == 0)
                return;
            var filterDescriptor = e.OldItems[0] as FilterDescriptor;
            if (filterDescriptor == null)
                return;
            var grid = multiColumnComboGridView;
            var currentRow = grid.CurrentRow;
            if (currentRow.RowElementType != typeof(GridFilterRowElement))
                return;
            var value = currentRow.Cells[filterDescriptor.PropertyName].Value ?? string.Empty;
            if (value == null)
                return;
            string activeEditorString = null;
            if (grid.ActiveEditor != null) {
                var activeEditorValue = grid.ActiveEditor.Value ?? string.Empty;
                activeEditorString = activeEditorValue.ToString();
            }
            if (e.NewValue == null && value.ToString() != string.Empty &&
                !(activeEditorString != null && activeEditorString == string.Empty &&
                  grid.CurrentColumn.Name == filterDescriptor.PropertyName))
                e.Cancel = true;
        }

        private static void RadMultiColumnComboBoxDropDownClosing(object sender, RadPopupClosingEventArgs args) {
            var radMultiColumnComboBox = sender as RadMultiColumnComboBox;
            if (radMultiColumnComboBox == null)
                return;
            var popupForm = radMultiColumnComboBox.MultiColumnComboBoxElement.PopupForm;
            var editorControl = radMultiColumnComboBox.MultiColumnComboBoxElement.EditorControl;
            MultiColumnComboBoxDropDownClosingHandler(popupForm, editorControl, args);
        }

        private static void MultiColumnComboBoxDropDownClosingHandler(RadPopupControlBase popupForm, RadGridView editorControl, RadPopupClosingEventArgs args) {
            var mousePosition = Control.MousePosition;
            var xIn = mousePosition.X >= popupForm.Location.X &&
                      mousePosition.X <= popupForm.Location.X + popupForm.Size.Width;
            var yIn = mousePosition.Y >= popupForm.Location.Y &&
                      mousePosition.Y <= popupForm.Location.Y + popupForm.Size.Height;
            if (editorControl.CurrentRow.Index == -1 && xIn && yIn) {
                args.Cancel = true;
            } else {
                if (editorControl.ActiveEditor != null) {
                    editorControl.ActiveEditor.EndEdit();
                }
                editorControl.FilterChanging -= RadMultiColumnComboBoxEditorControlFilterChanging;
                foreach (var column in editorControl.Columns) {
                    column.FilterDescriptor = null;
                }
                editorControl.FilterChanging += RadMultiColumnComboBoxEditorControlFilterChanging;
            }
        }
    }
}
