﻿using Telerik.WinControls;

namespace Jaeger.UI.Common.Services {
    public class LocalizationProviderCustomerMessageBox : RadMessageLocalizationProvider {
        public override string GetLocalizedString(string id) {
            switch (id) {
                case RadMessageStringID.AbortButton:
                    return "Cancelar";
                case RadMessageStringID.CancelButton:
                    return "Eliminar";
                case RadMessageStringID.IgnoreButton:
                    return "Ignorar";
                case RadMessageStringID.NoButton:
                    return "No";
                case RadMessageStringID.OKButton:
                    return "OK";
                case RadMessageStringID.RetryButton:
                    return "Reintentar";
                case RadMessageStringID.YesButton:
                    return "Sí";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }
}
