﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Jaeger.Helpers
{
    public class HelperXmlConvert
    {
        [DebuggerNonUserCode]
        public HelperXmlConvert()
        {
        }

        public static T DeserializeObject<T>(string xml)
        {
            T deserializeObject;
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                MemoryStream memoryStream = new MemoryStream(HelperXmlConvert.StringToUtf8ByteArray(xml));
                deserializeObject = (T)xmlSerializer.Deserialize(memoryStream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                deserializeObject = default(T);
            }
            return deserializeObject;
        }

        public static object DeserializeObject(string xml, Type type)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(type);
            MemoryStream memoryStream = new MemoryStream(HelperXmlConvert.StringToUtf8ByteArray(xml));
            return xmlSerializer.Deserialize(memoryStream);
        }

        public static string SerializeObject<T>(T obj)
        {
            string empty;
            UTF8Encoding utf8WithoutBom = new UTF8Encoding(false);
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, utf8WithoutBom)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 4
                };
                xmlSerializer.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                empty = utf8WithoutBom.GetString(memoryStream.ToArray());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                empty = string.Empty;
            }
            return empty;
        }

        public static string SerializeObject(object obj, Type type)
        {
            string empty;
            UTF8Encoding utf8WithoutBom = new UTF8Encoding(false);
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xmlSerializer = new XmlSerializer(type);
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, utf8WithoutBom);
                xmlSerializer.Serialize(xmlTextWriter, RuntimeHelpers.GetObjectValue(obj));
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                empty = utf8WithoutBom.GetString(memoryStream.ToArray());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                empty = string.Empty;
            }
            return empty;
        }

        public static byte[] StringToUtf8ByteArray(string pXmlString)
        {
            return (new UTF8Encoding(false)).GetBytes(pXmlString);
        }
    }
}