﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class GridTelerikCommon {
        #region constantes para formatos
        public const string FormatStringMoney = "{0:N2}";
        public const string FormatStringDate = "{0:dd MMM yy}";
        public const string FormatStringP = "{0:P0}";
        public const string FormatStringNumber = "{0:N2}";
        public const string FormarStringFolio = "{0:00000#}";
        #endregion

        #region bases
        /// <summary>
        /// base para columna de importes
        /// </summary>
        public static GridViewTextBoxColumn ColBaseMoney {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewDateTimeColumn ColBaseDateTime {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FormatString = GridTelerikCommon.FormatStringDate,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }
        #endregion

        #region columnas de fechas
        /// <summary>
        /// Columna DateTime para fecha de emision (FechaEmision)
        /// </summary>
        public static GridViewDateTimeColumn ColFechaEmision {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaEmision",
                    FormatString = GridTelerikCommon.FormatStringDate,
                    HeaderText = "Fec. Emisión",
                    Name = "FechaEmision",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                };
            }
        }

        /// <summary>
        /// Columna DateTime para fecha de cancelacion (FechaCancela)
        /// </summary>
        public static GridViewTextBoxColumn ColFechaCancela {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaCancela",
                    FormatString = GridTelerikCommon.FormatStringDate,
                    HeaderText = "Fec. Cancela",
                    Name = "FechaCancela",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna DateTime para fecha de entrega (FechaEntrega)
        /// </summary>
        public static GridViewDateTimeColumn ColFechaEntrega {
            get {
                return new GridViewDateTimeColumn {
                    FieldName = "FechaEntrega",
                    FormatString = GridTelerikCommon.FormatStringDate,
                    HeaderText = "Fec. Entrega",
                    Name = "FechaEntrega",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna DateTime para fechas de pedido (FechaPedido)
        /// </summary>
        public static GridViewDateTimeColumn ColFechaPedido {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaPedido",
                    FormatString = GridTelerikCommon.FormatStringDate,
                    HeaderText = "Fec. Pedido",
                    Name = "FechaPedido",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public static GridViewTextBoxColumn ColFechaNuevo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaNuevo",
                    FormatString = GridTelerikCommon.FormatStringDate,
                    HeaderText = "Fec. Sist.",
                    Name = "FecNuevo",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// obtener columna de fecha de modificacion 
        /// </summary>
        public static GridViewDateTimeColumn ColFechaModifica {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaModifica",
                    FormatString = GridTelerikCommon.FormatStringDate,
                    HeaderText = "Fec. Mod.",
                    Name = "FechaModifica",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    ReadOnly = true,
                    IsVisible = false
                };
            }
        }
        #endregion

        /// <summary>
        /// columna para indice
        /// </summary>
        public static GridViewTextBoxColumn ColId {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Id",
                    HeaderText = "Id",
                    Width = 50,
                    Name = "Id",
                    IsVisible = false,
                    VisibleInColumnChooser = false,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColSubId {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id",
                    VisibleInColumnChooser = false,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// columna para correo electronico
        /// </summary>
        public static GridViewTextBoxColumn ColCorreo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Correo",
                    HeaderText = "Correo",
                    Width = 120,
                    Name = "Correo"
                };
            }
        }

        public static GridViewTextBoxColumn ColVersion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Version",
                    HeaderText = "Ver.",
                    IsPinned = true,
                    Name = "Version",
                    Width = 50,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Clave",
                    HeaderText = "Clave",
                    Name = "Clave",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColCliente {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Cliente",
                    HeaderText = "Cliente",
                    IsPinned = true,
                    Name = "Cliente",
                    Width = 280,
                    ReadOnly = true
                };
            }
        }

        #region columnas comunes del grid

        /// <summary>
        /// obtner columna de registro activo
        /// </summary>
        public static GridViewCheckBoxColumn ColCheckActivo {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "Activo",
                    HeaderText = "Activo",
                    IsVisible = false,
                    Name = "Activo",
                    VisibleInColumnChooser = false,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// Columna textbox para Folio (Folio)
        /// </summary>
        public static GridViewTextBoxColumn ColFolio {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Folio",
                    HeaderText = "Folio",
                    Name = "Folio",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75,
                    FormatString = FormarStringFolio,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// Columna textbox para Serie (Serie)
        /// </summary>
        public static GridViewTextBoxColumn ColSerie {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Serie",
                    HeaderText = "Serie",
                    Name = "Serie",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// combo box para columna de status
        /// </summary>
        public static GridViewComboBoxColumn ColStatus {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(string),
                    FieldName = "IdStatus",
                    HeaderText = "Status",
                    Name = "IdStatus",
                    Width = 75,
                    DisplayMember = "Descriptor",
                    ValueMember = "Id",
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public static GridViewTextBoxColumn ColCreo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Creo",
                    HeaderText = "Creo",
                    Name = "Creo",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// obtener columna de usuario que modifica
        /// </summary>
        public static GridViewTextBoxColumn ColModifica {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Modifica",
                    HeaderText = "Modifica",
                    Name = "Modifica",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    IsVisible = false,
                    ReadOnly = true
                };
            }
        }

        /// <summary>
        /// Columna TextBox para usuario camcela (Cancela)
        /// </summary>
        public static GridViewTextBoxColumn ColCancela {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Cancela",
                    HeaderText = "Usu. Cancela",
                    IsVisible = false,
                    Name = "Cancela",
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna textbox para RFC (RFC)
        /// </summary>
        public static GridViewTextBoxColumn ColRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RFC",
                    HeaderText = "RFC",
                    Name = "RFC",
                    ReadOnly = true,
                    Width = 90
                };
            }
        }

        /// <summary>
        /// Columna TextBox para IdDocumento (IdDocumento)
        /// </summary>
        public static GridViewTextBoxColumn ColIdDocumento {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "IdDocumento",
                    HeaderText = "IdDocumento",
                    Name = "IdDocumento",
                    ReadOnly = true,
                    Width = 180
                };
            }
        }

        /// <summary>
        /// Columna para Identificador (Identificador)
        /// </summary>
        public static GridViewTextBoxColumn ColIdentificador {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Identificador",
                    HeaderText = "Identificador",
                    Name = "Identificador",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 115
                };
            }
        }

        /// <summary>
        /// Columna para cantidad (Cantidad)
        /// </summary>
        public static GridViewTextBoxColumn ColCantidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Cantidad",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Cantidad",
                    Name = "Cantidad",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 65
                };
            }
        }

        /// <summary>
        /// Columna de unidad en modo texto (Unidad)
        /// </summary>
        public static GridViewTextBoxColumn ColUnidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Unidad",
                    HeaderText = "Unidad",
                    Name = "Unidad",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna ComboBox para unidades (IdUnidad)
        /// </summary>
        public static GridViewComboBoxColumn ColCboUnidad {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdUnidad",
                    HeaderText = "Unidad",
                    Name = "IdUnidad",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 80
                };
            }
        }

        /// <summary>
        /// Columna para valor unitario fieldName (ValorUnitario)
        /// </summary>
        public static GridViewTextBoxColumn ColValorUnitario {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "ValorUnitario",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Unitario",
                    Name = "ValorUnitario",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para unitario fieldName (Unitario)
        /// </summary>
        public static GridViewTextBoxColumn ColUnitario {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Unitario",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Unitario",
                    Name = "Unitario",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para valor importe fieldName (Importe)
        /// </summary>
        public static GridViewTextBoxColumn ColImporte {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Importe",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Importe",
                    Name = "Importe",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para total del traslado de IVA (TotalTrasladoIVA)
        /// </summary>
        public static GridViewTextBoxColumn ColTotalTrasladoIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalTrasladoIVA",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Tras. IVA",
                    Name = "TotalTrasladoIVA",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para la tasa de IVA (TasaIVA)
        /// </summary>
        public static GridViewTextBoxColumn ColTasaIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TasaIVA",
                    FormatString = GridTelerikCommon.FormatStringP,
                    HeaderText = "% IVA",
                    Name = "TasaIVA",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para total del descuento aplicado (TotalDescuento)
        /// </summary>
        public static GridViewTextBoxColumn ColTotalDescuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalDescuento",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Descuento",
                    Name = "TotalDescuento",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColTotalEnvio {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalEnvio",
                    FormatString = "{0:N2}",
                    HeaderText = "C. Envío",
                    Name = "TotalEnvio",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para sub total (SubTotal)
        /// </summary>
        public static GridViewTextBoxColumn ColSubTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "SubTotal",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "SubTotal",
                    Name = "SubTotal",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para total (Total)
        /// </summary>
        public static GridViewTextBoxColumn ColTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Total",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Total",
                    Name = "Total",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna para gran total (GTotal)
        /// </summary>
        public static GridViewTextBoxColumn ColGranTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "GTotal",
                    FormatString = GridTelerikCommon.FormatStringMoney,
                    HeaderText = "Total",
                    Name = "GTotal",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// Columna combobox de vendedor (IdVendedor)
        /// </summary>
        public static GridViewComboBoxColumn ColIdVendedor {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdVendedor",
                    HeaderText = "Vendedor",
                    Name = "IdVendedor",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Observaciones",
                    Name = "Nota",
                    Width = 185
                };
            }
        }
        #endregion

        #region columnas Grid de direccion
        public static GridViewCommandColumn ColAutorizado {
            get {
                return new GridViewCommandColumn {
                    FieldName = "AutorizadoText",
                    HeaderText = "Autorizado",
                    Name = "AutorizadoText",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColCalle {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Calle",
                    HeaderText = "Calle",
                    Name = "Calle",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColNoExterior {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "NoExterior",
                    HeaderText = "Núm. Ext.",
                    Name = "NoExterior"
                };
            }
        }

        public static GridViewTextBoxColumn ColNoInterior {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "NoInterior",
                    HeaderText = "Núm. Int.",
                    Name = "NoInterior"
                };
            }
        }

        public static GridViewComboBoxColumn ColIdAsentamiento {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "IdTipoAsentamiento",
                    HeaderText = "Asentamiento",
                    Name = "IdTipoAsentamiento",
                    Width = 150,
                    IsVisible = false
                };
            }
        }

        public static GridViewTextBoxColumn ColColonia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Colonia",
                    HeaderText = "Colonia",
                    Name = "Colonia",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColDelegacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Delegacion",
                    HeaderText = "Delegación",
                    Name = "Delegacion",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColCodigoPostal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "CodigoPostal",
                    HeaderText = "C. P.",
                    Name = "CodigoPostal"
                };
            }
        }

        public static GridViewTextBoxColumn ColCiudad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Ciudad",
                    HeaderText = "Ciudad",
                    Name = "Ciudad",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColEstado {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Estado",
                    HeaderText = "Estado",
                    Name = "Estado",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColPais {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Pais",
                    HeaderText = "País",
                    Name = "Pais"
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColLocalidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Localidad",
                    HeaderText = "Localidad",
                    Name = "Localidad",
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColReferencia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Referencia",
                    HeaderText = "Referencia",
                    Name = "Referencia",
                    Width = 100
                };
            }
        }
        #endregion

        #region formatos condicionales
        /// <summary>
        /// formato condicional para formato de numero negativos
        /// </summary>
        /// <param name="expression">expresion: Saldo < 0 </param>
        /// <param name="name">nombre de la condicion</param>
        public static ExpressionFormattingObject SaldoNegativo(string expression, string name = "Saldo") {
            var expression1 = new ExpressionFormattingObject {
                Name = name,
                Expression = expression,
                ApplyToRow = true,
                CellForeColor = Color.Red,
            };
            return expression1;
        }

        public static ExpressionFormattingObject RegistroActivo(string expression, string name = "Reg. Activo") {
            var expression1 = new ExpressionFormattingObject {
                Name = name,
                Expression = expression,
                ApplyToRow = true,
                CellForeColor = Color.DarkGray,
                RowForeColor = Color.DarkSlateGray,
            };
            return expression1;
        }
        #endregion
    }
}
