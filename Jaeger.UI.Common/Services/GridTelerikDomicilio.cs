﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    public static class GridTelerikDomicilio {
        #region columnas grid de domicilios
        public static GridViewCommandColumn ColAutorizado {
            get {
                return new GridViewCommandColumn {
                    FieldName = "AutorizadoText",
                    HeaderText = "Autorizado",
                    Name = "AutorizadoText",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColCalle {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Calle",
                    HeaderText = "Calle",
                    Name = "Calle",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColNoExterior {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "NoExterior",
                    HeaderText = "Núm. Ext.",
                    Name = "NoExterior"
                };
            }
        }

        public static GridViewTextBoxColumn ColNoInterior {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "NoInterior",
                    HeaderText = "Núm. Int.",
                    Name = "NoInterior"
                };
            }
        }

        public static GridViewComboBoxColumn ColIdAsentamiento {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "IdTipoAsentamiento",
                    HeaderText = "Asentamiento",
                    Name = "IdTipoAsentamiento",
                    Width = 150,
                    IsVisible = false
                };
            }
        }

        public static GridViewTextBoxColumn ColColonia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Colonia",
                    HeaderText = "Colonia",
                    Name = "Colonia",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColDelegacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Delegacion",
                    HeaderText = "Delegación",
                    Name = "Delegacion",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColCodigoPostal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "CodigoPostal",
                    HeaderText = "C. P.",
                    Name = "CodigoPostal"
                };
            }
        }

        public static GridViewTextBoxColumn ColCiudad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Ciudad",
                    HeaderText = "Ciudad",
                    Name = "Ciudad",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColEstado {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Estado",
                    HeaderText = "Estado",
                    Name = "Estado",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColPais {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Pais",
                    HeaderText = "País",
                    Name = "Pais"
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColLocalidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Localidad",
                    HeaderText = "Localidad",
                    Name = "Localidad",
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColReferencia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Referencia",
                    HeaderText = "Referencia",
                    Name = "Referencia",
                    Width = 100
                };
            }
        }
        #endregion
    }
}
