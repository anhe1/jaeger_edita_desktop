﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace Jaeger.Helpers
{
    public class HelperTicket
    {
        #region declaraciones

        private int contadorGlobal;
        private readonly DataTable dtbLineas;
        private bool disposedValue = false;
        private double heightPage = 0;
        private double widthPage = 0;
        private int letrasPorLinea = 25;
        private Image headerImage;
        private Image qRCode;
        private int margenIzqLogo = 20;
        private int margenIzqQRCode = 100;
        private int imageHeight = 0;
        private int contador = 0;
        private int noPagina = 0;
        private HelperTicket.AlineacionLetra alineacionField;
        private double margenIzquierdo = 0;
        private double margenSuperiorField = 5;
        private string nombreDeLaFuente = "Lucida Console";
        private int tamanoDeLaFuente = 8;
        private Font fuenteImpresa;
        private readonly SolidBrush colorDeLaFuente = new SolidBrush(Color.Black);
        private PrintDocument withEventsFieldDocumentoAImprimir = new PrintDocument();

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public HelperTicket()
        {
            this.dtbLineas = new DataTable();
            this.dtbLineas.Columns.Add("Texto", Type.GetType("System.String"));
            this.dtbLineas.Columns.Add("Tamano", Type.GetType("System.Int32"));
            this.dtbLineas.Columns.Add("Estilo", Type.GetType("System.Int32"));
            this.DocumentoAImprimir.BeginPrint += new PrintEventHandler(this.DocumentoAImprimir_BeginPrint);
            this.DocumentoAImprimir.PrintPage += new PrintPageEventHandler(this.DocumentoAImprimir_PrintPage);
        }

        #region propiedades

        public HelperTicket.AlineacionLetra Alineacion
        {
            get
            {
                return this.alineacionField;
            }
            set
            {
                this.alineacionField = value;
            }
        }

        public PrintDocument DocumentoAImprimir
        {
            get
            {
                return this.withEventsFieldDocumentoAImprimir;
            }
            set
            {
                if (this.withEventsFieldDocumentoAImprimir != null)
                {
                    this.withEventsFieldDocumentoAImprimir.BeginPrint -= new PrintEventHandler(this.DocumentoAImprimir_BeginPrint);
                    this.withEventsFieldDocumentoAImprimir.PrintPage -= new PrintPageEventHandler(this.DocumentoAImprimir_PrintPage);
                }
                this.withEventsFieldDocumentoAImprimir = value;
                if (this.withEventsFieldDocumentoAImprimir != null)
                {
                    this.withEventsFieldDocumentoAImprimir.BeginPrint += new PrintEventHandler(this.DocumentoAImprimir_BeginPrint);
                    this.withEventsFieldDocumentoAImprimir.PrintPage += new PrintPageEventHandler(this.DocumentoAImprimir_PrintPage);
                }
            }
        }

        public int LetrasPorLinea
        {
            get
            {
                return this.letrasPorLinea;
            }
            set
            {
                this.letrasPorLinea = value;
            }
        }

        public double MargenSuperior
        {
            get
            {
                return this.margenSuperiorField;
            }
            set
            {
                this.margenSuperiorField = value;
            }
        }

        public int MargenIzqLogo
        {
            get
            {
                return this.margenIzqLogo;
            }
            set
            {
                this.margenIzqLogo = value;
            }
        }

        public int MargenIzqQRCode
        {
            get
            {
                return this.margenIzqQRCode;
            }
            set
            {
                this.margenIzqQRCode = value;
            }
        }

        public double MargenIzquierdo
        {
            get
            {
                return this.margenIzquierdo;
            }
            set
            {
                this.margenIzquierdo = value;
            }
        }

        public string NombreLetra
        {
            get
            {
                return this.nombreDeLaFuente;
            }
            set
            {
                if (value != this.nombreDeLaFuente)
                {
                    this.nombreDeLaFuente = value;
                }
            }
        }

        public Image QrCode
        {
            get
            {
                return this.qRCode;
            }
            set
            {
                this.qRCode = value;
            }
        }

        public Image Logotipo
        {
            get
            {
                return this.headerImage;
            }
            set
            {
                this.headerImage = value;
            }
        }

        public int TamanoLetra
        {
            get
            {
                return this.tamanoDeLaFuente;
            }
            set
            {
                if (value != this.tamanoDeLaFuente)
                {
                    this.tamanoDeLaFuente = value;
                }
            }
        }

        #endregion

        #region metodos publicos

        public void AgregarConcepto(string[] valorcolumna, int[] anchocolumna, HelperTicket.TamanoDeLetra tamanoLetra, FontStyle estiloFuente)
        {
            int i;
            if (valorcolumna.Length != anchocolumna.Length)
            {
                MessageBox.Show("No coinciden los valores");
            }
            else
            {
                string[][] strArrays = new string[valorcolumna.Length][];
                for (i = 0; i < valorcolumna.Length; i++)
                {
                    strArrays[i] = this.CuentaCaracteresCol(valorcolumna[i], anchocolumna[i]);
                }
                int length = strArrays[0].Length;
                for (i = 0; i < strArrays.Length; i++)
                {
                    if (strArrays[i].Length > length)
                    {
                        length = strArrays[i].Length;
                    }
                }
                int num = 0;
                while (num < length)
                {
                    string str = "";
                    for (int j = 0; j < valorcolumna.Length; j++)
                    {
                        try
                        {
                            if (j < strArrays[j][num].Length)
                            {
                                str = string.Concat(str, strArrays[j][num], " ");
                            }
                        }
                        catch
                        {
                            for (int k = 0; k < anchocolumna[j]; k++)
                            {
                                str = string.Concat(str, " ");
                            }
                        }
                    }
                    num++;
                    this.AgregaRenglon(str, (int)tamanoLetra, estiloFuente);
                }
            }
        }

        public void AgregarLinea(string cadenaTexto, HelperTicket.AlineacionLetra alineacion, HelperTicket.TamanoDeLetra tamanoLetra = TamanoDeLetra.Predeterminado, FontStyle estiloFuente = FontStyle.Regular)
        {
            this.Alineacion = alineacion;
            string[] strArrays = cadenaTexto.Split(new char[] { '\n' });
            for (int i = 0; i < strArrays.Length; i++)
            {
                string[] strArrays1 = this.CuentaCaracteres(strArrays[i], tamanoLetra);
                for (int j = 0; j < strArrays1.Length; j++)
                {
                    this.AgregaRenglon(strArrays1[j], (int)tamanoLetra, estiloFuente);
                }
            }
            GC.Collect();
        }

        public void AgregarLinea(string cadenaDeTexto, HelperTicket.TamanoDeLetra tamanoLetra = TamanoDeLetra.Predeterminado, FontStyle estiloTexto = FontStyle.Regular)
        {
            string[] strArrays = cadenaDeTexto.Split(new char[] { '\n' });
            for (int i = 0; i < strArrays.Length; i++)
            {
                string[] strArrays1 = this.CuentaCaracteres(strArrays[i], tamanoLetra);
                for (int j = 0; j < strArrays1.Length; j++)
                {
                    this.AgregaRenglon(strArrays1[j], (int)tamanoLetra, estiloTexto);
                }
            }
            GC.Collect();
        }

        public void VistaPreviaTicketTermico(string impresora)
        {
            this.fuenteImpresa = new Font(this.NombreLetra, (float)this.TamanoLetra, FontStyle.Regular);
            if (this.ImpresoraExistente(impresora))
            {
                this.DocumentoAImprimir.PrinterSettings.PrinterName = impresora;
                this.DocumentoAImprimir.PrintController = new StandardPrintController();
            }
        }

        public void ImprimeTicketTermico(string impresora)
        {
            this.fuenteImpresa = new Font(this.NombreLetra, (float)this.TamanoLetra, FontStyle.Regular);
            if (this.ImpresoraExistente(impresora))
            {
                this.DocumentoAImprimir.PrinterSettings.PrinterName = impresora;
                this.DocumentoAImprimir.PrintController = new StandardPrintController();
                this.DocumentoAImprimir.Print();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region metodos privados

        private void AgregaRenglon(string cadenaTexto1, int tamanoDeLetra, FontStyle estiloTexto)
        {
            DataRow cadenaTexto = this.dtbLineas.NewRow();
            cadenaTexto["Texto"] = cadenaTexto1;
            cadenaTexto["Tamano"] = tamanoDeLetra;
            cadenaTexto["Estilo"] = estiloTexto;
            this.dtbLineas.Rows.Add(cadenaTexto);
            cadenaTexto = null;
            GC.Collect();
        }

        private string Alineacion1(string cad, int nLetras)
        {
            int i;
            string str = "";
            string str1 = "";
            if (this.alineacionField == HelperTicket.AlineacionLetra.Derecha)
            {
                if (cad[cad.Length - 1] == ' ')
                {
                    cad.Remove(cad.Length - 1);
                }
                for (i = 0; i < nLetras - cad.Length; i++)
                {
                    str = string.Concat(str, " ");
                }
                str1 = string.Concat(str, cad);
            }
            else if (this.alineacionField != HelperTicket.AlineacionLetra.Centrada)
            {
                for (i = 0; i < nLetras - cad.Length; i++)
                {
                    str = string.Concat(str, " ");
                }
                str1 = string.Concat(cad, str);
            }
            else
            {
                for (i = 0; i < (nLetras - cad.Length) / 2; i++)
                {
                    str = string.Concat(str, " ");
                }
                str1 = string.Concat(str, cad);
            }
            return str1;
        }

        private string[] CuentaCaracteres(string var, HelperTicket.TamanoDeLetra tamanoLetra)
        {
            List<string> strs = new List<string>();
            string[] strArrays = var.Split(new char[] { ' ' });
            int num = 0;
            int letrasPorLinea = 11 * this.letrasPorLinea / (int)tamanoLetra;
            while (num < strArrays.Length)
            {
                string str = "";
                try
                {
                    if (strArrays[num].Length <= letrasPorLinea)
                    {
                        try
                        {
                            while (str.Length + strArrays[num].Length <= letrasPorLinea)
                            {
                                str = string.Concat(str, strArrays[num], " ");
                                num++;
                            }
                        }
                        catch
                        {
                        }
                        strs.Add(this.Alineacion1(str, letrasPorLinea));
                    }
                    else
                    {
                        int num1 = 0;
                        try
                        {
                            do
                            {
                                str = strArrays[num].Substring(num1, letrasPorLinea);
                                strs.Add(str);
                                num1 += letrasPorLinea;
                            }
                            while (num1 < strArrays[num].Length);
                        }
                        catch
                        {
                            str = strArrays[num].Substring(num1, strArrays[num].Length - num1);
                            strs.Add(this.Alineacion1(str, letrasPorLinea));
                        }
                        num++;
                    }
                }
                catch
                {
                }
            }
            return strs.ToArray();
        }

        private string[] CuentaCaracteresCol(string var, int noCaracteres1)
        {
            List<string> strs = new List<string>();
            string[] strArrays = var.Split(new char[] { ' ' });
            int num = 0;
            int noCaracteres = noCaracteres1;
            while (num <  strArrays.Length)
            {
                string str = "";
                try
                {
                    if (strArrays[num].Length <= noCaracteres)
                    {
                        try
                        {
                            while (str.Length + strArrays[num].Length <= noCaracteres)
                            {
                                str = string.Concat(str, strArrays[num], " ");
                                num++;
                            }
                        }
                        catch
                        {
                        }
                        strs.Add(this.Alineacion1(str, noCaracteres));
                    }
                    else
                    {
                        int num1 = 0;
                        try
                        {
                            do
                            {
                                str = strArrays[num].Substring(num1, noCaracteres);
                                strs.Add(str);
                                num1 += noCaracteres;
                            }
                            while (num1 < strArrays[num].Length);
                        }
                        catch
                        {
                            str = strArrays[num].Substring(num1, strArrays[num].Length - num1);
                            strs.Add(this.Alineacion1(str, noCaracteres));
                        }
                        num++;
                    }
                }
                catch
                {
                }
            }
            return strs.ToArray();
        }

        private bool DibujaEspacio(PrintPageEventArgs gfx)
        {
            bool flag;
            if (this.contador > 0)
            {
                if (!this.ImprimirLinea(" ", gfx, HelperTicket.TamanoDeLetra.Predeterminado, FontStyle.Regular))
                {
                    flag = false;
                    return flag;
                }
                this.contador++;
            }
            flag = true;
            return flag;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                }
            }
            this.disposedValue = true;
        }

        private void DocumentoAImprimir_BeginPrint(object sender, PrintEventArgs e)
        {
            this.DocumentoAImprimir.PrintController = new StandardPrintController();
            this.noPagina = 0;
            RectangleF printableArea = this.DocumentoAImprimir.DefaultPageSettings.PrintableArea;
            this.heightPage = (double)printableArea.Height * 25.4 / 100;
            printableArea = this.DocumentoAImprimir.DefaultPageSettings.PrintableArea;
            this.widthPage = (double)printableArea.Width * 25.4 / 100;
            if (this.widthPage < 50)
            {
                this.LetrasPorLinea = 25;
            }
        }

        private void DocumentoAImprimir_PrintPage(object sender, PrintPageEventArgs e)
        {
            double height;
            e.Graphics.PageUnit = GraphicsUnit.Display;
            this.noPagina++;
            if (this.Logotipo != null)
            {
                if (this.Logotipo.Width != 0 & this.noPagina == 1)
                {
                    try
                    {
                        try
                        {
                            e.Graphics.DrawImage(this.Logotipo, new Point(this.MargenIzqLogo, (int)this.Renglon(e)));
                            height = ((float)this.Logotipo.Height / this.Logotipo.HorizontalResolution * 100f);
                            this.imageHeight = (int)Math.Round(height) + 3;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    finally
                    {
                        this.Logotipo.Dispose();
                        this.Logotipo = null;
                    }
                }
            }
            if (this.noPagina > 1)
            {
                this.imageHeight = 0;
            }
            int num = 0;
            while (true)
            {
                if (num <= this.dtbLineas.Rows.Count - 1)
                {
                    if (this.heightPage == 285)
                    {
                        if (!this.ImprimirLinea(string.Concat(char.ConvertFromUtf32(10), char.ConvertFromUtf32(13)), e, (HelperTicket.TamanoDeLetra)Convert.ToInt32(this.dtbLineas.Rows[0].ItemArray[1].ToString()), (FontStyle)Convert.ToInt32(this.dtbLineas.Rows[0].ItemArray[2].ToString())))
                        {
                            this.contador++;
                            break;
                        }
                    }
                    if (this.ImprimirLinea(this.dtbLineas.Rows[0].ItemArray[0].ToString(), e, (HelperTicket.TamanoDeLetra)Convert.ToInt32(this.dtbLineas.Rows[0].ItemArray[1].ToString()), (FontStyle)Convert.ToInt32(this.dtbLineas.Rows[0].ItemArray[2].ToString())))
                    {
                        this.dtbLineas.Rows.RemoveAt(0);
                        this.contador++;
                    }
                    else
                    {
                        this.contador++;
                        break;
                    }
                }
                else
                {
                    if (this.qRCode != null)
                    {
                        bool flag = true;
                        if (this.QrCode.Width != 0)
                        {
                            try
                            {
                                try
                                {
                                    double num1 = this.margenIzquierdo;
                                    this.margenIzquierdo = (this.widthPage - (double)((float)this.QrCode.Width / this.QrCode.VerticalResolution) * 25.4) / 2;
                                    height = (double)((float)this.QrCode.Height / this.QrCode.HorizontalResolution) * 25.4;
                                    if (this.Renglon(e) + height <= this.heightPage)
                                    {
                                        e.Graphics.DrawImage(this.QrCode, new Point(this.MargenIzqQRCode, Convert.ToInt32(this.Renglon(e))));
                                        this.imageHeight += Convert.ToInt32(Math.Round(height) + 3);
                                        this.margenIzquierdo = num1;
                                    }
                                    else
                                    {
                                        e.HasMorePages = true;
                                        this.margenIzquierdo = num1;
                                        flag = false;
                                        this.contador = 0;
                                        break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            finally
                            {
                                if (flag)
                                {
                                    this.QrCode.Dispose();
                                    this.QrCode = null;
                                }
                            }
                        }
                    }
                    if (this.DibujaEspacio(e))
                    {
                        e.HasMorePages = false;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private bool ImpresoraExistente(string impresora)
        {
            bool flag;
            foreach (string installedPrinter in PrinterSettings.InstalledPrinters)
            {
                if (impresora == installedPrinter)
                {
                    flag = true;
                    return flag;
                }
            }
            flag = false;
            return flag;
        }

        private bool ImprimirLinea(string cadenaPorEscribir, PrintPageEventArgs gfx, HelperTicket.TamanoDeLetra tamanoDeLetra, FontStyle estiloTexto)
        {
            bool flag;
            double num = this.Renglon(gfx);
            this.contadorGlobal++;
            if (num <= this.heightPage + 800)
            {
                if (!(tamanoDeLetra != HelperTicket.TamanoDeLetra.Predeterminado | estiloTexto != FontStyle.Regular))
                {
                    gfx.Graphics.DrawString(cadenaPorEscribir, this.fuenteImpresa, this.colorDeLaFuente, (float)this.margenIzquierdo, (float)num + 1f, new StringFormat());
                }
                else
                {
                    Font font = new Font(this.NombreLetra, (float)tamanoDeLetra, estiloTexto);
                    gfx.Graphics.DrawString(cadenaPorEscribir, font, this.colorDeLaFuente, (float)this.margenIzquierdo, (float)num + 1f, new StringFormat());
                    font.Dispose();
                    GC.Collect();
                }
                flag = true;
            }
            else
            {
                gfx.HasMorePages = true;
                this.contador = 0;
                flag = false;
            }
            return flag;
        }

        private double Renglon(PrintPageEventArgs gfx)
        {
            double height = ((float)this.contador * this.fuenteImpresa.GetHeight(gfx.Graphics) + (float)this.imageHeight);
            if (this.noPagina == 1)
            {
                height += this.margenSuperiorField;
            }
            return height;
        }

        #endregion

        public enum AlineacionLetra
        {
            Izquierda,
            Centrada,
            Derecha
        }

        public enum TamanoDeLetra
        {
            SuperChica = 4,
            Diminuta = 5,
            Pequena = 6,
            Mediana = 7,
            Predeterminado = 8,
            Grande = 9,
            MuyGrande = 10,
            Gigantesco = 11
        }

    }
}