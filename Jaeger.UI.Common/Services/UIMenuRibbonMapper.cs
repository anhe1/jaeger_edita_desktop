﻿using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Telerik.WinControls;

namespace Jaeger.UI.Common.Services {
    /// <summary>
    /// mapar menu ribbon
    /// </summary>
    public class UIMenuRibbonMapper {
        private int _index = 1;

        /// <summary>
        /// constructor
        /// </summary>
        public UIMenuRibbonMapper() {

        }

        #region propidades
        public List<UIMenuElement> menus {
            get; set;
        }
        #endregion

        #region metodos publicos
        public void Start(RadRibbonBarCommandTabCollection tabCollection) {
            this.menus = new List<UIMenuElement>();

            foreach (RibbonTab item in tabCollection) {
                this.SetTab(item);
            }

        }

        public void Save() {
            foreach (var item in this.menus) {
                Console.WriteLine(String.Format("new UIMenuElement {{ Id = {0}, ParentId = {1}, Name = \"{2}\", Label = \"{3}\", Form = \"{4}\", TypeOf = \"{5}\", Rol = \"Administrador\" }},", 
                    item.Id, item.ParentId, item.Name, item.Label, item.Form, ""));
            }
        }
        #endregion

        #region metodos privados
        private void SetTab(RibbonTab tab) {
            // crear el elemento
            var _tab = new UIMenuElement { Id = this._index, ParentId = 0, Label = tab.Text, Name = tab.Name, Rol = "Administrador" };
            // agregar el nuevo elemento
            this.menus.Add(_tab);
            this._index++; // actualizar indice
            if (tab.Items.Count > 0) {
                // recorrer grupos tab
                for (int i = 0; i < tab.Items.Count; i++) {
                    this.SetBarGroup((RadRibbonBarGroup)tab.Items[i], _tab.Id);
                }
            }
        }

        private void SetBarGroup(RadRibbonBarGroup barGroup, int parentID) {
            // crear el grupo
            var _bargroup = this.SetMenuItem(barGroup, parentID);

            foreach (var item in barGroup.Items) {
                if (item is RadButtonElement) {
                    var dropDownElement = this.SetMenuItem(item, _bargroup.Id);
                } else if (item is RadRibbonBarButtonGroup) {
                    var dropDownElement = this.SetMenuItem(item, _bargroup.Id);
                    this.SetBarButtonGroup((RadRibbonBarButtonGroup)item, dropDownElement.Id);
                } else if (item is RadDropDownButtonElement) {
                    this.SetDropDownButtonElement((RadDropDownButtonElement)item, _bargroup.Id);
                }
            }
        }

        private void SetBarButtonGroup(RadRibbonBarButtonGroup buttonGroup, int parentID) {
            foreach (var item in buttonGroup.Items) {
                if (item is RadButtonElement) {
                    var dropDownElement = this.SetMenuItem(item, parentID);
                } else if (item is RadDropDownButtonElement) {
                    SetDropDownButtonElement((RadDropDownButtonElement)item, parentID);
                }
            }
        }

        private void SetDropDownButtonElement(RadDropDownButtonElement button, int parentID) {
            var dropDownElement = this.SetMenuItem(button, parentID); 
            foreach (var item in button.Items) {
                if (item is RadMenuItem) {
                    this.SetMenuItem(item, dropDownElement.Id);
                }
            }
        }

        private UIMenuElement SetMenuItem(RadItem radItem, int parentID) {
            var nuevo = new UIMenuElement { Id = this._index, ParentId = parentID, Label = radItem.Text, Name = radItem.Name, Form = (radItem.Tag != null ? radItem.Tag.ToString() : null), Rol = "administrador" };
            this.menus.Add(nuevo); 
            this._index++;
            return nuevo;
        }
        #endregion
    }
}