﻿using Telerik.WinControls.UI;
using System;

namespace Jaeger.UI.Common.Services {
    class CustomTreeViewElement : RadTreeViewElement {
        protected override Type ThemeEffectiveType {
            get {
                return typeof(RadTreeViewElement);
            }
        }

        protected override TreeViewDragDropService CreateDragDropService() {
            return new CustomDragDropService(this);
        }
    }

}
