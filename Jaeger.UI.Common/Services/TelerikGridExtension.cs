﻿/// Extensiones para la libreria telerik
/// develop:
/// purpose:
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Services {
    /// <summary>
    /// clase estatica con metodos de extension para funcionalidades del grid de Telerik
    /// </summary>
    public static class TelerikGridExtension {
        public static readonly string FolderTemporal = @"C:\Jaeger\Jaeger.Temporal\";

        /// <summary>
        /// implementacion de propiedades iniciales del grid de telerik
        /// </summary>
        /// <param name="gridView">RadGridView</param>
        public static void Standard(this RadGridView gridView) {
            try {
                gridView.AutoGenerateColumns = false;
                gridView.AllowAddNewRow = false;
                gridView.AllowEditRow = false;
                gridView.AllowDeleteRow = false;
                gridView.EnableFiltering = true;
                gridView.ShowGroupPanel = false;
                gridView.ShowFilteringRow = false;
                gridView.EnableAlternatingRowColor = true;
                gridView.AutoSizeRows = false;
                gridView.AllowRowResize = false;
            } catch (Exception ex) {
                Console.WriteLine($"Telerik-Extension {ex.Message}");
            }
        }

        /// <summary>
        /// implementacion de propiedades iniciales del grid de telerik
        /// </summary>
        /// <param name="gridView">GridViewTemplete</param>
        public static void Standard(this GridViewTemplate gridView) {
            gridView.AllowAddNewRow = false;
            gridView.AllowEditRow = false;
            gridView.AllowDeleteRow = false;
            gridView.EnableFiltering = true;
            gridView.ShowFilteringRow = false;
            gridView.EnableAlternatingRowColor = true;
            gridView.AllowRowResize = false;
        }

        /// <summary>
        /// accion de agrupacion de grid 
        /// </summary>
        /// <param name="gridView">RadGridView</param>
        /// <param name="state">Show Group Panel True</param>
        public static void Grouping(this RadGridView gridView, bool state) {
            gridView.ShowGroupPanel = state;
            if (gridView.ShowGroupPanel == false) {
                gridView.GroupDescriptors.Clear();
            } else {
                gridView.ShowGroupPanel = true;
            }
        }

        /// <summary>
        /// accion de agrupacion de grid 
        /// </summary>
        /// <param name="gridView">RadGridView</param>
        public static void Grouping(this RadGridView gridView) {
            if (gridView.GroupDescriptors.Count > 0) {
                gridView.GroupDescriptors.Clear();
                return;
            }
            gridView.EnableGrouping = true;
        }

        /// <summary>
        /// activar barra de filtros
        /// </summary>
        /// <param name="gridView">gridView</param>
        /// <param name="toggleState"></param>
        public static void ActivateFilter(this RadGridView gridView, ToggleState toggleState) {
            gridView.ShowFilteringRow = toggleState != ToggleState.On;
            if (gridView.ShowFilteringRow == false)
                gridView.FilterDescriptors.Clear();
        }

        /// <summary>
        /// activar barra de filtros
        /// </summary>
        /// <param name="gridView">gridView</param>
        /// <param name="showFilter"></param>
        public static bool AutoSum(this RadGridView gridView, bool showFilter = false) {
            if (showFilter) {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            } else {
                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
                foreach (var item in gridView.Columns) {
                    if (item.DataType == typeof(double)) {
                        GridViewSummaryItem summaryItem = new GridViewSummaryItem {
                            Name = item.Name,
                            FormatString = item.FormatString,
                            Aggregate = GridAggregateFunction.Sum
                        };
                        summaryRowItem.Add(summaryItem);
                    }
                }
                gridView.SummaryRowsTop.Add(summaryRowItem);
                gridView.SummaryRowsBottom.Add(summaryRowItem);
            }
            return true;
        }

        public static void AutoSum(this RadGridView gridView) {
            if (gridView.SummaryRowsTop.Count > 0 | gridView.SummaryRowsBottom.Count > 0) {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            } else {
                var summaryRowItem = new GridViewSummaryRowItem();
                foreach (GridViewDataColumn item in gridView.Columns) {
                    if (item.DataType == typeof(double) | item.DataType == typeof(decimal)) {
                        var summaryItem = new GridViewSummaryItem {
                            Name = item.Name,
                            FormatString = item.FormatString,
                            Aggregate = GridAggregateFunction.Sum
                        };
                        summaryRowItem.Add(summaryItem);
                    }
                }

                gridView.SummaryRowsTop.Add(summaryRowItem);
                gridView.SummaryRowsBottom.Add(summaryRowItem);
            }

            if (gridView.Templates.Count > 0) {
                foreach (var item in gridView.Templates) {
                    AutoSum(item);
                }
            }
        }

        public static void AutoSum(this GridViewTemplate gridView) {
            if (gridView.SummaryRowsTop.Count > 0 | gridView.SummaryRowsBottom.Count > 0) {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            } else {
                var summaryRowItem = new GridViewSummaryRowItem();
                foreach (GridViewDataColumn item in gridView.Columns) {
                    if (item.DataType == typeof(double) | item.DataType == typeof(decimal)) {
                        var summaryItem = new GridViewSummaryItem {
                            Name = item.Name,
                            FormatString = item.FormatString,
                            Aggregate = GridAggregateFunction.Sum
                        };
                        summaryRowItem.Add(summaryItem);
                    }
                }
                gridView.SummaryRowsTop.Add(summaryRowItem);
            }
        }

        public static void AutoSum(this GridViewTemplate gridView, bool show = false) {
            if (gridView.SummaryRowsTop.Count > 0 | gridView.SummaryRowsBottom.Count > 0) {
                gridView.SummaryRowsTop.Clear();
                gridView.SummaryRowsBottom.Clear();
            } else {
                var summaryRowItem = new GridViewSummaryRowItem();
                foreach (GridViewDataColumn item in gridView.Columns) {
                    if (item.DataType == typeof(double) | item.DataType == typeof(decimal)) {
                        var summaryItem = new GridViewSummaryItem {
                            Name = item.Name,
                            FormatString = item.FormatString,
                            Aggregate = GridAggregateFunction.Sum
                        };
                        summaryRowItem.Add(summaryItem);
                    }
                }
                gridView.SummaryRowsTop.Add(summaryRowItem);
            }
        }

        public static void Reset(this GridViewRowInfo gridViewRow) {
            var row = ((GridViewHierarchyRowInfo)gridViewRow);
            foreach (var item in row.Views) {
                item.Refresh();
            }
        }

        /// <summary>
        /// obtener tipo de columna
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="gridView">GridView</param>
        /// <param name="columnName">nombre de la columna</param>
        public static T1 GetColumn<T1>(this RadGridView gridView, string columnName) where T1 : class, new() {
            return gridView.Columns[columnName] as T1;
        }

        /// <summary>
        /// obtener tipo de columna
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="gridView">GridView</param>
        /// <param name="columnName">nombre de la columna</param>
        public static T1 GetColumn<T1>(this GridViewTemplate gridView, string columnName) where T1 : class, new() {
            try {
                var exists = gridView.Columns.Where(it => it.Name == columnName).FirstOrDefault() != null;
                if (exists)
                    return gridView.Columns[columnName] as T1;
            } catch (Exception ex) {
                Console.WriteLine($"La columna {columnName} no existe, error: {ex.Message}");
            }
            return null;
        }

        public static object ReturnRowSelected(this RadGridView gridView) {
            if (gridView.CurrentRow != null) {
                try {
                    return gridView.CurrentRow.DataBoundItem as object;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }

        public static T1 ReturnRowSelected<T1>(this RadGridView gridView) where T1 : class, new() {
            if (gridView.CurrentRow != null) {
                try {
                    return gridView.CurrentRow.DataBoundItem as T1;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }

        public static T1 ReturnRowSelected<T1>(this GridViewRowInfo currentRow) where T1 : class, new() {
            if (currentRow != null) {
                try {
                    return currentRow.DataBoundItem as T1;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// con este metodo refresacamos la vista de la fila seleccionada, principalmente utilizado las vistas de templetes
        /// </summary>
        /// <param name="row">GridViewHierarchyRowInfo</param>
        public static void RowRefresh(this GridViewHierarchyRowInfo row) {
            foreach (var item in row.Views) { item.Refresh(); }
        }

        /// <summary>
        /// con este metodo refresacamos la vista de la fila seleccionada, principalmente utilizado las vistas de templetes
        /// </summary>
        /// <param name="row">GridViewHierarchyRowInfo</param>
        public static void RowRefresh(this GridViewRowInfo row) {
            foreach (var item in (row as GridViewHierarchyRowInfo).Views) { item.Refresh(); row.IsExpanded = false; }
        }

        /// <summary>
        /// copiar el contenido de un grid al porta papeles
        /// </summary>
        /// <param name="radGridView"></param>
        /// <returns>string</returns>
        public static string CopyRows(this RadGridView radGridView) {
            StringBuilder sb = new StringBuilder();
            foreach (GridViewRowInfo row in radGridView.Rows) {
                int i = 0;
                while (i < row.Cells.Count) {
                    if (i > 0) {
                        sb.Append(",");
                    }
                    sb.Append(row.Cells[i].Value.ToString());
                    i++;
                }
                sb.Append("\r\n");
            }
            return sb.ToString();
        }

        /// <summary>
        /// copiar el contenido de un grid al porta papeles, solo las filas seleccionadas
        /// </summary>
        /// <param name="selectedRows">Collections</param>
        /// <returns>string</returns>
        public static string CopyRows(this GridViewSelectedRowsCollection selectedRows) {
            StringBuilder sb = new StringBuilder();
            foreach (GridViewRowInfo row in selectedRows) {
                int i = 0;
                while (i < row.Cells.Count) {
                    if (i > 0) {
                        sb.Append(",");
                    }
                    sb.Append(row.Cells[i].Value.ToString());
                    i++;
                }
                sb.Append("\r\n");
            }
            return sb.ToString();
        }

        public static bool ContextMenuOpening(this RadGridView gridView, ContextMenuOpeningEventArgs e, RadContextMenu contextMenu) {
            if (e.ContextMenuProvider is GridHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridRowHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridFilterCellElement) {
            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (gridView.CurrentRow.ViewInfo.ViewTemplate == gridView.MasterTemplate) {
                    e.ContextMenu = contextMenu.DropDown;
                }
            }
            return false;
        }

        #region layout
        public static bool Removable(this RadGridView gridView, string identificador = "") {
            var origen = TelerikGridExtension.FolderTemporal + "gridv_" + CreateGuid(new string[] { gridView.Name, gridView.Columns.Count.ToString(), identificador.ToLower().Trim() }).ToLower() + ".xml";
            if (!File.Exists(origen)) {
                return false;
            } else {
                try {
                    File.Delete(origen);
                    return true;
                } catch (Exception e) {
                    Console.WriteLine("GridTelerikExtension_Error: " + e.Message);
                    return false;
                }
            }
        }
        /// <summary>
        /// guardar vista del grid
        /// </summary>
        /// <param name="gridView">grid relacionado</param>
        /// <param name="identificador">identifiador unico</param>
        public static bool Loadvable(this RadGridView gridView, string identificador = "") {
            var origen = TelerikGridExtension.FolderTemporal + "gridv_" + CreateGuid(new string[] { gridView.Name, gridView.Columns.Count.ToString(), identificador.ToLower().Trim() }).ToLower() + ".xml";
            if (!File.Exists(origen)) {
                return false;
            } else {
                try {
                    gridView.LoadLayout(origen);
                    return true;
                } catch (Exception e) {
                    Console.WriteLine("GridTelerikExtension_Error: " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// cargar vista del grid
        /// </summary>
        /// <param name="gridView">grid relacionado</param>
        /// <param name="identificador">identifiador unico</param>
        public static bool Salveable(this RadGridView gridView, string identificador = "") {
            try {
                var fileLayOut = "gridv_" + CreateGuid(new string[] { gridView.Name, gridView.Columns.Count.ToString(), identificador.ToLower().Trim() }).ToLower() + ".xml";
                var destino = TelerikGridExtension.FolderTemporal + fileLayOut;
                gridView.SaveLayout(destino);
                return true;
            } catch (Exception e) {
                Console.WriteLine("GridTelerikExtension_Error: " + e.Message);
                return false;
            }
        }
        #endregion

        #region formatos condicionales
        /// <summary>
        /// condicional para la columna de registro activo, se aplica a toda la fila
        /// </summary>
        /// <param name="nameConditional">nombre de la condicional</param>
        /// <returns>ConditionalFormattingObject</returns>
        public static ConditionalFormattingObject RowActive(string nameConditional = "Registro Activo") {
            var registroActivo = new ConditionalFormattingObject() {
                Name = nameConditional,
                ApplyToRow = true,
                RowBackColor = System.Drawing.Color.Empty,
                RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = System.Drawing.Color.Gray,
                TValue1 = "false",
                TValue2 = "0"
            };
            return registroActivo;
        }
        #endregion

        public static string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }
    }
}
