﻿///develop: 220720171403
/// pupose: libreria para exportacion a formato excel o csv
/// 230720170056: agregamos funcion para exportar un objeto RadPivotGrid.
using System;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls;
using Telerik.WinControls.Export;

namespace Jaeger.UI.Common.Services {
    public static class HelperTelerikExport {
        /// <summary>
        /// metodo para exportar a formato csv
        /// </summary>
        public static bool RunExportToCsv(ref RadGridView oGridView, string sFileName, SummariesOption oSummariesOption = SummariesOption.ExportAll) {
            ExportToCSV exportToCsv = new ExportToCSV(oGridView) {
                SummariesExportOption = oSummariesOption
            };
            try {
                exportToCsv.RunExport(sFileName);
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                //Jaeger.Helpers.HelperLogError.LogWrite(new Jaeger.Entities.Property { Type = EnumPropertyType.Error, Name = "HelperTelerikExport:RunExportToCsv", Value = e.Message });
                return false;
            }
        }

        /// <summary>
        /// Exportación a formato Excel
        /// </summary>
        /// <param name="oGridView">Objeto RadGridView referenciado</param>
        /// <param name="sFileName">Nombre de archivo de salida</param>
        /// <param name="sSheet">Nombre de hoja</param>
        /// <param name="oSummariesOption">Opciones de sumas</param>
        /// <param name="oExcelMaxRows">Máximo de filas de excel</param>
        /// <param name="bExportVisualSettings">Exportar formatos</param>
        /// <returns>Regresa verdadero cuando se exporta correctamente</returns>
        /// <remarks></remarks>
        public static bool RunExportToExcel(ref RadGridView oGridView, string sFileName, string sSheet = "", SummariesOption oSummariesOption = SummariesOption.ExportAll,
            ExcelMaxRows oExcelMaxRows = ExcelMaxRows._1048576, bool bExportVisualSettings = false, bool bHiddenColumns = false, bool bExportHierarchy = false) {
            bool runExportToExcel;
            ExportToExcelML exportToExcelML = new ExportToExcelML(oGridView);
            if (sSheet != string.Empty) {
                exportToExcelML.SheetName = sSheet;
            }
            exportToExcelML.SummariesExportOption = oSummariesOption;
            exportToExcelML.SheetMaxRows = oExcelMaxRows;
            exportToExcelML.ExportVisualSettings = bExportVisualSettings;
            exportToExcelML.ExportHierarchy = bExportHierarchy;
            exportToExcelML.HiddenColumnOption = HiddenOption.DoNotExport;// (bHiddenColumns ? HiddenOption.ExportAsHidden : HiddenOption.DoNotExport);
            exportToExcelML.ChildViewExportMode = ChildViewExportMode.SelectViewToExport;
            //exportToExcelML.ExportHierarchy = true;

            try {
                exportToExcelML.RunExport(sFileName);
                runExportToExcel = true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                //Jaeger.Helpers.HelperLogError.LogWrite(new Jaeger.Entities.Property { Type = EnumPropertyType.Error, Name = "HelperTelerikExport:RunExportToExcel", Value = e.Message });
                runExportToExcel = false;
            }
            exportToExcelML = null;
            return runExportToExcel;
        }

        /// <summary>
        /// exportar infomración de un objeto RadPivotGrid
        /// </summary>
        /// <param name="objPivotGrid"></param>
        /// <param name="sFileName">nombre del archivo</param>
        /// <param name="sSheet">nombre de la hoja</param>
        /// <param name="formato">formato de exportación (Xlsx)</param>
        /// <returns></returns>
        public static bool RunExportPivotGrid(RadPivotGrid objPivotGrid, string sFileName, string sSheet = "") {
            PivotGridSpreadExport spreadExporter;
            spreadExporter = new PivotGridSpreadExport(objPivotGrid) {
                ExportFormat = SpreadExportFormat.Xlsx,
                ExportVisualSettings = false,
                ExportFlatData = true
            };

            try {
                spreadExporter.RunExport(sFileName, new SpreadExportRenderer());
                return true;
            }
            catch (Exception e) {
                RadMessageBox.Show("Error al exportar!", "Error", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error, e.Message);
                //Jaeger.Helpers.HelperLogError.LogWrite(new Jaeger.Entities.Property { Type = EnumPropertyType.Error, Name = "HelperTelerikExport:RunExportPivotGrid", Value = e.Message });
                return false;
            }
        }
    }
}