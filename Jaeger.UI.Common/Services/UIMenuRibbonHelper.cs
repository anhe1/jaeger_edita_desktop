﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Common.Services {
    public static class UIMenuRibbonHelper {
        public static Assembly GetAssembly(string assembly) {
            try {
                return Assembly.Load(assembly);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}