﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class Waiting3Form : Form {
        public Action Worker { get; set; }

        public Waiting3Form(Action worker) {
            InitializeComponent();
            if (worker == null)
                throw new ArgumentOutOfRangeException();
            this.Worker = worker;
        }

        private void Waiting2Form_Load(object sender, EventArgs e) {
            this.labelTitulo.Text = this.Text;
            this.WaitingBar.StartWaiting();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            Task.Factory.StartNew(this.Worker).ContinueWith(it => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
