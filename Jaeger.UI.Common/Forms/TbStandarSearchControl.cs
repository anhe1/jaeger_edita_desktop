﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class TbStandarSearchControl : UserControl {
        public TbStandarSearchControl() {
            InitializeComponent();
        }

        private void TbStandarSearchControl_Load(object sender, EventArgs e) {
            this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;

            this.Existencia.HostedItem = this.cExistencia.ButtonElement;
        }

        [Description("Texto que se muestra en el cuadro de busqueda"), Category("Botones")]
        public string Etiqueta {
            get {
                return this.lblCaption.Text;
            }
            set {
                this.lblCaption.Text = value;
                if (this.lblCaption.Text.Length > 0) {
                    this.lblCaption.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.lblCaption.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Mostrar botón agregar"), Category("Botones")]
        public bool ShowBuscar {
            get {
                return this.Buscar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Buscar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Buscar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar botón agregar"), Category("Botones")]
        public bool ShowAgregar {
            get {
                return this.Agregar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Agregar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Agregar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar botón agregar"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowExistencia {
            get {
                return this.Existencia.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Existencia.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Existencia.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowTextBox {
            get {
                return this.Descripcion.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Descripcion.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Descripcion.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }
    }
}
