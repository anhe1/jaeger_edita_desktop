﻿using System;

namespace Jaeger.UI.Common.Forms {
    public partial class ViewerPdfForm : Telerik.WinControls.UI.RadForm {
        protected internal string nombreDeArchivo;

        public ViewerPdfForm() {
            InitializeComponent();
        }

        public ViewerPdfForm(string archivo) {
            InitializeComponent();
            this.nombreDeArchivo = archivo;
        }

        private void ViewerPdf_Load(object sender, EventArgs e) {
            this.PdfViewer.LoadDocument(this.nombreDeArchivo);
        }

        private void ViewerPdf_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e) {
            this.PdfViewer.UnloadDocument();
            this.Dispose();
        }
    }
}
