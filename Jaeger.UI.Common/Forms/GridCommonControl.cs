﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class GridCommonControl : UserControl {
        #region declaraciones
        public RadContextMenu menuContextual = new RadContextMenu();
        protected internal RadMenuItem GridLayout = new RadMenuItem { Text = "Grid layout" };
        protected internal RadMenuItem LayoutSave = new RadMenuItem { Text = "Guardar" };
        protected internal RadMenuItem LayoutLoad = new RadMenuItem { Text = "Cargar" };
        protected internal RadMenuItem LayoutReset = new RadMenuItem { Text = "Reset" };

        public RadMenuItem ContextAcciones = new RadMenuItem { Text = "Acciones", Visibility = ElementVisibility.Collapsed };
        public RadMenuItem ContextCargar = new RadMenuItem { Text = "Cargar" };
        public RadMenuItem ContextDescargar = new RadMenuItem { Text = "Descargar" };
        public RadMenuItem ContextCopiar = new RadMenuItem { Text = "Copiar" };
        public RadMenuItem MultiSeleccion = new RadMenuItem { Text = "Selección múltiple" };
        public RadMenuItem ContextEditar = new RadMenuItem { Text = "Editar" };
        public RadMenuItem ContextCancelar = new RadMenuItem { Text = "Cancelar", Visibility = ElementVisibility.Collapsed };
        public RadMenuItem ContextImprimir = new RadMenuItem { Text = "Imprimir" };
        public RadMenuItem ContextImprimirC = new RadMenuItem { Text = "Imprimir con valores", Visibility = ElementVisibility.Collapsed };
        public RadMenuItem ContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        public RadMenuItem ContextAgrupar = new RadMenuItem { Text = "Agrupar", Visibility = ElementVisibility.Collapsed, CheckOnClick = true, ToolTipText = "Opción para agrupación por columnas." };

        protected internal UIAction _permisos = new UIAction();
        #endregion

        public GridCommonControl() {
            InitializeComponent();
            this.menuContextual.Items.AddRange(this.ContextCopiar, this.MultiSeleccion, ContextEditar, ContextCancelar, ContextDuplicar, ContextImprimir, ContextImprimirC, this.ContextAcciones);
            this.ContextAcciones.Items.AddRange(this.ContextCargar, this.ContextDescargar);
        }

        private void GridCommonControl_Load(object sender, EventArgs e) {
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
            this.HostEjercicio.HostedItem = this.Ejercicio.SpinElement;

            this.Ejercicio.Minimum = 2013;
            this.Ejercicio.Maximum = DateTime.Now.Year;
            this.Ejercicio.Value = DateTime.Now.Year;

            this.Periodo.DisplayMember = "Descripcion";
            this.Periodo.ValueMember = "Id";
            this.Periodo.DataSource = ConfigService.GetMeses();
            this.Periodo.SelectedIndex = DateTime.Now.Month;

            this.GridData.Standard();

            this.GridLayout.Items.Add(this.LayoutSave);
            this.GridLayout.Items.Add(this.LayoutLoad);
            this.GridLayout.Items.Add(this.LayoutReset);
            this.ContextAgrupar.Image = Properties.Resources.group_objects_16px;
            this.Herramientas.Items.Add(this.ContextAgrupar);
            this.Herramientas.Items.Add(this.GridLayout);

            this.ContextCopiar.Click += MContextual_Copiar_Click;
            this.MultiSeleccion.Click += this.MContextual_Seleccion_Click;
            this.ContextAgrupar.Click += this.Agrupar_Click;
            this.ExportarExcel.Click += this.ExportarExcel_Click;
            this.AutoSuma.Click += this.AutoSuma_Click;
            this.Filtro.Click += this.Filtro_Click;

            this.LayoutSave.Click += this.LayoutSave_Click;
            this.LayoutLoad.Click += this.LayoutLoad_Click;
            this.LayoutReset.Click += this.LayoutReset_Click;

            this.ToolBar.KeyDown += this.GridData_KeyDown;
        }

        #region barra de herramientas
        public virtual void ExportarExcel_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExportForm(this.GridData);
            exportar.ShowDialog(this);
        }

        public virtual void Filtro_Click(object sender, EventArgs e) {
            this.GridData.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        public virtual void AutoSuma_Click(object sender, EventArgs e) {
            this.GridData.AutoSum();
        }

        public virtual void Agrupar_Click(object sender, EventArgs eventArgs) {
            this.GridData.Grouping(this.ContextAgrupar.IsChecked);
        }

        public virtual void LayoutReset_Click(object sender, EventArgs e) {
            if (this.GridData.Removable(ConfigService.Piloto.Clave)) {
                RadMessageBox.Show(this, "Es necesario volver a abrir la vista del formulario", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        public virtual void LayoutSave_Click(object sender, EventArgs e) {
            this.GridData.Salveable(ConfigService.Piloto.Clave);
        }

        private void LayoutLoad_Click(object sender, EventArgs e) {
            this.GridData.Loadvable(ConfigService.Piloto.Clave);
        }
        #endregion

        #region acciones del grid
        protected virtual void GridData_CellEditorInitialized(object sender, GridViewCellEventArgs e) { }

        protected virtual void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) { }

        protected virtual void GridData_EditorRequired(object sender, EditorRequiredEventArgs e) { }

        protected virtual void GridData_CellValidating(object sender, CellValidatingEventArgs e) { }

        protected virtual void GridData_CellValidated(object sender, CellValidatedEventArgs e) { }

        protected virtual void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) { }

        protected virtual void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.Editar.Enabled = this.GridData.RowCount > 0 && this._permisos.Editar;
        }

        protected virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridRowHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridFilterCellElement) {
            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate) {
                    e.ContextMenu = this.menuContextual.DropDown;
                }
            }
        }

        protected virtual void GridData_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F2) {
                if (this.Editar.Enabled) {
                    this.Editar.PerformClick();
                }
            } else if (e.KeyCode == Keys.F3) {
                if (this.Nuevo.Enabled) {
                    this.Nuevo.PerformClick();
                }
            } else if (e.KeyCode == Keys.F4) {
                if (this.Cancelar.Enabled) {
                    this.Cancelar.PerformClick();
                }
            } else if (e.KeyCode == Keys.F5) {
                this.Actualizar.PerformClick();
            } else if (e.KeyCode == Keys.F9) {
                if (this.AutoSuma.Visibility == ElementVisibility.Visible) {
                    this.AutoSuma.PerformClick();
                }
            } else if (e.KeyCode == Keys.F11) {
                this.Filtro.PerformClick();
            }
        }

        protected virtual void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (string.IsNullOrEmpty(this.PDF))
                return;
            if (e.Column.Name.ToLower() == this.PDF.ToLower() | e.Column.Name.ToLower() == "") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images["pdf"];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    if (e.CellElement.Children.Count() > 0)
                        e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            } else {
                if (e.Row is GridViewRowInfo) {
                    e.CellElement.ToolTipText = e.CellElement.Text;
                }
            }
        }

        protected virtual void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) { }

        protected virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) { }

        protected virtual void GridData_CommandCellClick(object sender, GridViewCellEventArgs e) { }
        #endregion

        #region menu contextual
        public virtual void MContextual_Copiar_Click(object sender, EventArgs e) {
            System.Windows.Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        public virtual void MContextual_Seleccion_Click(object sender, EventArgs e) {
            this.MultiSeleccion.IsChecked = !this.MultiSeleccion.IsChecked;
            this.GridData.MultiSelect = this.MultiSeleccion.IsChecked;
        }
        #endregion

        #region metodos publicos
        /// <summary>
        /// obtener lista del filtro
        /// </summary>
        /// <typeparam name="T1">Modelo</typeparam>
        public virtual List<T1> GetFiltro<T1>() where T1 : class, new() {
            var d1 = new List<T1>();
            var s2 = this.GridData.ChildRows.Select(it => it.DataBoundItem as T1);
            return s2.ToList();
        }

        /// <summary>
        /// obtener lista de la seleccion actual
        /// </summary>
        /// <typeparam name="T1">Modelo</typeparam>
        public virtual List<T1> GetSeleccion<T1>() where T1 : class, new() {
            var d1 = new List<T1>();
            var s2 = this.GridData.ChildRows.Where(it => it.IsSelected).Select(it => it.DataBoundItem as T1);
            return s2.ToList();
        }

        /// <summary>
        /// obtener el objeto seleccionado
        /// </summary>
        /// <typeparam name="T1">Modelo</typeparam>
        public virtual T1 GetCurrent<T1>() where T1 : class, new() {
            if (this.GridData.CurrentRow != null) {
                var selected = this.GridData.CurrentRow.DataBoundItem as T1;
                return selected;
            }
            return null;
        }

        public int GetPeriodo() {
            if (this.Periodo.SelectedValue != null) {
                return (int)this.Periodo.SelectedValue;
            } else {
                return (int)Enum.Parse(typeof(MesesEnum), this.Periodo.Text);
            }
        }

        public int GetEjercicio() {
            return int.Parse(this.Ejercicio.Value.ToString());
        }

        public void SetEjercicio(int ejercicio) {
            this.Ejercicio.Value = ejercicio;
        }
        #endregion

        /// <summary>
        /// aplicar permisos
        /// </summary>
        protected internal void Aplicar() {
            this.Nuevo.Enabled = this._permisos.Agregar;
            this.Editar.Enabled = this._permisos.Editar;
            this.Cancelar.Enabled = this._permisos.Cancelar | this._permisos.Remover;
            this.ExportarExcel.Enabled = this._permisos.Exportar;

            this.ContextCargar.Enabled = this._permisos.Agregar;
            this.ContextDuplicar.Enabled = this._permisos.Agregar;
            this.ContextEditar.Enabled = this._permisos.Editar;
            this.ContextCancelar.Enabled = this._permisos.Cancelar | this._permisos.Remover;

            // si existen permisos para exportar entonces mostras si o si el las opciones de herramientas
            if (this._permisos.Exportar) {
                this.ShowHerramientas = true;
                this.ExportarExcel.Visibility = ElementVisibility.Visible;
            }

            if (_permisos.Cancelar | _permisos.Remover) {
                ContextCancelar.Visibility = ElementVisibility.Visible;
                this.Cancelar.Visibility = ElementVisibility.Visible;
            }
        }

        #region propiedades
        [Description("Hostitem"), Category("Botones")]
        public bool ShowItem {
            get {
                return this.ItemLbl.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.ItemLbl.Visibility = ElementVisibility.Visible;
                    this.ItemHost.Visibility = ElementVisibility.Visible;
                } else {
                    this.ItemLbl.Visibility = ElementVisibility.Collapsed;
                    this.ItemHost.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Periodo"), Category("Botones")]
        public bool ShowPeriodo {
            get {
                return this.lblPeriodo.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.lblPeriodo.Visibility = ElementVisibility.Visible;
                    this.Periodo.Visibility = ElementVisibility.Visible;
                } else {
                    this.lblPeriodo.Visibility = ElementVisibility.Collapsed;
                    this.Periodo.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Ejercicio"), Category("Botones")]
        public bool ShowEjercicio {
            get {
                return this.lblEjercicio.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.lblEjercicio.Visibility = ElementVisibility.Visible;
                    this.HostEjercicio.Visibility = ElementVisibility.Visible;
                } else {
                    this.lblEjercicio.Visibility = ElementVisibility.Collapsed;
                    this.HostEjercicio.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Nuevo"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Nuevo.Visibility = ElementVisibility.Visible;
                    this.ContextDuplicar.Visibility = ElementVisibility.Visible;
                } else {
                    this.Nuevo.Visibility = ElementVisibility.Collapsed;
                    this.ContextDuplicar.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Editar"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Editar.Visibility = ElementVisibility.Visible;
                    this.ContextEditar.Visibility = ElementVisibility.Visible;
                } else {
                    this.Editar.Visibility = ElementVisibility.Collapsed;
                    this.ContextEditar.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Cancelar"), Category("Botones")]
        public bool ShowCancelar {
            get {
                return this.Cancelar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Cancelar.Visibility = ElementVisibility.Visible;
                    this.ContextCancelar.Visibility = ElementVisibility.Visible;
                } else {
                    this.Cancelar.Visibility = ElementVisibility.Collapsed;
                    this.ContextCancelar.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Actualizar"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Filtro"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("AutoSuma"), Category("Botones")]
        public bool ShowAutosuma {
            get {
                return this.AutoSuma.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.AutoSuma.Visibility = ElementVisibility.Visible;
                else
                    this.AutoSuma.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Imprimir"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Imprimir.Visibility = ElementVisibility.Visible;
                    this.ContextImprimir.Visibility = ElementVisibility.Visible;
                } else {
                    this.Imprimir.Visibility = ElementVisibility.Collapsed;
                    this.ContextImprimir.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Herramientas"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Exportar en formato Excel"), Category("Botones")]
        public bool ShowExportarExcel {
            get {
                return this.ExportarExcel.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.ExportarExcel.Visibility = ElementVisibility.Visible;
                else
                    this.ExportarExcel.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar opciones para agrupación de columnas"), Category("Botones")]
        public bool ShowAgrupar {
            get { return this.ContextAgrupar.Visibility == ElementVisibility.Visible; }
            set {
                if (value == true) {
                    this.ContextAgrupar.Visibility = ElementVisibility.Visible;
                } else {
                    this.ContextAgrupar.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Cerrar"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Selección múltiple"), Category("Botones")]
        public bool ShowSeleccionMultiple {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                    this.MultiSeleccion.Visibility = ElementVisibility.Visible;
                } else {
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
                    this.MultiSeleccion.Visibility = ElementVisibility.Visible;
                }
            }
        }

        [Description("Nombre de la columna que contiene PDF"), Category("Botones")]
        public string PDF { get; set; }

        public UIAction Permisos {
            get { return _permisos; }
            set {
                if (value != null)
                    this._permisos = value;
                this.Aplicar();
            }
        }
        #endregion

        private void ToolBar_ItemsChanged(RadCommandBarBaseItemCollection changed, RadCommandBarBaseItem target, ItemsChangeOperation operation) {
            var si = this.Nuevo.Visibility == ElementVisibility.Visible && this.Editar.Visibility == ElementVisibility.Visible && this.Cancelar.Visibility == ElementVisibility.Visible;
            commandBarSeparator2.Visibility = (si ? ElementVisibility.Visible : ElementVisibility.Collapsed);
        }
    }
}
