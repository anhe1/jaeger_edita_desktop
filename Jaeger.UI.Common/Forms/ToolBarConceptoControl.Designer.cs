﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBarConceptoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar4 = new Telerik.WinControls.UI.RadCommandBar();
            this.CboConcepto = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CommandBarRowElement4 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.Opciones = new Telerik.WinControls.UI.CommandBarStripElement();
            this.LabelCaption = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.LabelProducto = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostConceptos = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.Productos = new Telerik.WinControls.UI.CommandBarButton();
            this.Unidades = new Telerik.WinControls.UI.CommandBarButton();
            this.Complementos = new Telerik.WinControls.UI.CommandBarDropDownButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar4)).BeginInit();
            this.RadCommandBar4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConcepto.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConcepto.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar4
            // 
            this.RadCommandBar4.Controls.Add(this.CboConcepto);
            this.RadCommandBar4.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar4.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar4.Name = "RadCommandBar4";
            this.RadCommandBar4.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement4});
            this.RadCommandBar4.Size = new System.Drawing.Size(994, 55);
            this.RadCommandBar4.TabIndex = 194;
            // 
            // CboConcepto
            // 
            // 
            // CboConcepto.NestedRadGridView
            // 
            this.CboConcepto.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboConcepto.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboConcepto.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboConcepto.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboConcepto.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboConcepto.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboConcepto.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn1.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn1.Name = "ClaveUnidad";
            gridViewTextBoxColumn2.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn2.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn2.Name = "ClaveProdServ";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 85;
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn3.Width = 220;
            this.CboConcepto.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.CboConcepto.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboConcepto.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboConcepto.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboConcepto.EditorControl.Name = "NestedRadGridView";
            this.CboConcepto.EditorControl.ReadOnly = true;
            this.CboConcepto.EditorControl.ShowGroupPanel = false;
            this.CboConcepto.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboConcepto.EditorControl.TabIndex = 0;
            this.CboConcepto.Location = new System.Drawing.Point(144, 4);
            this.CboConcepto.Name = "CboConcepto";
            this.CboConcepto.NullText = "Selecciona";
            this.CboConcepto.Size = new System.Drawing.Size(343, 20);
            this.CboConcepto.TabIndex = 195;
            this.CboConcepto.TabStop = false;
            this.CboConcepto.VisibleChanged += new System.EventHandler(this.CboConcepto_VisibleChanged);
            // 
            // CommandBarRowElement4
            // 
            this.CommandBarRowElement4.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement4.Name = "CommandBarRowElement4";
            this.CommandBarRowElement4.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.Opciones});
            this.CommandBarRowElement4.Text = "";
            // 
            // Opciones
            // 
            this.Opciones.DisplayName = "Barra de Conceptos";
            this.Opciones.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.LabelCaption,
            this.commandBarSeparator1,
            this.LabelProducto,
            this.HostConceptos,
            this.Agregar,
            this.Nuevo,
            this.Duplicar,
            this.Remover,
            this.Productos,
            this.Unidades,
            this.Complementos});
            this.Opciones.Name = "Opciones";
            // 
            // 
            // 
            this.Opciones.OverflowButton.Enabled = false;
            this.Opciones.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.Opciones.GetChildAt(2))).Enabled = false;
            // 
            // LabelCaption
            // 
            this.LabelCaption.DisplayName = "Caption";
            this.LabelCaption.Name = "LabelCaption";
            this.LabelCaption.Text = "Conceptos";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // LabelProducto
            // 
            this.LabelProducto.DisplayName = "CommandBarLabel2";
            this.LabelProducto.Name = "LabelProducto";
            this.LabelProducto.Text = "Prod. Serv.";
            // 
            // HostConceptos
            // 
            this.HostConceptos.DisplayName = "commandBarHostItem1";
            this.HostConceptos.MinSize = new System.Drawing.Size(360, 0);
            this.HostConceptos.Name = "HostConceptos";
            this.HostConceptos.Text = "";
            // 
            // Agregar
            // 
            this.Agregar.DisplayName = "Agregar";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Common.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Common.Properties.Resources.copy_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Remover
            // 
            this.Remover.DisplayName = "Quitar";
            this.Remover.DrawText = true;
            this.Remover.Image = global::Jaeger.UI.Common.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Quitar";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Productos
            // 
            this.Productos.DisplayName = "Buscar";
            this.Productos.DrawText = true;
            this.Productos.Image = global::Jaeger.UI.Common.Properties.Resources.product_16px;
            this.Productos.Name = "Productos";
            this.Productos.Text = "Productos";
            this.Productos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Unidades
            // 
            this.Unidades.DisplayName = "Unidad";
            this.Unidades.DrawText = true;
            this.Unidades.Image = global::Jaeger.UI.Common.Properties.Resources.cube_16px;
            this.Unidades.Name = "Unidades";
            this.Unidades.Text = "Unidad";
            this.Unidades.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Complementos
            // 
            this.Complementos.DisplayName = "commandBarDropDownButton1";
            this.Complementos.DrawText = true;
            this.Complementos.Image = global::Jaeger.UI.Common.Properties.Resources.plugin_16px;
            this.Complementos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Complementos.Name = "Complementos";
            this.Complementos.Text = "Complementos";
            this.Complementos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarConceptoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RadCommandBar4);
            this.Name = "ToolBarConceptoControl";
            this.Size = new System.Drawing.Size(994, 30);
            this.Load += new System.EventHandler(this.ToolBarConceptoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar4)).EndInit();
            this.RadCommandBar4.ResumeLayout(false);
            this.RadCommandBar4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboConcepto.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConcepto.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboConcepto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar4;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement4;
        internal Telerik.WinControls.UI.CommandBarStripElement Opciones;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarHostItem HostConceptos;
        public Telerik.WinControls.UI.CommandBarButton Agregar;
        public Telerik.WinControls.UI.CommandBarButton Nuevo;
        public Telerik.WinControls.UI.CommandBarButton Duplicar;
        public Telerik.WinControls.UI.CommandBarButton Remover;
        public Telerik.WinControls.UI.CommandBarButton Unidades;
        public Telerik.WinControls.UI.CommandBarButton Productos;
        public Telerik.WinControls.UI.RadMultiColumnComboBox CboConcepto;
        public Telerik.WinControls.UI.CommandBarDropDownButton Complementos;
        public Telerik.WinControls.UI.CommandBarLabel LabelCaption;
        public Telerik.WinControls.UI.CommandBarLabel LabelProducto;
    }
}
