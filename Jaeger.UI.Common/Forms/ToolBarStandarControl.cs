﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {

    public partial class ToolBarStandarControl : UserControl {
        private bool bShowFilter;
        private bool _ReadOnly = false;
        
        public ToolBarStandarControl() {
            InitializeComponent();
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set {
                this._ReadOnly = value;
                this.CrearReadOnly();
            }
        }

        private void ToolBarStandarControl_Load(object sender, EventArgs e) {
            this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
            BarSeparator2.Visibility = ((this.ShowNuevo && this.ShowEditar && this.ShowRemover && this.ShowAutorizar && this.ShowImagen && this.ShowGuardar) == true ? Telerik.WinControls.ElementVisibility.Visible : Telerik.WinControls.ElementVisibility.Collapsed);
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public string Etiqueta {
            get {
                return this.Titulo.Text;
            }
            set {
                this.Titulo.Text = value;
                if (this.Titulo.Text.Length > 0) {
                    this.Titulo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.BarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.Titulo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.BarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Editar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Editar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowRemover {
            get {
                return this.Remover.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Remover.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Remover.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowGuardar {
            get {
                return this.Guardar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowFiltro {
            get {
                //return this.Filtro.Visibility == Telerik.WinControls.ElementVisibility.Visible;
                return this.bShowFilter;
            }
            set {
                this.bShowFilter = value;
                if (this.bShowFilter)
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Botón de Autorización"), Category("Botones")]
        public bool ShowAutorizar {
            get {
                return this.Autorizar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Autorizar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Autorizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Botón carga de imagen"), Category("Botones")]
        public bool ShowImagen {
            get {
                return this.Imagen.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imagen.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Imagen.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Exportar en formato Excel"), Category("Botones")]
        public bool ShowExportarExcel {
            get {
                return this.ExportarExcel.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Cerrar ventana"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        private void CrearReadOnly() {
            this.Nuevo.Enabled = !this._ReadOnly;
            this.Editar.Enabled = !this._ReadOnly;
            this.Remover.Enabled = !this._ReadOnly;
            this.Guardar.Enabled = !this._ReadOnly;
            this.Imprimir.Enabled = !this._ReadOnly;
            this.Actualizar.Enabled = !this._ReadOnly;
            this.Autorizar.Enabled = !this._ReadOnly;
            this.Imagen.Enabled = !this._ReadOnly;
            this.Filtro.Enabled = !this._ReadOnly;
            this.ExportarExcel.Enabled = !this._ReadOnly;
        }

        private void ToolBar_ItemVisibleInStripChanged(object sender, EventArgs e) {
            
        }

        private void ToolBar_ItemsChanged(Telerik.WinControls.RadCommandBarBaseItemCollection changed, Telerik.WinControls.UI.RadCommandBarBaseItem target, Telerik.WinControls.ItemsChangeOperation operation) {
            
        }

        private void ToolBar_VisibleInCommandBarChanged(object sender, EventArgs e) {
            //var b0 = (this.ShowNuevo && this.ShowEditar && this.ShowRemover && this.ShowAutorizar && this.ShowImagen && this.ShowGuardar) == true;
            
        }
    }
}
