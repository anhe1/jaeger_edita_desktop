﻿namespace Jaeger.UI.Common.Forms {
    partial class TBarCommonControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.CommandBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ItemCaption = new Telerik.WinControls.UI.CommandBarLabel();
            this.ItemHost = new Telerik.WinControls.UI.CommandBarHostItem();
            this.HostSeparator = new Telerik.WinControls.UI.CommandBarSeparator();
            this.PeriodoLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.Periodo = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.EjercicioLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.EjercicioHost = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.AutoSuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            this.CommandBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Controls.Add(this.Ejercicio);
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement});
            this.CommandBar.Size = new System.Drawing.Size(1076, 55);
            this.CommandBar.TabIndex = 0;
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(456, 3);
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(55, 20);
            this.Ejercicio.TabIndex = 3;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CommandBarRowElement
            // 
            this.CommandBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement.Name = "commandBarRowElement1";
            this.CommandBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ItemCaption,
            this.ItemHost,
            this.HostSeparator,
            this.PeriodoLabel,
            this.Periodo,
            this.EjercicioLabel,
            this.EjercicioHost,
            this.Separator1,
            this.Nuevo,
            this.Editar,
            this.Cancelar,
            this.Separator2,
            this.Actualizar,
            this.Filtro,
            this.AutoSuma,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // ItemCaption
            // 
            this.ItemCaption.DisplayName = "commandBarLabel1";
            this.ItemCaption.Name = "ItemCaption";
            this.ItemCaption.Text = "lblItem";
            this.ItemCaption.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ItemHost
            // 
            this.ItemHost.DisplayName = "commandBarHostItem1";
            this.ItemHost.MinSize = new System.Drawing.Size(200, 0);
            this.ItemHost.Name = "ItemHost";
            this.ItemHost.Text = "";
            this.ItemHost.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // HostSeparator
            // 
            this.HostSeparator.DisplayName = "commandBarSeparator3";
            this.HostSeparator.Name = "HostSeparator";
            this.HostSeparator.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.HostSeparator.VisibleInOverflowMenu = false;
            // 
            // PeriodoLabel
            // 
            this.PeriodoLabel.DisplayName = "Etiqueta: Periodo";
            this.PeriodoLabel.Name = "PeriodoLabel";
            this.PeriodoLabel.Text = "Periodo:";
            // 
            // Periodo
            // 
            this.Periodo.AutoCompleteDisplayMember = "Descripcion";
            this.Periodo.AutoCompleteValueMember = "Id";
            this.Periodo.DisplayMember = "Descripcion";
            this.Periodo.DisplayName = "Periodo";
            this.Periodo.DropDownAnimationEnabled = true;
            this.Periodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Periodo.MaxDropDownItems = 0;
            this.Periodo.Name = "Periodo";
            this.Periodo.Text = "";
            this.Periodo.ValueMember = "Id";
            // 
            // EjercicioLabel
            // 
            this.EjercicioLabel.DisplayName = "Etiqueta: Ejercicio";
            this.EjercicioLabel.Name = "EjercicioLabel";
            this.EjercicioLabel.Text = "Ejercicio:";
            // 
            // EjercicioHost
            // 
            this.EjercicioHost.DisplayName = "Host: Ejercicio";
            this.EjercicioHost.MaxSize = new System.Drawing.Size(55, 20);
            this.EjercicioHost.MinSize = new System.Drawing.Size(55, 20);
            this.EjercicioHost.Name = "EjercicioHost";
            this.EjercicioHost.Text = "Selecciona";
            // 
            // Separator1
            // 
            this.Separator1.DisplayName = "commandBarSeparator1";
            this.Separator1.Name = "Separator1";
            this.Separator1.VisibleInOverflowMenu = false;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Editar
            // 
            this.Editar.DisplayName = "Editar";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_file_16px;
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Image = global::Jaeger.UI.Common.Properties.Resources.delete_file_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "commandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // AutoSuma
            // 
            this.AutoSuma.DisplayName = "AutoSuma";
            this.AutoSuma.DrawText = true;
            this.AutoSuma.Image = global::Jaeger.UI.Common.Properties.Resources.sigma_16px;
            this.AutoSuma.Name = "AutoSuma";
            this.AutoSuma.Text = "AutoSuma";
            this.AutoSuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarExcel});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Image = global::Jaeger.UI.Common.Properties.Resources.xls_16px;
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Text = "Exportar Excel";
            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TBarCommonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CommandBar);
            this.Name = "TBarCommonControl";
            this.Size = new System.Drawing.Size(1076, 30);
            this.Load += new System.EventHandler(this.TBarCommonControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            this.CommandBar.ResumeLayout(false);
            this.CommandBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        public Telerik.WinControls.UI.CommandBarLabel EjercicioLabel;
        public Telerik.WinControls.UI.CommandBarDropDownButton Nuevo;
        public Telerik.WinControls.UI.CommandBarButton Editar;
        public Telerik.WinControls.UI.CommandBarButton Cancelar;
        public Telerik.WinControls.UI.CommandBarDropDownButton Imprimir;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarToggleButton AutoSuma;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        public Telerik.WinControls.UI.RadMenuItem ExportarExcel;
        public Telerik.WinControls.UI.CommandBarLabel ItemCaption;
        public Telerik.WinControls.UI.CommandBarHostItem ItemHost;
        public Telerik.WinControls.UI.CommandBarSeparator HostSeparator;
        public Telerik.WinControls.UI.CommandBarLabel PeriodoLabel;
        public Telerik.WinControls.UI.CommandBarDropDownList Periodo;
        public Telerik.WinControls.UI.CommandBarHostItem EjercicioHost;
        public Telerik.WinControls.UI.CommandBarSeparator Separator1;
        public Telerik.WinControls.UI.CommandBarSeparator Separator2;
    }
}
