﻿using System;
using System.Windows.Forms;
using Telerik.Pivot.Core;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class PivotStandarControl : UserControl {
        protected LocalDataSourceProvider dataProvider;

        public PivotStandarControl() {
            PivotGridLocalizationProvider.CurrentProvider = new LocalizationProviderPivotGrid();
            InitializeComponent();
        }

        private void PivotStandarControl_Load(object sender, EventArgs e) {
            this.dataProvider = new LocalDataSourceProvider {
                Culture = new System.Globalization.CultureInfo("es-MX")
            };
            this.TPivot.ExportarExcel.Click += ExportarExcel_Click;
        }

        private void ExportarExcel_Click(object sender, EventArgs e) {
            var saveFileDialog1 = new SaveFileDialog {
                Filter = "Excel XLSX|*.xlsx",
                Title = "Export to File", 
                FileName = "Ventas_Resumen.xlsx"
            };
            if (saveFileDialog1.ShowDialog() == DialogResult.OK) {
                if (saveFileDialog1.FileName != "") {
                    HelperTelerikExport.RunExportPivotGrid(this.PivotGrid, saveFileDialog1.FileName, "Resumen");
                    MessageBox.Show("Successfully exported to " + saveFileDialog1.FileName, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try {
                        System.Diagnostics.Process.Start(saveFileDialog1.FileName);
                    } finally {
                    }
                }
            }
        }
    }
}
