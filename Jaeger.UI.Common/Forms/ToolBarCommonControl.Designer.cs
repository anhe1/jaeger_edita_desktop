﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBarCommonControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.SpinEjercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarComboBoxPeriodo = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.ToolBarLabelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarHostEjercicio = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.AutoSuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.HostCaption = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostItem = new Telerik.WinControls.UI.CommandBarHostItem();
            this.HostSeparator = new Telerik.WinControls.UI.CommandBarSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            this.CommandBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEjercicio)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Controls.Add(this.SpinEjercicio);
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.CommandBar.Size = new System.Drawing.Size(1076, 55);
            this.CommandBar.TabIndex = 0;
            // 
            // SpinEjercicio
            // 
            this.SpinEjercicio.Location = new System.Drawing.Point(1006, 3);
            this.SpinEjercicio.Name = "SpinEjercicio";
            this.SpinEjercicio.Size = new System.Drawing.Size(55, 20);
            this.SpinEjercicio.TabIndex = 3;
            this.SpinEjercicio.TabStop = false;
            this.SpinEjercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.HostCaption,
            this.HostItem,
            this.HostSeparator,
            this.ToolBarLabelPeriodo,
            this.ToolBarComboBoxPeriodo,
            this.ToolBarLabelEjercicio,
            this.ToolBarHostEjercicio,
            this.commandBarSeparator1,
            this.Nuevo,
            this.Editar,
            this.Cancelar,
            this.commandBarSeparator2,
            this.Actualizar,
            this.Filtro,
            this.AutoSuma,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.DisplayName = "Etiqueta: Periodo";
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Text = "Periodo:";
            // 
            // ToolBarComboBoxPeriodo
            // 
            this.ToolBarComboBoxPeriodo.AutoCompleteDisplayMember = "Descripcion";
            this.ToolBarComboBoxPeriodo.AutoCompleteValueMember = "Id";
            this.ToolBarComboBoxPeriodo.DisplayMember = "Descripcion";
            this.ToolBarComboBoxPeriodo.DisplayName = "Periodo";
            this.ToolBarComboBoxPeriodo.DropDownAnimationEnabled = true;
            this.ToolBarComboBoxPeriodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ToolBarComboBoxPeriodo.MaxDropDownItems = 0;
            this.ToolBarComboBoxPeriodo.Name = "ToolBarComboBoxPeriodo";
            this.ToolBarComboBoxPeriodo.Text = "";
            this.ToolBarComboBoxPeriodo.ValueMember = "Id";
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarHostEjercicio
            // 
            this.ToolBarHostEjercicio.DisplayName = "Host: Ejercicio";
            this.ToolBarHostEjercicio.MaxSize = new System.Drawing.Size(55, 20);
            this.ToolBarHostEjercicio.MinSize = new System.Drawing.Size(55, 20);
            this.ToolBarHostEjercicio.Name = "ToolBarHostEjercicio";
            this.ToolBarHostEjercicio.Text = "Selecciona";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Editar
            // 
            this.Editar.DisplayName = "Editar";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_file_16px;
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Image = global::Jaeger.UI.Common.Properties.Resources.delete_file_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // AutoSuma
            // 
            this.AutoSuma.DisplayName = "AutoSuma";
            this.AutoSuma.DrawText = true;
            this.AutoSuma.Image = global::Jaeger.UI.Common.Properties.Resources.sigma_16px;
            this.AutoSuma.Name = "AutoSuma";
            this.AutoSuma.Text = "AutoSuma";
            this.AutoSuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarExcel});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Image = global::Jaeger.UI.Common.Properties.Resources.xls_16px;
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Text = "Exportar Excel";
            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // HostCaption
            // 
            this.HostCaption.DisplayName = "commandBarLabel1";
            this.HostCaption.Name = "HostCaption";
            this.HostCaption.Text = "lblItem";
            this.HostCaption.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // HostItem
            // 
            this.HostItem.DisplayName = "commandBarHostItem1";
            this.HostItem.MinSize = new System.Drawing.Size(200, 0);
            this.HostItem.Name = "HostItem";
            this.HostItem.Text = "";
            this.HostItem.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // HostSeparator
            // 
            this.HostSeparator.DisplayName = "commandBarSeparator3";
            this.HostSeparator.Name = "HostSeparator";
            this.HostSeparator.Text = "";
            this.HostSeparator.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.HostSeparator.VisibleInOverflowMenu = false;
            // 
            // ToolBarCommonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CommandBar);
            this.Name = "ToolBarCommonControl";
            this.Size = new System.Drawing.Size(1076, 30);
            this.Load += new System.EventHandler(this.ToolBarCommonControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            this.CommandBar.ResumeLayout(false);
            this.CommandBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEjercicio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelPeriodo;
        private Telerik.WinControls.UI.CommandBarDropDownList ToolBarComboBoxPeriodo;
        public Telerik.WinControls.UI.CommandBarLabel ToolBarLabelEjercicio;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostEjercicio;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadSpinEditor SpinEjercicio;
        public Telerik.WinControls.UI.CommandBarDropDownButton Nuevo;
        public Telerik.WinControls.UI.CommandBarButton Editar;
        public Telerik.WinControls.UI.CommandBarButton Cancelar;
        public Telerik.WinControls.UI.CommandBarDropDownButton Imprimir;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarToggleButton AutoSuma;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        public Telerik.WinControls.UI.RadMenuItem ExportarExcel;
        public Telerik.WinControls.UI.CommandBarLabel HostCaption;
        public Telerik.WinControls.UI.CommandBarHostItem HostItem;
        public Telerik.WinControls.UI.CommandBarSeparator HostSeparator;
    }
}
