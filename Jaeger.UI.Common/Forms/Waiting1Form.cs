﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class Waiting1Form : Form {
        public Action Worker { get; set; }

        public Waiting1Form(Action worker) {
            InitializeComponent();
            if (worker == null)
                throw new ArgumentOutOfRangeException();
            this.Worker = worker;
        }

        private void Waiting1Form_Load(object sender, EventArgs e) {
            this.labelTitulo.Text = this.Text;
            this.waitingBar.StartWaiting();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            Task.Factory.StartNew(this.Worker).ContinueWith(it => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
