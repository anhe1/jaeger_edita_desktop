﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class ToolBarStandarBuscarControl : UserControl {
        public event EventHandler<EventArgs> ButtonBuscar_Click;
        public event EventHandler<EventArgs> ButtonAgregar_Click;
        public event EventHandler<EventArgs> ButtonFiltro_Click;
        public event EventHandler<EventArgs> ButtonCerrar_Click;
        public event EventHandler<EventArgs> ButtonExistencia_Click;

        public ToolBarStandarBuscarControl() {
            InitializeComponent();
        }

        protected void OnButtonBuscarClick(object sender, EventArgs e) {
            if (this.ButtonBuscar_Click != null)
                this.ButtonBuscar_Click(sender, e);
        }

        protected void OnButtonAgregarClick(object sender, EventArgs e) {
            if (this.ButtonAgregar_Click != null)
                this.ButtonAgregar_Click(sender, e);
        }

        protected void OnButtonFiltroClick(object sender, EventArgs e) {
            if (this.ButtonFiltro_Click != null)
                this.ButtonFiltro_Click(sender, e);
        }

        protected void OnButtonExistencia_Click(object sender, EventArgs e) {
            if (this.ButtonExistencia_Click != null)
                this.ButtonExistencia_Click(sender, e);
        }

        protected void OnButtonCerrarClick(object sender, EventArgs e) {
            if (this.ButtonCerrar_Click != null)
                this.ButtonCerrar_Click(sender, e);
        }

        private void ProductoBuscarToolBarControl_Load(object sender, EventArgs e) {
            this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;

            this.Existencia.HostedItem = this.cExistencia.ButtonElement;

            this.Buscar.Click += new EventHandler(this.OnButtonBuscarClick);
            this.Agregar.Click += new EventHandler(this.OnButtonAgregarClick);
            this.Filtro.Click += new EventHandler(this.OnButtonFiltroClick);
            this.Cerrar.Click += new EventHandler(this.OnButtonCerrarClick);
            this.cExistencia.Click += new EventHandler(this.OnButtonExistencia_Click);
        }

        [Description("Texto que se muestra en el cuadro de busqueda"), Category("Botones")]
        public string Etiqueta {
            get {
                return this.LabelBuscar.Text;
            }
            set {
                this.LabelBuscar.Text = value;
                if (this.LabelBuscar.Text.Length > 0) {
                    this.LabelBuscar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.LabelBuscar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Mostrar botón agregar"), Category("Botones")]
        public bool ShowBuscar {
            get {
                return this.Buscar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Buscar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Buscar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar botón agregar"), Category("Botones")]
        public bool ShowAgregar {
            get {
                return this.Agregar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Agregar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Agregar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar botón agregar"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowExistencia {
            get {
                return this.Existencia.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Existencia.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Existencia.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }
    }
}
