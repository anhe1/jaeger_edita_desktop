﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBarStandarBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.LabelBuscar = new Telerik.WinControls.UI.CommandBarLabel();
            this.Descripcion = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Existencia = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.cExistencia = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cExistencia)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(681, 55);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.LabelBuscar,
            this.Descripcion,
            this.Buscar,
            this.Agregar,
            this.Filtro,
            this.Existencia,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // LabelBuscar
            // 
            this.LabelBuscar.DisplayName = "Etiqueta: Producto";
            this.LabelBuscar.Name = "LabelBuscar";
            this.LabelBuscar.Text = "Producto / Servicio:";
            // 
            // Descripcion
            // 
            this.Descripcion.DisplayName = "Descripción";
            this.Descripcion.MaxSize = new System.Drawing.Size(260, 22);
            this.Descripcion.MinSize = new System.Drawing.Size(260, 22);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.StretchHorizontally = false;
            this.Descripcion.StretchVertically = false;
            this.Descripcion.Text = "";
            ((Telerik.WinControls.UI.RadTextBoxElement)(this.Descripcion.GetChildAt(0))).Text = "";
            // 
            // Buscar
            // 
            this.Buscar.DisplayName = "Buscar";
            this.Buscar.DrawText = true;
            this.Buscar.Image = global::Jaeger.UI.Common.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Agregar
            // 
            this.Agregar.DisplayName = "commandBarButton1";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Common.Properties.Resources.checkmark_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Existencia
            // 
            this.Existencia.DisplayName = "Existencia";
            this.Existencia.Name = "Existencia";
            this.Existencia.Text = "";
            this.Existencia.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // cExistencia
            // 
            this.cExistencia.Location = new System.Drawing.Point(510, 83);
            this.cExistencia.Name = "cExistencia";
            this.cExistencia.Size = new System.Drawing.Size(94, 18);
            this.cExistencia.TabIndex = 1;
            this.cExistencia.Text = "Solo existencia";
            // 
            // ToolBarStandarBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cExistencia);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ToolBarStandarBuscarControl";
            this.Size = new System.Drawing.Size(681, 30);
            this.Load += new System.EventHandler(this.ProductoBuscarToolBarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cExistencia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel LabelBuscar;
        public Telerik.WinControls.UI.CommandBarTextBox Descripcion;
        private Telerik.WinControls.UI.CommandBarHostItem Existencia;
        public Telerik.WinControls.UI.RadCheckBox cExistencia;
        public Telerik.WinControls.UI.CommandBarButton Buscar;
        public Telerik.WinControls.UI.CommandBarButton Agregar;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
    }
}
