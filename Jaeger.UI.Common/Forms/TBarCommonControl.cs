﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class TBarCommonControl : UserControl {
        public TBarCommonControl() {
            InitializeComponent();
        }

        private void TBarCommonControl_Load(object sender, EventArgs e) {
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
            this.EjercicioHost.HostedItem = this.Ejercicio.SpinElement;

            this.Ejercicio.Minimum = 2013;
            this.Ejercicio.Maximum = DateTime.Now.Year;
            this.Ejercicio.Value = DateTime.Now.Year;

            this.Periodo.DisplayMember = "Descripcion";
            this.Periodo.ValueMember = "Id";
            this.Periodo.DataSource = ConfigService.GetMeses();
            this.Periodo.SelectedIndex = DateTime.Now.Month;
        }

        #region metodos publicos
        public int GetMes() {
            if (this.Periodo.SelectedValue != null) {
                return (int)this.Periodo.SelectedValue;
            } else {
                return (int)Enum.Parse(typeof(MesesEnum), this.Periodo.Text);
            }
        }

        public int GetEjercicio() {
            return int.Parse(this.Ejercicio.Value.ToString());
        }
        #endregion

        [Description("Hostitem"), Category("Botones")]
        public bool ShowItem {
            get {
                return this.ItemCaption.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.ItemCaption.Visibility = ElementVisibility.Visible;
                    this.ItemHost.Visibility = ElementVisibility.Visible;
                    this.HostSeparator.Visibility = ElementVisibility.Visible;
                } else {
                    this.ItemCaption.Visibility = ElementVisibility.Collapsed;
                    this.ItemHost.Visibility = ElementVisibility.Collapsed;
                    this.HostSeparator.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Periodo"), Category("Botones")]
        public bool ShowPeriodo {
            get {
                return this.PeriodoLabel.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.PeriodoLabel.Visibility = ElementVisibility.Visible;
                    this.Periodo.Visibility = ElementVisibility.Visible;
                } else {
                    this.PeriodoLabel.Visibility = ElementVisibility.Collapsed;
                    this.Periodo.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Ejercicio"), Category("Botones")]
        public bool ShowEjercicio {
            get {
                return this.EjercicioLabel.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.EjercicioLabel.Visibility = ElementVisibility.Visible;
                    this.EjercicioHost.Visibility = ElementVisibility.Visible;
                } else {
                    this.EjercicioLabel.Visibility = ElementVisibility.Collapsed;
                    this.EjercicioHost.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Nuevo"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Nuevo.Visibility = ElementVisibility.Visible;
                else
                    this.Nuevo.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Editar"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Editar.Visibility = ElementVisibility.Visible;
                else
                    this.Editar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Cancelar"), Category("Botones")]
        public bool ShowCancelar {
            get {
                return this.Cancelar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cancelar.Visibility = ElementVisibility.Visible;
                else
                    this.Cancelar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Actualizar"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Filtro"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Autosuma"), Category("Botones")]
        public bool ShowAutosuma {
            get {
                return this.AutoSuma.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.AutoSuma.Visibility = ElementVisibility.Visible;
                else
                    this.AutoSuma.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Imprimir"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imprimir.Visibility = ElementVisibility.Visible;
                else
                    this.Imprimir.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Herramientas"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Exportar en formato Excel"), Category("Botones")]
        public bool ShowExportarExcel {
            get {
                return this.ExportarExcel.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.ExportarExcel.Visibility = ElementVisibility.Visible;
                else
                    this.ExportarExcel.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Cerrar"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
            }
        }
    }
}
