﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Common.Forms {
    public partial class ConfiguracionForm : RadForm {
        public ConfiguracionForm() {
            InitializeComponent();
        }

        public ConfiguracionForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        public virtual void ConfiguracionForm_Load(object sender, EventArgs e) {
            
        }

        public virtual void Guardar_Click(object sender, EventArgs e) {

        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
