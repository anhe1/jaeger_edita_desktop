﻿using System;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class ToolBarConceptoControl : UserControl {
        private bool isEditable = true;

        public ToolBarConceptoControl() {
            InitializeComponent();
        }
        
        private void ToolBarConceptoControl_Load(object sender, EventArgs e) {
            this.HostConceptos.HostedItem = this.CboConcepto.MultiColumnComboBoxElement;
        }

        private void SetEditable() {
            this.CboConcepto.SetEditable(this.IsEditable);
            this.Agregar.Enabled = this.isEditable;
            this.Unidades.Enabled = this.isEditable;
            this.Duplicar.Enabled = this.isEditable;
            this.Nuevo.Enabled = this.isEditable;
            this.Productos.Enabled = this.isEditable;
            this.Remover.Enabled = this.isEditable;
            this.Complementos.Enabled = this.isEditable;
        }

        private void CboConcepto_VisibleChanged(object sender, EventArgs e) {
            if (this.CboConcepto.Visible == false) {
                this.HostConceptos.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                this.LabelProducto.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            } else {
                this.HostConceptos.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                this.LabelProducto.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            }
        }

        public bool IsEditable {
            get {
                return this.isEditable;
            }
            set {
                this.isEditable = value;
                this.SetEditable();
            }
        }

        public bool ShowAgregar {
            get { return this.Agregar.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set { if (value == true)
                    this.Agregar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Agregar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        public bool ShowNuevo {
            get { return this.Nuevo.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value == true)
                    this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        public bool ShowRemover {
            get { return this.Remover.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value == true)
                    this.Remover.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Remover.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        public bool ShowDuplicar {
            get { return this.Duplicar.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value == true)
                    this.Duplicar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Duplicar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        public bool ShowProductos {
            get { return this.Productos.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value == true)
                    this.Productos.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Productos.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        public bool ShowUnidades {
            get { return this.Unidades.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value == true)
                    this.Unidades.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Unidades.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        public bool ShowComboConceptos {
            get { return this.CboConcepto.Visible; }
            set { this.CboConcepto.Visible = value; }
        }
    }
}
