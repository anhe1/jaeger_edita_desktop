﻿
namespace Jaeger.UI.Common.Forms {
    partial class GridStandarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imagen = new Telerik.WinControls.UI.CommandBarButton();
            this.Autorizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Titulo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ItemLbl = new Telerik.WinControls.UI.CommandBarLabel();
            this.ItemHost = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.AutoSuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.commandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarExcel});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Image = global::Jaeger.UI.Common.Properties.Resources.xls_16px;
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Text = "Exportar Excel";
            this.ExportarExcel.UseCompatibleTextRendering = false;
            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Filtro.Click += new System.EventHandler(this.Filtro_Click);
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator3
            // 
            this.Separator3.DisplayName = "commandBarSeparator1";
            this.Separator3.Name = "Separator3";
            this.Separator3.VisibleInOverflowMenu = false;
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Image = global::Jaeger.UI.Common.Properties.Resources.save_16px;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Imagen
            // 
            this.Imagen.DisplayName = "Imagen";
            this.Imagen.DrawText = true;
            this.Imagen.Image = global::Jaeger.UI.Common.Properties.Resources.add_image_16px;
            this.Imagen.Name = "Imagen";
            this.Imagen.Text = "Imagen";
            this.Imagen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imagen.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Autorizar
            // 
            this.Autorizar.DisplayName = "Autorizar";
            this.Autorizar.DrawText = true;
            this.Autorizar.Image = global::Jaeger.UI.Common.Properties.Resources.checkmark_16px;
            this.Autorizar.Name = "Autorizar";
            this.Autorizar.Text = "Autorizar";
            this.Autorizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Autorizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Remover
            // 
            this.Remover.DisplayName = "Remover";
            this.Remover.DrawText = true;
            this.Remover.Image = global::Jaeger.UI.Common.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Editar
            // 
            this.Editar.DisplayName = "Editar";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_16px;
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.add_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator1
            // 
            this.Separator1.DisplayName = "Separador";
            this.Separator1.MinSize = new System.Drawing.Size(2, 0);
            this.Separator1.Name = "Separator1";
            this.Separator1.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.Separator1.VisibleInOverflowMenu = false;
            // 
            // Titulo
            // 
            this.Titulo.DisplayName = "Titulo";
            this.Titulo.Name = "Titulo";
            this.Titulo.Text = "";
            this.Titulo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Titulo,
            this.Separator1,
            this.ItemLbl,
            this.ItemHost,
            this.Separator2,
            this.Nuevo,
            this.Editar,
            this.Remover,
            this.Autorizar,
            this.Imagen,
            this.Guardar,
            this.Separator3,
            this.Actualizar,
            this.Imprimir,
            this.Filtro,
            this.AutoSuma,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            this.ToolBar.Text = "";
            // 
            // ItemLbl
            // 
            this.ItemLbl.DisplayName = "Host label";
            this.ItemLbl.Name = "ItemLbl";
            this.ItemLbl.Text = "";
            this.ItemLbl.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ItemLbl.VisibleInOverflowMenu = false;
            // 
            // ItemHost
            // 
            this.ItemHost.DisplayName = "Host item";
            this.ItemHost.Name = "ItemHost";
            this.ItemHost.Text = "HostItem";
            this.ItemHost.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ItemHost.VisibleInOverflowMenu = false;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "commandBarSeparator1";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            this.Separator2.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // AutoSuma
            // 
            this.AutoSuma.DisplayName = "AutoSuma";
            this.AutoSuma.DrawText = true;
            this.AutoSuma.Image = global::Jaeger.UI.Common.Properties.Resources.sigma_16px;
            this.AutoSuma.Name = "AutoSuma";
            this.AutoSuma.Text = "AutoSuma";
            this.AutoSuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // BarRowElement
            // 
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.BarRowElement.Text = "";
            // 
            // commandBar
            // 
            this.commandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.commandBar.Location = new System.Drawing.Point(0, 0);
            this.commandBar.Name = "commandBar";
            this.commandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.commandBar.Size = new System.Drawing.Size(1066, 55);
            this.commandBar.TabIndex = 2;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 55);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1066, 499);
            this.GridData.TabIndex = 3;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            this.GridData.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            this.GridData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridData_KeyDown);
            // 
            // GridStandarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.commandBar);
            this.Name = "GridStandarControl";
            this.Size = new System.Drawing.Size(1066, 554);
            this.Load += new System.EventHandler(this.GridStandarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.commandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        public Telerik.WinControls.UI.RadMenuItem ExportarExcel;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarButton Imprimir;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        private Telerik.WinControls.UI.CommandBarSeparator Separator3;
        public Telerik.WinControls.UI.CommandBarButton Guardar;
        public Telerik.WinControls.UI.CommandBarButton Imagen;
        public Telerik.WinControls.UI.CommandBarButton Autorizar;
        public Telerik.WinControls.UI.CommandBarButton Remover;
        public Telerik.WinControls.UI.CommandBarButton Editar;
        public Telerik.WinControls.UI.CommandBarButton Nuevo;
        private Telerik.WinControls.UI.CommandBarSeparator Separator1;
        public Telerik.WinControls.UI.CommandBarLabel Titulo;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.RadCommandBar commandBar;
        public Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarToggleButton AutoSuma;
        public Telerik.WinControls.UI.CommandBarLabel ItemLbl;
        public Telerik.WinControls.UI.CommandBarHostItem ItemHost;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
    }
}
