﻿
namespace Jaeger.UI.Common.Forms {
    partial class TbPeriodoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TbPeriodoControl));
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.FFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FInicial = new Telerik.WinControls.UI.RadDateTimePicker();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarRowElement();
            this.TEvaluacion = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblDepartamento = new Telerik.WinControls.UI.CommandBarLabel();
            this.Departamento = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.SeparatorDepartamento = new Telerik.WinControls.UI.CommandBarSeparator();
            this.FInicialLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.THost1 = new Telerik.WinControls.UI.CommandBarHostItem();
            this.FFinalLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.THost2 = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.radCommandBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FInicial)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Controls.Add(this.FFinal);
            this.radCommandBar1.Controls.Add(this.FInicial);
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBar});
            this.radCommandBar1.Size = new System.Drawing.Size(1071, 55);
            this.radCommandBar1.TabIndex = 1;
            // 
            // FFinal
            // 
            this.FFinal.CustomFormat = "ddd dd/MM/yyyy";
            this.FFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FFinal.Location = new System.Drawing.Point(420, 5);
            this.FFinal.Name = "FFinal";
            this.FFinal.Size = new System.Drawing.Size(111, 20);
            this.FFinal.TabIndex = 2;
            this.FFinal.TabStop = false;
            this.FFinal.Text = "dom. 20/09/2020";
            this.FFinal.Value = new System.DateTime(2020, 9, 20, 0, 27, 19, 89);
            // 
            // FInicial
            // 
            this.FInicial.CustomFormat = "ddd dd/MM/yyyy";
            this.FInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FInicial.Location = new System.Drawing.Point(276, 5);
            this.FInicial.Name = "FInicial";
            this.FInicial.Size = new System.Drawing.Size(111, 20);
            this.FInicial.TabIndex = 3;
            this.FInicial.TabStop = false;
            this.FInicial.Text = "dom. 20/09/2020";
            this.FInicial.Value = new System.DateTime(2020, 9, 20, 0, 27, 19, 89);
            // 
            // ToolBar
            // 
            this.ToolBar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.TEvaluacion});
            this.ToolBar.Text = "";
            this.ToolBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.UseCompatibleTextRendering = false;
            // 
            // TEvaluacion
            // 
            this.TEvaluacion.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TEvaluacion.DisplayName = "commandBarStripElement1";
            this.TEvaluacion.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblDepartamento,
            this.Departamento,
            this.SeparatorDepartamento,
            this.FInicialLabel,
            this.THost1,
            this.FFinalLabel,
            this.THost2,
            this.Actualizar,
            this.Imprimir,
            this.Cerrar});
            this.TEvaluacion.Name = "TEvaluacion";
            this.TEvaluacion.StretchHorizontally = true;
            this.TEvaluacion.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TEvaluacion.UseCompatibleTextRendering = false;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblDepartamento.DisplayName = "Departamento";
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Text = "Departamento";
            this.lblDepartamento.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblDepartamento.UseCompatibleTextRendering = false;
            // 
            // Departamento
            // 
            this.Departamento.AutoSize = true;
            this.Departamento.AutoSizeItems = true;
            this.Departamento.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.Departamento.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Departamento.DisplayName = "Departamento";
            this.Departamento.DrawImage = false;
            this.Departamento.DrawText = true;
            this.Departamento.DropDownAnimationEnabled = true;
            this.Departamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Departamento.Image = ((System.Drawing.Image)(resources.GetObject("Departamento.Image")));
            this.Departamento.MaxDropDownItems = 0;
            this.Departamento.Name = "Departamento";
            this.Departamento.Text = "Seleccione";
            this.Departamento.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.Departamento.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Departamento.UseCompatibleTextRendering = false;
            // 
            // SeparatorDepartamento
            // 
            this.SeparatorDepartamento.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.SeparatorDepartamento.DisplayName = "commandBarSeparator1";
            this.SeparatorDepartamento.Name = "SeparatorDepartamento";
            this.SeparatorDepartamento.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.SeparatorDepartamento.UseCompatibleTextRendering = false;
            this.SeparatorDepartamento.VisibleInOverflowMenu = false;
            // 
            // FInicialLabel
            // 
            this.FInicialLabel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.FInicialLabel.DisplayName = "commandBarLabel1";
            this.FInicialLabel.Name = "FInicialLabel";
            this.FInicialLabel.Text = "Período del:";
            this.FInicialLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.FInicialLabel.UseCompatibleTextRendering = false;
            // 
            // THost1
            // 
            this.THost1.AutoSize = true;
            this.THost1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.THost1.DisplayName = "commandBarHostItem1";
            this.THost1.MinSize = new System.Drawing.Size(120, 0);
            this.THost1.Name = "THost1";
            this.THost1.Text = "StartDate";
            this.THost1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.THost1.UseCompatibleTextRendering = false;
            // 
            // FFinalLabel
            // 
            this.FFinalLabel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.FFinalLabel.DisplayName = "commandBarLabel2";
            this.FFinalLabel.Name = "FFinalLabel";
            this.FFinalLabel.Text = " al:";
            this.FFinalLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.FFinalLabel.UseCompatibleTextRendering = false;
            // 
            // THost2
            // 
            this.THost2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.THost2.DisplayName = "commandBarHostItem1";
            this.THost2.MinSize = new System.Drawing.Size(120, 0);
            this.THost2.Name = "THost2";
            this.THost2.Text = "EndDate";
            this.THost2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.THost2.UseCompatibleTextRendering = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Actualizar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.UseCompatibleTextRendering = false;
            // 
            // Imprimir
            // 
            this.Imprimir.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Imprimir.UseCompatibleTextRendering = false;
            // 
            // Cerrar
            // 
            this.Cerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.DisplayName = "commandBarButton1";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.UseCompatibleTextRendering = false;
            // 
            // TbPeriodoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radCommandBar1);
            this.Name = "TbPeriodoControl";
            this.Size = new System.Drawing.Size(1071, 30);
            this.Load += new System.EventHandler(this.TbPeriodoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.radCommandBar1.ResumeLayout(false);
            this.radCommandBar1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FInicial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement ToolBar;
        private Telerik.WinControls.UI.CommandBarStripElement TEvaluacion;
        private Telerik.WinControls.UI.CommandBarLabel lblDepartamento;
        public Telerik.WinControls.UI.CommandBarDropDownList Departamento;
        private Telerik.WinControls.UI.CommandBarSeparator SeparatorDepartamento;
        private Telerik.WinControls.UI.CommandBarHostItem THost1;
        private Telerik.WinControls.UI.CommandBarHostItem THost2;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        public Telerik.WinControls.UI.CommandBarButton Imprimir;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.RadDateTimePicker FFinal;
        public Telerik.WinControls.UI.RadDateTimePicker FInicial;
        public Telerik.WinControls.UI.CommandBarLabel FInicialLabel;
        public Telerik.WinControls.UI.CommandBarLabel FFinalLabel;
    }
}
