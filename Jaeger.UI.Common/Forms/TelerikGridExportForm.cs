﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class TelerikGridExportForm : RadForm {
        private BackgroundWorker consultar;
        private bool exportVisualSettings;
        private bool exportHiddenColumns;
        private bool bResultExport = false;
        private RadGridView objGridView;
        private string fileName;

        public TelerikGridExportForm(RadGridView objeto) {
            InitializeComponent();
            base.Load += TelerikGridExport_Load;
            this.objGridView = objeto;
        }

        private void TelerikGridExport_Load(object sender, EventArgs e) {
            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += ConsultarDoWork;
            this.consultar.RunWorkerCompleted += ConsultarRunWorkerCompleted;

            BindingList<FormatoExportarEnum> listaExporta = new BindingList<FormatoExportarEnum>()
            {
                FormatoExportarEnum.Excel,
                FormatoExportarEnum.Csv
            };
            this.CboExport.DataSource = listaExporta;
        }

        private void CboExportTextChanged(object sender, EventArgs e) {
            this.CheckExportSettings.Enabled = (this.CboExport.Text == "Excel");
            this.CheckExportSettings.Checked = (this.CboExport.Text == "Excel");
        }

        private void ChkExportSettingsToggleStateChanged(object sender, StateChangedEventArgs args) {
            this.OptExportMaxRows.Enabled = this.CheckExportSettings.Checked;
            this.OptExportRowsSupport.Enabled = this.CheckExportSettings.Checked;
            this.CboExportSummaries.Enabled = this.CheckExportSettings.Checked;
            this.TxbExportSheet.Enabled = this.CheckExportSettings.Checked;
        }

        private void BtnExportClick(object sender, EventArgs e) {
            bool bResultExport = false;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (this.CboExport.SelectedIndex != 0) {
                saveFileDialog.Filter = "CSV File (*.csv)|*.csv";
            }
            else {
                saveFileDialog.Filter = "Excel (*.xls)|*.xls";
            }
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                if (!saveFileDialog.FileName.Equals(string.Empty)) {
                    string fileName = saveFileDialog.FileName;
                    if (this.CheckExportSettings.ToggleState != ToggleState.On) {
                        this.exportVisualSettings = false;
                    }
                    else {
                        this.exportVisualSettings = true;
                    }
                    switch (this.CboExport.SelectedIndex) {
                        case 0: {
                                if (this.OptExportMaxRows.ToggleState == ToggleState.On) {
                                    bResultExport = Services.HelperTelerikExport.RunExportToExcel(ref this.objGridView, fileName, this.TxbExportSheet.Text, this.SummariesExportOption(), ExcelMaxRows._1048576, this.exportVisualSettings);
                                }
                                else if (this.OptExportRowsSupport.ToggleState == ToggleState.On) {
                                    bResultExport = Services.HelperTelerikExport.RunExportToExcel(ref this.objGridView, fileName, this.TxbExportSheet.Text, this.SummariesExportOption(), ExcelMaxRows._65536, this.exportVisualSettings);
                                }
                                break;
                            }
                        case 1: {
                                bResultExport = Services.HelperTelerikExport.RunExportToCsv(ref this.objGridView, fileName, this.SummariesExportOption());
                                break;
                            }
                    }
                    if (!bResultExport) {
                        RadMessageBox.Show("No fue posible exportar la información requerida!");
                    }
                    else if (RadMessageBox.Show("Se exporto correctamente. ¿Quieres abrir el documento?", "Exportar", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                        try {
                            //Process.Start(fileName);
                        }
                        catch (Exception exception) {
                            Exception ex = exception;
                            string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                            RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                    }
                }
                else {
                    RadMessageBox.Show("Por favor, introduzca un nombre de archivo.");
                }
            }
        }

        private SummariesOption SummariesExportOption() {
            SummariesOption summariesExportOption;
            switch (this.CboExportSummaries.SelectedIndex) {
                case 0: {
                        summariesExportOption = SummariesOption.ExportAll;
                        break;
                    }
                case 1: {
                        summariesExportOption = SummariesOption.ExportOnlyTop;
                        break;
                    }
                case 2: {
                        summariesExportOption = SummariesOption.ExportOnlyBottom;
                        break;
                    }
                case 3: {
                        summariesExportOption = SummariesOption.DoNotExport;
                        break;
                    }
                default: {
                        summariesExportOption = SummariesOption.DoNotExport;
                        break;
                    }
            }
            return summariesExportOption;
        }

        private void BttnClose_Click(object sender, EventArgs e) {
            this.Close();
            this.Dispose();
        }

        private void BttnExport_Click(object sender, EventArgs e) {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (this.CboExport.SelectedIndex != 0) {
                saveFileDialog.Filter = "CSV File (*.csv)|*.csv";
            }
            else {
                saveFileDialog.Filter = "Excel (*.xls)|*.xls";
            }
            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                if (!saveFileDialog.FileName.Equals(string.Empty)) {
                    fileName = saveFileDialog.FileName;
                    if (this.CheckExportSettings.ToggleState != ToggleState.On) {
                        this.exportVisualSettings = false;
                    }
                    else {
                        this.exportVisualSettings = true;
                    }
                    if (this.CheckExportHiddenColumns.ToggleState != ToggleState.On) {
                        this.exportHiddenColumns = false;
                    }
                    else {
                        this.exportHiddenColumns = true;
                    }
                    this.Waiting.StartWaiting();
                    this.Waiting.Visible = true;
                    this.consultar.RunWorkerAsync();
                    //WaitingForm.ShowForm();
                    //switch (this.CboExport.SelectedIndex)
                    //{
                    //    case 0:
                    //        {
                    //            if (this.OptExportMaxRows.ToggleState == ToggleState.On)
                    //            {
                    //                bResultExport = Jaeger.Helpers.HelperTelerikExport.RunExportToExcel(ref this.objGridView, fileName, this.TxbExportSheet.Text, this.SummariesExportOption(), ExcelMaxRows._1048576, this.exportVisualSettings, this.exportHiddenColumns, false);
                    //            }
                    //            else if (this.OptExportRowsSupport.ToggleState == ToggleState.On)
                    //            {
                    //                bResultExport = Jaeger.Helpers.HelperTelerikExport.RunExportToExcel(ref this.objGridView, fileName, this.TxbExportSheet.Text, this.SummariesExportOption(), ExcelMaxRows._65536, this.exportVisualSettings, false, false);
                    //            }
                    //            break;
                    //        }
                    //    case 1:
                    //        {
                    //            bResultExport = Jaeger.Helpers.HelperTelerikExport.RunExportToCsv(ref this.objGridView, fileName, this.SummariesExportOption());
                    //            break;
                    //        }
                    //}
                    //WaitingForm.CloseForm();
                    //if (!bResultExport)
                    //{
                    //    RadMessageBox.Show("No fue posible exportar la información requerida!");
                    //}
                    //else if (RadMessageBox.Show("Se exporto correctamente. ¿Quieres abrir el documento?", "Exportar", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                    //{
                    //    try
                    //    {
                    //        Process.Start(fileName);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                    //        RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    //    }
                    //}
                }
                else {
                    RadMessageBox.Show("Por favor, introduzca un nombre de archivo.");
                }
            }
        }

        private void ConsultarDoWork(object sender, DoWorkEventArgs e) {
            switch (this.CboExport.SelectedIndex) {
                case 0: {
                        if (this.OptExportMaxRows.ToggleState == ToggleState.On) {
                            bResultExport = HelperTelerikExport.RunExportToExcel(ref this.objGridView, fileName, this.TxbExportSheet.Text, this.SummariesExportOption(), ExcelMaxRows._1048576, this.exportVisualSettings, this.exportHiddenColumns, this.Hierarchy.Checked);
                        }
                        else if (this.OptExportRowsSupport.ToggleState == ToggleState.On) {
                            bResultExport = HelperTelerikExport.RunExportToExcel(ref this.objGridView, fileName, this.TxbExportSheet.Text, this.SummariesExportOption(), ExcelMaxRows._65536, this.exportVisualSettings, false, this.Hierarchy.Checked);
                        }
                        break;
                    }
                case 1: {
                        bResultExport = HelperTelerikExport.RunExportToCsv(ref this.objGridView, fileName, this.SummariesExportOption());
                        break;
                    }
            }
        }

        private void ConsultarRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (!bResultExport) {
                RadMessageBox.Show("No fue posible exportar la información requerida!");
            }
            else if (RadMessageBox.Show("Se exporto correctamente. ¿Quieres abrir el documento?", "Exportar", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                try {
                    Process.Start(fileName);
                }
                catch (Exception ex) {
                    string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                    RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
            this.Waiting.StopWaiting();
            this.Waiting.Visible = false;
            this.consultar.Dispose();
        }
    }
}
