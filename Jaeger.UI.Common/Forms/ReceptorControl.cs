﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class ReceptorControl : UserControl {
        private bool _ReadOnly = false;

        public ReceptorControl() {
            InitializeComponent();
        }

        private void ReceptorControl_Load(object sender, EventArgs e) {
            this.RFC.TextBoxElement.ToolTipText = "Clave del Registro Federal de Contribuyentes correspondiente al contribuyente";
            this.NumRegIdTrib.TextBoxElement.ToolTipText = "Número de registro de identidad fiscal cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.";
            this.RegimenFiscal.MultiColumnComboBoxElement.TextBoxElement.ToolTipText = "Clave del régimen del contribuyente";
            this.DomicilioFiscal.TextBoxElement.ToolTipText = "Registrar el código postal del domicilio fiscal.";
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set { this._ReadOnly = value; }
        }
    }
}
