﻿using System;
using System.IO;
using Microsoft.Reporting.WinForms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Forms {
    public partial class ReporteComunForm : RadForm {
        private string pathLogo;
        private string base64String;
        private System.Windows.Forms.Button Cerrar = new System.Windows.Forms.Button();
        public ReporteComunForm() {

        }
        public ReporteComunForm(string rfc, string razonSocial) {
            InitializeComponent();
            this.RFC = rfc;
            this.RazonSocial = razonSocial;
        }

        public string RFC {
            get; set;
        }

        public string RazonSocial {
            get; set;
        }

        public string ImagenQR {
            get; set;
        }

        public string PathLogo {
            get {
                return this.pathLogo;
            }
            set {
                this.pathLogo = value;
            }
        }

        public string LogoB64 {
            get; set;
        }

        public object CurrentObject {
            get; set;
        }

        public Stream LoadDefinition {
            set {
                this.Viewer.LocalReport.LoadReportDefinition(value);
            }
        }

        private void ReporteComunView_Load(object sender, EventArgs e) {
            this.Viewer.ReportError += this.Viewer_ReportError;
            if (File.Exists(this.PathLogo)) {
                byte[] _logo = File.ReadAllBytes(this.PathLogo);
                this.LogoB64 = Convert.ToBase64String(_logo);
            } else {
                this.LogoB64 = this.ImageToBase64();
            }
            this.Viewer.LocalReport.SubreportProcessing += this.LocalReport_SubreportProcessing;
            this.Viewer.LocalReport.ShowDetailedSubreportMessages = true;
            this.CancelButton = this.Cerrar;
            this.Cerrar.Click += Cerrar_Click;
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Viewer_ReportError(object sender, ReportErrorEventArgs e) {
            Console.WriteLine("Reporte [Error]: " + e.Exception.Message);
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e) {
            Console.WriteLine(e.ToString());
        }

        public void SetParameter(string nameParameter, string value) {
            this.Viewer.LocalReport.SetParameters(new ReportParameter(nameParameter, value));
        }

        public void SetDataSource(string name, System.Data.DataTable data) {
            this.Viewer.LocalReport.DataSources.Add(new ReportDataSource(name, data));
        }

        public void SetDisplayName(string name) {
            this.Viewer.LocalReport.DisplayName = name;
        }

        public void Procesar() {
            foreach (var item in this.Viewer.LocalReport.GetParameters()) {
                Console.WriteLine(item.Name);
                if (item.Name.ToLower() == "empresa") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter(item.Name, this.RazonSocial));
                } else if (item.Name.ToLower() == "imagenqr") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter(item.Name, this.ImagenQR));
                } else if (item.Name.ToLower() == "rfc") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter(item.Name, this.RFC));
                } else if (item.Name.ToLower() == "direccion") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter(item.Name, ""));
                }
            }

            if (this.LogoB64 == "") {
                this.LogoB64 = this.ImageToBase64();
            }

            try {
                this.Viewer.LocalReport.SetParameters(new ReportParameter("Logotipo", this.LogoB64));
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                this.Viewer.LocalReport.SetParameters(new ReportParameter("Logotipo", this.ImageToBase64()));
            }
        }

        public void Finalizar() {
            this.Viewer.RefreshReport();
            this.Viewer.SetDisplayMode(DisplayMode.PrintLayout);
            this.Viewer.ZoomMode = ZoomMode.Percent;
            this.Viewer.ZoomPercent = 100;
            this.Viewer = null;
        }

        private string ImageToBase64() {
            using (System.Drawing.Image image = Properties.Resources.logo_tipo) {
                using (MemoryStream m = new MemoryStream()) {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }
    }
}
