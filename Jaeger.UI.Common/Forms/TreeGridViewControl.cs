﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.Data;

namespace Jaeger.UI.Common.Forms {
    public partial class TreeGridViewControl : UserControl {
        public TreeGridViewControl() {
            InitializeComponent();
        }

        public event EventHandler<object> IndexChanged;
        public void OnIndexChange(object e) {
            if (this.IndexChanged != null)
                this.IndexChanged(this, e);
        }
        public string ValueMember { get; set; }
        public int NewIndex { get; set; }

        private void TreeGridViewControl_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            //register the custom row behavior
            BaseGridBehavior gridBehavior = this.GridData.GridBehavior as BaseGridBehavior;
            gridBehavior.UnregisterBehavior(typeof(GridViewDataRowInfo));
            gridBehavior.RegisterBehavior(typeof(GridViewDataRowInfo), new CustomGridDataRowBehavior());

            //handle drag and drop events for the grid through the DragDrop service
            RadDragDropService svc = this.GridData.GridViewElement.GetService<RadDragDropService>();
            svc.PreviewDragStart += Svc_PreviewDragStart;
            svc.PreviewDragDrop += Svc_PreviewDragDrop;
            svc.PreviewDragOver += Svc_PreviewDragOver;

            this.TreeData.SelectedNodeChanged += TreeCategorias_SelectedNodeChanged;
            this.TBar.Nuevo.Click += TCategoria_Nuevo_Click;
            this.TBar.Editar.Click += TCategoria_Editar_Click;
            this.TBar.Remover.Click += TCategoria_Remover_Click;
            this.TBar.Filtro.Click += Filtro_Click;
        }

        protected internal void TCategoria_Nuevo_Click(object sender, EventArgs e) {
            
        }

        protected internal void TCategoria_Editar_Click(object sender, EventArgs e) {

        }

        protected internal void TCategoria_Remover_Click(object sender, EventArgs e) {

        }

        private void Filtro_Click(object sender, EventArgs e) {
            this.GridData.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        #region acciones del grid
        //required to initiate drag and drop when grid is in bound mode
        private void Svc_PreviewDragStart(object sender, PreviewDragStartEventArgs e) {
            e.CanStart = true;
        }

        private void Svc_PreviewDragOver(object sender, RadDragOverEventArgs e) {
            if (e.DragInstance is GridDataRowElement) {
                e.CanDrop = e.HitTarget is GridDataRowElement ||
                            e.HitTarget is GridTableElement ||
                            e.HitTarget is GridSummaryRowElement;
            }
        }

        //initiate the move of selected row
        private void Svc_PreviewDragDrop(object sender, RadDropEventArgs e) {
            GridDataRowElement rowElement = e.DragInstance as GridDataRowElement;

            if (rowElement == null) {
                return;
            }
            e.Handled = true;

            RadItem dropTarget = e.HitTarget as RadItem;
            RadGridView targetGrid = dropTarget.ElementTree.Control as RadGridView;
            if (targetGrid == null) {
                return;
            }

            var dragGrid = rowElement.ElementTree.Control as RadGridView;
            if (targetGrid == dragGrid) {
                e.Handled = true;

                GridDataRowElement dropTargetRow = dropTarget as GridDataRowElement;
                int index = dropTargetRow != null ? this.GetTargetRowIndex(dropTargetRow, e.DropLocation) : targetGrid.RowCount;
                GridViewRowInfo rowToDrag = dragGrid.SelectedRows[0];
                this.MoveRows(dragGrid, rowToDrag, index);
            }
        }

        private int GetTargetRowIndex(GridDataRowElement row, Point dropLocation) {
            int halfHeight = row.Size.Height / 2;
            int index = row.RowInfo.Index;
            if (dropLocation.Y > halfHeight) {
                index++;
            }
            return index;
        }

        private void MoveRows(RadGridView dragGrid, GridViewRowInfo dragRow, int index) {
            dragGrid.BeginUpdate();

            GridViewRowInfo row = dragRow;
            if (row is GridViewSummaryRowInfo) {
                return;
            }
            if (dragGrid.DataSource != null && typeof(System.Collections.IList).IsAssignableFrom(dragGrid.DataSource.GetType())) {
                //bound to a list of objects scenario
                var sourceCollection = (System.Collections.IList)dragGrid.DataSource;
                if (row.Index < index) {
                    index--;
                }
                sourceCollection.Remove(row.DataBoundItem);
                sourceCollection.Insert(index, row.DataBoundItem);
                if (row.DataBoundItem != null) {
                    this.OnIndexChange(row.DataBoundItem);
                }
                this.NewIndex = index;
                //var d0 = row.DataBoundItem as ModeloXDetailModel;
                //d0.Secuencia = index;
                //Console.WriteLine("indice = " + index);
            } else {
                throw new ApplicationException("Unhandled Scenario");
            }

            dragGrid.EndUpdate(true);
        }
        #endregion

        private void TreeCategorias_SelectedNodeChanged(object sender, RadTreeViewEventArgs e) {
            if (this.GridData.FilterDescriptors.Count > 0) {
                for (int i = 0; i < this.GridData.FilterDescriptors.Count; i++) {
                    if (this.GridData.FilterDescriptors[i].PropertyName == this.ValueMember) {
                        this.GridData.FilterDescriptors.RemoveAt(i);
                    }
                }
            }
            // en caso de ser nulo
            if (e.Node == null) { return; }
            if ((int)e.Node.Value == 0) {
                return;
            } else {
                Console.WriteLine("Categoría: " + e.Node.Value.ToString());
                FilterDescriptor filter = new FilterDescriptor {
                    PropertyName = this.ValueMember,
                    Operator = FilterOperator.IsEqualTo,
                    Value = e.Node.Value
                };
                this.GridData.FilterDescriptors.Add(filter);
            }
        }

        private void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.LStatus.Text = $"{this.GridData.Rows.Count} Filas.";
        }
    }
}
