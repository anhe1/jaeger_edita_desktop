﻿namespace Jaeger.UI.Common.Forms
{
    partial class Waiting2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitulo = new System.Windows.Forms.Label();
            this.waitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.waitingBarIndicator = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.waitingBar)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitulo
            // 
            this.labelTitulo.Location = new System.Drawing.Point(1, 9);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(238, 13);
            this.labelTitulo.TabIndex = 1;
            this.labelTitulo.Text = "Procesando";
            this.labelTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // waitingBar
            // 
            this.waitingBar.Location = new System.Drawing.Point(12, 25);
            this.waitingBar.Name = "waitingBar";
            this.waitingBar.Size = new System.Drawing.Size(216, 24);
            this.waitingBar.TabIndex = 2;
            this.waitingBar.Text = "radWaitingBar1";
            this.waitingBar.WaitingIndicators.Add(this.waitingBarIndicator);
            this.waitingBar.WaitingSpeed = 80;
            this.waitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // waitingBarIndicator
            // 
            this.waitingBarIndicator.Name = "waitingBarIndicator";
            // 
            // ViewWaiting2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(240, 59);
            this.Controls.Add(this.waitingBar);
            this.Controls.Add(this.labelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewWaiting2Form";
            this.Opacity = 0.9D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Procesando";
            this.Load += new System.EventHandler(this.Waiting2Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.waitingBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTitulo;
        private Telerik.WinControls.UI.RadWaitingBar waitingBar;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement waitingBarIndicator;
    }
}