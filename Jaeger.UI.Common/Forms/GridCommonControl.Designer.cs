﻿namespace Jaeger.UI.Common.Forms {
    partial class GridCommonControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridCommonControl));
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.AutoSuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.HostEjercicio = new Telerik.WinControls.UI.CommandBarHostItem();
            this.lblEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.Periodo = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.lblPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ItemLbl = new Telerik.WinControls.UI.CommandBarLabel();
            this.ItemHost = new Telerik.WinControls.UI.CommandBarHostItem();
            this.CommandBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            this.CommandBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.ImageList = this.Iconos;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(969, 320);
            this.GridData.TabIndex = 3;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.EditorRequired += new Telerik.WinControls.UI.EditorRequiredEventHandler(this.GridData_EditorRequired);
            this.GridData.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridData_CellBeginEdit);
            this.GridData.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellEditorInitialized);
            this.GridData.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellEndEdit);
            this.GridData.CellValidating += new Telerik.WinControls.UI.CellValidatingEventHandler(this.GridData_CellValidating);
            this.GridData.CellValidated += new Telerik.WinControls.UI.CellValidatedEventHandler(this.GridData_CellValidated);
            this.GridData.RowSourceNeeded += new Telerik.WinControls.UI.GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.GridData.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.GridData_CommandCellClick);
            this.GridData.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            this.GridData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridData_KeyDown);
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "xml");
            this.Iconos.Images.SetKeyName(1, "pdf");
            this.Iconos.Images.SetKeyName(2, "UrlFilePdf");
            this.Iconos.Images.SetKeyName(3, "UrlFileXml");
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarExcel});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Image = global::Jaeger.UI.Common.Properties.Resources.xls_16px;
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Text = "Exportar Excel";
            this.ExportarExcel.UseCompatibleTextRendering = false;
            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // AutoSuma
            // 
            this.AutoSuma.DisplayName = "AutoSuma";
            this.AutoSuma.DrawText = true;
            this.AutoSuma.Image = global::Jaeger.UI.Common.Properties.Resources.sigma_16px;
            this.AutoSuma.Name = "AutoSuma";
            this.AutoSuma.Text = "AutoSuma";
            this.AutoSuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Image = global::Jaeger.UI.Common.Properties.Resources.delete_file_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Editar
            // 
            this.Editar.DisplayName = "Editar";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_file_16px;
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // HostEjercicio
            // 
            this.HostEjercicio.DisplayName = "Host: Ejercicio";
            this.HostEjercicio.MaxSize = new System.Drawing.Size(55, 20);
            this.HostEjercicio.MinSize = new System.Drawing.Size(55, 20);
            this.HostEjercicio.Name = "HostEjercicio";
            this.HostEjercicio.Text = "Selecciona";
            // 
            // lblEjercicio
            // 
            this.lblEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.lblEjercicio.Name = "lblEjercicio";
            this.lblEjercicio.Text = "Ejercicio:";
            // 
            // Periodo
            // 
            this.Periodo.AutoCompleteDisplayMember = "Descripcion";
            this.Periodo.AutoCompleteValueMember = "Id";
            this.Periodo.DisplayMember = "Descripcion";
            this.Periodo.DisplayName = "Periodo";
            this.Periodo.DropDownAnimationEnabled = true;
            this.Periodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Periodo.MaxDropDownItems = 0;
            this.Periodo.Name = "Periodo";
            this.Periodo.Text = "";
            this.Periodo.ValueMember = "Id";
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.DisplayName = "Etiqueta: Periodo";
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.Text = "Periodo:";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ItemLbl,
            this.ItemHost,
            this.lblPeriodo,
            this.Periodo,
            this.lblEjercicio,
            this.HostEjercicio,
            this.commandBarSeparator1,
            this.Nuevo,
            this.Editar,
            this.Cancelar,
            this.commandBarSeparator2,
            this.Actualizar,
            this.Filtro,
            this.AutoSuma,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            this.ToolBar.ItemsChanged += new Telerik.WinControls.RadCommandBarBaseItemCollectionItemChangedDelegate(this.ToolBar_ItemsChanged);
            // 
            // ItemLbl
            // 
            this.ItemLbl.DisplayName = "commandBarLabel1";
            this.ItemLbl.Name = "ItemLbl";
            this.ItemLbl.Text = "---";
            this.ItemLbl.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ItemHost
            // 
            this.ItemHost.DisplayName = "commandBarHostItem1";
            this.ItemHost.MinSize = new System.Drawing.Size(200, 0);
            this.ItemHost.Name = "ItemHost";
            this.ItemHost.Text = "host";
            this.ItemHost.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // CommandBarRowElement
            // 
            this.CommandBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement.Name = "CommandBarRowElement";
            this.CommandBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.CommandBarRowElement.Text = "";
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(209, 6);
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(55, 20);
            this.Ejercicio.TabIndex = 3;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CommandBar
            // 
            this.CommandBar.Controls.Add(this.Ejercicio);
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement});
            this.CommandBar.Size = new System.Drawing.Size(969, 30);
            this.CommandBar.TabIndex = 4;
            // 
            // GridCommonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.CommandBar);
            this.Name = "GridCommonControl";
            this.Size = new System.Drawing.Size(969, 350);
            this.Load += new System.EventHandler(this.GridCommonControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            this.CommandBar.ResumeLayout(false);
            this.CommandBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadGridView GridData;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        public Telerik.WinControls.UI.RadMenuItem ExportarExcel;
        public Telerik.WinControls.UI.CommandBarDropDownButton Imprimir;
        public Telerik.WinControls.UI.CommandBarToggleButton AutoSuma;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        public Telerik.WinControls.UI.CommandBarButton Cancelar;
        public Telerik.WinControls.UI.CommandBarButton Editar;
        public Telerik.WinControls.UI.CommandBarDropDownButton Nuevo;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarHostItem HostEjercicio;
        public Telerik.WinControls.UI.CommandBarLabel lblEjercicio;
        public Telerik.WinControls.UI.CommandBarDropDownList Periodo;
        public Telerik.WinControls.UI.CommandBarLabel lblPeriodo;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement;
        private Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        public Telerik.WinControls.UI.CommandBarLabel ItemLbl;
        public Telerik.WinControls.UI.CommandBarHostItem ItemHost;
        public System.Windows.Forms.ImageList Iconos;
    }
}
