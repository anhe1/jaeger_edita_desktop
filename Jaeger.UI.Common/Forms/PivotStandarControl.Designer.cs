﻿namespace Jaeger.UI.Common.Forms {
    partial class PivotStandarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.PivotGrid = new Telerik.WinControls.UI.RadPivotGrid();
            this.TPivot = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.PivotGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // PivotGrid
            // 
            this.PivotGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PivotGrid.Location = new System.Drawing.Point(0, 30);
            this.PivotGrid.Name = "PivotGrid";
            this.PivotGrid.Size = new System.Drawing.Size(844, 392);
            this.PivotGrid.TabIndex = 1;
            // 
            // TPivot
            // 
            this.TPivot.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPivot.Location = new System.Drawing.Point(0, 0);
            this.TPivot.Name = "TPivot";
            this.TPivot.ShowActualizar = true;
            this.TPivot.ShowAutosuma = false;
            this.TPivot.ShowCancelar = false;
            this.TPivot.ShowCerrar = true;
            this.TPivot.ShowEditar = false;
            this.TPivot.ShowEjercicio = true;
            this.TPivot.ShowExportarExcel = true;
            this.TPivot.ShowFiltro = false;
            this.TPivot.ShowHerramientas = true;
            this.TPivot.ShowImprimir = false;
            this.TPivot.ShowItem = false;
            this.TPivot.ShowNuevo = false;
            this.TPivot.ShowPeriodo = false;
            this.TPivot.Size = new System.Drawing.Size(844, 30);
            this.TPivot.TabIndex = 0;
            // 
            // PivotStandarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PivotGrid);
            this.Controls.Add(this.TPivot);
            this.Name = "PivotStandarControl";
            this.Size = new System.Drawing.Size(844, 422);
            this.Load += new System.EventHandler(this.PivotStandarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PivotGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public ToolBarCommonControl TPivot;
        public Telerik.WinControls.UI.RadPivotGrid PivotGrid;
    }
}
