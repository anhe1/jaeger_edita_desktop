﻿using System;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class ToolBarDoctoRelacionadoControl : UserControl {
        public ToolBarDoctoRelacionadoControl() {
            InitializeComponent();
        }

        public event EventHandler<EventArgs> ButtonBuscar_Click;
        public void OnButtonBuscar_Click(object sender, EventArgs e) {
            if (this.ButtonBuscar_Click != null)
                this.ButtonBuscar_Click(sender, e);
        }

        public event EventHandler<EventArgs> ButtonRemover_Click;
        public void OnButtonRemover_Click(object sender, EventArgs e) {
            if (this.ButtonRemover_Click != null)
                this.ButtonRemover_Click(sender, e);
        }
        public event EventHandler<EventArgs> ButtonIncluir_CheckStateChanged;
        public void OnButtonIncluir_CheckStateChanged(object sender, EventArgs e) {
            if (this.ButtonIncluir_CheckStateChanged != null)
                this.ButtonIncluir_CheckStateChanged(this, e);
        }

        private void ToolBarDoctoRelacionadoControl_Load(object sender, EventArgs e) {
            this.ToolBarHostItemIncluir.HostedItem = this.Incluir.ButtonElement;
            this.Buscar.Click += new EventHandler(this.OnButtonBuscar_Click);
            this.Remover.Click += new EventHandler(this.OnButtonRemover_Click);
            this.Incluir.CheckStateChanged += new EventHandler(this.OnButtonIncluir_CheckStateChanged);
        }

        private void Incluir_CheckStateChanged(object sender, EventArgs e) {
            this.TipoRelacion.Enabled = this.Incluir.Checked;
            if (this.Incluir.Checked == false) {
                this.TipoRelacion.SelectedValue = null;
                this.TipoRelacion.SelectedIndex = -1;
            }
            this.Buscar.Enabled = this.TipoRelacion.Enabled;
            this.Remover.Enabled = this.TipoRelacion.Enabled;
            //this.OnButtonIncluir_CheckStateChanged(sender, e);
        }

        public void SetEditable(bool editable) {
            this.Incluir.Enabled = editable;
            this.ToolBarHostItemIncluir.Enabled = editable;
            this.TipoRelacion.SetEditable(editable);
        }
    }
}
