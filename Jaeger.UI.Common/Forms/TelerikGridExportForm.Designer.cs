﻿namespace Jaeger.UI.Common.Forms
{
    partial class TelerikGridExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            this.RadGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.CheckExportHiddenColumns = new Telerik.WinControls.UI.RadCheckBox();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.CboExport = new Telerik.WinControls.UI.RadDropDownList();
            this.TxbExportSheet = new Telerik.WinControls.UI.RadTextBox();
            this._ExportLabelNameSheet = new Telerik.WinControls.UI.RadLabel();
            this.OptExportMaxRows = new Telerik.WinControls.UI.RadRadioButton();
            this.OptExportRowsSupport = new Telerik.WinControls.UI.RadRadioButton();
            this.CboExportSummaries = new Telerik.WinControls.UI.RadDropDownList();
            this.CheckExportSettings = new Telerik.WinControls.UI.RadCheckBox();
            this.BttnClose = new Telerik.WinControls.UI.RadButton();
            this.BttnExport = new Telerik.WinControls.UI.RadButton();
            this.Waiting = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.Hierarchy = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox2)).BeginInit();
            this.RadGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckExportHiddenColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbExportSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ExportLabelNameSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptExportMaxRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptExportRowsSupport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboExportSummaries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckExportSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BttnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BttnExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hierarchy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGroupBox2
            // 
            this.RadGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox2.Controls.Add(this.Hierarchy);
            this.RadGroupBox2.Controls.Add(this.CheckExportHiddenColumns);
            this.RadGroupBox2.Controls.Add(this.RadLabel5);
            this.RadGroupBox2.Controls.Add(this.CboExport);
            this.RadGroupBox2.Controls.Add(this.TxbExportSheet);
            this.RadGroupBox2.Controls.Add(this._ExportLabelNameSheet);
            this.RadGroupBox2.Controls.Add(this.OptExportMaxRows);
            this.RadGroupBox2.Controls.Add(this.OptExportRowsSupport);
            this.RadGroupBox2.Controls.Add(this.CboExportSummaries);
            this.RadGroupBox2.Controls.Add(this.CheckExportSettings);
            this.RadGroupBox2.HeaderText = "Exporta";
            this.RadGroupBox2.Location = new System.Drawing.Point(12, 12);
            this.RadGroupBox2.Name = "RadGroupBox2";
            this.RadGroupBox2.Size = new System.Drawing.Size(292, 262);
            this.RadGroupBox2.TabIndex = 31;
            this.RadGroupBox2.Text = "Exporta";
            // 
            // CheckExportHiddenColumns
            // 
            this.CheckExportHiddenColumns.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CheckExportHiddenColumns.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckExportHiddenColumns.Enabled = false;
            this.CheckExportHiddenColumns.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.CheckExportHiddenColumns.Location = new System.Drawing.Point(24, 74);
            this.CheckExportHiddenColumns.Name = "CheckExportHiddenColumns";
            this.CheckExportHiddenColumns.Size = new System.Drawing.Size(151, 18);
            this.CheckExportHiddenColumns.TabIndex = 26;
            this.CheckExportHiddenColumns.Text = "Exportar columnas ocultas";
            this.CheckExportHiddenColumns.ThemeName = "Office2007Silver";
            this.CheckExportHiddenColumns.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // RadLabel5
            // 
            this.RadLabel5.Location = new System.Drawing.Point(14, 21);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(59, 18);
            this.RadLabel5.TabIndex = 24;
            this.RadLabel5.Text = "Exportar a:";
            // 
            // CboExport
            // 
            radListDataItem1.Text = "Excel";
            radListDataItem2.Text = "CSV";
            this.CboExport.Items.Add(radListDataItem1);
            this.CboExport.Items.Add(radListDataItem2);
            this.CboExport.Location = new System.Drawing.Point(79, 21);
            this.CboExport.Name = "CboExport";
            this.CboExport.Size = new System.Drawing.Size(125, 20);
            this.CboExport.TabIndex = 23;
            // 
            // TxbExportSheet
            // 
            this.TxbExportSheet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TxbExportSheet.ForeColor = System.Drawing.Color.Black;
            this.TxbExportSheet.Location = new System.Drawing.Point(132, 222);
            this.TxbExportSheet.Name = "TxbExportSheet";
            this.TxbExportSheet.Size = new System.Drawing.Size(139, 20);
            this.TxbExportSheet.TabIndex = 20;
            this.TxbExportSheet.TabStop = false;
            this.TxbExportSheet.ThemeName = "Office2007Silver";
            // 
            // _ExportLabelNameSheet
            // 
            this._ExportLabelNameSheet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._ExportLabelNameSheet.ForeColor = System.Drawing.Color.Black;
            this._ExportLabelNameSheet.Location = new System.Drawing.Point(24, 222);
            this._ExportLabelNameSheet.Name = "_ExportLabelNameSheet";
            this._ExportLabelNameSheet.Size = new System.Drawing.Size(102, 18);
            this._ExportLabelNameSheet.TabIndex = 21;
            this._ExportLabelNameSheet.Text = "Nombre de la hoja:";
            // 
            // OptExportMaxRows
            // 
            this.OptExportMaxRows.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OptExportMaxRows.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OptExportMaxRows.Enabled = false;
            this.OptExportMaxRows.ForeColor = System.Drawing.Color.Black;
            this.OptExportMaxRows.Location = new System.Drawing.Point(24, 133);
            this.OptExportMaxRows.Name = "OptExportMaxRows";
            this.OptExportMaxRows.Size = new System.Drawing.Size(163, 18);
            this.OptExportMaxRows.TabIndex = 17;
            this.OptExportMaxRows.Text = "Máximo de filas compatibles";
            this.OptExportMaxRows.ThemeName = "Office2007Silver";
            this.OptExportMaxRows.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // OptExportRowsSupport
            // 
            this.OptExportRowsSupport.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OptExportRowsSupport.Enabled = false;
            this.OptExportRowsSupport.ForeColor = System.Drawing.Color.Black;
            this.OptExportRowsSupport.Location = new System.Drawing.Point(24, 157);
            this.OptExportRowsSupport.Name = "OptExportRowsSupport";
            this.OptExportRowsSupport.Size = new System.Drawing.Size(168, 33);
            this.OptExportRowsSupport.TabIndex = 18;
            this.OptExportRowsSupport.Text = "Máximo de filas compatibles\r\npor las versiones anteriores";
            this.OptExportRowsSupport.ThemeName = "Office2007Silver";
            // 
            // CboExportSummaries
            // 
            this.CboExportSummaries.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CboExportSummaries.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CboExportSummaries.ForeColor = System.Drawing.Color.Black;
            radListDataItem3.Text = "Todos los resúmenes";
            radListDataItem4.Text = "Sólo los mejores resúmenes";
            radListDataItem5.Text = "Solamente los Resúmenes de fondo";
            radListDataItem6.Text = "No exportar resúmenes";
            this.CboExportSummaries.Items.Add(radListDataItem3);
            this.CboExportSummaries.Items.Add(radListDataItem4);
            this.CboExportSummaries.Items.Add(radListDataItem5);
            this.CboExportSummaries.Items.Add(radListDataItem6);
            this.CboExportSummaries.Location = new System.Drawing.Point(24, 196);
            this.CboExportSummaries.Name = "CboExportSummaries";
            this.CboExportSummaries.NullText = "Cómo exportar resúmenes";
            this.CboExportSummaries.Size = new System.Drawing.Size(246, 20);
            this.CboExportSummaries.TabIndex = 19;
            this.CboExportSummaries.ThemeName = "Office2007Silver";
            // 
            // CheckExportSettings
            // 
            this.CheckExportSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CheckExportSettings.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckExportSettings.Enabled = false;
            this.CheckExportSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.CheckExportSettings.Location = new System.Drawing.Point(24, 50);
            this.CheckExportSettings.Name = "CheckExportSettings";
            this.CheckExportSettings.Size = new System.Drawing.Size(110, 18);
            this.CheckExportSettings.TabIndex = 16;
            this.CheckExportSettings.Text = "Exportar formatos";
            this.CheckExportSettings.ThemeName = "Office2007Silver";
            this.CheckExportSettings.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // BttnClose
            // 
            this.BttnClose.Location = new System.Drawing.Point(310, 59);
            this.BttnClose.Name = "BttnClose";
            this.BttnClose.Size = new System.Drawing.Size(110, 24);
            this.BttnClose.TabIndex = 30;
            this.BttnClose.Text = "Cerrar";
            this.BttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // BttnExport
            // 
            this.BttnExport.Location = new System.Drawing.Point(310, 27);
            this.BttnExport.Name = "BttnExport";
            this.BttnExport.Size = new System.Drawing.Size(110, 24);
            this.BttnExport.TabIndex = 32;
            this.BttnExport.Text = "Exportar";
            this.BttnExport.ThemeName = "Office2007Silver";
            this.BttnExport.Click += new System.EventHandler(this.BttnExport_Click);
            // 
            // Waiting
            // 
            this.Waiting.Location = new System.Drawing.Point(310, 89);
            this.Waiting.Name = "Waiting";
            this.Waiting.Size = new System.Drawing.Size(110, 24);
            this.Waiting.TabIndex = 33;
            this.Waiting.Text = "radWaitingBar1";
            this.Waiting.Visible = false;
            this.Waiting.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Waiting.WaitingSpeed = 80;
            this.Waiting.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // Hierarchy
            // 
            this.Hierarchy.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Hierarchy.Enabled = false;
            this.Hierarchy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.Hierarchy.Location = new System.Drawing.Point(24, 98);
            this.Hierarchy.Name = "Hierarchy";
            this.Hierarchy.Size = new System.Drawing.Size(110, 18);
            this.Hierarchy.TabIndex = 27;
            this.Hierarchy.Text = "Exportar templete";
            this.Hierarchy.ThemeName = "Office2007Silver";
            // 
            // TelerikGridExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 286);
            this.Controls.Add(this.Waiting);
            this.Controls.Add(this.RadGroupBox2);
            this.Controls.Add(this.BttnClose);
            this.Controls.Add(this.BttnExport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TelerikGridExportForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Exportar";
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox2)).EndInit();
            this.RadGroupBox2.ResumeLayout(false);
            this.RadGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckExportHiddenColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbExportSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ExportLabelNameSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptExportMaxRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptExportRowsSupport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboExportSummaries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckExportSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BttnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BttnExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hierarchy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal Telerik.WinControls.UI.RadGroupBox RadGroupBox2;
        internal Telerik.WinControls.UI.RadLabel RadLabel5;
        internal Telerik.WinControls.UI.RadDropDownList CboExport;
        private Telerik.WinControls.UI.RadTextBox TxbExportSheet;
        private Telerik.WinControls.UI.RadLabel _ExportLabelNameSheet;
        private Telerik.WinControls.UI.RadRadioButton OptExportMaxRows;
        private Telerik.WinControls.UI.RadRadioButton OptExportRowsSupport;
        private Telerik.WinControls.UI.RadDropDownList CboExportSummaries;
        private Telerik.WinControls.UI.RadCheckBox CheckExportSettings;
        internal Telerik.WinControls.UI.RadButton BttnClose;
        internal Telerik.WinControls.UI.RadButton BttnExport;
        private Telerik.WinControls.UI.RadCheckBox CheckExportHiddenColumns;
        private Telerik.WinControls.UI.RadWaitingBar Waiting;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.RadCheckBox Hierarchy;
    }
}
