﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Common.Forms {
    public partial class GridStandarControl : UserControl {
        #region declaraciones
        public RadContextMenu menuContextual = new RadContextMenu();
        protected internal RadMenuItem GridLayout = new RadMenuItem { Text = "Grid layout" };
        protected internal RadMenuItem LayoutSave = new RadMenuItem { Text = "Guardar" };
        protected internal RadMenuItem LayoutReset = new RadMenuItem { Text = "Reset" };

        public RadMenuItem MultiSeleccion = new RadMenuItem { Text = "Selección múltiple" };
        public RadMenuItem RegistroInactivo = new RadMenuItem { Text = "Mostrar registros inactivos", CheckOnClick = true };
        public RadMenuItem ContextualCopiar = new RadMenuItem { Text = "Copiar" };
        public RadMenuItem ContextEditar = new RadMenuItem { Text = "Editar" };
        public RadMenuItem ContextCancelar = new RadMenuItem { Text = "Cancelar", Visibility = ElementVisibility.Collapsed };
        public RadMenuItem ContextImprimir = new RadMenuItem { Text = "Imprimir" };
        public RadMenuItem ContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected internal UIAction _permisos = new UIAction();
        protected internal bool _ReadOnly = false;

        public UIAction Permisos {
            get { return this._permisos; }
            set {
                if (value != null)
                    this._permisos = value;
                this.Aplicar();
            }
        }

        public UIMenuElement MenuElement {
            set { this._permisos = new UIAction(value.Permisos); }
        }
        #endregion

        public GridStandarControl() {
            InitializeComponent();
        }

        private void GridStandarControl_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;

            this.menuContextual.Items.AddRange(this.ContextualCopiar, this.MultiSeleccion);
            this.ContextualCopiar.Click += MContextual_Copiar_Click;
            this.MultiSeleccion.Click += this.MContextual_Seleccion_Click;
            this.Filtro.Click += this.Filtro_Click;

            this.ExportarExcel.Click += this.ExportarExcel_Click;
            this.ExportarExcel.Enabled = this._permisos.Exportar;

            this.GridLayout.Items.Add(this.LayoutSave);
            this.GridLayout.Items.Add(this.LayoutReset);

            this.Herramientas.Items.Add(this.GridLayout);
            this.Herramientas.Items.Add(this.RegistroInactivo);

            this.LayoutSave.Click += this.LayoutSave_Click;
            this.LayoutReset.Click += this.LayoutReset_Click;
        }

        public virtual void ExportarExcel_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExportForm(this.GridData);
            exportar.ShowDialog(this);
        }

        public virtual void Filtro_Click(object sender, EventArgs e) {
            this.GridData.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        private void LayoutReset_Click(object sender, EventArgs e) {
            if (this.GridData.Removable(ConfigService.Piloto.Clave)) {
                RadMessageBox.Show(this, "Es necesario volver a abrir la vista del formulario", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        private void LayoutSave_Click(object sender, EventArgs e) {
            this.GridData.Salveable(ConfigService.Piloto.Clave);
        }

        public virtual void GridData_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F2) {
                if (this.Editar.Enabled) {
                    this.Editar.PerformClick();
                }
            } else if (e.KeyCode == Keys.F3) {
                if (this.Nuevo.Enabled) {
                    this.Nuevo.PerformClick();
                }
            } else if (e.KeyCode == Keys.F4) {
                if (this.Remover.Enabled) {
                    this.Remover.PerformClick();
                }
            } else if (e.KeyCode == Keys.F5) {
                this.Actualizar.PerformClick();
            } else if (e.KeyCode == Keys.F9) {
                if (this.AutoSuma.Visibility == ElementVisibility.Visible) {
                    this.AutoSuma.PerformClick();
                }
            } else if (e.KeyCode == Keys.F11) {
                this.Filtro.PerformClick();
            }
        }

        public virtual void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.Editar.Enabled = this.GridData.RowCount > 0 && this._permisos.Editar;
            this.Remover.Enabled = this.GridData.RowCount > 0 && this._permisos.Remover;
        }

        public virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {

            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate) {
                    e.ContextMenu = this.menuContextual.DropDown;
                }
            }
        }

        public virtual void MContextual_Copiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        public virtual void MContextual_Seleccion_Click(object sender, EventArgs e) {
            this.MultiSeleccion.IsChecked = !this.MultiSeleccion.IsChecked;
            this.GridData.MultiSelect = this.MultiSeleccion.IsChecked;
        }

        #region metodos publicos
        /// <summary>
        /// obtener lista del filtro
        /// </summary>
        /// <typeparam name="T1">Modelo</typeparam>
        public virtual List<T1> GetFiltro<T1>() where T1 : class, new() {
            var d1 = new List<T1>();
            var s2 = this.GridData.ChildRows.Select(it => it.DataBoundItem as T1);
            return s2.ToList();
        }

        /// <summary>
        /// obtener lista de la seleccion actual
        /// </summary>
        /// <typeparam name="T1">Modelo</typeparam>
        public virtual List<T1> GetSeleccion<T1>() where T1 : class, new() {
            var d1 = new List<T1>();
            var s2 = this.GridData.ChildRows.Where(it => it.IsSelected).Select(it => it.DataBoundItem as T1);
            return s2.ToList();
        }

        /// <summary>
        /// obtener el objeto seleccionado
        /// </summary>
        /// <typeparam name="T1">Modelo</typeparam>
        public virtual T1 GetCurrent<T1>() where T1 : class, new() {
            if (this.GridData.CurrentRow != null) {
                var selected = this.GridData.CurrentRow.DataBoundItem as T1;
                return selected;
            }
            return null;
        }
        #endregion

        protected internal void Aplicar() {
            this.Nuevo.Enabled = this._permisos.Agregar;
            this.Remover.Enabled = this._permisos.Remover;
            this.Editar.Enabled = this._permisos.Editar;
            this.Imagen.Enabled = this._permisos.Editar;
            this.Autorizar.Enabled = this._permisos.Autorizar;
            this.ExportarExcel.Enabled = this._permisos.Exportar;
        }

        #region propiedades
        [Description("Test text displayed in the textbox"), Category("Botones")]
        public string Caption {
            get {
                return this.Titulo.Text;
            }
            set {
                this.Titulo.Text = value;
                if (this.Titulo.Text.Length > 0) {
                    this.Titulo.Visibility = ElementVisibility.Visible;
                    this.Separator1.Visibility = ElementVisibility.Visible;
                } else {
                    this.Titulo.Visibility = ElementVisibility.Collapsed;
                    this.Separator1.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Hostitem"), Category("Botones")]
        public bool ShowItem {
            get {
                return this.ItemLbl.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.ItemLbl.Visibility = ElementVisibility.Visible;
                    this.ItemHost.Visibility = ElementVisibility.Visible;
                    this.Separator2.Visibility = ElementVisibility.Visible;
                } else {
                    this.ItemLbl.Visibility = ElementVisibility.Collapsed;
                    this.ItemHost.Visibility = ElementVisibility.Collapsed;
                    this.Separator2.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Mostrar boton nuevo"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Nuevo.Visibility = ElementVisibility.Visible;
                    this.ContextDuplicar.Visibility = ElementVisibility.Visible;
                } else {
                    this.Nuevo.Visibility = ElementVisibility.Collapsed;
                    this.ContextDuplicar.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Mostrar editar"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Editar.Visibility = ElementVisibility.Visible;
                    this.ContextEditar.Visibility = ElementVisibility.Visible;
                } else {
                    this.Editar.Visibility = ElementVisibility.Collapsed;
                    this.ContextEditar.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Mostrar cancelar"), Category("Botones")]
        public bool ShowRemover {
            get {
                return this.Remover.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Remover.Visibility = ElementVisibility.Visible;
                else
                    this.Remover.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowAutorizar {
            get {
                return this.Autorizar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Autorizar.Visibility = ElementVisibility.Visible;
                else
                    this.Autorizar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar botón imagen"), Category("Botones")]
        public bool ShowImagen {
            get {
                return this.Imagen.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imagen.Visibility = ElementVisibility.Visible;
                else
                    this.Imagen.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowAutosuma {
            get {
                return this.AutoSuma.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.AutoSuma.Visibility = ElementVisibility.Visible;
                else
                    this.AutoSuma.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Imprimir.Visibility = ElementVisibility.Visible;
                    this.ContextImprimir.Visibility = ElementVisibility.Visible;
                } else {
                    this.Imprimir.Visibility = ElementVisibility.Collapsed;
                    this.ContextImprimir.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Mostrar herramientas"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Exportar en formato Excel"), Category("Botones")]
        public bool ShowExportarExcel {
            get {
                return this.ExportarExcel.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.ExportarExcel.Visibility = ElementVisibility.Visible;
                else
                    this.ExportarExcel.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowSeleccionMultiple {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                    this.MultiSeleccion.Visibility = ElementVisibility.Visible;
                } else {
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
                    this.MultiSeleccion.Visibility = ElementVisibility.Visible;
                }
            }
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set {
                this._ReadOnly = value;
                this.SetReadOnly();
            }
        }
        #endregion

        private void SetReadOnly() {
            this.GridData.AllowEditRow = !this._ReadOnly;
            this.Nuevo.Enabled = !this._ReadOnly;
            this.Editar.Enabled = !this._ReadOnly;
            this.Remover.Enabled = !this._ReadOnly;
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            GridDataCellElement cell = e.CellElement as GridDataCellElement;
            if (cell != null) {
                if (cell.ContainsErrors) {
                    cell.DrawBorder = true;
                    cell.BorderBoxStyle = BorderBoxStyle.FourBorders;
                    cell.BorderBottomColor = cell.BorderTopColor = cell.BorderRightShadowColor = cell.BorderLeftShadowColor = Color.Transparent;
                    cell.BorderBottomShadowColor = cell.BorderTopShadowColor = cell.BorderRightColor = cell.BorderLeftColor = Color.Red;
                    cell.BorderBottomWidth = cell.BorderTopWidth = cell.BorderRightWidth = cell.BorderLeftWidth = 1;
                    cell.BorderDrawMode = BorderDrawModes.HorizontalOverVertical;
                    cell.ZIndex = 2;
                } else {
                    cell.ResetValue(LightVisualElement.DrawBorderProperty, ValueResetFlags.Local);
                    cell.ResetValue(LightVisualElement.BorderBoxStyleProperty, ValueResetFlags.Local);
                    cell.ResetValue(LightVisualElement.BorderDrawModeProperty, ValueResetFlags.Local);
                    cell.ResetValue(LightVisualElement.ZIndexProperty, ValueResetFlags.Local);
                }
            }
        }
    }
}
