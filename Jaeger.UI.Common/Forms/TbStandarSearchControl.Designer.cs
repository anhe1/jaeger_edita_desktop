﻿
namespace Jaeger.UI.Common.Forms {
    partial class TbStandarSearchControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowE = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblCaption = new Telerik.WinControls.UI.CommandBarLabel();
            this.Descripcion = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Existencia = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.cExistencia = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cExistencia)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowE});
            this.CommandBar.Size = new System.Drawing.Size(681, 55);
            this.CommandBar.TabIndex = 0;
            // 
            // commandBarRowE
            // 
            this.commandBarRowE.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowE.Name = "commandBarRowE";
            this.commandBarRowE.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblCaption,
            this.Descripcion,
            this.Buscar,
            this.Agregar,
            this.Filtro,
            this.Existencia,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // lblCaption
            // 
            this.lblCaption.DisplayName = "Etiqueta: Producto";
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.lblCaption.Text = "Buscar:";
            // 
            // Descripcion
            // 
            this.Descripcion.DisplayName = "Descripción";
            this.Descripcion.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Descripcion.MaxSize = new System.Drawing.Size(260, 22);
            this.Descripcion.MinSize = new System.Drawing.Size(260, 22);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Descripcion.StretchHorizontally = false;
            this.Descripcion.StretchVertically = false;
            this.Descripcion.Text = "";
            ((Telerik.WinControls.UI.RadTextBoxElement)(this.Descripcion.GetChildAt(0))).Text = "";
            // 
            // Buscar
            // 
            this.Buscar.DisplayName = "Buscar";
            this.Buscar.DrawText = true;
            this.Buscar.Image = global::Jaeger.UI.Common.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Agregar
            // 
            this.Agregar.DisplayName = "commandBarButton1";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Common.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Existencia
            // 
            this.Existencia.DisplayName = "Existencia";
            this.Existencia.Name = "Existencia";
            this.Existencia.Text = "";
            this.Existencia.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // cExistencia
            // 
            this.cExistencia.Location = new System.Drawing.Point(510, 83);
            this.cExistencia.Name = "cExistencia";
            this.cExistencia.Size = new System.Drawing.Size(94, 18);
            this.cExistencia.TabIndex = 1;
            this.cExistencia.Text = "Solo existencia";
            // 
            // TbStandarSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cExistencia);
            this.Controls.Add(this.CommandBar);
            this.Name = "TbStandarSearchControl";
            this.Size = new System.Drawing.Size(681, 30);
            this.Load += new System.EventHandler(this.TbStandarSearchControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cExistencia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowE;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        public Telerik.WinControls.UI.RadCheckBox cExistencia;
        public Telerik.WinControls.UI.CommandBarLabel lblCaption;
        public Telerik.WinControls.UI.CommandBarTextBox Descripcion;
        public Telerik.WinControls.UI.CommandBarHostItem Existencia;
        public Telerik.WinControls.UI.CommandBarButton Buscar;
        public Telerik.WinControls.UI.CommandBarButton Agregar;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
    }
}
