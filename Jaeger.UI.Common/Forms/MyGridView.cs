﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Forms {
    public class MyGridView : RadGridView {
        private bool ShowBajas = false;
        public MyGridView() :base() {
            this.KeyDown += MyGridView_KeyDown;
            this.RowsChanged += MyGridView_RowsChanged;
            this.DataBindingComplete += MyGridView_DataBindingComplete;
            //this.RowFormatting += MyGridView_RowFormatting;
        }

        private void MyGridView_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e) {
            
        }

        private void MyGridView_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            
        }

        private void MyGridView_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
            if (e.Control && e.KeyCode == System.Windows.Forms.Keys.B) {
                this.ShowBajas =!this.ShowBajas;
                for (int i = 0; i < this.Rows.Count; i++) {
                    try {
                        var b = (bool)this.Rows[i].Cells["IsActive"].Value;
                        if (b == false) {
                            this.Rows[i].IsVisible = this.ShowBajas;
                        }
                    } catch (Exception) {

                        throw;
                    }
                }
                this.Refresh();
            }
        }

        private void MyGridView_RowFormatting(object sender, RowFormattingEventArgs e) {
            if (!this.ShowBajas) {
                try {
                    var b = e.RowElement.RowInfo.Cells["IsActive"];
                    if (b != null) {
                        var c =(bool)b.Value;
                        if (c == false) {
                            e.RowElement.RowInfo.IsVisible = this.ShowBajas;
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
             //   Console.WriteLine(e.RowElement.BorderBoxStyle);
            }
        }
    }
}
