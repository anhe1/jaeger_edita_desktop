﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    /// <summary>
    /// Control de Barra de Herramientas comun para documentos
    /// </summary>
    public partial class TbDocumentoControl : UserControl {

        public TbDocumentoControl() {
            InitializeComponent();
        }

        private void ToolBarComprobanteFiscalControl_Load(object sender, EventArgs e) {
            this.Opciones.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.IdDocumento.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.IdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
        }

        public bool ShowGuardar {
            get { return this.Guardar.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value) {
                    this.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowActualizar {
            get { return this.Actualizar.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value) {
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowPDF {
            get { return this.FilePDF.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value) {
                    this.FilePDF.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.FilePDF.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowImprimir {
            get { return this.Imprimir.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value) {
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowEnviar {
            get { return this.SendEmail.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value) {
                    this.SendEmail.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.SendEmail.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowComplemento {
            get { return this.Complemento.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value) {
                    this.Complemento.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.Complemento.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowIdDocumento {
            get { return this.IdDocumento.Visibility == Telerik.WinControls.ElementVisibility.Visible; }
            set {
                if (value == false) {
                    this.IdDocumento.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.LabelUUID.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.Separator3.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                } else {
                    this.IdDocumento.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.LabelUUID.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.Separator3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                }
            }
        }
    }
}
