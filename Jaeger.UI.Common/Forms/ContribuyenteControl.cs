﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class ContribuyenteControl : UserControl {
        public ContribuyenteControl() {
            InitializeComponent();
        }

        private void ContribuyenteControl_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.RFC.MaskedEditBoxElement.TextBoxItem.MaxLength = 14;
            this.Nombre.ShowItemToolTips = true;
            this.Nombre.TextBoxElement.ToolTipText = "Utilizado en facturación";
            this.DomicilioFiscal.TextBoxElement.ToolTipText = "Codigo postal de la dirección fiscal utilizado para facturación.";

            // relaciones comerciales
            this.RelacionComercial.CheckedMember = "Activo";
            this.RelacionComercial.DisplayMember = "Name";
            this.RelacionComercial.ValueMember = "IdTipoRelacion";
        }
    }
}
