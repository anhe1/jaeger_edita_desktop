﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class TbPeriodoControl : UserControl {
        public TbPeriodoControl() {
            InitializeComponent();
        }

        private void TbPeriodoControl_Load(object sender, EventArgs e) {
            var sem = this.PrimerDiaSemana();
            this.FInicial.Value = sem;
            this.FFinal.Value = sem.AddDays(6);
            this.THost1.HostedItem = this.FInicial.DateTimePickerElement;
            this.THost2.HostedItem = this.FFinal.DateTimePickerElement;
        }

        //public int GetMes() {
        //    if (this.ToolBarComboBoxPeriodo.SelectedValue != null) {
        //        return (int)this.ToolBarComboBoxPeriodo.SelectedValue;
        //    } else {
        //        return (int)Enum.Parse(typeof(MesesEnum), this.ToolBarComboBoxPeriodo.Text);
        //    }
        //}

        //public int GetEjercicio() {
        //    return int.Parse(this.SpinEjercicio.Value.ToString());
        //}

        public string FInicialCaption {
            get { return this.FInicialLabel.Text; }
            set { this.FInicialLabel.Text = value; }
        }

        //[Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowFInicial {
            get {
                return this.THost1.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.FInicialLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.THost1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.FInicialLabel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.THost1.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public string FFinalCaption {
            get { return this.FFinalLabel.Text; }
            set { this.FFinalLabel.Text = value; }
        }

        public bool ShowFFinal {
            get {
                return this.FFinalLabel.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.FFinalLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.THost2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.FFinalLabel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.THost2.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        //[Description("Test text displayed in the textbox"), Category("Botones")]
        //public bool ShowNuevo {
        //    get {
        //        return this.Nuevo.Visibility == Telerik.WinControls.ElementVisibility.Visible;
        //    }
        //    set {
        //        if (value == true)
        //            this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        //        else
        //            this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        //    }
        //}

        //[Description("Test text displayed in the textbox"), Category("Botones")]
        //public bool ShowEditar {
        //    get {
        //        return this.Editar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
        //    }
        //    set {
        //        if (value == true)
        //            this.Editar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        //        else
        //            this.Editar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        //    }
        //}

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowDepartamento {
            get {
                return this.Departamento.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.lblDepartamento.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.SeparatorDepartamento.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.Departamento.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.lblDepartamento.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.SeparatorDepartamento.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.Departamento.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        //[Description("Test text displayed in the textbox"), Category("Botones")]
        //public bool ShowFiltro {
        //    get {
        //        return this.Filtro.Visibility == Telerik.WinControls.ElementVisibility.Visible;
        //    }
        //    set {
        //        if (value == true)
        //            this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        //        else
        //            this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        //    }
        //}

        //[Description("Test text displayed in the textbox"), Category("Botones")]
        //public bool ShowAutosuma {
        //    get {
        //        return this.AutoSuma.Visibility == Telerik.WinControls.ElementVisibility.Visible;
        //    }
        //    set {
        //        if (value == true)
        //            this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        //        else
        //            this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        //    }
        //}

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        //[Description("Test text displayed in the textbox"), Category("Botones")]
        //public bool ShowHerramientas {
        //    get {
        //        return this.Herramientas.Visibility == Telerik.WinControls.ElementVisibility.Visible;
        //    }
        //    set {
        //        if (value == true)
        //            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        //        else
        //            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        //    }
        //}

        //[Description("Exportar en formato Excel"), Category("Botones")]
        //public bool ShowExportarExcel {
        //    get {
        //        return this.ExportarExcel.Visibility == Telerik.WinControls.ElementVisibility.Visible;
        //    }
        //    set {
        //        if (value == true)
        //            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
        //        else
        //            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        //    }
        //}

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        /// <summary>
        /// funcion para devolver el primer dia de la semana
        /// </summary>
        private DateTime PrimerDiaSemana() {
            DateTime dt = DateTime.Now.AddDays(-6);
            DateTime wkStDt = dt.AddDays(1 - Convert.ToDouble(dt.DayOfWeek));
            DateTime fechadesdesemana = wkStDt.Date;
            return fechadesdesemana;
        }
    }
}
