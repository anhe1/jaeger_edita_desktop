﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Helpers;

namespace Jaeger.UI.Common.Forms {
    /// <summary>
    /// General purpose progress dialog that can be used to monitor the progress
    /// of IWorker derived classes.
    /// </summary>
    public partial class ProgressDialogForm : RadForm {
        #region Private Variables
        private bool _closed = false;
        private IWorker _worker = null;
        private object _result = null;
        private Exception _error = null;
        #endregion

        public ProgressDialogForm() {
            InitializeComponent();
            this.lblSecondaryMsg.AutoEllipsis = Services.EllipsisFormat.Middle;
            this.lblPrimaryMsg.AutoEllipsis = Services.EllipsisFormat.None;
        }

        #region Public Methods
        /// <summary>
        /// Start the worker object and show the dialog
        /// </summary>
        /// <param name="parent">The parent form</param>
        /// <param name="worker">The worker object to start.</param>
        public void Start(IWin32Window parent, IWorker worker) {
            if (_worker != null)
                throw new ApplicationException("progress dialog was already started");

            _worker = worker;
            _worker.ProgressChanged += new EventHandler<ProgressEventArgs>(Worker_ProgressChanged);

            // Adjust the dialog to display two progress bars or just one according to the
            // type of the worker assigned to it.
            if (worker.SupportsDualProgress) {
                this.Height = 180;
                pbrSecondaryProgress.Visible = true;
                lblSecondaryMsg.Visible = true;
            } else {
                this.Height = 126;
                pbrSecondaryProgress.Visible = false;
                lblSecondaryMsg.Visible = false;
            }

            if (worker.Caption != null)
                this.Text = worker.Caption;

            // en caso de que pueda cancelar la operacion
            if (worker.IsCancelable == false) {
                this.btnCancel.Visible = worker.IsCancelable;
                this.ControlBox = false;
            }

            this.StartPosition = FormStartPosition.CenterParent;
            this.ShowDialog(parent);
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Return the result object of the worker object
        /// </summary>
        public object Result {
            get {
                return _result;
            }
        }

        /// <summary>
        /// Return the error object (in case of an error)
        /// </summary>
        public Exception Error {
            get { return _error; }
        }
        #endregion

        #region Protected Overrided Methods
        protected override void OnShown(EventArgs e) {
            base.OnShown(e);
            _worker.BeginWork();
        }
        #endregion

        #region Event Handlers
        private void btnCancel_Click(object sender, EventArgs e) {
            if (btnCancel.Enabled)
                _worker.Cancel();
        }

        private void ProgressDialog_FormClosing(object sender, FormClosingEventArgs e) {
            if (!_closed)
                e.Cancel = true;
        }

        private void Worker_ProgressChanged(object sender, ProgressEventArgs e) {
            if (_closed)
                return;

            if (InvokeRequired)
                Invoke(new EventHandler<ProgressEventArgs>(Worker_ProgressChanged), sender, e);
            else {
                if (this.IsDisposed)
                    return;

                if (e.Error != null) {
                    if (!(e.Error is UserCancellationException)) {
                        MessageBox.Show(this, e.Error.Message, "Operation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    _worker.ProgressChanged -= new EventHandler<ProgressEventArgs>(Worker_ProgressChanged);
                    _error = e.Error;
                    _result = null;
                    _closed = true;
                    this.Close();
                } else {
                    lblPrimaryMsg.Text = e.Message;
                    pbrPrimaryProgress.Value1 = e.Progress;
                    if (e.NestedProgress != null) {
                        lblSecondaryMsg.Text = e.NestedProgress.Message;
                        pbrSecondaryProgress.Value1 = e.NestedProgress.Progress;
                    } else {
                        if (pbrSecondaryProgress.Value1 != 0) {
                            pbrSecondaryProgress.Value1 = 0;
                            lblSecondaryMsg.Text = string.Empty;
                        }
                    }

                    if (e.IsDone) {
                        _worker.ProgressChanged -= new EventHandler<ProgressEventArgs>(Worker_ProgressChanged);
                        _result = _worker.Result;
                        _closed = true;
                        this.Close();
                    }
                }
            }
        }
        #endregion
    }
}
