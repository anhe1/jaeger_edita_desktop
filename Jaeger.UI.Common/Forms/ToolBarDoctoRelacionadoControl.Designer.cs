﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBarDoctoRelacionadoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.RadCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.Incluir = new Telerik.WinControls.UI.RadCheckBox();
            this.CommandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostItemIncluir = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.TipoRelacion = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.ReceptorRFC = new Telerik.WinControls.UI.CommandBarTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).BeginInit();
            this.RadCommandBar2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Incluir)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar2
            // 
            this.RadCommandBar2.Controls.Add(this.Incluir);
            this.RadCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar2.Name = "RadCommandBar2";
            this.RadCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement2});
            this.RadCommandBar2.Size = new System.Drawing.Size(964, 55);
            this.RadCommandBar2.TabIndex = 2;
            // 
            // Incluir
            // 
            this.Incluir.Location = new System.Drawing.Point(20, 6);
            this.Incluir.Name = "Incluir";
            this.Incluir.Size = new System.Drawing.Size(51, 18);
            this.Incluir.TabIndex = 189;
            this.Incluir.Text = "Incluir";
            this.Incluir.CheckStateChanged += new System.EventHandler(this.Incluir_CheckStateChanged);
            // 
            // CommandBarRowElement2
            // 
            this.CommandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement2.Name = "CommandBarRowElement2";
            this.CommandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.CommandBarRowElement2.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "CommandBarStripElement2";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostItemIncluir,
            this.commandBarSeparator3,
            this.CommandBarLabel1,
            this.TipoRelacion,
            this.CommandBarSeparator1,
            this.Buscar,
            this.Remover,
            this.ReceptorRFC});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = false;
            this.ToolBar.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarHostItemIncluir
            // 
            this.ToolBarHostItemIncluir.DisplayName = "Incluir";
            this.ToolBarHostItemIncluir.MinSize = new System.Drawing.Size(65, 18);
            this.ToolBarHostItemIncluir.Name = "ToolBarHostItemIncluir";
            this.ToolBarHostItemIncluir.Text = "Incluir";
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // CommandBarLabel1
            // 
            this.CommandBarLabel1.DisplayName = "CommandBarLabel1";
            this.CommandBarLabel1.Name = "CommandBarLabel1";
            this.CommandBarLabel1.Text = "Tipo de Relación: ";
            // 
            // TipoRelacion
            // 
            this.TipoRelacion.AutoCompleteDisplayMember = "Descripcion";
            this.TipoRelacion.AutoCompleteValueMember = "Id";
            this.TipoRelacion.AutoSize = true;
            this.TipoRelacion.DisplayMember = "Descripcion";
            this.TipoRelacion.DisplayName = "Tipo de Relación";
            this.TipoRelacion.DropDownAnimationEnabled = true;
            this.TipoRelacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoRelacion.Enabled = false;
            this.TipoRelacion.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.TipoRelacion.MaxDropDownItems = 0;
            this.TipoRelacion.MinSize = new System.Drawing.Size(350, 22);
            this.TipoRelacion.Name = "TipoRelacion";
            this.TipoRelacion.Padding = new System.Windows.Forms.Padding(0);
            this.TipoRelacion.Text = "";
            this.TipoRelacion.ValueMember = "Id";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // Buscar
            // 
            this.Buscar.DisplayName = "Buscar";
            this.Buscar.DrawText = true;
            this.Buscar.Enabled = false;
            this.Buscar.Image = global::Jaeger.UI.Common.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Remover
            // 
            this.Remover.DisplayName = "Remover";
            this.Remover.DrawText = true;
            this.Remover.Enabled = false;
            this.Remover.Image = global::Jaeger.UI.Common.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DisplayName = "Receptor RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Text = "";
            this.ReceptorRFC.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // ToolBarDoctoRelacionadoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RadCommandBar2);
            this.Name = "ToolBarDoctoRelacionadoControl";
            this.Size = new System.Drawing.Size(964, 30);
            this.Load += new System.EventHandler(this.ToolBarDoctoRelacionadoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).EndInit();
            this.RadCommandBar2.ResumeLayout(false);
            this.RadCommandBar2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Incluir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar RadCommandBar2;
        private Telerik.WinControls.UI.RadCheckBox Incluir;
        private Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItemIncluir;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.CommandBarLabel CommandBarLabel1;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.CommandBarButton Buscar;
        internal Telerik.WinControls.UI.CommandBarButton Remover;
        internal Telerik.WinControls.UI.CommandBarTextBox ReceptorRFC;
        public Telerik.WinControls.UI.CommandBarDropDownList TipoRelacion;
    }
}
