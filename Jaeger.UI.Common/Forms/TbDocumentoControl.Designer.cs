﻿namespace Jaeger.UI.Common.Forms {
    partial class TbDocumentoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TbDocumentoControl));
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.Command = new Telerik.WinControls.UI.CommandBarRowElement();
            this.Opciones = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Emisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.LabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.Status = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.FilePDF = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.SendEmail = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Complemento = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Separator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.LabelUUID = new Telerik.WinControls.UI.CommandBarLabel();
            this.IdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.Command});
            this.RadCommandBar1.Size = new System.Drawing.Size(1276, 55);
            this.RadCommandBar1.TabIndex = 4;
            // 
            // Command
            // 
            this.Command.MinSize = new System.Drawing.Size(25, 25);
            this.Command.Name = "Command";
            this.Command.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.Opciones});
            this.Command.Text = "";
            // 
            // Opciones
            // 
            this.Opciones.DisplayName = "Opciones";
            this.Opciones.EnableFocusBorder = false;
            this.Opciones.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Emisor,
            this.Separador,
            this.LabelStatus,
            this.Status,
            this.Nuevo,
            this.Duplicar,
            this.Guardar,
            this.Actualizar,
            this.Cancelar,
            this.FilePDF,
            this.Imprimir,
            this.SendEmail,
            this.Separator2,
            this.Complemento,
            this.Separator3,
            this.LabelUUID,
            this.IdDocumento,
            this.Cerrar});
            this.Opciones.Name = "Opciones";
            // 
            // 
            // 
            this.Opciones.OverflowButton.Enabled = true;
            this.Opciones.ShowHorizontalLine = false;
            this.Opciones.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.Opciones.GetChildAt(2))).Enabled = true;
            // 
            // Emisor
            // 
            this.Emisor.DefaultItem = null;
            this.Emisor.DisplayName = "Emisor";
            this.Emisor.DrawText = true;
            this.Emisor.Image = global::Jaeger.UI.Common.Properties.Resources.administrator_male_16px;
            this.Emisor.Name = "Emisor";
            this.Emisor.Text = "XXXXXXXXXXXXXX";
            this.Emisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // LabelStatus
            // 
            this.LabelStatus.DisplayName = "Etiqueta Status";
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Text = "Status:";
            this.LabelStatus.UseCompatibleTextRendering = false;
            // 
            // Status
            // 
            this.Status.AutoCompleteDisplayMember = "Descripcion";
            this.Status.AutoCompleteValueMember = "Id";
            this.Status.DisplayMember = "Descripcion";
            this.Status.DisplayName = "Descripcion";
            this.Status.DrawImage = false;
            this.Status.DrawText = true;
            this.Status.DropDownAnimationEnabled = true;
            this.Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Status.Image = ((System.Drawing.Image)(resources.GetObject("Status.Image")));
            this.Status.MaxDropDownItems = 0;
            this.Status.Name = "Status";
            this.Status.Text = "";
            this.Status.ValueMember = "Id";
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Common.Properties.Resources.copy_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Duplicar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Enabled = false;
            this.Guardar.Image = global::Jaeger.UI.Common.Properties.Resources.save_16px;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Enabled = false;
            this.Cancelar.Image = global::Jaeger.UI.Common.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // FilePDF
            // 
            this.FilePDF.DisplayName = "Archivo PDF";
            this.FilePDF.DrawText = true;
            this.FilePDF.Enabled = false;
            this.FilePDF.Image = global::Jaeger.UI.Common.Properties.Resources.pdf_16px;
            this.FilePDF.Name = "FilePDF";
            this.FilePDF.Text = "PDF";
            this.FilePDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Archivo XML";
            this.Imprimir.DrawText = true;
            this.Imprimir.Enabled = false;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // SendEmail
            // 
            this.SendEmail.DisplayName = "Correo";
            this.SendEmail.DrawText = true;
            this.SendEmail.Enabled = false;
            this.SendEmail.Image = global::Jaeger.UI.Common.Properties.Resources.email_document_16px;
            this.SendEmail.Name = "SendEmail";
            this.SendEmail.Text = "Envíar";
            this.SendEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Complemento
            // 
            this.Complemento.DisplayName = "Series y Folios";
            this.Complemento.DrawText = true;
            this.Complemento.Enabled = false;
            this.Complemento.Image = global::Jaeger.UI.Common.Properties.Resources.plugin_16px;
            this.Complemento.Name = "Complemento";
            this.Complemento.Text = "Complementos";
            this.Complemento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator3
            // 
            this.Separator3.DisplayName = "CommandBarSeparator2";
            this.Separator3.Name = "Separator3";
            this.Separator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator3.VisibleInOverflowMenu = false;
            // 
            // LabelUUID
            // 
            this.LabelUUID.DisplayName = "UUID";
            this.LabelUUID.Name = "LabelUUID";
            this.LabelUUID.Text = "UUID:";
            this.LabelUUID.VisibleInOverflowMenu = false;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DisplayName = "IdDocumento";
            this.IdDocumento.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.IdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Text = "";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbDocumentoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RadCommandBar1);
            this.Name = "TbDocumentoControl";
            this.Size = new System.Drawing.Size(1276, 30);
            this.Load += new System.EventHandler(this.ToolBarComprobanteFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement Command;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel LabelStatus;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator3;
        internal Telerik.WinControls.UI.CommandBarLabel LabelUUID;
        public Telerik.WinControls.UI.CommandBarSplitButton Emisor;
        public Telerik.WinControls.UI.CommandBarDropDownList Status;
        public Telerik.WinControls.UI.CommandBarButton Nuevo;
        public Telerik.WinControls.UI.CommandBarButton Duplicar;
        public Telerik.WinControls.UI.CommandBarButton Guardar;
        public Telerik.WinControls.UI.CommandBarButton Cancelar;
        public Telerik.WinControls.UI.CommandBarButton FilePDF;
        public Telerik.WinControls.UI.CommandBarButton Imprimir;
        public Telerik.WinControls.UI.CommandBarButton SendEmail;
        public Telerik.WinControls.UI.CommandBarDropDownButton Complemento;
        public Telerik.WinControls.UI.CommandBarTextBox IdDocumento;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        public Telerik.WinControls.UI.CommandBarStripElement Opciones;
    }
}
