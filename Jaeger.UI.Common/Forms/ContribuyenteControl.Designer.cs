﻿namespace Jaeger.UI.Common.Forms {
    partial class ContribuyenteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Extranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.Regimen = new Telerik.WinControls.UI.RadDropDownList();
            this.lblRegimen = new Telerik.WinControls.UI.RadLabel();
            this.RelacionComercial = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.Clave = new Telerik.WinControls.UI.RadTextBox();
            this.lblClave = new Telerik.WinControls.UI.RadLabel();
            this.lblObservaciones = new Telerik.WinControls.UI.RadLabel();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.lblResidenciaFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblCURP = new Telerik.WinControls.UI.RadLabel();
            this.ResidenciaFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CURP = new Telerik.WinControls.UI.RadTextBox();
            this.lblRegistroFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblRazonSocial = new Telerik.WinControls.UI.RadLabel();
            this.lblUsoCFDI = new Telerik.WinControls.UI.RadLabel();
            this.Nombre = new Telerik.WinControls.UI.RadTextBox();
            this.lblRelacionComercial = new Telerik.WinControls.UI.RadLabel();
            this.RegimenFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCorreo = new Telerik.WinControls.UI.RadLabel();
            this.Telefono = new Telerik.WinControls.UI.RadTextBox();
            this.lblSitio = new Telerik.WinControls.UI.RadLabel();
            this.Sitio = new Telerik.WinControls.UI.RadTextBox();
            this.lblTelefono = new Telerik.WinControls.UI.RadLabel();
            this.Correo = new Telerik.WinControls.UI.RadTextBox();
            this.UsoCFDI = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.NumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.lblRegimenFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblDomicilioFiscal = new Telerik.WinControls.UI.RadLabel();
            this.DomicilioFiscal = new Telerik.WinControls.UI.RadTextBox();
            this.RFC = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.NombreComercial = new Telerik.WinControls.UI.RadTextBox();
            this.lblNombreComercial = new Telerik.WinControls.UI.RadLabel();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Extranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Regimen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelacionComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObservaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistroFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRazonSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRelacionComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSitio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sitio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Correo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilioFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DomicilioFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            this.SuspendLayout();
            // 
            // Extranjero
            // 
            this.Extranjero.Location = new System.Drawing.Point(605, 46);
            this.Extranjero.Name = "Extranjero";
            this.Extranjero.Size = new System.Drawing.Size(70, 18);
            this.Extranjero.TabIndex = 12;
            this.Extranjero.Text = "Extranjero";
            // 
            // Regimen
            // 
            this.Regimen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem4.Text = "NoDefinido";
            radListDataItem5.Text = "Fisica";
            radListDataItem6.Text = "Moral";
            this.Regimen.Items.Add(radListDataItem4);
            this.Regimen.Items.Add(radListDataItem5);
            this.Regimen.Items.Add(radListDataItem6);
            this.Regimen.Location = new System.Drawing.Point(410, 21);
            this.Regimen.Name = "Regimen";
            this.Regimen.NullText = "Selecciona";
            this.Regimen.Size = new System.Drawing.Size(118, 20);
            this.Regimen.TabIndex = 7;
            // 
            // lblRegimen
            // 
            this.lblRegimen.Location = new System.Drawing.Point(418, 0);
            this.lblRegimen.Name = "lblRegimen";
            this.lblRegimen.Size = new System.Drawing.Size(50, 18);
            this.lblRegimen.TabIndex = 6;
            this.lblRegimen.Text = "Régimen";
            // 
            // RelacionComercial
            // 
            this.RelacionComercial.Location = new System.Drawing.Point(509, 93);
            this.RelacionComercial.Name = "RelacionComercial";
            this.RelacionComercial.NullText = "Relación Comercial";
            this.RelacionComercial.Size = new System.Drawing.Size(166, 20);
            this.RelacionComercial.TabIndex = 18;
            // 
            // Clave
            // 
            this.Clave.Location = new System.Drawing.Point(38, 21);
            this.Clave.Name = "Clave";
            this.Clave.NullText = "Clave";
            this.Clave.Size = new System.Drawing.Size(83, 20);
            this.Clave.TabIndex = 1;
            this.Clave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblClave
            // 
            this.lblClave.Location = new System.Drawing.Point(38, 0);
            this.lblClave.Name = "lblClave";
            this.lblClave.Size = new System.Drawing.Size(35, 18);
            this.lblClave.TabIndex = 0;
            this.lblClave.Text = "Clave:";
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.Location = new System.Drawing.Point(3, 166);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(81, 18);
            this.lblObservaciones.TabIndex = 29;
            this.lblObservaciones.Text = "Observaciones:";
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(127, 0);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(131, 18);
            this.lblRFC.TabIndex = 2;
            this.lblRFC.Text = "Reg. Fed. Contribuyentes";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(90, 165);
            this.Nota.MaxLength = 250;
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(326, 20);
            this.Nota.TabIndex = 30;
            // 
            // lblResidenciaFiscal
            // 
            this.lblResidenciaFiscal.Location = new System.Drawing.Point(422, 118);
            this.lblResidenciaFiscal.Name = "lblResidenciaFiscal";
            this.lblResidenciaFiscal.Size = new System.Drawing.Size(68, 18);
            this.lblResidenciaFiscal.TabIndex = 21;
            this.lblResidenciaFiscal.Text = "Resid. Fiscal:";
            // 
            // lblCURP
            // 
            this.lblCURP.Location = new System.Drawing.Point(272, 0);
            this.lblCURP.Name = "lblCURP";
            this.lblCURP.Size = new System.Drawing.Size(132, 18);
            this.lblCURP.TabIndex = 4;
            this.lblCURP.Text = "Clv. Única Reg. Población";
            // 
            // ResidenciaFiscal
            // 
            // 
            // ResidenciaFiscal.NestedRadGridView
            // 
            this.ResidenciaFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ResidenciaFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResidenciaFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ResidenciaFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ResidenciaFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.ResidenciaFiscal.EditorControl.Name = "NestedRadGridView";
            this.ResidenciaFiscal.EditorControl.ReadOnly = true;
            this.ResidenciaFiscal.EditorControl.ShowGroupPanel = false;
            this.ResidenciaFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ResidenciaFiscal.EditorControl.TabIndex = 0;
            this.ResidenciaFiscal.Location = new System.Drawing.Point(496, 117);
            this.ResidenciaFiscal.Name = "ResidenciaFiscal";
            this.ResidenciaFiscal.Size = new System.Drawing.Size(179, 20);
            this.ResidenciaFiscal.TabIndex = 22;
            this.ResidenciaFiscal.TabStop = false;
            // 
            // CURP
            // 
            this.CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CURP.Location = new System.Drawing.Point(272, 21);
            this.CURP.Name = "CURP";
            this.CURP.NullText = "CURP";
            this.CURP.Size = new System.Drawing.Size(132, 20);
            this.CURP.TabIndex = 5;
            this.CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRegistroFiscal
            // 
            this.lblRegistroFiscal.Location = new System.Drawing.Point(534, 0);
            this.lblRegistroFiscal.Name = "lblRegistroFiscal";
            this.lblRegistroFiscal.Size = new System.Drawing.Size(106, 18);
            this.lblRegistroFiscal.TabIndex = 8;
            this.lblRegistroFiscal.Text = "Núm. Reg. Id. Fiscal:";
            // 
            // lblRazonSocial
            // 
            this.lblRazonSocial.Location = new System.Drawing.Point(3, 46);
            this.lblRazonSocial.Name = "lblRazonSocial";
            this.lblRazonSocial.Size = new System.Drawing.Size(72, 18);
            this.lblRazonSocial.TabIndex = 10;
            this.lblRazonSocial.Text = "Razón Social:";
            // 
            // lblUsoCFDI
            // 
            this.lblUsoCFDI.Location = new System.Drawing.Point(3, 118);
            this.lblUsoCFDI.Name = "lblUsoCFDI";
            this.lblUsoCFDI.Size = new System.Drawing.Size(70, 18);
            this.lblUsoCFDI.TabIndex = 19;
            this.lblUsoCFDI.Text = "Uso de CFDI:";
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(81, 45);
            this.Nombre.MaxLength = 200;
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Identidad Fiscal";
            this.Nombre.Size = new System.Drawing.Size(518, 20);
            this.Nombre.TabIndex = 11;
            // 
            // lblRelacionComercial
            // 
            this.lblRelacionComercial.Location = new System.Drawing.Point(424, 94);
            this.lblRelacionComercial.Name = "lblRelacionComercial";
            this.lblRelacionComercial.Size = new System.Drawing.Size(79, 18);
            this.lblRelacionComercial.TabIndex = 17;
            this.lblRelacionComercial.Text = "Rel. Comercial:";
            // 
            // RegimenFiscal
            // 
            // 
            // RegimenFiscal.NestedRadGridView
            // 
            this.RegimenFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.RegimenFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegimenFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RegimenFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Clave";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Descripcion";
            gridViewTextBoxColumn9.FieldName = "Descriptor";
            gridViewTextBoxColumn9.HeaderText = "Descriptor";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Descriptor";
            gridViewTextBoxColumn9.VisibleInColumnChooser = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.RegimenFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.RegimenFiscal.EditorControl.Name = "NestedRadGridView";
            this.RegimenFiscal.EditorControl.ReadOnly = true;
            this.RegimenFiscal.EditorControl.ShowGroupPanel = false;
            this.RegimenFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.RegimenFiscal.EditorControl.TabIndex = 0;
            this.RegimenFiscal.Location = new System.Drawing.Point(93, 93);
            this.RegimenFiscal.Name = "RegimenFiscal";
            this.RegimenFiscal.NullText = "Régimen Fiscal";
            this.RegimenFiscal.Size = new System.Drawing.Size(323, 20);
            this.RegimenFiscal.TabIndex = 16;
            this.RegimenFiscal.TabStop = false;
            // 
            // lblCorreo
            // 
            this.lblCorreo.Location = new System.Drawing.Point(422, 166);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(43, 18);
            this.lblCorreo.TabIndex = 31;
            this.lblCorreo.Text = "Correo:";
            // 
            // Telefono
            // 
            this.Telefono.Location = new System.Drawing.Point(59, 141);
            this.Telefono.MaxLength = 40;
            this.Telefono.Name = "Telefono";
            this.Telefono.NullText = "Teléfono";
            this.Telefono.Size = new System.Drawing.Size(114, 20);
            this.Telefono.TabIndex = 24;
            // 
            // lblSitio
            // 
            this.lblSitio.Location = new System.Drawing.Point(179, 142);
            this.lblSitio.Name = "lblSitio";
            this.lblSitio.Size = new System.Drawing.Size(31, 18);
            this.lblSitio.TabIndex = 25;
            this.lblSitio.Text = "Sitio:";
            // 
            // Sitio
            // 
            this.Sitio.Location = new System.Drawing.Point(216, 141);
            this.Sitio.Name = "Sitio";
            this.Sitio.NullText = "Sitio WEB";
            this.Sitio.Size = new System.Drawing.Size(249, 20);
            this.Sitio.TabIndex = 26;
            // 
            // lblTelefono
            // 
            this.lblTelefono.Location = new System.Drawing.Point(3, 142);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 18);
            this.lblTelefono.TabIndex = 23;
            this.lblTelefono.Text = "Teléfono:";
            // 
            // Correo
            // 
            this.Correo.Location = new System.Drawing.Point(471, 165);
            this.Correo.MaxLength = 100;
            this.Correo.Name = "Correo";
            this.Correo.NullText = "Correo Electrónico";
            this.Correo.Size = new System.Drawing.Size(204, 20);
            this.Correo.TabIndex = 32;
            // 
            // UsoCFDI
            // 
            // 
            // UsoCFDI.NestedRadGridView
            // 
            this.UsoCFDI.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.UsoCFDI.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsoCFDI.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UsoCFDI.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.UsoCFDI.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn10.FieldName = "Clave";
            gridViewTextBoxColumn10.HeaderText = "Clave";
            gridViewTextBoxColumn10.Name = "Clave";
            gridViewTextBoxColumn11.FieldName = "Descripcion";
            gridViewTextBoxColumn11.HeaderText = "Descripción";
            gridViewTextBoxColumn11.Name = "Descripcion";
            gridViewTextBoxColumn12.FieldName = "Descriptor";
            gridViewTextBoxColumn12.HeaderText = "Descriptor";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Descriptor";
            gridViewTextBoxColumn12.VisibleInColumnChooser = false;
            this.UsoCFDI.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.UsoCFDI.EditorControl.MasterTemplate.EnableGrouping = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.UsoCFDI.EditorControl.Name = "NestedRadGridView";
            this.UsoCFDI.EditorControl.ReadOnly = true;
            this.UsoCFDI.EditorControl.ShowGroupPanel = false;
            this.UsoCFDI.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.UsoCFDI.EditorControl.TabIndex = 0;
            this.UsoCFDI.Location = new System.Drawing.Point(79, 117);
            this.UsoCFDI.Name = "UsoCFDI";
            this.UsoCFDI.NullText = "Uso de CFDI";
            this.UsoCFDI.Size = new System.Drawing.Size(337, 20);
            this.UsoCFDI.TabIndex = 20;
            this.UsoCFDI.TabStop = false;
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.Location = new System.Drawing.Point(534, 21);
            this.NumRegIdTrib.MaxLength = 60;
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Size = new System.Drawing.Size(141, 20);
            this.NumRegIdTrib.TabIndex = 9;
            // 
            // lblRegimenFiscal
            // 
            this.lblRegimenFiscal.Location = new System.Drawing.Point(3, 94);
            this.lblRegimenFiscal.Name = "lblRegimenFiscal";
            this.lblRegimenFiscal.Size = new System.Drawing.Size(83, 18);
            this.lblRegimenFiscal.TabIndex = 15;
            this.lblRegimenFiscal.Text = "Régimen Fiscal:";
            // 
            // lblDomicilioFiscal
            // 
            this.lblDomicilioFiscal.Location = new System.Drawing.Point(475, 142);
            this.lblDomicilioFiscal.Name = "lblDomicilioFiscal";
            this.lblDomicilioFiscal.Size = new System.Drawing.Size(86, 18);
            this.lblDomicilioFiscal.TabIndex = 27;
            this.lblDomicilioFiscal.Text = "Domicilio Fiscal:";
            // 
            // DomicilioFiscal
            // 
            this.DomicilioFiscal.Location = new System.Drawing.Point(567, 141);
            this.DomicilioFiscal.MaxLength = 5;
            this.DomicilioFiscal.Name = "DomicilioFiscal";
            this.DomicilioFiscal.NullText = "C. Postal";
            this.DomicilioFiscal.Size = new System.Drawing.Size(108, 20);
            this.DomicilioFiscal.TabIndex = 28;
            // 
            // RFC
            // 
            this.RFC.BackColor = System.Drawing.SystemColors.Window;
            this.RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RFC.Location = new System.Drawing.Point(127, 21);
            this.RFC.MaskType = Telerik.WinControls.UI.MaskType.Regex;
            this.RFC.Name = "RFC";
            this.RFC.NullText = "RFC";
            this.RFC.Size = new System.Drawing.Size(120, 20);
            this.RFC.TabIndex = 3;
            this.RFC.TabStop = false;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // NombreComercial
            // 
            this.NombreComercial.Location = new System.Drawing.Point(110, 69);
            this.NombreComercial.MaxLength = 200;
            this.NombreComercial.Name = "NombreComercial";
            this.NombreComercial.NullText = "Nombre comercial";
            this.NombreComercial.Size = new System.Drawing.Size(565, 20);
            this.NombreComercial.TabIndex = 14;
            // 
            // lblNombreComercial
            // 
            this.lblNombreComercial.Location = new System.Drawing.Point(3, 70);
            this.lblNombreComercial.Name = "lblNombreComercial";
            this.lblNombreComercial.Size = new System.Drawing.Size(101, 18);
            this.lblNombreComercial.TabIndex = 13;
            this.lblNombreComercial.Text = "Nombre comercial:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Buscar.Enabled = false;
            this.Buscar.Image = global::Jaeger.UI.Common.Properties.Resources.search_16px;
            this.Buscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Buscar.Location = new System.Drawing.Point(246, 21);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(20, 20);
            this.Buscar.TabIndex = 383;
            // 
            // ContribuyenteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.lblNombreComercial);
            this.Controls.Add(this.NombreComercial);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.lblDomicilioFiscal);
            this.Controls.Add(this.DomicilioFiscal);
            this.Controls.Add(this.Extranjero);
            this.Controls.Add(this.Regimen);
            this.Controls.Add(this.lblRegimen);
            this.Controls.Add(this.RelacionComercial);
            this.Controls.Add(this.Clave);
            this.Controls.Add(this.lblClave);
            this.Controls.Add(this.lblObservaciones);
            this.Controls.Add(this.lblRFC);
            this.Controls.Add(this.Nota);
            this.Controls.Add(this.lblResidenciaFiscal);
            this.Controls.Add(this.lblCURP);
            this.Controls.Add(this.ResidenciaFiscal);
            this.Controls.Add(this.CURP);
            this.Controls.Add(this.lblRegistroFiscal);
            this.Controls.Add(this.lblRazonSocial);
            this.Controls.Add(this.lblUsoCFDI);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.lblRelacionComercial);
            this.Controls.Add(this.RegimenFiscal);
            this.Controls.Add(this.lblCorreo);
            this.Controls.Add(this.Telefono);
            this.Controls.Add(this.lblSitio);
            this.Controls.Add(this.Sitio);
            this.Controls.Add(this.lblTelefono);
            this.Controls.Add(this.Correo);
            this.Controls.Add(this.UsoCFDI);
            this.Controls.Add(this.NumRegIdTrib);
            this.Controls.Add(this.lblRegimenFiscal);
            this.MinimumSize = new System.Drawing.Size(680, 186);
            this.Name = "ContribuyenteControl";
            this.Size = new System.Drawing.Size(680, 186);
            this.Load += new System.EventHandler(this.ContribuyenteControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Extranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Regimen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelacionComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObservaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistroFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRazonSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRelacionComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSitio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sitio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Correo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilioFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DomicilioFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadLabel lblRegimen;
        public Telerik.WinControls.UI.RadLabel lblClave;
        public Telerik.WinControls.UI.RadLabel lblObservaciones;
        public Telerik.WinControls.UI.RadLabel lblRFC;
        public Telerik.WinControls.UI.RadLabel lblResidenciaFiscal;
        public Telerik.WinControls.UI.RadLabel lblCURP;
        public Telerik.WinControls.UI.RadLabel lblRegistroFiscal;
        public Telerik.WinControls.UI.RadLabel lblRazonSocial;
        public Telerik.WinControls.UI.RadLabel lblUsoCFDI;
        public Telerik.WinControls.UI.RadLabel lblRelacionComercial;
        public Telerik.WinControls.UI.RadLabel lblCorreo;
        public Telerik.WinControls.UI.RadLabel lblSitio;
        public Telerik.WinControls.UI.RadLabel lblTelefono;
        public Telerik.WinControls.UI.RadLabel lblRegimenFiscal;
        public Telerik.WinControls.UI.RadDropDownList Regimen;
        public Telerik.WinControls.UI.RadCheckedDropDownList RelacionComercial;
        public Telerik.WinControls.UI.RadTextBox Clave;
        public Telerik.WinControls.UI.RadTextBox Nota;
        public Telerik.WinControls.UI.RadMultiColumnComboBox ResidenciaFiscal;
        public Telerik.WinControls.UI.RadTextBox CURP;
        public Telerik.WinControls.UI.RadTextBox Nombre;
        public Telerik.WinControls.UI.RadMultiColumnComboBox RegimenFiscal;
        public Telerik.WinControls.UI.RadTextBox Telefono;
        public Telerik.WinControls.UI.RadTextBox Sitio;
        public Telerik.WinControls.UI.RadTextBox Correo;
        public Telerik.WinControls.UI.RadMultiColumnComboBox UsoCFDI;
        public Telerik.WinControls.UI.RadTextBox NumRegIdTrib;
        public Telerik.WinControls.UI.RadCheckBox Extranjero;
        public Telerik.WinControls.UI.RadLabel lblDomicilioFiscal;
        public Telerik.WinControls.UI.RadTextBox DomicilioFiscal;
        public Telerik.WinControls.UI.RadMaskedEditBox RFC;
        public Telerik.WinControls.UI.RadLabel lblNombreComercial;
        public Telerik.WinControls.UI.RadTextBox NombreComercial;
        public System.Windows.Forms.ErrorProvider errorProvider1;
        public Telerik.WinControls.UI.RadButton Buscar;
    }
}
