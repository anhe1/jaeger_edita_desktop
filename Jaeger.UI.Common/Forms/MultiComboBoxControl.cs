﻿using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Forms {
    public class MultiComboBoxControl : RadMultiColumnComboBox {
        protected internal bool _Enabled = false;
        public MultiComboBoxControl() : base() {
            ChangeEnabled();
        }

        public bool ReadOnly {
            get { return _Enabled; }
            set {
                _Enabled = value;
                ChangeEnabled();
            }
        }

        protected internal void ChangeEnabled() {
            if (!_Enabled) {
                DropDownOpening -= EditorControl_DropDownOpening;
                EditorControl.CurrentRowChanging -= EditorControl_CurrentRowChanging;
                MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = false;
                MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;
            } else {
                DropDownOpening += EditorControl_DropDownOpening;
                EditorControl.CurrentRowChanging += EditorControl_CurrentRowChanging;
                MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            }
        }

        public void EditorControl_DropDownOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        private void EditorControl_CurrentRowChanging(object sender, CurrentRowChangingEventArgs e) {
            e.Cancel = true;
        }
    }

}
