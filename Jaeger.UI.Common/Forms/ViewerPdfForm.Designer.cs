﻿namespace Jaeger.UI.Common.Forms
{
    partial class ViewerPdfForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NavegadorPDF = new Telerik.WinControls.UI.RadPdfViewerNavigator();
            this.PdfViewer = new Telerik.WinControls.UI.RadPdfViewer();
            ((System.ComponentModel.ISupportInitialize)(this.NavegadorPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PdfViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // NavegadorPDF
            // 
            this.NavegadorPDF.Dock = System.Windows.Forms.DockStyle.Top;
            this.NavegadorPDF.Location = new System.Drawing.Point(0, 0);
            this.NavegadorPDF.Name = "NavegadorPDF";
            this.NavegadorPDF.Size = new System.Drawing.Size(1092, 38);
            this.NavegadorPDF.TabIndex = 0;
            // 
            // PdfViewer
            // 
            this.PdfViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PdfViewer.Location = new System.Drawing.Point(0, 38);
            this.PdfViewer.Name = "PdfViewer";
            this.PdfViewer.Size = new System.Drawing.Size(1092, 399);
            this.PdfViewer.TabIndex = 1;
            this.PdfViewer.ThumbnailsScaleFactor = 0.15F;
            // 
            // ViewerPdf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 437);
            this.Controls.Add(this.PdfViewer);
            this.Controls.Add(this.NavegadorPDF);
            this.Name = "ViewerPdf";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "PDF:";
            this.Load += new System.EventHandler(this.ViewerPdf_Load);
            this.FormClosed += ViewerPdf_FormClosed;
            ((System.ComponentModel.ISupportInitialize)(this.NavegadorPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PdfViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private Telerik.WinControls.UI.RadPdfViewerNavigator NavegadorPDF;
        private Telerik.WinControls.UI.RadPdfViewer PdfViewer;
    }
}
