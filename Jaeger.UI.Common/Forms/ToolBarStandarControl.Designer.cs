﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBarStandarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.commandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Titulo = new Telerik.WinControls.UI.CommandBarLabel();
            this.BarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.Autorizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imagen = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.BarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.commandBar)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBar
            // 
            this.commandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.commandBar.Location = new System.Drawing.Point(0, 0);
            this.commandBar.Name = "commandBar";
            this.commandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.commandBar.Size = new System.Drawing.Size(810, 55);
            this.commandBar.TabIndex = 1;
            // 
            // BarRowElement
            // 
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.BarRowElement.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Titulo,
            this.BarSeparator1,
            this.Nuevo,
            this.Editar,
            this.Remover,
            this.Autorizar,
            this.Imagen,
            this.Guardar,
            this.BarSeparator2,
            this.Actualizar,
            this.Imprimir,
            this.Filtro,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            this.ToolBar.Text = "";
            this.ToolBar.ItemsChanged += new Telerik.WinControls.RadCommandBarBaseItemCollectionItemChangedDelegate(this.ToolBar_ItemsChanged);
            this.ToolBar.VisibleInCommandBarChanged += new System.EventHandler(this.ToolBar_VisibleInCommandBarChanged);
            this.ToolBar.ItemVisibleInStripChanged += new System.EventHandler(this.ToolBar_ItemVisibleInStripChanged);
            // 
            // Titulo
            // 
            this.Titulo.DisplayName = "Titulo";
            this.Titulo.Name = "Titulo";
            this.Titulo.Text = "";
            this.Titulo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // BarSeparator1
            // 
            this.BarSeparator1.DisplayName = "Separador";
            this.BarSeparator1.MinSize = new System.Drawing.Size(2, 0);
            this.BarSeparator1.Name = "BarSeparator1";
            this.BarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.BarSeparator1.VisibleInOverflowMenu = false;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.add_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Editar
            // 
            this.Editar.DisplayName = "Editar";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_16px;
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Remover
            // 
            this.Remover.DisplayName = "Remover";
            this.Remover.DrawText = true;
            this.Remover.Image = global::Jaeger.UI.Common.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Autorizar
            // 
            this.Autorizar.DisplayName = "Autorizar";
            this.Autorizar.DrawText = true;
            this.Autorizar.Image = global::Jaeger.UI.Common.Properties.Resources.checkmark_16px;
            this.Autorizar.Name = "Autorizar";
            this.Autorizar.Text = "Autorizar";
            this.Autorizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Autorizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Imagen
            // 
            this.Imagen.DisplayName = "Imagen";
            this.Imagen.DrawText = true;
            this.Imagen.Image = global::Jaeger.UI.Common.Properties.Resources.add_image_16px;
            this.Imagen.Name = "Imagen";
            this.Imagen.Text = "Imagen";
            this.Imagen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imagen.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Image = global::Jaeger.UI.Common.Properties.Resources.save_16px;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // BarSeparator2
            // 
            this.BarSeparator2.DisplayName = "commandBarSeparator1";
            this.BarSeparator2.Name = "BarSeparator2";
            this.BarSeparator2.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Common.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarExcel});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Image = global::Jaeger.UI.Common.Properties.Resources.xls_16px;
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Text = "Exportar Excel";
            this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarStandarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.commandBar);
            this.Name = "ToolBarStandarControl";
            this.Size = new System.Drawing.Size(810, 30);
            this.Load += new System.EventHandler(this.ToolBarStandarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.commandBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar commandBar;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        public Telerik.WinControls.UI.CommandBarButton Nuevo;
        public Telerik.WinControls.UI.CommandBarButton Editar;
        public Telerik.WinControls.UI.CommandBarButton Remover;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarButton Guardar;
        public Telerik.WinControls.UI.CommandBarButton Imprimir;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        private Telerik.WinControls.UI.CommandBarSeparator BarSeparator1;
        public Telerik.WinControls.UI.CommandBarLabel Titulo;
        private Telerik.WinControls.UI.CommandBarSeparator BarSeparator2;
        public Telerik.WinControls.UI.CommandBarButton Imagen;
        public Telerik.WinControls.UI.CommandBarButton Autorizar;
        public Telerik.WinControls.UI.RadMenuItem ExportarExcel;
    }
}
