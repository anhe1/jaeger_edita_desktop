﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Common.Forms {
    public partial class ComprobanteTreeViewControl : UserControl {
        XmlDocument xmlDoc;
        public ComprobanteTreeViewControl() {
            InitializeComponent();
        }

        private void ComprobanteTreeViewControl_Load(object sender, EventArgs e) {

        }

        public void Crear(string texto) {
            var encodedString = Convert.FromBase64String(texto);
            // Encode the XML string in a UTF-8 byte array
            //byte[] encodedString = Encoding.UTF8.GetBytes(texto);

            // Put the byte array into a stream and rewind it to the beginning
            MemoryStream ms = new MemoryStream();
            ms.Write(encodedString, 0, encodedString.Length);
            ms.Seek(0, SeekOrigin.Begin);
            ms.Flush();
            ms.Position = 0;
            var sr = new StreamReader(ms);

            this.xmlDoc = new XmlDocument {
                XmlResolver = null
            };

            this.xmlDoc.LoadXml(sr.ReadToEnd());

            if (this.xmlDoc != null) {
                this.treeView.Nodes.Clear();
            }

            // agregar el nodo raíz
            var root = this.treeView.Nodes.Add("xml");

            // crear la vista
            foreach (XmlNode item in this.xmlDoc.ChildNodes) {
                this.TreeViewNodeAdd(item, root);
            }
            root.ExpandAll();
        }

        /// <summary>
        /// crear vista de arbol de archivo XML
        /// </summary>
        /// <param name="xNode">Nodo XML</param>
        /// <param name="oNode">Nodo de la vista</param>
        private void TreeViewNodeAdd(XmlNode xNode, RadTreeNode oNode) {
            var mNode = new RadTreeNode(xNode.Name);
            switch (xNode.NodeType) {
                case XmlNodeType.Element:
                    mNode.ForeColor = Color.Blue;
                    oNode.Nodes.Add(mNode);
                    break;
                case XmlNodeType.Text:
                    oNode.Nodes.Add(mNode);
                    break;
                case XmlNodeType.Comment:
                    oNode.Nodes.Add(mNode);
                    break;
                default:
                    if (oNode != null)
                        mNode = oNode;
                    break;
            }

            if (xNode.Attributes != null) {
                foreach (XmlAttribute atributo in xNode.Attributes) {
                    switch (atributo.NodeType) {
                        case XmlNodeType.Attribute:
                            mNode.Nodes.Add(string.Format("[{0} = {1}]", atributo.Name, atributo.Value));
                            break;
                        case XmlNodeType.ProcessingInstruction:
                            mNode.Text = string.Format("{0} [{1} = {2}]", mNode.Text, atributo.Name, atributo.Value);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (xNode.ChildNodes.Count > 0) {
                foreach (XmlNode item in xNode.ChildNodes) {
                    this.TreeViewNodeAdd(item, mNode);
                }
            }
        }
    }
}
