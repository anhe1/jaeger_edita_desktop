﻿namespace Jaeger.UI.Common.Forms {
    partial class TreeGridViewControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.SplitContainer = new Telerik.WinControls.UI.RadSplitContainer();
            this.TreePanel = new Telerik.WinControls.UI.SplitPanel();
            this.TreeData = new Telerik.WinControls.UI.RadTreeView();
            this.GridPanel = new Telerik.WinControls.UI.SplitPanel();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.TStatus = new Telerik.WinControls.UI.RadStatusStrip();
            this.LStatus = new Telerik.WinControls.UI.RadLabelElement();
            this.TBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreePanel)).BeginInit();
            this.TreePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPanel)).BeginInit();
            this.GridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitContainer
            // 
            this.SplitContainer.Controls.Add(this.TreePanel);
            this.SplitContainer.Controls.Add(this.GridPanel);
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.Location = new System.Drawing.Point(0, 30);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // 
            // 
            this.SplitContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.SplitContainer.Size = new System.Drawing.Size(912, 419);
            this.SplitContainer.TabIndex = 1;
            this.SplitContainer.TabStop = false;
            // 
            // TreePanel
            // 
            this.TreePanel.Controls.Add(this.TreeData);
            this.TreePanel.Location = new System.Drawing.Point(0, 0);
            this.TreePanel.Name = "TreePanel";
            // 
            // 
            // 
            this.TreePanel.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.TreePanel.Size = new System.Drawing.Size(198, 419);
            this.TreePanel.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2819383F, 0F);
            this.TreePanel.SizeInfo.SplitterCorrection = new System.Drawing.Size(-256, 0);
            this.TreePanel.TabIndex = 0;
            this.TreePanel.TabStop = false;
            this.TreePanel.Text = "splitPanel1";
            // 
            // TreeData
            // 
            this.TreeData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeData.Location = new System.Drawing.Point(0, 0);
            this.TreeData.Name = "TreeData";
            this.TreeData.Size = new System.Drawing.Size(198, 419);
            this.TreeData.SpacingBetweenNodes = -1;
            this.TreeData.TabIndex = 0;
            // 
            // GridPanel
            // 
            this.GridPanel.Controls.Add(this.GridData);
            this.GridPanel.Controls.Add(this.TStatus);
            this.GridPanel.Location = new System.Drawing.Point(202, 0);
            this.GridPanel.Name = "GridPanel";
            // 
            // 
            // 
            this.GridPanel.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.GridPanel.Size = new System.Drawing.Size(710, 419);
            this.GridPanel.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2819383F, 0F);
            this.GridPanel.SizeInfo.SplitterCorrection = new System.Drawing.Size(256, 0);
            this.GridPanel.TabIndex = 1;
            this.GridPanel.TabStop = false;
            this.GridPanel.Text = "splitPanel2";
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(710, 393);
            this.GridData.TabIndex = 0;
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            // 
            // TStatus
            // 
            this.TStatus.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LStatus});
            this.TStatus.Location = new System.Drawing.Point(0, 393);
            this.TStatus.Name = "TStatus";
            this.TStatus.Size = new System.Drawing.Size(710, 26);
            this.TStatus.SizingGrip = false;
            this.TStatus.TabIndex = 1;
            // 
            // LStatus
            // 
            this.LStatus.Name = "LStatus";
            this.TStatus.SetSpring(this.LStatus, false);
            this.LStatus.Text = "";
            this.LStatus.TextWrap = true;
            // 
            // TBar
            // 
            this.TBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TBar.Etiqueta = "";
            this.TBar.Location = new System.Drawing.Point(0, 0);
            this.TBar.Name = "TBar";
            this.TBar.ReadOnly = false;
            this.TBar.ShowActualizar = true;
            this.TBar.ShowAutorizar = false;
            this.TBar.ShowCerrar = true;
            this.TBar.ShowEditar = true;
            this.TBar.ShowExportarExcel = false;
            this.TBar.ShowFiltro = true;
            this.TBar.ShowGuardar = false;
            this.TBar.ShowHerramientas = false;
            this.TBar.ShowImagen = false;
            this.TBar.ShowImprimir = false;
            this.TBar.ShowNuevo = true;
            this.TBar.ShowRemover = true;
            this.TBar.Size = new System.Drawing.Size(912, 30);
            this.TBar.TabIndex = 0;
            // 
            // TreeGridViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SplitContainer);
            this.Controls.Add(this.TBar);
            this.Name = "TreeGridViewControl";
            this.Size = new System.Drawing.Size(912, 449);
            this.Load += new System.EventHandler(this.TreeGridViewControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreePanel)).EndInit();
            this.TreePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreeData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPanel)).EndInit();
            this.GridPanel.ResumeLayout(false);
            this.GridPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public ToolBarStandarControl TBar;
        private Telerik.WinControls.UI.RadSplitContainer SplitContainer;
        private Telerik.WinControls.UI.SplitPanel TreePanel;
        private Telerik.WinControls.UI.SplitPanel GridPanel;
        public Telerik.WinControls.UI.RadTreeView TreeData;
        public Telerik.WinControls.UI.RadGridView GridData;
        public Telerik.WinControls.UI.RadStatusStrip TStatus;
        public Telerik.WinControls.UI.RadLabelElement LStatus;
    }
}
