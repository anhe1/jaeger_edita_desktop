﻿
namespace Jaeger.UI.Common.Forms {
    partial class ProgressDialogForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgressDialogForm));
            this.lblSecondaryMsg = new Jaeger.UI.Common.Forms.LabelEllipsis();
            this.pbrSecondaryProgress = new Telerik.WinControls.UI.RadProgressBar();
            this.pbrPrimaryProgress = new Telerik.WinControls.UI.RadProgressBar();
            this.lblPrimaryMsg = new Jaeger.UI.Common.Forms.LabelEllipsis();
            this.btnCancel = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.lblSecondaryMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrSecondaryProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrPrimaryProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrimaryMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSecondaryMsg
            // 
            this.lblSecondaryMsg.AutoEllipsis = Jaeger.UI.Common.Services.EllipsisFormat.None;
            this.lblSecondaryMsg.AutoSize = false;
            this.lblSecondaryMsg.Location = new System.Drawing.Point(12, 9);
            this.lblSecondaryMsg.Name = "lblSecondaryMsg";
            this.lblSecondaryMsg.Size = new System.Drawing.Size(329, 18);
            this.lblSecondaryMsg.TabIndex = 0;
            this.lblSecondaryMsg.Text = "Process ...";
            this.lblSecondaryMsg.TextWrap = false;
            // 
            // pbrSecondaryProgress
            // 
            this.pbrSecondaryProgress.Location = new System.Drawing.Point(15, 27);
            this.pbrSecondaryProgress.Name = "pbrSecondaryProgress";
            this.pbrSecondaryProgress.Size = new System.Drawing.Size(326, 18);
            this.pbrSecondaryProgress.TabIndex = 1;
            this.pbrSecondaryProgress.Value1 = 34;
            // 
            // pbrPrimaryProgress
            // 
            this.pbrPrimaryProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbrPrimaryProgress.Location = new System.Drawing.Point(15, 74);
            this.pbrPrimaryProgress.Name = "pbrPrimaryProgress";
            this.pbrPrimaryProgress.Size = new System.Drawing.Size(326, 18);
            this.pbrPrimaryProgress.TabIndex = 3;
            this.pbrPrimaryProgress.Value1 = 75;
            // 
            // lblPrimaryMsg
            // 
            this.lblPrimaryMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPrimaryMsg.AutoEllipsis = Jaeger.UI.Common.Services.EllipsisFormat.None;
            this.lblPrimaryMsg.Location = new System.Drawing.Point(12, 51);
            this.lblPrimaryMsg.Name = "lblPrimaryMsg";
            this.lblPrimaryMsg.Size = new System.Drawing.Size(86, 18);
            this.lblPrimaryMsg.TabIndex = 2;
            this.lblPrimaryMsg.Text = "75% Completed";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(266, 108);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ProgressDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(353, 141);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pbrPrimaryProgress);
            this.Controls.Add(this.lblPrimaryMsg);
            this.Controls.Add(this.pbrSecondaryProgress);
            this.Controls.Add(this.lblSecondaryMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressDialogForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Procesando";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProgressDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.lblSecondaryMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrSecondaryProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbrPrimaryProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrimaryMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Jaeger.UI.Common.Forms.LabelEllipsis lblSecondaryMsg;
        private Telerik.WinControls.UI.RadProgressBar pbrSecondaryProgress;
        private Telerik.WinControls.UI.RadProgressBar pbrPrimaryProgress;
        private Jaeger.UI.Common.Forms.LabelEllipsis lblPrimaryMsg;
        private Telerik.WinControls.UI.RadButton btnCancel;
    }
}