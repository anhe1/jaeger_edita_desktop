﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Telerik.WinControls;

namespace Jaeger.UI.Common.Forms {
    public partial class ToolBarCommonControl : UserControl {
        public ToolBarCommonControl() {
            InitializeComponent();
        }

        private void ToolBarCommonControl_Load(object sender, EventArgs e) {
            //this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
            this.ToolBarHostEjercicio.HostedItem = this.SpinEjercicio.SpinElement;

            this.SpinEjercicio.Minimum = 2013;
            this.SpinEjercicio.Maximum = DateTime.Now.Year;
            this.SpinEjercicio.Value = DateTime.Now.Year;

            this.ToolBarComboBoxPeriodo.DisplayMember = "Descripcion";
            this.ToolBarComboBoxPeriodo.ValueMember = "Id";
            this.ToolBarComboBoxPeriodo.DataSource = ConfigService.GetMeses();
            this.ToolBarComboBoxPeriodo.SelectedIndex = DateTime.Now.Month;
        }

        public int GetMes() {
            if (this.ToolBarComboBoxPeriodo.SelectedValue != null) {
                return (int)this.ToolBarComboBoxPeriodo.SelectedValue;
            } else {
                return (int)Enum.Parse(typeof(MesesEnum), this.ToolBarComboBoxPeriodo.Text);
            }
        }

        public int GetEjercicio() {
            return int.Parse(this.SpinEjercicio.Value.ToString());
        }

        [Description("Hostitem"), Category("Botones")]
        public bool ShowItem {
            get {
                return this.HostCaption.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.HostCaption.Visibility = ElementVisibility.Visible;
                    this.HostItem.Visibility = ElementVisibility.Visible;
                    this.HostSeparator.Visibility = ElementVisibility.Visible;
                } else {
                    this.HostCaption.Visibility = ElementVisibility.Collapsed;
                    this.HostItem.Visibility = ElementVisibility.Collapsed;
                    this.HostSeparator.Visibility = ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowPeriodo {
            get {
                return this.ToolBarLabelPeriodo.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.ToolBarLabelPeriodo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.ToolBarComboBoxPeriodo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.ToolBarLabelPeriodo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.ToolBarComboBoxPeriodo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        public bool ShowEjercicio {
            get {
                return this.ToolBarLabelEjercicio.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true) {
                    this.ToolBarLabelEjercicio.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    this.ToolBarHostEjercicio.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.ToolBarLabelEjercicio.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                    this.ToolBarHostEjercicio.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Editar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Editar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCancelar {
            get {
                return this.Cancelar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowAutosuma {
            get {
                return this.AutoSuma.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.AutoSuma.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Imprimir.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Exportar en formato Excel"), Category("Botones")]
        public bool ShowExportarExcel {
            get {
                return this.ExportarExcel.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.ExportarExcel.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == Telerik.WinControls.ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            }
        }
    }
}
