﻿namespace Jaeger.UI.Common.Forms {
    partial class Waiting3Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.labelTitulo = new System.Windows.Forms.Label();
            this.WaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.waitingBarIndicatorElement1 = new Telerik.WinControls.UI.WaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.WaitingBar)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitulo
            // 
            this.labelTitulo.Location = new System.Drawing.Point(1, 9);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(238, 13);
            this.labelTitulo.TabIndex = 1;
            this.labelTitulo.Text = "Procesando";
            this.labelTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WaitingBar
            // 
            this.WaitingBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.WaitingBar.Location = new System.Drawing.Point(0, 30);
            this.WaitingBar.Name = "WaitingBar";
            this.WaitingBar.ShowText = true;
            this.WaitingBar.Size = new System.Drawing.Size(240, 30);
            this.WaitingBar.TabIndex = 2;
            // 
            // 
            // 
            // 
            // 
            // 
            this.WaitingBar.WaitingBarElement.SeparatorElement.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Right;
            this.WaitingBar.WaitingBarElement.ShowText = true;
            this.WaitingBar.WaitingBarElement.WaitingSpeed = 65;
            this.WaitingBar.WaitingBarElement.WaitingStep = 3;
            this.WaitingBar.WaitingIndicators.Add(this.waitingBarIndicatorElement1);
            this.WaitingBar.WaitingSpeed = 65;
            this.WaitingBar.WaitingStep = 3;
            this.WaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            // 
            // waitingBarIndicatorElement1
            // 
            this.waitingBarIndicatorElement1.Name = "waitingBarIndicatorElement1";
            this.waitingBarIndicatorElement1.StretchHorizontally = false;
            // 
            // Waiting3Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(240, 60);
            this.ControlBox = false;
            this.Controls.Add(this.WaitingBar);
            this.Controls.Add(this.labelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Waiting3Form";
            this.Opacity = 0.9D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Procesando";
            this.Load += new System.EventHandler(this.Waiting2Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.WaitingBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTitulo;
        private Telerik.WinControls.UI.RadWaitingBar WaitingBar;
        private Telerik.WinControls.UI.WaitingBarIndicatorElement waitingBarIndicatorElement1;
    }
}