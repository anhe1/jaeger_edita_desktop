﻿using System;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface ITiendaMetodoPagoModel {
        /// <summary>
        /// Indice principal de la tabla
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// Indice de la tabla de pedido del cliente
        /// </summary>
        int IdPedido { get; set; }

        /// <summary>
        /// Indice de la tabla de directorio
        /// </summary>
        int IdCliente { get; set; }

        /// <summary>
        /// Metodo de pago: Deposito | MercadoPago | Paypal
        /// </summary>
        string Metodo { get; set; }

        /// <summary>
        /// Identificador de la transaccion
        /// </summary>
        string IdTransaccion { get; set; }

        /// <summary>
        /// Estatus de la transaccion
        /// </summary>
        string Status { get; set; }

        /// <summary>
        /// Identificador de la preferencia (MercadoPago)
        /// </summary>
        string IdPreferencia { get; set; }

        /// <summary>
        /// Identificador del comprador
        /// </summary>
        string IdComprador { get; set; }

        /// <summary>
        /// Nombre del comprador
        /// </summary>
        string Comprador { get; set; }

        /// <summary>
        /// Descripcion de la compra
        /// </summary>
        string GTotal { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        DateTime FechaNuevo { get; set; }
    }
}
