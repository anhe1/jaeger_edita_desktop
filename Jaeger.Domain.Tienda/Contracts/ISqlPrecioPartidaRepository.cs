﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface ISqlPrecioPartidaRepository : IGenericRepository<PrecioPartidaModel> {
        IEnumerable<PrecioPartidaModel> GetList(List<IConditional> conditionals);

        PrecioPartidaModel Save(PrecioPartidaModel model);
    }
}
