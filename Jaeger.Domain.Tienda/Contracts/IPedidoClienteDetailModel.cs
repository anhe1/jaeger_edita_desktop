﻿using System.ComponentModel;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.ValueObjects;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoClienteDetailModel : IPedidoClienteStatusModel {
        string ClienteClave { get; set; }
        string Cliente { get; set; }
        bool Editable { get; }
        PedidoClienteStatusEnum Status { get; set; }

        BindingList<PedidoClientePartidaDetailModel> Partidas { get; set; }

        BindingList<IPedidoClienteStatusModel> Autorizaciones { get; set; }
        BindingList<IPedidoClienteRelacionadoModel> Relaciones { get; set; }
    }
}
