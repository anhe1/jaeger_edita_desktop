﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface ISqlCuponRepository : IGenericRepository<CuponModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        CuponModel Save(CuponModel model);
    }
}
