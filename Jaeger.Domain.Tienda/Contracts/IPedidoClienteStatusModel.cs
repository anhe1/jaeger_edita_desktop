﻿using System;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoClienteStatusModel {
        int IdPedido { get; set; }

        int IdStatus { get; set; }

        int IdDirectorio { get; set; }

        int IdCvMotivo { get; set; }

        string CvMotivo { get; set; }

        string Nota { get; set; }

        decimal Total { get; set; }
        
        string UrlFilePDF { get; set; }
        
        string Cancela { get; set; }

        DateTime? FechaStatus { get; set; }
    }
}
