﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Domain.Tienda.Contracts {
    /// <summary>
    /// interface para la lista de precios de la tienda web
    /// </summary>
    public interface ISqlFAQRepository : IGenericRepository<PreguntaFrecuenteModel> {

        /// <summary>
        /// almacenar nuevo registro de pregunta
        /// </summary>
        PreguntaFrecuenteModel Save(PreguntaFrecuenteModel modelo);

        /// <summary>
        /// listado de preguntas frecuentes
        /// </summary>
        /// <param name="onlyActive">si es falso devuelve los registro no activos</param>
        /// <returns>lista</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
