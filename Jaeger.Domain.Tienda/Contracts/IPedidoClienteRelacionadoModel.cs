﻿using System;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoClienteRelacionadoModel {
        int IdPedido { get; set; }
        int IdDirectorio { get;set; }
        int IdRelacion { get; set; }
        string Relacion { get; set; }
        int Folio { get; set; }
        string Cliente { get; set; }
        DateTime FechaEmision { get; set; }
        decimal GTotal { get; set; }
        string Creo { get; set; }
        DateTime FechaNuevo { get; set; }
    }
}
