﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface ISqlPedidoClientePartidaRepository : IGenericRepository<PedidoClientePartidaModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        IEnumerable<PedidoClientePartidaDetailModel> GetList(List<IConditional> condiciones);

        PedidoClientePartidaDetailModel Save(PedidoClientePartidaDetailModel model);
    }
}
