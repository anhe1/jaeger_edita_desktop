﻿namespace Jaeger.Domain.Tienda.Contracts {
    /// <summary>
    /// interface de configuración
    /// </summary>
    public interface IConfiguration : Base.Contracts.IConfiguration {
        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        bool ModoProductivo { get; set; }

        string Folder { get; set; }

        /// <summary>
        /// obtener o establecer configuración de base de datos
        /// </summary>
        DataBase.Contracts.IDataBaseConfiguracion DataBase { get; set; }
    }
}
