﻿using System.ComponentModel;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoClienteStatusDetailModel : IPedidoClienteStatusModel {
        BindingList<IPedidoClienteRelacionadoModel> Relaciones { get; set; }
    }
        
}
