﻿namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoReceptorModel {
        int IdDirectorio {  get; set; }

        string RFC { get; set; }

        string Nombre { get; set; }
    }
}
