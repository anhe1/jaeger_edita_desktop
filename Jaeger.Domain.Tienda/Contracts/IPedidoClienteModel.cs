﻿using System;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoClienteModel {
        /// <summary>
        /// obtener o establecer numero de pedido (PDCLN_ID)
        /// </summary>
        int IdPedido { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del pedido (PDCLN_STTSDCS_ID)
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_ORGN_ID)
        /// </summary>
        int IdOrigen { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_FRMSPGS_ID)
        /// </summary>
        int IdFormaPago { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio de clientes (PDCLN_DRCTR_ID)
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer indice del domicilio de entrega (PDCLN_PDD_ID)
        /// </summary>
        int IdDireccion { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_VNDDR_ID)
        /// </summary>
        int IdVendedor { get; set; }

        /// <summary>
        /// obtener o establecer si el cliente requiere factura 1 = Sí, 0 = No(PDCLN_RQIRFCTR)
        /// </summary>
        int ReqFactura { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_AUT_ID)
        /// </summary>
        int IdAutorizacion { get; set; } 

        /// <summary>
        /// obtener o establecer (PDCLN_SBTTL$)
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_CSTENV$)
        /// </summary>
        decimal TotalEnvio { get; set; }

        /// <summary>
        /// obtener o establecer total de descuento
        /// </summary>
        decimal TotalDescuento { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_IVA$)
        /// </summary>
        decimal TotalTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_TTL$)
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_NOM)
        /// </summary>
        string Recibe { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_MTDENV)
        /// </summary>
        string MetodoEnvio { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_OBSRV)
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_FECPED)
        /// </summary>
        DateTime? FechaPedido { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_FECREQ)
        /// </summary>
        DateTime? FechaRequerida { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_FCHAUT)
        /// </summary>
        DateTime? FechaAutoriza { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_FCENTRG)
        /// </summary>
        DateTime? FechaEntrega { get; set; }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        string UrlFilePDF { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_AUT)
        /// </summary>
        string Autoriza { get; set; }

        //#region informacion de la cancelacion
        ///// <summary>
        ///// obtener o establecer (PDCLN_FCCNCL)
        ///// </summary>
        //new DateTime? FechaCancela { get; set; }

        ///// <summary>
        ///// obtener o establecer clave y descripcion del motivo de la cancelacion (catalogo)
        ///// </summary>
        //new string ClaveCancelacion { get; set; }

        ///// <summary>
        ///// obtener o establecer la nota referente a la cancelacion
        ///// </summary>
        //new string NotaCancelacion { get; set; }

        ///// <summary>
        ///// obtener o establecer (PDCLN_USU_CNCL)
        ///// </summary>
        //new string Cancela { get; set; }
        //#endregion

        /// <summary>
        /// obtener o establecer (PDCLN_FN)
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_N)
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_M)
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_SYNC_ID)
        /// </summary>
        int PDCLN_SYNC_ID { get; set; }
    }
}
