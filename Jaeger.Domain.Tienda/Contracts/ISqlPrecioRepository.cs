﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface ISqlPrecioRepository : IGenericRepository<PrecioModel> {
        IEnumerable<PrecioDetailModel> GetList(List<IConditional> conditionals);

        /// <summary>
        /// obtener listado de precios
        /// </summary>
        IEnumerable<LPrecioModel> GetList(int idCliente, int idCatalogo = 0);

        PrecioDetailModel Save(PrecioDetailModel model);
    }
}
