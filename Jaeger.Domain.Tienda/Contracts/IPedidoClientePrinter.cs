﻿using Jaeger.Domain.Almacen.PT.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.Tienda.Contracts {
    public interface IPedidoClientePrinter {
        string StatusText { get; }
        List<RemisionDetailModel> Remisiones { get; set; }
        bool IsCosto { get; set; }
        string Embarque { get; set; }
        string TotalEnLetra { get; }
        string[] QrText { get; }
    }
}
