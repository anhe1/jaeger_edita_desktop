﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Tienda.Contracts {
    /// <summary>
    /// repositorio de pedidos de cliente (pddcln)
    /// </summary>
    public interface ISqlPedidoClienteRepository : IGenericRepository<PedidoClienteModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        PedidoClienteDetailModel GetPedido(int index);

        /// <summary>
        /// pedidos de clientes
        /// </summary>
        /// <param name="condiciones">condiciones</param>
        IEnumerable<PedidoClienteDetailModel> GetList(List<IConditional> condiciones);

        IEnumerable<PedidoClientePrinter> GetListE(string sql, List<IConditional> condiciones);

        PedidoClienteDetailModel Save(PedidoClienteDetailModel model);

        bool Cancelar(IPedidoClienteStatusDetailModel model);
    }
}
