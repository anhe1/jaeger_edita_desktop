﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Tienda.Builder {
    public interface IProductosQueryBuilder : Almacen.Builder.IProductosQueryBuilder {
        IProductoAutizadoQueryBuilder Autorizado(bool isAutorize = false);

    }

    public interface IProductoSearchQueryBuilder : Almacen.Builder.IProductosActivoQueryBuilder {

    }

    public interface IProductoAutizadoQueryBuilder : IConditionalBuilder, IConditionalBuild {
    }
}
