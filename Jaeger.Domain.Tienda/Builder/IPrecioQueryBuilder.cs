﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Tienda.Builder {
    public interface IPrecioQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IPrecioIdClienteQueryBuilder WithIdCliente(int idCliente);
    }

    public interface IPrecioIdClienteQueryBuilder {

    }
}
