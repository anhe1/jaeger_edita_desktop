﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Tienda.Builder {
    public interface IResumenQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IResumenPedidoQueryBuilder WithPedido();
        IResumenPedidoQueryBuilder WithRemisionado();
    }

    public interface IResumenPedidoQueryBuilder : IConditionalBuilder {
        IResumenQueryYearBuilder Year(int year);
    }

    public interface IResumenQueryYearBuilder : IConditionalBuilder {
        IResumenQueryYearBuilder Month(int month = -1);
        IResumenClienteQueryBuilder IdCliente(int id);
    }

    public interface IResumenClienteQueryBuilder : IConditionalBuilder {

    }
}
