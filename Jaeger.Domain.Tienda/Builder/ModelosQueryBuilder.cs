﻿using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Tienda.Builder {
    public class ModelosQueryBuilder : Almacen.Builder.ModelosQueryBuilder, Almacen.Builder.IModelosQueryBuilder, IModelosQueryAlmacenBuilder, IModelosQueryActivoBuilder, IModelosQueryAuthorizedBuilder,
        IModelosIdProductoQueryBuilder {
        public ModelosQueryBuilder() : base() { }

        public override IModelosQueryAlmacenBuilder ByIdAlmacen(AlmacenEnum idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("CTMDL_ALM_ID", d0.ToString()));
            return this;
        }

        public override IModelosQueryActivoBuilder Activo(bool isActivo = true) {
            if (isActivo) {
                this._Conditionals.Add(new Conditional("CTMDL_A", "1"));
            }
            return this;
        }

        public override IModelosIdProductoQueryBuilder IdProducto(int[] ids) {
            if (ids.Length > 0) {
                this._Conditionals.Add(new Conditional("CTMDL_CTPRD_ID", string.Join(",", ids), ConditionalTypeEnum.In));
            }
            return this;
        }

        public override IConditionalBuilder OnlyAuthorized(bool onlyAuthorized = true) {
            if (onlyAuthorized)
                this._Conditionals.Add(new Conditional("CTMDL_ATRZD", "1"));
            return this;
        }
    }
}
