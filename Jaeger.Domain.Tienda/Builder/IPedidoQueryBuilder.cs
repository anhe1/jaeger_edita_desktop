﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Tienda.ValueObjects;

namespace Jaeger.Domain.Tienda.Builder {
    public interface IPedidoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IPedidoYearQueryBuilder Year(int  year);
        IPedidoClientesQueryBuilder Clientes();
    }

    public interface IPedidoYearQueryBuilder : IConditionalBuilder {
        IPedidoMonthQueryBuilder Month(int month = -1);
        IPedidoIdClienteQueryBuilder IdCliente(int idCliente);
        IPedidoStatusQueryBuilder Status(PedidoClienteStatusEnum status);
    }

    public interface IPedidoMonthQueryBuilder : IConditionalBuilder {
        IPedidoIdClienteQueryBuilder IdCliente(int idCliente);
    }

    public interface IPedidoIdClienteQueryBuilder : IConditionalBuilder {
        
    }

    public interface IPedidoStatusQueryBuilder : IConditionalBuilder {

    }

    public interface IPedidoClientesQueryBuilder : IConditionalBuilder {

    }
}
