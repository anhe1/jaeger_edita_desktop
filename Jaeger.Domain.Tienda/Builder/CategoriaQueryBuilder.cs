﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Tienda.Builder {
    public class CategoriaQueryBuilder : ConditionalBuilder, ICategoriaQueryBuilder, IConditionalBuilder, IConditionalBuild, ICategoriaTipoAlmacenQueryBuilder, ICategoriaActivoQueryBuilder,
        ICategoriaNivelQueryBuilder, ICategoriaProductosQueryBuilder {
        public CategoriaQueryBuilder() : base() {

        }

        [System.Obsolete("El indice del almacen no se utiliza")]
        public virtual ICategoriaTipoAlmacenQueryBuilder ByAlmacen(Base.ValueObjects.AlmacenEnum almacen) {
            var v1 = (int)almacen;
            this._Conditionals.Add(new Conditional("CTCAT_ALM_ID", v1.ToString()));
            return this;
        }

        public virtual ICategoriaActivoQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive)
                this._Conditionals.Add(new Conditional("CTCAT_A", "1"));
            return this;
        }

        public virtual ICategoriaProductosQueryBuilder Productos() {
            return this;
        }

        public virtual ICategoriaNivelQueryBuilder Nivel(int nivel = 1) {
            this._Conditionals.Add(new Conditional("_NIVEL", nivel.ToString()));
            return this;
        }
    }
}
