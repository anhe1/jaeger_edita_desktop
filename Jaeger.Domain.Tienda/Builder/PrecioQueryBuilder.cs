﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Tienda.Builder {
    public class PrecioQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IPrecioQueryBuilder, IPrecioIdClienteQueryBuilder {
        public PrecioQueryBuilder() : base() { }

        public IPrecioIdClienteQueryBuilder WithIdCliente(int idCliente) {
            this._Conditionals.Add(new Conditional());
            return this;
        }
    }
}
