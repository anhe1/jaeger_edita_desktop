﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Tienda.ValueObjects;

namespace Jaeger.Domain.Tienda.Builder {
    public class PedidoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IPedidoQueryBuilder, IPedidoYearQueryBuilder , IPedidoMonthQueryBuilder, IPedidoIdClienteQueryBuilder,
        IPedidoStatusQueryBuilder, IPedidoClientesQueryBuilder {

        public PedidoQueryBuilder() : base() { }
        public IPedidoYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("PDCLN_ANIO", year.ToString()));
            return this;
        }

        public IPedidoMonthQueryBuilder Month(int month = -1) {
            if (month > 0) {
                this._Conditionals.Add(new Conditional("PDCLN_MES", month.ToString()));
            }
            return this;
        }

        public IPedidoIdClienteQueryBuilder IdCliente(int idCliente) {
            this._Conditionals.Add(new Conditional("PDCLN_DRCTR_ID", idCliente.ToString()));
            return this;
        }

        public IPedidoStatusQueryBuilder Status(PedidoClienteStatusEnum status) {
            var idStatus = (int)status;
            this._Conditionals.Add(new Conditional("PDCLN_STTS_ID", idStatus.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        public IPedidoClientesQueryBuilder Clientes() {
            this._Conditionals.Clear();
            return this;
        }
    }
}
