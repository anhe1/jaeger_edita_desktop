﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Tienda.Builder {
    public interface ICategoriaQueryBuilder : IConditionalBuilder, IConditionalBuild {
        ICategoriaActivoQueryBuilder OnlyActive(bool onlyActive = true);
        [System.Obsolete("El indice del almacen no se utiliza")]
        ICategoriaTipoAlmacenQueryBuilder ByAlmacen(Base.ValueObjects.AlmacenEnum almacen);
        ICategoriaProductosQueryBuilder Productos();
    }

    [System.Obsolete("El indice del almacen no se utiliza")]
    public interface ICategoriaTipoAlmacenQueryBuilder : IConditionalBuilder {
        ICategoriaActivoQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface ICategoriaActivoQueryBuilder : IConditionalBuilder {

    }

    public interface ICategoriaProductosQueryBuilder : IConditionalBuilder {
        ICategoriaNivelQueryBuilder Nivel(int nivel = 1);
    }

    public interface ICategoriaNivelQueryBuilder : IConditionalBuilder {
        ICategoriaActivoQueryBuilder OnlyActive(bool onlyActive = true);
    }
}
