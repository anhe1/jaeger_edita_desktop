﻿using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Tienda.Builder {
    public class ProductosQueryBuilder : Almacen.Builder.ProductosQueryBuilder, IConditionalBuilder, IConditionalBuild, IProductosQueryBuilder,
        Domain.Almacen.Builder.IProductosSearchQueryBuilder, IProductoAutizadoQueryBuilder {
        public ProductosQueryBuilder() : base() { }

        public override IProductosSearchQueryBuilder Search(string description) {
            var search = "%" + description.ToLower().Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u") + "%";
            this._Conditionals.Add(new Conditional("SEARCH", search, ConditionalTypeEnum.Like));
            return this;
        }

        public override IProductosAlmacenQueryBuilder ByAlmacen(AlmacenEnum idAlmacen) {
            return this;
        }

        public override IProductosActivoQueryBuilder Activo(bool activo = true) {
            if (activo)
                this._Conditionals.Add(new Conditional("ACTIVO", "1"));
            return this;
        }

        public IProductoAutizadoQueryBuilder Autorizado(bool isAutorize = false) {
            if (isAutorize)
                this._Conditionals.Add(new Conditional("AUTORIZADO", "1"));
            return this;
        }
    }
}
