﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Tienda.Builder {
    public class ResumenQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IResumenQueryBuilder, IResumenPedidoQueryBuilder, IResumenQueryYearBuilder, IResumenClienteQueryBuilder {
        protected internal bool IsRemision = false;
        public ResumenQueryBuilder() : base() { }

        public IResumenPedidoQueryBuilder WithPedido() {
            this.IsRemision = false;
            return this;
        }

        public IResumenPedidoQueryBuilder WithRemisionado() {
            this.IsRemision = true;
            return this;
        }

        public IResumenQueryYearBuilder Year(int year) {
            if (this.IsRemision) {
                this._Conditionals.Add(new Conditional("RMSN_STTS_ID", "0", Base.ValueObjects.ConditionalTypeEnum.GreaterThan));
                this._Conditionals.Add(new Conditional("RMSN_ANIO", year.ToString()));
            } else {
                this._Conditionals.Add(new Conditional("PDCLN_STTS_ID", "0", Base.ValueObjects.ConditionalTypeEnum.GreaterThan));
                this._Conditionals.Add(new Conditional("PDCLN_ANIO", year.ToString()));
            }
            return this;
        }

        public IResumenQueryYearBuilder Month(int month = -1) {
            if (month > 0) {
                if (this.IsRemision) {
                    this._Conditionals.Add(new Conditional("RMSN_MES", month.ToString()));
                } else {
                    this._Conditionals.Add(new Conditional("PDCLN_MES", month.ToString()));
                }
            }
            return this;
        }

        public IResumenClienteQueryBuilder IdCliente(int id) {
            if (this.IsRemision) {
                this._Conditionals.Add(new Conditional("RMSN_DRCTR_ID", id.ToString()));
            } else {
                this._Conditionals.Add(new Conditional("PDCLN_DRCTR_ID", id.ToString()));
            }

            return this;
        }
    }
}
