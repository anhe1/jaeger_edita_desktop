﻿using System.ComponentModel;

namespace Jaeger.Domain.Tienda.ValueObjects {
    /// <summary>
    /// status de pedidos de clientes
    /// </summary>
    public enum PedidoClienteStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Pendiente")]
        Pendiente = 1,
        [Description("Por Surtir")]
        Por_Surtir = 2,
        [Description("Detenido")]
        Detenido = 3,
        [Description("Entregado")]
        Entregado = 4
    }
}
