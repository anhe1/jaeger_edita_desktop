﻿using System.ComponentModel;

namespace Jaeger.Domain.Tienda.ValueObjects {
    /// <summary>
    /// formas de pago aceptadas en el pedido
    /// </summary>
    public enum FormaPagoEnum {
        [Description("N/A")]
        None = 0,
        [Description("Depósito")]
        Deposito = 1,
        [Description("Mercado Pago")]
        MercadoPago = 2,
        [Description("PayPal")]
        PayPal = 3
    }
}
