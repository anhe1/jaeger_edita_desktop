﻿using System.ComponentModel;

namespace Jaeger.Domain.Tienda.ValueObjects {
    public enum PedidoCancelacionTipoEnum {
        [Description("No definido")]
        NoDefinido = 0,
        [Description("A solicitud del cliente")]
        ClienteSolicita = 1,
        [Description("Acuerdo con vendedor")]
        AcuerdoVendedor = 2,
        [Description("Sin disponibilidad del producto")]
        SinDisponibilidad = 3,
        [Description("Devolución de producto")]
        Devoluciion = 4,
        [Description("Sustituye pedido previo")]
        Sustituye = 5,
        [Description("Error de captura")]
        ErrorCaptura = 6,
        [Description("Cliente sin crédito")]
        ClienteSinCredito = 7,
    }
}
