﻿using System.ComponentModel;

namespace Jaeger.Domain.Tienda.ValueObjects {
    public enum CatalogoPrioridadEnum {
        [Description("Regular")]
        Regular = 0,
        [Description("Predeterminado")]
        Predeterminado = 1
    }
}
