﻿using System.ComponentModel;

namespace Jaeger.Domain.Tienda.ValueObjects {
    public enum PedigoOrigenEnum {
        [Description("Local")]
        Local = 0,
        [Description("Vendedor")]
        Vendedor = 1,
        [Description("TiendaWeb")]
        TiendaWeb = 2
    }
}
