﻿using System.ComponentModel;
using SqlSugar;

namespace Jaeger.Domain.Tienda.Entities {
    [SugarTable("CTPRC", "ventas: catalogo de precios")]
    public class PrecioDetailModel : PrecioModel, IDataErrorInfo {
        #region declaraciones
        private BindingList<PrecioPartidaModel> _Partidas;
        #endregion

        public PrecioDetailModel() : base() {
            this._Partidas = new BindingList<PrecioPartidaModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsVigente {
            get {
                if (this.IdPrioridad == 1)
                    return true;

                if (System.DateTime.Now >= this.FechaInicio && System.DateTime.Now <= this.FechaFin) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// obtener o establecer la lista de tamaños y precios
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<PrecioPartidaModel> Items {
            get { return this._Partidas; }
            set {
                this._Partidas = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (this.IsVigente == false) {
                    return "La lista de precios esta fuera de su vigencia";
                }
                return string.Empty;
            }
        }

        public string this[string columnName] {
            get {
                if (columnName == "FechaInicio" | columnName == "FechaFin" && this.IsVigente == false) {
                    return "Fecha de inicio o fin de vigencia no válida.";
                }
                return string.Empty;
            }
        }
    }
}
