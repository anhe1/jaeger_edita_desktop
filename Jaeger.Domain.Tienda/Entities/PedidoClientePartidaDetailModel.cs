﻿namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClientePartidaDetailModel : PedidoClientePartidaModel {
        public PedidoClientePartidaDetailModel() : base() {

        }

        public string NoIdentificacion {
            get {
                return string.Format("LT{0:00000}-{1:00000}-{2:00}", this.IdProducto, this.IdModelo, this.IdEspecificacion);
            }
        }
    }
}
