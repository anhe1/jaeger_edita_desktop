﻿using System.Collections.Generic;

namespace Jaeger.Domain.Tienda.Entities {
    public class LPrecioPrinter {
        public LPrecioPrinter() { }
        public string Titile {  get; set; }
        public List<LPrecioConceptoPrinter> Conceptos { get; set; }
    }
}
