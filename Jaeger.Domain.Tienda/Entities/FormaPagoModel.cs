﻿namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// forma de pago
    /// </summary>
    public class FormaPagoModel : Base.Abstractions.BaseSingleModel {
        public FormaPagoModel() { }

        public FormaPagoModel(int id, string descripcion) : base(id, descripcion) { }
    }
}
