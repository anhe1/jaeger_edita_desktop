﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Services.Mapping;
namespace Jaeger.Domain.Tienda.Entities {
    ///<summary>
    ///(PDCLN)
    ///</summary>
    [SugarTable("PDCLN", "tienda web: pedidos de clientes")]
    public class PedidoClienteModel : BasePropertyChangeImplementation, IPedidoClienteModel {
        #region declaraciones
        private int _PDCLN_ID;
        private int _PDCLN_A;
        private int _PDCLN_ORGN_ID;
        private int _PDCLN_FRMSPGS_ID;
        private int _PDCLN_DRCTR_ID;
        private int _PDCLN_VNDDR_ID;
        private int _PDCLN_STTSDCS_ID;
        private int _PDCLN_SYNC_ID;
        private int _PDCLN_RQIRFCTR;
        private int _PDCLN_AUT_ID;
        private int _PDCLN_PDD_ID;
        private decimal _PDCLN_CSTENV;
        private decimal _PDCLN_IVA;
        private decimal _PDCLN_SBTTL;
        private decimal _PDCLN_TTL;
        private string _PDCLN_NOM;
        private string _PDCLN_MTDENV;
        private string _PDCLN_OBSRV;
        private string _PDCLN_USU_AUT;
        private string _PDCLN_USU_N;
        private string _PDCLN_USU_M;
        
        private DateTime? _PDCLN_FECPED;
        private DateTime? _PDCLN_FECREQ;
        private DateTime _PDCLN_FN;
        private DateTime? _PDCLN_FM;
        
        private DateTime? _PDCLN_FCHAUT;
        private DateTime? _PDCLN_FCENTRG;
        
        private string _urlPDF;
        
        private decimal _TotalDescuento;
        #endregion

        public PedidoClienteModel() {
            this.FechaPedido = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.IdStatus = (int)ValueObjects.PedidoClienteStatusEnum.Pendiente;
            this.Activo = 1;
        }

        /// <summary>
        /// obtener o establecer numero de pedido (PDCLN_ID)
        /// </summary>
        [DataNames("PDCLN_ID")]
        [SugarColumn(ColumnName = "PDCLN_ID", ColumnDescription = "indice de la tabla (numero de pedido)", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPedido {
            get { return this._PDCLN_ID; }
            set {
                this._PDCLN_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_A)
        /// </summary>
        [DataNames("PDCLN_A")]
        [SugarColumn(ColumnName = "PDCLN_A", ColumnDescription = "registro activo")]
        public int Activo {
            get { return this._PDCLN_A; }
            set {
                this._PDCLN_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status del pedido (PDCLN_STTSDCS_ID)
        /// </summary>
        [DataNames("PDCLN_STTS_ID")]
        [SugarColumn(ColumnName = "PDCLN_STTS_ID", ColumnDescription = "indice del status del pedido")]
        public int IdStatus {
            get { return this._PDCLN_STTSDCS_ID; }
            set {
                this._PDCLN_STTSDCS_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_ORGN_ID)
        /// </summary>
        [DataNames("PDCLN_ORGN_ID")]
        [SugarColumn(ColumnName = "PDCLN_ORGN_ID", ColumnDescription = "origen del pedido (local = 0, Vendedor = 1, tienda web= 2)")]
        public int IdOrigen {
            get { return this._PDCLN_ORGN_ID; }
            set {
                this._PDCLN_ORGN_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FRMSPGS_ID)
        /// </summary>
        [DataNames("PDCLN_FRMPG_ID")]
        [SugarColumn(ColumnName = "PDCLN_FRMPG_ID", ColumnDescription = "forma de pago")]
        public int IdFormaPago {
            get { return this._PDCLN_FRMSPGS_ID; }
            set {
                this._PDCLN_FRMSPGS_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio de clientes (PDCLN_DRCTR_ID)
        /// </summary>
        [DataNames("PDCLN_DRCTR_ID")]
        [SugarColumn(ColumnName = "PDCLN_DRCTR_ID", ColumnDescription = "indice de relacion con la tabla del directorio (clientes)")]
        public int IdDirectorio {
            get { return this._PDCLN_DRCTR_ID; }
            set {
                this._PDCLN_DRCTR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del domicilio de entrega (PDCLN_PDD_ID)
        /// </summary>
        [DataNames("PDCLN_DRCCN_ID")]
        [SugarColumn(ColumnName = "PDCLN_DRCCN_ID", ColumnDescription = "indice de la direccion del envio (tabla de direcciones)")]
        public int IdDireccion {
            get { return this._PDCLN_PDD_ID; }
            set {
                this._PDCLN_PDD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_VNDDR_ID)
        /// </summary>
        [DataNames("PDCLN_VNDR_ID")]
        [SugarColumn(ColumnName = "PDCLN_VNDR_ID", ColumnDescription = "indice de la tabla de vendedores")]
        public int IdVendedor {
            get { return this._PDCLN_VNDDR_ID; }
            set {
                this._PDCLN_VNDDR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el cliente requiere factura 1 = Sí, 0 = No(PDCLN_RQIRFCTR)
        /// </summary>
        [DataNames("PDCLN_RQIRFCTR")]
        [SugarColumn(ColumnName = "PDCLN_RQIRFCTR", ColumnDescription = "requiere factura = 1")]
        public int ReqFactura {
            get { return this._PDCLN_RQIRFCTR; }
            set {
                this._PDCLN_RQIRFCTR = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer (PDCLN_AUT_ID)
        /// </summary>
        [DataNames("PDCLN_AUT_ID")]
        [SugarColumn(ColumnName = "PDCLN_AUT_ID", ColumnDescription = "autorizacion")]
        public int IdAutorizacion {
            get { return this._PDCLN_AUT_ID; }
            set {
                this._PDCLN_AUT_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_IVA$)
        /// </summary>
        [DataNames("PDCLN_IVA")]
        [SugarColumn(ColumnName = "PDCLN_IVA", ColumnDescription = "importe del iva causado")]
        public decimal TotalTrasladoIVA {
            get { return this._PDCLN_IVA; }
            set {
                this._PDCLN_IVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_SBTTL$)
        /// </summary>
        [DataNames("PDCLN_SBTTL")]
        [SugarColumn(ColumnName = "PDCLN_SBTTL", ColumnDescription = "subtotal")]
        public decimal SubTotal {
            get { return this._PDCLN_SBTTL; }
            set {
                this._PDCLN_SBTTL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer total del descuento aplicado antes de los impuestos
        /// </summary>
        [DataNames("PDCLN_DSCNT")]
        [SugarColumn(ColumnName = "PDCLN_DSCNT", ColumnDescription = "total del descuento")]
        public decimal TotalDescuento {
            get { return this._TotalDescuento; }
            set { this._TotalDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_CSTENV$)
        /// </summary>
        [DataNames("PDCLN_ENVCST")]
        [SugarColumn(ColumnName = "PDCLN_ENVCST", ColumnDescription = "totoal del envio")]
        public decimal TotalEnvio {
            get { return this._PDCLN_CSTENV; }
            set {
                this._PDCLN_CSTENV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_TTL$)
        /// </summary>
        [DataNames("PDCLN_TTL")]
        [SugarColumn(ColumnName = "PDCLN_TTL", ColumnDescription = "gran total")]
        public decimal Total {
            get { return this._PDCLN_TTL; }
            set {
                this._PDCLN_TTL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_NOM)
        /// </summary>
        [DataNames("PDCLN_RCB")]
        [SugarColumn(ColumnName = "PDCLN_RCB", ColumnDescription = "nombre de la persona que recibe")]
        public string Recibe {
            get { return this._PDCLN_NOM; }
            set {
                this._PDCLN_NOM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_MTDENV)
        /// </summary>
        [DataNames("PDCLN_MTDENV")]
        [SugarColumn(ColumnName = "PDCLN_MTDENV", ColumnDescription = "metodo de envio")]
        public string MetodoEnvio {
            get { return this._PDCLN_MTDENV; }
            set {
                this._PDCLN_MTDENV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_OBSRV)
        /// </summary>
        [DataNames("PDCLN_OBSRV")]
        [SugarColumn(ColumnName = "PDCLN_OBSRV", ColumnDescription = "observaciones")]
        public string Nota {
            get { return this._PDCLN_OBSRV; }
            set {
                this._PDCLN_OBSRV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FECPED)
        /// </summary>
        [DataNames("PDCLN_FCPED")]
        [SugarColumn(ColumnName = "PDCLN_FCPED", ColumnDescription = "fecha de pedido")]
        public DateTime? FechaPedido {
            get { return this._PDCLN_FECPED; }
            set {
                this._PDCLN_FECPED = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FECREQ)
        /// </summary>
        [DataNames("PDCLN_FCREQ")]
        [SugarColumn(ColumnName = "PDCLN_FCREQ", ColumnDescription = "fecha requerida por el cliente")]
        public DateTime? FechaRequerida {
            get { return this._PDCLN_FECREQ; }
            set {
                this._PDCLN_FECREQ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FCHAUT)
        /// </summary>
        [DataNames("PDCLN_FCHAUT")]
        [SugarColumn(ColumnName = "PDCLN_FCHAUT", ColumnDescription = "fecha de autorizacion")]
        public DateTime? FechaAutoriza {
            get { return this._PDCLN_FCHAUT; }
            set {
                this._PDCLN_FCHAUT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FCENTRG)
        /// </summary>
        [DataNames("PDCLN_FCENTR")]
        [SugarColumn(ColumnName = "PDCLN_FCENTR", ColumnDescription = "fecha de entrega al cliente")]
        public DateTime? FechaEntrega {
            get { return this._PDCLN_FCENTRG; }
            set {
                this._PDCLN_FCENTRG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("PDCLN_URL_PDF")]
        [SugarColumn(ColumnName = "PDCLN_URL_PDF", ColumnDescription = "url para archivo adjunto")]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_AUT)
        /// </summary>
        [DataNames("PDCLN_USU_AUT")]
        [SugarColumn(ColumnName = "PDCLN_USU_AUT", ColumnDescription = "clave del usuario que autoriza el pedido")]
        public string Autoriza {
            get { return this._PDCLN_USU_AUT; }
            set {
                this._PDCLN_USU_AUT = value;
                this.OnPropertyChanged();
            }
        }

        //#region informacion de la cancelacion
        ///// <summary>
        ///// obtener o establecer (PDCLN_FCCNCL)
        ///// </summary>
        //[DataNames("PDCLN_FCCNCL")]
        //[SugarColumn(ColumnName = "PDCLN_FCCNCL", ColumnDescription = "fecha de cancelacion")]
        //public DateTime? FechaCancela {
        //    get { return this._PDCLN_FCCNCL; }
        //    set {
        //        this._PDCLN_FCCNCL = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// obtener o establecer clave y descripcion del motivo de la cancelacion (catalogo)
        ///// </summary>
        //[DataNames("PDCLN_CLMTV")]
        //[SugarColumn(ColumnName = "PDCLN_CLMTV", ColumnDescription = "clave de motivo de la cancelacion del pedido")]
        //public string ClaveCancelacion {
        //    get { return this._MotivoCancelacion; }
        //    set {
        //        this._MotivoCancelacion = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// obtener o establecer la nota referente a la cancelacion
        ///// </summary>
        //[DataNames("PDCLN_CLNTA")]
        //[SugarColumn(ColumnName = "PDCLN_CLNTA", ColumnDescription = "nota de la cancelacion")]
        //public string NotaCancelacion {
        //    get { return this._CancelacionNota; }
        //    set {
        //        this._CancelacionNota = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// obtener o establecer (PDCLN_USU_CNCL)
        ///// </summary>
        //[DataNames("PDCLN_USU_CNCL")]
        //[SugarColumn(ColumnName = "PDCLN_USU_CNCL", ColumnDescription = "clave del usuario")]
        //public string Cancela {
        //    get { return this._PDCLN_USU_CNCL; }
        //    set {
        //        this._PDCLN_USU_CNCL = value;
        //        this.OnPropertyChanged();
        //    }
        //}
        //#endregion

        /// <summary>
        /// obtener o establecer (PDCLN_FN)
        /// </summary>
        [DataNames("PDCLN_FN")]
        [SugarColumn(ColumnName = "PDCLN_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this._PDCLN_FN; }
            set {
                this._PDCLN_FN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_N)
        /// </summary>
        [DataNames("PDCLN_USU_N")]
        [SugarColumn(ColumnName = "PDCLN_USU_N", ColumnDescription = "clave del usuario creador del registro", IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return this._PDCLN_USU_N; }
            set {
                this._PDCLN_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FM)
        /// </summary>
        [DataNames("PDCLN_FM")]
        [SugarColumn(ColumnName = "PDCLN_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                if (this._PDCLN_FM >= new DateTime(1900, 1, 1))
                    return this._PDCLN_FM;
                return null;
            }
            set {
                this._PDCLN_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_M)
        /// </summary>
        [DataNames("PDCLN_USU_M")]
        [SugarColumn(ColumnName = "PDCLN_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this._PDCLN_USU_M; }
            set {
                this._PDCLN_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_SYNC_ID)
        /// </summary>
        [DataNames("PDCLN_SYNC_ID")]
        [SugarColumn(ColumnName = "PDCLN_SYNC_ID", ColumnDescription = "sincronizado?")]
        public int PDCLN_SYNC_ID {
            get { return this._PDCLN_SYNC_ID; }
            set {
                this._PDCLN_SYNC_ID = value;
                this.OnPropertyChanged();
            }
        }
    }
}
