﻿namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// clase prioridad para el catalogo de precios
    /// </summary>
    public class PrioridadModel {

        public PrioridadModel() {

        }

        public PrioridadModel(int id, int activo, string descripcion) {
            this.Id = id;
            this.Activo = activo;
            this.Descripcion = descripcion;
        }

        public int Id { get; set; }

        public int Activo { get; set; }

        public string Descripcion { get; set; }
    }
}
