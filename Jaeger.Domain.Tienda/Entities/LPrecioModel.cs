﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// clase para vista de la lista de precios por especificaciones
    /// </summary>
    public class LPrecioModel {
        /// <summary>
        /// obtener o establecer el indice de la especificacion o tamanio
        /// </summary>
        [DataNames("ESPEC_ID")]
        public int IdEspecificacion { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de precios
        /// </summary>
        [DataNames("PRECIO_ID")]
        public int IdPrecio { get; set; }

        /// <summary>
        /// obtener o establer el indice de la partida del catalogo de precios
        /// </summary>
        [DataNames("PARTIDA_ID")]
        public int IdPartida { get; set; }

        /// <summary>
        /// obtener o establecer el valor unitario
        /// </summary>
        [DataNames("VUNITARIO", "UNITARIO")]
        public decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el costo 
        /// </summary>
        [DataNames("VCOSTO")]
        public decimal Costo { get; set; }
    }
}
