﻿using System;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClienteStatusModel : Base.Abstractions.BasePropertyChangeImplementation, IPedidoClienteStatusModel {
        #region
        private int _PDCLN_ID;
        private int _PDCLN_STTSDCS_ID;
        private string _MotivoCancelacion;
        private string _PDCLN_OBSRV;
        private decimal _PDCLN_TTL;
        private string _urlPDF;
        private string _PDCLN_USU_M;
        private int _IdMotivoCancelacion;
        private DateTime? _PDCLN_FM;
        private int PDCLS_DRCTR_ID;
        #endregion

        public PedidoClienteStatusModel() { }

        [DataNames("PDCLS_PDCLN_ID")]
        public int IdPedido {
            get { return this._PDCLN_ID; }
            set {
                this._PDCLN_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_STTS_ID")]
        public int IdStatus {
            get { return this._PDCLN_STTSDCS_ID; }
            set {
                this._PDCLN_STTSDCS_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_DRCTR_ID")]
        public int IdDirectorio {
            get { return this.PDCLS_DRCTR_ID; }
            set { this.PDCLS_DRCTR_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_CLMTV_ID")]
        public int IdCvMotivo {
            get { return this._IdMotivoCancelacion; }
            set {
                this._IdMotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_CLMTV")]
        public string CvMotivo {
            get { return this._MotivoCancelacion; }
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_OBSRV")]
        public string Nota {
            get { return this._PDCLN_OBSRV; }
            set {
                this._PDCLN_OBSRV = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_TTL")]
        public decimal Total {
            get { return this._PDCLN_TTL; }
            set {
                this._PDCLN_TTL = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_URL_PDF")]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_USU_M")]
        public string Cancela {
            get { return this._PDCLN_USU_M; }
            set {
                this._PDCLN_USU_M = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("PDCLS_FM")]
        public DateTime? FechaStatus {
            get {
                if (this._PDCLN_FM >= new DateTime(1900, 1, 1))
                    return this._PDCLN_FM;
                return null;
            }
            set {
                this._PDCLN_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}
