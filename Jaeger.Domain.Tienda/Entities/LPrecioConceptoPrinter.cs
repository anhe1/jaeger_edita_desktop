﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Tienda.Entities {
    public class LPrecioConceptoPrinter {

        public LPrecioConceptoPrinter() { }

        [DataNames("CTPRCP_ID")]
        [SugarColumn(ColumnName = "CTPRCP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPartida { get; set; }

        [DataNames("CTPRCP_A")]
        [SugarColumn(ColumnName = "CTPRCP_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo { get; set; }

        [DataNames("CTPRCP_CTPRC_ID")]
        [SugarColumn(ColumnName = "CTPRCP_CTPRC_ID", ColumnDescription = "indice de relacion con el catalogo de precios")]
        public int IdPrecio { get; set; }

        [DataNames("CTPRCP_CTESPC_ID")]
        [SugarColumn(ColumnName = "CTPRCP_CTESPC_ID", ColumnDescription = "indice de relacion con la tabla de especificaciones")]
        public int IdEspecificacion { get; set; }

        [DataNames("CTPRCP_UNITC")]
        [SugarColumn(ColumnName = "CTPRCP_UNITC", ColumnDescription = "costo unitario", DefaultValue = "0")]
        public decimal UnitarioC { get; set; }

        [DataNames("CTPRCP_UNIT")]
        [SugarColumn(ColumnName = "CTPRCP_UNIT", ColumnDescription = "valor unitario", IsNullable = false)]
        public decimal Unitario { get; set; }

        [SugarColumn(IsIgnore = true)]
        public decimal TasaIVA { get; set; }
    }
}
