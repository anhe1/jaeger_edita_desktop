﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Tienda.Entities {
    [SugarTable("CTPRCP", "tabla de partidas de la lista de precios")]
    public class PrecioPartidaModel : BasePropertyChangeImplementation, IComparable {
        #region declaraciones
        private int _Index;
        private bool _Activo;
        private int _IdPrecio;
        private int _IdEspecificacion;
        private decimal _UnitarioC;
        private decimal _Unitario;
        private string _Nota;
        private string _Creo;
        private string _Modifica;
        private DateTime _FechaNuevo;
        private DateTime? _FechaModifica;

        #endregion

        public PrecioPartidaModel() {
            this.Activo = true;
        }

        [DataNames("CTPRCP_ID")]
        [SugarColumn(ColumnName = "CTPRCP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPartida {
            get { return _Index; }
            set {
                _Index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_A")]
        [SugarColumn(ColumnName = "CTPRCP_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return _Activo; }
            set {
                _Activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_CTPRC_ID")]
        [SugarColumn(ColumnName = "CTPRCP_CTPRC_ID", ColumnDescription = "indice de relacion con el catalogo de precios")]
        public int IdPrecio {
            get { return _IdPrecio; }
            set {
                _IdPrecio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_CTESPC_ID")]
        [SugarColumn(ColumnName = "CTPRCP_CTESPC_ID", ColumnDescription = "indice de relacion con la tabla de especificaciones")]
        public int IdEspecificacion {
            get { return _IdEspecificacion; }
            set {
                this._IdEspecificacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_UNITC")]
        [SugarColumn(ColumnName = "CTPRCP_UNITC", ColumnDescription = "costo unitario", DefaultValue = "0")]
        public decimal UnitarioC {
            get { return _UnitarioC; }
            set {
                _UnitarioC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_UNIT")]
        [SugarColumn(ColumnName = "CTPRCP_UNIT", ColumnDescription = "valor unitario", IsNullable = false)]
        public decimal Unitario {
            get { return this._Unitario; }
            set {
                this._Unitario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_OBSRV")]
        [SugarColumn(ColumnName = "CTPRCP_OBSRV", ColumnDescription = "observaciones", IsNullable = true)]
        public string Nota {
            get { return _Nota; }
            set {
                _Nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_USU_N")]
        [SugarColumn(ColumnName = "CTPRC_USU_N", ColumnDescription = "clave del usuario creador del registro", IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return _Creo; }
            set {
                _Creo = value;
                OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_FN")]
        [SugarColumn(ColumnName = "CTPRC_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return _FechaNuevo; }
            set {
                _FechaNuevo = value;
                OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_USU_M")]
        [SugarColumn(ColumnName = "CTPRC_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return _Modifica; }
            set {
                _Modifica = value;
                OnPropertyChanged();
            }
        }

        [DataNames("CTPRCP_FM")]
        [SugarColumn(ColumnName = "CTPRC_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get { return _FechaModifica; }
            set {
                _FechaModifica = value;
                OnPropertyChanged();
            }
        }

        public int CompareTo(object obj) {
            return _IdPrecio.CompareTo(obj);
        }
    }
}
