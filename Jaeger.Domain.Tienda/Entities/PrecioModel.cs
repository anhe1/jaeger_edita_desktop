﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Tienda.Entities {
    [SugarTable("CTPRC", "ventas: catalogo de precios")]
    public class PrecioModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _Index;
        private bool _Activo;
        private int _Secuencia;
        private int _IdPrioridad;
        private string _Descripcion;
        private string _Nota;
        private DateTime? _FechaInicio;
        private DateTime? _FechaFin;
        private DateTime _FechaNuevo;
        private DateTime? _FechaModifica;
        private string _Creo;
        private string _Modifica;
        #endregion

        public PrecioModel() {
            this._Activo = true;
            this._FechaNuevo = DateTime.Now;
            this._FechaInicio = DateTime.Now;
            this._FechaFin = this._FechaInicio.Value.AddMonths(1);
        }

        /// <summary>
        /// obtener o establece indice de la tabla (CTPRC_ID)
        /// </summary>
        [DataNames("CTPRC_ID")]
        [SugarColumn(ColumnName = "CTPRC_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPrecio {
            get { return this._Index; }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("CTPRC_A")]
        [SugarColumn(ColumnName = "CTPRC_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer secuencia u orden en la lista
        /// </summary>
        [DataNames("CTPRC_SEC_ID")]
        [SugarColumn(ColumnName = "CTPRC_SEC_ID", ColumnDescription = "secuencia u orden")]
        public int Secuencia {
            get { return this._Secuencia; }
            set {
                this._Secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer prioridad de la lista
        /// </summary>
        [DataNames("CTPRC_STTS_ID")]
        [SugarColumn(ColumnName = "CTPRC_STTS_ID", ColumnDescription = "indice del status u prioridad")]
        public int IdPrioridad {
            get { return this._IdPrioridad; }
            set {
                this._IdPrioridad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la lista
        /// </summary>
        [DataNames("CTPRC_NOM")]
        [SugarColumn(ColumnName = "CTPRC_NOM", ColumnDescription = "nombre o descripcion", Length = 50, IsNullable = false)]
        public string Descripcion {
            get { return this._Descripcion; }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_FECINI)
        /// </summary>
        [DataNames("CTPRC_FECINI")]
        [SugarColumn(ColumnName = "CTPRC_FECINI", ColumnDescription = "fecha de inicio de vigencia", IsNullable = true)]
        public DateTime? FechaInicio {
            get { return _FechaInicio; }
            set {
                _FechaInicio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_FECVIG)
        /// </summary>
        [DataNames("CTPRC_FECFIN")]
        [SugarColumn(ColumnName = "CTPRC_FECFIN", ColumnDescription = "fecha final de la vigencia", IsNullable = true)]
        public DateTime? FechaFin {
            get { return _FechaFin; }
            set {
                _FechaFin = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_OBSRV)
        /// </summary>
        [DataNames("CTPRC_OBSRV")]
        [SugarColumn(ColumnName = "CTPRC_OBSRV", ColumnDescription = "observaciones", Length = 100)]
        public string Nota {
            get { return _Nota; }
            set {
                _Nota = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_USU_N)
        /// </summary>
        [DataNames("CTPRC_USU_N")]
        [SugarColumn(ColumnName = "CTPRC_USU_N", ColumnDescription = "clave del usuario creador del registro", IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return _Creo; }
            set {
                _Creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_FN)
        /// </summary>
        [DataNames("CTPRC_FN")]
        [SugarColumn(ColumnName = "CTPRC_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return _FechaNuevo; }
            set {
                _FechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_USU_M)
        /// </summary>
        [DataNames("CTPRC_USU_M")]
        [SugarColumn(ColumnName = "CTPRC_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return _Modifica; }
            set {
                _Modifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLPRCS_FM)
        /// </summary>
        [DataNames("CTPRC_FM")]
        [SugarColumn(ColumnName = "CTPRC_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get { return _FechaModifica; }
            set {
                _FechaModifica = value;
                OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag {
            get; set;
        }
    }
}
