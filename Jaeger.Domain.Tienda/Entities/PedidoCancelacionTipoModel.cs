﻿namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoCancelacionTipoModel : Base.Abstractions.BaseSingleModel {
        public PedidoCancelacionTipoModel() {

        }

        public PedidoCancelacionTipoModel(int id, string descripcion) {
            base.Id = id;
            base.Descripcion = descripcion;
        }
    }
}
