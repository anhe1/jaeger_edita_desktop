﻿using System.Globalization;
using System.Linq;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidosReportePrinter : PedidoClientePrinter {
        public PedidosReportePrinter() {

        }

        public PedidosReportePrinter(PedidoClienteDetailModel source) {
            MapperClassExtensions.MatchAndMap<PedidoClienteDetailModel, PedidosReportePrinter>(source, this);
        }

        public string Semana {
            get {
                DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                Calendar cal = dfi.Calendar;
                return string.Format("{0} - SEMANA {1} AL ", this.FechaPedido.Value.ToString("MMMM"), cal.GetWeekOfYear(this.FechaPedido.Value, dfi.CalendarWeekRule, dfi.FirstDayOfWeek), this.FechaPedido.Value.Day);
            }
        }

        public int DiasEntregado {
            get {
                if (this.FechaEntrega != null) {
                    return this.FechaEntrega.Value.Subtract(this.FechaPedido.Value).Days + 1;
                }
                return 0;
            }
        }

        public decimal TotalRemisionado {
            get {
                if (this.Remisiones != null) {
                    return this.Remisiones.Sum(it => it.Total);
                }
                return 0;
            }
        }

        public decimal Porcentaje {
            get {
                if (this.TotalRemisionado > 0 && this.Total > 0)
                    return this.TotalRemisionado / this.Total;
                return 0;
            }
        }

        public decimal TotalPorCobrarPactado {
            get {
                if (this.Remisiones != null) {
                    return this.Remisiones.Sum(it => it.PorCobrarPactado);
                }
                return 0;
            }
        }

        public decimal TotalAcumulado {
            get {
                if (this.Remisiones != null) {
                    return this.Remisiones.Sum(it => it.Acumulado);
                }
                return 0;
            }
        }
    }
}
