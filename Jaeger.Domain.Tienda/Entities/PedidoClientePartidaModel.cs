﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Tienda.Entities {
    ///<summary>
    ///rlcpdd
    ///</summary>
    public class PedidoClientePartidaModel : BasePropertyChangeImplementation, IDataErrorInfo {
        #region declaraciones
        private int _RLCPDD_ID;
        private int _RLCPDD_A;
        private int _RLCPDD_CATPRD_ID;
        private int _RLCPDD_CTLGPRDCTS_ID;
        private int _RLCPDD_CTLGMDLS_ID;
        private int _RLCPDD_TAM_ID;
        private int _RLCPDD_UNDD_ID;
        private decimal _RLCPDD_UNDD;
        private decimal _RLCPDD_CNTDD_PDD;
        private decimal _RLCPDD_UNTR;
        private decimal _RLCPDD_COST;
        private decimal _RLCPDD_SBTT;
        private string _RLCPDD_OBSRV;
        private string _RLCPDD_USU_N;
        private string _RLCPDD_USU_M;
        private DateTime _RLCPDD_FN;
        private DateTime? _RLCPDD_FM;
        private int _IdDirectorio;
        private int _IdPrecio;
        private int _IdPrecioP;
        private string _Unidad;
        private string _Catalogo;
        private string _Producto;
        private string _Modelo;
        private string _Marca;
        private string _Especificacion;
        private string _Tamanio;
        private decimal _CostoUnidad;
        private decimal _ValorUnitario;
        private decimal _Descuento;
        private decimal _Importe;
        private decimal _TrasladoIVA;
        private decimal _Total;
        private decimal _TasaIVA;
        private string _Identificador;
        #endregion

        public PedidoClientePartidaModel() {
            this.Activo = 1;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (RLCPDD_ID)
        /// </summary>
        [DataNames("PDCLP_ID")]
        [SugarColumn(ColumnName = "PDCLP_ID")]
        public int IdPartida {
            get { return this._RLCPDD_ID; }
            set {
                this._RLCPDD_ID = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer (RLCPDD_A)
        /// </summary>
        [DataNames("PDCLP_A")]
        [SugarColumn(ColumnName = "PDCLP_A")]
        public int Activo {
            get { return this._RLCPDD_A; }
            set {
                this._RLCPDD_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_CATPRD_ID)
        /// </summary>
        [DataNames("PDCLP_PDCLN_ID")]
        public int IdPedido {
            get { return this._RLCPDD_CATPRD_ID; }
            set {
                this._RLCPDD_CATPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLP_DRCTR_ID")]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLP_CTDPT_ID")]
        public int IdDepartamento { get; set; }

        [DataNames("PDCLP_CTALM_ID")]
        public int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer (RLCPDD_CTLGPRDCTS_ID)
        /// </summary>
        [DataNames("PDCLP_CTPRD_ID", "CTMDL_CTPRD_ID")]
        public int IdProducto {
            get { return this._RLCPDD_CTLGPRDCTS_ID; }
            set {
                this._RLCPDD_CTLGPRDCTS_ID = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer (RLCPDD_CTLGMDLS_ID)
        /// </summary>
        [DataNames("PDCLP_CTMDL_ID")]
        public int IdModelo {
            get { return this._RLCPDD_CTLGMDLS_ID; }
            set {
                this._RLCPDD_CTLGMDLS_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_TAM_ID)
        /// </summary>
        [DataNames("PDCLP_CTESPC_ID")]
        public int IdEspecificacion {
            get { return this._RLCPDD_TAM_ID; }
            set {
                this._RLCPDD_TAM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_UNDD_ID)
        /// </summary>
        [DataNames("PDCLP_CTUND_ID")]
        public int IdUnidad {
            get { return this._RLCPDD_UNDD_ID; }
            set {
                this._RLCPDD_UNDD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de precios asignado
        /// </summary>
        [DataNames("PDCLP_CTPRC_ID")]
        public int IdPrecio {
            get { return this._IdPrecio; }
            set {
                this._IdPrecio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de precios asignado (partida)
        /// </summary>
        [DataNames("PDCLP_CTPRCP_ID")]
        public int IdPrecioP {
            get { return this._IdPrecioP; }
            set {
                this._IdPrecioP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_UNDD)
        /// </summary>
        [DataNames("PDCLP_UNDF")]
        public decimal FactorUnidad {
            get { return this._RLCPDD_UNDD; }
            set {
                this._RLCPDD_UNDD = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre de la unidad
        /// </summary>
        [DataNames("PDCLP_UNDN")]
        public string Unidad {
            get { return this._Unidad; }
            set {
                this._Unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_CNTDD_PDD)
        /// </summary>
        [DataNames("PDCLP_CNTD")]
        public decimal Cantidad {
            get { return this._RLCPDD_CNTDD_PDD; }
            set {
                this._RLCPDD_CNTDD_PDD = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del catalogo de productos
        /// </summary>
        [DataNames("PDCLP_CTCLS")]
        public string Catalogo {
            get { return this._Catalogo; }
            set {
                this._Catalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del producto
        /// </summary>
        [DataNames("PDCLP_PRDN")]
        public string Producto {
            get { return this._Producto; }
            set {
                this._Producto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del modelo
        /// </summary>
        [DataNames("PDCLP_MDLN")]
        public string Modelo {
            get { return this._Modelo; }
            set {
                this._Modelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la marca
        /// </summary>
        [DataNames("PDCLP_MRC")]
        public string Marca {
            get { return this._Marca; }
            set {
                this._Marca = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la especificacion
        /// </summary>
        [DataNames("PDCLP_ESPC")]
        public string Especificacion {
            get { return this._Especificacion; }
            set {
                this._Especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del tamanio
        /// </summary>
        [DataNames("PDCLP_ESPN")]
        public string Tamanio {
            get { return this._Tamanio; }
            set {
                this._Tamanio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_COST$)
        /// </summary>
        [DataNames("PDCLP_UNTC")]
        public decimal CostoUnitario {
            get { return this._RLCPDD_COST; }
            set {
                this._RLCPDD_COST = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo de la unidad
        /// </summary>
        [DataNames("PDCLP_UNDC")]
        public decimal CostoUnidad {
            get { return this._CostoUnidad; }
            set {
                this._CostoUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario por pieza
        /// </summary>
        [DataNames("PDCLP_UNTR")]
        public decimal ValorUnitario {
            get { return this._ValorUnitario; }
            set {
                this._ValorUnitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_UNTR$)
        /// </summary>
        [DataNames("PDCLP_UNTR2")]
        public decimal Unitario {
            get { return this._RLCPDD_UNTR; }
            set {
                this._RLCPDD_UNTR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_SBTT$)
        /// </summary>
        [DataNames("PDCLP_SBTTL")]
        public decimal SubTotal {
            get { return this._RLCPDD_SBTT; }
            set {
                this._RLCPDD_SBTT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estsablecer el importe del descuento aplicable al producto o sevicio 
        /// </summary>
        [DataNames("PDCLP_DESC")]
        public decimal Descuento {
            get { return this._Descuento; }
            set {
                this._Descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del importe de la partida (SubTotal - Descuento)
        /// </summary>
        [DataNames("PDCLP_IMPRT")]
        public decimal Importe {
            get { return this._Importe; }
            set {
                this._Importe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tasa del IVA
        /// </summary>
        [DataNames("PDCLP_TSIVA")]
        public decimal TasaIVA {
            get { return this._TasaIVA; }
            set {
                this._TasaIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe trasladado del impuesto IVA
        /// </summary>
        [DataNames("PDCLP_TRIVA")]
        public decimal TrasladoIVA {
            get { return this._TrasladoIVA; }
            set {
                this._TrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de la partida (Importe + TrasladoIVA)
        /// </summary>
        [DataNames("PDCLP_TOTAL")]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("PDCLP_SKU")]
        public string Identificador {
            get { return this._Identificador; }
            set {
                this._Identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_OBSRV)
        /// </summary>
        [DataNames("PDCLP_NOTA")]
        public string Observaciones {
            get { return this._RLCPDD_OBSRV; }
            set {
                this._RLCPDD_OBSRV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_USU_N)
        /// </summary>
        [DataNames("PDCLP_USR_N")]
        public string Creo {
            get { return this._RLCPDD_USU_N; }
            set {
                this._RLCPDD_USU_N = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer (RLCPDD_USU_M)
        /// </summary>
        [DataNames("PDCLP_USR_M")]
        public string Modifica {
            get { return this._RLCPDD_USU_M; }
            set {
                this._RLCPDD_USU_M = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer (RLCPDD_FN)
        /// </summary>
        [DataNames("PDCLP_FN")]
        public DateTime FechaNuevo {
            get { return this._RLCPDD_FN; }
            set {
                this._RLCPDD_FN = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer (RLCPDD_FM)
        /// </summary>
        [DataNames("PDCLP_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._RLCPDD_FM >= new DateTime(1900, 1, 1))
                    return this._RLCPDD_FM;
                return null;
            }
            set {
                this._RLCPDD_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener la concatenacion de producto + modelo + tamanio
        /// </summary>
        public string Descriptor {
            get { return string.Format("{0} {1} {2}", this.Producto, this.Modelo, this.Tamanio); }
        }

        public string Error {
            get {
                if (this.IdUnidad == 0 | this.Cantidad <= 0)
                    return "Por favor ingrese datos válidos en esta fila!";
                return string.Empty;
            }
        }

        public string this[string columnName] {
            get {
                if (columnName == "IdUnidad" && this.IdUnidad == 0) {
                    return "Selecciona una unidad válida para este concepto";
                }
                if (columnName == "Cantidad" && this.Cantidad <= 0) {
                    return "Es necesario especificar la cantidad";
                }
                return string.Empty;
            }
        }
    }
}
