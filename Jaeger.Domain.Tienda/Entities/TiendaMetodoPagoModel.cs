﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// metodos de pago
    /// </summary>
    public class TiendaMetodoPagoModel : BasePropertyChangeImplementation, ITiendaMetodoPagoModel {
        #region declaraciones
        private int _TWMTDPG_ID;
        private bool _TWMTDPG_A;
        private int _TWMTDPG_PDCLN_ID;
        private int _TWMTDPG_DRCTR_ID;
        private string _TWMTDPG_MTD;
        private string _TWMTDPG_TRNSC_ID;
        private string _TWMTDPG_STTS;
        private string _TWMTDPG_PRFRNCID;
        private string _TWMTDPG_PYRID;
        private string _TWMTDPG_PYRNM;
        private string _TWMTDPG_TTL;
        private DateTime _FechaNuevo;
        private string _UsuarioConfirma;
        private DateTime? _FechaConfirmacion;
        private string _URL;
        #endregion

        public TiendaMetodoPagoModel() {
            _FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// Indice principal de la tabla
        /// </summary>
        [DataNames("TWMTDPG_ID")]
        [SugarColumn(ColumnName = "TWMTDPG_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return _TWMTDPG_ID; }
            set {
                _TWMTDPG_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Registro activo
        /// </summary>
        [DataNames("TWMTDPG_A")]
        [SugarColumn(ColumnName = "TWMTDPG_A", DefaultValue = "1")]
        public bool Activo {
            get { return _TWMTDPG_A; }
            set {
                _TWMTDPG_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Indice de la tabla de pedido del cliente
        /// </summary>
        [DataNames("TWMTDPG_PDCLN_ID")]
        [SugarColumn(ColumnName = "TWMTDPG_PDCLN_ID")]
        public int IdPedido {
            get { return _TWMTDPG_PDCLN_ID; }
            set {
                _TWMTDPG_PDCLN_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Indice de la tabla de directorio
        /// </summary>
        [DataNames("TWMTDPG_DRCTR_ID")]
        [SugarColumn(ColumnName = "TWMTDPG_DRCTR_ID")]
        public int IdCliente {
            get { return _TWMTDPG_DRCTR_ID; }
            set {
                _TWMTDPG_DRCTR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Metodo de pago: Deposito | MercadoPago | Paypal
        /// </summary>
        [DataNames("TWMTDPG_MTD")]
        [SugarColumn(ColumnName = "TWMTDPG_MTD")]
        public string Metodo {
            get { return _TWMTDPG_MTD; }
            set {
                _TWMTDPG_MTD = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Identificador de la transaccion
        /// </summary>
        [DataNames("TWMTDPG_TRNSC_ID")]
        [SugarColumn(ColumnName = "TWMTDPG_TRNSC_ID")]
        public string IdTransaccion {
            get { return _TWMTDPG_TRNSC_ID; }
            set {
                _TWMTDPG_TRNSC_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Estatus de la transaccion
        /// </summary>
        [DataNames("TWMTDPG_STTS")]
        [SugarColumn(ColumnName = "TWMTDPG_STTS")]
        public string Status {
            get { return _TWMTDPG_STTS; }
            set {
                _TWMTDPG_STTS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Identificador de la preferencia (MercadoPago)
        /// </summary>
        [DataNames("TWMTDPG_PRFRNCID")]
        [SugarColumn(ColumnName = "TWMTDPG_PRFRNCID")]
        public string IdPreferencia {
            get { return _TWMTDPG_PRFRNCID; }
            set {
                _TWMTDPG_PRFRNCID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Identificador del comprador
        /// </summary>
        [DataNames("TWMTDPG_PYRID")]
        [SugarColumn(ColumnName = "TWMTDPG_PYRID")]
        public string IdComprador {
            get { return _TWMTDPG_PYRID; }
            set {
                _TWMTDPG_PYRID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombre del comprador
        /// </summary>
        [DataNames("TWMTDPG_PYRNM")]
        [SugarColumn(ColumnName = "TWMTDPG_PYRNM")]
        public string Comprador {
            get { return _TWMTDPG_PYRNM; }
            set {
                _TWMTDPG_PYRNM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Descripcion de la compra
        /// </summary>
        [DataNames("TWMTDPG_TTL")]
        [SugarColumn(ColumnName = "TWMTDPG_TTL")]
        public string GTotal {
            get { return _TWMTDPG_TTL; }
            set {
                _TWMTDPG_TTL = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWMTDPG_FILE")]
        [SugarColumn(ColumnName = "TWMTDPG_FILE")]
        public string URL {
            get { return this._URL; }
            set { this._URL = value;
            this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave del usuario que confirma 
        /// </summary>
        [DataNames("TWMTDPG_CUSER")]
        [SugarColumn(ColumnName = "TWMTDPG_CUSER")]
        public string Confirma {
            get { return this._UsuarioConfirma; }
            set { this._UsuarioConfirma = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de confirmacion del pago
        /// </summary>
        [DataNames("TWMTDPG_CFECHA")]
        [SugarColumn(ColumnName = "TWMTDPG_CFECHA")]
        public DateTime? FConfirma {
            get { return this._FechaConfirmacion; }
            set { this._FechaConfirmacion = value;
            this.OnPropertyChanged();}
        }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [DataNames("TWMTDPG_FN")]
        [SugarColumn(ColumnName = "TWMTDPG_FN")]
        public DateTime FechaNuevo {
            get { return _FechaNuevo; }
            set {
                _FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
