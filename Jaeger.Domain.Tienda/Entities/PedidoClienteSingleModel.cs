﻿using System.ComponentModel;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClienteSingleModel : PedidoClienteDetailModel, IDataErrorInfo, IPedidoClienteDetailModel, IPedidoClienteStatusModel {
        public PedidoClienteSingleModel() : base() { }
    }
}
