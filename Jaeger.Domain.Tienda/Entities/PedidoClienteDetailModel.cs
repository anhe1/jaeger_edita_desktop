﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Tienda.ValueObjects;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClienteDetailModel : PedidoClienteModel, IDataErrorInfo, IPedidoClienteDetailModel, IPedidoClienteStatusModel {
        private int _IdMotivoCancelacion;
        private string _MotivoCancelacion;
        private string _Cancela;
        private DateTime? _PDCLN_FM;
        private BindingList<PedidoClientePartidaDetailModel> productos;
        private BindingList<IPedidoClienteStatusModel> autorizaciones;
        private BindingList<IPedidoClienteRelacionadoModel> relaciones;

        public PedidoClienteDetailModel() {
            this.productos = new BindingList<PedidoClientePartidaDetailModel>();
            //this.autorizaciones = new BindingList<Empresa.Entities.AutorizacionModel>();
        }

        [DataNames("DRCTR_CLV")]
        [SugarColumn(IsIgnore = true)]
        public string ClienteClave { get; set; }

        [DataNames("DRCTR_NOM")]
        [SugarColumn(IsIgnore = true)]
        public string Cliente { get; set; }

        [SugarColumn(IsIgnore = true)]
        public bool Editable {
            get { return this.IdPedido == 0; }
        }

        /// <summary>
        /// obtener o establecer status del pedido
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public PedidoClienteStatusEnum Status {
            get { return (PedidoClienteStatusEnum)this.IdStatus; }
            set { this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_CLMTV_ID")]
        [SugarColumn(IsIgnore = true)]
        public int IdCvMotivo {
            get { return this._IdMotivoCancelacion; }
            set {
                this._IdMotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_CLMTV")]
        [SugarColumn(IsIgnore = true)]
        public string CvMotivo {
            get { return this._MotivoCancelacion; }
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_USU_M")]
        [SugarColumn(IsIgnore = true)]
        public string Cancela {
            get { return this._Cancela; }
            set {
                this._Cancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLS_FM")]
        [SugarColumn(IsIgnore = true)]
        public DateTime? FechaStatus {
            get {
                if (this._PDCLN_FM >= new DateTime(1900, 1, 1))
                    return this._PDCLN_FM;
                return null;
            }
            set {
                this._PDCLN_FM = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<PedidoClientePartidaDetailModel> Partidas {
            get { return this.productos; }
            set {
                this.productos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer listado de autorizaciones
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<IPedidoClienteStatusModel> Autorizaciones {
            get {
                return this.autorizaciones;
            }
            set {
                this.autorizaciones = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<IPedidoClienteRelacionadoModel> Relaciones {
            get { return this.relaciones; }
            set { this.relaciones = value;
            this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if ((this.IdDirectorio == 0 | this.IdDireccion <= 0 | this.IdVendedor <= 0) && this.Status == PedidoClienteStatusEnum.Por_Surtir)
                    return "Existen errores de captura para este pedido.";

                var _error = this.Partidas.Where(it => it.Error != string.Empty);
                if (_error.Count() > 0) {
                    return "Existe un error en las partidas de este documento.";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "IdDirectorio" && this.IdDirectorio == 0) {
                    return "Falta seleccionar al cliente.";
                }
                if (columnName == "IdDireccion" && this.IdDireccion <= 0) {
                    return "Es necesario especificar la dirección de envío";
                }

                if (columnName == "IdVendedor" && this.IdVendedor <= 0) {
                    return "No se selecciono al vendedor.";
                }
                return string.Empty;
            }
        }
    }
}
