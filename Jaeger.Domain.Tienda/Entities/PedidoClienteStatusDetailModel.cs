﻿using Jaeger.Domain.Tienda.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClienteStatusDetailModel : PedidoClienteStatusModel, IPedidoClienteStatusDetailModel {
        private BindingList<IPedidoClienteRelacionadoModel> _Relaciones;

        public PedidoClienteStatusDetailModel() : base() {
            this._Relaciones = new BindingList<IPedidoClienteRelacionadoModel>();
        }
        public BindingList<IPedidoClienteRelacionadoModel> Relaciones {
            get { return this._Relaciones; }
            set { this._Relaciones = value;
            this.OnPropertyChanged();
            }
        }
    }
}
