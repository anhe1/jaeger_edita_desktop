﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// cupones de descuento
    /// </summary>
    [SugarTable("TWCPN", "tw: cupones de descuento")]
    public class CuponModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private string descripcion;
        private string clave;
        private DateTime fechaInicio;
        private DateTime fechaFin;
        private decimal valor;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string _Nombre;
        private int _UsosMaximos;
        private int _UsoActual;
        private decimal _MinmoCompra;
        #endregion

        public CuponModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        [DataNames("TWCPN_ID")]
        [SugarColumn(ColumnName = "TWCPN_ID", ColumnDescription = "identificador principal", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCupon {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_A")]
        [SugarColumn(ColumnName = "TWCPN_A", ColumnDescription = "obtener o establecer si el registro activo del registro", Length = 1, DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_DESC")]
        [SugarColumn(ColumnName = "TWCPN_DESC", ColumnDescription = "obtener o establecer la descripcion", Length = 255, IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_NOM")]
        [SugarColumn(ColumnName = "TWCPN_NOM", ColumnDescription = "", Length = 32)]
        public string Nombre {
            get { return this._Nombre; }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_clv")]
        [SugarColumn(ColumnName = "TWCPN_CLV", ColumnDescription = "obtener o establecer la clave del cupon", Length = 16)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_VGINI")]
        [SugarColumn(ColumnName = "TWCPN_VGINI", ColumnDescription = "obtener o establecer la fecha de inicio de vigencia", IsNullable = true)]
        public DateTime FechaInicio {
            get {
                return this.fechaInicio;
            }
            set {
                this.fechaInicio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_VGTER")]
        [SugarColumn(ColumnName = "TWCPN_VGTER", ColumnDescription = "obtener o establecer la fecha de fin de vigencia", IsNullable = true)]
        public DateTime FechaFin {
            get {
                return this.fechaFin;
            }
            set {
                this.fechaFin = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_DESCNT")]
        [SugarColumn(ColumnName = "TWCPN_DESCNT", ColumnDescription = "obtener o establecer el valor del descuento aplicable", Length = 14, DecimalDigits = 4, DefaultValue = "0.0")]
        public decimal Valor {
            get {
                return this.valor;
            }
            set {
                this.valor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_UMAX")]
        [SugarColumn(ColumnName = "TWCPN_UMA", ColumnDescription = "")]
        public int UsosMaximos {
            get { return this._UsosMaximos; }
            set { this._UsosMaximos = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_UACT")]
        [SugarColumn(ColumnName = "TWCPN_UACT")]
        public int UsoActual {
            get { return this._UsoActual; }
            set { this._UsoActual = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_MIN")]
        [SugarColumn(ColumnName = "TWCPN_MIN")]
        public  decimal MinmoCompra {
            get { return this._MinmoCompra; }
            set { this._MinmoCompra = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_USR_N")]
        [SugarColumn(ColumnName = "TWCPN_USR_N", ColumnDescription = "obtener o establecr la clave o correo electronico del usuario que crea el registro", IsNullable = true, Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_FN")]
        [SugarColumn(ColumnName = "TWCPN_FN", ColumnDescription = "obtener o establecer la fecha de creacion del registro", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_USR_M")]
        [SugarColumn(ColumnName = "TWCPN_USR_M", ColumnDescription = "obtener o establecer la clave o correo electronico del ultimo usuario que modifica", IsNullable = true, Length = 10)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWCPN_FM")]
        [SugarColumn(ColumnName = "TWCPN_FM", ColumnDescription = "obtener o establecer la ultima fecha de modificacion", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                return this.fechaModifica;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
