﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClienteRelacionadoModel : BasePropertyChangeImplementation, IPedidoClienteRelacionadoModel {
        #region
        private string _Relacion;
        private DateTime _FechaNuevo;
        private string _Creo;
        private decimal _GTotal;
        private DateTime _FechaEmision;
        private string _ReceptorNombre;
        private int _Folio;
        private int _IdPedido;
        private int _IdTipoRelacion;
        private int _IdDirectorio;
        #endregion

        public PedidoClienteRelacionadoModel() { }

        [DataNames("PDCLR_PDCLN_ID")]
        public int IdPedido {
            get {
                return this._IdPedido;
            }
            set {
                this._IdPedido = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLR_DRCTR_ID")]
        public int IdDirectorio {
            get {
                return this._IdDirectorio;
            }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLR_CTREL_ID")]
        public int IdRelacion {
            get { return this._IdTipoRelacion; }
            set {
                this._IdTipoRelacion = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("PDCLR_RELN")]
        public string Relacion {
            get { return this._Relacion; }
            set {
                this._Relacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLR_FOLIO")]
        public int Folio {
            get {
                return this._Folio;
            }
            set {
                this._Folio = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("PDCLR_NOMR")]
        public string Cliente {
            get {
                return this._ReceptorNombre;
            }
            set {
                this._ReceptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLR_FECEMS")]
        public DateTime FechaEmision {
            get {
                return this._FechaEmision;
            }
            set {
                this._FechaEmision = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("PDCLR_TOTAL")]
        public decimal GTotal {
            get {
                return this._GTotal;
            }
            set {
                this._GTotal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PDCLR_USR_N")]
        public string Creo {
            get {
                return this._Creo;
            }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("PDCLR_FN")]
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
