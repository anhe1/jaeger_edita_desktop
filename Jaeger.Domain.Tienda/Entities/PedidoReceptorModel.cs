﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoReceptorModel : Base.Abstractions.BasePropertyChangeImplementation, IPedidoReceptorModel {
        private int _IdDirectorio;
        private string _RFC;
        private string _Nombre;

        public PedidoReceptorModel() { }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id", "DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("_drctr_rfc", "DRCTR_RFC")]
        [SugarColumn(ColumnName = "_drctr_rfc", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsNullable = false)]
        public string RFC {
            get { return this._RFC; }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public string Nombre {
            get { return this._Nombre; }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }
    }
}
