﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// tienda web: preguntas frecuentes
    /// </summary>
    [SugarTable("TWFAQ", "tw: preguntas frecuentas")]
    public class PreguntaFrecuenteModel : BasePropertyChangeImplementation, IDataErrorInfo {
        #region declaraciones
        private int index;
        private bool activo;
        private string pregunta;
        private string respuesta;
        private DateTime fechaNuevo;
        private string _Creo;
        #endregion

        public PreguntaFrecuenteModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("TWFAQ_ID")]
        [SugarColumn(ColumnName = "TWFAQ_ID", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("TWFAQ_A")]
        [SugarColumn(ColumnName = "TWFAQ_A", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer contenido de la pregunta
        /// </summary>
        [DataNames("TWFAQ_QSTN")]
        [SugarColumn(ColumnName = "TWFAQ_QSTN", ColumnDescription = "pregunta", Length = 255)]
        public string Pregunta {
            get {
                return this.pregunta;
            }
            set {
                this.pregunta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer contenido de la respuesta
        /// </summary>
        [DataNames("TWFAQ_ANSWR")]
        [SugarColumn(ColumnName = "TWFAQ_ANSWR", ColumnDescription = "respuesta", Length = 8000)]
        public string Respuesta {
            get {
                return this.respuesta;
            }
            set {
                this.respuesta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de registro del sistema
        /// </summary>
        [DataNames("TWFAQ_FN")]
        [SugarColumn(ColumnName = "TWFAQ_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("TWFAQ_USU_N")]
        [SugarColumn(ColumnName = "TWFAQ_USU_N", ColumnDescription = "clave del usuario", Length = 10)]
        public string Creo {
            get { return this._Creo; }
            set { this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (!(this.Pregunta != null ? !(this.Pregunta.Trim() == "") : false) || !(this.Respuesta != null ? !(this.Respuesta.Trim() == "") : false))
                    return "Por favor ingrese datos válidos en esta fila!";
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "pregunta" && !(this.Pregunta != null))
                    return "Debes ingresar un concepto.";

                if (columnName == "Nombre" && !(this.Pregunta != null ? !(this.Pregunta.Trim() == "") : false))
                    return "Debe ingresar un concepto.";

                if (columnName == "Respuesta" && !(this.Respuesta != null ? !(this.Respuesta.Trim() == "") : false))
                    return "Es necesario agregsar una respuesta";

                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron) {
            if (!(valor == null)) {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
    }
}
