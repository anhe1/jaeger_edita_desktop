﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Domain.Tienda.Entities {
    public class PedidoClientePrinter : PedidoClienteDetailModel, IPedidoClientePrinter {
        private List<RemisionDetailModel> _Remisiones;

        public PedidoClientePrinter() : base() {
            this.IsCosto = true;
        }

        public PedidoClientePrinter(PedidoClienteDetailModel source) {
            MapperClassExtensions.MatchAndMap<PedidoClienteDetailModel, PedidoClientePrinter>(source, this);
        }

        public string StatusText {
            get { return this.Status.Description(); }
        }

        public List<RemisionDetailModel> Remisiones {
            get { return this._Remisiones; }
            set { this._Remisiones = value; }
        }

        public bool IsCosto {
            get; set;
        }

        public string Embarque {
            get; set;
        }

        /// <summary>
        /// obtener la cantidad en letra
        /// </summary>
        public string TotalEnLetra {
            get {
                return NumeroALetras.Convertir((double)this.Total, 1);
            }
        }

        /// <summary>
        /// informacion para QR
        /// </summary>
        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.IdPedido.ToString("#000000"), "Fecha=", this.FechaPedido.Value.ToString("dd-mm-yyyy"), "Cliente=", this.Cliente, "Vendedor", this.IdVendedor.ToString(), "Articulos=", this.Partidas.Count.ToString() };
            }
        }
    }
}
