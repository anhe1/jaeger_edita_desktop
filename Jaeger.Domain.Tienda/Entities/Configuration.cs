﻿namespace Jaeger.Domain.Tienda.Entities {
    /// <summary>
    /// clase para configuraciones
    /// </summary>
    public class Configuration : Base.Abstractions.Configuration, Contracts.IConfiguration, Base.Contracts.IConfiguration {
        private DataBase.Contracts.IDataBaseConfiguracion _DataBase;

        /// <summary>
        /// constructor
        /// </summary>
        public Configuration() {
            this.ModoProductivo = false;
            this.Folder = "tienda";
        }

        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        public bool ModoProductivo { get; set; }

        /// <summary>
        /// obtener o establecer folder utilizado en el bucket de amazon
        /// </summary>
        public string Folder { get; set; }

        /// <summary>
        /// obtener o establecer configuración de base de datos
        /// </summary>
        public DataBase.Contracts.IDataBaseConfiguracion DataBase {
            get {
                return _DataBase;
            }
            set {
                this._DataBase = value;
                this.OnPropertyChanged();
            }
        }
    }
}
