﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Jaeger.Aplication.Contribuyentes {
    public abstract class SociedadMercantil {
        protected readonly Dictionary<string, string> variantes = new Dictionary<string, string>();
        protected internal string nombreRazonSocial = "";
        protected internal string sociedadM;

        public SociedadMercantil() {
            this.variantes.Add(" SAB DE CV SOFOM ENR", "SAB DE CV SOFOM ENR");
            this.variantes.Add(" SPR DE RI DE CV", "SPR DE RI DE CV");
            this.variantes.Add(" SA DE RL DE CV", "SA DE RL DE CV");
            this.variantes.Add(" SC DE RL DE CV", "SC DE RL DE CV");
            this.variantes.Add(" S DE R L DE CV", "S DE R L DE CV");
            this.variantes.Add(" S DE RL DE CV", "S DE RL DE CV");
            this.variantes.Add(" S DE PR DE RL", "S DE PR DE RL");
            this.variantes.Add(" SAPI DE CV", "SAPI DE CV");
            this.variantes.Add(" S DE RL CV", "S DE RL CV");
            this.variantes.Add(" S DE RL MI", "S DE RL MI");
            this.variantes.Add(" S DE R L MI", "S DE RL MI");
            this.variantes.Add(" S A DE C V", "SA DE CV");
            this.variantes.Add(" S A C V", "SA DE CV");
            
            this.variantes.Add(" SA B DE CV", "SA B DE CV");
            this.variantes.Add(" SAS DE CV", "SAS DE CV");
            this.variantes.Add(" SCL DE CV", "SCL DE CV");
            this.variantes.Add(" SPR DE RL", "SPR DE RL");
            this.variantes.Add(" SRL DE CV", "SRL DE CV");
            this.variantes.Add(" SPR DE RI", "SPR DE RI");
            this.variantes.Add(" SA DE CV", "SA DE CV");
            this.variantes.Add(" SADE CV", "SADE CV");
            this.variantes.Add(" SA DECV", "SA DECV");
            this.variantes.Add(" S DE RL", "S DE RL");
            this.variantes.Add(" A EN P", "A EN P");
            this.variantes.Add(" SCI SC", "SCI SC");
            this.variantes.Add(" SCISC", "SCISC");
            this.variantes.Add(" SAPI", "SAPI");
            this.variantes.Add(" EPS", "EPS");
            this.variantes.Add(" SCL", "SCL");
            this.variantes.Add(" SCP", "SCP");
            this.variantes.Add(" SAB", "SAB");
            this.variantes.Add(" IAP", "IAP");
            this.variantes.Add(" AC", "AC");
            this.variantes.Add(" S C", "SC");
            this.variantes.Add(" SC", "SC");
            this.variantes.Add(" S A", "SA");
            this.variantes.Add(" SA", "SA");
            this.variantes.Add(" AR", "AR");
            this.variantes.Add(" S C DE R L DE C V", "SC DE RL DE CV");
        }

        public string RazonSocial {
            get { return nombreRazonSocial; }
        }

        public string Sociedad {
            get { return sociedadM; }
        }

        /// <summary>
        /// Remove diacritics, ex:
        // "ÁÂÃÄÅÇÈÉàáâãäåèéêëìíîïòóôõ" ====>> "AAAAACEEaaaaaaeeeeiiiioooo"
        /// </summary>
        /// <param name="word">target word</param>
        /// <returns>result word</returns>
        protected internal string StripAccents(string inputString) {
            string normal = inputString.Normalize(NormalizationForm.FormD);
            var converted = normal.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark);
            return new string(converted.ToArray());
        }
    }

}
