﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Jaeger.Aplication.Contribuyentes {

    public class SociedadMercantilBuilder : SociedadMercantil, ISociedadMercantilBuilder {

        public SociedadMercantilBuilder() : base() {
        }

        public ISociedadMercantilBuilder WithRazonSocial(string razonSocial) {
            sociedadM = "";
            nombreRazonSocial = "";
            razonSocial = razonSocial.ToUpper();
            if (Regex.IsMatch(razonSocial, "[.,:;{}[\\]´~*#&%$]+")) {
                razonSocial = Regex.Replace(razonSocial, "[.,:;{}[\\]´~*#&%$]+", "");
            }

            var razonSocialNormalizado = StripAccents(razonSocial).ToUpper();
            razonSocialNormalizado = razonSocialNormalizado.Replace(",", " ");
            razonSocialNormalizado = razonSocialNormalizado.Replace(".", "");
            razonSocialNormalizado = razonSocialNormalizado.Replace("  ", " ");
            razonSocialNormalizado = razonSocialNormalizado.Replace("  ", " ");
            razonSocialNormalizado = razonSocialNormalizado.Replace(",", "");
            razonSocialNormalizado = razonSocialNormalizado.Replace("\"", "");

            var result = this.variantes.Where(it => razonSocialNormalizado.Contains(it.Key));

            if (result.Count() > 0) {
                foreach (var item in result) {
                    var num = razonSocialNormalizado.LastIndexOf(item.Key);
                    sociedadM = item.Value;
                    nombreRazonSocial = razonSocialNormalizado.Substring(0, num);
                    var d0 = razonSocialNormalizado.Replace(nombreRazonSocial, "");
                    var d1 = d0.Replace(" ", "");
                    if (d1 == sociedadM.Replace(" ", "")) {
                        break;
                    } else {
                        sociedadM = "";
                        nombreRazonSocial = razonSocialNormalizado;
                    }
                }
            } else {
                sociedadM = "";
                nombreRazonSocial = razonSocialNormalizado;
            }
            return this;
        }

        public override string ToString() {
            return string.Format("Denominación ó Razón Social: {0}\r\nSociedad Mercantil: {1}", this.RazonSocial, this.sociedadM);
        }
    }
}
