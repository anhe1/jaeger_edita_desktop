﻿namespace Jaeger.Aplication.Contribuyentes {
    public interface ISociedadMercantilBuilder {
        ISociedadMercantilBuilder WithRazonSocial(string razonSocial);

        string RazonSocial { get; }

        string Sociedad { get; }
    }

}
