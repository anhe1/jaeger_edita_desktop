﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Aplication.Contribuyentes.Builder {
    public class RelacionComercialBuilder : IRelacionComercialBuilder {
        /// <summary>
        /// obtener la lista de relaciones comerciales
        /// </summary>
        public List<IRelacionComercialDetailModel> Get(BindingList<IRelacionComercialDetailModel> relaciones) {
            var relacionesComerciales = ((TipoRelacionComericalEnum[])Enum.GetValues(typeof(TipoRelacionComericalEnum)))
                .Select(c => new RelacionComercialDetailModel((int)c, true, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description)).ToList();

            var response = from p in relacionesComerciales
                              join co in relaciones on p.IdTipoRelacion equals co.IdTipoRelacion into _resultado
                              from result in _resultado.DefaultIfEmpty()
                              select new RelacionComercialDetailModel {
                                  Activo = (result != null ? result.Activo : false),
                                  IdTipoRelacion = (result != null ? result.IdTipoRelacion : p.IdTipoRelacion),
                                  Selected = (result != null ? (result.IdTipoRelacion > 0 && result.IdDirectorio > 0) : false),
                                  IdDirectorio = (result != null ? result.IdDirectorio : 0),
                                  FechaModifica = (result != null ? result.FechaModifica : null),
                                  Modifica = (result != null ? result.Modifica : null),
                                  Nota = (result != null ? result.Nota : "")
                              };
            return response.ToList<IRelacionComercialDetailModel>();
        }

        public BindingList<IRelacionComercialDetailModel> Set(BindingList<IRelacionComercialDetailModel> relaciones) {
            var response = new BindingList<IRelacionComercialDetailModel>();
            foreach (var relacion in relaciones) {
                var item = new RelacionComercialDetailModel {
                    Activo = relacion.Selected,
                    FechaModifica = relacion.FechaModifica,
                    Nota = relacion.Nota,
                    IdDirectorio = relacion.IdDirectorio,
                    Modifica = relacion.Modifica,
                    IdTipoRelacion = relacion.IdTipoRelacion,
                    IdComision = relacion.IdComision
                };
                if (relacion.Selected == true) {
                    response.Add(item);
                } else if (relacion.Selected == false && relacion.IdDirectorio > 0) {
                    response.Add(item);
                }
            }
            return response;
        }
    }
}
