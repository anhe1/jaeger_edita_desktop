﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Contribuyentes.Builder {
    public interface IContribuyenteBuilder {
        IContribuyenteDetailModel GetNew(TipoRelacionComericalEnum relacion = TipoRelacionComericalEnum.None);
    }
}
