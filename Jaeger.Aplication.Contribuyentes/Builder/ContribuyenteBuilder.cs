﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Aplication.Contribuyentes.Builder {
    public class ContribuyenteBuilder : IContribuyenteBuilder {
        /// <summary>
        /// obtener objeto nuevo con relacion 
        /// </summary>
        public virtual IContribuyenteDetailModel GetNew(TipoRelacionComericalEnum relacion = TipoRelacionComericalEnum.None) {
            var item = new ContribuyenteDetailModel { };
            return item;
        }
    }
}
