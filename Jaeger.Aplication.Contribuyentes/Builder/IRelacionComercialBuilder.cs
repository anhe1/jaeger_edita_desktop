﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Contribuyentes.Builder {
    public interface IRelacionComercialBuilder {
        List<IRelacionComercialDetailModel> Get(BindingList<IRelacionComercialDetailModel> relaciones);
        BindingList<IRelacionComercialDetailModel> Set(BindingList<IRelacionComercialDetailModel> relaciones);
    }
}
