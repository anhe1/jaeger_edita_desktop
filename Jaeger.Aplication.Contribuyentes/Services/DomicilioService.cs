﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Contribuyentes.Services {
    public class DomicilioService : IDomicilioService {
        public static List<DomicilioTipoModel> GetTipoDomicilio() {
            return EnumerationExtension.GetEnumToClass<DomicilioTipoModel, TipoDomicilioEnum>().ToList();
        }
    }
}
