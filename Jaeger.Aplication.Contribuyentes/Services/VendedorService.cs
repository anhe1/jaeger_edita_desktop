﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.DataAccess.Contribuyentes.Repositories;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Aplication.Contribuyentes.Services {
    /// <summary>
    /// Vendedor
    /// </summary>
    public class VendedorService : IVendedorService {
        protected ISqlVendedorRepository vendedorRepository;
        protected ISqlCarteraRepository carteraRepository;

        /// <summary>
        /// constructor
        /// </summary>
        public VendedorService() {
            this.OnLoad();
        }

        protected virtual void OnLoad() {
            this.vendedorRepository = new SqlSugarVendedorRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.carteraRepository = new SqlSugarVendedorCarteraRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener vendedor
        /// </summary>
        public virtual IVendedor2DetailModel GetById(int index) {
            var response = this.vendedorRepository.GetList<Vendedor2DetailModel>(new List<IConditional> {
                new Conditional("DRCTRR_DRCTR_ID", index.ToString())
            }).FirstOrDefault();

            if (response != null) {
                response.Clientes = new BindingList<IContribuyenteVendedorModel>(this.carteraRepository.GetClientes<ContribuyenteVendedorModel>(new List<IConditional> { new Conditional("DRCTRC_VNDR_ID", index.ToString()) }).ToList<IContribuyenteVendedorModel>());
            }
            return response;
        }

        /// <summary>
        /// almacenar
        /// </summary>
        public virtual IVendedor2DetailModel Saveable(IVendedor2DetailModel vendedor) {
            if (this.vendedorRepository.Saveable(vendedor)) {
                // clientes relacionados
                for (int i = 0; i < vendedor.Clientes.Count; i++) {
                    if (vendedor.Clientes[i].SetModified)
                        this.carteraRepository.Salveable(vendedor.Clientes[i]);
                }
            }
            return vendedor;
        }
    }
}
