﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.DataAccess.Contribuyentes.Repositories;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Contribuyentes.Services {
    public class ContribuyenteToService : ContribuyenteService, IContribuyenteService {
        public ContribuyenteToService() : base() {
            this.relacionComercial = TipoRelacionComericalEnum.None;
        }

        public ContribuyenteToService(TipoRelacionComericalEnum relacionComercial) : base() {
            this.relacionComercial = relacionComercial;
        }

        protected internal override void OnLoad() {
            this.contribuyenteRepository = new SqlContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.domicilioRepository = new SqlDomicilioRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.cuentaBancariaRepository = new SqlCuentaBancariaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.relacionComercialRepository = new SqlRelacionComercialRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.contactoRepository = new SqlSugarContactoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener objeto complejo contribuyente
        /// </summary>
        public override IContribuyenteDetailModel GetById(int index) {
            var d1 = this.contribuyenteRepository.GetById(index);
            // parche en EDITA
            //d1.Relaciones = this.SetRelacion(d1.Relacion, d1.IdDirectorio);
            //if (d1.Relaciones.Count == 0) {
            //    d1.Relaciones = this.SetRelacion("", d1.IdDirectorio);
            //}
            d1.Relaciones = new BindingList<IRelacionComercialDetailModel>(this.GetRelacionesComerciales(d1.Relaciones));
            return d1;
        }

        /// <summary>
        /// 
        /// </summary>
        public override IContribuyenteDetailModel GetByRFC(string rfc) {
            var d1 = this.contribuyenteRepository.GetByRFC(rfc);
            if (d1 != null) {
                // parche en EDITA
                d1.Relaciones = this.SetRelacion(d1.Relacion, d1.IdDirectorio);
                if (d1.Relaciones.Count == 0) {
                    d1.Relaciones = this.SetRelacion("", d1.IdDirectorio);
                }
            }
            return d1;
        }

        /// <summary>
        /// almacenar un contribuyente
        /// </summary>
        public override IContribuyenteDetailModel Save(IContribuyenteDetailModel model) {

            #region parche de relaciones con EDITA
            var rel = this.SetRelacionesComerciales(model.Relaciones);
            model.Relacion = string.Join(",", rel.Where(it => it.Activo).Select(it => it.Name).ToArray());
            var response = this.contribuyenteRepository.Save(model);

            // relaciones comerciales
            for (int i = 0; i < rel.Count; i++) {
                rel[i].IdDirectorio = model.IdDirectorio;
                this.relacionComercialRepository.Saveable(rel[i]);
            }
            model.Relaciones = this.SetRelacion(model.Relacion, model.IdDirectorio);
            #endregion

            //// domicilios
            //for (int i = 0; i < response.Domicilios.Count; i++) {
            //    response.Domicilios[i].IdContribuyente = response.IdDirectorio;
            //    response.Domicilios[i] = this.domicilioRepository.Save(response.Domicilios[i]);
            //}
            //// cuentas bancarias
            //for (int i = 0; i < response.CuentasBancarias.Count; i++) {
            //    response.CuentasBancarias[i].IdDirectorio = response.IdDirectorio;
            //    response.CuentasBancarias[i] = this.cuentaBancariaRepository.Save(response.CuentasBancarias[i]);
            //}
            //// contactos
            //for (int i = 0; i < response.Contactos.Count; i++) {
            //    response.Contactos[i].IdDirectorio = response.IdDirectorio;
            //    response.Contactos[i] = this.contactoRepository.Save(response.Contactos[i]);
            //}
            return response;
        }

        //public BindingList<IRelacionComercialDetailModel> GetRelacion(object index, bool onlyActive = true) {
        //    var condiciones = new List<IConditional>();

        //    if (index.GetType() == typeof(int)) {
        //        condiciones.Add(new Conditional("DRCTRR_DRCTR_ID", index.ToString()));
        //    } else if (index.GetType() == typeof(int[])) {
        //        var real = (int[])index;
        //        condiciones.Add(new Conditional("DRCTRR_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
        //    }

        //    if (onlyActive)
        //        condiciones.Add(new Conditional("DRCTRR_A", "1"));

        //    return new BindingList<IRelacionComercialDetailModel>(this.relacionComercialRepository.GetList<RelacionComercialDetailModel>(condiciones).ToList<IRelacionComercialDetailModel>());
        //}

        public override BindingList<IDomicilioFiscalDetailModel> GetDomicilios(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("DRCCN_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("DRCCN_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }
            if (onlyActive)
                condiciones.Add(new Conditional("DRCCN_A", "1"));
            return new BindingList<IDomicilioFiscalDetailModel>(this.domicilioRepository.GetList<DomicilioFiscalDetailModel>(condiciones).ToList<IDomicilioFiscalDetailModel>());
        }

        public override BindingList<IContribuyenteVendedorModel> GetVendedor(object index, bool onlyActive = true) {
            //var condiciones = new List<Conditional>();

            //if (index.GetType() == typeof(int)) {
            //    condiciones.Add(new Conditional("DRCTRC_DRCTR_ID", index.ToString()));
            //} else if (index.GetType() == typeof(int[])) {
            //    var real = (int[])index;
            //    condiciones.Add(new Conditional("DRCTRC_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            //}

            //if (onlyActive)
            //    condiciones.Add(new Conditional("DRCTRC_A", "1"));

            //return new BindingList<ContribuyenteVendedorModel>(this.carteraRepository.GetVendedores<ContribuyenteVendedorModel>(condiciones).ToList());
            return new BindingList<IContribuyenteVendedorModel>();
        }

        public override BindingList<ICuentaBancariaModel> GetCuentaBancarias(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("DRCTRB_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("DRCTRB_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }

            if (onlyActive)
                condiciones.Add(new Conditional("DRCTRB_A", "1"));

            return new BindingList<ICuentaBancariaModel>(this.cuentaBancariaRepository.GetList<CuentaBancariaModel>(condiciones).ToList<ICuentaBancariaModel>());
        }

        public override BindingList<IContactoDetailModel> GetContactos(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("DRCTRL_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("DRCTRL_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }

            if (onlyActive)
                condiciones.Add(new Conditional("DRCTRL_A", "1"));

            return new BindingList<IContactoDetailModel>(this.contactoRepository.GetList<ContactoDetailModel>(condiciones).ToList<IContactoDetailModel>());
        }

        #region metodos para la actualizacion del directorio
        /// <summary>
        /// con esta rutina se actualiza la tabla de relaciones comerciales una vez actualizado el directorio
        /// </summary>
        //public void Crear() {
        //    var datos = this.contribuyenteRepository.GetList<ContribuyenteDomicilioSingleModel>(new List<IConditional>());
        //    foreach (var registro in datos) {
        //        var rc = new RelacionComercialData();
        //        rc.SetRelacion(registro.Relacion);

        //        foreach (var item in rc.Items) {
        //            if (item.Selected) {
        //                this.relacionComercialRepository.Saveable(new RelacionComercialDetailModel { IdTipoRelacion = item.Id, Activo = true, IdDirectorio = registro.IdDirectorio });
        //            }
        //        }
        //    }
        //}

        //public BindingList<RelacionComercialDetailModel> Set2Relacion(BindingList<RelacionComercialDetailModel> relacion, int idDirectorio) {
        //    var relaciones = new BindingList<RelacionComercialDetailModel>(
        //        EnumerationExtension.GetEnumToClass<RelacionComercialDetailModel, TipoRelacionComericalEnum>().ToList());

        //    return relaciones;
        //}

        //public BindingList<IRelacionComercialDetailModel> SetRelacion(string Relacion, int idDirectorio) {
        //    var relaciones = new List<RelacionComercialDetailModel>(EnumerationExtension.GetEnumToClass<RelacionComercialDetailModel, TipoRelacionComericalEnum>().ToList());
        //    if (Relacion != null) {
        //        string[] arrayRelacion = Relacion.Split(new char[] { ',' });
        //        relaciones.Where(it => arrayRelacion.Contains(it.Name)).Select(it => { it.Activo = true; it.IdDirectorio = idDirectorio; return it; }).ToList();
        //    }
        //    return new BindingList<IRelacionComercialDetailModel>(relaciones.ToList<IRelacionComercialDetailModel>());
        //}

        //public BindingList<IRelacionComercialDetailModel> SetRelacionesComerciales(BindingList<IRelacionComercialDetailModel> relacions) {
        //    var _relaciones = new BindingList<IRelacionComercialDetailModel>();
        //    foreach (var item in relacions) {
        //        var item2 = new RelacionComercialDetailModel {
        //            Activo = item.Selected,
        //            FechaModifica = item.FechaModifica,
        //            Nota = item.Nota,
        //            IdDirectorio = item.IdDirectorio,
        //            Modifica = item.Modifica,
        //            IdTipoRelacion = item.IdTipoRelacion,
        //            IdComision = item.IdComision
        //        };
        //        if (item.Selected == true) {
        //            _relaciones.Add(item2);
        //        } else if (item.Selected == false && item.IdDirectorio > 0) {
        //            _relaciones.Add(item2);
        //        }
        //    }
        //    return _relaciones;
        //}
        #endregion

        #region metodos estaticos
        /// <summary>
        /// obtener listado de regimenes fiscales
        /// </summary>
        //public static List<TipoRegimenFiscalModel> GetTipoRegimenes() {
        //    return EnumerationExtension.GetEnumToClass<TipoRegimenFiscalModel, TipoRegimenEnum>().ToList();
        //}
        #endregion

        //public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
        //    var d0 = new List<IConditional>();
        //    foreach (var item in conditionals) {
        //        d0.Add(item as Conditional);
        //    }
        //    return this.contribuyenteRepository.GetList<T1>(d0);
        //}
    }
}