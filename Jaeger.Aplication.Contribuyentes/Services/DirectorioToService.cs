﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Aplication.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Contribuyentes.Services {
    public class DirectorioToService : ContribuyenteToService, IDirectorioService {

        public DirectorioToService() : base() { }

        public DirectorioToService(TipoRelacionComericalEnum relacionComercial) : base(relacionComercial) { }

        public List<T1> GetList<T1>(bool onlyActive = true, string search = "*") where T1 : class, new() {
            var condiciones = new List<IConditional>();

            if (onlyActive) {
                condiciones.Add(new Conditional("DRCTR_A", "1"));
                //if (relacionComercial != TipoRelacionComericalEnum.None) condiciones.Add(new Conditional("DRCTRR_A", "1"));
            }
            // si tenemos relacion comercial definida
            if (this.relacionComercial != TipoRelacionComericalEnum.None) {
                var d1 = (int)this.relacionComercial;
                condiciones.Add(new Conditional("DRCTRR_CTREL_ID", d1.ToString()));
                //condiciones.Add(new Conditional("DRCTR_RLCN", this.relacionComercial.ToString(), ConditionalTypeEnum.Like));
            }
            // en caso de busquedas
            if (search != "*") { condiciones.Add(new Conditional("@SEARCH", search, ConditionalTypeEnum.Like)); }

            var response = this.contribuyenteRepository.GetList<T1>(condiciones).ToList();
            if (typeof(T1) == typeof(ContribuyenteDetailModel)) {
                var index = response.Select(it => (it as ContribuyenteDetailModel).IdDirectorio).ToArray();
                var domicilios = this.GetDomicilios(index);
                var vendedores = this.GetVendedor(index);
                var contactos = this.GetContactos(index);

                for (int i = 0; i < response.Count; i++) {
                    (response[i] as ContribuyenteDetailModel).Domicilios = new BindingList<IDomicilioFiscalDetailModel>(domicilios.Where(it => it.IdContribuyente == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList<IDomicilioFiscalDetailModel>());
                    (response[i] as ContribuyenteDetailModel).Vendedores = new BindingList<IContribuyenteVendedorModel>(vendedores.Where(it => it.IdCliente == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList<IContribuyenteVendedorModel>());
                    (response[i] as ContribuyenteDetailModel).Contactos = new BindingList<IContactoDetailModel>(contactos.Where(it => it.IdDirectorio == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList<IContactoDetailModel>());
                }
            } else if (typeof(T1) == typeof(ContribuyenteDomicilioModel)) {
                var index = response.Select(it => (it as ContribuyenteDomicilioModel).Id).ToArray();
                var domicilios = this.GetDomicilios(index);
                var vendedores = this.GetVendedor(index);

                for (int i = 0; i < response.Count; i++) {
                    (response[i] as ContribuyenteDomicilioModel).Domicilios = new BindingList<IDomicilioFiscalDetailModel>(domicilios.Where(it => it.IdContribuyente == (response[i] as ContribuyenteDomicilioModel).Id).ToList<IDomicilioFiscalDetailModel>());
                    (response[i] as ContribuyenteDomicilioModel).Vendedores = new BindingList<IContribuyenteVendedorModel>(vendedores.Where(it => it.IdCliente == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList());
                }
            } else if (typeof(T1) == typeof(BeneficiarioDetailModel)) {
                var indexs = response.Select(it => (it as BeneficiarioDetailModel).IdDirectorio).ToArray();
                var cuentas = this.GetCuentaBancarias(indexs);

                for (int i = 0; i < response.Count; i++) {
                    (response[i] as BeneficiarioDetailModel).CuentasBancarias = new BindingList<ICuentaBancariaModel>(cuentas.Where(it => it.IdDirectorio == (response[i] as BeneficiarioDetailModel).IdDirectorio).ToList());
                }
            }
            return response;
        }

        public List<T1> GetList<T1>(List<ItemSelectedModel> relation, string search = "*") where T1 : class, new() {
            var relaciones = relation.Select(it => it.Id);

            var condiciones = new List<IConditional> {
                new Conditional("DRCTR_A", "0", ConditionalTypeEnum.GreaterThan),
                new Conditional("DRCTRR_CTREL_ID", string.Join(",", relaciones), ConditionalTypeEnum.In),
                new Conditional("DRCTR_NOM", "", ConditionalTypeEnum.IsNot)
            };

            if (search != "*")
                condiciones.Add(new Conditional("@SEARCH", search, ConditionalTypeEnum.Like));

            var response = this.contribuyenteRepository.GetList<T1>(condiciones).ToList();
            if (typeof(T1) == typeof(ContribuyenteDetailModel)) {
                var index = response.Select(it => (it as ContribuyenteDetailModel).IdDirectorio).ToArray();
                var domicilios = this.GetDomicilios(index);
                var vendedores = this.GetVendedor(index);
                var contactos = this.GetContactos(index);

                for (int i = 0; i < response.Count; i++) {
                    (response[i] as ContribuyenteDetailModel).Domicilios = new BindingList<IDomicilioFiscalDetailModel>(domicilios.Where(it => it.IdContribuyente == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList<IDomicilioFiscalDetailModel>());
                    (response[i] as ContribuyenteDetailModel).Vendedores = new BindingList<IContribuyenteVendedorModel>(vendedores.Where(it => it.IdCliente == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList<IContribuyenteVendedorModel>());
                    (response[i] as ContribuyenteDetailModel).Contactos = new BindingList<IContactoDetailModel>(contactos.Where(it => it.IdDirectorio == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList<IContactoDetailModel>());
                }
            } else if (typeof(T1) == typeof(ContribuyenteDomicilioModel)) {
                var index = response.Select(it => (it as ContribuyenteDomicilioModel).Id).ToArray();
                var domicilios = this.GetDomicilios(index);
                var vendedores = this.GetVendedor(index);

                for (int i = 0; i < response.Count; i++) {
                    (response[i] as ContribuyenteDomicilioModel).Domicilios = new BindingList<IDomicilioFiscalDetailModel>(domicilios.Where(it => it.IdContribuyente == (response[i] as ContribuyenteDomicilioModel).Id).ToList<IDomicilioFiscalDetailModel>());
                    (response[i] as ContribuyenteDomicilioModel).Vendedores = new BindingList<IContribuyenteVendedorModel>(vendedores.Where(it => it.IdCliente == (response[i] as ContribuyenteDetailModel).IdDirectorio).ToList());
                }
            } else if (typeof(T1) == typeof(BeneficiarioDetailModel)) {
                var indexs = response.Select(it => (it as BeneficiarioDetailModel).IdDirectorio).ToArray();
                var cuentas = this.GetCuentaBancarias(indexs);

                for (int i = 0; i < response.Count; i++) {
                    (response[i] as BeneficiarioDetailModel).CuentasBancarias = new BindingList<ICuentaBancariaModel>(cuentas.Where(it => it.IdDirectorio == (response[i] as BeneficiarioDetailModel).IdDirectorio).ToList());
                }
            }

            return response;
        }
    }
}
