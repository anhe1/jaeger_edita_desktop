﻿using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.DataAccess.Contribuyentes.Repositories;
using Jaeger.Domain.Contribuyentes.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Contribuyentes.Services {
    /// <summary>
    /// servicio de directorio
    /// </summary>
    public class ContribuyenteService : IContribuyenteService {
        #region declaraciones
        protected ISqlContribuyenteRepository contribuyenteRepository;
        protected ISqlDomicilioRepository domicilioRepository;
        protected ISqlCuentaBancariaRepository cuentaBancariaRepository;
        protected ISqlRelacionComercialRepository relacionComercialRepository;
        protected ISqlCarteraRepository carteraRepository;
        protected ISqlContactoRepository contactoRepository;
        // tipo de relacion comercial del directorio
        protected TipoRelacionComericalEnum relacionComercial;
        #endregion

        public ContribuyenteService() {
            this.OnLoad();
            this.relacionComercial = TipoRelacionComericalEnum.None;
        }

        public ContribuyenteService(TipoRelacionComericalEnum relacionComercial) {
            this.OnLoad();
            this.relacionComercial = relacionComercial;
        }

        protected internal virtual void OnLoad() {
            this.contribuyenteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.relacionComercialRepository = new SqlRelacionComercialRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.domicilioRepository = new SqlSugarDomicilioRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.cuentaBancariaRepository = new SqlSugarCuentaBancariaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.contactoRepository = new SqlSugarContactoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// desactivar registro
        /// </summary>
        public bool Remover(int id) {
            return this.contribuyenteRepository.Delete(id);
        }

        public bool SearchClave(string clave) {
            return this.contribuyenteRepository.ExistClave(clave);
        }

        public bool ExistRFC(string rfc) {
            return this.contribuyenteRepository.ReturnId(rfc) > 0;
        }

        /// <summary>
        /// almacenar la bandera de validacion del RFC
        /// </summary>
        public bool ValidaRFC(int index, string razonSocial, string nombreComercial, string domicilioFiscal, bool valido) {
            return this.contribuyenteRepository.ValidaRFC(index, razonSocial, nombreComercial, domicilioFiscal, valido);
        }

        public bool ValidaRFC(IValidoRFC valido) {
            return this.contribuyenteRepository.ValidaRFC(valido);
        }

        /// <summary>
        /// obtener objeto complejo contribuyente
        /// </summary>
        public virtual IContribuyenteDetailModel GetById(int index) {
            var d1 = this.contribuyenteRepository.GetById(index);
            // parche en EDITA
            d1.Relaciones = this.SetRelacion(d1.Relacion, d1.IdDirectorio);
            if (d1.Relaciones.Count == 0) {
                d1.Relaciones = this.SetRelacion("", d1.IdDirectorio);
            }
            return d1;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IContribuyenteDetailModel GetByRFC(string rfc) {
            var d1 = this.contribuyenteRepository.GetByRFC(rfc);
            if (d1 != null) {
                // parche en EDITA
                d1.Relaciones = this.SetRelacion(d1.Relacion, d1.IdDirectorio);
                if (d1.Relaciones.Count == 0) {
                    d1.Relaciones = this.SetRelacion("", d1.IdDirectorio);
                }
            }
            return d1;
        }

        /// <summary>
        /// crear archivo de salida con la informacion de la validacion de RFC
        /// </summary>
        public virtual bool GetValidaRFC(BindingList<IContribuyenteDomicilioSingleModel> contribuyentes, string fileName) {
            var d = new List<string>();
            foreach (var item in contribuyentes) {
                d.Add(string.Format("{0}|{1}|{2}|{3}", d.Count + 1, item.RFC, item.Nombre, item.CodigoPostal));
            }

            try {
                System.IO.File.WriteAllLines(fileName, d.ToArray());
            } catch (Exception) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// almacenar un contribuyente
        /// </summary>
        public virtual IContribuyenteDetailModel Save(IContribuyenteDetailModel model) {

            #region parche de relaciones con EDITA
            var rel = this.SetRelacionesComerciales(model.Relaciones);
            model.Relacion = string.Join(",", rel.Where(it => it.Activo).Select(it => it.Name).ToArray());
            var response = this.contribuyenteRepository.Save(model);

            // relaciones comerciales
            for (int i = 0; i < rel.Count; i++) {
                rel[i].IdDirectorio = model.IdDirectorio;
                this.relacionComercialRepository.Saveable(rel[i]);
            }
            model.Relaciones = this.SetRelacion(model.Relacion, model.IdDirectorio);
            #endregion

            //// domicilios
            //for (int i = 0; i < response.Domicilios.Count; i++) {
            //    response.Domicilios[i].IdContribuyente = response.IdDirectorio;
            //    response.Domicilios[i] = this.domicilioRepository.Save(response.Domicilios[i]);
            //}
            //// cuentas bancarias
            //for (int i = 0; i < response.CuentasBancarias.Count; i++) {
            //    response.CuentasBancarias[i].IdDirectorio = response.IdDirectorio;
            //    response.CuentasBancarias[i] = this.cuentaBancariaRepository.Save(response.CuentasBancarias[i]);
            //}
            //// contactos
            //for (int i = 0; i < response.Contactos.Count; i++) {
            //    response.Contactos[i].IdDirectorio = response.IdDirectorio;
            //    response.Contactos[i] = this.contactoRepository.Save(response.Contactos[i]);
            //}
            return response;
        }

        public virtual int Saveable(RelacionComercialDetailModel item) {
            return this.relacionComercialRepository.Saveable(item);
        }

        public virtual BindingList<IRelacionComercialDetailModel> GetRelacion(object index, bool onlyActive = true) {
            var query = RelacionQuery();
            query.IdDirectorio(index).OnlyActive(onlyActive);
            return new BindingList<IRelacionComercialDetailModel>(this.relacionComercialRepository.GetList<RelacionComercialDetailModel>(query.Build()).ToList<IRelacionComercialDetailModel>());
        }

        public virtual BindingList<IDomicilioFiscalDetailModel> GetDomicilios(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("_DRCCN_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("_DRCCN_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }
            if (onlyActive)
                condiciones.Add(new Conditional("_DRCCN_A", "1"));
            return new BindingList<IDomicilioFiscalDetailModel>(this.domicilioRepository.GetList<DomicilioFiscalDetailModel>(condiciones).ToList<IDomicilioFiscalDetailModel>());
        }

        public virtual BindingList<IContribuyenteVendedorModel> GetVendedor(object index, bool onlyActive = true) {
            //var condiciones = new List<Conditional>();

            //if (index.GetType() == typeof(int)) {
            //    condiciones.Add(new Conditional("DRCTRC_DRCTR_ID", index.ToString()));
            //} else if (index.GetType() == typeof(int[])) {
            //    var real = (int[])index;
            //    condiciones.Add(new Conditional("DRCTRC_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            //}

            //if (onlyActive)
            //    condiciones.Add(new Conditional("DRCTRC_A", "1"));

            //return new BindingList<ContribuyenteVendedorModel>(this.carteraRepository.GetVendedores<ContribuyenteVendedorModel>(condiciones).ToList());
            return new BindingList<IContribuyenteVendedorModel>();
        }

        public virtual BindingList<ICuentaBancariaModel> GetCuentaBancarias(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("DRCTRB_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("DRCTRB_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }

            if (onlyActive)
                condiciones.Add(new Conditional("DRCTRB_A", "1"));

            return new BindingList<ICuentaBancariaModel>(this.cuentaBancariaRepository.GetList<CuentaBancariaModel>(condiciones).ToList<ICuentaBancariaModel>());
        }

        public virtual BindingList<IContactoDetailModel> GetContactos(object index, bool onlyActive = true) {
            IContactosQueryBuilder query = new ContactosQueryBuilder();
            query.IdDirectorio(index).OnlyActive(onlyActive);
            return new BindingList<IContactoDetailModel>(this.contactoRepository.GetList<ContactoDetailModel>(query.Build()).ToList<IContactoDetailModel>());
        }

        /// <summary>
        /// obtener la lista de relaciones comerciales
        /// </summary>
        public virtual List<IRelacionComercialDetailModel> GetRelacionesComerciales(BindingList<IRelacionComercialDetailModel> relaciones) {
            var relacionEmpresa = ((TipoRelacionComericalEnum[])Enum.GetValues(typeof(TipoRelacionComericalEnum)))
                .Select(c => new RelacionComercialDetailModel((int)c, true, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description)).ToList();

            var response = from p in relacionEmpresa
                              join co in relaciones on p.IdTipoRelacion equals co.IdTipoRelacion into _resultado
                              from result in _resultado.DefaultIfEmpty()
                              select new RelacionComercialDetailModel {
                                  Activo = (result != null ? result.Activo : false),
                                  IdTipoRelacion = (result != null ? result.IdTipoRelacion : p.IdTipoRelacion),
                                  Selected = (result != null ? (result.IdTipoRelacion > 0 && result.IdDirectorio > 0) : false),
                                  IdDirectorio = (result != null ? result.IdDirectorio : 0),
                                  FechaModifica = (result != null ? result.FechaModifica : null),
                                  Modifica = (result != null ? result.Modifica : null),
                                  Nota = (result != null ? result.Nota : "")
                              };
            return response.ToList<IRelacionComercialDetailModel>();
        }

        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var d0 = new List<IConditional>();
            foreach (var item in conditionals) {
                d0.Add(item as Conditional);
            }
            return this.contribuyenteRepository.GetList<T1>(d0);
        }

        /// <summary>
        /// obtener objeto nuevo con relacion 
        /// </summary>
        public virtual ContribuyenteDetailModel GetNew(TipoRelacionComericalEnum relacion = TipoRelacionComericalEnum.None) {
            var d1 = new ContribuyenteDetailModel {
                Relaciones = this.SetRelacion("", 0)
            };
            return d1;
        }

        #region metodos para la actualizacion del directorio
        public BindingList<RelacionComercialDetailModel> Set2Relacion(BindingList<RelacionComercialDetailModel> relacion, int idDirectorio) {
            var relaciones = new BindingList<RelacionComercialDetailModel>(
                EnumerationExtension.GetEnumToClass<RelacionComercialDetailModel, TipoRelacionComericalEnum>().ToList());

            return relaciones;
        }

        public BindingList<IRelacionComercialDetailModel> SetRelacion(string Relacion, int idDirectorio) {
            var relaciones = new List<RelacionComercialDetailModel>(EnumerationExtension.GetEnumToClass<RelacionComercialDetailModel, TipoRelacionComericalEnum>().ToList());
            if (Relacion != null) {
                string[] arrayRelacion = Relacion.Split(new char[] { ',' });
                relaciones.Where(it => arrayRelacion.Contains(it.Name)).Select(it => { it.Activo = true; it.IdDirectorio = idDirectorio; return it; }).ToList();
            }
            return new BindingList<IRelacionComercialDetailModel>(relaciones.ToList<IRelacionComercialDetailModel>());
        }

        public BindingList<IRelacionComercialDetailModel> SetRelacionesComerciales(BindingList<IRelacionComercialDetailModel> relacions) {
            var _relaciones = new BindingList<IRelacionComercialDetailModel>();
            foreach (var item in relacions) {
                var item2 = new RelacionComercialDetailModel {
                    Activo = item.Selected,
                    FechaModifica = item.FechaModifica,
                    Nota = item.Nota,
                    IdDirectorio = item.IdDirectorio,
                    Modifica = item.Modifica,
                    IdTipoRelacion = item.IdTipoRelacion,
                    IdComision = item.IdComision
                };
                if (item.Selected == true) {
                    _relaciones.Add(item2);
                } else if (item.Selected == false && item.IdDirectorio > 0) {
                    _relaciones.Add(item2);
                }
            }
            return _relaciones;
        }
        #endregion

        #region metodos estaticos
        /// <summary>
        /// obtener listado de regimenes fiscales
        /// </summary>
        public static List<TipoRegimenFiscalModel> GetTipoRegimenes() {
            return EnumerationExtension.GetEnumToClass<TipoRegimenFiscalModel, TipoRegimenEnum>().ToList();
        }

        /// <summary>
        /// obtener listado de domicilios
        /// </summary>
        public static List<DomicilioTipoModel> GetTipoDomicilio() {
            return EnumerationExtension.GetEnumToClass<DomicilioTipoModel, TipoDomicilioEnum>().ToList();
        }

        public static IContribuyenteQueryBuilder Query() {
            return new ContribuyenteQueryBuilder();
        }

        public static IRelacionComercialQueryBuilder RelacionQuery() {
            return new RelacionComercialQueryBuilder();
        }

        public static IContribuyenteBuilder Create() {
            return new ContribuyenteBuilder();
        }
        #endregion
    }
}
