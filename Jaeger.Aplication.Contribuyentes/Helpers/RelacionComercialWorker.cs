﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Aplication.Contribuyentes.Helpers {
    /// <summary>
    /// con esta rutina se actualiza la tabla de relaciones comerciales una vez actualizado el directorio
    /// </summary>
    public class RelacionComercialWorker : Base.Abstracts.WorkerBase {
        protected IContribuyenteService Service;
        public RelacionComercialWorker(IContribuyenteService service) : base("RelacionComercialWorker") {
            this.Service = service;
        }

        #region implementacion
        public override bool SupportsDualProgress {
            get { return false; }
        }

        public override bool IsCancelable {
            get { return false; }
        }

        public override object Result {
            get { return null; }
        }

        public override void BeginWork() {
            if (_worker != null && _worker.IsAlive)
                throw new ApplicationException($"{this._workerName} is already in progress");

            this._cancelled = false;

            ThreadStart ts = delegate {
                try {
                    this.NotifyPrimaryProgress(0, "Consultando información del directorio");
                    this.Parche();
                    this.NotifyPrimaryProgress(true, 100, "Proceso terminado");
                } catch (Base.Helpers.UserCancellationException exp) {
                    this.NotifyPrimaryProgress(true, 100, "Usuario cancela." + exp.Message);
                } catch (Exception cex) {
                    this.NotifyPrimaryProgress(true, 100, "Error " + cex.Message);
                    Console.WriteLine(cex.Message);
                }
            };

            this.Thread(ts).Start();
        }
        #endregion

        #region 
        /// <summary>
        /// rutina para crear las relaciones comerciales
        /// </summary>
        protected void Parche() {
            var data = this.Service.GetList<ContribuyenteDomicilioSingleModel>(new List<IConditional>());
            var count = data.Count();
            var index = 0;
            foreach (var registro in data) {
                var rc = new RelacionComercialData();
                rc.SetRelacion(registro.Relacion);
                this.NotifyPrimaryProgress((int)this.Progress(index, count), $"Guardando... {index}");
                foreach (var item in rc.Items) {
                    if (item.Selected) {
                        this.Service.Saveable(new RelacionComercialDetailModel { IdTipoRelacion = item.Id, Activo = true, IdDirectorio = registro.IdDirectorio });
                    }
                }
                index++;
            }
        }
        #endregion
    }
}
