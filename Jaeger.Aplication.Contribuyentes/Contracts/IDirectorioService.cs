﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Aplication.Contribuyentes.Contracts {
    public interface IDirectorioService : IContribuyenteService {
        List<T1> GetList<T1>(List<ItemSelectedModel> relation, string search = "*") where T1 : class, new();

        List<T1> GetList<T1>(bool onlyActive = true, string search = "*") where T1 : class, new();
    }
}
