﻿using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Contribuyentes.Contracts {
    public interface IVendedorService {
        IVendedor2DetailModel GetById(int index);

        IVendedor2DetailModel Saveable(IVendedor2DetailModel vendedor);
    }
}
