﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Aplication.Contribuyentes.Contracts {
    /// <summary>
    /// servicio de directorio
    /// </summary>
    public interface IContribuyenteService {
        /// <summary>
        /// comprobar clave dee usuario
        /// </summary>
        bool SearchClave(string clave);

        /// <summary>
        /// comprobar la existencia de un RFC
        /// </summary>
        bool ExistRFC(string rfc);

        /// <summary>
        /// desactivar registro
        /// </summary>
        bool Remover(int id);

        /// <summary>
        /// almacenar la bandera de validacion del RFC
        /// </summary>
        bool ValidaRFC(int index, string razonSocial, string nombreComercial, string domicilioFiscal, bool valido);

        bool ValidaRFC(IValidoRFC valido);

        bool GetValidaRFC(BindingList<IContribuyenteDomicilioSingleModel> contribuyentes, string fileName);
        
        /// <summary>
        /// obtener objeto complejo contribuyente
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>objeto contribuyenteModel</returns>
        IContribuyenteDetailModel GetById(int index);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfc"></param>
        /// <returns></returns>
        IContribuyenteDetailModel GetByRFC(string rfc);

        ContribuyenteDetailModel GetNew(TipoRelacionComericalEnum relacion = TipoRelacionComericalEnum.None);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IContribuyenteDetailModel Save(IContribuyenteDetailModel model);

        int Saveable(RelacionComercialDetailModel item);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
