﻿using System.ComponentModel;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Aplication.Contribuyentes.Contracts {
    public interface IVendedoresService : IVendedorService {
        BindingList<Vendedor2DetailModel> GetList(bool onlyActive = true);
    }
}
