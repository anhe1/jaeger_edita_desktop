﻿using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    public class SqlDirectorioDocumentoRepository : MySqlSugarContext<ContribuyenteDocumentoModel>, ISqlDirectorioDocumentoRepository {
        public SqlDirectorioDocumentoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }
    }
}
