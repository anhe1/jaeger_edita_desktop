﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Contribuyentes.Builder;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// repositorio de directorio
    /// </summary>
    public class SqlContribuyenteRepository : MySqlSugarContext<ContribuyenteModel>, ISqlContribuyenteRepository {
        protected ISqlContactoRepository contactoRepository;
        protected ISqlDomicilioRepository domicilioRepository;
        protected ISqlCuentaBancariaRepository cuentaBancariaRepository;
        protected ISqlRelacionComercialRepository relacionComercialRepository;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlContribuyenteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.contactoRepository = new SqlSugarContactoRepository(configuracion, user);
            this.domicilioRepository = new SqlDomicilioRepository(configuracion, user);
            this.cuentaBancariaRepository = new SqlCuentaBancariaRepository(configuracion, user);
            this.relacionComercialRepository = new SqlRelacionComercialRepository(configuracion, user);
        }

        public override int Insert(ContribuyenteModel item) {
            var CommandText = @"INSERT INTO DRCTR ( DRCTR_ID, DRCTR_CVROLL, DRCTR_APIKEY, DRCTR_A, DRCTR_EXTRNJR, DRCTR_PRNCPL, DRCTR_CMSN_ID, DRCTR_DSCNT_ID, DRCTR_USR, DRCTR_DSP, DRCTR_USOCFDI, DRCTR_USR_N, DRCTR_RFCV, DRCTR_RGSTRPTRNL, DRCTR_RFC, DRCTR_CLV, DRCTR_CURP, DRCTR_RESFIS, DRCTR_NMREG, DRCTR_DOMFIS, DRCTR_PSW, DRCTR_RGF, DRCTR_RLCN, DRCTR_WEB, DRCTR_TEL, DRCTR_MAIL, DRCTR_NOM, DRCTR_NOMC, DRCTR_RGFSC, DRCTR_NTS, DRCTR_CRD, DRCTR_SCRD, DRCTR_PCTD, DRCTR_IVA, DRCTR_USR_ID, DRCTR_SYNC_ID, DRCTR_DSCRD, DRCTR_CTPRC_ID, DRCTR_CTRG_ID, DRCTR_FN) 
                           VALUES (@DRCTR_ID,@DRCTR_CVROLL,@DRCTR_APIKEY,@DRCTR_A,@DRCTR_EXTRNJR,@DRCTR_PRNCPL,@DRCTR_CMSN_ID,@DRCTR_DSCNT_ID,@DRCTR_USR,@DRCTR_DSP,@DRCTR_USOCFDI,@DRCTR_USR_N,@DRCTR_RFCV,@DRCTR_RGSTRPTRNL,@DRCTR_RFC,@DRCTR_CLV,@DRCTR_CURP,@DRCTR_RESFIS,@DRCTR_NMREG,@DRCTR_DOMFIS,@DRCTR_PSW,@DRCTR_RGF,@DRCTR_RLCN,@DRCTR_WEB,@DRCTR_TEL,@DRCTR_MAIL,@DRCTR_NOM,@DRCTR_NOMC,@DRCTR_RGFSC,@DRCTR_NTS,@DRCTR_CRD,@DRCTR_SCRD,@DRCTR_PCTD,@DRCTR_IVA,@DRCTR_USR_ID,@DRCTR_SYNC_ID,@DRCTR_DSCRD,@DRCTR_CTPRC_ID,@DRCTR_CTRG_ID,@DRCTR_FN) RETURNING DRCTR_ID;";

            var d0 = new List<SqlSugar.SugarParameter> {
                new SqlSugar.SugarParameter("@DRCTR_ID", DBNull.Value),
                new SqlSugar.SugarParameter("@DRCTR_CVROLL", item.ClaveRoll),
                new SqlSugar.SugarParameter("@DRCTR_APIKEY", item.ApiKEY),
                new SqlSugar.SugarParameter("@DRCTR_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCTR_EXTRNJR", item.Extranjero),
                new SqlSugar.SugarParameter("@DRCTR_PRNCPL", item.Principal),
                new SqlSugar.SugarParameter("@DRCTR_CMSN_ID", item.IdComision),
                new SqlSugar.SugarParameter("@DRCTR_DSCNT_ID", item.IdDescuento),
                new SqlSugar.SugarParameter("@DRCTR_USR", item.User),
                new SqlSugar.SugarParameter("@DRCTR_DSP", item.DiasEntrega),
                new SqlSugar.SugarParameter("@DRCTR_USOCFDI", item.ClaveUsoCFDI),
                new SqlSugar.SugarParameter("@DRCTR_USR_N", item.Creo),
                new SqlSugar.SugarParameter("@DRCTR_RFCV", item.IsValidRFC),
                new SqlSugar.SugarParameter("@DRCTR_RGSTRPTRNL", item.RegistroPatronal),
                new SqlSugar.SugarParameter("@DRCTR_RFC", item.RFC),
                new SqlSugar.SugarParameter("@DRCTR_CLV", item.Clave),
                new SqlSugar.SugarParameter("@DRCTR_CURP", item.CURP),
                new SqlSugar.SugarParameter("@DRCTR_RESFIS", item.ResidenciaFiscal),
                new SqlSugar.SugarParameter("@DRCTR_NMREG", item.NumRegIdTrib),
                new SqlSugar.SugarParameter("@DRCTR_DOMFIS", item.DomicilioFiscal),
                new SqlSugar.SugarParameter("@DRCTR_PSW", item.Password),
                new SqlSugar.SugarParameter("@DRCTR_RGF", item.Regimen),
                new SqlSugar.SugarParameter("@DRCTR_RLCN", item.Relacion),
                new SqlSugar.SugarParameter("@DRCTR_WEB", item.SitioWEB),
                new SqlSugar.SugarParameter("@DRCTR_TEL", item.Telefono),
                new SqlSugar.SugarParameter("@DRCTR_MAIL", item.Correo),
                new SqlSugar.SugarParameter("@DRCTR_NOM", item.Nombre),
                new SqlSugar.SugarParameter("@DRCTR_NOMC", item.NombreComercial),
                new SqlSugar.SugarParameter("@DRCTR_RGFSC", item.RegimenFiscal),
                new SqlSugar.SugarParameter("@DRCTR_NTS", item.Nota),
                new SqlSugar.SugarParameter("@DRCTR_CRD", item.Credito),
                new SqlSugar.SugarParameter("@DRCTR_SCRD", item.SobreCredito),
                new SqlSugar.SugarParameter("@DRCTR_PCTD", item.FactorDescuento),
                new SqlSugar.SugarParameter("@DRCTR_IVA", item.FactorIvaPactado),
                new SqlSugar.SugarParameter("@DRCTR_USR_ID", item.IdUsuario),
                new SqlSugar.SugarParameter("@DRCTR_SYNC_ID", item.IdSincronizado),
                new SqlSugar.SugarParameter("@DRCTR_DSCRD", item.DiasCredito),
                new SqlSugar.SugarParameter("@DRCTR_CTPRC_ID", item.IdPrecio),
                new SqlSugar.SugarParameter("@DRCTR_CTRG_ID", item.IdRegimen),
                new SqlSugar.SugarParameter("@DRCTR_FN", item.FechaNuevo)
            };
            return (int)this.Db.Ado.GetScalar(CommandText, d0);
        }

        public override int Update(ContribuyenteModel item) {
            var CommandText = @"UPDATE DRCTR SET DRCTR_CVROLL = @DRCTR_CVROLL, DRCTR_APIKEY = @DRCTR_APIKEY, DRCTR_A = @DRCTR_A, DRCTR_EXTRNJR = @DRCTR_EXTRNJR, DRCTR_PRNCPL = @DRCTR_PRNCPL,
DRCTR_CMSN_ID = @DRCTR_CMSN_ID, DRCTR_DSCNT_ID = @DRCTR_DSCNT_ID, DRCTR_USR = @DRCTR_USR, DRCTR_DSP = @DRCTR_DSP, DRCTR_USOCFDI = @DRCTR_USOCFDI, DRCTR_USR_M = @DRCTR_USR_M,
DRCTR_RFCV = @DRCTR_RFCV, DRCTR_RGSTRPTRNL = @DRCTR_RGSTRPTRNL, DRCTR_RFC = @DRCTR_RFC, DRCTR_CLV = @DRCTR_CLV, DRCTR_CURP = @DRCTR_CURP, DRCTR_RESFIS = @DRCTR_RESFIS, DRCTR_NMREG = @DRCTR_NMREG, 
DRCTR_DOMFIS = @DRCTR_DOMFIS, DRCTR_PSW = @DRCTR_PSW, DRCTR_RGF = @DRCTR_RGF, DRCTR_RLCN = @DRCTR_RLCN, DRCTR_WEB = @DRCTR_WEB, DRCTR_TEL = @DRCTR_TEL, DRCTR_MAIL = @DRCTR_MAIL, DRCTR_NOM = @DRCTR_NOM, 
DRCTR_NOMC = @DRCTR_NOMC, DRCTR_RGFSC = @DRCTR_RGFSC, DRCTR_NTS = @DRCTR_NTS, DRCTR_CRD = @DRCTR_CRD, DRCTR_SCRD = @DRCTR_SCRD, DRCTR_PCTD = @DRCTR_PCTD, DRCTR_IVA = @DRCTR_IVA, DRCTR_USR_ID = @DRCTR_USR_ID, 
DRCTR_SYNC_ID = @DRCTR_SYNC_ID, DRCTR_DSCRD = @DRCTR_DSCRD, DRCTR_CTPRC_ID = @DRCTR_CTPRC_ID, 
DRCTR_FM = @DRCTR_FM, DRCTR_CTRG_ID = @DRCTR_CTRG_ID WHERE (DRCTR_ID = @DRCTR_ID)";
            var d0 = new List<SqlSugar.SugarParameter> {
                new SqlSugar.SugarParameter("@DRCTR_ID", item.IdDirectorio),
                new SqlSugar.SugarParameter("@DRCTR_CVROLL", item.ClaveRoll),
                new SqlSugar.SugarParameter("@DRCTR_APIKEY", item.ApiKEY),
                new SqlSugar.SugarParameter("@DRCTR_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCTR_EXTRNJR", item.Extranjero),
                new SqlSugar.SugarParameter("@DRCTR_PRNCPL", item.Principal),
                new SqlSugar.SugarParameter("@DRCTR_CMSN_ID", item.IdComision),
                new SqlSugar.SugarParameter("@DRCTR_DSCNT_ID", item.IdDescuento),
                new SqlSugar.SugarParameter("@DRCTR_USR", item.User),
                new SqlSugar.SugarParameter("@DRCTR_DSP", item.DiasEntrega),
                new SqlSugar.SugarParameter("@DRCTR_USOCFDI", item.ClaveUsoCFDI),
                new SqlSugar.SugarParameter("@DRCTR_USR_M", item.Modifica),
                new SqlSugar.SugarParameter("@DRCTR_RFCV", item.IsValidRFC),
                new SqlSugar.SugarParameter("@DRCTR_RGSTRPTRNL", item.RegistroPatronal),
                new SqlSugar.SugarParameter("@DRCTR_RFC", item.RFC),
                new SqlSugar.SugarParameter("@DRCTR_CLV", item.Clave),
                new SqlSugar.SugarParameter("@DRCTR_CURP", item.CURP),
                new SqlSugar.SugarParameter("@DRCTR_RESFIS", item.ResidenciaFiscal),
                new SqlSugar.SugarParameter("@DRCTR_NMREG", item.NumRegIdTrib),
                new SqlSugar.SugarParameter("@DRCTR_DOMFIS", item.DomicilioFiscal),
                new SqlSugar.SugarParameter("@DRCTR_PSW", item.Password),
                new SqlSugar.SugarParameter("@DRCTR_RGF", item.Regimen),
                new SqlSugar.SugarParameter("@DRCTR_RLCN", item.Relacion),
                new SqlSugar.SugarParameter("@DRCTR_WEB", item.SitioWEB),
                new SqlSugar.SugarParameter("@DRCTR_TEL", item.Telefono),
                new SqlSugar.SugarParameter("@DRCTR_MAIL", item.Correo),
                new SqlSugar.SugarParameter("@DRCTR_NOM", item.Nombre),
                new SqlSugar.SugarParameter("@DRCTR_NOMC", item.NombreComercial),
                new SqlSugar.SugarParameter("@DRCTR_RGFSC", item.RegimenFiscal),
                new SqlSugar.SugarParameter("@DRCTR_NTS", item.Nota),
                new SqlSugar.SugarParameter("@DRCTR_CRD", item.Credito),
                new SqlSugar.SugarParameter("@DRCTR_SCRD", item.SobreCredito),
                new SqlSugar.SugarParameter("@DRCTR_PCTD", item.FactorDescuento),
                new SqlSugar.SugarParameter("@DRCTR_IVA", item.FactorIvaPactado),
                new SqlSugar.SugarParameter("@DRCTR_USR_ID", item.IdUsuario),
                new SqlSugar.SugarParameter("@DRCTR_SYNC_ID", item.IdSincronizado),
                new SqlSugar.SugarParameter("@DRCTR_DSCRD", item.DiasCredito),
                new SqlSugar.SugarParameter("@DRCTR_CTPRC_ID", item.IdPrecio),
                new SqlSugar.SugarParameter("@DRCTR_FM", item.FechaModifica),
                new SqlSugar.SugarParameter("@DRCTR_CTRG_ID", item.IdRegimen)
            };
            return this.ExecuteTransaction(CommandText, d0);
        }

        /// <summary>
        /// obtener indice del del contribuyente por el registro federal de contribuyentes
        /// </summary>
        public int ReturnId(string rfc) {
            var CommandText = @"SELECT DRCTR.DRCTR_ID FROM DRCTR WHERE UPPER(DRCTR.DRCTR_RFC) LIKE @clave";
            var d0 = new List<SqlSugar.SugarParameter> { new SqlSugar.SugarParameter("@clave", rfc.ToUpper()) };
            int indice = (int)this.Db.Ado.GetScalar(CommandText, d0);
            return indice;
        }

        /// <summary>
        /// comprobar la existencia de la clave de usuario
        /// </summary>
        public bool ExistClave(string clave) {
            var CommandText = @"SELECT DRCTR.DRCTR_ID FROM DRCTR WHERE UPPER(DRCTR.DRCTR_CLV) LIKE @clave";
            var d0 = new SqlSugar.SugarParameter("@clave", clave.ToUpper());
            var indice = (int)this.Db.Ado.GetScalar(CommandText, d0);
            return indice > 0;
        }

        public T1 GetById<T1>(int index) where T1 : class, new() {
            var sqlCommand = string.Format("SELECT FIRST 1 DRCTR.* FROM DRCTR WHERE DRCTR_ID = @id");
            var d0 = new SqlSugar.SugarParameter("@id", index);

            return this.GetMapper<T1>(sqlCommand).FirstOrDefault();
        }

        /// <summary>
        /// obtener objeto complejo contribuyente
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>objeto contribuyenteModel</returns>
        public new IContribuyenteDetailModel GetById(int index) {
            var sqlCommand = "SELECT * FROM DRCTR WHERE DRCTR_ID = @id";
            var d0 = new List<SqlSugar.SugarParameter> { new SqlSugar.SugarParameter("@id", index.ToString()) };
            var itemModel = this.GetMapper<ContribuyenteDetailModel>(sqlCommand, d0).FirstOrDefault();

            var domicilios = this.domicilioRepository.GetList<DomicilioFiscalDetailModel>(new DomicilioQueryBuilder().IdDirectorio(index).Build());
            var contactos = this.contactoRepository.GetList<ContactoDetailModel>(new ContactosQueryBuilder().IdDirectorio(index).Build());
            var cuentasBancarias = this.cuentaBancariaRepository.GetList<CuentaBancariaModel>(new CuentasBancariasQueryBuilder().IdDirectorio(index).Build());
            var relaciones = this.relacionComercialRepository.GetList<RelacionComercialDetailModel>(new List<IConditional>() { new Conditional("DRCTRR_DRCTR_ID", index.ToString()) });

            itemModel.Domicilios = new BindingList<IDomicilioFiscalDetailModel>(domicilios.Where(it => it.IdContribuyente == itemModel.IdDirectorio).ToList<IDomicilioFiscalDetailModel>());
            itemModel.CuentasBancarias = new BindingList<ICuentaBancariaModel>(cuentasBancarias.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<ICuentaBancariaModel>());
            itemModel.Contactos = new BindingList<IContactoDetailModel>(contactos.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<IContactoDetailModel>());
            itemModel.Relaciones = new BindingList<IRelacionComercialDetailModel>(relaciones.ToList<IRelacionComercialDetailModel>());
            //itemModel.Vendedores = new BindingList<ContribuyenteVendedorModel>(vendedores.Where(it => it.IdCliente == itemModel.Id).ToList());

            return itemModel;
        }

        /// <summary>
        /// obtener un objeto contribuyente por su registro federal de contribuyentes
        /// </summary>
        public IContribuyenteDetailModel GetByRFC(string rfc) {
            var sqlCommand = "SELECT * FROM DRCTR WHERE DRCTR_RFC = @id";
            var d0 = new List<SqlSugar.SugarParameter> { new SqlSugar.SugarParameter("@id", rfc) };
            
            var result = this.GetMapper<ContribuyenteDetailModel>(sqlCommand, d0).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// desactivar registro
        /// </summary>
        public bool Remover(int id) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
              .SetColumns(it => new ContribuyenteDetailModel() {
                  Activo = false
              }).Where(it => it.IdDirectorio == id);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// almacenar un contribuyente
        /// </summary>
        public IContribuyenteDetailModel Save(IContribuyenteDetailModel item) {
            var isStored = false;

            if (item.IdDirectorio == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                item.IdDirectorio = this.Insert(item as ContribuyenteModel);
                
                isStored = item.IdDirectorio > 0;
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                isStored = this.Update(item as ContribuyenteModel) > 0;
            }

            if (isStored) {
                // domicilios
                if (item.Domicilios != null) {
                    for (int i = 0; i < item.Domicilios.Count; i++) {
                        item.Domicilios[i].IdContribuyente = item.IdDirectorio;
                        item.Domicilios[i] = this.domicilioRepository.Save(item.Domicilios[i]);
                    }
                }

                // cuentas bancarias
                if (item.CuentasBancarias != null) {
                    for (int i = 0; i < item.CuentasBancarias.Count; i++) {
                        item.CuentasBancarias[i].IdDirectorio = item.IdDirectorio;
                        item.CuentasBancarias[i] = this.cuentaBancariaRepository.Save(item.CuentasBancarias[i]);
                    }
                }

                // contactos
                if (item.Contactos != null) {
                    for (int i = 0; i < item.Contactos.Count; i++) {
                        item.Contactos[i].IdDirectorio = item.IdDirectorio;
                        item.Contactos[i] = this.contactoRepository.Save(item.Contactos[i]);
                    }
                }

                // vendedores
                if (item.Vendedores != null) {
                    for (int i = 0; i < item.Vendedores.Count; i++) {
                        item.Vendedores[i].IdCliente = item.IdDirectorio;
                        item.Vendedores[i].FechaModifica = DateTime.Now;
                        this.Db.Saveable<ContribuyenteVendedorModel>((ContribuyenteVendedorModel)item.Vendedores[i]).ExecuteCommand();
                    }
                }
            }
            return item;
        }

        public bool Update(int index, string mail) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
               .SetColumns(it => new ContribuyenteDetailModel() {
                   Correo = mail
               }).Where(it => it.IdDirectorio == index);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// almacenar la bandera de validacion del RFC
        /// </summary>
        public bool ValidaRFC(int index, string razonSocial, string nombreComercial, string domicilioFiscal, bool valido) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
               .SetColumns(it => new ContribuyenteDetailModel() {
                   IsValidRFC = valido,
                   Nombre = razonSocial,
                   NombreComercial = nombreComercial,
                   DomicilioFiscal = domicilioFiscal
               }).Where(it => it.IdDirectorio == index);
            return this.Execute(result) > 0;
        }

        public bool ValidaRFC(IValidoRFC valido) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
               .SetColumns(it => new ContribuyenteDetailModel() {
                   IsValidRFC = valido.IsValidRFC,
                   DomicilioFiscal = valido.DomicilioFiscal
               }).Where(it => it.IdDirectorio == valido.IdDirectorio);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// Listado 
        /// </summary>
        /// <typeparam name="T1">objeto</typeparam>
        /// <param name="conditionals">si un condicional contiene @search se hace la busqueda por nombre</param>
        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = "";
            // en caso del parche
            var relacion2 = conditionals.Where(it => it.FieldName.ToLower().Contains("drctrr")).FirstOrDefault();

            if (typeof(T1) == typeof(ContribuyenteDomicilioSingleModel)) {
                if (relacion2 != null) {
                    sqlCommand = @"select drctr.*, drccn.* from drctr 
                               left join drccn on drccn.drccn_id = (select drccn.drccn_id from drccn where drccn.drccn_drctr_id = drctr.drctr_id and drccn.drccn_a = 1 order by drccn.drccn_ctdrc_id ASC LIMIT 1) @wcondiciones order by drctr_nom";
                } else {
                    sqlCommand = @"select drctr.*, drccn.* from drctr 
                              left join drccn on drccn.drccn_id = (select drccn.drccn_id from drccn where drccn.drccn_drctr_id = drctr.drctr_id and drccn.drccn_a = 1 LIMIT 1) @wcondiciones order by drctr_nom";
                }
            } else if (typeof(T1) == typeof(ContribuyenteDetailModel) | typeof(T1) == typeof(ContribuyenteModel)) {
                sqlCommand = @"select drctr.* from drctr @wcondiciones order by drctr_nom";
            }

            // en caso de busqueda
            var search1 = conditionals.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
            if (search1 != null) {
                if (search1.FieldValue != string.Empty) {
                    // en este caso se filtra unicamente por el nombre porque no encontre solucion
                    //sqlCommand = sqlCommand.Replace("@wcondiciones", "where concat(lower(replace(replace(replace(replace(replace(_drctr_nom, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')), ',', lower(_drctr_clv), ',', lower(_drctr_rfc)) like @search @condiciones ");
                    sqlCommand = sqlCommand.Replace("@wcondiciones", "where concat(lower(replace(replace(replace(replace(replace(drctr_nom, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u'))) like @search @condiciones ");
                    sqlCommand = sqlCommand.Replace("@search", "'%" + search1.FieldValue.ToLower() + "%'");
                }
                conditionals.Remove(search1);
            }

            if (relacion2 != null) {
                sqlCommand = sqlCommand.Replace("from drctr", ", DRCTRR.* from drctr left join DRCTRR on drctr.drctr_id = DRCTRR.drctrr_drctr_id ");
            }
            //var t = this.Convierte(conditionals);
            //var query = this.Db.Queryable<T1>(this.GetQuerySQL(sqlCommand, conditionals));
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IEnumerable<T1> GetBeneficiarios<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones";
            return this.GetMapper<T1>(CommandText, conditionals).ToList();
        }

        public static IContribuyenteQueryBuilder Create() {
            return new ContribuyenteQueryBuilder();
        }
    }
}
