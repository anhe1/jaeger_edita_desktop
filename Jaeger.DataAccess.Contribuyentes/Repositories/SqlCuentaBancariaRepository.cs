﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de cuentas bancarias
    /// </summary>
    public class SqlCuentaBancariaRepository : MySqlSugarContext<CuentaBancariaModel>, ISqlCuentaBancariaRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        /// <param name="user"></param>
        public SqlCuentaBancariaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override int Insert(CuentaBancariaModel item) {
            var sqlCommand = @"INSERT INTO DRCTRB (DRCTRB_ID, DRCTRB_A, DRCTRB_VRFCD, DRCTRB_DRCTR_ID, DRCTRB_CRGMX, DRCTRB_CLV, DRCTRB_REFNUM, DRCTRB_MND, DRCTRB_RFC, DRCTRB_SCRSL, DRCTRB_TIPO, DRCTRB_REFALF, DRCTRB_NMCTA, DRCTRB_CNTCLB, DRCTRB_NMCRT, DRCTRB_PRAPLL, DRCTRB_SGAPLL, DRCTRB_ALIAS, DRCTRB_BANCO, DRCTRB_BNFCR, DRCTRB_USR_N, DRCTRB_FN) 
                                           VALUES (@DRCTRB_ID, @DRCTRB_A, @DRCTRB_VRFCD, @DRCTRB_DRCTR_ID, @DRCTRB_CRGMX, @DRCTRB_CLV, @DRCTRB_REFNUM, @DRCTRB_MND, @DRCTRB_RFC, @DRCTRB_SCRSL, @DRCTRB_TIPO, @DRCTRB_REFALF, @DRCTRB_NMCTA, @DRCTRB_CNTCLB, @DRCTRB_NMCRT, @DRCTRB_PRAPLL, @DRCTRB_SGAPLL, @DRCTRB_ALIAS, @DRCTRB_BANCO, @DRCTRB_BNFCR, @DRCTRB_USR_N, @DRCTRB_FN) RETURNING DRCTRB_ID;";
            var d0 = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@DRCTRB_ID", DBNull.Value),
                new SqlSugar.SugarParameter("@DRCTRB_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCTRB_VRFCD", item.Verificado),
                new SqlSugar.SugarParameter("@DRCTRB_DRCTR_ID", item.IdDirectorio),
                new SqlSugar.SugarParameter("@DRCTRB_CRGMX", item.CargoMaximo),
                new SqlSugar.SugarParameter("@DRCTRB_CLV", item.Clave),
                new SqlSugar.SugarParameter("@DRCTRB_REFNUM", item.RefNumerica),
                new SqlSugar.SugarParameter("@DRCTRB_MND", item.Moneda),
                new SqlSugar.SugarParameter("@DRCTRB_RFC", item.RFC),
                new SqlSugar.SugarParameter("@DRCTRB_SCRSL", item.Sucursal),
                new SqlSugar.SugarParameter("@DRCTRB_TIPO", item.TipoCuenta),
                new SqlSugar.SugarParameter("@DRCTRB_REFALF", item.RefAlfanumerica),
                new SqlSugar.SugarParameter("@DRCTRB_NMCTA", item.NumeroDeCuenta),
                new SqlSugar.SugarParameter("@DRCTRB_CNTCLB", item.Clabe),
                new SqlSugar.SugarParameter("@DRCTRB_NMCRT", item.InsitucionBancaria),
                new SqlSugar.SugarParameter("@DRCTRB_PRAPLL", item.PrimerApellido),
                new SqlSugar.SugarParameter("@DRCTRB_SGAPLL", item.SegundoApellido),
                new SqlSugar.SugarParameter("@DRCTRB_ALIAS", item.Alias),
                new SqlSugar.SugarParameter("@DRCTRB_BANCO", item.Banco),
                new SqlSugar.SugarParameter("@DRCTRB_BNFCR", item.Nombre),
                new SqlSugar.SugarParameter("@DRCTRB_USR_N", item.Creo),
                new SqlSugar.SugarParameter("@DRCTRB_FN", item.FechaNuevo)
            };
            item.IdCuenta = (int)this.Db.Ado.GetScalar(sqlCommand, d0);
            if (item.IdCuenta > 0)
                return item.IdCuenta;
            return 0;
        }

        public override int Update(CuentaBancariaModel item) {
            var sqlCommand = @"UPDATE DRCTRB SET DRCTRB_A = @DRCTRB_A, DRCTRB_VRFCD = @DRCTRB_VRFCD, DRCTRB_DRCTR_ID = @DRCTRB_DRCTR_ID, DRCTRB_CRGMX = @DRCTRB_CRGMX, DRCTRB_CLV = @DRCTRB_CLV, DRCTRB_REFNUM = @DRCTRB_REFNUM, DRCTRB_MND = @DRCTRB_MND, DRCTRB_RFC = @DRCTRB_RFC, DRCTRB_SCRSL = @DRCTRB_SCRSL, DRCTRB_TIPO = @DRCTRB_TIPO, DRCTRB_REFALF = @DRCTRB_REFALF, DRCTRB_NMCTA = @DRCTRB_NMCTA, DRCTRB_CNTCLB = @DRCTRB_CNTCLB, DRCTRB_NMCRT = @DRCTRB_NMCRT, DRCTRB_PRAPLL = @DRCTRB_PRAPLL, DRCTRB_SGAPLL = @DRCTRB_SGAPLL, DRCTRB_ALIAS = @DRCTRB_ALIAS, DRCTRB_BANCO = @DRCTRB_BANCO, DRCTRB_BNFCR = @DRCTRB_BNFCR, DRCTRB_USR_M = @DRCTRB_USR_M, DRCTRB_FM = @DRCTRB_FM WHERE (DRCTRB_ID = @DRCTRB_ID)";
            var d0 = new List<SqlSugar.SugarParameter>() {
                new SqlSugar.SugarParameter("@DRCTRB_ID", item.IdCuenta),
                new SqlSugar.SugarParameter("@DRCTRB_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCTRB_VRFCD", item.Verificado),
                new SqlSugar.SugarParameter("@DRCTRB_DRCTR_ID", item.IdDirectorio),
                new SqlSugar.SugarParameter("@DRCTRB_CRGMX", item.CargoMaximo),
                new SqlSugar.SugarParameter("@DRCTRB_CLV", item.Clave),
                new SqlSugar.SugarParameter("@DRCTRB_REFNUM", item.RefNumerica),
                new SqlSugar.SugarParameter("@DRCTRB_MND", item.Moneda),
                new SqlSugar.SugarParameter("@DRCTRB_RFC", item.RFC),
                new SqlSugar.SugarParameter("@DRCTRB_SCRSL", item.Sucursal),
                new SqlSugar.SugarParameter("@DRCTRB_TIPO", item.TipoCuenta),
                new SqlSugar.SugarParameter("@DRCTRB_REFALF", item.RefAlfanumerica),
                new SqlSugar.SugarParameter("@DRCTRB_NMCTA", item.NumeroDeCuenta),
                new SqlSugar.SugarParameter("@DRCTRB_CNTCLB", item.Clabe),
                new SqlSugar.SugarParameter("@DRCTRB_NMCRT", item.InsitucionBancaria),
                new SqlSugar.SugarParameter("@DRCTRB_PRAPLL", item.PrimerApellido),
                new SqlSugar.SugarParameter("@DRCTRB_SGAPLL", item.SegundoApellido),
                new SqlSugar.SugarParameter("@DRCTRB_ALIAS", item.Alias),
                new SqlSugar.SugarParameter("@DRCTRB_BANCO", item.Banco),
                new SqlSugar.SugarParameter("@DRCTRB_BNFCR", item.Nombre),
                new SqlSugar.SugarParameter("@DRCTRB_USR_M", item.Modifica),
                new SqlSugar.SugarParameter("@DRCTRB_FM", item.FechaModifica)
                };
            return this.ExecuteTransaction(sqlCommand, d0);
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT DRCTRB.* FROM DRCTRB @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public ICuentaBancariaModel Save(ICuentaBancariaModel cuentaBancaria) {
            if (cuentaBancaria.IdCuenta == 0) {
                cuentaBancaria.Creo = this.User;
                cuentaBancaria.FechaNuevo = DateTime.Now;
                cuentaBancaria.IdCuenta = this.Insert(cuentaBancaria as CuentaBancariaModel);
            } else {
                cuentaBancaria.Modifica = this.User;
                cuentaBancaria.FechaModifica = DateTime.Now;
                var query = this.Update(cuentaBancaria as CuentaBancariaModel);
            }
            return cuentaBancaria;
        }
    }
}
