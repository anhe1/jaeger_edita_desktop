﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// vendedor
    /// </summary>
    public class SqlSugarVendedorRepository : MySqlSugarContext<Vendedor2Model>, ISqlVendedorRepository {
        /// <summary>
        /// constructor
        /// </summary>
        public SqlSugarVendedorRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Saveable(IVendedor2DetailModel vendedor) {
            throw new NotImplementedException();
        }
    }
}
