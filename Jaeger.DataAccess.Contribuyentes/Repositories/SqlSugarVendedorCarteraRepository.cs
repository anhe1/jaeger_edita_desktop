﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    public class SqlSugarVendedorCarteraRepository : MySqlSugarContext<CarteraModel>, ISqlCarteraRepository {
        public SqlSugarVendedorCarteraRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<T1> GetClientes<T1>(List<IConditional> conditionals) where T1 : class, new() {
            throw new NotImplementedException();
        }

        public IEnumerable<T1> GetVendedores<T1>(List<IConditional> conditionals) where T1 : class, new() {
            //return this.GetMapper<T1>("SELECT _DRCTRC.*, _DRCTR.* FROM _DRCTRC LEFT JOIN _DRCTR ON _DRCTR._DRCTR_ID = _DRCTRC._DRCTRC_VNDR_ID", conditionals);
            throw new NotImplementedException();
        }

        public int Salveable(ICarteraModel model) {
            return this.Db.Saveable<CarteraModel>((CarteraModel)model).ExecuteCommand();
        }
    }
}
