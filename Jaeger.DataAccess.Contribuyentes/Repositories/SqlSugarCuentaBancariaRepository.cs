﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de cuentas bancarias
    /// </summary>
    public class SqlSugarCuentaBancariaRepository : MySqlSugarContext<CuentaBancariaModel>, ISqlCuentaBancariaRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        /// <param name="user"></param>
        public SqlSugarCuentaBancariaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<CuentaBancariaModel> GetList(List<Conditional> conditionals) {
            throw new NotImplementedException();
        }

        public ICuentaBancariaModel Save(ICuentaBancariaModel cuentaBancaria) {
            if (cuentaBancaria.IdCuenta == 0) {
                cuentaBancaria.Creo = this.User;
                cuentaBancaria.FechaNuevo = DateTime.Now;
                var query = this.Db.Insertable<CuentaBancariaModel>(cuentaBancaria);
                cuentaBancaria.IdCuenta = this.Insert(query);
            } else {
                cuentaBancaria.Modifica = this.User;
                cuentaBancaria.FechaModifica = DateTime.Now;
                var query = this.Db.Updateable<CuentaBancariaModel>(cuentaBancaria);
                this.Execute(query);
            }
            return cuentaBancaria;
        }
    }
}
