﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Builder;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// repositorio de directorio
    /// </summary>
    public class SqlSugarContribuyenteRepository : MySqlSugarContext<ContribuyenteModel>, ISqlContribuyenteRepository {
        protected ISqlContactoRepository contactoRepository;
        protected ISqlDomicilioRepository domicilioRepository;
        protected ISqlCuentaBancariaRepository cuentaBancariaRepository;
        protected ISqlRelacionComercialRepository relacionComercialRepository;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlSugarContribuyenteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.contactoRepository = new SqlSugarContactoRepository(configuracion, user);
            this.domicilioRepository = new SqlSugarDomicilioRepository(configuracion, user);
            this.cuentaBancariaRepository = new SqlSugarCuentaBancariaRepository(configuracion, user);
            this.relacionComercialRepository = new SqlRelacionComercialRepository(configuracion, user);
        }

        /// <summary>
        /// obtener indice del del contribuyente por el registro federal de contribuyentes
        /// </summary>
        public int ReturnId(string rfc) {
            int indice = this.Db.Queryable<ContribuyenteModel>().Where(it => it.RFC == rfc).Select(it => it.IdDirectorio).First();
            return indice;
        }

        /// <summary>
        /// comprobar la existencia de la clave de usuario
        /// </summary>
        public bool ExistClave(string clave) {
            int indice = this.Db.Queryable<ContribuyenteModel>().Where(it => it.Clave == clave).Select(it => it.IdDirectorio).First();
            return indice > 0;
        }

        public T1 GetById<T1>(int index) where T1 : class, new() {
            return null;
        }

        /// <summary>
        /// obtener objeto complejo contribuyente
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>objeto contribuyenteModel</returns>
        public new IContribuyenteDetailModel GetById(int index) {
            var result = this.Db.Queryable<ContribuyenteDetailModel>().Where(it => it.IdDirectorio == index).Mapper((itemModel, cache) => {
                var domicilios = cache.Get(directorio => {
                    var allIds = directorio.Select(it => it.IdDirectorio).ToList();
                    return this.Db.Queryable<DomicilioFiscalDetailModel>().Where(it => allIds.Contains(it.IdContribuyente)).ToList();
                });

                var cuentasBancarias = cache.Get(directorio => {
                    var allIds = directorio.Select(it => it.IdDirectorio).ToList();
                    return this.Db.Queryable<CuentaBancariaModel>().Where(it => allIds.Contains(it.IdDirectorio)).ToList();
                });

                var contactos = cache.Get(contactos1 => {
                    var ids = contactos1.Select(it => it.IdDirectorio).ToList();
                    return this.Db.Queryable<ContactoDetailModel>().Where(it => ids.Contains(it.IdDirectorio)).ToList();
                });

                //var vendedores = cache.Get(vendedor => {
                //    var allids = vendedor.Select(it => it.Id).ToList();
                //    return this.Db.Queryable<ContribuyenteVendedorModel, ContribuyenteModel>((r, d) => new JoinQueryInfos(JoinType.Left,
                //        r.IdVendedor == d.Id)).Where((r, d) => allids.Contains(r.IdVendedor)
                //    ).Select("*").ToList();
                //});

                var relaciones = cache.Get(relacion => {
                    return this.Db.Queryable<RelacionComercialDetailModel>().Where(it => it.IdDirectorio == index).ToList();
                });

                itemModel.Domicilios = new BindingList<IDomicilioFiscalDetailModel>(domicilios.Where(it => it.IdContribuyente == itemModel.IdDirectorio).ToList<IDomicilioFiscalDetailModel>());
                itemModel.CuentasBancarias = new BindingList<ICuentaBancariaModel>(cuentasBancarias.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<ICuentaBancariaModel>());
                itemModel.Contactos = new BindingList<IContactoDetailModel>(contactos.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<IContactoDetailModel>());
                itemModel.Relaciones = new BindingList<IRelacionComercialDetailModel>(relaciones.ToList<IRelacionComercialDetailModel>());
                //itemModel.Vendedores = new BindingList<ContribuyenteVendedorModel>(vendedores.Where(it => it.IdCliente == itemModel.Id).ToList());

            }).Single();

            return result;
        }

        /// <summary>
        /// obtener un objeto contribuyente por su registro federal de contribuyentes
        /// </summary>
        public IContribuyenteDetailModel GetByRFC(string rfc) {
            //Manual mode
            var result = this.Db.Queryable<ContribuyenteDetailModel>().Where(it => it.RFC == rfc).Mapper((itemModel, cache) => {
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.IdDirectorio).ToList();
                    return this.Db.Queryable<DomicilioFiscalDetailModel>().Where(it => allIds.Contains(it.IdContribuyente)).Where(it => it.Activo == true).ToList();
                });
                itemModel.Domicilios = new BindingList<IDomicilioFiscalDetailModel>(allItems.Where(it => it.IdContribuyente == itemModel.IdDirectorio).ToList<IDomicilioFiscalDetailModel>());
            }).First();
            return result;
        }

        /// <summary>
        /// desactivar registro
        /// </summary>
        public bool Remover(int id) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
              .SetColumns(it => new ContribuyenteDetailModel() {
                  Activo = false
              }).Where(it => it.IdDirectorio == id);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// almacenar un contribuyente
        /// </summary>
        public IContribuyenteDetailModel Save(IContribuyenteDetailModel item) {
            var isStored = false;

            if (item.IdDirectorio == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                var result = this.Db.Insertable<ContribuyenteDetailModel>(item);
                item.IdDirectorio = this.Execute(result);
                isStored = item.IdDirectorio > 0;
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                var result = this.Db.Updateable<ContribuyenteModel>(item);
                isStored = this.Execute(result) > 0;
            }

            if (isStored) {
                // domicilios
                if (item.Domicilios != null) {
                    for (int i = 0; i < item.Domicilios.Count; i++) {
                        item.Domicilios[i].IdContribuyente = item.IdDirectorio;
                        item.Domicilios[i] = this.domicilioRepository.Save(item.Domicilios[i]);
                    }
                }

                // cuentas bancarias
                if (item.CuentasBancarias != null) {
                    for (int i = 0; i < item.CuentasBancarias.Count; i++) {
                        item.CuentasBancarias[i].IdDirectorio = item.IdDirectorio;
                        item.CuentasBancarias[i] = this.cuentaBancariaRepository.Save(item.CuentasBancarias[i]);
                    }
                }

                // contactos
                if (item.Contactos != null) {
                    for (int i = 0; i < item.Contactos.Count; i++) {
                        item.Contactos[i].IdDirectorio = item.IdDirectorio;
                        item.Contactos[i] = this.contactoRepository.Save(item.Contactos[i]);
                    }
                }

                // vendedores
                if (item.Vendedores != null) {
                    for (int i = 0; i < item.Vendedores.Count; i++) {
                        item.Vendedores[i].IdCliente = item.IdDirectorio;
                        item.Vendedores[i].FechaModifica = DateTime.Now;
                        this.Db.Saveable<ContribuyenteVendedorModel>((ContribuyenteVendedorModel)item.Vendedores[i]).ExecuteCommand();
                    }
                }
            }
            return item;
        }

        public bool Update(int index, string mail) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
               .SetColumns(it => new ContribuyenteDetailModel() {
                   Correo = mail
               }).Where(it => it.IdDirectorio == index);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// almacenar la bandera de validacion del RFC
        /// </summary>
        public bool ValidaRFC(int index, string razonSocial, string nombreComercial, string domicilioFiscal, bool valido) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
               .SetColumns(it => new ContribuyenteDetailModel() {
                   IsValidRFC = valido,
                   Nombre = razonSocial,
                   NombreComercial = nombreComercial,
                   DomicilioFiscal = domicilioFiscal
               }).Where(it => it.IdDirectorio == index);
            return this.Execute(result) > 0;
        }

        public bool ValidaRFC(IValidoRFC valido) {
            var result = this.Db.Updateable<ContribuyenteDetailModel>()
               .SetColumns(it => new ContribuyenteDetailModel() {
                   IsValidRFC = valido.IsValidRFC,
                   DomicilioFiscal = valido.DomicilioFiscal
               }).Where(it => it.IdDirectorio == valido.IdDirectorio);
            return this.Execute(result) > 0;
        }

        /// <summary>
        /// Listado 
        /// </summary>
        /// <typeparam name="T1">objeto</typeparam>
        /// <param name="conditionals">si un condicional contiene @search se hace la busqueda por nombre</param>
        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = "";
            // en caso del parche
            var relacion2 = conditionals.Where(it => it.FieldName.ToLower().Contains("drctrr")).FirstOrDefault();

            if (typeof(T1) == typeof(ContribuyenteDomicilioSingleModel)) {
                if (relacion2 != null) {
                    sqlCommand = @"select _drctr.*, _drccn.* from _drctr 
                               left join _drccn on _drccn._drccn_id = (select _drccn._drccn_id from _drccn where _drccn._drccn_drctr_id = _drctr._drctr_id and _drccn._drccn_a = 1 order by _drccn._drccn_ctdrc_id ASC LIMIT 1) @wcondiciones order by _drctr_nom";
                } else {
                    sqlCommand = @"select _drctr.*, _drccn.* from _drctr 
                              left join _drccn on _drccn._drccn_id = (select _drccn._drccn_id from _drccn where _drccn._drccn_drctr_id = _drctr._drctr_id and _drccn._drccn_a = 1 LIMIT 1) @wcondiciones order by _drctr_nom";
                }
            } else if (typeof(T1) == typeof(ContribuyenteDetailModel) | typeof(T1) == typeof(ContribuyenteModel)) {
                sqlCommand = @"select _drctr.* from _drctr @wcondiciones order by _drctr_nom";
            }

            // en caso de busqueda
            var search1 = conditionals.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
            if (search1 != null) {
                if (search1.FieldValue != string.Empty) {
                    // en este caso se filtra unicamente por el nombre porque no encontre solucion
                    //sqlCommand = sqlCommand.Replace("@wcondiciones", "where concat(lower(replace(replace(replace(replace(replace(_drctr_nom, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')), ',', lower(_drctr_clv), ',', lower(_drctr_rfc)) like @search @condiciones ");
                    sqlCommand = sqlCommand.Replace("@wcondiciones", "where concat(lower(replace(replace(replace(replace(replace(_drctr_nom, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u'))) like @search @condiciones ");
                    sqlCommand = sqlCommand.Replace("@search", "'%" + search1.FieldValue.ToLower() + "%'");
                }
                conditionals.Remove(search1);
            }

            if (relacion2 != null) {
                sqlCommand = sqlCommand.Replace("from _drctr", ", DRCTRR.* from _drctr left join DRCTRR on _drctr._drctr_id = DRCTRR.drctrr_drctr_id ");
            }
            //var t = this.Convierte(conditionals);
            //var query = this.Db.Queryable<T1>(this.GetQuerySQL(sqlCommand, conditionals));
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IEnumerable<T1> GetBeneficiarios<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones";
            return this.GetMapper<T1>(CommandText, conditionals).ToList();
        }
    }
}
