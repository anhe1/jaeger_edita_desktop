﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de domicilios
    /// </summary>
    public class SqlSugarDomicilioRepository : MySqlSugarContext<DomicilioFiscalModel>, ISqlDomicilioRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        /// <param name="user"></param>
        public SqlSugarDomicilioRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// guardar domicilio
        /// </summary>
        public IDomicilioFiscalModel Save(IDomicilioFiscalModel domicilio) {
            if (domicilio.IdDomicilio == 0) {
                domicilio.FechaNuevo = DateTime.Now;
                domicilio.Creo = this.User;
                var query = this.Db.Insertable<DomicilioFiscalModel>(domicilio);
                domicilio.IdDomicilio = this.Execute(query);
            } else {
                domicilio.FechaModifica = DateTime.Now;
                domicilio.Modifica = this.User;
                var query = this.Db.Updateable<DomicilioFiscalModel>(domicilio);
                this.Execute(query);
            }
            return domicilio;
        }

        public IDomicilioFiscalDetailModel Save(IDomicilioFiscalDetailModel domicilio) {
            if (domicilio.IdDomicilio == 0) {
                domicilio.FechaNuevo = DateTime.Now;
                domicilio.Creo = this.User;
                var query = this.Db.Insertable<DomicilioFiscalModel>(domicilio);
                domicilio.IdDomicilio = this.Execute(query);
            } else {
                domicilio.FechaModifica = DateTime.Now;
                domicilio.Modifica = this.User;
                var query = this.Db.Updateable<DomicilioFiscalModel>(domicilio);
                this.Execute(query);
            }
            return domicilio;
        }
    }
}
