﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de contactos del directorio
    /// </summary>
    public class SqlSugarContactoRepository : MySqlSugarContext<ContactoModel>, ISqlContactoRepository {
        /// <summary>
        /// repositorio de contactos del repositorio
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        /// <param name="user">usuario registrado</param>
        public SqlSugarContactoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// Almacenar un contacto del directorio
        /// </summary>
        public IContactoDetailModel Save(IContactoDetailModel contacto) {
            if (contacto.IdContacto == 0) {
                contacto.Creo = this.User;
                contacto.FechaNuevo = DateTime.Now;
                var query = this.Db.Insertable<ContactoDetailModel>(contacto);
                contacto.IdContacto = this.Insert(query);
            } else {
                contacto.Modifica = this.User;
                contacto.FechaModifica = DateTime.Now;
                var query = this.Db.Updateable<ContactoDetailModel>(contacto);
                this.Execute(query);
            }
            return contacto;
        }
    }
}
