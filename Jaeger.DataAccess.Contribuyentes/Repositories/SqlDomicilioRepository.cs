﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.DataAccess.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de domicilios
    /// </summary>
    public class SqlDomicilioRepository : MySqlSugarContext<DomicilioFiscalModel>, ISqlDomicilioRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        /// <param name="user"></param>
        public SqlDomicilioRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override int Insert(DomicilioFiscalModel item) {
            var sqlCommand = @"INSERT INTO DRCCN (DRCCN_ID, DRCCN_A, DRCCN_DRCTR_ID, DRCCN_TTL, DRCCN_CDGPS, DRCCN_CP, DRCCN_TP, DRCCN_ASNH, DRCCN_ASNTMNT_ID, DRCCN_EXTR, DRCCN_INTR, DRCCN_CLL, DRCCN_CLN, DRCCN_DLG, DRCCN_CDD, DRCCN_STD, DRCCN_PS, DRCCN_LCLDD, DRCCN_RFRNC, DRCCN_TLFNS, DRCCN_DSP, DRCCN_REQ, DRCCN_USR_N, DRCCN_FN, DRCCN_AUTO_ID, DRCCN_CTDRC_ID)
                                          VALUES (@DRCCN_ID,@DRCCN_A,@DRCCN_DRCTR_ID,@DRCCN_TTL,@DRCCN_CDGPS,@DRCCN_CP,@DRCCN_TP,@DRCCN_ASNH,@DRCCN_ASNTMNT_ID,@DRCCN_EXTR,@DRCCN_INTR,@DRCCN_CLL,@DRCCN_CLN,@DRCCN_DLG,@DRCCN_CDD,@DRCCN_STD,@DRCCN_PS,@DRCCN_LCLDD,@DRCCN_RFRNC,@DRCCN_TLFNS,@DRCCN_DSP,@DRCCN_REQ,@DRCCN_USR_N,@DRCCN_FN,@DRCCN_AUTO_ID,@DRCCN_CTDRC_ID) RETURNING DRCCN_ID;";
            var d0 = new List<SqlSugar.SugarParameter> {
                new SqlSugar.SugarParameter("@DRCCN_ID", DBNull.Value),
                new SqlSugar.SugarParameter("@DRCCN_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCCN_DRCTR_ID", item.IdContribuyente),
                new SqlSugar.SugarParameter("@DRCCN_TTL", item.Titulo),
                new SqlSugar.SugarParameter("@DRCCN_CDGPS", item.CodigoPais),
                new SqlSugar.SugarParameter("@DRCCN_CP", item.CodigoPostal),
                new SqlSugar.SugarParameter("@DRCCN_TP", item.Tipo),
                new SqlSugar.SugarParameter("@DRCCN_ASNH", item.Asentamiento),
                new SqlSugar.SugarParameter("@DRCCN_ASNTMNT_ID", 0),
                new SqlSugar.SugarParameter("@DRCCN_EXTR", item.NoExterior),
                new SqlSugar.SugarParameter("@DRCCN_INTR", item.NoInterior),
                new SqlSugar.SugarParameter("@DRCCN_CLL", item.Calle),
                new SqlSugar.SugarParameter("@DRCCN_CLN", item.Colonia),
                new SqlSugar.SugarParameter("@DRCCN_DLG", item.Municipio),
                new SqlSugar.SugarParameter("@DRCCN_CDD", item.Ciudad),
                new SqlSugar.SugarParameter("@DRCCN_STD", item.Estado),
                new SqlSugar.SugarParameter("@DRCCN_PS", item.Pais),
                new SqlSugar.SugarParameter("@DRCCN_LCLDD", item.Localidad),
                new SqlSugar.SugarParameter("@DRCCN_RFRNC", item.Referencia),
                new SqlSugar.SugarParameter("@DRCCN_TLFNS", item.Telefono),
                new SqlSugar.SugarParameter("@DRCCN_DSP", item.Notas),
                new SqlSugar.SugarParameter("@DRCCN_REQ", item.Requerimiento),
                new SqlSugar.SugarParameter("@DRCCN_USR_N", item.Creo),
                new SqlSugar.SugarParameter("@DRCCN_FN", item.FechaNuevo),
                new SqlSugar.SugarParameter("@DRCCN_USR_M", item.Modifica),
                new SqlSugar.SugarParameter("@DRCCN_FM", item.FechaModifica),
                new SqlSugar.SugarParameter("@DRCCN_AUTO_ID", item.IdAutorizado),
                new SqlSugar.SugarParameter("@DRCCN_CTDRC_ID", item.IdTipoDomicilio),
            };
            item.IdDomicilio = (int)this.Db.Ado.GetScalar(sqlCommand, d0);
            if (item.IdDomicilio > 0)
                return item.IdDomicilio;
            return 0;
        }

        public override int Update(DomicilioFiscalModel item) {
            var sqlCommand = @"UPDATE DRCCN SET DRCCN_A = @DRCCN_A, DRCCN_DRCTR_ID = @DRCCN_DRCTR_ID, DRCCN_TTL = @DRCCN_TTL, DRCCN_CDGPS = @DRCCN_CDGPS, DRCCN_CP = @DRCCN_CP, DRCCN_TP = @DRCCN_TP, 
DRCCN_ASNH = @DRCCN_ASNH, DRCCN_ASNTMNT_ID = @DRCCN_ASNTMNT_ID, DRCCN_EXTR = @DRCCN_EXTR, DRCCN_INTR = @DRCCN_INTR, DRCCN_CLL = @DRCCN_CLL, DRCCN_CLN = @DRCCN_CLN, DRCCN_DLG = @DRCCN_DLG, DRCCN_CDD = @DRCCN_CDD, 
DRCCN_STD = @DRCCN_STD, DRCCN_PS = @DRCCN_PS, DRCCN_LCLDD = @DRCCN_LCLDD, DRCCN_RFRNC = @DRCCN_RFRNC, DRCCN_TLFNS = @DRCCN_TLFNS, DRCCN_DSP = @DRCCN_DSP, DRCCN_REQ = @DRCCN_REQ, DRCCN_USR_M = @DRCCN_USR_M, 
DRCCN_FM = @DRCCN_FM, DRCCN_AUTO_ID = @DRCCN_AUTO_ID, DRCCN_CTDRC_ID = @DRCCN_CTDRC_ID WHERE ((DRCCN_ID = @DRCCN_ID))";
            var d0 = new List<SqlSugar.SugarParameter> {
                new SqlSugar.SugarParameter("@DRCCN_ID", item.IdDomicilio),
                new SqlSugar.SugarParameter("@DRCCN_A", item.Activo),
                new SqlSugar.SugarParameter("@DRCCN_DRCTR_ID", item.IdContribuyente),
                new SqlSugar.SugarParameter("@DRCCN_TTL", item.Titulo),
                new SqlSugar.SugarParameter("@DRCCN_CDGPS", item.CodigoPais),
                new SqlSugar.SugarParameter("@DRCCN_CP", item.CodigoPostal),
                new SqlSugar.SugarParameter("@DRCCN_TP", item.Tipo),
                new SqlSugar.SugarParameter("@DRCCN_ASNH", item.Asentamiento),
                new SqlSugar.SugarParameter("@DRCCN_ASNTMNT_ID", 0),
                new SqlSugar.SugarParameter("@DRCCN_EXTR", item.NoExterior),
                new SqlSugar.SugarParameter("@DRCCN_INTR", item.NoInterior),
                new SqlSugar.SugarParameter("@DRCCN_CLL", item.Calle),
                new SqlSugar.SugarParameter("@DRCCN_CLN", item.Colonia),
                new SqlSugar.SugarParameter("@DRCCN_DLG", item.Municipio),
                new SqlSugar.SugarParameter("@DRCCN_CDD", item.Ciudad),
                new SqlSugar.SugarParameter("@DRCCN_STD", item.Estado),
                new SqlSugar.SugarParameter("@DRCCN_PS", item.Pais),
                new SqlSugar.SugarParameter("@DRCCN_LCLDD", item.Localidad),
                new SqlSugar.SugarParameter("@DRCCN_RFRNC", item.Referencia),
                new SqlSugar.SugarParameter("@DRCCN_TLFNS", item.Telefono),
                new SqlSugar.SugarParameter("@DRCCN_DSP", item.Notas),
                new SqlSugar.SugarParameter("@DRCCN_REQ", item.Requerimiento),
                new SqlSugar.SugarParameter("@DRCCN_USR_N", item.Creo),
                new SqlSugar.SugarParameter("@DRCCN_FN", item.FechaNuevo),
                new SqlSugar.SugarParameter("@DRCCN_USR_M", item.Modifica),
                new SqlSugar.SugarParameter("@DRCCN_FM", item.FechaModifica),
                new SqlSugar.SugarParameter("@DRCCN_AUTO_ID", item.IdAutorizado),
                //new SqlSugar.SugarParameter("@DRCCN_SYNC_ID", item.id),
                new SqlSugar.SugarParameter("@DRCCN_CTDRC_ID", item.IdTipoDomicilio),
            };
            return (int)this.ExecuteTransaction(sqlCommand, d0);
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = @"SELECT DRCCN.* FROM DRCCN @wcondiciones ORDER BY DRCCN_ID DESC";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        /// <summary>
        /// guardar domicilio
        /// </summary>
        public IDomicilioFiscalModel Save(IDomicilioFiscalModel domicilio) {
            if (domicilio.IdDomicilio == 0) {
                domicilio.FechaNuevo = DateTime.Now;
                domicilio.Creo = this.User;
                
                domicilio.IdDomicilio = this.Insert(domicilio as DomicilioFiscalModel);
            } else {
                domicilio.FechaModifica = DateTime.Now;
                domicilio.Modifica = this.User;
                var query = this.Update(domicilio as DomicilioFiscalModel);
            }
            return domicilio;
        }

        public IDomicilioFiscalDetailModel Save(IDomicilioFiscalDetailModel domicilio) {
            if (domicilio.IdDomicilio == 0) {
                domicilio.FechaNuevo = DateTime.Now;
                domicilio.Creo = this.User;

                domicilio.IdDomicilio = this.Insert(domicilio as DomicilioFiscalModel);
            } else {
                domicilio.FechaModifica = DateTime.Now;
                domicilio.Modifica = this.User;
                var query = this.Update(domicilio as DomicilioFiscalModel);
            }
            return domicilio;
        }
    }
}
