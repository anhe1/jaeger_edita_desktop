﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace Jaeger.Amazon.SES.Abstractions {
    public abstract class MasterMailServer {
        private SmtpClient smtpClient;
        protected string senderMail {
            get; set;
        }
        protected string password {
            get; set;
        }
        protected string host {
            get; set;
        }
        protected int port {
            get; set;
        }
        protected bool ssl {
            get; set;
        }

        protected void initializeSmtpClient() {
            this.smtpClient = new SmtpClient();
            this.smtpClient.Credentials = new NetworkCredential(this.senderMail, this.password);
            this.smtpClient.Host = this.host;
            this.smtpClient.Port = this.port;
            this.smtpClient.EnableSsl = this.ssl;
        }

        public void sendMail(string subject, string body, List<string> recipientMail) {
            var mailMessage = new MailMessage();
            try {
                mailMessage.From = new MailAddress(senderMail);
                foreach (string mail in recipientMail) {
                    mailMessage.To.Add(mail);
                }
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.Priority = MailPriority.Normal;
                smtpClient.Send(mailMessage);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
