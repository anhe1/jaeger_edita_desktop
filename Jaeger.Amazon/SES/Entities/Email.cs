﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Jaeger.Amazon.SES.ValueObjects;

namespace Jaeger.Amazon.SES.Entities {
    public class Email {
        private EnumEmailStatus emailStatusField;
        private string emisorField;
        private string receptorField;
        private string asuntoField;
        private DateTime fechaRecepcionField;
        private string contenidoField;
        private List<EmailAttachment> adjuntosField;

        public Email() {
            this.emisorField = string.Empty;
            this.receptorField = string.Empty;
            this.asuntoField = string.Empty;
            this.fechaRecepcionField = DateTime.Now;
            this.contenidoField = string.Empty;
            this.adjuntosField = new List<EmailAttachment>();
        }

        public Email(string sender, string recipient, string subject, DateTime received) {
            this.emisorField = string.Empty;
            this.receptorField = string.Empty;
            this.asuntoField = string.Empty;
            this.fechaRecepcionField = DateTime.Now;
            this.contenidoField = string.Empty;
            this.adjuntosField = new List<EmailAttachment>();
            this.Emisor = sender;
            this.Receptor = recipient;
            this.Asunto = subject;
            this.Fecha = received;
        }

        [XmlElement]
        public List<EmailAttachment> Adjuntos {
            get {
                return this.adjuntosField;
            }
            set {
                this.adjuntosField = value;
            }
        }

        [XmlAttribute]
        public string Contenido {
            get {
                return this.contenidoField;
            }
            set {
                this.contenidoField = value;
            }
        }

        [XmlAttribute]
        public DateTime Fecha {
            get {
                return this.fechaRecepcionField;
            }
            set {
                this.fechaRecepcionField = value;
            }
        }

        [XmlAttribute]
        public string Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
            }
        }

        [XmlAttribute]
        public string Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
            }
        }

        [XmlAttribute]
        public EnumEmailStatus Status {
            get {
                return this.emailStatusField;
            }
            set {
                if (this.emailStatusField != value) {
                    this.emailStatusField = value;
                }
            }
        }

        [XmlAttribute]
        public string Asunto {
            get {
                return this.asuntoField;
            }
            set {
                this.asuntoField = value;
            }
        }
    }
}