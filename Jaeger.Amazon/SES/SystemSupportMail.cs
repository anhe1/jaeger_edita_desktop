﻿using Jaeger.Amazon.SES.Abstractions;

namespace Jaeger.Amazon.SES {
    public class SystemSupportMail : MasterMailServer {
        public SystemSupportMail() {
            this.senderMail = "";
            this.password = "";
            this.host = "";
            this.port = 587;
            this.ssl = true;
            this.initializeSmtpClient();
        }
    }
}
