﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Jaeger.Amazon.SES.Entities;

namespace Jaeger.Amazon.SES {
    public class SimpleEmailServiceAWS {
        private Settings configuracion;

        /// <summary>
        /// constructor
        /// </summary>
        public SimpleEmailServiceAWS() {
        }

        /// <summary>
        /// constructor
        /// </summary>
        public SimpleEmailServiceAWS(Settings configuracion) {
            this.configuracion = configuracion;
        }

        public Settings Configuracion {
            get {
                return this.configuracion;
            }
            set {
                this.configuracion = value;
            }
        }

        private Stream GetStreamDeBase64(string strB64) {
            return new MemoryStream(Convert.FromBase64String(strB64));
        }

        public bool SendEmail(Email objEmail) {
            List<EmailAttachment>.Enumerator enumerator = new List<EmailAttachment>.Enumerator();
            bool blnResult = false;
            MailMessage mensaje = new MailMessage() {
                From = new MailAddress(this.configuracion.Email)
            };
            mensaje.To.Add(new MailAddress(objEmail.Receptor));
            mensaje.Subject = objEmail.Asunto;
            mensaje.SubjectEncoding = Encoding.UTF8;
            mensaje.Body = objEmail.Contenido;
            if (objEmail.Adjuntos.Count > 0) {
                try {
                    enumerator = objEmail.Adjuntos.GetEnumerator();
                    while (enumerator.MoveNext()) {
                        EmailAttachment objAttachment = enumerator.Current;
                        if (objAttachment.Seleccionado) {
                            Attachment objAttach = new Attachment(this.GetStreamDeBase64(objAttachment.ContentB64), objAttachment.NombreArchivo) {
                                ContentType = new ContentType("application/octet-stream")
                            };
                            ContentDisposition disposition = objAttach.ContentDisposition;
                            disposition.DispositionType = "attachment";
                            disposition.CreationDate = DateTime.Now;
                            disposition.ModificationDate = DateTime.Now;
                            disposition.ReadDate = DateTime.Now;
                            mensaje.Attachments.Add(objAttach);
                        }
                    }
                } finally {
                    ((IDisposable)enumerator).Dispose();
                }
            }
            try {
                using (SmtpClient client = new SmtpClient(this.configuracion.Servidor, Convert.ToInt32(this.configuracion.Puerto))) {
                    client.Credentials = new NetworkCredential(this.configuracion.Usuario, this.configuracion.Password);
                    client.EnableSsl = true;
                    client.Send(mensaje);
                }
                blnResult = true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return blnResult;
        }
    }
}
