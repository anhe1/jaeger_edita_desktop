﻿namespace Jaeger.Amazon.SES.ValueObjects {
    public enum EnumEmailStatus {
        OnHold,
        Read,
        Unread,
        Ready,
        Enviado,
        Error
    }
}
