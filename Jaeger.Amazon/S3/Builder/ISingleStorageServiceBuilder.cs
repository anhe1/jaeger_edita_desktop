﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Amazon.S3.Build {
    public interface ISingleStorageServiceBuilder {
        ISingleStorageServiceSecretAccessKeyBuilder WithAccessKey(string accessKey);
    }

    public interface ISingleStorageServiceRegionBuilder {
        ISingleStorageService WithRegion(string region);
    }

    public interface ISingleStorageServiceAccessKeyIdBuilder {

    }

    public interface ISingleStorageServiceSecretAccessKeyBuilder {
        ISingleStorageServiceRegionBuilder WithSecretAccessKey(string SecretAccessKey);
    }
}
