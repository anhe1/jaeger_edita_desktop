﻿using System.IO;

namespace Jaeger.Amazon.S3.Build {
    public interface IRequestBuilder {

    }

    public interface IRequestUploadBuilder {
        IRequestFileNameBuilder ForFile(Stream inputStream);
        IRequestFileNameBuilder WithBytes(byte[] bytes);
    }

    public interface IRequestFileNameBuilder {
        IRequestKeyNameBuilder WithName(string fileName);
    }

    public interface IRequestKeyNameBuilder {
        IRequestContentTypeBuilder WithKeyName(string keyName);
    }

    public interface IRequestContentTypeBuilder {
        IRequestRegionBuilder WithContentType(string contentType);
    }

    public interface IRequestRegionBuilder {

    }

    public interface IRequestStorageClassTypeBuilder {

    }
}
