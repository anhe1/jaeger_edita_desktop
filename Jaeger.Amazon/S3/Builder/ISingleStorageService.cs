﻿using System;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;

namespace Jaeger.Amazon.S3.Builder {
    public interface ISingleStorageService {
        ISingleStorageServiceRegionEndPoint AddCredentials(string accessKeyId, string secretAccessKey);

        /// <summary>
        /// obtener o establecer credenciales de amazon
        /// </summary>
        AWSCredentials Credentials { get; set; }
    }

    public interface ISingleStorageServiceRegionEndPoint {
        ISingleStorageServiceConfiguration AddRegionEndPoint(string regionEndPoint);
    }

    public interface ISingleStorageServiceConfiguration {
        ISingleStorageService1 WithBuckeName(string bucketName);
    }

    public interface ISingleStorageService1 {
        ISingleStorageService1 AddKeyName(string keyName);
    }

    public interface ISingleStorageService2 {
        ISingleStorageService1 Execute();
    }

    public class SingleStorageService : Abstractions.SimpleStorageServiceBase, ISingleStorageService, ISingleStorageServiceRegionEndPoint, ISingleStorageServiceConfiguration, ISingleStorageService1, ISingleStorageService2 {
        protected internal string _BucketName;
        protected internal string _KeyName;

        public SingleStorageService() {
        }

        public AWSCredentials Credentials { get; set; }

        /// <summary>
        /// obtener o establecer cliente amazon S3
        /// </summary>
        private AmazonS3Client ClientS3 { get; set; }

        public ISingleStorageServiceRegionEndPoint AddCredentials(string accessKeyId, string secretAccessKey) {
            this.Credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
            return this;
        }

        

        public ISingleStorageServiceConfiguration AddRegionEndPoint(string regionEndpoint) {
            var config = new AmazonS3Config() { RegionEndpoint = RegionEndpoint.USEast1 };
            switch (regionEndpoint) {
                case "us-east-1":
                    config.RegionEndpoint = RegionEndpoint.USEast1;
                    break;
                case "us-west-1":
                    config.RegionEndpoint = RegionEndpoint.USWest1;
                    break;
                default:
                    config.RegionEndpoint = RegionEndpoint.USEast1;
                    break;
            }
            this.ClientS3 = new AmazonS3Client(this.Credentials, config);
            return this;
        }

        public ISingleStorageService1 AddKeyName(string keyName) {
            this._KeyName = keyName;
            return this;
        }

        public ISingleStorageService1 Execute() {
            throw new NotImplementedException();
        }

        public ISingleStorageService1 WithBuckeName(string bucketName) {
            this._BucketName = bucketName;
            return this;
        }
    }
}
