﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Jaeger.Amazon.S3.Abstractions;

namespace Jaeger.Amazon.S3.Helpers {
    /// <summary>
    /// interface del servicio Amazon Simple Storage Service (Amazon S3)
    /// </summary>
    public class SimpleStorage3AWS : SimpleStorageServiceBase, ISimpleStorage3AWS {
        private string fileNameLog = @"C:\Jaeger\Jaeger.Log\jaeger_amazon_aws.log";
        public AmazonS3Config AmazonS3Config { get; internal set; }
        public AWSCredentials Credentials { get; internal set; }
        public RegionEndpoint RegionEndpoint { get; internal set; }
        private AmazonS3Client clientS3;
        internal SimpleStorage3AWS() {
            
        }

        public SimpleStorage3AWS WithCredentials(AmazonS3Client clientS3) {
            this.clientS3 = clientS3;
            return this;
        }
    }

    public interface ISimpleStorage3AWS {
        SimpleStorage3AWS WithCredentials(AmazonS3Client clientS3);
    }

    public interface ICredentials {
        IRegionEndPoint WithAccessKeyId(string AccessKeyId, string SecretAccessKey);
    }

    public interface IRegionEndPoint {
        IBuild WithRegionEndPoint(string regionEndpoint);
    }

    public interface IBuild {
        ISimpleStorage3AWS Build();
    }

    public class SimpleStorage3Builder : ICredentials, IRegionEndPoint, IBuild {
        ISimpleStorage3AWS _Simple3;
        AWSCredentials Credentials;
        AmazonS3Config _AmazonS3Config;
        AmazonS3Client ClientS3;
        internal SimpleStorage3Builder() {
            
        }

        public static ICredentials Create() {
            return new SimpleStorage3Builder();
        }

        public ISimpleStorage3AWS Build() {
            this.ClientS3 = new AmazonS3Client(this.Credentials, this._AmazonS3Config);
            return new SimpleStorage3AWS().WithCredentials(this.ClientS3);
        }

        public IRegionEndPoint WithAccessKeyId(string AccessKeyId, string SecretAccessKey) {
            this.Credentials = new BasicAWSCredentials(AccessKeyId, SecretAccessKey);
            return this;
        }

        public IBuild WithRegionEndPoint(string regionEndpoint) {
            this._AmazonS3Config = new AmazonS3Config() { RegionEndpoint = RegionEndpoint.USEast1 };
            switch (regionEndpoint) {
                case "us-east-1":
                    this._AmazonS3Config.RegionEndpoint = RegionEndpoint.USEast1;
                    break;
                case "us-west-1":
                    this._AmazonS3Config.RegionEndpoint = RegionEndpoint.USWest1;
                    break;
                default:
                    this._AmazonS3Config.RegionEndpoint = RegionEndpoint.USEast1;
                    break;
            }
            IRequest d = new Request().WithBucketName().WithKeyName("").WithRegion("").WithFileName("").Execute();
            
            return this;
        }
    }
    // Stream inputStream, string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar
    public class Request : IRequest, IRequestBucketName, IRequestKeyName, IRquestRegion, IRequestFileName {
        public Request Execute() {
            throw new System.NotImplementedException();
        }

        public IRequestBucketName WithBucketName() {
            throw new System.NotImplementedException();
        }

        public IRequestFileName WithFileName(string d) {
            throw new System.NotImplementedException();
        }

        public IRequestKeyName WithKeyName(string keyName) {
            throw new System.NotImplementedException();
        }

        public IRquestRegion WithRegion(string e) {
            throw new System.NotImplementedException();
        }
    }

    public interface IRequest : IRequestBucketName, IRequestKeyName, IRquestRegion, IRequestFileName {
        IRequestBucketName WithBucketName();
    }

    public interface IRequestBucketName {
        IRequestKeyName WithKeyName(string keyName);
    }
    
    public interface IRequestKeyName {
        IRquestRegion WithRegion(string e);
    }

    public interface IRquestRegion {
        IRequestFileName WithFileName(string d);
    }

    public interface IRequestFileName {
        Request Execute();
    }
}
