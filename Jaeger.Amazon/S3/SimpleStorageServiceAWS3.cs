﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Jaeger.Amazon.S3.Abstractions;
using Jaeger.Amazon.S3.Contracts;
using Jaeger.Amazon.S3.ValueObjects;

namespace Jaeger.Amazon.S3.Helpers {
    /// <summary>
    /// interface del servicio Amazon Simple Storage Service (Amazon S3)
    /// </summary>
    public class SimpleStorageServiceAWS3 : SimpleStorageServiceBase, ISimpleStorageService {
        private string fileNameLog = @"C:\Jaeger\Jaeger.Log\jaeger_amazon_aws.log";

        public SimpleStorageServiceAWS3() {
            
            
        }

        public SimpleStorageServiceAWS3(string AccessKeyId, string SecretAccessKey, string regionEndpoint) {
            this.CreateConfig(AccessKeyId, SecretAccessKey, regionEndpoint);
        }

        public string Message { get; set; }

        /// <summary>
        /// obtener o establecer codigo de status
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// obtener o establecer credenciales de amazon
        /// </summary>
        public AWSCredentials Credentials { get; set; }

        /// <summary>
        /// obtener o establecer cliente amazon S3
        /// </summary>
        private AmazonS3Client ClientS3 { get; set; }

        private RegionEndpoint RegionEndpoint { get; set; }

        public void CreateConfig(string AccessKeyId, string SecretAccessKey, string regionEndpoint) {
            this.Credentials = new BasicAWSCredentials(AccessKeyId, SecretAccessKey);
            var config = new AmazonS3Config() { RegionEndpoint = RegionEndpoint.USEast1 };
            switch (regionEndpoint) {
                case "us-east-1":
                    config.RegionEndpoint = RegionEndpoint.USEast1;
                    break;
                case "us-west-1":
                    config.RegionEndpoint = RegionEndpoint.USWest1;
                    break;
                default:
                    config.RegionEndpoint = RegionEndpoint.USEast1;
                    break;
            }
            this.ClientS3 = new AmazonS3Client(this.Credentials, config);
        }

        /// <summary>
        /// Upload a file. The file name is used as the object key name.
        /// </summary>
        /// <param name="inputStream">The stream to upload.</param>
        /// <param name="keyName">The key name you will make a request against.</param>
        /// <param name="contentType"></param>
        /// <param name="makePublic"></param>
        /// <param name="bucketName">The bucket name you will make a request against.</param>
        /// <param name="region">The region you are using. For example us-east-1.</param>
        /// <returns>An authenticated URL.</returns>
        public string Upload(Stream inputStream, string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar) {
            this.StatusCode = 200;
            this.Message = string.Empty;
            var requestUrl = SimpleStorageServiceBase.BuildURL(region, bucketName, keyName);

            PutObjectRequest request = new PutObjectRequest() {
                InputStream = inputStream,
                BucketName = bucketName,
                Key = keyName,
                ContentType = contentType
            };

            // tipo de almacenamiento
            if (storageClassTypeEnum == StorageClassTypeEnum.ReducedRedundancy)
                request.StorageClass = S3StorageClass.ReducedRedundancy;
            else
                request.StorageClass = S3StorageClass.Standard;

            // crear objeto publico
            if (makePublic)
                request.CannedACL = S3CannedACL.PublicRead;

            try {
                PutObjectResponse response = this.ClientS3.PutObject(request);
                if (response.HttpStatusCode == HttpStatusCode.OK) {
                    this.StatusCode = 200;
                    this.Message = string.Empty;
                    return requestUrl;
                }
            }
            catch (AmazonS3Exception ex) {
                this.StatusCode = 403;
                this.Message = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return string.Empty;
        }

        /// <summary>
        /// Upload a file. The file name is used as the object key name.
        /// </summary>
        /// <param name="localFileName"></param>
        /// <param name="keyName"></param>
        /// <param name="contentType"></param>
        /// <param name="bucketName"></param>
        /// <param name="region"></param>
        /// <param name="makePublic"></param>
        /// <param name="storageClassTypeEnum"></param>
        /// <returns>An authenticated URL.</returns>
        public string Upload(string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar) {
            if (File.Exists(localFileName)) {
                using (var fileToUpload = new FileStream(localFileName, FileMode.Open, FileAccess.Read)) {
                    return this.Upload(fileToUpload, localFileName, keyName, contentType, bucketName, region, makePublic, storageClassTypeEnum);
                }
            }
            this.StatusCode = 1007;
            this.Message = "Error opening the file.";
            return string.Empty;
        }

        /// <summary>
        /// Upload a file. The file name is used as the object key name.
        /// </summary>
        /// <param name="localFileName">ruta completa del archivo local</param>
        /// <param name="bucketName">The bucket name you will make a request against.</param>
        /// <param name="keyName">The key name you will make a request against.</param>
        public bool Download(string localFileName, string bucketName, string keyName) {
            this.StatusCode = 200;
            this.Message = string.Empty;
            // Create a GetObject request
            GetObjectRequest request = new GetObjectRequest {
                BucketName = bucketName,
                Key = keyName,
            };

            try {
                // Issue request and remember to dispose of the response
                using (GetObjectResponse response = this.ClientS3.GetObject(request)) {
                    // Save object to local file
                    response.WriteResponseStreamToFile(localFileName);
                    return true;
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                this.LogWrite(ex.Message);
                this.Message = ex.Message;
                this.StatusCode = 403;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address">La URL contra la que realizará la solicitud.</param>
        /// <param name="localFileName">ruta completa del archivo local</param>
        public bool Download(string address, string localFileName) {
            try {
                var webClient = new WebClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var fileStream = new FileStream(localFileName, FileMode.Create);
                fileStream.Write(webClient.DownloadData(address), 0, checked(webClient.DownloadData(address).Length));
                fileStream.Flush();
                fileStream.Close();
                return true;
            }
            catch (Exception e) {
                this.LogWrite(e.Message);
                Console.WriteLine(e.Message);
            }
            return false;
        }

        /// <summary>
        /// eliminar objecto del bucket especifico
        /// </summary>
        /// <param name="bucketName">The bucket name you will make a request against.</param>
        /// <param name="keyName">The key name you will make a request against.</param>
        /// <returns></returns>
        public bool Delete(string bucketName, string keyName) {
            try {
                var deleteObjectRequest = new DeleteObjectRequest {
                    BucketName = bucketName,
                    Key = keyName
                };

                Console.WriteLine("Deleting an object");
                var response = this.ClientS3.DeleteObject(deleteObjectRequest);
                this.StatusCode = 200;
                return true;
            }
            catch (AmazonS3Exception e) {
                this.LogWrite(e.Message);
                Console.WriteLine("Error encountered on server. Message:'{0}' when deleting an object", e.Message);
            }
            catch (Exception e) {
                this.LogWrite(e.Message);
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when deleting an object", e.Message);
            }

            return false;
        }

        public bool Exists(string bucket, string key) {

            var request = new GetObjectRequest {
                BucketName = bucket, 
                Key = key,
                ByteRange = new ByteRange(0, 4)
            };
            
            Console.WriteLine(key); 
            string responseBody = "";
            try {
                using (var response = this.ClientS3.GetObjectAsync(request))
                using (Stream responseStream = response.Result.ResponseStream)
                using (StreamReader reader = new StreamReader(responseStream)) {
                    string title = response.Result.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
                    string contentType = response.Result.Headers["Content-Type"];
                    Console.WriteLine("Object metadata, Title: {0}", title);
                    Console.WriteLine("Content type: {0}", contentType);

                    responseBody = reader.ReadToEnd(); // Now you process the response body.
                    Console.WriteLine(responseBody);
                    if (response.Result.ResponseStream != null) {
                        return true;
                    }
                }
                //var response = this.ClientS3.GetObjectAsync(request);
                //if (response.Result.ResponseStream != null) {
                //    return true;
                //}
            }
            catch (AmazonS3Exception ex) {
                this.LogWrite(ex.Message);
                return false;
            }
            catch (WebException ex) {
                this.LogWrite(ex.Message);
                return false;
            }
            catch (Exception ex) {
                this.LogWrite(ex.Message);
                return false;
            }
            return false;
        }

        public void LogWrite(string oError) {
            try {
                if (!File.Exists(fileNameLog)) {
                    File.Create(fileNameLog).Close();
                }
                StreamWriter streamWriter = File.AppendText(fileNameLog);
                object[] type = new object[] { oError, "|", DateTime.Now };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        private static readonly Regex endpointRegex = new Regex(@"s3[.\-](.*?)\.amazonaws\.com$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.RightToLeft);

        /// <summary>
        /// Get corresponding region for input host.
        /// </summary>
        /// <param name="endpoint">S3 API endpoint</param>
        /// <returns>Region corresponding to the endpoint. Default is 'us-east-1'</returns>
        public static string GetRegionFromEndpoint(string endpoint) {
            Match match = endpointRegex.Match(endpoint);

            if (match.Success) {
                return match.Groups[1].Value;
            }

            return string.Empty;
        }
    }
}
