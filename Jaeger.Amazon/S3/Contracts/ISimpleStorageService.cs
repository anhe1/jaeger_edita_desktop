﻿using System.IO;
using Jaeger.Amazon.S3.ValueObjects;

namespace Jaeger.Amazon.S3.Contracts {
    /// <summary>
    /// interface del servicio Amazon Simple Storage Service (Amazon S3)
    /// </summary>
    public interface ISimpleStorageService {

        /// <summary>
        /// obtener o establecer mensaje de error
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// obtener o establecer codigo de status
        /// </summary>
        int StatusCode { get; set; }

        /// <summary>
        /// Upload a file. The file name is used as the object key name.
        /// </summary>
        /// <param name="inputStream">The stream to upload.</param>
        /// <param name="keyName">The key name you will make a request against.</param>
        /// <param name="contentType"></param>
        /// <param name="makePublic"></param>
        /// <param name="bucketName">The bucket name you will make a request against.</param>
        /// <param name="region">The region you are using. For example us-east-1.</param>
        /// <returns>An authenticated URL.</returns>
        string Upload(Stream inputStream, string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar);

        /// <summary>
        /// Upload a file. The file name is used as the object key name.
        /// </summary>
        /// <param name="localFileName"></param>
        /// <param name="keyName"></param>
        /// <param name="contentType"></param>
        /// <param name="bucketName"></param>
        /// <param name="region"></param>
        /// <param name="makePublic"></param>
        /// <param name="storageClassTypeEnum"></param>
        /// <returns>An authenticated URL.</returns>
        string Upload(string localFileName, string keyName, string contentType, string bucketName, string region, bool makePublic, StorageClassTypeEnum storageClassTypeEnum = StorageClassTypeEnum.Standar);

        /// <summary>
        /// Upload a file. The file name is used as the object key name.
        /// </summary>
        /// <param name="localFileName">ruta completa del archivo local</param>
        /// <param name="bucketName">The bucket name you will make a request against.</param>
        /// <param name="keyName">The key name you will make a request against.</param>
        /// <returns></returns>
        bool Download(string localFileName, string bucketName, string keyName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address">La URL contra la que realizará la solicitud.</param>
        /// <param name="localFileName">ruta completa del archivo local</param>
        /// <returns></returns>
        bool Download(string address, string localFileName);

        /// <summary>
        /// eliminar objecto del bucket especifico
        /// </summary>
        /// <param name="bucketName">The bucket name you will make a request against.</param>
        /// <param name="keyName">The key name you will make a request against.</param>
        /// <returns></returns>
        bool Delete(string bucketName, string keyName);

        bool Exists(string bucket, string key);
    }
}
