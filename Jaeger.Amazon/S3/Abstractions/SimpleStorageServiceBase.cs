﻿using Jaeger.Amazon.S3.Helpers;

namespace Jaeger.Amazon.S3.Abstractions {
    public abstract class SimpleStorageServiceBase : FileContentType {
        public SimpleStorageServiceBase() : base() {
        }

        public static string BuildURL(string RequestEndpoint, string bucketName, string keyName) {
            var requestUrl = string.Empty;
            if (RequestEndpoint != "us-east-1") {
                requestUrl = string.Concat(string.Concat("https://", "s3-", RequestEndpoint, ".amazonaws.com"), "/", bucketName, "/", keyName);
            }
            else {
                requestUrl = string.Concat("https://s3.amazonaws.com", "/", bucketName, "/", keyName);
            }
            return requestUrl;
        }

        //public static string BuildS3RequestURLShared(bool UseSSL, string RequestEndpoint, string BucketName, string KeyName, string QueryString) {
        //    BucketName = BucketName.Trim();
        //    string str = "";
        //    str = (!UseSSL ? string.Concat(str, "http://") : string.Concat(str, "https://"));
        //    bool isVirtualHostedBucket = SimpleStorageServiceBase.GetIsVirtualHostedBucket(BucketName);
        //    if (isVirtualHostedBucket && Operators.CompareString(BucketName, "", false) != 0) {
        //        str = string.Concat(str, BucketName, ".");
        //    }
        //    str = string.Concat(str, RequestEndpoint, "/");
        //    if (!isVirtualHostedBucket) {
        //        str = string.Concat(str, BucketName, "/");
        //    }
        //    str = string.Concat(str, Uri.EscapeDataString(KeyName));
        //    if (Operators.CompareString(QueryString, "", false) != 0) {
        //        if (Strings.InStr(QueryString, "?", CompareMethod.Binary) != 1) {
        //            QueryString = string.Concat("?", QueryString);
        //        }
        //        str = string.Concat(str, QueryString);
        //    }
        //    return str;
        //}

        //public static bool GetIsVirtualHostedBucket(string BucketName) {
        //    bool flag;
        //    if (Operators.CompareString(Strings.LCase(BucketName), BucketName, false) != 0) {
        //        flag = false;
        //    }
        //    else if (Strings.InStr(BucketName, "_", CompareMethod.Binary) <= 0) {
        //        flag = (BucketName.Length <= 63 ? true : false);
        //    }
        //    else {
        //        flag = false;
        //    }
        //    return flag;
        //}
    }
}
