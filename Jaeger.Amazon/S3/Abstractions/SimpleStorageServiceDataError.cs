﻿using Microsoft.VisualBasic.CompilerServices;

namespace Jaeger.Amazon.S3.Abstractions {
    public abstract class SimpleStorageServiceDataError {
        public static string GetErrorDescriptionFromErrorNumber(int ErrorNumber, string ExtraDescription) {
            string errorDescription = string.Empty;
            if (ErrorNumber == 1001) {
                errorDescription = "Invalid character in ExtraRequestHeader value.";
            }
            else if (ErrorNumber == 1002) {
                errorDescription = "Error when setting header.";
            }
            else if (ErrorNumber == 1003) {
                errorDescription = "Getting the response failed.";
            }
            else if (ErrorNumber == 1004) {
                errorDescription = "Opening the request stream failed.";
            }
            else if (ErrorNumber == 1005) {
                errorDescription = "Unexpected end of file.";
            }
            else if (ErrorNumber == 1006) {
                errorDescription = "Error getting the response stream.";
            }
            else if (ErrorNumber == 1007) {
                errorDescription = "Error opening the file.";
            }
            else if (ErrorNumber == 1008) {
                errorDescription = "The operation was aborted.";
            }
            else if (ErrorNumber == 1009) {
                errorDescription = "Error setting the byte range.";
            }
            else if (ErrorNumber == 1111) {
                errorDescription = "The range value is not valid.";
            }
            else if (ErrorNumber == 1112) {
                errorDescription = "Opening the local file failed.";
            }
            else if (ErrorNumber == 1113) {
                errorDescription = "Reading the source stream failed.";
            }
            else if (ErrorNumber == 1114) {
                errorDescription = "Writing to stream failed.";
            }
            else if (ErrorNumber == 1121) {
                errorDescription = "Reading the source file failed.";
            }
            else if (ErrorNumber == 1122) {
                errorDescription = "Writing to the upload stream failed.";
            }
            else if (ErrorNumber == 1123) {
                errorDescription = "Error opening local file.";
            }
            else if (ErrorNumber == 1131) {
                errorDescription = "The Method parameter is not valid.   Only GET, COPY, DELETE, and HEAD are allowed.";
            }
            else if (ErrorNumber == 1141) {
                errorDescription = "The Method parameter is not valid.   Only PUT is allowed.";
            }
            else if (ErrorNumber == 1142) {
                errorDescription = "Sending the request failed.";
            }
            else if (ErrorNumber == 1151) {
                errorDescription = "Error during encryption.";
            }
            else if (ErrorNumber == 1161) {
                errorDescription = "Error during decryption.";
            }
            else if (ErrorNumber == 1171) {
                errorDescription = "The ListBucketResult node does not exist in the response XML.";
            }
            else if (ErrorNumber == 1172) {
                errorDescription = "The operation was aborted.";
            }
            if (Operators.CompareString(ExtraDescription, "", false) != 0) {
                errorDescription = string.Concat(errorDescription, "   ", ExtraDescription);
            }
            return errorDescription;
        }
    }
}
