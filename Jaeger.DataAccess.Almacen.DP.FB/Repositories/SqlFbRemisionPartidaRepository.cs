﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.DP.Contracts;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.Almacen.DP.Repositories {
    public class SqlFbRemisionPartidaRepository : RepositoryMaster<RemisionConceptoModel>, ISqlRemisionPartidaRepository {
        public SqlFbRemisionPartidaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public RemisionConceptoModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM MVADP @wcondiciones"
            };

            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public IEnumerable<RemisionConceptoModel> GetList() {
            throw new NotImplementedException();
        }

        public int Insert(RemisionConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO MVADP (MVADP_ID, MVADP_A, MVADP_DOC_ID, MVADP_CTALM_ID, MVADP_ALMPT_ID, MVADP_RMSN_ID, MVADP_CTEFC_ID, MVADP_DRCTR_ID, MVADP_PDCLN_ID, MVADP_CTDPT_ID, MVADP_CTPRD_ID, MVADP_CTMDL_ID, MVADP_CTUND_ID, MVADP_UNDF, MVADP_UNDN, MVADP_CANTE, MVADP_CANTS, MVADP_PRDN, MVADP_UNTC, MVADP_UNDC, MVADP_UNTR, MVADP_UNTR2, MVADP_SBTTL, MVADP_DESC, MVADP_IMPRT, MVADP_TSIVA, MVADP_TRIVA, MVADP_TOTAL, MVADP_SKU, MVADP_USR_N, MVADP_FN, MVADP_NOMR, MVADP_COM) 
VALUES (@MVADP_ID, @MVADP_A, @MVADP_DOC_ID, @MVADP_CTALM_ID, @MVADP_ALMPT_ID, @MVADP_RMSN_ID, @MVADP_CTEFC_ID, @MVADP_DRCTR_ID, @MVADP_PDCLN_ID, @MVADP_CTDPT_ID, @MVADP_CTPRD_ID, @MVADP_CTMDL_ID, @MVADP_CTUND_ID, @MVADP_UNDF, @MVADP_UNDN, @MVADP_CANTE, @MVADP_CANTS, @MVADP_PRDN, @MVADP_UNTC, @MVADP_UNDC, @MVADP_UNTR, @MVADP_UNTR2, @MVADP_SBTTL, @MVADP_DESC, @MVADP_IMPRT, @MVADP_TSIVA, @MVADP_TRIVA, @MVADP_TOTAL, @MVADP_SKU, @MVADP_USR_N, @MVADP_FN,@MVADP_NOMR, @MVADP_COM) RETURNING MVADP_ID"
            };
            sqlCommand.Parameters.AddWithValue("@MVADP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@MVADP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MVADP_DOC_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@MVADP_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@MVADP_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTEFC_ID", item.IdEfecto);
            sqlCommand.Parameters.AddWithValue("@MVADP_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@MVADP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNDF", item.UnidadFactor);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@MVADP_CANTE", item.CantidadE);
            sqlCommand.Parameters.AddWithValue("@MVADP_CANTS", item.CantidadS);
            sqlCommand.Parameters.AddWithValue("@MVADP_PRDN", item.Producto);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNTC", item.CostoUnitario);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNDC", item.CostoUnidad);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNTR", item.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNTR2", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@MVADP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@MVADP_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@MVADP_IMPRT", item.Importe);
            sqlCommand.Parameters.AddWithValue("@MVADP_TSIVA", item.TasaIVA);
            sqlCommand.Parameters.AddWithValue("@MVADP_TRIVA", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@MVADP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@MVADP_SKU", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@MVADP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@MVADP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@MVADP_NOMR", item.Cliente);
            sqlCommand.Parameters.AddWithValue("@MVADP_COM", item.Componente);

            item.IdMovimiento = ExecuteScalar(sqlCommand);
            if (item.IdMovimiento > 0)
                return item.IdMovimiento;
            return 0;
        }

        public RemisionConceptoDetailModel Save(RemisionConceptoDetailModel model) {
            if (model.IdMovimiento == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = User;
                model.IdMovimiento = Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = User;
                Update(model);
            }
            return model;
        }

        public int Update(RemisionConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE MVADP SET MVADP_A = @MVADP_A, MVADP_DOC_ID = @MVADP_DOC_ID, MVADP_CTALM_ID = @MVADP_CTALM_ID, MVADP_ALMPT_ID = @MVADP_ALMPT_ID, MVADP_RMSN_ID = @MVADP_RMSN_ID, MVADP_CTEFC_ID = @MVADP_CTEFC_ID, MVADP_DRCTR_ID = @MVADP_DRCTR_ID, MVADP_PDCLN_ID = @MVADP_PDCLN_ID, MVADP_CTDPT_ID = @MVADP_CTDPT_ID, MVADP_CTPRD_ID = @MVADP_CTPRD_ID, MVADP_CTMDL_ID = @MVADP_CTMDL_ID, MVADP_CTUND_ID = @MVADP_CTUND_ID, MVADP_UNDF = @MVADP_UNDF, MVADP_UNDN = @MVADP_UNDN, MVADP_CANTE = @MVADP_CANTE, MVADP_CANTS = @MVADP_CANTS, MVADP_PRDN = @MVADP_PRDN, MVADP_UNTC = @MVADP_UNTC, MVADP_UNDC = @MVADP_UNDC, MVADP_UNTR = @MVADP_UNTR, MVADP_UNTR2 = @MVADP_UNTR2, MVADP_SBTTL = @MVADP_SBTTL, MVADP_DESC = @MVADP_DESC, MVADP_IMPRT = @MVADP_IMPRT, MVADP_TSIVA = @MVADP_TSIVA, MVADP_TRIVA = @MVADP_TRIVA, MVADP_TOTAL = @MVADP_TOTAL, MVADP_SKU = @MVADP_SKU, MVADP_USR_M = @MVADP_USR_M, MVADP_FM = @MVADP_FM, MVADP_NOMR = @MVADP_NOMR, MVADP_COM = @MVADP_COM WHERE (MVADP_ID = @MVADP_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@MVADP_ID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@MVADP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MVADP_DOC_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@MVADP_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@MVADP_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTEFC_ID", item.IdEfecto);
            sqlCommand.Parameters.AddWithValue("@MVADP_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@MVADP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@MVADP_CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNDF", item.UnidadFactor);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@MVADP_CANTE", item.CantidadE);
            sqlCommand.Parameters.AddWithValue("@MVADP_CANTS", item.CantidadS);
            sqlCommand.Parameters.AddWithValue("@MVADP_PRDN", item.Producto);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNTC", item.CostoUnitario);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNDC", item.CostoUnidad);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNTR", item.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@MVADP_UNTR2", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@MVADP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@MVADP_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@MVADP_IMPRT", item.Importe);
            sqlCommand.Parameters.AddWithValue("@MVADP_TSIVA", item.TasaIVA);
            sqlCommand.Parameters.AddWithValue("@MVADP_TRIVA", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@MVADP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@MVADP_SKU", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@MVADP_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@MVADP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@MVADP_NOMR", item.Cliente);
            sqlCommand.Parameters.AddWithValue("@MVADP_COM", item.Componente);
            return ExecuteTransaction(sqlCommand);
        }
    }
}
