﻿using System.ComponentModel;

namespace Jaeger.Domain.CCalidad.ValueObjects {
    public enum Clasificacion5Enum {
        [Description("No Aplica")]
        NA = 0,
        [Description("Maquinaria")]
        Maquinaria = 1,
        [Description("Método de Trabajo")]
        MetodoTrabajo = 2,
        [Description("Mano de Obra")]
        ManoObra = 3,
        [Description("Medio Ámbiente")]
        MedioAmbiente = 4,
        [Description("Materia Prima")]
        MateriaPrima = 5
    }
}
