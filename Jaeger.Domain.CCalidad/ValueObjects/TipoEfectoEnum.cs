﻿using System.ComponentModel;

namespace Jaeger.Domain.CCalidad.ValueObjects {
    public enum TipoEfectoEnum {
        [Description("No Aplica")]
        NA = 0,
        [Description("Calidad")]
        Calidad = 1,
        [Description("Cantidad")]
        Cantidad = 2,
        [Description("Costo")]
        Costo = 3,
        [Description("Tiempo")]
        Tiempo = 4
    }
}
