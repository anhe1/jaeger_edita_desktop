﻿using System.ComponentModel;

namespace Jaeger.Domain.CCalidad.ValueObjects {
    public enum TipoAccionEnum {
        [Description("No Aplica")]
        NA = 0,
        [Description("Consesión")]
        Consesion = 1,
        [Description("Reparación")]
        Reparacion = 2,
        [Description("Reproceso")]
        Reproceso = 3
    }
}
