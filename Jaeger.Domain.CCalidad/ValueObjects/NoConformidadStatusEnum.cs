﻿using System.ComponentModel;

namespace Jaeger.Domain.CCalidad.ValueObjects {
    public enum NoConformidadStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Proceso")]
        Proceso = 1,
        [Description("Autorizado")]
        Autorizado = 2,
        [Description("Auditado")]
        Auditado = 3,
    }
}
