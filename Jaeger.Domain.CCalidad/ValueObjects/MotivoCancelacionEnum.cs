﻿using System.ComponentModel;

namespace Jaeger.Domain.CCalidad.ValueObjects {
    public enum MotivoCancelacionEnum {
        [Description("No Aplica")]
        NA = 0,
        [Description("Comprobante emitido con errores")]
        PorError = 1,
        [Description("No se llevó a cabo la operación.")]
        SinOperacion = 2,
        [Description("Sustitución de un documento previo")]
        Sustitucion = 3,
        [Description("Rechazado por C. Calidad")]
        Rechazado = 4,
        [Description("Solicita Administrador")]
        Administrador = 99
    }
}
