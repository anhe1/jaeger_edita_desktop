﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.CCalidad.ValueObjects;

namespace Jaeger.Domain.CCalidad.Builder {
    public interface INoConformidadQueryBuilder : IConditionalBuilder {
        INoConformidadYearQueryBuilder Year(int year);
    }

    public interface INoConformidadYearQueryBuilder : IConditionalBuilder {
        INoConformidadMonthQueryBuilder Month(int month = 0);
        INoConformidadStatusQueryBuilder Status(NoConformidadStatusEnum status);
    }

    public interface INoConformidadMonthQueryBuilder : IConditionalBuilder {
        INoConformidadStatusQueryBuilder Status(NoConformidadStatusEnum status);
    }

    public interface INoConformidadStatusQueryBuilder : IConditionalBuilder {
    }
}
