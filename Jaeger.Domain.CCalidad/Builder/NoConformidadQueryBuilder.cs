﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.CCalidad.ValueObjects;

namespace Jaeger.Domain.CCalidad.Builder {
    public class NoConformidadQueryBuilder : ConditionalBuilder, IConditionalBuilder, INoConformidadQueryBuilder, INoConformidadYearQueryBuilder, INoConformidadMonthQueryBuilder, INoConformidadStatusQueryBuilder {
        public NoConformidadQueryBuilder() : base() { }

        public INoConformidadYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("NOCF_ANIO", year.ToString()));
            return this;
        }

        public INoConformidadMonthQueryBuilder Month(int month = 0) {
            if (month > 0) {
                this._Conditionals.Add(new Conditional("NOCF_MES", month.ToString()));
            }
            return this;
        }

        public INoConformidadStatusQueryBuilder Status(NoConformidadStatusEnum status) {
            var indice = (int)status;
            this._Conditionals.Add(new Conditional("NOCF_CTST_ID", indice.ToString()));
            return this;
        }
    }
}
