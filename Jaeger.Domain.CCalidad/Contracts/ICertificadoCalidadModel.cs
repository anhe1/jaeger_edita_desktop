﻿using System;

namespace Jaeger.Domain.CCalidad.Contracts {
    public interface ICertificadoCalidadModel {
        DateTime Fecha { get; set; }
        string Cliente { get; set; }
        int IdPedido { get; set; }
        string Descripcion { get; set; }
        string Leyenda1 { get; set; }
        string Leyenda2 { get; set; }
        string Nota { get; set; }
    }
}
