﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.CCalidad.Contracts {
    public interface ISqlOrdenProduccionRepository : IGenericRepository<OrdenProduccionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        OrdenProduccionDetailModel Save(OrdenProduccionDetailModel model);
    }
}
