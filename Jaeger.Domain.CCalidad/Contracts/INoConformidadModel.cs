﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Domain.CCalidad.Contracts {
    public interface INoConformidadModel {
        int IdNoConformidad { get; set; }

        bool Activo { get; set; }

        int IdStatus {  get; set; }
    }
}
