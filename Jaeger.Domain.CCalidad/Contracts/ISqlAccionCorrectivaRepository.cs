﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.CCalidad.Contracts {
    public interface ISqlAccionCorrectivaRepository : IGenericRepository<AccionCorrectivaModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        AccionCorrectivaDetailModel Save(AccionCorrectivaDetailModel model);
    }
}
