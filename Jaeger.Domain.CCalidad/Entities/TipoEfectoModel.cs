﻿namespace Jaeger.Domain.CCalidad.Entities {
    public class TipoEfectoModel : Base.Abstractions.BaseSingleModel {
        public TipoEfectoModel() : base() { }

        public TipoEfectoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
