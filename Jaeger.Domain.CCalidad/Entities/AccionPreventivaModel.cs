using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class AccionPreventivaModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region
        private int _NOCFAP_ID;
        private bool _NOCFAP_A;
        private int _NOCFAP_NOCF_ID;
        private System.DateTime? _NOCFAP_FEC;
        private string _NOCFAP_DESC;
        private string _Afectacion;
        private string _Responsable;
        #endregion

        public AccionPreventivaModel() {
            this.Fecha = System.DateTime.Now;
            this.Activo = true;
        }

        /// <summary>
        /// (NOCFAP_ID)
        /// </summary>
        [DataNames("NOCFAP_ID")]
        public int Id {
            get { return _NOCFAP_ID; }
            set {
                _NOCFAP_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFAP_A")]
        public bool Activo {
            get { return this._NOCFAP_A; }
            set {
                this._NOCFAP_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAP_NOCF_ID)
        /// </summary>
        [DataNames("NOCFAP_NOCF_ID")]
        public int IdNoConformidad {
            get { return _NOCFAP_NOCF_ID; }
            set {
                _NOCFAP_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAP_FEC)
        /// </summary>
        [DataNames("NOCFAP_FEC")]
        public System.DateTime? Fecha {
            get { return _NOCFAP_FEC; }
            set {
                _NOCFAP_FEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la accion preventiva (NOCFAP_DESC)
        /// </summary>
        [DataNames("NOCFAP_DESC")]
        public string Descripcion {
            get { return _NOCFAP_DESC; }
            set {
                _NOCFAP_DESC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFAP_AFEC")]
        public string Afectacion {
            get { return this._Afectacion; }
            set { this._Afectacion = value; }
        }

        [DataNames("NOCFAP_RESP")]
        public string Responsable {
            get { return _Responsable; }
            set { this._Responsable = value; }
        }
    }
}
