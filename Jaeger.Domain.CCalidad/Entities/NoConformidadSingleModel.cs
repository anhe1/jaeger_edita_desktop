﻿using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CCalidad.Contracts;

namespace Jaeger.Domain.CCalidad.Entities {
    public class NoConformidadSingleModel : NoConformidadDetailModel, INoConformidadModel {
        public NoConformidadSingleModel() : base() { }

        public void SetValues(NoConformidadDetailModel source) {
            MapperClassExtensions.MatchAndMap<NoConformidadDetailModel, NoConformidadSingleModel>(source, this);
        }
    }
}
