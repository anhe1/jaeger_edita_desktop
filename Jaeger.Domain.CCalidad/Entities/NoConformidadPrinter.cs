﻿using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class NoConformidadPrinter : NoConformidadDetailModel, INoConformidadModel {
        public NoConformidadPrinter() : base() { }

        public NoConformidadPrinter(NoConformidadDetailModel source) : base() {
            MapperClassExtensions.MatchAndMap<NoConformidadDetailModel, NoConformidadPrinter>(source, this);
        }

        [DataNames("EDP1_ARE")]
        public string Area { get; set; }

        [DataNames("EDP1_NOM")]
        public string Departamento { get; set; }

        /// <summary>
        /// informacion para QR
        /// </summary>
        public string[] QrText {
            get {
                return new string[] { "||Folio=", this.IdNoConformidad.ToString("#000000"), "|Fecha=", this.Fecha.ToString("dd-mm-yyyy"), "|Cliente=", "", "|Vendedor=", "", "|Articulos=", "||" };
            }
        }
    }
}
