using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// Orden de Produccion
    /// </summary>
    public class OrdenProduccionModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region
        private int _NOCFO_ID;
        private bool _NOCFO_A;
        private int _NOCFO_NOCF_ID;
        private int _NOCFO_PEDPRD_ID;
        private string _Receptor;
        private string _Descripcion;
        private System.DateTime _NOCFO_FN;
        #endregion

        public OrdenProduccionModel() {
            this.Activo = true;
            this.FechaNuevo = System.DateTime.Now;
        }

        /// <summary>
        /// (NOCFO_ID)
        /// </summary>
        [DataNames("NOCFO_ID")]
        public int Id {
            get { return _NOCFO_ID; }
            set {
                _NOCFO_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFO_A")]
        public bool Activo {
            get { return _NOCFO_A; }
            set {
                _NOCFO_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFO_NOCF_ID)
        /// </summary>
        [DataNames("NOCFO_NOCF_ID")]
        public int IdNoConformidad {
            get { return _NOCFO_NOCF_ID; }
            set {
                _NOCFO_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFO_PEDPRD_ID)
        /// </summary>
        [DataNames("NOCFO_PEDPRD_ID")]
        public int IdOrden {
            get { return _NOCFO_PEDPRD_ID; }
            set {
                _NOCFO_PEDPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFO_NOM")]
        public string Cliente {
            get { return this._Receptor; }
            set { this._Receptor =  value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFO_DESC")]
        public string Descripcion {
            get { return _Descripcion; }
            set { this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFO_FN)
        /// </summary>
        [DataNames("NOCFO_FN")]
        public System.DateTime FechaNuevo {
            get { return _NOCFO_FN; }
            set {
                _NOCFO_FN = value;
                this.OnPropertyChanged();
            }
        }
    }
}
