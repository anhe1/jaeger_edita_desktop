﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class NoConformidadStatusModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int NOCFS_NOCF_ID;
        private int NOCFS_CTSTA_ID;
        private int NOCFS_CTSTB_ID;
        private int NOCFS_NOCFR_ID;
        private string NOCFS_CVMTV;
        private string NOCFS_NOTA;
        private string NOCFS_USU_N;
        private System.DateTime NOCFS_FN;
        #endregion

        public NoConformidadStatusModel() : base() {
            this.NOCFS_FN = System.DateTime.Now;
        }

        /// <summary>
        /// indice de la no conformidad
        /// </summary>
        [DataNames("NOCFS_NOCF_ID")]
        public int IdNoConformidad {
            get { return NOCFS_NOCF_ID; }
            set {
                NOCFS_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status
        /// </summary>
        [DataNames("NOCFS_CTSTA_ID")]
        public int IdStatusA {
            get { return NOCFS_CTSTA_ID; }
            set {
                NOCFS_CTSTA_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status
        /// </summary>
        [DataNames("NOCFS_CTSTB_ID")]
        public int IdStatusB {
            get { return NOCFS_CTSTB_ID; }
            set {
                this.NOCFS_CTSTB_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de no conformidad relacionada
        /// </summary>
        [DataNames("NOCFS_NOCFR_ID")]
        public int IdNoConformidadR {
            get { return NOCFS_NOCFR_ID; }
            set {
                NOCFS_NOCFR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de motivo
        /// </summary>
        [DataNames("NOCFS_CVMTV")]
        public string CvMotivo {
            get { return NOCFS_CVMTV; }
            set {
                NOCFS_CVMTV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("NOCFS_NOTA")]
        public string Nota {
            get { return NOCFS_NOTA; }
            set {
                this.NOCFS_NOTA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer usuario que autoriza
        /// </summary>
        [DataNames("NOCFS_USU_N")]
        public string Usuario {
            get { return NOCFS_USU_N; }
            set {
                NOCFS_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("NOCFS_FN")]
        public System.DateTime FechaNuevo {
            get { return NOCFS_FN; }
            set {
                NOCFS_FN = value;
                this.OnPropertyChanged();
            }
        }
    }
}
