﻿namespace Jaeger.Domain.CCalidad.Entities {
    public class MotivoCancelacionModel : Base.Abstractions.BaseSingleModel {
        public MotivoCancelacionModel() : base() { }

        public MotivoCancelacionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
