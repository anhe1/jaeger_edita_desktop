﻿using Jaeger.Domain.Services.Mapping;
using System;

namespace Jaeger.Domain.CCalidad.Entities {
    public class NoConformidadPeriodoModel {
        [DataNames("Ejercicio")]
        public int Ejercicio {  get; set; }
        [DataNames("Periodo")]
        public int Periodo {  get; set; }
        [DataNames("Total")]
        public int Total {  get; set; }
        [DataNames("Resuelto")]
        public int Resuelto {  get; set; }
        public int Pendiente {
            get {
                    return this.Total - this.Resuelto;
            }
        }
        [DataNames("Costo")]
        public decimal Costo { get; set; }
        public string Mes {
            get {
                var fecha = new DateTime(this.Ejercicio, this.Periodo, 1);
                return fecha.ToString("MMMM");
            }
        }
    }
}
