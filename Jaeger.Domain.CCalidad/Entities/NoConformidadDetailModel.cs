﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.Domain.CCalidad.ValueObjects;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// modelo no conformidad
    /// </summary>
    public class NoConformidadDetailModel : NoConformidadModel, INoConformidadModel {
        #region declaracione
        private BindingList<CausaRaizDetailModel> _CausaRaiz;
        private BindingList<AccionCorrectivaDetailModel> _AccionesCorrectivas;
        private BindingList<AccionPreventivaDetailModel> _AccionesPreventivas;
        private BindingList<OrdenProduccionDetailModel> _OrdenProduccion;
        private BindingList<CostoNoCalidadDetailModel> _CostosNoCalidad;
        private BindingList<MediaModel> _Media;
        #endregion

        /// <summary>
        /// cosntructor
        /// </summary>
        public NoConformidadDetailModel() : base() {
            this._CausaRaiz = new BindingList<CausaRaizDetailModel>();
            this._AccionesCorrectivas = new BindingList<AccionCorrectivaDetailModel>();
            this._AccionesPreventivas = new BindingList<AccionPreventivaDetailModel>();
            this._OrdenProduccion = new BindingList<OrdenProduccionDetailModel>();
            this._CostosNoCalidad = new BindingList<CostoNoCalidadDetailModel>() { RaiseListChangedEvents = true };
            this._CostosNoCalidad.ListChanged += new ListChangedEventHandler(this.CostosNoCalidad_ListChanged);
            this._Media = new BindingList<MediaModel>();
        }

        #region propiedades
        public BindingList<CausaRaizDetailModel> CausaRaiz {
            get { return _CausaRaiz; }
            set { _CausaRaiz = value; }
        }

        public BindingList<AccionCorrectivaDetailModel> AccionesCorrectivas {
            get { return _AccionesCorrectivas; }
            set { this._AccionesCorrectivas = value; }
        }

        public BindingList<AccionPreventivaDetailModel> AccionesPreventivas {
            get { return _AccionesPreventivas; }
            set { _AccionesPreventivas = value; }
        }

        public BindingList<OrdenProduccionDetailModel> OrdenProduccion {
            get { return this._OrdenProduccion; }
            set { this._OrdenProduccion = value; }
        }

        public BindingList<CostoNoCalidadDetailModel> CostosNoCalidad {
            get { return _CostosNoCalidad; }
            set {
                if (this._CostosNoCalidad != null) {
                    this._CostosNoCalidad.ListChanged -= new ListChangedEventHandler(this.CostosNoCalidad_ListChanged);
                }
                _CostosNoCalidad = value;
                if (this._CostosNoCalidad != null) {
                    this._CostosNoCalidad.ListChanged += new ListChangedEventHandler(this.CostosNoCalidad_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        public BindingList<MediaModel> Media {
            get { return _Media; }
            set {
                this._Media = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status
        /// </summary>
        public NoConformidadStatusEnum Status {
            get { return (NoConformidadStatusEnum)this.IdStatus; }
            set { this.IdStatus = (int)value; }
        }

        public bool IsEditable {
            get { return (this.IdStatus == 1); }
        }
        #endregion

        #region metodos
        private void CostosNoCalidad_ListChanged(object sender, ListChangedEventArgs e) {
            var maquinaria = this.CostosNoCalidad.Where((CostoNoCalidadDetailModel p) => p.Activo == true).Sum((CostoNoCalidadDetailModel p) => p.Maquinaria);
            var materiaPrima = this.CostosNoCalidad.Where((CostoNoCalidadDetailModel p) => p.Activo == true).Sum((CostoNoCalidadDetailModel p) => p.MateriaPrima);
            var consumibles = this.CostosNoCalidad.Where((CostoNoCalidadDetailModel p) => p.Activo == true).Sum((CostoNoCalidadDetailModel p) => p.Consumibles);
            var manoObra = this.CostosNoCalidad.Where((CostoNoCalidadDetailModel p) => p.Activo == true).Sum((CostoNoCalidadDetailModel p) => p.ManoObra);
            this.Costo = manoObra + maquinaria + materiaPrima + consumibles;
        }
        #endregion

        /// <summary>
        /// obtener modelo de status
        /// </summary>
        public NoConformidadStatusModel GetStatusModel() {
            return new NoConformidadStatusModel {
                FechaNuevo = System.DateTime.Now,
                IdNoConformidad = this.IdNoConformidad,
                IdStatusB = this.IdStatus,
            };
        }
    }
}
