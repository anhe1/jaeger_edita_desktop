using System;
using System.Net;
using System.IO;
using System.Drawing;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class MediaModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region
        private int _NOCFM_ID;
        private bool _NOCFM_A;
        private int? _NOCFM_NOCF_ID;
        private string _NOCFM_URL;
        private string _NOCFM_DESC;
        private DateTime _NOCFM_FN;
        private Image imagen = null;
        #endregion

        public MediaModel() {
            this._NOCFM_FN = DateTime.Now;
            this.Activo = true;
        }

        #region propiedades
        /// <summary>
        /// (NOCFM_ID)
        /// </summary>
        [DataNames("NOCFM_ID")]
        public int Id {
            get { return _NOCFM_ID; }
            set {
                _NOCFM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (NOCFM_A)
        /// </summary>
        [DataNames("NOCFM_A")]
        public bool Activo {
            get { return _NOCFM_A; }
            set {
                _NOCFM_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFM_NOCF_ID)
        /// </summary>
        [DataNames("NOCFM_NOCF_ID")]
        public int? IdNoConformidad {
            get { return _NOCFM_NOCF_ID; }
            set {
                _NOCFM_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFM_URL)
        /// </summary>
        [DataNames("NOCFM_URL")]
        public string URL {
            get { return _NOCFM_URL; }
            set {
                _NOCFM_URL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFM_DESC)
        /// </summary>
        [DataNames("NOCFM_DESC")]
        public string Descripcion {
            get { return _NOCFM_DESC; }
            set {
                _NOCFM_DESC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro (NOCFM_FN)
        /// </summary>
        [DataNames("NOCFM_FN")]
        public DateTime FechaNuevo {
            get { return _NOCFM_FN; }
            set {
                _NOCFM_FN = value;
                this.OnPropertyChanged();
            }
        }

        public Image Imagen {
            get {
                if (this.imagen == null) {
                    try {
                        WebClient wc = new WebClient();
                        byte[] bytes = wc.DownloadData(this.URL);
                        MemoryStream ms = new MemoryStream(bytes);
                        this.imagen = Image.FromStream(ms);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                        return new Bitmap(10, 10);
                    }
                }
                return this.imagen;
            }
        }
        public string FileName { get; set; }
        public object Tag { get; set; }
        #endregion
    }
}
