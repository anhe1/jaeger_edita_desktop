﻿using Jaeger.Domain.CCalidad.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class CausaRaizDetailModel : CausaRaizModel {
        public CausaRaizDetailModel() : base() {
            this.IdClasificacion5 = 0;
            this.IdEfecto = 0;
        }

        [DataNames("EDP1_NOM")]
        public string Departamento { get; set; }

        public Clasificacion5Enum Clasificacion {
            get { return (Clasificacion5Enum)this.IdClasificacion5; }
            set { this.IdClasificacion5 = (int)value; }
        }

        public TipoEfectoEnum Efecto {
            get { return (TipoEfectoEnum)this.IdEfecto; }
            set { this.IdEfecto = (int)value; }
        }

        public string ClasificacionText {
            get { return this.Clasificacion.ToString(); }
        }

        public string EfectoText {
            get { return this.Efecto.ToString(); }
        }
    }
}
