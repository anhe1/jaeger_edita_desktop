﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// clase area y departamento EDP1
    /// </summary>
    public class DepartamentoModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int edp1id;
        private int edp1a;
        private string edp1nom;
        private string edp1are;
        private int edp1sec;
        private int edp1usufm;
        private DateTime? fechaModifica;
        #endregion

        public DepartamentoModel() { }

        /// <sumary>
        /// obtener o establecer (EDP1_ID)
        /// </sumary>
        [DataNames("EDP1_ID")]
        public int IdDepartamento {
            get { return edp1id; }
            set {
                edp1id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_A)
        /// </sumary>
        [DataNames("EDP1_A")]
        public int Activo {
            get { return edp1a; }
            set {
                edp1a = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer nombre o descripcion del departamento (EDP1_NOM)
        /// </sumary>
        [DataNames("EDP1_NOM")]
        public string Nombre {
            get { return edp1nom; }
            set {
                edp1nom = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_ARE)
        /// </sumary>
        [DataNames("EDP1_ARE")]
        public string Area {
            get { return edp1are; }
            set {
                edp1are = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_SEC)
        /// </sumary>
        [DataNames("EDP1_SEC")]
        public int Secuencia {
            get { return edp1sec; }
            set {
                edp1sec = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_USU_FM)
        /// </sumary>
        [DataNames("EDP1_USU_FM")]
        public int Modifica {
            get { return edp1usufm; }
            set {
                edp1usufm = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_FM)
        /// </sumary>
        [DataNames("EDP1_FM")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica > firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
            }
        }

        public string AreaDepartamento {
            get { return string.Concat(Area, "/", Nombre); }
        }

        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdDepartamento.ToString("00"), this.Nombre); }
        }
    }
}
