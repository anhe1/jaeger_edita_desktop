using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// 
    /// </summary>
    public class AccionCorrectivaModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region
        private int _NOCFAC_ID;
        private bool _NOCFAC_A;
        private int _NOCFAC_NOCF_ID;
        private string _NOCFAC_DESC;
        private string _NOCFAC_RESP;
        private int _NOCFAC_CTDEP_ID;
        private int _NOCFAC_CTTIP_ID;
        private System.DateTime _NOCFAC_FN;
        private System.DateTime _NOCFAC_FEC;
        #endregion

        public AccionCorrectivaModel() { 
            this.Fecha = System.DateTime.Now;
            this.FechaNuevo = System.DateTime.Now;
            this.Activo = true;
        }

        /// <summary>
        /// (NOCFAC_ID)
        /// </summary>
        [DataNames("NOCFAC_ID")]
        public int Id {
            get { return _NOCFAC_ID; }
            set {
                _NOCFAC_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFAC_A")]
        public bool Activo {
            get { return _NOCFAC_A; }
            set {
                _NOCFAC_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAC_NOCF_ID)
        /// </summary>
        [DataNames("NOCFAC_NOCF_ID")]
        public int IdNoConformidad {
            get { return _NOCFAC_NOCF_ID; }
            set {
                _NOCFAC_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAC_DESC)
        /// </summary>
        [DataNames("NOCFAC_DESC")]
        public string Descripcion {
            get { return _NOCFAC_DESC; }
            set {
                _NOCFAC_DESC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAC_RESP)
        /// </summary>
        [DataNames("NOCFAC_RESP")]
        public string Responsable {
            get { return _NOCFAC_RESP; }
            set {
                _NOCFAC_RESP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAC_CTDEP_ID)
        /// </summary>
        [DataNames("NOCFAC_CTDEP_ID")]
        public int IdDepartamento {
            get { return _NOCFAC_CTDEP_ID; }
            set {
                _NOCFAC_CTDEP_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de accion (NOCFAC_CTTIP_ID)
        /// </summary>
        [DataNames("NOCFAC_CTTIP_ID")]
        public int IdTipoAccion {
            get { return _NOCFAC_CTTIP_ID; }
            set {
                _NOCFAC_CTTIP_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de compromiso (NOCFAC_FEC)
        /// </summary>
        [DataNames("NOCFAC_FEC")]
        public System.DateTime Fecha {
            get { return _NOCFAC_FEC; }
            set {
                _NOCFAC_FEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFAC_FN)
        /// </summary>
        [DataNames("NOCFAC_FN")]
        public System.DateTime FechaNuevo {
            get { return _NOCFAC_FN; }
            set {
                _NOCFAC_FN = value;
                this.OnPropertyChanged();
            }
        }
    }
}
