using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class CostoNoCalidadModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region
        private int _NOCFC_ID;
        private bool _NOCFC_A;
        private int _NOCFC_NOCF_ID;
        private int _NOCFC_PEDPRD_ID;
        private string _Receptor;
        private string _Descripcion;
        private decimal _NOCFC_MAQ;
        private decimal _NOCFC_MO;
        private decimal _NOCFC_MA;
        private decimal _NOCFC_MP;
        private decimal _NOCFC_CONS;
        private System.DateTime _NOCFC_FN;
        #endregion

        public CostoNoCalidadModel() {
            this.Activo = true;
            this.FechaNuevo = System.DateTime.Now;
        }

        /// <summary>
        /// (NOCFC_ID)
        /// </summary>
        [DataNames("NOCFC_ID")]
        public int Id {
            get { return _NOCFC_ID; }
            set {
                _NOCFC_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFC_A")]
        public bool Activo {
            get { return _NOCFC_A; }
            set {
                _NOCFC_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_NOCF_ID)
        /// </summary>
        [DataNames("NOCFC_NOCF_ID")]
        public int IdNoConformidad {
            get { return _NOCFC_NOCF_ID; }
            set {
                _NOCFC_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_PEDPRD_ID)
        /// </summary>
        [DataNames("NOCFC_PEDPRD_ID")]
        public int IdOrden {
            get { return _NOCFC_PEDPRD_ID; }
            set {
                _NOCFC_PEDPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFC_NOM")]
        public string Cliente {
            get { return this._Receptor; }
            set {
                this._Receptor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFC_DESC")]
        public string Descripcion {
            get { return this._Descripcion; }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_MAQ)
        /// </summary>
        [DataNames("NOCFC_MAQ")]
        public decimal Maquinaria {
            get { return _NOCFC_MAQ; }
            set {
                _NOCFC_MAQ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_MO)
        /// </summary>
        [DataNames("NOCFC_MO")]
        public decimal ManoObra {
            get { return _NOCFC_MO; }
            set {
                _NOCFC_MO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_MA)
        /// </summary>
        [DataNames("NOCFC_MA")]
        public decimal MedioAmbiente {
            get { return _NOCFC_MA; }
            set {
                _NOCFC_MA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_MP)
        /// </summary>
        [DataNames("NOCFC_MP")]
        public decimal MateriaPrima {
            get { return _NOCFC_MP; }
            set {
                _NOCFC_MP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_CONS)
        /// </summary>
        [DataNames("NOCFC_CONS")]
        public decimal Consumibles {
            get { return _NOCFC_CONS; }
            set {
                _NOCFC_CONS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFC_FN)
        /// </summary>
        [DataNames("NOCFC_FN")]
        public System.DateTime FechaNuevo {
            get { return _NOCFC_FN; }
            set {
                _NOCFC_FN = value;
                this.OnPropertyChanged();
            }
        }
    }
}
