﻿namespace Jaeger.Domain.CCalidad.Entities {
    public class TipoAccionModel : Base.Abstractions.BaseSingleModel {
        public TipoAccionModel() : base() { }

        public TipoAccionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
