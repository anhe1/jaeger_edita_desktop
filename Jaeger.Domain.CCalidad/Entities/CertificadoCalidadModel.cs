﻿using System;
using Jaeger.Domain.CCalidad.Contracts;

namespace Jaeger.Domain.CCalidad.Entities {
    public class CertificadoCalidadModel : Base.Abstractions.BasePropertyChangeImplementation, ICertificadoCalidadModel {
        private DateTime _Fecha;
        private string _Cliente;
        private int _OrdenProduccion;
        private string _Descripcion;
        private string _Leyenda1;
        private string _Leyenda2;
        private string _Nota;

        public CertificadoCalidadModel() {
            this.Fecha = DateTime.Now;
            this.Leyenda1 = @"EL PRESENTE CERTIFICADO GARANTIZA QUE EL PRODUCTO LIBERADO CUMPLE CON LAS ESPECIFICACIONES DE CALIDAD DEL CLIENTE E INTERNAS, A FIN DE LOGRAR LA PLENA SATISFACCIÓN DEL CLIENTE Y CUMPLIENDO CON LOS OBJETIVOS DE LA EMPRESA EN SUS PRODUCTOS: CALIDAD, CANTIDAD Y CALENDARIO (ENTREGA OPORTUNA).";
            this.Leyenda2 = @"SOLO SE APLICARÁ EN CASO DE QUE EL PRODUCTO QUE NO CUMPLA CON LOS CRITERIOS DE CALIDAD, SIEMPRE Y CUANDO SEAN MENORES Y NO PONGAN EN RIESGO LA SATISFACCIÓN DEL CLIENTE. SOLO PODRÁ SER AUTORIZADO POR LA DIRECCIÓN Y RETIFICADO POR EL CLIENTE.";
        }

        public DateTime Fecha {
            get { return this._Fecha; }
            set {
                this._Fecha = value;
                this.OnPropertyChanged();
            }
        }

        public string Cliente {
            get { return this._Cliente; }
            set {
                this._Cliente = value;
                this.OnPropertyChanged();
            }
        }

        public int IdPedido {
            get { return this._OrdenProduccion; }
            set {
                this._OrdenProduccion = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion {
            get { return this._Descripcion; }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        public string Leyenda1 {
            get { return this._Leyenda1; }
            set {
                this._Leyenda1 = value;
                this.OnPropertyChanged();
            }
        }

        public string Leyenda2 {
            get { return this._Leyenda2; }
            set {
                this._Leyenda2 = value;
                this.OnPropertyChanged();
            }
        }

        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }
    }
}
