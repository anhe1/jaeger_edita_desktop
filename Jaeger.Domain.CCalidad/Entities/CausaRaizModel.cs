using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// modelo para el analisis causa raiz
    /// </summary>
    public class CausaRaizModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region
        private int _NOCFCR_ID;
        private bool _NOCFCR_A;
        private int _NOCFCR_NOCF_ID;
        private int _NOCFCR_CLS5M_ID;
        private int _NOCFCR_CTEFE_ID;
        private int NOCF_CTDPTO_ID;
        private string _NOCFCR_DESC;
        private string _NOCFCR_RESP;
        private int _NOCFCR_CTUSR_ID;
        private System.DateTime _NOCFCR_FN;
        private string _NOCFCR_USR_N;
        private bool _IsResponsable;
        #endregion

        public CausaRaizModel() {
            this._NOCFCR_A = true;
            this._NOCFCR_FN = System.DateTime.Now;
            this._NOCFCR_CTUSR_ID = -1;
            this._IsResponsable = false;
        }

        /// <summary>
        /// (NOCFCR_ID)
        /// </summary>
        [DataNames("NOCFCR_ID")]
        public int Id {
            get { return _NOCFCR_ID; }
            set {
                _NOCFCR_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFCR_A")]
        public bool Activo {
            get { return _NOCFCR_A; }
            set {
                this._NOCFCR_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_NOCF_ID)
        /// </summary>
        [DataNames("NOCFCR_NOCF_ID")]
        public int IdNoConformidad {
            get { return _NOCFCR_NOCF_ID; }
            set {
                _NOCFCR_NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_CLS5M_ID)
        /// </summary>
        [DataNames("NOCFCR_CLS5M_ID")]
        public int IdClasificacion5 {
            get { return _NOCFCR_CLS5M_ID; }
            set {
                _NOCFCR_CLS5M_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_CTEFE_ID)
        /// </summary>
        [DataNames("NOCFCR_CTEFE_ID")]
        public int IdEfecto {
            get { return _NOCFCR_CTEFE_ID; }
            set {
                _NOCFCR_CTEFE_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_DESC)
        /// </summary>
        [DataNames("NOCFCR_DESC")]
        public string Descripcion {
            get { return _NOCFCR_DESC; }
            set {
                _NOCFCR_DESC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFCR_CTDEP_ID")]
        public int IdDepartamento {
            get { return this.NOCF_CTDPTO_ID; }
            set {
                this.NOCF_CTDPTO_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NOCFCR_CTUSR_ID")]
        public int IdResponsable {
            get { return this._NOCFCR_CTUSR_ID; }
            set {
                this._NOCFCR_CTUSR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_RESP)
        /// </summary>
        [DataNames("NOCFCR_RESP")]
        public string Responsable {
            get { return _NOCFCR_RESP; }
            set {
                _NOCFCR_RESP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer casilla de verificacion del resposable de la no conformidad
        /// </summary>
        [DataNames("NOCFCR_RESP_C")]
        public bool IsResponsable {
            get { return this._IsResponsable; }
            set { this._IsResponsable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_FN)
        /// </summary>
        [DataNames("NOCFCR_FN")]
        public System.DateTime FechaNuevo {
            get { return _NOCFCR_FN; }
            set {
                _NOCFCR_FN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCFCR_USR_N)
        /// </summary>
        [DataNames("NOCFCR_USR_N")]
        public string Creo {
            get { return _NOCFCR_USR_N; }
            set {
                _NOCFCR_USR_N = value;
                this.OnPropertyChanged();
            }
        }
    }
}
