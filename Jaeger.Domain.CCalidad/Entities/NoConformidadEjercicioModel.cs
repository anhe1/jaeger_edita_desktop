﻿using Jaeger.Domain.Services.Mapping;
using System.Diagnostics;

namespace Jaeger.Domain.CCalidad.Entities {
    public class NoConformidadEjercicioModel {
        [DataNames("NOCFA_EJERCICIO")]
        public int Ejercicio { get; set; }
        [DataNames("NOCFA_ENE")]
        public int Enero { get; set; }
        [DataNames("NOCFA_FEB")]
        public int Febrero { get; set; }
        [DataNames("NOCFA_MAR")]
        public int Marzo { get; set; }
        [DataNames("NOCFA_ABR")]
        public int Abril { get; set; }
        [DataNames("NOCFA_MAY")]
        public int Mayo { get; set; }
        [DataNames("NOCFA_JUN")]
        public int Junio { get; set; }
        [DataNames("NOCFA_JUL")]
        public int Julio { get; set; }
        [DataNames("NOCFA_AGO")]
        public int Agosto { get; set; }
        [DataNames("NOCFA_SEP")]
        public int Septiembre { get; set; }
        [DataNames("NOCFA_OCT")]
        public int Octubre { get; set; }
        [DataNames("NOCFA_NOV")]
        public int Noviembre { get; set; }
        [DataNames("NOCFA_DIC")]
        public int Diciembre { get; set; }
    }
}
