﻿namespace Jaeger.Domain.CCalidad.Entities {
    public class Clasificacion5Model : Base.Abstractions.BaseSingleModel {
        public Clasificacion5Model() : base() { }

        public Clasificacion5Model(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
