﻿using System;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// modelo de no conformidad
    /// </summary>
    public class NoConformidadModel : Base.Abstractions.BasePropertyChangeImplementation, INoConformidadModel {
        #region
        private int _NOCF_ID;
        private bool _NOCF_A;
        private int _NOCF_CTST_ID;
        private int _NOCF_CTTIP_ID;
        private int _NOCF_AUT_ID;
        private int _NOCF_CTUSR_ID;
        private int _NOCF_CTDPTO_ID;
        private string _NOCF_DESC;
        private decimal _NOCF_CANT;
        private decimal _NOCF_COSTO;
        private string _NOCF_NOM;
        private int _NOCF_DRCTR_ID;
        private string _NOCF_NOTA;
        private DateTime _NOCF_FN;
        private string _NOCF_USU_N;
        private DateTime? _NOCF_FM;
        private string _NOCF_USU_M;
        #endregion

        public NoConformidadModel() {
            this._NOCF_FN = DateTime.Now;
            this._NOCF_CTST_ID = 1;
            this.Activo = true;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer numero consecutivo de la no conformidad (NOCF_ID)
        /// </summary>
        [DataNames("NOCF_ID")]
        public int IdNoConformidad {
            get { return _NOCF_ID; }
            set {
                _NOCF_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_A)
        /// </summary>
        [DataNames("NOCF_A")]
        public bool Activo {
            get { return _NOCF_A; }
            set {
                _NOCF_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_CTST_ID)
        /// </summary>
        [DataNames("NOCF_CTST_ID")]
        public int IdStatus {
            get { return _NOCF_CTST_ID; }
            set {
                _NOCF_CTST_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_CTTIP_ID)
        /// </summary>
        [DataNames("NOCF_CTTIP_ID")]
        public int NOCF_CTTIP_ID {
            get { return _NOCF_CTTIP_ID; }
            set {
                _NOCF_CTTIP_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_AUT_ID)
        /// </summary>
        [DataNames("NOCF_AUT_ID")]
        public int IdAutoriza {
            get { return _NOCF_AUT_ID; }
            set {
                _NOCF_AUT_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_CTUSR_ID)
        /// </summary>
        [DataNames("NOCF_CTUSR_ID")]
        public int IdUsuario {
            get { return _NOCF_CTUSR_ID; }
            set {
                _NOCF_CTUSR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_CTDPTO_ID)
        /// </summary>
        [DataNames("NOCF_CTDPTO_ID")]
        public int IdDepartamento {
            get { return _NOCF_CTDPTO_ID; }
            set {
                _NOCF_CTDPTO_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_DESC)
        /// </summary>
        [DataNames("NOCF_DESC")]
        public string Descripcion {
            get { return _NOCF_DESC; }
            set {
                _NOCF_DESC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_CANT)
        /// </summary>
        [DataNames("NOCF_CANT")]
        public decimal Cantidad {
            get { return _NOCF_CANT; }
            set {
                _NOCF_CANT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_COSTO)
        /// </summary>
        [DataNames("NOCF_COSTO")]
        public decimal Costo {
            get { return _NOCF_COSTO; }
            set {
                _NOCF_COSTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_NOM)
        /// </summary>
        [DataNames("NOCF_NOM")]
        public string Detector {
            get { return _NOCF_NOM; }
            set {
                _NOCF_NOM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_DRCTR_ID)
        /// </summary>
        [DataNames("NOCF_DRCTR_ID")]
        public int IdDirectorio {
            get { return _NOCF_DRCTR_ID; }
            set {
                _NOCF_DRCTR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_NOTA)
        /// </summary>
        [DataNames("NOCF_NOTA")]
        public string Nota {
            get { return _NOCF_NOTA; }
            set {
                _NOCF_NOTA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_FN)
        /// </summary>
        public DateTime Fecha {
            get { return this.FechaNuevo; }
        }

        /// <summary>
        /// (NOCF_USU_N)
        /// </summary>
        [DataNames("NOCF_USU_N")]
        public string Creo {
            get { return _NOCF_USU_N; }
            set {
                _NOCF_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("NOCF_FN")]
        public DateTime FechaNuevo {
            get { return _NOCF_FN; }
            set {
                _NOCF_FN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_FM)
        /// </summary>
        [DataNames("NOCF_FM")]
        public DateTime? FechaModifica {
            get { return _NOCF_FM; }
            set {
                _NOCF_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// (NOCF_USU_M)
        /// </summary>
        [DataNames("NOCF_USU_M")]
        public string Modifica {
            get { return _NOCF_USU_M; }
            set {
                _NOCF_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el periodo o mes
        /// </summary>
        [DataNames("NOCF_MES")]
        public int Periodo { get; set; }

        /// <summary>
        /// obtener o establecer el ejercicio
        /// </summary>
        [DataNames("NOCF_ANIO")]
        public int Ejercicio { get; set; }

        public object Tag { get; set; }
        #endregion
    }
}
