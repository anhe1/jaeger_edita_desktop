﻿using Jaeger.Domain.CCalidad.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class AccionCorrectivaDetailModel : AccionCorrectivaModel {
        public AccionCorrectivaDetailModel() : base() { }

        [DataNames("EDP1_NOM")]
        public string Departamento { get; set; }

        public TipoAccionEnum TipoAccion {
            get { return (TipoAccionEnum)this.IdTipoAccion; }
            set { this.IdTipoAccion = (int)value; }
        }

        public string TipoAccionText {
            get { return this.TipoAccion.ToString(); }
        }
    }
}
