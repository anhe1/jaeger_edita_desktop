﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CCalidad.Entities {
    public class EmpleadoModel {
        [DataNames("DIR_ID")]
        public int Id { get; set; }

        [DataNames("DIR_CLA")]
        public string Clave { get; set; }
        
        [DataNames("DIR_NOM")]
        public string Nombre { get; set; }
    }
}
