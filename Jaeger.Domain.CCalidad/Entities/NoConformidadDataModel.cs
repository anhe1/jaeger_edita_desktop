﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Jaeger.Domain.CCalidad.Entities {
    /// <summary>
    /// clase para reporte anual de no conformidades
    /// </summary>
    public class NoConformidadDataModel {
        #region declaraciones
        private List<NoConformidadDetailModel> dataSource;
        private List<NoConformidadByDepartamento> byDepartamentos;
        private List<DepartamentoModel> departamentos;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="noConformidades">lista de no conformidades</param>
        /// <param name="departamentos">lista de departmanetos</param>
        public NoConformidadDataModel(List<NoConformidadDetailModel> noConformidades, List<DepartamentoModel> departamentos) {
            this.dataSource = noConformidades;
            this.departamentos = departamentos;
            this.GetDataCompleted();
        }

        public void GetDataCompleted() {
            this.Departamentos = this.InitializeDepartamentosData();
            this.Total = this.Totales();
            this.EnProceso = this.InitialEnProceso();
            this.Autorizadas = this.InitialAutorizadas();
            this.Pendientes = this.InitialPendientes();
            this.Canceladas = this.InitialCancelado();
            this.Costos = this.InitialCostos();
        }

        #region propiedades
        public List<NoConformidadByMonth> Total { get; set; }

        public List<NoConformidadByMonth> EnProceso { get; set; }

        public List<NoConformidadByMonth> Autorizadas { get; set; }

        public List<NoConformidadByMonth> Pendientes { get; set; }

        public List<NoConformidadByMonth> Canceladas { get; set; }

        public List<NoConformidadByMonthCostoRange> Costos { get; set; }

        public List<NoConformidadByDepartamento> Departamentos {
            get { return this.byDepartamentos; }
            set { this.byDepartamentos = value; }
        }
        #endregion

        #region metodos
        private List<NoConformidadByMonth> Totales() {
            List<NoConformidadByMonth> byMonths = new List<NoConformidadByMonth>();
            for (int i = 0; i < 12; i++) {
                string month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i + 1);
                byMonths.Add(new NoConformidadByMonth(month));
            }

            foreach (NoConformidadDetailModel item in dataSource) {
                byMonths[item.Fecha.Month - 1].Data.Add(new ModelData {
                    Fecha = item.Fecha,
                    Ejercicio = item.Ejercicio,
                    IdDepartamento = item.IdDepartamento,
                    IdStatus = item.IdStatus,
                    Periodo = item.Periodo,
                    Costo = double.Parse(item.Costo.ToString()),
                });
            }
            return byMonths;
        }

        private List<NoConformidadByMonth> InitialEnProceso() {
            List<NoConformidadByMonth> byMonths = new List<NoConformidadByMonth>();
            for (int i = 0; i < 12; i++) {
                string month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i + 1);
                byMonths.Add(new NoConformidadByMonth(month));
            }

            foreach (NoConformidadDetailModel item in dataSource) {
                if (item.Status == ValueObjects.NoConformidadStatusEnum.Proceso)
                    byMonths[item.Fecha.Month - 1].Data.Add(new ModelData {
                        Fecha = item.Fecha,
                        Ejercicio = item.Ejercicio,
                        IdDepartamento = item.IdDepartamento,
                        IdStatus = item.IdStatus,
                        Periodo = item.Periodo,
                        Costo = 0
                    });
            }
            return byMonths;
        }

        private List<NoConformidadByMonth> InitialPendientes() {
            List<NoConformidadByMonth> byMonths = new List<NoConformidadByMonth>();
            for (int i = 0; i < 12; i++) {
                string month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i + 1);
                byMonths.Add(new NoConformidadByMonth(month));
            }
            foreach (NoConformidadDetailModel item in dataSource) {
                if (item.Status != ValueObjects.NoConformidadStatusEnum.Proceso || item.Status != ValueObjects.NoConformidadStatusEnum.Cancelado)
                    byMonths[item.Fecha.Month - 1].Data.Add(new ModelData {
                        Fecha = item.Fecha,
                        Ejercicio = item.Ejercicio,
                        IdDepartamento = item.IdDepartamento,
                        IdStatus = item.IdStatus,
                        Periodo = item.Periodo,
                        Costo = 0
                    });
            }
            return byMonths;
        }

        private List<NoConformidadByMonth> InitialAutorizadas() {
            List<NoConformidadByMonth> byMonths = new List<NoConformidadByMonth>();
            for (int i = 0; i < 12; i++) {
                string month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i + 1);
                byMonths.Add(new NoConformidadByMonth(month));
            }
            foreach (NoConformidadDetailModel item in dataSource) {
                if (item.IdStatus > 1)
                    byMonths[item.Fecha.Month - 1].Data.Add(new ModelData {
                        Fecha = item.Fecha,
                        Ejercicio = item.Ejercicio,
                        IdDepartamento = item.IdDepartamento,
                        IdStatus = item.IdStatus,
                        Periodo = item.Periodo,
                        Costo = 0
                    });
            }
            return byMonths;
        }

        private List<NoConformidadByMonth> InitialCancelado() {
            List<NoConformidadByMonth> byMonths = new List<NoConformidadByMonth>();
            for (int i = 0; i < 12; i++) {
                string month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i + 1);
                byMonths.Add(new NoConformidadByMonth(month));
            }

            foreach (NoConformidadDetailModel item in dataSource) {
                if (item.Status == ValueObjects.NoConformidadStatusEnum.Cancelado)
                    byMonths[item.Fecha.Month - 1].Data.Add(new ModelData {
                        Fecha = item.Fecha,
                        Ejercicio = item.Ejercicio,
                        IdDepartamento = item.IdDepartamento,
                        IdStatus = item.IdStatus,
                        Periodo = item.Periodo,
                        Costo = 0
                    });
            }
            return byMonths;
        }

        private List<NoConformidadByMonthCostoRange> InitialCostos() {
            List<NoConformidadByMonthCostoRange> byMonths = new List<NoConformidadByMonthCostoRange>();
            for (int i = 0; i < 12; i++) {
                string month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(i + 1);
                byMonths.Add(new NoConformidadByMonthCostoRange(month));
            }
            foreach (NoConformidadDetailModel item in dataSource) {
                if (item.Status != ValueObjects.NoConformidadStatusEnum.Cancelado)
                    byMonths[item.Fecha.Month - 1].Data.Add(new ModelData {
                        Fecha = item.Fecha,
                        Ejercicio = item.Ejercicio,
                        IdDepartamento = item.IdDepartamento,
                        IdStatus = item.IdStatus,
                        Periodo = item.Periodo,
                        Costo = double.Parse(item.Costo.ToString()),
                    });
            }
            return byMonths;
        }

        private List<NoConformidadByDepartamento> InitializeDepartamentosData() {
            List<NoConformidadByDepartamento> departamentos = new List<NoConformidadByDepartamento>();

            foreach (DepartamentoModel region in this.departamentos) {
                if (region.Activo == 1) {
                    NoConformidadByDepartamento ov = new NoConformidadByDepartamento(region.Nombre);
                    foreach (NoConformidadDetailModel order in this.dataSource) {
                        if (order.IdDepartamento == region.IdDepartamento) {
                            ov.Data.Add(new ModelData {
                                IdDepartamento = order.IdDepartamento,
                                Ejercicio = order.Ejercicio,
                                Fecha = order.Fecha,
                                IdStatus = order.IdStatus,
                                Periodo = order.Periodo,
                                Costo = 0
                            });
                        }
                    }
                    if (ov.Data.Count > 0)
                        departamentos.Add(ov);
                }
            }
            return departamentos;
        }
        #endregion

        #region sub clases
        public class ModelData {
            public ModelData() { }

            public int IdStatus { get; set; }
            public int IdDepartamento { get; set; }
            public double Costo { get; set; }
            public int Periodo { get; set; }
            public int Ejercicio { get; set; }
            public DateTime Fecha { get; set; }
        }

        public abstract class ModelDataView {
            private List<ModelData> data;

            public ModelDataView() {
                this.data = new List<ModelData>();
            }

            public virtual int Count {
                get {
                    return data.Count;
                }
            }

            public List<ModelData> Data {
                get { return data; }
            }
        }

        public class NoConformidadByMonth : ModelDataView {
            private string month;

            public NoConformidadByMonth() { }

            public NoConformidadByMonth(string month) : base() {
                this.month = month;
            }

            public string Month {
                get {
                    return this.month;
                }
            }
        }

        public class NoConformidadByMonthCostoRange : NoConformidadByMonth {
            public NoConformidadByMonthCostoRange(string month) : base(month) { }

            public override int Count {
                get {
                    int value = 0;
                    foreach (ModelData order in Data) {
                        value += 1;
                    }
                    return value;
                }
            }

            public double Total {
                get {
                    double total = 0;
                    foreach (var item in this.Data) {
                        total += item.Costo;
                    }
                    return total;
                }
            }
        }

        public class NoConformidadByDepartamento : ModelDataView {
            private string name;

            public NoConformidadByDepartamento(string name) : base() {
                this.name = name;
            }

            public string Name {
                get {
                    return this.name;
                }
            }
        }
        #endregion
    }
}