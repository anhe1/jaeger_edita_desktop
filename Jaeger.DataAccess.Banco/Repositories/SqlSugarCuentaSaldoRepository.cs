﻿using System;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarCuentaSaldoRepository : MySqlSugarContext<BancoCuentaSaldoModel>, ISqlCuentaSaldoRepository {
        public SqlSugarCuentaSaldoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public BancoCuentaSaldoDetailModel GetSaldo(BancoCuentaModel objeto, DateTime fecha) {
            int mes = fecha.Month;
            int anio = fecha.Year;
            return this.Db.Queryable<BancoCuentaSaldoDetailModel>().Where(it => it.IdCuenta == objeto.IdCuenta).Where(it => it.Periodo == mes).Where(it => it.Ejercicio == anio).Single();
        }

        /// <summary>
        /// obtener saldo de una cuenta bancaria
        /// </summary>
        public BancoCuentaSaldoDetailModel GetSaldo(int idCuenta, DateTime fecha) {
            int mes = fecha.Month;
            int anio = fecha.Year;
            var result = this.Db.Queryable<BancoCuentaSaldoDetailModel>()
                .Where(it => it.IdCuenta == idCuenta)
                .Where(it => it.Periodo == mes).Where(it => it.Ejercicio == anio)
                .Single();
            if (result == null)
                return new BancoCuentaSaldoDetailModel { IdCuenta = idCuenta, Periodo = fecha.Month, Ejercicio = fecha.Year, SaldoInicial = 0 };
            return result;
        }

        public BancoCuentaSaldoDetailModel Save(BancoCuentaSaldoDetailModel model) {
            if (model.IdSaldo == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Ejercicio = model.FechaInicial.Year;
                model.Periodo = model.FechaInicial.Month;
                model.IdSaldo = this.Db.Insertable<BancoCuentaSaldoModel>(model).ExecuteReturnIdentity();
            } else {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Ejercicio = model.FechaInicial.Year;
                model.Periodo = model.FechaInicial.Month;
                this.Db.Updateable<BancoCuentaSaldoModel>(model).ExecuteCommand();
            }
            return model;
        }

        public void Create() {
            this.CreateTable();
        }
    }
}
