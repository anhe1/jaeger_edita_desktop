﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de auxiliares a los movimientos bancarios
    /// </summary>
    public class SqlSugarMovimientoAuxiliarRepository : MySqlSugarContext<MovimientoBancarioAuxiliarModel>, ISqlMovimientoAuxiliarRepository {
        public SqlSugarMovimientoAuxiliarRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// listado de auxiliares por condiciones
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        public IEnumerable<MovimientoBancarioAuxiliarDetailModel> GetList(List<Conditional> conditionals) {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<MovimientoBancarioAuxiliarDetailModel>().Where(_conditionalList).OrderBy(it => it.IdAuxiliar, OrderByType.Desc).ToList();
        }

        /// <summary>
        /// listado de auxiliares por condiciones
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        public MovimientoBancarioAuxiliarDetailModel Save(MovimientoBancarioAuxiliarDetailModel item) {
            if (item.IdAuxiliar == 0) {
                item.Creo = this.User;
                var _insert = this.Db.Insertable((MovimientoBancarioAuxiliarDetailModel)item);
                item.IdAuxiliar = this.Execute(_insert);
            } else {
                item.Creo = this.User;
                var _update = this.Db.Updateable((MovimientoBancarioAuxiliarDetailModel)item);
                this.Execute(_update);
            }
            return item;
        }

        public int Update(MovimientoBancarioAuxiliarDetailModel item) {
            return base.Update((MovimientoBancarioAuxiliarDetailModel)item);
        }

        public int Push(MovimientoBancarioAuxiliarDetailModel item) {
            var sqlCommand = @"";
            var parameters = new List<SugarParameter>() {
                new SugarParameter("", this.User)
            };
            return this.ExecuteTransaction(sqlCommand, parameters);
        }

        public bool CreateTables() {
            return this.CreateTable();
        }
    }
}
