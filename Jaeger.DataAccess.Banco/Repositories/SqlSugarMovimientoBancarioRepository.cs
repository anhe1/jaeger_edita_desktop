﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de movimientos bancarios
    /// </summary>
    public class SqlSugarMovimientoBancarioRepository : MySqlSugarContext<MovimientoBancarioModel>, ISqlMovimientoBancarioRepository {
        protected ISqlMovmientoComprobanteRepository comprobanteRepository;
        protected ISqlMovimientoAuxiliarRepository auxiliarRepository;

        public SqlSugarMovimientoBancarioRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.comprobanteRepository = new SqlSugarMovimientoComprobanteRepository(configuracion, user);
            this.auxiliarRepository = new SqlSugarMovimientoAuxiliarRepository(configuracion, user);
            this.User = user;
        }

        /// <summary>
        /// actualizar status del moviminto
        /// </summary>
        /// <param name="idStatus">indice del nuevo status</param>
        /// <param name="index">indice del movimiento</param>
        /// <returns>verdadero si transaccion</returns>
        public bool Update(int idStatus, int index) {
            return this.Db.Updateable<MovimientoBancarioModel>(it => new MovimientoBancarioModel {
                IdStatus = idStatus,
                FechaModifica = DateTime.Now,
                Modifica = this.User
            })
                .Where(it => it.Id == index).ExecuteCommand() > 0;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(MovimientoBancarioDetailModel)) {
                var sqlCommand = @"SELECT BNCMOV.*, BNCCTA.* FROM BNCMOV LEFT JOIN BNCCTA ON BNCMOV.BNCMOV_CTAE_ID = BNCCTA.BNCCTA_ID @wcondiciones";
                var movimientos = this.GetMapper<T1>(sqlCommand, conditionals).ToList();
                if (movimientos.Count > 0) {
                    // indices
                    var indexs = (movimientos as List<MovimientoBancarioDetailModel>).Select((MovimientoBancarioDetailModel it) => it.Id).ToArray<int>();
                    var sqlComprobantes = "SELECT * FROM BNCCMP WHERE BNCCMP_SUBID IN (@indexs) AND BNCCMP_A > 0";
                    sqlComprobantes = sqlComprobantes.Replace("@indexs", string.Join(",", indexs));
                    var comprobantes = this.GetMapper<MovimientoBancarioComprobanteDetailModel>(sqlComprobantes).ToList();

                    var sqlAuxiliar = "SELECT BNCAUX.* FROM BNCAUX WHERE BNCAUX_SBID IN (@indexs) AND BNCAUX_A > 0".Replace("@indexs", string.Join(",", indexs));
                    var auxiliares = this.GetMapper<MovimientoBancarioAuxiliarDetailModel>(sqlAuxiliar).ToList();

                    for (int i = 0; i < movimientos.Count(); i++) {
                        var d0 = comprobantes.Where(it => it.IdMovimiento == (movimientos as List<MovimientoBancarioDetailModel>)[i].Id).ToArray();
                        if (d0.Count() > 0) {
                            (movimientos as List<MovimientoBancarioDetailModel>)[i].Comprobantes = new BindingList<MovimientoBancarioComprobanteDetailModel>(
                                comprobantes.Where(it => it.IdMovimiento == (movimientos as List<MovimientoBancarioDetailModel>)[i].Id).ToList<MovimientoBancarioComprobanteDetailModel>()
                                );
                        }

                        var d1 = auxiliares.Where(it => it.IdMovimiento == (movimientos as List<MovimientoBancarioDetailModel>)[i].Id).ToArray();
                        if (d1.Count() > 0) {
                            (movimientos as List<MovimientoBancarioDetailModel>)[i].Auxiliar = new BindingList<MovimientoBancarioAuxiliarDetailModel>(
                                auxiliares.Where(it => it.IdMovimiento == (movimientos as List<MovimientoBancarioDetailModel>)[i].Id).ToList<MovimientoBancarioAuxiliarDetailModel>()
                                );
                        }
                    }
                    return movimientos;
                }
            } else if (typeof(T1) == typeof(MovimientoComprobanteModel) | typeof(T1) == typeof(MovimientoComprobanteMergeModelView)) {
                var sql = @"SELECT BNCCMP.*, BNCMOV.* FROM BNCCMP LEFT JOIN BNCMOV ON BNCCMP.BNCCMP_SUBID = BNCMOV.BNCMOV_ID @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(MovimientoBancarioComprobantePago)) {
                // solo para obtener referencia del comprobante de pago
                var sql = this.GetQueryComplementoPago();
                return base.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(MovimientoBancarioComprobanteDetailModel)) {
                var isRemision = conditionals.Where(it => it.FieldName.ToLower().Contains("rmsn")).Count() > 0;
                if (isRemision == false) {
                    var sql = this.GetQueryComprobante();
                    return base.GetMapper<T1>(sql, conditionals);
                } else {
                    var sql = this.GetQueryRemision();
                    return base.GetMapper<T1>(sql, conditionals);
                }
            } else if (typeof(T1) == typeof(MovimientoBancarioModel)) {
                return base.GetList<T1>(conditionals);
            }
            return new List<T1>();
        }

        /// <summary>
        /// almacenar
        /// </summary>
        public IMovimientoBancarioDetailModel Save(IMovimientoBancarioDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Identificador = this.NoIndetificacion(model);
                model.InfoAuxiliar = model.Json();
                var _insert = this.Db.Insertable((MovimientoBancarioModel)model);
                model.Id = this.Execute(_insert);
            } else {
                model.FechaModifica = DateTime.Now;
                model.InfoAuxiliar = model.Json();
                var _update = this.Db.Updateable((MovimientoBancarioModel)model);
                this.Execute(_update);
            }

            if (model.Comprobantes != null) {
                if (model.Comprobantes.Count > 0) {
                    for (int i = 0; i < model.Comprobantes.Count; i++) {
                        model.Comprobantes[i].IdMovimiento = model.Id;
                        model.Comprobantes[i].Identificador = model.Identificador;
                        model.Comprobantes[i] = this.comprobanteRepository.Save(model.Comprobantes[i]);
                    }
                }
            }

            if (model.Auxiliar != null) {
                if (model.Auxiliar.Count > 0) {
                    for (int i = 0; i < model.Auxiliar.Count; i++) {
                        if (model.Auxiliar[i].IdAuxiliar == 0) {
                            model.Auxiliar[i].IdMovimiento = model.Id;
                            model.Auxiliar[i] = this.auxiliarRepository.Save(model.Auxiliar[i]);
                        }
                    }
                }
            }

            return model;
        }

        public bool Aplicar(IMovimientoBancarioDetailModel model) {
            if (model.Status == MovimientoBancarioStatusEnum.Cancelado) {
                var _update = this.Db.Updateable((MovimientoBancarioDetailModel)model).UpdateColumns(it => new { it.IdStatus, it.FechaModifica, it.Modifica, it.Cancela, it.FechaCancela });
                if (this.Execute(_update) > 0) {
                    if (this.Aplicar(model.Id) == false) {
                        Services.LogErrorService.LogWrite("No se actualizaron las partidas: " + model.Identificador);
                        return false;
                    }
                }
            } else {
                var _update = this.Db.Updateable((MovimientoBancarioDetailModel)model).UpdateColumns(it => new { it.IdStatus, it.FechaModifica, it.Modifica });
                if (this.Execute(_update) > 0) {
                    if (this.Aplicar(model.Id) == false) {
                        Services.LogErrorService.LogWrite("No se actualizaron las partidas: " + model.Identificador);
                        return false;
                    }
                }
            }
            return true;
        }

        public bool Aplicar(int model) {
            var sql = @"SELECT a.BNCMOV_TIPO_ID, b.BNCCMP_TIPO as documento, b.BNCCMP_IDCOM as idcomprobante, b.BNCCMP_EFECTO as efecto, b.BNCCMP_UUID as uuid, b.BNCCMP_TOTAL as total, b.BNCCMP_SBTP,
                        (SELECT SUM(c.BNCCMP_CARGO) FROM BNCCMP c, BNCMOV d WHERE c.BNCCMP_SUBID = d.BNCMOV_ID AND c.BNCCMP_IDCOM = b.BNCCMP_IDCOM AND (d.BNCMOV_STTS_ID = @status1 OR d.BNCMOV_STTS_ID = @status2)) AS cargo,
                        (SELECT SUM(c.BNCCMP_ABONO) FROM BNCCMP c, BNCMOV d WHERE c.BNCCMP_SUBID = d.BNCMOV_ID AND c.BNCCMP_IDCOM = b.BNCCMP_IDCOM AND (d.BNCMOV_STTS_ID = @status1 OR d.BNCMOV_STTS_ID = @status2)) AS abono,
                        (SELECT MAX(d.BNCMOV_FCCBR) FROM BNCCMP c, BNCMOV d WHERE c.BNCCMP_SUBID = d.BNCMOV_ID AND c.BNCCMP_IDCOM = b.BNCCMP_IDCOM AND (d.BNCMOV_STTS_ID = @status1 OR d.BNCMOV_STTS_ID = @status2)) AS aplicacion
                        FROM BNCMOV a, BNCCMP b
                        WHERE b.BNCCMP_SUBID = a.BNCMOV_ID
                        AND a.BNCMOV_ID = @index";

            var tabla = this.Db.Ado.GetDataTable(sql,
                new SugarParameter[] {
                    new SugarParameter("@index", model),
                    new SugarParameter("@status1", (int)MovimientoBancarioStatusEnum.Aplicado),
                    new SugarParameter("@status2", (int)MovimientoBancarioStatusEnum.Auditado)
                    });


            var result = this.GetMapper<MovimientoBancarioFactory>(tabla).ToList();
            int contador = 0;
            if (result.Count > 0) {
                foreach (var item in result) {
                    if (item.TipoDocumento == MovimientoBancarioTipoComprobanteEnum.CFDI) {
                        try {
                            var _update = this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel() { FechaUltimoPago = item.FechaAplicacion, Status = item.Status, Acumulado = item.Acumulado }).Where(it => it.IdComprobante == item.IdComprobante);
                            this.Execute(_update);
                        } catch (Exception ex) {
                            Services.LogErrorService.LogWrite(ex.Message + "| No se actualizaron las partidas: " + item.IdDocumento);
                        }
                    }
                    contador++;
                }
            }
            return result.Count == contador;
        }

        public bool CreateTables() {
            this.comprobanteRepository.Create();
            this.auxiliarRepository.CreateTables();
            return base.CreateTable();
        }

        /// <summary>
        /// calcular el numero de indentifiacion de la prepoliza
        /// </summary>
        private string NoIndetificacion(IMovimientoBancarioDetailModel modelo) {
            int mes = modelo.FechaEmision.Month;
            int anio = modelo.FechaEmision.Year;
            int indice = 0;

            try {
                indice = this.Db.Queryable<MovimientoBancarioDetailModel>()
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.FechaEmision.Month == mes)
                    .Where(it => it.IdTipo == modelo.IdTipo)
                    .Select(it => new { maximus = SqlFunc.AggregateCount(it.FechaEmision) }).Single().maximus + 1;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                indice = 0;
            }

            return string.Format("{0}{1}{2}",
                modelo.Tipo.ToString()[0],
                modelo.FechaEmision.ToString("yyMM"),
                indice.ToString("000#"));
        }

        /// <summary>
        /// consulta para obtener listado de comprobantes fiscales
        /// </summary>
        /// <returns></returns>
        private string GetQueryComprobante() {
            var sql = @"select 
_cfdi_ver    BNCCMP_VER, 
_cfdi_efecto BNCCMP_EFECTO, 
_cfdi_doc_id BNCCMP_SBTP, 
_cfdi_id     BNCCMP_IDCOM, 
_cfdi_folio  BNCCMP_FOLIO,
_cfdi_serie  BNCCMP_SERIE, 
_cfdi_status BNCCMP_STATUS,
_cfdi_rfce   BNCCMP_RFCE, 
_cfdi_nome   BNCCMP_EMISOR, 
_cfdi_rfcr   BNCCMP_RFCR, 
_cfdi_nomr   BNCCMP_RECEPTOR, 
_cfdi_fecems BNCCMP_FECEMS, 
_cfdi_mtdpg  BNCCMP_MTDPG, 
_cfdi_frmpg  BNCCMP_FRMPG,
_cfdi_moneda BNCCMP_MONEDA, 
_cfdi_uuid   BNCCMP_UUID, 
_cfdi_par    BNCCMP_PAR, 
_cfdi_total  BNCCMP_TOTAL, 
_cfdi_cbrd   BNCCMP_CBRD, 
_cfdi_estado BNCCMP_ESTADO, 
 (_cfdi_total-_cfdi_cbrd) BNCCMP_XCBR
from _cfdi @wcondiciones
order by _cfdi_fecems desc";
            return sql;
        }

        private string GetQueryRemision() {
            var sql = @"SELECT 
'Remision'          AS BNCCMP_TIPO,
RMSN.RMSN_VER       AS BNCCMP_VER,
'Ingreso'           AS BNCCMP_EFECTO, 
RMSN.RMSN_STTS_ID   AS CFDI_STATUS,
(CASE RMSN.RMSN_STTS_ID WHEN 0 THEN 'Cancelado' 
                               WHEN 1 THEN 'Pendiente' 
                               WHEN 2 THEN 'En Ruta...' 
                               WHEN 3 THEN 'Entregado' 
                               WHEN 4 THEN 'Por Cobrar' 
                               WHEN 5 THEN 'Cobrado' 
                               WHEN 6 THEN 'Facturado' 
                               WHEN 7 THEN 'Especial' END) BNCCMP_STATUS,
1                   AS BNCCMP_SBTP, 
RMSN.RMSN_ID        AS BNCCMP_IDCOM,
RMSN.RMSN_FOLIO     AS BNCCMP_FOLIO, 
RMSN.RMSN_SERIE     AS BNCCMP_SERIE,
RMSN.RMSN_RFCE      AS BNCCMP_RFCE,
CONF.CONF_DATA      AS BNCCMP_EMISOR,  
RMSN.RMSN_RFCR      AS BNCCMP_RFCR,
RMSN.RMSN_NOMR      AS BNCCMP_RECEPTOR,
RMSN.RMSN_FECEMS    AS BNCCMP_FECEMS,
RMSN.RMSN_MTDPG     AS BNCCMP_MTDPG,
RMSN.RMSN_FRMPG     AS BNCCMP_FRMPG, 
RMSN.RMSN_MONEDA    AS BNCCMP_MONEDA,
RMSN.RMSN_UUID      AS BNCCMP_UUID,
0                   AS BNCCMP_PAR,
RMSN.RMSN_TOTAL     AS BNCCMP_TOTAL1,
RMSN.RMSN_CBRD      AS BNCCMP_CBRD,
RMSN.RMSN_XCBRR     AS BNCCMP_TOTAL,
RMSN.RMSN_XCBRR     AS BNCCMP_XCBR,
(CASE RMSN.RMSN_STTS_ID WHEN 0 THEN 'Cancelado' ELSE 'Vigente' END) AS BNCCMP_ESTADO
FROM RMSN
LEFT JOIN CONF ON CONF.CONF_KEY = 4
@wcondiciones ORDER BY RMSN.RMSN_FECEMS DESC";
            return sql;
        }

        /// <summary>
        /// consulta para comprobantes fiscales, donde el resultado de esta sea la información del comprobante de pago, la referencia que se pasa es el uuid de uno de los documentos relacionados
        /// </summary>
        private string GetQueryComplementoPago() {
            return @"SELECT  
                    _cfdi_ver    BNCCMP_VER, 
                    _cfdi_efecto BNCCMP_EFECTO, 
                    _cfdi_doc_id BNCCMP_SBTP, 
                    _cfdi_id     BNCCMP_IDCOM, 
                    _cfdi_folio  BNCCMP_FOLIO,
                    _cfdi_serie  BNCCMP_SERIE, 
                    _cfdi_status BNCCMP_STATUS,
                    _cfdi_rfce   BNCCMP_RFCE, 
                    _cfdi_nome   BNCCMP_EMISOR, 
                    _cfdi_rfcr   BNCCMP_RFCR, 
                    _cfdi_nomr   BNCCMP_RECEPTOR, 
                    _cfdi_fecems BNCCMP_FECEMS, 
                    _cfdi_mtdpg  BNCCMP_MTDPG, 
                    _cfdi_frmpg  BNCCMP_FRMPG,
                    _cfdi_moneda BNCCMP_MONEDA, 
                    _cfdi_uuid   BNCCMP_UUID,
                    _cfdi_par    BNCCMP_PAR, 
                    _cfdi_total  BNCCMP_TOTAL, 
                    _cfdi_cbrd   BNCCMP_CBRD, 
                    _cfdi_estado BNCCMP_ESTADO, 
                     (_cfdi_total-_cfdi_cbrd) BNCCMP_XCBR,
                    _cmppgd.*
                    FROM _cmppg 
                    Left JOIN _cmppgd  ON ( _cmppg_id = _cmppgd_idpg )  
                    Left JOIN _cfdi  ON ( _cmppg_cfd_id = _cfdi_id ) @wcondiciones";
        }
    }
}
