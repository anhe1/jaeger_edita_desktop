﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de formas de pago 
    /// </summary>
    public class SqlSugarBancoFormaPagoRepository : MySqlSugarContext<BancoFormaPagoModel>, ISqlBancoFormaPagoRepository {
        public SqlSugarBancoFormaPagoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override bool Delete(int id) {
            return base.Delete(id);
        }

        /// <summary>
        /// obtener listado de formas de pago
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        //public IEnumerable<IBancoFormaPagoDetailModel> GetList(bool onlyActive) {
        //    return this.Db.Queryable<BancoFormaPagoDetailModel>().WhereIF(onlyActive == true, it => it.Activo == true).ToList();
        //}

        //public IEnumerable<BancoFormaPagoDetailModel> GetList(List<Conditional> conditionals) {
        //    var conModels = new List<IConditionalModel>();
        //    foreach (var item in conditionals) {
        //        conditionals.Add(new Conditional() { FieldName = item.FieldName, FieldValue = item.FieldValue, ConditionalType =  (ConditionalTypeEnum)item.ConditionalType });
        //    }

        //    var result = this.Db.Queryable<BancoFormaPagoDetailModel>().Where(conModels).OrderBy(it => it.Clave).ToList();
        //    return result;
        //}

        /// <summary>
        /// almacenar forma de pago
        /// </summary>
        /// <param name="item">BancoFormaPagoDetailModel</param>
        public IBancoFormaPagoDetailModel Save(IBancoFormaPagoDetailModel item) {
            if (item.Id == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                var _insert = this.Db.Insertable((BancoFormaPagoModel)item);
                item.Id = this.Execute(_insert);
            }
            else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                var _update = this.Db.Updateable((BancoFormaPagoModel)item);
                this.Execute(_update);
            }
            return item;
        }

        public bool CrearTabla() {
            return this.CreateTable();
        }
    }
}
