﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de conceptos de movimientos bancarios
    /// </summary>
    public class SqlSugarMovimientoConceptoRepository : SqlSugarContext<MovimientoConceptoModel>, ISqlMovimientoConceptoRepository {
        public SqlSugarMovimientoConceptoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<T1>().Where(_conditionalList).ToList();
        }

        public MovimientoConceptoDetailModel Save(MovimientoConceptoDetailModel model) {
            var seleccionados = model.ObjetosDirectorio.Where(it => it.Selected == true).Select(it => it.Id).ToArray();
            model.Directorio = string.Join(",", seleccionados);
            model.RelacionComprobantes = string.Join(",", model.ObjetosRelacion.Where(it => it.Selected == true).Select(it => it.Id).ToArray());
            model.TipoComprobante = string.Join(",", model.ObjetosComprobante.Where(it => it.Selected == true).Select(it => it.Id).ToArray());
            if (model.IdConcepto == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdConcepto = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }

        /// <summary>
        /// crear tabla
        /// </summary>
        public void Create() {
            this.CreateTable();
        }
    }
}
