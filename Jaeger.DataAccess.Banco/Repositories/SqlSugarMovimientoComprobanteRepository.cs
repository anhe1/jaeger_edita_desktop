﻿using System;
using System.Linq;
using SqlSugar;
using System.Collections.Generic;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de auxiliares de movimientos bancarios
    /// </summary>
    public class SqlSugarMovimientoComprobanteRepository : MySqlSugarContext<MovimientoBancarioComprobanteModel>, ISqlMovmientoComprobanteRepository {
        
        public SqlSugarMovimientoComprobanteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public MovimientoBancarioComprobanteDetailModel Save(MovimientoBancarioComprobanteDetailModel model) {
            if (model.Id == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                var _insert = this.Db.Insertable((MovimientoBancarioComprobanteDetailModel)model);
                model.Id = this.Execute(_insert);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                var _update = this.Db.Updateable((MovimientoBancarioComprobanteDetailModel)model);
                this.Execute(_update);
            }
            return model;
        }

        public IEnumerable<MovimientoBancarioComprobanteDetailModel> Save(List<MovimientoBancarioComprobanteDetailModel> models) {
            for (int i = 0; i < models.Count(); i++) {
                models[i] = this.Save(models[i]);
            }
            return models;
        }

        public IEnumerable<MovimientoBancarioComprobanteDetailModel> GetList(List<IConditional> conditionals) {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<MovimientoBancarioComprobanteDetailModel>().Where(_conditionalList).OrderBy(it => it.Id, OrderByType.Desc).ToList();
        }

        public void Create() {
            base.CreateTable();
        }
    }
}
