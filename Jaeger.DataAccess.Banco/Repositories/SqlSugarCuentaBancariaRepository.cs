﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de cuentas bancarias
    /// </summary>
    public class SqlSugarCuentaBancariaRepository : MySqlSugarContext<BancoCuentaModel>, ISqlCuentaBancariaRepository {
        public SqlSugarCuentaBancariaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        /// <summary>
        /// obtener cuenta bancaria
        /// </summary>
        /// <param name="index">indice de la tabla</param>
        public new BancoCuentaDetailModel GetById(int index) {
            return this.Db.Queryable<BancoCuentaDetailModel>().Where(it => it.IdCuenta == index).Single();
        }

        /// <summary>
        /// obtener listado de cuentas bancarias
        /// </summary>
        /// <param name="onlyActive">solo activos</param>
        public IEnumerable<IBancoCuentaDetailModel> GetList(bool onlyActive) {
            try {
                var result = this.Db.Queryable<BancoCuentaDetailModel>().WhereIF(onlyActive == true, it => it.Activo == true).ToList();
                return result;
            } catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// almacenar cuenta bancaria
        /// </summary>
        /// <param name="model">modelo</param>
        public IBancoCuentaDetailModel Save(IBancoCuentaDetailModel model) {
            if (model.IdCuenta == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                var _insert = this.Db.Insertable((BancoCuentaDetailModel)model);
                model.IdCuenta = this.Execute(_insert);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                var _update = this.Db.Updateable((BancoCuentaDetailModel)model);
                this.Execute(_update);
            }
            return model;
        }

        /// <summary>
        /// crear tabla del modelo
        /// </summary>
        public void Create() {
            this.CreateTable();
        }
    }
}
