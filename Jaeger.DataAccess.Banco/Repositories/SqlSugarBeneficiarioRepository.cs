﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarBeneficiarioRepository : MySqlSugarContext<BeneficiarioModel>, Jaeger.Domain.Banco.Contracts.ISqlBeneficiarioRepository {
        /// <summary>
        /// constructor
        /// </summary>
        public SqlSugarBeneficiarioRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public BeneficiarioDetailModel GetBeneficiario(int index) {
            var result = this.Db.Queryable<BeneficiarioDetailModel>().Where(it => it.IdDirectorio == index).Mapper((itemModel, cache) => {
                var cuentasBancarias = cache.Get(directorio => {
                    var allIds = directorio.Select(it => it.IdDirectorio).ToList();
                    return this.Db.Queryable<CuentaBancariaModel>().Where(it => allIds.Contains(it.IdDirectorio)).ToList();
                });
                itemModel.CuentasBancarias = new System.ComponentModel.BindingList<ICuentaBancariaModel>(cuentasBancarias.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<ICuentaBancariaModel>());

            }).Single();

            return result;
        }

        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(bool onlyActive) {
            return this.Db.Queryable<BeneficiarioDetailModel>()
                .WhereIF(onlyActive == true, it => it.Activo == true)
                .Where(it => it.Relacion.Contains("Beneficiario")).ToList();
        }

        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<TipoRelacionComericalEnum> tipoRelacion) {
            var stringList = new List<string>();
            var conModels = new List<IConditionalModel>();
            var c2 = new List<KeyValuePair<WhereType, ConditionalModel>>();
            foreach (var item in tipoRelacion) {
                c2.Add(new KeyValuePair<WhereType, ConditionalModel>(WhereType.Or, new ConditionalModel() { FieldName = "_drctr_rlcn", ConditionalType = ConditionalType.Like, FieldValue = item.ToString() }));
            }
            // solo registros activos
            c2.Add(new KeyValuePair<WhereType, ConditionalModel>(WhereType.And, new ConditionalModel() { FieldName = "_drctr_a", ConditionalType = ConditionalType.Equal, FieldValue = "1" }));
            conModels.Add(new ConditionalCollections() { ConditionalList = c2 });

            var result = this.Db.Queryable<BeneficiarioDetailModel>()
                .Where(conModels).Where(it => it.Nombre != null)
                .OrderBy(it => it.Nombre)
                .Mapper((itemModel, cache) => {
                    var allItems = cache.Get(orderList => {
                        var allIds = orderList.Select(it => it.IdDirectorio).ToList();
                        return this.Db.Queryable<CuentaBancariaModel>().Where(it => allIds.Contains(it.IdDirectorio)).ToList();
                    });
                    itemModel.CuentasBancarias = new System.ComponentModel.BindingList<ICuentaBancariaModel>(allItems.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<ICuentaBancariaModel>());
                }).ToList();
            return result;
        }

        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<Conditional> conditionals) {
            var conModels = new List<IConditionalModel>();
            foreach (var item in conditionals) {
                conModels.Add(new ConditionalModel() { FieldName = item.FieldName, FieldValue = item.FieldValue, ConditionalType = (ConditionalType)item.ConditionalType });
            }
            var result = this.Db.Queryable<BeneficiarioDetailModel>()
                .Where(conModels).Where(it => it.Nombre != null)
                .OrderBy(it => it.Nombre)
                .Mapper((itemModel, cache) => {
                    var allItems = cache.Get(orderList => {
                        var allIds = orderList.Select(it => it.IdDirectorio).ToList();
                        return this.Db.Queryable<CuentaBancariaModel>().Where(it => allIds.Contains(it.IdDirectorio)).ToList();
                    });
                    itemModel.CuentasBancarias = new System.ComponentModel.BindingList<ICuentaBancariaModel>(allItems.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<ICuentaBancariaModel>());
                }).ToList();
            return result;
        }

        /// <summary>
        /// listado del directorio por relaciones 
        /// </summary>
        /// <param name="relacion">lista de relaciones</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="search">palabra de busqueda</param>
        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<string> relacion, bool onlyActive, string search = "") {
            var conModels = new List<IConditionalModel>();
            foreach (var item in relacion) {
                conModels.Add(new ConditionalCollections() { ConditionalList = new List<KeyValuePair<WhereType, ConditionalModel>>() {
                    new  KeyValuePair<WhereType, ConditionalModel> ( WhereType.Or,new ConditionalModel() { FieldName = "_drctr_rlcn", ConditionalType = ConditionalType.Like, FieldValue = "%" + item + "%",  }) }
                });
            }

            var result = this.Db.Queryable<BeneficiarioDetailModel>()
                .Where(conModels)
                .Where(it => it.Nombre != null)
                .WhereIF(!string.IsNullOrEmpty(search), it => it.Nombre.ToLower().Contains(search.ToLower()))
                .WhereIF(onlyActive == true, it => it.Activo == true)
                .OrderBy(it => it.Nombre)
                .Mapper((itemModel, cache) => {
                    var allItems = cache.Get(orderList => {
                        var allIds = orderList.Select(it => it.IdDirectorio).ToList();
                        return this.Db.Queryable<CuentaBancariaModel>().Where(it => allIds.Contains(it.IdDirectorio)).ToList();
                    });
                    itemModel.CuentasBancarias = new System.ComponentModel.BindingList<ICuentaBancariaModel>(allItems.Where(it => it.IdDirectorio == itemModel.IdDirectorio).ToList<ICuentaBancariaModel>());
                }).ToList();
            return result;
        }

        public BeneficiarioDetailModel Save(BeneficiarioDetailModel item) {
            if (item.IdDirectorio == 0)
                item.IdDirectorio = this.Insert(item);
            else
                this.Update(item);

            // cuentas bancarias
            if (item.CuentasBancarias != null) {
                for (int i = 0; i < item.CuentasBancarias.Count; i++) {
                    if (item.CuentasBancarias[i].IdCuenta == 0) {
                        item.CuentasBancarias[i].IdDirectorio = item.IdDirectorio;
                        item.CuentasBancarias[i].FechaNuevo = DateTime.Now;
                        var d = this.Db.Insertable<CuentaBancariaModel>(item.CuentasBancarias[i]);
                        item.CuentasBancarias[i].IdCuenta = this.Execute(d);
                    } else {
                        item.CuentasBancarias[i].FechaModifica = DateTime.Now;
                        this.Db.Updateable<CuentaBancariaModel>(item.CuentasBancarias[i]).ExecuteCommand();
                    }
                }
            }

            return item;
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones";
            return this.GetMapper<T1>(CommandText, conditionals).ToList();
        }
    }
}
