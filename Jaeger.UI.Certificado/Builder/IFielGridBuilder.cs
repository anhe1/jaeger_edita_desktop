﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Certificado.Builder {
    public interface IFielGridBuilder : IGridViewBuilder {
        IFielTempleteGridBuilder Templetes();
    }

    public interface IFielTempleteGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IFielTempleteGridBuilder Master();
    }

    public interface IFielColumnsGridBuilder : IGridViewColumnsBuild {
        IFielColumnsGridBuilder RFC();
        IFielColumnsGridBuilder RazonSocial();
        IFielColumnsGridBuilder NoSerie();
        IFielColumnsGridBuilder InicioVigencia();
        IFielColumnsGridBuilder FinVigencia();
    }
}
