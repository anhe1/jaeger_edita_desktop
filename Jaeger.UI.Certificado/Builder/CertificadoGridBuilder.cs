﻿using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Certificado.Builder {
    public class CertificadoGridBuilder : GridViewBuilder, IGridViewBuilder, ICertificadoGridBuilder, ICertificadoTempleteGridBuilder, ICertificadoColumnsGridBuilder {
        public CertificadoGridBuilder() : base() { }

        public ICertificadoTempleteGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public ICertificadoTempleteGridBuilder Master() {
            this.RFC().RazonSocial().NoSerie().InicioVigencia().FinVigencia();
            return this;
        }

        /// <summary>
        /// registro federal de contribuyentes
        /// </summary>
        public ICertificadoColumnsGridBuilder RFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "RFC",
                Width = 90
            });
            return this;
        }

        /// <summary>
        /// nombre o razon social
        /// </summary>
        public ICertificadoColumnsGridBuilder RazonSocial() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RazonSocial",
                HeaderText = "Razón Social del Emisor",
                Name = "RazonSocial",
                Width = 220
            });
            return this;
        }

        /// <summary>
        /// numero de serie
        /// </summary>
        public ICertificadoColumnsGridBuilder NoSerie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Núm. Serie",
                Name = "Serie",
                Width = 110
            });
            return this;
        }

        /// <summary>
        /// fecha de inicio de vigencia
        /// </summary>
        public ICertificadoColumnsGridBuilder InicioVigencia() {
            this._Columns.Add(new GridViewDateTimeColumn {
                ExcelExportType = DisplayFormatType.ShortDate,
                FieldName = "NotBefore",
                FormatString = this.FormatStringDate,
                HeaderText = "Inicio\r\nVigencia",
                Name = "NotBefore",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
            });
            return this;
        }

        /// <summary>
        /// fecha final de vigencia
        /// </summary>
        public ICertificadoColumnsGridBuilder FinVigencia() {
            this._Columns.Add(new GridViewDateTimeColumn {
                ExcelExportType = DisplayFormatType.ShortDate,
                FieldName = "NotAfter",
                FormatString = this.FormatStringDate,
                HeaderText = "Final\r\nVigencia",
                Name = "NotAfter",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
            });
            return this;
        }
    }
}
