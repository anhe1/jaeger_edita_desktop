﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Certificado.Builder {
    public interface ICertificadoGridBuilder : IGridViewBuilder {
        ICertificadoTempleteGridBuilder Templetes();
    }

    public interface ICertificadoTempleteGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        ICertificadoTempleteGridBuilder Master();
    }

    public interface ICertificadoColumnsGridBuilder : IGridViewColumnsBuild {
        ICertificadoColumnsGridBuilder RFC();
        ICertificadoColumnsGridBuilder RazonSocial();
        ICertificadoColumnsGridBuilder NoSerie();
        ICertificadoColumnsGridBuilder InicioVigencia();
        ICertificadoColumnsGridBuilder FinVigencia();
    }
}
