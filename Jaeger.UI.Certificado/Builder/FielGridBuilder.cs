﻿using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Certificado.Builder {
    public class FielGridBuilder : GridViewBuilder, IGridViewBuilder, IFielGridBuilder, IFielTempleteGridBuilder, IFielColumnsGridBuilder {
        public FielGridBuilder() : base() { }

        public IFielTempleteGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public IFielTempleteGridBuilder Master() {
            this.RFC().RazonSocial().NoSerie().InicioVigencia().FinVigencia();
            return this;
        }

        /// <summary>
        /// registro federal de contribuyentes
        /// </summary>
        public IFielColumnsGridBuilder RFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "RFC",
                Width = 90
            });
            return this;
        }

        /// <summary>
        /// nombre o razon social
        /// </summary>
        public IFielColumnsGridBuilder RazonSocial() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RazonSocial",
                HeaderText = "Razón Social del Emisor",
                Name = "RazonSocial",
                Width = 220
            });
            return this;
        }

        /// <summary>
        /// numero de serie
        /// </summary>
        public IFielColumnsGridBuilder NoSerie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoSerie",
                HeaderText = "Núm. Serie",
                Name = "NoSerie",
                Width = 110
            });
            return this;
        }

        /// <summary>
        /// fecha de inicio de vigencia
        /// </summary>
        public IFielColumnsGridBuilder InicioVigencia() {
            this._Columns.Add(new GridViewDateTimeColumn {
                ExcelExportType = DisplayFormatType.ShortDate,
                FieldName = "InicioVigencia",
                FormatString = this.FormatStringDate,
                HeaderText = "Inicio\r\nVigencia",
                Name = "InicioVigencia",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
            });
            return this;
        }

        /// <summary>
        /// fecha final de vigencia
        /// </summary>
        public IFielColumnsGridBuilder FinVigencia() {
            this._Columns.Add(new GridViewDateTimeColumn {
                ExcelExportType = DisplayFormatType.ShortDate,
                FieldName = "FinalVigencia",
                FormatString = this.FormatStringDate,
                HeaderText = "Final\r\nVigencia",
                Name = "FinalVigencia",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
            });
            return this;
        }
    }
}
