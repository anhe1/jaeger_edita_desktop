﻿using System.Text.RegularExpressions;

namespace Jaeger.UI.Certificado.Helpers {
    public static class ValidaService {
        public static bool RFC(string rfc) {
            if (rfc == null)
                return false;
            return !Regex.IsMatch(rfc, "^[a-zA-Z&ñÑ]{3,4}(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\\d])|([0-9]{2})([0][2])([0][1-9]|[1][\\d]|[2][0-8]))(\\w{2}[A|a|0-9]{1})$") ? false : true;
        }
    }
}
