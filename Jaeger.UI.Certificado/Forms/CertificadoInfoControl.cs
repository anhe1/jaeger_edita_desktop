﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Certificado.Forms {
    public partial class CertificadoInfoControl : UserControl {
        public CertificadoInfoControl() {
            InitializeComponent();
        }

        private void CertificadoInfoControl_Load(object sender, EventArgs e) {
            this.RazonSocial.NullText = "Nombre o Razon Social";
            this.RFC.TextBoxElement.ToolTipText = "Registro Federal de Contribuyentes";
            this.RFC.NullText = "RFC";
            this.RFC.TextChanging += RFC_TextChanging;
        }

        private void RFC_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e) {
            this.providerError.Clear();
            if (!Helpers.ValidaService.RFC(this.RFC.TextBoxElement.Text)) {
                this.providerError.SetError(this.RFC, "RFC no válido");
            }
        }
    }
}
