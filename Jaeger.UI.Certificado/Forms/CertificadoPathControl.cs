﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Certificado.Forms {
    public partial class CertificadoPathControl : UserControl {
        #region
        private Color colorInvalido = Color.Bisque;
        private Color colorValido = Color.MintCream;
        private Color colorEditable = Color.LemonChiffon;
        #endregion

        public CertificadoPathControl() {
            InitializeComponent();
        }

        private void CertificadoPathControl_Load(object sender, EventArgs e) {
            this.PathCerButton.Click += this.ButtonCER_Click;
            this.PathKeyButton.Click += this.ButtonKEY_Click;
            this.CheckPassword.ToggleStateChanged += this.PasswordCheck_ToggleStateChanged;
            this.PasswordC.TextChanging += Password_TextChanging;
            this.PathCer.TextBoxElement.TextChanged += TextBoxElement_TextChanged;
            this.PathKey.TextBoxElement.TextChanged += TextBoxElement_TextChanged;
            this.PathCer.DragDrop += PathCer_DragDrop;
            this.PathCer.DragEnter += PathCer_DragEnter;
            this.PathKey.DragDrop += PathKey_DragDrop;
            this.PathKey.DragEnter += PathKey_DragEnter;
        }

        private void PathKey_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Copy;
        }

        private void PathKey_DragDrop(object sender, DragEventArgs e) {
            string fpath = GetDroppedFileName(e);
            if (fpath != null)
                PathKey.Text = fpath;
        }

        private void PathCer_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Copy;
        }

        private void PathCer_DragDrop(object sender, DragEventArgs e) {
            string fpath = GetDroppedFileName(e);
            if (fpath != null)
                PathCer.Text = fpath;
        }

        private void Password_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e) {
            if (this.Password.Text == "") {
                this.Password.BackColor = this.colorEditable;
                this.PasswordC.BackColor = this.colorEditable;
            } else if (this.Password.Text == this.PasswordC.Text) {
                this.Password.BackColor = this.colorEditable;
                this.PasswordC.BackColor = this.colorEditable;
            } else {
                this.Password.BackColor = this.colorInvalido;
                this.PasswordC.BackColor = this.colorInvalido;
            }
            this.UpdateState();
        }

        private void PasswordCheck_ToggleStateChanged(object sender, StateChangedEventArgs args) {
            if (!this.CheckPassword.Checked) {
                this.Password.PasswordChar = '*';
                this.Password.PasswordChar = '*';
            } else {
                this.Password.PasswordChar = '\0';
                this.PasswordC.PasswordChar = (char)0;
            }
        }

        private void TextBoxElement_TextChanged(object sender, EventArgs e) {
            if (!File.Exists(this.PathCer.Text) | Path.GetExtension(this.PathCer.Text).ToLower() != ".cer") {
                this.PathCer.BackColor = this.colorInvalido;
            } else {
                this.PathCer.BackColor = this.colorValido;
            }
            if (!File.Exists(this.PathKey.Text) | Path.GetExtension(this.PathKey.Text).ToLower() != ".key") {
                this.PathKey.BackColor = this.colorInvalido;
            } else {
                this.PathKey.BackColor = this.colorValido;
            }
            this.UpdateState();
        }

        private void ButtonCER_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.cer",
                DefaultExt = ".cer",
                Filter = "Archivo CER (*.cer)|*.cer",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.PathCer.Text = openFileDialog.FileName;
            }
        }

        private void ButtonKEY_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo de la llave privada *.key",
                DefaultExt = ".key",
                Filter = "Archivo KEY (*.key)|*.key",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.PathKey.Text = openFileDialog.FileName;
            }
        }

        private void UpdateFilePathTextBox(RadTextBox txt) {
            string path = txt.Text.Trim();
            if (File.Exists(path))
                txt.BackColor = colorValido;
            else
                txt.BackColor = this.colorInvalido;
        }

        private string GetDroppedFileName(DragEventArgs e) {
            IDataObject data = e.Data;
            if (data == null)
                return null;
            var array = data.GetData(DataFormats.FileDrop) as Array;
            if (array == null || array.Length == 0)
                return null;
            object value = array.GetValue(0);
            if (value == null)
                return null;
            return value.ToString();
        }

        private void UpdateState() {
            this.VerificarButton.Enabled = this.PathCer.BackColor == System.Drawing.Color.MintCream 
                && this.PathKey.BackColor == System.Drawing.Color.MintCream 
                && !string.IsNullOrEmpty(this.Password.Text) 
                && !string.IsNullOrEmpty(this.PasswordC.Text);
        }
    }
}
