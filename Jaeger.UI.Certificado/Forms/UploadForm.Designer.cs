﻿namespace Jaeger.UI.Certificado.Forms {
    partial class UploadForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadForm));
            this.HeaderBox = new System.Windows.Forms.PictureBox();
            this.GroupInfo = new Telerik.WinControls.UI.RadGroupBox();
            this.Certificado = new Jaeger.UI.Certificado.Forms.CertificadoPathControl();
            this.Guardar = new Telerik.WinControls.UI.RadButton();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.GeneraPFX = new Telerik.WinControls.UI.RadButton();
            this.Header = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupInfo)).BeginInit();
            this.GroupInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneraPFX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderBox
            // 
            this.HeaderBox.BackColor = System.Drawing.Color.White;
            this.HeaderBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderBox.Location = new System.Drawing.Point(0, 0);
            this.HeaderBox.Name = "HeaderBox";
            this.HeaderBox.Size = new System.Drawing.Size(358, 32);
            this.HeaderBox.TabIndex = 13;
            this.HeaderBox.TabStop = false;
            // 
            // GroupInfo
            // 
            this.GroupInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupInfo.Controls.Add(this.Certificado);
            this.GroupInfo.HeaderText = "Datos del certificado";
            this.GroupInfo.Location = new System.Drawing.Point(0, 38);
            this.GroupInfo.Name = "GroupInfo";
            this.GroupInfo.Size = new System.Drawing.Size(356, 197);
            this.GroupInfo.TabIndex = 12;
            this.GroupInfo.TabStop = false;
            this.GroupInfo.Text = "Datos del certificado";
            // 
            // Certificado
            // 
            this.Certificado.Location = new System.Drawing.Point(8, 14);
            this.Certificado.Name = "Certificado";
            this.Certificado.Size = new System.Drawing.Size(336, 178);
            this.Certificado.TabIndex = 0;
            // 
            // Guardar
            // 
            this.Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Guardar.Enabled = false;
            this.Guardar.Location = new System.Drawing.Point(190, 245);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(75, 23);
            this.Guardar.TabIndex = 15;
            this.Guardar.Text = "Guardar";
            this.Guardar.Click += new System.EventHandler(this.TSaveButton_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cerrar.Location = new System.Drawing.Point(271, 245);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 16;
            this.Cerrar.Text = "Cerrar";
            // 
            // GeneraPFX
            // 
            this.GeneraPFX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GeneraPFX.Location = new System.Drawing.Point(43, 245);
            this.GeneraPFX.Name = "GeneraPFX";
            this.GeneraPFX.Size = new System.Drawing.Size(141, 23);
            this.GeneraPFX.TabIndex = 17;
            this.GeneraPFX.Text = "Generar archivo PFX";
            this.GeneraPFX.Click += new System.EventHandler(this.TGenerarPfxButton_Click);
            // 
            // Header
            // 
            this.Header.AutoSize = true;
            this.Header.BackColor = System.Drawing.Color.White;
            this.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Header.Location = new System.Drawing.Point(12, 11);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(109, 13);
            this.Header.TabIndex = 18;
            this.Header.Text = "Certificado eFirma";
            // 
            // FielUploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cerrar;
            this.ClientSize = new System.Drawing.Size(358, 280);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.GeneraPFX);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.HeaderBox);
            this.Controls.Add(this.GroupInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FielUploadForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = " | Cargar";
            this.Load += new System.EventHandler(this.UploadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.HeaderBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupInfo)).EndInit();
            this.GroupInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneraPFX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox HeaderBox;
        private Telerik.WinControls.UI.RadGroupBox GroupInfo;
        protected CertificadoPathControl Certificado;
        protected Telerik.WinControls.UI.RadButton Guardar;
        protected Telerik.WinControls.UI.RadButton Cerrar;
        protected Telerik.WinControls.UI.RadButton GeneraPFX;
        public System.Windows.Forms.Label Header;
    }
}