﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Certificado.Forms {
    public partial class UploadForm : RadForm {

        public UploadForm() {
            InitializeComponent();
        }

        private void UploadForm_Load(object sender, EventArgs e) {
            this.Certificado.VerificarButton.Click += TVerificarButton_Click;
            this.Cerrar.Click += TCloseButton_Click;
        }
        
        protected virtual void TVerificarButton_Click(object sender, EventArgs eventArgs) {
            using (var espera = new Common.Forms.Waiting1Form(this.TVerificar)) {
                espera.Text = "Verificando";
                espera.ShowDialog(this);
            }
        }

        protected virtual void TGenerarPfxButton_Click(object sender, EventArgs e) {

        }

        protected virtual void TSaveButton_Click(object sender, EventArgs e) {
            using (var espera = new Common.Forms.Waiting1Form(this.Salveable)) {
                espera.Text = "Almacenando";
                espera.ShowDialog(this);
            }
        }

        protected virtual void TCloseButton_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void TVerificar() {

        }

        protected virtual void Salveable() {

        }
    }
}
