﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Certificado.Forms.Fiel {
    public partial class CatalogoForm : RadForm {
        #region declaraciones
        protected System.Windows.Forms.Button _Cancel = new System.Windows.Forms.Button();
        #endregion

        public CatalogoForm() {
            InitializeComponent();
        }

        private void CatalogoForm_Load(object sender, EventArgs e) {
            this.TCertificado.Nuevo.Enabled = true;
            this.TCertificado.Editar.Enabled = true;
            this.TCertificado.Remover.Enabled = true;
            this.TCertificado.Editar.Visibility = ElementVisibility.Collapsed;
            this.TCertificado.Nuevo.Click += TCertificado_Nuevo_Click;
            this.TCertificado.Editar.Click += TCertificado_Editar_Click;
            this.TCertificado.Remover.Click += TCertificado_Remover_Click;
            this.TCertificado.Cerrar.Click += TCertificado_Cerrar_Click;
            this.TCertificado.Actualizar.Click += TCertificado_Actualizar_Click;
            
            this.CancelButton = this._Cancel;
            this._Cancel.Click += this.TCertificado_Cerrar_Click;
        }

        #region barra de herramientas
        protected virtual void TCertificado_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
        }

        protected virtual void TCertificado_Nuevo_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual void TCertificado_Editar_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual void TCertificado_Remover_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual void TCertificado_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        protected virtual void Consulta() {
         
        }
    }
}
