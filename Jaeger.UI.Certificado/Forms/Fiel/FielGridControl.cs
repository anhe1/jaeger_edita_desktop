﻿using System;
using Jaeger.UI.Certificado.Builder;

namespace Jaeger.UI.Certificado.Forms.Fiel {
    public class FielGridControl : CertificadoGridControl {
        public FielGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            if (this.GridData.Columns.Count == 0)
                using (IFielGridBuilder view = new FielGridBuilder()) {
                    this.GridData.Columns.AddRange(view.Templetes().Master().Build());
                }
        }
    }
}
