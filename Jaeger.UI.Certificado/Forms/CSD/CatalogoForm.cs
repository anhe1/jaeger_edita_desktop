﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Certificado.Forms.CSD {
    public partial class CatalogoForm : RadForm {
        #region declaraciones
        protected System.Windows.Forms.Button _Cancel = new System.Windows.Forms.Button();
        #endregion

        public CatalogoForm() {
            InitializeComponent();
        }

        private void CertificadosForm_Load(object sender, EventArgs e) {
            this.TCertificado.Nuevo.Enabled = true;
            this.TCertificado.Editar.Enabled = true;
            this.TCertificado.Remover.Enabled = true;
            this.TCertificado.Editar.Visibility = ElementVisibility.Collapsed;
            this.TCertificado.Nuevo.Click += Nuevo_Click;
            this.TCertificado.Editar.Click += Editar_Click;
            this.TCertificado.Remover.Click += Remover_Click;
            this.TCertificado.Cerrar.Click += Cerrar_Click;
            this.TCertificado.Actualizar.Click += Actualizar_Click;
            
            this.CancelButton = this._Cancel;
            this._Cancel.Click += this.Cerrar_Click;
        }

        #region barra de herramientas
        protected virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
        }

        protected virtual void Nuevo_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual void Editar_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual void Remover_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        protected virtual void Consulta() {
         
        }
    }
}
