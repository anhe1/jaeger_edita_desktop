﻿using System;
using Jaeger.UI.Certificado.Builder;

namespace Jaeger.UI.Certificado.Forms.CSD {
    public class CertificadoGridControl : Forms.CertificadoGridControl {
        public CertificadoGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            if (this.GridData.Columns.Count == 0)
                using (ICertificadoGridBuilder view = new CertificadoGridBuilder()) {
                    this.GridData.Columns.AddRange(view.Templetes().Master().Build());
                }
        }
    }
}
