﻿using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Certificado.Forms {
    /// <summary>
    /// 
    /// </summary>
    public partial class CertificadoForm : RadForm {
        #region declaraciones
        protected Color colorInvalido;
        protected Color colorValido;
        protected Color colorEditable;
        protected string _pathOpenSSL = @"D:\bitbucket\jaeger_edita_desktop\documentos";
        protected Domain.Base.Contracts.ICertificado Certificado;
        #endregion

        public CertificadoForm() {
            InitializeComponent();
        }

        private void FielForm_Load(object sender, EventArgs e) {
            this.colorInvalido = Color.Bisque;
            this.colorValido = Color.MintCream;
            this.colorEditable = Color.LemonChiffon;
            this.Certificate.VerificarButton.Click += this.TVerificarButton_Click;
            this.CloseButton.Click += CloseButton_Click;
        }

        protected virtual void TVerificarButton_Click(object sender, EventArgs eventArgs) {
            using (var espera = new Common.Forms.Waiting1Form(this.TVerificar)) {
                espera.Text = "Verificando";
                espera.ShowDialog(this);
            }
        }

        protected virtual void OpenSslButton_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.exe",
                DefaultExt = ".exe",
                Filter = "Archivo CER (*.exe)|*.exe",
                FilterIndex = 1,
                FileName = "openssl.exe"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.OpenSslLabel.Text = openFileDialog.FileName;
            }
        }

        protected virtual void PathPFXButton_Click(object sender, EventArgs e) {
            var openFolrderDialog = new FolderBrowserDialog();
            if (openFolrderDialog.ShowDialog(this) == DialogResult.OK) {
                this.PathFilePFX.Text = openFolrderDialog.SelectedPath;
            }
        }

        protected virtual void CloseButton_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region metodos privados
        protected virtual bool EsInformacionValida() {
            bool flag;
            
            if (this.Certificate.PathCer.Text == "") {
                MessageBox.Show("No ha seleccionado el archivo del certificado", "Sin certificado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                flag = false;
            } else if (File.Exists(this.Certificate.PathCer.Text)) {
                if (this.Information.Base64.Text == "") {
                    MessageBox.Show("El certificado se encuentra vacío", "Certificado vacío", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                if (this.Certificate.PathKey.Text != "") {
                    if (!File.Exists(this.Certificate.PathKey.Text)) {
                        MessageBox.Show("El archivo de llave privada no existe", "Archivo inválido", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    if (this.Certificate.Password.Text == "") {
                        MessageBox.Show("La contrseña no puede estar vacía", "Sin contraseña", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    } else if (this.Certificate.Password.Text != this.Certificate.PasswordC.Text) {
                        MessageBox.Show("La contraseña y su confirmación son inconsistentes", "Confirmación erronea", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    } else if (DateTime.Compare(DateTime.Now.Date, Convert.ToDateTime(this.Information.ValidoDesde.Text.Substring(0, 10))) < 0) {
                        MessageBox.Show("La vigencia del certificado no ha entrado en vigor", "Vigencia sin vigor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    } else if (DateTime.Compare(DateTime.Now.Date, Convert.ToDateTime(this.Information.ValidoHasta.Text.Substring(0, 10))) <= 0) {
                        flag = true;
                    } else {
                        MessageBox.Show("La vigencia del certificado ha expirado", "Vigencia expirada", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    }
                } else {
                    MessageBox.Show("No ha seleccionado el archivo de la llave privada", "Sin llave privada", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    flag = false;
                }
            } else {
                MessageBox.Show("El archivo de certificado seleccionado no existe", "Archivo inválido", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                flag = false;
            }
            return flag;
        }
        #endregion

        protected virtual void TVerificar() {

        }

        protected virtual void TBinding() {
            this.Information.RazonSocial.Text = this.Certificado.RazonSocial;
            this.Information.RFC.Text = this.Certificado.RFC;
            this.Information.Tipo.Text = this.Certificado.Tipo;
            this.Information.Serie.Text = this.Certificado.Serie;
            this.Information.ValidoDesde.Text = this.Certificado.InicioVigencia.Value.ToString("dd/MM/yyyy HH:mm:ss");
            this.Information.ValidoHasta.Text = this.Certificado.FinalVigencia.Value.ToString("dd/MM/yyyy HH:mm:ss");
            this.Information.Base64.Text = this.Certificado.FileCer;
        }
    }
}
