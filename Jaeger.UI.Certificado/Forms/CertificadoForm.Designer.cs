﻿namespace Jaeger.UI.Certificado.Forms {
    partial class CertificadoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CertificadoForm));
            this.Header = new Telerik.WinControls.UI.RadLabel();
            this.PathPfxButton = new Telerik.WinControls.UI.RadButton();
            this.OpenSslLabel = new Telerik.WinControls.UI.RadLabel();
            this.OpenSslButton = new Telerik.WinControls.UI.RadButton();
            this.CreatePfxButton = new Telerik.WinControls.UI.RadButton();
            this.PathFilePFX = new Telerik.WinControls.UI.RadTextBox();
            this.FilePfxLabel = new Telerik.WinControls.UI.RadLabel();
            this.GroupOptions = new Telerik.WinControls.UI.RadGroupBox();
            this.GroupPathCertificate = new Telerik.WinControls.UI.RadGroupBox();
            this.GroupInformation = new Telerik.WinControls.UI.RadGroupBox();
            this.HeaderBox = new System.Windows.Forms.PictureBox();
            this.CloseButton = new Telerik.WinControls.UI.RadButton();
            this.Certificate = new Jaeger.UI.Certificado.Forms.CertificadoPathControl();
            this.Information = new Jaeger.UI.Certificado.Forms.CertificadoInfoControl();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathPfxButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenSslLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenSslButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatePfxButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathFilePFX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FilePfxLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupOptions)).BeginInit();
            this.GroupOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupPathCertificate)).BeginInit();
            this.GroupPathCertificate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupInformation)).BeginInit();
            this.GroupInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CloseButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.White;
            this.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Header.Location = new System.Drawing.Point(38, 7);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(89, 16);
            this.Header.TabIndex = 12;
            this.Header.Text = "Certificado SAT";
            // 
            // PathPfxButton
            // 
            this.PathPfxButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PathPfxButton.Location = new System.Drawing.Point(316, 36);
            this.PathPfxButton.Name = "PathPfxButton";
            this.PathPfxButton.Size = new System.Drawing.Size(29, 20);
            this.PathPfxButton.TabIndex = 7;
            this.PathPfxButton.Text = "...";
            this.PathPfxButton.Click += new System.EventHandler(this.PathPFXButton_Click);
            // 
            // OpenSslLabel
            // 
            this.OpenSslLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenSslLabel.AutoSize = false;
            this.OpenSslLabel.Location = new System.Drawing.Point(12, 90);
            this.OpenSslLabel.Name = "OpenSslLabel";
            this.OpenSslLabel.Size = new System.Drawing.Size(332, 18);
            this.OpenSslLabel.TabIndex = 6;
            this.OpenSslLabel.Text = "ubucación openSSL";
            // 
            // OpenSslButton
            // 
            this.OpenSslButton.Location = new System.Drawing.Point(152, 62);
            this.OpenSslButton.Name = "OpenSslButton";
            this.OpenSslButton.Size = new System.Drawing.Size(75, 23);
            this.OpenSslButton.TabIndex = 5;
            this.OpenSslButton.Text = "OpenSSL";
            this.OpenSslButton.Click += new System.EventHandler(this.OpenSslButton_Click);
            // 
            // CreatePfxButton
            // 
            this.CreatePfxButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreatePfxButton.Location = new System.Drawing.Point(233, 62);
            this.CreatePfxButton.Name = "CreatePfxButton";
            this.CreatePfxButton.Size = new System.Drawing.Size(112, 23);
            this.CreatePfxButton.TabIndex = 1;
            this.CreatePfxButton.Text = "Generar archivo PFX";
            // 
            // PathFilePFX
            // 
            this.PathFilePFX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PathFilePFX.Location = new System.Drawing.Point(12, 36);
            this.PathFilePFX.Name = "PathFilePFX";
            this.PathFilePFX.Size = new System.Drawing.Size(298, 20);
            this.PathFilePFX.TabIndex = 0;
            // 
            // FilePfxLabel
            // 
            this.FilePfxLabel.Location = new System.Drawing.Point(12, 17);
            this.FilePfxLabel.Name = "FilePfxLabel";
            this.FilePfxLabel.Size = new System.Drawing.Size(93, 18);
            this.FilePfxLabel.TabIndex = 4;
            this.FilePfxLabel.Text = "Archivo PFX (.pfx)";
            // 
            // GroupOptions
            // 
            this.GroupOptions.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupOptions.Controls.Add(this.PathPfxButton);
            this.GroupOptions.Controls.Add(this.OpenSslLabel);
            this.GroupOptions.Controls.Add(this.OpenSslButton);
            this.GroupOptions.Controls.Add(this.CreatePfxButton);
            this.GroupOptions.Controls.Add(this.PathFilePFX);
            this.GroupOptions.Controls.Add(this.FilePfxLabel);
            this.GroupOptions.HeaderText = "Opciones";
            this.GroupOptions.Location = new System.Drawing.Point(0, 244);
            this.GroupOptions.Name = "GroupOptions";
            this.GroupOptions.Size = new System.Drawing.Size(356, 116);
            this.GroupOptions.TabIndex = 10;
            this.GroupOptions.TabStop = false;
            this.GroupOptions.Text = "Opciones";
            // 
            // GroupPathCertificate
            // 
            this.GroupPathCertificate.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupPathCertificate.Controls.Add(this.Certificate);
            this.GroupPathCertificate.HeaderText = "Datos del certificado";
            this.GroupPathCertificate.Location = new System.Drawing.Point(0, 41);
            this.GroupPathCertificate.Name = "GroupPathCertificate";
            this.GroupPathCertificate.Size = new System.Drawing.Size(356, 197);
            this.GroupPathCertificate.TabIndex = 9;
            this.GroupPathCertificate.TabStop = false;
            this.GroupPathCertificate.Text = "Datos del certificado";
            // 
            // GroupInformation
            // 
            this.GroupInformation.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupInformation.Controls.Add(this.Information);
            this.GroupInformation.HeaderText = "Información del certificado";
            this.GroupInformation.Location = new System.Drawing.Point(362, 41);
            this.GroupInformation.Name = "GroupInformation";
            this.GroupInformation.Size = new System.Drawing.Size(316, 319);
            this.GroupInformation.TabIndex = 13;
            this.GroupInformation.TabStop = false;
            this.GroupInformation.Text = "Información del certificado";
            // 
            // HeaderBox
            // 
            this.HeaderBox.BackColor = System.Drawing.Color.White;
            this.HeaderBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderBox.Location = new System.Drawing.Point(0, 0);
            this.HeaderBox.Name = "HeaderBox";
            this.HeaderBox.Size = new System.Drawing.Size(682, 32);
            this.HeaderBox.TabIndex = 11;
            this.HeaderBox.TabStop = false;
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(595, 370);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 14;
            this.CloseButton.Text = "Cerrar";
            // 
            // Certificate
            // 
            this.Certificate.Location = new System.Drawing.Point(8, 14);
            this.Certificate.Name = "Certificate";
            this.Certificate.Size = new System.Drawing.Size(336, 178);
            this.Certificate.TabIndex = 0;
            // 
            // Information
            // 
            this.Information.Location = new System.Drawing.Point(8, 14);
            this.Information.Name = "Information";
            this.Information.Size = new System.Drawing.Size(301, 297);
            this.Information.TabIndex = 0;
            // 
            // CertificadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(682, 405);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.GroupOptions);
            this.Controls.Add(this.HeaderBox);
            this.Controls.Add(this.GroupPathCertificate);
            this.Controls.Add(this.GroupInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CertificadoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = " | Certificado SAT";
            this.Load += new System.EventHandler(this.FielForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathPfxButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenSslLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenSslButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatePfxButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathFilePFX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FilePfxLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupOptions)).EndInit();
            this.GroupOptions.ResumeLayout(false);
            this.GroupOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupPathCertificate)).EndInit();
            this.GroupPathCertificate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupInformation)).EndInit();
            this.GroupInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HeaderBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CloseButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel Header;
        private Telerik.WinControls.UI.RadLabel FilePfxLabel;
        private System.Windows.Forms.PictureBox HeaderBox;
        private Telerik.WinControls.UI.RadGroupBox GroupPathCertificate;
        private Telerik.WinControls.UI.RadGroupBox GroupInformation;
        protected Telerik.WinControls.UI.RadGroupBox GroupOptions;
        protected CertificadoInfoControl Information;
        protected CertificadoPathControl Certificate;
        protected Telerik.WinControls.UI.RadLabel OpenSslLabel;
        protected Telerik.WinControls.UI.RadTextBox PathFilePFX;
        protected internal Telerik.WinControls.UI.RadButton PathPfxButton;
        protected internal Telerik.WinControls.UI.RadButton OpenSslButton;
        protected internal Telerik.WinControls.UI.RadButton CreatePfxButton;
        protected internal Telerik.WinControls.UI.RadButton CloseButton;
    }
}