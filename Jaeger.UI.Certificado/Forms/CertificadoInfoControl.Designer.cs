﻿namespace Jaeger.UI.Certificado.Forms {
    partial class CertificadoInfoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.RazonSocial = new Telerik.WinControls.UI.RadTextBox();
            this.Tipo = new Telerik.WinControls.UI.RadTextBox();
            this.lblRazonSocial = new Telerik.WinControls.UI.RadLabel();
            this.lblTipoCertificado = new Telerik.WinControls.UI.RadLabel();
            this.RFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.ValidoHasta = new Telerik.WinControls.UI.RadTextBox();
            this.lblValidoHasta = new Telerik.WinControls.UI.RadLabel();
            this.ValidoDesde = new Telerik.WinControls.UI.RadTextBox();
            this.lblValidoDesde = new Telerik.WinControls.UI.RadLabel();
            this.Serie = new Telerik.WinControls.UI.RadTextBox();
            this.lblNumeroSerie = new Telerik.WinControls.UI.RadLabel();
            this.Base64 = new Telerik.WinControls.UI.RadTextBox();
            this.lblB64 = new Telerik.WinControls.UI.RadLabel();
            this.providerError = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRazonSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCertificado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidoHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValidoHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidoDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValidoDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Base64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblB64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).BeginInit();
            this.SuspendLayout();
            // 
            // RazonSocial
            // 
            this.RazonSocial.BackColor = System.Drawing.SystemColors.Window;
            this.RazonSocial.Location = new System.Drawing.Point(3, 21);
            this.RazonSocial.Name = "RazonSocial";
            this.RazonSocial.ReadOnly = true;
            this.RazonSocial.Size = new System.Drawing.Size(295, 20);
            this.RazonSocial.TabIndex = 22;
            // 
            // Tipo
            // 
            this.Tipo.BackColor = System.Drawing.SystemColors.Window;
            this.Tipo.Location = new System.Drawing.Point(144, 59);
            this.Tipo.Name = "Tipo";
            this.Tipo.ReadOnly = true;
            this.Tipo.Size = new System.Drawing.Size(51, 20);
            this.Tipo.TabIndex = 23;
            // 
            // lblRazonSocial
            // 
            this.lblRazonSocial.Location = new System.Drawing.Point(3, 3);
            this.lblRazonSocial.Name = "lblRazonSocial";
            this.lblRazonSocial.Size = new System.Drawing.Size(69, 18);
            this.lblRazonSocial.TabIndex = 29;
            this.lblRazonSocial.Text = "Razón Social";
            // 
            // lblTipoCertificado
            // 
            this.lblTipoCertificado.Location = new System.Drawing.Point(144, 41);
            this.lblTipoCertificado.Name = "lblTipoCertificado";
            this.lblTipoCertificado.Size = new System.Drawing.Size(28, 18);
            this.lblTipoCertificado.TabIndex = 30;
            this.lblTipoCertificado.Text = "Tipo";
            // 
            // RFC
            // 
            this.RFC.BackColor = System.Drawing.SystemColors.Window;
            this.RFC.Location = new System.Drawing.Point(201, 59);
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            this.RFC.Size = new System.Drawing.Size(97, 20);
            this.RFC.TabIndex = 19;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(201, 41);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(25, 18);
            this.lblRFC.TabIndex = 28;
            this.lblRFC.Text = "RFC";
            // 
            // ValidoHasta
            // 
            this.ValidoHasta.BackColor = System.Drawing.SystemColors.Window;
            this.ValidoHasta.Location = new System.Drawing.Point(153, 97);
            this.ValidoHasta.Name = "ValidoHasta";
            this.ValidoHasta.ReadOnly = true;
            this.ValidoHasta.Size = new System.Drawing.Size(145, 20);
            this.ValidoHasta.TabIndex = 21;
            this.ValidoHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblValidoHasta
            // 
            this.lblValidoHasta.Location = new System.Drawing.Point(153, 79);
            this.lblValidoHasta.Name = "lblValidoHasta";
            this.lblValidoHasta.Size = new System.Drawing.Size(70, 18);
            this.lblValidoHasta.TabIndex = 27;
            this.lblValidoHasta.Text = "Válido hasta:";
            // 
            // ValidoDesde
            // 
            this.ValidoDesde.BackColor = System.Drawing.SystemColors.Window;
            this.ValidoDesde.Location = new System.Drawing.Point(3, 97);
            this.ValidoDesde.Name = "ValidoDesde";
            this.ValidoDesde.ReadOnly = true;
            this.ValidoDesde.Size = new System.Drawing.Size(145, 20);
            this.ValidoDesde.TabIndex = 20;
            this.ValidoDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblValidoDesde
            // 
            this.lblValidoDesde.Location = new System.Drawing.Point(3, 79);
            this.lblValidoDesde.Name = "lblValidoDesde";
            this.lblValidoDesde.Size = new System.Drawing.Size(73, 18);
            this.lblValidoDesde.TabIndex = 26;
            this.lblValidoDesde.Text = "Válido desde:";
            // 
            // Serie
            // 
            this.Serie.BackColor = System.Drawing.SystemColors.Window;
            this.Serie.Location = new System.Drawing.Point(3, 59);
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            this.Serie.Size = new System.Drawing.Size(135, 20);
            this.Serie.TabIndex = 18;
            this.Serie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNumeroSerie
            // 
            this.lblNumeroSerie.Location = new System.Drawing.Point(3, 41);
            this.lblNumeroSerie.Name = "lblNumeroSerie";
            this.lblNumeroSerie.Size = new System.Drawing.Size(89, 18);
            this.lblNumeroSerie.TabIndex = 24;
            this.lblNumeroSerie.Text = "Número de serie";
            // 
            // Base64
            // 
            this.Base64.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Base64.AutoSize = false;
            this.Base64.BackColor = System.Drawing.SystemColors.Window;
            this.Base64.Location = new System.Drawing.Point(3, 135);
            this.Base64.Multiline = true;
            this.Base64.Name = "Base64";
            this.Base64.ReadOnly = true;
            this.Base64.Size = new System.Drawing.Size(295, 163);
            this.Base64.TabIndex = 17;
            // 
            // lblB64
            // 
            this.lblB64.Location = new System.Drawing.Point(3, 117);
            this.lblB64.Name = "lblB64";
            this.lblB64.Size = new System.Drawing.Size(102, 18);
            this.lblB64.TabIndex = 25;
            this.lblB64.Text = "Certificado Base 64";
            // 
            // providerError
            // 
            this.providerError.ContainerControl = this;
            // 
            // CertificadoInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RazonSocial);
            this.Controls.Add(this.Tipo);
            this.Controls.Add(this.lblRazonSocial);
            this.Controls.Add(this.lblTipoCertificado);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.lblRFC);
            this.Controls.Add(this.ValidoHasta);
            this.Controls.Add(this.lblValidoHasta);
            this.Controls.Add(this.ValidoDesde);
            this.Controls.Add(this.lblValidoDesde);
            this.Controls.Add(this.Serie);
            this.Controls.Add(this.lblNumeroSerie);
            this.Controls.Add(this.Base64);
            this.Controls.Add(this.lblB64);
            this.Name = "CertificadoInfoControl";
            this.Size = new System.Drawing.Size(301, 301);
            this.Load += new System.EventHandler(this.CertificadoInfoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRazonSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCertificado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidoHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValidoHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidoDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValidoDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Base64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblB64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Telerik.WinControls.UI.RadTextBox RazonSocial;
        public Telerik.WinControls.UI.RadTextBox Tipo;
        private Telerik.WinControls.UI.RadLabel lblRazonSocial;
        private Telerik.WinControls.UI.RadLabel lblTipoCertificado;
        public Telerik.WinControls.UI.RadTextBox RFC;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        public Telerik.WinControls.UI.RadTextBox ValidoHasta;
        private Telerik.WinControls.UI.RadLabel lblValidoHasta;
        public Telerik.WinControls.UI.RadTextBox ValidoDesde;
        private Telerik.WinControls.UI.RadLabel lblValidoDesde;
        public Telerik.WinControls.UI.RadTextBox Serie;
        private Telerik.WinControls.UI.RadLabel lblNumeroSerie;
        public Telerik.WinControls.UI.RadTextBox Base64;
        public Telerik.WinControls.UI.RadLabel lblB64;
        private System.Windows.Forms.ErrorProvider providerError;
    }
}
