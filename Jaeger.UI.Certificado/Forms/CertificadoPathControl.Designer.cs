﻿namespace Jaeger.UI.Certificado.Forms {
    partial class CertificadoPathControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.VerificarButton = new Telerik.WinControls.UI.RadButton();
            this.PasswordC = new Telerik.WinControls.UI.RadTextBox();
            this.lblPassword2 = new Telerik.WinControls.UI.RadLabel();
            this.CheckPassword = new Telerik.WinControls.UI.RadCheckBox();
            this.PathKeyButton = new Telerik.WinControls.UI.RadButton();
            this.PathCerButton = new Telerik.WinControls.UI.RadButton();
            this.PathCer = new Telerik.WinControls.UI.RadTextBox();
            this.Password = new Telerik.WinControls.UI.RadTextBox();
            this.lblCertificado = new Telerik.WinControls.UI.RadLabel();
            this.PathKey = new Telerik.WinControls.UI.RadTextBox();
            this.lblClavePrivada = new Telerik.WinControls.UI.RadLabel();
            this.lblPassword1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.VerificarButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPassword2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathKeyButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathCerButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathCer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCertificado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathKey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClavePrivada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPassword1)).BeginInit();
            this.SuspendLayout();
            // 
            // VerificarButton
            // 
            this.VerificarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.VerificarButton.Enabled = false;
            this.VerificarButton.Location = new System.Drawing.Point(221, 153);
            this.VerificarButton.Name = "VerificarButton";
            this.VerificarButton.Size = new System.Drawing.Size(100, 20);
            this.VerificarButton.TabIndex = 19;
            this.VerificarButton.Text = "Verificar";
            // 
            // PasswordC
            // 
            this.PasswordC.Location = new System.Drawing.Point(3, 153);
            this.PasswordC.Name = "PasswordC";
            this.PasswordC.PasswordChar = '*';
            this.PasswordC.Size = new System.Drawing.Size(213, 20);
            this.PasswordC.TabIndex = 17;
            // 
            // lblPassword2
            // 
            this.lblPassword2.Location = new System.Drawing.Point(3, 132);
            this.lblPassword2.Name = "lblPassword2";
            this.lblPassword2.Size = new System.Drawing.Size(238, 18);
            this.lblPassword2.TabIndex = 18;
            this.lblPassword2.Text = "Confirmación de contraseña de clave privada:*";
            // 
            // CheckPassword
            // 
            this.CheckPassword.Location = new System.Drawing.Point(222, 109);
            this.CheckPassword.Name = "CheckPassword";
            this.CheckPassword.Size = new System.Drawing.Size(89, 18);
            this.CheckPassword.TabIndex = 16;
            this.CheckPassword.Text = "ver caracteres";
            // 
            // PathKeyButton
            // 
            this.PathKeyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PathKeyButton.Location = new System.Drawing.Point(292, 65);
            this.PathKeyButton.Name = "PathKeyButton";
            this.PathKeyButton.Size = new System.Drawing.Size(29, 20);
            this.PathKeyButton.TabIndex = 14;
            this.PathKeyButton.Text = "...";
            // 
            // PathCerButton
            // 
            this.PathCerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PathCerButton.Location = new System.Drawing.Point(292, 21);
            this.PathCerButton.Name = "PathCerButton";
            this.PathCerButton.Size = new System.Drawing.Size(29, 20);
            this.PathCerButton.TabIndex = 10;
            this.PathCerButton.Text = "...";
            // 
            // PathCer
            // 
            this.PathCer.AllowDrop = true;
            this.PathCer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PathCer.Location = new System.Drawing.Point(3, 21);
            this.PathCer.Name = "PathCer";
            this.PathCer.Size = new System.Drawing.Size(283, 20);
            this.PathCer.TabIndex = 8;
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(3, 109);
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(213, 20);
            this.Password.TabIndex = 15;
            // 
            // lblCertificado
            // 
            this.lblCertificado.Location = new System.Drawing.Point(3, 3);
            this.lblCertificado.Name = "lblCertificado";
            this.lblCertificado.Size = new System.Drawing.Size(90, 18);
            this.lblCertificado.TabIndex = 9;
            this.lblCertificado.Text = "Certificado (.cer):";
            // 
            // PathKey
            // 
            this.PathKey.AllowDrop = true;
            this.PathKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PathKey.Location = new System.Drawing.Point(3, 65);
            this.PathKey.Name = "PathKey";
            this.PathKey.Size = new System.Drawing.Size(283, 20);
            this.PathKey.TabIndex = 13;
            // 
            // lblClavePrivada
            // 
            this.lblClavePrivada.Location = new System.Drawing.Point(3, 44);
            this.lblClavePrivada.Name = "lblClavePrivada";
            this.lblClavePrivada.Size = new System.Drawing.Size(105, 18);
            this.lblClavePrivada.TabIndex = 11;
            this.lblClavePrivada.Text = "Clave privada (.key):";
            // 
            // lblPassword1
            // 
            this.lblPassword1.Location = new System.Drawing.Point(3, 88);
            this.lblPassword1.Name = "lblPassword1";
            this.lblPassword1.Size = new System.Drawing.Size(154, 18);
            this.lblPassword1.TabIndex = 12;
            this.lblPassword1.Text = "Contraseña de clave privada:*";
            // 
            // CertificadoPathControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.VerificarButton);
            this.Controls.Add(this.PasswordC);
            this.Controls.Add(this.lblPassword2);
            this.Controls.Add(this.CheckPassword);
            this.Controls.Add(this.PathKeyButton);
            this.Controls.Add(this.PathCerButton);
            this.Controls.Add(this.PathCer);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.lblCertificado);
            this.Controls.Add(this.PathKey);
            this.Controls.Add(this.lblClavePrivada);
            this.Controls.Add(this.lblPassword1);
            this.Name = "CertificadoPathControl";
            this.Size = new System.Drawing.Size(325, 178);
            this.Load += new System.EventHandler(this.CertificadoPathControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VerificarButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPassword2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathKeyButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathCerButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathCer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCertificado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PathKey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClavePrivada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPassword1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadTextBox PasswordC;
        private Telerik.WinControls.UI.RadLabel lblPassword2;
        public Telerik.WinControls.UI.RadCheckBox CheckPassword;
        public Telerik.WinControls.UI.RadTextBox PathCer;
        public Telerik.WinControls.UI.RadTextBox Password;
        private Telerik.WinControls.UI.RadLabel lblCertificado;
        public Telerik.WinControls.UI.RadTextBox PathKey;
        private Telerik.WinControls.UI.RadLabel lblClavePrivada;
        private Telerik.WinControls.UI.RadLabel lblPassword1;
        public Telerik.WinControls.UI.RadButton VerificarButton;
        public Telerik.WinControls.UI.RadButton PathKeyButton;
        public Telerik.WinControls.UI.RadButton PathCerButton;
    }
}
