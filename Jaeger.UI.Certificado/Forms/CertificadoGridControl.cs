﻿namespace Jaeger.UI.Certificado.Forms {
    public class CertificadoGridControl : Common.Forms.GridStandarControl {
        public CertificadoGridControl() : base() {
            this.GridData.KeyDown += TGridData_KeyDown;
        }

        private void TGridData_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
            if (e.KeyCode == System.Windows.Forms.Keys.F2) {
                if (this.Editar.Enabled == true) {
                    this.Editar.PerformClick();
                }
            } else if (e.KeyCode == System.Windows.Forms.Keys.F3) {
                if (this.Nuevo.Enabled) {
                    this.Nuevo.PerformClick();
                }
            } else if (e.KeyCode == System.Windows.Forms.Keys.F4) {
                if (this.Remover.Enabled == true) {
                    this.Remover.PerformClick();
                }
            } else {
                if (e.KeyCode == System.Windows.Forms.Keys.F5) {
                    this.Actualizar.PerformClick();
                }
            }
        }
    }
}
