﻿namespace Jaeger.UI.Forms {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ComboBoxInterface = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxRegion = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.TextBoxAWSAccessKeyId = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBoxAWSSecretAccessKey = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TextBoxUploadBucketName = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.ComboBoxStorageClass = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.CheckBoxUploadMakePublic = new System.Windows.Forms.CheckBox();
            this.TextBoxUploadContentType = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.ButtonUploadBrowse = new System.Windows.Forms.Button();
            this.TextBoxUploadKeyName = new System.Windows.Forms.TextBox();
            this.TextBoxUploadFileName = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.ButtonUploadFile = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(717, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 400);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(717, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "Listo.";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.GroupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 376);
            this.panel1.TabIndex = 2;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(3, 249);
            this.textBox5.MaxLength = 1032767;
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox5.Size = new System.Drawing.Size(242, 122);
            this.textBox5.TabIndex = 27;
            this.textBox5.WordWrap = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ComboBoxInterface);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(3, 180);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 63);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // ComboBoxInterface
            // 
            this.ComboBoxInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxInterface.FormattingEnabled = true;
            this.ComboBoxInterface.Items.AddRange(new object[] {
            "SimpleStorageServiceAWS3",
            "SoftAWS"});
            this.ComboBoxInterface.Location = new System.Drawing.Point(6, 35);
            this.ComboBoxInterface.Name = "ComboBoxInterface";
            this.ComboBoxInterface.Size = new System.Drawing.Size(230, 21);
            this.ComboBoxInterface.TabIndex = 3;
            this.ComboBoxInterface.SelectedIndexChanged += new System.EventHandler(this.ComboBoxInterface_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Interface";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.label6);
            this.GroupBox1.Controls.Add(this.TextBoxRegion);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.TextBoxAWSAccessKeyId);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.TextBoxAWSSecretAccessKey);
            this.GroupBox1.Location = new System.Drawing.Point(3, 7);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(242, 167);
            this.GroupBox1.TabIndex = 26;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Global Settings (Enter your values below)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Region";
            // 
            // TextBoxRegion
            // 
            this.TextBoxRegion.Location = new System.Drawing.Point(6, 138);
            this.TextBoxRegion.Name = "TextBoxRegion";
            this.TextBoxRegion.Size = new System.Drawing.Size(230, 20);
            this.TextBoxRegion.TabIndex = 24;
            this.TextBoxRegion.Text = "us-east-1";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(3, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(91, 13);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "SecretAccessKey";
            // 
            // TextBoxAWSAccessKeyId
            // 
            this.TextBoxAWSAccessKeyId.Location = new System.Drawing.Point(6, 40);
            this.TextBoxAWSAccessKeyId.Name = "TextBoxAWSAccessKeyId";
            this.TextBoxAWSAccessKeyId.Size = new System.Drawing.Size(230, 20);
            this.TextBoxAWSAccessKeyId.TabIndex = 16;
            this.TextBoxAWSAccessKeyId.Text = "AKIAIF5K3IMRBYEVYGIQ";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(3, 24);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(69, 13);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "AccessKeyId";
            // 
            // TextBoxAWSSecretAccessKey
            // 
            this.TextBoxAWSSecretAccessKey.Location = new System.Drawing.Point(6, 90);
            this.TextBoxAWSSecretAccessKey.Name = "TextBoxAWSSecretAccessKey";
            this.TextBoxAWSSecretAccessKey.Size = new System.Drawing.Size(230, 20);
            this.TextBoxAWSSecretAccessKey.TabIndex = 18;
            this.TextBoxAWSSecretAccessKey.Text = "MttLLerFIcLfurUS06xXGBJyE4AIy9vOKidDO8Ix";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.TextBoxUploadBucketName);
            this.tabPage1.Controls.Add(this.Label8);
            this.tabPage1.Controls.Add(this.ComboBoxStorageClass);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Label22);
            this.tabPage1.Controls.Add(this.Label21);
            this.tabPage1.Controls.Add(this.CheckBoxUploadMakePublic);
            this.tabPage1.Controls.Add(this.TextBoxUploadContentType);
            this.tabPage1.Controls.Add(this.Label20);
            this.tabPage1.Controls.Add(this.Label18);
            this.tabPage1.Controls.Add(this.ButtonUploadBrowse);
            this.tabPage1.Controls.Add(this.TextBoxUploadKeyName);
            this.tabPage1.Controls.Add(this.TextBoxUploadFileName);
            this.tabPage1.Controls.Add(this.Label9);
            this.tabPage1.Controls.Add(this.Label7);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.ButtonUploadFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(459, 350);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Upload";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadBucketName
            // 
            this.TextBoxUploadBucketName.Location = new System.Drawing.Point(105, 72);
            this.TextBoxUploadBucketName.Name = "TextBoxUploadBucketName";
            this.TextBoxUploadBucketName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadBucketName.TabIndex = 69;
            this.TextBoxUploadBucketName.Text = "nme610911l71/correo";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(16, 75);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(72, 13);
            this.Label8.TabIndex = 70;
            this.Label8.Text = "Bucket Name";
            // 
            // ComboBoxStorageClass
            // 
            this.ComboBoxStorageClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxStorageClass.FormattingEnabled = true;
            this.ComboBoxStorageClass.Items.AddRange(new object[] {
            "STANDARD",
            "REDUCED_REDUNDANCY"});
            this.ComboBoxStorageClass.Location = new System.Drawing.Point(105, 178);
            this.ComboBoxStorageClass.Name = "ComboBoxStorageClass";
            this.ComboBoxStorageClass.Size = new System.Drawing.Size(163, 21);
            this.ComboBoxStorageClass.TabIndex = 68;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 67;
            this.label4.Text = "Storage Class ";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(358, 127);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(52, 13);
            this.Label22.TabIndex = 66;
            this.Label22.Text = "(Optional)";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(16, 154);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(66, 13);
            this.Label21.TabIndex = 64;
            this.Label21.Text = "Make Public";
            // 
            // CheckBoxUploadMakePublic
            // 
            this.CheckBoxUploadMakePublic.AutoSize = true;
            this.CheckBoxUploadMakePublic.Checked = true;
            this.CheckBoxUploadMakePublic.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxUploadMakePublic.Location = new System.Drawing.Point(105, 155);
            this.CheckBoxUploadMakePublic.Name = "CheckBoxUploadMakePublic";
            this.CheckBoxUploadMakePublic.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxUploadMakePublic.TabIndex = 63;
            this.CheckBoxUploadMakePublic.UseVisualStyleBackColor = true;
            // 
            // TextBoxUploadContentType
            // 
            this.TextBoxUploadContentType.Location = new System.Drawing.Point(105, 124);
            this.TextBoxUploadContentType.Name = "TextBoxUploadContentType";
            this.TextBoxUploadContentType.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadContentType.TabIndex = 62;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(16, 127);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(71, 13);
            this.Label20.TabIndex = 61;
            this.Label20.Text = "Content-Type";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(16, 16);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(186, 13);
            this.Label18.TabIndex = 60;
            this.Label18.Text = "Use the following form to upload a file.";
            // 
            // ButtonUploadBrowse
            // 
            this.ButtonUploadBrowse.Location = new System.Drawing.Point(361, 44);
            this.ButtonUploadBrowse.Name = "ButtonUploadBrowse";
            this.ButtonUploadBrowse.Size = new System.Drawing.Size(75, 23);
            this.ButtonUploadBrowse.TabIndex = 54;
            this.ButtonUploadBrowse.Text = "Browse";
            this.ButtonUploadBrowse.UseVisualStyleBackColor = true;
            this.ButtonUploadBrowse.Click += new System.EventHandler(this.ButtonUploadBrowse_Click);
            // 
            // TextBoxUploadKeyName
            // 
            this.TextBoxUploadKeyName.Location = new System.Drawing.Point(105, 98);
            this.TextBoxUploadKeyName.Name = "TextBoxUploadKeyName";
            this.TextBoxUploadKeyName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadKeyName.TabIndex = 56;
            // 
            // TextBoxUploadFileName
            // 
            this.TextBoxUploadFileName.Location = new System.Drawing.Point(105, 46);
            this.TextBoxUploadFileName.Name = "TextBoxUploadFileName";
            this.TextBoxUploadFileName.Size = new System.Drawing.Size(250, 20);
            this.TextBoxUploadFileName.TabIndex = 53;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(16, 101);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(56, 13);
            this.Label9.TabIndex = 59;
            this.Label9.Text = "Key Name";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(16, 49);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(83, 13);
            this.Label7.TabIndex = 57;
            this.Label7.Text = "Local File Name";
            // 
            // ButtonUploadFile
            // 
            this.ButtonUploadFile.Location = new System.Drawing.Point(105, 212);
            this.ButtonUploadFile.Name = "ButtonUploadFile";
            this.ButtonUploadFile.Size = new System.Drawing.Size(96, 23);
            this.ButtonUploadFile.TabIndex = 65;
            this.ButtonUploadFile.Text = "Upload File";
            this.ButtonUploadFile.UseVisualStyleBackColor = true;
            this.ButtonUploadFile.Click += new System.EventHandler(this.ButtonUploadFile_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(250, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(467, 376);
            this.tabControl1.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(19, 282);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(355, 51);
            this.textBox1.TabIndex = 71;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(380, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 51);
            this.button1.TabIndex = 65;
            this.button1.Text = "Copy";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 422);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Soft S3 All Operations for C# (Tester)";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox TextBoxAWSAccessKeyId;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBoxAWSSecretAccessKey;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxInterface;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        internal System.Windows.Forms.ComboBox ComboBoxStorageClass;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.CheckBox CheckBoxUploadMakePublic;
        internal System.Windows.Forms.TextBox TextBoxUploadContentType;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Button ButtonUploadBrowse;
        internal System.Windows.Forms.TextBox TextBoxUploadKeyName;
        internal System.Windows.Forms.TextBox TextBoxUploadFileName;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button ButtonUploadFile;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox TextBoxRegion;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        internal System.Windows.Forms.TextBox TextBoxUploadBucketName;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox textBox5;
        internal System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.Button button1;
    }
}