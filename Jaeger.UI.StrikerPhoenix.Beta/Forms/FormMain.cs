﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class FormMain : Form {
        protected Amazon.S3.Contracts.ISimpleStorageService editaS3 = null;

        public FormMain() {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.ComboBoxInterface.SelectedIndex = 0;
        }

        #region Upload

        private void ButtonUploadBrowse_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog();
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            this.TextBoxUploadFileName.Text = openFile.FileName;
            this.TextBoxUploadKeyName.Text = Path.GetFileName(this.TextBoxUploadFileName.Text);
        }

        private void ButtonUploadFile_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.UpLoad)) {
                espera.ShowDialog(this);
            }
        }

        private void UpLoad() {
            this.textBox1.Text = editaS3.Upload(this.TextBoxUploadFileName.Text, this.TextBoxUploadKeyName.Text, this.TextBoxUploadContentType.Text, this.TextBoxUploadBucketName.Text,this.TextBoxRegion.Text, this.CheckBoxUploadMakePublic.Checked, Amazon.S3.ValueObjects.StorageClassTypeEnum.Standar);
        }

        #endregion

        #region configuracion y servicio

        private void CreateService() {
            if (this.ComboBoxInterface.Text == this.ComboBoxInterface.Items[0].ToString()) {
                editaS3 = this.GetSimpleStorageServiceAWS3();
            }
        }

        private Amazon.S3.Helpers.SimpleStorageServiceAWS3 GetSimpleStorageServiceAWS3() {
            var d = this.Config();
            return new Amazon.S3.Helpers.SimpleStorageServiceAWS3(d.AccessKeyId, d.SecretAccessKey, d.Region);
        }

        private Amazon.S3.Settings Config() {
            return new Amazon.S3.Settings { AccessKeyId = this.TextBoxAWSAccessKeyId.Text, SecretAccessKey = this.TextBoxAWSSecretAccessKey.Text, Region = this.TextBoxRegion.Text };
        }

        #endregion

        private void ComboBoxInterface_SelectedIndexChanged(object sender, EventArgs e) {
            this.CreateService();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e) {
            Clipboard.SetText(this.textBox1.Text);
        }
    }
}
