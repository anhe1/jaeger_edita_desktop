﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Shatterdome.Entities;

namespace Jaeger.Domain.Shatterdome.Contracts {
    public interface ISqlShatterdomeRepository : IGenericRepository<ShatterdomeModel> {
        new IEnumerable<ShatterdomeDetailModel> GetList();

        /// <summary>
        /// obtener la configuración de la empresa por su rFC
        /// </summary>
        ShatterdomeModel GetByRFC(string rfc);

        /// <summary>
        /// desactivar empresa
        /// </summary>
        ShatterdomeModel Delete(ShatterdomeModel model);

        ShatterdomeModel Save(ShatterdomeModel model);

        /// <summary>
        /// prueba de conexion a base de datos
        /// </summary>
        bool TestConfig(DataBaseConfiguracion shatterdome);
    }
}
