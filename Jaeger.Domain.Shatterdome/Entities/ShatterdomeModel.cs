﻿/// develop: anhe
/// Shatterdomes es la sede principal del Cuerpo de Defensa Pan Pacific y fábricas para la construcción, reparación y mantenimiento de los Jaeger
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Domain.Shatterdome.Entities {
    /// <summary>
    /// configuracion de empresa
    /// </summary>
    [SugarTable("empr")]
    public class ShatterdomeModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int regimenFiscal;
        private string clave;
        private string creo;
        private string modifica;
        private string rfc;
        private string dominio;
        private string nombre;
        private string correo;
        private string representanteLega;
        private string data;
        private string direccionFiscal;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        #endregion

        public ShatterdomeModel() {
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// indice principal de la tabla
        /// </summary>
        [DataNames("empr_id")]
        [SugarColumn(ColumnName = "empr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro activo
        /// </summary>
        [DataNames("empr_a")]
        [SugarColumn(ColumnName = "empr_a", ColumnDescription = "registro activo")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_rgf_id")]
        [SugarColumn(ColumnName = "empr_rgf_id", ColumnDescription = "id del regimen fiscal")]
        public int RegimenFiscal {
            get {
                return this.regimenFiscal;
            }
            set {
                this.regimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_clv")]
        [SugarColumn(ColumnName = "empr_clv", ColumnDescription = "clave de la empresa")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_usr_n")]
        [SugarColumn(ColumnName = "empr_usr_n", ColumnDescription = "clave del usuario que crea el registro")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_usr_m")]
        [SugarColumn(ColumnName = "empr_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro")]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_rfc")]
        [SugarColumn(ColumnName = "empr_rfc", ColumnDescription = "clave del registro federal de contribuyentes")]
        public string RFC {
            get {
                return this.rfc;
            }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_dom")]
        [SugarColumn(ColumnName = "empr_dom", ColumnDescription = "dominio asigando a la empresa")]
        public string Dominio {
            get {
                return this.dominio;
            }
            set {
                this.dominio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_mail")]
        [SugarColumn(ColumnName = "empr_mail", ColumnDescription = "correo electronico de contacto")]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o denominacion social de la empresa registrada
        /// </summary>
        [DataNames("empr_nom")]
        [SugarColumn(ColumnName = "empr_nom", ColumnDescription = "nombre o denominacion social de la empresa registrada")]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_rep")]
        [SugarColumn(ColumnName = "empr_rep", ColumnDescription = "nombre del representante legal", Length = 255)]
        public string RepresentanteLegal {
            get {
                return this.representanteLega;
            }
            set {
                this.representanteLega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la configuracion general de la empresa.
        /// </summary>
        [DataNames("empr_conf")]
        [SugarColumn(ColumnName = "empr_conf", ColumnDataType = "TEXT", ColumnDescription = "configuracion general de la empresa.", IsNullable = true)]
        public string Data {
            get {
                return this.data;
            }
            set {
                this.data = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_direc")]
        [SugarColumn(ColumnName = "empr_direc", ColumnDataType = "TEXT", ColumnDescription = "direccion fiscal", IsNullable = true)]
        public string DireccionFiscal {
            get {
                return this.direccionFiscal;
            }
            set {
                this.direccionFiscal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_fn")]
        [SugarColumn(ColumnName = "empr_fn")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("empr_fm")]
        [SugarColumn(ColumnName = "empr_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica > firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public SynapsisModel Synapsis {
            get {
                return SynapsisModel.Json(this.Data);
            }
        }
    }
}
