﻿/// develop: anhe
/// Shatterdomes es la sede principal del Cuerpo de Defensa Pan Pacific y fábricas para la construcción, reparación y mantenimiento de los Jaeger
using Newtonsoft.Json;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Domain.Shatterdome.Entities {
    /// <summary>
    /// clase para la configuracion local de empresas
    /// </summary>
    [JsonObject("shatterdome")]
    public class ShatterdomeSynapsis {

        /// <summary>
        /// configuracion json
        /// </summary>
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        public ShatterdomeSynapsis() {
            this.Configuracion = new DataBaseConfiguracion();
            this.Bucket = new ShatterdomeBuckets();
        }

        [JsonProperty("configuracion")]
        public DataBaseConfiguracion Configuracion {
            get; set;
        }

        [JsonProperty("bucket")]
        public ShatterdomeBuckets Bucket {
            get; set;
        }

        public string Json() {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
