﻿/// develop: anhe
/// Shatterdomes es la sede principal del Cuerpo de Defensa Pan Pacific y fábricas para la construcción, reparación y mantenimiento de los Jaeger
using Newtonsoft.Json;
using System.ComponentModel;

namespace Jaeger.Domain.Shatterdome.Entities {
    public class ShatterdomeBuckets {

        [DisplayName("AccessKeyId"), Description("")]
        [JsonProperty("accessKeyId")]
        public string AccessKeyId {
            get; set;
        }

        [DisplayName("Region"), Description("")]
        [JsonProperty("region")]
        public string Region {
            get; set;
        }

        [DisplayName("SecretAccessKey"), Description("")]
        [JsonProperty("secretAccessKey")]
        public string SecretAccessKey {
            get; set;
        }
    }
}
