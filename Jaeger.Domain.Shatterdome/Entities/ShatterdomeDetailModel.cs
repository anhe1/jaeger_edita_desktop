﻿/// develop: anhe
/// Shatterdomes es la sede principal del Cuerpo de Defensa Pan Pacific y fábricas para la construcción, reparación y mantenimiento de los Jaeger

using SqlSugar;

namespace Jaeger.Domain.Shatterdome.Entities {
    /// <summary>
    /// configuracion de empresa
    /// </summary>
    [SugarTable("empr")]
    public class ShatterdomeDetailModel : ShatterdomeModel {
        public ShatterdomeDetailModel() : base() {

        }
    }
}
