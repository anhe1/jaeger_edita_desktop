﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using Google;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Newtonsoft.Json;
using Jaeger.Util.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.JGoogle {
    public class GoogleSheetsService : GoogleAplicacionServicio {
        private string urlSpreadSheet;
        private SheetsService servicio;

        private readonly string[] arrayScopes =
        {
            SheetsService.Scope.SpreadsheetsReadonly,
            SheetsService.Scope.Spreadsheets,
            SheetsService.Scope.DriveFile,
            SheetsService.Scope.Drive,
            SheetsService.Scope.DriveReadonly
        };


        public GoogleSheetsService() {
            if (!File.Exists(this.GoogleSecretJson)) {
                var recurso = new EmbeddedResources("Jaeger.Aplication.JGoogle");
                recurso.GetResource("Jaeger.Aplication.JGoogle.Resources." + this.SecretoCliente, ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Google, this.SecretoCliente));
            }

            this.GoogleServicesAuthorization(this.arrayScopes, ".credentials\\sheets.googleapis.com-dotnet-quickstart.json");
            BaseClientService.Initializer initializer = new BaseClientService.Initializer() {
                HttpClientInitializer = this.Credential,
                ApplicationName = this.ApplicationName
            };
            this.servicio = new SheetsService(initializer);
        }

        public SheetsService Servicio {
            get {
                return this.servicio;
            }
            set {
                this.servicio = value;
            }
        }

        public string UrlSpreadSheet {
            get {
                return this.urlSpreadSheet;
            }
            set {
                this.urlSpreadSheet = value;
            }
        }

        /// <summary>
        /// obtener información 
        /// </summary>
        public GoogleWorkbooks GetSpreadSheetInfo() {
            try {
                GoogleWorkbooks libro = new GoogleWorkbooks();
                IEnumerator<Sheet> enumerator = null;
                SpreadsheetsResource.GetRequest request = this.servicio.Spreadsheets.Get(this.GetSpreadsheetId());
                request.IncludeGridData = false;
                Spreadsheet response = request.Execute();
                libro.Titulo = response.Properties.Title;
                enumerator = response.Sheets.GetEnumerator();
                while (enumerator.MoveNext()) {
                    libro.Hojas.Add(new GoogleWorksheet { Title = enumerator.Current.Properties.Title, SheetId = enumerator.Current.Properties.SheetId });
                }
                libro.UrlSpreadSheet = this.urlSpreadSheet;
                return libro;
            }
            catch (Exception) {
                return null;
            }
        }

        public void Update(string range, DataTable tabla) {
            ValueRange requestBody = new ValueRange() {
                Values = GoogleSheetsService.TableToIList(tabla),
                MajorDimension = "ROWS"
            };

            SpreadsheetsResource.ValuesResource.UpdateRequest request = this.servicio.Spreadsheets.Values.Update(requestBody, this.GetSpreadsheetId(), range);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
            UpdateValuesResponse response = new UpdateValuesResponse();
            try {
                response = request.Execute();
            }
            catch (GoogleApiException ex) {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine(JsonConvert.SerializeObject(response, Formatting.Indented));
        }

        public UpdateValuesResponse UpdateR(string range, DataTable tabla) {
            ValueRange requestBody = new ValueRange() {
                Values = GoogleSheetsService.TableToIList(tabla),
                MajorDimension = "ROWS"
            };

            SpreadsheetsResource.ValuesResource.UpdateRequest request = this.servicio.Spreadsheets.Values.Update(requestBody, this.GetSpreadsheetId(), range);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
            UpdateValuesResponse response = new UpdateValuesResponse();
            try {
                response = request.Execute();
            }
            catch (GoogleApiException ex) {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine(JsonConvert.SerializeObject(response, Formatting.Indented));
            return response;
        }

        public ValueRange GetRange(string range) {
            SpreadsheetsResource.ValuesResource.GetRequest request = this.servicio.Spreadsheets.Values.Get(this.GetSpreadsheetId(), range);
            request.ValueRenderOption = SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum.FORMATTEDVALUE;
            request.DateTimeRenderOption = SpreadsheetsResource.ValuesResource.GetRequest.DateTimeRenderOptionEnum.FORMATTEDSTRING;
            request.MajorDimension = SpreadsheetsResource.ValuesResource.GetRequest.MajorDimensionEnum.ROWS;
            ValueRange response = request.Execute();
            FileService.WriteFileText("C:\\Jaeger\\Jaeger.Temporal\\NominaTesting.txt", JsonConvert.SerializeObject(response, Formatting.Indented));
            return response;
        }

        public BatchGetValuesResponse GetRanges(string ranges) {
            string[] e = ranges.Split(';');
            List<string> lista = new List<string>(e);
            SpreadsheetsResource.ValuesResource.BatchGetRequest.ValueRenderOptionEnum valueRenderOption = SpreadsheetsResource.ValuesResource.BatchGetRequest.ValueRenderOptionEnum.FORMATTEDVALUE;
            SpreadsheetsResource.ValuesResource.BatchGetRequest.DateTimeRenderOptionEnum dateTimeRenderOption = SpreadsheetsResource.ValuesResource.BatchGetRequest.DateTimeRenderOptionEnum.FORMATTEDSTRING;
            SpreadsheetsResource.ValuesResource.BatchGetRequest request = this.servicio.Spreadsheets.Values.BatchGet(this.GetSpreadsheetId());
            request.Ranges = lista;
            request.ValueRenderOption = valueRenderOption;
            request.DateTimeRenderOption = dateTimeRenderOption;
            request.MajorDimension = SpreadsheetsResource.ValuesResource.BatchGetRequest.MajorDimensionEnum.ROWS;
            BatchGetValuesResponse response = request.Execute();
            FileService.WriteFileText("C:\\Jaeger\\Jaeger.Temporal\\Testing.txt", JsonConvert.SerializeObject(response, Formatting.Indented));
            return response;
        }

        #region funciones

        public string GetSpreadsheetId() {
            return GoogleSheetsService.GetSpreadsheetId(this.urlSpreadSheet);
        }

        /// <summary>
        /// extraer la ID de hoja de cálculo de una URL de Hojas de cálculo.
        /// </summary>
        public static string GetSpreadsheetId(string url) {
            Match match = Regex.Match(url, @"\/spreadsheets\/d\/([a-zA-Z0-9-_]+)", RegexOptions.Multiline);
            if (match.Success) {
                return match.Groups[1].Value;
            }
            return string.Empty;
        }

        public string GetSheetId() {
            return GoogleSheetsService.GetSheetId(this.urlSpreadSheet);
        }

        /// <summary>
        /// La identificación de la hoja es numérica y la siguiente expresión regular se puede usar para extraerla de una URL de Hojas de cálculo de Google
        /// </summary>
        public static string GetSheetId(string url) {
            Match match = Regex.Match(url, @"[#&]gid=([0-9]+)", RegexOptions.Multiline);
            if (match.Success) {
                return match.Groups[1].Value;
            }
            return string.Empty;
        }

        public static IList<IList<object>> TableToIList(DataTable datos) {
            IList<IList<object>> result = new List<IList<object>>();
            foreach (DataRow fila in datos.Rows) {
                List<object> item = new List<object>();
                foreach (DataColumn col in fila.Table.Columns) {
                    // en caso de que sea tipo DateTime
                    if (fila[col].GetType().Equals(typeof(DateTime))) {
                        DateTime dateTime = Convert.ToDateTime(fila[col]);
                        item.Add(dateTime.ToShortDateString());
                    }
                    else {
                        item.Add(fila[col]);
                    }
                }
                result.Add(item);
            }
            return result;
        }

        #endregion
    }
}
