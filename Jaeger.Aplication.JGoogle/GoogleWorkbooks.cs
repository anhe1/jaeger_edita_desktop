﻿using System.ComponentModel;

namespace Jaeger.Aplication.JGoogle {
    public class GoogleWorkbooks {
        private string titleField;
        private string urlSpreadSheet;
        private BindingList<GoogleWorksheet> hojasField;

        /// <summary>
        /// contructor
        /// </summary>
        public GoogleWorkbooks() {
            this.hojasField = new BindingList<GoogleWorksheet>();
        }

        public string UrlSpreadSheet {
            get {
                return this.urlSpreadSheet;
            }
            set {
                this.urlSpreadSheet = value;
            }
        }

        public string Titulo {
            get {
                return this.titleField;
            }
            set {
                this.titleField = value;
            }
        }

        public BindingList<GoogleWorksheet> Hojas {
            get {
                return this.hojasField;
            }
            set {
                this.hojasField = value;
            }
        }
    }
}
