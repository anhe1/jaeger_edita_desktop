﻿using System;
using System.IO;
using System.Collections.Generic;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Util.Services;

namespace Jaeger.Aplication.JGoogle {
    public interface IGoogleCalendarService {
        void CrearEvento(string titulo, string descripcion, string localizacion, DateTime start, DateTime end);
    }

    public class GoogleCalendarService : GoogleAplicacionServicio, IGoogleCalendarService {
        protected CalendarService servicio;
        private readonly string[] arrayScopes = { CalendarService.Scope.Calendar };
        private readonly string zoneTime = "America/Mexico_City";

        public GoogleCalendarService(string googleSecretJsonFilePath) {
            this.GoogleSecretJson = googleSecretJsonFilePath;
            this.SecretoCliente = "calendar-dotnet-quickstart.json";
            if (!File.Exists(this.GoogleSecretJson)) {
                var recurso = new EmbeddedResources("jaeger_titan_redeemer");
                recurso.GetResource("Jaeger.Aplication.Resources." + this.SecretoCliente, ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Google, this.SecretoCliente));
            }

            this.GoogleServicesAuthorization(this.arrayScopes, ".credentials/calendar-dotnet-quickstart.json");
            this.servicio = new CalendarService(new BaseClientService.Initializer() {
                HttpClientInitializer = this.Credential,
                ApplicationName = ApplicationName,
            });
        }

        public CalendarService Servicio {
            get {
                return this.servicio;
            }
            set {
                this.servicio = value;
            }
        }

        public void CrearEvento(string titulo, string descripcion, string localizacion, DateTime start, DateTime end) {
            List<EventAttendee> attendees = new List<EventAttendee>();

            Event newEvent = new Event() {
                Summary = titulo,
                Location = localizacion,
                Description = descripcion,
                Start = new EventDateTime() {
                    DateTime = start,
                    TimeZone = this.zoneTime
                },
                End = new EventDateTime() {
                    DateTime = end,
                    TimeZone = this.zoneTime
                },
                //Recurrence = new String[] { "RRULE:FREQ=WEEKLY;UNTIL=20190213T200000Z" },
                Attendees = new EventAttendee[] {
                    new EventAttendee() { Email = ConfigService.Piloto.Correo }
                },
                Reminders = new Event.RemindersData() {
                    UseDefault = false,
                    Overrides = new EventReminder[] {
                        new EventReminder() { Method = "popup", Minutes = 10 },
                        new EventReminder() { Method = "email", Minutes = 10 },
                        new EventReminder() { Method = "email", Minutes = (60 * 24 * 1) },
                        new EventReminder() { Method = "email", Minutes = (60 * 24 * 2) },
                        new EventReminder() { Method = "email", Minutes = (60 * 24 * 3) }
                    }
                }
            };
            string calendarId = "primary";
            EventsResource.InsertRequest request = this.servicio.Events.Insert(newEvent, calendarId);
            Event createdEvent = request.Execute();
            Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);
        }

        public void Calendarios() {
            IList<CalendarListEntry> lista = this.servicio.CalendarList.List().Execute().Items;
            foreach (CalendarListEntry item in lista) {
                Console.WriteLine(item.Summary + ". Location: " + item.Location + ", TimeZone: " + item.TimeZone);
            }
        }

        public void Testing() {
            Event newEvent = new Event() {
                Summary = "Prueba en calendario con recurrencia",
                Location = "Calle 6 No. 208 Agricola Pantitlan",
                Description = "A chance to hear more about Google's developer products.",
                Start = new EventDateTime() {
                    DateTime = DateTime.Parse("2019-02-13T19:30:00-07:00"),
                    TimeZone = this.zoneTime
                },
                End = new EventDateTime() {
                    DateTime = DateTime.Parse("2019-02-13T19:40:00-07:00"),
                    TimeZone = this.zoneTime
                },
                Recurrence = new String[] { "RRULE:FREQ=WEEKLY;UNTIL=20190213T200000Z" },
                Attendees = new EventAttendee[] {
                    new EventAttendee() { Email = "sistemas.anhe@ipo.com.mx" },
                    new EventAttendee() { Email = "adm.dir@ipo.com.mx" },
                },
                Reminders = new Event.RemindersData() {
                    UseDefault = false,
                    Overrides = new EventReminder[] {
                        new EventReminder() { Method = "popup", Minutes = 5 },
                        new EventReminder() { Method = "email", Minutes = 5 }
                    }
                }
            };
            string calendarId = "primary";
            EventsResource.InsertRequest request = this.servicio.Events.Insert(newEvent, calendarId);
            Event createdEvent = request.Execute();
            Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);
        }

        public void Testing2() {
            // Define parameters of request.
            EventsResource.ListRequest request = this.servicio.Events.List("primary");
            //request.TimeMin = DateTime.Now;
            request.ShowDeleted = true;
            request.SingleEvents = true;
            //request.MaxResults = 10;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;
            // List events.
            Events events = request.Execute();
            Console.WriteLine("Upcoming events:");
            if (events.Items != null && events.Items.Count > 0) {
                foreach (var eventItem in events.Items) {
                    string when = eventItem.Start.DateTime.ToString();
                    if (String.IsNullOrEmpty(when)) {
                        when = eventItem.Start.Date;
                    }
                    Console.WriteLine("{0} ({1})", eventItem.Summary, when);
                }
            }
            else {
                Console.WriteLine("No upcoming events found.");
            }
            Console.Read();
        }
    }
}
