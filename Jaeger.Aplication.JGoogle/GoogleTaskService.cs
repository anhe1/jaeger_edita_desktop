﻿using System;
using Google.Apis.Tasks.v1;
using Google.Apis.Services;
using Google.Apis.Tasks.v1.Data;

namespace Jaeger.Aplication.JGoogle {
    public class GoogleTaskService : GoogleAplicacionServicio {
        private string tasklistIdentifier = "@default";
        private TasksService servicio;
        private readonly string[] arrayScopes =
        {
            TasksService.Scope.Tasks
        };

        public GoogleTaskService(string googleSecretJsonFilePath) {
            this.GoogleSecretJson = googleSecretJsonFilePath;
            this.GoogleServicesAuthorization(this.arrayScopes, ".credentials/tasks-dotnet-quickstart.json");
            this.servicio = new TasksService(new BaseClientService.Initializer() {
                HttpClientInitializer = this.Credential,
                ApplicationName = ApplicationName,
            });
            this.tasklistIdentifier = "@default";
        }

        public string TasklistIdentifier {
            get {
                return this.tasklistIdentifier;
            }
            set {
                this.tasklistIdentifier = value;
            }
        }

        public TasksService Servicio {
            get {
                return this.servicio;
            }
            set {
                this.servicio = value;
            }
        }

        public string Insert(string titulo, string nota) {
            Google.Apis.Tasks.v1.Data.Task task = new Google.Apis.Tasks.v1.Data.Task { Title = titulo };
            task.Notes = nota;
            task.Due = DateTime.Now.ToShortDateString();
            Google.Apis.Tasks.v1.TasksResource.InsertRequest result = this.servicio.Tasks.Insert(task, this.tasklistIdentifier);
            result.Execute();
            return task.Id;
        }

        public void ListTaskLists() {
            Console.WriteLine("\tTask lists:");
            var list = this.servicio.Tasklists.List().Execute();
            foreach (var item in list.Items) {
                Console.WriteLine("\t\t" + item.Title);
                Tasks tasks = this.servicio.Tasks.List(item.Id).Execute();
                if (tasks.Items != null) {
                    foreach (Task t in tasks.Items) {
                        Console.WriteLine("\t\t\t" + t.Title);
                    }
                }
            }
        }
        public void Testing() {
            Google.Apis.Tasks.v1.Data.Task task = new Google.Apis.Tasks.v1.Data.Task { Title = "Tarea, probando API" };
            task.Notes = "Please complete me";
            task.Due = DateTime.Now.ToShortDateString();
            Google.Apis.Tasks.v1.TasksResource.InsertRequest result = this.servicio.Tasks.Insert(task, this.tasklistIdentifier);
            result.Execute();
        }
    }
}
