﻿/// develop: anhe 190620181926
/// purpose:
using System;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.JGoogle {
    public class GoogleAplicacionServicio {
        private string applicationName = "gipsydanger";
        private string secretoCliente = "secreto_cliente.json";
        private string filePathGoogleSecretJson;
        private UserCredential credential;


        public GoogleAplicacionServicio() {
            this.filePathGoogleSecretJson = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Google), "secreto_cliente.json");
        }

        public string ApplicationName {
            get {
                return this.applicationName;
            }
            set {
                this.applicationName = value;
            }
        }

        public string SecretoCliente {
            get {
                return this.secretoCliente;
            }
            set {
                this.secretoCliente = value;
            }
        }

        public string GoogleSecretJson {
            get {
                return this.filePathGoogleSecretJson;
            }
            set {
                this.filePathGoogleSecretJson = value;
            }
        }

        public UserCredential Credential {
            get {
                return this.credential;
            }
            set {
                this.credential = value;
            }
        }

        /// <summary>
        /// crear autorización para el servicio
        /// </summary>
        /// <param name="arrayScopes">Scopes</param>
        /// <param name="quickstart">Ruta para almacenar credenciales del proyecto.</param>
        public void GoogleServicesAuthorization(string[] arrayScopes, string quickstart) {
            using (FileStream stream = new FileStream(this.filePathGoogleSecretJson, FileMode.Open, FileAccess.Read)) {
                string credPath = ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Google, quickstart);
                FileDataStore objeto = new FileDataStore(credPath, true);
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, arrayScopes, "user", CancellationToken.None, objeto, null).Result;
            }
        }

        public void RevokeAuthorizationToken() {
            if (this.credential != null) {
                this.credential.RevokeTokenAsync(CancellationToken.None);
            }
        }
    }
}
