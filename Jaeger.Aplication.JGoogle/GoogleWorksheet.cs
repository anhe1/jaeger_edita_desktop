﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Aplication.JGoogle {
    public partial class GoogleWorksheet : BasePropertyChangeImplementation {
        private int? indexField;
        private string sheetTitleField;
        private int? sheetIdField;
        private string tagField;

        /// <summary>
        /// constructor
        /// </summary>
        public GoogleWorksheet() {

        }

        public int? Index {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
                this.OnPropertyChanged();
            }
        }

        public string Title {
            get {
                return this.sheetTitleField;
            }
            set {
                this.sheetTitleField = value;
                this.OnPropertyChanged();
            }
        }

        public int? SheetId {
            get {
                return this.sheetIdField;
            }
            set {
                this.sheetIdField = value;
                this.OnPropertyChanged();
            }
        }

        public string Tag {
            get {
                return this.tagField;
            }
            set {
                this.tagField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
