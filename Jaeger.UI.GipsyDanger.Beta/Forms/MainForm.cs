﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Common.Services;
using System.Windows.Forms;
using System.Reflection;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            
            
            this.Text = Aplication.Base.ConfigService.Titulo();
            this.ButtonEmpresa.Text = Aplication.Base.ConfigService.Synapsis.Empresa.RFC;
            this.ButtonEmpresa.DropDownItems.Add(Aplication.Base.ConfigService.Synapsis.RDS.Edita.Database);
            this.ButtonEmpresa.DropDownItems.Add(Aplication.Base.ConfigService.Piloto.Clave);
            
        }

        private void m_Archivo_Salir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void MenuItem_Click(object sender, EventArgs e) {
            var item = (ToolStripMenuItem)sender;
            var localAssembly = Assembly.Load("Jaeger.UI.GipsyDanger.Beta");

            if (item.Tag != null) {
                try {
                    var d = localAssembly.GetTypes();
                    Type type = localAssembly.GetType("Jaeger.UI." + item.Tag.ToString(), true);
                    this.Form_Active(type);
                } catch (Exception ex) {
                    MessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            } else {
                MessageBox.Show(this, "No se implemento el formulario requerido.", "Jaeger.UI", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Form_Active(Type type) {
            this.menuStrip1.SuspendLayout();
            Form form = (Form)Activator.CreateInstance(type);
            form.MdiParent = this;
            form.Size = this.Size;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
            this.menuStrip1.Visible = false;
            this.menuStrip1.Visible = true;
            this.menuStrip1.ResumeLayout();
        }

        #region metodos privados
        public List<UIMenuElement> Menus() {
            var response = new List<UIMenuElement>();
            response.Add(new UIMenuElement { Name = "m_Archivo", Label = "Archivo", Rol = "*" });
            response.Add(new UIMenuElement { Name = "m_Archivo_Salir", Label = "Salir", Rol = "*" });

            response.Add(new UIMenuElement { Name = "m_Admon", Label = "Administración", Rol = "Administrador" });
            response.Add(new UIMenuElement { Name = "m_Admon_Clientes", Label = "Clientes", Rol = "Administrador" });
            response.Add(new UIMenuElement { Name = "m_Admon_Clientes_Expediente", Label = "Expediente", Rol = "Administrador" });
            response.Add(new UIMenuElement { Name = "m_Admon_Clientes_Remisionado", Label = "Remisionado", Rol = "Administrador" });

            response.Add(new UIMenuElement { Name = "m_Admon_Tesoreria", Label = "Tesorería y Bancos", Rol = "Administrador" });
            response.Add(new UIMenuElement { Name = "m_Admon_Tesoreria_Bancos", Label = "Cuentas Bancarias", Rol = "Administrador" });
            response.Add(new UIMenuElement { Name = "m_Admon_Tesoreria_Movimientos", Label = "Movimientos Bancarios", Rol = "Administrador" });
            response.Add(new UIMenuElement { Name = "m_Ventana", Label = "Ventana", Rol = "*" });
            response.Add(new UIMenuElement { Name = "m_Ayuda", Label = "Ayuda", Rol = "*" });
            return response;
        }
        #endregion
    }
}
