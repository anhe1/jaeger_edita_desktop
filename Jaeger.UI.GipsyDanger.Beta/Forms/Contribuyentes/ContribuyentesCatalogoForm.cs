﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Aplication.Contribuyentes;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Contribuyentes {
    public partial class ContribuyentesCatalogoForm : Form {
        protected IContribuyenteService service;
        protected TipoRelacionComericalEnum relacionComericalEnum;
        private BindingList<ContribuyenteDomicilioSingleModel> contribuyenteDomicilio;

        public ContribuyentesCatalogoForm() {
            InitializeComponent();
        }

        /// <summary>
        /// tipo de relacion 1 = Cliente, 2 = Provedor
        /// </summary>
        public ContribuyentesCatalogoForm(int relacionComericalEnum) {
            InitializeComponent();
            this.relacionComericalEnum = (TipoRelacionComericalEnum)relacionComericalEnum;
        }

        /// <summary>
        /// tipo de relacion 1 = Cliente, 2 = Provedor
        /// </summary>
        public ContribuyentesCatalogoForm(TipoRelacionComericalEnum relacionComericalEnum) {
            InitializeComponent();
            this.relacionComericalEnum = (TipoRelacionComericalEnum)relacionComericalEnum;
        }

        private void ContribuyentesCatalogoForm_Load(object sender, EventArgs e) {
            this.GridData.DataGridCommon();
            this.service = new ContribuyenteService(this.relacionComericalEnum);
        }

        #region barra de herramientas
        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.contribuyenteDomicilio;
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ContribuyenteForm(null)) {
                nuevo.Text = "Nuevo";
                nuevo.ShowDialog(this);
            }
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                using (var espera = new WaitingForm(this.Editar)) {
                    espera.Text = "Consultando información ...";
                    espera.ShowDialog(this);
                }
                if (this.Tag != null) {
                    var seleccionado = this.Tag as ContribuyenteDetailModel;
                    if (seleccionado != null) {
                        using (var editar = new ContribuyenteForm(seleccionado)) {
                            editar.ShowDialog(this);
                        }
                    }
                }
            }
        }

        private void ToolBarButtonRemover_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                if (MessageBox.Show(this, Properties.Resources.Pregunta_RemoveRegistro, "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {

                }
            }
        }

        #endregion

        private void Consultar() {
          //  this.contribuyenteDomicilio = new BindingList<ContribuyenteDomicilioSingleModel>(this.service.GetContribuyenteDomicilio().ToList());
        }

        private void Editar() {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ContribuyenteDomicilioSingleModel;
                //if (seleccionado != null)
                //    this.Tag = this.service.GetById(seleccionado.Id);
            }
        }
    }
}
