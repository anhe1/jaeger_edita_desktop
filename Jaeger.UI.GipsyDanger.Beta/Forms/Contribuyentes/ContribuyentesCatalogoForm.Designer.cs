﻿
namespace Jaeger.UI.Forms.Contribuyentes {
    partial class ContribuyentesCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarSearchControl();
            this.GridData = new System.Windows.Forms.DataGridView();
            this.Clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Calle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoExterior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Buscar:";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonNuevo_Click);
            this.ToolBar.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonEditar_Click);
            this.ToolBar.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonRemover_Click);
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonActualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonCerrar_Click);
            // 
            // GridData
            // 
            this.GridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Clave,
            this.RFC,
            this.Nombre,
            this.Telefono,
            this.Calle,
            this.NoExterior});
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 25);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(800, 425);
            this.GridData.TabIndex = 1;
            // 
            // Clave
            // 
            this.Clave.DataPropertyName = "Clave";
            this.Clave.HeaderText = "Clave";
            this.Clave.Name = "Clave";
            // 
            // RFC
            // 
            this.RFC.DataPropertyName = "RFC";
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            // 
            // Nombre
            // 
            this.Nombre.DataPropertyName = "Nombre";
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            // 
            // Telefono
            // 
            this.Telefono.DataPropertyName = "Telefono";
            this.Telefono.HeaderText = "Telefono";
            this.Telefono.Name = "Telefono";
            // 
            // Calle
            // 
            this.Calle.DataPropertyName = "Calle";
            this.Calle.HeaderText = "Calle";
            this.Calle.Name = "Calle";
            // 
            // NoExterior
            // 
            this.NoExterior.DataPropertyName = "NoExterior";
            this.NoExterior.HeaderText = "NoExterior";
            this.NoExterior.Name = "NoExterior";
            // 
            // ContribuyentesCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.ToolBar);
            this.Name = "ContribuyentesCatalogoForm";
            this.Text = "ContribuyentesCatalogoForm";
            this.Load += new System.EventHandler(this.ContribuyentesCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarSearchControl ToolBar;
        private System.Windows.Forms.DataGridView GridData;
        private System.Windows.Forms.DataGridViewTextBoxColumn Clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn Calle;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoExterior;
    }
}