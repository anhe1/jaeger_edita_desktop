﻿
namespace Jaeger.UI.Forms.Remisionado {
    partial class RemisionadoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.NoOrdenCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmisorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorRFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceptorNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveFormaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEntrega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCobranza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaUltPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCancela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoIVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PorCobrar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Acumulado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vendedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Creo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modifica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCancelar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowPeriodo = true;
            this.ToolBar.Size = new System.Drawing.Size(1267, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonActualizar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NoOrdenCliente,
            this.Folio,
            this.Serie,
            this.Status,
            this.EmisorRFC,
            this.ReceptorRFC,
            this.ReceptorNombre,
            this.IdDocumento,
            this.ClaveFormaPago,
            this.FechaEmision,
            this.FechaEntrega,
            this.FechaCobranza,
            this.FechaUltPago,
            this.FechaCancela,
            this.SubTotal,
            this.Descuento,
            this.TrasladoIVA,
            this.Total,
            this.PorCobrar,
            this.Acumulado,
            this.Vendedor,
            this.Contacto,
            this.Creo,
            this.Modifica});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1267, 425);
            this.dataGridView1.TabIndex = 1;
            // 
            // NoOrdenCliente
            // 
            this.NoOrdenCliente.DataPropertyName = "NoOrdenCliente";
            this.NoOrdenCliente.HeaderText = "Núm. Pedido";
            this.NoOrdenCliente.Name = "NoOrdenCliente";
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "Folio";
            dataGridViewCellStyle1.Format = "000000#";
            this.Folio.DefaultCellStyle = dataGridViewCellStyle1;
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            // 
            // Serie
            // 
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            // 
            // EmisorRFC
            // 
            this.EmisorRFC.DataPropertyName = "EmisorRFC";
            this.EmisorRFC.HeaderText = "Emisor (RFC)";
            this.EmisorRFC.Name = "EmisorRFC";
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DataPropertyName = "ReceptorRFC";
            this.ReceptorRFC.HeaderText = "RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            // 
            // ReceptorNombre
            // 
            this.ReceptorNombre.DataPropertyName = "ReceptorNombre";
            this.ReceptorNombre.HeaderText = "Receptor";
            this.ReceptorNombre.Name = "ReceptorNombre";
            // 
            // IdDocumento
            // 
            this.IdDocumento.DataPropertyName = "IdDocumento";
            this.IdDocumento.HeaderText = "IdDocumento";
            this.IdDocumento.Name = "IdDocumento";
            // 
            // ClaveFormaPago
            // 
            this.ClaveFormaPago.DataPropertyName = "ClaveFormaPago";
            this.ClaveFormaPago.HeaderText = "F. Pago";
            this.ClaveFormaPago.Name = "ClaveFormaPago";
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            this.FechaEmision.HeaderText = "Fec. Emisión";
            this.FechaEmision.Name = "FechaEmision";
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.DataPropertyName = "FechaEntrega";
            this.FechaEntrega.HeaderText = "Fec. Entrega";
            this.FechaEntrega.Name = "FechaEntrega";
            // 
            // FechaCobranza
            // 
            this.FechaCobranza.DataPropertyName = "FechaCobranza";
            this.FechaCobranza.HeaderText = "Fec. Cobranza";
            this.FechaCobranza.Name = "FechaCobranza";
            // 
            // FechaUltPago
            // 
            this.FechaUltPago.DataPropertyName = "FechaUltPago";
            this.FechaUltPago.HeaderText = "Fec. Ult. Pago";
            this.FechaUltPago.Name = "FechaUltPago";
            // 
            // FechaCancela
            // 
            this.FechaCancela.DataPropertyName = "FechaCancela";
            this.FechaCancela.HeaderText = "Fec. Cancela";
            this.FechaCancela.Name = "FechaCancela";
            // 
            // SubTotal
            // 
            this.SubTotal.DataPropertyName = "SubTotal";
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            // 
            // Descuento
            // 
            this.Descuento.DataPropertyName = "Descuento";
            this.Descuento.HeaderText = "Descuento";
            this.Descuento.Name = "Descuento";
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.DataPropertyName = "TrasladoIVA";
            this.TrasladoIVA.HeaderText = "TrasladoIVA";
            this.TrasladoIVA.Name = "TrasladoIVA";
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            // 
            // PorCobrar
            // 
            this.PorCobrar.DataPropertyName = "PorCobrar";
            this.PorCobrar.HeaderText = "Por Cobrar";
            this.PorCobrar.Name = "PorCobrar";
            // 
            // Acumulado
            // 
            this.Acumulado.DataPropertyName = "Acumulado";
            this.Acumulado.HeaderText = "Acumulado";
            this.Acumulado.Name = "Acumulado";
            // 
            // Vendedor
            // 
            this.Vendedor.DataPropertyName = "Vendedor";
            this.Vendedor.HeaderText = "Vendedor";
            this.Vendedor.Name = "Vendedor";
            // 
            // Contacto
            // 
            this.Contacto.DataPropertyName = "Contacto";
            this.Contacto.HeaderText = "Contacto";
            this.Contacto.Name = "Contacto";
            // 
            // Creo
            // 
            this.Creo.DataPropertyName = "Creo";
            this.Creo.HeaderText = "Creo";
            this.Creo.Name = "Creo";
            // 
            // Modifica
            // 
            this.Modifica.DataPropertyName = "Modifica";
            this.Modifica.HeaderText = "Modifica";
            this.Modifica.Name = "Modifica";
            // 
            // RemisionadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ToolBar);
            this.Name = "RemisionadoForm";
            this.Text = "RemisionadoForm";
            this.Load += new System.EventHandler(this.RemisionadoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBar1CommonControl ToolBar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOrdenCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmisorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorRFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceptorNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveFormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEntrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCobranza;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaUltPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCancela;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoIVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn PorCobrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Acumulado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vendedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modifica;
    }
}