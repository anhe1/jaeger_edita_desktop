﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Remisionado {
    public partial class RemisionadoForm : Form {
        protected Aplication.Almacen.PT.Contracts.IRemisionadoService service;
        protected Jaeger.Beta.Aplication.IRemisiondoCPService remisiondoCPService;

        private BindingList<Jaeger.Domain.Almacen.PT.Entities.RemisionSingleModel> remisiones;
        public RemisionadoForm() {
            InitializeComponent();
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            this.dataGridView1.DataGridCommon();
            
            this.remisiondoCPService = new Jaeger.Beta.Aplication.RemisiondoCPService();

            var CrearTablas = new ToolStripMenuItem() { Text = "Crear tablas" };
            CrearTablas.Click += CrearTablas_Click;
            var ButtonImportar = new ToolStripMenuItem() { Text = "Importar" };
            ButtonImportar.Click += ButtonImportar_Click;
            this.ToolBar.Herramientas.DropDownItems.AddRange(new ToolStripItem[] { CrearTablas, ButtonImportar });
        }

        private void CrearTablas_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, Properties.Resources.Message_Crear_Tablas, "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new WaitingForm(this.CrearTabla)) {
                    espera.Text = "Creando tablas ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ButtonImportar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Importar)) {
                espera.Text = "Importando ...";
                espera.ShowDialog(this);
            }
            ToolBar_ButtonActualizar_Click(sender, e);
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.dataGridView1.DataSource = this.remisiones;
        }

        private void Consultar() {
            //this.remisiones = this.service.GetList(this.ToolBar.GetEjercicio(), this.ToolBar.GetMes());
        }

        private void Importar() {
            this.remisiondoCPService.Importar(this.ToolBar.GetEjercicio(), this.ToolBar.GetMes());
        }

        private void CrearTabla() {
         //   this.service.CrearTablas();
        }
    }
}
