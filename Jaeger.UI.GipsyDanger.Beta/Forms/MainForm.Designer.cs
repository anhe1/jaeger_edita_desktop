﻿namespace Jaeger.UI.Forms {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.m_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Archivo_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Clientes = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Clientes_Expediente = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Clientes_OrdenCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Clientes_Remisionado = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Clientes_Comprobantes = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Tesoreria = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Tesoreria_Bancos = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Tesoreria_Movimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Contabilidad = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Almacen = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_AlmacenMP = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_AlmacenPT = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_AlmacenPT_Remisionado = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_Proveedor = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Admon_RecursosHumanos = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ButtonEmpresa = new System.Windows.Forms.ToolStripDropDownButton();
            this.m_Ventana = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Archivo,
            this.m_Admon,
            this.m_Ventana});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.m_Ventana;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1058, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // m_Archivo
            // 
            this.m_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Archivo_Salir});
            this.m_Archivo.Name = "m_Archivo";
            this.m_Archivo.Size = new System.Drawing.Size(76, 20);
            this.m_Archivo.Text = "m_Archivo";
            // 
            // m_Archivo_Salir
            // 
            this.m_Archivo_Salir.Name = "m_Archivo_Salir";
            this.m_Archivo_Salir.Size = new System.Drawing.Size(112, 22);
            this.m_Archivo_Salir.Text = "m_Salir";
            this.m_Archivo_Salir.Click += new System.EventHandler(this.m_Archivo_Salir_Click);
            // 
            // m_Admon
            // 
            this.m_Admon.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Admon_Clientes,
            this.m_Admon_Tesoreria,
            this.m_Admon_Contabilidad,
            this.m_Admon_Almacen,
            this.m_Admon_Proveedor,
            this.m_Admon_RecursosHumanos});
            this.m_Admon.Name = "m_Admon";
            this.m_Admon.Size = new System.Drawing.Size(116, 20);
            this.m_Admon.Text = "m_Administración";
            // 
            // m_Admon_Clientes
            // 
            this.m_Admon_Clientes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Admon_Clientes_Expediente,
            this.m_Admon_Clientes_OrdenCliente,
            this.m_Admon_Clientes_Remisionado,
            this.m_Admon_Clientes_Comprobantes});
            this.m_Admon_Clientes.Name = "m_Admon_Clientes";
            this.m_Admon_Clientes.Size = new System.Drawing.Size(189, 22);
            this.m_Admon_Clientes.Text = "m_Clientes";
            // 
            // m_Admon_Clientes_Expediente
            // 
            this.m_Admon_Clientes_Expediente.Name = "m_Admon_Clientes_Expediente";
            this.m_Admon_Clientes_Expediente.Size = new System.Drawing.Size(175, 22);
            this.m_Admon_Clientes_Expediente.Text = "Bttn_Expediente";
            this.m_Admon_Clientes_Expediente.Tag = "Forms.Contribuyentes.ClientesForm";
            this.m_Admon_Clientes_Expediente.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // m_Admon_Clientes_OrdenCliente
            // 
            this.m_Admon_Clientes_OrdenCliente.Name = "m_Admon_Clientes_OrdenCliente";
            this.m_Admon_Clientes_OrdenCliente.Size = new System.Drawing.Size(175, 22);
            this.m_Admon_Clientes_OrdenCliente.Text = "Bttn_OrdenCliente";
            // 
            // m_Admon_Clientes_Remisionado
            // 
            this.m_Admon_Clientes_Remisionado.Name = "m_Admon_Clientes_Remisionado";
            this.m_Admon_Clientes_Remisionado.Size = new System.Drawing.Size(175, 22);
            this.m_Admon_Clientes_Remisionado.Tag = "Forms.Remisionado.RemisionadoForm";
            this.m_Admon_Clientes_Remisionado.Text = "Bttn_Remision";
            this.m_Admon_Clientes_Remisionado.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // m_Admon_Clientes_Comprobantes
            // 
            this.m_Admon_Clientes_Comprobantes.Name = "m_Admon_Clientes_Comprobantes";
            this.m_Admon_Clientes_Comprobantes.Size = new System.Drawing.Size(175, 22);
            this.m_Admon_Clientes_Comprobantes.Text = "Bttn_Comprobante";
            // 
            // m_Admon_Tesoreria
            // 
            this.m_Admon_Tesoreria.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Admon_Tesoreria_Bancos,
            this.m_Admon_Tesoreria_Movimientos});
            this.m_Admon_Tesoreria.Name = "m_Admon_Tesoreria";
            this.m_Admon_Tesoreria.Size = new System.Drawing.Size(189, 22);
            this.m_Admon_Tesoreria.Text = "m_Tesoreria";
            // 
            // m_Admon_Tesoreria_Bancos
            // 
            this.m_Admon_Tesoreria_Bancos.Name = "m_Admon_Tesoreria_Bancos";
            this.m_Admon_Tesoreria_Bancos.Size = new System.Drawing.Size(160, 22);
            this.m_Admon_Tesoreria_Bancos.Tag = "Forms.Banco.CuentaBancariaCatalogoForm";
            this.m_Admon_Tesoreria_Bancos.Text = "m_BancoCtas";
            this.m_Admon_Tesoreria_Bancos.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // m_Admon_Tesoreria_Movimientos
            // 
            this.m_Admon_Tesoreria_Movimientos.Name = "m_Admon_Tesoreria_Movimientos";
            this.m_Admon_Tesoreria_Movimientos.Size = new System.Drawing.Size(160, 22);
            this.m_Admon_Tesoreria_Movimientos.Tag = "Forms.Banco.MovimientoCatalogoForm";
            this.m_Admon_Tesoreria_Movimientos.Text = "m_Movimientos";
            this.m_Admon_Tesoreria_Movimientos.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // m_Admon_Contabilidad
            // 
            this.m_Admon_Contabilidad.Name = "m_Admon_Contabilidad";
            this.m_Admon_Contabilidad.Size = new System.Drawing.Size(189, 22);
            this.m_Admon_Contabilidad.Text = "m_Contabilidad";
            // 
            // m_Admon_Almacen
            // 
            this.m_Admon_Almacen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Admon_AlmacenMP,
            this.m_Admon_AlmacenPT});
            this.m_Admon_Almacen.Name = "m_Admon_Almacen";
            this.m_Admon_Almacen.Size = new System.Drawing.Size(189, 22);
            this.m_Admon_Almacen.Text = "m_Almacen";
            // 
            // m_Admon_AlmacenMP
            // 
            this.m_Admon_AlmacenMP.Name = "m_Admon_AlmacenMP";
            this.m_Admon_AlmacenMP.Size = new System.Drawing.Size(155, 22);
            this.m_Admon_AlmacenMP.Text = "m_AlmacenMP";
            // 
            // m_Admon_AlmacenPT
            // 
            this.m_Admon_AlmacenPT.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Admon_AlmacenPT_Remisionado});
            this.m_Admon_AlmacenPT.Name = "m_Admon_AlmacenPT";
            this.m_Admon_AlmacenPT.Size = new System.Drawing.Size(155, 22);
            this.m_Admon_AlmacenPT.Text = "m_AlmacenPT";
            // 
            // m_Admon_AlmacenPT_Remisionado
            // 
            this.m_Admon_AlmacenPT_Remisionado.Name = "m_Admon_AlmacenPT_Remisionado";
            this.m_Admon_AlmacenPT_Remisionado.Size = new System.Drawing.Size(159, 22);
            this.m_Admon_AlmacenPT_Remisionado.Text = "m_Remisionado";
            // 
            // m_Admon_Proveedor
            // 
            this.m_Admon_Proveedor.Name = "m_Admon_Proveedor";
            this.m_Admon_Proveedor.Size = new System.Drawing.Size(189, 22);
            this.m_Admon_Proveedor.Text = "m_Proveedor";
            // 
            // m_Admon_RecursosHumanos
            // 
            this.m_Admon_RecursosHumanos.Name = "m_Admon_RecursosHumanos";
            this.m_Admon_RecursosHumanos.Size = new System.Drawing.Size(189, 22);
            this.m_Admon_RecursosHumanos.Text = "m_RecursosHumanos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButtonEmpresa});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1058, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ButtonEmpresa
            // 
            this.ButtonEmpresa.Image = global::Jaeger.UI.Properties.Resources.color_settings_16;
            this.ButtonEmpresa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonEmpresa.Name = "ButtonEmpresa";
            this.ButtonEmpresa.Size = new System.Drawing.Size(134, 20);
            this.ButtonEmpresa.Text = "XXXXXXXXXXXXXX";
            // 
            // m_Ventana
            // 
            this.m_Ventana.Name = "m_Ventana";
            this.m_Ventana.Size = new System.Drawing.Size(61, 20);
            this.m_Ventana.Text = "Ventana";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 562);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo;
        private System.Windows.Forms.ToolStripMenuItem m_Archivo_Salir;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem m_Admon;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Clientes;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Clientes_Expediente;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Clientes_OrdenCliente;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Clientes_Remisionado;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Clientes_Comprobantes;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Tesoreria;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Tesoreria_Bancos;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Tesoreria_Movimientos;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Contabilidad;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Almacen;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_Proveedor;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_RecursosHumanos;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_AlmacenMP;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_AlmacenPT;
        private System.Windows.Forms.ToolStripMenuItem m_Admon_AlmacenPT_Remisionado;
        private System.Windows.Forms.ToolStripDropDownButton ButtonEmpresa;
        private System.Windows.Forms.ToolStripMenuItem m_Ventana;
    }
}