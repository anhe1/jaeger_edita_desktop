﻿namespace Jaeger.UI.Forms.Banco {
    partial class MovimientoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBar1CommonControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Identificador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BeneficiarioT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Concepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveFormaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaDocto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumDocto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveBancoT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SucursalT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CuentaCLABET = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumAutorizacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Abono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Saldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaAplicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cancela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PorComprobar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Creo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCancelar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Identificador,
            this.Status,
            this.BeneficiarioT,
            this.Concepto,
            this.ClaveFormaPago,
            this.FechaDocto,
            this.NumDocto,
            this.ClaveBancoT,
            this.SucursalT,
            this.CuentaCLABET,
            this.Referencia,
            this.NumAutorizacion,
            this.Abono,
            this.Cargo,
            this.Saldo,
            this.FechaAplicacion,
            this.FechaEmision,
            this.Cancela,
            this.PorComprobar,
            this.Creo});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 425);
            this.dataGridView1.TabIndex = 1;
            // 
            // Identificador
            // 
            this.Identificador.DataPropertyName = "Identificador";
            this.Identificador.HeaderText = "Identificador";
            this.Identificador.Name = "Identificador";
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            // 
            // BeneficiarioT
            // 
            this.BeneficiarioT.DataPropertyName = "BeneficiarioT";
            this.BeneficiarioT.HeaderText = "BeneficiarioT";
            this.BeneficiarioT.Name = "BeneficiarioT";
            // 
            // Concepto
            // 
            this.Concepto.DataPropertyName = "Concepto";
            this.Concepto.HeaderText = "Concepto";
            this.Concepto.Name = "Concepto";
            // 
            // ClaveFormaPago
            // 
            this.ClaveFormaPago.DataPropertyName = "ClaveFormaPago";
            this.ClaveFormaPago.HeaderText = "ClaveFormaPago";
            this.ClaveFormaPago.Name = "ClaveFormaPago";
            // 
            // FechaDocto
            // 
            this.FechaDocto.DataPropertyName = "FechaDocto";
            this.FechaDocto.HeaderText = "FechaDocto";
            this.FechaDocto.Name = "FechaDocto";
            // 
            // NumDocto
            // 
            this.NumDocto.DataPropertyName = "NumDocto";
            this.NumDocto.HeaderText = "NumDocto";
            this.NumDocto.Name = "NumDocto";
            // 
            // ClaveBancoT
            // 
            this.ClaveBancoT.DataPropertyName = "ClaveBancoT";
            this.ClaveBancoT.HeaderText = "ClaveBancoT";
            this.ClaveBancoT.Name = "ClaveBancoT";
            // 
            // SucursalT
            // 
            this.SucursalT.DataPropertyName = "SucursalT";
            this.SucursalT.HeaderText = "SucursalT";
            this.SucursalT.Name = "SucursalT";
            // 
            // CuentaCLABET
            // 
            this.CuentaCLABET.DataPropertyName = "CuentaCLABET";
            this.CuentaCLABET.HeaderText = "CuentaCLABET";
            this.CuentaCLABET.Name = "CuentaCLABET";
            // 
            // Referencia
            // 
            this.Referencia.DataPropertyName = "Referencia";
            this.Referencia.HeaderText = "Referencia";
            this.Referencia.Name = "Referencia";
            // 
            // NumAutorizacion
            // 
            this.NumAutorizacion.DataPropertyName = "NumAutorizacion";
            this.NumAutorizacion.HeaderText = "NumAutorizacion";
            this.NumAutorizacion.Name = "NumAutorizacion";
            // 
            // Abono
            // 
            this.Abono.DataPropertyName = "Abono";
            this.Abono.HeaderText = "Abono";
            this.Abono.Name = "Abono";
            // 
            // Cargo
            // 
            this.Cargo.DataPropertyName = "Cargo";
            this.Cargo.HeaderText = "Cargo";
            this.Cargo.Name = "Cargo";
            // 
            // Saldo
            // 
            this.Saldo.DataPropertyName = "Saldo";
            this.Saldo.HeaderText = "Saldo";
            this.Saldo.Name = "Saldo";
            // 
            // FechaAplicacion
            // 
            this.FechaAplicacion.DataPropertyName = "FechaAplicacion";
            this.FechaAplicacion.HeaderText = "FechaAplicacion";
            this.FechaAplicacion.Name = "FechaAplicacion";
            // 
            // FechaEmision
            // 
            this.FechaEmision.DataPropertyName = "FechaEmision";
            this.FechaEmision.HeaderText = "FechaEmision";
            this.FechaEmision.Name = "FechaEmision";
            // 
            // Cancela
            // 
            this.Cancela.DataPropertyName = "Cancela";
            this.Cancela.HeaderText = "Cancela";
            this.Cancela.Name = "Cancela";
            // 
            // PorComprobar
            // 
            this.PorComprobar.DataPropertyName = "PorComprobar";
            this.PorComprobar.HeaderText = "PorComprobar";
            this.PorComprobar.Name = "PorComprobar";
            this.PorComprobar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Creo
            // 
            this.Creo.DataPropertyName = "Creo";
            this.Creo.HeaderText = "Creo";
            this.Creo.Name = "Creo";
            // 
            // MovimientoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ToolBar);
            this.Name = "MovimientoCatalogoForm";
            this.Text = "MovimientoCatalogoForm";
            this.Load += new System.EventHandler(this.MovimientoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBar1CommonControl ToolBar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Identificador;
        private System.Windows.Forms.DataGridViewComboBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn BeneficiarioT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Concepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveFormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaDocto;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumDocto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveBancoT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SucursalT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CuentaCLABET;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumAutorizacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Abono;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Saldo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaAplicacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cancela;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PorComprobar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creo;
    }
}