﻿using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Util.Services;
using System;
using System.Collections.Generic;
using System.IO;

namespace Jaeger.UI.Forms.Banco {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Banco");
        public ReporteForm() : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(MovimientoBancarioPrinter)) {
                this.CrearTransferencia();
            } else if (this.CurrentObject.GetType() == typeof(EstadoCuentaPrinter)) {
                this.CrearEstadoCuenta();
            } else if (this.CurrentObject.GetType() == typeof(MovmientosReportePrinter)) {

            }
        }

        private void CrearTransferencia() {
            var current = (MovimientoBancarioPrinter)this.CurrentObject;
            if (current.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancosTransferenciaCuentasReporte30.rdlc");
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancosMovimientoReporte30.rdlc");
                this.SetDataSource("MovimientoBancarioComprobantes", Domain.Services.DbConvert.ConvertToDataTable(current.Comprobantes));
            }

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d = Domain.Services.DbConvert.ConvertToDataTable<MovimientoBancarioPrinter>(new List<MovimientoBancarioPrinter>() { current });
            this.SetDataSource("MovimientoBancario", d);
            this.Finalizar();
        }

        private void CrearEstadoCuenta() {
            var current = (EstadoCuentaPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancoEstadoCuentav10Reporte.rdlc");
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d = Domain.Services.DbConvert.ConvertToDataTable<MovimientoBancarioDetailModel>(new List<MovimientoBancarioDetailModel>(current.Movimientos));
            var d1 = Domain.Services.DbConvert.ConvertToDataTable(new List<BancoCuentaDetailModel> { current.Cuenta });
            this.SetDataSource("Cuenta", d1);
            this.SetDataSource("Saldo", Domain.Services.DbConvert.ConvertToDataTable(new List<BancoCuentaSaldoModel> { current.Saldo }));
            this.SetDataSource("Movimientos", d);
            this.Finalizar();
        }
    }
}
