﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Banco {
    public partial class MovimientoCatalogoForm : Form {
        protected Aplication.Banco.IBancoService service;
        protected Beta.Aplication.Banco.IPrepolizaService prepoliza;
        private BindingList<MovimientoBancarioDetailModel> movimientos;

        public MovimientoCatalogoForm() {
            InitializeComponent();
        }

        private void MovimientoCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new Aplication.Banco.BancoService();
            this.prepoliza = new Beta.Aplication.Banco.PrepolizaService();

            this.dataGridView1.DataGridCommon();
            var ButtonImprimir = new ToolStripMenuItem() { Text = "Imprimir" };
            var ButtonImportar = new ToolStripMenuItem() { Text = "Importar" };
            var ButtonCrearTabla = new ToolStripMenuItem() { Text = "Crear tablas", Image = Properties.Resources.color_settings_16 };
            ButtonImportar.Click += ButtonImportar_Click;
            ButtonCrearTabla.Click += ButtonCrearTabla_Click;
            ButtonImprimir.Click += ButtonImprimir_Click;
            this.ToolBar.Herramientas.DropDownItems.Add(ButtonImprimir);
            this.ToolBar.Herramientas.DropDownItems.Add(ButtonImportar);
            this.ToolBar.Herramientas.DropDownItems.Add(ButtonCrearTabla);
            var comboStatus = this.dataGridView1.Columns["Status"] as DataGridViewComboBoxColumn;
            comboStatus.DataSource = BancoService.GetStatus();
            comboStatus.DisplayMember = "Descripcion";
            comboStatus.ValueMember = "Id";
        }

        private void ButtonImprimir_Click(object sender, EventArgs e) {
            var seleccionado = (MovimientoBancarioDetailModel)this.dataGridView1.CurrentRow.DataBoundItem;
            if (seleccionado != null) {
                var imprimir = new ReporteForm(new MovimientoBancarioPrinter(seleccionado));
                imprimir.Show();
            }
        }

        private void ButtonCrearTabla_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de crear las tablas del sistema?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new WaitingForm(this.Crear)) {
                    espera.Text = "Creando tablas del sistema, un momento...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ButtonImportar_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de iniciar el proceso de importación?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new WaitingForm(this.Importar)) {
                    espera.Text = "Importando información ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando ..." +
                    "";
                espera.ShowDialog(this);
            }
            this.dataGridView1.DataSource = this.movimientos;
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            this.movimientos = this.service.GetList(0, this.ToolBar.GetMes(), this.ToolBar.GetEjercicio(), 0);
        }

        private void Importar() {
            prepoliza.Importar(this.ToolBar.GetEjercicio(), this.ToolBar.GetMes());
        }

        private void Crear() {
            this.service.Create();
        }
    }
}
