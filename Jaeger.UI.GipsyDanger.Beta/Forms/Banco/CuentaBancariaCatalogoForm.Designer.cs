﻿
namespace Jaeger.UI.Forms.Banco {
    partial class CuentaBancariaCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuentaBancariaCatalogoForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IdCuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Activo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Beneficiario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Banco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveBanco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Alias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFCBenficiario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLABE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumCuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sucursal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plaza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaldoInicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiaCorte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoCheque = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Extranjero = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FechaApertura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Creo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdCuenta,
            this.Activo,
            this.Beneficiario,
            this.Banco,
            this.ClaveBanco,
            this.Alias,
            this.Moneda,
            this.RFCBenficiario,
            this.NumCliente,
            this.CLABE,
            this.NumCuenta,
            this.Sucursal,
            this.Funcionario,
            this.Plaza,
            this.Telefono,
            this.SaldoInicial,
            this.DiaCorte,
            this.NoCheque,
            this.Extranjero,
            this.FechaApertura,
            this.Creo});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 425);
            this.dataGridView1.TabIndex = 1;
            // 
            // IdCuenta
            // 
            this.IdCuenta.DataPropertyName = "IdCuenta";
            this.IdCuenta.HeaderText = "IdCuenta";
            this.IdCuenta.Name = "IdCuenta";
            this.IdCuenta.ReadOnly = true;
            this.IdCuenta.Width = 50;
            // 
            // Activo
            // 
            this.Activo.DataPropertyName = "Activo";
            this.Activo.HeaderText = "Activo";
            this.Activo.Name = "Activo";
            this.Activo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Activo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Activo.Width = 50;
            // 
            // Beneficiario
            // 
            this.Beneficiario.DataPropertyName = "Beneficiario";
            this.Beneficiario.HeaderText = "Beneficiario";
            this.Beneficiario.Name = "Beneficiario";
            this.Beneficiario.Width = 200;
            // 
            // Banco
            // 
            this.Banco.DataPropertyName = "Banco";
            this.Banco.HeaderText = "Banco";
            this.Banco.Name = "Banco";
            // 
            // ClaveBanco
            // 
            this.ClaveBanco.DataPropertyName = "ClaveBanco";
            this.ClaveBanco.HeaderText = "Clv. Banco";
            this.ClaveBanco.Name = "ClaveBanco";
            this.ClaveBanco.Width = 50;
            // 
            // Alias
            // 
            this.Alias.DataPropertyName = "Alias";
            this.Alias.HeaderText = "Alias";
            this.Alias.Name = "Alias";
            this.Alias.Width = 200;
            // 
            // Moneda
            // 
            this.Moneda.DataPropertyName = "Moneda";
            this.Moneda.HeaderText = "Moneda";
            this.Moneda.Name = "Moneda";
            this.Moneda.Width = 75;
            // 
            // RFCBenficiario
            // 
            this.RFCBenficiario.DataPropertyName = "RFCBenficiario";
            this.RFCBenficiario.HeaderText = "RFCBenficiario";
            this.RFCBenficiario.Name = "RFCBenficiario";
            // 
            // NumCliente
            // 
            this.NumCliente.DataPropertyName = "NumCliente";
            this.NumCliente.HeaderText = "Núm. Cliente";
            this.NumCliente.Name = "NumCliente";
            // 
            // CLABE
            // 
            this.CLABE.DataPropertyName = "CLABE";
            this.CLABE.HeaderText = "CLABE";
            this.CLABE.Name = "CLABE";
            this.CLABE.Width = 135;
            // 
            // NumCuenta
            // 
            this.NumCuenta.DataPropertyName = "NumCuenta";
            this.NumCuenta.HeaderText = "Núm. Cuenta";
            this.NumCuenta.Name = "NumCuenta";
            this.NumCuenta.Width = 150;
            // 
            // Sucursal
            // 
            this.Sucursal.DataPropertyName = "Sucursal";
            this.Sucursal.HeaderText = "Sucursal";
            this.Sucursal.Name = "Sucursal";
            // 
            // Funcionario
            // 
            this.Funcionario.DataPropertyName = "Funcionario";
            this.Funcionario.HeaderText = "Funcionario";
            this.Funcionario.Name = "Funcionario";
            this.Funcionario.Width = 150;
            // 
            // Plaza
            // 
            this.Plaza.DataPropertyName = "Plaza";
            this.Plaza.HeaderText = "Plaza";
            this.Plaza.Name = "Plaza";
            // 
            // Telefono
            // 
            this.Telefono.DataPropertyName = "Telefono";
            this.Telefono.HeaderText = "Telefono";
            this.Telefono.Name = "Telefono";
            // 
            // SaldoInicial
            // 
            this.SaldoInicial.DataPropertyName = "SaldoInicial";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SaldoInicial.DefaultCellStyle = dataGridViewCellStyle1;
            this.SaldoInicial.HeaderText = "Saldo Inicial";
            this.SaldoInicial.Name = "SaldoInicial";
            this.SaldoInicial.Width = 75;
            // 
            // DiaCorte
            // 
            this.DiaCorte.DataPropertyName = "DiaCorte";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiaCorte.DefaultCellStyle = dataGridViewCellStyle2;
            this.DiaCorte.HeaderText = "Día Corte";
            this.DiaCorte.Name = "DiaCorte";
            this.DiaCorte.Width = 50;
            // 
            // NoCheque
            // 
            this.NoCheque.DataPropertyName = "NoCheque";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NoCheque.DefaultCellStyle = dataGridViewCellStyle3;
            this.NoCheque.HeaderText = "No. Cheque";
            this.NoCheque.Name = "NoCheque";
            this.NoCheque.Width = 50;
            // 
            // Extranjero
            // 
            this.Extranjero.DataPropertyName = "Extranjero";
            this.Extranjero.HeaderText = "Extranjero";
            this.Extranjero.Name = "Extranjero";
            this.Extranjero.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Extranjero.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Extranjero.Width = 50;
            // 
            // FechaApertura
            // 
            this.FechaApertura.DataPropertyName = "FechaApertura";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd MMM yyyy";
            this.FechaApertura.DefaultCellStyle = dataGridViewCellStyle4;
            this.FechaApertura.HeaderText = "FechaApertura";
            this.FechaApertura.Name = "FechaApertura";
            // 
            // Creo
            // 
            this.Creo.DataPropertyName = "Creo";
            this.Creo.HeaderText = "Creo";
            this.Creo.Name = "Creo";
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(800, 25);
            this.ToolBar.TabIndex = 2;
            this.ToolBar.ButtonEditar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonEditar_Click);
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonActualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonCerrar_Click);
            // 
            // CuentaBancariaCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CuentaBancariaCatalogoForm";
            this.Text = "Cuenta Bancaria";
            this.Load += new System.EventHandler(this.CuentaBancariaCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private Common.Forms.ToolBarStandarControl ToolBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCuenta;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Activo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Beneficiario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Banco;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveBanco;
        private System.Windows.Forms.DataGridViewTextBoxColumn Alias;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCBenficiario;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLABE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumCuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sucursal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Plaza;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaldoInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiaCorte;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoCheque;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Extranjero;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaApertura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Creo;
    }
}