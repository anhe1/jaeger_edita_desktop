﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.UI.Forms.Banco {
    public partial class CuentaBancariaCatalogoForm : Form {
        protected Aplication.Banco.IBancoCuentaService service;
        protected Beta.Aplication.Banco.IPrepolizaService prepoliza;
        protected BindingList<BancoCuentaDetailModel> movimientos;
        public CuentaBancariaCatalogoForm() {
            InitializeComponent();
        }

        private void CuentaBancariaCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new Aplication.Banco.BancoCuentaService();
            this.prepoliza = new Beta.Aplication.Banco.PrepolizaService();
            this.dataGridView1.DataGridCommon();

            var ButtonImportar = new ToolStripMenuItem() { Text = "Importar" };
            var ButtonCrearTabla = new ToolStripMenuItem() { Text = "Crear tablas", Image = Properties.Resources.color_settings_16 };
            this.ToolBar.Herramientas.DropDownItems.Add(ButtonImportar);
            this.ToolBar.Herramientas.DropDownItems.Add(ButtonCrearTabla);
            ButtonImportar.Click += ButtonImportar_Click;
            ButtonCrearTabla.Click += ButtonCrearTabla_Click;
        }

        private void ButtonCrearTabla_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de crear las tablas del sistema?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new WaitingForm(this.Crear)) {
                    espera.Text = "Creando tablas del sistema, un momento...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void ButtonImportar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Importar)) {
                espera.Text = "Importando información ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new WaitingForm(this.Consultar)) {
                espera.Text = "Consultando";
                espera.ShowDialog(this);
            }
            this.dataGridView1.DataSource = this.movimientos;
        }

        private void ToolBar_ButtonEditar_Click(object sender, EventArgs e) {
            if (this.Seleccionado() != null) {
                using (var editar = new CuentaBancariaForm(this.Seleccionado())) {
                    editar.ShowDialog(this);
                }
            }
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            this.movimientos = this.service.GetList(true);
        }

        private void Importar() {
            prepoliza.ImportarCuentasBancarias();
        }

        private void Crear() {
            this.service.Create();
        }

        private BancoCuentaDetailModel Seleccionado() {
            if (this.dataGridView1.CurrentRow != null) {
                var seleccionado = this.dataGridView1.CurrentRow.DataBoundItem as BancoCuentaDetailModel;
                return seleccionado;
            }
            return null;
        }
    }
}
