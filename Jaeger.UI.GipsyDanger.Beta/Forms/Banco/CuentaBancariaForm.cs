﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Banco;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;

namespace Jaeger.UI.Forms.Banco {
    public partial class CuentaBancariaForm : Form {
        protected IBancoCuentaService service;
        protected IBancosCatalogo bancoCatalogo = new BancosCatalogo();
        private BancoCuentaDetailModel cuentaBancaria;

        public CuentaBancariaForm() {
            InitializeComponent();
        }

        public CuentaBancariaForm(BancoCuentaDetailModel model) {
            InitializeComponent();
            this.cuentaBancaria = model;
        }

        private void CuentaBancariaForm_Load(object sender, EventArgs e) {
            this.service = new BancoCuentaService();
            this.bancoCatalogo.Load();
            this.NumCuenta.KeyPress += Extensions.TextBoxOnlyNumbers_KeyPress;
            this.ClaveBanco.DataSource = this.bancoCatalogo.Items;
            this.ClaveBanco.SelectedIndex = -1;
            this.Moneda.DataSource = this.service.GetMonedas();
            this.ToolBar.Actualizar.PerformClick();
        }

        private void ClaveBanco_SelectedIndexChanged(object sender, EventArgs e) {
            var seleccionado = this.ClaveBanco.SelectedItem as ClaveBanco;
            if (seleccionado != null) {
                this.cuentaBancaria.Banco = seleccionado.Descripcion;
            }
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            this.cuentaBancaria = null;
            this.ToolBar.Actualizar.PerformClick();
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using (var espera = new UI.Common.Forms.WaitingForm(this.Guardar)) {
                    espera.ShowDialog(this);
                    this.Close();
                }
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            if (this.cuentaBancaria == null) {
                this.cuentaBancaria = new BancoCuentaDetailModel();
            }
            this.CreateBinding();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.Beneficiario.DataBindings.Clear();
            this.Beneficiario.DataBindings.Add("Text", this.cuentaBancaria, "Beneficiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BeneficiarioRFC.DataBindings.Clear();
            this.BeneficiarioRFC.DataBindings.Add("Text", this.cuentaBancaria, "RFCBenficiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCuenta.DataBindings.Clear();
            this.NumCuenta.DataBindings.Add("Text", this.cuentaBancaria, "NumCuenta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCliente.DataBindings.Clear();
            this.NumCliente.DataBindings.Add("Text", this.cuentaBancaria, "NumCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Clabe.DataBindings.Clear();
            this.Clabe.DataBindings.Add("Text", this.cuentaBancaria, "CLABE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CtaContable.DataBindings.Clear();
            this.CtaContable.DataBindings.Add("Text", this.cuentaBancaria, "CuentaContable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoRFC.DataBindings.Clear();
            this.BancoRFC.DataBindings.Add("Text", this.cuentaBancaria, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCliente.DataBindings.Clear();
            this.NumCliente.DataBindings.Add("Text", this.cuentaBancaria, "NumCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.DataBindings.Add("Text", this.cuentaBancaria, "Moneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ClaveBanco.DataBindings.Clear();
            this.ClaveBanco.DataBindings.Add("SelectedValue", this.cuentaBancaria, "ClaveBanco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Sucursal.DataBindings.Clear();
            this.Sucursal.DataBindings.Add("Text", this.cuentaBancaria, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Telefono.DataBindings.Clear();
            this.Telefono.DataBindings.Add("Text", this.cuentaBancaria, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Funcionario.DataBindings.Clear();
            this.Funcionario.DataBindings.Add("Text", this.cuentaBancaria, "Funcionario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Alias.DataBindings.Clear();
            this.Alias.DataBindings.Add("Text", this.cuentaBancaria, "Alias", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SaldoInicial.DataBindings.Clear();
            this.SaldoInicial.DataBindings.Add("Text", this.cuentaBancaria, "SaldoInicial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NoCheque.DataBindings.Clear();
            this.NoCheque.DataBindings.Add("Text", this.cuentaBancaria, "NoCheque", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DiaCorte.DataBindings.Clear();
            this.DiaCorte.DataBindings.Add("Text", this.cuentaBancaria, "DiaCorte", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaApertura.DataBindings.Clear();
            this.FechaApertura.DataBindings.Add("Value", this.cuentaBancaria, "FechaApertura", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoExtranjero.DataBindings.Clear();
            this.BancoExtranjero.DataBindings.Add("Checked", this.cuentaBancaria, "Extranjero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Activo.DataBindings.Clear();
            this.Activo.DataBindings.Add("Checked", this.cuentaBancaria, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Guardar() {
            this.cuentaBancaria = this.service.Save(this.cuentaBancaria);
        }

        private void Actualizar() {
            this.cuentaBancaria = this.service.GetCuenta(this.cuentaBancaria.IdCuenta);
        }

        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        private void TxbFolio_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }

        private bool Validar() {
            this.errorProvider1.Clear();
            if (this.cuentaBancaria.Beneficiario == null | string.IsNullOrEmpty(this.cuentaBancaria.Beneficiario.Trim())) {
                this.errorProvider1.SetError(this.Beneficiario, "Es necesario un nombre para el beneficiario de la cuenta.");
                return false;
            }

            if (this.cuentaBancaria.NumCuenta == null) {
                this.errorProvider1.SetError(this.NumCuenta, "Es necesario asociar un número de cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.NumCuenta.Trim())) {
                this.errorProvider1.SetError(this.NumCuenta, "Es necesario asociar un número de cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.ClaveBanco)) {
                this.errorProvider1.SetError(this.ClaveBanco, "Selecciona una clave de banco para la cuenta.");
                return false;
            }
            return true;
        }

        
    }
}
