﻿
namespace Jaeger.UI.Forms.Banco {
    partial class CuentaBancariaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SaldoInicial = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Activo = new System.Windows.Forms.CheckBox();
            this.Moneda = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DiaCorte = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.NoCheque = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.FechaApertura = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.NumCliente = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CtaContable = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Clabe = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NumCuenta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Beneficiario = new System.Windows.Forms.TextBox();
            this.BeneficiarioRFC = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Alias = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Funcionario = new System.Windows.Forms.TextBox();
            this.ClaveBanco = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Telefono = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.BancoRFC = new System.Windows.Forms.TextBox();
            this.BancoExtranjero = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Sucursal = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DiaCorte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoCheque)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(515, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.ButtonNuevo_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonNuevo_Click);
            this.ToolBar.ButtonGuardar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonGuardar_Click);
            this.ToolBar.ButtonActualizar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonActualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.ToolBarButtonCerrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SaldoInicial);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Activo);
            this.groupBox1.Controls.Add(this.Moneda);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.DiaCorte);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.NoCheque);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.FechaApertura);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.NumCliente);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.CtaContable);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Clabe);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.NumCuenta);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Beneficiario);
            this.groupBox1.Controls.Add(this.BeneficiarioRFC);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Alias);
            this.groupBox1.Location = new System.Drawing.Point(12, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 212);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de la cuenta";
            // 
            // SaldoInicial
            // 
            this.SaldoInicial.Location = new System.Drawing.Point(91, 180);
            this.SaldoInicial.Name = "SaldoInicial";
            this.SaldoInicial.Size = new System.Drawing.Size(93, 20);
            this.SaldoInicial.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 184);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Saldo Inicial:";
            // 
            // Activo
            // 
            this.Activo.AutoSize = true;
            this.Activo.Location = new System.Drawing.Point(372, 181);
            this.Activo.Name = "Activo";
            this.Activo.Size = new System.Drawing.Size(56, 17);
            this.Activo.TabIndex = 2;
            this.Activo.Text = "Activo";
            this.Activo.UseVisualStyleBackColor = true;
            // 
            // Moneda
            // 
            this.Moneda.FormattingEnabled = true;
            this.Moneda.Location = new System.Drawing.Point(375, 154);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(97, 21);
            this.Moneda.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(286, 158);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Moneda:";
            // 
            // DiaCorte
            // 
            this.DiaCorte.Location = new System.Drawing.Point(375, 128);
            this.DiaCorte.Name = "DiaCorte";
            this.DiaCorte.Size = new System.Drawing.Size(97, 20);
            this.DiaCorte.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(286, 132);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Día de corte:";
            // 
            // NoCheque
            // 
            this.NoCheque.Location = new System.Drawing.Point(375, 102);
            this.NoCheque.Name = "NoCheque";
            this.NoCheque.Size = new System.Drawing.Size(97, 20);
            this.NoCheque.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(286, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Núm. Cheque:";
            // 
            // FechaApertura
            // 
            this.FechaApertura.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaApertura.Location = new System.Drawing.Point(375, 76);
            this.FechaApertura.Name = "FechaApertura";
            this.FechaApertura.Size = new System.Drawing.Size(97, 20);
            this.FechaApertura.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Fecha Apertura:";
            // 
            // NumCliente
            // 
            this.NumCliente.Location = new System.Drawing.Point(91, 154);
            this.NumCliente.Name = "NumCliente";
            this.NumCliente.Size = new System.Drawing.Size(93, 20);
            this.NumCliente.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Núm. Cliente:";
            // 
            // CtaContable
            // 
            this.CtaContable.Location = new System.Drawing.Point(91, 128);
            this.CtaContable.Name = "CtaContable";
            this.CtaContable.Size = new System.Drawing.Size(93, 20);
            this.CtaContable.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Cta. Contable:";
            // 
            // Clabe
            // 
            this.Clabe.Location = new System.Drawing.Point(91, 102);
            this.Clabe.Name = "Clabe";
            this.Clabe.Size = new System.Drawing.Size(139, 20);
            this.Clabe.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "CLABE:";
            // 
            // NumCuenta
            // 
            this.NumCuenta.Location = new System.Drawing.Point(91, 76);
            this.NumCuenta.Name = "NumCuenta";
            this.NumCuenta.Size = new System.Drawing.Size(139, 20);
            this.NumCuenta.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Beneficiario:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Núm. Cuenta:";
            // 
            // Beneficiario
            // 
            this.Beneficiario.Location = new System.Drawing.Point(91, 24);
            this.Beneficiario.Name = "Beneficiario";
            this.Beneficiario.Size = new System.Drawing.Size(381, 20);
            this.Beneficiario.TabIndex = 3;
            // 
            // BeneficiarioRFC
            // 
            this.BeneficiarioRFC.Location = new System.Drawing.Point(372, 50);
            this.BeneficiarioRFC.Name = "BeneficiarioRFC";
            this.BeneficiarioRFC.Size = new System.Drawing.Size(100, 20);
            this.BeneficiarioRFC.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Alias:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(286, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "R.F.C.:";
            // 
            // Alias
            // 
            this.Alias.Location = new System.Drawing.Point(91, 50);
            this.Alias.Name = "Alias";
            this.Alias.Size = new System.Drawing.Size(139, 20);
            this.Alias.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Funcionario);
            this.groupBox2.Controls.Add(this.ClaveBanco);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.Telefono);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.BancoRFC);
            this.groupBox2.Controls.Add(this.BancoExtranjero);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.Sucursal);
            this.groupBox2.Location = new System.Drawing.Point(12, 249);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(492, 104);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos del Banco";
            // 
            // Funcionario
            // 
            this.Funcionario.Location = new System.Drawing.Point(306, 73);
            this.Funcionario.Name = "Funcionario";
            this.Funcionario.Size = new System.Drawing.Size(169, 20);
            this.Funcionario.TabIndex = 15;
            // 
            // ClaveBanco
            // 
            this.ClaveBanco.DisplayMember = "Descripcion";
            this.ClaveBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClaveBanco.FormattingEnabled = true;
            this.ClaveBanco.Location = new System.Drawing.Point(64, 20);
            this.ClaveBanco.Name = "ClaveBanco";
            this.ClaveBanco.Size = new System.Drawing.Size(411, 21);
            this.ClaveBanco.TabIndex = 5;
            this.ClaveBanco.ValueMember = "Clave";
            this.ClaveBanco.SelectedIndexChanged += new System.EventHandler(this.ClaveBanco_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(235, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Funcionario:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Banco:";
            // 
            // Telefono
            // 
            this.Telefono.Location = new System.Drawing.Point(64, 73);
            this.Telefono.Name = "Telefono";
            this.Telefono.Size = new System.Drawing.Size(165, 20);
            this.Telefono.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "R.F.C.:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Teléfono:";
            // 
            // BancoRFC
            // 
            this.BancoRFC.Location = new System.Drawing.Point(64, 47);
            this.BancoRFC.Name = "BancoRFC";
            this.BancoRFC.Size = new System.Drawing.Size(105, 20);
            this.BancoRFC.TabIndex = 9;
            // 
            // BancoExtranjero
            // 
            this.BancoExtranjero.AutoSize = true;
            this.BancoExtranjero.Location = new System.Drawing.Point(368, 49);
            this.BancoExtranjero.Name = "BancoExtranjero";
            this.BancoExtranjero.Size = new System.Drawing.Size(107, 17);
            this.BancoExtranjero.TabIndex = 2;
            this.BancoExtranjero.Text = "Banco Extranjero";
            this.BancoExtranjero.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(177, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Sucursal:";
            // 
            // Sucursal
            // 
            this.Sucursal.Location = new System.Drawing.Point(234, 47);
            this.Sucursal.Name = "Sucursal";
            this.Sucursal.Size = new System.Drawing.Size(119, 20);
            this.Sucursal.TabIndex = 11;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // CuentaBancariaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 358);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ToolBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CuentaBancariaForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cuenta Bancaria";
            this.Load += new System.EventHandler(this.CuentaBancariaForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DiaCorte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoCheque)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl ToolBar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Activo;
        private System.Windows.Forms.ComboBox Moneda;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown DiaCorte;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown NoCheque;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker FechaApertura;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox NumCliente;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox CtaContable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Clabe;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NumCuenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Beneficiario;
        private System.Windows.Forms.TextBox BeneficiarioRFC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Alias;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox Funcionario;
        private System.Windows.Forms.ComboBox ClaveBanco;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Telefono;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox BancoRFC;
        private System.Windows.Forms.CheckBox BancoExtranjero;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Sucursal;
        private System.Windows.Forms.TextBox SaldoInicial;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}