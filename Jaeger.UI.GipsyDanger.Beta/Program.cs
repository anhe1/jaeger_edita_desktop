﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.UI.Forms.Login;

namespace Jaeger.UI {
    static class Program {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm(args));
            if (ConfigService.Piloto != null && ConfigService.Synapsis != null)
                Application.Run(new Forms.MainForm());
        }
    }
}
