﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Aplication.Kaiju.Contracts {
    /// <summary>
    /// Servicio para usuarios del sistema
    /// </summary>
    public interface IUserService {
        /// <summary>
        /// buscar clave de usuario
        /// </summary>
        /// <param name="clave">clave de usuario</param>
        /// <returns></returns>
        int Search(string clave);

        /// <summary>
        /// desactivar usuario
        /// </summary>
        /// <param name="item">IUserModel</param>
        /// <returns>bool</returns>
        bool Desactiva(UserModel item);

        /// <summary>
        /// almacenar usuario
        /// </summary>
        /// <param name="usuario">IUserModel</param>
        /// <returns>IUserModel</returns>
        UserModel Save(UserModel usuario);

        /// <summary>
        /// listado
        /// </summary>
        /// <typeparam name="T1">T1</typeparam>
        /// <param name="conditionales">condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new();
    }
}
