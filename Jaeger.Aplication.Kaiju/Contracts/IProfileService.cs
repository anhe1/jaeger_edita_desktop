﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Aplication.Kaiju.Contracts {
    /// <summary>
    /// servicio de perfiles de sistema
    /// </summary>
    public interface IProfileService {
        /// <summary>
        /// obtener listado de perfiles del sistema
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new();

        /// <summary>
        /// almacenar perfile de sistema
        /// </summary>
        /// <param name="model">UserRolDetailModel</param>
        /// <returns>UserRolDetailModel</returns>
        UserRolDetailModel Save(UserRolDetailModel model);
    }
}
