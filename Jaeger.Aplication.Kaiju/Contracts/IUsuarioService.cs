﻿using System.ComponentModel;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Aplication.Kaiju.Contracts {
    /// <summary>
    /// Servicio para usuarios del sistema
    /// </summary>
    public interface IUsuarioService {
        int Search(string clave);

        bool Desactiva(UserModel item);

        /// <summary>
        /// guardar objeto usuario
        /// </summary>
        UserModel Save(UserModel usuario);

        /// <summary>
        /// 
        /// </summary>
        UserModel GetUsuario(int index);

        /// <summary>
        /// 
        /// </summary>
        BindingList<UserModel> GetUsuarios();

        /// <summary>
        /// 
        /// </summary>
        BindingList<UserRolDetailModel> GetPerfiles();

        UserRolDetailModel Save(UserRolDetailModel perfil);
    }
}
