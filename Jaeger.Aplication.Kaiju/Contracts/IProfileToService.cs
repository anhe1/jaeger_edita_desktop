﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Profile;

namespace Jaeger.Aplication.Kaiju.Contracts {
    /// <summary>
    /// servicio de perfiles de sistema
    /// </summary>
    public interface IProfileToService : Base.Contracts.IProfileService {
        /// <summary>
        /// crear menu del sistema
        /// </summary>
        IEnumerable<UIMenuElement> CreateMenus(int idUser);
            
        /// <summary>
        /// obtener perfil del usuario conectado
        /// </summary>
        KaijuLogger CreateProfile(KaijuLogger kaiju);

        /// <summary>
        /// obtener menu del sistema
        /// </summary>
        /// <returns>lista UIMenuElement</returns>
        List<UIMenuElement> GetMenus();
    }
}
