﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Profile;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.DataAccess.Kaiju;

namespace Jaeger.Aplication.Kaiju.Services {
    /// <summary>
    /// servicio de perfiles de sistema
    /// </summary>
    public class ProfileService : UserService, Contracts.IUserService, Contracts.IProfileService, Contracts.IProfileToService, Base.Contracts.IProfileService {
        #region declaraciones
        protected internal ISqlUIMenuRepository _MenuRepository;
        protected internal ISqlUIProfileRepository _ProfileRepository;
        protected internal ISqlUserRolRepository _UserRolRepository;
        protected internal ISqlUserRolRelacionRepository _RelacionRepository;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ProfileService() { 
            this._MenuRepository = new SqlSugarUIMenuRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.OnLoad(); 
        }

        /// <summary>
        /// carga de repositorios basicos
        /// </summary>
        protected override void OnLoad() {
            base.OnLoad();
            this._ProfileRepository = new SqlSugarUIProfileRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._UserRolRepository = new SqlSugarUserRolRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this._RelacionRepository = new SqlSugarUserRolRelacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// IEnumerable de entidades por condicionales
        /// </summary>
        /// <typeparam name="T1">Entidad</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) {
            if (typeof(T1) == typeof(UserModel)) {
                return base.GetList<T1>(conditionales);
            }
            return (IEnumerable<T1>)GetProfile(true);
        }

        /// <summary>
        /// obtener listado de perfiles del sistema
        /// </summary>
        /// <param name="onlyActive">verdadero solo activos</param>
        /// <returns>BindingList</returns>
        protected virtual BindingList<UserRolDetailModel> GetProfile(bool onlyActive) {
            var result = new BindingList<UserRolDetailModel>(this._ProfileRepository.GetProfile(onlyActive).ToList());
            var menus = this.GetMenus();

            var response = Domain.Kaiju.Services.ResolverProfileService.Resolver(menus, result.ToList());
            if (response.Count() > 0)
                return new BindingList<UserRolDetailModel>(response.ToList());
            return new BindingList<UserRolDetailModel>();
        }

        /// <summary>
        /// almacenar perfile de sistema
        /// </summary>
        /// <param name="model">UserRolDetailModel</param>
        /// <returns>UserRolDetailModel</returns>
        public UserRolDetailModel Save(UserRolDetailModel model) {
            model.IdUserRol = this._UserRolRepository.Save(model).IdUserRol;
            if (model.IdUserRol > 0) {
                for (int i = 0; i < model.Perfil.Count; i++) {
                    model.Perfil[i].IdRol = model.IdUserRol;
                    if (model.Perfil[i].IdRol > 0 && model.Perfil[i].SetModified) {
                        if (model.Perfil[i].SetModified) {
                            model.Perfil[i] = this._ProfileRepository.Save(model.Perfil[i]);
                        }
                    }
                }

                for (int i = 0; i < model.Relacion.Count; i++) {
                    model.Relacion[i].IdUserRol = model.IdUserRol;
                    model.Relacion[i] = this._RelacionRepository.Save(model.Relacion[i]);
                }
            }
            return model;
        }

        public UserRolModel GetByIdUser(int idUser) {
            return this._UserRolRepository.GetByIdUser(idUser);
        }

        /// <summary>
        /// obtener menu del sistema
        /// </summary>
        /// <returns>lista UIMenuElement</returns>
        public virtual IEnumerable<UIMenuElement> CreateMenus(int idUser) {
            var _result = this._ProfileRepository.GetBy(idUser);
            var _menus = this.GetMenus();
            return Domain.Kaiju.Services.ResolverProfileService.Resolver(_menus, _result).ToList();
        }

        /// <summary>
        /// obtener perfil del usuario conectado
        /// </summary>
        public virtual KaijuLogger CreateProfile(KaijuLogger kaiju) {
            var perfil = this.GetByIdUser(kaiju.Id);
            if (perfil != null) {
                kaiju.Roles.Add(new Rol(perfil.Nombre.ToLower()));
            } else {
                kaiju.Roles.Add(new Rol("Guest"));
            }
            if (kaiju.Clave.ToLower() == "anhe1") { (kaiju.Roles[0] as Rol).Name = "desarrollador"; }
            return kaiju;
        }

        /// <summary>
        /// crear menu del sistema
        /// </summary>
        public virtual List<UIMenuElement> GetMenus() {
            return this._MenuRepository.GetMenus().ToList();
        }

        #region metodos estaticos
        public new static Domain.Kaiju.Builder.IProfileQueryBuilder Query() {
            return new Domain.Kaiju.Builder.ProfileQueryBuilder();
        }
        #endregion
    }
}
