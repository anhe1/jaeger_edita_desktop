﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Kaiju.Contracts;
using Jaeger.DataAccess.Kaiju;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Aplication.Kaiju.Services {
    /// <summary>
    /// servicio para control de usuarios
    /// </summary>
    public class UsuarioService : IUsuarioService {
        protected ISqlUserRepository repository;
        protected ISqlUserRolRepository profile;

        public UsuarioService() {
            this.repository = new SqlSugarUserRepository(ConfigService.Synapsis.RDS.Edita);
            this.profile = new SqlSugarUserRolRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public int Search(string clave) {
            return this.repository.Search(clave);
        }

        public bool Desactiva(UserModel item) {
            return this.repository.Delete(item.IdUser);
        }

        public UserModel Save(UserModel usuario) {
            return this.repository.Save(usuario);
        }

        /// <summary>
        /// obtener 
        /// </summary>
        public UserModel GetUsuario(int index) {
            return this.repository.GetById(index);
        }

        /// <summary>
        /// obtener listado de usuarios
        /// </summary>
        public BindingList<UserModel> GetUsuarios() {
            return new BindingList<UserModel>(this.repository.GetList().ToList());
        }

        /// <summary>
        /// obtener listado de perfiles
        /// </summary>
        public BindingList<UserRolDetailModel> GetPerfiles() {
            //return new BindingList<UserRolDetailModel>(this.profile.GetList().ToList());
            return null;
        }

        public UserRolDetailModel Save(UserRolDetailModel perfil) {
            if (perfil.IdUserRol == 0) {
                perfil.Creo = ConfigService.Piloto.Clave;
                perfil.FechaNuevo = DateTime.Now;
                perfil.IdUserRol = this.profile.Insert(perfil);
            } else {
                perfil.Modifica = ConfigService.Piloto.Clave;
                perfil.FechaModifica = DateTime.Now;
                this.profile.Update(perfil);
            }
            return perfil;
        }
    }
}
