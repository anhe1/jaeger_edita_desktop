﻿using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Kaiju.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.DataAccess.Kaiju;

namespace Jaeger.Aplication.Kaiju.Services {
    /// <summary>
    /// Servicio para usuarios del sistema
    /// </summary>
    public class UserService : IUserService {
        #region declaraciones
        protected ISqlUserRepository usuarioRepository;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public UserService() {
            this.OnLoad();
        }

        /// <summary>
        /// sobre carga para repositorio
        /// </summary>
        protected virtual void OnLoad() {
            this.usuarioRepository = new SqlSugarUserRepository(ConfigService.Synapsis.RDS.Edita);
        }

        /// <summary>
        /// buscar clave de usuario
        /// </summary>
        /// <param name="clave">clave de usuario</param>
        /// <returns></returns>
        public int Search(string clave) {
            return this.usuarioRepository.Search(clave);
        }

        /// <summary>
        /// desactivar usuario
        /// </summary>
        /// <param name="item">IUserModel</param>
        /// <returns>bool</returns>
        public bool Desactiva(UserModel item) {
            return this.usuarioRepository.Delete(item.IdUser);
        }

        /// <summary>
        /// almacenar usuario
        /// </summary>
        /// <param name="usuario">IUserModel</param>
        /// <returns>IUserModel</returns>
        public UserModel Save(UserModel usuario) {
            return this.usuarioRepository.Save(usuario);
        }

        /// <summary>
        /// listado
        /// </summary>
        /// <typeparam name="T1">T1</typeparam>
        /// <param name="conditionales">condicionales</param>
        /// <returns>IEnumerable</returns>
        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            return this.usuarioRepository.GetList<T1>(conditionales);
        }

        #region metodos estaticos
        /// <summary>
        /// clase builder para consultas
        /// </summary>
        public static Domain.Kaiju.Builder.IUserQueryBuilder Query() {
            return new Domain.Kaiju.Builder.UserQueryBuilder();
        }
        #endregion
    }
}
