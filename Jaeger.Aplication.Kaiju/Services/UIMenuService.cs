﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.Kaiju;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Aplication.Kaiju.Services {
    public class UIMenuService {
        protected ISqlUIMenuRepository repository;

        public UIMenuService() {
            this.repository = new SqlSugarUIMenuRepository(ConfigService.Synapsis.RDS.LiteCP, ConfigService.Piloto.Clave);
        }

        public BindingList<UIMenuModel> GetList() {
            return new BindingList<UIMenuModel>(this.repository.GetList().ToList());
        }

        public UIMenuModel Save(UIMenuModel model) {
            return this.repository.Save(model);
        }
    }
}
