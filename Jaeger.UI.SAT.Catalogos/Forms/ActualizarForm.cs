﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.SAT.Catalogos;
using Jaeger.SAT.Catalogos.Builder;
using Jaeger.SAT.Catalogos.Scraping.Interfaces;

namespace Jaeger.UI.SAT.Catalogos.Forms {
    public partial class ActualizarForm : RadForm {
        private IOriginService Service;
        private BackgroundWorker worker;
        public ActualizarForm() {
            InitializeComponent();
        }

        private void ActualizarSATForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.Service = new OriginService();
            this.Service.GetAll();
            this.GridData.DataSource = this.Service.DataSource;
            this.VersionLabel.Text = $"Versión: {this.Service.Control.Builder} {this.Service.Control.Version}";
            this.worker = new BackgroundWorker();
            this.worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            this.worker.DoWork += Worker_DoWork;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            this.Cerrar.Enabled = false;
            this.Actualizar.Enabled = false;
            

            if (this.Service.DataSource != null) {
                for (int i = 0; i < this.Service.DataSource.Count; i++) {
                    var selected = this.Service.DataSource[i];
                    if (selected.AllowUpdate) {
                        var builder = ScrapingBuilder.Create().Origin(selected).Review();
                        var download = builder.Upgrader();
                        IUpdateRepositoryBuilder update = new UpdateRepositoryBuilder();
                        update.Origin(selected).Import();
                        this.Service.Save();
                    }
                }
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Cerrar.Enabled = true;
            this.Actualizar.Enabled = true;
            this.progressBar1.Visible = false;
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            if (!this.worker.IsBusy) {
                this.progressBar1.Visible = true;
                this.worker.RunWorkerAsync();
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
