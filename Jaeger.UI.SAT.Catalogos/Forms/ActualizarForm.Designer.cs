﻿
namespace Jaeger.UI.SAT.Catalogos.Forms {
    partial class ActualizarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.HeaderBox = new System.Windows.Forms.PictureBox();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.Actualizar = new Telerik.WinControls.UI.RadButton();
            this.TitleHeader = new System.Windows.Forms.Label();
            this.VersionLabel = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Location = new System.Drawing.Point(12, 42);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowAddNewRow = false;
            this.GridData.MasterTemplate.AllowCellContextMenu = false;
            this.GridData.MasterTemplate.AllowColumnChooser = false;
            this.GridData.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.GridData.MasterTemplate.AllowDeleteRow = false;
            this.GridData.MasterTemplate.AllowDragToGroup = false;
            this.GridData.MasterTemplate.AllowEditRow = false;
            this.GridData.MasterTemplate.AllowRowResize = false;
            this.GridData.MasterTemplate.AutoGenerateColumns = false;
            this.GridData.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn5.FieldName = "Name";
            gridViewTextBoxColumn5.HeaderText = "Recurso";
            gridViewTextBoxColumn5.Name = "Name";
            gridViewTextBoxColumn5.Width = 202;
            gridViewTextBoxColumn6.FieldName = "Url";
            gridViewTextBoxColumn6.HeaderText = "URL";
            gridViewTextBoxColumn6.Name = "Url";
            gridViewTextBoxColumn6.Width = 152;
            gridViewTextBoxColumn7.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn7.FieldName = "LastVersion";
            gridViewTextBoxColumn7.FormatString = "{0:d}";
            gridViewTextBoxColumn7.HeaderText = "Últ. Actualización";
            gridViewTextBoxColumn7.Name = "LastVersion";
            gridViewTextBoxColumn7.Width = 76;
            gridViewTextBoxColumn8.FieldName = "DownloadUrl";
            gridViewTextBoxColumn8.HeaderText = "Download";
            gridViewTextBoxColumn8.Name = "DownloadUrl";
            gridViewTextBoxColumn8.Width = 101;
            gridViewCheckBoxColumn2.FieldName = "AllowUpdate";
            gridViewCheckBoxColumn2.HeaderText = "Permitir";
            gridViewCheckBoxColumn2.Name = "AllowUpdate";
            gridViewCheckBoxColumn2.Width = 51;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewCheckBoxColumn2});
            this.GridData.MasterTemplate.ShowFilteringRow = false;
            this.GridData.MasterTemplate.ShowRowHeaderColumn = false;
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(579, 275);
            this.GridData.TabIndex = 4;
            // 
            // HeaderBox
            // 
            this.HeaderBox.BackColor = System.Drawing.Color.White;
            this.HeaderBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderBox.Location = new System.Drawing.Point(0, 0);
            this.HeaderBox.Name = "HeaderBox";
            this.HeaderBox.Size = new System.Drawing.Size(603, 36);
            this.HeaderBox.TabIndex = 5;
            this.HeaderBox.TabStop = false;
            // 
            // Cerrar
            // 
            this.Cerrar.Location = new System.Drawing.Point(495, 323);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(96, 23);
            this.Cerrar.TabIndex = 6;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // Actualizar
            // 
            this.Actualizar.Location = new System.Drawing.Point(393, 323);
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(96, 23);
            this.Actualizar.TabIndex = 6;
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.Click += new System.EventHandler(this.Actualizar_Click);
            // 
            // TitleHeader
            // 
            this.TitleHeader.AutoSize = true;
            this.TitleHeader.BackColor = System.Drawing.Color.White;
            this.TitleHeader.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.TitleHeader.Location = new System.Drawing.Point(48, 11);
            this.TitleHeader.Name = "TitleHeader";
            this.TitleHeader.Size = new System.Drawing.Size(84, 15);
            this.TitleHeader.TabIndex = 7;
            this.TitleHeader.Text = "Catálogos SAT";
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Location = new System.Drawing.Point(9, 326);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.Size = new System.Drawing.Size(48, 13);
            this.VersionLabel.TabIndex = 7;
            this.VersionLabel.Text = "Versión:";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(287, 323);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 8;
            this.progressBar1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Jaeger.UI.SAT.Catalogos.Properties.Resources.sat26px;
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // ActualizarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 348);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.VersionLabel);
            this.Controls.Add(this.TitleHeader);
            this.Controls.Add(this.Actualizar);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.HeaderBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ActualizarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catálogos SAT";
            this.Load += new System.EventHandler(this.ActualizarSATForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Telerik.WinControls.UI.RadGridView GridData;
        private System.Windows.Forms.PictureBox HeaderBox;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadButton Actualizar;
        private System.Windows.Forms.Label TitleHeader;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}