﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// repositorio del directorio de contribuyentes
    /// </summary>
    public interface ISqlContribuyenteRepository : IGenericRepository<ContribuyenteModel> {

        /// <summary>
        /// obtener indice del del contribuyente por el registro federal de contribuyentes
        /// </summary>
        int ReturnId(string rfc);

        bool ExistClave(string clave);

        /// <summary>
        /// almacenar cambio de correo electronico
        /// </summary>
        bool Update(int index, string mail);

        /// <summary>
        /// almacenar la bandera de validacion del RFC
        /// </summary>
        bool ValidaRFC(int index, string razonSocial, string nombreComercial, string domicilioFiscal, bool valido);

        bool ValidaRFC(IValidoRFC valido);

        /// <summary>
        /// obtener objeto complejo contribuyente
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>objeto contribuyenteModel</returns>
        new IContribuyenteDetailModel GetById(int index);

        /// <summary>
        /// obtener un objeto contribuyente por su registro federal de contribuyentes
        /// </summary>
        IContribuyenteDetailModel GetByRFC(string rfc);

        /// <summary>
        /// almacenar un contribuyente
        /// </summary>
        IContribuyenteDetailModel Save(IContribuyenteDetailModel item);

        T1 GetById<T1>(int index) where T1 : class, new();

        /// <summary>
        /// Listado de objetos ContribuyenteDomicilioSingleModel, ContribuyenteDetailModel y ContribuyenteModel
        /// </summary>
        /// <typeparam name="T1">dependiendo de cada objeto se agrega la relacion</typeparam>
        /// <param name="conditionals">sin se incluye @SEARCH se utiliza como busqueda</param>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener listado de beneficiarios del directorio, generalmente son todos los registros
        /// </summary>
        IEnumerable<T1> GetBeneficiarios<T1>(List<IConditional> keyValues) where T1 : class, new();
    }
}