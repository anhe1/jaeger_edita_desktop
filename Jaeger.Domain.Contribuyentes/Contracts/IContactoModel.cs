﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IContactoModel {
        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        int IdContacto {
            get;set;
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el directorio
        /// </summary>
        int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// nombre del contacto
        /// </summary>
        string Nombre {
            get; set;
        }

        /// <summary>
        /// trato sugerido
        /// </summary>
        string TratoSugerido {
            get; set;
        }

        /// <summary>
        /// puesto
        /// </summary>
        string Puesto {
            get; set;
        }

        /// <summary>
        /// detalles
        /// </summary>
        string Nota {
            get; set;
        }

        /// <summary>
        /// tipo de telefono
        /// </summary>
        string TelefonoTipo {
            get; set;
        }

        /// <summary>
        /// tipo de correo electronico
        /// </summary>
        string CorreoTipo {
            get; set;
        }

        /// <summary>
        /// telefono
        /// </summary>
        string Telefono {
            get; set;
        }

        /// <summary>
        /// correo electronico
        /// </summary>
        string Correo {
            get; set;
        }

        /// <summary>
        /// fecha especial
        /// </summary>
        string FechaEspecial {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        string Creo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica {
            get; set;
        }
    }
}