﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IRelacionComercialDetailModel : IRelacionComercialModel {
        string Name { get; }
        bool Selected { get; set; }
    }
}
