﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// Repositorio de cuentas bancarias del directorio
    /// </summary>
    public interface ISqlCuentaBancariaRepository : IGenericRepository<CuentaBancariaModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        ICuentaBancariaModel Save(ICuentaBancariaModel cuentaBancaria);
    }
}