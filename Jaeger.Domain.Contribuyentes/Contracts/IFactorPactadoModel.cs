﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IFactorPactadoModel {
        int IdDirectorio { get; set; }

        decimal Factor { get; set; }

        DateTime FechaNuevo { get; set; }

        string Modifica { get; set; }
    }
}
