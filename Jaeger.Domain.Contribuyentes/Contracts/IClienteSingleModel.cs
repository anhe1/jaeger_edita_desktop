﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IClienteSingleModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        string Nombre { get; set; }
    }
}
