﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface ISqlVendedorRepository : IGenericRepository<Vendedor2Model> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        bool Saveable(IVendedor2DetailModel vendedor);
    }
}