﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// clase que representa a un contribuyente
    /// </summary>
    public interface IContribuyenteModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        bool Activo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer id de catalogo de descuentos asignado cuando es cliente
        /// </summary>
        int IdDescuento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer dias de entrega
        /// </summary>
        int DiasEntrega {
            get; set;
        }

        /// <summary>
        /// obtener o establecer limite de credito
        /// </summary>
        decimal Credito {
            get; set;
        }

        /// <summary>
        /// obtener o establecer factor del descuento pactado
        /// </summary>
        decimal FactorDescuento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer factor del IVA pactado
        /// </summary>
        decimal FactorIvaPactado {
            get; set;
        }

        /// <summary>
        /// Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)
        /// </summary>
        bool Extranjero {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        string Clave {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        string CURP {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        string RFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        string Nombre {
            get; set;
        }

        string NombreComercial { get; set; }

        /// <summary>
        /// obtener o establecer numero telefonico de contacto
        /// </summary>
        string Telefono {
            get; set;
        }

        /// <summary>
        /// obtener o estabelcer direccion de correo electronico
        /// </summary>
        string Correo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer direccion del sitio web
        /// </summary>
        string SitioWEB {
            get; set;
        }

        /// <summary>
        /// obtener o establecer las etiquetas de las relaciones comerciales
        /// </summary>
        string Relacion {
            get; set;
        }

        /// <summary>
        /// observaciones
        /// </summary>
        string Nota {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        string Creo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer regimen fiscal (Fisica o Moral)
        /// </summary>
        string Regimen {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        string RegimenFiscal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer número de registro de identidad fiscal
        /// </summary>
        string NumRegIdTrib {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del país de residencia para efectos fiscales del receptor del comprobante
        /// </summary>
        string ResidenciaFiscal {
            get; set;
        }

        string DomicilioFiscal { get; set; }

        /// <summary>
        /// obtener o establecer uso de CFDI predeterminado
        /// </summary>
        string ClaveUsoCFDI {
            get; set;
        }

        /// <summary>
        /// obtener o establecer registro patronal
        /// </summary>
        string RegistroPatronal {
            get; set;
        }

        byte[] Avatar {
            get; set;
        }
    }
}