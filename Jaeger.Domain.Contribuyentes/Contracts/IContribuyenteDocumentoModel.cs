﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IContribuyenteDocumentoModel {
        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer factor del descuento pactado
        /// </summary>
        string URL { get; set; }

        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
    }
}
