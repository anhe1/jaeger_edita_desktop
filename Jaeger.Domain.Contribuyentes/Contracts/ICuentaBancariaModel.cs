﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// cuenta bancaria tabla _dcbnc
    /// </summary>
    public interface ICuentaBancariaModel {
        /// <summary>
        /// indice de la cuenta
        /// </summary>
        int IdCuenta {
            get; set;
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        bool Activo {
            get; set;
        }

        /// <summary>
        /// alias de la cuenta
        /// </summary>
        string Alias {
            get; set;
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        string Banco {
            get; set;
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        string Nombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        string PrimerApellido {
            get; set;
        }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        string SegundoApellido {
            get; set;
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        decimal CargoMaximo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE
        /// </summary>
        string Clabe {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el codigo de banco según catalogo del SAT
        /// </summary>
        string Clave {
            get; set;
        }

        /// <summary>
        /// obtener o establcer el nombre corto de la institución bancaria
        /// </summary>
        string InsitucionBancaria {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda
        /// </summary>
        string Moneda {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta bancaria
        /// </summary>
        string NumeroDeCuenta {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el numero de la sucursal
        /// </summary>
        string Sucursal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el RFC del beneficiario
        /// </summary>
        string RFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        string TipoCuenta {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la referencia numerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        string RefNumerica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la referencia alfanumerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        string RefAlfanumerica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer si la cuenta ya se encuentra verificada
        /// </summary>
        bool Verificado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer fecha del nuevo registro
        /// </summary>
        DateTime FechaNuevo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        string Creo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica {
            get;set;
        }
    }
}