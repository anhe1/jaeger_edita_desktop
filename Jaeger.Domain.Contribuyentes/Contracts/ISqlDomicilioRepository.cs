﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// Domicilios, relacion con el directorio
    /// </summary>
    public interface ISqlDomicilioRepository : IGenericRepository<DomicilioFiscalModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        IDomicilioFiscalDetailModel Save(IDomicilioFiscalDetailModel model);
        IDomicilioFiscalModel Save(IDomicilioFiscalModel domicilio);
    }
}