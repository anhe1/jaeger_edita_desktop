﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// cartera de vendedor
    /// </summary>
    public interface ICarteraModel {
        /// <summary>
        /// otener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }
        
        /// <summary>
        /// otener o establecer indice del catalogo de vendedores
        /// </summary>
        int IdVendedor { get; set; }
        
        /// <summary>
        /// otener o establecer indice del directorio
        /// </summary>
        int IdCliente { get; set; }
        
        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        string Nota { get; set; }
        
        /// <summary>
        /// otener o establecer fecha de modificacion
        /// </summary>
        DateTime FechaModifica { get; set; }

        /// <summary>
        /// otener o establecer usuario
        /// </summary>
        string Modifica { get; set; }
    }
}