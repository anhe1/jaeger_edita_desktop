﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IContribuyenteVendedorModel : ICarteraModel {
        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        string Nombre { get; set; }

        bool SetModified { get; set; }
    }
}
