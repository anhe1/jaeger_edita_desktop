﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IVendedor2Model {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int IdVendedor { get; set; }

        /// <summary>
        /// obtener o establecer el registro activo dentro del directorio
        /// </summary>
        bool ActivoD { get; set; }

        /// <summary>
        /// obtener o establecer el id del catalogo de comisiones asigando al vendedor en el directorio
        /// </summary>
        int IdComisionD { get; set; }

        /// <summary>
        /// obtener o establecer la clave unica de registro en sistema
        /// </summary>           
        string Clave { get; set; }

        /// <summary>
        /// Desc:CURP: requerido para la expresión de la CURP del trabajador
        /// </summary>           
        string CURP { get; set; }

        /// <summary>
        /// obtener o establecer Reg. Fed. De Contribuyentes (RFC)
        /// </summary>           
        string RFC { get; set; }

        /// <summary>
        /// Desc:nombre y apellido o razon social
        /// </summary>           
        string Nombre {            get; set;            }

        /// <summary>
        /// Desc:telefonos de contacto, separados por (;)
        /// </summary>           
        string Telefono { get; set; }

        /// <summary>
        /// Desc:correo electronic, separados por (;)
        /// </summary>           
        string Correo { get; set; }

        /// <summary>
        /// obtener o establecer la relacion del directorio
        /// </summary>
        string Relacion { get; set; }

        byte[] Avatar { get; set; }
    }
}
