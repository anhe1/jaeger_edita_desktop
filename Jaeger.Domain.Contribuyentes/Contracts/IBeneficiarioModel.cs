﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// interface para beneficiarios bancarios
    /// </summary>
    public interface IBeneficiarioModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int IdDirectorio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        bool Activo {
            get; set;
        }

        /// <summary>
        /// Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)
        /// </summary>
        bool Extranjero {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        string CURP {
            get; set;
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        string RFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        string Nombre {
            get; set;
        }

        /// <summary>
        /// obtener o estabelcer direccion de correo electronico
        /// </summary>
        string Correo {
            get; set;
        }
    }
}