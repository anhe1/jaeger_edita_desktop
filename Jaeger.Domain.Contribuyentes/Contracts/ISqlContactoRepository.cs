﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// Repositorio de contactos del directorio
    /// </summary>
    public interface ISqlContactoRepository : IGenericRepository<ContactoModel> {
        /// <summary>
        /// Lista de contactos del directorio
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="conditionals">IConditional</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// Almacenar un contacto del directorio
        /// </summary>
        IContactoDetailModel Save(IContactoDetailModel contacto);
    }
}