﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// Cartera de Clientes y Vendedores
    /// </summary>
    public interface ISqlCarteraRepository : IGenericRepository<CarteraModel> {
        /// <summary>
        /// obtener listado de clientes relacionados al vendedor
        /// </summary>
        IEnumerable<T1> GetClientes<T1>(List<IConditional> conditionals) where T1 : class, new();
        
        /// <summary>
        /// obtener listado de vendedores relacionados al cliente
        /// </summary>
        IEnumerable<T1> GetVendedores<T1>(List<IConditional> conditionals) where T1 : class, new();
        
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        
        int Salveable(ICarteraModel model);
    }
}