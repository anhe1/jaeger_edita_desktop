﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface ISqlFactorDescuentoRepository : IGenericRepository<FactorPactadoModel> {
        int Salveable(IFactorPactadoModel item);
    }
}