﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IValidoRFC {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int IdDirectorio { get; set; }

        bool IsValidRFC { get; set; }

        string DomicilioFiscal { get; set; }
    }
}