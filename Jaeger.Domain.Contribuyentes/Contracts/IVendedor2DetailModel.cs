﻿using System.ComponentModel;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IVendedor2DetailModel {
        BindingList<IContribuyenteVendedorModel> Clientes { get;set; }
    }
}
