﻿using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// para crear una vista de contribuyentes que contega la direccion fiscal
    /// </summary>
    public interface IContribuyenteDomicilioSingleModel : IContribuyenteModel {

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        int IdDomicilio { get; set; }

        /// <summary>
        /// alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// </summary>
        int IdTipoDomicilio { get; set; }

        /// <summary>
        /// obetener o establecer nombre de la calle
        /// </summary>           
        string Calle { get; set; }

        string NoExterior { get; set; }

        string NoInterior { get; set; }

        string Colonia { get; set; }

        string CodigoPostal { get; set; }

        string Estado { get; set; }

        string Municipio { get; set; }

        string Pais { get; set; }

        string Localidad { get; set; }

        string Referencia { get; set; }

        string Ciudad { get; set; }

        IContribuyenteModel Contribuyente { get; }

        IDomicilioFiscalModel Domicilio { get; set; }

        object Tag { get; set; }
    }
}