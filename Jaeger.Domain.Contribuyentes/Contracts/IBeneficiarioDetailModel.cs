﻿using System.ComponentModel;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// interface de beneficiario bancarios
    /// </summary>
    interface IBeneficiarioDetailModel : IBeneficiarioModel {
        /// <summary>
        /// obtener o establecer la lista de cuentas bancarias relacionadas
        /// </summary>
        BindingList<ICuentaBancariaModel> CuentasBancarias { get; set; }
    }
}