﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    /// <summary>
    /// Repositorio de relaciones comerciales
    /// </summary>
    public interface ISqlRelacionComercialRepository : IGenericRepository<RelacionComercialModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        int Saveable(RelacionComercialDetailModel item);

        int Saveable(IRelacionComercialModel item);

        void Saveable(List<IRelacionComercialModel> item);
    }
}