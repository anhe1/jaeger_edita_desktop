﻿using System;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IRelacionComercialModel {
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla del directorio
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer indice del tipo de relacion comercial
        /// </summary>
        int IdTipoRelacion { get; set; }

        /// <summary>
        /// obtener o establecer el indice del catalogo de comisiones asignado al vendedor en las relaciones del directorio
        /// </summary>
        int IdComision { get; set; }

        string Nota { get; set; }

        string Modifica { get; set; }

        DateTime? FechaModifica { get; set; }

        IRelacionComercialModel AddRelacion(TipoRelacionComericalEnum relacion);

        IRelacionComercialModel IsActivo(bool isActive);

        IRelacionComercialModel AddIndice(int idDirectorio);
    }
}
