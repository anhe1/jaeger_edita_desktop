﻿namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IVendedorClienteDetailModel : IContribuyenteVendedorModel, ICarteraModel {

    }
}
