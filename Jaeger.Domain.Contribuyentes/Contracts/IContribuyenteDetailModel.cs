﻿using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IContribuyenteDetailModel : IContribuyenteModel {
        /// <summary>
        /// obtener o establecer indice del catalogo de tipos de regimenes fiscales, No_Definido = 0, Persona_Fisica = 1, Persona_Moral = 2 (antes DRCTR_CTLGRGF_ID)
        /// </summary>
        TipoRegimenEnum RegimenF { get; set; }

        /// <summary>
        /// obtener o establecer la lista de domicilios
        /// </summary>
        BindingList<IDomicilioFiscalDetailModel> Domicilios { get; set; }
        
        /// <summary>
        /// obtener o establecer la lista de cuentas bancarias relacionadas al contribuyentes
        /// </summary>
        BindingList<ICuentaBancariaModel> CuentasBancarias { get; set; }
        
        /// <summary>
        /// obtener o establecer la lista de contactos relacionados al contribuyente
        /// </summary>
        BindingList<IContactoDetailModel> Contactos { get; set; }
        
        /// <summary>
        /// obtener o establecer la lista de vendedores asociados
        /// </summary>
        BindingList<IContribuyenteVendedorModel> Vendedores { get; set; }
        
        /// <summary>
        /// obtener o establecer las relaciones comerciales dentro del directorio
        /// </summary>
        BindingList<IRelacionComercialDetailModel> Relaciones { get; set; }
        
        object Tag { get; set; }
    }
}