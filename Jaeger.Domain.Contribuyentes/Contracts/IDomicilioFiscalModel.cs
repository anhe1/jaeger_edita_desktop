﻿using System;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IDomicilioFiscalModel {
        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        int IdDomicilio { get; set; }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>           
        bool Activo { get; set; }

        int IdTipoDomicilio { get; set; }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        int IdContribuyente { get; set; }

        /// <summary>
        /// obtener o establecer codigo de pais
        /// </summary>           
        string CodigoPais { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>           
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>           
        string Modifica { get; set; }

        /// <summary>
        /// Desc:codigo postal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string CodigoPostal { get; set; }

        /// <summary>
        /// Desc:alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// Default:
        /// Nullable:False
        /// </summary>           
        string Tipo { get; set; }

        /// <summary>
        /// Desc:asentamiento humano
        /// Default:
        /// Nullable:True
        /// </summary>           
        string Asentamiento { get; set; }

        /// <summary>
        /// Desc:numero exterior
        /// Default:
        /// Nullable:False
        /// </summary>           
        string NoExterior { get; set; }

        /// <summary>
        /// Desc:numero interior
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string NoInterior { get; set; }

        /// <summary>
        /// Desc:calle
        /// Default:
        /// Nullable:False
        /// </summary>           
        string Calle { get; set; }

        /// <summary>
        /// Desc:colonia
        /// Default:
        /// Nullable:False
        /// </summary>           
        string Colonia { get; set; }

        /// <summary>
        /// Desc:delegacion / municipio
        /// Default:
        /// Nullable:False
        /// </summary>           
        string Municipio { get; set; }

        /// <summary>
        /// Desc:ciudad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Ciudad { get; set; }

        /// <summary>
        /// Desc:estado
        /// Default:
        /// Nullable:False
        /// </summary>           
        string Estado { get; set; }

        /// <summary>
        /// Desc:pais
        /// Default:
        /// Nullable:False
        /// </summary>           
        string Pais { get; set; }

        /// <summary>
        /// Desc:localidad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Localidad { get; set; }

        /// <summary>
        /// Desc:referencia
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Referencia { get; set; }

        /// <summary>
        /// Desc:telefonos de contacto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Telefono { get; set; }

        /// <summary>
        /// Desc:descripcion de la ubicacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Notas { get; set; }

        /// <summary>
        /// Desc:horario y requisito de acceso
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Requerimiento { get; set; }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>           
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// domicilio fiscal completo
        /// </summary>
        string Completo { get; }

        string Error { get; }
    }
}