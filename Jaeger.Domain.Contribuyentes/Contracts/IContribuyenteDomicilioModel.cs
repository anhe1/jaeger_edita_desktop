﻿using Jaeger.Domain.Contribuyentes.Entities;
using System.ComponentModel;

namespace Jaeger.Domain.Contribuyentes.Contracts {
    public interface IContribuyenteDomicilioModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer limiete de credito
        /// </summary>
        decimal Credito { get; set; }

        decimal FactorDescuento { get; set; }

        decimal FactorIvaPactado { get; set; }

        string Clave             {get; set;}

        string RFC { get; set; }

        string Nombre { get; set; }

        string Telefono { get; set; }

        string Correo { get; set; }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        string Relacion { get; set; }

        BindingList<IDomicilioFiscalDetailModel> Domicilios { get; set; }

        BindingList<IContribuyenteVendedorModel> Vendedores { get; set; }
    }
}
