﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public class DomicilioQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IDomicilioQueryBuilder, IDomicilioByIdQueryBuilder, IDomicilioActiveQueryBuilder {
        public DomicilioQueryBuilder() : base() { }

        /// <summary>
        /// obtener domicilio por el indice de la tabla
        /// </summary>
        public IDomicilioByIdQueryBuilder Id(int index) {
            this._Conditionals.Add(new Conditional("DRCCN_ID", index.ToString()));
            return this;
        }

        /// <summary>
        /// obtener domicilios relacionados al indice del directorio
        /// </summary>
        public IDomicilioByIdQueryBuilder IdDirectorio(int index) {
            this._Conditionals.Add(new Conditional("DRCCN_DRCTR_ID", index.ToString()));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public IDomicilioActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive)
                this._Conditionals.Add(new Conditional("DRCCN_A", "1"));
            return this;
        }
    }
}
