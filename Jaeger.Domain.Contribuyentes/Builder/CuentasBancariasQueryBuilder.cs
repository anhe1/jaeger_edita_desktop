﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public class CuentasBancariasQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, ICuentasBancariasIdDirectorioQueryBuilder, ICuentasBancariasQueryBuilder, ICuentasBancariasActiveQueryBuilder {
        public CuentasBancariasQueryBuilder() : base() { }

        /// <summary>
        /// obtener cuenta bancaria por indice de la tabla
        /// </summary>
        public ICuentasBancariasIdDirectorioQueryBuilder ById(int index) {
            this._Conditionals.Add(new Conditional("DRCTRB_ID", index.ToString()));
            return this;
        }

        /// <summary>
        /// obtener cuenta bancaria por indice de relacion del directorio
        /// </summary>
        public ICuentasBancariasIdDirectorioQueryBuilder IdDirectorio(int idDirectorio) {
            this._Conditionals.Add(new Conditional("DRCTRB_DRCTR_ID", idDirectorio.ToString()));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public ICuentasBancariasActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("DRCTRB_A", "1"));
            return this;
        }
    }
}
