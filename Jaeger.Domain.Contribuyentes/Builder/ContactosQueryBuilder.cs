﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public class ContactosQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IContactosQueryBuilder, IContactosQueryIdDirectorioBuilder, IContactosQueryActiveBuilder {
        public ContactosQueryBuilder() : base() { }

        /// <summary>
        /// obtener listado de contactos relacionados al id del directorio
        /// </summary>
        public IContactosQueryIdDirectorioBuilder IdDirectorio(int idDirectorio) {
            this._Conditionals.Add(new Conditional("DRCTRL_DRCTR_ID", idDirectorio.ToString()));
            return this;
        }

        /// <summary>
        /// obtener listado de contactos relacionados a una lista de id's del directorio
        /// </summary>
        private IContactosQueryIdDirectorioBuilder IdDirectorio(int[] idDirectorio) {
            // es necesario convertir a una lista separada por comas y utilizar la clausula IN
            var lista = string.Join(",", idDirectorio);
            this._Conditionals.Add(new Conditional("DRCTRL_DRCTR_ID", lista,Base.ValueObjects.ConditionalTypeEnum.In));
            return this;
        }

        /// <summary>
        /// obtener listado de contactos relacionados a una lista de id's del directorio, recibe un objeto que representa un array o un indice
        /// </summary>
        public IContactosQueryIdDirectorioBuilder IdDirectorio(object idDirectorio) {
            if (idDirectorio.GetType() == typeof(int)) {
                this.IdDirectorio((int)idDirectorio);
            } else if (idDirectorio.GetType() == typeof(int[])) {
                var real = (int[])idDirectorio;
                this.IdDirectorio(real);
            }
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public IContactosQueryActiveBuilder OnlyActive(bool onlyActive = true) {
           if (onlyActive) this._Conditionals.Add(new Conditional("DRCTRL_A", "1"));
            return this;
        }
    }
}
