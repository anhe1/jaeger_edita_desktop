﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public interface IRelacionComercialQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IRelacionComercialByIdDirectorioQueryBuilder IdDirectorio(int idDirectorio);
        IRelacionComercialByIdDirectorioQueryBuilder IdDirectorio(int[] idsDirectorio);
        IRelacionComercialByIdDirectorioQueryBuilder IdDirectorio(object index);
    }

    public interface IRelacionComercialByIdDirectorioQueryBuilder : IConditionalBuild {
        IRelacionComercialActiveDirectorioQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IRelacionComercialActiveDirectorioQueryBuilder : IConditionalBuild { }
}
