﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public class ContribuyenteBuilder : IContribuyenteBuilder, IContribuyenteItemsBuilder, IContribuyenteDomicilioBuilder {
        protected internal IContribuyenteDetailModel _Contribuyente;

        public ContribuyenteBuilder() {
            this._Contribuyente = new Entities.ContribuyenteDetailModel() { Activo = true };
        }

        public IContribuyenteItemsBuilder AddClave(string clave) {
            this._Contribuyente.Clave = clave;
            return this;
        }

        public IContribuyenteItemsBuilder AddNombreComercial(string nombreComercial) {
            this._Contribuyente.NombreComercial = nombreComercial;
            return this;
        }

        public IContribuyenteItemsBuilder AddRazonSocial(string razonSocial) {
            this._Contribuyente.Nombre = razonSocial;
            return this;
        }

        public IContribuyenteItemsBuilder AddRelacion(TipoRelacionComericalEnum relacionComerical) {
            var d0 = new Entities.RelacionComercialDetailModel {
                TipoRelacion = relacionComerical
            };
            this._Contribuyente.Relaciones.Add(d0);
            return this;
        }

        public IContribuyenteItemsBuilder AddRFC(string rfc) {
            this._Contribuyente.RFC = rfc;
            return this;
        }

        public IContribuyenteDomicilioBuilder AddDomicilio(IDomicilioFiscalDetailModel domicilio) {
            if (this._Contribuyente.Domicilios == null)
                this._Contribuyente.Domicilios = new System.ComponentModel.BindingList<IDomicilioFiscalDetailModel>();
            this._Contribuyente.Domicilios.Add(new DomicilioFiscalDetailModel());
            return this;
        }

        public IContribuyenteDetailModel Build() {
            return this._Contribuyente;
        }
    }
}
