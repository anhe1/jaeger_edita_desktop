﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public interface IContactosQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// obtener listado de contactos relacionados al id del directorio
        /// </summary>
        IContactosQueryIdDirectorioBuilder IdDirectorio(int idDirectorio);

        /// <summary>
        /// obtener listado de contactos relacionados a una lista de id's del directorio, recibe un objeto que representa un array o un indice
        /// </summary>
        IContactosQueryIdDirectorioBuilder IdDirectorio(object idDirectorio);
    }

    public interface IContactosQueryIdDirectorioBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        IContactosQueryActiveBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IContactosQueryActiveBuilder : IConditionalBuilder {

    }
}
