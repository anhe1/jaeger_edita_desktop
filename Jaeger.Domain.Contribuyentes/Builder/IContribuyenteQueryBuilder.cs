﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public interface IContribuyenteQueryBuilder {
        IContribuyenteQueryBuild ById(int index);
        IContribuyenteQueryBuild ByRFC(string rfc);
        IContribuyenteQueryBuild WithRazonSocial(string razonSocial);
        IContribuyenteQueryBuild WithRelacionComercial(TipoRelacionComericalEnum relacionComercial);
        IContribuyenteQueryBuild WithRelacionesComerciales(TipoRelacionComericalEnum[] relacionesComerciales);
        IContribuyenteQueryBuild WithRelacionesComerciales(List<ItemSelectedModel> relation);
    }

    public interface IContribuyenteQueryBuild {
        IContribuyenteQueryBuild WithRazonSocial(string razonSocial);
        IContribuyenteQueryBuild RegistroActivo(bool isActive = true);
        List<IConditional> Build();
    }
}
