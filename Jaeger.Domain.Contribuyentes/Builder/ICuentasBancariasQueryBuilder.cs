﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public interface ICuentasBancariasQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// obtener cuenta bancaria por indice de la tabla
        /// </summary>
        ICuentasBancariasIdDirectorioQueryBuilder ById(int index);

        /// <summary>
        /// obtener cuenta bancaria por indice de relacion del directorio
        /// </summary>
        ICuentasBancariasIdDirectorioQueryBuilder IdDirectorio(int idDirectorio);
    }

    public interface ICuentasBancariasIdDirectorioQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// solo registros activos
        /// </summary>
        ICuentasBancariasActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface ICuentasBancariasActiveQueryBuilder : IConditionalBuilder, IConditionalBuild { }
}
