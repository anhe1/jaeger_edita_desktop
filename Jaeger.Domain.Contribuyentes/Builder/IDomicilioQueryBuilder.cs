﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public interface IDomicilioQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// obtener domicilio por el indice de la tabla
        /// </summary>
        IDomicilioByIdQueryBuilder Id(int index);

        /// <summary>
        /// obtener domicilios relacionados al indice del directorio
        /// </summary>
        IDomicilioByIdQueryBuilder IdDirectorio(int index);
    }
    public interface IDomicilioByIdQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        IDomicilioActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IDomicilioActiveQueryBuilder : IConditionalBuilder { }
}
