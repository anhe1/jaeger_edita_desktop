﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public interface IContribuyenteBuilder {
        IContribuyenteItemsBuilder AddRFC(string rfc);
        IContribuyenteItemsBuilder AddClave(string clave);
        IContribuyenteItemsBuilder AddRazonSocial(string razonSocial);
        IContribuyenteItemsBuilder AddNombreComercial(string nombreComercial);
        IContribuyenteItemsBuilder AddRelacion(TipoRelacionComericalEnum relacionComerical);
    }

    public interface IContribuyenteItemsBuilder {
        IContribuyenteDomicilioBuilder AddDomicilio(IDomicilioFiscalDetailModel domicilio);
        IContribuyenteDetailModel Build();
    }

    public interface IContribuyenteDomicilioBuilder {

    }
}
