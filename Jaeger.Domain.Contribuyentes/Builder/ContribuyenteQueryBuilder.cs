﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public class ContribuyenteQueryBuilder : IContribuyenteQueryBuilder, IContribuyenteQueryBuild {
        #region declaraciones
        private readonly List<IConditional> _Condicionales;
        #endregion

        public ContribuyenteQueryBuilder() {
            this._Condicionales = new List<IConditional>();
        }

        public List<IConditional> Build() {
            return this._Condicionales;
        }

        public virtual IContribuyenteQueryBuild ById(int index) {
            this._Condicionales.Add(new Conditional("_drctr_id", index.ToString()));
            return this;
        }

        public virtual IContribuyenteQueryBuild ByRFC(string rfc) {
            this._Condicionales.Add(new Conditional("_drctr_rfc", rfc.ToUpper()));
            return this;
        }

        public virtual IContribuyenteQueryBuild RegistroActivo(bool isActive = true) {
            if (isActive) this._Condicionales.Add(new Conditional("_drctr_a", "1"));
            return this;
        }

        public IContribuyenteQueryBuild WithRazonSocial(string razonSocial) {
            this._Condicionales.Add(new Conditional("@search", razonSocial, ConditionalTypeEnum.Like));
            return this;
        }

        public IContribuyenteQueryBuild WithRelacionComercial(TipoRelacionComericalEnum relacionComercial) {
            var d0 = (int)relacionComercial;
            this._Condicionales.Add(new Conditional("DRCTRR_CTREL_ID", d0.ToString()));
            return this;
        }

        public IContribuyenteQueryBuild WithRelacionesComerciales(TipoRelacionComericalEnum[] relacionesComerciales) {
            int[] result = Array.ConvertAll<TipoRelacionComericalEnum, int>(relacionesComerciales, delegate (TipoRelacionComericalEnum value) { return (int)value; });
            this._Condicionales.Add(new Conditional("DRCTRR_CTREL_ID", string.Join(",", result), ConditionalTypeEnum.In));
            return this;
        }

        public IContribuyenteQueryBuild WithRelacionesComerciales(List<ItemSelectedModel> relation) {
            var relaciones = relation.Select(it => it.Id);
            this._Condicionales.Add(new Conditional("DRCTRR_CTREL_ID", string.Join(",", relaciones), ConditionalTypeEnum.In));
            return this;
        }
    }
}
