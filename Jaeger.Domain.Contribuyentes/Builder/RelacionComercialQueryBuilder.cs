﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Contribuyentes.Builder {
    public class RelacionComercialQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IRelacionComercialQueryBuilder, IRelacionComercialByIdDirectorioQueryBuilder, IRelacionComercialActiveDirectorioQueryBuilder {
        public IRelacionComercialByIdDirectorioQueryBuilder IdDirectorio(int idDirectorio) {
            this._Conditionals.Clear();
            this._Conditionals.Add(new Conditional("DRCTRR_DRCTR_ID", idDirectorio.ToString()));
            return this;
        }

        public IRelacionComercialByIdDirectorioQueryBuilder IdDirectorio(int[] idsDirectorio) {
            this._Conditionals.Clear();
            this._Conditionals.Add(new Conditional("DRCTRR_DRCTR_ID", string.Join(",", idsDirectorio), ConditionalTypeEnum.In));
            return this;
        }

        public IRelacionComercialByIdDirectorioQueryBuilder IdDirectorio(object index) {
            this._Conditionals.Clear();
            if (index.GetType() == typeof(int)) {
                this.IdDirectorio((int)index);
            } else if (index.GetType() == typeof(int[])) {
                var array = (int[])index;
                this.IdDirectorio(array);
            }
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public IRelacionComercialActiveDirectorioQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("DRCTRR_A", "1"));
            return this;
        }
    }
}
