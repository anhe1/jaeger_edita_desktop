﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Contribuyentes.Abstractions {
    public abstract class CuentaBancaria : BasePropertyChangeImplementationDataError {
        #region declaraciones
        private string banco;
        private string nombre;
        private string primerApellido;
        private string segundoApellido;
        private string clabe;
        private string clave;
        private string insitucionBancaria;
        private string moneda;
        private string numeroDeCuenta;
        private string sucursal;
        private string rFC;
        private string tipoCuenta;
        private string refNumerica;
        private string alias;
        #endregion

        public CuentaBancaria() {

        }

        /// <summary>
        /// alias de la cuenta
        /// </summary>
        public string Alias {
            get {
                return this.alias;
            }
            set {
                this.alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        public string Banco {
            get {
                return this.banco;
            }
            set {
                this.banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        public string PrimerApellido {
            get {
                return this.primerApellido;
            }
            set {
                this.primerApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        public string SegundoApellido {
            get {
                return this.segundoApellido;
            }
            set {
                this.segundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE
        /// </summary>
        public string Clabe {
            get {
                return this.clabe;
            }
            set {
                this.clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el codigo de banco según catalogo del SAT
        /// </summary>
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el nombre corto de la institución bancaria (DRCTRB_NMCRT)
        /// </summary>
        public string InsitucionBancaria {
            get {
                return this.insitucionBancaria;
            }
            set {
                this.insitucionBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda
        /// </summary>
        public string Moneda {
            get {
                return this.moneda;
            }
            set {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta bancaria
        /// </summary>
        public string NumeroDeCuenta {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la sucursal
        /// </summary>
        public string Sucursal {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del beneficiario
        /// </summary>
        public string RFC {
            get {
                return this.rFC;
            }
            set {
                this.rFC = value.ToUpper().Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        public string TipoCuenta {
            get {
                return this.tipoCuenta;
            }
            set {
                this.tipoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la referencia numerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        public string RefNumerica {
            get {
                return this.refNumerica;
            }
            set {
                this.refNumerica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
