﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Contribuyentes.Abstractions {
    public abstract class Contribuyente : Persona {
        #region declaraciones
        private int _IdDirectorio;
        private bool _Activo;
        private int _IdRegimen;
        #endregion

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de tipos de regimenes fiscales, No_Definido = 0, Persona_Fisica = 1, Persona_Moral = 2 (antes DRCTR_CTLGRGF_ID)
        /// </summary>
        public int IdRegimen {
            get { return this._IdRegimen; }
            set {
                this._IdRegimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        public new string CURP {
            get { return base.CURP; }
            set {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        public new string RFC {
            get { return base.RFC; }
            set {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        public new string Nombre {
            get { return base.Nombre; }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }
    }
}
