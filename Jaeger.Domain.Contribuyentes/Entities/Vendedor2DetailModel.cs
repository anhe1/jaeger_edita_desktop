﻿using System.ComponentModel;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    public class Vendedor2DetailModel : Vendedor2Model, IVendedor2DetailModel {
        #region declaraciones
        private BindingList<IContribuyenteVendedorModel> _Clientes;
        #endregion

        public Vendedor2DetailModel() : base() { }

        public BindingList<IContribuyenteVendedorModel> Clientes {
            get { return this._Clientes; }
            set {
                this._Clientes = value;
                this.OnPropertyChanged();
            }
        }
    }
}
