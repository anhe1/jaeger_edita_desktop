﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// tabla de relacion del directorio
    /// </summary>
    [SugarTable("DRCTRR", "tabla de relaciones del directorio")]
    public class RelacionComercialModel : BasePropertyChangeImplementation, IRelacionComercialModel {
        #region declaraciones
        private bool _Activo;
        private int _IdDirectorio;
        private int _IdTipoRelacion;
        private string _Nota;
        private string _Modifica;
        private DateTime? _FechaModifica;
        private int _IdComision;
        #endregion

        /// <summary>
        /// cosntructor
        /// </summary>
        public RelacionComercialModel() {
            this._FechaModifica = DateTime.Now;
        }

        public IRelacionComercialModel AddIndice(int idDirectorio) {
            this._IdDirectorio = idDirectorio;
            return this;
        }

        public IRelacionComercialModel AddRelacion(TipoRelacionComericalEnum relacion) {
            this.IdTipoRelacion = (int)relacion;
            return this;
        }

        public IRelacionComercialModel IsActivo(bool isActive) {
            this.Activo = isActive;
            return this;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        //[DataNames("_drctrr_id", "DRCTRR_ID")]
        //[SugarColumn(ColumnName = "_drctrr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        //public int IdRelacion {
        //    get { return this._IdRelacion; }
        //    set {
        //        this._IdRelacion = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("DRCTRR_A")]
        [SugarColumn(ColumnName = "DRCTRR_A", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1, DefaultValue = "1")]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla del directorio
        /// </summary>
        [DataNames("DRCTRR_DRCTR_ID")]
        [SugarColumn(ColumnName = "DRCTRR_DRCTR_ID", ColumnDescription = "indice de relacion con la tabla de directorio", IsNullable = false)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de relacion comercial
        /// </summary>
        [DataNames("DRCTRR_CTREL_ID")]
        [SugarColumn(ColumnName = "DRCTRR_CTREL_ID", ColumnDescription = "indice de tipo de relacion", IsNullable = false)]
        public int IdTipoRelacion {
            get { return this._IdTipoRelacion; }
            set {
                this._IdTipoRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de comisiones asignado al vendedor en las relaciones del directorio
        /// </summary>
        [DataNames("DRCTRR_CTCMS_ID")]
        [SugarColumn(ColumnName = "DRCTRR_CTCMS_ID", ColumnDescription = "id del catalogo de comisiones en el caso de ser vendedor", IsNullable = true)]
        public int IdComision {
            get { return this._IdComision; }
            set { this._IdComision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer limite de credito
        /// </summary>
        //[DataNames("_drctr_crd", "DRCTR_CRD")]
        //[SugarColumn(ColumnName = "_drctr_crd", ColumnDescription = "importe del limite de credito", IsNullable = false, Length = 14, DecimalDigits = 4)]
        //public decimal Credito {
        //    get { return this.credito; }
        //    set {
        //        this.credito = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer dias de entrega
        /// </summary>
        //[DataNames("_drctr_dsp", "DRCTR_DSP")]
        //[SugarColumn(ColumnName = "_drctr_dsp", ColumnDescription = "dias pactados de entrega", IsNullable = true)]
        //public int DiasEntrega {
        //    get { return this.diasEntrega; }
        //    set {
        //        this.diasEntrega = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer uso de CFDI predeterminado
        /// </summary>
        //[DataNames("_drctrr_usocfdi", "DRCTRR_USOCFDI")]
        //[SugarColumn(ColumnName = "_drctrr_usocfdi", ColumnDescription = "clave de uso de cfdi por default", Length = 4, IsNullable = true)]
        //public string ClaveUsoCFDI {
        //    get { return this._ClaveUsoCFDI; }
        //    set {
        //        this._ClaveUsoCFDI = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        [DataNames("DRCTRR_NOTA")]
        [SugarColumn(ColumnName = "DRCTRR_NOTA", ColumnDescription = "Nota", IsNullable = false)]
        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        //[DataNames("_drctrr_usr_n", "DRCTRR_USR_N")]
        //[SugarColumn(ColumnName = "_drctrr_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = false, IsOnlyIgnoreUpdate = true, Length = 10)]
        //public string Creo {
        //    get { return this._Creo; }
        //    set {
        //        this._Creo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        //[DataNames("_drctrr_fn", "DRCTRR_FN")]
        //[SugarColumn(ColumnName = "_drctrr_fn", ColumnDescription = "fecha de creacion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        //public DateTime FechaNuevo {
        //    get { return this._FechaNuevo; }
        //    set {
        //        this._FechaNuevo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("DRCTRR_USR_M")]
        [SugarColumn(ColumnName = "DRCTRR_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = false, IsOnlyIgnoreInsert = true, Length = 10)]
        public string Modifica {
            get { return this._Modifica; }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("DRCTRR_FM")]
        [SugarColumn(ColumnName = "DRCTRR_FM", ColumnDescription = "fecha ultima de modificacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                if (this._FechaModifica >= new DateTime(1990, 1, 1))
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
