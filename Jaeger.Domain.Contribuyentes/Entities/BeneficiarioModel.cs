﻿using System;
using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Abstractions;

namespace Jaeger.Domain.Contribuyentes.Entities {
    [SugarTable("_drctr")]
    public class BeneficiarioModel : Contribuyente, IBeneficiarioModel {
        #region declaraciones
        private bool extranjero;
        private string clave;
        private string telefono;
        private string correo;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string relacion;
        private string numRegIdTrib;
        #endregion

        public BeneficiarioModel() {
            base.Activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id", "DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public new int IdDirectorio {
            get {
                return base.IdDirectorio;
            }
            set {
                base.IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("_drctr_a", "DRCTR_A")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de tipos de regimenes fiscales, No_Definido = 0, Persona_Fisica = 1, Persona_Moral = 2 (antes DRCTR_CTLGRGF_ID)
        /// </summary>
        [DataNames("_drctr_ctrg_id", "DRCTR_CTRG_ID")]
        [SugarColumn(ColumnName = "_drctr_ctrg_id", ColumnDescription = "regimen fiscal Fisica, Moral", Length = 64, IsNullable = false, IsIgnore = true)]
        public new int IdRegimen {
            get { return base.IdRegimen; }
            set {
                base.IdRegimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)
        /// </summary>
        [DataNames("_drctr_extrnjr", "DRCTR_EXTRNJR")]
        [SugarColumn(ColumnName = "_drctr_extrnjr", ColumnDescription = "Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)", IsNullable = true, Length = 1)]
        public bool Extranjero {
            get {
                return this.extranjero;
            }
            set {
                this.extranjero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        [DataNames("_drctr_clv", "DRCTR_CLV")]
        [SugarColumn(ColumnName = "_drctr_clv", ColumnDescription = "clave de usuario", Length = 10, IsNullable = false)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        [DataNames("_drctr_curp", "DRCTR_CURP")]
        [SugarColumn(ColumnName = "_drctr_curp", ColumnDescription = "Clave Unica de Registro de Poblacion (CURP)", Length = 18, IsNullable = false)]
        public new string CURP {
            get {
                return base.CURP;
            }
            set {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("_drctr_rfc", "DRCTR_RFC")]
        [SugarColumn(ColumnName = "_drctr_rfc", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsNullable = false)]
        public new string RFC {
            get {
                return base.RFC;
            }
            set {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer número de registro de identidad fiscal
        /// </summary>
        [DataNames("_drctr_nmreg", "DRCTR_NMREG")]
        [SugarColumn(ColumnName = "_drctr_nmreg", ColumnDescription = "número de registro de identidad fiscal", Length = 40, IsNullable = true)]
        public string NumRegIdTrib {
            get {
                return this.numRegIdTrib;
            }
            set {
                this.numRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", ColumnDescription = "nombre del beneficiario", Length = 255, IsNullable = false)]
        public new string Nombre {
            get {
                return base.Nombre;
            }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.
        /// </summary>
        [DataNames("_drctr_nomc", "DRCTR_NOMC")]
        [SugarColumn(ColumnName = "_drctr_nomc", ColumnDescription = "denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.", IsNullable = true, Length = 255)]
        public new string NombreComercial {
            get { return base.NombreComercial; }
            set {
                base.NombreComercial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_drctr_domfis", "DRCTR_DOMFIS")]
        [SugarColumn(ColumnName = "_drctr_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public new string DomicilioFiscal {
            get { return base.DomicilioFiscal; }
            set {
                base.DomicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_rlcn", "DRCTR_RLCN")]
        [SugarColumn(ColumnName = "_drctr_rlcn", ColumnDescription = "relacion comercial", IsNullable = false)]
        public string Relacion {
            get {
                return this.relacion;
            }
            set {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_tel", "DRCTR_TEL")]
        [SugarColumn(ColumnName = "_drctr_tel", ColumnDescription = "telefono de contacto", Length = 64, IsNullable = false)]
        public string Telefono {
            get {
                return this.telefono;
            }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_mail", "DRCTR_MAIL")]
        [SugarColumn(ColumnName = "_drctr_mail", ColumnDescription = "correo electronico", Length = 64, IsNullable = false)]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fn", "DRCTR_FN")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_fm", "DRCTR_FM")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsNullable = false)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate) {
                    return this.fechaModifica;
                }
                return null;
            }
            set {
                this.fechaModifica = value;
            }
        }

        [DataNames("_drctr_usr_n", "DRCTR_USR_N")]
        [SugarColumn(ColumnName = "_drctr_usr_n", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drctr_usr_m", "DRCTR_USR_M")]
        [SugarColumn(ColumnName = "_drctr_usr_m", IsNullable = false, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}