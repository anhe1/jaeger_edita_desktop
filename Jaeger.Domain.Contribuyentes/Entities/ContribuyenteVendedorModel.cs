﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// cartera de clientes o vendedores
    /// </summary>
    [SugarTable("_drctrc", "vendedores: cartera de clientes")]
    public class ContribuyenteVendedorModel : CarteraModel, IContribuyenteVendedorModel, ICarteraModel {
        #region declaraciones
        private string _Clave;
        private string _RFC;
        private string _Nombre;
        #endregion

        public ContribuyenteVendedorModel() {
            this.Activo = true;
            //base.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        [DataNames("_drctr_clv", "DRCTR_CLV")]
        [SugarColumn(ColumnName = "_drctr_clv", ColumnDescription = "clave del usuario", IsNullable = false, Length = 14)]
        public string Clave {
            get { return this._Clave; }
            set {
                this._Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("_drctr_rfc", "DRCTR_RFC")]
        [SugarColumn(ColumnName = "_drctr_rfc", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsNullable = false)]
        public string RFC {
            get { return this._RFC; }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public string Nombre {
            get { return this._Nombre; }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice principal
        /// </summary>
        //[DataNames("_ctlvndc_id")]
        //[SugarColumn(ColumnName = "_ctlvndc_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        //public int Id {
        //    get { return this.index; }
        //    set { this.index = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        //[DataNames("_ctlvndc_a", "DRCTRC_A")]
        //[SugarColumn(ColumnName = "_ctlvndc_a", ColumnDescription = "registro activo", IsNullable = false, DefaultValue = "1", Length = 1)]
        //public bool Activo {
        //    get { return this.activo; }
        //    set { this.activo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer el indice de relacion del directorio de clientes
        /// </summary>
        //[DataNames("_ctlvndc_drctr_id", "DRCTRC_DRCTR_ID")]
        //[SugarColumn(ColumnName = "_ctlvndc_drctr_id", ColumnDescription = "indice del directorio", IsNullable = false)]
        //public int IdCliente {
        //    get { return this.idCliente; }
        //    set { this.idCliente = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer el indice de relacion del vendedor, 
        /// </summary>
        //[DataNames("_ctlvndc_vnddr_id", "DRCTRC_VNDR_ID")]
        //[SugarColumn(ColumnName = "_ctlvndc_vnddr_id", ColumnDescription = "indice del directorio", IsNullable = false)]
        //public int IdVendedor {
        //    get { return this.idVendedor; }
        //    set { this.idVendedor = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_ctlvndc_nota", "DRCTRC_NOTA")]
        //[SugarColumn(ColumnName = "_ctlvndc_nota", ColumnDescription = "nota", IsNullable = true, Length = 255)]
        //public string Nota {
        //    get { return this.nota; }
        //    set { this.nota = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_ctlvndc_com")]
        //[SugarColumn(ColumnName = "_ctlvndc_com", ColumnDescription = "% de comision asignada por cliente", IsNullable = false, Length = 11, DecimalDigits = 4)]
        //public decimal Comision {
        //    get { return this.comision; }
        //    set { this.comision = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_ctlvndc_usr_n")]
        //[SugarColumn(ColumnName = "_ctlvndc_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsOnlyIgnoreUpdate = true, IsNullable = false, Length = 10)]
        //public string Creo {
        //    get { return this.creo; }
        //    set { this.creo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_ctlvndc_usr_m", "DRCTRC_FM")]
        //[SugarColumn(ColumnName = "_ctlvndc_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsOnlyIgnoreInsert = true, IsNullable = true, Length = 10)]
        //public string Modifica {
        //    get { return this.modifica; }
        //    set { this.modifica = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_ctlvndc_fn")]
        //[SugarColumn(ColumnName = "_ctlvndc_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true, DefaultValue = "CURRENT_TIMESTAMP")]
        //public DateTime FechaNuevo {
        //    get { return this.fechaNuevo; }
        //    set { this.fechaNuevo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_ctlvndc_fm", "DRCTRC_USR_M")]
        //[SugarColumn(ColumnName = "_ctlvndc_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        //public DateTime? FechaModifica {
        //    get {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (this.fechaModifica >= firstGoodDate) 
        //            return this.fechaModifica;
        //        return null;
        //    }
        //    set {
        //        this.fechaModifica = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        public bool SetModified { get; set; }
    }
}
