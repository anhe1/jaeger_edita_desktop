﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Contribuyentes.Entities {
    public class DomicilioTipoModel : BaseSingleModel {
        public DomicilioTipoModel() : base() { }

        public DomicilioTipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
