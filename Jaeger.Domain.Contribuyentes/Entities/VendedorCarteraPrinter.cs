﻿using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// para impresion de cartera de clientes
    /// </summary>
    public class VendedorCarteraPrinter : Vendedor2DetailModel, IVendedor2DetailModel {
        public VendedorCarteraPrinter() : base() { }
        public VendedorCarteraPrinter(Vendedor2DetailModel model) : base() { 
            this.IdVendedor = model.IdVendedor;
            this.Nombre = model.Nombre;
            this.RFC = model.RFC;
            this.Clientes = model.Clientes;
        }
    }
}
