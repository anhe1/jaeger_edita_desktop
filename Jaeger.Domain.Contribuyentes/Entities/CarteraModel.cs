﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// cartera de vendedor
    /// </summary>
    [SugarTable("_drctrc", "Vendedores: cartera de clientes, relacion entre clientes y vendedores")]
    public class CarteraModel : BasePropertyChangeImplementation, ICarteraModel {
        #region declaraciones
        private bool _Activo;
        private int _IdVendedor;
        private int _IdDirectorio;
        private DateTime _FechaModifica;
        private string _Creo;
        private string nota;
        #endregion

        public CarteraModel() {
            this._Activo = true;
            this._FechaModifica = DateTime.Now;
        }

        /// <summary>
        /// otener o establecer registro activo
        /// </summary>
        [DataNames("_drctrc_a", "DRCTRC_A")]
        [SugarColumn(ColumnName = "_drctrr_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// otener o establecer indice del catalogo de vendedores
        /// </summary>
        [DataNames("_drctrc_vndr_id", "DRCTRC_VNDR_ID")]
        [SugarColumn(ColumnName = "_drctrc_vndr_id", ColumnDescription = "indice de relacion con la tabla de vendedores", IsNullable = false)]
        public int IdVendedor {
            get { return this._IdVendedor; }
            set {
                this._IdVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// otener o establecer indice del directorio
        /// </summary>
        [DataNames("_drctrc_drctr_id", "DRCTRC_DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctrr_nota", ColumnDescription = "indice del directorio de clientes", IsNullable = false)]
        public int IdCliente {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("_drctrc_nota", "DRCTRC_NOTA")]
        [SugarColumn(ColumnName = "_drctrr_nota", ColumnDescription = "", DefaultValue = "1", IsNullable = true)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// otener o establecer fecha de modificacion
        /// </summary>
        [DataNames("_drctrc_fm", "DRCTRC_FM")]
        [SugarColumn(ColumnName = "_drctrc_fm", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = false)]
        public DateTime FechaModifica {
            get { return this._FechaModifica; }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// otener o establecer usuario
        /// </summary>
        [DataNames("_drctrc_usr_m", "DRCTRC_USR_M")]
        [SugarColumn(ColumnName = "_drctrc_usr_m", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = false)]
        public string Modifica {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
