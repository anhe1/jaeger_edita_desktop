﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Abstractions;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// clase que representa a un contribuyente
    /// </summary>
    [SugarTable("_drctr", "directorio proveedores y clientes")]
    public class ContribuyenteModel : Contribuyente, IContribuyenteModel, IBeneficiarioModel, IPersona, IValidoRFC, IEntityBase {
        #region declaraciones
        private int idDescuento;
        private int diasEntrega;
        private bool extranjero;
        private decimal credito;
        private decimal factorDescuento;
        private decimal factorIvaPactado;
        private string clave;
        private string telefono;
        private string correo;
        private string sitioweb;
        private string relacion;
        private string nota;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModificaField;
        private string regimen;
        private string regimenFiscal;
        private string numRegIdTrib;
        private string residenciaFiscal;

        private string claveUsoCFDI;
        private string registroPatronal;
        private byte[] avatar;
        private bool _ValidaRFC;

        private string _ClaveRoll;
        private string _ApiKEY;
        private int _Principal;
        private int _IdComision;
        private int _User;
        private string _Password;
        private decimal _SobreCredito;
        private int _IdUsuario;
        private int _DiasCredito;
        private int _IdPrecio;
        private string _IdSincronizado;
        #endregion

        public ContribuyenteModel() {
            this.factorIvaPactado = new decimal(.16);
            this.Activo = true;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id", "DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public new int IdDirectorio {
            get { return base.IdDirectorio; }
            set { base.IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("_drctr_a", "DRCTR_A")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public new bool Activo {
            get { return base.Activo; }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer id de catalogo de descuentos asignado cuando es cliente
        /// </summary>
        [DataNames("_drctr_dscnt_id", "DRCTR_DSCNT_ID")]
        [SugarColumn(ColumnName = "_drctr_dscnt_id", ColumnDescription = "id de catalogo de descuentos asignado cuando es cliente", IsNullable = true, Length = 11)]
        public int IdDescuento {
            get { return this.idDescuento; }
            set {
                this.idDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de entrega
        /// </summary>
        [DataNames("_drctr_dsp", "DRCTR_DSP")]
        [SugarColumn(ColumnName = "_drctr_dsp", ColumnDescription = "dias pactados de entrega", IsNullable = true)]
        public int DiasEntrega {
            get { return this.diasEntrega; }
            set {
                this.diasEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer limite de credito para el cliente
        /// </summary>
        [DataNames("_drctr_crd", "DRCTR_CRD")]
        [SugarColumn(ColumnName = "_drctr_crd", ColumnDescription = "importe del limite de credito para el cliente", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Credito {
            get { return this.credito; }
            set {
                this.credito = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sobre credito para el cliente
        /// </summary>
        [DataNames("_drctr_scrd", "DRCTR_SCRD")]
        [SugarColumn(ColumnName = "_drctr_scrd", ColumnDescription = "sobre credito para el cliente (solo bolsa de regalo)", DecimalDigits = 4, IsNullable = true, Length = 14, IsIgnore = true)]
        public decimal SobreCredito {
            get { return this._SobreCredito; }
            set {
                this._SobreCredito = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de credito del cliente (DRCTR_DSCRD)
        /// </summary>
        [DataNames("_drctr_dscrd", "DRCTR_DSCRD")]
        [SugarColumn(ColumnName = "_drctr_dscrd", ColumnDescription = "dias de credito", IsNullable = true, IsIgnore = true)]
        public int DiasCredito {
            get { return this._DiasCredito; }
            set {
                this._DiasCredito = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor del descuento pactado
        /// </summary>
        [DataNames("_drctr_pctd", "DRCTR_PCTD")]
        [SugarColumn(ColumnName = "_drctr_pctd", ColumnDescription = "factor del descuento pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal FactorDescuento {
            get { return this.factorDescuento; }
            set {
                this.factorDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor del IVA pactado
        /// </summary>
        [DataNames("_drctr_iva", "DRCTR_IVA")]
        [SugarColumn(ColumnName = "_drctr_iva", ColumnDescription = "factor del IVA pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal FactorIvaPactado {
            get { return this.factorIvaPactado; }
            set {
                this.factorIvaPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de comisiones cuando es vendedor
        /// </summary>
        [DataNames("_drctr_cmsn_id", "DRCTR_CMSN_ID")]
        [SugarColumn(ColumnName = "_drctr_cmsn_id", ColumnDescription = "indice del catalogo de comisiones cuando es vendedor", IsNullable = true)]
        public int IdComision {
            get { return this._IdComision; }
            set {
                this._IdComision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de precios solo aplicable en bolsa de regalo es ignorado en el directorio normal
        /// </summary>
        [DataNames("_drctr_ctprc_id", "DRCTR_CTPRC_ID")]
        [SugarColumn(ColumnName = "_drctr_ctprc_id", ColumnDescription = "indice del catalogo de precios solo aplicable en bolsa de regalo es ignorado en el directorio normal", IsNullable = true, IsIgnore = true)]
        public int IdPrecio {
            get { return this._IdPrecio; }
            set {
                this._IdPrecio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)
        /// </summary>
        [DataNames("_drctr_extrnjr", "DRCTR_EXTRNJR")]
        [SugarColumn(ColumnName = "_drctr_extrnjr", ColumnDescription = "Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)", IsNullable = true, Length = 1)]
        public bool Extranjero {
            get {
                return this.extranjero;
            }
            set {
                this.extranjero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        [DataNames("_drctr_clv", "DRCTR_CLV")]
        [SugarColumn(ColumnName = "_drctr_clv", ColumnDescription = "clave del usuario", IsNullable = false, Length = 14)]
        public string Clave {
            get { return this.clave; }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer bandera para indicar si el registro ya fue validado (Nombre, RFC y Domicilio Fiscal)
        /// </summary>
        [DataNames("_drctr_rfcv", "DRCTR_RFCV")]
        [SugarColumn(ColumnName = "_drctr_rfcv", ColumnDescription = "bandera para indicar si el registro ya fue validado (Nombre, RFC y Domicilio Fiscal)", DefaultValue = "0")]
        public bool IsValidRFC {
            get { return this._ValidaRFC; }
            set {
                this._ValidaRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// minLength = 12  maxLength = 13
        /// pattern = "[A-Z&amp;Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]"
        /// </summary>
        [DataNames("_drctr_rfc", "DRCTR_RFC")]
        [SugarColumn(ColumnName = "_drctr_rfc", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsNullable = false)]
        public new string RFC {
            get { return base.RFC; }
            set { base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Única de Registro de Población (CURP)
        /// length = 18
        /// pattern = "[A-Z][AEIOUX][A-Z]{2}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[MH]([ABCMTZ]S|[BCJMOT]C|[CNPST]L|[GNQ]T|[GQS]R|C[MH]|[MY]N|[DH]G|NE|VZ|DF|SP)[BCDFGHJ-NP-TV-Z]{3}[0-9A-Z][0-9]"
        /// </summary>
        [DataNames("_drctr_curp", "DRCTR_CURP")]
        [SugarColumn(ColumnName = "_drctr_curp", ColumnDescription = "Clave Unica de Registro de Poblacion (CURP)", Length = 18, IsNullable = false)]
        public new string CURP {
            get { return base.CURP; }
            set {
                base.CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public new string Nombre {
            get { return base.Nombre; }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.
        /// </summary>
        [DataNames("_drctr_nomc", "DRCTR_NOMC")]
        [SugarColumn(ColumnName = "_drctr_nomc", ColumnDescription = "denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.", IsNullable = true, Length = 255)]
        public new string NombreComercial {
            get { return base.NombreComercial; }
            set { base.NombreComercial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero telefonico de contacto
        /// </summary>
        [DataNames("_drctr_tel", "DRCTR_TEL")]
        [SugarColumn(ColumnName = "_drctr_tel", ColumnDescription = "telefono", IsNullable = false, Length = 64)]
        public string Telefono {
            get { return this.telefono; }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estabelcer direccion de correo electronico
        /// </summary>
        [DataNames("_drctr_mail", "DRCTR_MAIL")]
        [SugarColumn(ColumnName = "_drctr_mail", ColumnDescription = "correo electronico", IsNullable = false, Length = 128)]
        public string Correo {
            get { return this.correo; }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer direccion del sitio web
        /// </summary>
        [DataNames("_drctr_web", "DRCTR_WEB")]
        [SugarColumn(ColumnName = "_drctr_web", ColumnDescription = "sitio web", IsNullable = false, Length = 64)]
        public string SitioWEB {
            get { return this.sitioweb; }
            set {
                this.sitioweb = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las etiquetas de las relaciones comerciales
        /// </summary>
        [DataNames("_drctr_rlcn", "DRCTR_RLCN")]
        [SugarColumn(ColumnName = "_drctr_rlcn", ColumnDescription = "etiquetas de relacion del directorio", IsNullable = false, Length = 64)]
        public string Relacion {
            get { return this.relacion; }
            set {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// observaciones
        /// </summary>
        [DataNames("_drctr_nts", "DRCTR_NTS")]
        [SugarColumn(ColumnName = "_drctr_nts", ColumnDescription = "observaciones", IsNullable = false, Length = 255)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer regimen fiscal (Fisica o Moral) (DRCTR_RGF)
        /// </summary>
        [DataNames("_drctr_rgf", "DRCTR_RGF")]
        [SugarColumn(ColumnName = "_drctr_rgf", ColumnDescription = "regimen fiscal Fisica, Moral", Length = 64, IsNullable = false)]
        public string Regimen {
            get { return this.regimen; }
            set { this.regimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de tipos de regimenes fiscales, No_Definido = 0, Persona_Fisica = 1, Persona_Moral = 2 (antes DRCTR_CTLGRGF_ID)
        /// </summary>
        [DataNames("_drctr_ctrg_id", "DRCTR_CTRG_ID")]
        [SugarColumn(ColumnName = "_drctr_ctrg_id", ColumnDescription = "regimen fiscal Fisica, Moral", Length = 64, IsNullable = false, IsIgnore = true)]
        public new int IdRegimen {
            get { return base.IdRegimen; }
            set { base.IdRegimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        [DataNames("_drctr_rgfsc", "DRCTR_RGFSC")]
        [SugarColumn(ColumnName = "_drctr_rgfsc", ColumnDescription = "clave de regimen fiscal", Length = 255, IsNullable = false)]
        public string RegimenFiscal {
            get { return this.regimenFiscal; }
            set {
                this.regimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer número de registro de identidad fiscal
        /// </summary>
        [DataNames("_drctr_nmreg", "DRCTR_NMREG")]
        [SugarColumn(ColumnName = "_drctr_nmreg", ColumnDescription = "número de registro de identidad fiscal", Length = 40, IsNullable = true)]
        public string NumRegIdTrib {
            get { return this.numRegIdTrib; }
            set {
                this.numRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del país de residencia para efectos fiscales del receptor del comprobante (DRCTR_RESFIS)
        /// </summary>
        [DataNames("_drctr_resfis", "DRCTR_RESFIS")]
        [SugarColumn(ColumnName = "_drctr_resfis", ColumnDescription = "clave del país de residencia para efectos fiscales del receptor del comprobante", Length = 20, IsNullable = false)]
        public string ResidenciaFiscal {
            get { return this.residenciaFiscal; }
            set {
                this.residenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_drctr_domfis", "DRCTR_DOMFIS")]
        [SugarColumn(ColumnName = "_drctr_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public new string DomicilioFiscal {
            get { return base.DomicilioFiscal; }
            set { base.DomicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer uso de CFDI predeterminado
        /// </summary>
        [DataNames("_drctr_usocfdi", "DRCTR_USOCFDI")]
        [SugarColumn(ColumnName = "_drctr_usocfdi", ColumnDescription = "clave de uso de cfdi por default", Length = 4, IsNullable = true)]
        public string ClaveUsoCFDI {
            get { return this.claveUsoCFDI; }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        #region campos por definir en el directorio
        /// <summary>
        /// obtener o establecer clave de acceso web
        /// </summary>
        [DataNames("_drctr_psw", "DRCTR_PSW")]
        [SugarColumn(ColumnName = "_drctr_psw", ColumnDescription = "password (md5 y base64)", IsNullable = true, Length = 64)]
        public string Password {
            get { return this._Password; }
            set {
                this._Password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro patronal
        /// </summary>
        [DataNames("_drctr_rgstrptrnl", "DRCTR_RGSTRPTRNL")]
        [SugarColumn(ColumnName = "_drctr_rgstrptrnl", ColumnDescription = "registro patronal", IsNullable = false, Length = 12)]
        public string RegistroPatronal {
            get { return this.registroPatronal; }
            set {
                this.registroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de roll (DRCTR_CVROLL)
        /// </summary>
        [DataNames("_drctr_cvroll", "DRCTR_CVROLL")]
        [SugarColumn(ColumnName = "_drctr_cvroll", ColumnDescription = "clave de roll", IsNullable = true, Length = 32, IsIgnore = true)]
        public string ClaveRoll {
            get { return this._ClaveRoll; }
            set { this._ClaveRoll = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave APIKEY para el servicio web (DRCTR_CVROLL)
        /// </summary>
        [DataNames("_drctr_apikey", "DRCTR_APIKEY")]
        [SugarColumn(ColumnName = "_drctr_apikey", ColumnDescription = "APIKEY", IsNullable = true, Length = 32, IsIgnore = true)]
        public string ApiKEY {
            get { return this._ApiKEY; }
            set {
                this._ApiKEY = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave APIKEY para el servicio web (DRCTR_CVROLL)
        /// </summary>
        [DataNames("_drctr_prncpl", "DRCTR_PRNCPL")]
        [SugarColumn(ColumnName = "_drctr_prncpl", ColumnDescription = "principal", IsNullable = true, IsIgnore = true)]
        public int Principal {
            get { return this._Principal; }
            set {
                this._Principal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave APIKEY para el servicio web (DRCTR_CVROLL)
        /// </summary>
        [DataNames("_drctr_usr", "DRCTR_USR")]
        [SugarColumn(ColumnName = "_drctr_usr", ColumnDescription = "id del usuario", IsNullable = true, Length = 32)]
        public int User {
            get { return this._User; }
            set {
                this._User = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("_drctr_usr_id", "DRCTR_USR_ID")]
        [SugarColumn(ColumnName = "_drctr_usr_id", ColumnDescription = "", IsNullable = true, IsIgnore = true)]
        public int IdUsuario {
            get { return this._IdUsuario; }
            set {
                this._IdUsuario = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer indice de sincronizacion con otros directorios
        /// </summary>
        [DataNames("_drctr_sync_id", "DRCTR_SYNC_ID")]
        [SugarColumn(ColumnName = "_drctr_sync_id", ColumnDescription = "clave de roll", IsNullable = true, IsIgnore = true)]
        public string IdSincronizado {
            get { return this._IdSincronizado; }
            set {
                this._IdSincronizado = value;
                this.OnPropertyChanged();
            }
        }

        [Obsolete]
        [DataNames("_drctr_file", "DRCTR_FILE")]
        [SugarColumn(ColumnName = "_drctr_file", IsNullable = true, IsIgnore = true)]
        public byte[] Avatar {
            get { return this.avatar; }
            set {
                this.avatar = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region creacion del registro
        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("_drctr_usr_n", "DRCTR_USR_N")]
        [SugarColumn(ColumnName = "_drctr_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = false, IsOnlyIgnoreUpdate = true, Length = 10)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_drctr_fn", "DRCTR_FN")]
        [SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_drctr_usr_m", "DRCTR_USR_M")]
        [SugarColumn(ColumnName = "_drctr_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = false, IsOnlyIgnoreInsert = true, Length = 10)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_drctr_fm", "DRCTR_FM")]
        [SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate) {
                    return this.fechaModificaField;
                }
                return null;
            }
            set {
                this.fechaModificaField = value;
            }
        }
        #endregion
    }
}
