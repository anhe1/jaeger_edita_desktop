﻿using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    [SugarTable("_drctr")]
    public class ContribuyenteDomicilioModel : BasePropertyChangeImplementation, IContribuyenteDomicilioModel {
        #region declaraciones
        private int index;
        private bool activo;
        private decimal credito;
        private decimal factorDescuento;
        private decimal factorIvaPactado;
        private string clave;
        private string rFC;
        private string nombre;
        private string telefono;
        private string correo;
        private BindingList<IDomicilioFiscalDetailModel> domicilios;
        private BindingList<IContribuyenteVendedorModel> vendedores;
        private string relacion;
        #endregion

        public ContribuyenteDomicilioModel() { }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id", "DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id {
            get {
                return this.index;
            }
            set {
                if (this.index != value) {
                    this.index = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("_drctr_a", "DRCTR_A")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                if (this.activo != value) {
                    this.activo = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer limiete de credito
        /// </summary>
        [DataNames("_drctr_crd", "DRCTR_CRD")]
        [SugarColumn(ColumnName = "_drctr_crd", IsNullable = false)]
        public decimal Credito {
            get {
                return this.credito;
            }
            set {
                if (this.credito != value) {
                    this.credito = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_pctd", "DRCTR_PCTD")]
        [SugarColumn(ColumnName = "_drctr_pctd", IsNullable = false)]
        public decimal FactorDescuento {
            get {
                return this.factorDescuento;
            }
            set {
                if (this.factorDescuento != value) {
                    this.factorDescuento = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_iva", "DRCTR_IVA")]
        [SugarColumn(ColumnName = "_drctr_iva", IsNullable = false)]
        public decimal FactorIvaPactado {
            get {
                return this.factorIvaPactado;
            }
            set {
                if (this.factorIvaPactado != value) {
                    this.factorIvaPactado = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_clv", "DRCTR_CLV")]
        [SugarColumn(ColumnName = "_drctr_clv", IsNullable = false)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                if (this.clave != value) {
                    this.clave = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_rfc", "DRCTR_RFC")]
        [SugarColumn(ColumnName = "_drctr_rfc", IsNullable = false)]
        public string RFC {
            get {
                return this.rFC;
            }
            set {
                if (this.rFC != value) {
                    this.rFC = value.ToUpper().Trim();
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                if (this.nombre != value) {
                    this.nombre = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_tel", "DRCTR_TEL")]
        [SugarColumn(ColumnName = "_drctr_tel", IsNullable = false)]
        public string Telefono {
            get {
                return this.telefono;
            }
            set {
                if (this.telefono != value) {
                    this.telefono = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_drctr_mail", "DRCTR_MAIL")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                if (this.correo != value) {
                    this.correo = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Desc:relacion comercial con la empresa (27=Cliente,163=Proveedor)
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drctr_rlcn", "DRCTR_RLCN")]
        [SugarColumn(ColumnName = "_drctr_rlcn", IsNullable = false)]
        public string Relacion {
            get {
                return this.relacion;
            }
            set {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<IDomicilioFiscalDetailModel> Domicilios {
            get {
                return this.domicilios;
            }
            set {
                this.domicilios = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<IContribuyenteVendedorModel> Vendedores {
            get {
                return this.vendedores;
            }
            set {
                this.vendedores = value;
                this.OnPropertyChanged();
            }
        }
    }
}
