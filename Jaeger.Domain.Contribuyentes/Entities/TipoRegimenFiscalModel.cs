﻿namespace Jaeger.Domain.Contribuyentes.Entities {
    public class TipoRegimenFiscalModel {

        public TipoRegimenFiscalModel() {
            this.Activo = 1;
        }

        public TipoRegimenFiscalModel(int id, string descripcion) {
            this.Id = id;
            this.Activo = 1;
            this.Descripcion = descripcion;
        }

        public TipoRegimenFiscalModel(int id, int activo, string descripcion) {
            this.Id = id;
            this.Activo = activo;
            this.Descripcion = descripcion;
        }

        public int Id { get; set; }

        public int Activo { get; set; }

        public string Descripcion { get; set; }
    }
}
