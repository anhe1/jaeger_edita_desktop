﻿using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// interface de beneficiario bancarios
    /// </summary>
    [SugarTable("_drctr")]
    public class BeneficiarioDetailModel : BeneficiarioModel, IBeneficiarioDetailModel, IBeneficiarioModel {
        public BeneficiarioDetailModel() : base() {
            this.Relacion = "Beneficiario";
        }

        /// <summary>
        /// obtener o establecer la lista de cuentas bancarias relacionadas
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ICuentaBancariaModel> CuentasBancarias {
            get; set;
        }
    }
}