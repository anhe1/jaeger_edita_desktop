﻿using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// contacto del directorio
    /// </summary>
    [SugarTable("DRCTRL", "directorio: contactos del directorio")]
    public class ContactoDetailModel : ContactoModel, IContactoDetailModel {
        public ContactoDetailModel() : base() {}
    }
}
