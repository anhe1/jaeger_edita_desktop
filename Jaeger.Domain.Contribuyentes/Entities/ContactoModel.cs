﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// contacto del directorio
    /// </summary>
    [SugarTable("DRCTRL", "directorio: contactos del directorio")]
    public class ContactoModel : BasePropertyChangeImplementation, IContactoModel, Base.Contracts.IEntityBase {
        #region declaraciones
        private int index;
        private bool activo;
        private int subIndex;
        private string nombre;
        private string creo;
        private string modifica;
        private string correo;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string telefono;
        private string _TratoSugerido;
        private string _Puesto;
        private string _Nota;
        private string _TelefonoTipo;
        private string _CorreoTipo;
        #endregion

        public ContactoModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        [DataNames("DRCTRL_ID")]
        [SugarColumn(ColumnName = "DRCTRL_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdContacto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("DRCTRL_A")]
        [SugarColumn(ColumnName = "DRCTRL_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el directorio
        /// </summary>
        [DataNames("DRCTRL_DRCTR_ID")]
        [SugarColumn(ColumnName = "DRCTRL_DRCTR_ID", ColumnDescription = "indice de relacion con el directorio")]
        public int IdDirectorio {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del contacto
        /// </summary>
        [DataNames("DRCTRL_NOM")]
        [SugarColumn(ColumnName = "DRCTRL_NOM", ColumnDescription = "nombre del contacto", Length = 128)]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// trato sugerido
        /// </summary>
        [DataNames("DRCTRL_SUG")]
        [SugarColumn(ColumnName = "DRCTRL_SUG", ColumnDescription = "trato sugerido", Length = 32)]
        public string TratoSugerido {
            get { return this._TratoSugerido; }
            set { this._TratoSugerido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// puesto
        /// </summary>
        [DataNames("DRCTRL_PST")]
        [SugarColumn(ColumnName = "DRCTRL_PST", ColumnDescription = "puestp", Length = 128)]
        public string Puesto {
            get { return this._Puesto; }
            set { this._Puesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// detalles
        /// </summary>
        [DataNames("DRCTRL_NOTA")]
        [SugarColumn(ColumnName = "DRCTRL_NOTA", ColumnDescription = "detalles", Length = 128)]
        public string Nota {
            get { return this._Nota; }
            set { this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de telefono
        /// </summary>
        [DataNames("DRCTRL_TTEL")]
        [SugarColumn(ColumnName = "DRCTRL_TTEL", ColumnDescription = "telefono tipo", Length = 16)]
        public string TelefonoTipo {
            get { return this._TelefonoTipo; }
            set { this._TelefonoTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de correo electronico
        /// </summary>
        [DataNames("DRCTRL_TMAIL")]
        [SugarColumn(ColumnName = "DRCTRL_TMAIL", ColumnDescription = "correo tipo", Length = 16)]
        public string CorreoTipo {
            get { return this._CorreoTipo; }
            set { this._CorreoTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// telefono
        /// </summary>
        [DataNames("DRCTRL_TEL")]
        [SugarColumn(ColumnName = "DRCTRL_TEL", ColumnDescription = "telefono", Length = 64)]
        public string Telefono {
            get {
                return this.telefono;
            }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// correo electronico
        /// </summary>
        [DataNames("DRCTRL_MAIL")]
        [SugarColumn(ColumnName = "DRCTRL_MAIL", ColumnDescription = "e-mail", Length = 64)]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha especial
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string FechaEspecial {
            get; set;
        }

        #region creacion del registro
        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("DRCTRL_FN")]
        [SugarColumn(ColumnName = "DRCTRL_FN", ColumnDescription = "")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("DRCTRL_USR_N")]
        [SugarColumn(ColumnName = "DRCTRL_USR_N", ColumnDescription = "usuario creo", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("DRCTRL_USR_M")]
        [SugarColumn(ColumnName = "DRCTRL_USR_M", ColumnDescription = "ultimo usuario que modifico", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("DRCTRL_FM")]
        [SugarColumn(ColumnName = "DRCTRL_FM", ColumnDescription = "", IsOnlyIgnoreInsert = true, IsOnlyIgnoreUpdate = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
