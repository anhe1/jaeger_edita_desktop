﻿using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// relacion del directorio
    /// </summary>
    [SugarTable("DRCTRR", "tabla de relaciones del directorio")]
    public class RelacionComercialDetailModel : RelacionComercialModel, IRelacionComercialDetailModel, IRelacionComercialModel {
        /// <summary>
        /// constructor
        /// </summary>
        public RelacionComercialDetailModel() : base() {

        }

        public RelacionComercialDetailModel(int indice, string descripcion) : base() {
            base.IdTipoRelacion = indice;
            //this.Name = descripcion;
        }

        public RelacionComercialDetailModel(int indice, bool activo, string descripcion) : base() {
            base.IdTipoRelacion = indice;
            base.Activo = activo;
        }

        #region propiedades
        [SugarColumn(IsIgnore = true)]
        public TipoRelacionComericalEnum TipoRelacion {
            get { return (TipoRelacionComericalEnum)base.IdTipoRelacion; }
            set {
                base.IdTipoRelacion = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Name {
            get { return ((TipoRelacionComericalEnum)base.IdTipoRelacion).ToString(); }
        }

        [SugarColumn(IsIgnore = true)]
        public bool Selected {
            get { return base.Activo; }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
