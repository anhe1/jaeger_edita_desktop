﻿/// develop:
/// purpose: para crear una vista de contribuyentes que contega la direccion fiscal
using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// para crear una vista de contribuyentes que contega la direccion fiscal
    /// </summary>
    [SugarTable("_drctr")]
    public class ContribuyenteDomicilioSingleModel : ContribuyenteModel, IContribuyenteDomicilioSingleModel, IContribuyenteModel {

        public ContribuyenteDomicilioSingleModel() {
            this.Domicilio = new DomicilioFiscalModel();
        }

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        [DataNames("_drccn_id", "DRCCN_ID")]
        [SugarColumn(ColumnName = "_drccn_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDomicilio {
            get { return this.Domicilio.IdDomicilio; }
            set { this.Domicilio.IdDomicilio = value; }
        }

        /// <summary>
        /// alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// </summary>
        [DataNames("_drccn_ctdrc_id", "DRCCN_CTDRC_ID")]
        [SugarColumn(ColumnName = "_drccn_ctdrc_id", ColumnDescription = "alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)", IsNullable = true)]
        public int IdTipoDomicilio {
            get { return this.Domicilio.IdTipoDomicilio; }
            set { this.Domicilio.IdTipoDomicilio = value; }
        }

        /// <summary>
        /// obetener o establecer nombre de la calle
        /// </summary>           
        [DataNames("_drccn_cll", "DRCCN_CLL")]
        [SugarColumn(ColumnName = "_drccn_cll", ColumnDescription = "calle", IsNullable = true, Length = 0)]
        public string Calle {
            get {
                return this.Domicilio.Calle;
            }
            set {
                this.Domicilio.Calle = value;
            }
        }

        [DataNames("_drccn_extr", "DRCCN_EXTR")]
        public string NoExterior {
            get {
                return this.Domicilio.NoExterior;
            }
            set {
                this.Domicilio.NoExterior = value;
            }
        }

        [DataNames("_drccn_intr", "DRCCN_INTR")]
        public string NoInterior {
            get {
                return this.Domicilio.NoInterior;
            }
            set {
                this.Domicilio.NoInterior = value;
            }
        }

        [DataNames("_drccn_cln", "DRCCN_CLN")]
        public string Colonia {
            get {
                return this.Domicilio.Colonia;
            }
            set {
                this.Domicilio.Colonia = value;
            }
        }

        [DataNames("_drccn_cp", "DRCCN_CP")]
        public string CodigoPostal {
            get {
                return this.Domicilio.CodigoPostal;
            }
            set {
                this.Domicilio.CodigoPostal = value;
            }
        }

        [DataNames("_drccn_std", "DRCCN_STD")]
        public string Estado {
            get {
                return this.Domicilio.Estado;
            }
            set {
                this.Domicilio.Estado = value;
            }
        }

        [DataNames("_drccn_dlg", "DRCCN_DLG")]
        public string Municipio {
            get {
                return this.Domicilio.Municipio;
            }
            set {
                this.Domicilio.Municipio = value;
            }
        }

        [DataNames("_drccn_ps", "DRCCN_PS")]
        public string Pais {
            get {
                return this.Domicilio.Pais;
            }
            set {
                this.Domicilio.Pais = value;
            }
        }

        [DataNames("_drccn_lcldd", "DRCCN_LCLDD")]
        public string Localidad {
            get {
                return this.Domicilio.Localidad;
            }
            set {
                this.Domicilio.Localidad = value;
            }
        }

        [DataNames("_drccn_rfrnc", "DRCCN_RFRNC")]
        public string Referencia {
            get {
                return this.Domicilio.Referencia;
            }
            set {
                this.Domicilio.Referencia = value;
            }
        }

        [DataNames("_drccn_cdd", "DRCCN_CDD")]
        public string Ciudad {
            get {
                return this.Domicilio.Ciudad;
            }
            set {
                this.Domicilio.Ciudad = value;
            }
        }

        public IContribuyenteModel Contribuyente {
            get { return this; }
        }

        public IDomicilioFiscalModel Domicilio { get; set; }

        public object Tag { get; set; }
    }
}
