﻿using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    public class DomicilioFiscalDetailModel : DomicilioFiscalModel, IDomicilioFiscalModel, IDomicilioFiscalDetailModel {
        public DomicilioFiscalDetailModel() : base() { }

        /// <summary>
        /// obtener o establecer el tipo de domicilio registrado, esta campo modifica modo numerico utilizado en CPLite
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoDomicilioEnum TipoDomicilio {
            get { return (TipoDomicilioEnum)base.IdTipoDomicilio; }
            set {
                base.IdTipoDomicilio = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new int IdTipoDomicilio {
            get {
                if (base.IdTipoDomicilio == 0) {
                    if (!string.IsNullOrEmpty(base.Tipo)) {
                        if (base.Tipo == "No Definido")
                            return 0;
                        return (int)EnumerationExtension.ParseEnum<TipoDomicilioEnum>(base.Tipo);
                    }
                }
                return base.IdTipoDomicilio;
            }
            set {
                base.IdTipoDomicilio = value;
                base.Tipo = EnumerationExtension.Description((TipoDomicilioEnum)value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para el boton de autorizacion de la direccion 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string AutorizadoText {
            get {
                if (this.IdAutorizado == 1)
                    return "Autorizado";
                return "Pendiente";
            }
        }
    }
}
