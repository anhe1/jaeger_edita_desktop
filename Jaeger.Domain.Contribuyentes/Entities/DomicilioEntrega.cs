﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Jaeger.Domain.Contribuyentes.Entities {
    [JsonObject("domicilioFiscal")]
    public class DomicilioEntrega : DomicilioFiscalModel {
        [Browsable(false)]
        [JsonIgnore]
        public int Id {
            get {
                return base.IdDomicilio;
            }
            set {
                base.IdDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public int SubId {
            get {
                return base.IdContribuyente;
            }
            set {
                base.IdContribuyente = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Calle")]
        [JsonProperty("calle")]
        public new string Calle {
            get {
                return base.Calle;
            }
            set {
                base.Calle = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Ciudad")]
        [JsonProperty("ciudad")]
        public new string Ciudad {
            get {
                return base.Ciudad;
            }
            set {
                base.Ciudad = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Codigo de País")]
        [JsonProperty("codigoPais")]
        public new string CodigoPais {
            get {
                return base.CodigoPais;
            }
            set {
                base.CodigoPais = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Codigo Postal")]
        [JsonProperty("cp")]
        public new string CodigoPostal {
            get {
                return base.CodigoPostal;
            }
            set {
                base.CodigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Colonia")]
        [JsonProperty("col")]
        public new string Colonia {
            get {
                return base.Colonia;
            }
            set {
                base.Colonia = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Delegación ó Municipio")]
        [JsonProperty("deleg")]
        public string Delegacion {
            get {
                return base.Municipio;
            }
            set {
                base.Municipio = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Estado")]
        [JsonProperty("estado")]
        public new string Estado {
            get {
                return base.Estado;
            }
            set {
                base.Estado = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("No. Exterior")]
        [JsonProperty("noExterior")]
        public new string NoExterior {
            get {
                return base.NoExterior;
            }
            set {
                base.NoExterior = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("No. Interior")]
        [JsonProperty("noInterior")]
        public new string NoInterior {
            get {
                return base.NoInterior;
            }
            set {
                base.NoInterior = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("País")]
        [JsonProperty("Pais")]
        public new string Pais {
            get {
                return base.Pais;
            }
            set {
                base.Pais = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Localidad")]
        [JsonProperty("localidad")]
        public new string Localidad {
            get {
                return base.Localidad;
            }
            set {
                base.Localidad = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Referencia")]
        [JsonProperty("referencia")]
        public new string Referencia {
            get {
                return base.Referencia;
            }
            set {
                base.Referencia = value;
                this.OnPropertyChanged();
            }
        }

        //[JsonIgnore]
        //public new TipoDomicilioEnum TipoEnum {
        //    get {
        //        return base.TipoEnum;
        //    }
        //    set {
        //        base.TipoEnum = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// Desc:alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Browsable(false)]
        [JsonIgnore]
        public new string Tipo {
            get {
                return base.Tipo;
            }
            set {
                base.Tipo = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public new DateTime FechaNuevo {
            get {
                return base.FechaNuevo;
            }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public new DateTime? FechaModifica {
            get {
                return base.FechaModifica;
            }
            set {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public new string Creo {
            get {
                return base.Creo;
            }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        public new string Modifica {
            get {
                return base.Modifica;
            }
            set {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Texto {
            get {
                return this.ToString();
            }
        }

        public override string ToString() {
            string direccion = string.Empty;

            if (!(string.IsNullOrEmpty(this.Calle)))
                direccion = string.Concat("Calle ", this.Calle);

            if (!(string.IsNullOrEmpty(this.NoExterior)))
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);

            if (!(string.IsNullOrEmpty(this.NoInterior)))
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);

            if (!(string.IsNullOrEmpty(this.Colonia)))
                direccion = string.Concat(direccion, " Col. ", this.Colonia);

            if (!(string.IsNullOrEmpty(this.CodigoPostal)))
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);

            if (!(string.IsNullOrEmpty(this.Delegacion)))
                direccion = string.Concat(direccion, " ", this.Delegacion);

            if (!(string.IsNullOrEmpty(this.Estado)))
                direccion = string.Concat(direccion, ", ", this.Estado);

            if (!(string.IsNullOrEmpty(this.Referencia)))
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);

            if (!(string.IsNullOrEmpty(this.Localidad)))
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);

            return direccion;
        }

        public string Json(Formatting objFormat = 0) {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public static DomicilioEntrega Json(string inputString) {
            try {
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                return JsonConvert.DeserializeObject<DomicilioEntrega>(inputString, conf);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
