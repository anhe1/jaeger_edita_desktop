﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Abstractions;

namespace Jaeger.Domain.Contribuyentes.Entities.V2 {
    /// <summary>
    /// cuenta bancaria
    /// </summary>
    [SugarTable("DRCTRB", "directorio: cuentas bancarias relacionadas al directorio")]
    public class CuentaBancariaModel : CuentaBancaria, IDataErrorInfo, ICuentaBancariaModel, Base.Contracts.IEntityBase {
        #region declaraciones
        private int index;
        private int subId;
        private bool activo;
        private decimal cargoMaximo;
        private string refAlfanumerica;
        private bool verificado;
        private DateTime fechaNuevo;
        private DateTime? fechaModificaField;
        private string creo;
        private string modifica;
        #endregion

        public CuentaBancariaModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// indice de la cuenta
        /// </summary>
        [DataNames("_dcbnc_id", "DRCTRB_ID")]
        [SugarColumn(ColumnName = "DRCTRB_ID", ColumnDescription = "indice de cuenta", IsIdentity = true, IsPrimaryKey = true, Length = 11)]
        public int IdCuenta {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("_dcbnc_drctr_id", "DRCTRB_DRCTR_ID")]
        [SugarColumn(ColumnName = "DRCTRB_DRCTR_ID", ColumnDescription = "indice", IsNullable = true, Length = 11)]
        public int IdDirectorio {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("_dcbnc_a", "DRCTRB_A")]
        [SugarColumn(ColumnName = "DRCTRB_A", ColumnDescription = "registro activo", IsNullable = true, Length = 1)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// alias de la cuenta
        /// </summary>
        [DataNames("_dcbnc_alias", "DRCTRB_ALIAS")]
        [SugarColumn(ColumnName = "DRCTRB_ALIAS", ColumnDescription = "alias de la cuenta", IsNullable = true, Length = 255)]
        public new string Alias {
            get {
                return base.Alias;
            }
            set {
                base.Alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [DataNames("_dcbnc_banco", "DRCTRB_BANCO")]
        [SugarColumn(ColumnName = "DRCTRB_BANCO", ColumnDescription = "nombre del banco", IsNullable = true, Length = 255)]
        public new string Banco {
            get {
                return base.Banco;
            }
            set {
                base.Banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [DataNames("_dcbnc_bnfcr", "DRCTRB_BNFCR")]
        [SugarColumn(ColumnName = "DRCTRB_BNFCR", ColumnDescription = "nombre(s)", IsNullable = true, Length = 300)]
        public new string Nombre {
            get {
                return base.Nombre;
            }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        [DataNames("_dcbnc_prapll", "DRCTRB_PRAPLL")]
        [SugarColumn(ColumnName = "DRCTRB_PRAPLL", ColumnDescription = "apellido paterno", IsNullable = true, Length = 125)]
        public new string PrimerApellido {
            get {
                return base.PrimerApellido;
            }
            set {
                base.PrimerApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        [DataNames("_dcbnc_sgapll", "DRCTRB_SGAPLL")]
        [SugarColumn(ColumnName = "DRCTRB_SGAPLL", ColumnDescription = "apellido materno", IsNullable = true, Length = 125)]
        public new string SegundoApellido {
            get {
                return base.SegundoApellido;
            }
            set {
                base.SegundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        [DataNames("_dcbnc_crgmx", "DRCTRB_CRGMX")]
        [SugarColumn(ColumnName = "DRCTRB_CRGMX", ColumnDescription = "cargo maximo", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal CargoMaximo {
            get {
                return this.cargoMaximo;
            }
            set {
                this.cargoMaximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE
        /// </summary>
        [DataNames("_dcbnc_cntclb", "DRCTRB_CNTCLB")]
        [SugarColumn(ColumnName = "DRCTRB_CNTCLB", ColumnDescription = "cuenta clabe", IsNullable = true, Length = 50)]
        public new string Clabe {
            get {
                return base.Clabe;
            }
            set {
                base.Clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el codigo de banco según catalogo del SAT
        /// </summary>
        [DataNames("_dcbnc_clv", "DRCTRB_CLV")]
        [SugarColumn(ColumnName = "DRCTRB_CLV", ColumnDescription = "codigo de banco según catalogo del SAT", IsNullable = true, Length = 4)]
        public new string Clave {
            get {
                return base.Clave;
            }
            set {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el nombre corto de la institución bancaria (DRCTRB_NMCRT)
        /// </summary>
        [DataNames("_dcbnc_nmcrt", "DRCTRB_NMCRT")]
        [SugarColumn(ColumnName = "DRCTRB_NMCRT", ColumnDescription = "nombre corto de la institución bancaria", IsNullable = true, Length = 64)]
        public new string InsitucionBancaria {
            get {
                return base.InsitucionBancaria;
            }
            set {
                base.InsitucionBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda
        /// </summary>
        [DataNames("_dcbnc_mnd", "DRCTRB_MND")]
        [SugarColumn(ColumnName = "DRCTRB_MND", ColumnDescription = "clave de moneda", IsNullable = true, Length = 10)]
        public new string Moneda {
            get {
                return base.Moneda;
            }
            set {
                base.Moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta bancaria
        /// </summary>
        [DataNames("_dcbnc_nmcta", "DRCTRB_NMCTA")]
        [SugarColumn(ColumnName = "DRCTRB_NMCTA", ColumnDescription = "numero de cuenta bancaria", IsNullable = true, Length = 50)]
        public new string NumeroDeCuenta {
            get {
                return base.NumeroDeCuenta;
            }
            set {
                base.NumeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la sucursal
        /// </summary>
        [DataNames("_dcbnc_scrsl", "DRCTRB_SCRSL")]
        [SugarColumn(ColumnName = "DRCTRB_SCRSL", ColumnDescription = "numero de la sucursal", IsNullable = true, Length = 20)]
        public new string Sucursal {
            get {
                return base.Sucursal;
            }
            set {
                base.Sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del beneficiario
        /// </summary>
        [DataNames("_dcbnc_rfc", "DRCTRB_RFC")]
        [SugarColumn(ColumnName = "DRCTRB_RFC", ColumnDescription = "RFC del beneficiario", IsNullable = true, Length = 14)]
        public new string RFC {
            get {
                return base.RFC;
            }
            set {
                base.RFC = value.ToUpper().Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        [DataNames("_dcbnc_tipo", "DRCTRB_TIPO")]
        [SugarColumn(ColumnName = "DRCTRB_TIPO", ColumnDescription = "tipo de cuenta", IsNullable = true, Length = 20)]
        public new string TipoCuenta {
            get {
                return base.TipoCuenta;
            }
            set {
                base.TipoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la referencia numerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        [DataNames("_dcbnc_refnum", "DRCTRB_REFNUM")]
        [SugarColumn(ColumnName = "DRCTRB_REFNUM", ColumnDescription = "referencia numerica constante utilizada por el beneficiario de la cuenta", IsNullable = true, Length = 7)]
        public new string RefNumerica {
            get {
                return base.RefNumerica;
            }
            set {
                base.RefNumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la referencia alfanumerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        [DataNames("_dcbnc_refalf", "DRCTRB_REFALF")]
        [SugarColumn(ColumnName = "DRCTRB_REFALF", ColumnDescription = "referencia alfanumerica constante utilizada por el beneficiario de la cuenta", IsNullable = true, Length = 20)]
        public string RefAlfanumerica {
            get {
                return this.refAlfanumerica;
            }
            set {
                if (this.Verificado == false)
                    this.refAlfanumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si la cuenta ya se encuentra verificada
        /// </summary>
        [DataNames("_dcbnc_vrfcd", "DRCTRB_VRFCD")]
        [SugarColumn(ColumnName = "DRCTRB_VRFCD", ColumnDescription = "verificacion de la cuenta", IsNullable = true, Length = 1)]
        public bool Verificado {
            get {
                return this.verificado;
            }
            set {
                this.verificado = value;
                this.OnPropertyChanged();
            }
        }

        #region creacion del registro
        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("_dcbnc_usr_n", "DRCTRB_USR_N")]
        [SugarColumn(ColumnName = "DRCTRB_USR_N", ColumnDescription = "clave del usuario que crea el registro", IsNullable = true, Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del nuevo registro
        /// </summary>
        [DataNames("_dcbnc_fn", "DRCTRB_FN")]
        [SugarColumn(ColumnName = "DRCTRB_FN", ColumnDescription = "fecha del nuevo registro", IsNullable = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_dcbnc_usr_m", "DRCTRB_USR_M")]
        [SugarColumn(ColumnName = "DRCTRB_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true, Length = 20)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_dcbnc_fm", "DRCTRB_FM")]
        [SugarColumn(ColumnName = "DRCTRB_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate) {
                    return this.fechaModificaField;
                }
                return null;
            }
            set {
                this.fechaModificaField = value;
            }
        }
        #endregion

        #region metodos

        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (!this.RegexValido(this.Clabe, "[0-9]{10,18}$")) {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "Clabe" && !this.RegexValido(this.Clabe, "[0-9]{10,18}$")) {
                    return "Formato de CLABE no válido!";
                }
                return string.Empty;
            }
        }

        #endregion
    }
}
