﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities.V2 {

    [SugarTable("DRCCN", "directorio: domicilios")]
    public class DomicilioFiscalModel : BasePropertyChangeImplementation, IDomicilioFiscalModel, Base.Contracts.IEntityBase, IDataErrorInfo {
        #region declaraciones
        private int index;
        private bool isActive;
        private int subId;
        private string codigoPais;
        private string creo;
        private string modifica;
        private string codigoPostal;
        private string tipo;
        private string asentamiento;
        private string noExterior;
        private string noInterior;
        private string calle;
        private string colonia;
        private string municipio;
        private string ciudad;
        private string estado;
        private string pais;
        private string localidad;
        private string referencia;
        private string telefono;
        private string notas;
        private string requerimiento;
        private DateTime fechaNuevo;
        private DateTime? fechaModificaField;
        private string titulo;
        private int _Autorizado;
        private int _IdTipoDomicilio;
        #endregion

        public DomicilioFiscalModel() {
            this.Tipo = TipoDomicilioEnum.Fiscal.ToString();
            this.isActive = true;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        [JsonProperty("id")]
        [DataNames("_drccn_id", "DRCCN_ID")]
        [SugarColumn(ColumnName = "DRCCN_ID", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDomicilio {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_a", "DRCCN_A")]
        [SugarColumn(ColumnName = "DRCCN_A", ColumnDescription = "registro activo", IsNullable = false, Length = 1)]
        public bool Activo {
            get { return this.isActive; }
            set { this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        [JsonProperty("iddir")]
        [DataNames("_drccn_drctr_id", "DRCCN_DRCTR_ID")]
        [SugarColumn(ColumnName = "DRCCN_DRCTR_ID", ColumnDescription = "indice del relacion con el directorio", IsNullable = false, Length = 11)]
        public int IdContribuyente {
            get { return this.subId; }
            set { this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo de pais
        /// </summary>           
        [JsonProperty("cdgops")]
        [DataNames("_drccn_cdgps", "DRCCN_CDGPS")]
        [SugarColumn(ColumnName = "DRCCN_CDGPS", ColumnDescription = "codigo de pais", IsNullable = true, Length = 3)]
        public string CodigoPais {
            get { return this.codigoPais; }
            set { this.codigoPais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal en donde se da la ubicación.
        /// </summary>           
        [JsonProperty("cp")]
        [DataNames("_drccn_cp", "DRCCN_CP")]
        [SugarColumn(ColumnName = "DRCCN_CP", ColumnDescription = "codigo postal", IsNullable = true, Length = 10)]
        public string CodigoPostal {
            get { return this.codigoPostal; }
            set { this.codigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonProperty("tp")]
        [DataNames("_drccn_tp", "DRCCN_TP")]
        [SugarColumn(ColumnName = "DRCCN_TP", ColumnDescription = "alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)", IsNullable = true, Length = 64)]
        public string Tipo {
            get { return this.tipo; }
            set { this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_drccn_ctdrc_id", "DRCCN_CTDRC_ID")]
        [SugarColumn(ColumnName = "DRCCN_CTDRC_ID", ColumnDescription = "alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)", IsNullable = true)]
        public int IdTipoDomicilio {
            get { return this._IdTipoDomicilio; }
            set { this._IdTipoDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:asentamiento humano
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("ase")]
        [DataNames("_drccn_asnh", "DRCCN_ASNH")]
        [SugarColumn(ColumnName = "DRCCN_ASNH", ColumnDescription = "tipo de asentamiento humano", IsNullable = true, Length = 64)]
        public string Asentamiento {
            get { return this.asentamiento; }
            set { this.asentamiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero particular en donde se da la ubicación sobre una calle dada.
        /// </summary>           
        [JsonProperty("noext")]
        [DataNames("_drccn_extr", "DRCCN_EXTR")]
        [SugarColumn(ColumnName = "DRCCN_EXTR", ColumnDescription = "numero exterior", IsNullable = true, Length = 64)]
        public string NoExterior {
            get { return this.noExterior; }
            set { this.noExterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer información adicional para especificar la ubicación cuando calle y número exterior (noExterior) no resulten suficientes para determinar la ubicación de forma precisa.
        /// </summary>           
        [JsonProperty("noint")]
        [DataNames("_drccn_intr", "DRCCN_INTR")]
        [SugarColumn(ColumnName = "DRCCN_INTR", ColumnDescription = "numero interior", IsNullable = true, Length = 64)]
        public string NoInterior {
            get { return this.noInterior; }
            set { this.noInterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obetener o establecer la avenida, calle, camino o carretera donde se da la ubicación.
        /// </summary>           
        [JsonProperty("cll")]
        [DataNames("_drccn_cll", "DRCCN_CLL")]
        [SugarColumn(ColumnName = "DRCCN_CLL", ColumnDescription = "calle", IsNullable = true, Length = 124)]
        public string Calle {
            get { return this.calle; }
            set { this.calle = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la colonia en donde se da la ubicación cuando se desea ser más específico en casos de ubicaciones urbanas.
        /// </summary>           
        [JsonProperty("col")]
        [DataNames("_drccn_cln", "DRCCN_CLN")]
        [SugarColumn(ColumnName = "DRCCN_CLN", ColumnDescription = "colonia", IsNullable = true, Length = 124)]
        public string Colonia {
            get { return this.colonia; }
            set { this.colonia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el municipio o delegación (en el caso del Distrito Federal) en donde se da la ubicación.
        /// </summary>           
        [JsonProperty("dlg")]
        [DataNames("_drccn_dlg", "DRCCN_DLG")]
        [SugarColumn(ColumnName = "DRCCN_DLG", ColumnDescription = "delegacion o municipio", IsNullable = true, Length = 124)]
        public string Municipio {
            get { return this.municipio; }
            set { this.municipio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ciudad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cdd")]
        [DataNames("_drccn_cdd", "DRCCN_CDD")]
        [SugarColumn(ColumnName = "DRCCN_CDD", ColumnDescription = "ciudad", IsNullable = true, Length = 124)]
        public string Ciudad {
            get { return this.ciudad; }
            set { this.ciudad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado o entidad federativa donde se da la ubicación.
        /// </summary>           
        [JsonProperty("est")]
        [DataNames("_drccn_std", "DRCCN_STD")]
        [SugarColumn(ColumnName = "DRCCN_STD", ColumnDescription = "estado", IsNullable = true, Length = 124)]
        public string Estado {
            get { return this.estado; }
            set { this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el país donde se da la ubicación.
        /// </summary>           
        [JsonProperty("ps")]
        [DataNames("_drccn_ps", "DRCCN_PS")]
        [SugarColumn(ColumnName = "DRCCN_PS", ColumnDescription = "Pais", IsNullable = true, Length = 124)]
        public string Pais {
            get { return this.pais; }
            set { this.pais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ciudad o población donde se da la ubicación
        /// </summary>           
        [JsonProperty("loc")]
        [DataNames("_drccn_lcldd", "DRCCN_LCLDD")]
        [SugarColumn(ColumnName = "DRCCN_LCLDD", ColumnDescription = "localidad", IsNullable = true, Length = 124)]
        public string Localidad {
            get { return this.localidad; }
            set { this.localidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia de ubicación adicional
        /// </summary>           
        [JsonProperty("ref")]
        [DataNames("_drccn_rfrnc", "DRCCN_RFRNC")]
        [SugarColumn(ColumnName = "DRCCN_RFRNC", ColumnDescription = "referencia", IsNullable = true, Length = 124)]
        public string Referencia {
            get { return this.referencia; }
            set { this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("tel")]
        [DataNames("_drccn_tlfns", "DRCCN_TLFNS")]
        [SugarColumn(ColumnName = "DRCCN_TLFNS", ColumnDescription = "telefonos de contactos", IsNullable = true, Length = 124)]
        public string Telefono {
            get { return this.telefono; }
            set {
                if (value != null)
                    this.telefono = value.Replace("-","").Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion de la ubicacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_dsp", "DRCCN_DSP")]
        [SugarColumn(ColumnName = "DRCCN_DSP", ColumnDescription = "descripcion de la ubicacion", IsNullable = true, Length = 124)]
        public string Notas {
            get { return this.notas; }
            set { this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:horario y requisito de acceso
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_req", "DRCCN_REQ")]
        [SugarColumn(ColumnName = "DRCCN_REQ", ColumnDescription = "horario y requisito de acceso", IsNullable = true, Length = 124)]
        public string Requerimiento {
            get { return this.requerimiento; }
            set { this.requerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:horario y requisito de acceso
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_ttl", "DRCCN_TTL")]
        [SugarColumn(ColumnName = "DRCCN_TTL", ColumnDescription = "titulo del domicilio", IsNullable = true, Length = 50)]
        public string Titulo {
            get { return this.titulo; }
            set {
                this.titulo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer bandera de autorización del domicilio de entrega, por default el valor es 2, no disponible (DRCCN_AUTO_ID)
        /// </summary>
        [DataNames("_drccn_auto_id", "DRCCN_AUTO_ID")]
        [SugarColumn(ColumnName = "DRCCN_AUTO_ID", ColumnDescription = "bandera de autorización del domicilio de entrega, por default el valor es 2, no disponible (DRCCN_AUTO_ID)", IsNullable = false, DefaultValue = "0", IsIgnore = true)]
        public int IdAutorizado {
            get { return this._Autorizado; }
            set {
                this._Autorizado = value;
                this.OnPropertyChanged();
            }
        }

        #region creacion del registro
        /// <summary>
        /// Desc:fec. sist.
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_fn", "DRCCN_FN")]
        [SugarColumn(ColumnName = "DRCCN_FN", ColumnDescription = "fecha de sistema", IsNullable = false)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_usr_n", "DRCCN_USR_N")]
        [SugarColumn(ColumnName = "DRCCN_USR_N", ColumnDescription = "usuario creo", IsNullable = false, Length = 10)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_usr_m", "DRCCN_USR_M")]
        [SugarColumn(ColumnName = "DRCCN_USR_M", ColumnDescription = "usuario que modifico", IsOnlyIgnoreInsert = true, IsNullable = true, Length = 10)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_drccn_fm", "DRCCN_FM")]
        [SugarColumn(ColumnName = "DRCCN_FM", ColumnDescription = "ultima fecha de modificacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGoodDate) {
                    return this.fechaModificaField;
                }
                return null;
            }
            set {
                this.fechaModificaField = value;
            }
        }
        #endregion

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string Completo {
            get {
                string direccion = string.Empty;

                if (!(string.IsNullOrEmpty(this.Calle)))
                    direccion = string.Concat("Calle ", this.Calle);

                if (!(string.IsNullOrEmpty(this.NoExterior)))
                    direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);

                if (!(string.IsNullOrEmpty(this.NoInterior)))
                    direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);

                if (!(string.IsNullOrEmpty(this.Colonia)))
                    direccion = string.Concat(direccion, " Col. ", this.Colonia);

                if (!(string.IsNullOrEmpty(this.CodigoPostal)))
                    direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);

                if (!(string.IsNullOrEmpty(this.Municipio)))
                    direccion = string.Concat(direccion, " ", this.Municipio);

                if (!(string.IsNullOrEmpty(this.Estado)))
                    direccion = string.Concat(direccion, ", ", this.Estado);

                if (!(string.IsNullOrEmpty(this.Referencia)))
                    direccion = string.Concat(direccion, ", Ref: ", this.Referencia);

                if (!(string.IsNullOrEmpty(this.Localidad)))
                    direccion = string.Concat(direccion, ", Loc: ", this.Localidad);

                return direccion;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (string.IsNullOrEmpty(this.NoExterior) | string.IsNullOrEmpty(this.Calle) | string.IsNullOrEmpty(this.Colonia) | string.IsNullOrEmpty(this.Municipio) | string.IsNullOrEmpty(this.Estado))
                    return "Por favor ingrese datos válidos en esta fila!";
                return string.Empty;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "NoExterior" && string.IsNullOrEmpty(this.NoExterior))
                    return "El número exterior es requerido";
                return string.Empty;
            }
        }
    }
}
