﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities.V2 {
    [SugarTable("DRCTR", "directorio proveedores y clientes")]
    public class ContribuyenteDetailModel : V2.ContribuyenteModel, IContribuyenteDetailModel, IContribuyenteModel {
        #region declaraciones
        private BindingList<IDomicilioFiscalDetailModel> domicilios;
        private BindingList<ICuentaBancariaModel> cuentasBancarias;
        private BindingList<IContactoDetailModel> contactos;
        private BindingList<IContribuyenteVendedorModel> vendedores;
        private BindingList<IRelacionComercialDetailModel> relaciones;
        #endregion

        #region constructor
        public ContribuyenteDetailModel(ContribuyenteModel model) : base() {
            MapperClassExtensions.MatchAndMap<ContribuyenteModel, ContribuyenteDetailModel>(model, this);
            this.domicilios = new BindingList<IDomicilioFiscalDetailModel>();
            this.cuentasBancarias = new BindingList<ICuentaBancariaModel>();
            this.contactos = new BindingList<IContactoDetailModel>();
            this.vendedores = new BindingList<IContribuyenteVendedorModel>();
            this.relaciones = new BindingList<IRelacionComercialDetailModel>();
        }

        public ContribuyenteDetailModel() : base() {
            this.domicilios = new BindingList<IDomicilioFiscalDetailModel>();
            this.cuentasBancarias = new BindingList<ICuentaBancariaModel>();
            this.contactos = new BindingList<IContactoDetailModel>();
            this.vendedores = new BindingList<IContribuyenteVendedorModel>();
            this.relaciones = new BindingList<IRelacionComercialDetailModel>();
        }
        #endregion

        #region propiedades ignoradas
        /// <summary>
        /// obtener o establecer indice del catalogo de tipos de regimenes fiscales, No_Definido = 0, Persona_Fisica = 1, Persona_Moral = 2 (antes DRCTR_CTLGRGF_ID)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoRegimenEnum RegimenF {
            get { return (TipoRegimenEnum)base.IdRegimen; }
            set {
                base.IdRegimen = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de domicilios
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<IDomicilioFiscalDetailModel> Domicilios {
            get {
                return this.domicilios;
            }
            set {
                if (this.domicilios != null) {
                    this.Domicilios.AddingNew -= Domicilios_AddingNew;
                    this.Domicilios.ListChanged -= Domicilios_ListChanged;
                }
                this.domicilios = value;
                if (this.domicilios != null) {
                    this.Domicilios.AddingNew += Domicilios_AddingNew;
                    this.Domicilios.ListChanged += Domicilios_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de cuentas bancarias relacionadas al contribuyentes
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<CuentaBancariaModel> CuentasBancarias {
            get {
                return this.cuentasBancarias;
            }
            set {
                if (this.cuentasBancarias != null) {
                    this.CuentasBancarias.AddingNew -= this.CuentasBancarias_AddingNew;
                }
                this.cuentasBancarias = value;
                if (this.cuentasBancarias != null) {
                    this.CuentasBancarias.AddingNew += this.CuentasBancarias_AddingNew;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de contactos relacionados al contribuyente
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ContactoDetailModel> Contactos {
            get {
                return this.contactos;
            }
            set {
                this.contactos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de vendedores asociados
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ContribuyenteVendedorModel> Vendedores {
            get {
                return this.vendedores;
            }
            set {
                this.vendedores = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las relaciones comerciales dentro del directorio
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RelacionComercialDetailModel> Relaciones {
            get { return this.relaciones; }
            set {
                this.relaciones = value;
                //if (this.relaciones != null) {
                //    if (this.relaciones.Count > 0)
                //        base.Relacion = string.Join(",", this.relaciones.Where(it => it.Activo == true).Select(it => it.TipoRelacion.ToString()).ToArray());
                //}
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
        #endregion

        #region metodos
        private void Domicilios_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new DomicilioFiscalModel() { IdContribuyente = this.IdDirectorio, Creo = this.Creo, FechaNuevo = DateTime.Now };
        }

        private void Domicilios_ListChanged(object sender, ListChangedEventArgs e) {
            if (e.ListChangedType == ListChangedType.ItemAdded) {
                this.Domicilios[e.NewIndex].Creo = this.Creo;
            } else if (e.ListChangedType == ListChangedType.ItemChanged && this.Domicilios[e.NewIndex].IdDomicilio > 0) {
                //this.Domicilios[e.NewIndex].Modifica = this.Modifica;
            }
        }

        private void CuentasBancarias_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new CuentaBancariaModel { IdDirectorio = this.IdDirectorio, Creo = this.Creo, FechaNuevo = DateTime.Now };
        }

        private void Contactos_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new ContactoDetailModel() { IdDirectorio = this.IdDirectorio, Creo = this.Creo, FechaNuevo = DateTime.Now };
        }

        private void Contactos_ListChanged(object sender, ListChangedEventArgs e) {

        }
        #endregion
    }
}
