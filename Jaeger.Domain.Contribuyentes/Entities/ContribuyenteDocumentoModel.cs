﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    [SugarTable("DRCTRM", "directorio: historial de cambios del porcentaje de descuento al cliente")]
    public class ContribuyenteDocumentoModel : BasePropertyChangeImplementation, IContribuyenteDocumentoModel {
        #region declaraciones
        private int _Index;
        private bool _Activo;
        private int _IdDirectorio;
        private string _URL;
        private string _Observaciones;
        private DateTime _FechaNuevo;
        private string _Creo;
        #endregion

        public ContribuyenteDocumentoModel() {
            this._FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        [DataNames("DRCTRM_ID")]
        [SugarColumn(ColumnName = "DRCTRM_ID", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return this._Index; }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [DataNames("DRCTRM_A")]
        [SugarColumn(ColumnName = "DRCTRM_A", ColumnDescription = "registro activo", IsNullable = false, Length = 11)]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        [DataNames("DRCTRM_DRCTR_ID")]
        [SugarColumn(ColumnName = "DRCTRM_DRCTR_ID", ColumnDescription = "indice del relacion con el directorio", IsNullable = false, Length = 11)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor del descuento pactado
        /// </summary>
        [DataNames("DRCTRM_FACTOR")]
        [SugarColumn(ColumnName = "DRCTRM_FACTOR", ColumnDescription = "factor del descuento pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public string URL {
            get { return this._URL; }
            set {
                this._URL = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("DRCTRM_FACTOR")]
        [SugarColumn(ColumnName = "DRCTRM_FACTOR", ColumnDescription = "factor del descuento pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public string Nota {
            get { return this._Observaciones; }
            set {
                this._Observaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("DRCTRM_FM")]
        [SugarColumn(ColumnName = "DRCTRM_FM", ColumnDescription = "ultima fecha de modifificacion del registro")]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("DRCTRM_USR_M")]
        [SugarColumn(ColumnName = "DRCTRM_USR_M", ColumnDescription = "ultimo usuario que modifico", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this._Creo;
            }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
