using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Contribuyentes.Entities {
    public class ClienteSingleModel : IClienteSingleModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id", "DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer clave de usuario
        /// </summary>
        [DataNames("_drctr_clv", "DRCTR_CLV")]
        [SugarColumn(ColumnName = "_drctr_clv", ColumnDescription = "clave del usuario", IsNullable = false, Length = 14)]
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del contribuyente
        /// </summary>
        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", ColumnDescription = "nombre o razon social del contribuyente", IsNullable = false, Length = 255)]
        public string Nombre { get; set; }
    }
}