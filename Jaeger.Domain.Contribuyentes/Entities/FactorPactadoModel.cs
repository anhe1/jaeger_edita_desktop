﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    [SugarTable("DRCTRF", "directorio: historial de cambios del porcentaje de descuento al cliente")]
    public class FactorPactadoModel : BasePropertyChangeImplementation, IFactorPactadoModel {
        #region declaraciones
        private int _index;
        private int _IdDirectorio;
        private decimal _Factor;
        private DateTime _FechaNuevo;
        private string _Modifica;
        private bool _Activo;
        #endregion

        public FactorPactadoModel() {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        [DataNames("DRCTRF_ID")]
        [SugarColumn(ColumnName = "DRCTRF_ID", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return this._index; }
            set {
                this._index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        [DataNames("DRCTRF_DRCTR_ID")]
        [SugarColumn(ColumnName = "DRCTRF_DRCTR_ID", ColumnDescription = "indice del relacion con el directorio", IsNullable = false, Length = 11)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [DataNames("DRCTRF_A")]
        [SugarColumn(ColumnName = "DRCTRF_A", ColumnDescription = "registro activo", IsNullable = false, Length = 11)]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor del descuento pactado
        /// </summary>
        [DataNames("DRCTRF_FACTOR")]
        [SugarColumn(ColumnName = "DRCTRF_FACTOR", ColumnDescription = "factor del descuento pactado", IsNullable = false, Length = 14, DecimalDigits = 4)]
        public decimal Factor {
            get { return this._Factor; }
            set {
                this._Factor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("DRCTRF_FM")]
        [SugarColumn(ColumnName = "DRCTRF_FM", ColumnDescription = "ultima fecha de modifificacion del registro")]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("DRCTRF_USR_M")]
        [SugarColumn(ColumnName = "DRCTRF_USR_M", ColumnDescription = "ultimo usuario que modifico", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this._Modifica;
            }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
