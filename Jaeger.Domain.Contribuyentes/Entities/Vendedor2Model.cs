﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// Vendedore / Relacion Comercial
    /// </summary>
    [SugarTable("_drctr", "directorio proveedores, clientes, vendedores")]
    public class Vendedor2Model : RelacionComercialDetailModel, IVendedor2Model {
        #region declaraciones
        private int idVendedor;
        private bool activo;
        private int idComision;
        private string clave;
        private string curp;
        private string rfc;
        private string nombre;
        private string telefono;
        private string correo;
        private DateTime? fechaModifica;
        private string relacion;
        private byte[] avatar;
        #endregion

        public Vendedor2Model() {
            this.Activo = true;
            this.Relacion = "Vendedor";
            this.fechaModifica = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("_drctr_id", "DRCTR_ID")]
        [SugarColumn(ColumnName = "_drctr_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int IdVendedor {
            get { return this.idVendedor; }
            set {
                this.idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo dentro del directorio
        /// </summary>
        [DataNames("_drctr_a", "DRCTR_A")]
        [SugarColumn(ColumnName = "_drctr_a", ColumnDescription = "registro activo=1 registro inactivo=2", IsNullable = false, Length = 1)]
        public bool ActivoD {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del catalogo de comisiones asigando al vendedor en el directorio
        /// </summary>
        [DataNames("_drctr_cmsn_id", "DRCTR_CMSN_ID")]
        [SugarColumn(ColumnName = "_drctr_cmsn_id", ColumnDescription = "id del catalogo de comisiones en el caso de ser vendedor", IsNullable = true)]
        public int IdComisionD {
            get { return this.idComision; }
            set {
                this.idComision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave unica de registro en sistema
        /// </summary>           
        [DataNames("_drctr_clv", "DRCTR_CLV")]
        [SugarColumn(ColumnName = "_drctr_clv", ColumnDescription = "la clave unica de registro en sistema", IsNullable = false)]
        public string Clave {
            get { return this.clave; }
            set {
                this.clave = value.Trim().ToUpper();
                if (this.clave.Length > 10)
                    this.clave = this.clave.Substring(1, 10);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:CURP: requerido para la expresión de la CURP del trabajador
        /// </summary>           
        [DataNames("_drctr_curp", "DRCTR_CURP")]
        [SugarColumn(ColumnName = "_drctr_curp", IsNullable = true)]
        public string CURP {
            get { return this.curp; }
            set {
                this.curp = value.Trim().ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Reg. Fed. De Contribuyentes (RFC)
        /// </summary>           
        [DataNames("_drctr_rfc", "DRCTR_RFC")]
        [SugarColumn(ColumnName = "_drctr_rfc", ColumnDescription = "registro federal de contribuyentes", Length = 14, IsNullable = false)]
        public string RFC {
            get { return this.rfc; }
            set {
                if (value != null)
                this.rfc = value.Trim().ToUpper();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre y apellido o razon social
        /// </summary>           
        [DataNames("_drctr_nom", "DRCTR_NOM")]
        [SugarColumn(ColumnName = "_drctr_nom", IsNullable = false)]
        public string Nombre {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto, separados por (;)
        /// </summary>           
        [DataNames("_drctr_tel", "DRCTR_TEL")]
        [SugarColumn(ColumnName = "_drctr_tel", ColumnDescription = "telefono de contacto", IsNullable = false)]
        public string Telefono {
            get { return this.telefono; }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:correo electronic, separados por (;)
        /// </summary>           
        [DataNames("_drctr_mail", "DRCTR_MAIL")]
        [SugarColumn(ColumnName = "_drctr_mail", IsNullable = false)]
        public string Correo {
            get { return this.correo; }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la relacion del directorio
        /// </summary>
        [DataNames("_drctr_rlcn", "DRCTR_RLCN")]
        [SugarColumn(ColumnName = "_drctr_rlcn", ColumnDescription = "establecer la relacion en el directorio", IsNullable = false)]
        public string Relacion {
            get { return this.relacion; }
            set {
                this.relacion = value;
                this.OnPropertyChanged();
            }
        }

        //[DataNames("_drctr_usr_n", "DRCTR_USR_N")]
        //[SugarColumn(ColumnName = "_drctr_usr_n", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        //public string Creo {
        //    get { return this.creo; }
        //    set {
        //        this.creo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_drctr_usr_m", "DRCTR_USR_M")]
        //[SugarColumn(ColumnName = "_drctr_usr_m", IsNullable = false, IsOnlyIgnoreInsert = true)]
        //public string Modifica {
        //    get { return this.modifica; }
        //    set {
        //        this.modifica = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_drctr_fn", "DRCTR_FN")]
        //[SugarColumn(ColumnName = "_drctr_fn", ColumnDescription = "fecha de creacion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        //public DateTime FechaNuevo {
        //    get { return this.fechaNuevo; }
        //    set {
        //        this.fechaNuevo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_drctr_fm", "DRCTR_FM")]
        //[SugarColumn(ColumnName = "_drctr_fm", ColumnDescription = "fecha ultima de modificacion", IsNullable = false)]
        //public DateTime? FechaModifica {
        //    get {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (this.fechaModifica >= firstGoodDate) {
        //            return this.fechaModifica;
        //        }
        //        return null;
        //    }
        //    set {
        //        this.fechaModifica = value;
        //    }
        //}

        [DataNames("_drctr_file", "DRCTR_FILE")]
        [SugarColumn(ColumnName = "_drctr_file", IsNullable = false)]
        public byte[] Avatar {
            get { return this.avatar; }
            set {
                this.avatar = value;
                this.OnPropertyChanged();
            }
        }
    }
}
