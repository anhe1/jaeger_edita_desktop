﻿using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.Domain.Contribuyentes.Entities {
    /// <summary>
    /// cartera de clientes
    /// </summary>
    [SugarTable("_ctlvndc", "vendedores: cartera de clientes")]
    public class VendedorClienteDetailModel : ContribuyenteVendedorModel, IContribuyenteVendedorModel, IVendedorClienteDetailModel {
        public VendedorClienteDetailModel() : base() {
            Activo = true;
        }
    }
}
