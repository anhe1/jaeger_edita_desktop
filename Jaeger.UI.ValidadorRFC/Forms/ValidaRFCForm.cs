﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.SAT.RFC.Valida;
using Jaeger.SAT.RFC.Valida.Interfaces;

namespace Jaeger.UI.ValidadorRFC.Forms {
    public partial class ValidaRFCForm : RadForm {
        protected internal IService service = new Service();
        protected internal List<IRegistro> registros = new List<IRegistro>();
        protected internal RadMenuItem Exportar = new RadMenuItem { Text = "Exportar" };
        protected internal RadMenuItem Importar = new RadMenuItem { Text = "Importar" };

        public ValidaRFCForm() {
            InitializeComponent();
        }

        public ValidaRFCForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        public ValidaRFCForm(List<IRegistro> dataSource) {
            InitializeComponent();
            this.registros = dataSource;
        }

        private void ValidaRFCForm_Load(object sender, EventArgs e) {
            this.TValida.Nuevo.Text = "Agregar";
            this.TValida.Actualizar.Text = "Validar";
            this.TValida.Actualizar.Image = Properties.Resources.protect_16px;
            this.TValida.Nuevo.Click += this.Nuevo_Click;
            this.TValida.Remover.Click += this.Remover_Click;
            this.TValida.Actualizar.Click += this.Actualizar_Click;
            this.TValida.Cerrar.Click += this.Cerrar_Click;
            this.TValida.Herramientas.Items.AddRange(new RadMenuItem[] { this.Importar, this.Exportar });
            this.Importar.Click += this.Importar_Click;
            this.Exportar.Click += this.Exportar_Click;
            this.gridRegistros.Standard();
            this.gridRegistros.AllowEditRow = true;
            this.gridRegistros.ReadOnly = false;
            this.gridRegistros.DataSource = this.registros;
            this.lblStatus.Text = string.Format("Registros: {0}", this.registros.Count);
        }

        public virtual void Exportar_Click(object sender, EventArgs e) {
            if (this.registros.Count > 0) {
                var saveDialog = new SaveFileDialog {
                    Title = "Exportar",
                    Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                    RestoreDirectory = true
                };

                if (saveDialog.ShowDialog(this) == DialogResult.OK) {
                    var contenido = new List<string>();
                    foreach (var item in this.registros) {
                        contenido.Add(item.ToString() + Environment.NewLine);
                    }

                    try {
                        System.IO.File.WriteAllLines(saveDialog.FileName, contenido.ToArray(), System.Text.Encoding.UTF8);
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, ex.Message, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                    }
                }
            }
        }

        public virtual void Importar_Click(object sender, EventArgs e) {
            var _openDialog = new OpenFileDialog { Title = "Importar", Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*" };
            if (_openDialog.ShowDialog(this) == DialogResult.OK) {
                if (System.IO.File.Exists(_openDialog.FileName)) { 
                    var _contenido = System.IO.File.ReadAllText(_openDialog.FileName);
                    if (!string.IsNullOrEmpty(_contenido)) {
                        var _registros = _contenido.Split(new char[] { '\r' });
                        for (int i = 0; i < _registros.Length; i++) {
                            var columnas = _registros[i].Split(new char[] { '|' });
                            if (columnas.Count() > 3) {
                                var _registro = new Registro {
                                    Numero = Convert.ToInt32(columnas[0].ToString()),
                                    RFC = columnas[1].ToString(),
                                    NombreRazonSocial = columnas[2].ToString(),
                                    CodigoPostal = columnas[3].ToString(),
                                };
                                this.registros.Add(_registro);
                            } else {
                                //var _registro = new RegistroResponse {
                                //    Numero = Convert.ToInt32(columnas[0].ToString()),
                                //    RFC = columnas[1].ToString(),
                                //    Respuesta = string.Empty
                                //};
                                //this.registos.Add(_registro);
                            }
                        }
                    }
                } else {
                    RadMessageBox.Show(this, "No se tiene acceso al archivo.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.registros.Count > 0) {
                this.lblStatus.Text = "Esperando al servidor ...";
                using (var espera = new Waiting1Form(this.Consultar)) {
                    espera.Text = "Solitando información ...";
                    espera.ShowDialog(this);
                }
                this.gridRegistros.DataSource = this.registros;
            } else {
                RadMessageBox.Show(this, "Es necesario al menos algún registro para validar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            if (this.gridRegistros.CurrentRow != null) {
                this.gridRegistros.Rows.Remove(this.gridRegistros.CurrentRow);
            }
        }

        public virtual void Nuevo_Click(object sender, EventArgs e) {
            this.registros.Add(new Registro() { Numero = this.registros.Count + 1 });
        }

        public virtual void Consultar() {

            var validarfc = this.service.Execute(Request.Create().WithRegistros(this.registros).Build());
            var salida = string.Empty;
            if (validarfc.Status == SAT.RFC.Valida.Enums.StatusCodeEnum.Error) {
                this.Tag = validarfc.Message;
                return;
            }

            foreach (var item in validarfc.Records) {
                if (item.IsValid) {
                    try {
                        var s = this.registros.Where(it => it.RFC == item.RFC).FirstOrDefault();
                        if (s != null) {
                            //s.IsValidRFC = item.IsValid;
                            //this.service.ValidaRFC(s.IdDirectorio, item.CodigoPostal, item.IsValid);
                        }
                    } catch (Exception ex) {
                        Console.WriteLine("Contribuyentes: " + ex.Message);
                    }
                }
                salida = string.Concat(salida, item.ToString(), " Resultado: ", item.Respuesta, Environment.NewLine);
            }

            this.Tag = salida;
        }

        private void GridRegistros_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.lblStatus.Text = string.Format("Registros: {0}", this.registros.Count);
        }
    }
}
