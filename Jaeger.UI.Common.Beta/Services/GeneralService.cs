﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Common.Services {
    public static class GeneralService {
        /// <summary>
        /// obtener lista de meses
        /// </summary>
        public static List<MesModel> GetMeses() {
            List<MesModel> enums = ((MesesEnum[])Enum.GetValues(typeof(MesesEnum))).Select(c => new MesModel((int)c, c.ToString())).ToList();
            return enums;
        }
    }
}
