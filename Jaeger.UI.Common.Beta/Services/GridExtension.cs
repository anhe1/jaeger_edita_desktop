﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Jaeger.UI.Common.Services {
    public static class GridExtension {

        /// <summary>
        /// Color para alternar en la filas
        /// </summary>
        public static DataGridViewCellStyle cellStyleAlteratingRow = new DataGridViewCellStyle() {
            BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(245)), Convert.ToInt32(Convert.ToByte(238)), Convert.ToInt32(Convert.ToByte(248)))
        };

        public static DataGridViewCellStyle cellStyleColumnHeader = new DataGridViewCellStyle() {
            Alignment = DataGridViewContentAlignment.MiddleLeft,
            //BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(79)), Convert.ToInt32(Convert.ToByte(129)), Convert.ToInt32(Convert.ToByte(189))),
            BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(165)), Convert.ToInt32(Convert.ToByte(105)), Convert.ToInt32(Convert.ToByte(189))),
            Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = SystemColors.ControlLightLight,
            SelectionBackColor = SystemColors.Highlight,
            SelectionForeColor = SystemColors.HighlightText,
            WrapMode = DataGridViewTriState.True
        };

        /// <summary>
        /// estilo default de las celdas
        /// </summary>
        public static DataGridViewCellStyle cellStyleDefault = new DataGridViewCellStyle() {
            Alignment = DataGridViewContentAlignment.MiddleLeft,
            BackColor = SystemColors.ControlLightLight,
            Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = SystemColors.ControlText,
            // color de selección
            SelectionBackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(215)), Convert.ToInt32(Convert.ToByte(189)), Convert.ToInt32(Convert.ToByte(226))),
            SelectionForeColor = SystemColors.HighlightText,
            WrapMode = DataGridViewTriState.False
        };

        /// <summary>
        /// encabezados de las filas
        /// </summary>
        public static DataGridViewCellStyle cellStyleRowHeader = new DataGridViewCellStyle() {
            Alignment = DataGridViewContentAlignment.MiddleLeft,
            BackColor = Color.Lavender,
            Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = SystemColors.WindowText,
            SelectionBackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(187)), Convert.ToInt32(Convert.ToByte(89))),
            SelectionForeColor = SystemColors.HighlightText,
            WrapMode = DataGridViewTriState.True
        };

        public static bool DataGridCommon(this DataGridView gridView, int cual = 0) {
            if (cual == 1) {
                return DataGridCommon_cp(gridView);
            }
            return DataGridCommon_cplite(gridView);
        }

        public static bool DataGridCommon_cp(this DataGridView gridView) {

            gridView.AllowUserToAddRows = false;
            gridView.AllowUserToDeleteRows = false;
            gridView.AutoGenerateColumns = false;
            gridView.AllowUserToResizeRows = false;
            gridView.ReadOnly = true;
            gridView.BorderStyle = BorderStyle.None;

            gridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            gridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(165)), Convert.ToInt32(Convert.ToByte(105)), Convert.ToInt32(Convert.ToByte(189))),
                Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
                ForeColor = SystemColors.ControlLightLight,
                SelectionBackColor = SystemColors.Highlight,
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.True
            };

            gridView.ColumnHeadersHeight = 32;
            gridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            gridView.DefaultCellStyle = new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = SystemColors.ControlLightLight,
                Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
                ForeColor = SystemColors.ControlText,
                // color de selección
                SelectionBackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(215)), Convert.ToInt32(Convert.ToByte(189)), Convert.ToInt32(Convert.ToByte(226))),
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.False
            };

            gridView.EnableHeadersVisualStyles = false;
            gridView.GridColor = SystemColors.GradientInactiveCaption;

            gridView.RowHeadersVisible = false;
            gridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;

            gridView.Font = cellStyleColumnHeader.Font;
            gridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridView.AlternatingRowsDefaultCellStyle = cellStyleAlteratingRow;

            gridView.SetDataError();

            return true;
        }

        public static bool DataGridCommon_cplite(this DataGridView gridView, bool readOnly = true) {
            gridView.AllowUserToAddRows = false;
            gridView.AllowUserToDeleteRows = false;
            gridView.AutoGenerateColumns = false;
            gridView.AllowUserToResizeRows = false;

            gridView.BorderStyle = BorderStyle.None;

            gridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            gridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            gridView.ColumnHeadersHeight = 32;
            gridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(40)), Convert.ToInt32(Convert.ToByte(116)), Convert.ToInt32(Convert.ToByte(166))),
                Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
                ForeColor = SystemColors.ControlLightLight,
                SelectionBackColor = SystemColors.Highlight,
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.True
            };


            gridView.DefaultCellStyle = new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = SystemColors.ControlLightLight,
                Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
                ForeColor = SystemColors.ControlText,
                // color de selección
                SelectionBackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(133)), Convert.ToInt32(Convert.ToByte(193)), Convert.ToInt32(Convert.ToByte(233))),
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.False
            };

            gridView.EnableHeadersVisualStyles = false;
            gridView.GridColor = SystemColors.GradientInactiveCaption;

            gridView.RowHeadersVisible = false;
            gridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;

            gridView.Font = cellStyleColumnHeader.Font;
            gridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle() {
                BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(235)), Convert.ToInt32(Convert.ToByte(245)), Convert.ToInt32(Convert.ToByte(251)))
            };
            gridView.MultiSelect = false;
            gridView.SetDataError();
            gridView.SetCellBeginEdit();

            for (int i = 0; i < gridView.Columns.Count-1; i++) {
                gridView.Columns[i].Tag = gridView.Columns[i].ReadOnly;
            }

            gridView.ReadOnly = readOnly;
            return true;
        }

        public static void Editable(this DataGridView gridView, bool editable) {
            if (editable == false) {
                gridView.ReadOnly = true;
            } else {
                gridView.ReadOnly = false;
                for (int i = 0; i < gridView.Columns.Count-1; i++) {
                    gridView.Columns[i].ReadOnly = (bool)gridView.Columns[i].Tag;
                    Console.WriteLine(gridView.Columns[i].ReadOnly.ToString() + " = " + gridView.Columns[i].Tag.ToString());
                }
            }
        }

        public static bool DataGridCommon0(this DataGridView gridView) {
            DataGridView dataGridView = gridView;
            dataGridView.ReadOnly = true;
            dataGridView.RowHeadersVisible = false;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.AutoGenerateColumns = false;
            dataGridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle { BackColor = Color.FromArgb(224, 224, 225) };
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Sunken;
            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.ColumnHeadersHeight = 32;
            dataGridView.RowHeadersDefaultCellStyle = cellStyleColumnHeader;
            dataGridView.Font = cellStyleColumnHeader.Font;
            //dataGridView.BackgroundColor = SystemColors.Window;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView.SetDataError();
            return true;
        }

        public static bool ExportCSV(this DataGridView dataGridRemision) {
            var response = false;

            if (dataGridRemision.Rows.Count > 0) {
                var sfd = new SaveFileDialog();
                sfd.Filter = "CSV (*.csv)|*.csv";
                sfd.FileName = "Output.csv";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK) {
                    if (File.Exists(sfd.FileName)) {
                        try {
                            File.Delete(sfd.FileName);
                        } catch (IOException ex) {
                            fileError = true;
                            response = false;
                            MessageBox.Show("No fue posible escribir los datos en el disco." + ex.Message);
                        }
                    }

                    if (!fileError) {
                        try {
                            int columnCount = dataGridRemision.Columns.Count;
                            string columnNames = "";
                            string[] outputCsv = new string[dataGridRemision.Rows.Count + 1];
                            for (int i = 0; i < columnCount; i++) {
                                columnNames += dataGridRemision.Columns[i].HeaderText.ToString() + ",";
                            }
                            outputCsv[0] += columnNames;

                            for (int i = 1; (i - 1) < dataGridRemision.Rows.Count; i++) {
                                for (int j = 0; j < columnCount; j++) {
                                    if (dataGridRemision.Rows[i - 1].Cells[j].Value != null) {
                                        if (dataGridRemision.Rows[i - 1].Cells[j].ValueType == typeof(string)) {
                                            outputCsv[i] += "\"" + dataGridRemision.Rows[i - 1].Cells[j].Value.ToString() + "\",";
                                        } else {
                                            outputCsv[i] += dataGridRemision.Rows[i - 1].Cells[j].Value.ToString() + ",";
                                        }
                                    } else {
                                        outputCsv[i] += ",";
                                    }
                                }
                            }

                            File.WriteAllLines(sfd.FileName, outputCsv, Encoding.UTF8);
                            MessageBox.Show("Datos exportados correctamente !!!", "Info");
                            return true;
                        } catch (Exception ex) {
                            MessageBox.Show("Error :" + ex.Message);
                            response = false;
                        }
                    }
                }
            }
            return response;
        }

        public static void CopyDataGridViewToClipboard(this DataGridView dgv) {
            IEnumerator enumerator = null;
            try {
                string str = "";
                DataGridViewColumn firstColumn = dgv.Columns.GetFirstColumn(DataGridViewElementStates.Visible);
                do {
                    str = string.Concat(str, firstColumn.HeaderText, "\t");
                    firstColumn = dgv.Columns.GetNextColumn(firstColumn, DataGridViewElementStates.Visible, DataGridViewElementStates.None);
                }
                while (firstColumn != null);
                str = str.Substring(0, checked(str.Length - 1));
                str = string.Concat(str, Environment.NewLine);
                try {
                    enumerator = ((IEnumerable)dgv.Rows).GetEnumerator();
                    while (enumerator.MoveNext()) {
                        DataGridViewRow current = (DataGridViewRow)enumerator.Current;
                        firstColumn = dgv.Columns.GetFirstColumn(DataGridViewElementStates.Visible);
                        do {
                            if (current.Cells[firstColumn.Index].Value != null) {
                                str = string.Concat(str, current.Cells[firstColumn.Index].Value.ToString());
                            }
                            str = string.Concat(str, "\t");
                            firstColumn = dgv.Columns.GetNextColumn(firstColumn, DataGridViewElementStates.Visible, DataGridViewElementStates.None);
                        }
                        while (firstColumn != null);
                        str = str.Substring(0, checked(str.Length - 1));
                        str = string.Concat(str, Environment.NewLine);
                    }
                } finally {
                    if (enumerator is IDisposable) {
                        (enumerator as IDisposable).Dispose();
                    }
                }
                DataObject dataObject = new DataObject();
                dataObject.SetText(str);
                Clipboard.SetDataObject(dataObject, true);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Occurs when an external data-parsing or validation operation throws an exception, or when an attempt to commit data to a data source fails.
        /// </summary>
        public static void SetDataError(this DataGridView gridView) {
            gridView.DataError += DataGrid_DataError;
        }

        private static void SetCellBeginEdit(this DataGridView gridView) {
            gridView.CellBeginEdit += DataGrid_CellBeginEdit;
        }

        public static int DropDownWidth(this ComboBox myCombo) {
            int maxWidth = myCombo.Width + 60;
            int temp = 0;
            Label label1 = new Label();

            foreach (var obj in myCombo.Items) {
                label1.Text = obj.ToString();
                temp = label1.PreferredWidth;
                if (temp > maxWidth) {
                    maxWidth = temp;
                }
            }
            label1.Dispose();
            myCombo.DropDownWidth = maxWidth;
            return maxWidth;
        }

        private static void DataGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) {
            var d = (DataGridView)sender;
            if (d != null) {
                if ((bool)d.Columns[e.ColumnIndex].ReadOnly == true) {
                    e.Cancel = true;
                }
            }
        }

        private static void DataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e) {
            Console.WriteLine(e.Exception.Message);
        }
    }
}
