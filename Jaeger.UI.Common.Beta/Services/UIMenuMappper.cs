﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Common.Services {
    public class UIMenuMappper {
        private int indice;

        public UIMenuMappper() {
            indice = 1;
        }

        public virtual List<UIMenuElement> Start(MenuStrip menuStrip) {
            var _result = new List<UIMenuElement>();
            foreach (var item in menuStrip.Items) {
                // 1. get Menu Item
                var toolStripMenuItem = item as ToolStripMenuItem;
                if (toolStripMenuItem != null) {
                    var _item = new UIMenuElement {
                        Id = indice,
                        ParentId = 0,
                        Name = toolStripMenuItem.Name,
                        Label = toolStripMenuItem.Text,
                        Form = (string)toolStripMenuItem.Tag
                    };
                    _result.Add(_item);
                    indice++;
                    _result.AddRange(this.Start(ref toolStripMenuItem, _item.Id));
                }
            }
            foreach (var item in _result) {
                Console.WriteLine(String.Format("new UIMenuElement {{ Id = {0}, ParentId = {1}, Name = \"{2}\", Label = \"{3}\", Form = \"{4}\", Rol = ProfileService1._Administrador }},", item.Id, item.ParentId, item.Name, item.Label, item.Form));
            }
            return _result;
        }

        public virtual List<UIMenuElement> Start(ref ToolStripMenuItem menuItem, int parentID) {
            var _result = new List<UIMenuElement>();
            if (menuItem == null) {
                return null;
            }

            foreach (var item in menuItem.DropDownItems) {
                var childMenuItem = item as ToolStripMenuItem;
                var _item = new UIMenuElement {
                    Id = indice,
                    ParentId = parentID,
                    Name = childMenuItem.Name,
                    Label = childMenuItem.Text,
                    Form = (string)childMenuItem.Tag
                };
                _result.Add(_item);
                indice++;
                if (childMenuItem.DropDownItems.Count > 0) {
                    _result.AddRange(Start(ref childMenuItem, _item.Id));
                }
            }

            return _result;
        }
    }
}
