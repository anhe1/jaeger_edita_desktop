﻿using System.Windows.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Common.Services {
    public class UIMenuUtility {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mainForm"></param>
        /// <param name="clientInfo"></param>
        public static void SetPermission(Form mainForm, UsuarioBase clientInfo, UIMenuItemPermission menuPermissions) {
            var mainMenu = mainForm.MainMenuStrip;
            SetPermission(clientInfo, ref mainMenu, menuPermissions);
            mainForm.Invalidate();
            mainForm.Refresh();
            mainMenu.Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="menuStrip"></param>
        /// <param name="menuItemPermission"></param>
        public static void SetPermission(UsuarioBase user, ref MenuStrip menuStrip, UIMenuItemPermission menuItemPermission) {
            for (int i = 0; i < menuStrip.Items.Count; i++) {
                // 1. get Menu Item
                var toolStripMenuItem = menuStrip.Items[i] as ToolStripMenuItem;
                // 2. Set permission
                SetPermission(user, ref toolStripMenuItem, menuItemPermission);
                // 3. Set permission for child

            }
        }

        private static void SetPermission(UsuarioBase user, ref ToolStripMenuItem menuItem, UIMenuItemPermission permission) {
            // 1. get Menu Item
            // 2. Check role of user
            if (menuItem == null)
                return;
            bool hasPermission = permission.HasPermission(menuItem.Name.ToLower(), user);
            // 3. Process menu presentation base on user's role.
            if (hasPermission) {
                menuItem.Enabled = true;
                menuItem.Visible = true;
                menuItem.Text = permission.CurrentItem.Label;

            } else {
                switch (permission.DeniedAction) {
                    case UIPermissionEnum.Disabled:
                        menuItem.Enabled = false;
                        break;
                    case UIPermissionEnum.Invisible:
                        menuItem.Visible = false;
                        break;
                    case UIPermissionEnum.Popup:
                        break;
                    case UIPermissionEnum.Normal:
                        break;
                }
            }

            if (permission.CurrentItem != null) {
                menuItem.Enabled = permission.CurrentItem.IsAvailable;
            }

            for (int i = 0; i < menuItem.DropDownItems.Count; i++) {
                var childMenuItem = menuItem.DropDownItems[i] as ToolStripMenuItem;
                SetPermission(user, ref childMenuItem, permission);
            }
        }
    }
}
