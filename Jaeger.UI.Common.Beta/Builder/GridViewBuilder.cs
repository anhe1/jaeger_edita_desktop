﻿using Jaeger.UI.Common.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Builder {
    /// <summary>
    /// clase abstracta para builder de las vistas del Grid de telerik
    /// </summary>
    public abstract class GridViewBuilder : IGridViewBuilder {
        #region declaraciones
        protected internal List<DataGridViewColumn> _Columns;
        protected internal string FormatStringMoney = "{0:N2}";
        protected internal string FormatStringDate = "{0:dd MMM yy}";
        protected internal string FormatStringP = "{0:P0}";
        protected internal string FormatStringNumber = "{0:N2}";
        protected internal string FormarStringFolio = "{0:00000#}";
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public GridViewBuilder() {
            _Columns = new List<DataGridViewColumn>();
        }

        /// <summary>
        /// Build
        /// </summary>
        /// <returns>array de columnas (GridViewDataColumn)</returns>
        public virtual DataGridViewColumn[] Build() {
            return _Columns.ToArray();
        }

        public DataGridViewCellStyle GetFecha() {
            return new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleCenter,
                Format = "dd MMM yy"
            };
        }

        public DataGridViewCellStyle GetHora() {
            return new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleRight,
                Format = "H:mm:ss"
            };
        }

        public DataGridViewCellStyle GetNumero() {
            return new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleRight,
                Format = "N2"
            };
        }

        public DataGridViewCellStyle GetMoneda() {
            return new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleRight,
                Format = "N2"
            };
        }

        public void ViewDefault(DataGridView gridView) {
            gridView.AllowUserToAddRows = false;
            gridView.AllowUserToDeleteRows = false;
            gridView.AutoGenerateColumns = false;
            gridView.AllowUserToResizeRows = false;
            gridView.ReadOnly = true;
            gridView.BorderStyle = BorderStyle.None;

            gridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            gridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(165)), Convert.ToInt32(Convert.ToByte(105)), Convert.ToInt32(Convert.ToByte(189))),
                Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
                ForeColor = SystemColors.ControlLightLight,
                SelectionBackColor = SystemColors.Highlight,
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.True
            };

            gridView.ColumnHeadersHeight = 32;
            gridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            gridView.DefaultCellStyle = new DataGridViewCellStyle() {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = SystemColors.ControlLightLight,
                Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
                ForeColor = SystemColors.ControlText,
                // color de selección
                SelectionBackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(215)), Convert.ToInt32(Convert.ToByte(189)), Convert.ToInt32(Convert.ToByte(226))),
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.False
            };

            gridView.EnableHeadersVisualStyles = false;
            gridView.GridColor = SystemColors.GradientInactiveCaption;

            gridView.RowHeadersVisible = false;
            gridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;

            gridView.Font = cellStyleColumnHeader.Font;
            gridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridView.AlternatingRowsDefaultCellStyle = cellStyleAlteratingRow;

            gridView.SetDataError();

        }

        /// <summary>
        /// Color para alternar en la filas
        /// </summary>
        public static DataGridViewCellStyle cellStyleAlteratingRow = new DataGridViewCellStyle() {
            BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(245)), Convert.ToInt32(Convert.ToByte(238)), Convert.ToInt32(Convert.ToByte(248)))
        };

        public static DataGridViewCellStyle cellStyleColumnHeader = new DataGridViewCellStyle() {
            Alignment = DataGridViewContentAlignment.MiddleLeft,
            //BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(79)), Convert.ToInt32(Convert.ToByte(129)), Convert.ToInt32(Convert.ToByte(189))),
            BackColor = Color.FromArgb(Convert.ToInt32(Convert.ToByte(165)), Convert.ToInt32(Convert.ToByte(105)), Convert.ToInt32(Convert.ToByte(189))),
            Font = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(0)),
            ForeColor = SystemColors.ControlLightLight,
            SelectionBackColor = SystemColors.Highlight,
            SelectionForeColor = SystemColors.HighlightText,
            WrapMode = DataGridViewTriState.True
        };

        public void Dispose() {
            GC.Collect();
        }
    }
}
