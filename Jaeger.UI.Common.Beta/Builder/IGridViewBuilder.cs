﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Builder {
    public interface IGridViewBuilder : IDisposable {
        DataGridViewColumn[] Build();

        void ViewDefault(DataGridView gridView);
    }

    public interface IGridViewTempleteBuild : IGridViewColumnsBuild {

    }

    public interface IGridViewColumnsBuild {

    }
}
