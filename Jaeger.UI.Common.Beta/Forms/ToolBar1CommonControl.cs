﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Domain.Base.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Common.Forms {
    public partial class ToolBar1CommonControl : UserControl {
        public ToolBar1CommonControl() {
            InitializeComponent();
        }

        private void ToolBar1CommonControl_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.Periodo.ComboBox.DataSource = GeneralService.GetMeses();
            this.Periodo.ComboBox.ValueMember = "Id";
            this.Periodo.ComboBox.DisplayMember = "Descripcion";
            this.Periodo.ComboBox.SelectedIndex = DateTime.Now.Month;

            this.ToolBarHostEjercicio.Text = DateTime.Now.Year.ToString();
            this.ToolBarHostEjercicio.KeyPress += new KeyPressEventHandler(Extensions.TextBoxOnlyNumbers_KeyPress);
        }

        public bool ShowPeriodo {
            get {
                return this.ToolBarLabelPeriodo.Visible;
            }
            set {
                this.ToolBarLabelPeriodo.Visible = value;
                if (this.ToolBarLabelPeriodo.Visible == false) {
                    this.Periodo.Visible = false;
                    this.ToolBarLabelPeriodo.Visible = false;
                } else {
                    this.Periodo.Visible = true;
                    this.ToolBarLabelPeriodo.Visible = true;
                }
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visible;
            }
            set {
                this.Nuevo.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visible;
            }
            set {
                this.Editar.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCancelar {
            get {
                return this.Cancelar.Visible;
            }
            set {
                this.Cancelar.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visible;
            }
            set {
                this.Actualizar.Visible = value;
            }
        }

        [Description("Si debe ser mostrado el botón imprimir"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visible;
            }
            set {
                this.Imprimir.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visible;
            }
            set {
                this.Herramientas.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visible;
            }set {
                this.Cerrar.Visible = value;
            }
        }

        public int GetMes() {
            return (int)(this.Periodo.SelectedItem as MesModel).Id;
        }

        public int GetEjercicio() {
            return int.Parse(this.ToolBarHostEjercicio.Text);
        }
    }
}
