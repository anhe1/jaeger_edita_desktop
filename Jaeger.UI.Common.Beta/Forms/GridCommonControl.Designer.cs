﻿namespace Jaeger.UI.Common.Forms {
    partial class GridCommonControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new Jaeger.UI.Common.Forms.TbCommonControl();
            this.GridData = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCancelar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowPeriodo = true;
            this.ToolBar.Size = new System.Drawing.Size(1135, 25);
            this.ToolBar.TabIndex = 0;
            // 
            // GridData
            // 
            this.GridData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 25);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1135, 431);
            this.GridData.TabIndex = 1;
            // 
            // GridCommonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.ToolBar);
            this.Name = "GridCommonControl";
            this.Size = new System.Drawing.Size(1135, 456);
            this.Load += new System.EventHandler(this.GridCommonControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal TbCommonControl ToolBar;
        protected internal System.Windows.Forms.DataGridView GridData;
    }
}
