﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Jaeger.UI.Common.Forms {
    public partial class RichTextBoxXmlControl : RichTextBox {
        #region Constructor
        public RichTextBoxXmlControl() {
            InitializeComponent();

            this.Font = new System.Drawing.Font("Consolas", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            miCopyRtf.Click += new EventHandler(miCopyRtf_Click);
            miCopyText.Click += new EventHandler(miCopyText_Click);
            miSelectAll.Click += new EventHandler(miSelectAll_Click);
        }
        private IContainer components;
        private ContextMenuStrip contextMenuStrip2;
        private ToolStripMenuItem miCopyText;
        private ToolStripMenuItem miCopyRtf;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem miSelectAll;
        #endregion Constructor

        #region Properties
        private string xml = "";
        public string Xml {
            get {
                return xml;
            }
            set {
                this.Text = "";
                xml = value;
                SetXml(xml);
            }
        }
        #endregion Properties

        #region Private Methods
        private void SetXml(string s) {
            if (String.IsNullOrEmpty(s))
                return;

            XDocument xdoc = XDocument.Parse(s);

            string formattedText = xdoc.ToString().Trim();

            if (String.IsNullOrEmpty(formattedText))
                return;

            XmlStateMachine machine = new XmlStateMachine();

            if (s.StartsWith("<?")) {
                string xmlDeclaration = machine.GetXmlDeclaration(s);
                if (xmlDeclaration != String.Empty)
                    formattedText = xmlDeclaration + Environment.NewLine + formattedText;
            }

            int location = 0;
            int failCount = 0;
            int tokenTryCount = 0;
            XmlTokenType ttype = XmlTokenType.Unknown;
            while (location < formattedText.Length) {
                string token = machine.GetNextToken(formattedText, location, out ttype);
                Color color = machine.GetTokenColor(ttype);
                this.AppendText(token, color);
                location += token.Length;
                tokenTryCount++;

                // Check for ongoing failure
                if (token.Length == 0)
                    failCount++;
                if (failCount > 10 || tokenTryCount > formattedText.Length) {
                    string theRestOfIt = formattedText.Substring(location, formattedText.Length - location);
                    //this.AppendText(Environment.NewLine + Environment.NewLine + theRestOfIt); // DEBUG
                    this.AppendText(theRestOfIt);
                    break;
                }
            }
        }
        #endregion Private Methods

        #region Context Menu
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {

        }

        void miCopyText_Click(object sender, EventArgs e) {
            string s = this.SelectedText;
            try {
                XDocument doc = XDocument.Parse(s);
                s = doc.ToString();
            } catch { }
            Clipboard.SetText(s);
        }

        void miCopyRtf_Click(object sender, EventArgs e) {
            DataObject dto = new DataObject();
            dto.SetText(this.SelectedRtf, TextDataFormat.Rtf);
            dto.SetText(this.SelectedText, TextDataFormat.UnicodeText);
            Clipboard.Clear();
            Clipboard.SetDataObject(dto);
        }

        private void miSelectAll_Click(object sender, EventArgs e) {
            this.SelectAll();
        }
        #endregion Context Menu

        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miCopyText = new System.Windows.Forms.ToolStripMenuItem();
            this.miCopyRtf = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCopyText,
            this.miCopyRtf,
            this.toolStripSeparator1,
            this.miSelectAll});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(153, 76);
            // 
            // miCopyText
            // 
            this.miCopyText.Name = "miCopyText";
            this.miCopyText.Size = new System.Drawing.Size(152, 22);
            this.miCopyText.Text = "Copy Text";
            this.miCopyText.Click += new System.EventHandler(this.miCopyText_Click);
            // 
            // miCopyRtf
            // 
            this.miCopyRtf.Name = "miCopyRtf";
            this.miCopyRtf.Size = new System.Drawing.Size(152, 22);
            this.miCopyRtf.Text = "Copy Rich Text";
            this.miCopyRtf.Click += new System.EventHandler(this.miCopyRtf_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // miSelectAll
            // 
            this.miSelectAll.Name = "miSelectAll";
            this.miSelectAll.Size = new System.Drawing.Size(152, 22);
            this.miSelectAll.Text = "Select All";
            this.miSelectAll.Click += new System.EventHandler(this.miSelectAll_Click);
            // 
            // ucXmlRichTextBox
            // 
            this.ContextMenuStrip = this.contextMenuStrip2;
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
    }

    #region Extension Methods
    static class RichTextBoxExtensions {
        public static void AppendText(this RichTextBox box, string text, Color color) {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
    #endregion Extension Methods

    public class XmlStateMachine {
        #region Public Fields
        public XmlTokenType CurrentState = XmlTokenType.Unknown;
        #endregion Public Fields

        #region Private Fields
        private string subString = "";
        private string token = string.Empty;
        private Stack<XmlTokenType> PreviousStates = new Stack<XmlTokenType>();
        #endregion Private Fields

        #region Public Methods
        public string GetNextToken(string s, int location, out XmlTokenType ttype) {
            ttype = XmlTokenType.Unknown;

            // skip past any whitespace (token added to it at the end of method)
            string whitespace = GetWhitespace(s, location);
            if (!String.IsNullOrEmpty(whitespace)) {
                location += whitespace.Length;
            }

            subString = s.Substring(location, s.Length - location);
            token = string.Empty;

            if (CurrentState == XmlTokenType.CDataStart) {
                // check for empty CDATA
                if (subString.StartsWith("]]>")) {
                    CurrentState = XmlTokenType.CDataEnd;
                    token = "]]>";
                } else {
                    CurrentState = XmlTokenType.CDataValue;
                    int n = subString.IndexOf("]]>");
                    token = subString.Substring(0, n);
                }
            } else if (CurrentState == XmlTokenType.DocTypeStart) {
                CurrentState = XmlTokenType.DocTypeName;
                token = "DOCTYPE";
            } else if (CurrentState == XmlTokenType.DocTypeName) {
                CurrentState = XmlTokenType.DocTypeDeclaration;
                int n = subString.IndexOf("[");
                token = subString.Substring(0, n);
            } else if (CurrentState == XmlTokenType.DocTypeDeclaration) {
                CurrentState = XmlTokenType.DocTypeDefStart;
                token = "[";
            } else if (CurrentState == XmlTokenType.DocTypeDefStart) {
                if (subString.StartsWith("]>")) {
                    CurrentState = XmlTokenType.DocTypeDefEnd;
                    token = "]>";
                } else {
                    CurrentState = XmlTokenType.DocTypeDefValue;
                    int n = subString.IndexOf("]>");
                    token = subString.Substring(0, n);
                }
            } else if (CurrentState == XmlTokenType.DocTypeDefValue) {
                CurrentState = XmlTokenType.DocTypeDefEnd;
                token = "]>";
            } else if (CurrentState == XmlTokenType.DoubleQuotationMarkStart) {
                // check for empty attribute value
                if (subString[0] == '\"') {
                    CurrentState = XmlTokenType.DoubleQuotationMarkEnd;
                    token = "\"";
                } else {
                    CurrentState = XmlTokenType.AttributeValue;
                    int n = subString.IndexOf("\"");
                    token = subString.Substring(0, n);
                }
            } else if (CurrentState == XmlTokenType.SingleQuotationMarkStart) {
                // check for empty attribute value
                if (subString[0] == '\'') {
                    CurrentState = XmlTokenType.SingleQuotationMarkEnd;
                    token = "\'";
                } else {
                    CurrentState = XmlTokenType.AttributeValue;
                    int n = subString.IndexOf("'");
                    token = subString.Substring(0, n);
                }
            } else if (CurrentState == XmlTokenType.CommentStart) {
                // check for empty comment
                if (subString.StartsWith("-->")) {
                    CurrentState = XmlTokenType.CommentEnd;
                    token = "-->";
                } else {
                    CurrentState = XmlTokenType.CommentValue;
                    token = ReadCommentValue(subString, location);
                }
            } else if (CurrentState == XmlTokenType.NodeStart) {
                CurrentState = XmlTokenType.NodeName;
                token = ReadNodeName(subString, location);
            } else if (CurrentState == XmlTokenType.XmlDeclarationStart) {
                CurrentState = XmlTokenType.NodeName;
                token = ReadNodeName(subString, location);
            } else if (CurrentState == XmlTokenType.NodeName) {
                if (subString[0] != '/' &&
                    subString[0] != '>') {
                    CurrentState = XmlTokenType.AttributeName;
                    token = ReadAttributeName(subString, location);
                } else {
                    HandleReservedXmlToken();
                }
            } else if (CurrentState == XmlTokenType.NodeEndValueStart) {
                if (subString[0] == '<') {
                    HandleReservedXmlToken();
                } else {
                    CurrentState = XmlTokenType.NodeValue;
                    token = ReadNodeValue(subString, location);
                }
            } else if (CurrentState == XmlTokenType.DoubleQuotationMarkEnd) {
                HandleAttributeEnd(location);
            } else if (CurrentState == XmlTokenType.SingleQuotationMarkEnd) {
                HandleAttributeEnd(location);
            } else {
                HandleReservedXmlToken();
            }

            if (token != string.Empty) {
                ttype = CurrentState;
                return whitespace + token;
            }

            return string.Empty;

        }

        public Color GetTokenColor(XmlTokenType ttype) {
            Color brown = Color.FromArgb(238, 149, 68);

            switch (ttype) {
                case XmlTokenType.NodeValue:
                case XmlTokenType.EqualSignStart:
                case XmlTokenType.EqualSignEnd:
                case XmlTokenType.DoubleQuotationMarkStart:
                case XmlTokenType.DoubleQuotationMarkEnd:
                case XmlTokenType.SingleQuotationMarkStart:
                case XmlTokenType.SingleQuotationMarkEnd:
                    return Color.Black;

                case XmlTokenType.XmlDeclarationStart:
                case XmlTokenType.XmlDeclarationEnd:
                case XmlTokenType.NodeStart:
                case XmlTokenType.NodeEnd:
                case XmlTokenType.NodeEndValueStart:
                case XmlTokenType.CDataStart:
                case XmlTokenType.CDataEnd:
                case XmlTokenType.CommentStart:
                case XmlTokenType.CommentEnd:
                case XmlTokenType.AttributeValue:
                case XmlTokenType.DocTypeStart:
                case XmlTokenType.DocTypeEnd:
                case XmlTokenType.DocTypeDefStart:
                case XmlTokenType.DocTypeDefEnd:
                    return Color.Blue;

                case XmlTokenType.CDataValue:
                case XmlTokenType.DocTypeDefValue:
                    return Color.Gray;

                case XmlTokenType.CommentValue:
                    return Color.Green;

                case XmlTokenType.DocTypeName:
                case XmlTokenType.NodeName:
                    return Color.Brown;

                case XmlTokenType.AttributeName:
                case XmlTokenType.DocTypeDeclaration:
                    return Color.Red;

                default:
                    return Color.Orange;
            }
        }

        public string GetXmlDeclaration(string s) {
            int start = s.IndexOf("<?");
            int end = s.IndexOf("?>");
            if (start > -1 &&
                end > start) {
                return s.Substring(start, end - start + 2);
            } else
                return string.Empty;
        }
        #endregion Public Methods

        #region Private Methods
        private void HandleAttributeEnd(int location) {
            if (subString.StartsWith(">")) {
                HandleReservedXmlToken();
            } else if (subString.StartsWith("/>")) {
                HandleReservedXmlToken();
            } else if (subString.StartsWith("?>")) {
                HandleReservedXmlToken();
            } else {
                CurrentState = XmlTokenType.AttributeName;
                token = ReadAttributeName(subString, location);
            }
        }

        private void HandleReservedXmlToken() {
            // check if state changer
            // <, >, =, </, />, <![CDATA[, <!--, -->

            if (subString.StartsWith("<![CDATA[")) {
                CurrentState = XmlTokenType.CDataStart;
                token = "<![CDATA[";
            } else if (subString.StartsWith("<!DOCTYPE")) {
                CurrentState = XmlTokenType.DocTypeStart;
                token = "<!";
            } else if (subString.StartsWith("</")) {
                CurrentState = XmlTokenType.NodeStart;
                token = "</";
            } else if (subString.StartsWith("<!--")) {
                CurrentState = XmlTokenType.CommentStart;
                token = "<!--";
            } else if (subString.StartsWith("<?")) {
                CurrentState = XmlTokenType.XmlDeclarationStart;
                token = "<?";
            } else if (subString.StartsWith("<")) {
                CurrentState = XmlTokenType.NodeStart;
                token = "<";
            } else if (subString.StartsWith("=")) {
                CurrentState = XmlTokenType.EqualSignStart;
                if (CurrentState == XmlTokenType.AttributeValue)
                    CurrentState = XmlTokenType.EqualSignEnd;
                token = "=";
            } else if (subString.StartsWith("?>")) {
                CurrentState = XmlTokenType.XmlDeclarationEnd;
                token = "?>";
            } else if (subString.StartsWith(">")) {
                CurrentState = XmlTokenType.NodeEndValueStart;
                token = ">";
            } else if (subString.StartsWith("-->")) {
                CurrentState = XmlTokenType.CommentEnd;
                token = "-->";
            } else if (subString.StartsWith("]>")) {
                CurrentState = XmlTokenType.DocTypeEnd;
                token = "]>";
            } else if (subString.StartsWith("]]>")) {
                CurrentState = XmlTokenType.CDataEnd;
                token = "]]>";
            } else if (subString.StartsWith("/>")) {
                CurrentState = XmlTokenType.NodeEnd;
                token = "/>";
            } else if (subString.StartsWith("\"")) {
                if (CurrentState == XmlTokenType.AttributeValue) {
                    CurrentState = XmlTokenType.DoubleQuotationMarkEnd;
                } else {
                    CurrentState = XmlTokenType.DoubleQuotationMarkStart;
                }
                token = "\"";
            } else if (subString.StartsWith("'")) {
                CurrentState = XmlTokenType.SingleQuotationMarkStart;
                if (CurrentState == XmlTokenType.AttributeValue) {
                    CurrentState = XmlTokenType.SingleQuotationMarkEnd;
                }
                token = "'";
            }
        }

        private List<string> GetAttributeTokens(string s) {
            List<string> list = new List<string>();
            string[] arr = s.Split(' ');
            for (int i = 0; i < arr.Length; i++) {
                arr[i] = arr[i].Trim();
                if (arr[i].Length > 0)
                    list.Add(arr[i]);
            }
            return list;
        }

        private string ReadNodeName(string s, int location) {
            string nodeName = "";

            for (int i = 0; i < s.Length; i++) {
                if (s[i] == '/' ||
                    s[i] == ' ' ||
                    s[i] == '>') {
                    return nodeName;
                } else
                    nodeName += s[i].ToString();
            }

            return nodeName;
        }

        private string ReadAttributeName(string s, int location) {
            string attName = "";

            int n = s.IndexOf('=');
            if (n != -1)
                attName = s.Substring(0, n);

            return attName;
        }

        private string ReadNodeValue(string s, int location) {
            string nodeValue = "";

            int n = s.IndexOf('<');
            if (n != -1)
                nodeValue = s.Substring(0, n);

            return nodeValue;
        }

        private string ReadCommentValue(string s, int location) {
            string commentValue = "";

            int n = s.IndexOf("-->");
            if (n != -1)
                commentValue = s.Substring(0, n);

            return commentValue;
        }



        private string GetWhitespace(string s, int location) {
            bool foundWhitespace = false;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; (location + i) < s.Length; i++) {
                char c = s[location + i];
                if (Char.IsWhiteSpace(c)) {
                    foundWhitespace = true;
                    sb.Append(c);
                } else
                    break;
            }
            if (foundWhitespace)
                return sb.ToString();
            return String.Empty;
        }
        #endregion Private Methods
    }

    public enum XmlTokenType {
        Whitespace, XmlDeclarationStart, XmlDeclarationEnd, NodeStart, NodeEnd, NodeEndValueStart, NodeName, NodeValue,
        AttributeName, AttributeValue, EqualSignStart, EqualSignEnd, CommentStart, CommentValue, CommentEnd, CDataStart,
        CDataValue, CDataEnd, DoubleQuotationMarkStart, DoubleQuotationMarkEnd, SingleQuotationMarkStart, SingleQuotationMarkEnd,
        DocTypeStart, DocTypeName, DocTypeDeclaration, DocTypeDefStart, DocTypeDefValue, DocTypeDefEnd, DocTypeEnd,
        DocumentEnd, Unknown
    }
}
