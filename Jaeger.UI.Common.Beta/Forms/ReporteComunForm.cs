﻿using System;
using System.IO;
using Microsoft.Reporting.WinForms;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms { 
    public partial class ReporteComunForm : Form {
        public ReporteComunForm() {
            InitializeComponent();
        }

        public ReporteComunForm(string rfc, string razonSocial) {
            InitializeComponent();
            this.RFC = rfc;
            this.RazonSocial = razonSocial;
        }

        public string RFC {
            get; set;
        }

        public string RazonSocial {
            get; set;
        }

        public string ImagenQR {
            get; set;
        }

        private string pathLogo;
        private string base64String;

        public string PathLogo {
            get {
                return this.pathLogo;
            }
            set {
                this.pathLogo = value;
            }
        }

        public string LogoB64 {
            get; set;
        }

        public object CurrentObject {
            get; set;
        }

        public Stream LoadDefinition {
            set {
                this.Viewer.LocalReport.LoadReportDefinition(value);
            }
        }

        private void ReporteComunForm_Load(object sender, EventArgs e) {
            if (File.Exists(this.PathLogo)) {
                byte[] _logo = File.ReadAllBytes(this.PathLogo);
                this.LogoB64 = Convert.ToBase64String(_logo);
            } else {
                this.LogoB64 = this.ImageToBase64();
            }

            if (this.LogoB64.Length <= 0) {
                this.LogoB64 = "iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAIAAADYYG7QAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAGdJREFUWEft0rERACEMBDHov2j/U8GmDkR6iUfsnZmz6v0HrXpn1TXvuxwUAoQqEUKESqB2DREqgdo1RKgEatcQoRKoXUOESqB2DREqgdo1RKgEatcQoRKoXUOESqB2DREqgdrXNfQB4NNaFRTRJc8AAAAASUVORK5CYII=";
            }
        }

        public void SetParameter(string nameParameter, string value) {
            this.Viewer.LocalReport.SetParameters(new ReportParameter(nameParameter, value));
        }

        public void SetDataSource(string name, System.Data.DataTable data) {
            this.Viewer.LocalReport.DataSources.Add(new ReportDataSource(name, data));
        }

        public void SetDisplayName(string name) {
            this.Viewer.LocalReport.DisplayName = name;
        }

        public void Procesar() {
            var lista = this.Viewer.LocalReport.GetParameters();
            foreach (var item in lista) {
                if (item.Name == "Logotipo") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter("Logotipo", this.LogoB64));
                } else if (item.Name == "Empresa") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter("Empresa", this.RazonSocial));
                } else if (item.Name == "ImagenQR") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter("ImagenQR", this.ImagenQR));
                } else if (item.Name == "RFC") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter("RFC", this.RFC));
                } else if (item.Name == "Domicilio") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter("Domicilio", this.RFC));
                }
            }
        }

        public void SetLogotipo() {
            if (File.Exists(this.PathLogo)) {
                byte[] _logo = File.ReadAllBytes(this.PathLogo);
                this.LogoB64 = Convert.ToBase64String(_logo);
            } else {
                this.LogoB64 = this.ImageToBase64();
            }

            var lista = this.Viewer.LocalReport.GetParameters();
            foreach (var item in lista) {
                if (item.Name == "Logotipo") {
                    this.Viewer.LocalReport.SetParameters(new ReportParameter("Logotipo", this.LogoB64));
                }
            }
        }

        public void Finalizar() {
            this.Viewer.RefreshReport();
            this.Viewer.SetDisplayMode(DisplayMode.PrintLayout);
            this.Viewer.ZoomMode = ZoomMode.Percent;
            this.Viewer.ZoomPercent = 100;
            this.Viewer = null;
        }

        private string ImageToBase64() {
            using (System.Drawing.Image image = Properties.Resources.logo_tipo) {
                using (MemoryStream m = new MemoryStream()) {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }
    }
}
