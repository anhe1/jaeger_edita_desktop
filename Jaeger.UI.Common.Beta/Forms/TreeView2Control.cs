﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace Jaeger.UI.Common.Forms {
    public partial class TreeView2Control : UserControl {
        XmlDocument document = new XmlDocument();

        public TreeView2Control() {
            InitializeComponent();
        }

        private void TreeView2Control_Load(object sender, EventArgs e) {

        }

        public void Crear(string texto) {
            var encodedString = Convert.FromBase64String(texto);
            // Encode the XML string in a UTF-8 byte array
            //byte[] encodedString = Encoding.UTF8.GetBytes(texto);

            // Put the byte array into a stream and rewind it to the beginning
            MemoryStream ms = new MemoryStream();
            ms.Write(encodedString, 0, encodedString.Length);
            ms.Seek(0, SeekOrigin.Begin);
            ms.Flush();
            ms.Position = 0;
            StreamReader sr = new StreamReader(ms);

            if (treeView.Nodes.Count > 0) {
                treeView.Nodes.RemoveAt(0);
                Atributos.Text = string.Empty;
            }
            document.LoadXml(sr.ReadToEnd());
            // Upload root element and his childs in treeList
            XmlNode xn = document.DocumentElement;

            int counter = 0;
            TreeNode node = new TreeNode(xn.LocalName);
            treeView.Nodes.Add(node);

            XmlNodeList childs = xn.ChildNodes;
            foreach (XmlNode child in childs) {
                treeView.Nodes[counter].Nodes.Add(new TreeNode(child.LocalName));
            }
            counter++;
        }

        private void TreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e) {
            if (treeView.SelectedNode.Nodes.Count != 0
                && treeView.SelectedNode.Nodes[0].Nodes.Count == 0) {
                try {
                    Dictionary<string, int> foundedElems = new Dictionary<string, int>();

                    string tagName = string.Empty;
                    TreeNode chxn = new TreeNode();
                    int c = 0;
                    for (int indTreeElem = 0; indTreeElem < treeView.SelectedNode.Nodes.Count; indTreeElem++) {
                        c = 0;
                        chxn = treeView.SelectedNode.Nodes[indTreeElem];
                        tagName = GetTagName(new TreeViewEventArgs(chxn));
                        try {
                            foundedElems.Contains(new KeyValuePair<string, int>(tagName, foundedElems[tagName]));
                        } catch (KeyNotFoundException) {
                            foundedElems.Add(tagName, 0);
                        }

                        XmlNodeList xnl = GetElemInDoc(sender, new TreeViewEventArgs(chxn), true).ChildNodes;

                        foreach (XmlNode xn in xnl) {
                            if (xn.NodeType == XmlNodeType.Element) {
                                treeView.SelectedNode.Nodes[indTreeElem].Nodes.Add(xn.LocalName);
                                c++;
                            }
                        }
                        if (treeView.SelectedNode.Nodes[indTreeElem].Nodes.Count > c)
                            while (treeView.SelectedNode.Nodes[indTreeElem].Nodes.Count != c)
                                treeView.SelectedNode.Nodes[indTreeElem].Nodes.RemoveAt(treeView.SelectedNode.Nodes[indTreeElem].Nodes.Count - 1);
                    }

                } catch (Exception ex) {
                    string s = ex.Message;
                }
            }
        }

        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e) {

            string tagName = GetTagName(e);

            XmlNode xnode = GetElemInDoc(sender, e);

            string attrs = string.Empty;
            if (xnode.Attributes != null) {
                foreach (XmlAttribute xa in xnode.Attributes) {
                    attrs += string.Format("{0}=\"{1}\"\t", xa.Name, xa.Value);
                }

                textBoxName.Text = string.Format(xnode.Name);
                textBoxPath.Text = string.Format(treeView.SelectedNode.FullPath);
                Atributos.Text = string.Format("Atributos: {0}", attrs);
            }
        }

        private void TreeView_AfterExpand(object sender, TreeViewEventArgs e) {
            TreeNodeMouseClickEventArgs ev = new TreeNodeMouseClickEventArgs(e.Node,
                new MouseButtons(), 0, 0, 0);
            ((TreeView)sender).SelectedNode = e.Node;
            TreeView_NodeMouseDoubleClick(sender, ev);
        }

        private string GetTagName(TreeViewEventArgs e) {
            string tagName = string.Empty;
            if (document.DocumentElement.Prefix != string.Empty) {
                tagName = document.DocumentElement.Prefix + ":";
            }
            return tagName + e.Node.Text;
        }

        private List<int> IndexPathXml(object sender, TreeViewEventArgs e, bool isDblClk = false) {
            List<int> indexes = new List<int>();
            indexes.Add(isDblClk == true ? e.Node.Index : treeView.SelectedNode.Index);

            TreeNode xnode = isDblClk == true ? e.Node : treeView.SelectedNode;
            while (xnode.Parent != null) {
                indexes.Add(xnode.Parent.Index);
                xnode = xnode.Parent;
            }
            return indexes;
        }

        private XmlNode GetElemInDoc(object sender, TreeViewEventArgs e, bool isDblClk = false) {
            List<int> indxs = IndexPathXml(sender, e, isDblClk);

            int c = indxs.Count > 2 ? 2 : indxs.Count;
            XmlNode xnode = document.DocumentElement.ChildNodes[indxs[indxs.Count - c]];
            int indElemType = 0;
            while (indxs.Count - 1 - c >= 0) {
                xnode = xnode.ChildNodes[0];
                while (true) {
                    if (xnode.NodeType == XmlNodeType.Element)
                        indElemType++;
                    if (indElemType == indxs[indxs.Count - 1 - c] + 1) {
                        indElemType = 0;
                        break;
                    }
                    xnode = xnode.NextSibling;
                }
                c++;
            }
            return xnode;
        }
    }
}
