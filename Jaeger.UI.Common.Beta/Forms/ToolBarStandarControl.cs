﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class ToolBarStandarControl : UserControl {
        public event EventHandler<EventArgs> ButtonNuevo_Click;
        public event EventHandler<EventArgs> ButtonEditar_Click;
        public event EventHandler<EventArgs> ButtonRemover_Click;
        public event EventHandler<EventArgs> ButtonGuardar_Click;
        public event EventHandler<EventArgs> ButtonImprimir_Click;
        public event EventHandler<EventArgs> ButtonActualizar_Click;
        public event EventHandler<EventArgs> ButtonCerrar_Click;

        public ToolBarStandarControl() {
            InitializeComponent();
        }

        protected void OnButtonNuevoClick(object sender, EventArgs e) {
            if (this.ButtonNuevo_Click != null)
                this.ButtonNuevo_Click(sender, e);
        }

        protected void OnButtonEditarClick(object sender, EventArgs e) {
            if (this.ButtonEditar_Click != null)
                this.ButtonEditar_Click(sender, e);
        }

        protected void OnButtonRemoverClick(object sender, EventArgs e) {
            if (this.ButtonRemover_Click != null)
                this.ButtonRemover_Click(sender, e);
        }

        protected void OnButtonGuardarClick(object sender, EventArgs e) {
            if (this.ButtonGuardar_Click != null)
                this.ButtonGuardar_Click(sender, e);
        }

        protected void OnButtonImprimirClick(object sender, EventArgs e) {
            if (this.ButtonImprimir_Click != null)
                this.ButtonImprimir_Click(sender, e);
        }

        protected void OnButtonActualizarClick(object sender, EventArgs e) {
            if (this.ButtonActualizar_Click != null)
                this.ButtonActualizar_Click(sender, e);
        }

        protected void OnButtonCerrarClick(object sender, EventArgs e) {
            if (this.ButtonCerrar_Click != null)
                this.ButtonCerrar_Click(sender, e);
        }

        private void ToolBarStandarControl_Load(object sender, EventArgs e) {
            this.Nuevo.Click += new EventHandler(this.OnButtonNuevoClick);
            this.Editar.Click += new EventHandler(this.OnButtonEditarClick);
            this.Remover.Click += new EventHandler(this.OnButtonRemoverClick);
            this.Guardar.Click += new EventHandler(this.OnButtonGuardarClick);
            this.Imprimir.Click += new EventHandler(this.OnButtonImprimirClick);
            this.Actualizar.Click += new EventHandler(this.OnButtonActualizarClick);
            this.Cerrar.Click += new EventHandler(this.OnButtonCerrarClick);
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public string Etiqueta {
            get {
                return this.Titulo.Text;
            }
            set {
                this.Titulo.Text = value;
                if (this.Titulo.Text.Length > 0) {
                    this.Titulo.Visible = true;
                    this.Separador.Visible = true;
                } else {
                    this.Titulo.Visible = false;
                    this.Separador.Visible = false;
                }
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visible;
            }
            set {
                this.Nuevo.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visible;
            }
            set {
                this.Editar.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowRemover {
            get {
                return this.Remover.Visible;
            }
            set {
                this.Remover.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowGuardar {
            get {
                return this.Guardar.Visible;
            }
            set {
                this.Guardar.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visible;
            }
            set {
                this.Actualizar.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visible;
            }
            set {
                this.Imprimir.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visible;
            }
            set {
                this.Herramientas.Visible = value;
            }
        }

        [Description("Test text displayed in the textbox"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visible;
            }
            set {
                this.Cerrar.Visible = value;
            }
        }
    }
}
