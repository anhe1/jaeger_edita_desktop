﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBarStandarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.Titulo = new System.Windows.Forms.ToolStripLabel();
            this.Separador = new System.Windows.Forms.ToolStripSeparator();
            this.Nuevo = new System.Windows.Forms.ToolStripButton();
            this.Editar = new System.Windows.Forms.ToolStripButton();
            this.Remover = new System.Windows.Forms.ToolStripButton();
            this.Guardar = new System.Windows.Forms.ToolStripButton();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.Imprimir = new System.Windows.Forms.ToolStripButton();
            this.Herramientas = new System.Windows.Forms.ToolStripDropDownButton();
            this.Cerrar = new System.Windows.Forms.ToolStripButton();
            this.ToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Titulo,
            this.Separador,
            this.Nuevo,
            this.Editar,
            this.Remover,
            this.Guardar,
            this.Actualizar,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(661, 25);
            this.ToolBar.TabIndex = 1;
            this.ToolBar.Text = "toolStrip1";
            // 
            // Titulo
            // 
            this.Titulo.Name = "Titulo";
            this.Titulo.Size = new System.Drawing.Size(37, 22);
            this.Titulo.Text = "Titulo";
            // 
            // Separador
            // 
            this.Separador.Name = "Separador";
            this.Separador.Size = new System.Drawing.Size(6, 25);
            // 
            // Nuevo
            // 
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.add_16px;
            this.Nuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Size = new System.Drawing.Size(62, 22);
            this.Nuevo.Text = "Nuevo";
            // 
            // Editar
            // 
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_16px;
            this.Editar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(57, 22);
            this.Editar.Text = "Editar";
            // 
            // Remover
            // 
            this.Remover.Image = global::Jaeger.UI.Common.Properties.Resources.delete_16px;
            this.Remover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(74, 22);
            this.Remover.Text = "Remover";
            // 
            // Guardar
            // 
            this.Guardar.Image = global::Jaeger.UI.Common.Properties.Resources.save_16px;
            this.Guardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(69, 22);
            this.Guardar.Text = "Guardar";
            // 
            // Actualizar
            // 
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(79, 22);
            this.Actualizar.Text = "Actualizar";
            // 
            // Imprimir
            // 
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Size = new System.Drawing.Size(73, 22);
            this.Imprimir.Text = "Imprimir";
            // 
            // Herramientas
            // 
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Size = new System.Drawing.Size(107, 22);
            this.Herramientas.Text = "Herramientas";
            // 
            // Cerrar
            // 
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(59, 22);
            this.Cerrar.Text = "Cerrar";
            // 
            // ToolBarStandarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolBar);
            this.Name = "ToolBarStandarControl";
            this.Size = new System.Drawing.Size(661, 25);
            this.Load += new System.EventHandler(this.ToolBarStandarControl_Load);
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolBar;
        public System.Windows.Forms.ToolStripButton Cerrar;
        public System.Windows.Forms.ToolStripButton Nuevo;
        public System.Windows.Forms.ToolStripButton Editar;
        public System.Windows.Forms.ToolStripButton Remover;
        public System.Windows.Forms.ToolStripButton Guardar;
        public System.Windows.Forms.ToolStripButton Imprimir;
        private System.Windows.Forms.ToolStripLabel Titulo;
        private System.Windows.Forms.ToolStripSeparator Separador;
        public System.Windows.Forms.ToolStripDropDownButton Herramientas;
        public System.Windows.Forms.ToolStripButton Actualizar;
    }
}
