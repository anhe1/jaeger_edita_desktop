﻿namespace Jaeger.UI.Common.Forms {
    partial class ToolBar1CommonControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolBarLabelPeriodo = new System.Windows.Forms.ToolStripLabel();
            this.Periodo = new System.Windows.Forms.ToolStripComboBox();
            this.ToolBarLabelEjercicio = new System.Windows.Forms.ToolStripLabel();
            this.ToolBarHostEjercicio = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Nuevo = new System.Windows.Forms.ToolStripDropDownButton();
            this.Editar = new System.Windows.Forms.ToolStripButton();
            this.Cancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Actualizar = new System.Windows.Forms.ToolStripButton();
            this.Imprimir = new System.Windows.Forms.ToolStripButton();
            this.Herramientas = new System.Windows.Forms.ToolStripDropDownButton();
            this.ExportarExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.Cerrar = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarLabelPeriodo,
            this.Periodo,
            this.ToolBarLabelEjercicio,
            this.ToolBarHostEjercicio,
            this.toolStripSeparator1,
            this.Nuevo,
            this.Editar,
            this.Cancelar,
            this.toolStripSeparator2,
            this.Actualizar,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(794, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolBarLabelPeriodo
            // 
            this.ToolBarLabelPeriodo.Name = "ToolBarLabelPeriodo";
            this.ToolBarLabelPeriodo.Size = new System.Drawing.Size(51, 22);
            this.ToolBarLabelPeriodo.Text = "Período:";
            // 
            // Periodo
            // 
            this.Periodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Periodo.Name = "Periodo";
            this.Periodo.Size = new System.Drawing.Size(95, 25);
            // 
            // ToolBarLabelEjercicio
            // 
            this.ToolBarLabelEjercicio.Name = "ToolBarLabelEjercicio";
            this.ToolBarLabelEjercicio.Size = new System.Drawing.Size(54, 22);
            this.ToolBarLabelEjercicio.Text = "Ejercicio:";
            // 
            // ToolBarHostEjercicio
            // 
            this.ToolBarHostEjercicio.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolBarHostEjercicio.Name = "ToolBarHostEjercicio";
            this.ToolBarHostEjercicio.Size = new System.Drawing.Size(50, 25);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // Nuevo
            // 
            this.Nuevo.Image = global::Jaeger.UI.Common.Properties.Resources.new_file_16px;
            this.Nuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Size = new System.Drawing.Size(71, 22);
            this.Nuevo.Text = "Nuevo";
            // 
            // Editar
            // 
            this.Editar.Image = global::Jaeger.UI.Common.Properties.Resources.edit_16px;
            this.Editar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(57, 22);
            this.Editar.Text = "Editar";
            // 
            // Cancelar
            // 
            this.Cancelar.Image = global::Jaeger.UI.Common.Properties.Resources.cancel_16px;
            this.Cancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(73, 22);
            this.Cancelar.Text = "Cancelar";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // Actualizar
            // 
            this.Actualizar.Image = global::Jaeger.UI.Common.Properties.Resources.refresh_16px1;
            this.Actualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(79, 22);
            this.Actualizar.Text = "Actualizar";
            // 
            // Imprimir
            // 
            this.Imprimir.Image = global::Jaeger.UI.Common.Properties.Resources.print_16px;
            this.Imprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Size = new System.Drawing.Size(73, 22);
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.Visible = false;
            // 
            // Herramientas
            // 
            this.Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportarExcel});
            this.Herramientas.Image = global::Jaeger.UI.Common.Properties.Resources.toolbox_16px;
            this.Herramientas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Size = new System.Drawing.Size(107, 22);
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.Visible = false;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Image = global::Jaeger.UI.Common.Properties.Resources.xls_16px;
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Size = new System.Drawing.Size(157, 22);
            this.ExportarExcel.Text = "Exportar a Excel";
            // 
            // Cerrar
            // 
            this.Cerrar.Image = global::Jaeger.UI.Common.Properties.Resources.close_window_16px;
            this.Cerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(59, 20);
            this.Cerrar.Text = "Cerrar";
            // 
            // ToolBar1CommonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStrip1);
            this.Name = "ToolBar1CommonControl";
            this.Size = new System.Drawing.Size(794, 25);
            this.Load += new System.EventHandler(this.ToolBar1CommonControl_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel ToolBarLabelPeriodo;
        private System.Windows.Forms.ToolStripLabel ToolBarLabelEjercicio;
        private System.Windows.Forms.ToolStripTextBox ToolBarHostEjercicio;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripButton Cancelar;
        public System.Windows.Forms.ToolStripButton Actualizar;
        public System.Windows.Forms.ToolStripButton Cerrar;
        public System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripComboBox Periodo;
        private System.Windows.Forms.ToolStripMenuItem ExportarExcel;
        public System.Windows.Forms.ToolStripDropDownButton Herramientas;
        public System.Windows.Forms.ToolStripButton Imprimir;
        public System.Windows.Forms.ToolStripButton Editar;
        public System.Windows.Forms.ToolStripDropDownButton Nuevo;
    }
}
