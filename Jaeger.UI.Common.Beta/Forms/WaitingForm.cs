﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Common.Forms {
    public partial class WaitingForm : Form
    {
        public Action Worker { get; set; }

        public WaitingForm(Action worker)
        {
            InitializeComponent();
            if (worker == null)
                throw new ArgumentOutOfRangeException();
            this.Worker = worker;
            this.Text = "";
        }

        private void ViewWaiting2Form_Load(object sender, EventArgs e)
        {
            if (this.Text != "")
                this.label1.Text = this.Text;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(this.Worker).ContinueWith(it => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
