﻿using System;
using System.Threading;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Helpers;

namespace Jaeger.Aplication.Base.Abstracts {
    public abstract class WorkerBase : IWorker, IDisposable {
        #region
        protected Thread _worker;
        protected bool _cancelled;
        protected ProgressEventArgs _pevent;
        protected string _workerName;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public WorkerBase(string workerName) {
            _workerName = workerName;
            this._pevent = new ProgressEventArgs(false, 0, null, null) {
                NestedProgress = new ProgressEventArgs(false, 0, null, null)
            };
        }

        public event EventHandler<ProgressEventArgs> ProgressChanged;

        #region propiedades
        public abstract bool SupportsDualProgress { get; }

        public abstract bool IsCancelable { get; }

        public abstract object Result { get; }

        public virtual string Caption { get { return "Procesando ..."; } }
        #endregion

        #region implementation
        public abstract void BeginWork();

        public virtual void Cancel() {
            _cancelled = true;
            //IWorker comparer = this;
            //if (comparer != null)
            //    comparer.Cancel();
        }

        public virtual void Clear() {

        }

        public virtual void Dispose() {
            GC.SuppressFinalize(this);
        }
        #endregion

        protected virtual double Progress(int index, int count) {
            return (index * 100) / count;
        }

        protected virtual Thread Thread(ThreadStart ts) {
            return new Thread(ts) {
                IsBackground = true,
                Name = this._workerName
            };
        }

        #region eventos
        protected void NotifyPrimaryProgress(bool done, int progress, Exception error) {
            _pevent.IsDone = done;
            _pevent.Progress = progress;
            _pevent.Message = null;
            _pevent.Error = error;

            if (ProgressChanged != null)
                ProgressChanged(this, _pevent);
        }

        protected void NotifyPrimaryProgress(bool done, int progress, string msg) {
            _pevent.IsDone = done;
            _pevent.Progress = progress;
            _pevent.Message = msg;
            _pevent.Error = null;

            if (ProgressChanged != null)
                ProgressChanged(this, _pevent);
        }

        /// <summary>
        /// notificacion primaria
        /// </summary>
        /// <param name="progress">% proceso</param>
        /// <param name="error"></param>
        protected void NotifyPrimaryProgress(int progress, string msg) {
            _pevent.IsDone = false;
            _pevent.Progress = progress;
            _pevent.Message = msg;
            _pevent.Error = null;

            if (ProgressChanged != null)
                ProgressChanged(this, _pevent);
        }

        /// <summary>
        /// notificacion secundaria
        /// </summary>
        /// <param name="done">concluido</param>
        /// <param name="progress">porcentaje de progreso</param>
        /// <param name="error">mensaje</param>
        protected void NotifySecondaryProgress(bool done, int progress, Exception error) {
            _pevent.NestedProgress.IsDone = done;
            _pevent.NestedProgress.Progress = progress;
            _pevent.NestedProgress.Message = null;
            _pevent.NestedProgress.Error = error;

            if (ProgressChanged != null)
                ProgressChanged(this, _pevent);
        }

        /// <summary>
        /// notificacion secundaria
        /// </summary>
        /// <param name="done">concluido</param>
        /// <param name="progress">porcentaje de progreso</param>
        /// <param name="msg">mensaje</param>
        protected void NotifySecondaryProgress(bool done, int progress, string msg) {
            _pevent.NestedProgress.IsDone = done;
            _pevent.NestedProgress.Progress = progress;
            _pevent.NestedProgress.Message = msg;
            _pevent.NestedProgress.Error = null;

            if (ProgressChanged != null)
                ProgressChanged(this, _pevent);
        }
        #endregion
    }
}
