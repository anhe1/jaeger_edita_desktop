﻿using System;

namespace Jaeger.Aplication.Base.Helpers {
    /// <summary>
    /// Used to indicate that the user chose to cancel the operation
    /// </summary>
    public class UserCancellationException : Exception { }
}
