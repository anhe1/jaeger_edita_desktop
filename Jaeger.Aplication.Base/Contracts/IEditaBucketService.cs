﻿namespace Jaeger.Aplication.Base.Contracts {
    /// <summary>
    /// contrato para el servicio de almacenamiento AWS S3
    /// </summary>
    public interface IEditaBucketService {
        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        bool Produccion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el folder donde se deben almacenar los archivos en el bucket
        /// </summary>
        string Folder {
            get;set;
        }

        /// <summary>
        /// obtener el tipo de contenido del archivo basado en su extensión
        /// </summary>
        string ContenType(string localFileName);

        string Upload(string localFileName, string keyName);

        string Upload(string base64, string localFileName, string keyName);

        bool Download(string localFileName, string requestURL);

        /// <summary>
        /// comprobar la existencia de un objeto
        /// </summary>
        /// <param name="requestURL">url</param>
        /// <returns>verdadero si existe</returns>
        bool Exists(string requestURL);
    }
}
