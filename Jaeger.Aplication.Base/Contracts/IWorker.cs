﻿using System;
using Jaeger.Aplication.Base.Helpers;

namespace Jaeger.Aplication.Base.Contracts {
    /// <summary>
    /// Used to provide unified interface for invoking long running processes.
    /// </summary>
    public interface IWorker : IDisposable {
        /// <summary>
        /// Fired whenever the operation has progressed
        /// </summary>
        event EventHandler<ProgressEventArgs> ProgressChanged;

        /// <summary>
        /// Begin the operation
        /// </summary>
        void BeginWork();

        /// <summary>
        /// Cancel the operation
        /// </summary>
        void Cancel();

        /// <summary>
        /// 
        /// </summary>
        void Clear();

        /// <summary>
        /// Get the operation's result (valid only after the work has finished successfully).
        /// </summary>
        object Result {
            get;
        }

        /// <summary>
        /// In case the worker supports dual progress notifications (primary/secondary)
        /// this property will return TRUE.
        /// </summary>
        bool SupportsDualProgress {
            get;
        }

        bool IsCancelable {
            get;
        }

        /// <summary>
        /// obtener o establecer titulo para la ventana
        /// </summary>
        string Caption { get; }
    }
}
