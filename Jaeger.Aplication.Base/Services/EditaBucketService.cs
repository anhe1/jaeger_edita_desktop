﻿using System;
using System.IO;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Aplication.Base.Contracts;

namespace Jaeger.Aplication.Base.Services {
    /// <summary>
    /// Servicio de almacenamiento AWS S3
    /// </summary>
    public class EditaBucketService : IEditaBucketService {
        protected Amazon.S3.Contracts.ISimpleStorageService _S3;
        protected Amazon.S3.Helpers.FileContentType _ContentType;
        private SimpleStorageService _Configuracion;

        /// <summary>
        /// constructor
        /// </summary>
        public EditaBucketService() {
            // es modo productivo?
            this.Produccion = ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production;
            // configuracion del S3
            this._Configuracion = ConfigService.Synapsis.Amazon.S3;
            this._S3 = new Amazon.S3.Helpers.SimpleStorageServiceAWS3(_Configuracion.AccessKeyId, _Configuracion.SecretAccessKey, _Configuracion.Region);
            this._ContentType = new Amazon.S3.Helpers.FileContentType();
            this.Folder = _Configuracion.Folder;
        }

        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        public bool Produccion {
            get; set;
        }

        public string Folder {
            get;set;
        }

        /// <summary>
        /// obtener el tipo de contenido del archivo basado en su extensión
        /// </summary>
        public string ContenType(string localFileName) {
            return this._ContentType.ContentType(localFileName);
        }

        public bool Download(string localFileName, string requestURL) {
            return this._S3.Download(requestURL, localFileName);
        }

        /// <summary>
        /// comprobar la existencia de un objeto
        /// </summary>
        /// <param name="requestURL">url</param>
        /// <returns>verdadero si existe</returns>
        public bool Exists(string requestURL) {
            return this._S3.Exists(this.BucketFolder(), Path.GetFileName(requestURL));
        }

        public string Upload(string localFileName, string keyName) {
            var response = "";
            using (var fileToUpload = new FileStream(localFileName, FileMode.Open, FileAccess.Read)) {
                response = this._S3.Upload(fileToUpload, localFileName, this.CreateURL(keyName), this._ContentType.ContentType(localFileName), this._Configuracion.BucketName, this._Configuracion.Region, true);
            }
            return response;
        }

        public string Upload(string base64, string localFileName, string keyName) {
            if (string.IsNullOrEmpty(base64)) { return string.Empty;}
            var response = string.Empty;
            byte[] numArray = Convert.FromBase64String(base64);
            
            using (var fileToUpload = new MemoryStream(numArray)) {
                response = this._S3.Upload(fileToUpload, localFileName, this.CreateURL(keyName), this._ContentType.ContentType(localFileName), this._Configuracion.BucketName, this._Configuracion.Region, true);
            }
            return response;
        }

        private string CreateURL(string keyName) {
            if (this.Produccion) {
                if (this._Configuracion.Folder != "")
                    return string.Format("{0}/{1}", this.Folder, keyName);
            } else {
                return string.Format("{0}/{1}/{2}", "sandbox", this.Folder, keyName);
            }
            return keyName;
        }

        private string BucketFolder() {
            var bucketFolder = this._Configuracion.BucketName;
            if (this._Configuracion.Folder != "")
                bucketFolder = this._Configuracion.BucketName + "/" + this.Folder;
            return bucketFolder;
        }
    }
}
