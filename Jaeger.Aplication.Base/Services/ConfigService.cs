﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Profile;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Base.Services {
    /// <summary>
    /// servicio de configuracion 
    /// </summary>
    public class ConfigService : Contracts.IConfigService {
        /// <summary>
        /// modo de depuración
        /// </summary>
        public static bool IsDeveloper = false;

        /// <summary>
        /// nombre del usuario de sistema
        /// </summary>
        public static string Sysdba = "SYSDBA";

        /// <summary>
        /// objeto Rol de Administrador
        /// </summary>
        public static Rol Administrador = new Rol("administrador");

        /// <summary>
        /// Configuración de la empresa
        /// </summary>
        public static Configuracion Synapsis;

        /// <summary>
        /// Información del usuario activo
        /// </summary>
        public static KaijuLogger Piloto;

        /// <summary>
        /// informacion de menus de la aplicacion
        /// </summary>
        public static List<UIMenuElement> Menus;

        /// <summary>
        /// leyenda
        /// </summary>
        public static string EsperarServidor = "Esperando respuesta del servidor ...";

        /// <summary>
        /// titulo de aplicación
        /// </summary>
        public static string Titulo() {
            if (!string.IsNullOrEmpty(Synapsis.Empresa.NombreComercial)) {
                return string.Format("{0} [{1}]", Synapsis.Empresa.NombreComercial, Synapsis.Empresa.RFC);
            } else if (!string.IsNullOrEmpty(Synapsis.Empresa.RazonSocial)) {
                return string.Format("{0} [{1}]", Synapsis.Empresa.RazonSocial, Synapsis.Empresa.RFC);
            }
            return string.Format("{0} [{1}]", "No registrado ", Synapsis.Empresa.RFC);
        }

        /// <summary>
        /// obtener url de logo tipo
        /// </summary>
        /// <returns>url</returns>
        public static string UrlLogo() {
            return string.Format("https://s3.amazonaws.com/{0}/Conf/logo-{1}.png", ConfigService.Synapsis.Empresa.RFC.ToLower(), ConfigService.Synapsis.Empresa.Clave);
        }

        /// <summary>
        /// datos de prueba
        /// </summary>
        public static void IsTesting() {
            if (ConfigService.Synapsis.Empresa.RFC == "XAXX010101000") {
                Console.WriteLine("Modo Testing");
                ConfigService.Synapsis.Empresa.RegimenFiscal = "601";
                ConfigService.Synapsis.Empresa.RazonSocial = "Impresores Profesionales SA de CV";
                ConfigService.Synapsis.Empresa.RFC = "IPR981125PN9";
                ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal = "08100";
            }
        }

        /// <summary>
        /// obtener objeto del menu mediante la llave
        /// </summary>
        /// <param name="key">Llave</param>
        /// <returns>UIMenuElement</returns>
        public static UIMenuElement GeMenuElement(string key) {
            try {
                return Menus.Where(it => it.Name == key.ToLower()).FirstOrDefault();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return new UIMenuElement();
            }
        }

        /// <summary>
        /// permisos
        /// </summary>
        /// <param name="accessingObject">UIMenuElement</param>
        /// <returns>verdadero si tiene permisos</returns>
        public static bool HasPermission(UIMenuElement accessingObject) {
            if (accessingObject == null) {
                return false;
            }
            // get valid role for accessing object
            try {
                var validRoles = accessingObject.ListRol;
                // check whether user is valid for accessing object
                for (int i = 0; i < validRoles.Count; i++) {
                    Rol validRole = validRoles[i];
                    if (Piloto.IsInRole(validRole)) {
                        return true;
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// obtener lista de meses
        /// </summary>
        public static List<MesModel> GetMeses() {
            List<MesModel> enums = ((MesesEnum[])Enum.GetValues(typeof(MesesEnum))).Select(c => new MesModel((int)c, c.ToString())).ToList();
            return enums;
        }

        /// <summary>
        /// obtener el numero del mes correspondiente a la descripción del texto
        /// </summary>
        /// <param name="name">nombre del mes</param>
        /// <returns>numero correspondiente al mes</returns>
        public static int GetMeses(string name) {
            return (int)Enum.Parse(typeof(MesesEnum), name);
        }

        /// <summary>
        /// obtener listado de objetos model para la enumeracion CFDISubTipoEnum de los comprobantes fiscales
        /// </summary>
        /// <returns></returns>
        public static List<CFDISubTipoModel> CFDISubTipo() {
            return EnumerationExtension.GetEnumToClass<CFDISubTipoModel, CFDISubTipoEnum>().ToList();
        }

        /// <summary>
        /// obtener listado de objetos model para la enumeracion CFDIEstadoEnum de los estados SAT de un comprobante fiscal
        /// </summary>
        public static List<CFDIEstadoModel> CFDIEstado() {
            List<CFDIEstadoModel> enums = ((CFDIEstadoEnum[])Enum.GetValues(typeof(CFDIEstadoEnum))).Select(c => new CFDIEstadoModel((int)c, c.ToString())).ToList();
            return enums;
        }

        /// <summary>
        /// obtener listado de objetos model para la enumeracion CFDIStatusEmitidoEnum o CFDIStatusRecibidoEnum dependiendo del sub tipo del comprobante
        /// </summary>
        /// <param name="subTipoEnum">Emitido, Recibod, Nomina</param>
        public static List<CFDIStatusModel> CFDIStatus(CFDISubTipoEnum subTipoEnum) {
            switch (subTipoEnum) {
                case CFDISubTipoEnum.Emitido | CFDISubTipoEnum.Nomina:
                    return new List<CFDIStatusModel>(((CFDIStatusEmitidoEnum[])Enum.GetValues(typeof(CFDIStatusEmitidoEnum))).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
                case CFDISubTipoEnum.Recibido:
                    return new List<CFDIStatusModel>(((CFDIStatusRecibidoEnum[])Enum.GetValues(typeof(CFDIStatusRecibidoEnum))).Select(c => new CFDIStatusModel((int)c, c.ToString())).ToList());
            }
            return new List<CFDIStatusModel>();
        }

        /// <summary>
        /// obtener catalogo de monedas 
        /// </summary>
        /// <returns></returns>
        public static List<MonedaModel> GetMonedas() {
            var monedas = new List<MonedaModel> {
                new MonedaModel(0, "MXN"),
                new MonedaModel(1, "USD")
            };
            return monedas;
        }

        /// <summary>
        /// listado de impuestos aplicables
        /// </summary>
        public static List<ImpuestoModel> GetImpuestos() {
            return ((ImpuestoEnum[])Enum.GetValues(typeof(ImpuestoEnum))).Select(c => new ImpuestoModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// listado de factores aplicables a los tipos de impuesto
        /// </summary>
        /// <returns></returns>
        public static List<ImpuestoTipoFactorModel> GetImpuestoTipoFactor() {
            return ((ImpuestoTipoFactorEnum[])Enum.GetValues(typeof(ImpuestoTipoFactorEnum))).Select(c => new ImpuestoTipoFactorModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// listado tipos de impuestos
        /// </summary>
        /// <returns></returns>
        public static List<ImpuestoTipoModel> GetImpuestoTipo() {
            return ((ImpuestoTipoEnum[])Enum.GetValues(typeof(ImpuestoTipoEnum))).Select(c => new ImpuestoTipoModel((int)c, c.ToString())).ToList();
        }
    }
}
