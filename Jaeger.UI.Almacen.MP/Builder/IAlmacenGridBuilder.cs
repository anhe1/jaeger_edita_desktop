﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.MP.Builder {
    public interface IAlmacenGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IAlmacenTempleteGridBuilder Templetes();
    }

    public interface IAlmacenTempleteGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IAlmacenTempleteGridBuilder Master();

        IAlmacenTempleteGridBuilder Conceptos();

        IAlmacenTempleteGridBuilder Relacion();

        IAlmacenTempleteGridBuilder CStatus();

        IAlmacenTempleteGridBuilder Search();
    }

    public interface IAlmacenColumnsGridBuilder : IGridViewColumnsBuild {
        #region vales
        IAlmacenColumnsGridBuilder IdTipoComprobante();

        IAlmacenColumnsGridBuilder CheckActivo();

        IAlmacenColumnsGridBuilder SubId();

        IAlmacenColumnsGridBuilder TipoComprobante();

        IAlmacenColumnsGridBuilder Status();

        IAlmacenColumnsGridBuilder Folio();

        IAlmacenColumnsGridBuilder Serie();

        IAlmacenColumnsGridBuilder NoOrden();

        IAlmacenColumnsGridBuilder Receptor();

        IAlmacenColumnsGridBuilder ReceptorRFC();

        IAlmacenColumnsGridBuilder FechaEmision();

        IAlmacenColumnsGridBuilder FechaEntrega();

        IAlmacenColumnsGridBuilder FechaCancela();

        IAlmacenColumnsGridBuilder Cancela();

        IAlmacenColumnsGridBuilder ClaveCancelacion();

        IAlmacenColumnsGridBuilder Contacto();

        IAlmacenColumnsGridBuilder Referencia();

        IAlmacenColumnsGridBuilder Nota();

        IAlmacenColumnsGridBuilder Creo();

        IAlmacenColumnsGridBuilder IdDocumento();

        IAlmacenColumnsGridBuilder PDF();
        
        IAlmacenColumnsGridBuilder Modifica();
        IAlmacenColumnsGridBuilder FechaNuevo();
        IAlmacenColumnsGridBuilder FechaModifica();
        #endregion

        #region conceptos
        IAlmacenColumnsGridBuilder IdConcepto();
        IAlmacenColumnsGridBuilder IdUnidad();
        IAlmacenColumnsGridBuilder IdProducto();
        IAlmacenColumnsGridBuilder IdModelo();
        IAlmacenColumnsGridBuilder OrdenCompra();
        IAlmacenColumnsGridBuilder OrdenProduccion();
        IAlmacenColumnsGridBuilder Cantidad();
        IAlmacenColumnsGridBuilder Entrada();
        IAlmacenColumnsGridBuilder Salida();
        IAlmacenColumnsGridBuilder Unidad();
        IAlmacenColumnsGridBuilder Descripcion();
        IAlmacenColumnsGridBuilder Especificacion();
        IAlmacenColumnsGridBuilder Marca();
        IAlmacenColumnsGridBuilder NoIdentificacion();
        #endregion

        IAlmacenColumnsGridBuilder CveMotivo();
    }
}
