﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.MP.Services;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.MP.Builder {
    public class AlmacenGridBuilder : GridViewBuilder, IAlmacenGridBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IAlmacenColumnsGridBuilder,
        IGridViewColumnsBuild, IAlmacenTempleteGridBuilder {
        public AlmacenGridBuilder() : base() { }

        #region templetes
        public IAlmacenTempleteGridBuilder Templetes() {
            return this;
        }

        public IAlmacenTempleteGridBuilder Master() {
            this._Columns.Clear();
            this.IdTipoComprobante().CheckActivo().SubId().TipoComprobante().Status().Folio().Serie().ReceptorRFC().Receptor().IdDocumento().FechaEmision().Contacto().Referencia().FechaCancela().Cancela()
                .ClaveCancelacion().Nota().PDF().Creo().FechaNuevo();
            return this;
        }

        public IAlmacenTempleteGridBuilder Conceptos() {
            this._Columns.Clear();
            this.IdConcepto().CheckActivo().SubId().Cantidad().IdUnidad().Descripcion().Marca().Especificacion().NoIdentificacion().OrdenCompra().OrdenProduccion();
            return this;
        }

        /// <summary>
        /// relaciones entre comprobantes
        /// </summary>
        public IAlmacenTempleteGridBuilder Relacion() {
            this._Columns.Clear();
            this.IdTipoComprobante().Folio().Serie().FechaEmision().ReceptorRFC().Receptor().IdDocumento().Creo();
            return this;
        }

        /// <summary>
        /// templete de cambios de status
        /// </summary>
        public IAlmacenTempleteGridBuilder CStatus() {
            this._Columns.Clear();
            this.Status().CveMotivo().Nota().Creo().FechaNuevo();
            return this;
        }

        public IAlmacenTempleteGridBuilder Search() {
            this._Columns.Clear();
            this.IdTipoComprobante().Folio().Serie().ReceptorRFC().Receptor().FechaEmision().IdDocumento();
            return this;
        }
        #endregion

        #region columnas
        public IAlmacenColumnsGridBuilder IdTipoComprobante() {
            var combo = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdTipoComprobante",
                HeaderText = "Comprobante",
                IsVisible = true,
                Name = "IdTipoComprobante",
                ReadOnly = true,
                Width = 95
            };

            combo.DataSource = ValeAlmacenService.TipoOperacion();
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "IdDocumento";
            this._Columns.Add(combo);
            return this;
        }

        public new IAlmacenColumnsGridBuilder CheckActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                IsVisible = false,
                Name = "Activo",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder SubId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "SubId",
                HeaderText = "SubId",
                IsVisible = false,
                Name = "SubId",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder TipoComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoMovimiento",
                HeaderText = "Tipo",
                Name = "TipoMovimiento",
                ReadOnly = true,
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Status() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Status",
                HeaderText = "Status",
                Name = "Status",
                Width = 85, });
            return this;
        }

        public IAlmacenColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                FormatString = "{0:00000#}",
                HeaderText = "Folio",
                Name = "Folio",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85, });
            return this;
        }

        public IAlmacenColumnsGridBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                IsVisible = false,
                Name = "Serie",
                ReadOnly = true,
                Width = 75, });
            return this;
        }

        public IAlmacenColumnsGridBuilder NoOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "NoOrden",
                FormatString = "{0:00000#}",
                HeaderText = "O. C.",
                Name = "NoOrden",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Receptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Receptor",
                HeaderText = "Receptor",
                Name = "Receptor",
                ReadOnly = true,
                Width = 240
            }); return this;
        }

        public IAlmacenColumnsGridBuilder ReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "RFC",
                Name = "ReceptorRFC",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaEmision",
                DataType = typeof(DateTime),
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder FechaEntrega() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaEntrega",
                DataType = typeof(DateTime),
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Entrega",
                Name = "FechaEntrega",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85, });
            return this;
        }

        public IAlmacenColumnsGridBuilder FechaCancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaCancela",
                DataType = typeof(DateTime),
                FormatString = this.FormatStringDate,
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Cancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cancela",
                HeaderText = "Cancela",
                Name = "Cancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder ClaveCancelacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveCancelacion",
                HeaderText = "Cv. Cancelación",
                Name = "ClaveCancelacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Contacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Contacto",
                HeaderText = "Recibe",
                Name = "Contacto",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Referencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                ReadOnly = true,
                Width = 200
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                Name = "IdDocumento",
                ReadOnly = true,
                Width = 180,
                IsVisible = false
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder PDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFilePDF",
                HeaderText = "PDF",
                Name = "UrlFilePDF",
                ReadOnly = true,
                Width = 45,
                IsVisible = true
            });
            return this;
        }

        

        public IAlmacenColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Modifica() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = false,
                ReadOnly = true
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder FechaModifica() {
            _Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Mod.",
                Name = "FechaModifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }
        #endregion

        #region conceptos
        public IAlmacenColumnsGridBuilder IdConcepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdConcepto",
                HeaderText = "IdConcepto",
                Name = "IdConcepto",
                ReadOnly = true,
                IsVisible = false,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder IdUnidad() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdUnidad",
                HeaderText = "Unidad",
                Name = "IdUnidad",
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder IdProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdProducto",
                HeaderText = "IdProducto",
                Name = "IdProducto",
                ReadOnly = true,
                IsVisible = false,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder IdModelo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdModelo",
                HeaderText = "IdModelo",
                Name = "IdModelo",
                ReadOnly = true,
                IsVisible = false,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder OrdenCompra() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "OrdenCompra",
                HeaderText = "Ord. Compra",
                Name = "OrdenCompra",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder OrdenProduccion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "OrdenProduccion",
                HeaderText = "Ord. Prod.",
                Name = "OrdenProduccion",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringNumber,
                FieldName = "Cantidad",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                ReadOnly = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Entrada() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringNumber,
                FieldName = "Entrada",
                HeaderText = "Cantidad",
                Name = "Entrada",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Salida() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FormatString = this.FormatStringNumber,
                FieldName = "Salida",
                HeaderText = "Cantidad",
                Name = "Salida",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                ReadOnly = true,
                Width = 285
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Especificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Especificacion",
                HeaderText = "Especificación",
                Name = "Especificacion",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder Marca() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Marca",
                HeaderText = "Marca",
                Name = "Marca",
                ReadOnly = true,
                Width = 150
            });
            return this;
        }

        public IAlmacenColumnsGridBuilder NoIdentificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "NoIdentificacion",
                HeaderText = "No. Identificación",
                Name = "NoIdentificacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 95
            });
            return this;
        }
        #endregion

        #region historial de status
        public IAlmacenColumnsGridBuilder CveMotivo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CveMotivo",
                HeaderText = "Cv. Motivo",
                Name = "CveMotivo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 85
            });
            return this;
        }
        #endregion

        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, ValeAlmacenConceptoDetailModel item) {
            row.Cells["Cantidad"].Value = item.Cantidad;
            row.Cells["IdUnidad"].Value = item.IdUnidad;
            //row.Cells["Unidad"].Value = item.Unidad;
            row.Cells["Descripcion"].Value = item.Descripcion;
            row.Cells["Especificacion"].Value = item.Especificacion;
            row.Cells["Marca"].Value = item.Marca;
            row.Cells["NoIdentificacion"].Value = item.NoIdentificacion;
            row.Cells["OrdenCompra"].Value = item.OrdenCompra;
            row.Cells["OrdenProduccion"].Value = item.OrdenProduccion;
            return row;
        }

        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, ValeAlmacenRelacionModel item) {
            row.Cells["IdTipoComprobante"].Value = item.IdTipoComprobante;
            row.Cells["Folio"].Value = item.Folio;
            row.Cells["Serie"].Value = item.Serie;
            row.Cells["ReceptorRFC"].Value = item.ReceptorRFC;
            row.Cells["Receptor"].Value = item.Receptor;
            row.Cells["FechaEmision"].Value = item.FechaEmision;
            row.Cells["IdDocumento"].Value = item.IdDocumento;
            row.Cells["Creo"].Value = item.Creo;
            //row.Cells["OrdenProduccion"].Value = item.OrdenProduccion;
            return row;
        }

        public static GridViewRowInfo ConvertTo(GridViewRowInfo row, ValeAlmacenStatusModel item) {
            row.Cells["Status"].Value = item.IdStatusB;
            row.Cells["CveMotivo"].Value = item.CveMotivo;
            row.Cells["Nota"].Value = item.Nota;
            row.Cells["Creo"].Value = item.Creo;
            row.Cells["FecNuevo"].Value = item.FechaNuevo;
            return row;
        }
    }
}
