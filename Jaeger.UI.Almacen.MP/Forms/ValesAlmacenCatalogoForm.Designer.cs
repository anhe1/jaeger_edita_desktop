﻿namespace Jaeger.UI.Almacen.MP.Forms
{
    partial class ValesAlmacenCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValesAlmacenCatalogoForm));
            this.menuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.contextMenuSeleccion = new Telerik.WinControls.UI.RadMenuItem();
            this.contextMenuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.contextMenuEditar = new Telerik.WinControls.UI.RadMenuItem();
            this.contextMenuImprimir = new Telerik.WinControls.UI.RadMenuItem();
            this.TVale = new Jaeger.UI.Common.Forms.GridCommonControl();
            this.Almacen = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Almacen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // menuContextual
            // 
            this.menuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.contextMenuSeleccion,
            this.contextMenuCopiar,
            this.contextMenuEditar,
            this.contextMenuImprimir});
            // 
            // contextMenuSeleccion
            // 
            this.contextMenuSeleccion.Name = "contextMenuSeleccion";
            this.contextMenuSeleccion.Text = "Selección múltiple";
            this.contextMenuSeleccion.UseCompatibleTextRendering = false;
            // 
            // contextMenuCopiar
            // 
            this.contextMenuCopiar.Image = global::Jaeger.UI.Almacen.MP.Properties.Resources.copy_16px;
            this.contextMenuCopiar.Name = "contextMenuCopiar";
            this.contextMenuCopiar.Text = "Copiar";
            this.contextMenuCopiar.UseCompatibleTextRendering = false;
            // 
            // contextMenuEditar
            // 
            this.contextMenuEditar.Image = global::Jaeger.UI.Almacen.MP.Properties.Resources.edit_16px;
            this.contextMenuEditar.Name = "contextMenuEditar";
            this.contextMenuEditar.Text = "Editar";
            this.contextMenuEditar.UseCompatibleTextRendering = false;
            // 
            // contextMenuImprimir
            // 
            this.contextMenuImprimir.Image = global::Jaeger.UI.Almacen.MP.Properties.Resources.print_16px;
            this.contextMenuImprimir.Name = "contextMenuImprimir";
            this.contextMenuImprimir.Text = "Imprimir";
            this.contextMenuImprimir.UseCompatibleTextRendering = false;
            // 
            // TVale
            // 
            this.TVale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TVale.Location = new System.Drawing.Point(0, 0);
            this.TVale.Name = "TVale";
            this.TVale.ShowActualizar = true;
            this.TVale.ShowAutosuma = false;
            this.TVale.ShowCancelar = false;
            this.TVale.ShowCerrar = true;
            this.TVale.ShowEditar = false;
            this.TVale.ShowEjercicio = true;
            this.TVale.ShowExportarExcel = false;
            this.TVale.ShowFiltro = true;
            this.TVale.ShowHerramientas = true;
            this.TVale.ShowImprimir = true;
            this.TVale.ShowItem = true;
            this.TVale.ShowNuevo = true;
            this.TVale.ShowPeriodo = true;
            this.TVale.ShowSeleccionMultiple = true;
            this.TVale.Size = new System.Drawing.Size(1107, 514);
            this.TVale.TabIndex = 1;
            // 
            // Almacen
            // 
            this.Almacen.AutoSizeDropDownToBestFit = true;
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Almacen.NestedRadGridView
            // 
            this.Almacen.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Almacen.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Almacen.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Almacen.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Almacen.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Almacen.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Almacen.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdAlmacen";
            gridViewTextBoxColumn1.HeaderText = "IdAlmacen";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdAlmacen";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.Almacen.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Almacen.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Almacen.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Almacen.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Almacen.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Almacen.EditorControl.Name = "NestedRadGridView";
            this.Almacen.EditorControl.ReadOnly = true;
            this.Almacen.EditorControl.ShowGroupPanel = false;
            this.Almacen.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Almacen.EditorControl.MinimumSize = new System.Drawing.Size(240, 150);
            this.Almacen.EditorControl.TabIndex = 0;
            this.Almacen.Location = new System.Drawing.Point(36, 4);
            this.Almacen.Name = "Almacen";
            this.Almacen.NullText = "Documento";
            this.Almacen.Size = new System.Drawing.Size(151, 20);
            this.Almacen.TabIndex = 384;
            this.Almacen.TabStop = false;
            this.Almacen.ValueMember = "IdDocumento";
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "UrlFileXML");
            this.Iconos.Images.SetKeyName(1, "UrlFilePDF");
            // 
            // ValesAlmacenCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 514);
            this.Controls.Add(this.Almacen);
            this.Controls.Add(this.TVale);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ValesAlmacenCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Almacén MP: Movimientos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ValesAlmacenCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadContextMenu menuContextual;
        public Telerik.WinControls.UI.RadMenuItem contextMenuSeleccion;
        public Telerik.WinControls.UI.RadMenuItem contextMenuCopiar;
        private Telerik.WinControls.UI.RadMenuItem contextMenuEditar;
        private Telerik.WinControls.UI.RadMenuItem contextMenuImprimir;
        protected internal Common.Forms.GridCommonControl TVale;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Almacen;
        private System.Windows.Forms.ImageList Iconos;
    }
}
