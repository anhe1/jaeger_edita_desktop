﻿namespace Jaeger.UI.Almacen.MP.Forms
{
    partial class ValeAlmacenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValeAlmacenForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.Conceptos = new Telerik.WinControls.UI.RadGridView();
            this.TProductos = new Jaeger.UI.Almacen.Forms.ProductoServicioBuscarToolBarControl();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.ComprobanteConceptosPartes = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoParte = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoParte = new Telerik.WinControls.UI.RadGridView();
            this.TPartes = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PageUbicaciones = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridUbicaciones = new Telerik.WinControls.UI.RadGridView();
            this.TUbicacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TVale = new Jaeger.UI.Almacen.MP.Forms.ValeAlmacenControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Conceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Conceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConceptosPartes)).BeginInit();
            this.ComprobanteConceptosPartes.SuspendLayout();
            this.PageConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).BeginInit();
            this.PageUbicaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridUbicaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridUbicaciones.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 132);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1243, 397);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.Conceptos);
            this.splitPanel1.Controls.Add(this.TProductos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1243, 196);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // Conceptos
            // 
            this.Conceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Conceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.Conceptos.MasterTemplate.AllowAddNewRow = false;
            this.Conceptos.MasterTemplate.AllowDeleteRow = false;
            this.Conceptos.MasterTemplate.EnableGrouping = false;
            this.Conceptos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Conceptos.Name = "Conceptos";
            this.Conceptos.Size = new System.Drawing.Size(1243, 166);
            this.Conceptos.TabIndex = 109;
            this.Conceptos.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridConceptos_ContextMenuOpening);
            // 
            // TProductos
            // 
            this.TProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProductos.Location = new System.Drawing.Point(0, 0);
            this.TProductos.Name = "TProductos";
            this.TProductos.Size = new System.Drawing.Size(1243, 30);
            this.TProductos.TabIndex = 110;
            this.TProductos.ButtonBuscar_Click += new System.EventHandler<System.EventArgs>(this.TProducto_Buscar_Click);
            this.TProductos.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.TProducto_Remover_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.ComprobanteConceptosPartes);
            this.splitPanel2.Location = new System.Drawing.Point(0, 200);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1243, 197);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // ComprobanteConceptosPartes
            // 
            this.ComprobanteConceptosPartes.Controls.Add(this.PageConceptoParte);
            this.ComprobanteConceptosPartes.Controls.Add(this.PageUbicaciones);
            this.ComprobanteConceptosPartes.DefaultPage = this.PageConceptoParte;
            this.ComprobanteConceptosPartes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComprobanteConceptosPartes.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteConceptosPartes.Name = "ComprobanteConceptosPartes";
            this.ComprobanteConceptosPartes.SelectedPage = this.PageConceptoParte;
            this.ComprobanteConceptosPartes.Size = new System.Drawing.Size(1243, 197);
            this.ComprobanteConceptosPartes.TabIndex = 169;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConceptosPartes.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConceptosPartes.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobanteConceptosPartes.GetChildAt(0))).ShowItemCloseButton = false;
            // 
            // PageConceptoParte
            // 
            this.PageConceptoParte.Controls.Add(this.GridConceptoParte);
            this.PageConceptoParte.Controls.Add(this.TPartes);
            this.PageConceptoParte.ItemSize = new System.Drawing.SizeF(92F, 24F);
            this.PageConceptoParte.Location = new System.Drawing.Point(10, 33);
            this.PageConceptoParte.Name = "PageConceptoParte";
            this.PageConceptoParte.Size = new System.Drawing.Size(1222, 153);
            this.PageConceptoParte.Text = "Concepto: Parte";
            // 
            // GridConceptoParte
            // 
            this.GridConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoParte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoParte.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Cantidad";
            gridViewTextBoxColumn1.FormatString = "{0:n}";
            gridViewTextBoxColumn1.HeaderText = "Cantidad";
            gridViewTextBoxColumn1.Name = "Cantidad";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "Unidad";
            gridViewTextBoxColumn2.HeaderText = "Unidad";
            gridViewTextBoxColumn2.Name = "Unidad";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn3.HeaderText = "No. Ident.";
            gridViewTextBoxColumn3.Name = "NoIdentificacion";
            gridViewTextBoxColumn3.Width = 95;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn4.Width = 300;
            this.GridConceptoParte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.GridConceptoParte.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridConceptoParte.Name = "GridConceptoParte";
            this.GridConceptoParte.ShowGroupPanel = false;
            this.GridConceptoParte.Size = new System.Drawing.Size(1222, 123);
            this.GridConceptoParte.TabIndex = 0;
            // 
            // TPartes
            // 
            this.TPartes.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPartes.Etiqueta = "";
            this.TPartes.Location = new System.Drawing.Point(0, 0);
            this.TPartes.Name = "TPartes";
            this.TPartes.ReadOnly = false;
            this.TPartes.ShowActualizar = false;
            this.TPartes.ShowAutorizar = false;
            this.TPartes.ShowCerrar = false;
            this.TPartes.ShowEditar = false;
            this.TPartes.ShowExportarExcel = false;
            this.TPartes.ShowFiltro = true;
            this.TPartes.ShowGuardar = false;
            this.TPartes.ShowHerramientas = false;
            this.TPartes.ShowImagen = false;
            this.TPartes.ShowImprimir = false;
            this.TPartes.ShowNuevo = true;
            this.TPartes.ShowRemover = true;
            this.TPartes.Size = new System.Drawing.Size(1222, 30);
            this.TPartes.TabIndex = 1;
            // 
            // PageUbicaciones
            // 
            this.PageUbicaciones.Controls.Add(this.GridUbicaciones);
            this.PageUbicaciones.Controls.Add(this.TUbicacion);
            this.PageUbicaciones.ItemSize = new System.Drawing.SizeF(72F, 24F);
            this.PageUbicaciones.Location = new System.Drawing.Point(10, 37);
            this.PageUbicaciones.Name = "PageUbicaciones";
            this.PageUbicaciones.Size = new System.Drawing.Size(1222, 223);
            this.PageUbicaciones.Text = "Ubicaciones";
            // 
            // GridUbicaciones
            // 
            this.GridUbicaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridUbicaciones.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridUbicaciones.MasterTemplate.AllowAddNewRow = false;
            this.GridUbicaciones.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn5.FieldName = "Id";
            gridViewTextBoxColumn5.HeaderText = "Id";
            gridViewTextBoxColumn5.Name = "Id";
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = 0";
            expressionFormattingObject1.Name = "RegistroActivo";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewComboBoxColumn1.FieldName = "SubId";
            gridViewComboBoxColumn1.HeaderText = "Ubicación";
            gridViewComboBoxColumn1.Name = "Ubicacion";
            gridViewComboBoxColumn1.Width = 240;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "Orden";
            gridViewTextBoxColumn6.HeaderText = "Orden";
            gridViewTextBoxColumn6.Name = "Orden";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Cantidad";
            gridViewTextBoxColumn7.HeaderText = "Cantidad";
            gridViewTextBoxColumn7.Name = "Cantidad";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "Nota";
            gridViewTextBoxColumn8.HeaderText = "Observaciones";
            gridViewTextBoxColumn8.Name = "Nota";
            gridViewTextBoxColumn8.Width = 240;
            gridViewTextBoxColumn9.FieldName = "Creo";
            gridViewTextBoxColumn9.HeaderText = "Creo";
            gridViewTextBoxColumn9.Name = "Creo";
            gridViewTextBoxColumn9.Width = 75;
            this.GridUbicaciones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewCheckBoxColumn1,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.GridUbicaciones.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridUbicaciones.Name = "GridUbicaciones";
            this.GridUbicaciones.ShowGroupPanel = false;
            this.GridUbicaciones.Size = new System.Drawing.Size(1222, 193);
            this.GridUbicaciones.TabIndex = 1;
            // 
            // TUbicacion
            // 
            this.TUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUbicacion.Etiqueta = "";
            this.TUbicacion.Location = new System.Drawing.Point(0, 0);
            this.TUbicacion.Name = "TUbicacion";
            this.TUbicacion.ReadOnly = false;
            this.TUbicacion.ShowActualizar = false;
            this.TUbicacion.ShowAutorizar = false;
            this.TUbicacion.ShowCerrar = false;
            this.TUbicacion.ShowEditar = false;
            this.TUbicacion.ShowExportarExcel = false;
            this.TUbicacion.ShowFiltro = true;
            this.TUbicacion.ShowGuardar = false;
            this.TUbicacion.ShowHerramientas = false;
            this.TUbicacion.ShowImagen = false;
            this.TUbicacion.ShowImprimir = false;
            this.TUbicacion.ShowNuevo = true;
            this.TUbicacion.ShowRemover = true;
            this.TUbicacion.Size = new System.Drawing.Size(1222, 30);
            this.TUbicacion.TabIndex = 2;
            // 
            // TVale
            // 
            this.TVale.AutoSize = true;
            this.TVale.Dock = System.Windows.Forms.DockStyle.Top;
            this.TVale.Location = new System.Drawing.Point(0, 0);
            this.TVale.MaximumSize = new System.Drawing.Size(0, 132);
            this.TVale.MinimumSize = new System.Drawing.Size(976, 132);
            this.TVale.Name = "TVale";
            this.TVale.Size = new System.Drawing.Size(1243, 132);
            this.TVale.TabIndex = 3;
            // 
            // ValeAlmacenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 529);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TVale);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ValeAlmacenForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Vale de Almacén";
            this.Load += new System.EventHandler(this.ValeAlmacenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Conceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Conceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ComprobanteConceptosPartes)).EndInit();
            this.ComprobanteConceptosPartes.ResumeLayout(false);
            this.PageConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).EndInit();
            this.PageUbicaciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridUbicaciones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridUbicaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        internal Telerik.WinControls.UI.RadGridView Conceptos;
        internal Telerik.WinControls.UI.RadPageView ComprobanteConceptosPartes;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoParte;
        internal Telerik.WinControls.UI.RadGridView GridConceptoParte;
        private Telerik.WinControls.UI.RadPageViewPage PageUbicaciones;
        private Telerik.WinControls.UI.RadGridView GridUbicaciones;
        private Common.Forms.ToolBarStandarControl TPartes;
        private Common.Forms.ToolBarStandarControl TUbicacion;
        private Almacen.Forms.ProductoServicioBuscarToolBarControl TProductos;
        public Almacen.MP.Forms.ValeAlmacenControl TVale;
    }
}
