﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Almacen.MP.Services;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.UI.Almacen.MP.Builder;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Almacen.MP.Forms {
    public partial class ValeAlmacenBuscarForm : RadForm {
        protected IValeAlmacenService _Service;
        protected internal List<ValeAlmacenSingleModel> _SourceData;
        protected internal int _IdReceptor;
        protected internal int _IdDepartamento;

        public event EventHandler<ValeAlmacenSingleModel> Selected;
        public void OnSelected(ValeAlmacenSingleModel e) {
            if (this.Selected != null) {
                this.Selected(this, e);
            }
        }

        public ValeAlmacenBuscarForm(IValeAlmacenService service, int idReceptor) {
            InitializeComponent();
            this._Service = service;
            this._IdReceptor = idReceptor;
            this.TBuscar.Descripcion.MaxSize = new Size(75, 20);
            this.TBuscar.Etiqueta = "Folio: ";
            this.TBuscar.Descripcion.TextBoxElement.TextAlign = HorizontalAlignment.Center;
        }

        private void ValeAlmacenBuscarForm_Load(object sender, EventArgs e) {
            using (IAlmacenGridBuilder builder = new AlmacenGridBuilder()) {
                this.GridData.Columns.AddRange(builder.Templetes().Search().Build());
            }

            this.TBuscar.Agregar.Click += this.GridData_DoubleClick;
            this.TBuscar.Buscar.Click += this.TBuscar_Actualizar_Click;
            this.TBuscar.Cerrar.Click += this.TBuscar_Cerrar_Click;
            this.GridData.DoubleClick += this.GridData_DoubleClick;
        }

        #region barra de herramientas
        public virtual void TBuscar_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._SourceData;
        }

        public virtual void TBuscar_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.TBuscar.Filtro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        public virtual void TBuscar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_DoubleClick(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ValeAlmacenSingleModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }
        public virtual void Consultar() {
            var i1 = int.Parse(this.TBuscar.Descripcion.Text);
            var d0 = ValeAlmacenService.Query().ByFolio(i1).Build();
            this._SourceData = this._Service.GetList<ValeAlmacenSingleModel>(d0).ToList();
        }
        #endregion
    }
}
