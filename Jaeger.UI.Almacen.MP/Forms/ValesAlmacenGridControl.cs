﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Almacen.MP.Builder;

namespace Jaeger.UI.Almacen.MP.Forms {
    public class ValesAlmacenGridControl : GridCommonControl {
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal GridViewTemplate Relaciones = new GridViewTemplate() { Caption = "Relaciones" };
        protected internal GridViewTemplate Status = new GridViewTemplate() { Caption = "Cambio de Status" };

        public ValesAlmacenGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IAlmacenGridBuilder view = new AlmacenGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Master().Build());
                this.Conceptos.Columns.AddRange(view.Templetes().Conceptos().Build());
                this.Relaciones.Columns.AddRange(view.Templetes().Relacion().Build());
                this.Status.Columns.AddRange(view.Templetes().CStatus().Build());
            }
        }
    }
}
