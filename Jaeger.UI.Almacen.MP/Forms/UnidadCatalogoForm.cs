﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Almacen.MP.Forms {
    public class UnidadCatalogoForm : Almacen.Forms.UnidadCatalogoForm {

        public UnidadCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Service = new Aplication.Almacen.Services.UnidadService(AlmacenEnum.MP);
        }
    }
}
