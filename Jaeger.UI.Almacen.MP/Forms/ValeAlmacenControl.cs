﻿using System;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Aplication.Almacen.MP.Services;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.MP.Forms {
    public partial class ValeAlmacenControl : UserControl {
        #region declaraciones
        private string _DataError = string.Empty;
        public IValeAlmacenDetailModel Comprobante;
        public IValeAlmacenService Service;
        #endregion

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public ValeAlmacenControl() {
            InitializeComponent();
        }

        private void ValeAlmacenControl_Load(object sender, EventArgs e) {
            if (ConfigService.Synapsis != null) this.TDocumento.Emisor.Text = ConfigService.Synapsis.Empresa.RFC;

            this.TDocumento.Status.DataSource = ValeAlmacenService.GetStatus();
            this.TDocumento.Status.SetReadOnly(true);

            if (this.Service != null) this.Almacen.DataSource = this.Service.GetAlmacenes();
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.ValueMember = "IdAlmacen";

            this.Documento.DataSource = ValeAlmacenService.TipoOperacion();
            this.Documento.DisplayMember = "Descriptor";
            this.Documento.ValueMember = "IdDocumento";
            this.Documento.SelectedIndex = -1;

            this.Departamento.DataSource = ValeAlmacenService.GetDepartamentos();
            this.Departamento.DisplayMember = "Descriptor";
            this.Departamento.ValueMember = "IdDepartamento";
            this.Departamento.SelectedIndex = -1;

            this.TipoDevolucion.DataSource = ValeAlmacenService.GetTipoDevolucion();
            this.TipoDevolucion.DisplayMember = "Descriptor";
            this.TipoDevolucion.ValueMember = "Id";
            this.TipoDevolucion.SelectedIndex = -1;

            this.Documento.SelectedIndexChanged += new EventHandler(this.ConceptoMovimiento_SelectedIndexChanged);
            this.TDocumento.Nuevo.Click += this.TVale_Nuevo_Click;
            this.TDocumento.Actualizar.Click += this.TVale_Actualizar_Click;
            this.TDocumento.Guardar.Click += this.TVale_Guardar_Click;
            this.TDocumento.Imprimir.Click += this.TVale_Imprimir_Click;
            this.TDocumento.FilePDF.Click += this.TVale_FilePDF_Click;
            this.TDocumento.Cerrar.Click += this.TVale_Cerrar_Click;
            this.Receptor.Seleccionado += this.Receptor_Seleccionado;
        }

        private void Receptor_Seleccionado(object sender, Domain.Contribuyentes.Entities.ContribuyenteDetailModel e) {
            if (e != null) {

            }
        }

        private void ConceptoMovimiento_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.Documento.SelectedItem == null) return;
            var currentTipoMovimiento = ((GridViewDataRowInfo)this.Documento.SelectedItem).DataBoundItem as IDocumentoAlmacen;
            if (currentTipoMovimiento != null) {
                this.Comprobante.TipoMovimiento = currentTipoMovimiento.TipoMovimiento;
                this.Comprobante.TipoComprobante = currentTipoMovimiento.TipoComprobante;
                this.TipoDevolucion.Enabled = currentTipoMovimiento.TipoComprobante == Domain.Almacen.MP.ValueObjects.TipoComprobanteEnum.Devolucion;
            }
        }

        #region  barra de herramientas
        public virtual void TVale_Nuevo_Click(object sender, EventArgs eventArgs) {

        }

        public virtual void TVale_Guardar_Click(object sender, EventArgs e) {
            if (this.IsValid() == false) {
                RadMessageBox.Show(this, this._DataError, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            using (var espera = new Waiting1Form(this.Save)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.TVale_Actualizar_Click(sender, e);
        }

        public virtual void TVale_Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Text = Properties.Resources.Message_ComprobanteNew;
                this.Comprobante = ValeAlmacenService.Create().Almacen(this.Service.Almacen).Build();
            } else if (this.Comprobante.IdComprobante > 0) {
                using (var espera = new Waiting1Form(this.Cargar)) {
                    espera.Text = Properties.Resources.Message_ComprobanteGet;
                    espera.ShowDialog(this);
                    this.Text = string.Concat(this.Comprobante.Serie, " ", this.Comprobante.Folio);
                }
            }
            if (this.Comprobante.IsEditable) {
                this.Receptor.Init();
            }
            this.CreateBinding();
            this.OnBindingClompleted(e);
        }

        public virtual void TVale_Imprimir_Click(object sender, EventArgs e) {
            if (this.Comprobante.IdComprobante > 0) {
                var imprimir = new ReporteForm(new ValeAlmacenPrinter(this.Comprobante as ValeAlmacenDetailModel));
                imprimir.Show();
            }
        }

        public virtual void TVale_FilePDF_Click(object sender, EventArgs e) {

        }

        public virtual void TVale_Cerrar_Click(object sender, EventArgs e) {

        }
        #endregion

        public virtual void Cargar() {
            if (this.Comprobante.IdComprobante > 0)
                this.Comprobante = this.Service.GetComprobante(this.Comprobante.IdComprobante);
        }

        private void Save() {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        private void CreateBinding() {
            this.Documento.DataBindings.Clear();
            this.Documento.DataBindings.Add("SelectedValue", this.Comprobante, "IdTipoComprobante", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.Status.DataBindings.Clear();
            this.TDocumento.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.IdDocumento.DataBindings.Clear();
            this.TDocumento.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Almacen.DataBindings.Clear();
            this.Almacen.DataBindings.Add("SelectedValue", this.Comprobante, "IdAlmacen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Folio.DataBindings.Clear();
            this.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdSerie.DataBindings.Clear();
            this.IdSerie.DataBindings.Add("SelectedValue", this.Comprobante, "IdSerie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Nombre.DataBindings.Clear();
            this.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "Receptor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.RFC.DataBindings.Clear();
            this.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDirectorio.DataBindings.Clear();
            this.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Departamento.DataBindings.Clear();
            this.Departamento.DataBindings.Add("SelectedValue", this.Comprobante, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoDevolucion.DataBindings.Clear();
            this.TipoDevolucion.DataBindings.Add("SelectedValue", this.Comprobante, "IdCveDevolucion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contacto.DataBindings.Clear();
            this.Contacto.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nota.DataBindings.Clear();
            this.Nota.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Referencia.DataBindings.Clear();
            this.Referencia.DataBindings.Add("Text", this.Comprobante, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.FilePDF.DataBindings.Clear();
            this.TDocumento.FilePDF.DataBindings.Add("Enabled", this.Comprobante, "IdComprobante", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.Guardar.Enabled = this.Comprobante.IsEditable;
            this.TDocumento.Imprimir.Enabled = !this.Comprobante.IsEditable;
            this.TDocumento.Actualizar.Enabled = !this.Comprobante.IsEditable;
            this.Almacen.Enabled = this.Comprobante.IdComprobante == 0;
            this.IdSerie.Enabled = this.Comprobante.IdComprobante == 0;
            this.Documento.Enabled = this.Comprobante.IdComprobante == 0;
            this.Departamento.Enabled = this.Comprobante.IdComprobante == 0;
            this.Receptor.ReadOnly = !this.Comprobante.IsEditable;
            this.Contacto.ReadOnly = !this.Comprobante.IsEditable;
            this.Referencia.ReadOnly = !this.Comprobante.IsEditable;
            this.Nota.ReadOnly = !this.Comprobante.IsEditable;
            this.Vendedor.Enabled = !this.Comprobante.IsEditable;
        }

        public virtual bool IsValid() {
            this._DataError = string.Empty;
            this.Advertencia.Clear();

            if (this.Comprobante.TipoComprobante == 0) {
                this.Advertencia.SetError(this.Documento, "Selecciona un tipo de comprobante para poder continuar.");
                this._DataError = "Selecciona un tipo de comprobante para poder continuar.";
                return false;
            }

            if (this.Comprobante.IdDepartamento == 0) {
                this.Advertencia.SetError(this.Receptor.Nombre, "Debes seleccionar un departamento.");
                this._DataError = "Debes seleccionar un departamento.";
                return false;
            }

            if (this.Comprobante.IdDirectorio == 0) {
                this.Receptor.Advertencia.SetError(this.Receptor.Nombre, "Selecciona un receptor válido para este comprobante.");
                this._DataError = "Selecciona un receptor válido para este comprobante.";
                return false;
            }

            if (this.Comprobante.Conceptos != null) {
                if (this.Comprobante.Conceptos.Count == 0) {
                    return false;
                } else {
                    var d0 = this.Comprobante.Conceptos.Where(it => it.Cantidad == 0).Count();
                    if (d0 > 0) {
                        this._DataError = "La cantidad de uno o más conceptos no pueden ser cero.";
                        return false;
                    }
                    var d1 = this.Comprobante.Conceptos.Where(it => it.IdUnidad == 0).Count();
                    if (d1 > 0) {
                        this._DataError = "La unidad seleccionada no es válida.";
                        return false;
                    }
                }
            } else {
                return false;
            }

            if (string.IsNullOrEmpty(this.Comprobante.Contacto)) {
                this.Advertencia.SetError(this.Contacto, "Por favor ingresa un nombre o referencia.");
                this._DataError = "Por favor ingresa un nombre o referencia.";
                return false;
            }
            return true;
        }
    }
}
