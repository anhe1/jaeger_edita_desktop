﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Almacen.MP.Services;
using Jaeger.UI.Almacen.MP.Builder;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.MP.Forms {
    public partial class ValeAlmacenStatusForm : RadForm {
        protected internal IValeAlmacenService _Service;
        protected internal ValeAlmacenDetailModel _Current;

        public event EventHandler<IValeAlmacenStatusDetailModel> Selected;
        public void OnSelected(IValeAlmacenStatusDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public ValeAlmacenStatusForm(IValeAlmacenService service, ValeAlmacenDetailModel model) {
            InitializeComponent();
            this._Current = model;
            this._Service = service;
        }

        public virtual void ValeAlmacenPTStatusForm_Load(object sender, EventArgs e) {
            this.FechaEntrega.Value = DateTime.Now;
            this.FechaEntrega.SetEditable(false);
            this.IdDocumento.Text = this._Current.Folio.ToString();
            this.Cliente.Text = this._Current.Receptor;

            this.MotivoCancelacion.DataSource = ValeAlmacenService.GetTipoRelacion();
            this.IdStatus.DataSource = ValeAlmacenService.GetStatus();

            using (IAlmacenGridBuilder view = new AlmacenGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Relacion().Build());
            }
            this.GridData.DataSource = this._Current.Relaciones;
            this.TCancelar.Autorizar.Text = "Autorizar";
            this.TCancelar.Autorizar.Click += this.TCancelar_Autorizar_Click;
            this.TDocumento.Nuevo.Click += Nuevo_Click;
            this.TCancelar.Cerrar.Click += this.TCancelar_Cerrar_Click;
        }

        #region barra de herramientas
        private void Nuevo_Click(object sender, EventArgs e) {
            var buscar = new ValeAlmacenBuscarForm(this._Service, this._Current.IdDirectorio);
            buscar.Selected += Buscar_Selected;
            buscar.ShowDialog(this);
        }

        private void Buscar_Selected(object sender, ValeAlmacenSingleModel e) {
            if (e != null) {
                this._Current.Relaciones.Add(new ValeAlmacenRelacionModel() {
                    IdComprobante = e.IdComprobante,
                    IdDirectorio = e.IdDirectorio,
                    IdDocumento = e.IdDocumento,
                    Folio = e.Folio,
                    Serie = e.Serie,
                    Receptor = e.Receptor, 
                    ReceptorRFC = e.ReceptorRFC,
                    FechaEmision = e.FechaEmision,
                    IdTipoComprobante = e.IdTipoComprobante
                });
            }
        }

        public virtual void TCancelar_Autorizar_Click(object sender, EventArgs e) {
            var status = this.IdStatus.SelectedItem.DataBoundItem as StatusModel;
            var seleccionado = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as DocumentoTipoRelacion;
            if (seleccionado != null) {
                var response = new ValeAlmacenStatusDetailModel {
                    IdComprobante = _Current.IdComprobante,
                    CveMotivo = seleccionado.Descriptor,
                    IdCveMotivo = seleccionado.Id,
                    Creo = ConfigService.Piloto.Clave,
                    FechaNuevo = DateTime.Now,
                    IdStatus = this._Current.IdStatus,
                    IdStatusB = status.Id,
                    IdDirectorio = this._Current.IdDirectorio,
                    Nota = this.Nota.Text
                };
                this.OnSelected(response);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        public virtual void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion
    }
}
