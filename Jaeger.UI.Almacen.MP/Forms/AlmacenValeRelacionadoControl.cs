﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Almacen.MP.Services;

namespace Jaeger.UI.Almacen.MP.Forms {
    public partial class AlmacenValeRelacionadoControl : UserControl {
        protected IValeAlmacenService Service;
        private bool editable;

        public AlmacenValeRelacionadoControl() {
            InitializeComponent();
        }

        public void SetService(IValeAlmacenService service) {
            this.Service = service;
        }

        public bool Editable {
            get {
                return this.editable;
            }
            set {
                this.editable = value;
                //this.SetEditable();
            }
        }

        private void CFDIRelacionadoControl_Load(object sender, EventArgs e) {
            this.ToolBarHostItemIncluir.HostedItem = this.checkBoxIncluir.ButtonElement;
            this.TipoRelacion.DataSource = ValeAlmacenService.GetTipoRelacion();
            this.TipoRelacion.DisplayMember = "Descripcion";
            this.TipoRelacion.ValueMember = "Id";
            this.editable = true;
        }

        private void checkBoxIncluir_CheckStateChanged(object sender, EventArgs e) {
            this.TipoRelacion.Enabled = this.Editable;
            this.ToolBarButtonAgregarCFDI.Enabled = this.Editable;
            this.ToolBarButtonRemoverCFDI.Enabled = this.Editable;
        }
    }
}
