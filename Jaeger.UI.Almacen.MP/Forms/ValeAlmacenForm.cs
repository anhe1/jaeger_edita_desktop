﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.UI.Almacen.Forms;
using Jaeger.UI.Almacen.MP.Builder;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.MP.Forms {
    /// <summary>
    /// formulario para la emision de un vale de almacen de materia prima
    /// </summary>
    public partial class ValeAlmacenForm : RadForm {
        #region declaraciones
        public IUnidadService Unidad;
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };
        public RadContextMenu menuContextual = new RadContextMenu();
        #endregion

        public ValeAlmacenForm() {
            InitializeComponent();
        }

        public ValeAlmacenForm(IValeAlmacenService service) {
            InitializeComponent();
            this.TVale.Service = service;
        }

        private void ValeAlmacenForm_Load(object sender, EventArgs e) {
            using (IAlmacenGridBuilder view = new AlmacenGridBuilder()) {
                this.Conceptos.Columns.AddRange(view.Templetes().Conceptos().Build());
            }
            this.menuContextual.Items.AddRange(this.menuContextAplicar, this.menuContextDuplicar);
            this.TVale.BindingCompleted += TVale_BindingCompleted;
            this.TVale.TDocumento.Cerrar.Click += Cerrar_Click;
            this.TVale.TVale_Actualizar_Click(sender, e);
            this.Conceptos.CellEndEdit += TVale_Conceptos_CellEndEdit;
        }

        private void TVale_Conceptos_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "IdUnidad") {
                var seleccionado = this.Conceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoModel;
                if (seleccionado != null) {
                    GridViewComboBoxColumn comboBoxColumn = e.Column as GridViewComboBoxColumn;
                    var d0 = comboBoxColumn.DataSource as List<UnidadModel>;
                    var d1 = d0.Where(it => it.IdUnidad == (int)e.Value).FirstOrDefault();
                    if (d1 != null) {
                        seleccionado.Unidad = d1.Descripcion;
                        seleccionado.UnidadFactor = d1.Factor;
                    } 
                }
            }
        }

        #region barra de herramientas
        private void TProducto_Buscar_Click(object sender, EventArgs e) {
            var buscar = new ProductoModeloBuscarForm(Domain.Base.ValueObjects.AlmacenEnum.MP, true, false);
            buscar.Seleccionar += TProducto_Buscar_Seleccionar;
            buscar.ShowDialog(this);
        }

        private void TProducto_Buscar_Seleccionar(object sender, ProductoServicioModeloModel e) {
            if (e != null) {
                this.TVale.Comprobante.Conceptos.Add(new ValeAlmacenConceptoDetailModel {
                    Activo = true,
                    Descripcion = e.ProductoDescripcion,
                    //Producto = e.Producto,
                    Especificacion = e.Especificacion,
                    Marca = e.Marca,
                    NoIdentificacion = e.NoIdentificacion,
                    IdProducto = e.IdProducto,
                    IdModelo = e.IdModelo,
                    IdTipoComprobante = e.IdTipo,
                    Unidad = e.Unidad,
                    IdUnidad = e.IdUnidad,
                    Creo = ConfigService.Piloto.Clave,
                    FechaNuevo = DateTime.Now,
                    IdTipoMovimiento = this.TVale.Comprobante.IdTipoComprobante
                });
            }
        }

        public void TProducto_Remover_Click(object sender, EventArgs e) {
            if (this.Conceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.Conceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoDetailModel;
                    if (seleccionado.IdComprobante == 0) {
                        try {
                            this.TVale.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.Conceptos.Rows.Count == 0) {
                                this.TVale.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.Conceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de herramientas partes
        public virtual void TParte_Nuevo_Click(object sender, EventArgs e) {
            if (this.Conceptos.CurrentRow != null) {
                var seleccionado = this.Conceptos.CurrentRow.DataBoundItem as ValeAlmacenConceptoDetailModel;
                if (seleccionado != null) {
                    seleccionado.Partes.Add(new ValeAlmacenConceptoParte { Descripcion = "Descripcion" });
                }
            }
        }

        public virtual void TParte_Remover_Click(object sender, EventArgs e) {

        }
        #endregion

        #region menu contextual
        public virtual void MenuContextAplicar_Click(object sender, EventArgs e) {
            var d = this.Conceptos.CurrentCell.Value;
            foreach (var item in this.Conceptos.Rows) {
                item.Cells[this.Conceptos.CurrentColumn.Index].Value = d;
            }
        }
        #endregion

        #region acciones del grid de productos
        private void GridConceptos_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.Conceptos.CurrentRow.ViewInfo.ViewTemplate == this.Conceptos.MasterTemplate) {
                    this.menuContextAplicar.Enabled = this.Conceptos.CurrentColumn.Name == "OrdenProduccion" |
                        this.Conceptos.CurrentColumn.Name == "OrdenCompra" |
                        this.Conceptos.CurrentColumn.Name == "Entrada" |
                        this.Conceptos.CurrentColumn.Name == "Salida";
                }
                e.ContextMenu = this.menuContextual.DropDown;
            }
        }
        #endregion

        private void TVale_BindingCompleted(object sender, EventArgs e) {
            //this.Conceptos.DataSource = this.TVale.Comprobante.Conceptos;
            this.Conceptos.DataBindings.Clear();
            this.Conceptos.DataBindings.Add("DataSource", this.TVale.Comprobante, "Conceptos", true, DataSourceUpdateMode.OnPropertyChanged);
            this.GridConceptoParte.DataSource = this.TVale.Comprobante.Conceptos;
            this.GridConceptoParte.DataMember = "Partes";
            this.Conceptos.AllowEditRow = this.TVale.Comprobante.IsEditable;

            var comboUnidad = this.Conceptos.Columns["IdUnidad"] as GridViewComboBoxColumn;
            comboUnidad.DisplayMember = "Descripcion";
            comboUnidad.ValueMember = "IdUnidad";
            comboUnidad.DataSource = this.Unidad.GetList<UnidadModel>(UnidadService.Query().ByAlmacen(this.TVale.Service.Almacen).Activo().Build());

            this.TProductos.Enabled = this.TVale.Comprobante.IsEditable;
            this.TPartes.Enabled = this.TVale.Comprobante.IsEditable;
            this.Conceptos.ReadOnly = !this.TVale.Comprobante.IsEditable;
        }
    }
}
