﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Almacen.MP.Forms {
    /// <summary>
    /// Catalogo de Productos y Servicios de Materia Prima
    /// </summary>
    public class ProductoCatalogoForm : Almacen.Forms.ProductoCatalogoForm {
        public ProductoCatalogoForm() : base() { }

        public ProductoCatalogoForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Load += this.ProductoCatalogoForm_Load;
        }

        private void ProductoCatalogoForm_Load(object sender, EventArgs e) {
            this.Action = new UIAction(this.menuElement.Permisos);
            this.TProducto.Nuevo.Enabled = Action.Agregar;
            this.TProducto.Editar.Enabled = Action.Editar;
            this.TProducto.Remover.Enabled = Action.Remover;
            this.GProducto.AllowEditRow = Action.Editar;
            this.GModelos.AllowEditRow = Action.Editar;
            this.TModelos.Nuevo.Enabled = Action.Agregar;
            this.TModelos.Editar.Enabled = Action.Editar;
            this.TModelos.Remover.Enabled = Action.Remover;
            this.TModelos.Autorizar.Enabled = Action.Autorizar;
        }

        #region productos
        public override void TProducto_Nuevo_Click(object sender, EventArgs eventArgs) {
            var producto = new ProductoForm((Aplication.Almacen.MP.Contracts.IMateriaPrimaService)this.Service);
            producto.ShowDialog(this);
        }

        public override void TProducto_Editar_Click(object sender, EventArgs eventArgs) {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioDetailModel;
                if (seleccionado != null) {
                    var producto = new ProductoForm((Aplication.Almacen.MP.Contracts.IMateriaPrimaService)this.Service, seleccionado);
                    producto.ShowDialog(this);
                }
            }
        }
        #endregion

        #region modelos
        public override void TModelo_Nuevo_Click(object sender, EventArgs e) {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioDetailModel;
                if (seleccionado != null) {
                    var nuevo = new ModeloDetailModel { IdAlmacen = seleccionado.IdAlmacen, IdCategoria = seleccionado.IdCategoria, IdProducto = seleccionado.IdProducto };
                    var modelo = new ProductoModeloForm((Aplication.Almacen.MP.Contracts.IMateriaPrimaService)this.Service, nuevo);
                    modelo.ShowDialog(this);
                }
            }
        }

        public override void TModelo_Editar_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloDetailModel;
                if (seleccionado.Autorizado == 0) {
                    var modelo = new ProductoModeloForm((Aplication.Almacen.MP.Contracts.IMateriaPrimaService)this.Service, seleccionado);
                    modelo.ShowDialog(this);
                } else {
                    RadMessageBox.Show(this, "Properties.Resources.Message_Error_ProductoAutorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
        #endregion
    }
}
