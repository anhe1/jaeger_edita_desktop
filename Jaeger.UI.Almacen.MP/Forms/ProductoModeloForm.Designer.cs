﻿namespace Jaeger.UI.Almacen.MP.Forms
{
    partial class ProductoModeloForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoModeloForm));
            this.panelProducto = new Telerik.WinControls.UI.RadPanel();
            this.txbModelo = new Telerik.WinControls.UI.RadTextBox();
            this.lblModelo = new Telerik.WinControls.UI.RadLabel();
            this.cboCategorias = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCategoria = new Telerik.WinControls.UI.RadLabel();
            this.cboTipo = new Telerik.WinControls.UI.RadDropDownList();
            this.lblTipo = new Telerik.WinControls.UI.RadLabel();
            this.txbMarca = new Telerik.WinControls.UI.RadTextBox();
            this.lblMarca = new Telerik.WinControls.UI.RadLabel();
            this.txbExpecificacion = new Telerik.WinControls.UI.RadTextBox();
            this.lblEspecificacion = new Telerik.WinControls.UI.RadLabel();
            this.txbCodigoBarras = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoBarras = new Telerik.WinControls.UI.RadLabel();
            this.txbEtiquetas = new Telerik.WinControls.UI.RadTextBox();
            this.lblEtiquetas = new Telerik.WinControls.UI.RadLabel();
            this.pageViews = new Telerik.WinControls.UI.RadPageView();
            this.pageViewGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.gboxPrecioBase = new Telerik.WinControls.UI.RadGroupBox();
            this.txbPrecioBase = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.lblPrecioBase = new Telerik.WinControls.UI.RadLabel();
            this.gboxImpuestoRetenido = new Telerik.WinControls.UI.RadGroupBox();
            this.txbRetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbRetencionIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbRetencionIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.chkRentencionISR = new Telerik.WinControls.UI.RadCheckBox();
            this.chkRetnecionIVA = new Telerik.WinControls.UI.RadCheckBox();
            this.lblIEPS2 = new Telerik.WinControls.UI.RadLabel();
            this.cboRetencionIEPS = new Telerik.WinControls.UI.RadDropDownList();
            this.gboxIpuestoTraslado = new Telerik.WinControls.UI.RadGroupBox();
            this.cboTrasladoIVA = new Telerik.WinControls.UI.RadDropDownList();
            this.txbTrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbTrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblIEPS1 = new Telerik.WinControls.UI.RadLabel();
            this.cboTrasladoIEPS = new Telerik.WinControls.UI.RadDropDownList();
            this.lblIVA = new Telerik.WinControls.UI.RadLabel();
            this.gboxOtrasEspecificaciones = new Telerik.WinControls.UI.RadGroupBox();
            this.lblUnidad1 = new Telerik.WinControls.UI.RadLabel();
            this.txbLargo = new Telerik.WinControls.UI.RadTextBox();
            this.lblLargo = new Telerik.WinControls.UI.RadLabel();
            this.txbAncho = new Telerik.WinControls.UI.RadTextBox();
            this.cboUnidadZ = new Telerik.WinControls.UI.RadDropDownList();
            this.lblAncho = new Telerik.WinControls.UI.RadLabel();
            this.cboUnidadXY = new Telerik.WinControls.UI.RadDropDownList();
            this.txbAlto = new Telerik.WinControls.UI.RadTextBox();
            this.lblCalibre = new Telerik.WinControls.UI.RadLabel();
            this.gboxAlmacen = new Telerik.WinControls.UI.RadGroupBox();
            this.lblExistencia = new Telerik.WinControls.UI.RadLabel();
            this.lblStockMinimo = new Telerik.WinControls.UI.RadLabel();
            this.TxbStockMinimo = new Telerik.WinControls.UI.RadTextBox();
            this.TxbStockMaximo = new Telerik.WinControls.UI.RadTextBox();
            this.TxbExistencia = new Telerik.WinControls.UI.RadTextBox();
            this.lblStockMaximo = new Telerik.WinControls.UI.RadLabel();
            this.TxbStockReorden = new Telerik.WinControls.UI.RadTextBox();
            this.lblUnidad2 = new Telerik.WinControls.UI.RadLabel();
            this.lblReOrden = new Telerik.WinControls.UI.RadLabel();
            this.CboUnidadAlmacen = new Telerik.WinControls.UI.RadDropDownList();
            this.pageViewProveedor = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridProveedor = new Telerik.WinControls.UI.RadGridView();
            this.toolBarStandarControl1 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TProducto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).BeginInit();
            this.panelProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbMarca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMarca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbExpecificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEspecificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCodigoBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbEtiquetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtiquetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageViews)).BeginInit();
            this.pageViews.SuspendLayout();
            this.pageViewGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gboxPrecioBase)).BeginInit();
            this.gboxPrecioBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbPrecioBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrecioBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxImpuestoRetenido)).BeginInit();
            this.gboxImpuestoRetenido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRentencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetnecionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRetencionIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxIpuestoTraslado)).BeginInit();
            this.gboxIpuestoTraslado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxOtrasEspecificaciones)).BeginInit();
            this.gboxOtrasEspecificaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbLargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAncho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAncho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAlto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCalibre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxAlmacen)).BeginInit();
            this.gboxAlmacen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblExistencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMaximo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbExistencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMaximo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockReorden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUnidadAlmacen)).BeginInit();
            this.pageViewProveedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panelProducto
            // 
            this.panelProducto.Controls.Add(this.txbModelo);
            this.panelProducto.Controls.Add(this.lblModelo);
            this.panelProducto.Controls.Add(this.cboCategorias);
            this.panelProducto.Controls.Add(this.lblCategoria);
            this.panelProducto.Controls.Add(this.cboTipo);
            this.panelProducto.Controls.Add(this.lblTipo);
            this.panelProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelProducto.Location = new System.Drawing.Point(0, 30);
            this.panelProducto.Name = "panelProducto";
            this.panelProducto.Size = new System.Drawing.Size(671, 64);
            this.panelProducto.TabIndex = 1;
            // 
            // txbModelo
            // 
            this.txbModelo.Location = new System.Drawing.Point(75, 32);
            this.txbModelo.Name = "txbModelo";
            this.txbModelo.NullText = "Modelo";
            this.txbModelo.Size = new System.Drawing.Size(382, 20);
            this.txbModelo.TabIndex = 7;
            // 
            // lblModelo
            // 
            this.lblModelo.Location = new System.Drawing.Point(12, 33);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(47, 18);
            this.lblModelo.TabIndex = 6;
            this.lblModelo.Text = "Modelo:";
            // 
            // cboCategorias
            // 
            this.cboCategorias.AutoSizeDropDownToBestFit = true;
            // 
            // cboCategorias.NestedRadGridView
            // 
            this.cboCategorias.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboCategorias.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategorias.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboCategorias.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboCategorias.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Codigo";
            gridViewTextBoxColumn1.HeaderText = "Codigo";
            gridViewTextBoxColumn1.Name = "Codigo";
            gridViewTextBoxColumn2.FieldName = "IdProducto";
            gridViewTextBoxColumn2.HeaderText = "IdProducto";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "IdProducto";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "IdCategoria";
            gridViewTextBoxColumn3.HeaderText = "Id";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Id";
            gridViewTextBoxColumn4.DataType = typeof(int);
            gridViewTextBoxColumn4.FieldName = "SubId";
            gridViewTextBoxColumn4.HeaderText = "SubId";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "SubId";
            gridViewTextBoxColumn5.FieldName = "Tipo";
            gridViewTextBoxColumn5.HeaderText = "Tipo";
            gridViewTextBoxColumn5.Name = "Tipo";
            gridViewTextBoxColumn6.FieldName = "Clase";
            gridViewTextBoxColumn6.HeaderText = "Clase";
            gridViewTextBoxColumn6.Name = "Clase";
            gridViewTextBoxColumn7.FieldName = "Descripcion";
            gridViewTextBoxColumn7.HeaderText = "Descripción";
            gridViewTextBoxColumn7.Name = "Descripcion";
            gridViewTextBoxColumn8.FieldName = "Descriptor";
            gridViewTextBoxColumn8.HeaderText = "Descripcion2";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Descriptor";
            this.cboCategorias.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.cboCategorias.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboCategorias.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cboCategorias.EditorControl.Name = "NestedRadGridView";
            this.cboCategorias.EditorControl.ReadOnly = true;
            this.cboCategorias.EditorControl.ShowGroupPanel = false;
            this.cboCategorias.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboCategorias.EditorControl.TabIndex = 0;
            this.cboCategorias.Location = new System.Drawing.Point(131, 6);
            this.cboCategorias.Name = "cboCategorias";
            this.cboCategorias.NullText = "Categoría";
            this.cboCategorias.Size = new System.Drawing.Size(519, 20);
            this.cboCategorias.TabIndex = 3;
            this.cboCategorias.TabStop = false;
            // 
            // lblCategoria
            // 
            this.lblCategoria.Location = new System.Drawing.Point(12, 6);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(113, 18);
            this.lblCategoria.TabIndex = 2;
            this.lblCategoria.Text = "Categoría / Producto:";
            // 
            // cboTipo
            // 
            this.cboTipo.DisplayMember = "Descripcion";
            this.cboTipo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cboTipo.Location = new System.Drawing.Point(534, 32);
            this.cboTipo.Name = "cboTipo";
            this.cboTipo.Size = new System.Drawing.Size(116, 20);
            this.cboTipo.TabIndex = 1;
            this.cboTipo.ValueMember = "Id";
            // 
            // lblTipo
            // 
            this.lblTipo.Location = new System.Drawing.Point(497, 33);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(31, 18);
            this.lblTipo.TabIndex = 0;
            this.lblTipo.Text = "Tipo:";
            // 
            // txbMarca
            // 
            this.txbMarca.Location = new System.Drawing.Point(47, 8);
            this.txbMarca.Name = "txbMarca";
            this.txbMarca.NullText = "Marca";
            this.txbMarca.Size = new System.Drawing.Size(593, 20);
            this.txbMarca.TabIndex = 9;
            // 
            // lblMarca
            // 
            this.lblMarca.Location = new System.Drawing.Point(2, 9);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(39, 18);
            this.lblMarca.TabIndex = 8;
            this.lblMarca.Text = "Marca:";
            // 
            // txbExpecificacion
            // 
            this.txbExpecificacion.Location = new System.Drawing.Point(89, 34);
            this.txbExpecificacion.Name = "txbExpecificacion";
            this.txbExpecificacion.NullText = "Especificación";
            this.txbExpecificacion.Size = new System.Drawing.Size(551, 20);
            this.txbExpecificacion.TabIndex = 11;
            // 
            // lblEspecificacion
            // 
            this.lblEspecificacion.Location = new System.Drawing.Point(2, 35);
            this.lblEspecificacion.Name = "lblEspecificacion";
            this.lblEspecificacion.Size = new System.Drawing.Size(78, 18);
            this.lblEspecificacion.TabIndex = 10;
            this.lblEspecificacion.Text = "Especificación:";
            // 
            // txbCodigoBarras
            // 
            this.txbCodigoBarras.Location = new System.Drawing.Point(89, 60);
            this.txbCodigoBarras.Name = "txbCodigoBarras";
            this.txbCodigoBarras.NullText = "Cod. de Barras";
            this.txbCodigoBarras.Size = new System.Drawing.Size(186, 20);
            this.txbCodigoBarras.TabIndex = 13;
            // 
            // lblCodigoBarras
            // 
            this.lblCodigoBarras.Location = new System.Drawing.Point(2, 61);
            this.lblCodigoBarras.Name = "lblCodigoBarras";
            this.lblCodigoBarras.Size = new System.Drawing.Size(81, 18);
            this.lblCodigoBarras.TabIndex = 12;
            this.lblCodigoBarras.Text = "Cod. de Barras:";
            // 
            // txbEtiquetas
            // 
            this.txbEtiquetas.Location = new System.Drawing.Point(344, 60);
            this.txbEtiquetas.Name = "txbEtiquetas";
            this.txbEtiquetas.NullText = "Etiqueta de Busqueda";
            this.txbEtiquetas.Size = new System.Drawing.Size(296, 20);
            this.txbEtiquetas.TabIndex = 15;
            // 
            // lblEtiquetas
            // 
            this.lblEtiquetas.Location = new System.Drawing.Point(281, 61);
            this.lblEtiquetas.Name = "lblEtiquetas";
            this.lblEtiquetas.Size = new System.Drawing.Size(54, 18);
            this.lblEtiquetas.TabIndex = 14;
            this.lblEtiquetas.Text = "Etiquetas:";
            // 
            // pageViews
            // 
            this.pageViews.Controls.Add(this.pageViewGeneral);
            this.pageViews.Controls.Add(this.pageViewProveedor);
            this.pageViews.DefaultPage = this.pageViewGeneral;
            this.pageViews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageViews.Location = new System.Drawing.Point(0, 94);
            this.pageViews.Name = "pageViews";
            this.pageViews.SelectedPage = this.pageViewGeneral;
            this.pageViews.Size = new System.Drawing.Size(671, 388);
            this.pageViews.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageViews.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageViews.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Bottom;
            // 
            // pageViewGeneral
            // 
            this.pageViewGeneral.Controls.Add(this.gboxPrecioBase);
            this.pageViewGeneral.Controls.Add(this.gboxImpuestoRetenido);
            this.pageViewGeneral.Controls.Add(this.gboxIpuestoTraslado);
            this.pageViewGeneral.Controls.Add(this.gboxOtrasEspecificaciones);
            this.pageViewGeneral.Controls.Add(this.gboxAlmacen);
            this.pageViewGeneral.Controls.Add(this.txbEtiquetas);
            this.pageViewGeneral.Controls.Add(this.lblMarca);
            this.pageViewGeneral.Controls.Add(this.lblEtiquetas);
            this.pageViewGeneral.Controls.Add(this.txbMarca);
            this.pageViewGeneral.Controls.Add(this.txbCodigoBarras);
            this.pageViewGeneral.Controls.Add(this.lblEspecificacion);
            this.pageViewGeneral.Controls.Add(this.lblCodigoBarras);
            this.pageViewGeneral.Controls.Add(this.txbExpecificacion);
            this.pageViewGeneral.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.pageViewGeneral.Location = new System.Drawing.Point(10, 10);
            this.pageViewGeneral.Name = "pageViewGeneral";
            this.pageViewGeneral.Size = new System.Drawing.Size(650, 340);
            this.pageViewGeneral.Text = "General";
            // 
            // gboxPrecioBase
            // 
            this.gboxPrecioBase.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxPrecioBase.Controls.Add(this.txbPrecioBase);
            this.gboxPrecioBase.Controls.Add(this.lblPrecioBase);
            this.gboxPrecioBase.HeaderText = "Precio Base";
            this.gboxPrecioBase.Location = new System.Drawing.Point(453, 92);
            this.gboxPrecioBase.Name = "gboxPrecioBase";
            this.gboxPrecioBase.Size = new System.Drawing.Size(187, 49);
            this.gboxPrecioBase.TabIndex = 40;
            this.gboxPrecioBase.Text = "Precio Base";
            // 
            // txbPrecioBase
            // 
            this.txbPrecioBase.Location = new System.Drawing.Point(109, 19);
            this.txbPrecioBase.Name = "txbPrecioBase";
            this.txbPrecioBase.Size = new System.Drawing.Size(68, 20);
            this.txbPrecioBase.TabIndex = 10;
            this.txbPrecioBase.TabStop = false;
            this.txbPrecioBase.Value = "0";
            // 
            // lblPrecioBase
            // 
            this.lblPrecioBase.Location = new System.Drawing.Point(10, 22);
            this.lblPrecioBase.Name = "lblPrecioBase";
            this.lblPrecioBase.Size = new System.Drawing.Size(66, 18);
            this.lblPrecioBase.TabIndex = 9;
            this.lblPrecioBase.Text = "Precio Base:";
            // 
            // gboxImpuestoRetenido
            // 
            this.gboxImpuestoRetenido.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxImpuestoRetenido.Controls.Add(this.txbRetencionISR);
            this.gboxImpuestoRetenido.Controls.Add(this.txbRetencionIVA);
            this.gboxImpuestoRetenido.Controls.Add(this.txbRetencionIEPS);
            this.gboxImpuestoRetenido.Controls.Add(this.chkRentencionISR);
            this.gboxImpuestoRetenido.Controls.Add(this.chkRetnecionIVA);
            this.gboxImpuestoRetenido.Controls.Add(this.lblIEPS2);
            this.gboxImpuestoRetenido.Controls.Add(this.cboRetencionIEPS);
            this.gboxImpuestoRetenido.HeaderText = "Impuestos Retenidos";
            this.gboxImpuestoRetenido.Location = new System.Drawing.Point(453, 231);
            this.gboxImpuestoRetenido.Name = "gboxImpuestoRetenido";
            this.gboxImpuestoRetenido.Size = new System.Drawing.Size(187, 102);
            this.gboxImpuestoRetenido.TabIndex = 39;
            this.gboxImpuestoRetenido.Text = "Impuestos Retenidos";
            // 
            // txbRetencionISR
            // 
            this.txbRetencionISR.Location = new System.Drawing.Point(48, 47);
            this.txbRetencionISR.Mask = "n6";
            this.txbRetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionISR.Name = "txbRetencionISR";
            this.txbRetencionISR.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionISR.TabIndex = 47;
            this.txbRetencionISR.TabStop = false;
            this.txbRetencionISR.Text = "0.000000";
            this.txbRetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbRetencionIVA
            // 
            this.txbRetencionIVA.Location = new System.Drawing.Point(48, 21);
            this.txbRetencionIVA.Mask = "n6";
            this.txbRetencionIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionIVA.Name = "txbRetencionIVA";
            this.txbRetencionIVA.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionIVA.TabIndex = 46;
            this.txbRetencionIVA.TabStop = false;
            this.txbRetencionIVA.Text = "0.000000";
            this.txbRetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbRetencionIEPS
            // 
            this.txbRetencionIEPS.Location = new System.Drawing.Point(122, 70);
            this.txbRetencionIEPS.Mask = "n6";
            this.txbRetencionIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionIEPS.Name = "txbRetencionIEPS";
            this.txbRetencionIEPS.Size = new System.Drawing.Size(55, 20);
            this.txbRetencionIEPS.TabIndex = 43;
            this.txbRetencionIEPS.TabStop = false;
            this.txbRetencionIEPS.Text = "0.000000";
            this.txbRetencionIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkRentencionISR
            // 
            this.chkRentencionISR.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRentencionISR.Location = new System.Drawing.Point(7, 46);
            this.chkRentencionISR.Name = "chkRentencionISR";
            this.chkRentencionISR.Size = new System.Drawing.Size(36, 18);
            this.chkRentencionISR.TabIndex = 42;
            this.chkRentencionISR.Text = "ISR";
            // 
            // chkRetnecionIVA
            // 
            this.chkRetnecionIVA.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRetnecionIVA.Location = new System.Drawing.Point(7, 21);
            this.chkRetnecionIVA.Name = "chkRetnecionIVA";
            this.chkRetnecionIVA.Size = new System.Drawing.Size(38, 18);
            this.chkRetnecionIVA.TabIndex = 41;
            this.chkRetnecionIVA.Text = "IVA";
            // 
            // lblIEPS2
            // 
            this.lblIEPS2.Location = new System.Drawing.Point(7, 72);
            this.lblIEPS2.Name = "lblIEPS2";
            this.lblIEPS2.Size = new System.Drawing.Size(27, 18);
            this.lblIEPS2.TabIndex = 40;
            this.lblIEPS2.Text = "IEPS";
            // 
            // cboRetencionIEPS
            // 
            radListDataItem1.Text = "N/A";
            radListDataItem2.Text = "Tasa";
            radListDataItem3.Text = "Cuota";
            this.cboRetencionIEPS.Items.Add(radListDataItem1);
            this.cboRetencionIEPS.Items.Add(radListDataItem2);
            this.cboRetencionIEPS.Items.Add(radListDataItem3);
            this.cboRetencionIEPS.Location = new System.Drawing.Point(48, 71);
            this.cboRetencionIEPS.Name = "cboRetencionIEPS";
            this.cboRetencionIEPS.Size = new System.Drawing.Size(68, 20);
            this.cboRetencionIEPS.TabIndex = 39;
            // 
            // gboxIpuestoTraslado
            // 
            this.gboxIpuestoTraslado.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxIpuestoTraslado.Controls.Add(this.cboTrasladoIVA);
            this.gboxIpuestoTraslado.Controls.Add(this.txbTrasladoIEPS);
            this.gboxIpuestoTraslado.Controls.Add(this.txbTrasladoIVA);
            this.gboxIpuestoTraslado.Controls.Add(this.lblIEPS1);
            this.gboxIpuestoTraslado.Controls.Add(this.cboTrasladoIEPS);
            this.gboxIpuestoTraslado.Controls.Add(this.lblIVA);
            this.gboxIpuestoTraslado.HeaderText = "Impuestos Traslado";
            this.gboxIpuestoTraslado.Location = new System.Drawing.Point(453, 147);
            this.gboxIpuestoTraslado.Name = "gboxIpuestoTraslado";
            this.gboxIpuestoTraslado.Size = new System.Drawing.Size(187, 77);
            this.gboxIpuestoTraslado.TabIndex = 38;
            this.gboxIpuestoTraslado.Text = "Impuestos Traslado";
            // 
            // cboTrasladoIVA
            // 
            radListDataItem4.Text = "N/A";
            radListDataItem5.Text = "Tasa";
            radListDataItem6.Text = "Exento";
            this.cboTrasladoIVA.Items.Add(radListDataItem4);
            this.cboTrasladoIVA.Items.Add(radListDataItem5);
            this.cboTrasladoIVA.Items.Add(radListDataItem6);
            this.cboTrasladoIVA.Location = new System.Drawing.Point(37, 21);
            this.cboTrasladoIVA.Name = "cboTrasladoIVA";
            this.cboTrasladoIVA.Size = new System.Drawing.Size(66, 20);
            this.cboTrasladoIVA.TabIndex = 37;
            // 
            // txbTrasladoIEPS
            // 
            this.txbTrasladoIEPS.Location = new System.Drawing.Point(109, 47);
            this.txbTrasladoIEPS.Mask = "n6";
            this.txbTrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbTrasladoIEPS.Name = "txbTrasladoIEPS";
            this.txbTrasladoIEPS.Size = new System.Drawing.Size(68, 20);
            this.txbTrasladoIEPS.TabIndex = 45;
            this.txbTrasladoIEPS.TabStop = false;
            this.txbTrasladoIEPS.Text = "0.000000";
            this.txbTrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbTrasladoIVA
            // 
            this.txbTrasladoIVA.Location = new System.Drawing.Point(109, 20);
            this.txbTrasladoIVA.Mask = "n6";
            this.txbTrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbTrasladoIVA.Name = "txbTrasladoIVA";
            this.txbTrasladoIVA.Size = new System.Drawing.Size(68, 20);
            this.txbTrasladoIVA.TabIndex = 44;
            this.txbTrasladoIVA.TabStop = false;
            this.txbTrasladoIVA.Text = "0.000000";
            this.txbTrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblIEPS1
            // 
            this.lblIEPS1.Location = new System.Drawing.Point(4, 47);
            this.lblIEPS1.Name = "lblIEPS1";
            this.lblIEPS1.Size = new System.Drawing.Size(27, 18);
            this.lblIEPS1.TabIndex = 35;
            this.lblIEPS1.Text = "IEPS";
            // 
            // cboTrasladoIEPS
            // 
            radListDataItem7.Text = "N/A";
            radListDataItem8.Text = "Tasa";
            radListDataItem9.Text = "Cuota";
            this.cboTrasladoIEPS.Items.Add(radListDataItem7);
            this.cboTrasladoIEPS.Items.Add(radListDataItem8);
            this.cboTrasladoIEPS.Items.Add(radListDataItem9);
            this.cboTrasladoIEPS.Location = new System.Drawing.Point(37, 47);
            this.cboTrasladoIEPS.Name = "cboTrasladoIEPS";
            this.cboTrasladoIEPS.Size = new System.Drawing.Size(66, 20);
            this.cboTrasladoIEPS.TabIndex = 34;
            // 
            // lblIVA
            // 
            this.lblIVA.Location = new System.Drawing.Point(6, 22);
            this.lblIVA.Name = "lblIVA";
            this.lblIVA.Size = new System.Drawing.Size(24, 18);
            this.lblIVA.TabIndex = 33;
            this.lblIVA.Text = "IVA";
            // 
            // gboxOtrasEspecificaciones
            // 
            this.gboxOtrasEspecificaciones.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblUnidad1);
            this.gboxOtrasEspecificaciones.Controls.Add(this.txbLargo);
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblLargo);
            this.gboxOtrasEspecificaciones.Controls.Add(this.txbAncho);
            this.gboxOtrasEspecificaciones.Controls.Add(this.cboUnidadZ);
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblAncho);
            this.gboxOtrasEspecificaciones.Controls.Add(this.cboUnidadXY);
            this.gboxOtrasEspecificaciones.Controls.Add(this.txbAlto);
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblCalibre);
            this.gboxOtrasEspecificaciones.HeaderText = "Otras especificaciones";
            this.gboxOtrasEspecificaciones.Location = new System.Drawing.Point(3, 92);
            this.gboxOtrasEspecificaciones.Name = "gboxOtrasEspecificaciones";
            this.gboxOtrasEspecificaciones.Size = new System.Drawing.Size(444, 79);
            this.gboxOtrasEspecificaciones.TabIndex = 17;
            this.gboxOtrasEspecificaciones.Text = "Otras especificaciones";
            // 
            // lblUnidad1
            // 
            this.lblUnidad1.Location = new System.Drawing.Point(153, 22);
            this.lblUnidad1.Name = "lblUnidad1";
            this.lblUnidad1.Size = new System.Drawing.Size(42, 18);
            this.lblUnidad1.TabIndex = 4;
            this.lblUnidad1.Text = "Unidad";
            // 
            // txbLargo
            // 
            this.txbLargo.Location = new System.Drawing.Point(11, 45);
            this.txbLargo.Name = "txbLargo";
            this.txbLargo.NullText = "Largo";
            this.txbLargo.Size = new System.Drawing.Size(65, 20);
            this.txbLargo.TabIndex = 1;
            this.txbLargo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLargo
            // 
            this.lblLargo.Location = new System.Drawing.Point(11, 21);
            this.lblLargo.Name = "lblLargo";
            this.lblLargo.Size = new System.Drawing.Size(37, 18);
            this.lblLargo.TabIndex = 0;
            this.lblLargo.Text = "Largo:";
            // 
            // txbAncho
            // 
            this.txbAncho.Location = new System.Drawing.Point(82, 45);
            this.txbAncho.Name = "txbAncho";
            this.txbAncho.NullText = "Ancho";
            this.txbAncho.Size = new System.Drawing.Size(65, 20);
            this.txbAncho.TabIndex = 3;
            this.txbAncho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboUnidadZ
            // 
            this.cboUnidadZ.DisplayMember = "Descripcion";
            this.cboUnidadZ.Location = new System.Drawing.Point(327, 45);
            this.cboUnidadZ.Name = "cboUnidadZ";
            this.cboUnidadZ.NullText = "Selecciona";
            this.cboUnidadZ.Size = new System.Drawing.Size(107, 20);
            this.cboUnidadZ.TabIndex = 8;
            this.cboUnidadZ.ValueMember = "Id";
            // 
            // lblAncho
            // 
            this.lblAncho.Location = new System.Drawing.Point(82, 22);
            this.lblAncho.Name = "lblAncho";
            this.lblAncho.Size = new System.Drawing.Size(41, 18);
            this.lblAncho.TabIndex = 2;
            this.lblAncho.Text = "Ancho:";
            // 
            // cboUnidadXY
            // 
            this.cboUnidadXY.DisplayMember = "Descripcion";
            this.cboUnidadXY.Location = new System.Drawing.Point(153, 45);
            this.cboUnidadXY.Name = "cboUnidadXY";
            this.cboUnidadXY.NullText = "Selecciona";
            this.cboUnidadXY.Size = new System.Drawing.Size(107, 20);
            this.cboUnidadXY.TabIndex = 5;
            this.cboUnidadXY.ValueMember = "Id";
            // 
            // txbAlto
            // 
            this.txbAlto.Location = new System.Drawing.Point(266, 45);
            this.txbAlto.Name = "txbAlto";
            this.txbAlto.NullText = "Calibre";
            this.txbAlto.Size = new System.Drawing.Size(55, 20);
            this.txbAlto.TabIndex = 7;
            this.txbAlto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCalibre
            // 
            this.lblCalibre.Location = new System.Drawing.Point(266, 22);
            this.lblCalibre.Name = "lblCalibre";
            this.lblCalibre.Size = new System.Drawing.Size(75, 18);
            this.lblCalibre.TabIndex = 6;
            this.lblCalibre.Text = "Alto / Calibre:";
            // 
            // gboxAlmacen
            // 
            this.gboxAlmacen.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxAlmacen.Controls.Add(this.lblExistencia);
            this.gboxAlmacen.Controls.Add(this.lblStockMinimo);
            this.gboxAlmacen.Controls.Add(this.TxbStockMinimo);
            this.gboxAlmacen.Controls.Add(this.TxbStockMaximo);
            this.gboxAlmacen.Controls.Add(this.TxbExistencia);
            this.gboxAlmacen.Controls.Add(this.lblStockMaximo);
            this.gboxAlmacen.Controls.Add(this.TxbStockReorden);
            this.gboxAlmacen.Controls.Add(this.lblUnidad2);
            this.gboxAlmacen.Controls.Add(this.lblReOrden);
            this.gboxAlmacen.Controls.Add(this.CboUnidadAlmacen);
            this.gboxAlmacen.HeaderText = "Almacén";
            this.gboxAlmacen.Location = new System.Drawing.Point(3, 177);
            this.gboxAlmacen.Name = "gboxAlmacen";
            this.gboxAlmacen.Size = new System.Drawing.Size(444, 156);
            this.gboxAlmacen.TabIndex = 18;
            this.gboxAlmacen.Text = "Almacén";
            // 
            // lblExistencia
            // 
            this.lblExistencia.Location = new System.Drawing.Point(266, 21);
            this.lblExistencia.Name = "lblExistencia";
            this.lblExistencia.Size = new System.Drawing.Size(57, 18);
            this.lblExistencia.TabIndex = 6;
            this.lblExistencia.Text = "Existencia:";
            // 
            // lblStockMinimo
            // 
            this.lblStockMinimo.Location = new System.Drawing.Point(11, 21);
            this.lblStockMinimo.Name = "lblStockMinimo";
            this.lblStockMinimo.Size = new System.Drawing.Size(78, 18);
            this.lblStockMinimo.TabIndex = 0;
            this.lblStockMinimo.Text = "Stock Mínimo:";
            // 
            // TxbStockMinimo
            // 
            this.TxbStockMinimo.Location = new System.Drawing.Point(11, 45);
            this.TxbStockMinimo.Name = "TxbStockMinimo";
            this.TxbStockMinimo.NullText = "0.00";
            this.TxbStockMinimo.Size = new System.Drawing.Size(78, 20);
            this.TxbStockMinimo.TabIndex = 1;
            this.TxbStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbStockMaximo
            // 
            this.TxbStockMaximo.Location = new System.Drawing.Point(98, 45);
            this.TxbStockMaximo.Name = "TxbStockMaximo";
            this.TxbStockMaximo.NullText = "0.00";
            this.TxbStockMaximo.Size = new System.Drawing.Size(78, 20);
            this.TxbStockMaximo.TabIndex = 3;
            this.TxbStockMaximo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbExistencia
            // 
            this.TxbExistencia.Location = new System.Drawing.Point(266, 45);
            this.TxbExistencia.Name = "TxbExistencia";
            this.TxbExistencia.NullText = "0.00";
            this.TxbExistencia.Size = new System.Drawing.Size(78, 20);
            this.TxbExistencia.TabIndex = 7;
            this.TxbExistencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblStockMaximo
            // 
            this.lblStockMaximo.Location = new System.Drawing.Point(98, 21);
            this.lblStockMaximo.Name = "lblStockMaximo";
            this.lblStockMaximo.Size = new System.Drawing.Size(79, 18);
            this.lblStockMaximo.TabIndex = 2;
            this.lblStockMaximo.Text = "Stock Máximo:";
            // 
            // TxbStockReorden
            // 
            this.TxbStockReorden.Location = new System.Drawing.Point(182, 45);
            this.TxbStockReorden.Name = "TxbStockReorden";
            this.TxbStockReorden.NullText = "0.00";
            this.TxbStockReorden.Size = new System.Drawing.Size(78, 20);
            this.TxbStockReorden.TabIndex = 5;
            this.TxbStockReorden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblUnidad2
            // 
            this.lblUnidad2.Location = new System.Drawing.Point(350, 21);
            this.lblUnidad2.Name = "lblUnidad2";
            this.lblUnidad2.Size = new System.Drawing.Size(45, 18);
            this.lblUnidad2.TabIndex = 8;
            this.lblUnidad2.Text = "Unidad:";
            // 
            // lblReOrden
            // 
            this.lblReOrden.Location = new System.Drawing.Point(182, 21);
            this.lblReOrden.Name = "lblReOrden";
            this.lblReOrden.Size = new System.Drawing.Size(51, 18);
            this.lblReOrden.TabIndex = 4;
            this.lblReOrden.Text = "Reorden:";
            // 
            // CboUnidadAlmacen
            // 
            this.CboUnidadAlmacen.DisplayMember = "Descripcion";
            this.CboUnidadAlmacen.Location = new System.Drawing.Point(350, 45);
            this.CboUnidadAlmacen.Name = "CboUnidadAlmacen";
            this.CboUnidadAlmacen.NullText = "Selecciona";
            this.CboUnidadAlmacen.Size = new System.Drawing.Size(80, 20);
            this.CboUnidadAlmacen.TabIndex = 9;
            this.CboUnidadAlmacen.ValueMember = "Id";
            // 
            // pageViewProveedor
            // 
            this.pageViewProveedor.Controls.Add(this.gridProveedor);
            this.pageViewProveedor.Controls.Add(this.toolBarStandarControl1);
            this.pageViewProveedor.ItemSize = new System.Drawing.SizeF(68F, 28F);
            this.pageViewProveedor.Location = new System.Drawing.Point(10, 10);
            this.pageViewProveedor.Name = "pageViewProveedor";
            this.pageViewProveedor.Size = new System.Drawing.Size(650, 340);
            this.pageViewProveedor.Text = "Proveedor";
            // 
            // gridProveedor
            // 
            this.gridProveedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProveedor.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "Id";
            gridViewTextBoxColumn9.HeaderText = "Id";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Id";
            gridViewTextBoxColumn9.VisibleInColumnChooser = false;
            gridViewMultiComboBoxColumn1.FieldName = "IdProveedor";
            gridViewMultiComboBoxColumn1.HeaderText = "Proveedor";
            gridViewMultiComboBoxColumn1.Name = "IdProveedor";
            gridViewMultiComboBoxColumn1.Width = 280;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Unitario";
            gridViewTextBoxColumn10.HeaderText = "Unitario";
            gridViewTextBoxColumn10.Name = "Unitario";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            gridViewTextBoxColumn11.FieldName = "IdModelo";
            gridViewTextBoxColumn11.HeaderText = "IdModelo";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "IdModelo";
            gridViewTextBoxColumn11.VisibleInColumnChooser = false;
            gridViewTextBoxColumn12.FieldName = "Nota";
            gridViewTextBoxColumn12.HeaderText = "Nota";
            gridViewTextBoxColumn12.Name = "Nota";
            gridViewTextBoxColumn12.Width = 150;
            gridViewTextBoxColumn13.FieldName = "FechaNuevo";
            gridViewTextBoxColumn13.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn13.Name = "FechaNuevo";
            gridViewTextBoxColumn13.Width = 85;
            gridViewTextBoxColumn14.FieldName = "Creo";
            gridViewTextBoxColumn14.HeaderText = "Creo";
            gridViewTextBoxColumn14.Name = "Creo";
            gridViewTextBoxColumn14.Width = 85;
            this.gridProveedor.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.gridProveedor.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridProveedor.Name = "gridProveedor";
            this.gridProveedor.Size = new System.Drawing.Size(650, 310);
            this.gridProveedor.TabIndex = 1;
            // 
            // toolBarStandarControl1
            // 
            this.toolBarStandarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl1.Etiqueta = "";
            this.toolBarStandarControl1.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl1.Name = "toolBarStandarControl1";
            this.toolBarStandarControl1.ReadOnly = false;
            this.toolBarStandarControl1.ShowActualizar = false;
            this.toolBarStandarControl1.ShowAutorizar = false;
            this.toolBarStandarControl1.ShowCerrar = false;
            this.toolBarStandarControl1.ShowEditar = false;
            this.toolBarStandarControl1.ShowExportarExcel = false;
            this.toolBarStandarControl1.ShowFiltro = false;
            this.toolBarStandarControl1.ShowGuardar = false;
            this.toolBarStandarControl1.ShowHerramientas = false;
            this.toolBarStandarControl1.ShowImagen = false;
            this.toolBarStandarControl1.ShowImprimir = false;
            this.toolBarStandarControl1.ShowNuevo = true;
            this.toolBarStandarControl1.ShowRemover = true;
            this.toolBarStandarControl1.Size = new System.Drawing.Size(650, 30);
            this.toolBarStandarControl1.TabIndex = 42;
            // 
            // TProducto
            // 
            this.TProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProducto.Etiqueta = "";
            this.TProducto.Location = new System.Drawing.Point(0, 0);
            this.TProducto.Name = "TProducto";
            this.TProducto.ReadOnly = false;
            this.TProducto.ShowActualizar = false;
            this.TProducto.ShowAutorizar = false;
            this.TProducto.ShowCerrar = true;
            this.TProducto.ShowEditar = false;
            this.TProducto.ShowExportarExcel = false;
            this.TProducto.ShowFiltro = false;
            this.TProducto.ShowGuardar = true;
            this.TProducto.ShowHerramientas = false;
            this.TProducto.ShowImagen = false;
            this.TProducto.ShowImprimir = false;
            this.TProducto.ShowNuevo = false;
            this.TProducto.ShowRemover = false;
            this.TProducto.Size = new System.Drawing.Size(671, 30);
            this.TProducto.TabIndex = 41;
            // 
            // ProductoModeloForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 482);
            this.Controls.Add(this.pageViews);
            this.Controls.Add(this.panelProducto);
            this.Controls.Add(this.TProducto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoModeloForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Producto - Modelo";
            this.Load += new System.EventHandler(this.ProductoModeloForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).EndInit();
            this.panelProducto.ResumeLayout(false);
            this.panelProducto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbMarca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMarca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbExpecificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEspecificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCodigoBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbEtiquetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtiquetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageViews)).EndInit();
            this.pageViews.ResumeLayout(false);
            this.pageViewGeneral.ResumeLayout(false);
            this.pageViewGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gboxPrecioBase)).EndInit();
            this.gboxPrecioBase.ResumeLayout(false);
            this.gboxPrecioBase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbPrecioBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrecioBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxImpuestoRetenido)).EndInit();
            this.gboxImpuestoRetenido.ResumeLayout(false);
            this.gboxImpuestoRetenido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRentencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetnecionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRetencionIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxIpuestoTraslado)).EndInit();
            this.gboxIpuestoTraslado.ResumeLayout(false);
            this.gboxIpuestoTraslado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxOtrasEspecificaciones)).EndInit();
            this.gboxOtrasEspecificaciones.ResumeLayout(false);
            this.gboxOtrasEspecificaciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbLargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAncho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAncho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAlto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCalibre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxAlmacen)).EndInit();
            this.gboxAlmacen.ResumeLayout(false);
            this.gboxAlmacen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblExistencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMaximo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbExistencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMaximo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockReorden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUnidadAlmacen)).EndInit();
            this.pageViewProveedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel panelProducto;
        private Telerik.WinControls.UI.RadTextBox txbEtiquetas;
        private Telerik.WinControls.UI.RadLabel lblEtiquetas;
        private Telerik.WinControls.UI.RadTextBox txbCodigoBarras;
        private Telerik.WinControls.UI.RadLabel lblCodigoBarras;
        private Telerik.WinControls.UI.RadTextBox txbExpecificacion;
        private Telerik.WinControls.UI.RadLabel lblEspecificacion;
        private Telerik.WinControls.UI.RadTextBox txbMarca;
        private Telerik.WinControls.UI.RadLabel lblMarca;
        private Telerik.WinControls.UI.RadTextBox txbModelo;
        private Telerik.WinControls.UI.RadLabel lblModelo;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cboCategorias;
        private Telerik.WinControls.UI.RadLabel lblCategoria;
        private Telerik.WinControls.UI.RadDropDownList cboTipo;
        private Telerik.WinControls.UI.RadLabel lblTipo;
        private Telerik.WinControls.UI.RadPageView pageViews;
        private Telerik.WinControls.UI.RadPageViewPage pageViewGeneral;
        private Telerik.WinControls.UI.RadGroupBox gboxOtrasEspecificaciones;
        private Telerik.WinControls.UI.RadLabel lblPrecioBase;
        private Telerik.WinControls.UI.RadLabel lblUnidad1;
        private Telerik.WinControls.UI.RadTextBox txbLargo;
        private Telerik.WinControls.UI.RadLabel lblLargo;
        private Telerik.WinControls.UI.RadTextBox txbAncho;
        private Telerik.WinControls.UI.RadDropDownList cboUnidadZ;
        private Telerik.WinControls.UI.RadLabel lblAncho;
        private Telerik.WinControls.UI.RadDropDownList cboUnidadXY;
        private Telerik.WinControls.UI.RadTextBox txbAlto;
        private Telerik.WinControls.UI.RadLabel lblCalibre;
        private Telerik.WinControls.UI.RadGroupBox gboxAlmacen;
        private Telerik.WinControls.UI.RadLabel lblStockMinimo;
        private Telerik.WinControls.UI.RadLabel lblExistencia;
        private Telerik.WinControls.UI.RadTextBox TxbStockMinimo;
        private Telerik.WinControls.UI.RadTextBox TxbExistencia;
        private Telerik.WinControls.UI.RadTextBox TxbStockMaximo;
        private Telerik.WinControls.UI.RadLabel lblReOrden;
        private Telerik.WinControls.UI.RadLabel lblStockMaximo;
        private Telerik.WinControls.UI.RadTextBox TxbStockReorden;
        private Telerik.WinControls.UI.RadLabel lblUnidad2;
        private Telerik.WinControls.UI.RadDropDownList CboUnidadAlmacen;
        private Telerik.WinControls.UI.RadPageViewPage pageViewProveedor;
        private Telerik.WinControls.UI.RadGridView gridProveedor;
        private Telerik.WinControls.UI.RadGroupBox gboxImpuestoRetenido;
        private Telerik.WinControls.UI.RadMaskedEditBox txbRetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox txbRetencionIVA;
        private Telerik.WinControls.UI.RadMaskedEditBox txbRetencionIEPS;
        private Telerik.WinControls.UI.RadCheckBox chkRentencionISR;
        private Telerik.WinControls.UI.RadCheckBox chkRetnecionIVA;
        private Telerik.WinControls.UI.RadLabel lblIEPS2;
        private Telerik.WinControls.UI.RadDropDownList cboRetencionIEPS;
        private Telerik.WinControls.UI.RadGroupBox gboxIpuestoTraslado;
        private Telerik.WinControls.UI.RadDropDownList cboTrasladoIVA;
        private Telerik.WinControls.UI.RadMaskedEditBox txbTrasladoIEPS;
        private Telerik.WinControls.UI.RadMaskedEditBox txbTrasladoIVA;
        private Telerik.WinControls.UI.RadLabel lblIEPS1;
        private Telerik.WinControls.UI.RadDropDownList cboTrasladoIEPS;
        private Telerik.WinControls.UI.RadLabel lblIVA;
        private Telerik.WinControls.UI.RadGroupBox gboxPrecioBase;
        private Telerik.WinControls.UI.RadCalculatorDropDown txbPrecioBase;
        private Common.Forms.ToolBarStandarControl TProducto;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl1;
    }
}
