﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.MP.Forms {
    public class ProductoModeloForm : UI.Almacen.Forms.ProductoModeloForm {
        protected internal IMateriaPrimaService Service;
        protected internal BindingList<UnidadModel> unidades;

        public ProductoModeloForm(IMateriaPrimaService service) : base() {
            this.Service = service;
            this.Load += ProductoModeloForm_Load1;
        }

        public ProductoModeloForm(IMateriaPrimaService service, ModeloDetailModel model) : base() {
            this.Service = service;
            this._Modelo = model;
            this.Load += ProductoModeloForm_Load1;
        }

        private void ProductoModeloForm_Load1(object sender, EventArgs e) {
            this.cboCategorias.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Producto", FilterOperator.StartsWith, ""));
            this.cboCategorias.DataSource = this.Service.GetList<ClasificacionProductoModel>(CatalogoProductoService.Query().ByAlmacen(Domain.Base.ValueObjects.AlmacenEnum.MP).Activo().Build());
            this.cboCategorias.DisplayMember = "Descriptor";
            this.cboCategorias.ValueMember = "IdProducto";
            this.cboCategorias.AutoFilter = true;

            this.unidades = this.Service.GetUnidades();
            this.CboUnidadAlmacen.DataSource = this.unidades;
            this.CboUnidadAlmacen.ValueMember = "Descripcion";
            this.CboUnidadAlmacen.DisplayMember = "Descripcion";

            this.cboUnidadXY.DataSource = new BindingList<UnidadModel>(this.unidades);
            this.cboUnidadXY.ValueMember = "IdUnidad";
            this.cboUnidadXY.DisplayMember = "Descripcion";

            this.cboUnidadZ.DataSource = new BindingList<UnidadModel>(this.unidades);
            this.cboUnidadZ.ValueMember = "IdUnidad";
            this.cboUnidadZ.DisplayMember = "Descripcion";

            cboTrasladoIVA.DisplayMember = "Descripcion";
            cboTrasladoIVA.ValueMember = "Descripcion";
            cboTrasladoIVA.DataSource = CatalogoProductoService.FactorIVA();
            this.cboCategorias.SelectedValueChanged += CboCategorias_SelectedValueChanged;
            this.CreateBinding();
        }

        protected override void CboCategorias_SelectedValueChanged(object sender, EventArgs e) {
            base.CboCategorias_SelectedValueChanged(sender, e);
            var temporal = this.cboCategorias.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var receptor = temporal.DataBoundItem as ClasificacionProductoModel;
                if (receptor != null) {
                    this._Modelo.IdCategoria = receptor.IdCategoria;
                }
            }
        }

        public override void Guardar() {
            this._Modelo = this.Service.Save(this._Modelo);
        }
    }
}
