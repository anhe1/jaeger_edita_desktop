﻿using System;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Almacen.MP.Builder;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Almacen.MP.Services;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Util;

namespace Jaeger.UI.Almacen.MP.Forms {
    public partial class ValesAlmacenCatalogoForm : RadForm {
        #region declaraciones
        protected internal IValeAlmacenService Service;
        protected internal UIMenuElement menuElement;
        protected internal BindingList<ValeAlmacenSingleModel> datos;
        protected internal RadMenuItem TNuevo = new RadMenuItem { Text = "Movimiento" };
        protected internal RadMenuItem TInventario = new RadMenuItem { Text = "Inventario" };
        protected internal RadMenuItem TComprobante = new RadMenuItem { Text = "Comprobante" };
        protected internal RadMenuItem TLista = new RadMenuItem { Text = "Listado" };
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal GridViewTemplate Relaciones = new GridViewTemplate() { Caption = "Relaciones" };
        protected internal GridViewTemplate Status = new GridViewTemplate() { Caption = "Cambio de Status" };
        protected internal RadMenuItem TCargarPDF = new RadMenuItem { Text = "Cargar" };
        protected internal RadMenuItem TDescargaPDF = new RadMenuItem { Text = "Descargar" };
        #endregion

        public ValesAlmacenCatalogoForm() {
            InitializeComponent();
            this.menuElement = new UIMenuElement();
            this.Initialization();
        }

        public ValesAlmacenCatalogoForm(UIMenuElement element) {
            InitializeComponent();
            if (element == null)
                element = new UIMenuElement();
            this.menuElement = element;
            this.Initialization();
        }

        private void ValesAlmacenCatalogoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.CreateView();

        }

        #region barra de herramientas
        public virtual void TVale_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new ValeAlmacenForm(this.Service) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        public virtual void TVale_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.TVale.GridData.CurrentRow.DataBoundItem as ValeAlmacenSingleModel;
            if (seleccionado != null) {
                if (seleccionado.IdStatus == 0) {
                    RadMessageBox.Show(this, Properties.Resources.Message_ComprobanteCancelado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                } else {
                    if (RadMessageBox.Show(this, Properties.Resources.Message_PreguntaCancelar, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        var cancela = new ValeAlmacenStatusForm(this.Service, seleccionado);
                        cancela.Selected += this.TCancelar_Selected;
                        cancela.ShowDialog(this);

                    }
                }
            }
        }

        private void TCancelar_Selected(object sender, IValeAlmacenStatusDetailModel e) {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado == null)
                return;
            seleccionado.IdStatus = e.IdStatusB;
            seleccionado.Cancela = e.Creo;
            seleccionado.FechaCancela = e.FechaNuevo;
            seleccionado.ClaveCancelacion = e.CveMotivo;
            seleccionado.Autorizacion.Add(e as ValeAlmacenStatusModel);
            if (e.Relaciones != null) { // no se actualizan debido a que la relacion se modifica en el formulario de status *TODO*
                if (seleccionado.Relaciones == null)
                    seleccionado.Relaciones = new BindingList<ValeAlmacenRelacionModel>();
                for (int i = 0; i < e.Relaciones.Count; i++) {
                    e.Relaciones[i].ClaveRelacion = e.CveMotivo;
                    e.Relaciones[i].IdClaveRelacion = e.IdCveMotivo;
                    seleccionado.Relaciones.Add(e.Relaciones[i]);
                }
            }
            this.Service.Save(seleccionado);
        }

        public virtual void TVale_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.TVale.GridData.DataSource = this.datos;
        }

        private void TComprobante_Imprimir_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetComprobante)) {
                espera.ShowDialog(this);
            }
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado != null) {
                var d0 = this.Service.GetPrinter(seleccionado.IdComprobante);
                var imprimir = new ReporteForm(d0);
                imprimir.Show();
            }
        }

        private void TListado_Imprimir_Click(object sender, EventArgs eventArgs) {
            var seleccionados = this.TVale.GetFiltro<ValeAlmacenSingleModel>();
            if (seleccionados != null) {
                var lista = new System.Collections.Generic.List<ValeAlmacenSinglePrinter>();
                var almacen = this.GetAlmacen();
                foreach (var item in seleccionados) {
                    var d0 = new ValeAlmacenSinglePrinter();
                    d0.SetValues(item);
                    d0.Almacen = almacen.Descriptor;
                    lista.Add(d0);
                }
                var imprimir = new ReporteForm(lista);
                imprimir.Show();
            }
        }

        public virtual void TVale_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.Conceptos.Caption) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.ShowDialog(this);
                }
                var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Conceptos != null) {
                        foreach (var item in seleccionado.Conceptos) {
                            var row = e.Template.Rows.NewRow();
                            AlmacenGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.Relaciones.Caption) {
                var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Relaciones != null) {
                        foreach (var item in seleccionado.Relaciones) {
                            var row = e.Template.Rows.NewRow();
                            AlmacenGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.Status.Caption) {
                var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.Autorizacion != null) {
                        foreach (var item in seleccionado.Autorizacion) {
                            var row = e.Template.Rows.NewRow();
                            AlmacenGridBuilder.ConvertTo(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        public virtual void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.TVale.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name == "UrlFilePDF" || e.Column.Name == "UrlFilePDF") {
                        var single = this.TVale.GridData.CurrentRow.DataBoundItem as ValeAlmacenSingleModel;
                        string liga = "";
                        if (e.Column.Name == "UrlFileXml")
                            liga = single.UrlFilePDF;
                        else
                            if (e.Column.Name == "UrlFilePdf")
                            liga = single.UrlFilePDF;
                        // validar la liga de descarga
                        if (ValidacionService.URL(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region context menu
        public virtual void TDescargaPDF_Click(object sender, EventArgs e) {
            var single = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (single != null) {
                string liga = single.UrlFilePDF;
                // validar la liga de descarga
                if (ValidacionService.URL(liga)) {
                    var savefiledialog = new SaveFileDialog {
                        FileName = System.IO.Path.GetFileName(liga)
                    };
                    if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                        return;
                    if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                        DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                        if (dr == DialogResult.Yes) {
                            try {
                                System.Diagnostics.Process.Start(savefiledialog.FileName);
                            } catch (Exception ex) {
                                string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        public virtual void TCargarPDF_Click(object sender, EventArgs e) {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado != null) {
                var openFileDialog = new OpenFileDialog() { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
                if (openFileDialog.ShowDialog() != DialogResult.OK) {
                    return;
                }

                if (!string.IsNullOrEmpty(seleccionado.UrlFilePDF)) {
                    if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName,
                        ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                }

                if (File.Exists(openFileDialog.FileName) == false) {
                    RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }

                seleccionado.Tag = openFileDialog.FileName;
                using (var espera = new Waiting1Form(this.UploadPDF)) {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        public virtual void Initialization() {
            this.TVale.Permisos = new Domain.Base.ValueObjects.UIAction(this.menuElement.Permisos);
            this.TVale.Nuevo.Items.Add(TNuevo);
            this.TVale.Nuevo.Items.Add(TInventario);
            this.TVale.Imprimir.Items.Add(this.TComprobante);
            this.TVale.Imprimir.Items.Add(this.TLista);
            this.TVale.ContextAcciones.Visibility = ElementVisibility.Visible;
            this.TVale.ItemLbl.Text = "Almacén:  ";
            this.TVale.ItemHost.HostedItem = this.Almacen.MultiColumnComboBoxElement;
            this.TVale.PDF = "UrlFilePdf";
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.ValueMember = "IdAlmacen";

            this.TVale.GridData.MasterTemplate.Templates.AddRange(this.Conceptos);
            this.TVale.GridData.MasterTemplate.Templates.AddRange(this.Relaciones);
            this.TVale.GridData.MasterTemplate.Templates.AddRange(this.Status);

            this.TNuevo.Click += this.TVale_Nuevo_Click;
            this.TVale.Actualizar.Click += this.TVale_Actualizar_Click;
            this.TVale.Cancelar.Click += this.TVale_Cancelar_Click;
            this.TVale.Cerrar.Click += this.TVale_Cerrar_Click;
            this.TVale.ContextCancelar.Click += this.TVale_Cancelar_Click;
            this.TVale.ContextCargar.Click += this.TCargarPDF_Click;
            this.TVale.ContextDescargar.Click += this.TDescargaPDF_Click;
            this.TComprobante.Click += this.TComprobante_Imprimir_Click;
            this.TLista.Click += this.TListado_Imprimir_Click;
            this.TVale.GridData.CellDoubleClick += this.GridData_CellDoubleClick;
        }

        /// <summary>
        /// carga vista basica del grid
        /// </summary>
        public virtual void CreateView() {
            using (IAlmacenGridBuilder view = new AlmacenGridBuilder()) {
                this.TVale.GridData.Columns.AddRange(view.Templetes().Master().Build());
                this.Conceptos.Columns.AddRange(view.Templetes().Conceptos().Build());
                this.Relaciones.Columns.AddRange(view.Templetes().Relacion().Build());
                this.Status.Columns.AddRange(view.Templetes().CStatus().Build());
            }

            this.Conceptos.Standard();
            this.Relaciones.Standard();
            this.Status.Standard();

            this.TVale.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.Conceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.Conceptos);
            this.Relaciones.HierarchyDataProvider = new GridViewEventDataProvider(this.Relaciones);
            this.Status.HierarchyDataProvider = new GridViewEventDataProvider(this.Status);
        }

        public virtual void Consultar() {
            if (this.Almacen.SelectedItem == null)
                return;
            var current = ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;

            this.datos = new BindingList<ValeAlmacenSingleModel>(this.Service.GetList<ValeAlmacenSingleModel>(
                ValeAlmacenService.Query().ByIdAlmacen(current.IdAlmacen).ByTipoAlmacen(this.Service.Almacen).WithYear(this.TVale.GetEjercicio()).WithMonth(this.TVale.GetPeriodo()).Build()
                ).ToList());
        }

        public virtual void GetComprobante() {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado != null) {
                var d0 = this.Service.GetComprobante(seleccionado.IdComprobante);
                if (d0 != null) {
                    seleccionado.SetValues(d0 as ValeAlmacenDetailModel);
                }
            }
        }

        public virtual IAlmacenModel GetAlmacen() {
            if (this.Almacen.SelectedItem != null)
                return ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            return null;
        }

        public virtual void UploadPDF() {
            var seleccionado = this.TVale.GetCurrent<ValeAlmacenSingleModel>();
            if (seleccionado != null) {
                IValeAlmacenDetailModel d1 = this.Service.UpdatePDF(seleccionado);
                seleccionado.SetValues(d1);
            }
        }
    }
}
