﻿using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.MP.Forms {
    public class ProductoForm : Almacen.Forms.ProductoForm {
        public ProductoForm(IMateriaPrimaService service) : base(service) {
            this._Producto = new ProductoServicioDetailModel();
        }

        public ProductoForm(IMateriaPrimaService service, ProductoServicioDetailModel producto) : base(service, producto) {

        }
    }
}
