﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.QRCode.Helpers;
using Jaeger.Util;

namespace Jaeger.UI.Almacen.MP.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources local = new EmbeddedResources("Jaeger.Domain.Almacen.MP");

        public ReporteForm() : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(ValeAlmacenPrinter)) {
                this.CrearComprobante();
            } else if (this.CurrentObject.GetType() == typeof(List<ValeAlmacenSinglePrinter>)) {
                this.CrearListado();
            }
        }

        private void CrearComprobante() {
            var current = (ValeAlmacenPrinter)this.CurrentObject;
            if (current.TipoComprobante == Domain.Almacen.MP.ValueObjects.TipoComprobanteEnum.Devolucion) {
                this.LoadDefinition = this.local.GetStream("Jaeger.Domain.Almacen.MP.Reports.DevolucionV21Reporte.rdlc");
            } else {
                if (current.Conceptos.Count > 3) {
                    this.LoadDefinition = this.local.GetStream("Jaeger.Domain.Almacen.MP.Reports.ValeAlmacenV21Reporte.rdlc");
                } else {
                    this.LoadDefinition = this.local.GetStream("Jaeger.Domain.Almacen.MP.Reports.ValeAlmacenV22Reporte.rdlc");
                }
            }

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText());
            this.Procesar();
            this.SetDisplayName("Vale de Almacén");
            this.SetParameter("Receptor", current.Receptor);
            this.SetParameter("LeyendaPiePagina", string.Join("", current.QrText()));
            this.SetParameter("NoOrdenCompra", "");
            this.SetParameter("Fecha", current.FechaEmision.ToShortDateString());
            this.SetParameter("TipoComprobante", current.EfectoText);
            this.SetParameter("Contacto", current.Contacto);
            this.SetParameter("Nota", current.Nota);
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(new List<ValeAlmacenPrinter>() { current }));
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearListado() {
            var current = (List<ValeAlmacenSinglePrinter>)this.CurrentObject;
                this.LoadDefinition = this.local.GetStream("Jaeger.Domain.Almacen.MP.Reports.ValeAlmacen15LReporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Listado de vales");
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }
    }
}
