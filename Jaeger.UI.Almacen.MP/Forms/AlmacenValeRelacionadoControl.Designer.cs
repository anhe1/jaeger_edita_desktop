﻿namespace Jaeger.UI.Almacen.MP.Forms {
    partial class AlmacenValeRelacionadoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.checkBoxIncluir = new Telerik.WinControls.UI.RadCheckBox();
            this.CommandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarHostItemIncluir = new Telerik.WinControls.UI.CommandBarHostItem();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.TipoRelacion = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonAgregarCFDI = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonRemoverCFDI = new Telerik.WinControls.UI.CommandBarButton();
            this.ReceptorRFC = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Comprobantes = new Telerik.WinControls.UI.RadGridView();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).BeginInit();
            this.RadCommandBar2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxIncluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar2
            // 
            this.RadCommandBar2.Controls.Add(this.checkBoxIncluir);
            this.RadCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar2.Name = "RadCommandBar2";
            this.RadCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement2});
            this.RadCommandBar2.Size = new System.Drawing.Size(969, 55);
            this.RadCommandBar2.TabIndex = 1;
            // 
            // checkBoxIncluir
            // 
            this.checkBoxIncluir.Location = new System.Drawing.Point(20, 6);
            this.checkBoxIncluir.Name = "checkBoxIncluir";
            this.checkBoxIncluir.Size = new System.Drawing.Size(51, 18);
            this.checkBoxIncluir.TabIndex = 189;
            this.checkBoxIncluir.Text = "Incluir";
            this.checkBoxIncluir.CheckStateChanged += new System.EventHandler(this.checkBoxIncluir_CheckStateChanged);
            // 
            // CommandBarRowElement2
            // 
            this.CommandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement2.Name = "CommandBarRowElement2";
            this.CommandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.CommandBarRowElement2.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "CommandBarStripElement2";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarHostItemIncluir,
            this.commandBarSeparator3,
            this.CommandBarLabel1,
            this.TipoRelacion,
            this.CommandBarSeparator1,
            this.ToolBarButtonAgregarCFDI,
            this.ToolBarButtonRemoverCFDI,
            this.ReceptorRFC});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = false;
            this.ToolBar.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = false;
            // 
            // ToolBarHostItemIncluir
            // 
            this.ToolBarHostItemIncluir.DisplayName = "Incluir";
            this.ToolBarHostItemIncluir.MinSize = new System.Drawing.Size(65, 18);
            this.ToolBarHostItemIncluir.Name = "ToolBarHostItemIncluir";
            this.ToolBarHostItemIncluir.Text = "Incluir";
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // CommandBarLabel1
            // 
            this.CommandBarLabel1.DisplayName = "CommandBarLabel1";
            this.CommandBarLabel1.Name = "CommandBarLabel1";
            this.CommandBarLabel1.Text = "Tipo de Relación: ";
            // 
            // TipoRelacion
            // 
            this.TipoRelacion.AutoSize = true;
            this.TipoRelacion.DisplayName = "Tipo de Relación";
            this.TipoRelacion.DropDownAnimationEnabled = true;
            this.TipoRelacion.Enabled = false;
            this.TipoRelacion.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.TipoRelacion.MaxDropDownItems = 0;
            this.TipoRelacion.MinSize = new System.Drawing.Size(350, 22);
            this.TipoRelacion.Name = "TipoRelacion";
            this.TipoRelacion.Padding = new System.Windows.Forms.Padding(0);
            this.TipoRelacion.Text = "";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonAgregarCFDI
            // 
            this.ToolBarButtonAgregarCFDI.DisplayName = "Agregar";
            this.ToolBarButtonAgregarCFDI.DrawText = true;
            this.ToolBarButtonAgregarCFDI.Enabled = false;
            this.ToolBarButtonAgregarCFDI.Image = global::Jaeger.UI.Almacen.MP.Properties.Resources.add_16px;
            this.ToolBarButtonAgregarCFDI.Name = "ToolBarButtonAgregarCFDI";
            this.ToolBarButtonAgregarCFDI.Text = "Agregar";
            this.ToolBarButtonAgregarCFDI.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonRemoverCFDI
            // 
            this.ToolBarButtonRemoverCFDI.DisplayName = "Quitar";
            this.ToolBarButtonRemoverCFDI.DrawText = true;
            this.ToolBarButtonRemoverCFDI.Enabled = false;
            this.ToolBarButtonRemoverCFDI.Image = global::Jaeger.UI.Almacen.MP.Properties.Resources.delete_16px;
            this.ToolBarButtonRemoverCFDI.Name = "ToolBarButtonRemoverCFDI";
            this.ToolBarButtonRemoverCFDI.Text = "Remover";
            this.ToolBarButtonRemoverCFDI.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.DisplayName = "Receptor RFC";
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Text = "";
            this.ReceptorRFC.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // Comprobantes
            // 
            this.Comprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Comprobantes.Location = new System.Drawing.Point(0, 55);
            // 
            // 
            // 
            this.Comprobantes.MasterTemplate.AllowAddNewRow = false;
            this.Comprobantes.MasterTemplate.AllowEditRow = false;
            this.Comprobantes.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "IdDocumento";
            gridViewTextBoxColumn1.HeaderText = "UUID";
            gridViewTextBoxColumn1.Name = "IdDocumento";
            gridViewTextBoxColumn1.Width = 220;
            gridViewTextBoxColumn2.FieldName = "RFC";
            gridViewTextBoxColumn2.HeaderText = "RFC";
            gridViewTextBoxColumn2.Name = "RFC";
            gridViewTextBoxColumn2.Width = 110;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 250;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Folio";
            gridViewTextBoxColumn5.HeaderText = "Folio";
            gridViewTextBoxColumn5.Name = "Folio";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "Total";
            gridViewTextBoxColumn6.FormatString = "{0:n}";
            gridViewTextBoxColumn6.HeaderText = "Total";
            gridViewTextBoxColumn6.Name = "Total";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 95;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "ImporteAplicado";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "Importe";
            gridViewTextBoxColumn7.Name = "ImporteAplicado";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 95;
            this.Comprobantes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.Comprobantes.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Comprobantes.Name = "Comprobantes";
            this.Comprobantes.ShowGroupPanel = false;
            this.Comprobantes.Size = new System.Drawing.Size(969, 121);
            this.Comprobantes.TabIndex = 188;
            // 
            // AlmacenValeRelacionadoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Comprobantes);
            this.Controls.Add(this.RadCommandBar2);
            this.Name = "AlmacenValeRelacionadoControl";
            this.Size = new System.Drawing.Size(969, 176);
            this.Load += new System.EventHandler(this.CFDIRelacionadoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar2)).EndInit();
            this.RadCommandBar2.ResumeLayout(false);
            this.RadCommandBar2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxIncluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.CommandBarHostItem ToolBarHostItemIncluir;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        internal Telerik.WinControls.UI.CommandBarDropDownList TipoRelacion;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarButtonAgregarCFDI;
        internal Telerik.WinControls.UI.CommandBarButton ToolBarButtonRemoverCFDI;
        private Telerik.WinControls.UI.RadCheckBox checkBoxIncluir;
        internal System.ComponentModel.BackgroundWorker Preparar;
        internal Telerik.WinControls.UI.CommandBarTextBox ReceptorRFC;
        internal Telerik.WinControls.UI.RadGridView Comprobantes;
        private Telerik.WinControls.UI.RadCommandBar RadCommandBar2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarLabel CommandBarLabel1;
        private Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
    }
}
