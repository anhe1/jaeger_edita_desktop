﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Builder {
    public interface IProfileRolBuilder {
        IProfileRolMenusBuilder Menus(IEnumerable<UIMenuElement> menus);
    }

    public interface IProfileRolMenusBuilder {
        IProfileRolUserBuilder UserRol(List<UserRolDetailModel> perfiles);
    }

    public interface IProfileRolUserBuilder {
        IEnumerable<UserRolDetailModel> Build();
    }

    public class ProfileRolBuilder : IProfileRolBuilder, IProfileRolMenusBuilder, IProfileRolUserBuilder {
        protected IEnumerable<UIMenuElement> _Menus;
        protected List<UserRolDetailModel> _Perfiles;

        public ProfileRolBuilder() { }

        public IProfileRolMenusBuilder Menus(IEnumerable<UIMenuElement> menus) {
            this._Menus = menus;
            return this;
        }

        public IProfileRolUserBuilder UserRol(List<UserRolDetailModel> perfiles) {
            this._Perfiles = perfiles;
            return this;
        }

        public IEnumerable<UserRolDetailModel> Build() {
            for (int i = 0; i < _Perfiles.Count(); i++) {
                var persona = from _menu in _Menus
                              join co in _Perfiles[i].Perfil on _menu.Name.ToLower() equals co.Key.ToLower() into _perfil
                              from pco in _perfil.DefaultIfEmpty()
                              select new UIMenuProfileDetailModel {
                                  Id = _menu.Id,
                                  ParentId = _menu.ParentId,
                                  Activo = pco == null ? 0 : pco.Activo,
                                  Name = _menu.Name.ToLower(),
                                  Label = _menu.Label,
                                  Rol = (_menu.Rol != "*" ? (_menu.Rol + (pco != null ? "," + pco.Rol : "")) : "*"),
                                  Permisos = _menu.Permisos,
                                  Action2 = pco == null ? "0000000000" : pco.Action2,
                                  IdPerfil = pco == null ? 0 : pco.IdPerfil,
                                  IdRol = pco == null ? 0 : pco.IdRol,
                                  IdPerfilMenu = pco == null ? 0 : pco.IdPerfilMenu,
                                  Key = pco == null ? null : pco.Key.ToLower(),
                                  Form = _menu.Form,
                                  ToolTipText = _menu.ToolTipText,
                                  Default = _menu.Default,
                                  Assembly = _menu.Assembly,
                              };
                _Perfiles[i].Perfil = new BindingList<UIMenuProfileDetailModel>(persona.ToList());
            }

            return _Perfiles;
        }
    }
}
