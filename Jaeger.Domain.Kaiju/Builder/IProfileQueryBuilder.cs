﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Kaiju.Builder {
    public interface IProfileQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IProfileOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IProfileOnlyActiveQueryBuilder : IConditionalBuilder {

    }
}
