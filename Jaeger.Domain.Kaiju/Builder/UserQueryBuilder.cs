﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Kaiju.Builder {
    public class UserQueryBuilder : ConditionalBuilder, IConditionalBuilder, IUserQueryBuilder, IUserOnlyActiveQueryBuilder {
        public UserQueryBuilder() : base() { }

        public IUserOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            this._Conditionals.Clear();
            if (onlyActive)
                this._Conditionals.Add(new Conditional("_user_a", "1"));
            return this;
        }
    }
}
