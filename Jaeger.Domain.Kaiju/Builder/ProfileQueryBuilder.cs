﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Kaiju.Builder {
    public class ProfileQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IProfileQueryBuilder, IProfileOnlyActiveQueryBuilder {
        public ProfileQueryBuilder() : base() { }

        public IProfileOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive == true)
                this._Conditionals.Add(new Conditional("UIPERFIL_A", "1"));
            return this;
        }
    }
}
