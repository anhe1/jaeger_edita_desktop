﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Kaiju.Builder {
    public interface IUserQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IUserOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IUserOnlyActiveQueryBuilder : IConditionalBuilder {

    }

}
