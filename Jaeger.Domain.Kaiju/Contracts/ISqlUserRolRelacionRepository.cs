﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// interface para repositorio de relaciones
    /// </summary>
    public interface ISqlUserRolRelacionRepository : IGenericRepository<UIMenuRolRelacionModel> {
        /// <summary>
        /// listado de objetos por condicionales
        /// </summary>
        /// <typeparam name="T1">Entidad</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// almacenar relacion de usuario
        /// </summary>
        /// <param name="item">UIMenuRolRelacionModel</param>
        /// <returns>UIMenuRolRelacionModel</returns>
        UIMenuRolRelacionModel Save(UIMenuRolRelacionModel item);
    }
}