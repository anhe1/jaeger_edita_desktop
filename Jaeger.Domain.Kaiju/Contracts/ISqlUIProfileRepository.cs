﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// interface para perfiles de usuario
    /// </summary>
    public interface ISqlUIProfileRepository : IGenericRepository<UIMenuProfileModel> {
        /// <summary>
        /// obtener listado de objetos por lista condicional
        /// </summary>
        /// <typeparam name="T1">Entidad</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// almacenar perfil de usuario
        /// </summary>
        /// <param name="model">UIMenuProfileDetailModel</param>
        /// <returns>UIMenuProfileDetailModel</returns>
        UIMenuProfileDetailModel Save(UIMenuProfileDetailModel model);

        int Salveable(UIMenuProfileModel menu);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onlyActive">verdadero</param>
        /// <returns>UserRolDetailModel</returns>
        IEnumerable<UserRolDetailModel> GetProfile(bool onlyActive);

        /// <summary>
        /// obtener enumeracion
        /// </summary>
        /// <param name="idUser">indice de usuario</param>
        /// <returns>UIMenuProfileDetailModel</returns>
        IEnumerable<UIMenuProfileDetailModel> GetBy(int idUser);
    }
}