﻿using System;

namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// 
    /// </summary>
    public interface IUIMenuProfileModel {
        /// <summary>
        /// obtener o establecer indice de la tabla de permisos (UIPERFIL_ID)
        /// </summary>
        int IdPerfil { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice UIMENU_ID
        /// </summary>
        int IdPerfilMenu { get; set; }

        /// <summary>
        /// obtener o establecer indce del rol de usuario (UIPERFIL_RLSUSR_ID)
        /// </summary>
        int IdRol { get; set; }

        /// <summary>
        /// obtener o establecer la llave del menu
        /// </summary>
        string Key { get; set; }

        /// <summary>
        /// obtener o establecer la cinta de permisos asigandos al perfil
        /// </summary>
        string Action2 { get; set; }

        /// <summary>
        /// obtener o estableer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer si es modificado
        /// </summary>
        bool SetModified { get; set; }
    }
}