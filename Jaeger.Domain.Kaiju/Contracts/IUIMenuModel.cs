﻿namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// Interface para el menu
    /// </summary>
    public interface IUIMenuModel {
        /// <summary>
        /// obtener o establecer el indice de menu (MENU_ID)
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obenter o establecer indice de relacion
        /// </summary>
        int ParentId { get; set; }
        
        /// <summary>
        /// obtener o establecer la llave 
        /// </summary>
        string Name { get; set; }
        
        /// <summary>
        /// obtener o establecer la etiqueta que aparecera en el elemento del menu
        /// </summary>
        string Label { get; set; }
        
        /// <summary>
        /// obtener o establecer el tip de ayuda del elemento
        /// </summary>
        string ToolTipText { get; set; }
        
        /// <summary>
        /// obtener o establecer nombre del formulario a invocar
        /// </summary>
        string Form { get; set; }
        
        /// <summary>
        /// obterner o establecer los permisos que puede tener el menu
        /// </summary>
        string Permisos { get; set; }
        
        /// <summary>
        /// obtenr o establecer el nombre del rol
        /// </summary>
        string Rol { get; set; }
    }
}