﻿namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// 
    /// </summary>
    public interface IUIMenuRolRelacionModel {
        /// <summary>
        /// obtener o establecer indice de relacion
        /// </summary>
        int IdRelacion { get; set; }

        /// <summary>
        /// registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del usuario
        /// </summary>
        int IdUser { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion del ROL
        /// </summary>
        int IdUserRol { get; set; }
    }
}