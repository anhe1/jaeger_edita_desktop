﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// repositorio para menus
    /// </summary>
    public interface ISqlUIMenuRepository : IGenericRepository<UIMenuModel> {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        UIMenuModel Save(UIMenuModel model);

        /// <summary>
        /// obtener listado de menus de la aplicacion EDITA CP
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<UIMenuElement> GetMenus();

        /// <summary>
        /// obtener listado de menus de la aplicacion CPLite
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<UIMenuElement> GetCPLite();

        /// <summary>
        /// obtener listado de menus de aplicacion Edita Nomina
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<UIMenuElement> GetNominaBeta();

        /// <summary>
        /// obtener listado de menus de aplicacion CP Beta
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<UIMenuElement> GetCPBeta();

        /// <summary>
        /// obtener listado de menus de aplicacion Repositorio Beta
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<UIMenuElement> GetRepositorioBeta();

        /// <summary>
        /// obtener listado de menus de la apliacion del validador de comprobantes
        /// </summary>
        /// <returns>IEnumerable</returns>
        IEnumerable<UIMenuElement> GetStrikerEureka();
    }
}
