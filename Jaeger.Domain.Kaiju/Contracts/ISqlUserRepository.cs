﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// repositorio de usuarios
    /// </summary>
    public interface ISqlUserRepository : IGenericRepository<UserModel> {
        /// <summary>
        /// buscar clave de usuario
        /// </summary>
        /// <param name="clave">clave de usuario</param>
        /// <returns>indice de usuario, 0 si no existe</returns>
        int Search(string clave);

        /// <summary>
        /// obtener usuario por clave o correo
        /// </summary>
        /// <param name="clave">clave de usuario</param>
        /// <param name="correo">es corrreo</param>
        /// <returns>Usuario</returns>
        UserModel GetUserBy(string clave, bool correo);

        /// <summary>
        /// almacenar usuario
        /// </summary>
        /// <param name="item">Usuario</param>
        /// <returns>Modelo</returns>
        UserModel Save(UserModel item);

        /// <summary>
        /// obtener lista de objetos similares por condiciones
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
