﻿namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// 
    /// </summary>
    public interface IUserRolModel {
        /// <summary>
        /// obtener o establecer el indice del rol de usuario
        /// </summary>           
        int IdUserRol { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice que indica si el usuario es master
        /// </summary>
        int IdMaster { get; set; }

        /// <summary>
        /// obtener o establecer clave de rol
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del rol
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        System.DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>  
        System.DateTime? FechaModifica { get; set; }
    }
}