﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// interface para repositorio de usuarios
    /// </summary>
    public interface ISqlUserRolRepository : IGenericRepository<UserRolModel> {
        /// <summary>
        /// obtener informacion del rol asignado al usuario
        /// </summary>
        /// <param name="idUser">indice del usuario</param>
        UserRolModel GetByIdUser(int idUser);

        /// <summary>
        /// almacenar usuario
        /// </summary>
        /// <param name="item">UserRolModel</param>
        /// <returns>UserRolModel</returns>
        UserRolModel Save(UserRolModel item);
    }
}
