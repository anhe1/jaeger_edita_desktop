﻿namespace Jaeger.Domain.Kaiju.Contracts {
    /// <summary>
    /// interface de usuario del sistema
    /// </summary>
    public interface IUserModel {
        /// <summary>
        /// obtener o establecer indice del usuario
        /// </summary>
        int IdUser { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con el directorio
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer indice del perfil asociado al usuario
        /// </summary>
        int IdUserRol { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del usuario
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer password
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// obtener o establecer APIKEY para webservice
        /// </summary>
        string ApiKey { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer correo electronico asociado
        /// </summary>
        string Correo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        System.DateTime FechaNuevo { get; set; }
    }
}