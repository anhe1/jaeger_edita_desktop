﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Kaiju.Entities {
    /// <summary>
    /// combinacion de menu con el perfil de usuario
    /// </summary>
    [SugarTable("_uiperfil")]
    public class UIMenuProfileDetailModel : UIMenuProfileModel, Contracts.IUIMenuProfileModel {
        private string _rol = "administrador";
        private string _Name;

        public UIMenuProfileDetailModel() : base() { }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice del menu
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [DataNames("UIMENU_ID")]
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con el menu
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [DataNames("UIMENU_SBID")]
        public int ParentId { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string Parent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool Default { get; set; }

        /// <summary>
        /// obtener o establecer la llave 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [DataNames("UIMENU_KEY")]
        public string Name {
            get { return this._Name; }
            set {
                if (value != null)
                    this._Name = value.ToLower();
                else
                    this._Name = value;
            }
        }

        /// <summary>
        /// obtener o establecer la etiqueta que aparecera en el elemento del menu
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [DataNames("UIMENU_LABEL")]
        public string Label {
            get; set;
        }

        [Obsolete("No existe referencia para esta propiedad")]
        [SugarColumn(IsIgnore = true)]
        public string TypeOf {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el tip de ayuda del elemento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [DataNames("UIMENU_TOOLTIP")]
        public string ToolTipText {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre del formulario asignado
        /// </summary>
        [DataNames("UIMENU_FORM")]
        [SugarColumn(IsIgnore = true)]
        public string Form {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre del ensamble donde se encuentra el formulario
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Assembly {
            get; set;
        }

        /// <summary>
        /// obtener o establecer lista de permisos default 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [DataNames("UIMENU_ACTION")]
        public string Permisos {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el rol que debe ser aplicado separado por comas (,)
        /// </summary>
        [DataNames("UIMENU_ROL", "RLSUSR_NMBR", "_rlsusr_nmbr")]
        [SugarColumn(IsIgnore = true)]
        public string Rol {
            get {
                if (this._rol != null)
                    return this._rol.ToLower();
                return string.Empty;
            }
            set {
                if (value != null) {
                    this._rol = value.ToLower();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener listado de roles asignados
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public List<Rol> ListRol {
            get {
                return new List<Rol>(this.Rol.Split(',').Select(c => new Rol(c)));
            }
        }
        #endregion
    }
}
