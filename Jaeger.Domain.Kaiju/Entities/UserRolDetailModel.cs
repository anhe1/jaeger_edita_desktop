﻿using SqlSugar;
using System.ComponentModel;

namespace Jaeger.Domain.Kaiju.Entities {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("_rlsusr")]
    public class UserRolDetailModel : UserRolModel {
        private BindingList<UIMenuProfileDetailModel> _perfil;
        private BindingList<UIMenuRolRelacionModel> _relacion;

        /// <summary>
        /// constructor
        /// </summary>
        public UserRolDetailModel() {
            this._perfil = new BindingList<UIMenuProfileDetailModel>();
            this._relacion = new BindingList<UIMenuRolRelacionModel>();
        }

        #region propiedades
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<UIMenuProfileDetailModel> Perfil {
            get { return _perfil; }
            set {
                _perfil = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<UIMenuRolRelacionModel> Relacion {
            get { return this._relacion; }
            set { this._relacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
