﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Kaiju.Entities {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("_rlusrl")]
    public class UIMenuRolRelacionModel : Base.Abstractions.UIMenuRolRelacion, Contracts.IUIMenuRolRelacionModel {
        private bool _activo;

        /// <summary>
        /// constructor
        /// </summary>
        public UIMenuRolRelacionModel() : base() {
            _activo = true;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice de relacion
        /// </summary>
        [DataNames("RLUSRL_ID", "_rlusrl_id")]
        [SugarColumn(ColumnName = "_rlusrl_id", ColumnDescription = "", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdRelacion {
            get { return base.IdRelacion; }
            set {
                base.IdRelacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("RLUSRL_A", "_rlusrl_a")]
        [SugarColumn(ColumnName = "_rlusrl_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return _activo; }
            set {
                _activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del usuario
        /// </summary>
        [DataNames("RLUSRL_USR_ID", "_rlusrl_usr_id")]
        [SugarColumn(ColumnName = "_rlusrl_usr_id", ColumnDescription = "indice del usuario")]
        public new int IdUser {
            get { return base.IdUser; }
            set {
                base.IdUser = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion del ROL
        /// </summary>
        [DataNames("RLUSRL_RLSUSR_ID", "_rlusrl_rlsusr_id")]
        [SugarColumn(ColumnName = "_rlusrl_rlsusr_id", ColumnDescription = "indice de relacion con el rol")]
        public new int IdUserRol {
            get { return base.IdUserRol; }
            set {
                base.IdUserRol = value;
                OnPropertyChanged();
            }
        }
        #endregion
    }
}
