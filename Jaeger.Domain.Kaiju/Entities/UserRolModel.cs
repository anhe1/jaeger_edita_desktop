﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Kaiju.Entities {
    ///<summary>
    /// Roles de usuario (_rlsusr)
    ///</summary>
    [SugarTable("_rlsusr")]
    public partial class UserRolModel : UserRol, Contracts.IUserRolModel {
        #region declaraciones
        private string perfilField;
        private string _creo;
        private string _modifica;
        private DateTime _fechaNuevo;
        private DateTime? _fechaModifica;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public UserRolModel() : base() {
            this.Activo = true;
            this._fechaNuevo = DateTime.Now;
        }

        #region propiedades
        /// <summary>
        /// indice de la tabla
        /// </summary>           
        [DataNames("_rlsusr_id", "RLSUSR_ID")]
        [SugarColumn(ColumnName = "_rlsusr_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdUserRol {
            get {
                return base.IdUserRol;
            }
            set {
                base.IdUserRol = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:IsActive
        /// Default:1
        /// Nullable:False
        /// </summary>       
        [DataNames("_rlsusr_a", "RLSUSR_A")]
        [SugarColumn(ColumnName = "_rlsusr_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Maestro
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [DataNames("_rlsusr_m", "RLSUSR_M")]
        [SugarColumn(ColumnName = "_rlsusr_m", ColumnDescription = "maestro", IsNullable = false, DefaultValue = "0")]
        public new int IdMaster {
            get {
                return base.IdMaster;
            }
            set {
                base.IdMaster = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Clave
        /// Default:
        /// Nullable:False
        /// </summary>
        [DataNames("_rlsusr_clv", "RLSUSR_CLV")]
        [SugarColumn(ColumnName = "_rlsusr_clv", ColumnDescription = "clave", IsNullable = false, Length = 100)]
        public new string Clave {
            get {
                return base.Clave;
            }
            set {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_rlsusr_nmbr", "RLSUSR_NMBR")]
        [SugarColumn(ColumnName = "_rlsusr_nmbr", ColumnDescription = "nombre", IsNullable = false, Length = 100)]
        public new string Nombre {
            get {
                return base.Nombre;
            }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Fecha nuevo
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>  
        [DataNames("_rlsusr_fn", "RLSUSR_FN")]
        [SugarColumn(ColumnName = "_rlsusr_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this._fechaNuevo;
            }
            set {
                this._fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ViewUsuario nuevo
        /// Default:NULL
        /// Nullable:True
        /// </summary>
        [DataNames("_rlsusr_usr_n", "RLSUSR_USR_N")]
        [SugarColumn(ColumnName = "_rlsusr_usr_n", ColumnDescription = "clave del creador del registro", IsNullable = true, Length = 50)]
        public string Creo {
            get {
                return this._creo;
            }
            set {
                this._creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ViewUsuario modifica
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_rlsusr_usr_m", "RLSUSR_USR_M")]
        [SugarColumn(ColumnName = "_rlsusr_usr_m", ColumnDescription = "clave del usuario que modifica el registro", IsNullable = true, IsOnlyIgnoreInsert = true, Length = 50)]
        public string Modifica {
            get {
                return this._modifica;
            }
            set {
                this._modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Fecha modifica
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>  
        [DataNames("_rlsusr_fm", "RLSUSR_FM")]
        [SugarColumn(ColumnName = "_rlsusr_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this._fechaModifica >= firstGoodDate)
                    return this._fechaModifica;
                return null;
            }
            set {
                this._fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
