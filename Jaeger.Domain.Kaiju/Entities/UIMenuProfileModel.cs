﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Kaiju.Contracts;

namespace Jaeger.Domain.Kaiju.Entities {
    /// <summary>
    /// combinacion de menu con el perfil de usuario
    /// </summary>
    [SugarTable("_uiperfil")]
    public class UIMenuProfileModel : Base.Abstractions.BasePropertyChangeImplementation, IUIMenuProfileModel {
        #region declaraciones
        private int _UIPERFIL_ID;
        private int _UIPERFIL_A;
        private int _UIPERFIL_UIMENU_ID;
        private int _UIPERFIL_RLSUSR_ID;
        private string _UIPERFIL_ACTION;
        private DateTime _UIPERFIL_FN;
        private string _UIPERFIL_USR_N;
        private DateTime? _UIPERFIL_FM;
        private string _UIPERFIL_USR_M;
        private string _UIPERFIL_KEY;
        private bool _SetModified = false;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public UIMenuProfileModel() : base() {
            this._UIPERFIL_FN = DateTime.Now;
            this._SetModified = false;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice de la tabla de permisos (UIPERFIL_ID)
        /// </summary>
        [DataNames("_uiperfil_id", "UIPERFIL_ID")]
        [SugarColumn(ColumnName = "_uiperfil_id", ColumnDescription = "indice de la tabla de perfiles", IsPrimaryKey = false, IsIdentity = false)]
        public int IdPerfil {
            get { return _UIPERFIL_ID; }
            set {
                _UIPERFIL_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_uiperfil_a", "UIPERFIL_A")]
        [SugarColumn(ColumnName = "_uiperfil_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public int Activo {
            get { return _UIPERFIL_A; }
            set {
                _UIPERFIL_A = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice UIMENU_ID
        /// </summary>
        [DataNames("_uiperfil_uimenu_id", "UIPERFIL_UIMENU_ID")]
        [SugarColumn(ColumnName = "_uiperfil_uimenu_id", ColumnDescription = "indice de la ")]
        public int IdPerfilMenu {
            get { return _UIPERFIL_UIMENU_ID; }
            set {
                _UIPERFIL_UIMENU_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indce del rol de usuario (UIPERFIL_RLSUSR_ID)
        /// </summary>
        [DataNames("_uiperfil_rlsusr_id", "UIPERFIL_RLSUSR_ID")]
        [SugarColumn(ColumnName = "_uiperfil_rlsusr_id", ColumnDescription = "indice del rol", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRol {
            get { return _UIPERFIL_RLSUSR_ID; }
            set {
                _UIPERFIL_RLSUSR_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la llave de el menu
        /// </summary>
        [DataNames("_uiperfil_key", "UIPERFIL_KEY")]
        [SugarColumn(ColumnName = "_uiperfil_key", ColumnDescription = "llave del menu", Length = 25, IsPrimaryKey = true, IsIdentity = true)]
        public string Key {
            get { return _UIPERFIL_KEY; }
            set { if (value != null)
                    _UIPERFIL_KEY = value.ToLower();
                else _UIPERFIL_KEY = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cinta de permisos asigandos al perfil
        /// </summary>
        [DataNames("_uiperfil_action", "UIPERFIL_ACTION")]
        [SugarColumn(ColumnName = "_uiperfil_action", ColumnDescription = "", Length = 10)]
        public string Action2 {
            get { return _UIPERFIL_ACTION; }
            set {
                _UIPERFIL_ACTION = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estableer fecha de creacion del registro
        /// </summary>
        [DataNames("_uiperfil_fn", "UIPERFIL_FN")]
        [SugarColumn(ColumnName = "_uiperfil_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return _UIPERFIL_FN; }
            set {
                _UIPERFIL_FN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("_uiperfil_usr_n", "UIPERFIL_USR_N")]
        [SugarColumn(ColumnName = "_uiperfil_usr_n", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return _UIPERFIL_USR_N; }
            set {
                _UIPERFIL_USR_N = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_uiperfil_fm", "UIPERFIL_FM")]
        [SugarColumn(ColumnName = "_uiperfil_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get { return _UIPERFIL_FM; }
            set {
                _UIPERFIL_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_uiperfil_usr_m", "UIPERFIL_USR_M")]
        [SugarColumn(ColumnName = "_uiperfil_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get { return _UIPERFIL_USR_M; }
            set {
                _UIPERFIL_USR_M = value;
                OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer si el registro fue modificado, solo se utiliza para la vista
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool SetModified {
            get { return this._SetModified; }
            set {
                this._SetModified = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
