﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Kaiju.Entities {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("_uimenu")]
    public class UIMenuModel : Base.Abstractions.UIMenuBaseItem, Contracts.IUIMenuModel {
        /// <summary>
        /// constructor
        /// </summary>
        public UIMenuModel() {
            Rol = string.Empty;
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">key</param>
        /// <param name="label">etiqueta</param>
        /// <param name="toolTipText">tip de ayufa</param>
        /// <param name="rol">nombres de los roles separados por comas (,)</param>
        public UIMenuModel(string name, string label, string toolTipText, string rol) {
            Label = label;
            Name = name;
            ToolTipText = toolTipText;
            Rol = rol;
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="baseItem"></param>
        public UIMenuModel(Base.Abstractions.UIMenuBaseItem baseItem) {
            Label = baseItem.Label;
            Name = baseItem.Name;
            ToolTipText = baseItem.ToolTipText;
            Rol = baseItem.Rol;
            Form = baseItem.Form;
            Permisos = baseItem.Permisos;
            Id = baseItem.Id;
            ParentId = baseItem.ParentId;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer el indice de menu (MENU_ID)
        /// </summary>
        [DataNames("_UIMENU_ID")]
        [SugarColumn(ColumnName = "_uimenu_id", ColumnDescription = "indice del menu", IsIdentity = true, IsPrimaryKey = true)]
        public new int Id {
            get { return base.Id; }
            set {
                base.Id = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obenter o establecer indice de relacion
        /// </summary>
        [DataNames("_UIMENU_SBID")]
        [SugarColumn(ColumnName = "_uimenu_sbid", ColumnDescription = "indice de relacion")]
        public new int ParentId {
            get { return base.ParentId; }
            set {
                base.ParentId = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la llave 
        /// </summary>
        [DataNames("_UIMENU_KEY")]
        [SugarColumn(ColumnName = "_uimenu_key", ColumnDescription = "nombre o llave del menu", Length = 25)]
        public new string Name {
            get { return base.Name; }
            set {
                base.Name = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la etiqueta que aparecera en el elemento del menu
        /// </summary>
        [DataNames("_UIMENU_LABEL")]
        [SugarColumn(ColumnName = "_uimenu_label", ColumnDescription = "etiqueta que aparece en el elemento del menu", Length = 50)]
        public new string Label {
            get { return base.Label; }
            set {
                base.Label = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tip de ayuda del elemento
        /// </summary>
        [DataNames("_UIMENU_TOOLTIP")]
        [SugarColumn(ColumnName = "_uimenu_tooltip", ColumnDescription = "tipo de ayuda del elemento", Length = 100)]
        public new string ToolTipText {
            get { return base.ToolTipText; }
            set {
                base.ToolTipText = value;
                OnPropertyChanged();
            }
        }

        [DataNames("_UIMENU_FORM")]
        [SugarColumn(ColumnName = "_uimenu_form", ColumnDescription = "formulario relacionado", Length = 100)]
        public new string Form {
            get { return base.Form; }
            set {
                base.Form = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obterner o establecer los permisos que puede tener el menu
        /// </summary>
        [DataNames("_UIMENU_ACTION")]
        [SugarColumn(ColumnName = "_uimenu_action", ColumnDescription = "permisos del usuario", Length = 10)]
        public new string Permisos {
            get { return base.Permisos; }
            set {
                base.Permisos = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtenr o establecer el nombre del rol
        /// </summary>
        [DataNames("_UIMENU_ROL", "_RLSUSR_NMBR", "RLSUSR_NMBR")]
        [SugarColumn(ColumnName = "_uimenu_rol", ColumnDescription = "nombre del rol")]
        public new string Rol {
            get { return base.Rol; }
            set {
                base.Rol = value;
                OnPropertyChanged();
            }
        }
        #endregion
    }
}
