﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Kaiju.Entities {
    ///<summary>
    ///catalogo de usuarios
    ///</summary>
    [SugarTable("_user", "usuarios: tabla de usuarios")]
    public class UserModel : Base.Abstractions.BasePropertyChangeImplementation, Contracts.IUserModel {
        #region declaraciones
        private int index;
        private int idPerfil;
        private int idDirectorio;
        private bool activo;
        private string apiKey;
        private string clave;
        private string password;
        private string correo;
        private string nombre;
        private DateTime fechaNuevo;
        private string creo;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public UserModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        #region propiedades
        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_user_id", "USER_ID")]
        [SugarColumn(ColumnName = "_user_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true, IsOnlyIgnoreInsert = true)]
        public int IdUser {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [DataNames("_user_a", "USER_A")]
        [SugarColumn(ColumnName = "_user_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ID de roll
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [DataNames("_user_idroll", "USER_IDROLL")]
        [SugarColumn(ColumnName = "_user_idroll", ColumnDescription = "ID de roll", DefaultValue = "0")]
        public int IdUserRol {
            get {
                return this.idPerfil;
            }
            set {
                this.idPerfil = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio, utilizado principalmente para clientes y vendedores
        /// </summary>
        [DataNames("_user_iddir", "USER_IDDIR")]
        [SugarColumn(ColumnName = "_user_iddir", ColumnDescription = "ID de directorio", DefaultValue = "0")]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave unica de registro en sistema
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_user_clv", "USER_CLV")]
        [SugarColumn(ColumnName = "_user_clv", ColumnDescription = "clave unica de registro en sistema", Length = 14, IsNullable = true)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_user_apikey", "USER_APIKEY")]
        [SugarColumn(ColumnName = "_user_apikey", ColumnDescription = "clave de acceso al API de forma externa", Length = 32, IsNullable = true)]
        public string ApiKey {
            get {
                return this.apiKey;
            }
            set {
                this.apiKey = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:password (md5 y base64)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_user_psw", "USER_PSW")]
        [SugarColumn(ColumnName = "_user_psw", ColumnDescription = "password (md5 y base64)", Length = 64)]
        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:correo electronic, separados por (;)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_user_mail", "USER_MAIL")]
        [SugarColumn(ColumnName = "_user_mail", ColumnDescription = "correo electronico, separados por (;)", IsNullable = true, Length = 128)]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_user_nom", "USER_NOM")]
        [SugarColumn(ColumnName = "_user_nom", ColumnDescription = "nombre completo del usuario", Length = 128)]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:True
        /// </summary>           
        [DataNames("_user_fn", "USER_FN")]
        [SugarColumn(ColumnName = "_user_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_user_usr_n", "USER_USR_N")]
        [SugarColumn(ColumnName = "_user_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
