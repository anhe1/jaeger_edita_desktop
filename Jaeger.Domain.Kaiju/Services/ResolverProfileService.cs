﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.Domain.Kaiju.Services {
    public static class ResolverProfileService {
        /// <summary>
        /// obtener listado de menus con los permisos asignados al perfil
        /// </summary>
        /// <param name="menus">lista base de menus</param>
        /// <param name="profile">objeto del perfil</param>
        public static IEnumerable<UIMenuElement> Resolver(IEnumerable<UIMenuElement> menus, IEnumerable<UIMenuProfileDetailModel> profile) {
            if (profile != null) {
                if (profile.Count() > 1) {
                    try {
                        var persona = from _menu in menus
                                      join co in profile on _menu.Name.ToLower() equals co.Key.ToLower() into _perfil
                                      from pco in _perfil.DefaultIfEmpty()
                                      select new UIMenuElement {
                                          Id = _menu.Id,
                                          ParentId = _menu.ParentId,
                                          Parent = _menu.Parent,
                                          Name = _menu.Name.ToLower(),
                                          Label = _menu.Label,
                                          Form = _menu.Form,
                                          Assembly = _menu.Assembly,
                                          Rol = (_menu.Rol != "*" ? (_menu.Rol + (pco != null ? "," + pco.Rol : "")) : "*"),
                                          Permisos = (pco != null ? pco.Action2 : "0000000000"),
                                          Default = _menu.Default,
                                          ToolTipText = _menu.ToolTipText,
                                          IsAvailable = _menu.IsAvailable, IsEnabled = (pco != null ? pco.Activo > 0 : false)
                                      };
                        return persona;
                    } catch (System.Exception) {

                        
                    }
                }
            }

            return menus;
        }


        public static IEnumerable<UserRolDetailModel> Resolver(IEnumerable<UIMenuElement> menus, List<UserRolDetailModel> perfiles) {
            for (int i = 0; i < perfiles.Count(); i++) {
                var persona = from _menu in menus
                              join co in perfiles[i].Perfil on _menu.Name.ToLower() equals co.Key.ToLower() into _perfil
                              from pco in _perfil.DefaultIfEmpty()
                              select new UIMenuProfileDetailModel {
                                  Id = _menu.Id,
                                  ParentId = _menu.ParentId,
                                  Activo = pco == null ? 0 : pco.Activo,
                                  Name = _menu.Name.ToLower(),
                                  Parent = _menu.Parent,
                                  Label = _menu.Label,
                                  Rol = (_menu.Rol != "*" ? (_menu.Rol + (pco != null ? "," + pco.Rol : "")) : "*"),
                                  Permisos = _menu.Permisos,
                                  Action2 = pco == null ? "0000000000" : pco.Action2,
                                  IdPerfil = pco == null ? 0 : pco.IdPerfil,
                                  IdRol = pco == null ? 0 : pco.IdRol,
                                  IdPerfilMenu = pco == null ? 0 : pco.IdPerfilMenu,
                                  Key = pco == null ? null : pco.Key.ToLower(),
                                  Form = _menu.Form,
                                  ToolTipText = _menu.ToolTipText,
                                  Default = _menu.Default,
                                  Assembly = _menu.Assembly,
                              };
                perfiles[i].Perfil = new BindingList<UIMenuProfileDetailModel>(persona.ToList());
            }

            return perfiles;
        }
    }
}
