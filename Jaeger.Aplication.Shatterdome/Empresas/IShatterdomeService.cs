﻿using System.ComponentModel;
using Jaeger.Domain.Shatterdome.Entities;

namespace Jaeger.Aplication.Shatterdome.Empresas {
    public interface IShatterdomeService {
        BindingList<ShatterdomeDetailModel> GetList();
        /// <summary>
        /// desactivar empresa
        /// </summary>
        ShatterdomeModel Delete(ShatterdomeModel model);

        bool TestService(ShatterdomeDetailModel model);

        ShatterdomeModel Save(ShatterdomeModel model);
    }
}
