﻿namespace Jaeger.Aplication.Shatterdome.Empresas {
    public interface IPasswordGeneratorService {
        string Generate();

        string Exclusions {
            get;set;
        }

        int Minimum {
            get;set;
        }

        int Maximum {
            get;set;
        }

        bool ExcludeSymbols {
            get;set;
        }

        bool RepeatCharacters {
            get;set;
        }

        bool ConsecutiveCharacters {
            get;set;
        }
    }
}
