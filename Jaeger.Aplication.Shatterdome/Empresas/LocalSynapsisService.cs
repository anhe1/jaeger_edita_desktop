﻿using System;
using System.IO;
using Newtonsoft.Json;
using Jaeger.Domain.Services;
using Jaeger.Domain.Shatterdome.Entities;
using Jaeger.Util.Services;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Shatterdome.Empresas {
    public class LocalSynapsisService {

        /// <summary>
        /// configuracion json
        /// </summary>
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        /// <summary>
        /// nombre local del archivo de configuracion
        /// </summary>
        protected string fileSynapsis = @"c:\Jaeger\jaeger_alpha_tango.json";

        public LocalSynapsisService() {

        }

        public bool Load() {
            var horizon = new EmbeddedResources("Jaeger.Aplication.Shatterdome");
            var local = new ShatterdomeSynapsis();
            string contenido;

            if (!this.Exists())
                contenido = horizon.GetAsString("Jaeger.Aplication.Shatterdome.Properties.Resources.jaeger_alpha_tango.json");
            else
                contenido = FileService.ReadFileText(this.fileSynapsis);

            try {
                local = JsonConvert.DeserializeObject<ShatterdomeSynapsis>(HelperCsTripleDes.Decrypt(contenido, "", true), this.conf);
                if (local == null)
                    local = JsonConvert.DeserializeObject<ShatterdomeSynapsis>(contenido, this.conf);
                ConfigService.Synapsis = local;
                return true;
            } catch (JsonException ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public bool Save() {
            var local = new ShatterdomeSynapsis();
            local = ConfigService.Synapsis;
            FileService.WriteFileText(this.fileSynapsis, local.Json());
            return false;
        }

        public bool Exists() {
            return File.Exists(this.fileSynapsis);
        }
    }
}
