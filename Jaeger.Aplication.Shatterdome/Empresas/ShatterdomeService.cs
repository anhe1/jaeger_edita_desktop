﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Dataaccess.Shatterdome.Repositories;
using Jaeger.Domain.Shatterdome.Contracts;
using Jaeger.Domain.Shatterdome.Entities;

namespace Jaeger.Aplication.Shatterdome.Empresas {
    public class ShatterdomeService : IShatterdomeService {
        protected ISqlShatterdomeRepository repository;

        public ShatterdomeService() {
            this.repository = new SqlSugarShatterdomeRepository(ConfigService.Synapsis.Configuracion);
        }

        /// <summary>
        /// desactivar empresa
        /// </summary>
        public ShatterdomeModel Delete(ShatterdomeModel model) {
            return this.repository.Delete(model);
        }

        public ShatterdomeModel Save(ShatterdomeModel model) {
            return this.repository.Save(model);
        }

        public BindingList<ShatterdomeDetailModel> GetList() {
            return new BindingList<ShatterdomeDetailModel>(this.repository.GetList().ToList());
        }

        public bool TestService(ShatterdomeDetailModel model) {
            return this.repository.TestConfig(model.Synapsis.BaseDatos.Edita);
        }
    }
}
