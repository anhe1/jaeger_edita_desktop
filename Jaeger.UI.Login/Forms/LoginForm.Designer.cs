﻿namespace Jaeger.UI.Login.Forms {
    partial class LoginForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.txtPassword = new Telerik.WinControls.UI.RadTextBox();
            this.lblEmpresa = new Telerik.WinControls.UI.RadLabel();
            this.lblUsuario = new Telerik.WinControls.UI.RadLabel();
            this.lblPassword = new Telerik.WinControls.UI.RadLabel();
            this.cboUsuario = new Telerik.WinControls.UI.RadAutoCompleteBox();
            this.btnAceptar = new Telerik.WinControls.UI.RadButton();
            this.btnCerrar = new Telerik.WinControls.UI.RadButton();
            this.lblInformacion = new Telerik.WinControls.UI.RadLabel();
            this.Waiting = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.office2010BlackTheme = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.office2010BlueTheme = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.office2010SilverTheme = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.dotsLineWaitingBarIndicatorElement2 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.cboEmpresa = new Telerik.WinControls.UI.RadDropDownList();
            this.linkRecupera = new System.Windows.Forms.LinkLabel();
            this.LogoBackGround = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LogoPicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInformacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoBackGround)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(82, 67);
            this.txtPassword.MaxLength = 256;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.NullText = "Contraseña";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.ShowClearButton = true;
            this.txtPassword.Size = new System.Drawing.Size(197, 20);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxbPassword_KeyDown);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.Location = new System.Drawing.Point(11, 15);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(51, 18);
            this.lblEmpresa.TabIndex = 2;
            this.lblEmpresa.Text = "Empresa:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.Location = new System.Drawing.Point(11, 41);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(47, 18);
            this.lblUsuario.TabIndex = 3;
            this.lblUsuario.Text = "Usuario:";
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(11, 68);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(65, 18);
            this.lblPassword.TabIndex = 4;
            this.lblPassword.Text = "Contraseña:";
            // 
            // cboUsuario
            // 
            this.cboUsuario.Location = new System.Drawing.Point(82, 41);
            this.cboUsuario.MaxLength = 256;
            this.cboUsuario.Name = "cboUsuario";
            this.cboUsuario.NullText = "Usuario ó Correo Electrónico";
            this.cboUsuario.ShowClearButton = true;
            this.cboUsuario.Size = new System.Drawing.Size(197, 20);
            this.cboUsuario.TabIndex = 1;
            this.cboUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboUsuario_KeyDown);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(82, 112);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(96, 24);
            this.btnAceptar.TabIndex = 3;
            this.btnAceptar.Text = "Iniciar";
            this.btnAceptar.Click += new System.EventHandler(this.Button_Aceptar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Location = new System.Drawing.Point(183, 112);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(96, 24);
            this.btnCerrar.TabIndex = 4;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.Click += new System.EventHandler(this.Button_Cerrar_Click);
            // 
            // lblInformacion
            // 
            this.lblInformacion.BackColor = System.Drawing.Color.White;
            this.lblInformacion.Location = new System.Drawing.Point(11, 194);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(194, 18);
            this.lblInformacion.TabIndex = 31;
            this.lblInformacion.Text = "Proporcione la información necesaria.";
            this.lblInformacion.Visible = false;
            // 
            // Waiting
            // 
            this.Waiting.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Waiting.Location = new System.Drawing.Point(0, 366);
            this.Waiting.Name = "Waiting";
            this.Waiting.ShowText = true;
            this.Waiting.Size = new System.Drawing.Size(297, 24);
            this.Waiting.TabIndex = 32;
            this.Waiting.Text = "Solicitando datos al servidor";
            this.Waiting.Visible = false;
            this.Waiting.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Waiting.WaitingSpeed = 80;
            this.Waiting.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Waiting.GetChildAt(0))).WaitingSpeed = 80;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Waiting.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Waiting.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            this.dotsLineWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsLineWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // dotsLineWaitingBarIndicatorElement2
            // 
            this.dotsLineWaitingBarIndicatorElement2.Name = "dotsLineWaitingBarIndicatorElement2";
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.FormatString = "{0}";
            this.cboEmpresa.Location = new System.Drawing.Point(82, 15);
            this.cboEmpresa.MaxLength = 14;
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.NullText = "RFC";
            this.cboEmpresa.Size = new System.Drawing.Size(197, 20);
            this.cboEmpresa.TabIndex = 0;
            this.cboEmpresa.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.TxbEmpresa_SelectedIndexChanged);
            // 
            // linkRecupera
            // 
            this.linkRecupera.AutoSize = true;
            this.linkRecupera.Location = new System.Drawing.Point(224, 90);
            this.linkRecupera.Name = "linkRecupera";
            this.linkRecupera.Size = new System.Drawing.Size(55, 13);
            this.linkRecupera.TabIndex = 33;
            this.linkRecupera.TabStop = true;
            this.linkRecupera.Text = "Recupera";
            this.linkRecupera.Visible = false;
            this.linkRecupera.Click += new System.EventHandler(this.LinkRecupera_Click);
            // 
            // LogoBackGround
            // 
            this.LogoBackGround.BackColor = System.Drawing.Color.White;
            this.LogoBackGround.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogoBackGround.Location = new System.Drawing.Point(0, 0);
            this.LogoBackGround.Name = "LogoBackGround";
            this.LogoBackGround.Size = new System.Drawing.Size(297, 218);
            this.LogoBackGround.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.LogoBackGround.TabIndex = 30;
            this.LogoBackGround.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblEmpresa);
            this.panel1.Controls.Add(this.linkRecupera);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.cboEmpresa);
            this.panel1.Controls.Add(this.lblUsuario);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.lblPassword);
            this.panel1.Controls.Add(this.btnAceptar);
            this.panel1.Controls.Add(this.cboUsuario);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 218);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 148);
            this.panel1.TabIndex = 34;
            // 
            // LogoPicture
            // 
            this.LogoPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LogoPicture.BackColor = System.Drawing.Color.White;
            this.LogoPicture.Location = new System.Drawing.Point(15, 12);
            this.LogoPicture.Name = "LogoPicture";
            this.LogoPicture.Size = new System.Drawing.Size(264, 193);
            this.LogoPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.LogoPicture.TabIndex = 35;
            this.LogoPicture.TabStop = false;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCerrar;
            this.ClientSize = new System.Drawing.Size(297, 390);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.LogoPicture);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LogoBackGround);
            this.Controls.Add(this.Waiting);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inicio de Sesión";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInformacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Waiting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoBackGround)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected internal Telerik.WinControls.UI.RadTextBox txtPassword;
        protected internal Telerik.WinControls.UI.RadLabel lblEmpresa;
        protected internal Telerik.WinControls.UI.RadLabel lblUsuario;
        protected internal Telerik.WinControls.UI.RadLabel lblPassword;
        protected internal Telerik.WinControls.UI.RadAutoCompleteBox cboUsuario;
        protected internal Telerik.WinControls.UI.RadButton btnAceptar;
        protected internal Telerik.WinControls.UI.RadButton btnCerrar;
        protected internal Telerik.WinControls.UI.RadLabel lblInformacion;
        protected internal Telerik.WinControls.UI.RadWaitingBar Waiting;
        protected internal Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme;
        protected internal Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme;
        protected internal Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme;
        protected internal Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        protected internal Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement2;
        protected internal Telerik.WinControls.UI.RadDropDownList cboEmpresa;
        protected internal System.Windows.Forms.LinkLabel linkRecupera;
        private System.Windows.Forms.Panel panel1;
        protected internal System.Windows.Forms.PictureBox LogoPicture;
        protected internal System.Windows.Forms.PictureBox LogoBackGround;
    }
}
