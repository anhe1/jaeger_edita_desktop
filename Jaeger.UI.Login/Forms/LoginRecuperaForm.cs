﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.LockOn.Services.Restore;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Login.Forms {
    public partial class LoginRecuperaForm : Telerik.WinControls.UI.RadForm {
        private string rfc;
        private string correo;
        private string dominio;

        public LoginRecuperaForm(string rfc, string correo, string dominio) {
            InitializeComponent();
            this.rfc = rfc;
            this.correo = correo;
            this.dominio = dominio;
        }

        private void LoginRecuperaForm_Load(object sender, EventArgs e) {
            if (ValidacionService.IsMail(this.correo)) {
                this.txbCorreo.Text = this.correo;
            }
        }

        private void ButtonSolicitar_Click(object sender, EventArgs e) {
            IResetPasswordService rClient = new ResetPasswordService().WithDomain(this.dominio).WithMail(this.txbCorreo.Text);
            IResponse respuesta = rClient.Execute();

            if (respuesta != null) {
                if (respuesta.IsSuccess == true) {
                    RadMessageBox.Show(this, respuesta.Message.App[0], "Información", MessageBoxButtons.OK);
                }
            }
            this.Close();
        }
    }
}
