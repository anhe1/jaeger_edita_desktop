﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.LockOn.Interfaces;
using Jaeger.LockOn.Services;
using Jaeger.LockOn.Entities;

namespace Jaeger.UI.Login.Forms {
    public partial class LoginForm : RadForm {
        #region variables
        private BackgroundWorker consultar;
        private IRecienteModel _User;
        private IRecienteRepository _Recientes;
        private ILogin _Service = new LockOn.Services.Login();
        private string[] _Arguments;
        private bool _Success = false;
        #endregion

        public event EventHandler<ConfigService> Selected;
        public void OnSelected(ConfigService configuration) {
            if (this.Selected != null)
                this.Selected(this, configuration);
        }

        public LoginForm(string[] args) {
            InitializeComponent();
            this._Arguments = args;
        }

        private void LoginForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this._Recientes = new RecienteRepository().Load();

            using (ICommandService d1 = new CommandService()) {
                this._User = d1.Execute(this._Arguments);
            }

            this._Service = LoginBuilder.Create().Load().Connect();

            if (this._User == null) this._User = new RecienteModel();

            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += Consultar_DoWork;
            this.consultar.RunWorkerCompleted += Consultar_RunWorkerCompleted;

            this.cboEmpresa.DropDownListElement.TextBox.TextBoxItem.TextBoxControl.CharacterCasing = CharacterCasing.Upper;
            this.cboEmpresa.DisplayMember = "RFC";
            this.cboEmpresa.ValueMember = "RFC";
            this.cboEmpresa.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cboEmpresa.DropDownMaxSize = new Size(200, 0);
            this.cboEmpresa.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Collapsed;
            this.cboEmpresa.DataSource = this._Recientes.Items;
            this.cboEmpresa.DataBindings.Clear();
            this.cboEmpresa.DataBindings.Add("Text", this._User, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cboUsuario.DataBindings.Add("Text", this._User, "User", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtPassword.DataBindings.Add("Text", this._User, "Password", true, DataSourceUpdateMode.OnPropertyChanged);
            
            if (this._User.Password != null)
                this.btnAceptar.PerformClick();
        }

        private void cboUsuario_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter)
                this.txtPassword.Focus();
        }

        private void TxbPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter)
                this.btnAceptar.PerformClick();
        }

        private void TxbEmpresa_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {
            if (this.cboEmpresa.SelectedItem != null) {
                var reciente = this.cboEmpresa.SelectedItem.DataBoundItem as IRecienteModel;
                this.cboUsuario.Text = reciente.User;
            }
        }

        private void Button_Aceptar_Click(object sender, EventArgs e) {
            if (!ValidacionService.RFC(this._User.RFC)) {
                RadMessageBox.Show(this, "RFC no válido.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            if (this._User.User == null | this._User.Password == null | !ValidacionService.RFC(this._User.RFC)) {
                RadMessageBox.Show(this, "Ingresa RFC, usuario y contraseña para poder continuar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            this.cboEmpresa.Enabled = false;
            this.cboUsuario.Enabled = false;
            this.txtPassword.Enabled = false;
            this.btnAceptar.Enabled = false;
            this.btnCerrar.Enabled = false;
            this.Waiting.Visible = true;
            this.Waiting.StartWaiting();
            this.consultar.RunWorkerAsync();
        }

        private void Button_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
            this.Dispose();
        }

        private void Consultar_DoWork(object sender, DoWorkEventArgs e) {
            if (this._Service.Execute(this._User)) {
                this._Recientes.Add(this.cboEmpresa.Text, this.cboUsuario.Text);
                this._Recientes.Save();
                ConfigService.Synapsis = new Domain.Empresa.Entities.Configuracion {
                    RDS = new Domain.Empresa.Entities.BaseDatos()
                };
                ConfigService.Piloto = new Domain.Base.Profile.KaijuLogger();
                ConfigService.Synapsis = this._Service.Data;
                ConfigService.Piloto = this._Service.Piloto;
                this._Success = true;
            }
        }

        private void Consultar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (this._Success) {
                this.Hide();
                this.Close();
            }

            this.cboEmpresa.Enabled = true;
            this.cboUsuario.Enabled = true;
            this.txtPassword.Enabled = true;
            this.btnAceptar.Enabled = true;
            this.btnCerrar.Enabled = true;
            this.Waiting.Visible = false;
            this.Visible = true;

            if (this._Success == false) {
                if (!string.IsNullOrEmpty(this._Service.Message))
                    RadMessageBox.Show(this, this._Service.Message, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                this.linkRecupera.Visible = true;
            } else {
                this.OnSelected(null);
            }
        }

        /// <summary>
        /// establecer la imagen que se presenta en el formulario
        /// </summary>
        public Image LogoTipo {
            set { this.LogoPicture.Image = value; }
        }

        private void LinkRecupera_Click(object sender, EventArgs e) {
            var _recuperar = new LoginRecuperaForm(this.cboEmpresa.Text, this.cboUsuario.Text, this._Service.Dominio);
            _recuperar.ShowDialog(this);
        }
    }
}
