﻿namespace Jaeger.UI.Login.Forms {
    partial class LoginRecuperaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbCorreo = new Telerik.WinControls.UI.RadTextBox();
            this.buttonSolicitar = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txbCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSolicitar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // txbCorreo
            // 
            this.txbCorreo.Location = new System.Drawing.Point(12, 57);
            this.txbCorreo.Name = "txbCorreo";
            this.txbCorreo.NullText = "correo electrónico";
            this.txbCorreo.ShowClearButton = true;
            this.txbCorreo.ShowNullText = true;
            this.txbCorreo.Size = new System.Drawing.Size(329, 20);
            this.txbCorreo.TabIndex = 0;
            // 
            // buttonSolicitar
            // 
            this.buttonSolicitar.Location = new System.Drawing.Point(231, 83);
            this.buttonSolicitar.Name = "buttonSolicitar";
            this.buttonSolicitar.Size = new System.Drawing.Size(110, 24);
            this.buttonSolicitar.TabIndex = 1;
            this.buttonSolicitar.Text = "Recuperar";
            this.buttonSolicitar.Click += new System.EventHandler(this.ButtonSolicitar_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 33);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(250, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Inidica el correo electrónico de tu incio de sesión";
            // 
            // LoginRecuperaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 122);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.buttonSolicitar);
            this.Controls.Add(this.txbCorreo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginRecuperaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recuperar contraseña";
            this.Load += new System.EventHandler(this.LoginRecuperaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txbCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSolicitar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox txbCorreo;
        private Telerik.WinControls.UI.RadButton buttonSolicitar;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}
