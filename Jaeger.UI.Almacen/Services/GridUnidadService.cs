﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Almacen.Services {
    public static class GridUnidadService {
        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] {
                ColId, ColActivo, ColDescripion, ColFactor, ColClaveUnidad, ColCreo
            };
        }

        public static GridViewTextBoxColumn ColDescripion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Descripcion",
                    HeaderText = "Descripción",
                    Name = "Descripcion",
                    Width = 140,
                    MaxLength = 20
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveUnidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveUnidad",
                    HeaderText = "Clv. Unidad \r\nSAT",
                    Name = "ClaveUnidad",
                    Width = 80,
                    MaxLength = 3, MaxWidth = 95
                };
            }
        }

        public static GridViewTextBoxColumn ColId {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id"
                };
            }
        }

        public static GridViewCheckBoxColumn ColActivo {
            get {
                var activo = new GridViewCheckBoxColumn {
                    FieldName = "Activo",
                    HeaderText = "A",
                    IsVisible = true,
                    Name = "Activo",
                    Width = 50
                };

                activo.ConditionalFormattingObjectList.Add(new ConditionalFormattingObject {
                    ApplyToRow = true, Name = "RegistroActivo", RowBackColor = System.Drawing.Color.Gray, TValue1 = "false", TValue2 = "false"
                });
                return activo;
            }
        }

        public static GridViewTextBoxColumn ColCreo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Creo",
                    HeaderText = "Creo",
                    Name = "Creo",
                    Width = 85,
                    MaxLength = 10, 
                    MaxWidth = 85
                };
            }
        }

        public static GridViewCalculatorColumn ColFactor {
            get {
                return new GridViewCalculatorColumn {
                    DataType = typeof(decimal),
                    FieldName = "Factor",
                    HeaderText = "Factor",
                    Name = "Factor",
                    Width = 50
                };
            }
        }
    }
}
