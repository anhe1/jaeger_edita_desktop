﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public interface ICategoriaGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        ICategoriaTempletesGridBuilder Templetes();
    }

    public interface ICategoriaTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        ICategoriaTempletesGridBuilder Master();

        /// <summary>
        /// templete para mostrar categorias para la seleccion
        /// </summary>
        ICategoriaTempletesGridBuilder Categorias();
    }

    public interface ICategoriaColumnsGridBuilder : IGridViewColumnsBuild {
        ICategoriaColumnsGridBuilder IdCategoria(bool isVisible = false);
        ICategoriaColumnsGridBuilder IdSubCategoria(bool isVisible = false);
        ICategoriaColumnsGridBuilder IdClase(bool isVisible = false);
        ICategoriaColumnsGridBuilder Tipo();
        ICategoriaColumnsGridBuilder Categoria();
        ICategoriaColumnsGridBuilder Clase();
        ICategoriaColumnsGridBuilder Descriptor();
    }
}
