﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public class ProductoGridBuilder : GridViewBuilder, IProductoGridBuilder, IGridViewBuilder, IGridViewTempleteBuild, IProductoTempletesGridBuilder, IProductoColumnsGridBuilder,
        IGridViewColumnsBuild, IDisposable {

        public ProductoGridBuilder() : base() { }

        #region templetes
        public IProductoTempletesGridBuilder Templetes() {
            return this;
        }

        public IProductoTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.Codigo(true).IdCategoria().Nombre().Creo().FechaNuevo();
            return this;
        }

        /// <summary>
        /// templete utilizado para la vista de productos modelos sin precios
        /// </summary>
        public virtual IProductoTempletesGridBuilder ProductoModeloAlmacen() {
            this._Columns.Clear();
            this.Activo().IdCategoria(true).Producto();
            this._Columns.AddRange(new ModeloGridBuilder().Descripcion().Marca().Especificacion().Largo().Ancho().UnidadXY().Alto().UnidadZ().Minimo().Maximo().ReOrden().Existencia().IdUnidad().NoIdentificacion().Creo().FechaNuevo().Build());
            return this;
        }

        public IProductoTempletesGridBuilder Search() {
            this._Columns.Clear();
            this.Producto();
            this._Columns.AddRange(new ModeloGridBuilder().Descripcion().Especificacion().Marca().Unitario().Largo().Ancho().Alto().AutorizadoCheck().Build());
            return this;
        }

        public IProductoTempletesGridBuilder Modelos() {
            this._Columns.Clear();
            this._Columns.AddRange(new ModeloGridBuilder().Tipo().Activo().Codigo().Descripcion().Marca().Especificacion().Unitario().FactorTrasladoIVA().ValorTrasladoIVA().IdUnidad().AutorizadoText()
                .Largo().Ancho().UnidadXY().Alto().UnidadZ().ClaveUnidad().ClaveProdServ().NoIdentificacion().Creo().FechaNuevo().Build());
            return this;
        }

        public IProductoTempletesGridBuilder ModelosTienda() {
            this._Columns.Clear();
            this._Columns.AddRange(new ModeloGridBuilder().Tipo().Activo().Codigo().Descripcion().Marca().IdVisible().Unitario().FactorTrasladoIVA().ValorTrasladoIVA().IdUnidad().AutorizadoText()
                .Largo().Ancho().UnidadXY().Alto().UnidadZ().ClaveUnidad().ClaveProdServ().NoIdentificacion().Creo().FechaNuevo().Build());
            return this;
        }

        public IProductoTempletesGridBuilder ModelosImportar() {
            this._Columns.Clear();
            this._Columns.AddRange(new ModeloGridBuilder().Tipo().Build());
            this.IdProducto();
            this._Columns.AddRange(new ModeloGridBuilder().Descripcion().Marca().Especificacion().FactorTrasladoIVA().ValorTrasladoIVA().Unitario().IdUnidad().Largo().Ancho().UnidadXY().Codigo(true).ClaveUnidad().ClaveProdServ().Build());
            return this;
        }

        /// <summary>
        /// utilizado para informacion de secciones y publicaciones
        /// </summary>
        public IProductoTempletesGridBuilder ModelosImportar1() {
            this._Columns.Clear();
            this._Columns.AddRange(new ModeloGridBuilder().Tipo().IdCategoria().Secuencia().Descripcion().Marca().Especificacion().FactorTrasladoIVA().ValorTrasladoIVA().Unitario().IdUnidad().Codigo(false).NoIdentificacion().AutorizadoText().Build());
            return this;
        }

        public IProductoTempletesGridBuilder Proveedor() {
            this._Columns.Clear();
            //this.IdProveedor().Unitario().AddFechaNuevo().AddCreo();
            return this;
        }

        public IProductoTempletesGridBuilder Existencias() {
            this._Columns.Clear();
            this._Columns.AddRange(new ModeloGridBuilder().Tipo().Build());
            this.Activo().Nombre();
            this._Columns.AddRange(new ModeloGridBuilder().Descripcion().Marca().Especificacion().Variante().Minimo().Maximo().ReOrden().Existencia().Unidad().Codigo(true).NoIdentificacion().Build());
            return this;
        }
        #endregion

        #region columnas de categorias
        /// <summary>
        /// Categoria en modo multi combobox
        /// </summary>
        /// <param name="isVisible">visible</param>
        public IProductoColumnsGridBuilder IdCategoria(bool isVisible = true) {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                FieldName = "IdCategoria",
                HeaderText = "Categoría",
                Name = "IdCategoria",
                Width = 400,
                IsVisible = isVisible
            });
            return this;
        }
        #endregion

        #region columnas de productos
        /// <summary>
        /// Producto o Servicio en modo multicombobox
        /// </summary>
        public IProductoColumnsGridBuilder IdProducto() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                FieldName = "IdProducto",
                HeaderText = "Producto",
                Name = "IdProducto",
                Width = 275,
            });
            return this;
        }

        /// <summary>
        /// Producto o Servicio en modo de caja de texto, es solo para el indice
        /// </summary>
        public IProductoColumnsGridBuilder IndiceProducto() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                FieldName = "IdProducto",
                HeaderText = "ID Producto",
                Name = "IdProducto",
                Width = 275,
                IsVisible = false,
                VisibleInColumnChooser = false,
                DataType = typeof(int),
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        /// <summary>
        /// Producto o Servicio en modo texto
        /// </summary>
        public IProductoColumnsGridBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Producto o Servicio",
                Name = "Nombre",
                Width = 350
            });
            return this;
        }

        public IProductoColumnsGridBuilder Producto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Producto",
                Name = "Nombre",
                Width = 200
            });
            return this;
        }

        public IProductoColumnsGridBuilder Activo() {
            var expresion = new ExpressionFormattingObject {
                ApplyToRow = true,
                CellBackColor = Color.Empty,
                CellForeColor = Color.Empty,
                Expression = "Activo = false",
                Name = "Registro Activo",
                RowBackColor = Color.Empty,
                RowFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Italic, GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = Color.Gray
            };

            var activo = new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                IsVisible = false,
                Name = "Activo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 40
            };
            activo.ConditionalFormattingObjectList.Add(expresion);
            this._Columns.Add(activo);
            return this;
        }
        #endregion

        public IProductoColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IProductoColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IProductoColumnsGridBuilder Codigo(bool visible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Codigo",
                HeaderText = "Codigo",
                IsVisible = visible,
                Name = "Codigo",
                Width = 100,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        #region para existencias
        public IProductoColumnsGridBuilder TotalEntrada() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "TotalEntrada",
                HeaderText = "Total\r\nEntrada",
                Name = "TotalEntrada",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IProductoColumnsGridBuilder TotalSalida() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "TotalSalida",
                HeaderText = "Total\r\nSalidas",
                Name = "TotalSalida",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }
        #endregion

        #region formato condicional
        /// <summary>
        /// formato condicional para el estado del comprobante
        /// </summary>
        /// <returns>ExpressionFormattingObject</returns>
        public static ExpressionFormattingObject ConditionalNegativo() {
            return new ExpressionFormattingObject("Negativo", "Existencia < 0", true) {
                RowForeColor = Color.FromArgb(255, 125, 10),
                CellForeColor = Color.Red
            };
        }
        #endregion
    }
}
