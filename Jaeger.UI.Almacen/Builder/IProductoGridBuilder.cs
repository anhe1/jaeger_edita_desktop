﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public interface IProductoGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IProductoTempletesGridBuilder Templetes();
    }

    public interface IProductoTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IProductoTempletesGridBuilder Master();
        IProductoTempletesGridBuilder ProductoModeloAlmacen();
        IProductoTempletesGridBuilder Search();
        IProductoTempletesGridBuilder Modelos();
        
        IProductoTempletesGridBuilder ModelosTienda();
        IProductoTempletesGridBuilder Proveedor();
        IProductoTempletesGridBuilder Existencias();
        IProductoTempletesGridBuilder ModelosImportar();
        IProductoTempletesGridBuilder ModelosImportar1();
    }

    public interface IProductoColumnsGridBuilder : IGridViewColumnsBuild {
        IProductoColumnsGridBuilder IdCategoria(bool isVisible = true);

        IProductoColumnsGridBuilder IdProducto();
        
        IProductoColumnsGridBuilder Nombre();

        IProductoColumnsGridBuilder Activo();

        IProductoColumnsGridBuilder Codigo(bool visible = false);

        IProductoColumnsGridBuilder Producto();

        IProductoColumnsGridBuilder Creo();

        IProductoColumnsGridBuilder FechaNuevo();
    }
}
