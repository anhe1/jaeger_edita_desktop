﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public class UnidadGridBuilder : GridViewBuilder, IUnidadGridBuilder, IGridViewBuilder, IUnidadTempletesGridBuilder, IGridViewTempleteBuild, IUnidadColumnsGridBuilder, IGridViewColumnsBuild, IDisposable {
        public UnidadGridBuilder() : base() {

        }

        #region templetes
        public IUnidadTempletesGridBuilder Templetes() {
            return this;
        }

        public IUnidadTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.ColId().ColActivo().ColDescripion().ColFactor().ColClaveUnidad().ColCreo();
            return this;
        }
        #endregion

        #region columnas
        public IUnidadColumnsGridBuilder ColDescripion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                Width = 140,
                MaxLength = 20
            });
            return this;
        }

        public IUnidadColumnsGridBuilder ColClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "Clv. Unidad \r\nSAT",
                Name = "ClaveUnidad",
                Width = 80,
                MaxLength = 3,
                MaxWidth = 95
            });
            return this;
        }

        public IUnidadColumnsGridBuilder ColId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id"
            });
            return this;
        }

        public IUnidadColumnsGridBuilder ColActivo() {
            var activo = new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                IsVisible = true,
                Name = "Activo",
                Width = 50
            };

            activo.ConditionalFormattingObjectList.Add(new ConditionalFormattingObject {
                ApplyToRow = true, Name = "RegistroActivo", 
                RowBackColor = System.Drawing.Color.Gray,
                TValue1 = "false", 
                TValue2 = "false"
            });
            this._Columns.Add(activo);
            return this;
        }

        public IUnidadColumnsGridBuilder ColCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                Width = 85,
                MaxLength = 10,
                MaxWidth = 85
            });
            return this;
        }

        public IUnidadColumnsGridBuilder ColFactor() {
            this._Columns.Add(new GridViewCalculatorColumn {
                DataType = typeof(decimal),
                FieldName = "Factor",
                HeaderText = "Factor",
                Name = "Factor",
                Width = 50
            });
            return this;
        }
        #endregion
    }
}
