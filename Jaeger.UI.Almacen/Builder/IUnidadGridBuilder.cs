﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public interface IUnidadGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IUnidadTempletesGridBuilder Templetes();
    }

    public interface IUnidadTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IUnidadTempletesGridBuilder Master();
    }

    public interface IUnidadColumnsGridBuilder : IGridViewColumnsBuild {
        IUnidadColumnsGridBuilder ColId();

        IUnidadColumnsGridBuilder ColActivo();

        IUnidadColumnsGridBuilder ColDescripion();

        IUnidadColumnsGridBuilder ColClaveUnidad();

        IUnidadColumnsGridBuilder ColCreo();

        IUnidadColumnsGridBuilder ColFactor();
    }
}
