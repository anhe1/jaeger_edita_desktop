﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public interface IModeloGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IModeloTempletesGridBuilder Templetes();
    }

    public interface IModeloTempletesGridBuilder : IGridViewTempleteBuild {
        IModeloTempletesGridBuilder Master();
    }

    public interface IModeloColumnsGridBuilder : IGridViewColumnsBuild, IGridViewBuilder {
        IModeloColumnsGridBuilder IdCategoria(bool isVisible = false);
        IModeloColumnsGridBuilder IdModelo(bool isVisible = false);
        #region modelos
        IModeloColumnsGridBuilder Activo();

        IModeloColumnsGridBuilder Tipo();

        IModeloColumnsGridBuilder Codigo(bool visible = false);

        /// <summary>
        /// nombre del modelo o descripcion del modelo
        /// </summary>
        IModeloColumnsGridBuilder Descripcion();

        /// <summary>
        /// Marca
        /// </summary>
        IModeloColumnsGridBuilder Marca();

        /// <summary>
        /// Especificacion del modelo
        /// </summary>
        IModeloColumnsGridBuilder Especificacion();

        /// <summary>
        /// Unidad base
        /// </summary>
        IModeloColumnsGridBuilder Unidad();

        IModeloColumnsGridBuilder Unitario();

        IModeloColumnsGridBuilder FactorTrasladoIVA();

        IModeloColumnsGridBuilder ValorTrasladoIVA();

        /// <summary>
        /// columna de boton para autorizacion del modelo
        /// </summary>
        IModeloColumnsGridBuilder AutorizadoText();

        /// <summary>
        /// columna de checkbox para autorizacion del modelo
        /// </summary>
        IModeloColumnsGridBuilder AutorizadoCheck();

        IModeloColumnsGridBuilder UnidadXY();

        IModeloColumnsGridBuilder UnidadZ();

        IModeloColumnsGridBuilder IdUnidad();

        IModeloColumnsGridBuilder Minimo();

        IModeloColumnsGridBuilder Maximo();

        IModeloColumnsGridBuilder ReOrden();

        IModeloColumnsGridBuilder Existencia();

        IModeloColumnsGridBuilder Largo();

        IModeloColumnsGridBuilder Ancho();

        IModeloColumnsGridBuilder Alto();

        IModeloColumnsGridBuilder ClaveUnidad();

        IModeloColumnsGridBuilder ClaveProdServ();

        IModeloColumnsGridBuilder NoIdentificacion();

        /// <summary>
        /// variante del modelo, utilizado en la tienda
        /// </summary>
        IModeloColumnsGridBuilder Variante();

        IModeloColumnsGridBuilder Secuencia();
        
        IModeloColumnsGridBuilder IdVisible();

        IModeloColumnsGridBuilder Contenido();

        IModeloColumnsGridBuilder CtaPredial();

        IModeloColumnsGridBuilder NumRequerimiento();

        IModeloColumnsGridBuilder Creo();

        IModeloColumnsGridBuilder FechaNuevo();
        #endregion
    }
}
