﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public class ModeloGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IModeloGridBuilder, IModeloTempletesGridBuilder, IModeloColumnsGridBuilder {
        public ModeloGridBuilder() : base() { }

        #region templetes
        public IModeloTempletesGridBuilder Templetes() { return this; }

        public IModeloTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.IdModelo().Tipo().Activo().Codigo().Descripcion().Marca().Especificacion().Unitario().FactorTrasladoIVA().ValorTrasladoIVA().IdUnidad().AutorizadoText()
                .Largo().Ancho().UnidadXY().Alto().UnidadZ().ClaveUnidad().ClaveProdServ().NoIdentificacion().Creo().FechaNuevo();
            return this;
        }
        #endregion

        #region columnas para modelos
        /// <summary>
        /// columna indice de la categoria
        /// </summary>    
        public IModeloColumnsGridBuilder IdCategoria(bool isVisible = false) {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdCategoria",
                HeaderText = "IdCategoria",
                Name = "IdCategoria",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleRight,
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible
            });
            return this;
        }

        /// <summary>
        /// columna indice del modelo
        /// </summary>
        public IModeloColumnsGridBuilder IdModelo(bool isVisible = false) {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdModelo",
                HeaderText = "IdModelo",
                Name = "IdModelo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleRight,
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible
            });
            return this;
        }

        /// <summary>
        /// CheckBoxColumna para registro activo
        /// </summary>
        public virtual IModeloColumnsGridBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }

        /// <summary>
        /// columna tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). Es utilizado en CFDI para hacer la distinción de producto o servicio.
        /// </summary>
        public IModeloColumnsGridBuilder Tipo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 85
            });
            return this;
        }

        public IModeloColumnsGridBuilder Codigo(bool visible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Codigo",
                HeaderText = "Codigo",
                IsVisible = visible,
                Name = "Codigo",
                Width = 100,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// modelo o descripcion del modelo
        /// </summary>
        public IModeloColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                Width = 200
            });
            return this;
        }

        /// <summary>
        /// columna marca del modelo
        /// </summary>
        public IModeloColumnsGridBuilder Marca() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Marca",
                HeaderText = "Marca",
                Name = "Marca",
                Width = 200
            });
            return this;
        }

        public IModeloColumnsGridBuilder Especificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Especificacion",
                HeaderText = "Especificación",
                Name = "Especificacion",
                Width = 150
            });
            return this;
        }

        /// <summary>
        /// columna unidad personalizada
        /// </summary>
        public IModeloColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                Width = 95
            });
            return this;
        }

        public IModeloColumnsGridBuilder Unitario() {
            this._Columns.Add(new GridViewCalculatorColumn {
                FieldName = "Unitario",
                HeaderText = "Unitario",
                Name = "Unitario",
                Width = 75,
                FormatString = this.FormatStringMoney
            });
            return this;
        }

        public IModeloColumnsGridBuilder FactorTrasladoIVA() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "FactorTrasladoIVA",
                HeaderText = "Fac. IVA",
                Name = "FactorTrasladoIVA",
                Width = 65,
                FormatString = this.FormatStringP
            });
            return this;
        }

        public IModeloColumnsGridBuilder ValorTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ValorTrasladoIVA",
                HeaderText = "% IVA",
                Name = "ValorTrasladoIVA",
                FormatString = this.FormatStringP,
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        /// <summary>
        /// columna de boton para autorizacion del modelo
        /// </summary>
        public IModeloColumnsGridBuilder AutorizadoText() {
            this._Columns.Add(new GridViewCommandColumn {
                FieldName = "AutorizadoText",
                HeaderText = "Autorizado",
                Name = "AutorizadoText",
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// columna de checkbox para autorizacion del modelo
        /// </summary>
        public IModeloColumnsGridBuilder AutorizadoCheck() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Autorizado",
                HeaderText = "Autorizado",
                Name = "Autorizado",
                Width = 50
            });
            return this;
        }

        public IModeloColumnsGridBuilder UnidadXY() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "UnidadXY",
                HeaderText = "Unidad",
                Name = "UnidadXY",
                Width = 75
            });
            return this;
        }

        public IModeloColumnsGridBuilder UnidadZ() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "UnidadZ",
                HeaderText = "Unidad",
                Name = "UnidadZ",
                Width = 75
            });
            return this;
        }

        public IModeloColumnsGridBuilder IdUnidad() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdUnidad",
                HeaderText = "Unidad",
                Name = "IdUnidad",
                Width = 75
            });
            return this;
        }

        public IModeloColumnsGridBuilder Minimo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Minimo",
                HeaderText = "Minimo",
                Name = "Minimo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IModeloColumnsGridBuilder Maximo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Maximo",
                HeaderText = "Maximo",
                Name = "Maximo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65,
                FormatString = this.FormatStringNumber
            });
            return this;
        }

        public IModeloColumnsGridBuilder ReOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ReOrden",
                HeaderText = "ReOrden",
                Name = "ReOrden",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65,
                FormatString = this.FormatStringNumber
            });
            return this;
        }

        public IModeloColumnsGridBuilder Existencia() {
            var existencia = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Existencia",
                HeaderText = "Existencia",
                Name = "Existencia",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65,
                FormatString = this.FormatStringNumber
            };
            var formato = existencia.ConditionalFormattingObjectList;
            formato.Add(ConditionalNegativo());
            this._Columns.Add(existencia);
            return this;
        }

        public IModeloColumnsGridBuilder Largo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Largo",
                HeaderText = "Largo",
                Name = "Largo",
                TextAlignment = ContentAlignment.MiddleRight,
                FormatString = this.FormatStringNumber,
                Width = 65
            });
            return this;
        }

        public IModeloColumnsGridBuilder Ancho() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Ancho",
                HeaderText = "Ancho",
                Name = "Ancho",
                TextAlignment = ContentAlignment.MiddleRight,
                FormatString = this.FormatStringNumber,
                Width = 65
            });
            return this;
        }

        public IModeloColumnsGridBuilder Alto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Alto",
                HeaderText = "Alto",
                Name = "Alto",
                TextAlignment = ContentAlignment.MiddleRight,
                FormatString = this.FormatStringNumber,
                Width = 65
            });
            return this;
        }

        public IModeloColumnsGridBuilder Secuencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Secuencia",
                HeaderText = "Secuencia",
                Name = "Secuencia",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public IModeloColumnsGridBuilder ClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "ClaveUnidad",
                IsVisible = false,
                Name = "ClaveUnidad"
            });
            return this;
        }

        public IModeloColumnsGridBuilder ClaveProdServ() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveProdServ",
                HeaderText = "ClaveProdServ",
                IsVisible = false,
                Name = "ClaveProdServ"
            });
            return this;
        }

        public IModeloColumnsGridBuilder NoIdentificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoIdentificacion",
                HeaderText = "NoIdentificacion",
                Name = "NoIdentificacion",
                Width = 120
            });
            return this;
        }

        public IModeloColumnsGridBuilder Variante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Variante",
                HeaderText = "Variante",
                Name = "Variante",
                Width = 150
            });
            return this;
        }

        public IModeloColumnsGridBuilder IdVisible() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdVisible",
                HeaderText = "Visibilidad",
                Name = "IdVisible",
                Width = 110
            });
            return this;
        }

        public IModeloColumnsGridBuilder Contenido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Contenido",
                HeaderText = "Contenido",
                Name = "Publicacion.Contenido",
                Width = 200,
                Multiline = true
            });
            return this;
        }

        public IModeloColumnsGridBuilder CtaPredial() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CtaPredial",
                HeaderText = "Cta. Predial",
                Name = "CtaPredial",
                Width = 85
            });
            return this;
        }

        public IModeloColumnsGridBuilder NumRequerimiento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumRequerimiento",
                HeaderText = "Núm Req.",
                Name = "NumRequerimiento",
                Width = 85
            });
            return this;
        }

        public IModeloColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IModeloColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        #endregion

        #region formato condicional
        /// <summary>
        /// formato condicional para el estado del comprobante
        /// </summary>
        /// <returns>ExpressionFormattingObject</returns>
        public static ExpressionFormattingObject ConditionalNegativo() {
            return new ExpressionFormattingObject("Negativo", "Existencia < 0", true) {
                RowForeColor = Color.FromArgb(255, 125, 10),
                CellForeColor = Color.Red
            };
        }
        #endregion
    }
}
