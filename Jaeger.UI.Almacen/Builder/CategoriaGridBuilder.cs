﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.Builder {
    public class CategoriaGridBuilder : GridViewBuilder, ICategoriaGridBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IGridViewColumnsBuild, ICategoriaTempletesGridBuilder, 
        ICategoriaColumnsGridBuilder {

        public CategoriaGridBuilder() : base() { }

        #region templetes
        public ICategoriaTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public ICategoriaTempletesGridBuilder Master() {
            this.Codigo().IdCategoria().IdSubCategoria().IdClase().Tipo().Categoria().Clase().Descriptor();
            return this;
        }

        public ICategoriaTempletesGridBuilder Categorias() {
            this.Codigo().IdCategoria().IdSubCategoria().IdClase().Tipo().Categoria().Clase().Descriptor();
            return this;
        }
        #endregion

        #region columnas
        public ICategoriaColumnsGridBuilder IdCategoria(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdCategoria",
                HeaderText = "IdCategoria",
                Name = "IdCategoria",
                Width = 50,
                FormatString = this.FormarStringFolio,
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder IdSubCategoria(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdSubCategoria",
                HeaderText = "IdSubCategoria",
                Name = "IdSubCategoria",
                Width = 50,
                FormatString = this.FormarStringFolio,
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder IdClase(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdClase",
                HeaderText = "IdClase",
                Name = "IdClase",
                Width = 50,
                FormatString = this.FormarStringFolio,
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder Tipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 150
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder Categoria() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Categoría",
                Name = "Descripcion",
                Width = 140,
                MaxLength = 20
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder Clase() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clase",
                HeaderText = "Clase",
                Name = "Clase",
                Width = 140,
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder Descriptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descriptor",
                HeaderText = "Descriptor",
                Name = "Desciptor",
                Width = 140,
            });
            return this;
        }

        public ICategoriaColumnsGridBuilder Codigo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Codigo",
                HeaderText = "Código",
                Name = "Codigo",
                Width = 85,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }
        #endregion
    }
}
