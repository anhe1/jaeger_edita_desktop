﻿namespace Jaeger.UI.Almacen.Forms {
    partial class ProductoExistencia2Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TExistencia = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.GridData = new Telerik.WinControls.UI.RadPivotGrid();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TExistencia
            // 
            this.TExistencia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TExistencia.Location = new System.Drawing.Point(0, 0);
            this.TExistencia.Name = "TExistencia";
            this.TExistencia.ShowActualizar = true;
            this.TExistencia.ShowAutosuma = false;
            this.TExistencia.ShowCancelar = false;
            this.TExistencia.ShowCerrar = true;
            this.TExistencia.ShowEditar = true;
            this.TExistencia.ShowEjercicio = true;
            this.TExistencia.ShowExportarExcel = false;
            this.TExistencia.ShowFiltro = true;
            this.TExistencia.ShowHerramientas = false;
            this.TExistencia.ShowImprimir = false;
            this.TExistencia.ShowItem = false;
            this.TExistencia.ShowNuevo = true;
            this.TExistencia.ShowPeriodo = true;
            this.TExistencia.Size = new System.Drawing.Size(800, 30);
            this.TExistencia.TabIndex = 0;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(800, 420);
            this.GridData.TabIndex = 1;
            // 
            // ProductoExistencia2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TExistencia);
            this.Name = "ProductoExistencia2Form";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ProductoExistencia2Form";
            this.Load += new System.EventHandler(this.ProductoExistencia2Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.ToolBarCommonControl TExistencia;
        protected internal Telerik.WinControls.UI.RadPivotGrid GridData;
    }
}