﻿namespace Jaeger.UI.Almacen.Forms {
    partial class EspecificacionCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TEspecificacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TEspecificacion
            // 
            this.TEspecificacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TEspecificacion.Etiqueta = "";
            this.TEspecificacion.Location = new System.Drawing.Point(0, 0);
            this.TEspecificacion.Name = "TEspecificacion";
            this.TEspecificacion.ReadOnly = false;
            this.TEspecificacion.ShowActualizar = true;
            this.TEspecificacion.ShowAutorizar = false;
            this.TEspecificacion.ShowCerrar = true;
            this.TEspecificacion.ShowEditar = false;
            this.TEspecificacion.ShowExportarExcel = false;
            this.TEspecificacion.ShowFiltro = true;
            this.TEspecificacion.ShowGuardar = true;
            this.TEspecificacion.ShowHerramientas = true;
            this.TEspecificacion.ShowImagen = false;
            this.TEspecificacion.ShowImprimir = false;
            this.TEspecificacion.ShowNuevo = true;
            this.TEspecificacion.ShowRemover = true;
            this.TEspecificacion.Size = new System.Drawing.Size(560, 30);
            this.TEspecificacion.TabIndex = 14;
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridData.MasterTemplate.AllowAddNewRow = false;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn1.FieldName = "Descripcion";
            gridViewTextBoxColumn1.HeaderText = "Descripción";
            gridViewTextBoxColumn1.MaxLength = 50;
            gridViewTextBoxColumn1.Name = "Descripcion";
            gridViewTextBoxColumn1.Width = 200;
            gridViewTextBoxColumn2.FieldName = "Nota";
            gridViewTextBoxColumn2.HeaderText = "Nota";
            gridViewTextBoxColumn2.MaxLength = 58;
            gridViewTextBoxColumn2.Name = "Nota";
            gridViewTextBoxColumn2.Width = 150;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.FieldName = "FechaNuevo";
            gridViewTextBoxColumn3.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn3.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "FechaNuevo";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "Creo";
            gridViewTextBoxColumn4.HeaderText = "Creó";
            gridViewTextBoxColumn4.Name = "Creo";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 65;
            this.gridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.gridData.MasterTemplate.EnableFiltering = true;
            this.gridData.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor1.PropertyName = "Inicio";
            this.gridData.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridData.Name = "gridData";
            this.gridData.ShowGroupPanel = false;
            this.gridData.Size = new System.Drawing.Size(560, 418);
            this.gridData.TabIndex = 16;
            this.gridData.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridData_CellBeginEdit);
            this.gridData.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellEndEdit);
            // 
            // EspecificacionCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 448);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.TEspecificacion);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EspecificacionCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CPLite: Especificación";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EspecificacionCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TEspecificacion;
        private Telerik.WinControls.UI.RadGridView gridData;
    }
}