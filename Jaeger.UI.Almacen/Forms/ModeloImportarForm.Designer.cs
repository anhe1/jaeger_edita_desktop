﻿namespace Jaeger.UI.Almacen.Forms {
    partial class ModeloImportarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TControl = new Jaeger.UI.Almacen.Forms.ModeloGridControl();
            this.TStatus = new Telerik.WinControls.UI.RadStatusStrip();
            this.LStatus = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.TStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TControl
            // 
            this.TControl.Caption = "";
            this.TControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TControl.Location = new System.Drawing.Point(0, 0);
            this.TControl.Name = "TControl";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TControl.Permisos = uiAction1;
            this.TControl.ShowActualizar = true;
            this.TControl.ShowAutorizar = false;
            this.TControl.ShowAutosuma = false;
            this.TControl.ShowCerrar = true;
            this.TControl.ShowEditar = true;
            this.TControl.ShowExportarExcel = false;
            this.TControl.ShowFiltro = true;
            this.TControl.ShowHerramientas = false;
            this.TControl.ShowImagen = false;
            this.TControl.ShowImprimir = false;
            this.TControl.ShowNuevo = true;
            this.TControl.ShowRemover = true;
            this.TControl.ShowSeleccionMultiple = true;
            this.TControl.Size = new System.Drawing.Size(800, 424);
            this.TControl.TabIndex = 0;
            // 
            // TStatus
            // 
            this.TStatus.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LStatus});
            this.TStatus.Location = new System.Drawing.Point(0, 424);
            this.TStatus.Name = "TStatus";
            this.TStatus.Size = new System.Drawing.Size(800, 26);
            this.TStatus.SizingGrip = false;
            this.TStatus.TabIndex = 2;
            // 
            // LStatus
            // 
            this.LStatus.Name = "LStatus";
            this.TStatus.SetSpring(this.LStatus, false);
            this.LStatus.Text = "Esperando ...";
            this.LStatus.TextWrap = true;
            // 
            // ModeloImportarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TControl);
            this.Controls.Add(this.TStatus);
            this.Name = "ModeloImportarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Importación";
            this.Load += new System.EventHandler(this.ModeloImportarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal ModeloGridControl TControl;
        private Telerik.WinControls.UI.RadStatusStrip TStatus;
        private Telerik.WinControls.UI.RadLabelElement LStatus;
    }
}