﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Almacen.Builder;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoModeloBuscarForm : RadForm {
        #region declaraciones
        private List<ProductoServicioModeloModel> _SourceData;
        protected ICatalogoProductoService _Service;
        protected AlmacenEnum _Almacen;
        protected internal bool _AuthorizedOnly;
        private BackgroundWorker _Consultar;
        #endregion

        public event EventHandler<ProductoServicioModeloModel> Seleccionar;
        public void OnSeleccionar(ProductoServicioModeloModel e) {
            if (this.Seleccionar != null)
                this.Seleccionar(this, e);
        }

        public ProductoModeloBuscarForm(AlmacenEnum almacen, bool stockOnly = false, bool authorizedOnly = true) {
            InitializeComponent();
            this.TBuscar.cExistencia.Checked = stockOnly;
            this._Almacen = almacen;
            this._AuthorizedOnly = authorizedOnly;
        }

        private void ProductoModeloBuscarForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this._Service = new CatalogoProductoService(AlmacenEnum.MP);
            this.GridData.Standard();
            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Search().Build());
            }
            this._Consultar = new BackgroundWorker();
            this._Consultar.DoWork += Consultar_DoWork;
            this._Consultar.RunWorkerCompleted += Consultar_RunWorkerCompleted;
            this.TBuscar.Descripcion.TextChanged += TBuscar_TextChanged;
        }

        private void TBuscar_TextChanged(object sender, EventArgs e) {
            if (this.TBuscar.Buscar.Text.Length > 3) {
                if (_Consultar.IsBusy == false) {
                    this.Espera.Visible = true;
                    this.Espera.StartWaiting();
                    _Consultar.RunWorkerAsync();
                }
            }
        }

        private void TBuscar_Seleccionar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var producto = this.GridData.CurrentRow.DataBoundItem as ProductoServicioModeloModel;
                if (producto != null) {
                    if (producto.Autorizado == 0 && this._AuthorizedOnly) {
                        RadMessageBox.Show(this, Properties.Resources.Message_Error_ProduductoNoAutorizado, "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error);
                    } else {
                        this.OnSeleccionar(producto);
                    }
                }
            }
        }

        private void TBuscar_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.TBuscar.Filtro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void TBuscar_Buscar_Click(object sender, EventArgs e) {
            if (this.TBuscar.Buscar.Text.Length > 3) {
                if (_Consultar.IsBusy == false) {
                    this.Espera.Visible = true;
                    this.Espera.StartWaiting();
                    _Consultar.RunWorkerAsync();
                }
            }
        }

        private void TBuscar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void GridData_DoubleClick(object sender, EventArgs e) {
            this.TBuscar.Agregar.PerformClick();
        }

        private void Consultar_DoWork(object sender, DoWorkEventArgs e) {
            var d0 = CatalogoModeloService.Query().ByIdAlmacen(this._Service.Almacen).Activo().Search(this.TBuscar.Descripcion.Text).OnlyAuthorized().Build();
            this._SourceData = this._Service.GetList<ProductoServicioModeloModel>(d0).ToList();
        }

        private void Consultar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = this._SourceData;
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
        }
    }
}
