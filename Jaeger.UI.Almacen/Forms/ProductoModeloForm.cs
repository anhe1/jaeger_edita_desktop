﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoModeloForm : RadForm {
        protected internal ModeloDetailModel _Modelo;

        public ProductoModeloForm() {
            InitializeComponent();
        }

        public virtual void ProductoModeloForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridProveedor.Standard();
            this.cboCategorias.EditorControl.EnableAlternatingRowColor = true;
            this.cboCategorias.EditorControl.AllowRowResize = false;
            this.cboCategorias.EditorControl.AllowSearchRow = true;
            this.cboCategorias.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.cboCategorias.MultiColumnComboBoxElement.DropDownSizingMode = SizingMode.UpDownAndRightBottom;

            this.cboTipo.DataSource = CatalogoProductoService.GetTipos();
            this.TProducto.Guardar.Click += this.TModelo_Guardar_Click;
            this.TProducto.Cerrar.Click += this.TModelo_Cerrar_Click;
        }

        public virtual void TModelo_Guardar_Click(object sender, EventArgs eventArgs) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Existen algunos errores de captura, por favor verifica.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        public virtual void TModelo_Cerrar_Click(object sender, EventArgs eventArgs) {
            this.Close();
        }

        /// <summary>
        /// asegurar que la categoria seleccionada no contenga mas niveles, siempre seleccionar el ultimo nivel
        /// </summary>
        protected virtual void CboCategorias_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.cboCategorias.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var nivel = (int)temporal.Cells["Childs"].Value;
                if (nivel > 0) { this.errorProvider1.SetError(this.cboCategorias, "Categoría no válida."); } else { 
                    this.errorProvider1.Clear();
                    this._Modelo.IdCategoria = (int)temporal.Cells["IdCategoria"].Value;
                }
            }
        }

        public virtual void CreateBinding() {
            this.cboCategorias.DataBindings.Clear();
            this.cboCategorias.DataBindings.Add("SelectedValue", this._Modelo, "IdProducto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbModelo.DataBindings.Clear();
            this.txbModelo.DataBindings.Add("Text", this._Modelo, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbMarca.DataBindings.Clear();
            this.txbMarca.DataBindings.Add("Text", this._Modelo, "Marca", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbExpecificacion.DataBindings.Clear();
            this.txbExpecificacion.DataBindings.Add("Text", this._Modelo, "Especificacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTipo.DataBindings.Clear();
            this.cboTipo.DataBindings.Add("SelectedValue", this._Modelo, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbCodigoBarras.DataBindings.Clear();
            this.txbCodigoBarras.DataBindings.Add("Text", this._Modelo, "Codigo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbEtiquetas.DataBindings.Clear();
            this.txbEtiquetas.DataBindings.Add("Text", this._Modelo, "Etiquetas", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbLargo.DataBindings.Clear();
            this.txbLargo.DataBindings.Add("Text", this._Modelo, "Largo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbAncho.DataBindings.Clear();
            this.txbAncho.DataBindings.Add("Text", this._Modelo, "Ancho", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbAlto.DataBindings.Clear();
            this.txbAlto.DataBindings.Add("Text", this._Modelo, "Alto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbPrecioBase.DataBindings.Clear();
            this.txbPrecioBase.DataBindings.Add("Value", this._Modelo, "Unitario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockMinimo.DataBindings.Clear();
            this.TxbStockMinimo.DataBindings.Add("Text", this._Modelo, "Minimo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockMaximo.DataBindings.Clear();
            this.TxbStockMaximo.DataBindings.Add("Text", this._Modelo, "Maximo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockReorden.DataBindings.Clear();
            this.TxbStockReorden.DataBindings.Add("Text", this._Modelo, "ReOrden", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbExistencia.DataBindings.Clear();
            this.TxbExistencia.DataBindings.Add("Text", this._Modelo, "Existencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboUnidadAlmacen.DataBindings.Clear();
            this.CboUnidadAlmacen.DataBindings.Add("Text", this._Modelo, "Unidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTrasladoIVA.DataBindings.Clear();
            this.cboTrasladoIVA.DataBindings.Add("Text", this._Modelo, "FactorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbTrasladoIVA.DataBindings.Clear();
            this.txbTrasladoIVA.DataBindings.Add("Value", this._Modelo, "ValorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboUnidadXY.DataBindings.Clear();
            this.cboUnidadXY.DataBindings.Add("SelectedValue", this._Modelo, "UnidadXY", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboUnidadZ.DataBindings.Clear();
            this.cboUnidadZ.DataBindings.Add("SelectedValue", this._Modelo, "UnidadZ", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public virtual void Guardar() {

        }

        public virtual bool Validar() {
            this.errorProvider1.Clear();
            if (_Modelo.IdProducto <= 0) {
                this.errorProvider1.SetError(this.cboCategorias, "Selecciona un producto de la lista.");
                return false;
            }

            if (string.IsNullOrEmpty(this._Modelo.Descripcion)) {
                this.errorProvider1.SetError(this.txbModelo, "Es necesario una descripción del modelo");
                return false;
            }

            if (string.IsNullOrEmpty(this._Modelo.Unidad)) {
                this.errorProvider1.SetError(this.CboUnidadAlmacen, "Selecciona una unidad para el almacén");
                return false;
            }
            return true;
        }
    }
}
