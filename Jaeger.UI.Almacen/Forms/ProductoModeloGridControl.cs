﻿using Jaeger.UI.Almacen.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.Forms {
    public class ProductoModeloGridControl : Common.Forms.GridStandarControl {
        public ProductoModeloGridControl() : base() {
            this.Load += ProductoModeloGridControl_Load;
        }

        private void ProductoModeloGridControl_Load(object sender, System.EventArgs e) {
            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.GridData.Standard();
                this.GridData.Columns.AddRange(view.Templetes().ProductoModeloAlmacen().Build());
            }
        }
    }
}
