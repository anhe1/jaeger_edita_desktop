﻿namespace Jaeger.UI.Almacen.Forms {
    partial class ProductoExistenciaTDForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TExistencia = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.radGridData = new Telerik.WinControls.UI.RadPivotGrid();
            this.Almacen = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.radGridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TExistencia
            // 
            this.TExistencia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TExistencia.Location = new System.Drawing.Point(0, 0);
            this.TExistencia.Name = "TExistencia";
            this.TExistencia.ShowActualizar = true;
            this.TExistencia.ShowAutosuma = false;
            this.TExistencia.ShowCancelar = false;
            this.TExistencia.ShowCerrar = true;
            this.TExistencia.ShowEditar = false;
            this.TExistencia.ShowEjercicio = false;
            this.TExistencia.ShowExportarExcel = true;
            this.TExistencia.ShowFiltro = false;
            this.TExistencia.ShowHerramientas = true;
            this.TExistencia.ShowImprimir = false;
            this.TExistencia.ShowItem = true;
            this.TExistencia.ShowNuevo = false;
            this.TExistencia.ShowPeriodo = false;
            this.TExistencia.Size = new System.Drawing.Size(1016, 30);
            this.TExistencia.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 537);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1016, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // radGridData
            // 
            this.radGridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridData.Location = new System.Drawing.Point(0, 30);
            this.radGridData.Name = "radGridData";
            this.radGridData.Size = new System.Drawing.Size(1016, 507);
            this.radGridData.TabIndex = 4;
            // 
            // Almacen
            // 
            this.Almacen.AutoSizeDropDownToBestFit = true;
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Almacen.NestedRadGridView
            // 
            this.Almacen.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Almacen.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Almacen.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Almacen.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Almacen.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Almacen.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Almacen.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdAlmacen";
            gridViewTextBoxColumn1.HeaderText = "IdAlmacen";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdAlmacen";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.Almacen.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Almacen.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Almacen.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Almacen.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Almacen.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Almacen.EditorControl.Name = "NestedRadGridView";
            this.Almacen.EditorControl.ReadOnly = true;
            this.Almacen.EditorControl.ShowGroupPanel = false;
            this.Almacen.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Almacen.EditorControl.TabIndex = 0;
            this.Almacen.Location = new System.Drawing.Point(36, 5);
            this.Almacen.Name = "Almacen";
            this.Almacen.NullText = "Almacén";
            this.Almacen.Size = new System.Drawing.Size(181, 20);
            this.Almacen.TabIndex = 385;
            this.Almacen.TabStop = false;
            this.Almacen.ValueMember = "IdDocumento";
            // 
            // ProductoModeloExistenciaTDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 559);
            this.Controls.Add(this.Almacen);
            this.Controls.Add(this.radGridData);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.TExistencia);
            this.Name = "ProductoModeloExistenciaTDForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: TD Existencia";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductoExistenciaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Common.Forms.ToolBarCommonControl TExistencia;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private Telerik.WinControls.UI.RadPivotGrid radGridData;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Almacen;
    }
}