﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Almacen.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ClasificacionCategoriaForm : RadForm {
        protected internal IClasificacionService Service;
        protected internal List<IClasificacionSingle> _DataSource;

        public ClasificacionCategoriaForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ClasificacionCategoriaForm_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            using (ICategoriaGridBuilder view = new CategoriaGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Master().Build());
            }

            this.GridData.AllowAutoSizeColumns = true;
            this.GridData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.GridData.Relations.AddSelfReference(this.GridData.MasterTemplate, "IdCategoria","IdSubCategoria");

            this.Service = new ClasificacionService();

            this.TCategoria.Actualizar.Click += Actualizar_Click;
            this.TCategoria.Cerrar.Click += Cerrar_Click;
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            var d0 = ClasificacionService.Query().ByAlmacen(Domain.Base.ValueObjects.AlmacenEnum.PT).OnlyActive(false).Build();
            this._DataSource = this.Service.GetList<ClasificacionSingle>(d0).ToList<IClasificacionSingle>();
            this.GridData.DataSource = this._DataSource;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
