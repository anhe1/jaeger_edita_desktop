﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Almacen.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.Forms {
    /// <summary>
    /// catalogo de productos y servicios
    /// </summary>
    public partial class ProductoCatalogoForm : RadForm {
        // los permisos se asignan en el formulario derivado
        #region declaraciones
        protected internal ICatalogoProductoService Service;
        protected internal IClasificacionService _CategoriaS;
        protected internal UIMenuElement menuElement = new UIMenuElement();
        protected internal UIAction Action;
        private BindingList<ProductoServicioDetailModel> _DataSource;
        private List<IClasificacionSingle> Categorias;
        private BindingList<UnidadModel> unidades;
        private BindingList<ReceptorModel> _ListaProveedor = new BindingList<ReceptorModel>();
        private List<ProductoServicioTipoModel> tipos;
        #endregion

        public ProductoCatalogoForm() {
            InitializeComponent();
        }

        private void ProductoCatalogoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;

            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.GProducto.Name = "Productos";
                this.GModelos.Name = "Modelos";
                this.GProducto.Standard();
                this.GModelos.Standard();
                this.TProveedor.GridData.Name = "Proveedor";
                this.GProducto.Columns.AddRange(view.Templetes().Master().Build());
                this.GModelos.Columns.AddRange(view.Templetes().Modelos().Build());
                this.TProveedor.GridData.Columns.AddRange(view.Templetes().Proveedor().Build());
            }

            this.ragGridMedios.TableElement.RowHeight = 300;

            this.TProducto.Nuevo.Click += this.TProducto_Nuevo_Click;
            this.TProducto.Editar.Click += this.TProducto_Editar_Click;
            this.TProducto.Remover.Click += this.TProducto_Remover_Click;
            this.TProducto.Actualizar.Click += this.TProducto_Actualizar_Click;
            this.TProducto.Filtro.CheckStateChanged += this.TFiltro_CheckStateChanged;
            this.TProducto.Cerrar.Click += this.TProducto_Cerrar_Click;

            this.GProducto.EditorRequired += this.GProducto_EditorRequired;
            this.GProducto.CellBeginEdit += this.RadGridView_CellBeginEdit;
            this.GProducto.CellEndEdit += this.RadGridView_CellEndEdit;
            this.GProducto.RowsChanging += this.RadGridView_RowsChanging;

            this.TModelos.Nuevo.Click += this.TModelo_Nuevo_Click;
            this.TModelos.Editar.Click += this.TModelo_Editar_Click;
            this.TModelos.Remover.Click += this.TModelo_Remover_Click;
            this.TModelos.Filtro.CheckStateChanged += this.TFiltro_CheckStateChanged;
            this.TModelos.Imagen.Click += this.TModelo_Imagen_Click;
            this.TModelos.Autorizar.Click += this.TModelo_Autorizar_Click;

            this.GModelos.CellBeginEdit += this.RadGridView_CellBeginEdit;
            this.GModelos.CellEndEdit += this.RadGridView_CellEndEdit;
            this.GModelos.RowsChanging += this.RadGridView_RowsChanging;
            this.GModelos.CommandCellClick += this.RadGridModelos_CommandCellClick;

            this.TProveedor.Nuevo.Click += this.TProveedor_Nuevo_Click;
            this.TProveedor.Remover.Click += this.TProveedor_Remover_Click;
            this.TProveedor.Filtro.CheckStateChanged += this.TFiltro_CheckStateChanged;
        }

        #region barra de herramientas productos
        public virtual void TProducto_Nuevo_Click(object sender, EventArgs eventArgs) {

        }

        public virtual void TProducto_Editar_Click(object sender, EventArgs eventArgs) {

        }

        public virtual void TProducto_Remover_Click(object sender, EventArgs e) {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioDetailModel;
                if (seleccionado.IdProducto == 0) {
                    this.GProducto.Rows.Remove(this.GProducto.CurrentRow);
                } else {
                    if (RadMessageBox.Show(this, Properties.Resources.Message_RemoverProducto, "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                        return;
                }
            }
        }

        public virtual void TProducto_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Producto_Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }

            var cboCategoria = this.GProducto.Columns["IdCategoria"] as GridViewComboBoxColumn;
            cboCategoria.DataSource = this.Categorias;
            cboCategoria.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboCategoria.DisplayMember = "Descriptor";
            cboCategoria.ValueMember = "IdCategoria";

            // catalogo de unidades del almacen
            //var comboUnidades = this.GModelos.Columns["Unidad"] as GridViewComboBoxColumn;
            //comboUnidades.DisplayMember = "Descripcion";
            //comboUnidades.ValueMember = "Descripcion";
            //comboUnidades.DataSource = this.unidades;

            var cboTipo = this.GModelos.Columns["Tipo"] as GridViewComboBoxColumn;
            cboTipo.DisplayMember = "Descripcion";
            cboTipo.ValueMember = "Id";
            cboTipo.DataSource = this.tipos;

            var cboFactorTrasladoIVA = this.GModelos.Columns["FactorTrasladoIVA"] as GridViewComboBoxColumn;
            cboFactorTrasladoIVA.DisplayMember = "Descripcion";
            cboFactorTrasladoIVA.ValueMember = "Descripcion";
            cboFactorTrasladoIVA.DataSource = CatalogoProductoService.FactorIVA();

            var cboUnidadXY = this.GModelos.Columns["UnidadXY"] as GridViewComboBoxColumn;
            cboUnidadXY.DisplayMember = "Descripcion";
            cboUnidadXY.ValueMember = "IdUnidad";
            cboUnidadXY.DataSource = new BindingList<UnidadModel>(this.unidades);

            var cboUnidadZ = this.GModelos.Columns["UnidadZ"] as GridViewComboBoxColumn;
            cboUnidadZ.DisplayMember = "Descripcion";
            cboUnidadZ.ValueMember = "IdUnidad";
            cboUnidadZ.DataSource = new BindingList<UnidadModel>(this.unidades);

            var cboIdUnidad = this.GModelos.Columns["IdUnidad"] as GridViewComboBoxColumn;
            cboIdUnidad.DisplayMember = "Descripcion";
            cboIdUnidad.ValueMember = "IdUnidad";
            cboIdUnidad.DataSource = new BindingList<UnidadModel>(this.unidades);

            this.GProducto.DataSource = this._DataSource;
            this.GModelos.DataSource = this._DataSource;
            this.ragGridMedios.DataSource = this._DataSource;
            this.TProveedor.GridData.DataSource = this._DataSource;
            this.GModelos.DataMember = "Modelos";
            this.ragGridMedios.DataMember = "Modelos.Medios";
            this.TProveedor.GridData.DataMember = "Modelos.Proveedores";
        }

        public virtual void TFiltro_CheckStateChanged(object sender, EventArgs e) {
            var a = (CommandBarToggleButton)sender;
            this.TProducto.Filtro.ToggleState = a.ToggleState;
            this.TModelos.Filtro.ToggleState = a.ToggleState;
            this.TProveedor.Filtro.ToggleState = a.ToggleState;
            this.GProducto.ShowFilteringRow = a.ToggleState == ToggleState.On;
            this.GModelos.ShowFilteringRow = this.GProducto.ShowFilteringRow;
            this.TProveedor.GridData.ShowFilteringRow = this.GProducto.ShowFilteringRow;

            if (this.GProducto.ShowFilteringRow == false) {
                this.GProducto.FilterDescriptors.Clear();
                this.GModelos.FilterDescriptors.Clear();
                this.TProveedor.GridData.FilterDescriptors.Clear();
            }
        }

        public virtual void TProducto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de herramientas modelos
        public virtual void TModelo_Nuevo_Click(object sender, EventArgs e) {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioDetailModel;
                if (seleccionado != null) {
                    seleccionado.Modelos.Add(new ModeloDetailModel { IdAlmacen = seleccionado.IdAlmacen, IdCategoria = seleccionado.IdCategoria, IdProducto = seleccionado.IdProducto });
                }
            }
        }

        public virtual void TModelo_Editar_Click(object sender, EventArgs e) {

        }

        public virtual void TModelo_Remover_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdModelo == 0) {
                        this.GModelos.Rows.Remove(this.GModelos.CurrentRow);
                    } else {
                        if (RadMessageBox.Show(this, Properties.Resources.Message_RemoverModelo, "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                            return;
                    }
                }
            }
        }

        public virtual void TModelo_Autorizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Modelo_Autorizar)) {
                espera.Text = "Autorizando";
                espera.ShowDialog(this);
            }
        }

        public virtual void TModelo_Imagen_Click(object sender, EventArgs e) {
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            if (System.IO.File.Exists(openFile.FileName) == false) {
                RadMessageBox.Show("No existe el archivo");
                return;
            }
            using (var espera = new Waiting1Form(this.CargarImagen)) {
                espera.Text = "Cargando imagen ...";
                espera.ShowDialog(this);
            }
        }
        #endregion

        #region barra de herramientas proveedores
        public virtual void TProveedor_Nuevo_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdModelo == 0) {
                        return;
                    } else {
                        seleccionado.Proveedores.Add(new ModeloProveedorModel { Activo = true, IdModelo = seleccionado.IdModelo, Creo = ConfigService.Piloto.Clave });
                    }
                }
            }
        }

        public virtual void TProveedor_Remover_Click(object sender, EventArgs e) {
            if (this.TProveedor.GridData.CurrentRow != null) {
                var seleccionado = this.TProveedor.GridData.CurrentRow.DataBoundItem as ModeloProveedorModel;
                if (seleccionado != null) {
                    if (seleccionado.Id == 0) {
                        this.TProveedor.GridData.Rows.Remove(this.TProveedor.GridData.CurrentRow);
                    } else {
                        if (RadMessageBox.Show(this, Properties.Resources.Message_RemoverModelo, "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                            return;
                    }
                }
            }
        }
        #endregion

        #region metodos del grid
        private void GProducto_EditorRequired(object sender, EditorRequiredEventArgs e) {
            if (this.GProducto.CurrentColumn is GridViewComboBoxColumn && this.GProducto.CurrentRow is GridViewFilteringRowInfo) {
                RadTextBoxEditor editor = new RadTextBoxEditor();
                e.Editor = editor;
            }
        }

        public virtual void RadGridView_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            // si es fila de filtro regresamos
            if (this.GProducto.CurrentRow is GridViewFilteringRowInfo) { return; }
            // si tengo permisos para autorizar
            if (this.Action.Autorizar == false) {
                e.Cancel = true;
                return;
            }

            var seleccionado1 = e.Row.DataBoundItem as ModeloDetailModel;
            if (seleccionado1 != null) {
                if (seleccionado1.Autorizado == 1) {
                    RadMessageBox.Show(this, Properties.Resources.Message_Error_ProductoAutorizado, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Error);
                    e.Cancel = true;
                }
            }
            if (e.Column.Name == "IdCategoria") {
                if (e.Column.Tag == null) {
                    e.Column.Tag = true;
                    var editor = (RadMultiColumnComboBoxElement)this.GProducto.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("IdCategoria", "IdCategoria") { IsVisible = false });
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Codigo", "Codigo"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Clase", "Clase"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Descripción", "Descriptor"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Childs", "Childs") { IsVisible = false });
                    editor.EditorControl.EnableAlternatingRowColor = true;
                    editor.EditorControl.AllowRowResize = false;
                    editor.EditorControl.AllowSearchRow = true;
                    editor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
                    editor.AutoSizeDropDownToBestFit = true;
                    var expressionFormattingObject1 = new ExpressionFormattingObject {
                        ApplyToRow = true,
                        CellBackColor = System.Drawing.Color.Empty,
                        CellForeColor = System.Drawing.Color.Empty,
                        Expression = "Childs > 0",
                        Name = "NewCondition",
                        RowBackColor = System.Drawing.Color.Empty,
                        RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                        RowForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))))
                    };
                    editor.EditorControl.Columns[0].ConditionalFormattingObjectList.Add(expressionFormattingObject1);
                }
            } else if (e.Column.Name == "Unitario") {
                var seleccionado = e.Row.DataBoundItem as ModeloDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.Autorizado == 1) {
                        RadMessageBox.Show(this, Properties.Resources.Message_Error_ProductoAutorizado, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Error);
                        e.Cancel = true;
                    }
                }
            }
        }

        public virtual void RadGridView_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (this.TProducto.Tag != null) {
                if ((bool)this.TProducto.Tag == true) {
                    if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.GModelos.Name) {
                        var seleccionado = e.Row.DataBoundItem as ModeloDetailModel;
                        if (seleccionado != null) {
                            this.GModelos.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Modelo_Guardar)) {
                                espera.Text = "Guardando...";
                                espera.ShowDialog(this);
                            }
                        }
                    } else if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.GProducto.Name) {
                        var seleccionado = e.Row.DataBoundItem as ProductoServicioDetailModel;
                        if (seleccionado != null) {
                            this.GProducto.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Producto_Guardar)) {
                                espera.Text = "Guardando ...";
                                espera.ShowDialog(this);
                            }
                        }
                    }
                }
            }
            this.TProducto.Tag = null;
        }

        public virtual void RadGridView_RowsChanging(object sender, GridViewCollectionChangingEventArgs e) {
            if (e.Action == Telerik.WinControls.Data.NotifyCollectionChangedAction.ItemChanging) {
                if (e.OldValue == e.NewValue) {
                    e.Cancel = true;
                } else
                    this.TProducto.Tag = true;
            }
        }

        public virtual void RadGridModelos_CommandCellClick(object sender, GridViewCellEventArgs e) {
            var seleccionado = e.Row.DataBoundItem as ModeloDetailModel;
            if (seleccionado != null) {
                if (this.Action.Autorizar == false) {
                    return;
                }
                if (seleccionado.Unitario <= 0) {
                    RadMessageBox.Show(this, Properties.Resources.Message_Error_UnitarioCero, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
                using (var espera = new Waiting2Form(this.Modelo_Autorizar)) {
                    espera.Text = "Guardando...";
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        #region metodos privados
        private void Producto_Consultar() {
            this._DataSource = new BindingList<ProductoServicioDetailModel>(this.Service.GetList<ProductoServicioDetailModel>(new ProductosQueryBuilder().ByAlmacen(this.Service.Almacen).Activo(true).Build()).ToList());
            this.Categorias = this.Service.GetClasificacion();
            this.unidades = this.Service.GetUnidades();
            this.tipos = CatalogoProductoService.GetTipos();
            this.DirProveedores.RunWorkerAsync();
        }

        private void Modelo_Autorizar() {
            var lista = new List<ModeloDetailModel>();
            lista.AddRange(this.GModelos.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as ModeloDetailModel));
            for (int i = 0; i < lista.Count; i++) {
                lista[i].Autorizado = (lista[i].Autorizado == 0 ? 1 : 0);
            }
            this.Service.Autoriza(lista);
        }

        private void Producto_Guardar() {
            var seleccionado = this.GProducto.Tag as ProductoServicioDetailModel;
            if (seleccionado != null) {
                this.Service.Save(seleccionado);
            }
            this.GProducto.Tag = null;
        }

        private void Modelo_Guardar() {
            var seleccionado = this.GModelos.Tag as ModeloDetailModel;
            if (seleccionado != null) {
                this.Service.Save(seleccionado);
            }
            this.GModelos.Tag = null;
        }
        #endregion

        private void DirProveedores_DoWork(object sender, DoWorkEventArgs e) {
            //this.listaProveedor = this.service.GetList(TipoRelacionComericalEnum.Proveedor);
        }

        private void DirProveedores_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            var categorias = this.TProveedor.GridData.Columns["IdProveedor"] as GridViewComboBoxColumn;
            //categorias.DataSource = this._ListaProveedor;
            //categorias.DisplayMember = "Nombre";
            //categorias.ValueMember = "Id";
        }

        private void CargarImagen() {
            var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloDetailModel;
            var nuevo = this.Service.Save(new MedioModel(), openFile.FileName, string.Format("{0}-{1}.jpg", seleccionado.NoIdentificacion, seleccionado.Descripcion.ToLower().Replace(" ", "-").Trim()));
            seleccionado.Medios.Add(nuevo);
            this.Service.Save(seleccionado);
        }
    }
}
