﻿using System;
using System.Collections.Generic;
using Telerik.Pivot.Core;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoExistenciaTDForm : RadForm {
        #region declaraciones
        protected internal List<ProductoModeloExistenciaModel> _DataSource;
        protected internal ICatalogoProductoService Service;
        #endregion

        public ProductoExistenciaTDForm() {
            InitializeComponent();
        }

        private void ProductoExistenciaForm_Load(object sender, EventArgs e) {
            PivotGridLocalizationProvider.CurrentProvider = new LocalizationProviderPivotGrid();
            this.TExistencia.HostCaption.Text = "Almacén:";
            this.TExistencia.HostItem.HostedItem = this.Almacen.MultiColumnComboBoxElement;
            this.DefaultView();
            this.TExistencia.Actualizar.Click += Actualizar_Click;
            this.TExistencia.ExportarExcel.Click += this.ExportarExcel_Click;
            this.TExistencia.Cerrar.Click += Cerrar_Click;
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            var waiting = new Wating4Form(() => {
                this.Cargar();
            }, "Cargando datos, espere un momento...", false, false);
            waiting.ShowDialog(this);
            this.radGridData.DataSource = this._DataSource;
        }

        private void ExportarExcel_Click(object sender, EventArgs e) {
            var saveFileDialog1 = new System.Windows.Forms.SaveFileDialog {
                Filter = "Excel XLSX|*.xlsx",
                Title = "Export to File"
            };
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "") {
                HelperTelerikExport.RunExportPivotGrid(this.radGridData, saveFileDialog1.FileName, "TablaDinamica");
                RadMessageBox.Show("Successfully exported to " + saveFileDialog1.FileName, "Information", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                try {
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName);
                } finally {
                }
            }
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Cargar() {

        }

        /// <summary>
        /// vista default para la tabla dinamica
        /// </summary>
        public virtual void DefaultView() {
            ///configuracion de la tabla pivote
            var sumAggregateFunctionExistencia = new SumAggregateFunction();
            var propertyAggregateDescriptionExistencia = new PropertyAggregateDescription() {
                AggregateFunction = sumAggregateFunctionExistencia,
                CustomName = "Existencia",
                PropertyName = "Existencia",
                StringFormat = "#,###0",
                StringFormatSelector = null,
                TotalFormat = null
            };

            var gComparerProducto = new GroupNameComparer();
            var propertyGroupDescriptionProducto = new PropertyGroupDescription() {
                CustomName = "Producto",
                GroupComparer = gComparerProducto,
                GroupFilter = null,
                PropertyName = "Nombre",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending
            };

            var gComparerModelo = new GroupNameComparer();
            var propertyGroupDescriptionModelo = new PropertyGroupDescription() {
                CustomName = "Modelo",
                GroupComparer = gComparerModelo,
                GroupFilter = null,
                PropertyName = "Descripcion",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending
            };

            var groupNameComparerTamanio = new GroupNameComparer();
            var dateTimeGroupDescriptionTamanio = new PropertyGroupDescription() {
                CustomName = "Esp-Tamaño",
                GroupComparer = groupNameComparerTamanio,
                GroupFilter = null,
                PropertyName = "Variante",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending,
            };

            this.radGridData.AggregateDescriptions.Add(propertyAggregateDescriptionExistencia);
            this.radGridData.RowGroupDescriptions.Add(propertyGroupDescriptionProducto);
            this.radGridData.RowGroupDescriptions.Add(propertyGroupDescriptionModelo);
            this.radGridData.ColumnGroupDescriptions.Add(dateTimeGroupDescriptionTamanio);
            this.radGridData.PivotGridElement.RowHeadersLayout = PivotLayout.Compact;
        }
    }
}
