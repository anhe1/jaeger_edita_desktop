﻿namespace Jaeger.UI.Almacen.Forms {
    partial class ProductoExistencia1Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TExistencia = new Jaeger.UI.Common.Forms.GridCommonControl();
            this.Almacen = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TExistencia
            // 
            this.TExistencia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TExistencia.Location = new System.Drawing.Point(0, 0);
            this.TExistencia.Name = "TExistencia";
            this.TExistencia.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TExistencia.Permisos = uiAction1;
            this.TExistencia.ShowActualizar = true;
            this.TExistencia.ShowAutosuma = false;
            this.TExistencia.ShowCancelar = false;
            this.TExistencia.ShowCerrar = true;
            this.TExistencia.ShowEditar = false;
            this.TExistencia.ShowEjercicio = false;
            this.TExistencia.ShowExportarExcel = false;
            this.TExistencia.ShowFiltro = true;
            this.TExistencia.ShowHerramientas = false;
            this.TExistencia.ShowImprimir = true;
            this.TExistencia.ShowItem = true;
            this.TExistencia.ShowNuevo = false;
            this.TExistencia.ShowPeriodo = false;
            this.TExistencia.ShowSeleccionMultiple = true;
            this.TExistencia.Size = new System.Drawing.Size(800, 450);
            this.TExistencia.TabIndex = 1;
            // 
            // Almacen
            // 
            this.Almacen.AutoSizeDropDownToBestFit = true;
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Almacen.NestedRadGridView
            // 
            this.Almacen.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Almacen.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Almacen.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Almacen.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Almacen.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Almacen.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Almacen.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdAlmacen";
            gridViewTextBoxColumn1.HeaderText = "IdAlmacen";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdAlmacen";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.Almacen.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Almacen.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Almacen.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Almacen.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Almacen.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Almacen.EditorControl.Name = "NestedRadGridView";
            this.Almacen.EditorControl.ReadOnly = true;
            this.Almacen.EditorControl.ShowGroupPanel = false;
            this.Almacen.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Almacen.EditorControl.TabIndex = 0;
            this.Almacen.Location = new System.Drawing.Point(37, 5);
            this.Almacen.Name = "Almacen";
            this.Almacen.NullText = "Almacén";
            this.Almacen.Size = new System.Drawing.Size(181, 20);
            this.Almacen.TabIndex = 386;
            this.Almacen.TabStop = false;
            this.Almacen.ValueMember = "IdDocumento";
            // 
            // ProductoExistencia1Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Almacen);
            this.Controls.Add(this.TExistencia);
            this.Name = "ProductoExistencia1Form";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ProductoExistencia1Form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductoExistencia1Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Almacen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal Common.Forms.GridCommonControl TExistencia;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Almacen;
    }
}