﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.Forms {
    public partial class Producto2CatalogoForm : RadForm {
        //private List<ICategoriaSingle> categorias;
        protected internal RadContextMenu conextMenuModelos = new RadContextMenu();
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuModelosAplicar = new RadMenuItem { Text = "Aplica a todos" };
        protected internal RadMenuItem RegistrosInactivos = new RadMenuItem { Text = "Registro inactivos", IsChecked = false, CheckOnClick = true };

        public Producto2CatalogoForm() {
            InitializeComponent();
        }

        private void Producto2CatalogoForm_Load(object sender, EventArgs e) {
            this.gProducto.Standard();
            this.gModelo.Standard();
            this.gVariante.Standard();

            this.gImagen.TableElement.RowHeight = 300;

            this.TProductos.Herramientas.Items.Add(this.RegistrosInactivos);
            this.TProductos.Nuevo.Click += this.TProductos_Nuevo_Click;
            this.TProductos.Editar.Click += this.TProductos_Editar_Click;
            this.TProductos.Remover.Click += this.TProductos_Remover_Click;
            this.TProductos.Actualizar.Click += this.TProductos_Actualizar_Click;
            this.TProductos.Filtro.CheckStateChanged += TFiltro_CheckStateChanged;
            this.TProductos.Cerrar.Click += this.TProductos_Cerrar_Click;

            this.TModelos.Nuevo.Click += this.TModelos_Agregar_Click;
            this.TModelos.Editar.Click += this.TModelos_Editar_Click;
            this.TModelos.Remover.Click += this.TModelos_Remover_Click;
            this.TModelos.Autorizar.Click += this.TModelos_Autorizar_Click;
            this.TModelos.Imagen.Click += this.TModelos_Imagen_Click;
            this.TModelos.Filtro.CheckStateChanged += TFiltro_CheckStateChanged;

            this.TDisponible.Nuevo.Click += this.TDisponible_Nuevo_Click;
            this.TDisponible.Remover.Click += this.TDisponible_Remover_Click;

            this.TImagen.Nuevo.Click += this.Nuevo_Click;
            this.TImagen.Remover.Click += this.Remover_Click;

            var cboFactorTrasladoIVA = this.gModelo.Columns["FactorTrasladoIVA"] as GridViewComboBoxColumn;
            cboFactorTrasladoIVA.DisplayMember = "Descripcion";
            cboFactorTrasladoIVA.ValueMember = "Descripcion";

            this.menuModelosAplicar.Click += this.MenuModelosAplicar_Click;
            this.conextMenuModelos.Items.Add(this.menuModelosAplicar);

            this.gProducto.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.RadGridImagen_ContextMenuOpening);
            this.gProducto.EditorRequired += this.GProducto_EditorRequired;
            this.gModelo.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.RadGridImagen_ContextMenuOpening);
            this.gImagen.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.RadGridImagen_ContextMenuOpening);
        }

        #region barra de herramientas productos
        public virtual void TProductos_Nuevo_Click(object sender, EventArgs e) {
            
        }

        public virtual void TProductos_Editar_Click(object sender, EventArgs e) {
        
        }

        public virtual void TProductos_Remover_Click(object sender, EventArgs e) {
        
        }

        public virtual void TProductos_Actualizar_Click(object sender, EventArgs e) {
            
        }

        public virtual void TFiltro_CheckStateChanged(object sender, EventArgs e) {
            
        }

        public virtual void TProductos_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #endregion

        #region barra de herramientas modelos
        /// <summary>
        /// agregar un modelo
        /// </summary>
        public virtual void TModelos_Agregar_Click(object sender, EventArgs e) {
            
        }

        public virtual void TModelos_Editar_Click(object sender, EventArgs e) {
        
        }

        public virtual void TModelos_Remover_Click(object sender, EventArgs e) {
        
        }

        public virtual void TModelos_Autorizar_Click(object sender, EventArgs e) {
        
        }

        public virtual void TModelos_Imagen_Click(object sender, EventArgs e) {

        }
        #endregion

        #region imagenes
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            
        }
        #endregion

        #region disponibles
        public virtual void TDisponible_Nuevo_Click(object sender, EventArgs e) {
            
        }

        public virtual void TDisponible_Remover_Click(object sender, EventArgs e) {
            
        }
        #endregion

        public virtual void MenuModelosAplicar_Click(object sender, EventArgs e) {

        }

        private void RadGridImagen_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {

            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.gModelo.CurrentRow.ViewInfo.ViewTemplate == this.gModelo.MasterTemplate) {
                    e.ContextMenu = this.conextMenuModelos.DropDown;
                }
            }
        }

        private void GProducto_EditorRequired(object sender, EditorRequiredEventArgs e) {
            if (this.gProducto.CurrentColumn is GridViewComboBoxColumn && this.gProducto.CurrentRow is GridViewFilteringRowInfo) {
                RadTextBoxEditor editor = new RadTextBoxEditor();
                e.Editor = editor;
            }
        }
    }
}
