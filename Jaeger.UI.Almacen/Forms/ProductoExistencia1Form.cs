﻿using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoExistencia1Form : RadForm {
        protected internal ICatalogoProductoService _Service;
        protected internal List<ProductoXModelo> _DataSource;

        public ProductoExistencia1Form() {
            InitializeComponent();
        }

        private void ProductoExistencia1Form_Load(object sender, EventArgs e) {
            this.TExistencia.ItemLbl.Text = "Almacén:";
            this.TExistencia.ItemHost.HostedItem = this.Almacen.MultiColumnComboBoxElement;
            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.ValueMember = "IdAlmacen";
            
            this.TExistencia.Actualizar.Click += TExistencia_Actualizar_Click;
        }

        public virtual void TExistencia_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TExistencia.GridData.DataSource = this._DataSource;
        }

        public virtual void Consultar() {

        }

        public virtual IAlmacenModel GetAlmacen() {
            if (this.Almacen.SelectedItem != null) { }
            return ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
        }
    }
}
