﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.UI.Almacen.Builder;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoExistenciaForm : RadForm {
        #region declaraciones
        protected internal List<IProductoExistenciaModel> _DataSource;
        protected internal ICatalogoProductoService _CatalogoService;
        protected internal RadMenuItem TTablaDinamica = new RadMenuItem { Text = "Tabla dinámica" };
        #endregion

        public ProductoExistenciaForm() {
            InitializeComponent();
        }

        private void ProductoExistenciaForm_Load(object sender, EventArgs e) {
            this.TExistencia.ItemLbl.Text = "Almacén:";
            this.TExistencia.ItemHost.HostedItem = this.Almacen.MultiColumnComboBoxElement;
            this.TExistencia.ShowImprimir = false;

            this.Almacen.DisplayMember = "Descriptor";
            this.Almacen.ValueMember = "IdAlmacen";

            this.TExistencia.Herramientas.Items.Add(this.TTablaDinamica);

            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.TExistencia.GridData.Columns.AddRange(view.Templetes().Existencias().Build());
            }
            this.TExistencia.Actualizar.Click += Actualizar_Click;
            this.TExistencia.Cerrar.Click += Cerrar_Click;
            this.TTablaDinamica.Click += TExistencia_TablaDinamica_Click;
        }

        public virtual void TExistencia_TablaDinamica_Click(object sender, EventArgs e) {
            var exitenciaTD = new ProductoExistenciaTDForm() { MdiParent = this.ParentForm };
            exitenciaTD.Show();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Cargar)) {
                espera.Text = "Calculando existencias ...";
                espera.ShowDialog(this);
            }
            this.TExistencia.GridData.DataSource = this._DataSource;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Cargar() {
            if (this.Almacen.SelectedItem == null) return;
            var current = ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            this._DataSource = this._CatalogoService.GetList<ProductoModeloExistenciaModel>(ExistenciasServices.Query().FromAlmacen(current.IdAlmacen).ByIdAlmacen(this._CatalogoService.Almacen).Activo().Build()).ToList<IProductoExistenciaModel>();
        }

        public virtual IAlmacenModel GetAlmacen() {
            if (this.Almacen.SelectedItem != null) { }
            return ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
        }
    }
}
