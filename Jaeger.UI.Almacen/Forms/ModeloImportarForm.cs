﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Almacen.Builder;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ModeloImportarForm : RadForm {
        #region declaraciones
        protected internal IProductoGridBuilder gridViewBuilder = new ProductoGridBuilder();
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Publicación" };
        protected internal List<ProductoModeloLayout> _DataSource;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ModeloImportarForm() {
            InitializeComponent();
        }

        private void ModeloImportarForm_Load(object sender, EventArgs e) {
            this.TControl.Nuevo.Text = "Agregar";
            this.TControl.Guardar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.TControl.Guardar.Click += this.TControl_Guardar_Click;
            this.TControl.Remover.Click += Remover_Click;
            this.TControl.Cerrar.Click += Cerrar_Click;
            this.Conceptos.Standard();
            this.TControl.GridData.Templates.Add(this.Conceptos);
            this.Conceptos.AllowRowResize = true;
            this.Conceptos.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.Conceptos.AllowEditRow = true;
            this.Conceptos.Columns.Add(new GridViewTextBoxColumn {
                Name = "Contenido",
                HeaderText = "Publicación",
                FieldName = "Contenido",
                Width = 200
            });
            this.Conceptos.CreateRowInfo += Conceptos_CreateRowInfo;
            this.TControl.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.TControl.GridData.CellBeginEdit += this.RadGridView_CellBeginEdit;
            this.TControl.GridData.RowsChanged += GridData_RowsChanged;
            this.Conceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.Conceptos);
        }

        

        

        protected internal void TControl_Agregar_Click(object sender, EventArgs e) {
        }

        protected virtual void TControl_Guardar_Click(object sender, EventArgs e) {
        }

        protected virtual void Remover_Click(object sender, EventArgs e) {

        }

        protected virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Conceptos_CreateRowInfo(object sender, GridViewCreateRowInfoEventArgs e) {
            //            e.RowInfo.Height = 200;
        }

        protected virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.Conceptos.Caption) {

            }
        }

        protected virtual void RadGridView_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {

        }

        protected virtual void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.LStatus.Text = string.Format("{0} fila(s)", this.TControl.GridData.Rows.Count);
            this.TControl.Remover.Enabled = this.TControl.GridData.RowCount > 0;
        }
    }
}
