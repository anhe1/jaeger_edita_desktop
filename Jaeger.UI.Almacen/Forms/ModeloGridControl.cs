﻿using Jaeger.UI.Almacen.Builder;

namespace Jaeger.UI.Almacen.Forms {
    public class ModeloGridControl : Common.Forms.GridStandarControl {
        protected internal IProductoGridBuilder gridViewBuilder = new ProductoGridBuilder();

        public ModeloGridControl() : base() {
        }

        public virtual void CreateView() {
            this.GridData.Columns.AddRange(this.gridViewBuilder.Templetes().ModelosImportar().Build());
        }

        public virtual void CreateView2() {
            this.GridData.Columns.AddRange(this.gridViewBuilder.Templetes().ModelosImportar1().Build());
        }
    }
}
