﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoModeloCatalogoForm : RadForm {
        protected internal BindingList<ProductoServicioModeloModel> _DataSource;
        private List<IClasificacionSingle> _Categorias;
        private BindingList<UnidadModel> _Unidades;
        protected internal ICatalogoProductoService _CatalogoService;

        public ProductoModeloCatalogoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TModelos.Permisos = new UIAction(menuElement.Permisos);
        }

        private void MateriaPrimaCatalogo_Load(object sender, EventArgs e) {
            this.TModelos.Actualizar.Click += Actualizar_Click;
            this.TModelos.Editar.Click += Editar_Click;
            this.TModelos.ShowHerramientas = true;
            this.TModelos.Cerrar.Click += this.TProducto_Cerrar_Click;
            this.TModelos.Nuevo.Enabled = this.TModelos.Permisos.Agregar;
            this.TModelos.Editar.Enabled = this.TModelos.Permisos.Editar;
            this.TModelos.Remover.Enabled = this.TModelos.Permisos.Remover;
            this.TModelos.Autorizar.Enabled = this.TModelos.Permisos.Autorizar;
        }

        protected virtual void Editar_Click(object sender, EventArgs e) {
            
        }

        protected virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
            this.TModelos.GridData.DataSource = this._DataSource;
            var cboCategoria = this.TModelos.GridData.Columns["IdCategoria"] as GridViewComboBoxColumn;
            cboCategoria.DataSource = this._Categorias;
            cboCategoria.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboCategoria.DisplayMember = "Descriptor";
            cboCategoria.ValueMember = "IdCategoria";

            var cboUnidadXY = this.TModelos.GridData.Columns["UnidadXY"] as GridViewComboBoxColumn;
            cboUnidadXY.DisplayMember = "Descripcion";
            cboUnidadXY.ValueMember = "IdUnidad";
            cboUnidadXY.DataSource = new BindingList<UnidadModel>(this._Unidades);

            var cboUnidadZ = this.TModelos.GridData.Columns["UnidadZ"] as GridViewComboBoxColumn;
            cboUnidadZ.DisplayMember = "Descripcion";
            cboUnidadZ.ValueMember = "IdUnidad";
            cboUnidadZ.DataSource = new BindingList<UnidadModel>(this._Unidades);

            var cboIdUnidad = this.TModelos.GridData.Columns["IdUnidad"] as GridViewComboBoxColumn;
            cboIdUnidad.DisplayMember = "Descripcion";
            cboIdUnidad.ValueMember = "IdUnidad";
            cboIdUnidad.DataSource = new BindingList<UnidadModel>(this._Unidades);
        }

        protected virtual void TProducto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consulta() {
            var queryProductos = CatalogoModeloService.Query().ByIdAlmacen(this._CatalogoService.Almacen).Activo().Build();
            this._DataSource = new BindingList<ProductoServicioModeloModel>(this._CatalogoService.GetList<ProductoServicioModeloModel>(queryProductos).ToList());
            this._Categorias = this._CatalogoService.GetClasificacion();
            this._Unidades = this._CatalogoService.GetUnidades();
        }
    }
}
