﻿namespace Jaeger.UI.Almacen.Forms {
    partial class UnidadCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnidadCatalogoForm));
            this.TUnidad = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TUnidad
            // 
            this.TUnidad.Caption = "";
            this.TUnidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TUnidad.Location = new System.Drawing.Point(0, 0);
            this.TUnidad.Name = "TUnidad";
            this.TUnidad.ShowActualizar = true;
            this.TUnidad.ShowAutorizar = false;
            this.TUnidad.ShowAutosuma = false;
            this.TUnidad.ShowCerrar = true;
            this.TUnidad.ShowEditar = true;
            this.TUnidad.ShowExportarExcel = false;
            this.TUnidad.ShowFiltro = true;
            this.TUnidad.ShowHerramientas = false;
            this.TUnidad.ShowImagen = false;
            this.TUnidad.ShowImprimir = false;
            this.TUnidad.ShowNuevo = true;
            this.TUnidad.ShowRemover = true;
            this.TUnidad.ShowSeleccionMultiple = true;
            this.TUnidad.Size = new System.Drawing.Size(564, 330);
            this.TUnidad.TabIndex = 0;
            // 
            // UnidadCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 330);
            this.Controls.Add(this.TUnidad);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UnidadCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Catálogo de unidades";
            this.Load += new System.EventHandler(this.UnidadCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.GridStandarControl TUnidad;
    }
}