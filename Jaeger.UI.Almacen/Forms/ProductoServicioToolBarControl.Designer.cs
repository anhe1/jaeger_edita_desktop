﻿namespace Jaeger.UI.Almacen.Forms {
    partial class ProductoServicioBuscarToolBarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.Productos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.LabelProducto = new Telerik.WinControls.UI.CommandBarLabel();
            this.ButtonHostConcepto = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ButtonAgregarConcepto = new Telerik.WinControls.UI.CommandBarButton();
            this.ButtonBuscarConcepto = new Telerik.WinControls.UI.CommandBarButton();
            this.ButtonRemoverConcepto = new Telerik.WinControls.UI.CommandBarButton();
            this.ButtonNuevoConcepto = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.radCommandBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Productos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Controls.Add(this.Productos);
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(759, 55);
            this.radCommandBar1.TabIndex = 0;
            // 
            // Productos
            // 
            this.Productos.AutoSizeDropDownToBestFit = true;
            // 
            // Productos.NestedRadGridView
            // 
            this.Productos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Productos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Productos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Productos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Productos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Productos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Productos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn14.FieldName = "IdModelo";
            gridViewTextBoxColumn14.FormatString = "{0:PT00000#}";
            gridViewTextBoxColumn14.HeaderText = "IdModelo";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "IdModelo";
            gridViewTextBoxColumn14.VisibleInColumnChooser = false;
            gridViewTextBoxColumn14.Width = 65;
            gridViewTextBoxColumn15.FieldName = "Tipo";
            gridViewTextBoxColumn15.HeaderText = "Tipo";
            gridViewTextBoxColumn15.Name = "Tipo";
            gridViewTextBoxColumn15.Width = 80;
            gridViewTextBoxColumn16.FieldName = "ProductoDescripcion";
            gridViewTextBoxColumn16.HeaderText = "Descripción";
            gridViewTextBoxColumn16.Name = "ProductoDescripcion";
            gridViewTextBoxColumn16.Width = 350;
            gridViewTextBoxColumn17.FieldName = "Marca";
            gridViewTextBoxColumn17.HeaderText = "Marca";
            gridViewTextBoxColumn17.Name = "Marca";
            gridViewTextBoxColumn17.Width = 200;
            gridViewTextBoxColumn18.FieldName = "Especificacion";
            gridViewTextBoxColumn18.HeaderText = "Especificación";
            gridViewTextBoxColumn18.Name = "Especificacion";
            gridViewTextBoxColumn18.Width = 95;
            gridViewTextBoxColumn19.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn19.HeaderText = "Clave\\r\\nProd. Serv.";
            gridViewTextBoxColumn19.Name = "ClaveProdServ";
            gridViewTextBoxColumn19.Width = 80;
            gridViewTextBoxColumn20.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn20.HeaderText = "Clave\\r\\nUnidad";
            gridViewTextBoxColumn20.Name = "ClaveUnidad";
            gridViewTextBoxColumn20.Width = 80;
            gridViewTextBoxColumn21.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn21.HeaderText = "Identificador";
            gridViewTextBoxColumn21.Name = "NoIdentificacion";
            gridViewTextBoxColumn21.Width = 95;
            gridViewTextBoxColumn22.FieldName = "Unidad";
            gridViewTextBoxColumn22.HeaderText = "Unidad";
            gridViewTextBoxColumn22.Name = "Unidad";
            gridViewTextBoxColumn22.Width = 75;
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.FieldName = "Unitario";
            gridViewTextBoxColumn23.FormatString = "{0:n}";
            gridViewTextBoxColumn23.HeaderText = "Unitario";
            gridViewTextBoxColumn23.Name = "Unitario";
            gridViewTextBoxColumn23.Width = 85;
            gridViewTextBoxColumn24.FieldName = "CtaPredial";
            gridViewTextBoxColumn24.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn24.Name = "CtaPredial";
            gridViewTextBoxColumn24.Width = 85;
            gridViewTextBoxColumn25.FieldName = "NumRequerimiento";
            gridViewTextBoxColumn25.HeaderText = "Núm Req.";
            gridViewTextBoxColumn25.Name = "NumRequerimiento";
            gridViewTextBoxColumn25.Width = 85;
            gridViewTextBoxColumn26.FieldName = "Creo";
            gridViewTextBoxColumn26.HeaderText = "Creó";
            gridViewTextBoxColumn26.Name = "Creo";
            gridViewTextBoxColumn26.Width = 75;
            this.Productos.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26});
            this.Productos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Productos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Productos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Productos.EditorControl.Name = "NestedRadGridView";
            this.Productos.EditorControl.ReadOnly = true;
            this.Productos.EditorControl.ShowGroupPanel = false;
            this.Productos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Productos.EditorControl.TabIndex = 0;
            this.Productos.Location = new System.Drawing.Point(132, 4);
            this.Productos.Name = "Productos";
            this.Productos.Size = new System.Drawing.Size(331, 20);
            this.Productos.TabIndex = 1;
            this.Productos.TabStop = false;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.LabelProducto,
            this.ButtonHostConcepto,
            this.ButtonAgregarConcepto,
            this.ButtonBuscarConcepto,
            this.ButtonRemoverConcepto,
            this.ButtonNuevoConcepto});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            this.commandBarStripElement1.StretchHorizontally = true;
            // 
            // LabelProducto
            // 
            this.LabelProducto.DisplayName = "Etiqueta: Producto / Servicio";
            this.LabelProducto.Name = "LabelProducto";
            this.LabelProducto.Text = "Producto / Servicio:";
            // 
            // ButtonHostConcepto
            // 
            this.ButtonHostConcepto.DisplayName = "Producto Servicio";
            this.ButtonHostConcepto.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.ButtonHostConcepto.MinSize = new System.Drawing.Size(350, 0);
            this.ButtonHostConcepto.Name = "ButtonHostConcepto";
            this.ButtonHostConcepto.Text = "Combo";
            // 
            // ButtonAgregarConcepto
            // 
            this.ButtonAgregarConcepto.DisplayName = "commandBarButton1";
            this.ButtonAgregarConcepto.DrawText = true;
            this.ButtonAgregarConcepto.Image = global::Jaeger.UI.Almacen.Properties.Resources.add_16px;
            this.ButtonAgregarConcepto.Name = "ButtonAgregarConcepto";
            this.ButtonAgregarConcepto.Text = "Agregar";
            this.ButtonAgregarConcepto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonAgregarConcepto.Click += new System.EventHandler(this.OnButtonNuevoClick);
            // 
            // ButtonBuscarConcepto
            // 
            this.ButtonBuscarConcepto.DisplayName = "Buscar";
            this.ButtonBuscarConcepto.DrawText = true;
            this.ButtonBuscarConcepto.Image = global::Jaeger.UI.Almacen.Properties.Resources.search_16px;
            this.ButtonBuscarConcepto.Name = "ButtonBuscarConcepto";
            this.ButtonBuscarConcepto.Text = "Buscar";
            this.ButtonBuscarConcepto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ButtonRemoverConcepto
            // 
            this.ButtonRemoverConcepto.DisplayName = "Remover";
            this.ButtonRemoverConcepto.DrawText = true;
            this.ButtonRemoverConcepto.Image = global::Jaeger.UI.Almacen.Properties.Resources.delete_16px;
            this.ButtonRemoverConcepto.Name = "ButtonRemoverConcepto";
            this.ButtonRemoverConcepto.Text = "Remover";
            this.ButtonRemoverConcepto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ButtonNuevoConcepto
            // 
            this.ButtonNuevoConcepto.DisplayName = "Nuevo";
            this.ButtonNuevoConcepto.DrawText = true;
            this.ButtonNuevoConcepto.Image = global::Jaeger.UI.Almacen.Properties.Resources.add_new_16px;
            this.ButtonNuevoConcepto.Name = "ButtonNuevoConcepto";
            this.ButtonNuevoConcepto.Text = "Nuevo";
            this.ButtonNuevoConcepto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ProductoServicioBuscarToolBarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ProductoServicioBuscarToolBarControl";
            this.Size = new System.Drawing.Size(759, 30);
            this.Load += new System.EventHandler(this.ProductoServicioToolBarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.radCommandBar1.ResumeLayout(false);
            this.radCommandBar1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarLabel LabelProducto;
        private Telerik.WinControls.UI.CommandBarHostItem ButtonHostConcepto;
        private Telerik.WinControls.UI.CommandBarButton ButtonAgregarConcepto;
        private Telerik.WinControls.UI.CommandBarButton ButtonBuscarConcepto;
        private Telerik.WinControls.UI.CommandBarButton ButtonRemoverConcepto;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Productos;
        private Telerik.WinControls.UI.CommandBarButton ButtonNuevoConcepto;
    }
}
