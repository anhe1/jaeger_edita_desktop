﻿
namespace Jaeger.UI.Almacen.Forms {
    partial class ClasificacionCategoriaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TCategoria = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TCategoria
            // 
            this.TCategoria.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCategoria.Etiqueta = "";
            this.TCategoria.Location = new System.Drawing.Point(0, 0);
            this.TCategoria.Name = "TCategoria";
            this.TCategoria.ReadOnly = false;
            this.TCategoria.ShowActualizar = true;
            this.TCategoria.ShowAutorizar = false;
            this.TCategoria.ShowCerrar = true;
            this.TCategoria.ShowEditar = true;
            this.TCategoria.ShowExportarExcel = false;
            this.TCategoria.ShowFiltro = false;
            this.TCategoria.ShowGuardar = false;
            this.TCategoria.ShowHerramientas = false;
            this.TCategoria.ShowImagen = false;
            this.TCategoria.ShowImprimir = true;
            this.TCategoria.ShowNuevo = true;
            this.TCategoria.ShowRemover = true;
            this.TCategoria.Size = new System.Drawing.Size(800, 30);
            this.TCategoria.TabIndex = 0;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(800, 420);
            this.GridData.TabIndex = 7;
            // 
            // ClasificacionCategoriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TCategoria);
            this.Name = "ClasificacionCategoriaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Categorías";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ClasificacionCategoriaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TCategoria;
        public Telerik.WinControls.UI.RadGridView GridData;
    }
}