﻿
namespace Jaeger.UI.Almacen.Forms {
    partial class Producto2CatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn1 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Producto2CatalogoForm));
            this.gProducto = new Telerik.WinControls.UI.RadGridView();
            this.TModelos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gModelo = new Telerik.WinControls.UI.RadGridView();
            this.gImagen = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.pageDisponible = new Telerik.WinControls.UI.RadPageViewPage();
            this.gVariante = new Telerik.WinControls.UI.RadGridView();
            this.TDisponible = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.pageImagen = new Telerik.WinControls.UI.RadPageViewPage();
            this.TImagen = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.contextMenuImagen = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ToolBarButtonAgregaImagen = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonRemoverImagen = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonReemplazarImagen = new Telerik.WinControls.UI.RadMenuItem();
            this.TProductos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.gProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gProducto.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gModelo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.pageDisponible.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).BeginInit();
            this.pageImagen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gProducto
            // 
            this.gProducto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gProducto.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewMultiComboBoxColumn1.FieldName = "IdCategoria";
            gridViewMultiComboBoxColumn1.HeaderText = "Categoría";
            gridViewMultiComboBoxColumn1.Name = "IdCategoria";
            gridViewMultiComboBoxColumn1.Width = 250;
            gridViewTextBoxColumn1.FieldName = "Nombre";
            gridViewTextBoxColumn1.HeaderText = "Producto o Servicio";
            gridViewTextBoxColumn1.Name = "Nombre";
            gridViewTextBoxColumn1.Width = 300;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.FieldName = "FechaNuevo";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn1.Name = "FechaNuevo";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 75;
            gridViewTextBoxColumn2.FieldName = "Creo";
            gridViewTextBoxColumn2.HeaderText = "Creó";
            gridViewTextBoxColumn2.Name = "Creo";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 65;
            this.gProducto.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn2});
            this.gProducto.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gProducto.Name = "gProducto";
            this.gProducto.Size = new System.Drawing.Size(1091, 242);
            this.gProducto.TabIndex = 1;
            // 
            // TModelos
            // 
            this.TModelos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TModelos.Etiqueta = "Modelos";
            this.TModelos.Location = new System.Drawing.Point(0, 0);
            this.TModelos.Name = "TModelos";
            this.TModelos.ReadOnly = false;
            this.TModelos.ShowActualizar = false;
            this.TModelos.ShowAutorizar = true;
            this.TModelos.ShowCerrar = false;
            this.TModelos.ShowEditar = true;
            this.TModelos.ShowExportarExcel = true;
            this.TModelos.ShowFiltro = true;
            this.TModelos.ShowGuardar = false;
            this.TModelos.ShowHerramientas = false;
            this.TModelos.ShowImagen = true;
            this.TModelos.ShowImprimir = false;
            this.TModelos.ShowNuevo = true;
            this.TModelos.ShowRemover = true;
            this.TModelos.Size = new System.Drawing.Size(606, 30);
            this.TModelos.TabIndex = 0;
            // 
            // gModelo
            // 
            this.gModelo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gModelo.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = false";
            expressionFormattingObject1.Name = "NewCondition";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn2.Width = 40;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 85;
            gridViewTextBoxColumn3.FieldName = "Codigo";
            gridViewTextBoxColumn3.HeaderText = "Codigo";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Codigo";
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.FieldName = "Marca";
            gridViewTextBoxColumn5.HeaderText = "Marca";
            gridViewTextBoxColumn5.Name = "Marca";
            gridViewTextBoxColumn5.Width = 200;
            gridViewTextBoxColumn6.FieldName = "Especificacion";
            gridViewTextBoxColumn6.HeaderText = "Especificación";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Especificacion";
            gridViewTextBoxColumn6.Width = 150;
            gridViewComboBoxColumn2.FieldName = "Unidad";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.IsVisible = false;
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 95;
            gridViewCalculatorColumn1.FieldName = "Unitario";
            gridViewCalculatorColumn1.HeaderText = "Unitario";
            gridViewCalculatorColumn1.IsVisible = false;
            gridViewCalculatorColumn1.Name = "Unitario";
            gridViewCalculatorColumn1.Width = 75;
            gridViewMultiComboBoxColumn2.DataType = typeof(int);
            gridViewMultiComboBoxColumn2.FieldName = "IdPrecio";
            gridViewMultiComboBoxColumn2.HeaderText = "Cat. Precios";
            gridViewMultiComboBoxColumn2.IsVisible = false;
            gridViewMultiComboBoxColumn2.Name = "IdPrecio";
            gridViewMultiComboBoxColumn2.Width = 85;
            gridViewComboBoxColumn3.FieldName = "FactorTrasladoIVA";
            gridViewComboBoxColumn3.HeaderText = "Factor IVA";
            gridViewComboBoxColumn3.Name = "FactorTrasladoIVA";
            gridViewComboBoxColumn3.VisibleInColumnChooser = false;
            gridViewComboBoxColumn3.Width = 65;
            gridViewComboBoxColumn3.WrapText = true;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "ValorTrasladoIVA";
            gridViewTextBoxColumn7.FormatString = "{0:P0}";
            gridViewTextBoxColumn7.HeaderText = "% IVA";
            gridViewTextBoxColumn7.Name = "ValorTrasladoIVA";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.VisibleInColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "Etiquetas";
            gridViewTextBoxColumn8.HeaderText = "Etiquetas de búsqueda";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Etiquetas";
            gridViewTextBoxColumn8.Width = 100;
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "UnidadXY";
            gridViewTextBoxColumn9.HeaderText = "UnidadXY";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "UnidadXY";
            gridViewTextBoxColumn10.FieldName = "UnidadZ";
            gridViewTextBoxColumn10.HeaderText = "UnidadZ";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "UnidadZ";
            gridViewComboBoxColumn4.DataType = typeof(int);
            gridViewComboBoxColumn4.FieldName = "IdUnidad";
            gridViewComboBoxColumn4.HeaderText = "Unidad";
            gridViewComboBoxColumn4.Name = "IdUnidad";
            gridViewComboBoxColumn4.Width = 85;
            gridViewCommandColumn1.FieldName = "AutorizadoText";
            gridViewCommandColumn1.HeaderText = "Autorizado";
            gridViewCommandColumn1.Name = "AutorizadoText";
            gridViewCommandColumn1.Width = 75;
            gridViewCheckBoxColumn3.FieldName = "Autorizado";
            gridViewCheckBoxColumn3.HeaderText = "Autorizado";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "Autorizado";
            gridViewComboBoxColumn5.FieldName = "IdVisible";
            gridViewComboBoxColumn5.HeaderText = "Visible";
            gridViewComboBoxColumn5.Name = "IdVisible";
            gridViewComboBoxColumn5.Width = 75;
            gridViewTextBoxColumn11.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn11.HeaderText = "No. Identificación";
            gridViewTextBoxColumn11.Name = "NoIdentificacion";
            gridViewTextBoxColumn11.Width = 120;
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "Minimo";
            gridViewTextBoxColumn12.FormatString = "{0:N0}";
            gridViewTextBoxColumn12.HeaderText = "Minimo";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Minimo";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 65;
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "Maximo";
            gridViewTextBoxColumn13.FormatString = "{0:N0}";
            gridViewTextBoxColumn13.HeaderText = "Maximo";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Maximo";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 65;
            gridViewTextBoxColumn14.DataType = typeof(int);
            gridViewTextBoxColumn14.FieldName = "ReOrden";
            gridViewTextBoxColumn14.FormatString = "{0:N0}";
            gridViewTextBoxColumn14.HeaderText = "ReOrden";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "ReOrden";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 65;
            gridViewTextBoxColumn15.DataType = typeof(int);
            gridViewTextBoxColumn15.FieldName = "Existencia";
            gridViewTextBoxColumn15.FormatString = "{0:N0}";
            gridViewTextBoxColumn15.HeaderText = "Existencia";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "Existencia";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 65;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "Largo";
            gridViewTextBoxColumn16.FormatString = "{0:N2}";
            gridViewTextBoxColumn16.HeaderText = "Largo";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Largo";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 65;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "Ancho";
            gridViewTextBoxColumn17.FormatString = "{0:N2}";
            gridViewTextBoxColumn17.HeaderText = "Ancho";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "Ancho";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 65;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "Alto";
            gridViewTextBoxColumn18.FormatString = "{0:N2}";
            gridViewTextBoxColumn18.HeaderText = "Alto";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "Alto";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 65;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "Peso";
            gridViewTextBoxColumn19.FormatString = "{0:N2}";
            gridViewTextBoxColumn19.HeaderText = "Peso";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Peso";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 65;
            gridViewTextBoxColumn20.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn20.HeaderText = "ClaveUnidad";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "ClaveUnidad";
            gridViewTextBoxColumn21.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn21.HeaderText = "ClaveProdServ";
            gridViewTextBoxColumn21.IsVisible = false;
            gridViewTextBoxColumn21.Name = "ClaveProdServ";
            gridViewTextBoxColumn22.FieldName = "Creo";
            gridViewTextBoxColumn22.HeaderText = "Creó";
            gridViewTextBoxColumn22.IsVisible = false;
            gridViewTextBoxColumn22.Name = "Creo";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.Width = 65;
            gridViewDateTimeColumn2.FieldName = "FechaNuevo";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn2.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn2.IsVisible = false;
            gridViewDateTimeColumn2.Name = "FechaNuevo";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 75;
            gridViewTextBoxColumn23.FieldName = "Modifica";
            gridViewTextBoxColumn23.HeaderText = "Modifica";
            gridViewTextBoxColumn23.IsVisible = false;
            gridViewTextBoxColumn23.Name = "Modifica";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn24.FieldName = "FechaModifica";
            gridViewTextBoxColumn24.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn24.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn24.IsVisible = false;
            gridViewTextBoxColumn24.Name = "FechaModifica";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.gModelo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn2,
            gridViewCalculatorColumn1,
            gridViewMultiComboBoxColumn2,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewComboBoxColumn4,
            gridViewCommandColumn1,
            gridViewCheckBoxColumn3,
            gridViewComboBoxColumn5,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24});
            this.gModelo.MasterTemplate.MultiSelect = true;
            this.gModelo.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gModelo.Name = "gModelo";
            this.gModelo.Size = new System.Drawing.Size(606, 333);
            this.gModelo.TabIndex = 2;
            // 
            // gImagen
            // 
            this.gImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gImagen.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gImagen.MasterTemplate.AllowAddNewRow = false;
            this.gImagen.MasterTemplate.AutoGenerateColumns = false;
            gridViewImageColumn1.FieldName = "Imagen";
            gridViewImageColumn1.HeaderText = "Imagen";
            gridViewImageColumn1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            gridViewImageColumn1.Name = "column1";
            gridViewImageColumn1.Width = 300;
            this.gImagen.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1});
            this.gImagen.MasterTemplate.ShowFilteringRow = false;
            this.gImagen.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gImagen.Name = "gImagen";
            this.gImagen.ShowGroupPanel = false;
            this.gImagen.Size = new System.Drawing.Size(460, 285);
            this.gImagen.TabIndex = 0;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1091, 609);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.gProducto);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1091, 242);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -60);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 246);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1091, 363);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 60);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1091, 363);
            this.radSplitContainer2.TabIndex = 3;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radSplitContainer3);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(1091, 363);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2201414F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(311, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel4);
            this.radSplitContainer3.Controls.Add(this.splitPanel5);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(1091, 363);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.gModelo);
            this.splitPanel4.Controls.Add(this.TModelos);
            this.splitPanel4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(606, 363);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.05749768F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(62, 0);
            this.splitPanel4.TabIndex = 0;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.radPageView1);
            this.splitPanel5.Location = new System.Drawing.Point(610, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(481, 363);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.05749771F, 0F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(-62, 0);
            this.splitPanel5.TabIndex = 1;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.pageDisponible);
            this.radPageView1.Controls.Add(this.pageImagen);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.pageDisponible;
            this.radPageView1.Size = new System.Drawing.Size(481, 363);
            this.radPageView1.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pageDisponible
            // 
            this.pageDisponible.Controls.Add(this.gVariante);
            this.pageDisponible.Controls.Add(this.TDisponible);
            this.pageDisponible.ItemSize = new System.Drawing.SizeF(88F, 28F);
            this.pageDisponible.Location = new System.Drawing.Point(10, 37);
            this.pageDisponible.Name = "pageDisponible";
            this.pageDisponible.Size = new System.Drawing.Size(460, 315);
            this.pageDisponible.Text = "Disponibilidad";
            // 
            // gVariante
            // 
            this.gVariante.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnF2;
            this.gVariante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gVariante.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gVariante.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn6.DataType = typeof(int);
            gridViewComboBoxColumn6.FieldName = "IdEspecificacion";
            gridViewComboBoxColumn6.HeaderText = "Variante";
            gridViewComboBoxColumn6.Name = "IdEspecificacion";
            gridViewComboBoxColumn6.Width = 150;
            gridViewComboBoxColumn7.FieldName = "IdVisibilidad";
            gridViewComboBoxColumn7.HeaderText = "Visibilidad";
            gridViewComboBoxColumn7.Name = "IdVisibilidad";
            gridViewComboBoxColumn7.Width = 80;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "Minimo";
            gridViewTextBoxColumn25.FormatString = "{0:N2}";
            gridViewTextBoxColumn25.HeaderText = "Minimo";
            gridViewTextBoxColumn25.Name = "Minimo";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 65;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "Maximo";
            gridViewTextBoxColumn26.FormatString = "{0:N2}";
            gridViewTextBoxColumn26.HeaderText = "Maximo";
            gridViewTextBoxColumn26.Name = "Maximo";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 65;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "ReOrden";
            gridViewTextBoxColumn27.FormatString = "{0:N2}";
            gridViewTextBoxColumn27.HeaderText = "ReOrden";
            gridViewTextBoxColumn27.Name = "ReOrden";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 65;
            gridViewTextBoxColumn28.FieldName = "Modifica";
            gridViewTextBoxColumn28.HeaderText = "Modifica";
            gridViewTextBoxColumn28.IsVisible = false;
            gridViewTextBoxColumn28.Name = "Modifica";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn28.Width = 75;
            gridViewTextBoxColumn29.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn29.FieldName = "FechaModifica";
            gridViewTextBoxColumn29.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn29.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn29.IsVisible = false;
            gridViewTextBoxColumn29.Name = "FechaModifica";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn29.Width = 65;
            this.gVariante.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn6,
            gridViewComboBoxColumn7,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29});
            this.gVariante.MasterTemplate.EnableFiltering = true;
            this.gVariante.MasterTemplate.ShowFilteringRow = false;
            this.gVariante.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.gVariante.Name = "gVariante";
            this.gVariante.ShowGroupPanel = false;
            this.gVariante.Size = new System.Drawing.Size(460, 285);
            this.gVariante.TabIndex = 5;
            // 
            // TDisponible
            // 
            this.TDisponible.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDisponible.Etiqueta = "Disponibilidad";
            this.TDisponible.Location = new System.Drawing.Point(0, 0);
            this.TDisponible.Name = "TDisponible";
            this.TDisponible.ReadOnly = false;
            this.TDisponible.ShowActualizar = false;
            this.TDisponible.ShowAutorizar = false;
            this.TDisponible.ShowCerrar = false;
            this.TDisponible.ShowEditar = false;
            this.TDisponible.ShowExportarExcel = false;
            this.TDisponible.ShowFiltro = true;
            this.TDisponible.ShowGuardar = false;
            this.TDisponible.ShowHerramientas = false;
            this.TDisponible.ShowImagen = false;
            this.TDisponible.ShowImprimir = false;
            this.TDisponible.ShowNuevo = true;
            this.TDisponible.ShowRemover = true;
            this.TDisponible.Size = new System.Drawing.Size(460, 30);
            this.TDisponible.TabIndex = 6;
            // 
            // pageImagen
            // 
            this.pageImagen.Controls.Add(this.gImagen);
            this.pageImagen.Controls.Add(this.TImagen);
            this.pageImagen.ItemSize = new System.Drawing.SizeF(64F, 28F);
            this.pageImagen.Location = new System.Drawing.Point(10, 37);
            this.pageImagen.Name = "pageImagen";
            this.pageImagen.Size = new System.Drawing.Size(460, 315);
            this.pageImagen.Text = "Imagenes";
            // 
            // TImagen
            // 
            this.TImagen.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImagen.Etiqueta = "";
            this.TImagen.Location = new System.Drawing.Point(0, 0);
            this.TImagen.Name = "TImagen";
            this.TImagen.ReadOnly = false;
            this.TImagen.ShowActualizar = false;
            this.TImagen.ShowAutorizar = false;
            this.TImagen.ShowCerrar = false;
            this.TImagen.ShowEditar = false;
            this.TImagen.ShowExportarExcel = false;
            this.TImagen.ShowFiltro = true;
            this.TImagen.ShowGuardar = false;
            this.TImagen.ShowHerramientas = false;
            this.TImagen.ShowImagen = false;
            this.TImagen.ShowImprimir = false;
            this.TImagen.ShowNuevo = true;
            this.TImagen.ShowRemover = true;
            this.TImagen.Size = new System.Drawing.Size(460, 30);
            this.TImagen.TabIndex = 7;
            // 
            // contextMenuImagen
            // 
            this.contextMenuImagen.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonAgregaImagen,
            this.ToolBarButtonRemoverImagen,
            this.ToolBarButtonReemplazarImagen});
            // 
            // ToolBarButtonAgregaImagen
            // 
            this.ToolBarButtonAgregaImagen.Name = "ToolBarButtonAgregaImagen";
            this.ToolBarButtonAgregaImagen.Text = "Agregar imagen";
            // 
            // ToolBarButtonRemoverImagen
            // 
            this.ToolBarButtonRemoverImagen.Name = "ToolBarButtonRemoverImagen";
            this.ToolBarButtonRemoverImagen.Text = "Remover";
            // 
            // ToolBarButtonReemplazarImagen
            // 
            this.ToolBarButtonReemplazarImagen.Name = "ToolBarButtonReemplazarImagen";
            this.ToolBarButtonReemplazarImagen.Text = "Reemplazar";
            // 
            // TProductos
            // 
            this.TProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProductos.Etiqueta = "Productos";
            this.TProductos.Location = new System.Drawing.Point(0, 0);
            this.TProductos.Name = "TProductos";
            this.TProductos.ReadOnly = false;
            this.TProductos.ShowActualizar = true;
            this.TProductos.ShowAutorizar = false;
            this.TProductos.ShowCerrar = true;
            this.TProductos.ShowEditar = true;
            this.TProductos.ShowExportarExcel = false;
            this.TProductos.ShowFiltro = true;
            this.TProductos.ShowGuardar = false;
            this.TProductos.ShowHerramientas = false;
            this.TProductos.ShowImagen = false;
            this.TProductos.ShowImprimir = false;
            this.TProductos.ShowNuevo = true;
            this.TProductos.ShowRemover = true;
            this.TProductos.Size = new System.Drawing.Size(1091, 30);
            this.TProductos.TabIndex = 3;
            // 
            // Producto2CatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 639);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TProductos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Producto2CatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Catálogo de Productos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Producto2CatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gProducto.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gModelo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.pageDisponible.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).EndInit();
            this.pageImagen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadContextMenu contextMenuImagen;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonAgregaImagen;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonRemoverImagen;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonReemplazarImagen;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage pageDisponible;
        private Telerik.WinControls.UI.RadPageViewPage pageImagen;
        protected internal Telerik.WinControls.UI.RadGridView gProducto;
        protected internal Telerik.WinControls.UI.RadGridView gImagen;
        protected internal Common.Forms.ToolBarStandarControl TProductos;
        protected internal Telerik.WinControls.UI.RadGridView gModelo;
        protected internal Common.Forms.ToolBarStandarControl TModelos;
        protected internal Telerik.WinControls.UI.RadGridView gVariante;
        protected internal Common.Forms.ToolBarStandarControl TDisponible;
        protected internal Common.Forms.ToolBarStandarControl TImagen;
    }
}