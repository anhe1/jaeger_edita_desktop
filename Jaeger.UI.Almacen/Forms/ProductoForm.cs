﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoForm : RadForm {
        #region declaraciones
        protected internal ICatalogoProductoService _Service;
        protected internal ProductoServicioDetailModel _Producto;
        protected internal List<IClasificacionSingle> _Categorias;
        #endregion

        public ProductoForm() {
            InitializeComponent();
        }

        public ProductoForm(ICatalogoProductoService service, ProductoServicioDetailModel producto) {
            InitializeComponent();
            this._Service = service;
            this._Producto = producto;
        }

        public ProductoForm(ICatalogoProductoService producto) {
            InitializeComponent();
            this._Service = producto;
        }

        private void ProductoForm_Load(object sender, EventArgs e) {
            this.cboTipo.DataSource = CatalogoProductoService.GetTipos();
            this.cboCategorias.DataSource = this._Service.GetClasificacion();
            this.cboCategorias.DisplayMember = "Descriptor";
            this.cboCategorias.ValueMember = "IdCategoria";
            this.cboCategorias.EditorControl.EnableAlternatingRowColor = true;
            this.cboCategorias.EditorControl.AllowRowResize = false;
            this.cboCategorias.EditorControl.AllowSearchRow = true;
            this.cboCategorias.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.cboCategorias.SelectedIndexChanged += CboCategorias_SelectedIndexChanged;
            this.Initialization();
            this.CreateBinding();
        }

        private void CboCategorias_SelectedIndexChanged(object sender, EventArgs e) {
            var indice = (this.cboCategorias.SelectedItem as GridViewDataRowInfo).DataBoundItem as IClasificacionSingle;
            if (indice.Childs > 0) {
                this.TProducto.Guardar.Enabled = false;
                this.Label.Text = "Selección no válida";
            } else {
                this.TProducto.Guardar.Enabled = true;
                this.Label.Text = "";
            }
        }

        public virtual void Initialization() {
            this.cboCategorias.EditorControl.EnableAlternatingRowColor = true;
            this.cboCategorias.EditorControl.AllowRowResize = false;
            this.cboCategorias.EditorControl.AllowSearchRow = true;
            this.cboCategorias.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.TProducto.Cerrar.Click += TProducto_Cerrar_Click;
            this.TProducto.Guardar.Click += TProducto_Guardar_Click;
        }

        private void CreateBinding() {
            this.cboCategorias.DataBindings.Clear();
            this.cboCategorias.DataBindings.Add("SelectedValue", this._Producto, "IdCategoria", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.cboTipo.DataBindings.Clear();
            this.cboTipo.DataBindings.Add("SelectedValue",this._Producto, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbProducto.DataBindings.Clear();
            this.txbProducto.DataBindings.Add("Text", this._Producto, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DataBindings.Clear();
            this.DataBindings.Add("Text", this._Producto, "Codigo", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public virtual void TProducto_Guardar_Click(object sender, EventArgs e) {
            var d0 = this._Service.Save(this._Producto);
            this._Producto.IdProducto = d0.IdProducto;
        }

        public virtual void TProducto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
