﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.UI.Almacen.Builder;
using Jaeger.Aplication.Almacen.Services;
using System.Linq;

namespace Jaeger.UI.Almacen.Forms {
    public partial class UnidadCatalogoForm : RadForm {
        #region declaraciones
        protected internal IUnidadService Service;
        protected internal BindingList<UnidadModel> SourceData;
        #endregion

        public UnidadCatalogoForm() {
            InitializeComponent();
            this.TUnidad.Permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public UnidadCatalogoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TUnidad.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void UnidadCatalogoForm_Load(object sender, EventArgs e) {
            
            using (IUnidadGridBuilder view = new UnidadGridBuilder()) {
                this.TUnidad.GridData.Columns.AddRange(view.Templetes().Master().Build());
            }

            this.TUnidad.GridData.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            this.TUnidad.Editar.Visibility = ElementVisibility.Collapsed;
            this.TUnidad.Filtro.Visibility = ElementVisibility.Collapsed;
            this.TUnidad.Imprimir.Visibility = ElementVisibility.Collapsed;
            this.TUnidad.Guardar.Visibility = ElementVisibility.Visible;
            this.TUnidad.Herramientas.Visibility = ElementVisibility.Visible;
            
            this.TUnidad.Nuevo.Click += this.Nuevo_Click;
            this.TUnidad.Remover.Click += this.Remover_Click;
            this.TUnidad.Guardar.Click += this.Guardar_Click;
            this.TUnidad.Guardar.Enabled = this.TUnidad.Nuevo.Enabled | this.TUnidad.Editar.Enabled;
            this.TUnidad.Actualizar.Click += this.Actualizar_Click;
            this.TUnidad.Cerrar.Click += this.Cerrar_Click;
            this.TUnidad.GridData.CellBeginEdit += this.GridData_CellBeginEdit;
            this.TUnidad.GridData.CellEndEdit += this.GridData_CellEndEdit;
            this.TUnidad.GridData.AllowEditRow = this.TUnidad.Permisos.Editar;
        }

        #region barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            this.SourceData.Add(UnidadService.Create().Build());
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.TUnidad.GridData.DataSource = this.SourceData;
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            var _seleccionado = this.TUnidad.GetCurrent<UnidadModel>();
            if (_seleccionado != null) {
                if (RadMessageBox.Show(this, "¿Esta seguro de remover el objeto selecciondo?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    using (var espera = new Waiting1Form(this.Remover)) {
                        espera.ShowDialog(this);
                    }
                    this.TUnidad.GridData.DataSource = this.SourceData;
                }
            }
        }

        public virtual void Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Save)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.TUnidad.GridData.DataSource = this.SourceData;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        private void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Column.Name == "Activo") {
                if (this.TUnidad.Remover.Enabled == false) {
                    e.Cancel = true;
                }
            }
        }

        private void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
                var _seleccionado = this.TUnidad.GetCurrent<UnidadModel>();
                if (_seleccionado != null) {
                    _seleccionado.SetModified = true;
            }
        }
        #endregion

        public virtual void Actualizar() {
            var d0 = UnidadService.Query().ByAlmacen(this.Service.Almacen).Activo(!this.TUnidad.RegistroInactivo.IsChecked).Build();
            this.SourceData = new BindingList<UnidadModel>(this.Service.GetList<UnidadModel>(d0).ToList());
        }

        public virtual void Save() {
            this.SourceData = this.Service.Save(this.SourceData);
        }

        public virtual void Remover() {
            var _seleccionado = this.TUnidad.GetCurrent<UnidadModel>();
            if (_seleccionado != null) {
                _seleccionado.Activo = false;
                _seleccionado = this.Service.Save(_seleccionado);
            }
        }
    }
}
