﻿
namespace Jaeger.UI.Almacen.Forms {
    partial class ProductoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoForm));
            this.panelProducto = new Telerik.WinControls.UI.RadPanel();
            this.txbProducto = new Telerik.WinControls.UI.RadTextBox();
            this.lblModelo = new Telerik.WinControls.UI.RadLabel();
            this.cboCategorias = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCategoria = new Telerik.WinControls.UI.RadLabel();
            this.cboTipo = new Telerik.WinControls.UI.RadDropDownList();
            this.lblTipo = new Telerik.WinControls.UI.RadLabel();
            this.TProducto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.Label = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).BeginInit();
            this.panelProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panelProducto
            // 
            this.panelProducto.Controls.Add(this.txbProducto);
            this.panelProducto.Controls.Add(this.lblModelo);
            this.panelProducto.Controls.Add(this.cboCategorias);
            this.panelProducto.Controls.Add(this.lblCategoria);
            this.panelProducto.Controls.Add(this.cboTipo);
            this.panelProducto.Controls.Add(this.lblTipo);
            this.panelProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelProducto.Location = new System.Drawing.Point(0, 30);
            this.panelProducto.Name = "panelProducto";
            this.panelProducto.Size = new System.Drawing.Size(515, 123);
            this.panelProducto.TabIndex = 42;
            // 
            // txbProducto
            // 
            this.txbProducto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbProducto.Location = new System.Drawing.Point(12, 87);
            this.txbProducto.MaxLength = 1000;
            this.txbProducto.Name = "txbProducto";
            this.txbProducto.NullText = "Descripción";
            this.txbProducto.Size = new System.Drawing.Size(488, 20);
            this.txbProducto.TabIndex = 7;
            // 
            // lblModelo
            // 
            this.lblModelo.Location = new System.Drawing.Point(12, 63);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(121, 18);
            this.lblModelo.TabIndex = 6;
            this.lblModelo.Text = "Nombre o Descripción:";
            // 
            // cboCategorias
            // 
            this.cboCategorias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCategorias.AutoSizeDropDownToBestFit = true;
            // 
            // cboCategorias.NestedRadGridView
            // 
            this.cboCategorias.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboCategorias.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategorias.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboCategorias.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboCategorias.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "IdCategoria";
            gridViewTextBoxColumn1.HeaderText = "IdCategoria";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdCategoria";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Codigo";
            gridViewTextBoxColumn2.HeaderText = "Código";
            gridViewTextBoxColumn2.Name = "Codigo";
            gridViewTextBoxColumn3.FieldName = "Clase";
            gridViewTextBoxColumn3.HeaderText = "Clase";
            gridViewTextBoxColumn3.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            gridViewTextBoxColumn3.Name = "Clase";
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn5.FieldName = "Descriptor";
            gridViewTextBoxColumn5.HeaderText = "Descriptor";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Descriptor";
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Childs > 0";
            expressionFormattingObject1.Name = "NewCondition";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            gridViewTextBoxColumn6.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "Childs";
            gridViewTextBoxColumn6.HeaderText = "Childs";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Childs";
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            this.cboCategorias.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.cboCategorias.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboCategorias.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cboCategorias.EditorControl.Name = "NestedRadGridView";
            this.cboCategorias.EditorControl.ReadOnly = true;
            this.cboCategorias.EditorControl.ShowGroupPanel = false;
            this.cboCategorias.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboCategorias.EditorControl.TabIndex = 0;
            this.cboCategorias.Location = new System.Drawing.Point(12, 30);
            this.cboCategorias.Name = "cboCategorias";
            this.cboCategorias.NullText = "Categoría";
            this.cboCategorias.Size = new System.Drawing.Size(488, 20);
            this.cboCategorias.TabIndex = 3;
            this.cboCategorias.TabStop = false;
            // 
            // lblCategoria
            // 
            this.lblCategoria.Location = new System.Drawing.Point(12, 6);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(57, 18);
            this.lblCategoria.TabIndex = 2;
            this.lblCategoria.Text = "Categoría:";
            // 
            // cboTipo
            // 
            this.cboTipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTipo.DisplayMember = "Descripcion";
            this.cboTipo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cboTipo.Location = new System.Drawing.Point(384, 62);
            this.cboTipo.Name = "cboTipo";
            this.cboTipo.Size = new System.Drawing.Size(116, 20);
            this.cboTipo.TabIndex = 1;
            this.cboTipo.ValueMember = "Id";
            // 
            // lblTipo
            // 
            this.lblTipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipo.Location = new System.Drawing.Point(347, 63);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(31, 18);
            this.lblTipo.TabIndex = 0;
            this.lblTipo.Text = "Tipo:";
            // 
            // TProducto
            // 
            this.TProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProducto.Etiqueta = "";
            this.TProducto.Location = new System.Drawing.Point(0, 0);
            this.TProducto.Name = "TProducto";
            this.TProducto.ReadOnly = false;
            this.TProducto.ShowActualizar = false;
            this.TProducto.ShowAutorizar = false;
            this.TProducto.ShowCerrar = true;
            this.TProducto.ShowEditar = false;
            this.TProducto.ShowExportarExcel = false;
            this.TProducto.ShowFiltro = false;
            this.TProducto.ShowGuardar = true;
            this.TProducto.ShowHerramientas = false;
            this.TProducto.ShowImagen = false;
            this.TProducto.ShowImprimir = false;
            this.TProducto.ShowNuevo = false;
            this.TProducto.ShowRemover = false;
            this.TProducto.Size = new System.Drawing.Size(515, 30);
            this.TProducto.TabIndex = 43;
            // 
            // TEstado
            // 
            this.TEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Label});
            this.TEstado.Location = new System.Drawing.Point(0, 166);
            this.TEstado.Name = "TEstado";
            this.TEstado.Size = new System.Drawing.Size(515, 26);
            this.TEstado.SizingGrip = false;
            this.TEstado.TabIndex = 44;
            // 
            // Label
            // 
            this.Label.Name = "Label";
            this.TEstado.SetSpring(this.Label, false);
            this.Label.Text = "";
            this.Label.TextWrap = true;
            // 
            // ProductoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 192);
            this.Controls.Add(this.TEstado);
            this.Controls.Add(this.panelProducto);
            this.Controls.Add(this.TProducto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Producto";
            this.Load += new System.EventHandler(this.ProductoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).EndInit();
            this.panelProducto.ResumeLayout(false);
            this.panelProducto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal Telerik.WinControls.UI.RadPanel panelProducto;
        protected internal Telerik.WinControls.UI.RadTextBox txbProducto;
        protected internal Telerik.WinControls.UI.RadLabel lblModelo;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox cboCategorias;
        protected internal Telerik.WinControls.UI.RadLabel lblCategoria;
        protected internal Telerik.WinControls.UI.RadDropDownList cboTipo;
        protected internal Telerik.WinControls.UI.RadLabel lblTipo;
        protected internal Common.Forms.ToolBarStandarControl TProducto;
        private Telerik.WinControls.UI.RadStatusStrip TEstado;
        private Telerik.WinControls.UI.RadLabelElement Label;
    }
}