﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;

namespace Jaeger.UI.Almacen.Forms {
    public partial class EspecificacionCatalogoForm : RadForm {
        protected internal IEspecificacionService _Service;
        protected internal BindingList<EspecificacionModel> _DataSource;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;
        private readonly RadMenuItem _RegistroInactivo = new RadMenuItem { Text = "Mostrar registros inactivos", CheckOnClick = true };

        public EspecificacionCatalogoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void EspecificacionCatalogoForm_Load(object sender, EventArgs e) {
            this.TEspecificacion.Herramientas.Items.Add(_RegistroInactivo);
            this.TEspecificacion.Nuevo.Enabled = this._permisos.Agregar;
            this.TEspecificacion.Remover.Enabled = this._permisos.Remover;
            this.gridData.AllowEditRow = this._permisos.Editar;

            this.TEspecificacion.Nuevo.Click += this.TEspecifiacion_Nuevo_Click;
            this.TEspecificacion.Remover.Click += this.TEspecifiacion_Remover_Click;
            this.TEspecificacion.Guardar.Click += this.TEspecificacion_Guardar_Click;
            this.TEspecificacion.Actualizar.Click += this.TEspecificacion_Actualizar_Click;
            this.TEspecificacion.Filtro.Click += this.TEspecificacion_Filtro_Click;
            this.TEspecificacion.Cerrar.Click += this.TEspecificacion_Cerrar_Click;
        }

        #region barra de herramientas
        private void TEspecifiacion_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new EspecificacionModel {
                SetModified = true
            };
            this._DataSource.Add(_nuevo);
        }

        private void TEspecifiacion_Remover_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as EspecificacionModel;
                if (_seleccionado != null) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover el objeto selecciondo?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        using (var espera = new Waiting1Form(this.Remover)) {
                            espera.ShowDialog(this);
                        }
                        this.gridData.Rows.Remove(this.gridData.CurrentRow);
                    }
                }
            }
        }

        private void TEspecificacion_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Save)) {
                espera.ShowDialog(this);
            }
            this.gridData.DataSource = this._DataSource;
        }

        private void TEspecificacion_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.gridData.DataSource = this._DataSource;
        }

        private void TEspecificacion_Filtro_Click(object sender, EventArgs e) {
            this.gridData.ShowFilteringRow = this.TEspecificacion.Filtro.ToggleState != ToggleState.On;
            if (this.gridData.ShowFilteringRow == false)
                this.gridData.FilterDescriptors.Clear();
        }

        private void TEspecificacion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        private void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Column.Name == "Activo") {
                if (this._permisos.Remover == false) {
                    e.Cancel = true;
                }
            }
        }

        private void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as EspecificacionModel;
                if (seleccionado != null) {
                    seleccionado.SetModified = true;
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this._DataSource = new BindingList<EspecificacionModel>(
                this._Service.GetList<EspecificacionModel>(EspecificacionService.Query().IsActive(this._RegistroInactivo.IsChecked).Build()).ToList());
        }

        private void Save() {
            this._DataSource = this._Service.Save(this._DataSource);
        }

        private void Remover() {
            var seleccionado = this.gridData.CurrentRow.DataBoundItem as EspecificacionModel;
            if (seleccionado != null) {
                seleccionado.Activo = false;
                seleccionado = this._Service.Save(seleccionado);
            }
        }
        #endregion
    }
}
