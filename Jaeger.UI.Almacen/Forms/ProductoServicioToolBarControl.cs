﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Almacen.Contracts;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoServicioBuscarToolBarControl : UserControl {
        private BackgroundWorker consultar;

        #region eventos
        public event EventHandler<ProductoServicioModeloModel> ButtonAgregar_Click;
        public event EventHandler<EventArgs> ButtonBuscar_Click;
        public event EventHandler<EventArgs> ButtonRemover_Click;
        public event EventHandler<EventArgs> ButtonNuevo_Click;

        public void OnButtonAgregarClick(object sender, ProductoServicioModeloModel e) {
            if (this.ButtonAgregar_Click != null) {
                this.ButtonAgregar_Click(sender, e);
            }
        }

        public void OnButtonBuscarClick(object sender, EventArgs e) {
            if (this.ButtonBuscar_Click != null) {
                this.ButtonBuscar_Click(sender, e);
            }
        }

        public void OnButtonRemoverClick(object sender, EventArgs e) {
            if (this.ButtonRemover_Click != null) {
                this.ButtonRemover_Click(sender, e);
            }
        }

        public void OnButtonNuevoClick(object sender, EventArgs e) {
            if (this.ButtonNuevo_Click != null) {
                this.ButtonNuevo_Click(sender, e);
            }
        }
        #endregion

        #region propiedades
        private ICatalogoProductoService Service;
        #endregion

        public ProductoServicioBuscarToolBarControl() {
            InitializeComponent();
            this.Service = null;
        }

        private void ProductoServicioToolBarControl_Load(object sender, EventArgs e) {
            this.ButtonHostConcepto.HostedItem = this.Productos.MultiColumnComboBoxElement;
            this.ButtonAgregarConcepto.Click += this.ToolBar_ButtonAgregar_Click;
            this.ButtonBuscarConcepto.Click += new EventHandler(this.OnButtonBuscarClick);
            this.ButtonNuevoConcepto.Click += new EventHandler(this.OnButtonNuevoClick);
            this.ButtonRemoverConcepto.Click += new EventHandler(this.OnButtonRemoverClick);
            //this.Service = new Aplication.Almacen.Services.CatalogoMateriaPrimaService();
            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += Consultar_DoWork;
            this.consultar.RunWorkerCompleted += Consultar_RunWorkerCompleted;
            this.consultar.RunWorkerAsync();
        }

        private void ToolBar_ButtonAgregar_Click(object sender, EventArgs e) {
            if (this.Productos.SelectedItem != null) {
                var producto = ((GridViewDataRowInfo)this.Productos.SelectedItem).DataBoundItem as ProductoServicioModeloModel;
                    this.OnButtonAgregarClick(sender, producto);
                }
        }

        private void Consultar_DoWork(object sender, DoWorkEventArgs e) {
            //this.resultado = this.Service.Search("").ToList();
        }

        private void Consultar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //this.Productos.DataSource = this.resultado;
        }
    }
}
