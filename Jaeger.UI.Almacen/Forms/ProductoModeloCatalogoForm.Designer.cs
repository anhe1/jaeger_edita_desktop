﻿namespace Jaeger.UI.Almacen.Forms
{
    partial class ProductoModeloCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoModeloCatalogoForm));
            this.TModelos = new Jaeger.UI.Almacen.Forms.ProductoModeloGridControl();
            this.MenuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ToolBarButtonSeleccion = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonDuplicar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoProducto = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoValeEntrada = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoValeSalida = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.RadMenuItem();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radGridMedios = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarMedios = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonImagen = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonRemoverI = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridMedios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridMedios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TModelos
            // 
            this.TModelos.Caption = "";
            this.TModelos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TModelos.Location = new System.Drawing.Point(0, 0);
            this.TModelos.Name = "TModelos";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TModelos.Permisos = uiAction1;
            this.TModelos.ShowActualizar = true;
            this.TModelos.ShowAutorizar = false;
            this.TModelos.ShowAutosuma = false;
            this.TModelos.ShowCerrar = true;
            this.TModelos.ShowEditar = true;
            this.TModelos.ShowExportarExcel = false;
            this.TModelos.ShowFiltro = true;
            this.TModelos.ShowHerramientas = false;
            this.TModelos.ShowImagen = false;
            this.TModelos.ShowImprimir = false;
            this.TModelos.ShowNuevo = true;
            this.TModelos.ShowRemover = true;
            this.TModelos.ShowSeleccionMultiple = true;
            this.TModelos.Size = new System.Drawing.Size(1117, 634);
            this.TModelos.TabIndex = 1;
            // 
            // MenuContextual
            // 
            this.MenuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonSeleccion,
            this.ToolBarButtonCopiar,
            this.ToolBarButtonDuplicar,
            this.ToolBarNuevo,
            this.ToolBarButtonAgregar});
            // 
            // ToolBarButtonSeleccion
            // 
            this.ToolBarButtonSeleccion.Name = "ToolBarButtonSeleccion";
            this.ToolBarButtonSeleccion.Text = "Selección múltiple";
            // 
            // ToolBarButtonCopiar
            // 
            this.ToolBarButtonCopiar.Name = "ToolBarButtonCopiar";
            this.ToolBarButtonCopiar.Text = "Copiar";
            // 
            // ToolBarButtonDuplicar
            // 
            this.ToolBarButtonDuplicar.Name = "ToolBarButtonDuplicar";
            this.ToolBarButtonDuplicar.Text = "Duplicar";
            // 
            // ToolBarNuevo
            // 
            this.ToolBarNuevo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarNuevoProducto,
            this.ToolBarNuevoValeEntrada,
            this.ToolBarNuevoValeSalida});
            this.ToolBarNuevo.Name = "ToolBarNuevo";
            this.ToolBarNuevo.Text = "Nuevo";
            // 
            // ToolBarNuevoProducto
            // 
            this.ToolBarNuevoProducto.Name = "ToolBarNuevoProducto";
            this.ToolBarNuevoProducto.Text = "Producto";
            // 
            // ToolBarNuevoValeEntrada
            // 
            this.ToolBarNuevoValeEntrada.Name = "ToolBarNuevoValeEntrada";
            this.ToolBarNuevoValeEntrada.Text = "Vale de Entrada";
            // 
            // ToolBarNuevoValeSalida
            // 
            this.ToolBarNuevoValeSalida.Name = "ToolBarNuevoValeSalida";
            this.ToolBarNuevoValeSalida.Text = "Vale de Salida";
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar ubicación";
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationType = Telerik.WinControls.UI.CollapsiblePanelAnimationType.Slide;
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.IsExpanded = false;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(1117, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(1117, 0, 364, 634);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radPanel1);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(0, 0);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(21, 634);
            this.radCollapsiblePanel1.TabIndex = 2;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radGridMedios);
            this.radPanel1.Controls.Add(this.radCommandBar2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(0, 0);
            this.radPanel1.TabIndex = 4;
            this.radPanel1.Text = "radPanel1";
            // 
            // radGridMedios
            // 
            this.radGridMedios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridMedios.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.radGridMedios.MasterTemplate.AllowAddNewRow = false;
            this.radGridMedios.MasterTemplate.AutoGenerateColumns = false;
            gridViewImageColumn1.FieldName = "Imagen";
            gridViewImageColumn1.HeaderText = "Imagen";
            gridViewImageColumn1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            gridViewImageColumn1.Name = "column1";
            gridViewImageColumn1.Width = 300;
            this.radGridMedios.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1});
            this.radGridMedios.MasterTemplate.ShowFilteringRow = false;
            this.radGridMedios.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridMedios.Name = "radGridMedios";
            this.radGridMedios.ShowGroupPanel = false;
            this.radGridMedios.Size = new System.Drawing.Size(0, 0);
            this.radGridMedios.TabIndex = 1;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.radCommandBar2.Size = new System.Drawing.Size(0, 30);
            this.radCommandBar2.TabIndex = 0;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarMedios});
            // 
            // ToolBarMedios
            // 
            this.ToolBarMedios.DisplayName = "Medios";
            this.ToolBarMedios.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonImagen,
            this.ToolBarButtonRemoverI});
            this.ToolBarMedios.Name = "ToolBarMedios";
            this.ToolBarMedios.StretchHorizontally = true;
            // 
            // ToolBarButtonImagen
            // 
            this.ToolBarButtonImagen.DisplayName = "Imagen";
            this.ToolBarButtonImagen.DrawText = true;
            this.ToolBarButtonImagen.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonImagen.Image")));
            this.ToolBarButtonImagen.Name = "ToolBarButtonImagen";
            this.ToolBarButtonImagen.Text = "Imagen";
            this.ToolBarButtonImagen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonRemoverI
            // 
            this.ToolBarButtonRemoverI.DisplayName = "Remover";
            this.ToolBarButtonRemoverI.DrawText = true;
            this.ToolBarButtonRemoverI.Image = ((System.Drawing.Image)(resources.GetObject("ToolBarButtonRemoverI.Image")));
            this.ToolBarButtonRemoverI.Name = "ToolBarButtonRemoverI";
            this.ToolBarButtonRemoverI.Text = "Remover";
            this.ToolBarButtonRemoverI.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ProductoModeloCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 634);
            this.Controls.Add(this.TModelos);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductoModeloCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Almacén MP: Cat. Productos-Modelos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MateriaPrimaCatalogo_Load);
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridMedios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridMedios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal Jaeger.UI.Almacen.Forms.ProductoModeloGridControl TModelos;
        private Telerik.WinControls.UI.RadContextMenu MenuContextual;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCopiar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonDuplicar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoProducto;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoValeEntrada;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoValeSalida;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonAgregar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonSeleccion;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarMedios;
        private Telerik.WinControls.UI.RadGridView radGridMedios;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonImagen;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonRemoverI;
    }
}
