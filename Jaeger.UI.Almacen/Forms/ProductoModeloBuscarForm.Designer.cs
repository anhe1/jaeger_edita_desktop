﻿namespace Jaeger.UI.Almacen.Forms
{
    partial class ProductoModeloBuscarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.TBuscar = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(806, 321);
            this.GridData.TabIndex = 4;
            this.GridData.DoubleClick += new System.EventHandler(this.GridData_DoubleClick);
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(361, 124);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // TBuscar
            // 
            this.TBuscar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TBuscar.Etiqueta = "Producto / Servicio:";
            this.TBuscar.Location = new System.Drawing.Point(0, 0);
            this.TBuscar.Name = "TBuscar";
            this.TBuscar.ShowAgregar = true;
            this.TBuscar.ShowBuscar = true;
            this.TBuscar.ShowCerrar = true;
            this.TBuscar.ShowExistencia = true;
            this.TBuscar.ShowFiltro = true;
            this.TBuscar.Size = new System.Drawing.Size(806, 30);
            this.TBuscar.TabIndex = 5;
            this.TBuscar.ButtonBuscar_Click += new System.EventHandler<System.EventArgs>(this.TBuscar_Buscar_Click);
            this.TBuscar.ButtonAgregar_Click += new System.EventHandler<System.EventArgs>(this.TBuscar_Seleccionar_Click);
            this.TBuscar.ButtonFiltro_Click += new System.EventHandler<System.EventArgs>(this.TBuscar_Filtro_Click);
            this.TBuscar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TBuscar_Cerrar_Click);
            // 
            // ProductoModeloBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 351);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TBuscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoModeloBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Materia Prima Buscar";
            this.Load += new System.EventHandler(this.ProductoModeloBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Common.Forms.ToolBarStandarBuscarControl TBuscar;
    }
}
