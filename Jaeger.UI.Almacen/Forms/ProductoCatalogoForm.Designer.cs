﻿namespace Jaeger.UI.Almacen.Forms {
    partial class ProductoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoCatalogoForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.GProducto = new Telerik.WinControls.UI.RadGridView();
            this.TProducto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.GModelos = new Telerik.WinControls.UI.RadGridView();
            this.TModelos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.ragGridMedios = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.TProveedor = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.DirProveedores = new System.ComponentModel.BackgroundWorker();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GProducto.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel4);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1325, 666);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.GProducto);
            this.splitPanel1.Controls.Add(this.TProducto);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1325, 215);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.006900222F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -5);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // GProducto
            // 
            this.GProducto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GProducto.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GProducto.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GProducto.Name = "GProducto";
            this.GProducto.Size = new System.Drawing.Size(1325, 185);
            this.GProducto.TabIndex = 6;
            // 
            // TProducto
            // 
            this.TProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProducto.Etiqueta = "";
            this.TProducto.Location = new System.Drawing.Point(0, 0);
            this.TProducto.Name = "TProducto";
            this.TProducto.ReadOnly = false;
            this.TProducto.ShowActualizar = true;
            this.TProducto.ShowAutorizar = false;
            this.TProducto.ShowCerrar = true;
            this.TProducto.ShowEditar = true;
            this.TProducto.ShowExportarExcel = false;
            this.TProducto.ShowFiltro = true;
            this.TProducto.ShowGuardar = false;
            this.TProducto.ShowHerramientas = false;
            this.TProducto.ShowImagen = false;
            this.TProducto.ShowImprimir = false;
            this.TProducto.ShowNuevo = true;
            this.TProducto.ShowRemover = true;
            this.TProducto.Size = new System.Drawing.Size(1325, 30);
            this.TProducto.TabIndex = 5;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 219);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1325, 226);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.0106157F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 7);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1325, 226);
            this.radSplitContainer2.TabIndex = 3;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.GModelos);
            this.splitPanel3.Controls.Add(this.TModelos);
            this.splitPanel3.Controls.Add(this.radCollapsiblePanel1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(1325, 226);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2201414F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(311, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // GModelos
            // 
            this.GModelos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GModelos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GModelos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GModelos.Name = "GModelos";
            this.GModelos.Size = new System.Drawing.Size(1304, 196);
            this.GModelos.TabIndex = 4;
            // 
            // TModelos
            // 
            this.TModelos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TModelos.Etiqueta = "";
            this.TModelos.Location = new System.Drawing.Point(0, 0);
            this.TModelos.Name = "TModelos";
            this.TModelos.ReadOnly = false;
            this.TModelos.ShowActualizar = false;
            this.TModelos.ShowAutorizar = false;
            this.TModelos.ShowCerrar = false;
            this.TModelos.ShowEditar = true;
            this.TModelos.ShowExportarExcel = false;
            this.TModelos.ShowFiltro = true;
            this.TModelos.ShowGuardar = false;
            this.TModelos.ShowHerramientas = false;
            this.TModelos.ShowImagen = false;
            this.TModelos.ShowImprimir = false;
            this.TModelos.ShowNuevo = true;
            this.TModelos.ShowRemover = true;
            this.TModelos.Size = new System.Drawing.Size(1304, 30);
            this.TModelos.TabIndex = 2;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radCollapsiblePanel1.IsExpanded = false;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(1304, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(1304, 0, 372, 226);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.ragGridMedios);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(0, 0);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(21, 226);
            this.radCollapsiblePanel1.TabIndex = 1;
            // 
            // ragGridMedios
            // 
            this.ragGridMedios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ragGridMedios.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ragGridMedios.MasterTemplate.AllowAddNewRow = false;
            this.ragGridMedios.MasterTemplate.AutoGenerateColumns = false;
            gridViewImageColumn1.FieldName = "Imagen";
            gridViewImageColumn1.HeaderText = "Imagen";
            gridViewImageColumn1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            gridViewImageColumn1.Name = "column1";
            gridViewImageColumn1.Width = 300;
            this.ragGridMedios.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1});
            this.ragGridMedios.MasterTemplate.ShowFilteringRow = false;
            this.ragGridMedios.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.ragGridMedios.Name = "ragGridMedios";
            this.ragGridMedios.ShowGroupPanel = false;
            this.ragGridMedios.Size = new System.Drawing.Size(0, 0);
            this.ragGridMedios.TabIndex = 0;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.TProveedor);
            this.splitPanel4.Location = new System.Drawing.Point(0, 449);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(1325, 217);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.003715509F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -2);
            this.splitPanel4.TabIndex = 2;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // TProveedor
            // 
            this.TProveedor.Caption = "";
            this.TProveedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TProveedor.Location = new System.Drawing.Point(0, 0);
            this.TProveedor.Name = "TProveedor";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TProveedor.Permisos = uiAction1;
            this.TProveedor.ShowActualizar = false;
            this.TProveedor.ShowAutorizar = false;
            this.TProveedor.ShowAutosuma = false;
            this.TProveedor.ShowCerrar = false;
            this.TProveedor.ShowEditar = true;
            this.TProveedor.ShowExportarExcel = false;
            this.TProveedor.ShowFiltro = true;
            this.TProveedor.ShowHerramientas = false;
            this.TProveedor.ShowImagen = false;
            this.TProveedor.ShowImprimir = false;
            this.TProveedor.ShowNuevo = true;
            this.TProveedor.ShowRemover = true;
            this.TProveedor.ShowSeleccionMultiple = false;
            this.TProveedor.Size = new System.Drawing.Size(1325, 217);
            this.TProveedor.TabIndex = 0;
            // 
            // DirProveedores
            // 
            this.DirProveedores.DoWork += new System.ComponentModel.DoWorkEventHandler(this.DirProveedores_DoWork);
            this.DirProveedores.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.DirProveedores_RunWorkerCompleted);
            // 
            // openFile
            // 
            this.openFile.Filter = "*.jpg|*.JPG";
            // 
            // ProductoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1325, 666);
            this.Controls.Add(this.radSplitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Almacén MP: Catálogo de Productos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GProducto.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GModelos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadGridView ragGridMedios;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private System.ComponentModel.BackgroundWorker DirProveedores;
        private System.Windows.Forms.OpenFileDialog openFile;
        public Common.Forms.GridStandarControl TProveedor;
        public Common.Forms.ToolBarStandarControl TModelos;
        public Telerik.WinControls.UI.RadGridView GModelos;
        public Telerik.WinControls.UI.RadGridView GProducto;
        public Common.Forms.ToolBarStandarControl TProducto;
    }
}