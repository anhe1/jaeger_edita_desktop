﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Contracts;

namespace Jaeger.UI.Almacen.Forms {
    public partial class CategoriasTreeViewForm : RadForm {
        #region declaraciones
        protected internal BindingList<IClasificacionSingle> _Clasificacion;
        protected internal BindingList<IClasificacionSingle> _Clasificacion2;
        protected internal BindingList<UnidadModel> _UnidadesAlmacen;
        protected internal BindingList<UnidadModel> _UnidadesZ;
        protected internal BindingList<UnidadModel> _UnidadesXY;
        protected internal IClasificacionService _ClasificacionService;
        protected internal ICatalogoProductoService _CatalogoService;
        protected internal BindingList<ProductoServicioModeloModel> _DataSource;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public CategoriasTreeViewForm() {
            InitializeComponent();
        }

        private void CategoriasTreeViewForm_Load(object sender, EventArgs e) {
            this.TControl.TBar.Actualizar.Click += TControl_Actualizar_Click;
            this.TControl.TBar.Guardar.Click += TControl_Guardar_Click;
            this.TControl.TBar.Cerrar.Click += TControl_Cerrar_Click;
        }

        protected virtual void TControl_Actualizar_Click(object sender, EventArgs e) {
            var espera = new Common.Forms.Wating4Form(() => {
                this._Clasificacion = new BindingList<IClasificacionSingle>(this._ClasificacionService.GetList<ClasificacionSingle>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>()).ToList<IClasificacionSingle>());
            }, "Consultando...", false, false);
            espera.ShowDialog(this);

            this.TControl.IndexChanged += TControl_IndexChanged;
            this.TControl.TreeData.DataSource = this._Clasificacion;
            this.TControl.TreeData.DisplayMember = "Clase";
            this.TControl.TreeData.ValueMember = "IdCategoria";
            this.TControl.TreeData.ParentMember = "IdSubCategoria";
            this.TControl.TreeData.ChildMember = "IdCategoria";
            this.TControl.TreeData.SelectedNode = this.TControl.TreeData.Nodes[0];
            this.TControl.TreeData.ExpandAll();
            this.TControl.GridData.DataSource = this._DataSource;
        }

        private void TControl_IndexChanged(object sender, object e) {
            var d0 = e as ModeloXDetailModel;
            d0.Secuencia = this.TControl.NewIndex;
        }

        protected virtual void TControl_Guardar_Click(object sender, EventArgs e) {

        }

        protected virtual void TControl_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void Consultar() {
            this._Clasificacion = new BindingList<IClasificacionSingle>(this._ClasificacionService.GetList<ClasificacionSingle>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>()).ToList<IClasificacionSingle>());
            this._Clasificacion2 = new BindingList<IClasificacionSingle>(this._Clasificacion);
            var queryProductos = Aplication.Almacen.Services.CatalogoProductoService.Query().ByAlmacen(this._CatalogoService.Almacen).Activo().Build();
            this._DataSource = new BindingList<ProductoServicioModeloModel>(this._CatalogoService.GetList<ProductoServicioModeloModel>(queryProductos).ToList());
            this._UnidadesAlmacen = new BindingList<UnidadModel>(this._CatalogoService.GetList<UnidadModel>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>().ToList()).ToList());
            this._UnidadesXY = new BindingList<UnidadModel>(this._UnidadesAlmacen);
            this._UnidadesZ = new BindingList<UnidadModel>(this._UnidadesAlmacen);
        }
    }
}
