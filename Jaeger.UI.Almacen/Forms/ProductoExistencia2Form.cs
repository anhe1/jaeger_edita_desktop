﻿using Jaeger.Domain.Almacen.Entities;
using Jaeger.UI.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Almacen.Forms {
    public partial class ProductoExistencia2Form : RadForm {
        protected internal List<ProductoXModelo> _DataSource;
        public ProductoExistencia2Form() {
            InitializeComponent();
        }

        private void ProductoExistencia2Form_Load(object sender, EventArgs e) {
            this.TExistencia.Actualizar.Click += TExistencia_Actualizar_Click;
        }

        public virtual void TExistencia_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._DataSource;
        }

        public virtual void Consultar() {

        }
    }
}
