﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.MySql.Services;
using Jaeger.Domain.Shatterdome.Contracts;
using Jaeger.Domain.Shatterdome.Entities;
using Jaeger.DataAccess.Services;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Dataaccess.Shatterdome.Repositories {
    public class SqlSugarShatterdomeRepository : MySqlSugarContext<ShatterdomeModel>, ISqlShatterdomeRepository {
        public SqlSugarShatterdomeRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener la configuración de la empresa por su rFC
        /// </summary>
        public ShatterdomeModel GetByRFC(string rfc) {
            try {
                return this.Db.Queryable<ShatterdomeModel>().Where(it => it.RFC == rfc).Single();
            } catch (System.Exception ex) {
                try {
                    var d = this.Db.Queryable<ShatterdomeModel>().Where(it => it.RFC == rfc).ToSql();
                    var d1 = this.GetMapper<ShatterdomeModel>(d.Key, d.Value).First();
                    return d1;
                } catch (System.Exception ex2) {
                    LogErrorService.LogWrite(ex.Message);
                    this.Message = ex2.Message;
                }
                LogErrorService.LogWrite(ex.Message);
                this.Message = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// desactivar empresa
        /// </summary>
        public ShatterdomeModel Delete(ShatterdomeModel model) {
            this.IsError = false;
            model.Activo = false;
            try {
                this.Db.Updateable(model).ExecuteCommand();
            } catch (System.Exception ex) {
                this.Message = ex.Message;
                this.IsError = true;
            }
            return model;
        }

        public new IEnumerable<ShatterdomeDetailModel> GetList() {
            return this.Db.Queryable<ShatterdomeDetailModel>().ToList();
        }

        public ShatterdomeModel Save(ShatterdomeModel model) {
            if (model.Id == 0) {
                model.Creo = this.User;
                model.FechaNuevo = System.DateTime.Now;
                var _insert = this.Db.Insertable(model);
                model.Id = this.Execute(_insert);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = System.DateTime.Now;
                var _update = this.Db.Updateable(model);
                this.Execute(_update);
            }
            return model;
        }

        public bool TestConfig(DataBaseConfiguracion configuracion) {
            var maintenance = new SqlSugarMaintenance(new DataBaseConfiguracion {
                Database = configuracion.Database,
                HostName = configuracion.HostName,
                Charset = configuracion.Charset,
                ForcedWrite = configuracion.ForcedWrite,
                PageSize = configuracion.PageSize,
                Password = configuracion.Password,
                Pooling = configuracion.Pooling,
                PortNumber = configuracion.PortNumber,
                UserID = configuracion.UserID,
                ConnectionLifeTime = configuracion.ConnectionLifeTime,
                ConnectionTimeout = configuracion.ConnectionTimeout,
                ModoSSL = configuracion.ModoSSL
            });

            if (maintenance.ConnectionOpen() == false) {
                this.Message = maintenance.Message;
                return false;
            }
            return true;
        }
    }
}
