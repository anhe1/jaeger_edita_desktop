﻿using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Shatterdome.Contracts;
using Jaeger.Domain.Shatterdome.Entities;

namespace Jaeger.Dataaccess.Shatterdome.Repositories {
    public class MySqlShatterdomeRepository : RepositoryMaster<ShatterdomeModel>, ISqlShatterdomeRepository {
        public MySqlShatterdomeRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }

        public ShatterdomeModel GetById(int index) {
            var sqlComand = new MySqlCommand() { CommandText = "select empr.* from empr where empr_id = @index" };
            sqlComand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlComand);
            var mapper = new DataNamesMapper<ShatterdomeModel>();
            return mapper.Map(tabla).FirstOrDefault();
        }

        public ShatterdomeModel GetByRFC(string rfc) {
            if (rfc == null)
                return null;
            var sqlComand = new MySqlCommand() { CommandText = "select empr.* from empr where empr_rfc = @rfc limit 1" };
            sqlComand.Parameters.AddWithValue("@rfc", rfc.ToUpper());
            var tabla = this.ExecuteReader(sqlComand);
            var mapper = new DataNamesMapper<ShatterdomeModel>();
            return mapper.Map(tabla).FirstOrDefault();
        }

        public IEnumerable<ShatterdomeDetailModel> GetList() {
            var sqlComand = new MySqlCommand() { CommandText = "select empr.* from empr" };
            var tabla = this.ExecuteReader(sqlComand);
            var mapper = new DataNamesMapper<ShatterdomeDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(ShatterdomeModel item) {
            throw new System.NotImplementedException();
        }

        public bool TestConfig(DataBaseConfiguracion shatterdome) {
            throw new System.NotImplementedException();
        }

        public int Update(ShatterdomeModel item) {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// desactivar empresa
        /// </summary>
        public ShatterdomeModel Delete(ShatterdomeModel model) {
            model.Activo = false;
            return model;
        }

        IEnumerable<ShatterdomeModel> IGenericRepository<ShatterdomeModel>.GetList() {
            throw new System.NotImplementedException();
        }
    }
}
