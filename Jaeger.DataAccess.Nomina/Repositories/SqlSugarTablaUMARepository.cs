﻿using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using System.Collections.Generic;

namespace Jaeger.DataAccess.Repositories
{
    public class SqlSugarTablaUMA : SqlSugarContext<TablaUMAModel>, ISqlTablaUMARepository
    {
        public SqlSugarTablaUMA(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        public TablaUMAModel GetByActivo()
        {
            return this.Db.Queryable<TablaUMAModel>().OrderBy(it => it.FechaInicio, OrderByType.Desc).First();
        }

        public BindingList<TablaUMAModel> Save(BindingList<TablaUMAModel> items)
        {
            return items;
        }

        List<TablaUMAModel> ISqlTablaUMARepository.GetList() {
            throw new System.NotImplementedException();
        }
    }
}
