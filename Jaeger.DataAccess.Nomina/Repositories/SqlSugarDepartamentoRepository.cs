﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de departamentos
    /// </summary>
    public class SqlSugarDepartamentoRepository : SqlSugarContext<DepartamentoModel>, ISqlDepartamentoRepository {

        public SqlSugarDepartamentoRepository(Domain.DataBase.Entities.DataBaseConfiguracion objeto) : base(objeto) {
            this.Puestos = new SqlSugarDeptoPuestos(objeto);
        }

        public ISqlDeptoPuestosRepository Puestos { get; set; }

        public IEnumerable<DepartamentoDetailModel> GetList(bool onlyActive) {
            //Manual mode
            var result = this.Db.Queryable<DepartamentoDetailModel>().Mapper((itemModel, cache) => {
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.Id).ToList();
                    return this.Db.Queryable<DepartamentoPuestoModel>().Where(it => allIds.Contains(it.IdDepto)).ToList();//Execute only once
                });
                itemModel.Puestos = new BindingList<DepartamentoPuestoModel>(allItems.Where(it => it.IdDepto == itemModel.Id).ToList());
            }).ToList();
            return result;
        }

        public DepartamentoDetailModel Save(DepartamentoDetailModel objeto) {
            if (objeto.Id == 0)
                objeto.Id = this.Insert(objeto);
            else
                this.Update(objeto);
            return objeto;
        }

        public IEnumerable<DepartamentoModel> Save(List<DepartamentoModel> datos) {
            //for (int i = 0; i < datos.Count; i++) {
            //    if (datos[i].Id == 0) {
            //        datos[i].Id = this.Insert(datos[i]);
            //    }
            //    else {
            //        this.Update(datos[i]);
            //    }
            //    if (datos[i].Id > 0 && datos[i].Puestos.Count > 0) {
            //        for (int idPuesto = 0; idPuesto < datos[i].Puestos.Count; idPuesto++) {
            //            if (datos[i].Puestos[idPuesto].Id == 0) {
            //                datos[i].Puestos[idPuesto].IdDepto = datos[i].Id;
            //                datos[i].Puestos[idPuesto].Id = this.Puestos.Insert(datos[i].Puestos[idPuesto]);
            //            }
            //            else
            //                this.Puestos.Update(datos[i].Puestos[idPuesto]);
            //        }
            //    }
            //}
            return datos;
        }

        public bool Create() {
            try {
                this.Db.CodeFirst.InitTables<DepartamentoModel, DepartamentoPuestoModel, DepartamentoFuncionModel>();
                return true;
            }
            catch (SqlSugar.VersionExceptions ex) {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        
    }
}
