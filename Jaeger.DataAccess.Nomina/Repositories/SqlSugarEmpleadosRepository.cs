﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// catalogo de empleados
    /// </summary>
    public class SqlSugarEmpleadosRepository : MySqlSugarContext<EmpleadoModel>, ISqlEmpleadoRepository {

        /// <summary>
        /// cosntructor
        /// </summary>
        public SqlSugarEmpleadosRepository(Domain.DataBase.Entities.DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEmpleadoDetailModel Save(IEmpleadoDetailModel model) {
            if (model.IdEmpleado == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdEmpleado = this.Insert(model as EmpleadoModel);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model as EmpleadoModel);
            }
            return model;
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = @"SELECT * FROM _ctlemp @condiciones";

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        /// <summary>
        /// obtener indice de empleado por su registro federal de contribuyentes
        /// </summary>
        /// <param name="rfc">Registro Federal de Contribuyentes</param>
        /// <returns>numero entero que representa el indice</returns>
        //public int ReturnId(string rfc) {
        //    var indice = 0;
        //    try {
        //        indice = this.Db.Queryable<EmpleadoModel>().Where(it => it.RFC == rfc).Select(it => it.Id).Single();
        //    } catch (SqlSugarException ex) {
        //        Console.WriteLine(ex.Message);
        //        indice = this.Db.Queryable<EmpleadoModel>().Where(it => it.RFC == rfc).Select(it => it.Id).First();
        //    }
        //    return indice;
        //}

        //public EmpleadoDetailModel GetEmpleado(int index) {
        //    return this.GetEmpleados(index, true).FirstOrDefault();
        //}

        /// <summary>
        /// lista de empleados con detalles del expediente
        /// </summary>
        /// <param name="index">si es mayor de cero obtiene el registro relacionado al indice</param>
        /// <param name="onlyActive">false incluye registros no activos</param>
        //public IEnumerable<EmpleadoDetailModel> GetEmpleados(int index, bool onlyActive = true) {
        //    //Manual mode
        //    var result = this.Db.Queryable<EmpleadoDetailModel>().Mapper((itemModel, cache) => {
        //        var allItems = cache.Get(orderList => {
        //            var allIds = orderList.Select(it => it.Id).ToList();
        //            return this.Db.Queryable<EmpleadoConceptoNomina>().WhereIF(onlyActive == true, it => it.Activo == true).Where(it => allIds.Contains(it.IdEmpleado)).ToList();
        //        });
        //        itemModel.Conceptos = new System.ComponentModel.BindingList<EmpleadoConceptoNomina>(allItems.Where(it => it.IdEmpleado == itemModel.Id).ToList());
        //    }).WhereIF(onlyActive, it => it.Activo == true).WhereIF(index > 0, it => it.Id == index).ToList();
        //    return result;
        //}

        /// <summary>
        /// lista de empleados con detalles de los conceptos de nomina
        /// </summary>
        /// <param name="index">si es mayor de cero obtiene el registro relacionado al indice</param>
        /// <param name="onlyActive">false incluye registros no activos</param>
        //public IEnumerable<EmpleadoDetailSingleModel> GetEmpleadosSingle(int index, bool onlyActive = true) {
        //    //Manual mode
        //    var result = this.Db.Queryable<EmpleadoDetailSingleModel>().Mapper((itemModel, cache) => {
        //        var allItems = cache.Get(orderList => {
        //            var allIds = orderList.Select(it => it.Id).ToList();
        //            return this.Db.Queryable<EmpleadoConceptoNomina>().WhereIF(onlyActive, it => it.Activo == true).Where(it => allIds.Contains(it.IdEmpleado)).ToList();//Execute only once
        //        });
        //        itemModel.Conceptos = new System.ComponentModel.BindingList<EmpleadoConceptoNomina>(allItems.Where(it => it.IdEmpleado == itemModel.Id).ToList()); //Every time it's executed
        //    }).WhereIF(onlyActive, it => it.Activo == true).WhereIF(index > 0, it => it.Id == index).ToList();
        //    return result;
        //}

        /// <summary>
        /// insertar lista de objetos empleado
        /// </summary>
        //public int Insert(EmpleadoModel[] data) {
        //    return this.Db.Insertable(data).ExecuteCommand();
        //}

        //public EmpleadoDetailModel Save(EmpleadoDetailModel item) {
        //    if (item.Id == 0) {
        //        item.Id = this.Db.Insertable<EmpleadoDetailModel>(item).ExecuteReturnIdentity();
        //    }
        //    else {
        //        item.FechaModifica = DateTime.Now;
        //        this.Db.Updateable<EmpleadoDetailModel>(item).ExecuteCommand();
        //    }

        //    if (item.Conceptos.Count > 0) {
        //        for (int i = 0; i < item.Conceptos.Count; i++) {
        //            if (item.Conceptos[i].Id == 0) {
        //                item.Conceptos[i].IdEmpleado = item.Id;
        //                item.Conceptos[i].Id = this.Db.Insertable<EmpleadoConceptoNomina>(item.Conceptos[i]).ExecuteReturnIdentity();
        //            }
        //            else {
        //                this.Db.Updateable<EmpleadoConceptoNomina>(item.Conceptos[i]).ExecuteCommand();
        //            }
        //        }
        //    }
        //    return item;
        //}

        /// <summary>
        /// obtener lista de empleados
        /// </summary>
        //public IEnumerable<EmpleadoModel> GetList(bool activos = true) {
        //    return this.Db.Queryable<EmpleadoModel>().WhereIF(activos == true, it => it.Activo == true).OrderBy(it => it.PrimerApellido).ToList();
        //}

        /// <summary>
        /// procedimiento para crear tabla de catalogo de empleados
        /// </summary>
        public new void CreateTable() {
            //this.Db.CodeFirst.InitTables(typeof(EmpleadoModel));
            //this.Db.CodeFirst.InitTables(typeof(EmpleadoConceptoNomina));
            //this.Db.CodeFirst.InitTables(typeof(DepartamentoModel));
            //this.Db.CodeFirst.InitTables(typeof(DepartamentoPuestoModel));
        }

    }
}
