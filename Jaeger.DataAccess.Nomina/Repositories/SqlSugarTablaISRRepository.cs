﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarTablaISRRepository : SqlSugarContext<TablaImpuestoSobreRentaModel>, ISqlTablaISRRepository {
        public SqlSugarTablaISRRepository(DataBaseConfiguracion objeto) : base(objeto) {
        }

        public BindingList<TablaImpuestoSobreRentaModel> Save(BindingList<TablaImpuestoSobreRentaModel> items) {
            for (int i = 0; i < items.Count; i++) {
                if (items[i].Id == 0)
                    items[i].Id = this.Insert(items[i]);
                else
                    this.Update(items[i]);
            }
            return items;
        }
    }
}
