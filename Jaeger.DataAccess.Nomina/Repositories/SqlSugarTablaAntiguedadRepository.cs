using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarTablaAntiguedad : SqlSugarContext<AntiguedadModel>, ISqlTablaAntiguedadRepository {
        public SqlSugarTablaAntiguedad(Domain.DataBase.Entities.DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// almacenar tabla de antiguedades
        /// </summary>
        /// <param name="tabla">List<ViewModelAntiguedad></param>
        public BindingList<AntiguedadModel> Save(BindingList<AntiguedadModel> tabla) {
            for (int i = 0; i < tabla.Count; i++) {
                if (tabla[i].Id == 0) {
                    tabla[i].Id = this.Db.Insertable(tabla[i]).ExecuteReturnIdentity();
                }
                else {
                    this.Db.Updateable(tabla[i]).ExecuteCommand();
                }
            }
            return tabla;
        }
    }
}