﻿using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories
{
    public class SqlSugarTablaImpuestoSobreRenta : SqlSugarContext<TablaImpuestoSobreRentaModel>, ISqlTablaImpuestoSobreRentaRepository
    {
        public SqlSugarTablaImpuestoSobreRenta(DataBaseConfiguracion objeto) : base(objeto)
        {
        }

        //public BindingList<ViewModelTablaImpuestoSobreRenta> GetList()
        //{
        //    return new BindingList<ViewModelTablaImpuestoSobreRenta>(this.Db.Queryable<ViewModelTablaImpuestoSobreRenta>().ToList());
        //}

        public TablaImpuestoSobreRentaModel GetListBy(int periodo)
        {
            return this.Db.Queryable<TablaImpuestoSobreRentaModel>().Where(it => it.Periodo == periodo).ToList()[0];
        }
    }
}
