﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.DataAccess.Repositories {
    public class MySqlControlNominaRepository : RepositoryMaster<ControlNominaModel>, ISqlControlNominaRepository {
        public MySqlControlNominaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }

        public ControlNominaModel GetById(int index) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<ControlNominaModel> GetList(bool onlyActive) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select * from _nmnctrl"
            };

            throw new System.NotImplementedException();
        }

        public IEnumerable<ControlNominaModel> GetList() {
            throw new System.NotImplementedException();
        }

        public int Insert(ControlNominaModel item) {
            throw new System.NotImplementedException();
        }

        public int Update(ControlNominaModel item) {
            throw new System.NotImplementedException();
        }
    }
}
