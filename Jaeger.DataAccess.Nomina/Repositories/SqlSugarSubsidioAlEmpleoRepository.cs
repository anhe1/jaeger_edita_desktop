﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarSubsidioAlEmpleoRepository : SqlSugarContext<TablaSubsidioAlEmpleoModel>, ISqlSubsidioAlEmpleoRepository {
        public SqlSugarSubsidioAlEmpleoRepository(Domain.DataBase.Entities.DataBaseConfiguracion objeto) : base(objeto) {
        }

        public TablaSubsidioAlEmpleoModel GetListBy(int periodo) {
            return this.Db.Queryable<TablaSubsidioAlEmpleoModel>().Where(it => it.Periodo == periodo).ToList()[0];
        }

        public BindingList<TablaSubsidioAlEmpleoModel> Save(BindingList<TablaSubsidioAlEmpleoModel> items) {
            for (int i = 0; i < items.Count; i++) {
                if (items[i].Id == 0)
                    items[i].Id = this.Insert(items[i]);
                else
                    this.Update(items[i]);
            }
            return items;
        }
    }
}
