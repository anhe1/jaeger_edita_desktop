﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarComprobanteNominaRepository : MySqlSugarContext<ComplementoNominaModel>, ISqlComprobanteNominaRepository {
        public SqlSugarComprobanteNominaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener listado de comprobantes de nómina por el indice del control 
        /// </summary>
        /// <param name="index">indice de control (_cfdnmn_nmnctrl_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(int index, bool onlyActive = true) {
            var sqlCommand = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,
                                      _cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,
                                      _cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,
                                      _cfdi_rfce,_cfdi_rfcr, _cfdi_id, _cfdi_url_xml, _cfdi_url_pdf 
                                from _cfdi, _cfdnmn 
                                where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 and _cfdnmn_nmnctrl_id = @index ";

            var parameter = new SugarParameter("@index", index);
            if (onlyActive == false) {
                sqlCommand = sqlCommand.Replace("and _cfdnmn_a = 1", "");
            }

            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.Db.Ado.GetDataTable(sqlCommand, parameter);
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de comprobantes de nomina por el nombre del empleado
        /// </summary>
        /// <param name="employee">nombre del empleado</param>
        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(string employee) {
            var sqlCommand = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,
                                      _cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,
                                      _cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id 
                               from _cfdi, _cfdnmn 
                               where _cfdnmn_cfdi_id = _cfdi_id and _cfdi_nomr like @empleado";

            var parameter = new SugarParameter("@empleado", employee);
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.Db.Ado.GetDataTable(sqlCommand, parameter);
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de comprobantes por campo uuid
        /// </summary>
        /// <param name="idDocumento">folio fiscal (uuid)</param>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(string idDocumento, bool onlyActive) {
            var sqlCommand = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,
                                      _cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,
                                      _cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id 
                              from _cfdi, _cfdnmn 
                              where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 and _cfdi_uuid like @uuid";

            var parameter = new SugarParameter("@uuid", idDocumento);
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.Db.Ado.GetDataTable(sqlCommand, parameter);
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateBy"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="depto"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(CFDIFechaEnum dateBy, DateTime dateStart, DateTime dateEnd, string depto, string employee) {
            var wheres = string.Empty;

            if (depto != "") {
                wheres = " and _cfdnmn_depto like @depto";
            }

            if (employee != "") {
                wheres = string.Concat(wheres, " and _cfdi_nomr like @employee");
            }

            if (dateBy == CFDIFechaEnum.FechaEmision) {
                wheres = string.Concat(wheres, " and (_cfdi_fecems >= '@fec_inicio') and (_cfdi_fecems < '@fec_fin')");
            } else if (dateBy == CFDIFechaEnum.FechaDePago) {
                wheres = string.Concat(wheres, " and (_cfdnmn_fchpgo >= '@fec_inicio') and (_cfdnmn_fchpgo < '@fec_fin')");
            } else if (dateBy == CFDIFechaEnum.FechaTimbre) {
                wheres = string.Concat(wheres, " and (_cfdi_feccert >= '@fec_inicio') and (_cfdi_feccert <= '@fec_fin')");
            }

            wheres = wheres.Replace("@fec_inicio", dateStart.ToString("yyyy/MM/dd"));
            wheres = wheres.Replace("@fec_fin", dateEnd.AddDays(1).ToString("yyyy/MM/dd")); 

            var sqlCommand = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,
                                    _cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,
                                    _cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_rfce,_cfdnmn_rfc,_cfdi_total,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_id, _cfdi_uuid 
                            from _cfdnmn,_cfdi,_ctlemp 
                            where _cfdnmn_cfdi_id=_cfdi_id and _cfdnmn_drctr_id=_ctlemp_drctr_id " + wheres;
            
            var parameter1 = new SugarParameter("@depto", depto);
            var parameter2 = new SugarParameter("@employee", employee);
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.Db.Ado.GetDataTable(sqlCommand, new SugarParameter[] { parameter1, parameter2 });
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        ///  obtener lista de resumen de nomina para tabla dinamica
        /// </summary>
        /// <param name="year"></param>
        /// <param name="estado"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public DataTable GetResumen(int year, int month = 0, int estado = -1) {
            string sqlCommand = @"select _nmnprt_id as Indice,_nmnprt_tipo as Tipo,_nmnprt_dias as Dias,_nmnprt_hrsxtr as HorasExtra,_nmnprt_dsincp as DiasIncapacidad,_nmnprt_impgrv as ImporteGravado,_nmnprt_impext as ImporteExento,_nmnprt_dscnt as Descuento,
                                         _nmnprt_imppgd ImportePagado,_nmnprt_tphrs as TipoHoras,_nmnprt_clv as Clave,_nmnprt_cncpt as Concepto,_nmnprt_numem as NumEmpleado,_cfdnmn_fchpgo as FechaPagoNoSemana, _cfdnmn_numem EmpleadoNum, _cfdnmn_rfc EmpleadoRfc,
                                         _cfdi_nomr Empleado, _cfdi_estado as Estado, _cfdi_uuid iddocumento 
                                  from _nmnprt, _cfdnmn, _cfdi 
                                  where _nmnprt_cfdnmn_id=_cfdnmn_id and _cfdnmn_cfdi_id=_cfdi_id and _cfdi_uuid<>'' and year(_cfdnmn_fchpgo) = @anio and month(_cfdnmn_fchpgo) = @mes and _cfdi_estado=@estado";
            if (month == 0)
                sqlCommand = @"select _nmnprt_id as Indice,_nmnprt_tipo as Tipo,_nmnprt_dias as Dias,_nmnprt_hrsxtr as HorasExtra,_nmnprt_dsincp as DiasIncapacidad,_nmnprt_impgrv as ImporteGravado,_nmnprt_impext as ImporteExento,_nmnprt_dscnt as Descuento,
                                      _nmnprt_imppgd ImportePagado,_nmnprt_tphrs as TipoHoras,_nmnprt_clv as Clave,_nmnprt_cncpt as Concepto,_nmnprt_numem as NumEmpleado,_cfdnmn_fchpgo as FechaPagoNoSemana, _cfdnmn_numem EmpleadoNum, _cfdnmn_rfc EmpleadoRfc, 
                                      _cfdi_nomr Empleado, _cfdi_estado as Estado, _cfdi_uuid iddocumento 
                               from _nmnprt, _cfdnmn, _cfdi 
                               where _nmnprt_cfdnmn_id=_cfdnmn_id and _cfdnmn_cfdi_id=_cfdi_id and _cfdi_uuid<>'' and year(_cfdnmn_fchpgo) = @anio and _cfdi_estado=@estado";
            if (estado == -1)
                sqlCommand = sqlCommand.Replace("and _cfdi_estado=@estado", ";");
            return this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>() {
                new SugarParameter("@mes", month),
                new SugarParameter("@anio", year),
                new SugarParameter("@estado", estado.ToString())
            });
        }

        public DataTable GetResumen(int indice) {
            string sqlCommand = @"select _nmnprt_id as Indice,_nmnprt_tipo as Tipo,_nmnprt_dias as Dias,_nmnprt_hrsxtr as HorasExtra,_nmnprt_dsincp as DiasIncapacidad,_nmnprt_impgrv as ImporteGravado,_nmnprt_impext as ImporteExento,_nmnprt_dscnt as Descuento,
                                        _nmnprt_imppgd ImportePagado,_nmnprt_tphrs as TipoHoras,_nmnprt_clv as Clave,_nmnprt_cncpt as Concepto,_nmnprt_numem as NumEmpleado,_cfdnmn_fchpgo as FechaPagoNoSemana, _cfdnmn_numem EmpleadoNum, _cfdnmn_rfc EmpleadoRfc, 
                                        _cfdi_nomr Empleado, _cfdi_estado as Estado, _cfdi_uuid iddocumento 
                                 from _nmnprt, _cfdnmn, _cfdi 
                                 where _nmnprt_cfdnmn_id=_cfdnmn_id and _cfdnmn_cfdi_id=_cfdi_id and _cfdi_uuid<>'' and _cfdnmn_nmnctrl_id=@index";
            return this.Db.Ado.GetDataTable(sqlCommand, new List<SugarParameter>() {
                new SugarParameter("@index", indice.ToString())
            });
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,
                                      _cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,
                                      _cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,
                                      _cfdi_rfce,_cfdi_rfcr, _cfdi_id, _cfdi_url_xml, _cfdi_url_pdf 
                                from _cfdi, _cfdnmn 
                                where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 @condiciones";

            if (typeof(T1) == typeof(DepartamentoSingleModel)) {
                sqlCommand = "select distinct(_cfdnmn_depto) from _cfdnmn where _cfdnmn_depto <> '';";
            } else if (typeof(T1) == typeof(EmpleadoSingleModel)) {
                sqlCommand = "select distinct(_cfdi_nomr),_cfdnmn_numem from _cfdnmn,_cfdi where _cfdnmn_cfdi_id=_cfdi_id and _cfdi_nomr <> '' order by _cfdnmn_numem asc;";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        #region metodos de la version anteior
        
        #endregion
    }
}
