﻿using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de conceptos de nomina
    /// </summary>
    public class SqlSugarNominaConceptoRepository : SqlSugarContext<NominaConceptoModel>, ISqlNominaConceptoRepository {

        public SqlSugarNominaConceptoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener listado de conceptos de nomina
        /// </summary>
        /// <param name="onlyAcive">false incluye todos los registros desactivados</param>
        public IEnumerable<NominaConceptoModel> GetConceptos(bool onlyAcive) {
            return this.Db.Queryable<NominaConceptoModel>().WhereIF(onlyAcive, it => it.Activo == true).OrderBy(it => it.Orden, SqlSugar.OrderByType.Asc).ToList();
        }

        public NominaConceptoModel Save(NominaConceptoModel model) {
            if (model.IdConcepto == 0) {
                model.IdConcepto = this.Insert(model);
            }
            else {
                this.Update(model);
            }
            return model;
        }

        /// <summary>
        /// lista de tablas de sistema
        /// </summary>
        /// <param name="onlyActive">false incluye los registros inactivos</param>
        public IEnumerable<TablaProduccionModel> GetTablasSistema(bool onlyActive) {
            return this.Db.Queryable<TablaProduccionModel>().WhereIF(onlyActive, it => it.Activo == true).ToList();
        }

        /// <summary>
        /// almacenar lista de tablas de sistema
        /// </summary>
        /// <param name="models">lista de tablas</param>
        public List<TablaProduccionModel> Save(List<TablaProduccionModel> models) {
            for (int i = 0; i < models.Count; i++) {
                if (models[i].IdTabla == 0) {
                    models[i].IdTabla = this.Db.Insertable<TablaProduccionModel>(models[i]).ExecuteReturnIdentity();
                }
                else {
                    this.Db.Updateable<TablaProduccionModel>(models[i]).ExecuteCommand();
                }
            }
            return models;
        }

        /// <summary>
        /// crear tablas
        /// </summary>
        public bool CrearTabla() {
            this.Db.CodeFirst.InitTables<TablaProduccionModel>();
            return this.CreateTable();
        }
    }
}
