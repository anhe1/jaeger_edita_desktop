﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarFunciones : SqlSugarContext<DepartamentoFuncionModel>, ISqlFuncionesRepository
    {
        public SqlSugarFunciones(Domain.DataBase.Entities.DataBaseConfiguracion configuracion)
            : base(configuracion)
        {

        }

        public BindingList<DepartamentoFuncionModel> GetLitBy(int index, bool activo = true)
        {
            return new BindingList<DepartamentoFuncionModel>(this.Db.Queryable<DepartamentoFuncionModel>().Where(it => it.IdPuesto == index).WhereIF(activo, it => it.Activo == true).OrderBy(it => it.Secuencia, SqlSugar.OrderByType.Asc).ToList());
        }

        public DepartamentoFuncionModel Save(DepartamentoFuncionModel funcion)
        {
            if (funcion.Id == 0)
                funcion.Id = this.Insert(funcion);
            else
                this.Update(funcion);
            return funcion;
        }
    }
}
