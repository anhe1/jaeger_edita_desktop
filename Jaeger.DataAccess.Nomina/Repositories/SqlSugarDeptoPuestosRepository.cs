﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

/// <summary>
/// develop: anhe 23082338
/// purpose: 
/// </summary>
namespace Jaeger.DataAccess.Repositories
{
    public class SqlSugarDeptoPuestos : SqlSugarContext<DepartamentoPuestoModel>, ISqlDeptoPuestosRepository
    {
        public SqlSugarDeptoPuestos(DataBaseConfiguracion configuracion) : base(configuracion)
        {
            this.Funciones = new SqlSugarFunciones(configuracion);
        }

        public ISqlFuncionesRepository Funciones { get; set; }

        /// <summary>
        /// obtener listado de puestos relacionados al indice del departamento
        /// </summary>
        /// <param name="index">indice del departamento</param>
        /// <param name="activo">falso si debe obtener registros no activos</param>
        /// <returns></returns>
        public BindingList<DepartamentoPuestoModel> GetListBy(int index, bool activo = true)
        {
            return new BindingList<DepartamentoPuestoModel>(this.Db.Queryable<DepartamentoPuestoModel>().Where(it => it.IdDepto == index).WhereIF(activo, it => it.Activo == true).OrderBy(it => it.Secuencia, SqlSugar.OrderByType.Asc).ToList());
        }

        public DepartamentoPuestoModel Save(DepartamentoPuestoModel puesto)
        {
            if (puesto.Id == 0)
                puesto.Id = this.Insert(puesto);
            else
                this.Update(puesto);
            return puesto;
        }

        public bool Create()
        {
            try
            {
                this.Db.CodeFirst.InitTables(typeof(DepartamentoPuestoModel));
                return true;
            }
            catch (SqlSugar.VersionExceptions ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            return false;
        }
    }
}
