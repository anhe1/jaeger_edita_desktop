﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.DataAccess.Repositories {
    public class MySqlComprobanteNominaRepository : RepositoryMaster<ComplementoNominaModel>, ISqlComprobanteNominaRepository {
        public MySqlComprobanteNominaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"update _cfdnmn set _cfdnmn_a = 0 where _cfdnmn_id = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ComplementoNominaModel GetById(int index) {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// obtener listado de comprobantes de nómina por el indice del control 
        /// </summary>
        /// <param name="index">indice de control (_cfdnmn_nmnctrl_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(int index, bool onlyActive = true) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id, _cfdi_url_xml, _cfdi_url_pdf 
                                from _cfdi, _cfdnmn 
                                where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 and _cfdnmn_nmnctrl_id = @index "
            };

            sqlCommand.Parameters.AddWithValue("@index", index);
            if (onlyActive == false) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("and _cfdnmn_a = 1", "");
            }
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.ExecuteReader(sqlCommand);
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(string employee) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,
                                      _cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,
                                      _cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id 
                               from _cfdi, _cfdnmn 
                               where _cfdnmn_cfdi_id = _cfdi_id and _cfdi_nomr like @empleado"
            };

            sqlCommand.Parameters.AddWithValue("@empleado", employee);
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.ExecuteReader(sqlCommand);
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(string idDocumento, bool onlyActive) {
            var sqlCommand = new MySqlCommand {
                CommandText = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,_cfdnmn_dscnt,_cfdnmn_hrsxtr,
                                      _cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdi_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,_cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,
                                      _cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_sbttl,_cfdi_total,_cfdi_dscnt,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_rfce,_cfdi_rfcr, _cfdi_id 
                              from _cfdi, _cfdnmn 
                              where _cfdnmn_cfdi_id = _cfdi_id and _cfdnmn_a = 1 and _cfdi_uuid like @uuid"
            };

            sqlCommand.Parameters.AddWithValue("@uuid", idDocumento);
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.ExecuteReader(sqlCommand);
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(CFDIFechaEnum dateBy, DateTime dateStart, DateTime dateEnd, string depto, string employee) {
            var wheres = string.Empty;

            if (depto != "") {
                wheres = " and _cfdnmn_depto like @depto";
            }

            if (employee != "") {
                wheres = string.Concat(wheres, " and _cfdi_nomr like @employee");
            }

            if (dateBy == CFDIFechaEnum.FechaEmision) {
                wheres = string.Concat(wheres, " and (_cfdi_fecems >= '@fec_inicio') and (_cfdi_fecems < '@fec_fin')");
            } else if (dateBy == CFDIFechaEnum.FechaDePago) {
                wheres = string.Concat(wheres, " and (_cfdnmn_fchpgo >= '@fec_inicio') and (_cfdnmn_fchpgo < '@fec_fin')");
            } else if (dateBy == CFDIFechaEnum.FechaTimbre) {
                wheres = string.Concat(wheres, " and (_cfdi_feccert >= '@fec_inicio') and (_cfdi_feccert <= '@fec_fin')");
            }

            wheres = wheres.Replace("@fec_inicio", dateStart.ToString("yyyy/MM/dd"));
            wheres = wheres.Replace("@fec_fin", dateEnd.AddDays(1).ToString("yyyy/MM/dd"));

            var sqlCommand = new MySqlCommand {
                CommandText = @"select _cfdnmn_id,_cfdnmn_a,_cfdnmn_drctr_id,_cfdnmn_cfdi_id,_cfdnmn_tpreg,_cfdnmn_rsgpst,_cfdnmn_qnc,_cfdnmn_anio,_cfdnmn_antgdd,_cfdnmn_dspgds,_cfdnmn_bsctap,_cfdnmn_drintg,_cfdnmn_pttlgrv,_cfdnmn_pttlexn,_cfdnmn_dttlgrv,_cfdnmn_dttlexn,
                                    _cfdnmn_dscnt,_cfdnmn_hrsxtr,_cfdnmn_ver,_cfdnmn_banco,_cfdnmn_rgp,_cfdnmn_numem,_cfdnmn_curp,_cfdnmn_numsgr,_cfdnmn_clabe,_cfdnmn_tipo,_cfdnmn_uuid,_cfdnmn_depto,_cfdnmn_puesto,_cfdnmn_tpcntr,_cfdnmn_tpjrn,_cfdnmn_prddpg,_cfdnmn_fchpgo,
                                    _cfdnmn_fchini,_cfdnmn_fchfnl,_cfdnmn_fchrel,_cfdnmn_fn,_cfdnmn_fm,_cfdnmn_usr_n,_cfdnmn_usr_m,_cfdi_nomr,_cfdi_rfce,_cfdnmn_rfc,_cfdi_total,_cfdi_estado,_cfdi_fecedo,_cfdi_feccert,_cfdi_id, _cfdi_uuid 
                            from _cfdnmn,_cfdi,_ctlemp 
                            where _cfdnmn_cfdi_id=_cfdi_id and _cfdnmn_drctr_id=_ctlemp_drctr_id " + wheres
            };

            sqlCommand.Parameters.AddWithValue("@depto", depto);
            sqlCommand.Parameters.AddWithValue("@employee", employee);
            var mapper = new DataNamesMapper<ComprobanteNominaSingleModel>();
            var tabla = this.ExecuteReader(sqlCommand);
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ComplementoNominaModel> GetList() {
            throw new System.NotImplementedException();
        }

        public DataTable GetResumen(int year, int month = 0, int estado = 1) {
            throw new System.NotImplementedException();
        }

        public DataTable GetResumen(int indice) {
            throw new System.NotImplementedException();
        }

        public int Insert(ComplementoNominaModel item) {
            return 0;
        }

        public int Update(ComplementoNominaModel item) {
            return 0;
        }
    }
}
