﻿using System.Collections.Generic;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarControlNominaRepository : SqlSugarContext<ControlNominaModel>, ISqlControlNominaRepository {
        public SqlSugarControlNominaRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public IEnumerable<ControlNominaModel> GetList(bool onlyActive) {
            return this.Db.Queryable<ControlNominaModel>().Where(it => it.Activo == true).OrderBy(it => it.IdControl, SqlSugar.OrderByType.Desc).ToList();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            //var sqlCommand = "select * from _nmnctrl @wcondiciones";
            return null;
        }
    }
}
