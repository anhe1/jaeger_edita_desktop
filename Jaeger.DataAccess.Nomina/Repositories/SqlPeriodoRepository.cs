﻿/// develop: anhe 060520202212
/// purpose: control 
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de periodos de nomina
    /// </summary>
    public class SqlPeriodoRepository : SqlSugarContext<PeriodoModel>, ISqlPeriodoRepository {
        public SqlPeriodoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener listado de periodos por fecha de emision
        /// </summary>
        /// <param name="month">mes</param>
        /// <param name="year">año</param>
        public IEnumerable<PeriodoModel> GetPeriodo(int month, int year) {
            return this.Db.Queryable<PeriodoModel>().Where(it => it.FechaNuevo.Month == month).Where(it => it.FechaNuevo.Year == year).ToList();
        }

        public IEnumerable<NominaPeriodoDetailModel> GetPeriodos(bool onlyActive) {
            var result = this.Db.Queryable<NominaPeriodoDetailModel>().Mapper((itemModel, cache) => {
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.IdPeriodo).ToList();
                    var result2 = this.Db.Queryable<NominaPeriodoEmpleadoDetailModel>().Mapper((itemModel2, cache2) => {
                        var allitems2 = cache2.Get(orderList2 => {
                            var allids2 = orderList2.Select(it => it.IdEmpleado).ToList();
                            return this.Db.Queryable<NominaPeriodoEmpleadoConcepto>().Where(it => allids2.Contains(it.IdEmpleado)).ToList();
                        });
                        itemModel2.Conceptos = new BindingList<NominaPeriodoEmpleadoConcepto>(allitems2.Where(it => it.IdEmpleado == itemModel2.IdEmpleado).ToList());
                    }).Where(it => allIds.Contains(it.IdPeriodo)).ToList();
                    return result2;
                });
                itemModel.Empleados = new BindingList<NominaPeriodoEmpleadoDetailModel>(allItems.Where(it => it.IdPeriodo == itemModel.IdPeriodo).ToList());//Every time it's executed
            }).ToList();
            return result;
        }

        /// <summary>
        /// recuperar periodo de nomina
        /// </summary>
        /// <param name="idPeriodo">indice</param>
        /// <param name="onlyActive">verdadero solo registros activos</param>
        public NominaPeriodoDetailModel GetPeriodo(int idPeriodo, bool onlyActive = true) {
            var result = this.Db.Queryable<NominaPeriodoDetailModel>().Mapper((periodo, cache_Periodo) => {
                // cache del periodo
                var allItems = cache_Periodo.Get(orderList => {
                    var allIds = orderList.Select(it => it.IdPeriodo).ToList();
                    var result2 = this.Db.Queryable<NominaPeriodoEmpleadoDetailModel>().Mapper((itemModel2, cache2) => {
                        var allitems2 = cache2.Get(orderList2 => {
                            var allids2 = orderList2.Select(it => it.IdEmpleado).ToList();
                            var allids3 = orderList2.Select(it => it.Id).ToList();
                            return this.Db.Queryable<NominaPeriodoEmpleadoConcepto>().Where(it => allids3.Contains(it.SubId)).Where(it => allids2.Contains(it.IdEmpleado)).ToList();
                        });
                        itemModel2.Conceptos = new BindingList<NominaPeriodoEmpleadoConcepto>(allitems2.Where(it => it.IdEmpleado == itemModel2.IdEmpleado).ToList());
                    }).Where(it => allIds.Contains(it.IdPeriodo)).ToList();
                    return result2;
                });
                periodo.Empleados = new BindingList<NominaPeriodoEmpleadoDetailModel>(allItems.Where(it => it.IdPeriodo == periodo.IdPeriodo).ToList());//Every time it's executed
            }).Where(it => it.IdPeriodo == idPeriodo).Single();
            return result;
        }

        /// <summary>
        /// ob
        /// </summary>
        /// <returns></returns>
        public IEnumerable<NominaPeriodoEmpleadoResumen> GetResumen(List<int> indices) {
            //var result = this.Db.Queryable<NominaPeriodoModel, NominaPeriodoEmpleadoModel, NominaPeriodoEmpleadoConcepto, EmpleadoModel>((p, i, c, e) => new JoinQueryInfos(
            //    JoinType.Left, p.IdPeriodo == i.IdPeriodo,
            //    JoinType.Left, i.IdEmpleado == e.Id,
            //    JoinType.Left, i.Id == c.SubId
            //)).Where(c => c.Activo == true).Select<NominaPeriodoEmpleadoResumen>().ToList();
            var sql = @"SELECT _nmnprd._nmnprd_stts, _nmnprd._nmnprd_desc, _ctlemp._ctlemp_clv, _ctlemp._ctlemp_numem, _ctlemp._ctlemp_nom, _ctlemp._ctlemp_pape, _ctlemp._ctlemp_sape, _nmnprdc._nmnprdc_cnpt, _nmnprdc._nmnprdc_impfj, _nmnprdc._nmnprdc_ext, _nmnprdc._nmnprdc_grv
FROM _nmnprd
LEFT JOIN _nmnprde ON _nmnprd._nmnprd_id = _nmnprde._nmnprde_sbid
LEFT JOIN _ctlemp ON _nmnprde._nmnprde_empid = _ctlemp._ctlemp_id
LEFT JOIN _nmnprdc ON _nmnprde._nmnprde_id = _nmnprdc._nmnprdc_sbid
WHERE _nmnprd_id IN (@indices)";
            sql = sql.Replace("@indices", string.Join(",", indices.ToArray()));
            var tabla = this.Db.Ado.GetDataTable(sql);
            var mapper = new Domain.Services.Mapping.DataNamesMapper<NominaPeriodoEmpleadoResumen>();
            var result = mapper.Map(tabla).ToList();
            return result;
        }

        public NominaPeriodoDetailModel Save(NominaPeriodoDetailModel model) {
            if (model.IdPeriodo == 0) {
                model.FechaNuevo = DateTime.Now;
                model.IdPeriodo = this.Insert(model);
            }
            else {
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }

            if (model.IdPeriodo > 0) {
                model.Empleados = this.Save(model.Empleados, model.IdPeriodo);
            }
            return model;
        }

        public BindingList<NominaPeriodoEmpleadoDetailModel> Save(BindingList<NominaPeriodoEmpleadoDetailModel> empleados, int idPeriodo) {
            for (int iEmpleado = 0; iEmpleado < empleados.Count; iEmpleado++) {
                if (empleados[iEmpleado].Id == 0) {
                    empleados[iEmpleado].IdPeriodo = idPeriodo;
                    empleados[iEmpleado].Id = this.Db.Insertable<NominaPeriodoEmpleadoModel>(empleados[iEmpleado]).ExecuteReturnIdentity();
                }
                else {
                    empleados[iEmpleado].IdPeriodo = idPeriodo;
                    this.Db.Updateable<NominaPeriodoEmpleadoModel>(empleados[iEmpleado]).ExecuteCommand();
                }

                if (empleados[iEmpleado].Id > 0) {
                    for (int iConcepto = 0; iConcepto < empleados[iEmpleado].Conceptos.Count; iConcepto++) {
                        if (empleados[iEmpleado].Conceptos[iConcepto].Id == 0) {
                            empleados[iEmpleado].Conceptos[iConcepto].SubId = empleados[iEmpleado].Id;
                            empleados[iEmpleado].Conceptos[iConcepto].Id = this.Db.Insertable<NominaPeriodoEmpleadoConcepto>(empleados[iEmpleado].Conceptos[iConcepto]).ExecuteReturnIdentity();
                        }
                        else {
                            empleados[iEmpleado].Conceptos[iConcepto].SubId = empleados[iEmpleado].Id;
                            this.Db.Updateable<NominaPeriodoEmpleadoConcepto>(empleados[iEmpleado].Conceptos[iConcepto]).ExecuteCommand();
                        }
                    }
                }
            }
            return empleados;
        }

        public bool CrearTablas() {
            try {
                this.CreateTable();
                this.Db.CodeFirst.InitTables<NominaPeriodoEmpleadoModel, NominaPeriodoEmpleadoConcepto>();
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
    }
}
