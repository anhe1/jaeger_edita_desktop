﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using MySql.Data.MySqlClient;

namespace Jaeger.DataAccess.Repositories {
    public class MySqlNominaPeriodoRepository : RepositoryMaster, ISqlNominaPeriodoRepository {
        public MySqlNominaPeriodoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool CrearTablas() {
            throw new NotImplementedException();
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public NominaPeriodoModel GetById(int index) {
            throw new NotImplementedException();
        }

        public List<NominaPeriodoModel> GetList() {
            throw new NotImplementedException();
        }

        public IEnumerable<NominaPeriodoDetailModel> GetPeriodos(bool onlyActive) {
            throw new NotImplementedException();
        }

        public NominaPeriodoDetailModel Save(NominaPeriodoDetailModel model) {
            if (model.IdPeriodo == 0) {
                model.FechaNuevo = DateTime.Now;
                model.IdPeriodo = this.Insert(model);
            }
            else {
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }

            if (model.IdPeriodo > 0) {
                model.Empleados = this.Save(model.Empleados, model.IdPeriodo);
            }
            return model;
        }

        public BindingList<NominaPeriodoEmpleadoDetailModel> Save(BindingList<NominaPeriodoEmpleadoDetailModel> empleados, int idPeriodo) {
            for (int iEmpleado = 0; iEmpleado < empleados.Count; iEmpleado++) {
                if (empleados[iEmpleado].Id == 0) {
                    empleados[iEmpleado].IdPeriodo = idPeriodo;
                    empleados[iEmpleado].Id = this.Insert(empleados[iEmpleado]);
                }
                else {
                    empleados[iEmpleado].IdPeriodo = idPeriodo;
                    this.Update(empleados[iEmpleado]);
                }

                if (empleados[iEmpleado].Id > 0) {
                    for (int iConcepto = 0; iConcepto < empleados[iEmpleado].Conceptos.Count; iConcepto++) {
                        if (empleados[iEmpleado].Conceptos[iConcepto].Id == 0) {
                            empleados[iEmpleado].Conceptos[iConcepto].SubId = empleados[iEmpleado].Id;
                            empleados[iEmpleado].Conceptos[iConcepto].Id = this.Insert(empleados[iEmpleado].Conceptos[iConcepto]);
                        }
                        else {
                            empleados[iEmpleado].Conceptos[iConcepto].SubId = empleados[iEmpleado].Id;
                            this.Update(empleados[iEmpleado].Conceptos[iConcepto]);
                        }
                    }
                }
            }
            return empleados;
        }

        private void Update(NominaPeriodoEmpleadoConcepto nominaPeriodoEmpleadoConcepto) {
            throw new NotImplementedException();
        }

        private int Insert(NominaPeriodoEmpleadoConcepto model) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @"INSERT INTO `_nmnprdc`  
                                   (`_nmnprdc_sbid`,`_nmnprdc_a`,`_nmnprdc_cnp_id`,`_nmnprdc_emp_id`,`_nmnprdc_tbl_id`,`_nmnprdc_tpcnp`,`_nmnprdc_ident`,`_nmnprdc_clv`,`_nmnprdc_clvsat`,`_nmnprdc_cnpt`,`_nmnprdc_impfj`,`_nmnprdc_ext`,`_nmnprdc_grv`)
                             VALUES
                                   (@_nmnprdc_sbid,@_nmnprdc_a,@_nmnprdc_cnp_id,@_nmnprdc_emp_id,@_nmnprdc_tbl_id,@_nmnprdc_tpcnp,@_nmnprdc_ident,@_nmnprdc_clv,@_nmnprdc_clvsat,@_nmnprdc_cnpt,@_nmnprdc_impfj,@_nmnprdc_ext,@_nmnprdc_grv) ;SELECT LAST_INSERT_ID();"
            };

            sqlCommand.Parameters.AddWithValue("@_nmnprdc_sbid", model.SubId);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_a", model.Activo);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_cnp_id", model.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_emp_id", model.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_tbl_id", model.IdTabla);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_tpcnp", model.TipoConcepto);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_ident", model.Identificador);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_clv", model.Clave);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_clvsat", model.ClaveSAT);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_cnpt", model.Concepto);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_impfj", model.ImporteFijo);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_ext", model.ImporteExento);
            sqlCommand.Parameters.AddWithValue("@_nmnprdc_grv", model.ImporteGravado);

            model.Id = this.ExecuteScalar(sqlCommand);
            return model.Id;
        }

        private void Update(NominaPeriodoEmpleadoDetailModel nominaPeriodoEmpleadoDetailModel) {
            throw new NotImplementedException();
        }

        public int Update(NominaPeriodoModel item) {
            throw new NotImplementedException();
        }

        public int Insert(NominaPeriodoModel model) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @"INSERT INTO `_nmnprd`  
                                (`_nmnprd_a`,`_nmnprd_stts`,`_nmnprd_tipo`,`_nmnprd_tpr`,`_nmnprd_desc`,`_nmnprd_fecini`,`_nmnprd_fecfin`,`_nmnprd_fecpag`,`_nmnprd_fn`,`_nmnprd_usr_c`,`_nmnprd_usr_n`)
                             VALUES
                                (@_nmnprd_a,@_nmnprd_stts,@_nmnprd_tipo,@_nmnprd_tpr,@_nmnprd_desc,@_nmnprd_fecini,@_nmnprd_fecfin,@_nmnprd_fecpag,@_nmnprd_fn,@_nmnprd_usr_c,@_nmnprd_usr_n) ;SELECT LAST_INSERT_ID();"
            };

            sqlCommand.Parameters.AddWithValue("@_nmnprd_a", model.Activo);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_stts", model.Estado);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_tipo", model.Tipo);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_tpr", model.TipoPeriodo);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_desc", model.Descripcion);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_fecini", model.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_fecfin", model.FechaFinal);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_fecpag", model.FechaPago);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_fn", model.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_usr_c", model.Cancela);
            sqlCommand.Parameters.AddWithValue("@_nmnprd_usr_n", model.Creo);
            model.IdPeriodo = this.ExecuteScalar(sqlCommand);
            return model.IdPeriodo;
        }

        public int Insert(NominaPeriodoEmpleadoModel model) {
            var sqlCommand = new MySqlCommand() {
                CommandText = @"INSERT INTO `_nmnprde`  
                                   (`_nmnprde_sbid`,`_nmnprde_empid`,`_nmnprde_pntld`,`_nmnprde_asnc`,`_nmnprde_inc`,`_nmnprde_hrrlj`,`_nmnprde_hrsextr`,`_nmnprde_hrsaut`,`_nmnprde_hrscrg`,`_nmnprde_hrsabn`,`_nmnprde_vac`,`_nmnprde_prod`,`_nmnprde_sd`,`_nmnprde_sdi`)
                             VALUES
                                   (@_nmnprde_sbid,@_nmnprde_empid,@_nmnprde_pntld,@_nmnprde_asnc,@_nmnprde_inc,@_nmnprde_hrrlj,@_nmnprde_hrsextr,@_nmnprde_hrsaut,@_nmnprde_hrscrg,@_nmnprde_hrsabn,@_nmnprde_vac,@_nmnprde_prod,@_nmnprde_sd,@_nmnprde_sdi) ;SELECT LAST_INSERT_ID();"
            };

            sqlCommand.Parameters.AddWithValue("@_nmnprde_sbid", model.IdPeriodo);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_empid", model.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_pntld", model.Puntualidad);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_asnc", model.Ausencia);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_inc", model.Incapacidad);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_hrrlj", model.HorasReloj);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_hrsextr", model.HorasExtra);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_hrsaut", model.HorasAutorizadas);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_hrscrg", model.HorasCargo);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_hrsabn", model.HorasAbono);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_vac", model.DiasVacaciones);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_prod", model.Productividad);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_sd", model.SalarioDiario);
            sqlCommand.Parameters.AddWithValue("@_nmnprde_sdi", model.SalarioDiarioIntegrado);
            model.Id = this.ExecuteScalar(sqlCommand);
            return model.Id;
        }

        public IEnumerable<NominaPeriodoModel> GetPeriodo(int month, int year) {
            throw new NotImplementedException();
        }
    }
}
