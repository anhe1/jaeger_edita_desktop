﻿using System;

namespace Jaeger.DataAccess.Entities {
    public class MessageError {
        public MessageError() {
            this.DateTime = DateTime.Now;
            this.Type = "Advertencia";
        }

        public MessageError(string message) {
            this.Value = message;
        }

        public MessageError(string message, int noError, string type) {
            this.DateTime = DateTime.Now;
            this.Value = message;
            this.NoError = noError;
            this.Type = type;
        }

        public DateTime DateTime {
            get; set;
        }

        public int NoError {
            get; set;
        }

        public string Value {
            get; set;
        }

        public string Type {
            get; set;
        }

        public override string ToString() {
            return string.Concat(this.DateTime.ToShortDateString(), "|", this.Type, "|", this.NoError.ToString(), this.Value);
        }
    }
}
