﻿using System;

namespace Jaeger.DataAccess.Base.Services.ClassGenerator {
    public static partial class PocoClassGenerator {

        private class ColumnInfo {
            public Type DataType { get; set; }
            public string ColumnName { get; set; }
            public bool IsNullable { get; set; }
            public string Name { get; set; }
            public bool IsKey { get; set; }
            public bool IsAutoIncrement { get; set; }
            public string Comments { get; set; }
        }
    }
}