﻿using System.Collections.Generic;

namespace Jaeger.DataAccess.Base.Services.ClassGenerator {
    public static partial class PocoClassGenerator {
        private class SchemaTable {
            public string Name { get; set; } = string.Empty;

            public List<ColumnInfo> Fields { get; set; }
        }
    }
}