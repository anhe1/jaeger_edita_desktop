﻿using System.Text.RegularExpressions;

namespace Jaeger.DataAccess.Services {
    public static class ExpressionsToSql { 
        public static string ToRaw(string commandText) {
            RegexOptions regExOptions = (RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string rawText = commandText;
            string regExText = @"('(''|[^'])*')|([\r|\n][\s| ]*[\r|\n])|(--[^\r\n]*)|(/\*[\w\W]*?(?=\*/)\*/)";
            //string regExText = @"('(''|[^'])*')|[\t\r\n]|(--[^\r\n]*)|(/\*[\w\W]*?(?=\*/)\*/)";
            //'Replace Tab, Carriage Return, Line Feed, Single-row Comments and
            //'Multi-row Comments with a space when not included inside a text block.

            MatchCollection patternMatchList = Regex.Matches(rawText, regExText, regExOptions);
            int iSkipLength = 0;
            for (int patternIndex = 0; patternIndex < patternMatchList.Count; patternIndex++) {
                if (!patternMatchList[patternIndex].Value.StartsWith("'") && !patternMatchList[patternIndex].Value.EndsWith("'")) {
                    rawText = rawText.Substring(0, patternMatchList[patternIndex].Index - iSkipLength) + " " + rawText.Substring(patternMatchList[patternIndex].Index - iSkipLength + patternMatchList[patternIndex].Length);
                    iSkipLength += (patternMatchList[patternIndex].Length - " ".Length);
                }
            }
            //'Remove extra spacing that is not contained inside text qualifers.
            patternMatchList = Regex.Matches(rawText, "'([^']|'')*'|[ ]{2,}", regExOptions);
            iSkipLength = 0;
            for (int patternIndex = 0; patternIndex < patternMatchList.Count; patternIndex++) {
                if (!patternMatchList[patternIndex].Value.StartsWith("'") && !patternMatchList[patternIndex].Value.EndsWith("'")) {
                    rawText = rawText.Substring(0, patternMatchList[patternIndex].Index - iSkipLength) + " " + rawText.Substring(patternMatchList[patternIndex].Index - iSkipLength + patternMatchList[patternIndex].Length);
                    iSkipLength += (patternMatchList[patternIndex].Length - " ".Length);
                }
            }
            //'Return value without leading and trailing spaces.
            return rawText.Trim();

        }
    }
}
