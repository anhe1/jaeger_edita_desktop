﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    public class SqlFbMovimientoAlmacenRepository : Abstractions.RepositoryMaster<MovimientoModel>, ISqlMovimientoAlmacenPT {
        public SqlFbMovimientoAlmacenRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM MVAPT WHERE ((MVAPT_ID = @p1))" };
            throw new NotImplementedException();
        }

        public MovimientoModel GetById(int index) {
            return GetList(new List<IConditional>() {
                new Conditional("RMSN_ID", index.ToString()) }).FirstOrDefault();
        }

        public int Insert(MovimientoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO MVAPT ( MVAPT_ID, MVAPT_A, MVAPT_DOC_ID, MVAPT_RMSN_ID, MVAPT_DRCTR_ID, MVAPT_CTDPT_ID, MVAPT_CTALM_ID, MVAPT_CTPRD_ID, MVAPT_CTMDL_ID, MVAPT_CTPRC_ID, MVAPT_CTESPC_ID, MVAPT_CTUND_ID, MVAPT_UNDF, MVAPT_UNDN, MVAPT_CANTE, MVAPT_CANTS, MVAPT_CTCLS, MVAPT_PRDN, MVAPT_MDLN, MVAPT_MRC, MVAPT_ESPC, MVAPT_ESPN, MVAPT_UNTC, MVAPT_UNDC, MVAPT_UNTR, MVAPT_UNTR2, MVAPT_SBTTL, MVAPT_DESC, MVAPT_IMPRT, MVAPT_TSIVA, MVAPT_TRIVA, MVAPT_TOTAL, MVAPT_SKU, MVAPT_USR_N, MVAPT_FN, MVAPT_ALMPT_ID, MVAPT_CTEFC_ID, MVAPT_PDCLN_ID) 
                                           VALUES (@MVAPT_ID,@MVAPT_A,@MVAPT_DOC_ID,@MVAPT_RMSN_ID,@MVAPT_DRCTR_ID,@MVAPT_CTDPT_ID,@MVAPT_CTALM_ID,@MVAPT_CTPRD_ID,@MVAPT_CTMDL_ID,@MVAPT_CTPRC_ID,@MVAPT_CTESPC_ID,@MVAPT_CTUND_ID,@MVAPT_UNDF,@MVAPT_UNDN,@MVAPT_CANTE,@MVAPT_CANTS,@MVAPT_CTCLS,@MVAPT_PRDN,@MVAPT_MDLN,@MVAPT_MRC,@MVAPT_ESPC,@MVAPT_ESPN,@MVAPT_UNTC,@MVAPT_UNDC,@MVAPT_UNTR,@MVAPT_UNTR2,@MVAPT_SBTTL,@MVAPT_DESC,@MVAPT_IMPRT,@MVAPT_TSIVA,@MVAPT_TRIVA,@MVAPT_TOTAL,@MVAPT_SKU,@MVAPT_USR_N,@MVAPT_FN,@MVAPT_ALMPT_ID,@MVAPT_CTEFC_ID,@MVAPT_PDCLN_ID) RETURNING MVAPT_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@MVAPT_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@MVAPT_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_DOC_ID", item.IdTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@MVAPT_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTEFC_ID", item.IdTipoMovimiento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@MVAPT_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNDF", item.UnidadFactor);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CANTE", item.CantidadE);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CANTS", item.CantidadS);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTCLS", item.Catalogo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_PRDN", item.Producto);
            sqlCommand.Parameters.AddWithValue("@MVAPT_MDLN", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@MVAPT_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@MVAPT_ESPN", item.Tamanio);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNTC", item.CostoUnitario);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNDC", item.CostoUnidad);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNTR", item.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNTR2", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@MVAPT_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@MVAPT_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_IMPRT", item.Importe);
            sqlCommand.Parameters.AddWithValue("@MVAPT_TSIVA", item.TasaIVA);
            sqlCommand.Parameters.AddWithValue("@MVAPT_TRIVA", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@MVAPT_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@MVAPT_SKU", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@MVAPT_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_PDCLN_ID", item.IdPedido);
            item.IdMovimiento = ExecuteScalar(sqlCommand);
            if (item.IdMovimiento > 0)
                return item.IdMovimiento;
            return 0;
        }

        public int Update(MovimientoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE MVAPT SET MVAPT_A = @MVAPT_A, MVAPT_DOC_ID = @MVAPT_DOC_ID, MVAPT_RMSN_ID = @MVAPT_RMSN_ID, MVAPT_DRCTR_ID = @MVAPT_DRCTR_ID, MVAPT_CTDPT_ID = @MVAPT_CTDPT_ID, 
MVAPT_CTALM_ID = @MVAPT_CTALM_ID, MVAPT_CTPRD_ID = @MVAPT_CTPRD_ID, MVAPT_CTMDL_ID = @MVAPT_CTMDL_ID, MVAPT_CTPRC_ID = @MVAPT_CTPRC_ID, MVAPT_CTESPC_ID = @MVAPT_CTESPC_ID, MVAPT_CTUND_ID = @MVAPT_CTUND_ID, 
MVAPT_UNDF = @MVAPT_UNDF, MVAPT_UNDN = @MVAPT_UNDN, MVAPT_CANTE = @MVAPT_CANTE, MVAPT_CANTS = @MVAPT_CANTS, MVAPT_CTCLS = @MVAPT_CTCLS, MVAPT_PRDN = @MVAPT_PRDN, MVAPT_MDLN = @MVAPT_MDLN, MVAPT_MRC = @MVAPT_MRC, 
MVAPT_ESPC = @MVAPT_ESPC, MVAPT_ESPN = @MVAPT_ESPN, MVAPT_UNTC = @MVAPT_UNTC, MVAPT_UNDC = @MVAPT_UNDC, MVAPT_UNTR = @MVAPT_UNTR, MVAPT_UNTR2 = @MVAPT_UNTR2, MVAPT_SBTTL = @MVAPT_SBTTL, MVAPT_DESC = @MVAPT_DESC, 
MVAPT_IMPRT = @MVAPT_IMPRT, MVAPT_TSIVA = @MVAPT_TSIVA, MVAPT_TRIVA = @MVAPT_TRIVA, MVAPT_TOTAL = @MVAPT_TOTAL, MVAPT_SKU = @MVAPT_SKU, MVAPT_USR_M = @MVAPT_USR_M, MVAPT_FM = @MVAPT_FM, MVAPT_ALMPT_ID = @MVAPT_ALMPT_ID, 
MVAPT_CTEFC_ID = @MVAPT_CTEFC_ID, MVAPT_PDCLN_ID = @MVAPT_PDCLN_ID WHERE ((MVAPT_ID = @MVAPT_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@MVAPT_ID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_DOC_ID", item.IdTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@MVAPT_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@MVAPT_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTEFC_ID", item.IdTipoMovimiento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@MVAPT_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNDF", item.UnidadFactor);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CANTE", item.CantidadE);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CANTS", item.CantidadS);
            sqlCommand.Parameters.AddWithValue("@MVAPT_CTCLS", item.Catalogo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_PRDN", item.Producto);
            sqlCommand.Parameters.AddWithValue("@MVAPT_MDLN", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@MVAPT_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@MVAPT_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@MVAPT_ESPN", item.Tamanio);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNTC", item.CostoUnitario);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNDC", item.CostoUnidad);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNTR", item.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@MVAPT_UNTR2", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@MVAPT_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@MVAPT_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@MVAPT_IMPRT", item.Importe);
            sqlCommand.Parameters.AddWithValue("@MVAPT_TSIVA", item.TasaIVA);
            sqlCommand.Parameters.AddWithValue("@MVAPT_TRIVA", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@MVAPT_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@MVAPT_SKU", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@MVAPT_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@MVAPT_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@MVAPT_PDCLN_ID", item.IdPedido);
            return ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<MovimientoModel> GetList() {
            return GetList(new List<IConditional>());
        }
        #endregion

        public IEnumerable<MovimientoDetailModel> GetList(List<IConditional> condidional) {
            return GetList<MovimientoDetailModel>(condidional);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM MVAPT @wcondiciones"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            var result = GetMapper<T1>(sqlCommand).ToList();
            return result;
        }

        public MovimientoModel Save(MovimientoModel movimiento) {
            if (movimiento.IdMovimiento == 0) {
                movimiento.FechaNuevo = DateTime.Now;
                movimiento.Creo = User;
                movimiento.IdMovimiento = Insert(movimiento);
            } else {
                movimiento.FechaModifica = DateTime.Now;
                movimiento.Modifica = User;
                Update(movimiento);
            }
            return movimiento;
        }

        public MovimientoDetailModel Save(MovimientoDetailModel model) {
            if (model.IdMovimiento == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = User;
                model.IdMovimiento = Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = User;
                Update(model);
            }
            return model;
        }

        public RemisionConceptoDetailModel Save(RemisionConceptoDetailModel concepto) {
            if (concepto.IdMovimiento == 0) {
                concepto.FechaNuevo = DateTime.Now;
                concepto.Creo = User;
                concepto.IdMovimiento = Insert(concepto);
            } else {
                concepto.FechaModifica = DateTime.Now;
                concepto.Modifica = User;
                Update(concepto);
            }
            return concepto;
        }

        public ValeAlmacenConceptoModel Save(ValeAlmacenConceptoModel vale) {
            if (vale.IdMovimiento == 0) {
                vale.FechaNuevo = DateTime.Now;
                vale.Creo = User;
                vale.IdMovimiento = Insert(vale);
            } else {
                vale.FechaModifica = DateTime.Now;
                vale.Modifica = User;
                Update(vale);
            }
            return vale;
        }

        public bool Existencia(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"
MERGE INTO CTMDLX m
USING(
    SELECT
    MVAPT.MVAPT_CTPRD_ID  IDPRODUCTO,
    MVAPT.MVAPT_CTMDL_ID  IDMODELO,
    MVAPT.MVAPT_CTESPC_ID IDESPECIFICACION,
    MVAPT.MVAPT_CTEFC_ID  EFECTO,
    MVAPT.MVAPT_CTALM_ID  IDALMACEN,
    MVAPT.MVAPT_UNDF      FACTOR,
    IIF(MVAPT.MVAPT_CTEFC_ID=1,MVAPT.MVAPT_CANTE, (MVAPT.MVAPT_CANTS*-1)) CANTIDAD
    FROM MVAPT
    @wcondiciones
) x
 ON m.CTMDLX_CTMDL_ID = x.IDMODELO AND m.CTMDLX_CTESPC_ID = x.IDESPECIFICACION AND m.CTMDLX_CTPRD_ID = x.IDPRODUCTO AND m.CTMDLX_CTALM_ID = x.IDALMACEN
 WHEN MATCHED THEN UPDATE SET m.CTMDLX_EXT = m.CTMDLX_EXT + x.CANTIDAD
 WHEN NOT MATCHED THEN INSERT (CTMDLX_CTALM_ID, CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID, CTMDLX_CTPRD_ID, CTMDLX_EXT) VALUES (x.IDALMACEN, x.IDMODELO, x.IDESPECIFICACION, x.IDPRODUCTO, x.CANTIDAD)"
            };
            // en caso de estar cancelado entonces quitamos y removemos la condicional
            var isCancelado = conditionals.Where(it => it.FieldName.ToLower().Contains("@cancelado")).FirstOrDefault();
            if (isCancelado != null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("*-1", "");
                conditionals.Remove(isCancelado);
            }
            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }
    }
}
