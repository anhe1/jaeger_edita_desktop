﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    /// <summary>
    /// repositorio de relaciones entre remisiones
    /// </summary>
    public class SqlFbRemisionadoRRepository : Abstractions.RepositoryMaster<RemisionRelacionadaModel>, ISqlRemisionadoRRepository {
        public SqlFbRemisionadoRRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM RMSNR WHERE ((RMSNR_ID = @RMSNR_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@RMSNR_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionRelacionadaModel GetById(int index) {
            return GetList(new List<IConditional> { new Conditional("RMSNR_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<RemisionRelacionadaModel> GetList(List<IConditional> condidional) {
            return GetList<RemisionRelacionadaModel>(condidional);
        }

        public IEnumerable<RemisionRelacionadaModel> GetList() {
            return GetList(new List<IConditional>());
        }

        public int Insert(RemisionRelacionadaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RMSNR (RMSNR_RMSN_ID, RMSNR_DRCTR_ID, RMSNR_CTREL_ID, RMSNR_RELN, RMSNR_UUID, RMSNR_SERIE, RMSNR_FOLIO, RMSNR_NOMR, RMSNR_FECEMS, RMSNR_TOTAL, RMSNR_USR_N, RMSNR_FN)
                                          VALUES (@RMSNR_RMSN_ID,@RMSNR_DRCTR_ID,@RMSNR_CTREL_ID,@RMSNR_RELN,@RMSNR_UUID,@RMSNR_SERIE,@RMSNR_FOLIO,@RMSNR_NOMR,@RMSNR_FECEMS,@RMSNR_TOTAL,@RMSNR_USR_N,@RMSNR_FN) RETURNING RMSNR_ID;"
            };
            //sqlCommand.Parameters.AddWithValue("@RMSNR_ID", DBNull.Value);
            //sqlCommand.Parameters.AddWithValue("@RMSNR_A", 1);
            sqlCommand.Parameters.AddWithValue("@RMSNR_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNR_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@RMSNR_CTREL_ID", item.IdTipoRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_RELN", item.Relacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSNR_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@RMSNR_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSNR_TOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@RMSNR_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FN", item.FechaNuevo);

            //item.IdRelacion = ExecuteScalar(sqlCommand);
            //if (item.IdRelacion > 0)
            //    return item.IdRelacion;
            //return 0;
            return ExecuteTransaction(sqlCommand);
        }

        public int Update(RemisionRelacionadaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSNR SET RMSNR_RMSN_ID = @RMSNR_RMSN_ID, RMSNR_DRCTR_ID = @RMSNR_DRCTR_ID, RMSNR_CTREL_ID = @RMSNR_CTREL_ID, RMSNR_RELN = @RMSNR_RELN, RMSNR_UUID = @RMSNR_UUID, RMSNR_SERIE = @RMSNR_SERIE, RMSNR_FOLIO = @RMSNR_FOLIO, RMSNR_NOMR = @RMSNR_NOMR, RMSNR_FECEMS = @RMSNR_FECEMS, RMSNR_TOTAL = @RMSNR_TOTAL, RMSNR_USR_M = @RMSNR_USR_M, RMSNR_FM = @RMSNR_FM WHERE ((RMSNR_ID = @RMSNR_ID))"
            };
            //sqlCommand.Parameters.AddWithValue("@RMSNR_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_A", 1);
            sqlCommand.Parameters.AddWithValue("@RMSNR_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNR_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@RMSNR_CTREL_ID", item.IdTipoRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_RELN", item.Relacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSNR_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@RMSNR_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSNR_TOTAL", item.GTotal);
            //sqlCommand.Parameters.AddWithValue("@RMSNR_USR_M", item.Modifica);
            //sqlCommand.Parameters.AddWithValue("@RMSNR_FM", item.FechaModifica);
            return ExecuteTransaction(sqlCommand);
        }

        public int Saveable(RemisionRelacionadaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO RMSNR ( RMSNR_RMSN_ID, RMSNR_DRCTR_ID, RMSNR_CTREL_ID, RMSNR_RELN, RMSNR_UUID, RMSNR_SERIE, RMSNR_FOLIO, RMSNR_NOMR, RMSNR_FECEMS, RMSNR_TOTAL, RMSNR_USR_N, RMSNR_FN)
                                                     VALUES (@RMSNR_RMSN_ID,@RMSNR_DRCTR_ID,@RMSNR_CTREL_ID,@RMSNR_RELN,@RMSNR_UUID,@RMSNR_SERIE,@RMSNR_FOLIO,@RMSNR_NOMR,@RMSNR_FECEMS,@RMSNR_TOTAL,@RMSNR_USR_N,@RMSNR_FN)
                                                            MATCHING (RMSNR_RMSN_ID, RMSNR_DRCTR_ID, RMSNR_CTREL_ID, RMSNR_UUID);"
            };
            //sqlCommand.Parameters.AddWithValue("@RMSNR_ID", DBNull.Value);
            //sqlCommand.Parameters.AddWithValue("@RMSNR_A", 1);
            sqlCommand.Parameters.AddWithValue("@RMSNR_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNR_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@RMSNR_CTREL_ID", item.IdTipoRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_RELN", item.Relacion);
            sqlCommand.Parameters.AddWithValue("@RMSNR_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSNR_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@RMSNR_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSNR_TOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@RMSNR_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@RMSNR_FN", item.FechaNuevo);

            //item.IdRelacion = ExecuteScalar(sqlCommand);
            //if (item.IdRelacion > 0)
            //    return item.IdRelacion;
            //return 0;
            return ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT RMSNR.* FROM RMSNR @wcondiciones ORDER BY RMSNR_FOLIO DESC"
            };
            return this. GetMapper<T1>(sqlCommand, conditionals);
        }

        //public RemisionRelacionadaModel Save(RemisionRelacionadaModel model) {
        //    if (model.IdRelacion == 0) {
        //        model.FechaNuevo = DateTime.Now;
        //        model.Creo = User;
        //        model.IdRelacion = Insert(model);
        //    } else {
        //        model.FechaModifica = DateTime.Now;
        //        model.Modifica = User;
        //        Update(model);
        //    }
        //    return model;
        //}
    }
}
