﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Contracts;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    public class SqlFbValeRelacionRepository : RepositoryMaster<ValeAlmacenRelacionModel>, ISqlValeRelacionRepository {
        public SqlFbValeRelacionRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public ValeAlmacenRelacionModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<ValeAlmacenRelacionModel> GetList() {
            throw new NotImplementedException();
        }

        public int Insert(ValeAlmacenRelacionModel item) {
            throw new NotImplementedException();
        }

        public int Update(ValeAlmacenRelacionModel item) {
            throw new NotImplementedException();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM ALMPTR @wcondiciones"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            var result = GetMapper<T1>(sqlCommand).ToList();
            return result;
        }

        public int Save(IValeAlmacenRelacionModel item) {
            item.FechaNuevo = DateTime.Now;
            item.Creo = this.User;
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO ALMPTR ( ALMPTR_ALMPT_ID, ALMPTR_DOC_ID, ALMPTR_CTREL_ID, ALMPTR_CTREL, ALMPTR_UUID, ALMPTR_DRCTR_ID, ALMPTR_FOLIO, ALMPTR_SERIE, ALMPTR_CLV, ALMPTR_RFCR, ALMPTR_NOM, ALMPTR_USR_N, ALMPTR_FN, ALMPTR_FECEMS) 
                                                      VALUES (@ALMPTR_ALMPT_ID,@ALMPTR_DOC_ID,@ALMPTR_CTREL_ID,@ALMPTR_CTREL,@ALMPTR_UUID,@ALMPTR_DRCTR_ID,@ALMPTR_FOLIO,@ALMPTR_SERIE,@ALMPTR_CLV,@ALMPTR_RFCR,@ALMPTR_NOM,@ALMPTR_USR_N,@ALMPTR_FN,@ALMPTR_FECEMS)
                                                    MATCHING (ALMPTR_ALMPT_ID,ALMPTR_DOC_ID,ALMPTR_CTREL_ID,ALMPTR_UUID,ALMPTR_DRCTR_ID,ALMPTR_FOLIO);"
            };
            var parametros = new List<FbParameter> {
                new FbParameter("@ALMPTR_ALMPT_ID", item.IdComprobante),
                new FbParameter("@ALMPTR_DOC_ID", item.IdTipoComprobante),
                new FbParameter("@ALMPTR_CTREL_ID", item.IdClaveRelacion),
                new FbParameter("@ALMPTR_CTREL", item.ClaveRelacion),
                new FbParameter("@ALMPTR_UUID", item.IdDocumento),
                new FbParameter("@ALMPTR_DRCTR_ID", item.IdDirectorio),
                new FbParameter("@ALMPTR_FOLIO", item.Folio),
                new FbParameter("@ALMPTR_SERIE", item.Serie),
                new FbParameter("@ALMPTR_CLV", item.Clave),
                new FbParameter("@ALMPTR_RFCR", item.ReceptorRFC),
                new FbParameter("@ALMPTR_NOM", item.Receptor),
                new FbParameter("@ALMPTR_USR_N", item.Creo),
                new FbParameter("@ALMPTR_FN", item.FechaNuevo),
                new FbParameter("@ALMPTR_FECEMS", item.FechaEmision)
            };
            sqlCommand.Parameters.Add(parametros);
            return ExecuteTransaction(sqlCommand);
        }
    }
}
