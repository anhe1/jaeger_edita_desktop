﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    public class SqlFbValeStatusRepository : RepositoryMaster<ValeAlmacenStatusModel>, ISqlValeStatusRepository {
        public SqlFbValeStatusRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }
        
        public IEnumerable<ValeAlmacenStatusModel> GetList() {
            throw new NotImplementedException();
        }
        
        public int Insert(ValeAlmacenStatusModel item) {
            throw new NotImplementedException();
        }

        public int Update(ValeAlmacenStatusModel item) {
            throw new NotImplementedException();
        }

        public ValeAlmacenStatusModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ValeAlmacenStatusModel)) {
                var sqlCommand = new FbCommand { CommandText = @"SELECT * FROM ALMPTS @wcondiciones" };
                sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
                var result = this.GetMapper<T1>(sqlCommand).ToList();
                return result;
            }
            return this.GetList<T1>(conditionals);
        }
        #endregion

        public IValeAlmacenStatusModel Save(IValeAlmacenStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO ALMPTS ( ALMPTS_ALMPT_ID, ALMPTS_STTS_ID, ALMPTS_STTSB_ID, ALMPTS_CTMTV_ID, ALMPTS_DRCTR_ID, ALMPTS_CTMTV, ALMPTS_NOTA, ALMPTS_USR_N, ALMPTS_FN) 
                                                      VALUES (@ALMPTS_ALMPT_ID,@ALMPTS_STTS_ID,@ALMPTS_STTSB_ID,@ALMPTS_CTMTV_ID,@ALMPTS_DRCTR_ID,@ALMPTS_CTMTV,@ALMPTS_NOTA,@ALMPTS_USR_N,@ALMPTS_FN) 
                                                    MATCHING (ALMPTS_ALMPT_ID, ALMPTS_STTS_ID, ALMPTS_STTSB_ID, ALMPTS_CTMTV_ID);"
            };


            sqlCommand.Parameters.AddWithValue("@ALMPTS_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_STTSB_ID", item.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_CTMTV_ID", item.IdCveMotivo);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_CTMTV", item.CveMotivo);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@ALMPTS_FN", item.FechaNuevo);

            var d0 = ExecuteTransaction(sqlCommand) > 0;
            return item;
        }
    }
}
