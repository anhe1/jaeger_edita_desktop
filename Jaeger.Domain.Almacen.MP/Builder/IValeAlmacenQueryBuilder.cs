﻿using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.MP.Builder {
    public interface IValeAlmacenQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IConditionalBuilder ByFolio(int folio);
        IValeAlmacenIdDirectorioQueryBuilder ById(int idComprobante);
        IValeAlmacenIdDirectorioQueryBuilder ByIdReceptor(int idComprobante);
        IValeAlmacenIdAlmacenQueryBuilder ByIdAlmacen(int idAlmacen);
    }

    public interface IValeAlmacenIdAlmacenQueryBuilder {
        IValeAlmacenIdDirectorioQueryBuilder ByTipoAlmacen(AlmacenEnum idAlmacen);
    }

    public interface IValeAlmacenIdDirectorioQueryBuilder {
        IValeAlmacenQueryYearBuilder WithYear(int year);
        IValeAlmacenIdDirectorioQueryBuilder ByType(TipoComprobanteEnum movimientoTipo);
    }

    public interface IValeAlmacenQueryYearBuilder {
        IValeAlmacenQueryMonthBuilder WithMonth(int month);
    }

    public interface IValeAlmacenQueryMonthBuilder : IConditionalBuilder {
        IConditionalBuild ByStatus(int idAlmacen);
    }
}
