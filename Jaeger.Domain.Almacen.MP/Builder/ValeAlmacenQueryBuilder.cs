﻿using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.MP.Builder {
    public class ValeAlmacenQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IValeAlmacenQueryBuilder, IValeAlmacenIdDirectorioQueryBuilder, IValeAlmacenQueryYearBuilder, IValeAlmacenQueryMonthBuilder,
        IValeAlmacenIdAlmacenQueryBuilder {
        public ValeAlmacenQueryBuilder() : base() {

        }

        public IValeAlmacenIdDirectorioQueryBuilder ById(int idComprobante) {
            this._Conditionals.Add(new Conditional("ALMMP_ID", idComprobante.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder TipoComprobante(TipoComprobanteEnum tipo) {
            var d0 = (int)tipo;
            this._Conditionals.Add(new Conditional("ALMMP_DOC_ID", d0.ToString()));
            return this;
        }

        public IValeAlmacenIdAlmacenQueryBuilder ByIdAlmacen(int idAlmacen) {
            this._Conditionals.Add(new Conditional("ALMMP_CTALM_ID", idAlmacen.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder ByTipoAlmacen(AlmacenEnum idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("ALMMP_TPALM_ID", d0.ToString()));
            return this;
        }

        public IConditionalBuilder ByFolio(int folio) {
            this._Conditionals.Clear();
            this._Conditionals.Add(new Conditional("ALMMP_FOLIO", folio.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder IdDepartamento(int folio) {
            this._Conditionals.Add(new Conditional("ALMMP_CTDEP_ID", folio.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder ByIdReceptor(int idDirectorio) {
            this._Conditionals.Add(new Conditional("ALMMP_DRCTR_ID", idDirectorio.ToString()));
            return this;
        }

        public IConditionalBuild ByStatus(int idStatus) {
            this._Conditionals.Add(new Conditional("ALMMP_STTS_ID", idStatus.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder ByType(TipoComprobanteEnum movimientoTipo) {
            this._Conditionals.Add(new Conditional("ALMPT_CTEFC_ID", movimientoTipo.ToString()));
            return this;
        }

        public IValeAlmacenQueryMonthBuilder WithMonth(int month) {
            if (month > 0) this._Conditionals.Add(new Conditional("ALMMP_MES", month.ToString()));
            return this;
        }

        public IValeAlmacenQueryYearBuilder WithYear(int year) {
            this._Conditionals.Add(new Conditional("ALMMP_ANIO", year.ToString()));
            return this;
        }
    }
}
