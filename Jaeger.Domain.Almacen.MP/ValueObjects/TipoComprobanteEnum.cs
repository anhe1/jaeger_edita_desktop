﻿/// develop:
/// purpose: almacen de materia prima: tipos de movientos para los registros de los productos del almacen
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.MP.ValueObjects {
    /// <summary>
    /// tipos de comprobante del almacen de materia prima (Entrada, Salida, Ajuste, Inventario)
    /// </summary>
    public enum TipoComprobanteEnum {
        [Description("Ninguno")]
        None,
        [Description("Entrada")]
        Entrada = 1,
        [Description("Salida")]
        Salida = 2,
        [Description("Traslado")]
        Traslado = 3,
        [Description("Devolución")]
        Devolucion = 4,
        [Description("Ajuste")]
        Ajuste = 5,
        [Description("Inventario")]
        Inventario = 99
    }
}
