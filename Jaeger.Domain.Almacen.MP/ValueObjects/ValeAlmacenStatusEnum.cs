using System.ComponentModel;

namespace Jaeger.Domain.Almacen.MP.ValueObjects {
    /// <summary>
    /// lista de estado del vale de almacen
    /// </summary>
    public enum ValeAlmacenStatusEnum {
        [Description("Cancelado")]
        Cancelado,
        [Description("Pendiente")]
        Pendiente,
        [Description("Entregado")]
        Entregado,
        [Description("Recibido")]
        Recibido,
        [Description("Rechazado")]
        Rechazado
    }
}