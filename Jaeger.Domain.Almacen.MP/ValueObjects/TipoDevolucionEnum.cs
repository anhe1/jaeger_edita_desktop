﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.MP.ValueObjects {
    public enum TipoDevolucionEnum {
        [Description("No definido")]
        None = 0,
        [Description("Por Proveedor")]
        Proveedor = 1,
        [Description("Producto con defecto")]
        PorDefecto = 2,
        [Description("No se llevó a cabo la operación.")]
        SinOperacion = 3,
        [Description("Materia prima del Cliente")]
        DelCliente = 4,
        [Description("Devolución de mercancías")]
        Devolucion = 5,
        [Description("Materia prima no solicitada")]
        SinSolicitud = 6,
        [Description("Solicita Administrador")]
        Administrador = 99
    }
}