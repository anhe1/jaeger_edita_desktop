﻿/// develop:
/// purpose: almacen de materia prima: tipos de movientos para los registros de los productos del almacen
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.MP.ValueObjects {
    /// <summary>
    /// tipo de movimiento, si es interno es para un departamento, si es externo es para un proveedor
    /// </summary>
    public enum MovimientoSubTipoEnum {
        [Description("No Definido")]
        None,
        [Description("Interno")]
        Interno,
        [Description("Externo")]
        Externo
    }
}
