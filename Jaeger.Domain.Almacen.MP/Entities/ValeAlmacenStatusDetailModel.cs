﻿using Jaeger.Domain.Almacen.MP.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class ValeAlmacenStatusDetailModel : ValeAlmacenStatusModel, IValeAlmacenStatusModel, IValeAlmacenStatusDetailModel {
        public ValeAlmacenStatusDetailModel() : base() {

        }

        public BindingList<ValeAlmacenRelacionModel> Relaciones { get; set; }
    }
}
