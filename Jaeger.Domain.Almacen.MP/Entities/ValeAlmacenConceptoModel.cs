﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.MP.ValueObjects;

namespace Jaeger.Domain.Almacen.MP.Entities {
    [SugarTable("MVAMP", "amp: conceptos de vales de entradas, salidas y inventario.")]
    public class ValeAlmacenConceptoModel : BasePropertyChangeImplementation, IValeAlmacenConcepto {
        #region declaraciones
        private int index;
        private bool activo;
        private int subIndex;
        private int idProducto;
        private int idModelo;
        private int idUnidad;
        private int idTipo;
        private int efecto;
        private int ordenCompra;
        private int ordenProduccion;
        private string unidad;
        private decimal cantidadEntrada;
        private decimal cantidadSalida;
        //private string producto;
        private string descripcion;
        private string noIdentificacion;
        private string especificacion;
        private string marca;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private decimal _CostoUnitario;
        private decimal _CostoUnidad;
        private decimal _ValorUnitario;
        private decimal _Unitario;
        private decimal _SubTotal;
        private decimal _Descuento;
        private decimal _Importe;
        private decimal _TasaIVA;
        private decimal _TrasladoIVA;
        private decimal _Total;
        private decimal _UnidadFactor;
        private BindingList<ValeAlmacenConceptoParte> partes;
        #endregion

        public ValeAlmacenConceptoModel() {
            this.activo = true;
            this.partes = new BindingList<ValeAlmacenConceptoParte>();
        }

        /// <summary>
        /// obtener o establecer el indice del concepto del vale de almacen
        /// </summary>
        [DataNames("MVAMP_ID")]
        [SugarColumn(ColumnName = "MVAMP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdConcepto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("MVAMP_A")]
        [SugarColumn(ColumnName = "MVAMP_A", ColumnDescription = "obtener o establecer registro activo", IsNullable = true)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el vale del almacen
        /// </summary>
        [DataNames("MVAMP_ALMMP_ID")]
        [SugarColumn(ColumnName = "MVAMP_ALMMP_ID", ColumnDescription = "obtener o establecer el indice de relacion con el vale del almacen", IsNullable = true)]
        public int IdComprobante {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si es un producto o modelo
        /// </summary>
        [DataNames("MVAMP_DOC_ID")]
        [SugarColumn(ColumnName = "MVAMP_DOC_ID", ColumnDescription = "obtener o establecer si es un producto o modelo", IsNullable = true)]
        public int IdTipoComprobante {
            get {
                return this.idTipo;
            }
            set {
                this.idTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de movimiento dentro del almacen
        /// </summary>
        [DataNames("MVAMP_CTEFC_ID")]
        [SugarColumn(ColumnName = "MVAMP_CTEFC_ID", ColumnDescription = "obtener o establecer tipo de movimiento dentro del almacen")]
        public int IdTipoMovimiento {
            get {
                return this.efecto;
            }
            set {
                this.efecto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        [DataNames("MVAMP_CTPRD_ID")]
        [SugarColumn(ColumnName = "MVAMP_CTPRD_ID", ColumnDescription = "indice de relación con la tabla de productos", IsNullable = false)]
        public int IdProducto {
            get {
                return this.idProducto;
            }
            set {
                this.idProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        [DataNames("MVAMP_CTMDL_ID")]
        [SugarColumn(ColumnName = "MVAMP_CTMDL_ID", ColumnDescription = "obtener o establecer el indice del modelo", IsNullable = true)]
        public int IdModelo {
            get {
                return this.idModelo;
            }
            set {
                this.idModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        [DataNames("MVAMP_CTUND_ID")]
        [SugarColumn(ColumnName = "MVAMP_CTUND_ID", ColumnDescription = "obtener o establecer la unidad de almacen", IsNullable = true)]
        public int IdUnidad {
            get {
                return this.idUnidad;
            }
            set {
                this.idUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor de la unidad
        /// </summary>
        [DataNames("MVAMP_UNDF")]
        [SugarColumn(ColumnName = "MVAMP_UNDF", ColumnDescription = "factor de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal UnidadFactor {
            get { return this._UnidadFactor; }
            set {
                this._UnidadFactor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden de compra relacionada
        /// </summary>
        [DataNames("MVAMP_OCMP")]
        [SugarColumn(ColumnName = "MVAMP_OCMP", ColumnDescription = "obtener o establecer el numero de orden de compra relacionada", IsNullable = true)]
        public int OrdenCompra {
            get {
                return this.ordenCompra;
            }
            set {
                this.ordenCompra = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden de produccion relacionada
        /// </summary>
        [DataNames("MVAMP_OPRD")]
        [SugarColumn(ColumnName = "MVAMP_OPRD", ColumnDescription = "obtener o establecer el numero de orden de produccion relacionada", IsNullable = true)]
        public int OrdenProduccion {
            get {
                return this.ordenProduccion;
            }
            set {
                this.ordenProduccion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad entrada del movimiento
        /// </summary>
        [DataNames("MVAMP_CNTE")]
        [SugarColumn(ColumnName = "MVAMP_CNTE", ColumnDescription = "obtener o establecer la cantidad entrada del movimiento", IsNullable = true)]
        public decimal Entrada {
            get {
                return this.cantidadEntrada;
            }
            set {
                this.cantidadEntrada = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de salida del movimiento
        /// </summary>
        [DataNames("MVAMP_CNTS")]
        [SugarColumn(ColumnName = "MVAMP_CNTS", ColumnDescription = "obtener o establecer la cantidad de salida del movimiento", IsNullable = true)]
        public decimal Salida {
            get {
                return this.cantidadSalida;
            }
            set {
                this.cantidadSalida = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal Cantidad {
            get {
                if (this.IdTipoMovimiento == (int)TipoComprobanteEnum.Entrada)
                    return this.Entrada;
                else return this.Salida;
            }
            set {
                if (this.IdTipoMovimiento == (int)TipoComprobanteEnum.Entrada)
                    this.Entrada = value;
                else
                    this.Salida = value;
            }
        }

        /// <summary>
        /// obtener o establecer la unidad personalizada de la unidad
        /// </summary>
        [DataNames("MVAMP_CTUND")]
        [SugarColumn(ColumnName = "MVAMP_CTUND", ColumnDescription = "obtener o establecer la unidad personalizada de la unidad", IsNullable = true)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del modelo
        /// </summary>
        [DataNames("MVAMP_NOM")]
        [SugarColumn(ColumnName = "MVAMP_NOM", ColumnDescription = "obtener o establecer la descripcion del modelo", IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer especificacones del modelo
        /// </summary>
        [DataNames("MVAMP_ESPC")]
        [SugarColumn(ColumnName = "MVAMP_ESPC", ColumnDescription = "obtener o establecer especificacones del modelo", IsNullable = true)]
        public string Especificacion {
            get {
                return this.especificacion;
            }
            set {
                this.especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la marca del modelo
        /// </summary>
        [DataNames("MVAMP_MRC")]
        [SugarColumn(ColumnName = "MVAMP_MRC", ColumnDescription = "obtener o establecer la marca del modelo", IsNullable = true)]
        public string Marca {
            get {
                return this.marca;
            }
            set {
                this.marca = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador del modelo SKU
        /// </summary>
        [DataNames("MVAMP_SKU")]
        [SugarColumn(ColumnName = "MVAMP_SKU", ColumnDescription = "obtener o establecer identificador del modelo SKU", IsNullable = true)]
        public string NoIdentificacion {
            get {
                return this.noIdentificacion;
            }
            set {
                this.noIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo unitario (MVAMP_UNTC)
        /// </summary>
        [DataNames("MVAMP_UNTC")]
        [SugarColumn(ColumnName = "MVAMP_UNTC", ColumnDescription = "costo unitario", Length = 14, DecimalDigits = 4)]
        public decimal CostoUnitario {
            get { return this._CostoUnitario; }
            set {
                this._CostoUnitario = value;
                this._CostoUnidad = value * this._UnidadFactor;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo de la unidad (MVAMP_UNDC)
        /// </summary>
        [DataNames("MVAMP_UNDC")]
        [SugarColumn(ColumnName = "MVAMP_UNDC", ColumnDescription = "costo de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal CostoUnidad {
            get { return this._CostoUnidad; }
            set {
                this._CostoUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario por pieza (MVAMP_UNTR)
        /// </summary>
        [DataNames("MVAMP_UNTR")]
        [SugarColumn(ColumnName = "MVAMP_UNTR", ColumnDescription = "unitario por pieza", Length = 14, DecimalDigits = 4)]
        public decimal ValorUnitario {
            get { return this._ValorUnitario; }
            set {
                this._ValorUnitario = value;
                this.Unitario = this._ValorUnitario * this.UnidadFactor;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario de la unidad (MVAMP_UNTR2)
        /// </summary>
        [DataNames("MVAMP_UNTR2")]
        [SugarColumn(ColumnName = "MVAMP_UNTR2", ColumnDescription = "unitario de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal Unitario {
            get { return this._Unitario; }
            set {
                this._Unitario = value;
                this.SubTotal = this.Cantidad * this.Unitario;
                this.Importe = this.SubTotal - this.Descuento;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub total de la partida
        /// </summary>
        [DataNames("MVAMP_SBTTL")]
        [SugarColumn(ColumnName = "MVAMP_SBTTL", ColumnDescription = "sub total de la partida", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set {
                this._SubTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estsablecer el importe del descuento aplicable al producto o sevicio 
        /// </summary>
        [DataNames("MVAMP_DESC")]
        [SugarColumn(ColumnName = "MVAMP_DESC", ColumnDescription = "importe del descuento aplicable al producto o sevicio ", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get { return this._Descuento; }
            set {
                this._Descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del importe de la partida (SubTotal - Descuento)
        /// </summary>
        [DataNames("MVAMP_IMPRT")]
        [SugarColumn(ColumnName = "MVAMP_IMPRT", ColumnDescription = "valor del importe de la partida (SubTotal - Descuento)", Length = 14, DecimalDigits = 4)]
        public decimal Importe {
            get { return this._Importe; }
            set {
                this._Importe = value;
                this.TrasladoIVA = this._Importe * this.TasaIVA;
                this.Total = this._Importe + this.TrasladoIVA;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tasa del IVA
        /// </summary>
        [DataNames("MVAMP_TSIVA")]
        [SugarColumn(ColumnName = "MVAMP_TSIVA", ColumnDescription = "tasa del IVA", Length = 14, DecimalDigits = 4)]
        public decimal TasaIVA {
            get { return this._TasaIVA; }
            set {
                this._TasaIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe trasladado del impuesto IVA
        /// </summary>
        [DataNames("MVAMP_TRIVA")]
        [SugarColumn(ColumnName = "MVAMP_TRIVA", ColumnDescription = "importe trasladado del impuesto IVA", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get { return this._TrasladoIVA; }
            set {
                this._TrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de la partida (Importe + TrasladoIVA)
        /// </summary>
        [DataNames("MVAMP_TOTAL")]
        [SugarColumn(ColumnName = "MVAMP_TOTAL", ColumnDescription = "total de la partida (Importe + TrasladoIVA)", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("MVAMP_FN")]
        [SugarColumn(ColumnName = "MVAMP_FN", ColumnDescription = "obtener o establecer la fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que creo el registro
        /// </summary>
        [DataNames("MVAMP_USR_N")]
        [SugarColumn(ColumnName = "MVAMP_USR_N", ColumnDescription = "obtener o establecer la clave del usuario que creo el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("MVAMP_FM")]
        [SugarColumn(ColumnName = "MVAMP_FM", ColumnDescription = "obtener o establecer la ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>
        [DataNames("_ampvlc_usr_m")]
        [SugarColumn(ColumnName = "MVAMP_USR_M", ColumnDescription = "obtener o establecer la clave del ultimo usuario que modifico el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenConceptoParte> Partes {
            get {
                return this.partes;
            }
            set {
                this.partes = value;
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
