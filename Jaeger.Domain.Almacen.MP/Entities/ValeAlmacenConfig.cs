﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Almacen.MP.Entities {
    [JsonObject("conf")]
    public class ValeAlmacenConfig {

        public ValeAlmacenConfig() {
            this.LeyendaPiePagina = "Esperamos su factura.";
        }

        [JsonProperty("leyendaPiePagina")]
        public string LeyendaPiePagina { get; set; }

        public string Json() {
            return "";
        }
    }
}
