﻿using System;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class ValeAlmacenPrinter : ValeAlmacenDetailModel, IValeAlmacenDetailModel {

        public ValeAlmacenPrinter() {

        }

        public ValeAlmacenPrinter(ValeAlmacenDetailModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenPrinter>(source, this);
            this.Conceptos = source.Conceptos;
        }

        public ValeAlmacenConfig Configuracion { get; set; }

        public string Departamento { get; set; }

        public string ClaveDevolucion { get; set; }

        public string EfectoText {
            get {
                return Enum.GetName(typeof(TipoComprobanteEnum), this.TipoComprobante);
            }
            set {
                this.TipoComprobante = (TipoComprobanteEnum)Enum.Parse(typeof(TipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }

        public string[] QrText() {
            return new string[] { "||VL|", this.IdAlmacen.ToString("0#"), this.IdTipoAlmacen.ToString("#0"), this.IdTipoComprobante.ToString("0#"), this.IdTipoMovimiento.ToString("0#"), this.Version, this.Folio.ToString(), this.Serie, this.ReceptorRFC, this.Receptor, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Total.ToString("0.00"),
                "|Articulos=", this.Conceptos.Count.ToString(),"|", "creo=", this.Creo, "||" };
        }
    }
}
