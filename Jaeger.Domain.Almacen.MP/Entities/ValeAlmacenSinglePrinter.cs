﻿using SqlSugar;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.MP.Entities {
    [SugarTable("ALMMP")]
    public class ValeAlmacenSinglePrinter : ValeAlmacenSingleModel {
        public ValeAlmacenSinglePrinter() : base() {

        }

        /// <summary>
        /// obtener o establecer descripcion del almacen
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Almacen {
            get; set;
        }


        public new virtual void SetValues(ValeAlmacenSingleModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenSingleModel>(source, this);
        }
    }
}
