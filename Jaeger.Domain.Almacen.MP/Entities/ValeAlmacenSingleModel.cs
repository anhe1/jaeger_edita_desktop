﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.MP.Contracts;

namespace Jaeger.Domain.Almacen.MP.Entities {
    [SugarTable("ALMMP")]
    public class ValeAlmacenSingleModel : ValeAlmacenDetailModel, IValeAlmacenDetailModel, IValeAlmacenModel {
        #region declaraciones
        private string _MotivoCancelacion;
        private DateTime? _FechaCancela;
        private string _Creo;
        private string _Observaciones;
        #endregion

        public ValeAlmacenSingleModel() : base() {

        }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        [DataNames("ALMMPS_FN")]
        [SugarColumn(IsIgnore = true)]
        public DateTime? FechaCancela {
            get {
                if (this._FechaCancela >= new DateTime(1900, 1, 1))
                    return this._FechaCancela;
                return null;
            }
            set {
                this._FechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("ALMMPS_USR_N")]
        [SugarColumn(IsIgnore = true)]
        public string Cancela {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPS_CTMTV")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveCancelacion {
            get { return this._MotivoCancelacion; }
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("ALMMPS_NOTA")]
        [SugarColumn(IsIgnore = true)]
        public string NotaB {
            get { return this._Observaciones; }
            set {
                this._Observaciones = value;
                this.OnPropertyChanged();
            }
        }

        public virtual void SetValues(IValeAlmacenDetailModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenSingleModel>((ValeAlmacenDetailModel)source, this);
        }

        public new virtual void SetValues(ValeAlmacenDetailModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenSingleModel>(source, this);
        }

        public virtual void SetValues(ValeAlmacenSingleModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenSingleModel, ValeAlmacenSingleModel>(source, this);
        }
    }
}
