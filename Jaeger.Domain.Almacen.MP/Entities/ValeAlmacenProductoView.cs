﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class ValeAlmacenProductoView : ValeAlmacenSingleModel {
        [DataNames("MVAMP_ID")]
        public int IdConcepto { get; set; }
        [DataNames("MVAMP_CTTIPO_ID")]
        public int IdTipo { get; set; }
        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        [DataNames("MVAMP_CTPRD_ID")]
        public int IdProducto { get; set; }
        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        [DataNames("MVAMP_CTMDL_ID")]
        public int IdModelo { get; set; }
    }
}
