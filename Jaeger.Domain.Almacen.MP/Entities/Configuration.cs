﻿using Jaeger.DataAccess.Almacen.MP.Contracts;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class Configuration : IConfiguration {
        public string LeyendaPiePagina { get; set; }
    }
}
