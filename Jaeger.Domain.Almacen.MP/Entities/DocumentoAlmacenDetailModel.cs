﻿using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Abstractions;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.MP.Contracts;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class DocumentoAlmacenDetailModel : ComprobanteAlmacen, IComprobanteAlmacen, IDocumentoAlmacen {
        #region declaraciones
        private bool _ready;
        private BindingList<ItemSelectedModel> _relacionDirectorio;
        private TipoComprobanteEnum _TipoComprobante;
        #endregion

        public DocumentoAlmacenDetailModel() : base() {
            this._relacionDirectorio = new BindingList<ItemSelectedModel>(((TipoRelacionComericalEnum[])Enum.GetValues(typeof(TipoRelacionComericalEnum))).Where(it => it.ToString() != "None")
                .Select(c => new ItemSelectedModel((int)c, false, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description))
                .ToList());
        }

        /// <summary>
        /// obtener o establecer indice del documento
        /// </summary>
        public new int IdDocumento {
            get { return base.IdDocumento; }
            set { base.IdDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el objeto es de solo lectura
        /// </summary>
        public new bool IsReadOnly {
            get { return base.IsReadOnly; }
            set {
                base.IsReadOnly = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el documento debe afectar los saldos
        /// </summary>
        public new bool IsSum {
            get { return base.IsSum; }
            set {
                base.IsSum = value;
                this.OnPropertyChanged();
            }
        }

        public new AlmacenEnum Tipo {
            get { return base.Tipo; }
            set { base.Tipo = value;
                this.OnPropertyChanged();
            } 
        }

        /// <summary>
        /// tipos de comprobante del almacen de materia prima (Entrada, Salida, Ajuste, Inventario)
        /// </summary>
        public TipoComprobanteEnum TipoComprobante {
            get { return this._TipoComprobante; }
            set {
                this._TipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de movimiento del almacen
        /// </summary>
        public new TipoMovimientoEnum TipoMovimiento {
            get { return base.TipoMovimiento; }
            set {
                base.TipoMovimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del tipo de operacion
        /// </summary>
        public new string Descripcion {
            get { return base.Descripcion; }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el formato de impresion del documento
        /// </summary>
        public new string FormatoImpreso {
            get { return base.FormatoImpreso; }
            set { base.FormatoImpreso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia de tipos de objetos utilizados como receptores del tipo de movimiento
        /// </summary>
        public new string Receptores {
            get { return base.Receptores; }
            set { base.Receptores = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objetos del directorio
        /// </summary>
        public BindingList<ItemSelectedModel> ReceptorTipos {
            get {
                if (this._ready == false) {
                    if (this.Receptores != null) {
                        if (this.Receptores.Length > 0) {
                            var d = this.Receptores.Split(',');
                            this._relacionDirectorio.ToList().ForEach(it => { it.Selected = d.Contains(it.Id.ToString()); });
                        }
                    }
                    this._ready = true;
                }
                return this._relacionDirectorio;
            }
            set {
                this._relacionDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el descriptor del tipo de documento {Clave:Descripcion}
        /// </summary>
        public override string Descriptor {
            get { return string.Format("{0}: {1}", this.IdDocumento.ToString("00"), this.Descripcion); }
        }
    }
}
