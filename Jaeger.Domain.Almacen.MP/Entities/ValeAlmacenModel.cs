﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.MP.Contracts;

namespace Jaeger.Domain.Almacen.MP.Entities {
    /// <summary>
    /// vale del almacen de materia prima
    /// </summary>
    [SugarTable("ALMMP", "ALMMP: documentos de almacen de materia prima")]
    public class ValeAlmacenModel : BasePropertyChangeImplementation, IValeAlmacenModel {
        #region declaraciones
        private int _IdVale;
        private int _IdEfecto;
        private int _IdTipoAlmacen;
        private int _IdStatus;
        private int _IdDirectorio;
        private string _Version;
        private DateTime _FechaEmision;
        private DateTime? _FechaIngreso;
        private int _Folio;
        private string _Serie;
        private decimal _TotalDescuento;
        private decimal _TotalTrasladoIVA;
        private decimal _SubTotal;
        private decimal _Total;
        private string _Entrega;
        private string _Observaciones;
        private DateTime _FechaNuevo;
        private string _Creo;
        private DateTime? _FechaModifica;
        private string _Modifica;
        private string _ReceptorRFC;
        private string _Receptor;
        private int idRelacion;
        private string _Referencia;
        private string idDocumento;
        private int _IdTipoComprobante;
        private string _urlPDF;
        private int _IdSerie;
        private int _IdCveDevolucion;
        #endregion
        private int _IdAlmacen;
        public ValeAlmacenModel() {
            this._FechaNuevo = DateTime.Now;
            this._FechaEmision = DateTime.Now;
            this._IdTipoAlmacen = 1;
            this._IdAlmacen = 0;
            this._IdStatus = 1;
            this._Version = "2.0";
            this._IdCveDevolucion = -1;
        }

        /// <summary>
        /// obtener o establecer indice (ALMMP_ID)
        /// </summary>
        [DataNames("ALMMP_ID")]
        [SugarColumn(ColumnName = "ALMMP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdComprobante {
            get { return this._IdVale; }
            set {
                this._IdVale = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de almacenes
        /// </summary>
        [DataNames("ALMMP_CTALM_ID")]
        [SugarColumn(ColumnName = "ALMMP_CTALM_ID", ColumnDescription = "indice de la tabla de almacenes")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer incide de tipo de almacen (MP, PT, DP, PP)
        /// </summary>
        [DataNames("ALMMP_TPALM_ID")]
        [SugarColumn(ColumnName = "ALMMP_TPALM_ID", ColumnDescription = "incide de tipo de almacen (MP, PT, DP, PP)")]
        public int IdTipoAlmacen {
            get { return this._IdTipoAlmacen; }
            set {
                this._IdTipoAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del documento
        /// </summary>
        [DataNames("ALMMP_VER")]
        [SugarColumn(ColumnName = "ALMMP_VER", ColumnDescription = "version del comprobante", Length = 3, DefaultValue = "2.0")]
        public string Version {
            get { return this._Version; }
            set {
                this._Version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("ALMMP_DOC_ID")]
        [SugarColumn(ColumnName = "ALMMP_DOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoComprobante {
            get { return this._IdTipoComprobante; }
            set {
                this._IdTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de documento
        /// </summary>
        [DataNames("ALMMP_CTEFC_ID")]
        [SugarColumn(ColumnName = "ALMMP_CTEFC_ID", ColumnDescription = "Efecto del comprobante")]
        public int IdTipoMovimiento {
            get { return this._IdEfecto; }
            set {
                this._IdEfecto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtenero o establcer indice del tipo de devolucion
        /// </summary>
        [DataNames("ALMMP_CTDEV_ID")]
        [SugarColumn(ColumnName = "ALMMP_CTDEV_ID", ColumnDescription = "indice del tipo de devolucion")]
        public int IdCveDevolucion {
            get { return this._IdCveDevolucion; }
            set { this._IdCveDevolucion = value;
                this.OnPropertyChanged();
            } 
        }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        [DataNames("ALMMP_STTS_ID")]
        [SugarColumn(ColumnName = "ALMMP_STTS_ID", ColumnDescription = "indice del catalogo de status")]
        public int IdStatus {
            get { return this._IdStatus; }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("ALMMP_UUID")]
        [SugarColumn(ColumnName = "ALMMP_UUID", ColumnDescription = "id de documento", Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("ALMMP_CTREL_ID")]
        [SugarColumn(ColumnName = "ALMMP_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("ALMMP_DRCTR_ID")]
        [SugarColumn(ColumnName = "ALMMP_DRCTR_ID", ColumnDescription = "indice del directorio de clientes", IsNullable = true)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento relacionado
        /// </summary>
        [DataNames("ALMMP_CTDEP_ID")]
        [SugarColumn(ColumnName = "ALMMP_CTDEP_ID", ColumnDescription = "indice del departamento relacionado", DefaultValue = "0")]
        public int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        [DataNames("ALMMP_FOLIO")]
        [SugarColumn(ColumnName = "ALMMP_FOLIO", ColumnDescription = "folio de control interno", IsNullable = false)]
        public int Folio {
            get { return this._Folio; }
            set {
                this._Folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la serie
        /// </summary>
        [DataNames("ALMMP_CTSR_ID")]
        [SugarColumn(ColumnName = "ALMMP_CTSR_ID", ColumnDescription = "indice del id de la serie", IsNullable = true)]
        public int IdSerie {
            get { return this._IdSerie; }
            set {
                this._IdSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer serie de control interno
        /// </summary>
        [DataNames("ALMMP_SERIE")]
        [SugarColumn(ColumnName = "ALMMP_SERIE", ColumnDescription = "serie de control interno", IsNullable = true)]
        public string Serie {
            get { return this._Serie; }
            set {
                this._Serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>
        [DataNames("ALMMP_FECEMS")]
        [SugarColumn(ColumnName = "ALMMP_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get { return this._FechaEmision; }
            set {
                this._FechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estabelcer clave de control del directorio
        /// </summary>
        [DataNames("ALMMP_RFCR")]
        [SugarColumn(ColumnName = "ALMMP_RFCR", ColumnDescription = "clave de control del directorio", Length = 10, IsNullable = true)]
        public string ReceptorRFC {
            get { return this._ReceptorRFC; }
            set {
                this._ReceptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o denominacion del receptor
        /// </summary>
        [DataNames("ALMMP_NOM")]
        [SugarColumn(ColumnName = "ALMMP_NOM", ColumnDescription = "nombre o razon social del receptor", Length = 255)]
        public string Receptor {
            get { return this._Receptor; }
            set {
                this._Receptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("ALMMP_CNTCT")]
        [SugarColumn(ColumnName = "ALMMP_CNTCT", ColumnDescription = "", Length = 100, IsNullable = true)]
        public string Contacto {
            get { return this._Entrega; }
            set {
                this._Entrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("ALMMP_RFRNC")]
        [SugarColumn(ColumnName = "ALMMP_RFRNC", ColumnDescription = "", Length = 100, IsNullable = true)]
        public string Referencia {
            get { return this._Referencia; }
            set {
                this._Referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de ingreso al almacen
        /// </summary>
        [DataNames("ALMMP_FCING")]
        [SugarColumn(ColumnName = "ALMMP_FCING", ColumnDescription = "fecha de ingreso al almacen", IsNullable = true)]
        public DateTime? FechaIngreso {
            get { return this._FechaIngreso; }
            set {
                this._FechaIngreso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub total
        /// </summary>
        [DataNames("ALMMP_SBTTL")]
        [SugarColumn(ColumnName = "ALMMP_SBTTL", ColumnDescription = "sub total", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set {
                this._SubTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del descuento
        /// </summary>
        [DataNames("ALMMP_DSCT")]
        [SugarColumn(ColumnName = "ALMMP_DSCT", ColumnDescription = "total del descuento", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalDescuento {
            get { return this._TotalDescuento; }
            set {
                this._TotalDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("ALMMP_TRIVA")]
        [SugarColumn(ColumnName = "ALMMP_TRIVA", ColumnDescription = "total traslado IVA", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIVA {
            get { return this._TotalTrasladoIVA; }
            set {
                this._TotalTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Total = SubTotal + TotalTrasladoIVA
        /// </summary>
        [DataNames("ALMMP_GTOTAL")]
        [SugarColumn(ColumnName = "ALMMP_GTOTAL", ColumnDescription = "gran total", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("ALMMP_OBSRV")]
        [SugarColumn(ColumnName = "ALMMP_OBSRV", ColumnDescription = "observaciones", Length = 100, IsNullable = true)]
        public string Nota {
            get { return this._Observaciones; }
            set {
                this._Observaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("ALMMP_URL_PDF")]
        [SugarColumn(ColumnName = "ALMMP_URL_PDF", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text", IsNullable = true)]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        [DataNames("_ALMMP_fn", "ALMMP_FN")]
        [SugarColumn(ColumnName = "ALMMP_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("ALMMP_USU_N")]
        [SugarColumn(ColumnName = "ALMMP_USU_N", ColumnDescription = "clave del usuario creador del registro", Length = 10)]
        public string Creo {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_ALMMP_fm", "ALMMP_FM")]
        [SugarColumn(ColumnName = "ALMMP_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this._FechaModifica >= firstGoodDate)
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_ALMMP_usu_m", "ALMMP_USU_M")]
        [SugarColumn(ColumnName = "ALMMP_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this._Modifica; }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMP_ANIO")]
        //[SugarColumn(ColumnName = "ALMMP_ANIO", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        [SugarColumn(ColumnName = "ALMMP_ANIO", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public int Ejercicio { get; set; }

        [DataNames("ALMMP_MES")]
        //[SugarColumn(ColumnName = "ALMMP_MES", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        [SugarColumn(ColumnName = "ALMMP_MES", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public int Periodo { get; set; }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
