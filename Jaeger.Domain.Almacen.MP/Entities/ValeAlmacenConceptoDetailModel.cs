﻿using SqlSugar;
using System.ComponentModel;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.MP.Contracts;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class ValeAlmacenConceptoDetailModel : ValeAlmacenConceptoModel, IValeAlmacenConcepto, IDataErrorInfo {
        /// <summary>
        /// obtener o establecer si es un producto o modelo
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ProdServTipoEnum Tipo {
            get {
                if (this.IdTipoComprobante == 1)
                    return ProdServTipoEnum.Producto;
                else if (this.IdTipoComprobante == 2)
                    return ProdServTipoEnum.Servicio;
                else
                    return ProdServTipoEnum.NoDefinido;
            }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de movimiento dentro del almacen
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoComprobanteEnum TipoComprobante {
            get { return (TipoComprobanteEnum)this.IdTipoComprobante; }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoMovimientoEnum TipoMovimiento {
            get { return (TipoMovimientoEnum)this.IdTipoMovimiento; }
            set {
                this.IdTipoMovimiento = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get { return string.Join("|", new string[] { this.Cantidad.ToString("#0.00"), this.Unidad, this.Descripcion, this.NoIdentificacion, this.Creo }); }
        }

        public new ValeAlmacenConceptoModel Clone() {
            var duplicado = (ValeAlmacenConceptoModel)base.Clone();
            return duplicado;
        }

        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                //if ((this.Efecto == MovimientoTipoEnum.Entrada && this.cantidadEntrada <= 0) | this.Efecto == MovimientoTipoEnum.Salida && this.cantidadSalida <= 0)
                //    return "Por favor ingrese datos válidos en esta fila!";
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                //if (columnName == "Entrada" && this.Efecto == MovimientoTipoEnum.Entrada && this.Entrada <= 0)
                //    return "Es necesario ingresar una cantidad";
                return string.Empty;
            }
        }
    }
}
