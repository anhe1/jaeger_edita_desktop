﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.MP.Contracts;

namespace Jaeger.Domain.Almacen.MP.Entities {
    [SugarTable("ALMMPR", "ALMMP: documentos relacionados del almacen de materia prima")]
    public class ValeAlmacenRelacionModel : BasePropertyChangeImplementation, IValeAlmacenRelacionModel {
        #region
        private int _Index;
        private string _Serie;
        private int _Folio;
        private string _Creo;
        private string _IdDocumento;
        private string _ClaveRelacion;
        private DateTime _FechaNuevo;
        private DateTime _FechaEmision;
        private int _IdTipoComprobante;
        private int _IdDirectorio;
        private string _Receptor;
        private int _IdClaveRelacion;
        private string _ReceptorRFC;
        #endregion

        public ValeAlmacenRelacionModel() {

        }

        /// <summary>
        /// obtener o establecer indice (ALMMP_ID)
        /// </summary>
        [DataNames("ALMMPR_ALMMP_ID")]
        [SugarColumn(ColumnName = "ALMMPR_ALMMP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdComprobante {
            get {
                return _Index;
            }
            set {
                _Index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_DOC_ID")]
        [SugarColumn(ColumnName = "ALMMPR_DOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoComprobante {
            get { return this._IdTipoComprobante; }
            set {
                this._IdTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_CTREL_ID")]
        [SugarColumn(ColumnName = "ALMMPR_CTREL_ID", ColumnDescription = "indice de la tabla de tipos de relacion")]
        public int IdClaveRelacion {
            get { return this._IdClaveRelacion; }
            set {
                this._IdClaveRelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_CTREL")]
        [SugarColumn(ColumnName = "ALMMPR_CTREL", ColumnDescription = "descripcion de la clave de relacion")]
        public string ClaveRelacion {
            get { return this._ClaveRelacion; }
            set {
                this._ClaveRelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_UUID")]
        [SugarColumn(ColumnName = "ALMMPR_UUID", ColumnDescription = "identificador unico", Length = 36)]
        public string IdDocumento {
            get {
                return this._IdDocumento;
            }
            set {
                this._IdDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("ALMMPR_DRCTR_ID")]
        [SugarColumn(ColumnName = "ALMMPR_DRCTR_ID", ColumnDescription = "indice del directorio de clientes", IsNullable = true)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_FOLIO")]
        [SugarColumn(ColumnName = "ALMMPR_FOLIO", ColumnDescription = "", Length = 36)]
        public int Folio {
            get {
                return _Folio;
            }
            set {
                _Folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_SERIE")]
        [SugarColumn(ColumnName = "ALMMPR_SERIE", ColumnDescription = "", Length = 36)]
        public string Serie {
            get {
                return _Serie;
            }
            set {
                _Serie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_FECEMS")]
        [SugarColumn(ColumnName = "ALMMPR_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return _FechaEmision;
            }
            set {
                _FechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_CLV")]
        [SugarColumn(ColumnName = "ALMMPR_CLV", ColumnDescription = "", Length = 36)]
        public string Clave {
            get {
                return this._ClaveRelacion;
            }
            set {
                this._ClaveRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estabelcer clave de control del directorio
        /// </summary>
        [DataNames("ALMMPR_RFCR")]
        [SugarColumn(ColumnName = "ALMMPR_RFCR", ColumnDescription = "clave de control del directorio", Length = 14, IsNullable = true)]
        public string ReceptorRFC {
            get { return this._ReceptorRFC; }
            set {
                this._ReceptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o denominacion del receptor
        /// </summary>
        [DataNames("ALMMPR_NOM")]
        [SugarColumn(ColumnName = "ALMMPR_NOM", ColumnDescription = "nombre o razon social del receptor", Length = 255)]
        public string Receptor {
            get { return this._Receptor; }
            set {
                this._Receptor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_USR_N")]
        [SugarColumn(ColumnName = "ALMMPR_USR_N", ColumnDescription = "", Length = 10)]
        public string Creo {
            get {
                return _Creo;
            }
            set {
                _Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMPR_FN")]
        [SugarColumn(ColumnName = "ALMMPR_FN", ColumnDescription = "")]
        public DateTime FechaNuevo {
            get {
                return _FechaNuevo;
            }
            set {
                _FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
