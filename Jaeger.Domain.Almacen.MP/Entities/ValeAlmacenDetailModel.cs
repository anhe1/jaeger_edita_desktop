﻿using System;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.MP.Entities {
    /// <summary>
    /// vale del almacen de materia prima
    /// </summary>
    [SugarTable("ALMMP", "amp: vales de entradas, salidas e inventario.")]
    public class ValeAlmacenDetailModel : ValeAlmacenModel, IValeAlmacenDetailModel, IValeAlmacenModel, ICloneable {
        #region declaraciones
        private BindingList<ValeAlmacenConceptoDetailModel> conceptos;
        private BindingList<ValeAlmacenStatusModel> _status;
        private BindingList<ValeAlmacenRelacionModel> _Relacion;
        #endregion

        public ValeAlmacenDetailModel() {
            this.Serie = "";
            this.Version = "2.0";
            this.Status = ValeAlmacenStatusEnum.Recibido;
            this.TipoMovimiento = TipoMovimientoEnum.Ingreso;
            this.conceptos = new BindingList<ValeAlmacenConceptoDetailModel>() { RaiseListChangedEvents = true };
            this.conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
            this._status = new BindingList<ValeAlmacenStatusModel>();
            this._Relacion = new BindingList<ValeAlmacenRelacionModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public AlmacenEnum TipoAlmacen {
            get { return (AlmacenEnum)Enum.Parse(typeof(AlmacenEnum), this.IdTipoAlmacen.ToString()); }
            set { this.IdTipoAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipos de comprobante del almacen de materia prima (Entrada, Salida, Ajuste, Inventario)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoComprobanteEnum TipoComprobante {
            get {
                if (this.IdTipoComprobante == 1)
                    return TipoComprobanteEnum.Entrada;
                else if (this.IdTipoComprobante == 2)
                    return TipoComprobanteEnum.Salida;
                else if (this.IdTipoComprobante == 3)
                    return TipoComprobanteEnum.Traslado;
                else if (this.IdTipoComprobante == 4)
                    return TipoComprobanteEnum.Devolucion;
                else if (this.IdTipoComprobante == 5)
                    return TipoComprobanteEnum.Ajuste;
                else if (this.IdTipoComprobante == 99)
                    return TipoComprobanteEnum.Inventario;
                else
                    return TipoComprobanteEnum.None;
            }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de movimiento del almacen
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoMovimientoEnum TipoMovimiento {
            get {
                if (this.IdTipoMovimiento == 1)
                    return TipoMovimientoEnum.Ingreso;
                else if (this.IdTipoMovimiento == 2)
                    return TipoMovimientoEnum.Egreso;
                return TipoMovimientoEnum.None;
            }
            set {
                this.IdTipoMovimiento = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ValeAlmacenStatusEnum Status {
            get {
                if (this.IdStatus == 1)
                    return ValeAlmacenStatusEnum.Pendiente;
                else if (this.IdStatus == 2)
                    return ValeAlmacenStatusEnum.Entregado;
                else if (this.IdStatus == 3)
                    return ValeAlmacenStatusEnum.Recibido;
                else if (this.IdStatus == 4)
                    return ValeAlmacenStatusEnum.Rechazado;
                else
                    return ValeAlmacenStatusEnum.Cancelado;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el status del comprobante en modo texto
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string StatusText {
            get { return EnumerationExtension.GetEnumDescription((ValeAlmacenStatusEnum)this.IdStatus); }
        }

        [SugarColumn(IsIgnore = true)]
        public string TipoComprobanteText {
            get { return EnumerationExtension.GetEnumDescription((TipoComprobanteEnum)this.IdTipoComprobante); }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsEditable { get { return string.IsNullOrEmpty(this.IdDocumento); } }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenConceptoDetailModel> Conceptos {
            get {
                return this.conceptos;
            }
            set {
                if (this.conceptos != null)
                    this.conceptos.AddingNew -= new AddingNewEventHandler(this.Conceptos_AddingNew);
                this.conceptos = value;
                if (this.conceptos != null)
                    this.conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenStatusModel> Autorizacion {
            get {
                return this._status;
            }
            set {
                this._status = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenRelacionModel> Relaciones {
            get { return this._Relacion; }
            set { this._Relacion = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get {
                var d1 = string.Join("", this.Conceptos.Select(it => it.GetOriginalString));
                var d2 = string.Join("|", new string[] { this.IdAlmacen.ToString("0#"), this.IdTipoAlmacen.ToString("#0"), this.IdTipoComprobante.ToString("0#"), this.IdTipoMovimiento.ToString("0#"), this.Version, this.Folio.ToString(), this.Serie, this.ReceptorRFC, this.Receptor, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Total.ToString("0.00") });
                return "||" + string.Join("|", d2 + d1) + "||";
            }
        }

        /// <summary>
        /// obtener keyname del documento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string KeyName {
            get { return string.Format("VAL-{0}-{1}-{2}-{3}-{4}", this.Creo, this.ReceptorRFC, this.IdDocumento, this.Folio.ToString("000000"), this.FechaEmision.ToString("yyyyMMddHHmmss")); }
        }

        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            //e.NewObject = new AlmacenValeConceptoModel { IdVale = this.SubId, Efecto = this.Efecto };
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.SubTotal);
            this.TotalDescuento = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.Descuento);
            this.TotalTrasladoIVA = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.TrasladoIVA);
            this.Total = (this.SubTotal - this.TotalDescuento) + (this.TotalTrasladoIVA);
        }

        public void SetValues(ValeAlmacenDetailModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenDetailModel>(source, this);
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
