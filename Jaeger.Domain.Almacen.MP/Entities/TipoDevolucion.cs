﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.MP.Entities {
    public class TipoDevolucion : BaseSingleModel {
        public TipoDevolucion() {
        }

        public TipoDevolucion(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
