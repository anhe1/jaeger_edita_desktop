﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.Abstractions;

namespace Jaeger.Domain.Almacen.MP.Entities {
    /// <summary>
    /// vale del almacen de materia prima
    /// </summary>
    [SugarTable("ALMMPS", "ALMMP: historial de cambios de status de los documentos del almacen")]
    public class ValeAlmacenStatusModel : AlmacenStatus, IValeAlmacenStatusModel {

        public ValeAlmacenStatusModel() : base() {

        }

        /// <summary>
        /// obtener o establecer indice del comprobante (ALMMP_ID)
        /// </summary>
        [DataNames("ALMMPS_ALMMP_ID")]
        [SugarColumn(ColumnName = "ALMMPS_ALMMP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdComprobante {
            get { return base.IdComprobante; }
            set {
                base.IdComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("ALMMPS_DRCTR_ID")]
        [SugarColumn(ColumnName = "ALMMPS_DRCTR_ID", ColumnDescription = "indice del directorio de clientes", IsNullable = true)]
        public new int IdDirectorio {
            get { return base.IdDirectorio; }
            set {
                base.IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status del documento antes de la modificacion
        /// </summary>
        [DataNames("ALMMPS_STTS_ID")]
        [SugarColumn(ColumnName = "ALMMPS_STTS_ID", ColumnDescription = "indice del catalogo de status")]
        public new int IdStatus {
            get { return base.IdStatus; }
            set {
                base.IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status autorizado
        /// </summary>
        [DataNames("ALMMPS_STTSB_ID")]
        [SugarColumn(ColumnName = "ALMMPS_STTSB_ID", ColumnDescription = "indice del catalogo de status")]
        public new int IdStatusB {
            get { return base.IdStatusB; }
            set {
                base.IdStatusB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la clave del motivo de modificacion del status
        /// </summary>
        [DataNames("ALMMPS_CTMTV_ID")]
        [SugarColumn(ColumnName = "ALMMPS_CTMTV_ID", ColumnDescription = "indice de la tabla de motivos de modificacion del status", IsNullable = true)]
        public new int IdCveMotivo {
            get { return base.IdCveMotivo; }
            set {
                base.IdCveMotivo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer desripcion del motivo del cambio de status (clave+descripcion)
        /// </summary>
        [DataNames("ALMMPS_CTMTV")]
        [SugarColumn(ColumnName = "ALMMPS_CTMTV", ColumnDescription = "clave del motivo del cambio de status")]
        public new string CveMotivo {
            get { return base.CveMotivo; }
            set {
                base.CveMotivo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("ALMMPS_NOTA")]
        [SugarColumn(ColumnName = "ALMMPS_NOTA", ColumnDescription = "observaciones", Length = 50, IsNullable = true)]
        public new string Nota {
            get { return base.Nota; }
            set {
                base.Nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        [DataNames("ALMMPS_FN")]
        [SugarColumn(ColumnName = "ALMMPS_FN", ColumnDescription = "fecha de creacion del registro")]
        public new DateTime FechaNuevo {
            get { return base.FechaNuevo; }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("ALMMPS_USR_N")]
        [SugarColumn(ColumnName = "ALMMPS_USR_N", ColumnDescription = "clave del usuario creador del registro")]
        public new string Creo {
            get { return base.Creo; }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
