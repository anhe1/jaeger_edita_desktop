﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Almacen.MP.Entities {
    [JsonObject("parte")]
    public class ValeAlmacenConceptoParte {
        [JsonProperty("cantidad")]
        public decimal Cantidad { get; set; }
        [JsonProperty("unidad")]
        public string Unidad { get; set; }
        [JsonProperty("noIdent")]
        public string NoIdentificacion { get; set; }
        [JsonProperty("desc")]
        public string Descripcion { get; set; }
    }
}
