﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.MP.Contracts {
    public interface IDocumentoAlmacen: IComprobanteAlmacen {
        TipoComprobanteEnum TipoComprobante { get; set; }
        BindingList<ItemSelectedModel> ReceptorTipos { get; set; }
    }
}
