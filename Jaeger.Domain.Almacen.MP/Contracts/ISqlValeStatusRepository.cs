﻿using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.MP.Contracts {
    public interface ISqlValeStatusRepository : IGenericRepository<ValeAlmacenStatusModel> {
        IValeAlmacenStatusModel Save(IValeAlmacenStatusModel item);
    }
}
