﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Domain.Almacen.MP.Contracts {
    /// <summary>
    /// catalogo de clasificacion de categorias de productos, bienes y servicios (BPS) del almacen de materia prima
    /// </summary>
    public interface ISqlCategoriaRepository : IGenericRepository<ClasificacionModel> {
        /// <summary>
        /// obtener una categoria por el indice
        /// </summary>
        new ClasificacionModel GetById(int id);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener lista de categorias
        /// </summary>
        new IEnumerable<ClasificacionModel> GetList();

        /// <summary>
        /// obtener lista de catgorias relacionados por el sub indice de categorias (IdSubCategoria)
        /// </summary>
        //IEnumerable<CategoriaModel> GetList(int subId);

        /// <summary>
        /// obtener vista de categorias
        /// </summary>
        IEnumerable<ClasificacionSingle> GetCategorias();

        //IEnumerable<CategoriaMateriaPrimaSingle> GetCatProductos();
    }
}
