﻿using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.MP.Contracts {
    public interface ISqlValeRelacionRepository : IGenericRepository<ValeAlmacenRelacionModel> {
        IValeAlmacenRelacionModel Save(IValeAlmacenRelacionModel item);
    }
}
