﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.MP.Contracts {
    public interface IValeAlmacenDetailModel : IValeAlmacenModel {
        TipoComprobanteEnum TipoComprobante { get; set; }
        TipoMovimientoEnum TipoMovimiento { get; set; }
        ValeAlmacenStatusEnum Status { get; set; }
        BindingList<ValeAlmacenConceptoDetailModel> Conceptos { get; set; }
        BindingList<ValeAlmacenStatusModel> Autorizacion { get; set; }
        BindingList<ValeAlmacenRelacionModel> Relaciones { get; set; }
        bool IsEditable { get; }
    }
}
