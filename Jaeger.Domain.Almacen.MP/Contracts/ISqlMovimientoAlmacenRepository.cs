﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;

namespace Jaeger.DataAccess.Almacen.MP.Contracts {
    public interface ISqlMovimientoAlmacenRepository : IGenericRepository<ValeAlmacenConceptoModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        bool Existencia(List<IConditional> conditionals);
    }
}
