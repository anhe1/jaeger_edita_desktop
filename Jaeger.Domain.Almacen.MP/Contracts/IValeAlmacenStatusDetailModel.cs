﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.MP.Entities;

namespace Jaeger.Domain.Almacen.MP.Contracts {
    public interface IValeAlmacenStatusDetailModel : IValeAlmacenStatusModel {
        BindingList<ValeAlmacenRelacionModel> Relaciones { get; set; }
    }
}
