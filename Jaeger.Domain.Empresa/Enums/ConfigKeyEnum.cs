﻿using System.ComponentModel;

namespace Jaeger.Domain.Empresa.Enums {
    /// <summary>
    /// Llave
    /// </summary>
    public enum ConfigKeyEnum {
        [Description("Configuración")]
        Configuracion = 00,
        [Description("RFC")]
        RFC = 01,
        CURP = 02,
        RazonSocial = 03,
        NombreComercial = 04,
        SociedadCapital = 05,
        RegimenFiscal = 06,
        UsodeCFDI = 07,
        RegistroPatronal = 08,
        CIF = 09,
        CIFUrl = 10,
        CorreoElectronico = 11,
        Dominio = 12,
        SubDominio = 13,
        TipoPersona = 14,
        TipoVialidad = 15,
        [Description("Calle")]
        Calle = 16,
        [Description("Codigo Postal")]
        CodigoPostal = 17,
        Colonia = 18,
        NumExterior = 19,
        NumInterior = 20,
        MunicipioDelegacion = 21,
        Estado = 22,
        CodigoDePais = 23,
        Pais = 24,
        Ciudad = 25,
        Telefono = 26,
        Referencia = 27,
        Localidad = 28,
        LugarExpedicion = 29,
        OcultarPercepcion = 30,
        OcultarDeduccion = 31,
        OcultarOtrosPagos = 32,
        Token = 33,
        UserID = 34,
        Password = 35,
        ModoProductivo = 36,
        Facebook = 37,
        Youtube = 38,
        Telegram = 39,
        Whatsapp = 40,
        CuentasBancarias = 41,
        HorariosDeAtención = 42,
        Moneda = 43,
        ProveedorPAC = 44,
        UrlUpdate = 45,
        SolucionFactible = 46,
        /// <summary>
        /// configuracion de base de datos
        /// </summary>
        ConfiguracionBD = 47,
        Pagare = 48,
        Leyenda = 49,
        Expresion = 50,
        [Description("Folder")]
        Folder = 51,
        [Description("Templete")]
        Templete = 52,
        [Description("View")]
        View = 53,
        /// <summary>
        /// numero de decimales
        /// </summary>
        [Description("Decimales")]
        Decimales = 54,
        [Description("Redondeo")]
        Redondeo = 55,
        /// <summary>
        /// representa path o ruta 
        /// </summary>
        [Description("Path")]
        Path = 56,
        /// <summary>
        /// indice del movimiento aplicado al recibo de cobro
        /// </summary>
        [Description("Recibo de Cobro")]
        ReciboCobroID = 57,
        /// <summary>
        /// indice del movimiento aplicado al recibo de cobropago
        /// </summary>
        [Description("Recibo de Pago")]
        ReciboPagoID = 58,
        [Description("identificador del chat (Telegram)")]
        ChatID = 60,
        [Description("")]
        PublicKey = 61,
        /// <summary>
        /// indice del movimiento aplicado al recibo de comisiones
        /// </summary>
        [Description("Recibo de Comisión")]
        ReciboComisionID = 59,
        [Description("Versión")]
        Version = 99
    }
}
