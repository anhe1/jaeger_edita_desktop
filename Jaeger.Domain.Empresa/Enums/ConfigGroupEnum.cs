﻿using System.ComponentModel;

namespace Jaeger.Domain.Empresa.Enums {
    /// <summary>
    /// Grupos
    /// </summary>
    public enum ConfigGroupEnum {
        [Description("Administrador")]
        Administrador = 00,
        [Description("Contribuyente Emisor")]
        Contribuyente = 01,
        [Description("Domicilio Fiscal")]
        DomicilioFiscal = 02,
        [Description("CFDI")]
        CFDI = 03,
        [Description("CFDI Retención")]
        CFDRetencion = 04,
        [Description("CFDI Nómina")]
        Nomina = 05,
        [Description("CFDI Carta Porte")]
        CartaPorte = 06,
        [Description("CFDI Recepcion de Pagos")]
        RecepcionPago = 07,
        [Description("Validador")]
        Validador = 08,
        [Description("Adquisiciones")]
        Adquisiciones = 09,
        [Description("Bancos")]
        Bancos = 10,
        [Description("Almacenes")]
        Almacenes = 11,
        [Description("Tienda web")]
        Tienda = 12,
        [Description("Servicio PayPal")]
        Paypal = 13,
        [Description("Servicio Mercado Pago")]
        MercadoPago = 14,
        [Description("Redes Sociales")]
        RedesSociales = 15,
        [Description("Almacén de Producto Terminado")]
        AlmacenPT = 16,
        [Description("Almacén de Materia Prima")]
        AlmacenMP = 17,
        AlmacenPP = 18,
        Repositorio = 21, 
        [Description("Configuración Cotizador")]
        Cotizador = 92,
        /// <summary>
        /// Proveedor Autorizado de Certifiación (PAC)
        /// </summary>
        [Description("Proveedor Autorizado de Certificación")]
        ProveedorAutorizado = 93,
        [Description("Menus")]
        Menus = 94,
        [Description("Perfiles")]
        Perfiles = 95,
        [Description("Usuarios")]
        Usuarios = 96,
        [Description("Configuración")]
        Configuracion = 97,
        [Description("Configuración CP")]
        CP = 98,
        [Description("Configuración CP Lite")]
        CPLite = 99,
    }
}
