﻿using System;

namespace Jaeger.Domain.Empresa.Contracts {
    /// <summary>
    /// interface para Departamento
    /// </summary>
    public interface IDepatamentoModel {
        /// <summary>
        /// obtener o establece indice del departamento
        /// </summary>
        int IdDepartamento { get; set; }
        
        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del area
        /// </summary>
        int IdArea { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del area
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de nuevo registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer descrptor del objeto
        /// </summary>
        string Descriptor { get; }
    }
}
