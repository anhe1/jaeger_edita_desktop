﻿namespace Jaeger.Domain.Empresa.Contracts {
    public interface IEmpresaConfiguracionModel {
        /// <summary>
        /// obtener o establecer el indice 
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer la llave de la configuración
        /// </summary>
        string Key { get; set; }

        /// <summary>
        /// obtener o establecer el objeto json de configuracion
        /// </summary>
        string Data { get; set; }
    }
}
