﻿using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Contracts {
    /// <summary>
    /// Interface para modelo de parametro
    /// </summary>
    public interface IParametroModel {
        /// <summary>
        /// obtener o establecer identificador de parametro
        /// </summary>
        ConfigKeyEnum Key { get; set; }

        /// <summary>
        /// obtener o establecer grupo de parametro
        /// </summary>
        ConfigGroupEnum Group { get; set; }

        /// <summary>
        /// obtener o establecer valor do parametro
        /// </summary>
        string Data { get; set; }
    }
}
