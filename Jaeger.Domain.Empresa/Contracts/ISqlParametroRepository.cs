﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Contracts {
    /// <summary>
    /// repositorio de parametros
    /// </summary>
    public interface ISqlParametroRepository {
        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="group">Llave de grupo</param>
        IEnumerable<IParametroModel> Get(ConfigGroupEnum group);

        /// <summary>
        /// obtener lista de parametros
        /// </summary>
        /// <param name="groups">array de llave de grupos</param>
        IEnumerable<IParametroModel> Get(ConfigGroupEnum[] groups);

        /// <summary>
        /// obtener lista de llaves 
        /// </summary>
        /// <param name="keys">array de indices de llaves</param>
        IEnumerable<IParametroModel> Get(ConfigKeyEnum[] keys);

        /// <summary>
        /// almacenar parametro
        /// </summary>
        /// <param name="parameter">IParameter</param>
        /// <returns>verdadero</returns>
        bool Save(IParametroModel parameter);

        /// <summary>
        /// almacenar lista de parametros
        /// </summary>
        /// <param name="parametros">Lista de parametros</param>
        bool Save(List<IParametroModel> parametros);
    }
}
