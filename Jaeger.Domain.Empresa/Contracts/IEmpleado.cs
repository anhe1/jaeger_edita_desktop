﻿namespace Jaeger.Domain.Empresa.Contracts {
    public interface IEmpleado {
        /// <summary>
        /// obtener o establecer indice del empleado
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer clave
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes (RFC)
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion (CURP)
        /// </summary>
        string CURP { get; set; }

        /// <summary>
        /// obtenner o establecer nombre
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        string ApellidoPaterno { get; set; }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        string ApellidoMaterno { get; set; }

        /// <summary>
        /// obtener o establecer fecha de nacimiento
        /// </summary>
        System.DateTime? FecNacimiento { get; set; }

        /// <summary>
        /// obtener o establecer nacionalidad
        /// </summary>
        string Nacionalidad { get; set; }

        /// <summary>
        /// obtener o establecer lugar de nacimiento
        /// </summary>
        string LugarNacimiento { get; set; }

        /// <summary>
        /// obtener o establecer numero de seguridad social (NSS)
        /// </summary>
        string NSS { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        System.DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener nombre completo
        /// </summary>
        string FullName { get; }

    }
}
