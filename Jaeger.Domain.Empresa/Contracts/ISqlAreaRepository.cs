﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Domain.Empresa.Contracts {
    /// <summary>
    /// repositorio para areas
    /// </summary>
    public interface ISqlAreaRepository : IGenericRepository<AreaModel> {
        /// <summary>
        /// obtener lista de condicional de areas
        /// </summary>
        /// <typeparam name="T1">T1</typeparam>
        /// <param name="conditionals">condiciones</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        AreaModel Save(AreaModel model);
    }
}
