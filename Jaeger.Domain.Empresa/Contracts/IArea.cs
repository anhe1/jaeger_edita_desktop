﻿using System;

namespace Jaeger.Domain.Empresa.Contracts {
    /// <summary>
    /// interface para Area
    /// </summary>
    public interface IArea {
        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del area
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de nuevo registro
        /// </summary>
        DateTime FechaNuevo { get; set; }
    }
}
