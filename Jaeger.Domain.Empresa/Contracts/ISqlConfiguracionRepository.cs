﻿using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Domain.Empresa.Contracts {
    /// <summary>
    /// repositorio de configuraciones del sistema
    /// </summary>
    public interface ISqlConfiguracionRepository {
        /// <summary>
        /// obtener la configuracion para la empresa segun la llave
        /// </summary>
        EmpresaConfiguracionModel GetByKey(string key);

        /// <summary>
        /// almacenar configuracion
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        EmpresaConfiguracionModel Save(EmpresaConfiguracionModel item);
    }
}
