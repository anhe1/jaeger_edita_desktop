﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Contracts;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Domain.Empresa.Builder {
    /// <summary>
    /// interface de constructor de configuración
    /// </summary>
    public interface IConfigurationBuilder {
        /// <summary>
        /// construir configuración
        /// </summary>
        /// <param name="parametros">IParameters</param>
        /// <returns>IConfiguration</returns>
        IConfiguration Build(List<IParametroModel> parametros);
    }

    /// <summary>
    /// interface de constructor de configuración
    /// </summary>
    public interface IConfigurationBuild {
        /// <summary>
        /// construir configuración
        /// </summary>
        /// <param name="configuration">IConfiguration</param>
        /// <returns>Parameters</returns>
        List<IParametroModel> Build(IConfiguration configuration);
    }
}
