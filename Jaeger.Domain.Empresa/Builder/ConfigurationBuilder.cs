﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Contracts;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Domain.Empresa.Builder
{
    public abstract class ConfigurationBuilder : IConfigurationBuilder, IConfigurationBuild {
        /// <summary>
        /// convertir parametros a configuracion
        /// </summary>
        /// <param name="parametros">List<IParametroModel></param>
        /// <returns>IConfiguration</returns>
        public abstract IConfiguration Build(List<IParametroModel> parametros);

        /// <summary>
        /// convertir configuracion a parametros
        /// </summary>
        /// <param name="configuration">IConfiguration</param>
        /// <returns>List<IParametroModel></returns>
        public abstract List<IParametroModel> Build(IConfiguration configuration);
    }
}
