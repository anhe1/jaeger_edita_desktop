﻿using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Builder {
    public interface IConfiguracionQueryBuilder {
        IConfiguracionQueryBuild WithGroup(ConfigGroupEnum group);
        IConfiguracionQueryBuild WithKey(ConfigKeyEnum key);
    }

    public interface IConfiguracionQueryBuild {

    }
}
