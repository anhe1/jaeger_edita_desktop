﻿namespace Jaeger.Domain.Empresa.Abstractions {
    public abstract class Clasificacion : Base.Abstractions.BasePropertyChangeImplementation {
        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>
        public int IdClasificacion { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de la sub categoria
        /// </summary>
        public int IdSubClasificacion { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        public int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer clave de la categoria
        /// </summary>
        public string Clase { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        public string Descripcion { get; set; }
    }
}
