﻿namespace Jaeger.Domain.Empresa.Abstractions {
    /// <summary>
    /// clase abstracta para Area
    /// </summary>
    public abstract class Area : Base.Abstractions.BasePropertyChangeImplementation {
        /// <summary>
        /// constructor
        /// </summary>
        public Area() : base() { }

        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer indice de categoria
        /// </summary>
        public int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer cuenta contable
        /// </summary>
        public string CtaContable { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer nombre del responsable del departamento
        /// </summary>
        public string Responsable { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        public System.DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        public string Creo { get; set; }

        public string Descriptor {
            get { return $"{this.Id} - {this.Descripcion}"; }
        }
    }
}
