﻿namespace Jaeger.Domain.Empresa.Abstractions {
    /// <summary>
    /// clase abstracta del objeto de Empleado
    /// </summary>
    public abstract class Empleado : Contracts.IEmpleado {
        /// <summary>
        /// constructor
        /// </summary>
        public Empleado() { }

        /// <summary>
        /// obtener o establecer indice del empleado
        /// </summary>
        public int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer clave
        /// </summary>
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes (RFC)
        /// </summary>
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion (CURP)
        /// </summary>
        public string CURP { get; set; }

        /// <summary>
        /// obtenner o establecer nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// obtener o establecer fecha de nacimiento
        /// </summary>
        public System.DateTime? FecNacimiento { get; set; }

        /// <summary>
        /// obtener o establecer nacionalidad
        /// </summary>
        public string Nacionalidad { get; set; }

        /// <summary>
        /// obtener o establecer lugar de nacimiento
        /// </summary>
        public string LugarNacimiento { get; set; }

        /// <summary>
        /// obtener o establecer numero de seguridad social (NSS)
        /// </summary>
        public string NSS { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        public System.DateTime FechaNuevo { get; set; }
        
        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        public string Creo { get; set; }
        
        /// <summary>
        /// obtener nombre completo
        /// </summary>
        public abstract string FullName { get; }
    }
}
