﻿using System;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Domain.Empresa.Abstractions {
    /// <summary>
    /// clase abstracta para departamento
    /// </summary>
    public abstract class Departamento : Base.Abstractions.BasePropertyChangeImplementation, IDepatamentoModel {
        /// <summary>
        /// constructor
        /// </summary>
        public Departamento() {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establece indice del departamento
        /// </summary>
        public int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo { get; set; }
        
        /// <summary>
        /// obtener o establecer indice de categoria
        /// </summary>
        public int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer indice del area
        /// </summary>
        public int IdArea { get; set; }
        
        /// <summary>
        /// obtener o establecer cuenta contable
        /// </summary>
        public string CtaContable { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer nombre del responsable del departamento
        /// </summary>
        public string Responsable { get; set; }

        /// <summary>
        /// obtener o establecer fecha de nuevo registro
        /// </summary>
        public DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        public string Creo { get; set; }

        /// <summary>
        /// obtener o establecer descrptor del objeto
        /// </summary>
        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdDepartamento.ToString("00"), this.Descripcion); }
        }
    }
}
