﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// configuraciones de la empresa
    /// </summary>
    [SugarTable("_conf")]
    public class EmpresaConfiguracionModel : BasePropertyChangeImplementation, IEmpresaConfiguracionModel {
        private int index;
        private string keyname;
        private string data;

        /// <summary>
        /// obtener o establecer el indice 
        /// </summary>
        [DataNames("_conf_id")]
        [SugarColumn(ColumnName = "_conf_id", ColumnDescription = "ID de Configuracion", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la llave de la configuración
        /// </summary>
        [DataNames("_conf_key")]
        [SugarColumn(ColumnName = "_conf_key", ColumnDescription = "llave de configuracion", Length = 20)]
        public string Key {
            get { return this.keyname; }
            set { this.keyname = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el objeto json de configuracion
        /// </summary>
        [DataNames("_conf_data")]
        [SugarColumn(ColumnName = "_conf_data", ColumnDescription = "objeto json de configuracion", IsNullable = true, ColumnDataType = "TEXT")]
        public string Data {
            get { return this.data; }
            set { this.data = value;
                this.OnPropertyChanged();
            }
        }
    }
}
