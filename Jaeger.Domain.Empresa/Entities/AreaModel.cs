﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// modelo de area
    /// </summary>
    [SugarTable("CTAREA")]
    public class AreaModel : Abstractions.Area, Contracts.IArea {
        /// <summary>
        /// constructor
        /// </summary>
        public AreaModel() : base() { }

        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        [DataNames("CTAREA_ID")]
        [SugarColumn(ColumnName = "CTDPT_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int Id {
            get { return base.Id; }
            set {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la categoria 
        /// </summary>
        [DataNames("CTAREA_CTCAT_ID")]
        [SugarColumn(ColumnName = "CTAREA_CTCAT_ID", ColumnDescription = "indice de la categoria general")]
        public new int IdCategoria {
            get { return base.IdCategoria; }
            set {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta contable
        /// </summary>
        [DataNames("CTAREA_CNTBL_ID")]
        [SugarColumn(ColumnName = "CTAREA_CNTBL_ID", ColumnDescription = "contable", Length = 36)]
        public new string CtaContable {
            get { return base.CtaContable; }
            set {
                base.CtaContable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del departamento
        /// </summary>
        [DataNames("CTAREA_NOM")]
        [SugarColumn(ColumnName = "CTAREA_NOM", ColumnDescription = "nombre o descripcion del departamento", Length = 255)]
        public new string Descripcion {
            get { return base.Descripcion; }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del responsable
        /// </summary>
        [DataNames("CTAREA_RESP")]
        [SugarColumn(ColumnName = "CTAREA_RESP", ColumnDescription = "nombre del responsable", Length = 255)]
        public new string Responsable {
            get { return base.Responsable; }
            set {
                base.Responsable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CTAREA_FN")]
        [SugarColumn(ColumnName = "CTAREA_FN", ColumnDescription = "fecha de creacion del registro")]
        public new DateTime FechaNuevo {
            get { return base.FechaNuevo; }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("CTAREA_USR_N")]
        [SugarColumn(ColumnName = "CTAREA_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10)]
        public new string Creo {
            get { return base.Creo; }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
