﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase modelo para departamento
    /// </summary>
    [SugarTable("CTDPT", "empresa: catalogo de departamentos")]
    public class DepartamentoModel : Abstractions.Departamento, Contracts.IDepatamentoModel {
        public DepartamentoModel() : base() { }

        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        [DataNames("CTDPT_ID")]
        [SugarColumn(ColumnName = "CTDPT_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdDepartamento {
            get { return base.IdDepartamento; }
            set {
                base.IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("CTDPT_A")]
        [SugarColumn(ColumnName = "CTDPT_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public new bool Activo {
            get { return base.Activo; }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la categoria 
        /// </summary>
        [DataNames("CTDPT_CTCAT_ID")]
        [SugarColumn(ColumnName = "CTDPT_CTCAT_ID", ColumnDescription = "indice de la categoria general")]
        public new int IdCategoria {
            get { return base.IdCategoria; }
            set {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del area
        /// </summary>
        [DataNames("CTDPT_CTAREA_ID")]
        [SugarColumn(ColumnName = "CTDPT_CTAREA_ID", ColumnDescription = "indice del catalogo de areas")]
        public new int IdArea {
            get { return base.IdArea; }
            set {
                base.IdArea = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta contable
        /// </summary>
        [DataNames("CTDPT_CNTBL_ID")]
        [SugarColumn(ColumnName = "CTDPT_CNTBL_ID", ColumnDescription = "cuenta contable asociada")]
        public new string CtaContable {
            get { return base.CtaContable; }
            set {
                base.CtaContable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del departamento
        /// </summary>
        [DataNames("CTDPT_NOM")]
        [SugarColumn(ColumnName = "CTDPT_NOM", ColumnDescription = "nombre o descripcion del departamento", Length = 255)]
        public new string Descripcion {
            get { return base.Descripcion; }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del responsable
        /// </summary>
        [DataNames("CTDPT_RESP")]
        [SugarColumn(ColumnName = "CTDPT_RESP", ColumnDescription = "nombre del reponsable")]
        public new string Responsable {
            get { return base.Responsable; }
            set {
                base.Responsable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CTDPT_FN")]
        [SugarColumn(ColumnName = "CTDPT_FN", ColumnDescription = "fecha de creacion del registro")]
        public new DateTime FechaNuevo {
            get { return base.FechaNuevo; }
            set { base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("CTDPT_USR_N")]
        [SugarColumn(ColumnName = "CTDPT_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10)]
        public new string Creo {
            get { return base.Creo; }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
