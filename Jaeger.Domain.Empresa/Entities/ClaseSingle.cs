﻿using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase que contiene un elemento de la clasificacion general
    /// </summary>
    public class ClaseSingle : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private int subIndex;
        private string tipo;
        private string clase;
        private string descripcion;
        private BindingList<ClasificacionGeneralModel> clases;
        #endregion

        public ClaseSingle() {
            this.clases = new BindingList<ClasificacionGeneralModel>() { RaiseListChangedEvents = true };
            this.clases.AddingNew += Clases_AddingNew;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        public int Id {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el sub indice de relacion
        /// </summary>
        public int SubId {
            get { return this.subIndex; }
            set { this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Tipo {
            get { return this.tipo; }
            set { this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Clase {
            get { return this.clase; }
            set { this.clase = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Descripcion {
            get { return this.descripcion; }
            set { this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public BindingList<ClasificacionGeneralModel> Clases {
            get {
                return this.clases;
            }
            set {
                if (this.clases != null)
                    this.clases.AddingNew -= new AddingNewEventHandler(this.Clases_AddingNew);
                this.clases = value;
                if (this.clases != null)
                    this.clases.AddingNew += new AddingNewEventHandler(this.Clases_AddingNew);
            }
        }

        /// <summary>
        /// obtener o establecer ruta completa creada
        /// </summary>
        public string Resume {
            get {
                return string.Format("{0} - {1} - {2}", this.Tipo, this.Clase, this.Descripcion);
            }
        }
        #endregion

        public override string ToString() {
            return string.Format("{0} {1} {2}", this.Tipo, this.Clase, this.Descripcion);
        }

        private void Clases_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new ClasificacionGeneralModel { SubId = this.Id };
        }
    }
}
