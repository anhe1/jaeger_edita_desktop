﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// configuracion
    /// </summary>
    public class CFDIConfiguracion : Base.Abstractions.BasePropertyChangeImplementation {
        #region variables
        private string _UserID;
        private string _Password;
        private bool _Produccion;
        private string _RFC;
        private string _LugarExpedicion;
        private string _RegimenFiscal;
        private string _RazonSocial;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public CFDIConfiguracion() { }

        /// <summary>
        /// obtener o establecer nombre o razon social
        /// </summary>
        public string RazonSocial {
            get { return this._RazonSocial; }
            set {
                this._RazonSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes
        /// </summary>
        public string RFC {
            get { return this._RFC; }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave de Regimen Fiscal
        /// </summary>
        public string RegimenFiscal {
            get { return this._RegimenFiscal; }
            set {
                this._RegimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Lugar de expedicion del comprobante fiscal
        /// </summary>
        public string LugarExpedicion {
            get { return this._LugarExpedicion; }
            set {
                this._LugarExpedicion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre de usuario
        /// </summary>
        public string UserID {
            get { return this._UserID; }
            set {
                this._UserID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer password
        /// </summary>
        public string Password {
            get { return this._Password; }
            set {
                this._Password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        public bool Produccion {
            get { return this._Produccion; }
            set {
                this._Produccion = value;
                this.OnPropertyChanged();
            }
        }

        #region Build
        public CFDIConfiguracion Build(List<IParametroModel> parametros) {
            foreach (var item in parametros) {
                switch (item.Key) {
                    case ConfigKeyEnum.RazonSocial:
                        this._RazonSocial = item.Data;
                        break;
                    case ConfigKeyEnum.RFC:
                        this._RFC = item.Data;
                        break;
                    case ConfigKeyEnum.RegimenFiscal:
                        this._RegimenFiscal = item.Data;
                        break;
                    case ConfigKeyEnum.UserID:
                        this._UserID = item.Data;
                        break;
                    case ConfigKeyEnum.Password:
                        this._Password = item.Data;
                        break;
                    case ConfigKeyEnum.ModoProductivo:
                        if (item.Data.ToLower().Contains("true") || item.Data.StartsWith("1"))
                            this._Produccion = true; //int.Parse(item.Data) > 0;
                        else {
                            this._Produccion = false;
                        }
                        break;
                    case ConfigKeyEnum.CodigoPostal:
                        this._LugarExpedicion = item.Data;
                        break;
                }
            }
            return this;
        }
        #endregion
    }
}
