﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// configuracion
    /// </summary>
    public class EmpresaConfiguracion : Base.Abstractions.BasePropertyChangeImplementation {
        #region variables
        private string _RazonSocial;
        private string _NombreComercial;
        private string _RFC;
        private string _SociedadCapital;
        private string _CURP;
        private string _RegimenFiscal;
        private string _CIF;
        private string _CIFUrl;
        private string _TipoPersona;
        private string _RegistroPatronal;
        private string _Dominio;
        #endregion

        public EmpresaConfiguracion() { }

        /// <summary>
        /// obtener o establecer nombre de la razon social
        /// </summary>
        [DisplayName("Nombre ó Razon Social")]
        public string RazonSocial {
            get { return this._RazonSocial; }
            set { this._RazonSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre comercial
        /// </summary>
        [DisplayName("Nombre Comercial")]
        public string NombreComercial {
            get { return this._NombreComercial; }
            set { this._NombreComercial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Causantes (RFC)
        /// </summary>
        [DisplayName("Registro Federal de Causantes (RFC)")]
        public string RFC {
            get { return this._RFC; }
            set { this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CURP
        /// </summary>
        [DisplayName("CURP")]
        public string CURP {
            get { return this._CURP; }
            set { this._CURP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sociedad capital
        /// </summary>
        [DisplayName("Sociedad Capital")]
        public string SociedadCapital {
            get { return this._SociedadCapital; }
            set { this._SociedadCapital = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        [DisplayName("Clave de Régimen Fiscal")]
        public string RegimenFiscal {
            get { return this._RegimenFiscal; }
            set { this._RegimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de registro patronal
        /// </summary>
        [DisplayName("Registro Patronal")]
        public string RegistroPatronal {
            get { return this._RegistroPatronal; }
            set { this._RegistroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de CIF
        /// </summary>
        [DisplayName("Número de CIF")]
        public string CIF {
            get { return this._CIF; }
            set { this._CIF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string CIFUrl {
            get { return this._CIFUrl; }
            set {
                this._CIFUrl = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string TipoPersona {
            get { return this._TipoPersona; }
            set { this._TipoPersona = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre de dominio
        /// </summary>
        [DisplayName("Dominio")]
        public string Dominio {
            get { return this._Dominio; }
            set { this._Dominio = value;
                this.OnPropertyChanged();
            }
        }

        #region build
        public EmpresaConfiguracion Build(List<IParametroModel> parametros) {
            foreach (var item in parametros) {
                switch (item.Key) {
                    case ConfigKeyEnum.RFC:
                        this._RFC = item.Data;
                        break;
                    case ConfigKeyEnum.CURP:
                        this._CURP = item.Data;
                        break;
                    case ConfigKeyEnum.RazonSocial:
                        this._RazonSocial = item.Data;
                        break;
                    case ConfigKeyEnum.NombreComercial:
                        this._NombreComercial = item.Data;
                        break;
                    case ConfigKeyEnum.SociedadCapital:
                        this._SociedadCapital = item.Data;
                        break;
                    case ConfigKeyEnum.RegimenFiscal:
                        this._RegimenFiscal = item.Data;
                        break;
                    case ConfigKeyEnum.RegistroPatronal:
                        this._RegistroPatronal = item.Data;
                        break;
                    case ConfigKeyEnum.CIF:
                        this._CIF = item.Data;
                        break;
                    case ConfigKeyEnum.CIFUrl:
                        this._CIFUrl = item.Data;
                        break;
                    case ConfigKeyEnum.TipoPersona:
                        this._TipoPersona = item.Data;
                        break;
                    case ConfigKeyEnum.Dominio:
                        this._Dominio = item.Data;
                        break;
                    default:
                        break;
                }
            }
            return this;
        }

        public List<IParametroModel> Build() {
            var d0 = new List<IParametroModel> {
                new ParametroModel(ConfigKeyEnum.RFC, ConfigGroupEnum.Contribuyente, this.RFC),
                new ParametroModel(ConfigKeyEnum.CURP, ConfigGroupEnum.Contribuyente, this.CURP),
                new ParametroModel(ConfigKeyEnum.RazonSocial, ConfigGroupEnum.Contribuyente, this.RazonSocial),
                new ParametroModel(ConfigKeyEnum.NombreComercial, ConfigGroupEnum.Contribuyente, this.NombreComercial),
                new ParametroModel(ConfigKeyEnum.SociedadCapital, ConfigGroupEnum.Contribuyente, this.SociedadCapital),
                new ParametroModel(ConfigKeyEnum.RegimenFiscal, ConfigGroupEnum.Contribuyente, this.RegimenFiscal),
                new ParametroModel(ConfigKeyEnum.RegistroPatronal, ConfigGroupEnum.Contribuyente, this.RegistroPatronal),
                new ParametroModel(ConfigKeyEnum.CIF, ConfigGroupEnum.Contribuyente, this.CIF),
                new ParametroModel(ConfigKeyEnum.CIFUrl, ConfigGroupEnum.Contribuyente, this.CIFUrl),
                new ParametroModel(ConfigKeyEnum.TipoPersona, ConfigGroupEnum.Contribuyente, this.TipoPersona),
                new ParametroModel(ConfigKeyEnum.Dominio, ConfigGroupEnum.Contribuyente, this.Dominio)
            };
            return d0;
        }
        #endregion
    }
}
