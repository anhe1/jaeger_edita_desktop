﻿using System;
using SqlSugar;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Enums;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// modelo para parametro
    /// </summary>
    [SugarTable("CONF")]
    public class ParametroModel : IParametroModel {
        /// <summary>
        /// constructor
        /// </summary>
        public ParametroModel() { }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="key">ConfigKeyEnum</param>
        /// <param name="group">ConfigGroupEnum</param>
        /// <param name="data">data</param>
        public ParametroModel(ConfigKeyEnum key, ConfigGroupEnum group, string data) {
            Key = key;
            Group = group;
            Data = data;
        }

        #region propiedades
        [SugarColumn(ColumnName = "CONF_KEY", ColumnDescription = "", Length = 20)]
        public ConfigKeyEnum Key { get; set; }

        [DataNames("CONF_KEY")]
        [SugarColumn(IsIgnore = true)]
        public int IdKey {
            get { return (int)this.Key; }
            set {
                this.Key = (ConfigKeyEnum)Enum.Parse(typeof(ConfigKeyEnum), value.ToString());
                if (!Enum.IsDefined(typeof(ConfigKeyEnum), this.Key) && !this.Key.ToString().Contains(",")) {
                    throw new InvalidOperationException(
                        $"{value} is not an underlying value of the YourEnum enumeration."
                    );
                }
            }
        }

        /// <summary>
        /// obtener o establecer clave de grupo 
        /// </summary>
        [SugarColumn(ColumnName = "CONF_GRP", ColumnDescription = "")]
        public ConfigGroupEnum Group { get; set; }

        [DataNames("CONF_GRP")]
        [SugarColumn(IsIgnore = true)]
        public int IdGroup {
            get { return (int)this.Group; }
            set {
                this.Group = (ConfigGroupEnum)Enum.Parse(typeof(ConfigGroupEnum), value.ToString());
                if (!Enum.IsDefined(typeof(ConfigGroupEnum), this.Group) && !this.Group.ToString().Contains(",")) {
                    throw new InvalidOperationException(
                        $"{value} is not an underlying value of the YourEnum enumeration."
                    );
                }
            }
        }

        [DataNames("CONF_DATA")]
        [SugarColumn(ColumnName = "CONF_DATA", ColumnDescription = "", Length = 500)]
        public string Data { get; set; }
        #endregion

        #region builder
        public ParametroModel WithKey(ConfigKeyEnum key) {
            this.Key = key;
            return this;
        }

        public ParametroModel WithGroup(ConfigGroupEnum group) {
            this.Group = group;
            return this;
        }

        public ParametroModel With(string data) {
            this.Data = data;
            return this;
        }
        #endregion
    }
}
