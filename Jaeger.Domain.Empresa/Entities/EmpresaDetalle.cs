﻿namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// 
    /// </summary>
    public class EmpresaDetalle {
        /// <summary>
        /// constructor
        /// </summary>
        public EmpresaDetalle() {
            this.Empresa = new EmpresaConfiguracion();
            this.DomicilioFiscal = new DomicilioFiscalConfiguracion();
        }

        /// <summary>
        /// obtener o establecer datos de la empresa
        /// </summary>
        public EmpresaConfiguracion Empresa { get; set; }

        /// <summary>
        /// obtener o establecer domicilio fiscal
        /// </summary>
        public DomicilioFiscalConfiguracion DomicilioFiscal { get; set; }
    }
}
