﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// catalogo de clasificacion general
    /// </summary>
    [SugarTable("_ctlcla", "catalogo de clasificaciones generales")]
    public class ClasificacionGeneralModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private string nombre;

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [SugarColumn(ColumnName = "_ctlcla_id", ColumnDescription = "indice principal de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion en si mismo
        /// </summary>
        [SugarColumn(ColumnName = "_ctlcla_sbid", ColumnDescription = "indice de relacion", IsNullable = false)]
        public int SubId {
            get { return this.subIndex; }
            set { this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre de la clase
        /// </summary>
        [SugarColumn(ColumnName = "_cltcla_cla", ColumnDescription = "descripcion de la clase", Length = 64)]
        public string Clase {
            get { return this.nombre; }
            set { this.nombre = value;
                this.OnPropertyChanged();
            }
        }
    }
}
