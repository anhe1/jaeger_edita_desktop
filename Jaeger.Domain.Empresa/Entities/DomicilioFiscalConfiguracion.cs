﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Entities {
    public class DomicilioFiscalConfiguracion : Base.Abstractions.BasePropertyChangeImplementation, IDomicilioFiscal {
        #region variables
        private string _MunicipioDelegacion;
        private string _Colonia;
        private string _NumExterior;
        private string _NumInterior;
        private string _CodigoPostal;
        private string _NombreVialidad;
        private string _EntidadFederativa;
        private string _Referencia;
        private string _Localidad;
        private string _Correo;
        private string _TipoVialidad;
        private string _Telefono;
        private string _Ciudad;
        #endregion

        public DomicilioFiscalConfiguracion() { }

        #region datos de ubicacion (domicilio fiscal, vigente)
        /// <summary>
        /// obtener o establecer entidad federativa
        /// </summary>
        public string EntidadFederativa {
            get { return this._EntidadFederativa; }
            set {
                this._EntidadFederativa = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer municipio o delegacion
        /// </summary>
        public string MunicipioDelegacion {
            get {
                if (!string.IsNullOrEmpty(this._MunicipioDelegacion))
                    return this._MunicipioDelegacion;
                return string.Empty;
            }
            set { this._MunicipioDelegacion = value; }
        }

        /// <summary>
        /// obtener o establecer colonia
        /// </summary>
        public string Colonia {
            get { return this._Colonia; }
            set {
                this._Colonia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de vialidad
        /// </summary>
        public string TipoVialidad {
            get { return this._TipoVialidad; }
            set {
                this._TipoVialidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre de la vialidad
        /// </summary>
        public string NombreVialidad {
            get { return this._NombreVialidad; }
            set {
                this._NombreVialidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero exterior
        /// </summary>
        public string NumExterior {
            get { return this._NumExterior; }
            set {
                this._NumExterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero interior
        /// </summary>
        public string NumInterior {
            get { return this._NumInterior; }
            set {
                this._NumInterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtenr o establecer codigo postal
        /// </summary>
        public string CodigoPostal {
            get { return this._CodigoPostal; }
            set {
                this._CodigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        public string Referencia {
            get { return this._Referencia; }
            set {
                this._Referencia = value;
                this.OnPropertyChanged();
            }
        }

        public string Localidad {
            get { return this._Localidad; }
            set {
                this._Localidad = value;
                this.OnPropertyChanged();
            }
        }

        public string Ciudad {
            get { return this._Ciudad; }
            set { this._Ciudad = value;
                this.OnPropertyChanged();
            }
        }

        public string Telefono {
            get { return this._Telefono; }
            set { this._Telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer correo electronico
        /// </summary>
        public string Correo {
            get { return this._Correo; }
            set {
                this._Correo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region Build
        public DomicilioFiscalConfiguracion Build(List<IParametroModel> parametros) {
            foreach (var item in parametros) {
                switch (item.Key) {
                    case ConfigKeyEnum.CodigoPostal:
                        this._CodigoPostal = item.Data;
                        break;
                    case ConfigKeyEnum.Colonia:
                        this._Colonia = item.Data;
                        break;
                    case ConfigKeyEnum.NumExterior:
                        this._NumExterior = item.Data;
                        break;
                    case ConfigKeyEnum.NumInterior:
                        this._NumInterior = item.Data;
                        break;
                    case ConfigKeyEnum.MunicipioDelegacion:
                        this._MunicipioDelegacion = item.Data;
                        break;
                    case ConfigKeyEnum.TipoVialidad:
                        this._TipoVialidad = item.Data;
                        break;
                    case ConfigKeyEnum.Calle:
                        this._NombreVialidad = item.Data;
                        break;
                    case ConfigKeyEnum.Estado:
                        this._EntidadFederativa = item.Data;
                        break;
                    case ConfigKeyEnum.Referencia:
                        this._Referencia = item.Data;
                        break;
                    case ConfigKeyEnum.Localidad:
                        this._Localidad = item.Data;
                        break;
                    case ConfigKeyEnum.CorreoElectronico:
                        this._Correo = item.Data;
                        break;
                    case ConfigKeyEnum.Telefono:
                        this._Telefono = item.Data;
                        break;
                    case ConfigKeyEnum.Ciudad:
                        this._Ciudad = item.Data;
                        break;
                    default:
                        break;
                }
            }
            return this;
        }

        public List<IParametroModel> Build() {
            var d0 = new List<IParametroModel> {
                new ParametroModel(ConfigKeyEnum.CodigoPostal, ConfigGroupEnum.DomicilioFiscal, this.CodigoPostal),
                new ParametroModel(ConfigKeyEnum.Colonia, ConfigGroupEnum.DomicilioFiscal, this.Colonia),
                new ParametroModel(ConfigKeyEnum.NumExterior, ConfigGroupEnum.DomicilioFiscal, this.NumExterior),
                new ParametroModel(ConfigKeyEnum.NumInterior, ConfigGroupEnum.DomicilioFiscal, this.NumInterior),
                new ParametroModel(ConfigKeyEnum.MunicipioDelegacion, ConfigGroupEnum.DomicilioFiscal, this.MunicipioDelegacion),
                new ParametroModel(ConfigKeyEnum.TipoVialidad, ConfigGroupEnum.DomicilioFiscal, this.TipoVialidad),
                new ParametroModel(ConfigKeyEnum.Calle, ConfigGroupEnum.DomicilioFiscal, this.NombreVialidad),
                new ParametroModel(ConfigKeyEnum.Estado, ConfigGroupEnum.DomicilioFiscal, this.EntidadFederativa),
                new ParametroModel(ConfigKeyEnum.Referencia, ConfigGroupEnum.DomicilioFiscal, this.Referencia),
                new ParametroModel(ConfigKeyEnum.Localidad, ConfigGroupEnum.DomicilioFiscal, this.Localidad),
                new ParametroModel(ConfigKeyEnum.Ciudad, ConfigGroupEnum.DomicilioFiscal, this.Ciudad),
                new ParametroModel(ConfigKeyEnum.Telefono, ConfigGroupEnum.DomicilioFiscal, this.Telefono),
                new ParametroModel(ConfigKeyEnum.CorreoElectronico, ConfigGroupEnum.DomicilioFiscal, this.Correo),
            };
            return d0;
        }
        #endregion
    }
}
