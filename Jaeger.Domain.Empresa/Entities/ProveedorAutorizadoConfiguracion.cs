﻿using System.Collections.Generic;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Enums;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// Credenciales para el Proveedor Autorizado de Certificacion (PAC)
    /// </summary>
    public class ProveedorAutorizadoConfiguracion : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private string _UserID;
        private string _Password;
        private bool _Produccion;
        private string _RFC;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ProveedorAutorizadoConfiguracion() { }

        #region propiedades
        /// <summary>
        /// obtener o establecer nombre de usuario
        /// </summary>
        public string UserID {
            get { return this._UserID; }
            set {
                this._UserID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer password del servicio
        /// </summary>
        public string Password {
            get { return this._Password; }
            set {
                this._Password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        public string RFC {
            get { return this._RFC; }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer modo productivo del servicio
        /// </summary>
        public bool Produccion {
            get { return this._Produccion; }
            set {
                this._Produccion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region build
        /// <summary>
        /// Builder de parametros
        /// </summary>
        /// <param name="parametros"></param>
        public ProveedorAutorizadoConfiguracion Build(List<IParametroModel> parametros) {
            foreach (var item in parametros) {
                switch (item.Key) {
                    case ConfigKeyEnum.UserID:
                        this._UserID = item.Data;
                        break;
                    case ConfigKeyEnum.Password:
                        this._Password = item.Data;
                        break;
                    case ConfigKeyEnum.ModoProductivo:
                        if (item.Data.ToLower().Contains("true") || item.Data.StartsWith("1")) {
                            this._Produccion = true; //int.Parse(item.Data) > 0;
                        } else {
                            this._Produccion = false;
                        }
                        break;
                    case ConfigKeyEnum.RFC:
                        this._RFC = item.Data;
                        break;
                }
            }
            return this;
        }

        /// <summary>
        /// Builder de parametros
        /// </summary>
        public List<IParametroModel> Build() {
            var d0 = new List<IParametroModel>() {
                new ParametroModel(ConfigKeyEnum.UserID, ConfigGroupEnum.ProveedorAutorizado, this.UserID),
                new ParametroModel(ConfigKeyEnum.Password, ConfigGroupEnum.ProveedorAutorizado, this.Password),
                new ParametroModel(ConfigKeyEnum.ModoProductivo, ConfigGroupEnum.ProveedorAutorizado, this.Produccion.ToString()),
            };
            return d0;
        }
        #endregion
    }
}
