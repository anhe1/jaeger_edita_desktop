﻿using System;
using SqlSugar;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// clase modelo para categorias o clasificacio de bienes productos y servicios general (BPS) 
    /// </summary>
    [SugarTable("CTCAT", "categorias de bienes, productos y servicios (BPS) del almacen de materia prima")]
    public class ClasificacionModel : Abstractions.Clasificacion, IClasificacionModel {

        public ClasificacionModel() : base() { }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("CTCAT_ID", "IdCategoria")]
        [SugarColumn(ColumnName = "CTCAT_ID", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int IdCategoria {
            get {
                return base.IdCategoria;
            }
            set {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si la categoria se encuentra activa
        /// </summary>
        [DataNames("CTCAT_A", "Activo")]
        [SugarColumn(ColumnName = "CTCAT_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion de la subcategoria
        /// </summary>
        [DataNames("CTCAT_CTCAT_ID", "SubId")]
        [SugarColumn(ColumnName = "CTCAT_CTCAT_ID", ColumnDescription = "indice de relacion de la subcategoria", DefaultValue = "0")]
        public int IdSubCategoria {
            get {
                return base.IdSubCategoria;
            }
            set {
                base.IdSubCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la clasificacion 
        /// </summary>
        [DataNames("CTCAT_CLV", "Clave")]
        [SugarColumn(ColumnName = "CTCAT_CLV", ColumnDescription = "indice de relacion de la subcategoria", DefaultValue = "0")]
        [Obsolete("No se esta utilizando para esta version")]
        public new string Clave {
            get { return base.Clave; }
            set {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre de la categoria 
        /// </summary>
        [DataNames("CTCAT_NOM", "Descripcion")]
        [SugarColumn(ColumnName = "CTCAT_NOM", ColumnDescription = "nombre o descripcion de la categoria", Length = 128)]
        public new string Descripcion {
            get {
                return base.Descripcion;
            }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("CTCAT_USR_N")]
        [SugarColumn(ColumnName = "CTCAT_USR_N", ColumnDescription = "clave del usuario que crea el registro", Length = 10)]
        public string Creo {
            get {
                return base.Creo;
            }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("CTCAT_FN")]
        [SugarColumn(ColumnName = "CTCAT_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get {
                return base.FechaNuevo;
            }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// dato ignorado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        [Obsolete("No se utiliza en esta versión")]
        public int IdAlmacen {
            get { return base.IdAlmacen; }
            set { base.IdAlmacen = value; }
        }
    }
}
