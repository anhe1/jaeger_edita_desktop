﻿using System.ComponentModel;
using SqlSugar;

namespace Jaeger.Domain.Empresa.Entities {
    /// <summary>
    /// modelo de area
    /// </summary>
    [SugarTable("CTAREA")]
    public class AreaDetailModel : AreaModel, Contracts.IArea {
        private BindingList<Contracts.IDepatamentoModel> _Depatamentos;

        /// <summary>
        /// constructor
        /// </summary>
        public AreaDetailModel() : base() { }

        /// <summary>
        /// obtener o establecer lista de departamentos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<Contracts.IDepatamentoModel> Depatamentos {
            get { return this._Depatamentos; }
            set {
                this._Depatamentos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
