﻿using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.Contracts;
using Jaeger.DataAccess.Services;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.Abstractions {
    public abstract class RepositoryMaster<T> : Repository, IRepositoryMaster where T : class, new() {
        /// <summary>
        /// lista de parametros al pasar comando sql en modo texto
        /// </summary>
        protected List<FbParameter> parameters;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public RepositoryMaster(IDataBaseConfiguracion configuracion) : base(configuracion) {
            this.User = "SYSDBA";
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public RepositoryMaster(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public string User {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected int ExecuteNoQuery(FbCommand sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;
                return sqlCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// ejecutar sentencia SQL en modo texto, es necesario incluir los parametros en propiedad "parametros"
        /// </summary>
        /// <param name="sqlcommand"></param>
        /// <returns></returns>
        protected int ExecuteScalar(string sqlcommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new FbCommand()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlcommand;
                    comando.Parameters.AddRange(this.parameters);
                    int indice = 0;
                    try {
                        indice = Convert.ToInt32(comando.ExecuteScalar());
                    } catch (Exception ex) {
                        FbException fbex = ex as FbException;
                        if (fbex != null) {
                            LogErrorService.LogWrite("Firebird: " + fbex.Message);
                            Console.WriteLine("Firebird: " + fbex.Message);
                        } else {
                            Console.WriteLine("Exception: " + ex.Message);
                            LogErrorService.LogWrite("Exception: " + ex.Message);
                        }
                        indice = 0;
                    }
                    return indice;
                }
            }
        }

        /// <summary>
        /// ejecutar sentencia SQL en modo texto, es necesario incluir los parametros en propiedad "parametros"
        /// </summary>
        /// <param name="sqlcommand"></param>
        /// <returns></returns>
        protected int ExecuteScalar(FbCommand command) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                int indice = 0;
                try {
                    indice = Convert.ToInt32(command.ExecuteScalar());
                } catch (Exception ex) {
                    var fbex = ex as FbException;
                    if (fbex != null) {
                        Console.WriteLine("Firebird: " + fbex.Message);
                        LogErrorService.LogWrite("Exception: " + fbex.Message);
                    } else
                        Console.WriteLine("Exception: " + ex.Message);
                    LogErrorService.LogWrite("Exception: " + ex.Message);
                    indice = -1;
                }
                return indice;
            }
        }

        /// <summary>
        /// ejecutar comando FbCommand, este debe incluir los parametros
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader(FbCommand sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                sqlCommand.Connection = connection;

                var reader = sqlCommand.ExecuteReader();
                using (var table = new DataTable()) {
                    table.Load(reader);
                    reader.Dispose();
                    return table;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected DataTable ExecuteReader(string sqlCommand) {
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var comando = new FbCommand()) {
                    comando.Connection = connection;
                    comando.CommandType = CommandType.Text;
                    comando.CommandText = sqlCommand;
                    if (this.parameters != null)
                        comando.Parameters.AddRange(this.parameters);
                    return this.ExecuteReader(comando);
                }
            }
        }

        /// <summary>
        /// ejecutar transaccion 
        /// </summary>
        /// <param name="sqlCommand">objeto FbCommand</param>
        /// <returns></returns>
        protected int ExecuteTransaction(FbCommand sqlCommand) {
            int response = 0;
            using (var connection = this.GetConnection()) {
                connection.Open();
                using (var transaction = connection.BeginTransaction()) {
                    sqlCommand.Connection = connection;
                    sqlCommand.Transaction = transaction;
                    response = sqlCommand.ExecuteNonQuery();
                    try {
                        transaction.Commit();
                    } catch (Exception ex) {
                        FbException fbex = ex as FbException;
                        if (fbex != null)
                            LogErrorService.LogWrite(fbex.StackTrace);
                        Console.WriteLine("firebird: " + fbex.Message);
                        LogErrorService.LogWrite("Exception: " + ex.Message);
                        response = 0;
                    }
                }
            }
            return response;
        }

        /// <summary>
        /// obtener el numero maximo de una tabla (solo para CP)
        /// </summary>
        /// <param name="fieldName">nombre del campo</param>
        /// <returns>maximo</returns>
        protected int Max(string fieldName) {
            var tableName = fieldName.Substring(0, fieldName.IndexOf('_'));
            try {
                var sqlCommand = new FbCommand {
                    CommandText = @"select max(@fieldName) + 1 from @tableName;"
                };
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@fieldName", fieldName);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@tableName", tableName);
                int maximo = this.ExecuteScalar(sqlCommand);
                return maximo;
            } catch (Exception ex) {
                FbException fbex = ex as FbException;
                if (fbex != null)
                    Console.WriteLine("firebird: " + fbex.Message);
                LogErrorService.LogWrite("Exception: " + ex.Message);
                return 0;
            }
        }

        public string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }

        protected FbCommand GetConditional(FbCommand sqlCommand, List<Conditional> conditionals) {
            if (conditionals.Count > 0) {
                var _parameters = ExpressionTool.ConditionalModelToSql(conditionals);
                if (!sqlCommand.CommandText.ToLower().Contains("where")) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "where" + _parameters.Key);
                } else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "and" + _parameters.Key);
                }
                sqlCommand.Parameters.AddRange(_parameters.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }
            return sqlCommand;
        }

        protected FbCommand GetConditional(FbCommand sqlCommand, List<IConditional> conditionals) {
            if (conditionals.Count > 0) {
                var _parameters = ExpressionTool.ConditionalModelToSql(conditionals);
                if (sqlCommand.CommandText.ToLower().Contains("@wcondiciones")) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "where" + _parameters.Key);
                } else if (!sqlCommand.CommandText.ToLower().Contains("where")) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "where" + _parameters.Key);
                } else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "and" + _parameters.Key);
                }
                sqlCommand.Parameters.AddRange(_parameters.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "");
            }
            return sqlCommand;
        }

        protected IEnumerable<T> GetMapper(FbCommand sqlcommand) {
            var tabla = this.ExecuteReader(sqlcommand);
            var mapper = new DataNamesMapper<T>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(DataTable sqlcommand) where T1 : class, new() {
            var tabla = sqlcommand;
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(string sqlcommand) where T1 : class, new() {
            var tabla = this.ExecuteReader(sqlcommand);
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(FbCommand sqlcommand) where T1 : class, new() {
            var tabla = this.ExecuteReader(sqlcommand);
            var mapper = new DataNamesMapper<T1>();
            return mapper.Map(tabla).ToList();
        }

        protected IEnumerable<T1> GetMapper<T1>(FbCommand sqlCommand, List<IConditional> conditionals) where T1 : class, new() {
            return this.GetMapper<T1>(this.GetConditional(sqlCommand, conditionals));
        }

        protected void CreateCommandFB(string tablaName) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0}", tablaName),
                Connection = this.GetConnection()
            };

            FbDataAdapter adapter = new FbDataAdapter(sqlCommand);
            FbCommandBuilder builder = new FbCommandBuilder(adapter);
            DataSet ds = new DataSet();
            adapter.Fill(ds, tablaName);
            var _insertCommand = builder.GetInsertCommand();
            var _updateCommand = builder.GetUpdateCommand();
            var _deleteCommand = builder.GetDeleteCommand();

            var _insert = _insertCommand.CommandText;
            var _update = _updateCommand.CommandText;
            var _delete = _deleteCommand.CommandText;

            foreach (FbParameter item in _insertCommand.Parameters) {
                Console.WriteLine(String.Format("sqlCommand.Parameters.AddWithValue(\"@{0}\", {1});", item.SourceColumn, item.ParameterName));
                _insert = SafeReplace(_insert, item.ParameterName.Replace("@", ""), item.SourceColumn);
                _update = SafeReplace(_update, item.ParameterName.Replace("@", ""), item.SourceColumn);
            }

            Console.WriteLine(String.Format("var sqlCommand = new FbCommand {{ CommandText = @\"{0}\" }};", _insert.Replace("\"", "")));
            Console.WriteLine(String.Format("var sqlCommand = new FbCommand {{ CommandText = @\"{0}\" }};", _update.Replace("\"", "")));
            Console.WriteLine(String.Format("var sqlCommand = new FbCommand {{ CommandText = @\"{0}\" }};", _delete.Replace("\"", "")));
        }

        private string SafeReplace(string input, string find, string replace) {
            string textToFind = string.Format(@"\b{0}\b", find);
            return Regex.Replace(input, textToFind, replace);
        }

        public string CreateClass(string sql, string className) {
            return FireBird.Abstractions.PocoClassGenerator.GenerateClass(GetConnection(), sql, className);
        }
    }
}
