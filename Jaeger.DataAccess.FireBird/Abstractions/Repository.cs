﻿using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Contracts;

namespace Jaeger.DataAccess.Abstractions {
    /// <summary>
    /// clase abstracta para del repositorio
    /// </summary>
    public abstract class Repository {
        private readonly string connectionString;
        private IDataBaseConfiguracion configuracion;

        public string Message { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public Repository(IDataBaseConfiguracion configuracion) {
            this.configuracion = configuracion;
            this.connectionString = this.StringBuilder(this.configuracion).ConnectionString;
        }

        /// <summary>
        /// obtener conexion valida a la base de datos
        /// </summary>
        /// <returns>retorna un objeto FbConnection</returns>
        protected FbConnection GetConnection() {
            return new FbConnection(this.connectionString);
        }

        /// <summary>
        /// generador de cadena de conexion de la base de datos
        /// </summary>
        /// <param name="configuracion">objeto de configuracion de la base de datos</param>
        /// <returns>retorna un objeto FbConnectionStringBuilder</returns>
        private FbConnectionStringBuilder StringBuilder(IDataBaseConfiguracion configuracion) {
            var builder = new FbConnectionStringBuilder();
            builder.UserID = configuracion.UserID;
            builder.Password = configuracion.Password;
            builder.DataSource = configuracion.HostName;
            builder.Database = configuracion.Database;
            builder.Port = configuracion.PortNumber;
            builder.Charset = configuracion.Charset;
            builder.Pooling = false;
            return builder;
        }
    }
}
