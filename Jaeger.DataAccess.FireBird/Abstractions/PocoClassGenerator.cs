﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Jaeger.DataAccess.Base.Services.ClassGenerator;

namespace Jaeger.DataAccess.FireBird.Abstractions {
    public static class PocoClassGenerator {
        private static readonly Dictionary<Type, string> TypeAliases = new Dictionary<Type, string> {
               { typeof(int), "int" },
               { typeof(short), "short" },
               { typeof(byte), "byte" },
               { typeof(byte[]), "byte[]" },
               { typeof(long), "long" },
               { typeof(double), "double" },
               { typeof(decimal), "decimal" },
               { typeof(float), "float" },
               { typeof(bool), "bool" },
               { typeof(string), "string" }
       };

        private static readonly HashSet<Type> NullableTypes = new HashSet<Type> {
               typeof(int),
               typeof(short),
               typeof(long),
               typeof(double),
               typeof(decimal),
               typeof(float),
               typeof(bool),
               typeof(DateTime)
       };

        public static string GenerateClass(this IDbConnection connection, string sql, string className = null, GeneratorBehavior generatorBehavior = GeneratorBehavior.Default) {
            if (connection.State != ConnectionState.Open)
                connection.Open();

            var tableName = string.Empty;
            var fields = new List<FieldModel>();
            //Get Table Name
            //Fix : [When View using CommandBehavior.KeyInfo will get duplicate columns Issue #8  shps951023/PocoClassGenerator](https://github.com/shps951023/PocoClassGenerator/issues/8 )
            var isFromMutiTables = false;
            using (var command = connection.CreateCommand(sql))
            using (var reader = command.ExecuteReader(CommandBehavior.KeyInfo | CommandBehavior.SingleRow)) {
                var tables = reader.GetSchemaTable().Select().Select(s => s["BaseTableName"] as string).Distinct();
                tableName = string.IsNullOrWhiteSpace(className) ? tables.First() ?? "Info" : className;

                isFromMutiTables = tables.Count() > 1;
            }

            //Get Columns 
            var behavior = isFromMutiTables ? (CommandBehavior.SchemaOnly | CommandBehavior.SingleRow) : (CommandBehavior.KeyInfo | CommandBehavior.SingleRow);

            using (var command = connection.CreateCommand(sql))
            using (var reader = command.ExecuteReader(behavior)) {
                do {
                    var schema = reader.GetSchemaTable();
                    foreach (DataRow row in schema.Rows) {
                        var field = new FieldModel {
                            Type = (Type)row["DataType"]
                        };
                        field.FieldName = TypeAliases.ContainsKey(field.Type) ? TypeAliases[field.Type] : field.Type.FullName;
                        field.IsNullable = (bool)row["AllowDBNull"] && NullableTypes.Contains(field.Type);
                        field.ColumnName = (string)row["ColumnName"];

                        fields.Add(field);
                    }
                } while (reader.NextResult());
            }

            var builder = new StringBuilder();
            builder.AppendLine("using Jaeger.Domain.Services.Mapping;\r\n");
            builder.AppendLine("namespace Jaeger.Domain.Cotizador.Entities {");
            builder.AppendFormat("	public class {0}{1} : Base.Abstractions.BasePropertyChangeImplementation", tableName.Replace(" ", ""), Environment.NewLine);
            builder.AppendLine("	{");
            builder.AppendLine("#region");
            // creamos todas las variables locales en la clase
            foreach (var item in fields) {
                builder.AppendLine(string.Format("		private {0}{1} _{2};", item.FieldName, item.IsNullable ? "?" : string.Empty, item.ColumnName.Replace("$","")));
            }
            builder.AppendLine("#endregion\r\n");

            foreach (var item in fields) {
                builder.AppendLine($"		/// <summary>\r\n/// ({item.ColumnName})\r\n /// </summary>");
                builder.AppendLine($"		[DataNames(\"{item.ColumnName}\")]");
                builder.AppendLine(string.Format("		public {0}{1} {2} {{ \r\nget {{return _{3};}} \r\nset{{ _{3} = value;\r\nthis.OnPropertyChanged(); }} }}\r\n", item.FieldName, item.IsNullable ? "?" : string.Empty, item.ColumnName.Replace("$", ""), item.ColumnName.Replace("$", "")));
            }
            builder.AppendLine("	}");
            builder.AppendLine("}");
            return builder.ToString();
        }

        public static string GenerateRepository(this IDbConnection connection, string sql, string className = null, GeneratorBehavior generatorBehavior = GeneratorBehavior.Default) {
            if (connection.State != ConnectionState.Open)
                connection.Open();

            var tableName = string.Empty;
            var fields = new List<FieldModel>();
            var isFromMutiTables = false;
            using (var command = connection.CreateCommand(sql))
            using (var reader = command.ExecuteReader(CommandBehavior.KeyInfo | CommandBehavior.SingleRow)) {
                var tables = reader.GetSchemaTable().Select().Select(s => s["BaseTableName"] as string).Distinct();
                tableName = string.IsNullOrWhiteSpace(className) ? tables.First() ?? "Info" : className;

                isFromMutiTables = tables.Count() > 1;
            }

            //Get Columns 
            var behavior = isFromMutiTables ? (CommandBehavior.SchemaOnly | CommandBehavior.SingleRow) : (CommandBehavior.KeyInfo | CommandBehavior.SingleRow);

            using (var command = connection.CreateCommand(sql))
            using (var reader = command.ExecuteReader(behavior)) {
                do {
                    var schema = reader.GetSchemaTable();
                    foreach (DataRow row in schema.Rows) {
                        var field = new FieldModel {
                            Type = (Type)row["DataType"]
                        };
                        field.FieldName = TypeAliases.ContainsKey(field.Type) ? TypeAliases[field.Type] : field.Type.FullName;
                        field.IsNullable = (bool)row["AllowDBNull"] && NullableTypes.Contains(field.Type);
                        field.ColumnName = (string)row["ColumnName"];

                        fields.Add(field);
                    }
                } while (reader.NextResult());
            }

            var builder = new StringBuilder();
            builder.AppendLine("using Jaeger.Domain.Services.Mapping;\r\n");
            builder.AppendLine("namespace Jaeger.Domain.Cotizador.Entities {");
            builder.AppendFormat("	public class {0}{1} : Base.Abstractions.BasePropertyChangeImplementation", tableName.Replace(" ", ""), Environment.NewLine);
            builder.AppendLine("	{");
            builder.AppendLine("#region");
            // creamos todas las variables locales en la clase
            foreach (var item in fields) {
                builder.AppendLine(string.Format("		private {0}{1} _{2};", item.FieldName, item.IsNullable ? "?" : string.Empty, item.ColumnName.Replace("$", "")));
            }
            builder.AppendLine("#endregion\r\n");

            foreach (var item in fields) {
                builder.AppendLine($"		/// <summary>\r\n/// ({item.ColumnName})\r\n /// </summary>");
                builder.AppendLine($"		[DataNames(\"{item.ColumnName}\")]");
                builder.AppendLine(string.Format("		public {0}{1} {2} {{ \r\nget {{return _{3};}} \r\nset{{ _{3} = value;\r\nthis.OnPropertyChanged(); }} }}\r\n", item.FieldName, item.IsNullable ? "?" : string.Empty, item.ColumnName.Replace("$", ""), item.ColumnName.Replace("$", "")));
            }
            builder.AppendLine("	}");
            builder.AppendLine("}");
            return builder.ToString();
        }

        private static IDbCommand CreateCommand(this IDbConnection connection, string sql) {
            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            return cmd;
        }

        partial class FieldModel {
            public string FieldName { get; set; }
            public string ColumnName { get; set; }
            public bool IsNullable { get; set; }
            public Type Type { get; set; }
        }
    }
}
