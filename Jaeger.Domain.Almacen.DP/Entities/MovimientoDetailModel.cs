﻿using SqlSugar;

namespace Jaeger.Domain.Almacen.DP.Entities {
    [SugarTable("_dpmva", "ALMPT: movimientos")]
    public class MovimientoDetailModel : MovimientoModel {
        public MovimientoDetailModel() : base() {

        }
    }
}
