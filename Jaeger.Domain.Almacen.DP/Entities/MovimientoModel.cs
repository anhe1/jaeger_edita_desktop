﻿using System;
using System.Text.RegularExpressions;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.DP.Contracts;

namespace Jaeger.Domain.Almacen.DP.Entities {
    [SugarTable("MVADP", "ALMPT: movimientos")]
    public class MovimientoModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _IdMovimiento;
        private bool _Activo;
        private int _IdComprobante;
        private int _IdAlmacen;
        private int _IdProducto;
        private int _IdModelo;
        private int _IdRemision;
        private int _IdDepartamento;
        private int _IdUnidad;
        private decimal _CantidadEntrante;
        private decimal _CantidadSalida;
        private decimal _CostoUnitario;
        private decimal _SubTotal;
        private string _Creo;
        private DateTime _FechaNuevo;
        private string _Modifica;
        private DateTime? _FechaModifica;
        private decimal _TasaIVA;
        private decimal _TrasladoIVA;
        private decimal _UnidadFactor;
        private string _Unidad;
        private string _Producto;
        private decimal _ValorUnitario;
        private decimal _CostoUnidad;
        private decimal _Importe;
        private string _Identificador;
        private int _idPedido;
        private int _IdCliente;
        private decimal _Unitario;
        private decimal _Descuento;
        private decimal _Total;
        private int _IdTipo;
        private int _IdEfecto;
        private string _Cliente;
        private string _Componente;
        #endregion

        public MovimientoModel() {
            this._FechaNuevo = DateTime.Now;
            this._Activo = true;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (MVADP_ID)
        /// </summary>
        [DataNames("MVADP_ID", "MVADP_ID")]
        [SugarColumn(ColumnName = "MVADP_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdMovimiento {
            get { return _IdMovimiento; }
            set {
                _IdMovimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (MVADP_A)
        /// </summary>
        [DataNames("MVADP_A", "MVADP_A")]
        [SugarColumn(ColumnName = "MVADP_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento (26= Remision
        /// </summary>
        [DataNames("MVADP_doc_id", "MVADP_DOC_ID")]
        [SugarColumn(ColumnName = "MVADP_doc_id", ColumnDescription = "tipo de documento")]
        public int IdTipo {
            get { return this._IdTipo; }
            set {
                this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MVADP_ctefc_id", "MVADP_CTEFC_ID")]
        [SugarColumn(ColumnName = "MVADP_ctefc_id", ColumnDescription = "Efecto del comprobante")]
        public int IdEfecto {
            get { return this._IdEfecto; }
            set {
                this._IdEfecto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de Almacen de Producto Terminado (ALMPT)
        /// </summary>
        [DataNames("MVADP_almpt_id", "MVADP_ALMPT_ID")]
        [SugarColumn(ColumnName = "MVADP_almpt_id", ColumnDescription = "indice de relacion con la tabla de Almacen de Producto Terminado (ALMPT)")]
        public int IdComprobante {
            get { return this._IdComprobante; }
            set {
                this._IdComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de remisiones
        /// </summary>
        [DataNames("MVADP_rmsn_id", "MVADP_RMSN_ID")]
        [SugarColumn(ColumnName = "MVADP_rmsn_id", ColumnDescription = "indice de relacion con la tabla de remisiones")]
        public int IdRemision {
            get { return this._IdRemision; }
            set {
                this._IdRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("MVADP_drctr_id", "MVADP_DRCTR_ID")]
        [SugarColumn(ColumnName = "MVADP_drctr_id", ColumnDescription = "indice del directorio")]
        public int IdCliente {
            get { return this._IdCliente; }
            set {
                this._IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de departamentos
        /// </summary>
        [DataNames("MVADP_CTDPT_ID", "MVADP_CTDPT_ID")]
        [SugarColumn(ColumnName = "MVADP_ctdpt_id", ColumnDescription = "indice del catalogo de departamentos")]
        public int IdDepartamento {
            get { return this._IdDepartamento; }
            set {
                this._IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento (MVADP_CTALM_ID)
        /// </summary>
        [DataNames("MVADP_CTALM_ID", "MVADP_CTALM_ID")]
        [SugarColumn(ColumnName = "MVADP_ctalm_id", ColumnDescription = "indice del departamento")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("MVADP_CTPRD_ID", "MVADP_CTPRD_ID")]
        [SugarColumn(ColumnName = "MVADP_ctprd_id", ColumnDescription = "indice del catalogo de productos")]
        public int IdProducto {
            get { return this._IdProducto; }
            set {
                this._IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de modelos
        /// </summary>
        [DataNames("MVADP_CTMDL_ID", "MVADP_CTMDL_ID")]
        [SugarColumn(ColumnName = "MVADP_ctmdl_id", ColumnDescription = "indice del catalogo de modelos")]
        public int IdModelo {
            get { return this._IdModelo; }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de unidad
        /// </summary>
        [DataNames("MVADP_CTUND_ID", "MVADP_CTUND_ID")]
        [SugarColumn(ColumnName = "MVADP_ctund_id", ColumnDescription = "el indice del catalogo de unidad")]
        public int IdUnidad {
            get { return this._IdUnidad; }
            set {
                this._IdUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor de la unidad
        /// </summary>
        [DataNames("MVADP_undf", "MVADP_UNDF")]
        [SugarColumn(ColumnName = "MVADP_undf", ColumnDescription = "factor de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal UnidadFactor {
            get { return this._UnidadFactor; }
            set {
                this._UnidadFactor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre de la unidad
        /// </summary>
        [DataNames("MVADP_undn", "MVADP_UNDN")]
        [SugarColumn(ColumnName = "MVADP_undn", ColumnDescription = "nombre de la unidad", Length = 50)]
        public string Unidad {
            get { return this._Unidad; }
            set {
                this._Unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad entrante
        /// </summary>
        [DataNames("MVADP_cante", "MVADP_CANTE")]
        [SugarColumn(ColumnName = "MVADP_cante", ColumnDescription = "cantidad entrante", Length = 14, DecimalDigits = 4)]
        public decimal CantidadE {
            get { return this._CantidadEntrante; }
            set {
                this._CantidadEntrante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad saliente
        /// </summary>
        [DataNames("MVADP_cants", "MVADP_CANTS")]
        [SugarColumn(ColumnName = "MVADP_cants", ColumnDescription = "cantidad saliente", Length = 14, DecimalDigits = 4)]
        public decimal CantidadS {
            get { return this._CantidadSalida; }
            set {
                this._CantidadSalida = value;
                this.Unitario = this.Unitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de pedido del cliente hasta 21 caracteres
        /// </summary>
        [DataNames("MVADP_pdcln_id", "MVADP_PDCLN_ID")]
        [SugarColumn(ColumnName = "MVADP_pdcln_id", ColumnDescription = "numero de pedido del cliente hasta 21 caracteres")]
        public int IdPedido {
            get { return this._idPedido; }
            set {
                this._idPedido = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MVADP_NOMR")]
        public string Cliente {
            get { return this._Cliente; }
            set {
                this._Cliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del producto
        /// </summary>
        [DataNames("MVADP_prdn", "MVADP_PRDN")]
        [SugarColumn(ColumnName = "MVADP_prdn", ColumnDescription = "nombre o descripcion del producto", Length = 128)]
        public string Producto {
            get { return this._Producto; }
            set {
                this._Producto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MVADP_COM")]
        public string Componente {
            get { return this._Componente; }
            set { this._Componente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo unitario (MVADP_UNTC)
        /// </summary>
        [DataNames("MVADP_untc", "MVADP_UNTC")]
        [SugarColumn(ColumnName = "MVADP_untc", ColumnDescription = "costo unitario", Length = 14, DecimalDigits = 4)]
        public decimal CostoUnitario {
            get { return this._CostoUnitario; }
            set {
                this._CostoUnitario = value;
                this._CostoUnidad = value * this._UnidadFactor;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo de la unidad (MVADP_UNDC)
        /// </summary>
        [DataNames("MVADP_undc", "MVADP_UNDC")]
        [SugarColumn(ColumnName = "MVADP_undc", ColumnDescription = "costo de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal CostoUnidad {
            get { return this._CostoUnidad; }
            set {
                this._CostoUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario por pieza (MVADP_UNTR)
        /// </summary>
        [DataNames("MVADP_untr", "MVADP_UNTR")]
        [SugarColumn(ColumnName = "MVADP_untr", ColumnDescription = "unitario por pieza", Length = 14, DecimalDigits = 4)]
        public decimal ValorUnitario {
            get { return this._ValorUnitario; }
            set {
                this._ValorUnitario = value;
                this.Unitario = this._ValorUnitario * this.UnidadFactor;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario de la unidad (MVADP_UNTR2)
        /// </summary>
        [DataNames("MVADP_untr2", "MVADP_UNTR2")]
        [SugarColumn(ColumnName = "MVADP_untr2", ColumnDescription = "unitario de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal Unitario {
            get { return this._Unitario; }
            set {
                this._Unitario = value;
                this.SubTotal = this.CantidadS * this.Unitario;
                this.Importe = this.SubTotal - this.Descuento;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub total de la partida
        /// </summary>
        [DataNames("MVADP_sbttl", "MVADP_SBTTL")]
        [SugarColumn(ColumnName = "MVADP_sbttl", ColumnDescription = "sub total de la partida", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set {
                this._SubTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estsablecer el importe del descuento aplicable al producto o sevicio 
        /// </summary>
        [DataNames("MVADP_desc", "MVADP_DESC")]
        [SugarColumn(ColumnName = "MVADP_desc", ColumnDescription = "importe del descuento aplicable al producto o sevicio ", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get { return this._Descuento; }
            set {
                this._Descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del importe de la partida (SubTotal - Descuento)
        /// </summary>
        [DataNames("MVADP_imprt", "MVADP_IMPRT")]
        [SugarColumn(ColumnName = "MVADP_imprt", ColumnDescription = "valor del importe de la partida (SubTotal - Descuento)", Length = 14, DecimalDigits = 4)]
        public decimal Importe {
            get { return this._Importe; }
            set {
                this._Importe = value;
                this.TrasladoIVA = this._Importe * this.TasaIVA;
                this.Total = this._Importe + this.TrasladoIVA;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tasa del IVA
        /// </summary>
        [DataNames("MVADP_tsiva", "MVADP_TSIVA")]
        [SugarColumn(ColumnName = "MVADP_tsiva", ColumnDescription = "tasa del IVA", Length = 14, DecimalDigits = 4)]
        public decimal TasaIVA {
            get { return this._TasaIVA; }
            set {
                this._TasaIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe trasladado del impuesto IVA
        /// </summary>
        [DataNames("MVADP_triva", "MVADP_TRIVA")]
        [SugarColumn(ColumnName = "MVADP_triva", ColumnDescription = "importe trasladado del impuesto IVA", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get { return this._TrasladoIVA; }
            set {
                this._TrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de la partida (Importe + TrasladoIVA)
        /// </summary>
        [DataNames("MVADP_total", "MVADP_TOTAL")]
        [SugarColumn(ColumnName = "MVADP_total", ColumnDescription = "total de la partida (Importe + TrasladoIVA)", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVADP_sku", "MVADP_SKU")]
        [SugarColumn(ColumnName = "MVADP_sku", ColumnDescription = "identificador: IdProducto + IdModelo + IdEspecificacion (Tamanio)", Length = 20)]
        public string Identificador {
            get { return this._Identificador; }
            set {
                this._Identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVADP_usr_n", "MVADP_USR_N")]
        [SugarColumn(ColumnName = "MVADP_usr_n", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this._Creo;
            }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("MVADP_fn", "MVADP_FN")]
        [SugarColumn(ColumnName = "MVADP_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVADP_usr_m", "MVADP_USR_M")]
        [SugarColumn(ColumnName = "MVADP_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this._Modifica;
            }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVADP_fm", "MVADP_FM")]
        [SugarColumn(ColumnName = "MVADP_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                if (this._FechaModifica >= new DateTime(1900, 1, 1))
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
