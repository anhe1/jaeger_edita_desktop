﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionRelacionadaModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private int idRemision;
        private string idDocumento;
        private string serie;
        private int folio;
        private int idDirectorio;
        private DateTime fechaEmision;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string receptorNombre;
        private decimal total;
        private int _IdTipoRelacion;
        private string _Relacion;
        private DateTime fechaNuevo;
        #endregion

        public RemisionRelacionadaModel() {
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("_dprmsr_id", "DPRMSR_ID")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRelacion {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion de la remision
        /// </summary>
        [DataNames("_dprmsr_dprms_id", "DPRMSR_DPRMS_ID")]
        [SugarColumn(ColumnName = "_dprmsr_dprms_id", ColumnDescription = "")]
        public int IdRemision {
            get {
                return this.idRemision;
            }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_dprmsr_drctr_id", "DPRMSR_DRCTR_ID")]
        [SugarColumn(ColumnName = "_dprmsr_drctr_id", ColumnDescription = "")]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de tipos de relacion
        /// </summary>
        [DataNames("_dprmsr_ctrel_id", "DPRMSR_CTREL_ID")]
        [SugarColumn(ColumnName = "_dprmsr_ctrel_id", ColumnDescription = "")]
        public int IdTipoRelacion {
            get { return this._IdTipoRelacion; }
            set {
                this._IdTipoRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion o motivo de la relacion
        /// </summary>
        [DataNames("_dprmsr_reln", "DPRMSR_RELN")]
        [SugarColumn(ColumnName = "_dprmsr_reln", ColumnDescription = "")]
        public string Relacion {
            get { return this._Relacion; }
            set {
                this._Relacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("_dprmsr_uuid", "DPRMSR_UUID")]
        [SugarColumn(ColumnName = "_dprmsr_uuid", ColumnDescription = "")]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_dprmsr_serie", "DPRMSR_SERIE")]
        [SugarColumn(ColumnName = "_dprmsr_serie", ColumnDescription = "")]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("DPRMSR_FOLIO")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("DPRMSR_NOMR")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public string Receptor {
            get {
                return this.receptorNombre;
            }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("DPRMSR_FECEMS")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("DPRMSR_TOTAL")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public decimal Total {
            get {
                return this.total;
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("DPRMSR_FN")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("DPRMSR_USR_N")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("DPRMSR_FM")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        [DataNames("DPRMSR_USR_M")]
        [SugarColumn(ColumnName = "_dprms_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
