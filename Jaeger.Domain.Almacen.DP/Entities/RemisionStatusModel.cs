﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionStatusModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int idRemision;
        private int idStatus;
        private DateTime? fechaModifica;
        private string modifica;
        #endregion

        public RemisionStatusModel() : base() {

        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("RMSDP_ID")]
        [SugarColumn(ColumnName = "RMSDP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this.idRemision; }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("RMSDP_STTS_ID")]
        [SugarColumn(ColumnName = "RMSDP_STTS_ID", ColumnDescription = "indice del status del comprobante")]
        public int IdStatus {
            get { return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("RMSDP_FM")]
        [SugarColumn(ColumnName = "RMSDP_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public DateTime? FechaModifica {
            get { if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        [DataNames("RMSDP_USR_M")]
        [SugarColumn(ColumnName = "RMSDP_USR_M", ColumnDescription = "clave del ultimo usuario que modifica", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
