﻿using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Almacen.DP.Contracts;
using Jaeger.Domain.Almacen.DP.ValueObjects;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.DP.Entities {
    [SugarTable("_rmsn", "Almacen PT: Remisionado")]
    public class RemisionDetailModel : RemisionModel, IRemisionDetailModel, IRemisionModel {
        #region declaraciones
        private BindingList<RemisionRelacionadaModel> remisionRelacionadaModels;
        private BindingList<RemisionConceptoDetailModel> _Conceptos;
        #endregion

        public RemisionDetailModel() : base() {
            this._Conceptos = new BindingList<RemisionConceptoDetailModel>() { RaiseListChangedEvents = true };
            this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
        }

        [SugarColumn(IsIgnore = true)]
        public RemisionStatusEnum Status {
            get { return (RemisionStatusEnum)this.IdStatus; }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string StatusText {
            get { return EnumerationExtension.GetEnumDescription((RemisionStatusEnum)this.IdStatus); }
        }

        /// <summary>
        /// obtener estado del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Estado {
            get {
                if (this.IdStatus > 0)
                    return "Vigente";
                return "Cancelado";
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get { return this.IdRemision == 0; }
        }

        /// <summary>
        /// lista de remisiones relacionadas
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionRelacionadaModel> RemisionRelacionada {
            get { return this.remisionRelacionadaModels; }
            set {
                this.remisionRelacionadaModels = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de conceptos de la remision
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionConceptoDetailModel> Conceptos {
            get { return this._Conceptos; }
            set {
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        public RemisionCalidadModel GetCalidad() {
            if (this.FechaCalidad != null) {
                return new RemisionCalidadModel {
                    Auditor = this.Auditor,
                    FechaCalidad = this.FechaCalidad,
                    NotaCalidad = this.NotaCalidad
                };
            }
            return null;
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.SubTotal);
            this.TotalDescuento = this.Conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.Descuento);
            this.TotalTrasladoIVA = this.Conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.TrasladoIVA);
            this.GTotal = (this.SubTotal - this.TotalDescuento) + (this.TotalTrasladoIVA + this.TotalTrasladoIEPS) - (this.TotalRetencionIVA + this.TotalRetencionISR);
        }
    }
}
