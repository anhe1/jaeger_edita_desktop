﻿using System;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionPartidaModel : RemisionConceptoDetailModel {
        #region declaraciones
        private int folio;
        private int idVendedor;
        private int idStatus;
        private int idTipoDocumento;
        private DateTime fechaEmision;
        private DateTime? fechaEntrega;
        private string version;
        private string serie;
        private string idDocumento;
        private string emisorRFC;
        private string receptorRFC;
        private string receptorNombre;
        private decimal total;
        private string contacto;
        private int idMetodoEnvio;
        private int idDomicilio;
        private int idNumeroGuia;
        private int idSerie;
        private int idCatalogo;
        private int idRelacion;
        private string _ReceptorClave;
        private DateTime ?fechaCancela;
        private string cancela;
        private int _ClaveCancela;
        private string _MotivoCancelacion;
        private string _NotaCancelacion;
        #endregion

        public RemisionPartidaModel() : base() {
        }

        #region datos generales del comprobante
        /// <summary>
        /// obtener o establecer tipo de documento
        /// </summary>
        [DataNames("RMSDP_CTDOC_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTDOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoDocumento {
            get {
                return this.idTipoDocumento;
            }
            set {
                this.idTipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [DataNames("RMSDP_VER")]
        [SugarColumn(ColumnName = "RMSDP_VER", ColumnDescription = "version del comprobante", Length = 3)]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("RMSDP_STTS_ID")]
        [SugarColumn(ColumnName = "RMSDP_STTS_ID", ColumnDescription = "indice del status del comprobante")]
        public int IdStatus {
            get {
                return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("RMSDP_FOLIO")]
        [SugarColumn(ColumnName = "RMSDP_FOLIO", ColumnDescription = "folio de control interno")]
        public int Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        [DataNames("RMSDP_CTSR_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTSR_ID", ColumnDescription = "indice de la tabla de series")]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        [DataNames("RMSDP_SERIE")]
        [SugarColumn(ColumnName = "RMSDP_SERIE", ColumnDescription = "serie de control interno del documento en modo texto", Length = 10, IsNullable = true)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("RMSDP_CTCLS_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTCLS_ID", ColumnDescription = "indice del catalogo de productos")]
        public int IdCatalogo {
            get { return this.idCatalogo; }
            set {
                this.idCatalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del metodo de envio (DPRMS_MTDENV_ID)
        /// </summary>
        [DataNames("RMSDP_CTENV_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTENV_ID", ColumnDescription = "indice de la tabla del metodo de envio")]
        public int IdMetodoEnvio {
            get { return this.idMetodoEnvio; }
            set {
                this.idMetodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        [DataNames("RMSDP_DRCCN_ID")]
        [SugarColumn(ColumnName = "RMSDP_DRCCN_ID", ColumnDescription = "indice del domicilio del directorio")]
        public int IdDomicilio {
            get { return this.idDomicilio; }
            set {
                this.idDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del vendedor
        /// </summary>
        [DataNames("RMSDP_VNDDR_ID")]
        [SugarColumn(ColumnName = "RMSDP_VNDDR_ID", ColumnDescription = "indice de la tabla de vendedores")]
        public int IdVendedor {
            get { return this.idVendedor; }
            set {
                this.idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de envio
        /// </summary>
        [DataNames("RMSDP_GUIA_ID")]
        [SugarColumn(ColumnName = "RMSDP_GUIA_ID", ColumnDescription = "numero de guia o referencia del metodo de envio")]
        public int NoGuia {
            get { return this.idNumeroGuia; }
            set {
                this.idNumeroGuia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("RMSDP_CTREL_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        [DataNames("RMSDP_RFCE")]
        [SugarColumn(ColumnName = "RMSDP_RFCE", ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("RMSDP_CLVR")]
        [SugarColumn(ColumnName = "RMSDP_CLVR", ColumnDescription = "clave de control interno de cliente", Length = 10)]
        public string ReceptorClave {
            get { return this._ReceptorClave; }
            set {
                this._ReceptorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        [DataNames("RMSDP_RFCR")]
        [SugarColumn(ColumnName = "RMSDP_RFCR", ColumnDescription = "registro federal de contribuyentes del cliente o receptor", Length = 14)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        [DataNames("RMSDP_NOMR")]
        [SugarColumn(ColumnName = "RMSDP_NOMR", ColumnDescription = "nombre o razon social del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        [DataNames("RMSDP_CNTCT")]
        [SugarColumn(ColumnName = "RMSDP_CNTCT", ColumnDescription = "nombre de la persona que recibe", Length = 255)]
        public string Contacto {
            get { return this.contacto; }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("RMSDP_UUID")]
        [SugarColumn(ColumnName = "RMSDP_UUID", ColumnDescription = "id de documento", Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("RMSDP_FECEMS")]
        [SugarColumn(ColumnName = "RMSDP_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega (DPRMS_FECENT)
        /// </summary>
        [DataNames("RMSDP_FECENT")]
        [SugarColumn(ColumnName = "RMSDP_FECENT", ColumnDescription = "fecha de entrega del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate)
                    return this.fechaEntrega;
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("RMSDP_GTOTAL")]
        [SugarColumn(ColumnName = "RMSDP_GTOTAL", ColumnDescription = "total del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal GTotal {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region cancelacion
        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("RMSDP_FCCNCL")]
        [SugarColumn(ColumnName = "RMSDP_FCCNCL", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que cancela el comprobante
        /// </summary>
        [DataNames("RMSDP_USR_C")]
        [SugarColumn(ColumnName = "RMSDP_USR_C", ColumnDescription = "clave del usuario que cancela el comprobante", Length = 10, IsNullable = true)]
        public string Cancela {
            get { return this.cancela; }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CVCAN")]
        [SugarColumn(ColumnName = "RMSDP_CVCAN", ColumnDescription = "clave del tipo de cancelacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLMTV")]
        [SugarColumn(ColumnName = "RMSDP_CLMTV", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string ClaveCancelacion {
            get => this._MotivoCancelacion;
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLNTA")]
        [SugarColumn(ColumnName = "RMSDP_CLNTA", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string NotaCancelacion {
            get { return this._NotaCancelacion; }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
