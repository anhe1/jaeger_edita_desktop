﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionRelacionModel : BaseSingleModel {
        public RemisionRelacionModel() {

        }

        public RemisionRelacionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
