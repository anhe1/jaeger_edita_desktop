﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Almacen.DP.Entities {
    /// <summary>
    /// clase para cancelacion de remision interna
    /// </summary>
    public class RemisionCancelacionModel : BasePropertyChangeImplementation, ICancelacion {
        #region declaraciones
        private int idRemision;
        private int idRelacion;
        private int idStatus;
        private DateTime? fechaCancela;
        private string cancela;
        private int _ClaveCancela;
        private string _MotivoCancelacion;
        private string _NotaCancelacion;
        #endregion

        public RemisionCancelacionModel() {

        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("RMSDP_ID")]
        [SugarColumn(ColumnName = "RMSDP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this.idRemision; }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("RMSDP_STTS_ID")]
        [SugarColumn(ColumnName = "RMSDP_STTS_ID", ColumnDescription = "indice del status del comprobante")]
        public int IdStatus {
            get {
                return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("RMSDP_CTREL_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        #region cancelacion
        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("RMSDP_FCCNCL")]
        [SugarColumn(ColumnName = "RMSDP_FCCNCL", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que cancela el comprobante
        /// </summary>
        [DataNames("RMSDP_USR_C")]
        [SugarColumn(ColumnName = "RMSDP_USR_C", ColumnDescription = "clave del usuario que cancela el comprobante", Length = 10, IsNullable = true)]
        public string Cancela {
            get { return this.cancela; }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CVCAN")]
        [SugarColumn(ColumnName = "RMSDP_CVCAN", ColumnDescription = "clave del tipo de cancelacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLMTV")]
        [SugarColumn(ColumnName = "RMSDP_CLMTV", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string ClaveCancelacion {
            get => this._MotivoCancelacion;
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLNTA")]
        [SugarColumn(ColumnName = "RMSDP_CLNTA", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string NotaCancelacion {
            get { return this._NotaCancelacion; }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        public object Tag { get; set; }
    }
}
