﻿using SqlSugar;
using System.ComponentModel;
using Jaeger.Domain.Almacen.DP.ValueObjects;

namespace Jaeger.Domain.Almacen.DP.Entities {
    [SugarTable("_mvapt", "ALMPT: movimientos")]
    public class RemisionConceptoDetailModel : RemisionConceptoModel {
        #region declaraciones
        private BindingList<RemisionConceptoParte> remisionConceptoPartes;
        #endregion

        public RemisionConceptoDetailModel() : base() {
            this.remisionConceptoPartes = new BindingList<RemisionConceptoParte>();
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionConceptoParte> Partes {
            get { return this.remisionConceptoPartes; }
            set {
                this.remisionConceptoPartes = value;
                this.OnPropertyChanged();
            }
        }

        //[SugarColumn(IsIgnore = true)]
        //public Documento2TipoEnum Tipo {
        //    get { return (Documento2TipoEnum)this.IdTipo; }
        //    set {
        //        this.IdTipo = (int)value;
        //        this.OnPropertyChanged();
        //    }
        //}

        [SugarColumn(IsIgnore = true)]
        public ValeAlmPTTipoEnum Efecto {
            get { return (ValeAlmPTTipoEnum)this.IdEfecto; }
            set {
                this.IdEfecto = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal Cantidad {
            get {
                if (this.Efecto == ValeAlmPTTipoEnum.Ingreso)
                    return base.CantidadE;
                else return base.CantidadS;
            }
            set {
                if (this.Efecto == ValeAlmPTTipoEnum.Ingreso)
                    base.CantidadE = value;
                else
                    base.CantidadS = value; ;
            }
        }

        public new RemisionConceptoDetailModel Clone() {
            var duplicado = (RemisionConceptoDetailModel)base.Clone();
            // concepto parte sin referencia anterior
            return duplicado;
        }
    }
}
