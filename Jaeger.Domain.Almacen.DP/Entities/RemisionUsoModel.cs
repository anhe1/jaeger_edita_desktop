﻿namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionUsoModel : Base.Abstractions.BaseSingleModel {
        public RemisionUsoModel() { }

        public RemisionUsoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
