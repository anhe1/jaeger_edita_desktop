﻿using System;
using SqlSugar;

namespace Jaeger.Domain.Almacen.DP.Entities {
    /// <summary>
    /// partidas o conceptos del comprobante de remision
    /// </summary>
    [SugarTable("_dpmva", "ALMPT: movimientos")]
    public class RemisionConceptoModel : MovimientoDetailModel, ICloneable {
        #region declaraciones
        #endregion

        public RemisionConceptoModel() : base() {
            this.IdTipo = 26;
            this.IdEfecto = 2;
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
