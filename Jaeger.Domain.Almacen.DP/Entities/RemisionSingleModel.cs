﻿using Jaeger.Domain.Almacen.DP.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionSingleModel : RemisionDetailModel, IRemisionDetailModel, IRemisionSingleModel, IRemisionModel {
        public RemisionSingleModel() : base() {

        }

        public RemisionSingleModel(RemisionDetailModel source) : base() {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionSingleModel>(source, this);
        }

        public void SetValues(RemisionDetailModel source) {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionSingleModel>(source, this);
        }
    }
}
