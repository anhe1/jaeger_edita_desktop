﻿using System;
using SqlSugar;
using Jaeger.Domain.Almacen.DP.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Almacen.DP.Entities {
    [SugarTable("RMSDP", "Departamentos: Remisionado")]
    public class RemisionModel : BasePropertyChangeImplementation, IRemisionModel, IControlCalidad, ICancelacion {
        #region declaraciones
        private int idRemision;
        private bool activo;
        private int precisionDecimal;
        private int idCliente;
        private int folio;
        private int idVendedor;
        private int idStatus;
        private int idTipoDocumento;
        private int _IdDepartamento;
        private DateTime fechaEmision;
        private DateTime? fechaCancela;
        private DateTime? fechaEntrega;
        private DateTime? fechaModifica;
        private DateTime fechaNuevo;
        private string version;
        private string serie;
        private string idDocumento;
        private string emisorRFC;
        private string receptorRFC;
        private string receptorNombre;
        private decimal tipoCambio;
        private decimal trasladoIVA;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private string claveMoneda;
        private string vendedor;
        private string creo;
        private string modifica;
        private string nota;
        private string contacto;
        private int idMetodoEnvio;
        private int idDomicilio;
        private int idNumeroGuia;
        private int idSerie;
        private int idCatalogo;
        private int idRelacion;
        private string cancela;
        private string _ReceptorClave;
        private string _DomicilioEntrega;
        private decimal _TotalRetencionISR;
        private decimal _TotalRetencionIVA;
        private decimal _TotalRetencionIEPS;
        private decimal _TotalTrasladoIEPS;
        private string _urlPDF;
        private string _Auditor;
        private DateTime? _FechaCalidad;
        private string _NotaCalidad;
        private int _ClaveCancela;
        private string _MotivoCancelacion;
        private string _NotaCancelacion;
        #endregion

        public RemisionModel() {
            this.Version = "2.0";
            this.Activo = true;
            this.IdStatus = 1;
            this.PrecisionDecimal = 2;
            this.idTipoDocumento = 1;
            this.FechaNuevo = DateTime.Now;
            this.FechaEmision = DateTime.Now;
            this.claveMoneda = "MXN";
            this.IdMetodoEnvio = 1;
        }

        #region datos generales del comprobante
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("RMSDP_ID")]
        [SugarColumn(ColumnName = "RMSDP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this.idRemision; }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("RMSDP_A")]
        [SugarColumn(ColumnName = "RMSDP_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CTDEP_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTDEP_ID", ColumnDescription = "indice de relacion de la tabla de departamentos", DefaultValue = "1", IsNullable = false)]
        public int IdDepartamento {
            get { return this._IdDepartamento; }
            set {
                this._IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento
        /// </summary>
        [DataNames("RMSDP_CTDOC_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTDOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoDocumento {
            get {
                return this.idTipoDocumento;
            }
            set {
                this.idTipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [DataNames("RMSDP_VER")]
        [SugarColumn(ColumnName = "RMSDP_VER", ColumnDescription = "version del comprobante", Length = 3)]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// precision decimal
        /// </summary>
        [DataNames("RMSDP_DECI")]
        [SugarColumn(ColumnName = "RMSDP_DECI", ColumnDescription = "posiciones de decimales", Length = 4)]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set {
                this.precisionDecimal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("RMSDP_STTS_ID")]
        [SugarColumn(ColumnName = "RMSDP_STTS_ID", ColumnDescription = "indice del status del comprobante")]
        public int IdStatus {
            get {
                return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("RMSDP_FOLIO")]
        [SugarColumn(ColumnName = "RMSDP_FOLIO", ColumnDescription = "folio de control interno")]
        public int Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        [DataNames("RMSDP_CTSR_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTSR_ID", ColumnDescription = "indice de la tabla de series")]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        [DataNames("RMSDP_SERIE")]
        [SugarColumn(ColumnName = "RMSDP_SERIE", ColumnDescription = "serie de control interno del documento en modo texto", Length = 10, IsNullable = true)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("RMSDP_CTCLS_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTCLS_ID", ColumnDescription = "indice del catalogo de productos")]
        public int IdCatalogo {
            get { return this.idCatalogo; }
            set {
                this.idCatalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del metodo de envio (DPRMS_MTDENV_ID)
        /// </summary>
        [DataNames("RMSDP_CTENV_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTENV_ID", ColumnDescription = "indice de la tabla del metodo de envio")]
        public int IdMetodoEnvio {
            get { return this.idMetodoEnvio; }
            set {
                this.idMetodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [DataNames("RMSDP_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSDP_DRCTR_ID", ColumnDescription = "indice del direcotorio")]
        public int IdCliente {
            get { return this.idCliente; }
            set {
                this.idCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        [DataNames("RMSDP_DRCCN_ID")]
        [SugarColumn(ColumnName = "RMSDP_DRCCN_ID", ColumnDescription = "indice del domicilio del directorio")]
        public int IdDomicilio {
            get { return this.idDomicilio; }
            set {
                this.idDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del vendedor
        /// </summary>
        [DataNames("RMSDP_VNDDR_ID")]
        [SugarColumn(ColumnName = "RMSDP_VNDDR_ID", ColumnDescription = "indice de la tabla de vendedores")]
        public int IdVendedor {
            get { return this.idVendedor; }
            set {
                this.idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de envio
        /// </summary>
        [DataNames("RMSDP_GUIA_ID")]
        [SugarColumn(ColumnName = "RMSDP_GUIA_ID", ColumnDescription = "numero de guia o referencia del metodo de envio")]
        public int NoGuia {
            get { return this.idNumeroGuia; }
            set {
                this.idNumeroGuia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("RMSDP_CTREL_ID")]
        [SugarColumn(ColumnName = "RMSDP_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        [DataNames("RMSDP_RFCE")]
        [SugarColumn(ColumnName = "RMSDP_RFCE", ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("RMSDP_CLVR")]
        [SugarColumn(ColumnName = "RMSDP_CLVR", ColumnDescription = "clave de control interno de cliente", Length = 10)]
        public string ReceptorClave {
            get { return this._ReceptorClave; }
            set {
                this._ReceptorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        [DataNames("RMSDP_RFCR")]
        [SugarColumn(ColumnName = "RMSDP_RFCR", ColumnDescription = "registro federal de contribuyentes del cliente o receptor", Length = 14)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "").Replace(" ", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        [DataNames("RMSDP_NOMR")]
        [SugarColumn(ColumnName = "RMSDP_NOMR", ColumnDescription = "nombre o razon social del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        [DataNames("RMSDP_CNTCT")]
        [SugarColumn(ColumnName = "RMSDP_CNTCT", ColumnDescription = "nombre de la persona que recibe", Length = 255)]
        public string Contacto {
            get { return this.contacto; }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("RMSDP_UUID")]
        [SugarColumn(ColumnName = "RMSDP_UUID", ColumnDescription = "id de documento", Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("RMSDP_FECEMS")]
        [SugarColumn(ColumnName = "RMSDP_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega (DPRMS_FECENT)
        /// </summary>
        [DataNames("RMSDP_FECENT")]
        [SugarColumn(ColumnName = "RMSDP_FECENT", ColumnDescription = "fecha de entrega del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate)
                    return this.fechaEntrega;
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        [DataNames("RMSDP_TPCMB")]
        [SugarColumn(ColumnName = "RMSDP_TPCMB", ColumnDescription = "tipo de cambio", DefaultValue = "1", IsNullable = true)]
        public decimal TipoCambio {
            get { return this.tipoCambio; }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subtotal del comprobante
        /// </summary>
        [DataNames("RMSDP_SBTTL")]
        [SugarColumn(ColumnName = "RMSDP_SBTTL", ColumnDescription = "subtotal", DefaultValue = "0")]
        public decimal SubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del descuento (suma de los descuentos aplicados en las partidas de la remision)
        /// </summary>
        [DataNames("RMSDP_DSCNT")]
        [SugarColumn(ColumnName = "RMSDP_DSCNT", ColumnDescription = "importe del descuento (suma de los descuentos aplicados en las partidas de la remision)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalDescuento {
            get { return this.descuento; }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establer el importe de la remision antes de impuestos (SubToal - TotalDescuentos)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal Importe {
            get { return (this.SubTotal - this.TotalDescuento); }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA (suma del iva de las partidas de la remision)
        /// </summary>
        [DataNames("RMSDP_TRSIVA")]
        [SugarColumn(ColumnName = "RMSDP_TRSIVA", ColumnDescription = "total del impuesto trasladado IVA (suma del iva de las partidas de la remision)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de retencion del impuesto ISR
        /// </summary>
        [DataNames("RMSDP_RETISR")]
        [SugarColumn(ColumnName = "RMSDP_RETISR", ColumnDescription = "total de retencion del impuesto ISR", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalRetencionISR {
            get { return this._TotalRetencionISR; }
            set {
                this._TotalRetencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IVA
        /// </summary>
        [DataNames("RMSDP_RETIVA")]
        [SugarColumn(ColumnName = "RMSDP_RETIVA", ColumnDescription = "total del impuesto retenido IVA", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalRetencionIVA {
            get { return this._TotalRetencionIVA; }
            set {
                this._TotalRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IEPS
        /// </summary>
        [DataNames("RMSDP_RETIEPS")]
        [SugarColumn(ColumnName = "RMSDP_RETIEPS", ColumnDescription = "total del impuesto retenido IEPS", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalRetencionIEPS {
            get { return this._TotalRetencionIEPS; }
            set {
                this._TotalRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto traslado IEPS
        /// </summary>
        [DataNames("RMSDP_TRSIEPS")]
        [SugarColumn(ColumnName = "RMSDP_TRSIEPS", ColumnDescription = "total del impuesto traslado IEPS", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIEPS {
            get { return this._TotalTrasladoIEPS; }
            set {
                _TotalTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("RMSDP_GTOTAL")]
        [SugarColumn(ColumnName = "RMSDP_GTOTAL", ColumnDescription = "total del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal GTotal {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("RMSDP_MONEDA")]
        [SugarColumn(ColumnName = "RMSDP_MONEDA", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la dirección de entrega en modo texto (DPRMS_DRCCN)
        /// </summary>
        [DataNames("RMSDP_DRCCN")]
        [SugarColumn(ColumnName = "RMSDP_DRCCN", ColumnDescription = "dirección de entrega en modo texto", Length = 400)]
        public string DomicilioEntrega {
            get { return this._DomicilioEntrega; }
            set {
                this._DomicilioEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer una nota
        /// </summary>
        [DataNames("RMSDP_NOTA")]
        [SugarColumn(ColumnName = "RMSDP_NOTA", ColumnDescription = "observaciones", Length = 255, IsNullable = true)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociada
        /// </summary>
        [DataNames("RMSDP_VNDDR")]
        [SugarColumn(ColumnName = "RMSDP_VNDDR", ColumnDescription = "clave del vendedor asociado", Length = 10, IsNullable = true)]
        public string Vendedor {
            get { return this.vendedor; }
            set {
                this.vendedor = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region cancelacion
        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("RMSDP_FCCNCL")]
        [SugarColumn(ColumnName = "RMSDP_FCCNCL", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que cancela el comprobante
        /// </summary>
        [DataNames("RMSDP_USR_C")]
        [SugarColumn(ColumnName = "RMSDP_USR_C", ColumnDescription = "clave del usuario que cancela el comprobante", Length = 10, IsNullable = true)]
        public string Cancela {
            get { return this.cancela; }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CVCAN")]
        [SugarColumn(ColumnName = "RMSDP_CVCAN", ColumnDescription = "clave del tipo de cancelacion", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLMTV")]
        [SugarColumn(ColumnName = "RMSDP_CLMTV", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string ClaveCancelacion {
            get => this._MotivoCancelacion;
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLNTA")]
        [SugarColumn(ColumnName = "RMSDP_CLNTA", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string NotaCancelacion {
            get { return this._NotaCancelacion; }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region creacion
        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("RMSDP_URL_PDF")]
        [SugarColumn(ColumnName = "RMSDP_URL_PDF", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("RMSDP_FN")]
        [SugarColumn(ColumnName = "RMSDP_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario creador del registro
        /// </summary>
        [DataNames("RMSDP_USR_N")]
        [SugarColumn(ColumnName = "RMSDP_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("RMSDP_FM")]
        [SugarColumn(ColumnName = "RMSDP_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        [DataNames("RMSDP_USR_M")]
        [SugarColumn(ColumnName = "RMSDP_USR_M", ColumnDescription = "clave del ultimo usuario que modifica", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region control de calidad
        [DataNames("RMSDP_CLDD_USR")]
        public string Auditor {
            get { return this._Auditor; }
            set {
                this._Auditor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLDD_FEC")]
        public DateTime? FechaCalidad {
            get { return this._FechaCalidad; }
            set {
                this._FechaCalidad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSDP_CLDD_NOT")]
        public string NotaCalidad {
            get { return this._NotaCalidad; }
            set {
                this._NotaCalidad = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        [SugarColumn(IsIgnore = true)]
        public RemisionCancelacionModel Cancelacion {
            get {
                return new RemisionCancelacionModel {
                    IdRelacion = this.IdRelacion,
                    IdRemision = this.IdRemision,
                    IdStatus = this.IdStatus,
                    Cancela = this.Cancela,
                    ClaveCancela = this.ClaveCancela,
                    ClaveCancelacion = this.ClaveCancelacion,
                    FechaCancela = this.FechaCancela,
                    NotaCancelacion = this.NotaCancelacion
                };
            }
            set {
                if (value != null) {
                    IdRelacion = value.IdRelacion;
                    IdRemision = value.IdRemision;
                    IdStatus = value.IdStatus;
                    Cancela = value.Cancela;
                    ClaveCancela = value.ClaveCancela;
                    ClaveCancelacion = value.ClaveCancelacion;
                    FechaCancela = value.FechaCancela;
                    NotaCancelacion = value.NotaCancelacion;
                }
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag {
            get; set;
        }
    }
}
