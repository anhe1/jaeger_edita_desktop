﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionPrinter : RemisionDetailModel {
        public RemisionPrinter() : base() { 
        }

        public RemisionPrinter(RemisionDetailModel source) : base() {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionPrinter>(source, this);
        }

        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.IdDocumento,
                    (this.Folio.ToString("000000") == null ? "" : "&folio=" + this.Folio.ToString()),
                    "&rfce=", this.EmisorRFC, "&rfcr=", this.ReceptorRFC, "&fecha=", this.FechaEmision.ToShortDateString(), "&total=", this.Importe.ToString("0.00") };
            }
        }
    }
}
