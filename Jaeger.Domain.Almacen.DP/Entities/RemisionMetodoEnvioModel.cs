﻿namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionMetodoEnvioModel : Base.Abstractions.BaseSingleModel {
        public RemisionMetodoEnvioModel() { }

        public RemisionMetodoEnvioModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
