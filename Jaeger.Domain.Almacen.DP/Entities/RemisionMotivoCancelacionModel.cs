﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.DP.Entities {
    /// <summary>
    /// clase para motivos de cancelacion
    /// </summary>
    public class RemisionMotivoCancelacionModel : BaseSingleModel {
        public RemisionMotivoCancelacionModel() {

        }

        public RemisionMotivoCancelacionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
