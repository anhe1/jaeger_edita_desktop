﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionCalidadModel : BasePropertyChangeImplementation, IControlCalidad {
        #region declaraciones
        private int _IdRemision;
        private string _Auditor;
        private DateTime? _FechaCalidad;
        private string _NotaCalidad;
        #endregion

        public RemisionCalidadModel() {

        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("RMSDP_ID")]
        [SugarColumn(ColumnName = "RMSDP_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this._IdRemision; }
            set {
                this._IdRemision = value;
                this.OnPropertyChanged();
            }
        }

        #region control de calidad
        /// <summary>
        /// obtener o establecer la clave del auditor
        /// </summary>
        [DataNames("RMSDP_CLDD_USR")]
        public string Auditor {
            get { return this._Auditor; }
            set {
                this._Auditor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de calidad
        /// </summary>
        [DataNames("RMSDP_CLDD_FEC")]
        public DateTime? FechaCalidad {
            get { return this._FechaCalidad; }
            set {
                this._FechaCalidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        [DataNames("RMSDP_CLDD_NOT")]
        public string NotaCalidad {
            get { return this._NotaCalidad; }
            set {
                this._NotaCalidad = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
