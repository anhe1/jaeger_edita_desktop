﻿using Jaeger.Domain.Almacen.DP.ValueObjects;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.DP.Entities {
    public class RemisionDetailPrinter : RemisionDetailModel {

        public RemisionDetailPrinter() : base() {

        }

        public RemisionDetailPrinter(RemisionDetailModel source) : base() {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionDetailPrinter>(source, this);
        }

        public string DepartamentoText { get; set; }

        public string UsoRemisionText {
            get { return EnumerationExtension.GetEnumDescription((RemisionUsoEnum)this.IdTipoDocumento); }
        }

        public string MetodoEnvioText {
            get; set;
        }

        /// <summary>
        /// informacion para QR
        /// </summary>
        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.IdDocumento,
                    (this.Folio.ToString("000000") == null ? "" : "&folio=" + this.Folio.ToString()),
                    "&rfce=", this.EmisorRFC, "&rfcr=", this.ReceptorRFC, "&fecha=", this.FechaEmision.ToShortDateString(), "&total=", this.Importe.ToString("0.00") };
            }
        }
    }
}
