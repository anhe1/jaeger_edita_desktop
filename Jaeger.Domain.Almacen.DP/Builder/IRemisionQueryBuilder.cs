﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.DP.Builder {
    public interface IRemisionQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IRemisionYearQueryBuilder Year(int year);
        IRemisionByOrdenQueryBuilder ByOrden(int year);
    }

    public interface IRemisionYearQueryBuilder : IConditionalBuilder {
        IRemisionYearQueryBuilder Month(int month = 0);
        IRemisionIdClienteQueryBuilder IdCliente(int idCliente);
    }

    public interface IRemisionMonthQueryBuilder : IConditionalBuilder { }

    public interface IRemisionIdClienteQueryBuilder : IConditionalBuilder { }

    public interface IRemisionByOrdenQueryBuilder : IConditionalBuilder { }
}
