﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.DP.Builder {
    public class RemisionQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IRemisionQueryBuilder, IRemisionYearQueryBuilder, IRemisionMonthQueryBuilder, IRemisionIdClienteQueryBuilder,
        IRemisionByOrdenQueryBuilder {

        public RemisionQueryBuilder() : base() { }

        public IRemisionYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("RMSDP_ANIO", year.ToString()));
            return this;
        }

        public IRemisionYearQueryBuilder Month(int month = 0) {
            if (month > 0)
                this._Conditionals.Add(new Conditional("RMSDP_MES", month.ToString()));
            return this;
        }

        public IRemisionIdClienteQueryBuilder IdCliente(int idCliente) {
            this._Conditionals.Add(new Conditional("RMSDP_DRCTR_ID", idCliente.ToString()));
            return this;
        }

        public IRemisionByOrdenQueryBuilder ByOrden(int idOrden) {
            this._Conditionals.Add(new Conditional("MVADP_PDCLN_ID", idOrden.ToString(), Base.ValueObjects.ConditionalTypeEnum.In));
            return this;
        }
    }
}
