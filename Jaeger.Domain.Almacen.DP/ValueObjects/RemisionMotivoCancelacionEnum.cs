﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.DP.ValueObjects {
    public enum RemisionMotivoCancelacionEnum {
        [Description("Sustitución de remisión previa")]
        Sustitucion = 1,
        [Description("Error al emitir")]
        ErrorEmision = 2,
    }
}
