﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.DP.ValueObjects {
    public enum RemisionStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Emitido")]
        Emitido = 1,
        [Description("Entregado")]
        Entregado = 2,
        [Description("Recibido")]
        Recibido = 3
    }
}
