﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.DP.ValueObjects {
    public enum RemisionUsoEnum {
        [Description("No Definido")]
        NoDefinido = 0,
        [Description("Control Interno")]
        ControlInterno = 1,
        [Description("Maquila")]
        Maquila = 2,
    }
}
