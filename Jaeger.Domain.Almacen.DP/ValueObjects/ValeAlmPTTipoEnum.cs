﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.DP.ValueObjects {
    /// <summary>
    /// tipos de vales
    /// </summary>
    public enum ValeAlmPTTipoEnum {
        [Description("Ingreso")]
        Ingreso = 1,
        [Description("Egreso")]
        Egreso = 2,
        [Description("Traslado")]
        Traslado = 3
    }
}
