﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.DP.Contracts {
    public interface ISqlRemisionPartidaRepository : IGenericRepository<RemisionConceptoModel> {
        RemisionConceptoDetailModel Save(RemisionConceptoDetailModel model);
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
