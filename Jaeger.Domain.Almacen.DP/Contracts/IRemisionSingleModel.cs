﻿namespace Jaeger.Domain.Almacen.DP.Contracts {
    public interface IRemisionSingleModel : IRemisionModel, IRemisionDetailModel {

    }
}
