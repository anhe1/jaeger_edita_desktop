﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Almacen.DP.ValueObjects;

namespace Jaeger.Domain.Almacen.DP.Contracts {
    public interface IRemisionDetailModel : IRemisionModel {
        RemisionStatusEnum Status { get; set; }
        string StatusText { get; }

        /// <summary>
        /// obtener estado del comprobante
        /// </summary>
        string Estado { get; }

        bool IsEditable { get; }

        /// <summary>
        /// lista de remisiones relacionadas
        /// </summary>
        BindingList<RemisionRelacionadaModel> RemisionRelacionada { get; set; }

        /// <summary>
        /// obtener o establecer la lista de conceptos de la remision
        /// </summary>
        BindingList<RemisionConceptoDetailModel> Conceptos {  get; set; }

        RemisionCalidadModel GetCalidad();
    }
}
