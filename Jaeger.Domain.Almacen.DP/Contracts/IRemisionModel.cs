﻿using System;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Almacen.DP.Contracts {
    public interface IRemisionModel : IEntityBase {
        /// <summary>
        /// obtener o establecer el indice de la remision
        /// </summary>
        int IdRemision { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer precision decimal
        /// </summary>
        int PrecisionDecimal { get; set; }

        /// <summary>
        /// obtener o establcer el folio de control interno
        /// </summary>
        int Folio { get; set; }

        /// <summary>
        /// obtener o establecer version del comprobante
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        int IdSerie { get; set; }

        /// <summary>
        /// obtener o establecer serie del comprobante
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int IdTipoDocumento { get; set; }

        /// <summary>
        /// obtener o establecer indice del metodo de envio
        /// </summary>
        int IdMetodoEnvio { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_CTLGVDDR_ID)
        /// </summary>
        int IdVendedor { get; set; }

        /// <summary>
        /// obtener o establecer el indice del stastus del comprobante
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        int IdCliente { get; set; }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        int IdDomicilio { get; set; }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de pago
        /// </summary>
        int NoGuia { get; set; }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        string Contacto { get; set; }

        /// <summary>
        /// obtener o establecer iddocumento
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer notas
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancela { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de entrega del comprobante
        /// </summary>
        DateTime? FechaEntrega { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        decimal TipoCambio { get; set; }

        /// <summary>
        /// obtener o establecer subtotal del comprobante
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer el importe del impuesto trasladado IVA
        /// </summary>
        decimal TotalTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer importe del descuento
        /// </summary>
        decimal TotalDescuento { get; set; }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        decimal GTotal { get; set; }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociado
        /// </summary>
        string Vendedor { get; set; }
    }
}
