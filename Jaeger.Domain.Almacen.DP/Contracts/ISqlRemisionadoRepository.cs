﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.DP.Contracts {
    public interface ISqlRemisionadoRepository : IGenericRepository<RemisionModel> {
        T1 GetById<T1>(int index) where T1 : class, new();

        RemisionDetailModel Save(RemisionDetailModel model);

        bool Cancelar(RemisionCancelacionModel item);

        bool SetStatus(RemisionStatusModel item);

        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
