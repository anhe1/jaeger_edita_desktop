﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Tienda.Builder {
    public interface IPreguntasFrecuentesGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IPreguntasFrecuentesTempleteGridBuilder Templetes();
    }

    public interface IPreguntasFrecuentesTempleteGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IPreguntasFrecuentesTempleteGridBuilder Master();
    }

    public interface IPreguntasFrecuentesColumnsGridBuilder : IGridViewColumnsBuild {
        IPreguntasFrecuentesColumnsGridBuilder ColPregunta();
        IPreguntasFrecuentesColumnsGridBuilder ColRespuesta();
        IPreguntasFrecuentesColumnsGridBuilder ColFechaNuevo();
        IPreguntasFrecuentesColumnsGridBuilder ColCreo();
    }
}
