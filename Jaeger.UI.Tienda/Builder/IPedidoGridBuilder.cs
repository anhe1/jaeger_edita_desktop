﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Tienda.Builder {
    public interface IPedidoGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IPedidoTempleteGridBuilder Templetes();
    }

    public interface IPedidoTempleteGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IPedidoTempleteGridBuilder Master(bool values = true);
        IPedidoTempleteGridBuilder SinImportes();
        IPedidoTempleteGridBuilder Cancelacion();
        IPedidoTempleteGridBuilder Conceptos(bool values = true);
        IPedidoTempleteGridBuilder ConceptosSinImportes();
        IPedidoTempleteGridBuilder MetodoPago();
    }

    public interface IPedidoColumnsGridBuilder : IGridViewColumnsBuild {
        IPedidoColumnsGridBuilder IdPedido();
        IPedidoColumnsGridBuilder CboStatus();
        IPedidoColumnsGridBuilder ColCboIdStatus();
        IPedidoColumnsGridBuilder IdOrigen();
        IPedidoColumnsGridBuilder IdFormaPago();
        IPedidoColumnsGridBuilder Confirma();
        IPedidoColumnsGridBuilder FechaConfirma();
        IPedidoColumnsGridBuilder ClienteClave();
        IPedidoColumnsGridBuilder Cliente();
        IPedidoColumnsGridBuilder Recibe();
        IPedidoColumnsGridBuilder MetodoEnvio();
        IPedidoColumnsGridBuilder IdVendedor();
        IPedidoColumnsGridBuilder FechaPedido();
        IPedidoColumnsGridBuilder FechaAcuerdo();
        IPedidoColumnsGridBuilder FechaEntrega();
        IPedidoColumnsGridBuilder FechaCancela();
        IPedidoColumnsGridBuilder Cancela();
        IPedidoColumnsGridBuilder ClaveCancelacion();
        IPedidoColumnsGridBuilder NotaCancelacion();
        IPedidoColumnsGridBuilder SubTotal();
        IPedidoColumnsGridBuilder TotalDescuento();
        IPedidoColumnsGridBuilder TotalEnvio();
        IPedidoColumnsGridBuilder TotalTrasladoIVA();
        IPedidoColumnsGridBuilder GranTotal();
        IPedidoColumnsGridBuilder ColReqFactura();
        IPedidoColumnsGridBuilder Nota();
        IPedidoColumnsGridBuilder Creo();
        IPedidoColumnsGridBuilder FechaNuevo();

        #region conceptos
        IPedidoColumnsGridBuilder Cantidad();
        IPedidoColumnsGridBuilder Unidad();
        IPedidoColumnsGridBuilder Descriptor();
        IPedidoColumnsGridBuilder Unitario();
        IPedidoColumnsGridBuilder Descuento();
        IPedidoColumnsGridBuilder Importe();
        IPedidoColumnsGridBuilder TasaIVA();
        IPedidoColumnsGridBuilder ColTrasladoIVA();
        IPedidoColumnsGridBuilder Identificador();
        #endregion
    }
}
