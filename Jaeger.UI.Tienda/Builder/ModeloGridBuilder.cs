﻿using Jaeger.UI.Almacen.Builder;

namespace Jaeger.UI.Tienda.Builder {
    public class ModeloGridBuilder : UI.Almacen.Builder.ModeloGridBuilder {
        public ModeloGridBuilder() : base() { }
    }

    public class ProductoGridBuilder : UI.Almacen.Builder.ProductoGridBuilder {
        public ProductoGridBuilder() : base() { }

        public override IProductoTempletesGridBuilder ProductoModeloAlmacen() {
            this._Columns.Clear();
            this.Activo().IdCategoria(true).Producto();
            this._Columns.AddRange(new ModeloGridBuilder().Descripcion().Marca().Especificacion().Variante().Largo().Ancho().UnidadXY().Alto().UnidadZ().Minimo().Maximo().ReOrden().Existencia().IdUnidad().NoIdentificacion().Creo().FechaNuevo().Build());
            return this;
        }
    }
}
