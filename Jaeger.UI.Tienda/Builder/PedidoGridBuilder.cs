﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Tienda.Services;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Tienda.Builder {
    public class PedidoGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IPedidoColumnsGridBuilder, IGridViewColumnsBuild, IPedidoGridBuilder,
        IPedidoTempleteGridBuilder {
        public PedidoGridBuilder() : base() { }

        public IPedidoTempleteGridBuilder Templetes() {
            return this;
        }

        public IPedidoTempleteGridBuilder Master(bool values = true) {
            this._Columns.Clear();
            if (values) {
                this.IdPedido().CboStatus().ClienteClave().Cliente().FechaPedido().FechaAcuerdo().FechaEntrega().SubTotal().TotalDescuento().TotalEnvio().TotalTrasladoIVA().GranTotal()
                    .FechaCancela().Cancela().ClaveCancelacion().ColReqFactura().IdOrigen().IdFormaPago().Confirma().FechaConfirma().Recibe().MetodoEnvio().IdVendedor().Nota().Creo().FechaNuevo();
            } else {
                return this.SinImportes();
            }
            return this;
        }

        public IPedidoTempleteGridBuilder SinImportes() {
            this._Columns.Clear();
            this.IdPedido().CboStatus().ClienteClave().Cliente().Recibe().MetodoEnvio().IdVendedor().FechaPedido().FechaAcuerdo().FechaEntrega().FechaCancela()
                  .Cancela().ClaveCancelacion().NotaCancelacion().ColReqFactura().IdOrigen().IdFormaPago().Confirma().FechaConfirma().Nota().Creo().FechaNuevo();
            return this;
        }

        public IPedidoTempleteGridBuilder Cancelacion() {
            this._Columns.Clear();
            CboStatus().FechaCancela().Cancela().ClaveCancelacion().NotaCancelacion();
            return this;
        }

        public IPedidoTempleteGridBuilder Conceptos(bool values = true) {
            this._Columns.Clear();
            if (values) {
                this.Cantidad().Unidad().Descriptor().Unitario().SubTotal().Descuento().Importe().TasaIVA().ColTrasladoIVA().GranTotal().Identificador().FechaNuevo();
            } else {
                return this.ConceptosSinImportes();
            }
            return this;
        }

        public IPedidoTempleteGridBuilder ConceptosSinImportes() {
            this._Columns.Clear();
            this.Cantidad().Unidad().Descriptor().Identificador().FechaNuevo();
            return this;
        }

        public IPedidoTempleteGridBuilder MetodoPago() {
            this._Columns.Clear();
            this.IdPedido().FechaEntrega().Creo();
            return this;
        }

        public IPedidoColumnsGridBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdP",
                DataType = typeof(int),
                HeaderText = "#ID",
                Name = "Id",
                TextAlignment = ContentAlignment.MiddleCenter,
                MaxLength = 5,
                Width = 65
            });
            return this;
        }

        #region pedidos
        public IPedidoColumnsGridBuilder IdPedido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdPedido",
                DataType = typeof(int),
                HeaderText = "# Pedido",
                Name = "IdPedido",
                TextAlignment = ContentAlignment.MiddleCenter,
                MaxLength = 5,
                PinPosition = PinnedColumnPosition.Left,
                Width = 65
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Confirma() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Confirma",
                DataType = typeof(string),
                HeaderText = "Confirma",
                Name = "confirma",
                TextAlignment = ContentAlignment.MiddleCenter,
                MaxLength = 5,
                Width = 65
            });
            return this;
        }

        public IPedidoColumnsGridBuilder FechaConfirma() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FConfirma",
                DataType = typeof(DateTime),
                HeaderText = "Fec.\r\nConfirma",
                Name = "FConfirma",
                TextAlignment = ContentAlignment.MiddleCenter,
                MaxLength = 5,
                Width = 65,
                FormatString = this.FormatStringDate
            });
            return this;
        }

        public IPedidoColumnsGridBuilder CboStatus() {
            var column = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75,
                DataSource = PedidoService.GetStatus(),
                DisplayMember = "Descripcion",
                ValueMember = "Id"
            };
            var c0 = column.ConditionalFormattingObjectList;
            c0.Add(this.CanceladoConditionalFormat());
            this._Columns.Add(column);
            return this;
        }

        public IPedidoColumnsGridBuilder IdOrigen() {
            var origen = new GridViewComboBoxColumn {
                FieldName = "IdOrigen",
                DataType = typeof(int),
                HeaderText = "Origen",
                Name = "IdOrigen",
                Width = 65
            };
            origen.DataSource = PedidoService.GetOrignenes();
            origen.DisplayMember = "Descriptor";
            origen.ValueMember = "Id";
            this._Columns.Add(origen);
            return this;
        }

        public IPedidoColumnsGridBuilder IdFormaPago() {
            var colFormaPago = new GridViewComboBoxColumn {
                FieldName = "IdFormaPago",
                DataType = typeof(int),
                HeaderText = "Forma de\r\nPago",
                Name = "IdFormaPago",
                Width = 95,
                DataSource = PedidoService.GetFormaPagos(),
                DisplayMember = "Descriptor",
                ValueMember = "Id"
            };
            this._Columns.Add(colFormaPago);
            return this;
        }

        public IPedidoColumnsGridBuilder ColCboIdStatus() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder ClienteClave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClienteClave",
                HeaderText = "Clave",
                IsPinned = true,
                Name = "ClienteClave",
                PinPosition = PinnedColumnPosition.Left,
                Width = 65
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Cliente() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cliente",
                HeaderText = "Cliente",
                IsPinned = true,
                Name = "Cliente",
                PinPosition = PinnedColumnPosition.Left,
                Width = 280
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Recibe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Recibe",
                HeaderText = "Recibe",
                IsVisible = false,
                Name = "Recibe",
                Width = 100
            });
            return this;
        }

        public IPedidoColumnsGridBuilder MetodoEnvio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetodoEnvio",
                HeaderText = "Método de envío",
                Name = "MetodoEnvio",
                Width = 150
            });
            return this;
        }

        public IPedidoColumnsGridBuilder IdVendedor() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdVendedor",
                HeaderText = "Vendedor",
                Name = "IdVendedor",
                Width = 85
            });
            return this;
        }

        public IPedidoColumnsGridBuilder FechaPedido() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaPedido",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Pedido",
                Name = "FechaPedido",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IPedidoColumnsGridBuilder FechaAcuerdo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaAcuerdo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Prioridad",
                Name = "FechaAcuerdo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder FechaEntrega() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEntrega",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Entrega",
                Name = "FechaEntrega",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder FechaCancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaStatus",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Cancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cancela",
                HeaderText = "Usu. Cancela",
                IsVisible = false,
                Name = "Cancela",
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder ClaveCancelacion() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "CvMotivo",
                HeaderText = "Clave de cancelación",
                Name = "ClaveCancelacion",
                Width = 150
            });
            return this;
        }

        public IPedidoColumnsGridBuilder NotaCancelacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NotaCancelacion",
                HeaderText = "Cancelación: Observaciones",
                Name = "NotaCancelacion",
                ReadOnly = true,
                Width = 200,
                IsVisible = false
            });
            return this;
        }

        public IPedidoColumnsGridBuilder ColTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = "{0:n}",
                HeaderText = "% IVA",
                Name = "TrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IPedidoColumnsGridBuilder SubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "SubTotal",
                Name = "SubTotal",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder TotalDescuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalDescuento",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Descuento",
                Name = "TotalDescuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder TotalEnvio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalEnvio",
                FormatString = "{0:N2}",
                HeaderText = "C. Envío",
                Name = "TotalEnvio",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder TotalTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalTrasladoIVA",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Tras. IVA",
                Name = "TotalTrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder GranTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IPedidoColumnsGridBuilder ColReqFactura() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "ReqFactura",
                HeaderText = "Facturar",
                Name = "ReqFactura"
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Observaciones",
                Name = "Nota",
                Width = 270
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IPedidoColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }
        #endregion

        #region conceptos
        public IPedidoColumnsGridBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = "{0:N2}",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 75
            });
            return this;
        }
        public IPedidoColumnsGridBuilder Catalogo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Catalogo",
                HeaderText = "Catálogo",
                Name = "Catalogo",
                Width = 100
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Producto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Producto",
                HeaderText = "Producto",
                Name = "Producto",
                Width = 200
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Marca() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Marca",
                HeaderText = "Marca",
                Name = "Marca",
                Width = 150
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Modelo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Modelo",
                HeaderText = "Modelo",
                Name = "Modelo",
                Width = 150
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Especificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Especificacion",
                HeaderText = "Especificación",
                Name = "Especificacion",
                Width = 100
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Tamanio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Tamanio",
                HeaderText = "Variante",
                Name = "Tamanio",
                Width = 115
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Descriptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descriptor",
                HeaderText = "Descripción",
                Name = "Descriptor",
                Width = 510,
                ReadOnly = true
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Unitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Unitario",
                FormatString = "{0:N2}",
                HeaderText = "Unitario",
                Name = "Unitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Descuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:N2}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = "{0:N2}",
                HeaderText = "Importe",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder TasaIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TasaIVA",
                FormatString = "{0:N2}",
                HeaderText = "% IVA",
                Name = "TasaIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IPedidoColumnsGridBuilder Identificador() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Identificador",
                HeaderText = "Identificador",
                Name = "Identificador",
                Width = 115
            });
            return this;
        }
        #endregion

        #region formatos condiciones
        /// <summary>
        /// formato condicional para registros inactivos
        /// </summary>
        public ExpressionFormattingObject CanceladoConditionalFormat() {
            return new ExpressionFormattingObject("Pedido cancelado", "IdStatus = 0", true) {
                RowForeColor = Color.DarkGray
            };
        }
        #endregion
    }
}
