﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Tienda.Builder {
    public class PreguntasFrecuentesGridBuilder : GridViewBuilder, IPreguntasFrecuentesGridBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IPreguntasFrecuentesTempleteGridBuilder,
        IPreguntasFrecuentesColumnsGridBuilder {

        public PreguntasFrecuentesGridBuilder() : base() { }

        public IPreguntasFrecuentesTempleteGridBuilder Templetes() {
            return this;
        }

        public IPreguntasFrecuentesTempleteGridBuilder Master() {
            this._Columns.Clear();
            this.ColPregunta().ColRespuesta().ColFechaNuevo().ColCreo();
            return this;
        }

        public IPreguntasFrecuentesColumnsGridBuilder ColPregunta() {
                this._Columns.Add( new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Pregunta",
                    HeaderText = "Pregunta",
                    Name = "Pregunta",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 385
                });
            return this;
        }

        public IPreguntasFrecuentesColumnsGridBuilder ColRespuesta() {
                this._Columns.Add(new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Respuesta",
                    HeaderText = "Respuesta",
                    Name = "Respuesta",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    AutoSizeMode = BestFitColumnMode.DisplayedDataCells,
                    Width = 385
                });
            return this;
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public IPreguntasFrecuentesColumnsGridBuilder ColId() {
                this._Columns.Add( new GridViewTextBoxColumn {
                    DataType = typeof(DateTime),
                    FieldName = "ColId",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Sist.",
                    Name = "FecNuevo",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter
                });
            return this;
        }

        public IPreguntasFrecuentesColumnsGridBuilder ColCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IPreguntasFrecuentesColumnsGridBuilder ColFechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }
    }
}
