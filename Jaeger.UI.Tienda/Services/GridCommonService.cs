﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Tienda.Services {
    public static class GridCommonService {
        public const string FormatStringMoney = "{0:N2}";
        public const string FormatStringDate = "{0:dd MMM yy}";
        public const string FormatStringP = "{0:P0}";

        /// <summary>
        /// base para columna de importes
        /// </summary>
        public static GridViewTextBoxColumn ColBaseMoney {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FormatString = GridCommonService.FormatStringMoney,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewDateTimeColumn ColBaseDateTime {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FormatString = GridCommonService.FormatStringDate,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// obtner columna de registro activo
        /// </summary>
        public static GridViewCheckBoxColumn ColCheckActivo {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "Activo",
                    HeaderText = "Activo",
                    IsVisible = false,
                    Name = "Activo",
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColFolio {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Folio",
                    HeaderText = "Folio",
                    Name = "Folio",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColSerie {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "Serie",
                    HeaderText = "Serie",
                    Name = "Serie",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColIdPedido {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "IdPedido",
                    DataType = typeof(int),
                    HeaderText = "# Pedido",
                    Name = "IdPedido",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    MaxLength = 5,
                    Width = 65
                };
            }
        }

        public static GridViewDateTimeColumn ColFechaEmision {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaEmision",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Emisión",
                    Name = "FechaEmision",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                };
            }
        }

        /// <summary>
        /// obtener columna de fecha de cancelacion
        /// </summary>
        public static GridViewDateTimeColumn ColFechaCancela {
            get {
                return new GridViewDateTimeColumn {
                    FieldName = "FechaCancela",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Cancela",
                    Name = "FechaCancela",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// obtener clave del usuario que cancela el documento
        /// </summary>
        public static GridViewTextBoxColumn ColCancela {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Cancela",
                    HeaderText = "Cancela",
                    Name = "Cancela",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// combo box para columna de status
        /// </summary>
        public static GridViewComboBoxColumn ColStatus {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(string),
                    FieldName = "IdStatus",
                    HeaderText = "Status",
                    Name = "IdStatus",
                    Width = 75
                };
            }
        }

        /// <summary>
        /// columna combobox de Status
        /// </summary>
        public static GridViewComboBoxColumn ColCboIdStatus {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdStatus",
                    HeaderText = "Status",
                    Name = "IdStatus",
                    Width = 75
                };
            }
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public static GridViewTextBoxColumn ColCreo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Creo",
                    HeaderText = "Creo",
                    Name = "Creo",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public static GridViewTextBoxColumn ColFechaNuevo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaNuevo",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Sist.",
                    Name = "FecNuevo",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter
                };
            }
        }

        /// <summary>
        /// obtener columna de usuario que modifica
        /// </summary>
        public static GridViewTextBoxColumn ColModifica {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Modifica",
                    HeaderText = "Modifica",
                    Name = "Modifica",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    IsVisible = false
                };
            }
        }

        /// <summary>
        /// obtener columna de fecha de modificacion 
        /// </summary>
        public static GridViewTextBoxColumn ColFechaModifica {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "FechaModifica",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Mod.",
                    Name = "FechaModifica",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    IsVisible = false
                };
            }
        }

        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Nota",
                    Name = "Nota",
                    Width = 200,
                    IsVisible = false,
                };
            }
        }

        public static GridViewTextBoxColumn ColCantidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Cantidad",
                    FormatString = "{0:N2}",
                    HeaderText = "Cantidad",
                    Name = "CantidadS",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 65,
                    ReadOnly = false
                };
            }
        }

        public static GridViewTextBoxColumn ColIdentificador {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Identificador",
                    HeaderText = "Identificador",
                    Name = "NoIdentificacion",
                    Width = 100,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColIdDocumento {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "IdDocumento",
                    HeaderText = "IdDocumento",
                    Name = "IdDocumento",
                    Width = 220,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColSubTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "SubTotal",
                    FormatString = "{0:n2}",
                    HeaderText = "SubTotal",
                    Name = "SubTotal",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTotalDescuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalDescuento",
                    FormatString = "{0:n2}",
                    HeaderText = "Descuento",
                    Name = "TotalDescuento",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTotalTrasladoIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalTrasladoIVA",
                    FormatString = "{0:n2}",
                    HeaderText = "Tras. IVA",
                    Name = "TotalTrasladoIVA",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Total",
                    FormatString = "{0:n2}",
                    HeaderText = "Total",
                    Name = "Total",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColGranTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "GTotal",
                    FormatString = "{0:n2}",
                    HeaderText = "Total",
                    Name = "Total",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Clave",
                    HeaderText = "Clave",
                    Name = "Clave",
                    Width = 65,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 110,
                    ReadOnly = true,
                };
            }
        }

        public static GridViewTextBoxColumn ColDomicilioFiscal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "DomicilioFiscalF",
                    HeaderText = "Dom. Fiscal",
                    IsVisible = false,
                    Name = "DomicilioFiscalF",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RFC",
                    HeaderText = "RFC",
                    Name = "RFC",
                    TextImageRelation = TextImageRelation.TextBeforeImage,
                    Width = 90,
                    MaxLength = 15,
                    ColumnCharacterCasing = CharacterCasing.Upper,
                    ReadOnly = true,
                };
            }
        }

        public static GridViewCheckBoxColumn ColValidaRFC {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "ValidaRFC",
                    HeaderText = "Val.",
                    Name = "ValidaRFC",
                    Width = 25
                };
            }
        }

        public static GridViewTextBoxColumn ColCorreo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Correo",
                    HeaderText = "Correo",
                    Name = "Correo",
                    Width = 185,
                    ReadOnly = true,
                };
            }
        }
    }
}
