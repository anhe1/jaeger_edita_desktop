﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.UI.Tienda.Forms {
    public partial class ProductoForm : RadForm {
        protected internal ICatalogoProductosService _Service;
        protected internal ProductoServicioXDetailModel _Producto;
        public List<IClasificacionSingle> _Categorias;
        public event EventHandler<ProductoServicioXDetailModel> Agregate;
        protected bool IsNew = false;

        public void OnAgregate(ProductoServicioXDetailModel e) {
            if (this.Agregate != null)
                this.Agregate(this, e);
        }

        public ProductoForm(ICatalogoProductosService service, ProductoServicioXDetailModel producto) {
            InitializeComponent();
            this._Service = service;
            this._Producto = producto;
            this.IsNew = this._Producto.IdProducto == 0;
        }

        private void ProductoForm_Load(object sender, EventArgs e) {
            this.cboCategorias.DisplayMember = "Descripcion";
            this.cboCategorias.ValueMember = "IdCategoria";
            this.cboCategorias.DataSource = _Categorias;
            this.cboCategorias.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cboCategorias.EditorControl.EnableAlternatingRowColor = true;
            this.cboCategorias.EditorControl.AllowRowResize = false;
            this.cboCategorias.EditorControl.AllowSearchRow = true;
            this.cboCategorias.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.cboCategorias.SelectedIndexChanged += CboCategorias_SelectedIndexChanged;
            this.cboCategorias.DataBindings.Clear();
            this.cboCategorias.DataBindings.Add("SelectedValue", this._Producto, "IdCategoria", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbProducto.DataBindings.Clear();
            this.txbProducto.DataBindings.Add("Text", this._Producto, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void CboCategorias_SelectedIndexChanged(object sender, EventArgs e) {
            var indice = (this.cboCategorias.SelectedItem as GridViewDataRowInfo).DataBoundItem as IClasificacionSingle;
            if (indice.Childs > 0) {
                this.Guardar.Enabled = false;
                this.StatusLabel.Text = "Selección no válida, la categoría no es último nivel.";
            } else {
                this.Guardar.Enabled = true;
                this.StatusLabel.Text = "";
            }
        }

        private void Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Save)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }

            if (this._Producto.IdProducto > 0 && this.IsNew) {
                this.OnAgregate(this._Producto);
                this.Close();
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Save() {
            this._Producto = this._Service.Save(this._Producto);
        }
    }
}
