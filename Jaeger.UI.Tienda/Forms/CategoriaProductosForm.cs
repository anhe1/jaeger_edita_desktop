﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class CategoriaProductosForm : RadForm {
        #region declaraciones
        protected internal ICatalogoProductoService _Service;
        protected internal ICategoriasService _CategoriasService;
        protected internal BindingList<CategoriaProductoModel> _Productos;
        protected internal BindingList<ModeloXDetailModel> _DataSource;
        protected internal BindingList<CategoriaSingle> _Categorias;
        protected internal BindingList<UnidadModel> _Unidades;
        #endregion

        public CategoriaProductosForm() {
            InitializeComponent();
        }

        private void CategoriaProductosForm_Load(object sender, EventArgs e) {
            this.TControl.TBar.Actualizar.Click += this.TControl_Actualizar_Click;
            this.TControl.TBar.Cerrar.Click += TControl_Cerrar_Click;
        }


        protected virtual void TControl_Actualizar_Click(object sender, EventArgs e) {

        }

        private void TControl_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void Consultar() {

        }
    }
}
