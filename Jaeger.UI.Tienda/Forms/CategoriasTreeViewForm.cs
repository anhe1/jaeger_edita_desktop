﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.UI.Almacen.Builder;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Tienda.Forms {
    public partial class CategoriasTreeViewForm : RadForm {
        protected internal List<IClasificacionSingle> _Categorias;
        protected Aplication.Tienda.Contracts.ICatalogoProductosService _Service;
        protected BindingList<ProductoServicioModeloModel> _DataSource;
        private BindingList<ClasificacionProductoModel> _Productos;
        public CategoriasTreeViewForm() {
            InitializeComponent();
        }

        private void CategoriasTreeViewForm_Load(object sender, EventArgs e) {
            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.TControl.GridData.Columns.AddRange(view.Templetes().ProductoModeloAlmacen().Build());
            }
            this.TControl.TBar.Actualizar.Click += TControl_Actualizar_Click;
            this.TControl.TBar.Guardar.Click += TControl_Guardar_Click;
            this.TControl.TBar.Cerrar.Click += TControl_Cerrar_Click;
        }

        protected virtual void TControl_Actualizar_Click(object sender, EventArgs e) {
            var espera = new Common.Forms.Wating4Form(() => {
                this._Categorias = this._Service.GetClasificacion();
                var query = Aplication.Almacen.Services.CatalogoModeloService.Query().ByIdAlmacen(this._Service.Almacen).Activo().Build();
                this._DataSource = new BindingList<ProductoServicioModeloModel>(this._Service.GetList<ProductoServicioModeloModel>(query).ToList());
                this._Productos = new BindingList<ClasificacionProductoModel>(this._Service.GetList<ClasificacionProductoModel>(Aplication.Tienda.Services.CatalogoModeloService.Query().ByIdAlmacen(AlmacenEnum.PT).Activo(true).Build()).ToList());
            }, "Consultando...", false, false);
            espera.ShowDialog(this);
            this.TControl.ValueMember = "IdCategoria";
            this.TControl.IndexChanged += TControl_IndexChanged;
            this.TControl.GridData.DataSource = this._DataSource;
            this.TControl.TreeData.DataSource = this._Categorias;
            this.TControl.TreeData.DisplayMember = "Clase";
            this.TControl.TreeData.ValueMember = "IdCategoria";
            this.TControl.TreeData.ParentMember = "IdSubCategoria";
            this.TControl.TreeData.ChildMember = "IdCategoria";
            this.TControl.TreeData.SelectedNode = this.TControl.TreeData.Nodes[0];
        }

        private void TControl_IndexChanged(object sender, object e) {
            var d0 = e as ModeloXDetailModel;
            d0.Secuencia = this.TControl.NewIndex;
        }
        protected virtual void TControl_Guardar_Click(object sender, EventArgs e) {

        }

        protected virtual void TControl_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
