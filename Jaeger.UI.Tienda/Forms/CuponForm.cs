﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Tienda.Forms {
    public partial class CuponForm : RadForm {
        protected internal ICuponesService service;
        protected internal CuponModel model;
        protected bool IsNew = false;
        public event EventHandler<CuponModel> Agregate;

        public void OnAgregate(CuponModel e) {
            if (this.Agregate != null)
                this.Agregate(this, e);
        }


        public CuponForm(ICuponesService service, CuponModel model) {
            InitializeComponent();
            this.service = service;
            this.model = model;
        }

        private void CuponForm_Load(object sender, EventArgs e) {
            if (model == null) {
                this.model = new CuponModel();
                this.IsNew = true;
            }
            this.CreateBindig();
        }

        protected virtual void CreateBindig() {
            //this.txbPregunta.DataBindings.Clear();
            //this.txbPregunta.DataBindings.Add("Text", this.model, "Pregunta", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.txbRespuesta.DataBindings.Clear();
            //this.txbRespuesta.DataBindings.Add("Text", this.model, "Respuesta", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.chkActivo.DataBindings.Clear();
            //this.chkActivo.DataBindings.Add("Checked", this.model, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        protected virtual void BtnGuadar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Espere un momento...";
                espera.ShowDialog(this);
            }
            if (this.model.IdCupon > 0 && this.IsNew) {
                this.OnAgregate(this.model);
                this.Close();
            }
        }

        protected virtual void BtnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void Guardar() {
            this.model = this.service.Save(this.model);
        }
    }
}
