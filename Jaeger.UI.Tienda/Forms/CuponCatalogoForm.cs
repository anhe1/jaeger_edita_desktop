﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Tienda.Contracts;

namespace Jaeger.UI.Tienda.Forms {
    public partial class CuponCatalogoForm : RadForm {
        protected internal ICuponesService service;
        protected internal BindingList<CuponModel> datos;

        public CuponCatalogoForm() {
            InitializeComponent();
        }

        private void PreguntaFrecuenteCatalogo_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            this.GridData.AllowEditRow = true;
            this.TCupon.Nuevo.Click += this.TCupon_Nuevo_Click;
            this.TCupon.Editar.Click += this.TCupon_Editar_Click;
            this.TCupon.Remover.Click += this.TCupon_Remover_Click;
            this.TCupon.Actualizar.Click += this.TCupon_Actualizar_Click;
            this.TCupon.Filtro.Click += this.TCupon_Filtro_Click;
            this.TCupon.Cerrar.Click += this.TCupon_Cerrar_Click;
        }

        public virtual void TCupon_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new CuponForm(this.service, null);
            nuevo.ShowDialog(this);
        }

        public virtual void TCupon_Editar_Click(object sender, EventArgs eventArgs) {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as CuponModel;
            var nuevo = new CuponForm(this.service, seleccionado);
            nuevo.ShowDialog(this);
        }

        public virtual void TCupon_Remover_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as PreguntaFrecuenteModel;
                if (seleccionado.Id == 0)
                    this.GridData.Rows.Remove(this.GridData.CurrentRow);
                else {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover el elemento seleccionado?", "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                        return;
                    seleccionado.Activo = false;
                }
            }
        }

        public virtual void TCupon_Actualizar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Espere un momento...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.datos;
        }

        public virtual void TCupon_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = !(this.TCupon.Filtro.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        public virtual void TCupon_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            this.datos = this.service.GetList(true);
        }
    }
}
