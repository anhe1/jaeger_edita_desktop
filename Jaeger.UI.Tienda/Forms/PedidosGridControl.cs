﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.UI.Tienda.Builder;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Tienda.Forms {
    public class PedidosGridControl : GridCommonControl {
        #region
        public IPedidosService Service;
        protected internal GridViewTemplate GridConceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal GridViewTemplate GridAutorizacion = new GridViewTemplate() { Caption = "Cambios de Status" };
        public BindingList<PedidoClienteDetailModel> _DataSource;
        public List<Vendedor2DetailModel> Vendedores;
        public PedidoClientePrinter Printer;
        protected internal RadMenuItem ImprimirListado = new RadMenuItem { Text = "Listado" };
        protected internal RadMenuItem ImprimirListadoG = new RadMenuItem { Text = "Listado por semana" };
        protected internal RadMenuItem ImprimirComprobante = new RadMenuItem { Text = "Comprobante" };
        public bool IsCosto = true;
        public delegate void Notify();
        public event Notify Consultar;
        public event Notify Cancel;
        #endregion

        protected virtual void OnConsultar() {
            Consultar?.Invoke();
        }

        protected virtual void OnCancel() {
            Cancel?.Invoke();
        }

        public PedidosGridControl() : base() {
            this.GridAutorizacion.Standard();
            this.GridConceptos.Standard();

            this.GridData.MasterTemplate.Templates.AddRange(this.GridConceptos);
            this.GridData.MasterTemplate.Templates.AddRange(this.GridAutorizacion);
            this.GridData.RowSourceNeeded += this.GridData_RowSourceNeeded;
            
            this.GridConceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridConceptos);
            this.GridAutorizacion.HierarchyDataProvider = new GridViewEventDataProvider(this.GridAutorizacion);

            this.ShowAutosuma = true;
            this.ShowAgrupar = true;

            this.Imprimir.Items.Add(this.ImprimirComprobante);
            this.Imprimir.Items.Add(this.ImprimirListado);
            this.Imprimir.Items.Add(this.ImprimirListadoG);

            this.Actualizar.Click += TPedido_Actualizar_Click;
            this.Cancelar.Click += this.TPedido_Cancelar_Click;

            this.ImprimirListado.Click += this.TPedido_Imprimir_Listado_Click;
            this.ImprimirListadoG.Click += this.TPedido_Imprimir_ListadoG_Click;
            this.ImprimirComprobante.Click += this.TPedido_Imprimir_Comprobante_Click;
        }

        public virtual void TPedido_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.OnConsultar)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._DataSource;
        }

        public virtual void TPedido_Cancelar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as IPedidoClienteDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdStatus == 0) {
                        RadMessageBox.Show(this, Properties.Resources.msg_pedido_cancelado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                    if (RadMessageBox.Show(this, string.Format(Properties.Resources.msg_pedido_cancelar, seleccionado.IdPedido),
                        "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        using (var _espera = new PedidoCancelarForm(seleccionado)) {
                            _espera.Selected += this.TPedido_Cancelar_Selected;
                            _espera.ShowDialog(this);
                        }
                    }
                }
            }
        }

        public virtual void TPedido_Cancelar_Selected(object sender, IPedidoClienteStatusDetailModel e) {
            if (e != null) {
                this.Tag = e;
                using (var espera = new Waiting1Form(this.Cancelando)) {
                    espera.Text = "Solicitando cancelación ...";
                    espera.ShowDialog(this);
                }
                if ((bool)this.Tag == false) {

                } else {
                    var seleccionado = this.GridData.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
                    if (seleccionado != null) {
                        seleccionado.IdStatus = e.IdStatus;
                        seleccionado.FechaStatus = e.FechaStatus;
                        seleccionado.CvMotivo = e.CvMotivo;
                        //seleccionado.NotaCancelacion = e.NotaCancelacion;
                        seleccionado.Cancela = e.Cancela;
                    }
                }
            }
        }

        public virtual void TPedido_Imprimir_Comprobante_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
                if (seleccionado != null) {
                    using (var espera = new Waiting1Form(this.GetPrinter)) {
                        espera.Text = "Consultando partidas ...";
                        espera.ShowDialog(this);
                    }
                    if (this.Printer != null) {
                        var _reporte = new ReporteForm(this.Printer);
                        _reporte.Show();
                    }
                }
            }
        }

        public virtual void TPedido_Imprimir_Listado_Click(object sender, EventArgs e) {
            if (this.GridData.ChildRows.Count > 0) {
                var lista = new List<PedidoClientePrinter>();
                var seleccion = new List<PedidoClienteDetailModel>();
                seleccion.AddRange(this.GridData.ChildRows.Select(x => x.DataBoundItem as PedidoClienteDetailModel));

                foreach (var item in seleccion) {
                    var d0 = new PedidoClientePrinter(item) {
                        IsCosto = this.IsCosto
                    };
                    lista.Add(d0);
                }

                lista = this.Service.GetRemisiones(lista);
                var _reporte = new ReporteForm(lista);
                _reporte.Show();
            }
        }

        public virtual void TPedido_Imprimir_ListadoG_Click(object sender, EventArgs e) {
            if (this.GridData.ChildRows.Count > 0) {
                var lista = new List<PedidoClientePrinter>();
                var seleccion = new List<PedidoClienteDetailModel>();
                seleccion.AddRange(this.GridData.ChildRows.Select(x => x.DataBoundItem as PedidoClienteDetailModel));

                foreach (var item in seleccion) {
                    var d0 = new PedidoClientePrinter(item) {
                        IsCosto = this.IsCosto
                    };
                    lista.Add(d0);
                }

                lista = this.Service.GetRemisiones(lista);

                var lista1 = new List<PedidosReportePrinter>();
                foreach (var item in lista) {
                        var n = new PedidosReportePrinter(item) {
                            Remisiones = item.Remisiones
                        };
                        lista1.Add(n);
                }
                var _reporte = new ReporteForm(lista1);
                _reporte.Show();
            }
        }

        public virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.GridConceptos.Caption) {
                using (var espera = new Waiting1Form(this.GetPedido)) {
                    espera.Text = "Consultando partidas ...";
                    espera.ShowDialog(this);
                }
                var rowData = e.ParentRow.DataBoundItem as PedidoClienteDetailModel;
                foreach (var item in rowData.Partidas) {
                    var row = e.Template.Rows.NewRow();
                    row.Cells["Cantidad"].Value = item.Cantidad;
                    row.Cells["Unidad"].Value = item.Unidad;
                    row.Cells["Descriptor"].Value = item.Descriptor;
                    if (this.IsCosto) {
                        row.Cells["Unitario"].Value = item.Unitario;
                        row.Cells["SubTotal"].Value = item.SubTotal;
                        row.Cells["Descuento"].Value = item.Descuento;
                        row.Cells["Importe"].Value = item.Importe;
                        row.Cells["TasaIVA"].Value = item.TasaIVA;
                        row.Cells["Importe"].Value = item.SubTotal;
                        row.Cells["TrasladoIVA"].Value = item.TrasladoIVA;
                        row.Cells["Total"].Value = item.Total;
                    }
                    row.Cells["Identificador"].Value = item.NoIdentificacion;
                    row.Cells["FechaNuevo"].Value = item.FechaNuevo;
                    e.SourceCollection.Add(row);
                }
            } else if (e.Template.Caption == this.GridAutorizacion.Caption) {
                var rowData = e.ParentRow.DataBoundItem as PedidoClienteDetailModel;
                if (rowData.Autorizaciones != null) {
                    foreach (var item in rowData.Autorizaciones) {
                        var row = e.Template.Rows.NewRow();
                        row.Cells["IdStatus"].Value = item.IdStatus;
                        row.Cells["Cancela"].Value = item.Cancela;
                        row.Cells["FechaCancela"].Value = item.FechaStatus;
                        row.Cells["ClaveCancelacion"].Value = item.CvMotivo;
                        row.Cells["NotaCancelacion"].Value = item.Nota;
                        e.SourceCollection.Add(row);
                    }
                }
            } 
        }

        public virtual void GetPedido() {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
            if (seleccionado != null) {
                var pedido = this.Service.GetPedido(seleccionado.IdPedido);
                if (pedido != null) {
                    seleccionado.Autorizaciones = pedido.Autorizaciones;
                    seleccionado.Partidas = pedido.Partidas;
                }
            }
        }

        public virtual void Cancelando() {
            if (this.Tag != null) {
                var autorizacion = (IPedidoClienteStatusDetailModel)this.Tag;
                if (autorizacion != null) {
                    this.Tag = this.Service.Cancelar(autorizacion);
                }
            }
        }

        public virtual void GetPrinter() {
            var _seleccionado = this.GridData.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
            if (_seleccionado != null) {
                this.Printer = this.Service.GetPrinter(_seleccionado.IdPedido);
                this.Printer.IsCosto = this.IsCosto;
            } else {
                this.Printer = null;
            }
        }

        public virtual void CreateView() {
            using (IPedidoGridBuilder buider = new PedidoGridBuilder()) {
                this.GridData.Columns.AddRange(buider.Templetes().Master(IsCosto).Build());
                this.GridConceptos.Columns.AddRange(buider.Templetes().Conceptos(IsCosto).Build());
                this.GridAutorizacion.Columns.AddRange(buider.Templetes().Cancelacion().Build());
            }
        }
    }
}
