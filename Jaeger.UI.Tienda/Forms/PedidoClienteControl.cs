﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Contribuyentes.Entities;
using System.Linq;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PedidoClienteControl : UserControl {
        public Aplication.Contribuyentes.Contracts.IContribuyenteService service;
        private List<ContribuyenteDetailModel> contribuyenteDetailModels;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public event EventHandler<EventArgs> ButtonCerrar_Click;

        public void OnButtonCerrarClick(object sender, EventArgs e) {
            if (this.ButtonCerrar_Click != null) {
                this.ButtonCerrar_Click(sender, e);
            }
        }

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public void CreateService() {
            this.Cliente.Enabled = false;
            this.backClientes.RunWorkerAsync();
        }

        public PedidoClienteControl() {
            InitializeComponent();
        }

        private void PedidoClienteControl_Load(object sender, EventArgs e) {
            this.Cliente.DisplayMember = "Nombre";
            this.Cliente.ValueMember = "IdDirectorio";

            this.Embarque.DisplayMember = "Completo";
            this.Embarque.ValueMember = "Id";

            this.Vendedor.DisplayMember = "ClaveVendedor";
            this.Vendedor.ValueMember = "IdVendedor";

            this.Status.DisplayMember = "Descripcion";
            this.Status.ValueMember = "Id";
        }

        private void BackClientes_DoWork(object sender, DoWorkEventArgs e) {
            if (this.service == null) {
                this.contribuyenteDetailModels = new List<ContribuyenteDetailModel>();
                return;
            }

            var query = new Domain.Contribuyentes.Builder.ContribuyenteQueryBuilder();
            query.WithRelacionComercial(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
            this.contribuyenteDetailModels = this.service.GetList<ContribuyenteDetailModel>(query.Build()).ToList();
        }

        private void BackClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Cliente.DataSource = this.contribuyenteDetailModels;
            this.Cliente.Enabled = true;
            this.OnBindingClompleted(e);
        }

        private void CboPersona_SelectedIndexChanged(object sender, EventArgs e) {
            var t = this.Cliente.SelectedItem as GridViewRowInfo;
            if (t != null) {
                var s = t.DataBoundItem as ContribuyenteDetailModel;
                if (s != null) {
                    this.Embarque.DataSource = s.Domicilios;
                    this.Vendedor.DataSource = s.Vendedores;
                    this.Clave.Text = s.Clave;
                }
            }
        }

        public virtual void Buscar_Click(object sender, EventArgs e) {
            //var _buscar = new Contribuyente.ClienteBuscarForm();
            //_buscar.Selected += Buscar_Selected;
            //_buscar.ShowDialog(this);
        }

        private void Buscar_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                this.Cliente.Text = e.Nombre;
            }
        }
    }
}
