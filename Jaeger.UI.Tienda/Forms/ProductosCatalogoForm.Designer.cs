﻿namespace Jaeger.UI.Tienda.Forms {
    partial class ProductosCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductosCatalogoForm));
            this.GProducto = new Telerik.WinControls.UI.RadGridView();
            this.TModelos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.GModelos = new Telerik.WinControls.UI.RadGridView();
            this.gImagen = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.PDisponible = new Telerik.WinControls.UI.RadPageViewPage();
            this.gVariante = new Telerik.WinControls.UI.RadGridView();
            this.TDisponible = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PImagen = new Telerik.WinControls.UI.RadPageViewPage();
            this.TImagen = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.contextMenuImagen = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ToolBarButtonAgregaImagen = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonRemoverImagen = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonReemplazarImagen = new Telerik.WinControls.UI.RadMenuItem();
            this.TProductos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.GProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GProducto.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.PDisponible.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).BeginInit();
            this.PImagen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GProducto
            // 
            this.GProducto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GProducto.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GProducto.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GProducto.Name = "GProducto";
            this.GProducto.Size = new System.Drawing.Size(1091, 302);
            this.GProducto.TabIndex = 1;
            this.GProducto.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_CellBeginEdit);
            this.GProducto.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_CellEndEdit);
            this.GProducto.RowsChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.RadGridView_RowsChanging);
            // 
            // TModelos
            // 
            this.TModelos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TModelos.Etiqueta = "Modelos";
            this.TModelos.Location = new System.Drawing.Point(0, 0);
            this.TModelos.Name = "TModelos";
            this.TModelos.ReadOnly = false;
            this.TModelos.ShowActualizar = false;
            this.TModelos.ShowAutorizar = true;
            this.TModelos.ShowCerrar = false;
            this.TModelos.ShowEditar = true;
            this.TModelos.ShowExportarExcel = false;
            this.TModelos.ShowFiltro = true;
            this.TModelos.ShowGuardar = false;
            this.TModelos.ShowHerramientas = false;
            this.TModelos.ShowImagen = false;
            this.TModelos.ShowImprimir = false;
            this.TModelos.ShowNuevo = true;
            this.TModelos.ShowRemover = true;
            this.TModelos.Size = new System.Drawing.Size(745, 30);
            this.TModelos.TabIndex = 0;
            // 
            // GModelos
            // 
            this.GModelos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GModelos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GModelos.MasterTemplate.MultiSelect = true;
            this.GModelos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GModelos.Name = "GModelos";
            this.GModelos.Size = new System.Drawing.Size(745, 273);
            this.GModelos.TabIndex = 2;
            this.GModelos.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_CellBeginEdit);
            this.GModelos.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_CellEndEdit);
            this.GModelos.RowsChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.RadGridView_RowsChanging);
            this.GModelos.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.RadGridModelos_CommandCellClick);
            // 
            // gImagen
            // 
            this.gImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gImagen.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gImagen.MasterTemplate.AllowAddNewRow = false;
            this.gImagen.MasterTemplate.AutoGenerateColumns = false;
            gridViewImageColumn1.FieldName = "Imagen";
            gridViewImageColumn1.HeaderText = "Imagen";
            gridViewImageColumn1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            gridViewImageColumn1.Name = "column1";
            gridViewImageColumn1.Width = 300;
            this.gImagen.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1});
            this.gImagen.MasterTemplate.ShowFilteringRow = false;
            this.gImagen.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gImagen.Name = "gImagen";
            this.gImagen.ShowGroupPanel = false;
            this.gImagen.Size = new System.Drawing.Size(460, 225);
            this.gImagen.TabIndex = 0;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1091, 609);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.GProducto);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1091, 302);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 306);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1091, 303);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1091, 303);
            this.radSplitContainer2.TabIndex = 3;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radSplitContainer3);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(1091, 303);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2201414F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(311, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel4);
            this.radSplitContainer3.Controls.Add(this.splitPanel5);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(1091, 303);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.GModelos);
            this.splitPanel4.Controls.Add(this.TModelos);
            this.splitPanel4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(745, 303);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.1853726F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(201, 0);
            this.splitPanel4.TabIndex = 0;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.radPageView1);
            this.splitPanel5.Location = new System.Drawing.Point(749, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(342, 303);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.1853726F, 0F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(-201, 0);
            this.splitPanel5.TabIndex = 1;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.PDisponible);
            this.radPageView1.Controls.Add(this.PImagen);
            this.radPageView1.DefaultPage = this.PDisponible;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.PDisponible;
            this.radPageView1.Size = new System.Drawing.Size(342, 303);
            this.radPageView1.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PDisponible
            // 
            this.PDisponible.Controls.Add(this.gVariante);
            this.PDisponible.Controls.Add(this.TDisponible);
            this.PDisponible.ItemSize = new System.Drawing.SizeF(74F, 28F);
            this.PDisponible.Location = new System.Drawing.Point(10, 37);
            this.PDisponible.Name = "PDisponible";
            this.PDisponible.Size = new System.Drawing.Size(321, 255);
            this.PDisponible.Text = "Disponibles";
            // 
            // gVariante
            // 
            this.gVariante.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnF2;
            this.gVariante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gVariante.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gVariante.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn1.DataType = typeof(int);
            gridViewComboBoxColumn1.FieldName = "IdEspecificacion";
            gridViewComboBoxColumn1.HeaderText = "Variante";
            gridViewComboBoxColumn1.Name = "IdEspecificacion";
            gridViewComboBoxColumn1.Width = 185;
            gridViewComboBoxColumn2.FieldName = "IdVisibilidad";
            gridViewComboBoxColumn2.HeaderText = "Visibilidad";
            gridViewComboBoxColumn2.Name = "IdVisibilidad";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Minimo";
            gridViewTextBoxColumn1.FormatString = "{0:N2}";
            gridViewTextBoxColumn1.HeaderText = "Minimo";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Minimo";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn1.Width = 65;
            gridViewTextBoxColumn2.DataType = typeof(decimal);
            gridViewTextBoxColumn2.FieldName = "Maximo";
            gridViewTextBoxColumn2.FormatString = "{0:N2}";
            gridViewTextBoxColumn2.HeaderText = "Maximo";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Maximo";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.Width = 65;
            gridViewTextBoxColumn3.DataType = typeof(decimal);
            gridViewTextBoxColumn3.FieldName = "ReOrden";
            gridViewTextBoxColumn3.FormatString = "{0:N2}";
            gridViewTextBoxColumn3.HeaderText = "ReOrden";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "ReOrden";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn3.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.Width = 65;
            gridViewTextBoxColumn4.FieldName = "Modifica";
            gridViewTextBoxColumn4.HeaderText = "Modifica";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "Modifica";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.VisibleInColumnChooser = false;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn5.FieldName = "FechaModifica";
            gridViewTextBoxColumn5.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn5.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "FechaModifica";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.VisibleInColumnChooser = false;
            gridViewTextBoxColumn5.Width = 65;
            this.gVariante.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.gVariante.MasterTemplate.EnableFiltering = true;
            this.gVariante.MasterTemplate.ShowFilteringRow = false;
            this.gVariante.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.gVariante.Name = "gVariante";
            this.gVariante.ShowGroupPanel = false;
            this.gVariante.Size = new System.Drawing.Size(321, 225);
            this.gVariante.TabIndex = 5;
            this.gVariante.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_CellBeginEdit);
            this.gVariante.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_CellEndEdit);
            this.gVariante.RowsChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.RadGridView_RowsChanging);
            // 
            // TDisponible
            // 
            this.TDisponible.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDisponible.Etiqueta = "Disponibilidad";
            this.TDisponible.Location = new System.Drawing.Point(0, 0);
            this.TDisponible.Name = "TDisponible";
            this.TDisponible.ReadOnly = false;
            this.TDisponible.ShowActualizar = false;
            this.TDisponible.ShowAutorizar = false;
            this.TDisponible.ShowCerrar = false;
            this.TDisponible.ShowEditar = false;
            this.TDisponible.ShowExportarExcel = false;
            this.TDisponible.ShowFiltro = true;
            this.TDisponible.ShowGuardar = false;
            this.TDisponible.ShowHerramientas = false;
            this.TDisponible.ShowImagen = false;
            this.TDisponible.ShowImprimir = false;
            this.TDisponible.ShowNuevo = true;
            this.TDisponible.ShowRemover = true;
            this.TDisponible.Size = new System.Drawing.Size(321, 30);
            this.TDisponible.TabIndex = 6;
            // 
            // PImagen
            // 
            this.PImagen.Controls.Add(this.gImagen);
            this.PImagen.Controls.Add(this.TImagen);
            this.PImagen.ItemSize = new System.Drawing.SizeF(64F, 28F);
            this.PImagen.Location = new System.Drawing.Point(10, 37);
            this.PImagen.Name = "PImagen";
            this.PImagen.Size = new System.Drawing.Size(460, 255);
            this.PImagen.Text = "Imagenes";
            // 
            // TImagen
            // 
            this.TImagen.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImagen.Etiqueta = "";
            this.TImagen.Location = new System.Drawing.Point(0, 0);
            this.TImagen.Name = "TImagen";
            this.TImagen.ReadOnly = false;
            this.TImagen.ShowActualizar = false;
            this.TImagen.ShowAutorizar = false;
            this.TImagen.ShowCerrar = false;
            this.TImagen.ShowEditar = false;
            this.TImagen.ShowExportarExcel = false;
            this.TImagen.ShowFiltro = true;
            this.TImagen.ShowGuardar = false;
            this.TImagen.ShowHerramientas = false;
            this.TImagen.ShowImagen = false;
            this.TImagen.ShowImprimir = false;
            this.TImagen.ShowNuevo = true;
            this.TImagen.ShowRemover = true;
            this.TImagen.Size = new System.Drawing.Size(460, 30);
            this.TImagen.TabIndex = 7;
            // 
            // contextMenuImagen
            // 
            this.contextMenuImagen.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonAgregaImagen,
            this.ToolBarButtonRemoverImagen,
            this.ToolBarButtonReemplazarImagen});
            // 
            // ToolBarButtonAgregaImagen
            // 
            this.ToolBarButtonAgregaImagen.Name = "ToolBarButtonAgregaImagen";
            this.ToolBarButtonAgregaImagen.Text = "Agregar imagen";
            this.ToolBarButtonAgregaImagen.Click += new System.EventHandler(this.TModelos_Imagen_Click);
            // 
            // ToolBarButtonRemoverImagen
            // 
            this.ToolBarButtonRemoverImagen.Name = "ToolBarButtonRemoverImagen";
            this.ToolBarButtonRemoverImagen.Text = "Remover";
            // 
            // ToolBarButtonReemplazarImagen
            // 
            this.ToolBarButtonReemplazarImagen.Name = "ToolBarButtonReemplazarImagen";
            this.ToolBarButtonReemplazarImagen.Text = "Reemplazar";
            this.ToolBarButtonReemplazarImagen.Click += new System.EventHandler(this.TModelos_Imagen_Click);
            // 
            // TProductos
            // 
            this.TProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProductos.Etiqueta = "";
            this.TProductos.Location = new System.Drawing.Point(0, 0);
            this.TProductos.Name = "TProductos";
            this.TProductos.ReadOnly = false;
            this.TProductos.ShowActualizar = true;
            this.TProductos.ShowAutorizar = false;
            this.TProductos.ShowCerrar = true;
            this.TProductos.ShowEditar = true;
            this.TProductos.ShowExportarExcel = false;
            this.TProductos.ShowFiltro = true;
            this.TProductos.ShowGuardar = false;
            this.TProductos.ShowHerramientas = true;
            this.TProductos.ShowImagen = false;
            this.TProductos.ShowImprimir = false;
            this.TProductos.ShowNuevo = true;
            this.TProductos.ShowRemover = true;
            this.TProductos.Size = new System.Drawing.Size(1091, 30);
            this.TProductos.TabIndex = 3;
            // 
            // ProductosCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 639);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TProductos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductosCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Catálogo de Productos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GProducto.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GModelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.PDisponible.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).EndInit();
            this.PImagen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        protected internal Telerik.WinControls.UI.RadGridView gImagen;
        private Telerik.WinControls.UI.RadContextMenu contextMenuImagen;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonAgregaImagen;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonRemoverImagen;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonReemplazarImagen;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage PDisponible;
        private Telerik.WinControls.UI.RadPageViewPage PImagen;
        protected internal Telerik.WinControls.UI.RadGridView gVariante;
        protected internal Common.Forms.ToolBarStandarControl TImagen;
        protected internal Common.Forms.ToolBarStandarControl TModelos;
        protected internal Common.Forms.ToolBarStandarControl TDisponible;
        protected internal Telerik.WinControls.UI.RadGridView GProducto;
        protected internal Telerik.WinControls.UI.RadGridView GModelos;
        protected internal Common.Forms.ToolBarStandarControl TProductos;
    }
}