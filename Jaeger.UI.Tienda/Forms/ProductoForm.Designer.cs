﻿namespace Jaeger.UI.Tienda.Forms {
    partial class ProductoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoForm));
            this.panelProducto = new Telerik.WinControls.UI.RadPanel();
            this.txbProducto = new Telerik.WinControls.UI.RadTextBox();
            this.lblProducto = new Telerik.WinControls.UI.RadLabel();
            this.cboCategorias = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCategoria = new Telerik.WinControls.UI.RadLabel();
            this.Guardar = new Telerik.WinControls.UI.RadButton();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.TEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.StatusLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.HeaderLabel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).BeginInit();
            this.panelProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panelProducto
            // 
            this.panelProducto.Controls.Add(this.txbProducto);
            this.panelProducto.Controls.Add(this.lblProducto);
            this.panelProducto.Controls.Add(this.cboCategorias);
            this.panelProducto.Controls.Add(this.lblCategoria);
            this.panelProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelProducto.Location = new System.Drawing.Point(0, 50);
            this.panelProducto.Name = "panelProducto";
            this.panelProducto.Size = new System.Drawing.Size(477, 113);
            this.panelProducto.TabIndex = 2;
            // 
            // txbProducto
            // 
            this.txbProducto.Location = new System.Drawing.Point(12, 80);
            this.txbProducto.Name = "txbProducto";
            this.txbProducto.NullText = "Producto";
            this.txbProducto.Size = new System.Drawing.Size(445, 20);
            this.txbProducto.TabIndex = 7;
            // 
            // lblProducto
            // 
            this.lblProducto.Location = new System.Drawing.Point(12, 56);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(54, 18);
            this.lblProducto.TabIndex = 6;
            this.lblProducto.Text = "Producto:";
            // 
            // cboCategorias
            // 
            this.cboCategorias.AutoSizeDropDownToBestFit = true;
            // 
            // cboCategorias.NestedRadGridView
            // 
            this.cboCategorias.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboCategorias.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategorias.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboCategorias.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboCategorias.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdCategoria";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdCategoria";
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "SubId";
            gridViewTextBoxColumn2.HeaderText = "SubId";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "SubId";
            gridViewTextBoxColumn3.FieldName = "Codigo";
            gridViewTextBoxColumn3.HeaderText = "Código";
            gridViewTextBoxColumn3.Name = "Codigo";
            gridViewTextBoxColumn3.Width = 85;
            gridViewTextBoxColumn4.FieldName = "Clase";
            gridViewTextBoxColumn4.HeaderText = "Clase";
            gridViewTextBoxColumn4.Name = "Clase";
            gridViewTextBoxColumn4.Width = 90;
            gridViewTextBoxColumn5.FieldName = "Descripcion";
            gridViewTextBoxColumn5.HeaderText = "Descripción";
            gridViewTextBoxColumn5.Name = "Descripcion";
            this.cboCategorias.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.cboCategorias.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboCategorias.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cboCategorias.EditorControl.Name = "NestedRadGridView";
            this.cboCategorias.EditorControl.ReadOnly = true;
            this.cboCategorias.EditorControl.ShowGroupPanel = false;
            this.cboCategorias.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboCategorias.EditorControl.TabIndex = 0;
            this.cboCategorias.Location = new System.Drawing.Point(12, 30);
            this.cboCategorias.Name = "cboCategorias";
            this.cboCategorias.NullText = "Categoría";
            this.cboCategorias.Size = new System.Drawing.Size(445, 20);
            this.cboCategorias.TabIndex = 3;
            this.cboCategorias.TabStop = false;
            // 
            // lblCategoria
            // 
            this.lblCategoria.Location = new System.Drawing.Point(12, 6);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(57, 18);
            this.lblCategoria.TabIndex = 2;
            this.lblCategoria.Text = "Categoría:";
            // 
            // Guardar
            // 
            this.Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Guardar.Location = new System.Drawing.Point(309, 178);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(75, 23);
            this.Guardar.TabIndex = 3;
            this.Guardar.Text = "Guardar";
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.Location = new System.Drawing.Point(390, 178);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 4;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // TEstado
            // 
            this.TEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.StatusLabel});
            this.TEstado.Location = new System.Drawing.Point(0, 208);
            this.TEstado.Name = "TEstado";
            this.TEstado.Size = new System.Drawing.Size(477, 26);
            this.TEstado.SizingGrip = false;
            this.TEstado.TabIndex = 45;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.TEstado.SetSpring(this.StatusLabel, false);
            this.StatusLabel.Text = "";
            this.StatusLabel.TextWrap = true;
            this.StatusLabel.UseCompatibleTextRendering = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(477, 50);
            this.pictureBox1.TabIndex = 46;
            this.pictureBox1.TabStop = false;
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.BackColor = System.Drawing.Color.White;
            this.HeaderLabel.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Bold);
            this.HeaderLabel.Location = new System.Drawing.Point(12, 12);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(67, 20);
            this.HeaderLabel.TabIndex = 2;
            this.HeaderLabel.Text = "Productos";
            // 
            // ProductoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 234);
            this.Controls.Add(this.TEstado);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.HeaderLabel);
            this.Controls.Add(this.panelProducto);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Producto";
            this.Load += new System.EventHandler(this.ProductoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).EndInit();
            this.panelProducto.ResumeLayout(false);
            this.panelProducto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel panelProducto;
        protected internal Telerik.WinControls.UI.RadTextBox txbProducto;
        private Telerik.WinControls.UI.RadLabel lblProducto;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox cboCategorias;
        private Telerik.WinControls.UI.RadLabel lblCategoria;
        private Telerik.WinControls.UI.RadButton Guardar;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadStatusStrip TEstado;
        private Telerik.WinControls.UI.RadLabelElement StatusLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel HeaderLabel;
    }
}