﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Aplication.Tienda.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PrecioCatalogoForm : RadForm {
        protected IPrecioCatalogoService service;
        private BindingList<PrecioDetailModel> catalogoPrecios;
        private List<EspecificacionModel> tamanios;
        private readonly RadMenuItem _registroInactivo = new RadMenuItem { Text = "Mostrar registros inactivos", CheckOnClick = true };
        private readonly RadMenuItem _exportar = new RadMenuItem { Text = "Exportar", CheckOnClick = true };
        private Domain.Base.ValueObjects.UIAction _permisos;

        public PrecioCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void PrecioCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new PrecioCatalogoService();
            this.dataGridCatalogo.Standard();
            this.dataGridPartidas.Standard();
            this.dataGridPartidas.AllowEditRow = false;

            this.TCatalogo.Herramientas.Items.Add(_exportar);
            this.TCatalogo.Herramientas.Items.Add(_registroInactivo);
            this.TCatalogo.Nuevo.Enabled = this._permisos.Agregar;
            this.TCatalogo.Remover.Enabled = this._permisos.Remover;
            this.TCatalogo.Editar.Enabled = this._permisos.Editar;
            this.TPartida.Nuevo.Enabled = this._permisos.Agregar;
            this.TPartida.Remover.Enabled = this._permisos.Remover;

            _exportar.Click += _exportar_Click;
        }

        private void _exportar_Click(object sender, EventArgs e) {
            var _nuevo = new Common.Forms.TelerikGridExportForm(this.dataGridPartidas);
            _nuevo.ShowDialog(this);
        }

        #region barra de herramientas catalogo
        private void TCatalogo_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new PrecioForm(this.service, null);
            _nuevo.ShowDialog(this);
        }

        private void TCatalogo_Editar_Click(object sender, EventArgs e) {
            if (this.dataGridCatalogo.CurrentRow != null) {
                var _seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as PrecioDetailModel;
                if (_seleccionado != null) { 
                    if (_seleccionado.Activo == true) {
                        var _editar = new PrecioForm(this.service, _seleccionado);
                        _editar.ShowDialog(this);
                    } else {
                        RadMessageBox.Show(this, Properties.Resources.msg_Catalogo_Precio_NoEditar, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    }
                }
            }
        }

        private void TCatalogo_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridCatalogo.CurrentRow != null) {
                var seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as PrecioDetailModel;
                if (seleccionado != null) {
                    if (RadMessageBox.Show(this, Properties.Resources.msg_Catalogo_Precio_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        using (var _espera = new Waiting1Form(this.RemoverCatalogo)) {
                            _espera.Text = "Espere un momento...";
                            _espera.ShowDialog(this);
                        }
                        if (this.Tag != null) {
                            bool _eliminado = (bool)this.Tag;
                            if (_eliminado) {
                                this.dataGridCatalogo.Rows.Remove(this.dataGridCatalogo.CurrentRow);
                            }
                        }
                    }
                }
            }
            this.Tag = null;
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            var IdPrioridad = this.dataGridCatalogo.Columns["IdPrioridad"] as GridViewComboBoxColumn;
            IdPrioridad.DataSource = this.service.GetPrioridad();
            IdPrioridad.DisplayMember = "Descripcion";
            IdPrioridad.ValueMember = "Id";

            var IdTamanio = this.dataGridPartidas.Columns["IdEspecificacion"] as GridViewComboBoxColumn;
            IdTamanio.DataSource = this.tamanios;
            IdTamanio.DisplayMember = "Descripcion";
            IdTamanio.ValueMember = "IdEspecificacion";

            this.dataGridCatalogo.DataSource = this.catalogoPrecios;
            this.dataGridPartidas.DataSource = this.catalogoPrecios;
            this.dataGridPartidas.DataMember = "Items";
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void DataGridCatalogo_SelectionChanged(object sender, EventArgs e) {
            if (this.dataGridCatalogo.CurrentRow != null) {
                var seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as PrecioDetailModel;
                if (seleccionado != null) {
                    this.TCatalogo.ShowGuardar = seleccionado.IdPrecio == 0;
                }
            }
        }
        #endregion

        #region barra de herramientas partidas
        private void TPartida_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridPartidas.CurrentRow != null) {
                var _seleccionado = this.dataGridPartidas.CurrentRow.DataBoundItem as PrecioPartidaModel;
                if (_seleccionado != null) {
                    if (RadMessageBox.Show(this, Properties.Resources.msg_Catalogo_Precio_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        if (_seleccionado.IdPrecio == 0) {
                            this.dataGridPartidas.Rows.Remove(this.dataGridPartidas.CurrentRow);
                        } else {
                            using(var _espera = new Waiting1Form(this.RemoverPartida)) {
                                _espera.Text = "Espere un momento...";
                                _espera.ShowDialog(this);
                            }
                        }
                        if (this.Tag != null) {
                            bool _eliminado = (bool)this.Tag;
                            if (_eliminado) {
                                this.dataGridPartidas.Rows.Remove(this.dataGridPartidas.CurrentRow);
                            }
                        }
                    }
                }
            }
            this.Tag = null;
        }

        private void TPartida_Nuevo_Click(object sender, EventArgs e) {
            if (this.dataGridCatalogo.CurrentRow != null) {
                var _selccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as PrecioDetailModel;
                if (_selccionado != null) {
                    var _nuevo = new PrecioPartidaModel() { IdPrecio = _selccionado.IdPrecio };
                    _selccionado.Items.Add(_nuevo);
                }
            }
        }

        private void TPartida_Guardar_Click(object sender, EventArgs e) {
            if (this.dataGridPartidas.CurrentRow != null) {
                var _selccionado = this.dataGridPartidas.CurrentRow.DataBoundItem as PrecioPartidaModel;
             //   this.service.Save(_selccionado);
            }
        }

        private void DataGridPartidas_SelectionChanged(object sender, EventArgs e) {
            if (this.dataGridPartidas.CurrentRow != null) {
                var seleccionado = this.dataGridPartidas.CurrentRow.DataBoundItem as PrecioPartidaModel;
                if (seleccionado != null) {
                    this.TPartida.ShowGuardar = seleccionado.IdPrecio == 0;
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.catalogoPrecios = this.service.GetList(!this._registroInactivo.IsChecked);
            this.tamanios = this.service.GetTamanios();
        }

        private void RemoverCatalogo() {
            var seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as PrecioDetailModel;
            if (seleccionado != null) {
                //this.Tag = this.service.Remover(seleccionado);
            }
        }

        private void RemoverPartida() {
            var seleccionado = this.dataGridPartidas.CurrentRow.DataBoundItem as PrecioPartidaModel;
            if (seleccionado != null) {
                //this.Tag = this.service.Remover(seleccionado);
            }
        }

        private void DataGridPartidas_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            if (e.Action == Telerik.WinControls.Data.NotifyCollectionChangedAction.ItemChanged) {

            Console.WriteLine(e.Action.ToString());
            }
        }
        #endregion
    }
}
