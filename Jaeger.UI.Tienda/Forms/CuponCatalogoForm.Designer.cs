﻿namespace Jaeger.UI.Tienda.Forms {
    partial class CuponCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn1 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuponCatalogoForm));
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.TCupon = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.Width = 220;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 220;
            gridViewCalculatorColumn1.FieldName = "Valor";
            gridViewCalculatorColumn1.HeaderText = "Valor";
            gridViewCalculatorColumn1.Name = "Valor";
            gridViewCalculatorColumn1.Width = 75;
            gridViewDateTimeColumn1.FieldName = "FechaInicio";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Vig. Desde";
            gridViewDateTimeColumn1.Name = "FechaInicio";
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 75;
            gridViewDateTimeColumn2.FieldName = "FechaFin";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn2.HeaderText = "Vig. Hasta";
            gridViewDateTimeColumn2.Name = "FechaFin";
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 75;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.FieldName = "FechaNuevo";
            gridViewTextBoxColumn3.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn3.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn3.Name = "FechaNuevo";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "FechaModifica";
            gridViewTextBoxColumn4.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn4.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "FechaModifica";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCalculatorColumn1,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(956, 582);
            this.GridData.TabIndex = 1;
            // 
            // TCupon
            // 
            this.TCupon.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCupon.Etiqueta = "";
            this.TCupon.Location = new System.Drawing.Point(0, 0);
            this.TCupon.Name = "TCupon";
            this.TCupon.ReadOnly = false;
            this.TCupon.ShowActualizar = true;
            this.TCupon.ShowAutorizar = false;
            this.TCupon.ShowCerrar = true;
            this.TCupon.ShowEditar = true;
            this.TCupon.ShowExportarExcel = false;
            this.TCupon.ShowFiltro = true;
            this.TCupon.ShowGuardar = true;
            this.TCupon.ShowHerramientas = false;
            this.TCupon.ShowImagen = false;
            this.TCupon.ShowImprimir = false;
            this.TCupon.ShowNuevo = true;
            this.TCupon.ShowRemover = true;
            this.TCupon.Size = new System.Drawing.Size(956, 30);
            this.TCupon.TabIndex = 2;
            // 
            // CuponCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 612);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TCupon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CuponCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Tienda Web: Cupos de Descuento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PreguntaFrecuenteCatalogo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Telerik.WinControls.UI.RadGridView GridData;
        protected internal Common.Forms.ToolBarStandarControl TCupon;
    }
}