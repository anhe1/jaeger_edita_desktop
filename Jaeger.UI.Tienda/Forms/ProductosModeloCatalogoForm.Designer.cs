﻿namespace Jaeger.UI.Tienda.Forms
{
    partial class ProductosModeloCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn1 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductosModeloCatalogoForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.gModelos = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gVariante = new Telerik.WinControls.UI.RadGridView();
            this.TDisponible = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gImagen = new Telerik.WinControls.UI.RadGridView();
            this.TImagen = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TModelo = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gModelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gModelos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1115, 595);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.gModelos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(757, 595);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.1813681F, 0F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(201, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // gModelos
            // 
            this.gModelos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gModelos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdProducto";
            gridViewTextBoxColumn1.HeaderText = "IdProducto";
            gridViewTextBoxColumn1.Name = "IdProducto2";
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = false";
            expressionFormattingObject1.Name = "NewCondition";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn1.Width = 40;
            gridViewComboBoxColumn1.DataType = typeof(int);
            gridViewComboBoxColumn1.FieldName = "IdProducto";
            gridViewComboBoxColumn1.HeaderText = "Producto";
            gridViewComboBoxColumn1.Name = "IdProducto";
            gridViewComboBoxColumn1.Width = 250;
            gridViewComboBoxColumn2.FieldName = "Tipo";
            gridViewComboBoxColumn2.HeaderText = "Tipo";
            gridViewComboBoxColumn2.Name = "Tipo";
            gridViewComboBoxColumn2.Width = 85;
            gridViewTextBoxColumn2.FieldName = "Codigo";
            gridViewTextBoxColumn2.HeaderText = "Codigo";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Codigo";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn3.Width = 200;
            gridViewTextBoxColumn4.FieldName = "Marca";
            gridViewTextBoxColumn4.HeaderText = "Marca";
            gridViewTextBoxColumn4.Name = "Marca";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.FieldName = "Especificacion";
            gridViewTextBoxColumn5.HeaderText = "Especificación";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Especificacion";
            gridViewTextBoxColumn5.Width = 150;
            gridViewComboBoxColumn3.FieldName = "Unidad";
            gridViewComboBoxColumn3.HeaderText = "Unidad";
            gridViewComboBoxColumn3.IsVisible = false;
            gridViewComboBoxColumn3.Name = "Unidad";
            gridViewComboBoxColumn3.Width = 95;
            gridViewCalculatorColumn1.FieldName = "Unitario";
            gridViewCalculatorColumn1.FormatString = "{0:N2}";
            gridViewCalculatorColumn1.HeaderText = "Unitario";
            gridViewCalculatorColumn1.Name = "Unitario";
            gridViewCalculatorColumn1.Width = 75;
            gridViewMultiComboBoxColumn1.DataType = typeof(int);
            gridViewMultiComboBoxColumn1.FieldName = "IdPrecio";
            gridViewMultiComboBoxColumn1.HeaderText = "Cat. Precios";
            gridViewMultiComboBoxColumn1.IsVisible = false;
            gridViewMultiComboBoxColumn1.Name = "IdPrecio";
            gridViewMultiComboBoxColumn1.Width = 85;
            gridViewComboBoxColumn4.FieldName = "FactorTrasladoIVA";
            gridViewComboBoxColumn4.HeaderText = "Factor IVA";
            gridViewComboBoxColumn4.Name = "FactorTrasladoIVA";
            gridViewComboBoxColumn4.VisibleInColumnChooser = false;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "ValorTrasladoIVA";
            gridViewTextBoxColumn6.FormatString = "{0:P0}";
            gridViewTextBoxColumn6.HeaderText = "% IVA";
            gridViewTextBoxColumn6.Name = "ValorTrasladoIVA";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Etiquetas";
            gridViewTextBoxColumn7.HeaderText = "Etiquetas de búsqueda";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "Etiquetas";
            gridViewTextBoxColumn7.Width = 100;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "UnidadXY";
            gridViewTextBoxColumn8.HeaderText = "UnidadXY";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "UnidadXY";
            gridViewTextBoxColumn9.FieldName = "UnidadZ";
            gridViewTextBoxColumn9.HeaderText = "UnidadZ";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "UnidadZ";
            gridViewComboBoxColumn5.DataType = typeof(int);
            gridViewComboBoxColumn5.FieldName = "IdUnidad";
            gridViewComboBoxColumn5.HeaderText = "Unidad";
            gridViewComboBoxColumn5.Name = "IdUnidad";
            gridViewComboBoxColumn5.Width = 85;
            gridViewCommandColumn1.FieldName = "AutorizadoText";
            gridViewCommandColumn1.HeaderText = "Autorizado";
            gridViewCommandColumn1.Name = "AutorizadoText";
            gridViewCommandColumn1.Width = 75;
            gridViewCheckBoxColumn2.FieldName = "Autorizado";
            gridViewCheckBoxColumn2.HeaderText = "Autorizado";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Autorizado";
            gridViewComboBoxColumn6.DataType = typeof(int);
            gridViewComboBoxColumn6.FieldName = "IdVisible";
            gridViewComboBoxColumn6.HeaderText = "Visible";
            gridViewComboBoxColumn6.Name = "IdVisible";
            gridViewComboBoxColumn6.Width = 95;
            gridViewTextBoxColumn10.DataType = typeof(int);
            gridViewTextBoxColumn10.FieldName = "Secuencia";
            gridViewTextBoxColumn10.FormatString = "{0:N0}";
            gridViewTextBoxColumn10.HeaderText = "Sec.";
            gridViewTextBoxColumn10.Name = "Secuencia";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn11.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn11.HeaderText = "No. Identificación";
            gridViewTextBoxColumn11.Name = "NoIdentificacion";
            gridViewTextBoxColumn11.Width = 120;
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "Minimo";
            gridViewTextBoxColumn12.FormatString = "{0:N0}";
            gridViewTextBoxColumn12.HeaderText = "Minimo";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Minimo";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 65;
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "Maximo";
            gridViewTextBoxColumn13.FormatString = "{0:N0}";
            gridViewTextBoxColumn13.HeaderText = "Maximo";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Maximo";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 65;
            gridViewTextBoxColumn14.DataType = typeof(int);
            gridViewTextBoxColumn14.FieldName = "ReOrden";
            gridViewTextBoxColumn14.FormatString = "{0:N0}";
            gridViewTextBoxColumn14.HeaderText = "ReOrden";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "ReOrden";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 65;
            gridViewTextBoxColumn15.DataType = typeof(int);
            gridViewTextBoxColumn15.FieldName = "Existencia";
            gridViewTextBoxColumn15.FormatString = "{0:N0}";
            gridViewTextBoxColumn15.HeaderText = "Existencia";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "Existencia";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 65;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "Largo";
            gridViewTextBoxColumn16.FormatString = "{0:N2}";
            gridViewTextBoxColumn16.HeaderText = "Largo";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Largo";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 65;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "Ancho";
            gridViewTextBoxColumn17.FormatString = "{0:N2}";
            gridViewTextBoxColumn17.HeaderText = "Ancho";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "Ancho";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 65;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "Alto";
            gridViewTextBoxColumn18.FormatString = "{0:N2}";
            gridViewTextBoxColumn18.HeaderText = "Alto";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "Alto";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 65;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "Peso";
            gridViewTextBoxColumn19.FormatString = "{0:N2}";
            gridViewTextBoxColumn19.HeaderText = "Peso";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Peso";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 65;
            gridViewTextBoxColumn20.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn20.HeaderText = "ClaveUnidad";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "ClaveUnidad";
            gridViewTextBoxColumn21.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn21.HeaderText = "ClaveProdServ";
            gridViewTextBoxColumn21.IsVisible = false;
            gridViewTextBoxColumn21.Name = "ClaveProdServ";
            gridViewTextBoxColumn22.FieldName = "Creo";
            gridViewTextBoxColumn22.HeaderText = "Creó";
            gridViewTextBoxColumn22.Name = "Creo";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.Width = 65;
            gridViewDateTimeColumn1.FieldName = "FechaNuevo";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn1.Name = "FechaNuevo";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 75;
            gridViewTextBoxColumn23.FieldName = "Modifica";
            gridViewTextBoxColumn23.HeaderText = "Modifica";
            gridViewTextBoxColumn23.IsVisible = false;
            gridViewTextBoxColumn23.Name = "Modifica";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn24.FieldName = "FechaModifica";
            gridViewTextBoxColumn24.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn24.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn24.IsVisible = false;
            gridViewTextBoxColumn24.Name = "FechaModifica";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.gModelos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewComboBoxColumn3,
            gridViewCalculatorColumn1,
            gridViewMultiComboBoxColumn1,
            gridViewComboBoxColumn4,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewComboBoxColumn5,
            gridViewCommandColumn1,
            gridViewCheckBoxColumn2,
            gridViewComboBoxColumn6,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24});
            this.gModelos.MasterTemplate.MultiSelect = true;
            this.gModelos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gModelos.Name = "gModelos";
            this.gModelos.Size = new System.Drawing.Size(757, 595);
            this.gModelos.TabIndex = 3;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.splitContainer1);
            this.splitPanel2.Location = new System.Drawing.Point(761, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(354, 595);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.1813681F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(-201, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gVariante);
            this.splitContainer1.Panel1.Controls.Add(this.TDisponible);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gImagen);
            this.splitContainer1.Panel2.Controls.Add(this.TImagen);
            this.splitContainer1.Size = new System.Drawing.Size(354, 595);
            this.splitContainer1.SplitterDistance = 254;
            this.splitContainer1.TabIndex = 9;
            // 
            // gVariante
            // 
            this.gVariante.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnF2;
            this.gVariante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gVariante.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gVariante.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn7.DataType = typeof(int);
            gridViewComboBoxColumn7.FieldName = "IdEspecificacion";
            gridViewComboBoxColumn7.HeaderText = "Variante";
            gridViewComboBoxColumn7.Name = "IdEspecificacion";
            gridViewComboBoxColumn7.Width = 150;
            gridViewComboBoxColumn8.DataType = typeof(int);
            gridViewComboBoxColumn8.FieldName = "IdVisibilidad";
            gridViewComboBoxColumn8.HeaderText = "Visibilidad";
            gridViewComboBoxColumn8.Name = "IdVisibilidad";
            gridViewComboBoxColumn8.Width = 95;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "Minimo";
            gridViewTextBoxColumn25.FormatString = "{0:N2}";
            gridViewTextBoxColumn25.HeaderText = "Minimo";
            gridViewTextBoxColumn25.Name = "Minimo";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 65;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "Maximo";
            gridViewTextBoxColumn26.FormatString = "{0:N2}";
            gridViewTextBoxColumn26.HeaderText = "Maximo";
            gridViewTextBoxColumn26.Name = "Maximo";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 65;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "ReOrden";
            gridViewTextBoxColumn27.FormatString = "{0:N2}";
            gridViewTextBoxColumn27.HeaderText = "ReOrden";
            gridViewTextBoxColumn27.Name = "ReOrden";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 65;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "Existencia";
            gridViewTextBoxColumn28.HeaderText = "Existencia";
            gridViewTextBoxColumn28.Name = "Existencia";
            gridViewTextBoxColumn28.Width = 75;
            gridViewTextBoxColumn29.FieldName = "Modifica";
            gridViewTextBoxColumn29.HeaderText = "Modifica";
            gridViewTextBoxColumn29.Name = "Modifica";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.Width = 75;
            gridViewTextBoxColumn30.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn30.FieldName = "FechaModifica";
            gridViewTextBoxColumn30.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn30.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn30.Name = "FechaModifica";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn30.Width = 65;
            this.gVariante.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn7,
            gridViewComboBoxColumn8,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30});
            this.gVariante.MasterTemplate.EnableFiltering = true;
            this.gVariante.MasterTemplate.ShowFilteringRow = false;
            this.gVariante.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gVariante.Name = "gVariante";
            this.gVariante.ShowGroupPanel = false;
            this.gVariante.Size = new System.Drawing.Size(354, 224);
            this.gVariante.TabIndex = 7;
            this.gVariante.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_CellEndEdit);
            this.gVariante.RowsChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.RadGridView_RowsChanging);
            // 
            // TDisponible
            // 
            this.TDisponible.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDisponible.Etiqueta = "Disponibilidad";
            this.TDisponible.Location = new System.Drawing.Point(0, 0);
            this.TDisponible.Name = "TDisponible";
            this.TDisponible.ReadOnly = false;
            this.TDisponible.ShowActualizar = false;
            this.TDisponible.ShowAutorizar = false;
            this.TDisponible.ShowCerrar = false;
            this.TDisponible.ShowEditar = false;
            this.TDisponible.ShowExportarExcel = false;
            this.TDisponible.ShowFiltro = true;
            this.TDisponible.ShowGuardar = false;
            this.TDisponible.ShowHerramientas = false;
            this.TDisponible.ShowImagen = false;
            this.TDisponible.ShowImprimir = false;
            this.TDisponible.ShowNuevo = true;
            this.TDisponible.ShowRemover = true;
            this.TDisponible.Size = new System.Drawing.Size(354, 30);
            this.TDisponible.TabIndex = 8;
            // 
            // gImagen
            // 
            this.gImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gImagen.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gImagen.MasterTemplate.AllowAddNewRow = false;
            this.gImagen.MasterTemplate.AutoGenerateColumns = false;
            gridViewImageColumn1.FieldName = "Imagen";
            gridViewImageColumn1.HeaderText = "Imagen";
            gridViewImageColumn1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            gridViewImageColumn1.Name = "column1";
            gridViewImageColumn1.Width = 300;
            gridViewTextBoxColumn31.FieldName = "URL";
            gridViewTextBoxColumn31.HeaderText = "URL";
            gridViewTextBoxColumn31.Name = "URL";
            this.gImagen.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1,
            gridViewTextBoxColumn31});
            this.gImagen.MasterTemplate.ShowFilteringRow = false;
            this.gImagen.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gImagen.Name = "gImagen";
            this.gImagen.ShowGroupPanel = false;
            this.gImagen.Size = new System.Drawing.Size(354, 307);
            this.gImagen.TabIndex = 8;
            // 
            // TImagen
            // 
            this.TImagen.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImagen.Etiqueta = "";
            this.TImagen.Location = new System.Drawing.Point(0, 0);
            this.TImagen.Name = "TImagen";
            this.TImagen.ReadOnly = false;
            this.TImagen.ShowActualizar = false;
            this.TImagen.ShowAutorizar = false;
            this.TImagen.ShowCerrar = false;
            this.TImagen.ShowEditar = false;
            this.TImagen.ShowExportarExcel = false;
            this.TImagen.ShowFiltro = false;
            this.TImagen.ShowGuardar = false;
            this.TImagen.ShowHerramientas = false;
            this.TImagen.ShowImagen = false;
            this.TImagen.ShowImprimir = false;
            this.TImagen.ShowNuevo = true;
            this.TImagen.ShowRemover = true;
            this.TImagen.Size = new System.Drawing.Size(354, 30);
            this.TImagen.TabIndex = 9;
            // 
            // TModelo
            // 
            this.TModelo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TModelo.Etiqueta = "";
            this.TModelo.Location = new System.Drawing.Point(0, 0);
            this.TModelo.Name = "TModelo";
            this.TModelo.ReadOnly = false;
            this.TModelo.ShowActualizar = true;
            this.TModelo.ShowAutorizar = true;
            this.TModelo.ShowCerrar = true;
            this.TModelo.ShowEditar = true;
            this.TModelo.ShowExportarExcel = false;
            this.TModelo.ShowFiltro = true;
            this.TModelo.ShowGuardar = false;
            this.TModelo.ShowHerramientas = true;
            this.TModelo.ShowImagen = false;
            this.TModelo.ShowImprimir = false;
            this.TModelo.ShowNuevo = true;
            this.TModelo.ShowRemover = true;
            this.TModelo.Size = new System.Drawing.Size(1115, 30);
            this.TModelo.TabIndex = 3;
            // 
            // ProductosModeloCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 625);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TModelo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductosModeloCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Catálogo Productos-Modelos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductosCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gModelos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gModelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        protected internal Common.Forms.ToolBarStandarControl TModelo;
        protected internal Telerik.WinControls.UI.RadGridView gModelos;
        protected internal Telerik.WinControls.UI.RadGridView gVariante;
        protected internal Common.Forms.ToolBarStandarControl TDisponible;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Telerik.WinControls.UI.RadGridView gImagen;
        protected internal Common.Forms.ToolBarStandarControl TImagen;
    }
}
