﻿
namespace Jaeger.UI.Tienda.Forms {
    partial class PrecioForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Orden = new Telerik.WinControls.UI.RadSpinEditor();
            this.Prioridad = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Extranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TPartida = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.dataGridPartidas = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Orden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Extranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ReadOnly = false;
            this.TCatalogo.ShowActualizar = true;
            this.TCatalogo.ShowAutorizar = false;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowEditar = false;
            this.TCatalogo.ShowExportarExcel = false;
            this.TCatalogo.ShowFiltro = false;
            this.TCatalogo.ShowGuardar = true;
            this.TCatalogo.ShowHerramientas = false;
            this.TCatalogo.ShowImagen = false;
            this.TCatalogo.ShowImprimir = false;
            this.TCatalogo.ShowNuevo = false;
            this.TCatalogo.ShowRemover = false;
            this.TCatalogo.Size = new System.Drawing.Size(617, 30);
            this.TCatalogo.TabIndex = 2;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.Orden);
            this.radGroupBox1.Controls.Add(this.Prioridad);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.Extranjero);
            this.radGroupBox1.Controls.Add(this.Nota);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.Descripcion);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "General";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(617, 75);
            this.radGroupBox1.TabIndex = 5;
            this.radGroupBox1.Text = "General";
            // 
            // Orden
            // 
            this.Orden.Location = new System.Drawing.Point(459, 46);
            this.Orden.Name = "Orden";
            this.Orden.Size = new System.Drawing.Size(55, 20);
            this.Orden.TabIndex = 68;
            this.Orden.TabStop = false;
            this.Orden.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Prioridad
            // 
            this.Prioridad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Prioridad.AutoSizeDropDownHeight = true;
            this.Prioridad.AutoSizeDropDownToBestFit = true;
            this.Prioridad.DisplayMember = "Descripcion";
            this.Prioridad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Prioridad.NestedRadGridView
            // 
            this.Prioridad.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Prioridad.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prioridad.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Prioridad.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Prioridad.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Prioridad.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Prioridad.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdVendedor";
            gridViewTextBoxColumn1.HeaderText = "IdVendedor";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdVendedor";
            gridViewTextBoxColumn2.FieldName = "ClaveVendedor";
            gridViewTextBoxColumn2.HeaderText = "Vendedor";
            gridViewTextBoxColumn2.Name = "ClaveVendedor";
            this.Prioridad.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Prioridad.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Prioridad.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Prioridad.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Prioridad.EditorControl.Name = "NestedRadGridView";
            this.Prioridad.EditorControl.ReadOnly = true;
            this.Prioridad.EditorControl.ShowGroupPanel = false;
            this.Prioridad.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Prioridad.EditorControl.TabIndex = 0;
            this.Prioridad.Location = new System.Drawing.Point(459, 20);
            this.Prioridad.Name = "Prioridad";
            this.Prioridad.Size = new System.Drawing.Size(89, 20);
            this.Prioridad.TabIndex = 67;
            this.Prioridad.TabStop = false;
            this.Prioridad.ValueMember = "Id";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(399, 21);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(54, 18);
            this.radLabel4.TabIndex = 66;
            this.radLabel4.Text = "Prioridad:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(399, 47);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(40, 18);
            this.radLabel2.TabIndex = 65;
            this.radLabel2.Text = "Orden:";
            // 
            // Extranjero
            // 
            this.Extranjero.Location = new System.Drawing.Point(554, 47);
            this.Extranjero.Name = "Extranjero";
            this.Extranjero.Size = new System.Drawing.Size(51, 18);
            this.Extranjero.TabIndex = 64;
            this.Extranjero.Text = "Activo";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(83, 46);
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(310, 20);
            this.Nota.TabIndex = 3;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 47);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(33, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Nota:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(83, 20);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(310, 20);
            this.Descripcion.TabIndex = 1;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(67, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Descripción:";
            // 
            // TPartida
            // 
            this.TPartida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPartida.Etiqueta = "";
            this.TPartida.Location = new System.Drawing.Point(0, 105);
            this.TPartida.Name = "TPartida";
            this.TPartida.ReadOnly = false;
            this.TPartida.ShowActualizar = false;
            this.TPartida.ShowAutorizar = false;
            this.TPartida.ShowCerrar = false;
            this.TPartida.ShowEditar = false;
            this.TPartida.ShowExportarExcel = false;
            this.TPartida.ShowFiltro = false;
            this.TPartida.ShowGuardar = false;
            this.TPartida.ShowHerramientas = false;
            this.TPartida.ShowImagen = false;
            this.TPartida.ShowImprimir = false;
            this.TPartida.ShowNuevo = true;
            this.TPartida.ShowRemover = true;
            this.TPartida.Size = new System.Drawing.Size(617, 30);
            this.TPartida.TabIndex = 7;
            // 
            // dataGridPartidas
            // 
            this.dataGridPartidas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridPartidas.Location = new System.Drawing.Point(0, 135);
            // 
            // 
            // 
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "Nuevo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "0";
            gridViewTextBoxColumn3.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "IdPrecio";
            gridViewTextBoxColumn3.HeaderText = "IdRelacion";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "IdPrecio";
            gridViewTextBoxColumn3.VisibleInColumnChooser = false;
            conditionalFormattingObject2.ApplyToRow = true;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.Name = "RegistroActivo";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject2.TValue1 = "0";
            conditionalFormattingObject2.TValue2 = "0";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewComboBoxColumn1.DataType = typeof(int);
            gridViewComboBoxColumn1.FieldName = "IdEspecificacion";
            gridViewComboBoxColumn1.HeaderText = "Especificación";
            gridViewComboBoxColumn1.Name = "IdEspecificacion";
            gridViewComboBoxColumn1.Width = 200;
            gridViewTextBoxColumn4.DataType = typeof(decimal);
            gridViewTextBoxColumn4.FieldName = "UnitarioC";
            gridViewTextBoxColumn4.FormatString = "{0:N2}";
            gridViewTextBoxColumn4.HeaderText = "$ Costo";
            gridViewTextBoxColumn4.Name = "UnitarioC";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.DataType = typeof(decimal);
            gridViewTextBoxColumn5.FieldName = "Unitario";
            gridViewTextBoxColumn5.FormatString = "{0:N2}";
            gridViewTextBoxColumn5.HeaderText = "$ Unitario";
            gridViewTextBoxColumn5.Name = "Unitario";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.FieldName = "Nota";
            gridViewTextBoxColumn6.HeaderText = "Observaciones";
            gridViewTextBoxColumn6.Name = "Nota";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn7.FieldName = "FechaNuevo";
            gridViewTextBoxColumn7.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn7.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn7.Name = "FechaNuevo";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.FieldName = "Creo";
            gridViewTextBoxColumn8.HeaderText = "Creó";
            gridViewTextBoxColumn8.Name = "Creo";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 65;
            gridViewTextBoxColumn9.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn9.FieldName = "FechaModifica";
            gridViewTextBoxColumn9.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn9.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn9.Name = "FechaModifica";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.FieldName = "Modifica";
            gridViewTextBoxColumn10.HeaderText = "Modifica";
            gridViewTextBoxColumn10.Name = "Modifica";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 65;
            conditionalFormattingObject3.ApplyToRow = true;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.Name = "Nueva Partida";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.TValue1 = "0";
            conditionalFormattingObject3.TValue2 = "0";
            gridViewTextBoxColumn11.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "IdPartida";
            gridViewTextBoxColumn11.HeaderText = "IdPartida";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "IdPartida";
            gridViewTextBoxColumn11.VisibleInColumnChooser = false;
            this.dataGridPartidas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewCheckBoxColumn1,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.dataGridPartidas.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridPartidas.Name = "dataGridPartidas";
            this.dataGridPartidas.Size = new System.Drawing.Size(617, 315);
            this.dataGridPartidas.TabIndex = 8;
            // 
            // PrecioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 450);
            this.Controls.Add(this.dataGridPartidas);
            this.Controls.Add(this.TPartida);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.TCatalogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrecioForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catálogo de Precio";
            this.Load += new System.EventHandler(this.PrecioForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Orden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Extranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TCatalogo;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Common.Forms.ToolBarStandarControl TPartida;
        private Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadCheckBox Extranjero;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Prioridad;
        private Telerik.WinControls.UI.RadSpinEditor Orden;
        private Telerik.WinControls.UI.RadGridView dataGridPartidas;
    }
}