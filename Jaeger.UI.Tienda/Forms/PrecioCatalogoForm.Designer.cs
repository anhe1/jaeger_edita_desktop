﻿namespace Jaeger.UI.Tienda.Forms
{
    partial class PrecioCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject4 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrecioCatalogoForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridCatalogo = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridPartidas = new Telerik.WinControls.UI.RadGridView();
            this.TPartida = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(928, 545);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.dataGridCatalogo);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(928, 270);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // dataGridCatalogo
            // 
            this.dataGridCatalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridCatalogo.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "RegistroActivo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "0";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewComboBoxColumn1.FieldName = "IdPrioridad";
            gridViewComboBoxColumn1.HeaderText = "Prioridad";
            gridViewComboBoxColumn1.Name = "IdPrioridad";
            gridViewComboBoxColumn1.Width = 100;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Secuencia";
            gridViewTextBoxColumn1.HeaderText = "Orden";
            gridViewTextBoxColumn1.Name = "Secuencia";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 45;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.FieldName = "FechaInicio";
            gridViewTextBoxColumn3.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn3.HeaderText = "Vig. Inicio";
            gridViewTextBoxColumn3.Name = "FechaInicio";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn4.FieldName = "FechaFin";
            gridViewTextBoxColumn4.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn4.HeaderText = "Vig. Fin";
            gridViewTextBoxColumn4.Name = "FechaFin";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Nota";
            gridViewTextBoxColumn5.HeaderText = "Nota";
            gridViewTextBoxColumn5.Name = "Nota";
            gridViewTextBoxColumn5.Width = 200;
            gridViewTextBoxColumn6.FieldName = "FechaNuevo";
            gridViewTextBoxColumn6.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn6.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn6.Name = "FechaNuevo";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.FieldName = "Creo";
            gridViewTextBoxColumn7.HeaderText = "Creo";
            gridViewTextBoxColumn7.Name = "Creo";
            gridViewTextBoxColumn7.Width = 65;
            conditionalFormattingObject2.ApplyToRow = true;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.Name = "Vigente";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject2.TValue1 = "False";
            conditionalFormattingObject2.TValue2 = "False";
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewCheckBoxColumn2.FieldName = "IsVigente";
            gridViewCheckBoxColumn2.HeaderText = "IsVigente";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "IsVigente";
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            this.dataGridCatalogo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewCheckBoxColumn2});
            this.dataGridCatalogo.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridCatalogo.Name = "dataGridCatalogo";
            this.dataGridCatalogo.Size = new System.Drawing.Size(928, 270);
            this.dataGridCatalogo.TabIndex = 0;
            this.dataGridCatalogo.SelectionChanged += new System.EventHandler(this.DataGridCatalogo_SelectionChanged);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.dataGridPartidas);
            this.splitPanel2.Controls.Add(this.TPartida);
            this.splitPanel2.Location = new System.Drawing.Point(0, 274);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(928, 271);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // dataGridPartidas
            // 
            this.dataGridPartidas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridPartidas.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            conditionalFormattingObject3.ApplyToRow = true;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.Name = "Nuevo";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.TValue1 = "0";
            conditionalFormattingObject3.TValue2 = "0";
            gridViewTextBoxColumn8.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "IdPrecio";
            gridViewTextBoxColumn8.HeaderText = "IdRelacion";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "IdPrecio";
            gridViewTextBoxColumn8.VisibleInColumnChooser = false;
            conditionalFormattingObject4.ApplyToRow = true;
            conditionalFormattingObject4.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.Name = "RegistroActivo";
            conditionalFormattingObject4.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject4.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject4.TValue1 = "0";
            conditionalFormattingObject4.TValue2 = "0";
            gridViewCheckBoxColumn3.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            gridViewCheckBoxColumn3.FieldName = "Activo";
            gridViewCheckBoxColumn3.HeaderText = "Activo";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "Activo";
            gridViewComboBoxColumn2.DataType = typeof(int);
            gridViewComboBoxColumn2.FieldName = "IdEspecificacion";
            gridViewComboBoxColumn2.HeaderText = "Especificación";
            gridViewComboBoxColumn2.Name = "IdEspecificacion";
            gridViewComboBoxColumn2.Width = 200;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "UnitarioC";
            gridViewTextBoxColumn9.FormatString = "{0:N2}";
            gridViewTextBoxColumn9.HeaderText = "$ Costo";
            gridViewTextBoxColumn9.Name = "UnitarioC";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Unitario";
            gridViewTextBoxColumn10.FormatString = "{0:N2}";
            gridViewTextBoxColumn10.HeaderText = "$ Unitario";
            gridViewTextBoxColumn10.Name = "Unitario";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.FieldName = "Nota";
            gridViewTextBoxColumn11.HeaderText = "Observaciones";
            gridViewTextBoxColumn11.Name = "Nota";
            gridViewTextBoxColumn11.Width = 200;
            gridViewTextBoxColumn12.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn12.FieldName = "FechaNuevo";
            gridViewTextBoxColumn12.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn12.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn12.Name = "FechaNuevo";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.FieldName = "Creo";
            gridViewTextBoxColumn13.HeaderText = "Creó";
            gridViewTextBoxColumn13.Name = "Creo";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.Width = 65;
            gridViewTextBoxColumn14.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn14.FieldName = "FechaModifica";
            gridViewTextBoxColumn14.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn14.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn14.Name = "FechaModifica";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.FieldName = "Modifica";
            gridViewTextBoxColumn15.HeaderText = "Modifica";
            gridViewTextBoxColumn15.Name = "Modifica";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 65;
            this.dataGridPartidas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewCheckBoxColumn3,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15});
            this.dataGridPartidas.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridPartidas.Name = "dataGridPartidas";
            this.dataGridPartidas.Size = new System.Drawing.Size(928, 241);
            this.dataGridPartidas.TabIndex = 0;
            this.dataGridPartidas.SelectionChanged += new System.EventHandler(this.DataGridPartidas_SelectionChanged);
            this.dataGridPartidas.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.DataGridPartidas_RowsChanged);
            // 
            // TPartida
            // 
            this.TPartida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPartida.Etiqueta = "";
            this.TPartida.Location = new System.Drawing.Point(0, 0);
            this.TPartida.Name = "TPartida";
            this.TPartida.ShowActualizar = false;
            this.TPartida.ShowAutorizar = false;
            this.TPartida.ShowCerrar = false;
            this.TPartida.ShowEditar = false;
            this.TPartida.ShowExportarExcel = false;
            this.TPartida.ShowFiltro = false;
            this.TPartida.ShowGuardar = false;
            this.TPartida.ShowHerramientas = false;
            this.TPartida.ShowImagen = false;
            this.TPartida.ShowImprimir = false;
            this.TPartida.ShowNuevo = true;
            this.TPartida.ShowRemover = true;
            this.TPartida.Size = new System.Drawing.Size(928, 30);
            this.TPartida.TabIndex = 2;
            this.TPartida.Visible = false;
            this.TPartida.Nuevo.Click += this.TPartida_Nuevo_Click;
            this.TPartida.Remover.Click += this.TPartida_Remover_Click;
            this.TPartida.Guardar.Click += this.TPartida_Guardar_Click;
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowActualizar = true;
            this.TCatalogo.ShowAutorizar = false;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowEditar = true;
            this.TCatalogo.ShowExportarExcel = false;
            this.TCatalogo.ShowFiltro = true;
            this.TCatalogo.ShowGuardar = false;
            this.TCatalogo.ShowHerramientas = true;
            this.TCatalogo.ShowImagen = false;
            this.TCatalogo.ShowImprimir = false;
            this.TCatalogo.ShowNuevo = true;
            this.TCatalogo.ShowRemover = true;
            this.TCatalogo.Size = new System.Drawing.Size(928, 30);
            this.TCatalogo.TabIndex = 1;
            this.TCatalogo.Nuevo.Click += this.TCatalogo_Nuevo_Click;
            this.TCatalogo.Editar.Click += this.TCatalogo_Editar_Click;
            this.TCatalogo.Remover.Click += this.TCatalogo_Remover_Click;
            this.TCatalogo.Actualizar.Click += this.TCatalogo_Actualizar_Click;
            this.TCatalogo.Cerrar.Click += this.TCatalogo_Cerrar_Click;
            // 
            // PrecioCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 575);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TCatalogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrecioCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Tag = "Jaeger.UI.CPLite.Forms.PrecioCatalogoForm";
            this.Text = "CP Lite: Catálogo de Precios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PrecioCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadGridView dataGridCatalogo;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView dataGridPartidas;
        private Common.Forms.ToolBarStandarControl TCatalogo;
        private Common.Forms.ToolBarStandarControl TPartida;
    }
}
