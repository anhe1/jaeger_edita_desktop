﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Tienda.Builder;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PreguntaFrecuenteCatalogoForm : RadForm {
        protected internal IPreguntasFrecuentesService service;
        protected internal BindingList<PreguntaFrecuenteModel> datos;

        public PreguntaFrecuenteCatalogoForm() {
            InitializeComponent();
        }

        private void PreguntaFrecuenteCatalogo_Load(object sender, EventArgs e) {
            using (IPreguntasFrecuentesGridBuilder builder = new PreguntasFrecuentesGridBuilder()) {
                this.GFaq.GridData.Columns.AddRange(builder.Templetes().Master().Build());
            }
            this.GFaq.Nuevo.Click += this.TTienda_Nuevo_Click;
            this.GFaq.Remover.Click += this.TTienda_Remover_Click;
            this.GFaq.Editar.Click += this.TTienda_Editar_Click;
            this.GFaq.Actualizar.Click += this.TTienda_Actualizar_Click;
            this.GFaq.Cerrar.Click += this.TTienda_Cerrar_Click;
        }

        private void TTienda_Nuevo_Click(object sender, EventArgs e) {
            var nuevaPregunta = new PreguntaForm(this.service, null);
            nuevaPregunta.Agregate += this.NuevaPregunta_Agregate;
            nuevaPregunta.ShowDialog(this);
        }

        private void NuevaPregunta_Agregate(object sender, PreguntaFrecuenteModel e) {
            if (e == null) return;
            this.datos.Add(e);
        }

        private void TTienda_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.GFaq.GetCurrent<PreguntaFrecuenteModel>();
            if (seleccionado != null) {
                var editarPregunta = new PreguntaForm(this.service, seleccionado);
                editarPregunta.ShowDialog(this);
            }
        }

        private void TTienda_Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.GFaq.GetCurrent<PreguntaFrecuenteModel>();
            if (seleccionado != null) {
                if (seleccionado.Id == 0)
                    this.GFaq.GridData.Rows.Remove(this.GFaq.GridData.CurrentRow);
                else {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover el elemento seleccionado?", "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                        return;
                    seleccionado.Activo = false;
                }
            }
        }

        private void TTienda_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Espere un momento...";
                espera.ShowDialog(this);
            }
            this.GFaq.GridData.DataSource = this.datos;
        }

        private void TTienda_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            this.datos = this.service.GetList(true);
        }
    }
}
