﻿using System;
using System.Linq;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Aplication.Tienda.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PedidoForm : RadForm {
        protected internal IPedidoService service;
        protected internal IEspecificacionService especificacionService;
        protected internal IUnidadService unidadService;
        private PedidoClienteDetailModel comprobante = new PedidoClienteDetailModel();

        public PedidoForm() {
            InitializeComponent();
        }

        public PedidoForm(IPedidoService service) {
            InitializeComponent();
            this.service = service;
        }

        public PedidoForm(IPedidoService service, int index) {
            InitializeComponent();
            this.service = service;
            this.comprobante.IdPedido = index;
        }

        private void PedidoForm_Load(object sender, EventArgs e) {
            this.pedidoControl.CreateService();
            this.dataGridProductos.Standard();

            this.pedidoControl.ToolBar.Nuevo.Click += Nuevo_Click;
            this.pedidoControl.ToolBar.Actualizar.Click += Actualizar_Click;
            this.pedidoControl.ToolBar.Cerrar.Click += Cerrar_Click;
            this.pedidoControl.ToolBar.Guardar.Click += Guardar_Click;
            this.pedidoControl.ToolBar.Imprimir.Click += Imprimir_Click;
            this.pedidoControl.BindingCompleted += pedidoControl_BindingCompleted;

            this.pedidoControl.ToolBar.ShowNuevo = false;
            this.dataGridProductos.AllowEditRow = true;
            if (ConfigService.Piloto.IsInRole(ConfigService.Administrador)) {
                this.pedidoControl.ToolBar.ShowNuevo = true;
            }
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            this.comprobante = new PedidoClienteDetailModel();
            this.Actualizar_Click(sender, e);
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            if (this.comprobante.IdPedido > 0) {
                var _p = new ReporteForm(new PedidoClientePrinter(this.comprobante));
                _p.Show();
            } else {
                MessageBox.Show(this, Properties.Resources.msg_pedido_imprimir_error, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Guardar_Click(object sender, EventArgs e) {
            this.pedidoControl.errorProvider1.Clear();
            if (this.comprobante.Error != string.Empty) {
                MessageBox.Show(this, this.comprobante.Error, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (this.comprobante["IdDirectorio"] != string.Empty) {
                    this.pedidoControl.errorProvider1.SetError(this.pedidoControl.Cliente, this.comprobante["IdDirectorio"]);
                }

                if (this.comprobante["IdVendedor"] != string.Empty) {
                    this.pedidoControl.errorProvider1.SetError(this.pedidoControl.Vendedor, this.comprobante["IdVendedor"]);
                }

                return;
            }

            if (this.comprobante.Partidas.Count() == 0) {
                MessageBox.Show(this, "No se agregaron partias a este pedido.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Cargando...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void pedidoControl_BindingCompleted(object sender, EventArgs e) {
            this.pedidoControl.ToolBar.Actualizar.PerformClick();
        }

        private void CreateBinding() {
            this.Text = "Pedido: " + this.comprobante.IdPedido.ToString("#000000");
            this.pedidoControl.ToolBar.Etiqueta = this.comprobante.IdPedido.ToString("#000000");

            this.pedidoControl.Cliente.DataBindings.Clear();
            this.pedidoControl.Cliente.DisplayMember = "Nombre";
            this.pedidoControl.Cliente.ValueMember = "IdDirectorio";
            this.pedidoControl.Cliente.DataBindings.Add("SelectedValue", this.comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.Cliente.Enabled = this.comprobante.Editable;

            this.pedidoControl.Status.DataBindings.Clear();
            this.pedidoControl.Status.DataBindings.Add("SelectedValue", this.comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.Status.Enabled = false; // this.comprobante.Editable;

            this.pedidoControl.Facturar.DataBindings.Clear();
            this.pedidoControl.Facturar.DataBindings.Add("Checked", this.comprobante, "ReqFactura", true, DataSourceUpdateMode.OnPropertyChanged);

            this.pedidoControl.Embarque.DataBindings.Clear();
            this.pedidoControl.Embarque.DataBindings.Add("SelectedValue", this.comprobante, "IdDireccion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.Embarque.Enabled = this.comprobante.Editable;

            this.pedidoControl.comboBox1.DataBindings.Clear();
            this.pedidoControl.comboBox1.DataBindings.Add("Text", this.comprobante, "MetodoEnvio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.comboBox1.Enabled = this.comprobante.Editable;

            this.pedidoControl.NoPedido.DataBindings.Clear();
            this.pedidoControl.NoPedido.DataBindings.Add("Text", this.comprobante, "IdPedido", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.NoPedido.Enabled = this.comprobante.Editable;

            this.pedidoControl.FechaEmision.DataBindings.Clear();
            this.pedidoControl.FechaEmision.DataBindings.Add("Value", this.comprobante, "FechaNuevo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.FechaEmision.Enabled = false;

            this.pedidoControl.FechaEntrega.DataBindings.Clear();
            this.pedidoControl.FechaEntrega.DataBindings.Add("Value", this.comprobante, "FechaEntrega", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.FechaEntrega.Enabled = false;

            this.pedidoControl.Recibe.DataBindings.Clear();
            this.pedidoControl.Recibe.DataBindings.Add("Text", this.comprobante, "Recibe", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.Recibe.ReadOnly = !this.comprobante.Editable;

            this.pedidoControl.Observaciones.DataBindings.Clear();
            this.pedidoControl.Observaciones.DataBindings.Add("Text", this.comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.Observaciones.ReadOnly = !this.comprobante.Editable;

            this.pedidoControl.Vendedor.DataBindings.Clear();
            this.pedidoControl.Vendedor.DataBindings.Add("SelectedValue", this.comprobante, "IdVendedor", true, DataSourceUpdateMode.OnPropertyChanged);
            this.pedidoControl.Vendedor.Enabled = this.comprobante.Editable;

            this.pedidoControl.txbCreo.DataBindings.Clear();
            this.pedidoControl.txbCreo.DataBindings.Add("Text", this.comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.pedidoControl.txbModifica.DataBindings.Clear();
            this.pedidoControl.txbModifica.DataBindings.Add("Text", this.comprobante, "Modifica", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dataGridProductos.DataSource = this.comprobante.Partidas;

            this.pedidoControl.Status.DisplayMember = "Descripcion";
            this.pedidoControl.Status.ValueMember = "Id";

            this.pedidoControl.comboBox1.DisplayMember = "Descripcion";
            this.pedidoControl.comboBox1.ValueMember = "Id";

            this.pedidoControl.Buscar.Enabled = this.comprobante.Editable;
            this.pedidoControl.Vendedor.Enabled = this.comprobante.Editable;
            this.pedidoControl.ToolBar.Guardar.Enabled = this.comprobante.Editable;

            this.TConcepto.Enabled = this.comprobante.Editable;
            this.dataGridProductos.ReadOnly = !this.comprobante.Editable;
        }

        private void Consultar() {
            if (this.comprobante.IdPedido == 0) {
                this.comprobante = new PedidoClienteDetailModel();
            } else {
                this.comprobante = this.service.GetPedido(this.comprobante.IdPedido);
            }
            this.pedidoControl.Status.DataSource = PedidoService.GetStatus();
            this.pedidoControl.comboBox1.DataSource = this.service.GetEnvio();
        }

        private void Guardar() {
            this.comprobante = this.service.Save(this.comprobante);
        }

        private void Agregar_Selected(object sender, ProductoXModelo e) {
            this.toolStripStatusLabel1.Text = string.Empty;
            if (e != null) {
                var _existe = this.comprobante.Partidas.Where(it => it.NoIdentificacion == e.NoIdentificacion).FirstOrDefault();
                if (_existe == null) {
                    var _concepto = new PedidoClientePartidaDetailModel {
                        IdModelo = e.IdModelo,
                        Activo = 1,
                        Catalogo = e.Categoria,
                        //Clave = e.Clave,
                        Modelo = e.Descripcion,
                        Especificacion = e.Especificacion,
                        IdProducto = e.IdProducto,
                        IdEspecificacion = e.IdEspecificacion,
                        IdUnidad = e.IdUnidad,
                        FactorUnidad = e.FactorUnidad,
                        Marca = e.Marca,
                        Producto = e.Nombre,
                        Unidad = e.Unidad,
                        Identificador = e.Identificador,
                        Tamanio = e.Variante,
                        TasaIVA = e.TasaIVA,
                        Creo = ConfigService.Piloto.Clave
                    };
                    this.comprobante.Partidas.Add(_concepto);
                } else {
                    this.toolStripStatusLabel1.Text = string.Format("El producto {0} {1} ya se encuentra listado", e.Descripcion, e.IdUnidad);
                }
            }
        }

        private void Conceptos_ButtonRemover_Click(object sender, EventArgs e) {
            if (this.dataGridProductos.CurrentRow != null) {
                var _seleccionado = this.dataGridProductos.CurrentRow.DataBoundItem as PedidoClientePartidaDetailModel;
                if (_seleccionado != null) {
                    if (MessageBox.Show(this, Properties.Resources.msg_remision_removerobjeto, "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        if (_seleccionado.IdPartida == 0) {
                            this.dataGridProductos.Rows.Remove(this.dataGridProductos.CurrentRow);
                        } else {
                            var index = this.dataGridProductos.CurrentRow.Index;
                            _seleccionado.Activo = 0;
                            this.dataGridProductos.Rows[index].IsVisible = false;
                        }
                    }
                }
            }
        }

        private void dataGridProductos_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            this.labelPartidas.Text = string.Format("Partidas: {0}", this.dataGridProductos.Rows.Count);
        }
    }
}
