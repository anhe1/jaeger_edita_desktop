﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Almacen.Builder;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.UI.Tienda.Forms {
    /// <summary>
    /// catalogo de productos
    /// </summary>
    public partial class ProductosCatalogoForm : RadForm {
        #region declaraciones
        protected ICatalogoProductosService service;
        private List<IClasificacionSingle> categorias;
        private BindingList<ProductoServicioXDetailModel> productoServicios;
        private List<UnidadModel> unidades;
        private BindingList<EspecificacionModel> especifiaciones;
        private List<VisibilidadModel> visibilidad;
        private List<VisibilidadModel> visibilidad1;
        private List<ProductoServicioTipoModel> tipos;
        protected internal RadContextMenu conextMenuModelos = new RadContextMenu();
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuModelosAplicar = new RadMenuItem { Text = "Aplica a todos" };
        protected internal RadMenuItem RegistrosInactivos = new RadMenuItem { Text = "Registro inactivos", IsChecked = false, CheckOnClick = true };
        #endregion

        public ProductosCatalogoForm() {
            InitializeComponent();

            BaseGridBehavior gridBehavior = this.GProducto.GridBehavior as BaseGridBehavior;
            gridBehavior.UnregisterBehavior(typeof(GridViewDataRowInfo));
            gridBehavior.RegisterBehavior(typeof(GridViewDataRowInfo), new CustomGridRowBehavior());
        }

        private void ProductoCatalogoForm_Load(object sender, EventArgs e) {
            using (IProductoGridBuilder view = new ProductoGridBuilder()) {
                this.GProducto.Name = "Productos";
                this.GModelos.Name = "Modelos";
                this.GProducto.Standard();
                this.GModelos.Standard();
                this.GProducto.Columns.AddRange(view.Templetes().Master().Build());
                this.GModelos.Columns.AddRange(view.Templetes().ModelosTienda().Build());
                this.gVariante.Standard();
            }

            this.gImagen.TableElement.RowHeight = 300;
            this.Formato_Condicional();

            this.TProductos.Herramientas.Items.Add(this.RegistrosInactivos);
            this.TProductos.Nuevo.Click += this.TProductos_Nuevo_Click;
            this.TProductos.Editar.Click += this.TProductos_Editar_Click;
            this.TProductos.Remover.Click += this.TProductos_Remover_Click;
            this.TProductos.Actualizar.Click += this.TProductos_Actualizar_Click;
            this.TProductos.Filtro.CheckStateChanged += TFiltro_CheckStateChanged;
            this.TProductos.Cerrar.Click += this.TProductos_Cerrar_Click;

            this.TModelos.Nuevo.Click += this.TModelos_Agregar_Click;
            this.TModelos.Editar.Click += this.TModelos_Editar_Click;
            this.TModelos.Remover.Click += this.TModelos_Remover_Click;
            this.TModelos.Autorizar.Click += this.TModelos_Autorizar_Click;
            this.TModelos.Filtro.CheckStateChanged += TFiltro_CheckStateChanged;

            this.TDisponible.Nuevo.Click += this.TDisponible_Nuevo_Click;
            this.TDisponible.Nuevo.ToolTipText = "Al agregar la disponibilidad el precio sera basado en lista de precios.";
            this.TDisponible.Remover.Click += this.TDisponible_Remover_Click;

            this.TImagen.Nuevo.Click += this.Nuevo_Click;
            this.TImagen.Remover.Click += this.Remover_Click;

            var cboFactorTrasladoIVA = this.GModelos.GetColumn<GridViewComboBoxColumn>("FactorTrasladoIVA");
            cboFactorTrasladoIVA.DisplayMember = "Descripcion";
            cboFactorTrasladoIVA.ValueMember = "Descripcion";
            cboFactorTrasladoIVA.DataSource = Aplication.Almacen.Services.CatalogoProductoService.FactorIVA();

            this.menuModelosAplicar.Click += this.MenuModelosAplicar_Click;
            this.conextMenuModelos.Items.Add(this.menuModelosAplicar);

            this.GProducto.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.RadGridImagen_ContextMenuOpening);
            this.GProducto.EditorRequired += this.GProducto_EditorRequired;
            this.GModelos.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.RadGridImagen_ContextMenuOpening);
            this.gImagen.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.RadGridImagen_ContextMenuOpening);
        }

        #region barra de herramientas productos
        public virtual void TProductos_Nuevo_Click(object sender, EventArgs e) {
            if (this.categorias == null || this.categorias.Count == 0) {
                using (var espera = new Waiting1Form(this.Categorias_Consultar)) {
                    espera.Text = "Consultando categorías";
                    espera.ShowDialog(this);
                }
            }
            var d1 = new ProductoForm(this.service, new Aplication.Tienda.Builder.ProductoModeloBuilder().Almacen(Domain.Base.ValueObjects.AlmacenEnum.PT).Tipo(Domain.Base.ValueObjects.ProdServTipoEnum.Producto).AddDescripcion("").Build<ProductoServicioXDetailModel>()) {
                _Categorias = new List<IClasificacionSingle>(this.categorias.ToList())
            };
            d1.Agregate += this.TProducto_Agregar;
            d1.ShowDialog(this);
        }

        private void TProducto_Agregar(object sender, ProductoServicioXDetailModel e) {
            if (e != null) {
                this.productoServicios?.Add(e);
            }
        }

        public virtual void TProductos_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioXDetailModel;
            var d1 = new ProductoForm(this.service, seleccionado) {
                _Categorias = new List<IClasificacionSingle>(this.categorias.ToList())
            };
            d1.ShowDialog(this);
        }

        public virtual void TProductos_Remover_Click(object sender, EventArgs e) {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioXDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdProducto == 0) {
                        this.GProducto.Rows.Remove(this.GProducto.CurrentRow);
                    } else {
                        if (RadMessageBox.Show(this, Properties.Resources.msg_producto_remover, "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                            return;

                        using (var espera = new Waiting2Form(this.Producto_Remover)) {
                            espera.Text = "Eliminando producto con sus modelos, espere...";
                            espera.ShowDialog(this);
                        }
                    }
                }
            }
        }

        public virtual void TProductos_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Producto_Consultar)) {
                espera.Text = "Espere un momento ...";
                espera.ShowDialog(this);
            }

            var cboCategoria = this.GProducto.Columns["IdCategoria"] as GridViewComboBoxColumn;
            cboCategoria.DataSource = this.categorias;
            cboCategoria.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboCategoria.AutoCompleteMode = AutoCompleteMode.Suggest;
            cboCategoria.DropDownStyle = RadDropDownStyle.DropDownList;
            cboCategoria.DisplayMember = "Descripcion";
            cboCategoria.ValueMember = "IdCategoria";

            var cboEspecificacion = this.gVariante.Columns["IdEspecificacion"] as GridViewComboBoxColumn;
            cboEspecificacion.DataSource = this.especifiaciones;
            cboEspecificacion.DisplayMember = "Descripcion";
            cboEspecificacion.ValueMember = "IdEspecificacion";

            // catalogo de unidades del almacen
            GridViewComboBoxColumn comboUnidades = this.GModelos.Columns["IdUnidad"] as GridViewComboBoxColumn;
            comboUnidades.DisplayMember = "Descripcion";
            comboUnidades.ValueMember = "IdUnidad";
            comboUnidades.AutoSizeMode = BestFitColumnMode.DisplayedCells;
            comboUnidades.DropDownStyle = RadDropDownStyle.DropDownList;
            comboUnidades.DataSource = this.unidades;

            var cboUnidadXY = this.GModelos.Columns["UnidadXY"] as GridViewComboBoxColumn;
            cboUnidadXY.DisplayMember = "Descripcion";
            cboUnidadXY.ValueMember = "IdUnidad";
            cboUnidadXY.DataSource = new BindingList<UnidadModel>(this.unidades);

            var cboUnidadZ = this.GModelos.Columns["UnidadZ"] as GridViewComboBoxColumn;
            cboUnidadZ.DisplayMember = "Descripcion";
            cboUnidadZ.ValueMember = "IdUnidad";
            cboUnidadZ.DataSource = new BindingList<UnidadModel>(this.unidades);

            GridViewComboBoxColumn cboVisible = this.GModelos.Columns["IdVisible"] as GridViewComboBoxColumn;
            cboVisible.DisplayMember = "Descripcion";
            cboVisible.ValueMember = "Id";
            cboVisible.DataSource = this.visibilidad;

            GridViewComboBoxColumn cboTipo = this.GModelos.Columns["Tipo"] as GridViewComboBoxColumn;
            cboTipo.DisplayMember = "Descripcion";
            cboTipo.ValueMember = "Id";
            cboTipo.DataSource = this.tipos;

            GridViewComboBoxColumn cboVisible1 = this.gVariante.Columns["IdVisibilidad"] as GridViewComboBoxColumn;
            cboVisible1.DisplayMember = "Descripcion";
            cboVisible1.ValueMember = "Id";
            cboVisible1.DataSource = this.visibilidad1;

            this.GProducto.DataSource = this.productoServicios;
            this.GModelos.DataSource = this.productoServicios;
            this.gImagen.DataSource = this.productoServicios;
            this.gVariante.DataSource = this.productoServicios;
            this.GModelos.DataMember = "Modelos";
            this.gImagen.DataMember = "Modelos.Medios";
            this.gVariante.DataMember = "Modelos.Disponibles";
        }

        public virtual void TFiltro_CheckStateChanged(object sender, EventArgs e) {
            var a = (CommandBarToggleButton)sender;
            this.TProductos.Filtro.ToggleState = a.ToggleState;
            this.TModelos.Filtro.ToggleState = a.ToggleState;
            this.GProducto.ShowFilteringRow = a.ToggleState == ToggleState.On;
            this.GModelos.ShowFilteringRow = this.GProducto.ShowFilteringRow;
            if (this.GProducto.ShowFilteringRow == false) {
                this.GProducto.FilterDescriptors.Clear();
                this.GModelos.FilterDescriptors.Clear();
            }
        }

        public virtual void TProductos_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #endregion

        #region barra de herramientas modelos
        /// <summary>
        /// agregar un modelo
        /// </summary>
        public virtual void TModelos_Agregar_Click(object sender, EventArgs e) {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioXDetailModel;
                if (seleccionado != null) {
                    var dn = new Aplication.Tienda.Builder.ProductoModeloBuilder().Almacen(Domain.Base.ValueObjects.AlmacenEnum.PT).Tipo(Domain.Base.ValueObjects.ProdServTipoEnum.Producto).AddDescripcion("").Build<ModeloXDetailModel>();
                    dn.IdProducto = seleccionado.IdProducto;
                    dn.IdCategoria = seleccionado.IdCategoria;
                    var nuevo = new ProductoModeloForm(this.service, dn);
                    nuevo.Agregate += this.Nuevo_Agregate;
                    nuevo.ShowDialog(this);
                }
            }
        }

        public virtual void Nuevo_Agregate(object sender, ModeloXDetailModel e) {
            if (e != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioXDetailModel;
                if (seleccionado != null) {
                    seleccionado.Modelos.Add(e);
                }
            }
        }

        public virtual void TModelos_Editar_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 0) {
                    var nuevo = new ProductoModeloForm(this.service, seleccionado);
                    nuevo.ShowDialog(this);
                } else {
                    RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        public virtual void TModelos_Remover_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdModelo == 0) {
                        this.GModelos.Rows.Remove(this.GModelos.CurrentRow);
                    } else {
                        if (RadMessageBox.Show(this, Properties.Resources.msg_modelo_remover, "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                            return;
                        using (var espera = new Waiting1Form(this.Modelo_Remover)) {
                            espera.Text = "Desactivando registro";
                            espera.ShowDialog(this);
                        }
                    }
                }
            }
        }

        public virtual void TModelos_Autorizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Modelo_Autorizar)) {
                espera.Text = "Autorizando";
                espera.ShowDialog(this);
            }
        }

        public virtual void TModelos_Imagen_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.jpg|*.JPG" };
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            if (System.IO.File.Exists(openFile.FileName) == false) {
                RadMessageBox.Show("No existe el archivo");
                return;
            }
            var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
            var nuevo = this.service.Save(new MedioModel(), openFile.FileName, string.Format("{0}-{1}.jpg", seleccionado.IdModelo.ToString(), seleccionado.Descripcion.ToLower().Replace(" ", "-").Trim()));
            seleccionado.Medios.Add(nuevo);
            this.service.Save(seleccionado);
        }
        #endregion

        #region imagenes
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.jpg|*.JPG|*.jpeg|*.JPEG|*.png|*.PNG" };
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            if (System.IO.File.Exists(openFile.FileName) == false) {
                RadMessageBox.Show("No existe el archivo");
                return;
            }

            var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
            var nuevo = this.service.Save(new MedioModel() { IdModelo = seleccionado.IdModelo }, openFile.FileName, string.Format("{0}-{1}-{2}.png", seleccionado.NoIdentificacion, seleccionado.Descripcion.ToLower().Replace(" ", "-").Trim(), DateTime.Now.ToString("yyyyMMddHHmmss")));
            seleccionado.Medios.Add(nuevo);
            this.service.Save(seleccionado);
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            if (this.gImagen.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_remover_objeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    var seleccionado = this.gImagen.CurrentRow.DataBoundItem as MedioModel;
                    if (seleccionado.Id > 0) {
                        if (this.service.Remover(seleccionado)) {
                            try {
                                this.gImagen.Rows.Remove(this.gImagen.CurrentRow);
                            } catch (Exception ex) {
                                RadMessageBox.Show(this, "Error: " + ex.Message, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region disponibles
        public virtual void TDisponible_Nuevo_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 0) {
                    if (seleccionado.Unitario > 0) {
                        RadMessageBox.Show(this, Properties.Resources.msg_variante_agregar_error, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    seleccionado.Disponibles.Add(new ModeloYEspecificacionModel());
                } else {
                    RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        public virtual void TDisponible_Remover_Click(object sender, EventArgs e) {
            if (this.gVariante.CurrentRow != null) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 0) {
                    var variante = this.gVariante.CurrentRow.DataBoundItem as ModeloYEspecificacionModel;
                    if (variante.Id == 0) {
                        this.gVariante.Rows.Remove(this.gVariante.CurrentRow);
                    } else {
                        variante.Activo = false;
                        this.gVariante.Tag = variante;
                        this.gVariante.CurrentRow.IsVisible = false;
                        using (var espera = new Waiting2Form(this.Variante_Guardar)) {
                            espera.Text = "Guardando ...";
                            espera.ShowDialog(this);
                        }
                    }
                } else {
                    RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }
        #endregion

        #region acciones del grid
        private void RadGridView_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (this.GProducto.CurrentRow is GridViewFilteringRowInfo | this.GModelos.CurrentRow is GridViewFilteringRowInfo) {
                return;
            }

            if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.GModelos.Name |
                e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.gVariante.Name) {
                var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 1)
                    e.Cancel = true;
                return;
            }

            if (e.Column.Name == "IdPrecio") {
                if (e.Column.Tag == null) {
                    e.Column.Tag = true;
                    var editor = (RadMultiColumnComboBoxElement)this.GModelos.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("IdPrecio", "IdPrecio") { IsVisible = false });
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Nombre", "Nombre"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Descripción", "Descripcion"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Unitario", "Unitario") { FormatString = "{0:N2}", TextAlignment = System.Drawing.ContentAlignment.BottomRight });
                    editor.AutoSizeDropDownToBestFit = true;
                }
            } else if (e.Column.Name == "IdCategoria") {
                // solo se podra modificar la categoria por el formulario de edicion
                e.Cancel = true;
                if (e.Column.Tag == null) {
                    e.Column.Tag = true;
                    var editor = (RadMultiColumnComboBoxElement)this.GProducto.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    editor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("IdCategoria", "IdCategoria") { IsVisible = false });
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("IdSubCategoria", "IdSubCategoria") { IsVisible = false });
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Descripción", "Descripcion"));
                    editor.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
                    editor.EditorControl.BestFitColumns();
                    editor.AutoSizeDropDownToBestFit = true;
                    editor.AutoSizeMode = RadAutoSizeMode.WrapAroundChildren;
                    editor.AutoSizeDropDownColumnMode = BestFitColumnMode.DisplayedCells;
                    editor.AutoSizeDropDownToBestFit = true;
                    editor.AutoSizeDropDownHeight = true;
                }
            }
        }

        private void RadGridView_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (this.TProductos.Tag != null) {
                if ((bool)this.TProductos.Tag == true) {
                    if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.GModelos.Name) {
                        var seleccionado = e.Row.DataBoundItem as ModeloXDetailModel;
                        if (seleccionado != null) {
                            this.GModelos.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Modelo_Guardar)) {
                                espera.Text = "Guardando...";
                                espera.ShowDialog(this);
                            }
                        }
                    } else if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.GProducto.Name) {
                        var seleccionado = e.Row.DataBoundItem as ProductoServicioXDetailModel;
                        if (seleccionado != null) {
                            this.GProducto.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Producto_Guardar)) {
                                espera.Text = "Guardando ...";
                                espera.ShowDialog(this);
                            }
                        }
                    } else if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.gVariante.Name) {
                        var seleccionado = e.Row.DataBoundItem as ModeloYEspecificacionModel;
                        if (seleccionado != null) {
                            this.gVariante.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Variante_Guardar)) {
                                espera.Text = "Guardando ...";
                                espera.ShowDialog(this);
                            }
                        }
                    }
                }
            }
            this.TProductos.Tag = null;
        }

        private void RadGridView_RowsChanging(object sender, GridViewCollectionChangingEventArgs e) {
            if (e.Action == Telerik.WinControls.Data.NotifyCollectionChangedAction.ItemChanging) {
                if (e.OldValue == e.NewValue) {
                    e.Cancel = true;
                } else
                    this.TProductos.Tag = true;
            }
        }

        private void RadGridModelos_CommandCellClick(object sender, GridViewCellEventArgs e) {
            var seleccionado = e.Row.DataBoundItem as ModeloXDetailModel;
            if (seleccionado != null) {
                if (this.TModelos.Autorizar.Enabled) {
                    using (var espera = new Waiting2Form(this.Modelo_Autorizar)) {
                        espera.Text = "Guardando...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        private void RadGridImagen_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {

            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GModelos.CurrentRow.ViewInfo.ViewTemplate == this.GModelos.MasterTemplate) {
                    e.ContextMenu = this.conextMenuModelos.DropDown;
                }
            }
        }

        private void GProducto_EditorRequired(object sender, EditorRequiredEventArgs e) {
            if (this.GProducto.CurrentColumn is GridViewComboBoxColumn && this.GProducto.CurrentRow is GridViewFilteringRowInfo) {
                RadTextBoxEditor editor = new RadTextBoxEditor();
                e.Editor = editor;
            }
        }
        #endregion

        #region menu contextual
        private void MenuModelosAplicar_Click(object sender, EventArgs e) {
            if (this.GModelos.CurrentRow != null) {
                if (this.GModelos.ChildRows.Count > 0) {
                    var d = this.GModelos.CurrentCell.Value;
                    foreach (var item in this.GModelos.ChildRows) {
                        var d1 = item.DataBoundItem as ModeloXDetailModel;
                        if (d1.Autorizado == 0) {
                            item.Cells[this.GModelos.CurrentColumn.Index].BeginEdit();
                            item.Cells[this.GModelos.CurrentColumn.Index].Value = d;
                            item.Cells[this.GModelos.CurrentColumn.Index].EndEdit();
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Formato_Condicional() {
            this.GModelos.Columns["Activo"].ConditionalFormattingObjectList.Add(TelerikGridExtension.RowActive());
        }

        private void Producto_Consultar() {
            this.Categorias_Consultar();
            var queryProductos = new Domain.Almacen.Builder.ProductosQueryBuilder().Activo().Build();
            this.productoServicios = new BindingList<ProductoServicioXDetailModel>(this.service.GetList<ProductoServicioXDetailModel>(queryProductos).ToList());
            this.unidades = this.service.GetList<UnidadModel>(Aplication.Almacen.Services.UnidadService.Query().ByAlmacen(this.service.Almacen).Activo().Build()).ToList();
            this.especifiaciones = this.service.GetEspecificaciones();
            this.visibilidad = this.service.GetVisibilidad();
            this.visibilidad1 = this.service.GetVisibilidad();
            this.tipos = Aplication.Almacen.Services.CatalogoProductoService.GetTipos();
        }

        private void Categorias_Consultar() {
            this.categorias = this.service.GetClasificacion();
        }

        private void Producto_Guardar() {
            var seleccionado = this.GProducto.Tag as ProductoServicioXDetailModel;
            if (seleccionado != null) {
                this.service.Save(seleccionado);
            }
            this.GProducto.Tag = null;
        }

        private void Producto_Remover() {
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioXDetailModel;
                if (seleccionado != null) {
                    seleccionado.Activo = false;
                    this.service.Remover<ProductoServicioXDetailModel>(seleccionado);
                }
            }
        }

        private void Modelo_Guardar() {
            var seleccionado = this.GModelos.Tag as ModeloXDetailModel;
            if (seleccionado != null) {
                this.service.Save<ModeloXDetailModel>(seleccionado);
            }
            this.GModelos.Tag = null;
        }

        private void Modelo_Autorizar() {
            var lista = new List<ModeloXDetailModel>();
            lista.AddRange(this.GModelos.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as ModeloXDetailModel));
            for (int i = 0; i < lista.Count; i++) {
                lista[i].Autorizado = (lista[i].Autorizado == 1 ? 0 : 1);
                this.service.Save(lista[i]);
            }
        }

        private void Modelo_Remover() {
            var seleccionado = this.GModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
            this.service.Remover<ModeloXDetailModel>(seleccionado);
        }

        private void Variante_Guardar() {
            var seleccionado = this.gVariante.Tag as ModeloYEspecificacionModel;
            if (seleccionado != null) {
                this.service.Save(seleccionado);
            }
            this.gVariante.Tag = null;
        }
        #endregion

        private class CustomGridRowBehavior : GridDataRowBehavior {
            protected override bool ProcessTabKey(KeyEventArgs keys) {
                if (this.GridControl.CurrentColumn.Name == "Categoría") {
                    this.GridControl.GridNavigator.SelectLastColumn();
                }
                return base.ProcessTabKey(keys);
            }
        }
        /// <summary>
        /// esta rutina solo se utilizo para subir las fotos del catalogo antiguo de modelos
        /// </summary>
        private void test() {
            var folder = @"C:\Users\anhed\OneDrive\Bolsa de regalo\productos";
            if (this.GProducto.CurrentRow != null) {
                var seleccionado = this.GProducto.CurrentRow.DataBoundItem as ProductoServicioXDetailModel;
                for (int i = 0; i < seleccionado.Modelos.Count; i++) {
                    if (seleccionado.Modelos[i].Medios.Count == 0) {
                        var uno = seleccionado.Modelos[i].Descripcion.Replace(" ", "-") + ".jpg";
                        uno = uno.Replace("-Papel-72x102", "");
                        uno = uno.Replace("-Papel-50x70", "");
                        uno = uno.Replace("-Chica", "");
                        uno = uno.Replace("-Mediana", "");
                        uno = uno.Replace("-Grande", "");
                        uno = uno.Replace("-Jumbo", "");
                        uno = uno.Replace("-Mini", "");
                        uno = uno.Replace("á", "a");
                        uno = uno.Replace("e", "e");
                        uno = uno.Replace("í", "i");
                        uno = uno.Replace("ó", "o");
                        uno = uno.Replace("ú", "u");
                        uno = uno.Replace("ñ", "n");
                        uno = folder + "\\" + uno;
                        var dos = seleccionado.Modelos[i].Descripcion.ToLower().Replace(" ", "-").Trim().Replace("ñ", "n").Replace("á", "a").Replace("e", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");
                        if (System.IO.File.Exists(uno)) {
                            Console.WriteLine(uno);
                            var nuevo = this.service.Save(new Domain.Almacen.Entities.MedioModel(), uno, string.Format("{0}-{1}.jpg", seleccionado.Modelos[i].IdModelo.ToString(), dos));
                            seleccionado.Modelos[i].Medios.Add(nuevo);
                            this.service.Save(seleccionado.Modelos[i]);
                        }
                    }
                }
            }
        }
    }
}
