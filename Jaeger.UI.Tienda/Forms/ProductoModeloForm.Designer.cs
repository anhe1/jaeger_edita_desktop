﻿namespace Jaeger.UI.Tienda.Forms {
    partial class ProductoModeloForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn9 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn3 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoModeloForm));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            this.panelProducto = new Telerik.WinControls.UI.RadPanel();
            this.Activo = new Telerik.WinControls.UI.RadCheckBox();
            this.txbModelo = new Telerik.WinControls.UI.RadTextBox();
            this.lblModelo = new Telerik.WinControls.UI.RadLabel();
            this.cboCategorias = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCategoria = new Telerik.WinControls.UI.RadLabel();
            this.cboTipo = new Telerik.WinControls.UI.RadDropDownList();
            this.lblTipo = new Telerik.WinControls.UI.RadLabel();
            this.txbMarca = new Telerik.WinControls.UI.RadTextBox();
            this.lblMarca = new Telerik.WinControls.UI.RadLabel();
            this.txbExpecificacion = new Telerik.WinControls.UI.RadTextBox();
            this.lblEspecificacion = new Telerik.WinControls.UI.RadLabel();
            this.txbCodigoBarras = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoBarras = new Telerik.WinControls.UI.RadLabel();
            this.Etiquetas = new Telerik.WinControls.UI.RadTextBox();
            this.lblEtiquetas = new Telerik.WinControls.UI.RadLabel();
            this.pageViews = new Telerik.WinControls.UI.RadPageView();
            this.pageViewGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.TxtSKU = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.gVariante = new Telerik.WinControls.UI.RadGridView();
            this.TDisponible = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gboxAlmacen = new Telerik.WinControls.UI.RadGroupBox();
            this.lblExistencia = new Telerik.WinControls.UI.RadLabel();
            this.lblStockMinimo = new Telerik.WinControls.UI.RadLabel();
            this.TxbStockMinimo = new Telerik.WinControls.UI.RadTextBox();
            this.TxbStockMaximo = new Telerik.WinControls.UI.RadTextBox();
            this.TxbExistencia = new Telerik.WinControls.UI.RadTextBox();
            this.lblStockMaximo = new Telerik.WinControls.UI.RadLabel();
            this.TxbStockReorden = new Telerik.WinControls.UI.RadTextBox();
            this.lblUnidad2 = new Telerik.WinControls.UI.RadLabel();
            this.lblReOrden = new Telerik.WinControls.UI.RadLabel();
            this.CboUnidadAlmacen = new Telerik.WinControls.UI.RadDropDownList();
            this.gboxPrecioBase = new Telerik.WinControls.UI.RadGroupBox();
            this.txbPrecioBase = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.lblPrecioBase = new Telerik.WinControls.UI.RadLabel();
            this.GroupBoxImpuestoRetenido = new Telerik.WinControls.UI.RadGroupBox();
            this.txbRetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbRetencionIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbRetencionIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.chkRentencionISR = new Telerik.WinControls.UI.RadCheckBox();
            this.chkRetnecionIVA = new Telerik.WinControls.UI.RadCheckBox();
            this.lblIEPS2 = new Telerik.WinControls.UI.RadLabel();
            this.cboRetencionIEPS = new Telerik.WinControls.UI.RadDropDownList();
            this.gboxIpuestoTraslado = new Telerik.WinControls.UI.RadGroupBox();
            this.cboTrasladoIVA = new Telerik.WinControls.UI.RadDropDownList();
            this.txbTrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbTrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lblIEPS1 = new Telerik.WinControls.UI.RadLabel();
            this.cboTrasladoIEPS = new Telerik.WinControls.UI.RadDropDownList();
            this.lblIVA = new Telerik.WinControls.UI.RadLabel();
            this.gboxOtrasEspecificaciones = new Telerik.WinControls.UI.RadGroupBox();
            this.lblUnidad1 = new Telerik.WinControls.UI.RadLabel();
            this.txbLargo = new Telerik.WinControls.UI.RadTextBox();
            this.lblLargo = new Telerik.WinControls.UI.RadLabel();
            this.txbAncho = new Telerik.WinControls.UI.RadTextBox();
            this.cboUnidadZ = new Telerik.WinControls.UI.RadDropDownList();
            this.lblAncho = new Telerik.WinControls.UI.RadLabel();
            this.cboUnidadXY = new Telerik.WinControls.UI.RadDropDownList();
            this.txbAlto = new Telerik.WinControls.UI.RadTextBox();
            this.lblCalibre = new Telerik.WinControls.UI.RadLabel();
            this.pageViewProveedor = new Telerik.WinControls.UI.RadPageViewPage();
            this.TProveedor = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gridProveedor = new Telerik.WinControls.UI.RadGridView();
            this.pageViewPublicacion = new Telerik.WinControls.UI.RadPageViewPage();
            this.Publicacion = new Zoople.HTMLEditControl();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Seccion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.TProducto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).BeginInit();
            this.panelProducto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbMarca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMarca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbExpecificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEspecificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCodigoBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Etiquetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtiquetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageViews)).BeginInit();
            this.pageViews.SuspendLayout();
            this.pageViewGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSKU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxAlmacen)).BeginInit();
            this.gboxAlmacen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblExistencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMaximo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbExistencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMaximo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockReorden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUnidadAlmacen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxPrecioBase)).BeginInit();
            this.gboxPrecioBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbPrecioBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrecioBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxImpuestoRetenido)).BeginInit();
            this.GroupBoxImpuestoRetenido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRentencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetnecionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRetencionIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxIpuestoTraslado)).BeginInit();
            this.gboxIpuestoTraslado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxOtrasEspecificaciones)).BeginInit();
            this.gboxOtrasEspecificaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbLargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAncho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAncho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAlto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCalibre)).BeginInit();
            this.pageViewProveedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor.MasterTemplate)).BeginInit();
            this.pageViewPublicacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Seccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seccion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seccion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panelProducto
            // 
            this.panelProducto.Controls.Add(this.Activo);
            this.panelProducto.Controls.Add(this.txbModelo);
            this.panelProducto.Controls.Add(this.lblModelo);
            this.panelProducto.Controls.Add(this.cboCategorias);
            this.panelProducto.Controls.Add(this.lblCategoria);
            this.panelProducto.Controls.Add(this.cboTipo);
            this.panelProducto.Controls.Add(this.lblTipo);
            this.panelProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelProducto.Location = new System.Drawing.Point(0, 30);
            this.panelProducto.Name = "panelProducto";
            this.panelProducto.Size = new System.Drawing.Size(741, 82);
            this.panelProducto.TabIndex = 1;
            // 
            // Activo
            // 
            this.Activo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Activo.Location = new System.Drawing.Point(514, 55);
            this.Activo.Name = "Activo";
            this.Activo.Size = new System.Drawing.Size(51, 18);
            this.Activo.TabIndex = 8;
            this.Activo.Text = "Activo";
            // 
            // txbModelo
            // 
            this.txbModelo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbModelo.Location = new System.Drawing.Point(65, 55);
            this.txbModelo.Name = "txbModelo";
            this.txbModelo.NullText = "Modelo";
            this.txbModelo.Size = new System.Drawing.Size(443, 20);
            this.txbModelo.TabIndex = 7;
            // 
            // lblModelo
            // 
            this.lblModelo.Location = new System.Drawing.Point(12, 56);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(47, 18);
            this.lblModelo.TabIndex = 6;
            this.lblModelo.Text = "Modelo:";
            // 
            // cboCategorias
            // 
            this.cboCategorias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCategorias.AutoSizeDropDownToBestFit = true;
            this.cboCategorias.DisplayMember = "Descriptor";
            this.cboCategorias.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // cboCategorias.NestedRadGridView
            // 
            this.cboCategorias.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboCategorias.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategorias.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboCategorias.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboCategorias.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboCategorias.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn55.DataType = typeof(int);
            gridViewTextBoxColumn55.FieldName = "IdProducto";
            gridViewTextBoxColumn55.HeaderText = "IdProducto";
            gridViewTextBoxColumn55.IsVisible = false;
            gridViewTextBoxColumn55.Name = "IdProducto";
            gridViewTextBoxColumn55.VisibleInColumnChooser = false;
            gridViewTextBoxColumn56.DataType = typeof(int);
            gridViewTextBoxColumn56.FieldName = "IdCategoria";
            gridViewTextBoxColumn56.HeaderText = "IdCategoria";
            gridViewTextBoxColumn56.IsVisible = false;
            gridViewTextBoxColumn56.Name = "IdCategoria";
            gridViewTextBoxColumn56.VisibleInColumnChooser = false;
            gridViewTextBoxColumn57.DataType = typeof(int);
            gridViewTextBoxColumn57.FieldName = "IdSubCategoria";
            gridViewTextBoxColumn57.HeaderText = "IdSubCategoria";
            gridViewTextBoxColumn57.IsVisible = false;
            gridViewTextBoxColumn57.Name = "IdSubCategoria";
            gridViewTextBoxColumn57.VisibleInColumnChooser = false;
            gridViewTextBoxColumn58.FieldName = "Codigo";
            gridViewTextBoxColumn58.HeaderText = "Codigo";
            gridViewTextBoxColumn58.Name = "Codigo";
            gridViewTextBoxColumn59.FieldName = "Clase";
            gridViewTextBoxColumn59.HeaderText = "Clase";
            gridViewTextBoxColumn59.Name = "Clase";
            gridViewTextBoxColumn60.FieldName = "Categoria";
            gridViewTextBoxColumn60.HeaderText = "Categoria";
            gridViewTextBoxColumn60.IsVisible = false;
            gridViewTextBoxColumn60.Name = "Categoria";
            gridViewTextBoxColumn60.VisibleInColumnChooser = false;
            gridViewTextBoxColumn61.FieldName = "Nombre";
            gridViewTextBoxColumn61.HeaderText = "Producto";
            gridViewTextBoxColumn61.Name = "Nombre";
            gridViewTextBoxColumn61.Width = 100;
            gridViewTextBoxColumn62.FieldName = "Descriptor";
            gridViewTextBoxColumn62.HeaderText = "Descriptor";
            gridViewTextBoxColumn62.Name = "Descriptor";
            this.cboCategorias.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62});
            this.cboCategorias.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboCategorias.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboCategorias.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.cboCategorias.EditorControl.Name = "NestedRadGridView";
            this.cboCategorias.EditorControl.ReadOnly = true;
            this.cboCategorias.EditorControl.ShowGroupPanel = false;
            this.cboCategorias.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboCategorias.EditorControl.TabIndex = 0;
            this.cboCategorias.Location = new System.Drawing.Point(15, 28);
            this.cboCategorias.Name = "cboCategorias";
            this.cboCategorias.NullText = "Categoría";
            this.cboCategorias.Size = new System.Drawing.Size(714, 20);
            this.cboCategorias.TabIndex = 3;
            this.cboCategorias.TabStop = false;
            this.cboCategorias.ValueMember = "IdProducto";
            this.cboCategorias.SelectedValueChanged += new System.EventHandler(this.CboCategorias_SelectedValueChanged);
            // 
            // lblCategoria
            // 
            this.lblCategoria.Location = new System.Drawing.Point(12, 6);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(113, 18);
            this.lblCategoria.TabIndex = 2;
            this.lblCategoria.Text = "Categoría / Producto:";
            // 
            // cboTipo
            // 
            this.cboTipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTipo.DisplayMember = "Descripcion";
            this.cboTipo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cboTipo.Location = new System.Drawing.Point(614, 54);
            this.cboTipo.Name = "cboTipo";
            this.cboTipo.Size = new System.Drawing.Size(116, 20);
            this.cboTipo.TabIndex = 1;
            this.cboTipo.ValueMember = "Id";
            // 
            // lblTipo
            // 
            this.lblTipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipo.Location = new System.Drawing.Point(577, 55);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(31, 18);
            this.lblTipo.TabIndex = 0;
            this.lblTipo.Text = "Tipo:";
            // 
            // txbMarca
            // 
            this.txbMarca.Location = new System.Drawing.Point(47, 8);
            this.txbMarca.Name = "txbMarca";
            this.txbMarca.NullText = "Marca";
            this.txbMarca.Size = new System.Drawing.Size(314, 20);
            this.txbMarca.TabIndex = 9;
            // 
            // lblMarca
            // 
            this.lblMarca.Location = new System.Drawing.Point(2, 9);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(39, 18);
            this.lblMarca.TabIndex = 8;
            this.lblMarca.Text = "Marca:";
            // 
            // txbExpecificacion
            // 
            this.txbExpecificacion.Location = new System.Drawing.Point(454, 8);
            this.txbExpecificacion.Name = "txbExpecificacion";
            this.txbExpecificacion.NullText = "Especificación";
            this.txbExpecificacion.Size = new System.Drawing.Size(261, 20);
            this.txbExpecificacion.TabIndex = 11;
            // 
            // lblEspecificacion
            // 
            this.lblEspecificacion.Location = new System.Drawing.Point(367, 9);
            this.lblEspecificacion.Name = "lblEspecificacion";
            this.lblEspecificacion.Size = new System.Drawing.Size(78, 18);
            this.lblEspecificacion.TabIndex = 10;
            this.lblEspecificacion.Text = "Especificación:";
            // 
            // txbCodigoBarras
            // 
            this.txbCodigoBarras.Location = new System.Drawing.Point(89, 33);
            this.txbCodigoBarras.Name = "txbCodigoBarras";
            this.txbCodigoBarras.NullText = "Cod. de Barras";
            this.txbCodigoBarras.Size = new System.Drawing.Size(150, 20);
            this.txbCodigoBarras.TabIndex = 13;
            // 
            // lblCodigoBarras
            // 
            this.lblCodigoBarras.Location = new System.Drawing.Point(2, 34);
            this.lblCodigoBarras.Name = "lblCodigoBarras";
            this.lblCodigoBarras.Size = new System.Drawing.Size(81, 18);
            this.lblCodigoBarras.TabIndex = 12;
            this.lblCodigoBarras.Text = "Cod. de Barras:";
            // 
            // Etiquetas
            // 
            this.Etiquetas.Enabled = false;
            this.Etiquetas.Location = new System.Drawing.Point(504, 33);
            this.Etiquetas.Name = "Etiquetas";
            this.Etiquetas.NullText = "Etiqueta de Busqueda";
            this.Etiquetas.Size = new System.Drawing.Size(211, 20);
            this.Etiquetas.TabIndex = 15;
            // 
            // lblEtiquetas
            // 
            this.lblEtiquetas.Location = new System.Drawing.Point(448, 34);
            this.lblEtiquetas.Name = "lblEtiquetas";
            this.lblEtiquetas.Size = new System.Drawing.Size(54, 18);
            this.lblEtiquetas.TabIndex = 14;
            this.lblEtiquetas.Text = "Etiquetas:";
            // 
            // pageViews
            // 
            this.pageViews.Controls.Add(this.pageViewGeneral);
            this.pageViews.Controls.Add(this.pageViewProveedor);
            this.pageViews.Controls.Add(this.pageViewPublicacion);
            this.pageViews.DefaultPage = this.pageViewGeneral;
            this.pageViews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageViews.Location = new System.Drawing.Point(0, 112);
            this.pageViews.Name = "pageViews";
            this.pageViews.SelectedPage = this.pageViewGeneral;
            this.pageViews.Size = new System.Drawing.Size(741, 412);
            this.pageViews.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageViews.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageViews.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Bottom;
            // 
            // pageViewGeneral
            // 
            this.pageViewGeneral.Controls.Add(this.TxtSKU);
            this.pageViewGeneral.Controls.Add(this.radLabel2);
            this.pageViewGeneral.Controls.Add(this.radGroupBox2);
            this.pageViewGeneral.Controls.Add(this.gboxAlmacen);
            this.pageViewGeneral.Controls.Add(this.gboxPrecioBase);
            this.pageViewGeneral.Controls.Add(this.gboxIpuestoTraslado);
            this.pageViewGeneral.Controls.Add(this.gboxOtrasEspecificaciones);
            this.pageViewGeneral.Controls.Add(this.Etiquetas);
            this.pageViewGeneral.Controls.Add(this.lblMarca);
            this.pageViewGeneral.Controls.Add(this.lblEtiquetas);
            this.pageViewGeneral.Controls.Add(this.txbMarca);
            this.pageViewGeneral.Controls.Add(this.txbCodigoBarras);
            this.pageViewGeneral.Controls.Add(this.lblEspecificacion);
            this.pageViewGeneral.Controls.Add(this.lblCodigoBarras);
            this.pageViewGeneral.Controls.Add(this.txbExpecificacion);
            this.pageViewGeneral.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.pageViewGeneral.Location = new System.Drawing.Point(10, 10);
            this.pageViewGeneral.Name = "pageViewGeneral";
            this.pageViewGeneral.Size = new System.Drawing.Size(720, 364);
            this.pageViewGeneral.Text = "General";
            // 
            // TxtSKU
            // 
            this.TxtSKU.Location = new System.Drawing.Point(280, 33);
            this.TxtSKU.Name = "TxtSKU";
            this.TxtSKU.NullText = "SKU";
            this.TxtSKU.Size = new System.Drawing.Size(156, 20);
            this.TxtSKU.TabIndex = 43;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(245, 34);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(29, 18);
            this.radLabel2.TabIndex = 42;
            this.radLabel2.Text = "SKU:";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.gVariante);
            this.radGroupBox2.Controls.Add(this.TDisponible);
            this.radGroupBox2.Controls.Add(this.GroupBoxImpuestoRetenido);
            this.radGroupBox2.HeaderText = "Variantes";
            this.radGroupBox2.Location = new System.Drawing.Point(3, 190);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(712, 167);
            this.radGroupBox2.TabIndex = 41;
            this.radGroupBox2.Text = "Variantes";
            // 
            // gVariante
            // 
            this.gVariante.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnF2;
            this.gVariante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gVariante.Location = new System.Drawing.Point(2, 48);
            // 
            // 
            // 
            this.gVariante.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn7.DataType = typeof(int);
            gridViewComboBoxColumn7.FieldName = "IdEspecificacion";
            gridViewComboBoxColumn7.HeaderText = "Variante";
            gridViewComboBoxColumn7.Name = "IdEspecificacion";
            gridViewComboBoxColumn7.Width = 200;
            gridViewComboBoxColumn8.FieldName = "IdVisibilidad";
            gridViewComboBoxColumn8.HeaderText = "Visibilidad";
            gridViewComboBoxColumn8.Name = "IdVisibilidad";
            gridViewComboBoxColumn8.Width = 80;
            gridViewTextBoxColumn63.DataType = typeof(decimal);
            gridViewTextBoxColumn63.FieldName = "Minimo";
            gridViewTextBoxColumn63.FormatString = "{0:N2}";
            gridViewTextBoxColumn63.HeaderText = "Minimo";
            gridViewTextBoxColumn63.Name = "Minimo";
            gridViewTextBoxColumn63.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn63.VisibleInColumnChooser = false;
            gridViewTextBoxColumn63.Width = 65;
            gridViewTextBoxColumn64.DataType = typeof(decimal);
            gridViewTextBoxColumn64.FieldName = "Maximo";
            gridViewTextBoxColumn64.FormatString = "{0:N2}";
            gridViewTextBoxColumn64.HeaderText = "Maximo";
            gridViewTextBoxColumn64.Name = "Maximo";
            gridViewTextBoxColumn64.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn64.VisibleInColumnChooser = false;
            gridViewTextBoxColumn64.Width = 65;
            gridViewTextBoxColumn65.DataType = typeof(decimal);
            gridViewTextBoxColumn65.FieldName = "ReOrden";
            gridViewTextBoxColumn65.FormatString = "{0:N2}";
            gridViewTextBoxColumn65.HeaderText = "ReOrden";
            gridViewTextBoxColumn65.Name = "ReOrden";
            gridViewTextBoxColumn65.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn65.VisibleInColumnChooser = false;
            gridViewTextBoxColumn65.Width = 65;
            gridViewTextBoxColumn66.DataType = typeof(decimal);
            gridViewTextBoxColumn66.FieldName = "Existencia";
            gridViewTextBoxColumn66.FormatString = "{0:N2}";
            gridViewTextBoxColumn66.HeaderText = "Existencia";
            gridViewTextBoxColumn66.Name = "Existencia";
            gridViewTextBoxColumn66.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn66.Width = 65;
            gridViewComboBoxColumn9.DataType = typeof(int);
            gridViewComboBoxColumn9.FieldName = "IdUnidad";
            gridViewComboBoxColumn9.HeaderText = "Unidad";
            gridViewComboBoxColumn9.Name = "IdUnidad";
            gridViewComboBoxColumn9.Width = 95;
            gridViewTextBoxColumn67.FieldName = "Modifica";
            gridViewTextBoxColumn67.HeaderText = "Modifica";
            gridViewTextBoxColumn67.IsVisible = false;
            gridViewTextBoxColumn67.Name = "Modifica";
            gridViewTextBoxColumn67.ReadOnly = true;
            gridViewTextBoxColumn67.VisibleInColumnChooser = false;
            gridViewTextBoxColumn67.Width = 75;
            gridViewTextBoxColumn68.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn68.FieldName = "FechaModifica";
            gridViewTextBoxColumn68.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn68.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn68.IsVisible = false;
            gridViewTextBoxColumn68.Name = "FechaModifica";
            gridViewTextBoxColumn68.ReadOnly = true;
            gridViewTextBoxColumn68.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn68.VisibleInColumnChooser = false;
            gridViewTextBoxColumn68.Width = 65;
            this.gVariante.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn7,
            gridViewComboBoxColumn8,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewComboBoxColumn9,
            gridViewTextBoxColumn67,
            gridViewTextBoxColumn68});
            this.gVariante.MasterTemplate.EnableFiltering = true;
            this.gVariante.MasterTemplate.ShowFilteringRow = false;
            this.gVariante.MasterTemplate.ShowRowHeaderColumn = false;
            this.gVariante.MasterTemplate.ViewDefinition = tableViewDefinition10;
            this.gVariante.Name = "gVariante";
            this.gVariante.ShowGroupPanel = false;
            this.gVariante.Size = new System.Drawing.Size(708, 117);
            this.gVariante.TabIndex = 7;
            // 
            // TDisponible
            // 
            this.TDisponible.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDisponible.Etiqueta = "Disponibilidad";
            this.TDisponible.Location = new System.Drawing.Point(2, 18);
            this.TDisponible.Name = "TDisponible";
            this.TDisponible.ReadOnly = false;
            this.TDisponible.ShowActualizar = false;
            this.TDisponible.ShowAutorizar = false;
            this.TDisponible.ShowCerrar = false;
            this.TDisponible.ShowEditar = false;
            this.TDisponible.ShowExportarExcel = false;
            this.TDisponible.ShowFiltro = true;
            this.TDisponible.ShowGuardar = false;
            this.TDisponible.ShowHerramientas = false;
            this.TDisponible.ShowImagen = false;
            this.TDisponible.ShowImprimir = false;
            this.TDisponible.ShowNuevo = true;
            this.TDisponible.ShowRemover = true;
            this.TDisponible.Size = new System.Drawing.Size(708, 30);
            this.TDisponible.TabIndex = 8;
            // 
            // gboxAlmacen
            // 
            this.gboxAlmacen.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxAlmacen.Controls.Add(this.lblExistencia);
            this.gboxAlmacen.Controls.Add(this.lblStockMinimo);
            this.gboxAlmacen.Controls.Add(this.TxbStockMinimo);
            this.gboxAlmacen.Controls.Add(this.TxbStockMaximo);
            this.gboxAlmacen.Controls.Add(this.TxbExistencia);
            this.gboxAlmacen.Controls.Add(this.lblStockMaximo);
            this.gboxAlmacen.Controls.Add(this.TxbStockReorden);
            this.gboxAlmacen.Controls.Add(this.lblUnidad2);
            this.gboxAlmacen.Controls.Add(this.lblReOrden);
            this.gboxAlmacen.Controls.Add(this.CboUnidadAlmacen);
            this.gboxAlmacen.HeaderText = "Almacén";
            this.gboxAlmacen.Location = new System.Drawing.Point(245, 60);
            this.gboxAlmacen.Name = "gboxAlmacen";
            this.gboxAlmacen.Size = new System.Drawing.Size(218, 124);
            this.gboxAlmacen.TabIndex = 18;
            this.gboxAlmacen.Text = "Almacén";
            // 
            // lblExistencia
            // 
            this.lblExistencia.Location = new System.Drawing.Point(11, 70);
            this.lblExistencia.Name = "lblExistencia";
            this.lblExistencia.Size = new System.Drawing.Size(57, 18);
            this.lblExistencia.TabIndex = 6;
            this.lblExistencia.Text = "Existencia:";
            // 
            // lblStockMinimo
            // 
            this.lblStockMinimo.Location = new System.Drawing.Point(11, 21);
            this.lblStockMinimo.Name = "lblStockMinimo";
            this.lblStockMinimo.Size = new System.Drawing.Size(58, 18);
            this.lblStockMinimo.TabIndex = 0;
            this.lblStockMinimo.Text = "Stock Mín:";
            // 
            // TxbStockMinimo
            // 
            this.TxbStockMinimo.Location = new System.Drawing.Point(11, 45);
            this.TxbStockMinimo.Name = "TxbStockMinimo";
            this.TxbStockMinimo.NullText = "0.00";
            this.TxbStockMinimo.Size = new System.Drawing.Size(60, 20);
            this.TxbStockMinimo.TabIndex = 1;
            this.TxbStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbStockMaximo
            // 
            this.TxbStockMaximo.Location = new System.Drawing.Point(77, 45);
            this.TxbStockMaximo.Name = "TxbStockMaximo";
            this.TxbStockMaximo.NullText = "0.00";
            this.TxbStockMaximo.Size = new System.Drawing.Size(60, 20);
            this.TxbStockMaximo.TabIndex = 3;
            this.TxbStockMaximo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxbExistencia
            // 
            this.TxbExistencia.Location = new System.Drawing.Point(11, 95);
            this.TxbExistencia.Name = "TxbExistencia";
            this.TxbExistencia.NullText = "0.00";
            this.TxbExistencia.Size = new System.Drawing.Size(63, 20);
            this.TxbExistencia.TabIndex = 7;
            this.TxbExistencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblStockMaximo
            // 
            this.lblStockMaximo.Location = new System.Drawing.Point(80, 20);
            this.lblStockMaximo.Name = "lblStockMaximo";
            this.lblStockMaximo.Size = new System.Drawing.Size(60, 18);
            this.lblStockMaximo.TabIndex = 2;
            this.lblStockMaximo.Text = "Stock Máx:";
            // 
            // TxbStockReorden
            // 
            this.TxbStockReorden.Location = new System.Drawing.Point(142, 45);
            this.TxbStockReorden.Name = "TxbStockReorden";
            this.TxbStockReorden.NullText = "0.00";
            this.TxbStockReorden.Size = new System.Drawing.Size(60, 20);
            this.TxbStockReorden.TabIndex = 5;
            this.TxbStockReorden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblUnidad2
            // 
            this.lblUnidad2.Location = new System.Drawing.Point(77, 70);
            this.lblUnidad2.Name = "lblUnidad2";
            this.lblUnidad2.Size = new System.Drawing.Size(45, 18);
            this.lblUnidad2.TabIndex = 8;
            this.lblUnidad2.Text = "Unidad:";
            // 
            // lblReOrden
            // 
            this.lblReOrden.Location = new System.Drawing.Point(145, 20);
            this.lblReOrden.Name = "lblReOrden";
            this.lblReOrden.Size = new System.Drawing.Size(51, 18);
            this.lblReOrden.TabIndex = 4;
            this.lblReOrden.Text = "Reorden:";
            // 
            // CboUnidadAlmacen
            // 
            this.CboUnidadAlmacen.DisplayMember = "Descripcion";
            this.CboUnidadAlmacen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CboUnidadAlmacen.Location = new System.Drawing.Point(77, 95);
            this.CboUnidadAlmacen.Name = "CboUnidadAlmacen";
            this.CboUnidadAlmacen.NullText = "Selecciona";
            this.CboUnidadAlmacen.Size = new System.Drawing.Size(125, 20);
            this.CboUnidadAlmacen.TabIndex = 9;
            this.CboUnidadAlmacen.ValueMember = "IdUnidad";
            // 
            // gboxPrecioBase
            // 
            this.gboxPrecioBase.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxPrecioBase.Controls.Add(this.txbPrecioBase);
            this.gboxPrecioBase.Controls.Add(this.lblPrecioBase);
            this.gboxPrecioBase.HeaderText = "Precio Base";
            this.gboxPrecioBase.Location = new System.Drawing.Point(469, 60);
            this.gboxPrecioBase.Name = "gboxPrecioBase";
            this.gboxPrecioBase.Size = new System.Drawing.Size(177, 41);
            this.gboxPrecioBase.TabIndex = 40;
            this.gboxPrecioBase.Text = "Precio Base";
            // 
            // txbPrecioBase
            // 
            this.txbPrecioBase.Location = new System.Drawing.Point(104, 15);
            this.txbPrecioBase.Name = "txbPrecioBase";
            this.txbPrecioBase.Size = new System.Drawing.Size(68, 20);
            this.txbPrecioBase.TabIndex = 10;
            this.txbPrecioBase.TabStop = false;
            this.txbPrecioBase.Value = "0";
            // 
            // lblPrecioBase
            // 
            this.lblPrecioBase.Location = new System.Drawing.Point(10, 18);
            this.lblPrecioBase.Name = "lblPrecioBase";
            this.lblPrecioBase.Size = new System.Drawing.Size(66, 18);
            this.lblPrecioBase.TabIndex = 9;
            this.lblPrecioBase.Text = "Precio Base:";
            // 
            // GroupBoxImpuestoRetenido
            // 
            this.GroupBoxImpuestoRetenido.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBoxImpuestoRetenido.Controls.Add(this.txbRetencionISR);
            this.GroupBoxImpuestoRetenido.Controls.Add(this.txbRetencionIVA);
            this.GroupBoxImpuestoRetenido.Controls.Add(this.txbRetencionIEPS);
            this.GroupBoxImpuestoRetenido.Controls.Add(this.chkRentencionISR);
            this.GroupBoxImpuestoRetenido.Controls.Add(this.chkRetnecionIVA);
            this.GroupBoxImpuestoRetenido.Controls.Add(this.lblIEPS2);
            this.GroupBoxImpuestoRetenido.Controls.Add(this.cboRetencionIEPS);
            this.GroupBoxImpuestoRetenido.HeaderText = "Impuestos Retenidos";
            this.GroupBoxImpuestoRetenido.Location = new System.Drawing.Point(716, 0);
            this.GroupBoxImpuestoRetenido.Name = "GroupBoxImpuestoRetenido";
            this.GroupBoxImpuestoRetenido.Size = new System.Drawing.Size(177, 97);
            this.GroupBoxImpuestoRetenido.TabIndex = 39;
            this.GroupBoxImpuestoRetenido.Text = "Impuestos Retenidos";
            // 
            // txbRetencionISR
            // 
            this.txbRetencionISR.Location = new System.Drawing.Point(48, 47);
            this.txbRetencionISR.Mask = "n6";
            this.txbRetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionISR.Name = "txbRetencionISR";
            this.txbRetencionISR.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionISR.TabIndex = 47;
            this.txbRetencionISR.TabStop = false;
            this.txbRetencionISR.Text = "0.000000";
            this.txbRetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbRetencionIVA
            // 
            this.txbRetencionIVA.Location = new System.Drawing.Point(48, 21);
            this.txbRetencionIVA.Mask = "n6";
            this.txbRetencionIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionIVA.Name = "txbRetencionIVA";
            this.txbRetencionIVA.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionIVA.TabIndex = 46;
            this.txbRetencionIVA.TabStop = false;
            this.txbRetencionIVA.Text = "0.000000";
            this.txbRetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbRetencionIEPS
            // 
            this.txbRetencionIEPS.Location = new System.Drawing.Point(122, 70);
            this.txbRetencionIEPS.Mask = "n6";
            this.txbRetencionIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionIEPS.Name = "txbRetencionIEPS";
            this.txbRetencionIEPS.Size = new System.Drawing.Size(50, 20);
            this.txbRetencionIEPS.TabIndex = 43;
            this.txbRetencionIEPS.TabStop = false;
            this.txbRetencionIEPS.Text = "0.000000";
            this.txbRetencionIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkRentencionISR
            // 
            this.chkRentencionISR.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRentencionISR.Location = new System.Drawing.Point(7, 46);
            this.chkRentencionISR.Name = "chkRentencionISR";
            this.chkRentencionISR.Size = new System.Drawing.Size(36, 18);
            this.chkRentencionISR.TabIndex = 42;
            this.chkRentencionISR.Text = "ISR";
            // 
            // chkRetnecionIVA
            // 
            this.chkRetnecionIVA.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRetnecionIVA.Location = new System.Drawing.Point(7, 21);
            this.chkRetnecionIVA.Name = "chkRetnecionIVA";
            this.chkRetnecionIVA.Size = new System.Drawing.Size(38, 18);
            this.chkRetnecionIVA.TabIndex = 41;
            this.chkRetnecionIVA.Text = "IVA";
            // 
            // lblIEPS2
            // 
            this.lblIEPS2.Location = new System.Drawing.Point(7, 72);
            this.lblIEPS2.Name = "lblIEPS2";
            this.lblIEPS2.Size = new System.Drawing.Size(27, 18);
            this.lblIEPS2.TabIndex = 40;
            this.lblIEPS2.Text = "IEPS";
            // 
            // cboRetencionIEPS
            // 
            radListDataItem7.Text = "N/A";
            radListDataItem8.Text = "Tasa";
            radListDataItem9.Text = "Cuota";
            this.cboRetencionIEPS.Items.Add(radListDataItem7);
            this.cboRetencionIEPS.Items.Add(radListDataItem8);
            this.cboRetencionIEPS.Items.Add(radListDataItem9);
            this.cboRetencionIEPS.Location = new System.Drawing.Point(48, 71);
            this.cboRetencionIEPS.Name = "cboRetencionIEPS";
            this.cboRetencionIEPS.Size = new System.Drawing.Size(68, 20);
            this.cboRetencionIEPS.TabIndex = 39;
            // 
            // gboxIpuestoTraslado
            // 
            this.gboxIpuestoTraslado.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxIpuestoTraslado.Controls.Add(this.cboTrasladoIVA);
            this.gboxIpuestoTraslado.Controls.Add(this.txbTrasladoIEPS);
            this.gboxIpuestoTraslado.Controls.Add(this.txbTrasladoIVA);
            this.gboxIpuestoTraslado.Controls.Add(this.lblIEPS1);
            this.gboxIpuestoTraslado.Controls.Add(this.cboTrasladoIEPS);
            this.gboxIpuestoTraslado.Controls.Add(this.lblIVA);
            this.gboxIpuestoTraslado.HeaderText = "Impuestos Traslado";
            this.gboxIpuestoTraslado.Location = new System.Drawing.Point(469, 107);
            this.gboxIpuestoTraslado.Name = "gboxIpuestoTraslado";
            this.gboxIpuestoTraslado.Size = new System.Drawing.Size(177, 77);
            this.gboxIpuestoTraslado.TabIndex = 38;
            this.gboxIpuestoTraslado.Text = "Impuestos Traslado";
            // 
            // cboTrasladoIVA
            // 
            radListDataItem10.Text = "N/A";
            radListDataItem11.Text = "Tasa";
            radListDataItem12.Text = "Exento";
            this.cboTrasladoIVA.Items.Add(radListDataItem10);
            this.cboTrasladoIVA.Items.Add(radListDataItem11);
            this.cboTrasladoIVA.Items.Add(radListDataItem12);
            this.cboTrasladoIVA.Location = new System.Drawing.Point(38, 20);
            this.cboTrasladoIVA.Name = "cboTrasladoIVA";
            this.cboTrasladoIVA.Size = new System.Drawing.Size(66, 20);
            this.cboTrasladoIVA.TabIndex = 37;
            // 
            // txbTrasladoIEPS
            // 
            this.txbTrasladoIEPS.Location = new System.Drawing.Point(110, 46);
            this.txbTrasladoIEPS.Mask = "n6";
            this.txbTrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbTrasladoIEPS.Name = "txbTrasladoIEPS";
            this.txbTrasladoIEPS.Size = new System.Drawing.Size(60, 20);
            this.txbTrasladoIEPS.TabIndex = 45;
            this.txbTrasladoIEPS.TabStop = false;
            this.txbTrasladoIEPS.Text = "0.000000";
            this.txbTrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbTrasladoIVA
            // 
            this.txbTrasladoIVA.Location = new System.Drawing.Point(110, 19);
            this.txbTrasladoIVA.Mask = "n6";
            this.txbTrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbTrasladoIVA.Name = "txbTrasladoIVA";
            this.txbTrasladoIVA.Size = new System.Drawing.Size(60, 20);
            this.txbTrasladoIVA.TabIndex = 44;
            this.txbTrasladoIVA.TabStop = false;
            this.txbTrasladoIVA.Text = "0.000000";
            this.txbTrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblIEPS1
            // 
            this.lblIEPS1.Location = new System.Drawing.Point(5, 46);
            this.lblIEPS1.Name = "lblIEPS1";
            this.lblIEPS1.Size = new System.Drawing.Size(27, 18);
            this.lblIEPS1.TabIndex = 35;
            this.lblIEPS1.Text = "IEPS";
            // 
            // cboTrasladoIEPS
            // 
            radListDataItem1.Text = "N/A";
            radListDataItem2.Text = "Tasa";
            radListDataItem3.Text = "Cuota";
            this.cboTrasladoIEPS.Items.Add(radListDataItem1);
            this.cboTrasladoIEPS.Items.Add(radListDataItem2);
            this.cboTrasladoIEPS.Items.Add(radListDataItem3);
            this.cboTrasladoIEPS.Location = new System.Drawing.Point(38, 46);
            this.cboTrasladoIEPS.Name = "cboTrasladoIEPS";
            this.cboTrasladoIEPS.Size = new System.Drawing.Size(66, 20);
            this.cboTrasladoIEPS.TabIndex = 34;
            // 
            // lblIVA
            // 
            this.lblIVA.Location = new System.Drawing.Point(7, 21);
            this.lblIVA.Name = "lblIVA";
            this.lblIVA.Size = new System.Drawing.Size(24, 18);
            this.lblIVA.TabIndex = 33;
            this.lblIVA.Text = "IVA";
            // 
            // gboxOtrasEspecificaciones
            // 
            this.gboxOtrasEspecificaciones.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblUnidad1);
            this.gboxOtrasEspecificaciones.Controls.Add(this.txbLargo);
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblLargo);
            this.gboxOtrasEspecificaciones.Controls.Add(this.txbAncho);
            this.gboxOtrasEspecificaciones.Controls.Add(this.cboUnidadZ);
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblAncho);
            this.gboxOtrasEspecificaciones.Controls.Add(this.cboUnidadXY);
            this.gboxOtrasEspecificaciones.Controls.Add(this.txbAlto);
            this.gboxOtrasEspecificaciones.Controls.Add(this.lblCalibre);
            this.gboxOtrasEspecificaciones.HeaderText = "Otras especificaciones";
            this.gboxOtrasEspecificaciones.Location = new System.Drawing.Point(3, 60);
            this.gboxOtrasEspecificaciones.Name = "gboxOtrasEspecificaciones";
            this.gboxOtrasEspecificaciones.Size = new System.Drawing.Size(236, 124);
            this.gboxOtrasEspecificaciones.TabIndex = 17;
            this.gboxOtrasEspecificaciones.Text = "Otras especificaciones";
            // 
            // lblUnidad1
            // 
            this.lblUnidad1.Location = new System.Drawing.Point(151, 22);
            this.lblUnidad1.Name = "lblUnidad1";
            this.lblUnidad1.Size = new System.Drawing.Size(42, 18);
            this.lblUnidad1.TabIndex = 4;
            this.lblUnidad1.Text = "Unidad";
            // 
            // txbLargo
            // 
            this.txbLargo.Location = new System.Drawing.Point(9, 45);
            this.txbLargo.Name = "txbLargo";
            this.txbLargo.NullText = "Largo";
            this.txbLargo.Size = new System.Drawing.Size(65, 20);
            this.txbLargo.TabIndex = 1;
            this.txbLargo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLargo
            // 
            this.lblLargo.Location = new System.Drawing.Point(9, 21);
            this.lblLargo.Name = "lblLargo";
            this.lblLargo.Size = new System.Drawing.Size(37, 18);
            this.lblLargo.TabIndex = 0;
            this.lblLargo.Text = "Largo:";
            // 
            // txbAncho
            // 
            this.txbAncho.Location = new System.Drawing.Point(78, 45);
            this.txbAncho.Name = "txbAncho";
            this.txbAncho.NullText = "Ancho";
            this.txbAncho.Size = new System.Drawing.Size(65, 20);
            this.txbAncho.TabIndex = 3;
            this.txbAncho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboUnidadZ
            // 
            this.cboUnidadZ.DisplayMember = "Descripcion";
            this.cboUnidadZ.Location = new System.Drawing.Point(70, 94);
            this.cboUnidadZ.Name = "cboUnidadZ";
            this.cboUnidadZ.NullText = "Selecciona";
            this.cboUnidadZ.Size = new System.Drawing.Size(95, 20);
            this.cboUnidadZ.TabIndex = 8;
            this.cboUnidadZ.ValueMember = "Id";
            // 
            // lblAncho
            // 
            this.lblAncho.Location = new System.Drawing.Point(80, 22);
            this.lblAncho.Name = "lblAncho";
            this.lblAncho.Size = new System.Drawing.Size(41, 18);
            this.lblAncho.TabIndex = 2;
            this.lblAncho.Text = "Ancho:";
            // 
            // cboUnidadXY
            // 
            this.cboUnidadXY.DisplayMember = "Descripcion";
            this.cboUnidadXY.Location = new System.Drawing.Point(146, 45);
            this.cboUnidadXY.Name = "cboUnidadXY";
            this.cboUnidadXY.NullText = "Selecciona";
            this.cboUnidadXY.Size = new System.Drawing.Size(85, 20);
            this.cboUnidadXY.TabIndex = 5;
            this.cboUnidadXY.ValueMember = "Id";
            // 
            // txbAlto
            // 
            this.txbAlto.Location = new System.Drawing.Point(9, 94);
            this.txbAlto.Name = "txbAlto";
            this.txbAlto.NullText = "Calibre";
            this.txbAlto.Size = new System.Drawing.Size(55, 20);
            this.txbAlto.TabIndex = 7;
            this.txbAlto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCalibre
            // 
            this.lblCalibre.Location = new System.Drawing.Point(9, 71);
            this.lblCalibre.Name = "lblCalibre";
            this.lblCalibre.Size = new System.Drawing.Size(75, 18);
            this.lblCalibre.TabIndex = 6;
            this.lblCalibre.Text = "Alto / Calibre:";
            // 
            // pageViewProveedor
            // 
            this.pageViewProveedor.Controls.Add(this.TProveedor);
            this.pageViewProveedor.Controls.Add(this.gridProveedor);
            this.pageViewProveedor.ItemSize = new System.Drawing.SizeF(68F, 28F);
            this.pageViewProveedor.Location = new System.Drawing.Point(10, 10);
            this.pageViewProveedor.Name = "pageViewProveedor";
            this.pageViewProveedor.Size = new System.Drawing.Size(647, 311);
            this.pageViewProveedor.Text = "Proveedor";
            // 
            // TProveedor
            // 
            this.TProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProveedor.Etiqueta = "";
            this.TProveedor.Location = new System.Drawing.Point(0, 0);
            this.TProveedor.Name = "TProveedor";
            this.TProveedor.ReadOnly = false;
            this.TProveedor.ShowActualizar = false;
            this.TProveedor.ShowAutorizar = false;
            this.TProveedor.ShowCerrar = false;
            this.TProveedor.ShowEditar = false;
            this.TProveedor.ShowExportarExcel = false;
            this.TProveedor.ShowFiltro = false;
            this.TProveedor.ShowGuardar = false;
            this.TProveedor.ShowHerramientas = false;
            this.TProveedor.ShowImagen = false;
            this.TProveedor.ShowImprimir = false;
            this.TProveedor.ShowNuevo = true;
            this.TProveedor.ShowRemover = true;
            this.TProveedor.Size = new System.Drawing.Size(647, 30);
            this.TProveedor.TabIndex = 42;
            // 
            // gridProveedor
            // 
            this.gridProveedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProveedor.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn69.DataType = typeof(int);
            gridViewTextBoxColumn69.FieldName = "Id";
            gridViewTextBoxColumn69.HeaderText = "Id";
            gridViewTextBoxColumn69.IsVisible = false;
            gridViewTextBoxColumn69.Name = "Id";
            gridViewTextBoxColumn69.VisibleInColumnChooser = false;
            gridViewMultiComboBoxColumn3.FieldName = "IdProveedor";
            gridViewMultiComboBoxColumn3.HeaderText = "Proveedor";
            gridViewMultiComboBoxColumn3.Name = "IdProveedor";
            gridViewMultiComboBoxColumn3.Width = 280;
            gridViewTextBoxColumn70.DataType = typeof(decimal);
            gridViewTextBoxColumn70.FieldName = "Unitario";
            gridViewTextBoxColumn70.HeaderText = "Unitario";
            gridViewTextBoxColumn70.Name = "Unitario";
            gridViewTextBoxColumn70.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn70.Width = 85;
            gridViewTextBoxColumn71.FieldName = "IdModelo";
            gridViewTextBoxColumn71.HeaderText = "IdModelo";
            gridViewTextBoxColumn71.IsVisible = false;
            gridViewTextBoxColumn71.Name = "IdModelo";
            gridViewTextBoxColumn71.VisibleInColumnChooser = false;
            gridViewTextBoxColumn72.FieldName = "Nota";
            gridViewTextBoxColumn72.HeaderText = "Nota";
            gridViewTextBoxColumn72.Name = "Nota";
            gridViewTextBoxColumn72.Width = 150;
            gridViewTextBoxColumn73.FieldName = "FechaNuevo";
            gridViewTextBoxColumn73.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn73.Name = "FechaNuevo";
            gridViewTextBoxColumn73.Width = 85;
            gridViewTextBoxColumn74.FieldName = "Creo";
            gridViewTextBoxColumn74.HeaderText = "Creo";
            gridViewTextBoxColumn74.Name = "Creo";
            gridViewTextBoxColumn74.Width = 85;
            this.gridProveedor.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn69,
            gridViewMultiComboBoxColumn3,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74});
            this.gridProveedor.MasterTemplate.ViewDefinition = tableViewDefinition11;
            this.gridProveedor.Name = "gridProveedor";
            this.gridProveedor.Size = new System.Drawing.Size(647, 311);
            this.gridProveedor.TabIndex = 1;
            // 
            // pageViewPublicacion
            // 
            this.pageViewPublicacion.Controls.Add(this.Publicacion);
            this.pageViewPublicacion.Controls.Add(this.radGroupBox1);
            this.pageViewPublicacion.ItemSize = new System.Drawing.SizeF(73F, 28F);
            this.pageViewPublicacion.Location = new System.Drawing.Point(10, 10);
            this.pageViewPublicacion.Name = "pageViewPublicacion";
            this.pageViewPublicacion.Size = new System.Drawing.Size(647, 360);
            this.pageViewPublicacion.Text = "Publicación";
            // 
            // Publicacion
            // 
            this.Publicacion.AcceptsReturn = true;
            this.Publicacion.AvailableInlineStyles = ((System.Collections.Generic.List<string>)(resources.GetObject("Publicacion.AvailableInlineStyles")));
            this.Publicacion.BaseURL = null;
            this.Publicacion.CleanMSWordHTMLOnPaste = true;
            this.Publicacion.CodeEditor.Enabled = true;
            this.Publicacion.CodeEditor.Locked = false;
            this.Publicacion.CodeEditor.TabWidth = 720;
            this.Publicacion.CodeEditor.WordWrap = false;
            this.Publicacion.CSSText = null;
            this.Publicacion.CustomColorPalette = null;
            this.Publicacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Publicacion.DocumentHTML = null;
            this.Publicacion.EnableInlineSpelling = false;
            this.Publicacion.FontSizesList = null;
            this.Publicacion.FontsList = null;
            this.Publicacion.HiddenButtons = "";
            this.Publicacion.ImageStorageLocation = null;
            this.Publicacion.InCodeView = false;
            this.Publicacion.IndentAmount = 2;
            this.Publicacion.IndentsUseBlockuote = false;
            this.Publicacion.LanguageFile = null;
            this.Publicacion.LicenceActivationKey = null;
            this.Publicacion.LicenceKey = null;
            this.Publicacion.LicenceKeyInlineSpelling = null;
            this.Publicacion.Location = new System.Drawing.Point(0, 37);
            this.Publicacion.Name = "Publicacion";
            this.Publicacion.Size = new System.Drawing.Size(647, 323);
            this.Publicacion.SpellingAutoCorrectionList = ((System.Collections.Hashtable)(resources.GetObject("Publicacion.SpellingAutoCorrectionList")));
            this.Publicacion.TabIndex = 42;
            this.Publicacion.ToolstripImageScalingSize = new System.Drawing.Size(16, 16);
            this.Publicacion.UseParagraphAsDefault = false;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.Seccion);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(647, 37);
            this.radGroupBox1.TabIndex = 43;
            // 
            // Seccion
            // 
            this.Seccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Seccion.AutoSizeDropDownToBestFit = true;
            this.Seccion.DisplayMember = "Clase";
            this.Seccion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Seccion.NestedRadGridView
            // 
            this.Seccion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Seccion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seccion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Seccion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Seccion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Seccion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Seccion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn75.DataType = typeof(int);
            gridViewTextBoxColumn75.FieldName = "IdCategoria";
            gridViewTextBoxColumn75.HeaderText = "IdCategoria";
            gridViewTextBoxColumn75.IsVisible = false;
            gridViewTextBoxColumn75.Name = "IdCategoria";
            gridViewTextBoxColumn75.VisibleInColumnChooser = false;
            gridViewTextBoxColumn76.DataType = typeof(int);
            gridViewTextBoxColumn76.FieldName = "IdSubCategoria";
            gridViewTextBoxColumn76.HeaderText = "SubIdCategoria";
            gridViewTextBoxColumn76.IsVisible = false;
            gridViewTextBoxColumn76.Name = "IdSubCategoria";
            gridViewTextBoxColumn76.VisibleInColumnChooser = false;
            gridViewTextBoxColumn77.FieldName = "Codigo";
            gridViewTextBoxColumn77.HeaderText = "Codigo";
            gridViewTextBoxColumn77.IsVisible = false;
            gridViewTextBoxColumn77.Name = "Codigo";
            gridViewTextBoxColumn78.FieldName = "Clase";
            gridViewTextBoxColumn78.HeaderText = "Clase";
            gridViewTextBoxColumn78.Name = "Clase";
            gridViewTextBoxColumn79.FieldName = "Categoria";
            gridViewTextBoxColumn79.HeaderText = "Categoria";
            gridViewTextBoxColumn79.IsVisible = false;
            gridViewTextBoxColumn79.Name = "Categoria";
            gridViewTextBoxColumn79.VisibleInColumnChooser = false;
            gridViewTextBoxColumn80.FieldName = "Descriptor";
            gridViewTextBoxColumn80.HeaderText = "Descriptor";
            gridViewTextBoxColumn80.Name = "Descriptor";
            gridViewTextBoxColumn81.FieldName = "Childs";
            gridViewTextBoxColumn81.HeaderText = "Childs";
            gridViewTextBoxColumn81.IsVisible = false;
            gridViewTextBoxColumn81.Name = "Childs";
            gridViewTextBoxColumn81.VisibleInColumnChooser = false;
            this.Seccion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79,
            gridViewTextBoxColumn80,
            gridViewTextBoxColumn81});
            this.Seccion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Seccion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Seccion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition12;
            this.Seccion.EditorControl.Name = "NestedRadGridView";
            this.Seccion.EditorControl.ReadOnly = true;
            this.Seccion.EditorControl.ShowGroupPanel = false;
            this.Seccion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Seccion.EditorControl.TabIndex = 0;
            this.Seccion.Location = new System.Drawing.Point(68, 8);
            this.Seccion.Name = "Seccion";
            this.Seccion.NullText = "Categoría";
            this.Seccion.Size = new System.Drawing.Size(565, 0);
            this.Seccion.TabIndex = 5;
            this.Seccion.TabStop = false;
            this.Seccion.ValueMember = "IdCategoria";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(5, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(57, 18);
            this.radLabel1.TabIndex = 4;
            this.radLabel1.Text = "Categoría:";
            // 
            // TProducto
            // 
            this.TProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TProducto.Etiqueta = "";
            this.TProducto.Location = new System.Drawing.Point(0, 0);
            this.TProducto.Name = "TProducto";
            this.TProducto.ReadOnly = false;
            this.TProducto.ShowActualizar = false;
            this.TProducto.ShowAutorizar = false;
            this.TProducto.ShowCerrar = true;
            this.TProducto.ShowEditar = false;
            this.TProducto.ShowExportarExcel = false;
            this.TProducto.ShowFiltro = false;
            this.TProducto.ShowGuardar = true;
            this.TProducto.ShowHerramientas = false;
            this.TProducto.ShowImagen = false;
            this.TProducto.ShowImprimir = false;
            this.TProducto.ShowNuevo = false;
            this.TProducto.ShowRemover = false;
            this.TProducto.Size = new System.Drawing.Size(741, 30);
            this.TProducto.TabIndex = 41;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ProductoModeloForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 524);
            this.Controls.Add(this.pageViews);
            this.Controls.Add(this.panelProducto);
            this.Controls.Add(this.TProducto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoModeloForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Producto - Modelo";
            this.Load += new System.EventHandler(this.ProductoModeloForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelProducto)).EndInit();
            this.panelProducto.ResumeLayout(false);
            this.panelProducto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCategorias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbMarca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMarca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbExpecificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEspecificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCodigoBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Etiquetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtiquetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageViews)).EndInit();
            this.pageViews.ResumeLayout(false);
            this.pageViewGeneral.ResumeLayout(false);
            this.pageViewGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSKU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gVariante.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVariante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxAlmacen)).EndInit();
            this.gboxAlmacen.ResumeLayout(false);
            this.gboxAlmacen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblExistencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockMaximo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbExistencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStockMaximo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbStockReorden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUnidadAlmacen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxPrecioBase)).EndInit();
            this.gboxPrecioBase.ResumeLayout(false);
            this.gboxPrecioBase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbPrecioBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrecioBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxImpuestoRetenido)).EndInit();
            this.GroupBoxImpuestoRetenido.ResumeLayout(false);
            this.GroupBoxImpuestoRetenido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRentencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetnecionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRetencionIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxIpuestoTraslado)).EndInit();
            this.gboxIpuestoTraslado.ResumeLayout(false);
            this.gboxIpuestoTraslado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIEPS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gboxOtrasEspecificaciones)).EndInit();
            this.gboxOtrasEspecificaciones.ResumeLayout(false);
            this.gboxOtrasEspecificaciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbLargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAncho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAncho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboUnidadXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbAlto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCalibre)).EndInit();
            this.pageViewProveedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProveedor)).EndInit();
            this.pageViewPublicacion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Seccion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seccion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel panelProducto;
        protected internal Telerik.WinControls.UI.RadTextBox Etiquetas;
        private Telerik.WinControls.UI.RadLabel lblEtiquetas;
        protected internal Telerik.WinControls.UI.RadTextBox txbCodigoBarras;
        private Telerik.WinControls.UI.RadLabel lblCodigoBarras;
        protected internal Telerik.WinControls.UI.RadTextBox txbExpecificacion;
        private Telerik.WinControls.UI.RadLabel lblEspecificacion;
        protected internal Telerik.WinControls.UI.RadTextBox txbMarca;
        private Telerik.WinControls.UI.RadLabel lblMarca;
        protected internal Telerik.WinControls.UI.RadTextBox txbModelo;
        private Telerik.WinControls.UI.RadLabel lblModelo;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox cboCategorias;
        private Telerik.WinControls.UI.RadLabel lblCategoria;
        protected internal Telerik.WinControls.UI.RadDropDownList cboTipo;
        private Telerik.WinControls.UI.RadLabel lblTipo;
        private Telerik.WinControls.UI.RadPageView pageViews;
        private Telerik.WinControls.UI.RadPageViewPage pageViewGeneral;
        private Telerik.WinControls.UI.RadGroupBox gboxOtrasEspecificaciones;
        private Telerik.WinControls.UI.RadLabel lblPrecioBase;
        private Telerik.WinControls.UI.RadLabel lblUnidad1;
        protected internal Telerik.WinControls.UI.RadTextBox txbLargo;
        private Telerik.WinControls.UI.RadLabel lblLargo;
        protected internal Telerik.WinControls.UI.RadTextBox txbAncho;
        protected internal Telerik.WinControls.UI.RadDropDownList cboUnidadZ;
        private Telerik.WinControls.UI.RadLabel lblAncho;
        protected internal Telerik.WinControls.UI.RadDropDownList cboUnidadXY;
        protected internal Telerik.WinControls.UI.RadTextBox txbAlto;
        private Telerik.WinControls.UI.RadLabel lblCalibre;
        private Telerik.WinControls.UI.RadGroupBox gboxAlmacen;
        private Telerik.WinControls.UI.RadLabel lblStockMinimo;
        private Telerik.WinControls.UI.RadLabel lblExistencia;
        protected internal Telerik.WinControls.UI.RadTextBox TxbStockMinimo;
        protected internal Telerik.WinControls.UI.RadTextBox TxbExistencia;
        protected internal Telerik.WinControls.UI.RadTextBox TxbStockMaximo;
        private Telerik.WinControls.UI.RadLabel lblReOrden;
        private Telerik.WinControls.UI.RadLabel lblStockMaximo;
        protected internal Telerik.WinControls.UI.RadTextBox TxbStockReorden;
        private Telerik.WinControls.UI.RadLabel lblUnidad2;
        protected internal Telerik.WinControls.UI.RadDropDownList CboUnidadAlmacen;
        private Telerik.WinControls.UI.RadPageViewPage pageViewProveedor;
        protected internal Telerik.WinControls.UI.RadGridView gridProveedor;
        private Telerik.WinControls.UI.RadGroupBox GroupBoxImpuestoRetenido;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox txbRetencionISR;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox txbRetencionIVA;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox txbRetencionIEPS;
        protected internal Telerik.WinControls.UI.RadCheckBox chkRentencionISR;
        protected internal Telerik.WinControls.UI.RadCheckBox chkRetnecionIVA;
        private Telerik.WinControls.UI.RadLabel lblIEPS2;
        protected internal Telerik.WinControls.UI.RadDropDownList cboRetencionIEPS;
        private Telerik.WinControls.UI.RadGroupBox gboxIpuestoTraslado;
        protected internal Telerik.WinControls.UI.RadDropDownList cboTrasladoIVA;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox txbTrasladoIEPS;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox txbTrasladoIVA;
        private Telerik.WinControls.UI.RadLabel lblIEPS1;
        protected internal Telerik.WinControls.UI.RadDropDownList cboTrasladoIEPS;
        private Telerik.WinControls.UI.RadLabel lblIVA;
        private Telerik.WinControls.UI.RadGroupBox gboxPrecioBase;
        protected internal Telerik.WinControls.UI.RadCalculatorDropDown txbPrecioBase;
        protected internal Common.Forms.ToolBarStandarControl TProducto;
        protected internal Common.Forms.ToolBarStandarControl TProveedor;
        protected internal Telerik.WinControls.UI.RadCheckBox Activo;
        protected internal Zoople.HTMLEditControl Publicacion;
        private Telerik.WinControls.UI.RadPageViewPage pageViewPublicacion;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Seccion;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        protected internal Telerik.WinControls.UI.RadGridView gVariante;
        protected internal Common.Forms.ToolBarStandarControl TDisponible;
        protected internal Telerik.WinControls.UI.RadTextBox TxtSKU;
        private Telerik.WinControls.UI.RadLabel radLabel2;
    }
}