﻿namespace Jaeger.UI.Tienda.Forms {
    partial class PedidoClienteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Facturar = new Telerik.WinControls.UI.RadCheckBox();
            this.Status = new System.Windows.Forms.ComboBox();
            this.label12 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.NoPedido = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.Vendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txbModifica = new Telerik.WinControls.UI.RadTextBox();
            this.txbCreo = new Telerik.WinControls.UI.RadTextBox();
            this.Observaciones = new Telerik.WinControls.UI.RadTextBox();
            this.Recibe = new Telerik.WinControls.UI.RadTextBox();
            this.Embarque = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Cliente = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.Clave = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.backClientes = new System.ComponentModel.BackgroundWorker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Facturar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbModifica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ReadOnly = false;
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowExportarExcel = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = true;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(1079, 30);
            this.ToolBar.TabIndex = 0;
            // 
            // Facturar
            // 
            this.Facturar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Facturar.Location = new System.Drawing.Point(417, 65);
            this.Facturar.Name = "Facturar";
            this.Facturar.Size = new System.Drawing.Size(61, 18);
            this.Facturar.TabIndex = 131;
            this.Facturar.Text = "Facturar";
            // 
            // Status
            // 
            this.Status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Status.FormattingEnabled = true;
            this.Status.Location = new System.Drawing.Point(546, 37);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(80, 21);
            this.Status.TabIndex = 130;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Location = new System.Drawing.Point(503, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 18);
            this.label12.TabIndex = 129;
            this.label12.Text = "Status";
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEntrega.Location = new System.Drawing.Point(704, 64);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.Size = new System.Drawing.Size(199, 20);
            this.FechaEntrega.TabIndex = 127;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "martes, 27 de julio de 2021";
            this.FechaEntrega.Value = new System.DateTime(2021, 7, 27, 22, 12, 11, 710);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.Location = new System.Drawing.Point(634, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 18);
            this.label11.TabIndex = 128;
            this.label11.Text = "Fec. Entrega:";
            // 
            // NoPedido
            // 
            this.NoPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NoPedido.Location = new System.Drawing.Point(546, 64);
            this.NoPedido.MaxLength = 100;
            this.NoPedido.Name = "NoPedido";
            this.NoPedido.Size = new System.Drawing.Size(80, 20);
            this.NoPedido.TabIndex = 125;
            this.NoPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(483, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 18);
            this.label5.TabIndex = 126;
            this.label5.Text = "No. Pedido:";
            // 
            // Vendedor
            // 
            this.Vendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Vendedor.AutoSizeDropDownHeight = true;
            this.Vendedor.AutoSizeDropDownToBestFit = true;
            this.Vendedor.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Vendedor.NestedRadGridView
            // 
            this.Vendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Vendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Vendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Vendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Vendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdVendedor";
            gridViewTextBoxColumn1.HeaderText = "IdVendedor";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdVendedor";
            gridViewTextBoxColumn2.FieldName = "ClaveVendedor";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "ClaveVendedor";
            this.Vendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Vendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Vendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Vendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Vendedor.EditorControl.Name = "NestedRadGridView";
            this.Vendedor.EditorControl.ReadOnly = true;
            this.Vendedor.EditorControl.ShowGroupPanel = false;
            this.Vendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Vendedor.EditorControl.TabIndex = 0;
            this.Vendedor.Enabled = false;
            this.Vendedor.Location = new System.Drawing.Point(965, 89);
            this.Vendedor.Name = "Vendedor";
            this.Vendedor.Size = new System.Drawing.Size(91, 20);
            this.Vendedor.TabIndex = 124;
            this.Vendedor.TabStop = false;
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.Location = new System.Drawing.Point(458, 37);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(20, 21);
            this.Buscar.TabIndex = 123;
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.Location = new System.Drawing.Point(704, 37);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(199, 20);
            this.FechaEmision.TabIndex = 110;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "martes, 27 de julio de 2021";
            this.FechaEmision.Value = new System.DateTime(2021, 7, 27, 22, 12, 11, 750);
            // 
            // txbModifica
            // 
            this.txbModifica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txbModifica.Location = new System.Drawing.Point(965, 64);
            this.txbModifica.Name = "txbModifica";
            this.txbModifica.ReadOnly = true;
            this.txbModifica.Size = new System.Drawing.Size(91, 20);
            this.txbModifica.TabIndex = 122;
            this.txbModifica.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbCreo
            // 
            this.txbCreo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txbCreo.Location = new System.Drawing.Point(965, 37);
            this.txbCreo.Name = "txbCreo";
            this.txbCreo.ReadOnly = true;
            this.txbCreo.Size = new System.Drawing.Size(91, 20);
            this.txbCreo.TabIndex = 111;
            this.txbCreo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Observaciones
            // 
            this.Observaciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Observaciones.Location = new System.Drawing.Point(704, 89);
            this.Observaciones.MaxLength = 100;
            this.Observaciones.Name = "Observaciones";
            this.Observaciones.Size = new System.Drawing.Size(197, 20);
            this.Observaciones.TabIndex = 112;
            // 
            // Recibe
            // 
            this.Recibe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Recibe.Location = new System.Drawing.Point(54, 64);
            this.Recibe.MaxLength = 100;
            this.Recibe.Name = "Recibe";
            this.Recibe.Size = new System.Drawing.Size(353, 20);
            this.Recibe.TabIndex = 113;
            // 
            // Embarque
            // 
            this.Embarque.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Embarque.AutoSizeDropDownHeight = true;
            this.Embarque.AutoSizeDropDownToBestFit = true;
            this.Embarque.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Embarque.NestedRadGridView
            // 
            this.Embarque.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Embarque.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Embarque.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Embarque.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Embarque.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Embarque.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Embarque.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "IdDrccn";
            gridViewTextBoxColumn3.HeaderText = "IdDrccn";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "IdDrccn";
            gridViewTextBoxColumn4.FieldName = "Completo";
            gridViewTextBoxColumn4.HeaderText = "Dirección";
            gridViewTextBoxColumn4.Name = "Completo";
            this.Embarque.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.Embarque.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Embarque.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Embarque.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Embarque.EditorControl.Name = "NestedRadGridView";
            this.Embarque.EditorControl.ReadOnly = true;
            this.Embarque.EditorControl.ShowGroupPanel = false;
            this.Embarque.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Embarque.EditorControl.TabIndex = 0;
            this.Embarque.Location = new System.Drawing.Point(68, 90);
            this.Embarque.Name = "Embarque";
            this.Embarque.Size = new System.Drawing.Size(339, 20);
            this.Embarque.TabIndex = 114;
            this.Embarque.TabStop = false;
            // 
            // Cliente
            // 
            this.Cliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Cliente.AutoSizeDropDownToBestFit = true;
            this.Cliente.DisplayMember = "Nombre";
            this.Cliente.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Cliente.NestedRadGridView
            // 
            this.Cliente.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Cliente.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cliente.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Cliente.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Cliente.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Cliente.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Cliente.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.FieldName = "IdDirectorio";
            gridViewTextBoxColumn5.HeaderText = "IdDirectorio";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "IdDirectorio";
            gridViewTextBoxColumn6.FieldName = "Nombre";
            gridViewTextBoxColumn6.HeaderText = "Cliente";
            gridViewTextBoxColumn6.Name = "Nombre";
            this.Cliente.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.Cliente.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Cliente.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Cliente.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.Cliente.EditorControl.Name = "NestedRadGridView";
            this.Cliente.EditorControl.ReadOnly = true;
            this.Cliente.EditorControl.ShowGroupPanel = false;
            this.Cliente.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Cliente.EditorControl.TabIndex = 0;
            this.Cliente.Location = new System.Drawing.Point(123, 37);
            this.Cliente.Name = "Cliente";
            this.Cliente.Size = new System.Drawing.Size(329, 20);
            this.Cliente.TabIndex = 109;
            this.Cliente.TabStop = false;
            this.Cliente.ValueMember = "IdDirectorio";
            this.Cliente.SelectedIndexChanged += new System.EventHandler(this.CboPersona_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.Location = new System.Drawing.Point(632, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 18);
            this.label8.TabIndex = 118;
            this.label8.Text = "Fec. Emisión:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(621, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 18);
            this.label4.TabIndex = 117;
            this.label4.Text = "Observaciones:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Location = new System.Drawing.Point(907, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 18);
            this.label6.TabIndex = 120;
            this.label6.Text = "Vendedor:";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Location = new System.Drawing.Point(907, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 18);
            this.label10.TabIndex = 121;
            this.label10.Text = "Modifica:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(6, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 18);
            this.label7.TabIndex = 116;
            this.label7.Text = "Recibe:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Location = new System.Drawing.Point(909, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 18);
            this.label9.TabIndex = 119;
            this.label9.Text = "Creó:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 115;
            this.label3.Text = "Embarque:";
            // 
            // Clave
            // 
            this.Clave.Location = new System.Drawing.Point(54, 37);
            this.Clave.Name = "Clave";
            this.Clave.ReadOnly = true;
            this.Clave.Size = new System.Drawing.Size(63, 20);
            this.Clave.TabIndex = 108;
            this.Clave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 107;
            this.label2.Text = "Cliente:";
            // 
            // backClientes
            // 
            this.backClientes.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackClientes_DoWork);
            this.backClientes.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackClientes_RunWorkerCompleted);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(463, 89);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(152, 21);
            this.comboBox1.TabIndex = 133;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel1.Location = new System.Drawing.Point(420, 90);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(36, 18);
            this.radLabel1.TabIndex = 132;
            this.radLabel1.Text = "Envío:";
            // 
            // PedidoClienteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.Facturar);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.FechaEntrega);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.NoPedido);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Vendedor);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.FechaEmision);
            this.Controls.Add(this.txbModifica);
            this.Controls.Add(this.txbCreo);
            this.Controls.Add(this.Observaciones);
            this.Controls.Add(this.Recibe);
            this.Controls.Add(this.Embarque);
            this.Controls.Add(this.Cliente);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Clave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ToolBar);
            this.Name = "PedidoClienteControl";
            this.Size = new System.Drawing.Size(1079, 118);
            this.Load += new System.EventHandler(this.PedidoClienteControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Facturar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbModifica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal Telerik.WinControls.UI.RadCheckBox Facturar;
        internal System.Windows.Forms.ComboBox Status;
        private Telerik.WinControls.UI.RadLabel label12;
        public Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        private Telerik.WinControls.UI.RadLabel label11;
        public Telerik.WinControls.UI.RadTextBox NoPedido;
        private Telerik.WinControls.UI.RadLabel label5;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Vendedor;
        internal Telerik.WinControls.UI.RadButton Buscar;
        public Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        public Telerik.WinControls.UI.RadTextBox txbModifica;
        public Telerik.WinControls.UI.RadTextBox txbCreo;
        public Telerik.WinControls.UI.RadTextBox Observaciones;
        public Telerik.WinControls.UI.RadTextBox Recibe;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Embarque;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Cliente;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label7;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label3;
        public Telerik.WinControls.UI.RadTextBox Clave;
        private Telerik.WinControls.UI.RadLabel label2;
        internal Common.Forms.ToolBarStandarControl ToolBar;
        private System.ComponentModel.BackgroundWorker backClientes;
        internal System.Windows.Forms.ErrorProvider errorProvider1;
        internal System.Windows.Forms.ComboBox comboBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}
