﻿namespace Jaeger.UI.Tienda.Forms
{
    partial class PedidoCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PedidoCatalogoForm));
            this.menuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.menuContextualCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.menuContextualEditar = new Telerik.WinControls.UI.RadMenuItem();
            this.TPedido = new Jaeger.UI.Tienda.Forms.PedidosGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // menuContextual
            // 
            this.menuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.menuContextualCopiar,
            this.menuContextualEditar});
            // 
            // menuContextualCopiar
            // 
            this.menuContextualCopiar.Name = "menuContextualCopiar";
            this.menuContextualCopiar.Text = "Copiar";
            // 
            // menuContextualEditar
            // 
            this.menuContextualEditar.Name = "menuContextualEditar";
            this.menuContextualEditar.Text = "Editar";
            // 
            // TPedido
            // 
            this.TPedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TPedido.Location = new System.Drawing.Point(0, 0);
            this.TPedido.Name = "TPedido";
            this.TPedido.PDF = null;
            this.TPedido.ShowActualizar = true;
            this.TPedido.ShowAutosuma = true;
            this.TPedido.ShowCancelar = false;
            this.TPedido.ShowCerrar = true;
            this.TPedido.ShowEditar = false;
            this.TPedido.ShowEjercicio = true;
            this.TPedido.ShowExportarExcel = false;
            this.TPedido.ShowFiltro = true;
            this.TPedido.ShowHerramientas = false;
            this.TPedido.ShowImprimir = false;
            this.TPedido.ShowItem = false;
            this.TPedido.ShowNuevo = true;
            this.TPedido.ShowPeriodo = true;
            this.TPedido.ShowSeleccionMultiple = true;
            this.TPedido.Size = new System.Drawing.Size(1404, 646);
            this.TPedido.TabIndex = 0;
            // 
            // PedidoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1404, 646);
            this.Controls.Add(this.TPedido);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PedidoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: Pedidos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PedidoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal Telerik.WinControls.UI.RadContextMenu menuContextual;
        protected internal Telerik.WinControls.UI.RadMenuItem menuContextualCopiar;
        protected internal Telerik.WinControls.UI.RadMenuItem menuContextualEditar;
        protected internal PedidosGridControl TPedido;
    }
}
