﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Tienda.Forms {
    public partial class CategoriasCatalogoForm : RadForm {
        protected internal ICategoriaService Service;
        protected internal BindingList<CategoriaModel> Data;
        public CategoriasCatalogoForm() {
            InitializeComponent();
        }

        private void CategoriaForm_Load(object sender, EventArgs e) {
            this.GridData.Relations.AddSelfReference(this.GridData.MasterTemplate, "IdCategoria", "IdSubCategoria");
            this.TCategoria.Nuevo.Click += this.Nuevo_Click;
            this.TCategoria.Actualizar.Click += this.Actualizar_Click;
            this.TCategoria.Cerrar.Click += this.Cerrar_Click;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as CategoriaModel;
                if (seleccionado != null) {
                    Random d = new Random();
                    int d1 = d.Next() * -1;
                    var nuevo = new CategoriaModel { IdCategoria = d1, Activo = true, IdSubCategoria = seleccionado.IdCategoria };
                    this.Data.Add(nuevo);
                }
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Espere un momento...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.Data;
            this.treeView1.DataSource = this.Data;
            this.treeView1.TreeViewElement.AllowAlternatingRowColor = true;
            this.treeView1.DisplayMember = "Descripcion";
            this.treeView1.ParentMember = "IdSubCategoria";
            this.treeView1.ChildMember = "IdCategoria";
            this.ExpandNodes();
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            this.Data = this.Service.GetList();
        }

        private void ExpandNodes() {
            using (this.treeView1.DeferRefresh()) {
                RadTreeNode root = this.treeView1.Nodes[0];
                root.Expand();
                int index = 0;
                foreach (RadTreeNode node in root.Nodes) {
                    if (index % 2 == 0) { node.Expand(); }
                    index++;
                }
                if (root.Nodes.Count > 0) {
                    root.Nodes[root.Nodes.Count - 1].ExpandAll();
                }
            }
        }
    }
}
