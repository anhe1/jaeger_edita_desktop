﻿namespace Jaeger.UI.Tienda.Forms
{
    partial class PedidoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PedidoForm));
            this.pedidoControl = new Jaeger.UI.Tienda.Forms.PedidoClienteControl();
            this.TConcepto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.labelPartidas = new Telerik.WinControls.UI.RadLabelElement();
            this.toolStripStatusLabel1 = new Telerik.WinControls.UI.RadLabelElement();
            this.dataGridProductos = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProductos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pedidoControl
            // 
            this.pedidoControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.pedidoControl.Location = new System.Drawing.Point(0, 0);
            this.pedidoControl.Name = "pedidoControl";
            this.pedidoControl.Size = new System.Drawing.Size(1176, 118);
            this.pedidoControl.TabIndex = 0;
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Etiqueta = "Conceptos:";
            this.TConcepto.Location = new System.Drawing.Point(0, 118);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ReadOnly = false;
            this.TConcepto.ShowActualizar = false;
            this.TConcepto.ShowAutorizar = false;
            this.TConcepto.ShowCerrar = false;
            this.TConcepto.ShowEditar = false;
            this.TConcepto.ShowExportarExcel = false;
            this.TConcepto.ShowFiltro = true;
            this.TConcepto.ShowGuardar = false;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImagen = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = true;
            this.TConcepto.ShowRemover = true;
            this.TConcepto.Size = new System.Drawing.Size(1176, 30);
            this.TConcepto.TabIndex = 1;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.labelPartidas,
            this.toolStripStatusLabel1});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 565);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(1176, 26);
            this.radStatusStrip1.TabIndex = 2;
            // 
            // labelPartidas
            // 
            this.labelPartidas.Name = "labelPartidas";
            this.radStatusStrip1.SetSpring(this.labelPartidas, false);
            this.labelPartidas.Text = "Partidas: 0";
            this.labelPartidas.TextWrap = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.radStatusStrip1.SetSpring(this.toolStripStatusLabel1, false);
            this.toolStripStatusLabel1.Text = "";
            this.toolStripStatusLabel1.TextWrap = true;
            // 
            // dataGridProductos
            // 
            this.dataGridProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridProductos.Location = new System.Drawing.Point(0, 148);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Cantidad";
            gridViewTextBoxColumn1.FormatString = "{0:N2}";
            gridViewTextBoxColumn1.HeaderText = "Cantidad";
            gridViewTextBoxColumn1.Name = "Cantidad";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 65;
            gridViewTextBoxColumn2.FieldName = "Unidad";
            gridViewTextBoxColumn2.HeaderText = "Unidad";
            gridViewTextBoxColumn2.Name = "Unidad";
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "Catalogo";
            gridViewTextBoxColumn3.HeaderText = "Catálogo";
            gridViewTextBoxColumn3.Name = "Catalogo";
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.FieldName = "Producto";
            gridViewTextBoxColumn4.HeaderText = "Producto";
            gridViewTextBoxColumn4.Name = "Producto";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.FieldName = "Marca";
            gridViewTextBoxColumn5.HeaderText = "Marca";
            gridViewTextBoxColumn5.Name = "Marca";
            gridViewTextBoxColumn5.Width = 150;
            gridViewTextBoxColumn6.FieldName = "Modelo";
            gridViewTextBoxColumn6.HeaderText = "Modelo";
            gridViewTextBoxColumn6.Name = "Modelo";
            gridViewTextBoxColumn6.Width = 150;
            gridViewTextBoxColumn7.FieldName = "Especificacion";
            gridViewTextBoxColumn7.HeaderText = "Especificación";
            gridViewTextBoxColumn7.Name = "Especificacion";
            gridViewTextBoxColumn7.Width = 100;
            gridViewTextBoxColumn8.FieldName = "Tamanio";
            gridViewTextBoxColumn8.HeaderText = "Variante";
            gridViewTextBoxColumn8.Name = "Tamanio";
            gridViewTextBoxColumn8.Width = 115;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Unitario";
            gridViewTextBoxColumn9.FormatString = "{0:N2}";
            gridViewTextBoxColumn9.HeaderText = "Unitario";
            gridViewTextBoxColumn9.Name = "Unitario";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "SubTotal";
            gridViewTextBoxColumn10.FormatString = "{0:N2}";
            gridViewTextBoxColumn10.HeaderText = "SubTotal";
            gridViewTextBoxColumn10.Name = "SubTotal";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "Descuento";
            gridViewTextBoxColumn11.FormatString = "{0:N2}";
            gridViewTextBoxColumn11.HeaderText = "Descuento";
            gridViewTextBoxColumn11.Name = "Descuento";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "Importe";
            gridViewTextBoxColumn12.FormatString = "{0:N2}";
            gridViewTextBoxColumn12.HeaderText = "Importe";
            gridViewTextBoxColumn12.Name = "Importe";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "TasaIVA";
            gridViewTextBoxColumn13.FormatString = "{0:N2}";
            gridViewTextBoxColumn13.HeaderText = "% IVA";
            gridViewTextBoxColumn13.Name = "TasaIVA";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "TrasladoIVA";
            gridViewTextBoxColumn14.FormatString = "{0:N2}";
            gridViewTextBoxColumn14.HeaderText = "TrasladoIVA";
            gridViewTextBoxColumn14.Name = "TrasladoIVA";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.FieldName = "Total";
            gridViewTextBoxColumn15.FormatString = "{0:N2}";
            gridViewTextBoxColumn15.HeaderText = "Total";
            gridViewTextBoxColumn15.Name = "Total";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 75;
            gridViewTextBoxColumn16.FieldName = "Identificador";
            gridViewTextBoxColumn16.HeaderText = "Identificador";
            gridViewTextBoxColumn16.Name = "Identificador";
            gridViewTextBoxColumn16.Width = 115;
            gridViewTextBoxColumn17.FieldName = "FechaNuevo";
            gridViewTextBoxColumn17.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn17.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn17.Name = "FechaNuevo";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn17.Width = 75;
            this.dataGridProductos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17});
            this.dataGridProductos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridProductos.Name = "dataGridProductos";
            this.dataGridProductos.Size = new System.Drawing.Size(1176, 417);
            this.dataGridProductos.TabIndex = 3;
            // 
            // PedidoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 591);
            this.Controls.Add(this.dataGridProductos);
            this.Controls.Add(this.radStatusStrip1);
            this.Controls.Add(this.TConcepto);
            this.Controls.Add(this.pedidoControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PedidoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Pedido";
            this.Load += new System.EventHandler(this.PedidoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProductos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PedidoClienteControl pedidoControl;
        private Common.Forms.ToolBarStandarControl TConcepto;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadGridView dataGridProductos;
        private Telerik.WinControls.UI.RadLabelElement labelPartidas;
        private Telerik.WinControls.UI.RadLabelElement toolStripStatusLabel1;
    }
}
