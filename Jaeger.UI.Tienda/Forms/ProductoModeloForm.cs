﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Tienda.Contracts;

namespace Jaeger.UI.Tienda.Forms {
    public partial class ProductoModeloForm : RadForm {
        #region declaraciones
        protected internal ICatalogoProductosService _Service;
        protected internal ModeloXDetailModel _Modelo;
        protected internal List<UnidadModel> _Unidades;
        protected internal List<VisibilidadModel> _Visibilidad;
        protected internal BindingList<EspecificacionModel> _Especifiaciones;
        private readonly bool IsNew = false;
        public event EventHandler<ModeloXDetailModel> Agregate;
        #endregion

        public void OnAgregate(ModeloXDetailModel e) {
            if (this.Agregate != null)
                this.Agregate(this, e);
        }

        public ProductoModeloForm(ICatalogoProductosService service, ModeloXDetailModel modelo) {
            InitializeComponent();
            this._Service = service;
            this._Modelo = modelo;
            this.IsNew = this._Modelo.IdModelo == 0;
        }

        private void ProductoModeloForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.gridProveedor.Standard();
            this.gVariante.Standard();

            this._Especifiaciones = this._Service.GetEspecificaciones();
            this._Visibilidad = this._Service.GetVisibilidad();

            this.TProducto.Actualizar.Click += TModelo_Actualizar_Click;
            this.TProducto.Actualizar.Image = Properties.Resources.Web_Design_16px;
            this.TProducto.Actualizar.Text = "Crear publicación";
            this.TProducto.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.TProducto.Filtro.Image = Properties.Resources.delete_16px;
            this.TProducto.Filtro.Text = "Remover publicación";
            this.TProducto.Filtro.Click += TProducto_Filtro_Click;

            this.pageViewPublicacion.Enabled = false;

            this.cboCategorias.DataSource = this._Service.GetList<ClasificacionProductoModel>(new List<Domain.Base.Builder.IConditional>());
            this.cboCategorias.EditorControl.EnableAlternatingRowColor = true;
            this.cboCategorias.EditorControl.AllowRowResize = false;
            this.cboCategorias.EditorControl.AllowSearchRow = true;
            this.cboCategorias.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;

            this.Seccion.DataSource = this._Service.GetList<CategoriaSingle>(new List<Domain.Base.Builder.IConditional>());
            this.Seccion.EditorControl.EnableAlternatingRowColor = true;
            this.Seccion.EditorControl.AllowRowResize = false;
            this.Seccion.EditorControl.AllowSearchRow = true;
            this.Seccion.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;

            this.cboTipo.DataSource = Aplication.Almacen.Services.CatalogoProductoService.GetTipos();
            this.cboTipo.DisplayMember = "Descripcion";
            this.cboTipo.ValueMember = "Id";

            this._Unidades = this._Service.GetUnidades().ToList();
            this.CboUnidadAlmacen.DataSource = this._Unidades;

            this.cboUnidadXY.DataSource = new List<UnidadModel>(this._Unidades);
            this.cboUnidadXY.ValueMember = "IdUnidad";
            this.cboUnidadXY.DisplayMember = "Descripcion";


            this.cboUnidadZ.DataSource = new List<UnidadModel>(this._Unidades);
            this.cboUnidadZ.ValueMember = "IdUnidad";
            this.cboUnidadZ.DisplayMember = "Descripcion";

            this.cboTrasladoIVA.DisplayMember = "Descripcion";
            this.cboTrasladoIVA.ValueMember = "Descripcion";

            var cboEspecificacion = this.gVariante.Columns["IdEspecificacion"] as GridViewComboBoxColumn;
            cboEspecificacion.DisplayMember = "Descripcion";
            cboEspecificacion.ValueMember = "IdEspecificacion";
            cboEspecificacion.DataSource = this._Especifiaciones;

            var cboVisible1 = this.gVariante.Columns["IdVisibilidad"] as GridViewComboBoxColumn;
            cboVisible1.DisplayMember = "Descripcion";
            cboVisible1.ValueMember = "Id";
            cboVisible1.DataSource = this._Visibilidad;

            var cboVisible = this.gVariante.Columns["IdUnidad"] as GridViewComboBoxColumn;
            cboVisible.DisplayMember = "Descripcion";
            cboVisible.ValueMember = "Id";
            cboVisible.DataSource = new List<UnidadModel>(this._Unidades);

            this.TProducto.Guardar.Click += this.TModelo_Guardar_Click;
            this.TProducto.Cerrar.Click += this.TModelo_Cerrar_Click;

            this.TDisponible.Nuevo.Click += TDisponible_Nuevo_Click;
            this.TDisponible.Nuevo.ToolTipText = "Al agregar la disponibilidad el precio sera basado en lista de precios.";
            this.TDisponible.Remover.Click += TDisponible_Remover_Click;
            this.gVariante.AllowEditRow = true;
            this.CreateBinding();
        }

        private void TProducto_Filtro_Click(object sender, EventArgs e) {

        }

        private void TModelo_Actualizar_Click(object sender, EventArgs e) {
            this._Modelo.Publicacion = new PublicacionModel();
            this.Publicar();
            this.TProducto.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.TProducto.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.pageViewPublicacion.Enabled = true;


        }

        public virtual void TModelo_Guardar_Click(object sender, EventArgs eventArgs) {
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }

            if (this._Modelo.IdModelo > 0 && this.IsNew) {
                this.OnAgregate(this._Modelo);
                this.Close();
            }
        }

        public virtual void TModelo_Cerrar_Click(object sender, EventArgs eventArgs) {
            this.Close();
        }

        protected virtual void CboCategorias_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.cboCategorias.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var receptor = temporal.DataBoundItem as ClasificacionProductoModel;
                if (receptor != null) {
                    this._Modelo.IdCategoria = receptor.IdCategoria;
                }
            }
        }

        #region
        protected virtual void TDisponible_Nuevo_Click(object sender, EventArgs e) {
            if (this._Modelo.Autorizado == 0) {
                if (this._Modelo.Unitario > 0) {
                    RadMessageBox.Show(this, Properties.Resources.msg_variante_agregar_error, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }
                this._Modelo.Disponibles.Add(new ModeloYEspecificacionModel());
            } else {
                RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        protected virtual void TDisponible_Remover_Click(object sender, EventArgs e) {
            if (this._Modelo.Autorizado == 0) {
                var variante = this.gVariante.CurrentRow.DataBoundItem as ModeloYEspecificacionModel;
                if (variante.Id == 0) {
                    this.gVariante.Rows.Remove(this.gVariante.CurrentRow);
                } else {
                    variante.Activo = false;
                    this.gVariante.Tag = variante;
                    this.gVariante.CurrentRow.IsVisible = false;
                }
            } else {
                RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }
        #endregion

        public virtual void CreateBinding() {
            this.cboCategorias.DataBindings.Clear();
            this.cboCategorias.DataBindings.Add("SelectedValue", this._Modelo, "IdProducto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTipo.DataBindings.Clear();
            this.cboTipo.DataBindings.Add("SelectedValue", this._Modelo, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbModelo.DataBindings.Clear();
            this.txbModelo.DataBindings.Add("Text", this._Modelo, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Activo.DataBindings.Clear();
            this.Activo.DataBindings.Add("Checked", this._Modelo, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbMarca.DataBindings.Clear();
            this.txbMarca.DataBindings.Add("Text", this._Modelo, "Marca", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbExpecificacion.DataBindings.Clear();
            this.txbExpecificacion.DataBindings.Add("Text", this._Modelo, "Especificacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTipo.DataBindings.Clear();
            this.cboTipo.DataBindings.Add("SelectedValue", this._Modelo, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbCodigoBarras.DataBindings.Clear();
            this.txbCodigoBarras.DataBindings.Add("Text", this._Modelo, "Codigo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxtSKU.DataBindings.Clear();
            this.TxtSKU.DataBindings.Add("Text", this._Modelo, "NoIdentificacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TxtSKU.DataBindings.Add("Enabled", this._Modelo, "IsExistSKU", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbLargo.DataBindings.Clear();
            this.txbLargo.DataBindings.Add("Text", this._Modelo, "Largo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbAncho.DataBindings.Clear();
            this.txbAncho.DataBindings.Add("Text", this._Modelo, "Ancho", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbAlto.DataBindings.Clear();
            this.txbAlto.DataBindings.Add("Text", this._Modelo, "Alto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbPrecioBase.DataBindings.Clear();
            this.txbPrecioBase.DataBindings.Add("Value", this._Modelo, "Unitario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockMinimo.DataBindings.Clear();
            this.TxbStockMinimo.DataBindings.Add("Text", this._Modelo, "Minimo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockMaximo.DataBindings.Clear();
            this.TxbStockMaximo.DataBindings.Add("Text", this._Modelo, "Maximo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbStockReorden.DataBindings.Clear();
            this.TxbStockReorden.DataBindings.Add("Text", this._Modelo, "ReOrden", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbExistencia.DataBindings.Clear();
            this.TxbExistencia.DataBindings.Add("Text", this._Modelo, "Existencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboUnidadAlmacen.DataBindings.Clear();
            this.CboUnidadAlmacen.DataBindings.Add("SelectedValue", this._Modelo, "IdUnidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboTrasladoIVA.DataBindings.Clear();
            this.cboTrasladoIVA.DataBindings.Add("Text", this._Modelo, "FactorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbTrasladoIVA.DataBindings.Clear();
            this.txbTrasladoIVA.DataBindings.Add("Value", this._Modelo, "ValorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboUnidadXY.DataBindings.Clear();
            this.cboUnidadXY.DataBindings.Add("SelectedValue", this._Modelo, "UnidadXY", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboUnidadZ.DataBindings.Clear();
            this.cboUnidadZ.DataBindings.Add("SelectedValue", this._Modelo, "UnidadZ", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gVariante.DataSource = this._Modelo.Disponibles;

            if (this._Modelo.Publicacion != null) {
                this.TProducto.Filtro.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                this.pageViewPublicacion.Enabled = true;
                this.Publicar();
            } else {
                this.TProducto.Actualizar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                this.pageViewPublicacion.Enabled = false;
            }
        }

        public virtual void Publicar() {
            this.Publicacion.DataBindings.Clear();
            this.Publicacion.DataBindings.Add("DocumentHTML", this._Modelo.Publicacion, "Contenido", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Seccion.DataBindings.Clear();
            this.Seccion.DataBindings.Add("SelectedValue", this._Modelo.Publicacion, "IdCategoria", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Etiquetas.DataBindings.Clear();
            this.Etiquetas.DataBindings.Add("Text", this._Modelo.Publicacion, "Etiquetas", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public virtual void Guardar() {
            this._Modelo = this._Service.Save(this._Modelo);
        }
    }
}
