﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PreguntaForm : RadForm {
        protected internal IPreguntasFrecuentesService service;
        protected internal PreguntaFrecuenteModel model;
        protected bool IsNew = false;
        public event EventHandler<PreguntaFrecuenteModel> Agregate;

        public void OnAgregate(PreguntaFrecuenteModel e) {
            if (this.Agregate != null)
                this.Agregate(this, e);
        }

        public PreguntaForm() {
            InitializeComponent();
        }

        public PreguntaForm(IPreguntasFrecuentesService service, PreguntaFrecuenteModel model) {
            InitializeComponent();
            this.service = service;
            this.model = model;
        }

        private void PreguntaForm_Load(object sender, EventArgs e) {
            if (model == null) {
                this.model = new PreguntaFrecuenteModel();
                this.IsNew = true;
            }
            this.CreateBindig();
        }

        protected virtual void CreateBindig() {
            this.txbPregunta.DataBindings.Clear();
            this.txbPregunta.DataBindings.Add("Text", this.model, "Pregunta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbRespuesta.DataBindings.Clear();
            this.txbRespuesta.DataBindings.Add("DocumentHTML", this.model, "Respuesta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.chkActivo.DataBindings.Clear();
            this.chkActivo.DataBindings.Add("Checked", this.model, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        protected virtual void BtnGuadar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Espere un momento...";
                espera.ShowDialog(this);
            }
            if (this.model.Id > 0 && this.IsNew) {
                this.OnAgregate(this.model);
                this.Close();
            }
        }

        protected virtual void BtnCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void Guardar() {
            this.model = this.service.Save(this.model);
        }
    }
}
