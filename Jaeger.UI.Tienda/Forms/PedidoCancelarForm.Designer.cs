﻿namespace Jaeger.UI.Tienda.Forms {
    partial class PedidoCancelarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PedidoCancelarForm));
            this.MotivoCancelacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.Cliente = new Telerik.WinControls.UI.RadTextBox();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.IdStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.lblStatus = new Telerik.WinControls.UI.RadLabel();
            this.Relacionado = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.TCancelar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MotivoCancelacion
            // 
            this.MotivoCancelacion.AutoSizeDropDownHeight = true;
            this.MotivoCancelacion.AutoSizeDropDownToBestFit = true;
            this.MotivoCancelacion.DisplayMember = "Descriptor";
            this.MotivoCancelacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MotivoCancelacion.NestedRadGridView
            // 
            this.MotivoCancelacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MotivoCancelacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotivoCancelacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MotivoCancelacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.MotivoCancelacion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.MotivoCancelacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.MotivoCancelacion.EditorControl.Name = "NestedRadGridView";
            this.MotivoCancelacion.EditorControl.ReadOnly = true;
            this.MotivoCancelacion.EditorControl.ShowGroupPanel = false;
            this.MotivoCancelacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MotivoCancelacion.EditorControl.TabIndex = 0;
            this.MotivoCancelacion.Location = new System.Drawing.Point(139, 100);
            this.MotivoCancelacion.Name = "MotivoCancelacion";
            this.MotivoCancelacion.NullText = "Selecciona";
            this.MotivoCancelacion.Size = new System.Drawing.Size(285, 20);
            this.MotivoCancelacion.TabIndex = 11;
            this.MotivoCancelacion.TabStop = false;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(12, 101);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(121, 18);
            this.radLabel6.TabIndex = 10;
            this.radLabel6.Text = "Motivo de cancelación:";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.FechaEntrega);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Cliente);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.Nota);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.IdStatus);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.Relacionado);
            this.groupBox1.Controls.Add(this.radLabel2);
            this.groupBox1.Controls.Add(this.IdDocumento);
            this.groupBox1.Controls.Add(this.radLabel1);
            this.groupBox1.Controls.Add(this.radLabel6);
            this.groupBox1.Controls.Add(this.MotivoCancelacion);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "Para cada pedido que desea cancelar debe capturar el motivo de cancelación";
            this.groupBox1.Location = new System.Drawing.Point(0, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(443, 165);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Para cada pedido que desea cancelar debe capturar el motivo de cancelación";
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Location = new System.Drawing.Point(226, 26);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.ReadOnly = true;
            this.FechaEntrega.Size = new System.Drawing.Size(199, 20);
            this.FechaEntrega.TabIndex = 3;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "martes, 27 de julio de 2021";
            this.FechaEntrega.Value = new System.DateTime(2021, 7, 27, 22, 12, 11, 710);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(187, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 18);
            this.label11.TabIndex = 2;
            this.label11.Text = "Fecha:";
            // 
            // Cliente
            // 
            this.Cliente.Location = new System.Drawing.Point(60, 75);
            this.Cliente.MaxLength = 100;
            this.Cliente.Name = "Cliente";
            this.Cliente.ReadOnly = true;
            this.Cliente.Size = new System.Drawing.Size(364, 20);
            this.Cliente.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Cliente:";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(99, 125);
            this.Nota.MaxLength = 100;
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(325, 20);
            this.Nota.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "Observaciones:";
            // 
            // IdStatus
            // 
            this.IdStatus.DisplayMember = "Descriptor";
            this.IdStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdStatus.Enabled = false;
            this.IdStatus.Location = new System.Drawing.Point(57, 26);
            this.IdStatus.Name = "IdStatus";
            this.IdStatus.Size = new System.Drawing.Size(120, 20);
            this.IdStatus.TabIndex = 1;
            this.IdStatus.ValueMember = "Id";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(12, 27);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 18);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Status:";
            // 
            // Relacionado
            // 
            this.Relacionado.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Relacionado.Location = new System.Drawing.Point(343, 51);
            this.Relacionado.Name = "Relacionado";
            this.Relacionado.NullText = "#Pedido";
            this.Relacionado.ReadOnly = true;
            this.Relacionado.Size = new System.Drawing.Size(81, 20);
            this.Relacionado.TabIndex = 7;
            this.Relacionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(233, 52);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(105, 18);
            this.radLabel2.TabIndex = 6;
            this.radLabel2.Text = "Pedido relacionado:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdDocumento.Location = new System.Drawing.Point(106, 51);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.NullText = "#Pedido";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Size = new System.Drawing.Size(81, 20);
            this.IdDocumento.TabIndex = 5;
            this.IdDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 52);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(90, 18);
            this.radLabel1.TabIndex = 4;
            this.radLabel1.Text = "Núm. de Pedido:";
            // 
            // TCancelar
            // 
            this.TCancelar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCancelar.Etiqueta = "";
            this.TCancelar.Location = new System.Drawing.Point(0, 0);
            this.TCancelar.Name = "TCancelar";
            this.TCancelar.ReadOnly = false;
            this.TCancelar.ShowActualizar = false;
            this.TCancelar.ShowAutorizar = true;
            this.TCancelar.ShowCerrar = true;
            this.TCancelar.ShowEditar = false;
            this.TCancelar.ShowExportarExcel = false;
            this.TCancelar.ShowFiltro = false;
            this.TCancelar.ShowGuardar = false;
            this.TCancelar.ShowHerramientas = false;
            this.TCancelar.ShowImagen = false;
            this.TCancelar.ShowImprimir = false;
            this.TCancelar.ShowNuevo = false;
            this.TCancelar.ShowRemover = false;
            this.TCancelar.Size = new System.Drawing.Size(443, 29);
            this.TCancelar.TabIndex = 0;
            // 
            // PedidoCancelarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 194);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TCancelar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PedidoCancelarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cancelación";
            this.Load += new System.EventHandler(this.PedidoCancelarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TCancelar;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MotivoCancelacion;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadTextBox Relacionado;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        internal Telerik.WinControls.UI.RadTextBox IdDocumento;
        internal Telerik.WinControls.UI.RadDropDownList IdStatus;
        private Telerik.WinControls.UI.RadLabel lblStatus;
        public Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel label4;
        public Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        private Telerik.WinControls.UI.RadLabel label11;
        public Telerik.WinControls.UI.RadTextBox Cliente;
        private Telerik.WinControls.UI.RadLabel label7;
    }
}