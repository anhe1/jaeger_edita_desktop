﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Tienda.Services;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PedidoCatalogoForm : RadForm {
        protected internal PedidoClientePrinter Printer;
        
        public PedidoCatalogoForm() {
            InitializeComponent();
        }

        private void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            //this.TPedido.ShowNuevo = false;
            //this.TPedido.ShowEditar = false;
            //this.TPedido.ShowCancelar = true;
            this.TPedido.ShowImprimir = true;
            this.TPedido.ContextImprimirC.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            
            this.TPedido.ContextImprimir.Click += this.TPedido.TPedido_Imprimir_Comprobante_Click;
            this.TPedido.ShowHerramientas = true;
            this.TPedido.ShowExportarExcel = true;
            this.TPedido.ShowAutosuma = true;
            this.TPedido.CreateView();
            this.TPedido.Nuevo.Click += this.TPedido_Nuevo_Click;
            this.TPedido.Editar.Click += this.TPedido_Editar_Click;
            this.TPedido.Cerrar.Click += this.TPedido_Cerrar_Click;
            this.TPedido.Consultar += TPedido_Consultar;
        }

        #region barra de herramientas
        public virtual void TPedido_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new PedidoForm(this.TPedido.Service) { MdiParent = ParentForm };
            _nuevo.Show();
        }

        public virtual void TPedido_Editar_Click(object sender, EventArgs e) {
            if (this.TPedido.GridData.CurrentRow != null) {
                var seleccionado = this.TPedido.GridData.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
                if (seleccionado != null) {
                    var nuevo = new PedidoForm(this.TPedido.Service, seleccionado.IdPedido) { MdiParent = ParentForm };
                    nuevo.Show();
                }
            }
        }

        public virtual void TPedido_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        public virtual void TPedido_Consultar() {
            this.TPedido._DataSource = new BindingList<PedidoClienteDetailModel>(
                this.TPedido.Service.GetList<PedidoClienteDetailModel>(
                    PedidosService.Query().Year(this.TPedido.GetEjercicio()).Month(TPedido.GetPeriodo()).Build()
                    ).ToList());
            
            this.Vendedores();
            var IdVendedor = this.TPedido.GridData.Columns["IdVendedor"] as GridViewComboBoxColumn;
            IdVendedor.DataSource = this.TPedido.Vendedores;
            IdVendedor.DisplayMember = "Clave";
            IdVendedor.ValueMember = "IdVendedor";
        }
        #endregion

        public virtual void Vendedores() {

        }
    }
}
