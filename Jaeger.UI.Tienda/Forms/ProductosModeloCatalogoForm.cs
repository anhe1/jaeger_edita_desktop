﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class ProductosModeloCatalogoForm : RadForm {
        #region declaraciones
        protected internal UIAction _permisos;
        protected Aplication.Tienda.Contracts.ICatalogoProductosService _Service;
        private BindingList<ClasificacionProductoModel> _Productos;
        private BindingList<ModeloXDetailModel> modelos;
        private List<UnidadModel> unidades;
        private BindingList<EspecificacionModel> especifiaciones;
        private List<VisibilidadModel> visibilidad;
        private List<VisibilidadModel> visibilidad1;
        private List<ProductoServicioTipoModel> tipos;
        public RadContextMenu menuContextual = new RadContextMenu();
        protected internal RadMenuItem RegistrosInactivos = new RadMenuItem { Text = "Registro inactivos", IsChecked = false, CheckOnClick = true };
        public RadMenuItem ContextualCopiar = new RadMenuItem { Text = "Copiar" };
        #endregion

        #region formulario
        public ProductosModeloCatalogoForm() {
            InitializeComponent();
        }

        private void ProductosCatalogoForm_Load(object sender, EventArgs e) {
            this.gModelos.Standard();
            this.gVariante.Standard();

            this.TModelo.Nuevo.Click += this.TModelos_Agregar_Click;
            this.TModelo.Editar.Click += this.TModelos_Editar_Click;
            this.TModelo.Actualizar.Click += this.TModelo_Acutalizar_Click;
            this.TModelo.Filtro.Click += this.TModelo_Filtrar_Click;
            this.TModelo.Herramientas.Items.Add(this.RegistrosInactivos);
            this.TModelo.Cerrar.Click += this.TModelo_Cerrar_Click;

            this.TDisponible.Nuevo.Click += this.TDisponible_Nuevo_Click;
            this.TDisponible.Remover.Click += this.TDisponible_Remover_Click;

            this.TImagen.Nuevo.Click += this.Nuevo_Click;
            this.TImagen.Remover.Click += this.Remover_Click;

            this.gImagen.TableElement.RowHeight = 300;
            this.gModelos.AllowEditRow = true;
            this.gVariante.AllowEditRow = true;

            this.gModelos.EditorRequired += this.GProducto_EditorRequired;
            this.gModelos.CellBeginEdit += this.GridModelos_CellBeginEdit;
            this.gModelos.CellEndEdit += this.RadGridView_CellEndEdit;
            this.gModelos.RowsChanging += this.RadGridView_RowsChanging;
            this.gModelos.CommandCellClick += this.RadGriViews_CommandCellClick;
            this.ContextualCopiar.Click += MContextual_Copiar_Click;
            this.menuContextual.Items.AddRange(this.ContextualCopiar);
            this.gImagen.ContextMenuOpening += new ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
        }
        #endregion

        #region barra de herramientas modelos
        private void TModelos_Agregar_Click(object sender, EventArgs e) {
            if (this.gModelos.CurrentRow != null) {
                var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
            }
        }

        private void Nuevo_Agregate(object sender, ModeloXDetailModel e) {
            if (e != null) {
                var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado != null) {
                }
            }
        }

        private void TModelos_Editar_Click(object sender, EventArgs e) {
            if (this.gModelos.CurrentRow != null) {
                var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 0) {
                } else {
                    RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        private void TModelo_Acutalizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            var cboCategoria = this.gModelos.Columns["IdProducto"] as GridViewComboBoxColumn;
            cboCategoria.DataSource = this._Productos;
            cboCategoria.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboCategoria.AutoCompleteMode = AutoCompleteMode.Suggest;
            cboCategoria.DropDownStyle = RadDropDownStyle.DropDownList;
            cboCategoria.DisplayMember = "Nombre";
            cboCategoria.ValueMember = "IdProducto";

            var combo = this.gModelos.Columns["FactorTrasladoIVA"] as GridViewComboBoxColumn;
            combo.DataSource = Enum.GetNames(typeof(ImpuestoTipoFactorEnum));

            // catalogo de unidades del almacen
            var cboUnidades = this.gModelos.Columns["IdUnidad"] as GridViewComboBoxColumn;
            cboUnidades.DisplayMember = "Descripcion";
            cboUnidades.ValueMember = "IdUnidad";
            cboUnidades.AutoSizeMode = BestFitColumnMode.DisplayedCells;
            cboUnidades.DropDownStyle = RadDropDownStyle.DropDownList;
            cboUnidades.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboUnidades.DataSource = this.unidades;

            var cboVisible = this.gModelos.Columns["IdVisible"] as GridViewComboBoxColumn;
            cboVisible.DisplayMember = "Descripcion";
            cboVisible.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboVisible.ValueMember = "Id";
            cboVisible.DataSource = this.visibilidad;

            var cboTipo = this.gModelos.Columns["Tipo"] as GridViewComboBoxColumn;
            cboTipo.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboTipo.AutoCompleteMode = AutoCompleteMode.Suggest;
            cboTipo.DropDownStyle = RadDropDownStyle.DropDownList;
            cboTipo.DisplayMember = "Descripcion";
            cboTipo.ValueMember = "Id";
            cboTipo.DataSource = this.tipos;

            var cboEspecificacion = this.gVariante.Columns["IdEspecificacion"] as GridViewComboBoxColumn;
            cboEspecificacion.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboEspecificacion.DataSource = this.especifiaciones;
            cboEspecificacion.DisplayMember = "Descripcion";
            cboEspecificacion.ValueMember = "IdEspecificacion";

            var cboVisible1 = this.gVariante.Columns["IdVisibilidad"] as GridViewComboBoxColumn;
            cboVisible1.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboVisible1.DisplayMember = "Descripcion";
            cboVisible1.ValueMember = "Id";
            cboVisible1.DataSource = this.visibilidad1;

            this.gModelos.DataSource = this.modelos;
            this.gVariante.DataSource = this.modelos;
            this.gImagen.DataSource = this.modelos;
            this.gVariante.DataMember = "Disponibles";
            this.gImagen.DataMember = "Medios";
        }

        private void TModelo_Filtrar_Click(object sender, EventArgs e) {
            this.gModelos.ShowFilteringRow = this.TModelo.Filtro.ToggleState != ToggleState.On;
            if (this.gModelos.ShowFilteringRow == false)
                this.gModelos.FilterDescriptors.Clear();
        }

        private void TModelo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region disponibles
        private void TDisponible_Nuevo_Click(object sender, EventArgs e) {
            if (this.gModelos.CurrentRow != null) {
                var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 0) {
                    if (seleccionado.Unitario > 0) {
                        RadMessageBox.Show(this, Properties.Resources.msg_variante_agregar_error, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    seleccionado.Disponibles.Add(new ModeloYEspecificacionModel());
                } else {
                    RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }

        private void TDisponible_Remover_Click(object sender, EventArgs e) {
            if (this.gVariante.CurrentRow != null) {
                var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Autorizado == 0) {
                    var variante = this.gVariante.CurrentRow.DataBoundItem as ModeloYEspecificacionModel;
                    if (variante.Id == 0) {
                        this.gVariante.Rows.Remove(this.gVariante.CurrentRow);
                    } else {
                        variante.Activo = false;
                    }
                } else {
                    RadMessageBox.Show(this, "No es posible editar un modelo autorizado", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
            }
        }
        #endregion

        #region imagenes
        private void Nuevo_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.jpg|*.JPG|*.jpeg|*.JPEG" };
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;
            if (System.IO.File.Exists(openFile.FileName) == false) {
                RadMessageBox.Show("No existe el archivo");
                return;
            }

            var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
            var nuevo = this._Service.Save(new MedioModel() { IdModelo = seleccionado.IdModelo }, openFile.FileName, string.Format("{0}-{1}-{2}.jpg", seleccionado.NoIdentificacion, seleccionado.Descripcion.ToLower().Replace(" ", "-").Trim(), DateTime.Now.ToString("yyyyMMddHHmmss")));
            seleccionado.Medios.Add(nuevo);
            this._Service.Save(seleccionado);
        }

        private void Remover_Click(object sender, EventArgs e) {
            if (this.gImagen.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_remover_objeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    var seleccionado = this.gImagen.CurrentRow.DataBoundItem as MedioModel;
                    if (seleccionado.Id > 0) {
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this._Productos = new BindingList<ClasificacionProductoModel>(this._Service.GetList<ClasificacionProductoModel>(Aplication.Tienda.Services.CatalogoModeloService.Query().ByIdAlmacen(AlmacenEnum.PT).Activo(true).Build()).ToList());
            this.modelos = new BindingList<ModeloXDetailModel>(this._Service.GetList<ModeloXDetailModel>(Aplication.Tienda.Services.CatalogoModeloService.Query().ByIdAlmacen(AlmacenEnum.PT).Activo(!this.RegistrosInactivos.IsChecked).Build()).ToList());
            this.unidades = this._Service.GetList<UnidadModel>(Aplication.Almacen.Services.UnidadService.Query().ByAlmacen(this._Service.Almacen).Activo().Build()).ToList();
            this.especifiaciones = this._Service.GetEspecificaciones();
            this.visibilidad = this._Service.GetVisibilidad();
            this.visibilidad1 = this._Service.GetVisibilidad();
            this.tipos = Aplication.Almacen.Services.CatalogoProductoService.GetTipos();
        }

        private void Modelo_Guardar() {
            var seleccionado = this.gModelos.CurrentRow.DataBoundItem as ModeloXDetailModel;
            if (seleccionado != null) {
                this._Service.Save(seleccionado);
            }
            this.gModelos.Tag = null;
        }

        private void Variante_Guardar() {
            var seleccionado = this.gVariante.Tag as ModeloYEspecificacionModel;
            if (seleccionado != null) {
            }
            this.gVariante.Tag = null;
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {

            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.gImagen.CurrentRow.ViewInfo.ViewTemplate == this.gImagen.MasterTemplate) {
                    e.ContextMenu = this.menuContextual.DropDown;
                }
            }
        }

        private void GProducto_EditorRequired(object sender, EditorRequiredEventArgs e) {
            if (this.gModelos.CurrentColumn is GridViewComboBoxColumn && this.gModelos.CurrentRow is GridViewFilteringRowInfo) {
                RadTextBoxEditor editor = new RadTextBoxEditor();
                e.Editor = editor;
            }
        }

        private void GridModelos_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (this.gModelos.CurrentRow is GridViewFilteringRowInfo) {
                return;
            }
            var seleccionado = e.Row.DataBoundItem as ModeloXDetailModel;
            if (seleccionado != null) {
                if (seleccionado.Autorizado > 0)
                    e.Cancel = true;
                    return;
            }
            if (this.gModelos.CurrentColumn is GridViewMultiComboBoxColumn) {
                var editor = (RadMultiColumnComboBoxElement)this.gModelos.ActiveEditor;
                if ((string)editor.Tag == "listo") { return; }
                editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "IdProducto", Name = "IdProducto", FieldName = "IdProducto", IsVisible = false });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "SubClase", Name = "SubClase", FieldName = "SubClase" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Producto", Name = "Producto", FieldName = "Producto" });
                editor.AutoSizeDropDownToBestFit = true;
                editor.Tag = "listo";
            } else if(e.Column.Name == "Unitario") {
                //var seleccionado = e.Row.DataBoundItem as ModeloXDetailModel;
                if (seleccionado.Activo == true && seleccionado.Autorizado == 0) {
                    if (seleccionado.Disponibles.Count > 0) {
                        RadMessageBox.Show(this, "No es posible asignar un precio unitario si el modelo tiene variantes", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                        e.Cancel = true;
                    }
                }
            }
        }

        private void RadGridView_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (this.TModelo.Tag != null) {
                if ((bool)this.TModelo.Tag == true) {
                    if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.gModelos.Name) {
                        var seleccionado = e.Row.DataBoundItem as ModeloXDetailModel;
                        if (seleccionado != null) {
                            this.gModelos.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Modelo_Guardar)) {
                                espera.Text = "Guardando...";
                                espera.ShowDialog(this);
                            }
                        }
                    } else if (e.Column.OwnerTemplate.MasterTemplate.Owner.Name == this.gVariante.Name) {
                        var seleccionado = e.Row.DataBoundItem as ModeloYEspecificacionModel;
                        if (seleccionado != null) {
                            this.gVariante.Tag = seleccionado;
                            using (var espera = new Waiting2Form(this.Variante_Guardar)) {
                                espera.Text = "Guardando ...";
                                espera.ShowDialog(this);
                            }
                        }
                    }
                }
            }
            this.TModelo.Tag = null;
        }

        private void RadGriViews_CommandCellClick(object sender, GridViewCellEventArgs e) {
            var seleccionado = e.Row.DataBoundItem as ModeloXDetailModel;
            if (seleccionado != null) {
                if (seleccionado.Activo == false) {
                    RadMessageBox.Show(this, "No es posible editar un modelo inactivo", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }
                    
                if (this.TModelo.Autorizar.Enabled) {
                    seleccionado.Autorizado = (seleccionado.Autorizado == 1 ? 0 : 1);
                    using (var espera = new Waiting2Form(this.Modelo_Guardar)) {
                        espera.Text = "Guardando...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        private void RadGridView_RowsChanging(object sender, GridViewCollectionChangingEventArgs e) {
            if (e.Action == Telerik.WinControls.Data.NotifyCollectionChangedAction.ItemChanging) {
                if (e.OldValue == e.NewValue) {
                    e.Cancel = true;
                } else
                    this.TModelo.Tag = true;
            }
        }
        #endregion

        public virtual void MContextual_Copiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.gImagen.CurrentCell.Value.ToString());
        }
    }
}
