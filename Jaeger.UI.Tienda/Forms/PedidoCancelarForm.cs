﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Tienda.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PedidoCancelarForm : RadForm {
        private readonly IPedidoClienteDetailModel _Current;

        public PedidoCancelarForm(IPedidoClienteDetailModel model) {
            InitializeComponent();
            this._Current = model;
        }

        public event EventHandler<IPedidoClienteStatusDetailModel> Selected;
        public void OnSelected(IPedidoClienteStatusDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        private void PedidoCancelarForm_Load(object sender, EventArgs e) {
            this.MotivoCancelacion.DataSource = PedidoService.GetCancelacionTipo();
            this.FechaEntrega.Value = DateTime.Now;
            this.FechaEntrega.SetEditable(false);
            this.IdDocumento.Text = this._Current.IdPedido.ToString();
            this.Cliente.Text = this._Current.Cliente;
            this.IdStatus.DataSource = PedidoService.GetStatus();
            this.TCancelar.Autorizar.Click += this.TCancelar_Autorizar_Click;
            this.TCancelar.Cerrar.Click += this.TCancelar_Cerrar_Click;
        }

        #region barra de herramientas
        private void TCancelar_Autorizar_Click(object sender, EventArgs e) {
            var motivoCancelacion = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as PedidoCancelacionTipoModel;
            if (motivoCancelacion != null) {
                var _response = new PedidoClienteStatusDetailModel {
                    IdPedido = _Current.IdPedido,
                    CvMotivo = motivoCancelacion.Descriptor,
                    IdCvMotivo = motivoCancelacion.Id,
                    Cancela = ConfigService.Piloto.Clave,
                    FechaStatus = DateTime.Now,
                    IdStatus = 0,
                    IdDirectorio = _Current.IdDirectorio,
                    Total = _Current.Total,
                    Nota = this.Nota.Text
                };
                this.OnSelected(_response);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion
    }
}
