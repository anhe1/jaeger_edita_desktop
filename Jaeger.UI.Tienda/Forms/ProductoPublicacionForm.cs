﻿using System;
using System.ComponentModel;
using System.Drawing;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class ProductoPublicacionForm : UI.Almacen.Forms.CategoriasTreeViewForm {
        protected internal ICatalogoProductoService _Service;
        protected internal BindingList<ClasificacionProductoModel> _Productos;
        protected internal new BindingList<ModeloXDetailModel> _DataSource;
        protected internal BindingList<IClasificacionSingle> _Categorias;
        private BindingList<UnidadModel> _Unidades;

        public ProductoPublicacionForm() : base() { this.Load += ProductoPublicacionForm_Load; }

        private void ProductoPublicacionForm_Load(object sender, EventArgs e) {
            this.TControl.GridData.AllowRowReorder = false;
            this.TControl.TBar.Guardar.Visibility = ElementVisibility.Visible;
        }

        #region acciones del grid
        //required to initiate drag and drop when grid is in bound mode
        private void Svc_PreviewDragStart(object sender, PreviewDragStartEventArgs e) {
            e.CanStart = true;
        }

        private void Svc_PreviewDragOver(object sender, RadDragOverEventArgs e) {
            if (e.DragInstance is GridDataRowElement) {
                e.CanDrop = e.HitTarget is GridDataRowElement ||
                            e.HitTarget is GridTableElement ||
                            e.HitTarget is GridSummaryRowElement;
            }
        }

        //initiate the move of selected row
        private void Svc_PreviewDragDrop(object sender, RadDropEventArgs e) {
            GridDataRowElement rowElement = e.DragInstance as GridDataRowElement;

            if (rowElement == null) {
                return;
            }
            e.Handled = true;

            RadItem dropTarget = e.HitTarget as RadItem;
            RadGridView targetGrid = dropTarget.ElementTree.Control as RadGridView;
            if (targetGrid == null) {
                return;
            }

            var dragGrid = rowElement.ElementTree.Control as RadGridView;
            if (targetGrid == dragGrid) {
                e.Handled = true;

                GridDataRowElement dropTargetRow = dropTarget as GridDataRowElement;
                int index = dropTargetRow != null ? this.GetTargetRowIndex(dropTargetRow, e.DropLocation) : targetGrid.RowCount;
                GridViewRowInfo rowToDrag = dragGrid.SelectedRows[0];
                this.MoveRows(dragGrid, rowToDrag, index);
            }
        }

        private int GetTargetRowIndex(GridDataRowElement row, Point dropLocation) {
            int halfHeight = row.Size.Height / 2;
            int index = row.RowInfo.Index;
            if (dropLocation.Y > halfHeight) {
                index++;
            }
            return index;
        }

        private void MoveRows(RadGridView dragGrid, GridViewRowInfo dragRow, int index) {
            dragGrid.BeginUpdate();

            GridViewRowInfo row = dragRow;
            if (row is GridViewSummaryRowInfo) {
                return;
            }
            if (dragGrid.DataSource != null && typeof(System.Collections.IList).IsAssignableFrom(dragGrid.DataSource.GetType())) {
                //bound to a list of objects scenario
                var sourceCollection = (System.Collections.IList)dragGrid.DataSource;
                if (row.Index < index) {
                    index--;
                }
                sourceCollection.Remove(row.DataBoundItem);
                sourceCollection.Insert(index, row.DataBoundItem);
                var d0 = row.DataBoundItem as ModeloXDetailModel;
                d0.Secuencia = index;
                Console.WriteLine("indice = " + index);
            } else {
                throw new ApplicationException("Unhandled Scenario");
            }

            dragGrid.EndUpdate(true);
        }
        #endregion

        private void TreeCategorias_SelectedNodeChanged(object sender, RadTreeViewEventArgs e) {
            if (this.TControl.GridData.FilterDescriptors.Count > 0) {
                for (int i = 0; i < this.TControl.GridData.FilterDescriptors.Count; i++) {
                    if (this.TControl.GridData.FilterDescriptors[i].PropertyName == "IdCategoria") {
                        this.TControl.GridData.FilterDescriptors.RemoveAt(i);
                    }
                }
            }
            // en caso de ser nulo
            if (e.Node == null) { return; }
            if ((int)e.Node.Value == 0) {
                return;
            } else {
                FilterDescriptor filter = new FilterDescriptor {
                    PropertyName = "IdCategoria",
                    Operator = FilterOperator.IsEqualTo,
                    Value = e.Node.Value
                };
                this.TControl.GridData.FilterDescriptors.Add(filter);
            }
        }
    }
}
