﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.UI.Tienda.Forms {
    public partial class PrecioForm : RadForm {
        private PrecioDetailModel currentCatalogo = null;
        private List<EspecificacionModel> tamanios;
        private IPrecioCatalogoService service;

        public PrecioForm() {
            InitializeComponent();
        }

        public PrecioForm(IPrecioCatalogoService service, PrecioDetailModel model) {
            InitializeComponent();
            this.service = service;
            this.currentCatalogo = model;
        }

        private void PrecioForm_Load(object sender, EventArgs e) {
            this.dataGridPartidas.Standard();
            this.dataGridPartidas.AllowEditRow = true;
            this.Prioridad.DataSource = this.service.GetPrioridad();
            this.tamanios = this.service.GetTamanios();
            this.TCatalogo.Actualizar.Click += this.TCatalogo_Actualizar_Click;
            this.TCatalogo.Guardar.Click += this.TCatalogo_Guardar_Click;
            this.TCatalogo.Cerrar.Click += this.TCatalogo_Cerrar_Click;

            this.TPartida.Nuevo.Click += this.TPartida_Nuevo_Click;
            this.TPartida.Remover.Click += this.TPartida_Remover_Click;
            this.TCatalogo.Actualizar.PerformClick();
        }

        #region catalogo
        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            if (this.currentCatalogo == null) {
                this.currentCatalogo = new PrecioDetailModel();
            }

            var IdTamanio = this.dataGridPartidas.Columns["IdEspecificacion"] as GridViewComboBoxColumn;
            IdTamanio.DataSource = this.tamanios;
            IdTamanio.DisplayMember = "Descripcion";
            IdTamanio.ValueMember = "IdEspecificacion";

            this.CreateBinding();
        }

        private void TCatalogo_Guardar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion;

        private void TPartida_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridPartidas.CurrentRow != null) {
                var _seleccionado = this.dataGridPartidas.CurrentRow.DataBoundItem as PrecioPartidaModel;
                if (_seleccionado != null) {
                    if (RadMessageBox.Show(this, Properties.Resources.msg_Catalogo_Precio_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        if (_seleccionado.IdPrecio == 0) {
                            this.dataGridPartidas.Rows.Remove(this.dataGridPartidas.CurrentRow);
                        }
                        else {
                            _seleccionado.Activo = false;
                            this.dataGridPartidas.CurrentRow.IsVisible = false;
                        }
                    }
                }
            }
            this.Tag = null;
        }

        private void TPartida_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new PrecioPartidaModel() { IdPrecio = this.currentCatalogo.IdPrecio };
            this.currentCatalogo.Items.Add(_nuevo);
        }

        private void CreateBinding() {
            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this.currentCatalogo, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Prioridad.DataBindings.Clear();
            this.Prioridad.DataBindings.Add("SelectedValue", this.currentCatalogo, "IdPrioridad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Orden.DataBindings.Clear();
            this.Orden.DataBindings.Add("Value", this.currentCatalogo, "Secuencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Nota.DataBindings.Clear();
            this.Nota.DataBindings.Add("Text", this.currentCatalogo, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);
            this.dataGridPartidas.DataSource = this.currentCatalogo.Items;
        }

        private void Guardar() {
            this.currentCatalogo = this.service.Save(this.currentCatalogo);
        }
    }
}
