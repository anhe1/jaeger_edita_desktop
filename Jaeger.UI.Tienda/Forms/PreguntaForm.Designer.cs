﻿namespace Jaeger.UI.Tienda.Forms {
    partial class PreguntaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreguntaForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txbPregunta = new Telerik.WinControls.UI.RadTextBox();
            this.txbRespuesta = new Zoople.HTMLEditControl();
            this.lblRespuesta = new Telerik.WinControls.UI.RadLabel();
            this.chkActivo = new Telerik.WinControls.UI.RadCheckBox();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.btnGuadar = new Telerik.WinControls.UI.RadButton();
            this.btnCerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPregunta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRespuesta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGuadar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(784, 40);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txbPregunta
            // 
            this.txbPregunta.Location = new System.Drawing.Point(12, 27);
            this.txbPregunta.Name = "txbPregunta";
            this.txbPregunta.Size = new System.Drawing.Size(750, 20);
            this.txbPregunta.TabIndex = 2;
            // 
            // txbRespuesta
            // 
            this.txbRespuesta.AcceptsReturn = true;
            this.txbRespuesta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbRespuesta.AvailableInlineStyles = ((System.Collections.Generic.List<string>)(resources.GetObject("txbRespuesta.AvailableInlineStyles")));
            this.txbRespuesta.BaseURL = null;
            this.txbRespuesta.CleanMSWordHTMLOnPaste = true;
            this.txbRespuesta.CodeEditor.Enabled = true;
            this.txbRespuesta.CodeEditor.Locked = false;
            this.txbRespuesta.CodeEditor.TabWidth = 720;
            this.txbRespuesta.CodeEditor.WordWrap = false;
            this.txbRespuesta.CSSText = null;
            this.txbRespuesta.CustomColorPalette = null;
            this.txbRespuesta.DocumentHTML = null;
            this.txbRespuesta.EnableInlineSpelling = false;
            this.txbRespuesta.FontSizesList = null;
            this.txbRespuesta.FontsList = null;
            this.txbRespuesta.HiddenButtons = null;
            this.txbRespuesta.ImageStorageLocation = null;
            this.txbRespuesta.InCodeView = false;
            this.txbRespuesta.IndentAmount = 2;
            this.txbRespuesta.IndentsUseBlockuote = false;
            this.txbRespuesta.LanguageFile = null;
            this.txbRespuesta.LicenceActivationKey = null;
            this.txbRespuesta.LicenceKey = null;
            this.txbRespuesta.LicenceKeyInlineSpelling = null;
            this.txbRespuesta.Location = new System.Drawing.Point(12, 78);
            this.txbRespuesta.Name = "txbRespuesta";
            this.txbRespuesta.Size = new System.Drawing.Size(750, 278);
            this.txbRespuesta.SpellingAutoCorrectionList = ((System.Collections.Hashtable)(resources.GetObject("txbRespuesta.SpellingAutoCorrectionList")));
            this.txbRespuesta.TabIndex = 4;
            this.txbRespuesta.ToolstripImageScalingSize = new System.Drawing.Size(16, 16);
            this.txbRespuesta.UseParagraphAsDefault = true;
            // 
            // lblRespuesta
            // 
            this.lblRespuesta.Location = new System.Drawing.Point(12, 54);
            this.lblRespuesta.Name = "lblRespuesta";
            this.lblRespuesta.Size = new System.Drawing.Size(60, 18);
            this.lblRespuesta.TabIndex = 3;
            this.lblRespuesta.Text = "Respuesta:";
            // 
            // chkActivo
            // 
            this.chkActivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkActivo.Location = new System.Drawing.Point(566, 409);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(51, 18);
            this.chkActivo.TabIndex = 5;
            this.chkActivo.Text = "Activo";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txbPregunta);
            this.groupBox1.Controls.Add(this.txbRespuesta);
            this.groupBox1.Controls.Add(this.lblRespuesta);
            this.groupBox1.HeaderText = "Pregunta";
            this.groupBox1.Location = new System.Drawing.Point(0, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(779, 361);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pregunta";
            // 
            // btnGuadar
            // 
            this.btnGuadar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuadar.Location = new System.Drawing.Point(623, 407);
            this.btnGuadar.Name = "btnGuadar";
            this.btnGuadar.Size = new System.Drawing.Size(75, 23);
            this.btnGuadar.TabIndex = 7;
            this.btnGuadar.Text = "Guardar";
            this.btnGuadar.Click += new System.EventHandler(this.BtnGuadar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.Location = new System.Drawing.Point(704, 407);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 7;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // PreguntaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 439);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.chkActivo);
            this.Controls.Add(this.btnGuadar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PreguntaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pregunta";
            this.Load += new System.EventHandler(this.PreguntaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPregunta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRespuesta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGuadar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal System.Windows.Forms.PictureBox pictureBox1;
        protected internal Telerik.WinControls.UI.RadTextBox txbPregunta;
        protected internal Zoople.HTMLEditControl txbRespuesta;
        protected internal Telerik.WinControls.UI.RadLabel lblRespuesta;
        protected internal Telerik.WinControls.UI.RadCheckBox chkActivo;
        protected internal Telerik.WinControls.UI.RadGroupBox groupBox1;
        protected internal Telerik.WinControls.UI.RadButton btnGuadar;
        protected internal Telerik.WinControls.UI.RadButton btnCerrar;
    }
}