﻿namespace Jaeger.Aplication.Html.Contracts {
    /// <summary>
    /// clase para la conversion de templetes HTML a PDF
    /// </summary>
    public interface IHtmlToPDFService {
        /// <summary>
        /// obtener o establecer el logotipo que se incluira en la representacion impresa
        /// </summary>
        string Logo {
            get; set;
        }

        /// <summary>
        /// obtener o establcer la ruta del templete a utilizar, en caso de no existir se utiliza el templete por default almacenado en los recursos
        /// </summary>
        string PathTemplete {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ruta de archivos temporales
        /// </summary>
        string PathTemporal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ruta donde se almacenara la representacion impresa PDF
        /// </summary>
        string PathDefault {
            get; set;
        }

        string PDF {
            get; set;
        }

        /// <summary>
        /// crear representacion impresa xml a PDF
        /// </summary>
        /// <param name="cfdi">objeto Comprobante</param>
        /// <param name="keyName">nombre del archivo</param>
        /// <returns>ruta del archivo creado (PDF)</returns>
        string Procesar(object cfdi, string keyName);

        /// <summary>
        /// crear representacion impresa xml a PDF
        /// </summary>
        /// <param name="cfdi">objeto string XML valido</param>
        /// <param name="keyName">nombre del archivo</param>
        /// <returns>ruta del archivo creado (PDF)</returns>
        string Procesar(string cfdi, string keyName);
    }
}