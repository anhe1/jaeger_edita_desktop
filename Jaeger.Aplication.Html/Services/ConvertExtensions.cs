﻿using System;

namespace Jaeger.Aplication.Html.Services {
    public class ConvertExtensions {
        public ConvertExtensions() {
        }

        public static bool ConvertBool(object boolValue) {
            if (boolValue != DBNull.Value) {
                if (boolValue != null) {
                    if ((object)boolValue.ToString().Trim() != (object)string.Empty) {
                        return Convert.ToBoolean(boolValue);
                    }
                }
            }
            return false;
        }

        public static DateTime ConvertDateTime(object dtValue) {
            if (dtValue != DBNull.Value) {
                if (dtValue != null) {
                    if ((object)dtValue.ToString().Trim() != (object)string.Empty) {
                        return Convert.ToDateTime(dtValue);
                    }
                }
            }
            return DateTime.MinValue;
        }

        public static double ConvertDouble(object dbValue) {
            if (dbValue == DBNull.Value | dbValue == null | (object)dbValue.ToString().Trim() == (object)string.Empty | (dbValue.ToString() == "")) {
                return 0;
            }
            return Convert.ToDouble(dbValue);
        }

        public static Decimal ConvertDecimal(object dbValue) {
            if (dbValue == DBNull.Value | dbValue == null | (object)dbValue.ToString().Trim() == (object)string.Empty | (dbValue.ToString() == "")) {
                return 0;
            }
            return Convert.ToDecimal(dbValue);
        }

        public static short ConvertInt16(object intValue) {
            if (intValue != DBNull.Value) {
                if (intValue != null) {
                    if ((object)intValue.ToString().Trim() != (object)string.Empty) {
                        return Convert.ToInt16(intValue);
                    }
                }
            }
            return 0;
        }

        public static int ConvertInt32(object intValue) {
            if (intValue != DBNull.Value) {
                if (intValue != null) {
                    if ((object)intValue.ToString().Trim() != (object)string.Empty) {
                        return Convert.ToInt32(intValue);
                    }
                }
            }
            return 0;
        }

        public static string ConvertString(object strValue) {
            if (strValue != DBNull.Value) {
                if (strValue != null) {
                    if ((object)strValue.ToString().Trim() != (object)string.Empty) {
                        return strValue.ToString();
                    }
                }
            }
            return "";
        }
    }
}
