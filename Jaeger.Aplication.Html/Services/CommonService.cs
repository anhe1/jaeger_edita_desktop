﻿using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jaeger.Aplication.Html.Services {
    public static class CommonService {
        public static string ImagenQR(string emisorRFC, string receptorRFC, decimal total, string uuid, string selloSAT, string htmlContent) {
            byte[] qr = QRCodeExtensions.CopyImageToByteArray(
                            QRCodeExtensions.GetCodigoQR(
                                "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?",
                                emisorRFC, receptorRFC, total.ToString(), uuid, selloSAT));
            return Convert.ToBase64String(qr);
        }

        public static Dictionary<string, string> MesesCatalogo() {
            IMesesCatalogo mesesCatalogo = new MesesCatalogo();
            mesesCatalogo.Load();
            return mesesCatalogo.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor); ;
        }

        public static Dictionary<string, string> PeriodicidadCatalogo() {
            IPeriodicidadCatalogo periodicidadCatalogo = new PeriodicidadCatalogo();
            periodicidadCatalogo.Load();
            return periodicidadCatalogo.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor); ;
        }

        public static string TotalEnLetra(decimal total) {
            string totalEnLetra = NumeroALetras.Convertir(Double.Parse(total.ToString()), 1);
            return totalEnLetra;
        }

        public static Dictionary<string, string> RegimenesFiscales() {
            var d = new Jaeger.Catalogos.RegimenesFiscalesCatalogo();
            var d1 = new Dictionary<string, string>();
            d.Load();
            foreach (var item in d.Items) {
                d1.Add(item.Clave, item.Descriptor);
            }
            return d1;
        }

        public static Dictionary<string, string> FormaPago() {
            var d = new Catalogos.Repositories.FormaPagoCatalogo();
            var d1 = new Dictionary<string, string>();
            d.Load();
            return d.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor);
        }

        public static Dictionary<string, string> UsoCFDI() {
            var d = new Catalogos.Repositories.UsoCFDICatalogo();
            var d1 = new Dictionary<string, string>();
            d.Load();
            return d.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor);
        }

        public static Dictionary<string, string> Moneda() {
            var d = new Catalogos.Repositories.MonedaCatalogo();
            var d1 = new Dictionary<string, string>();
            d.Load();
            return d.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor);
        }

        public static Dictionary<string, string> ConfigAutotransporteCatalogo() {
            var d = new Catalogos.Repositories.ConfigAutotransporteCatalogo();
            var d1 = new Dictionary<string, string>();
            d.Load();
            return d.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor);
        }

        public static Dictionary<string, string> FiguraTransporte() {
            var figuraTransporteCatalogo = new CveFiguraTransporteCatalogo();
            figuraTransporteCatalogo.Load();

            return figuraTransporteCatalogo.Items.ToDictionary(keySelector: it => it.Clave, elementSelector: it => it.Descriptor);
        }
    }
}
