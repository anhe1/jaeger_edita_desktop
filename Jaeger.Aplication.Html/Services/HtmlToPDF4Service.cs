﻿using System.IO;
using System.Linq;
using System;
using HtmlAgilityPack;
using Jaeger.Aplication.Html.Abstractions;
using Jaeger.SAT.CFDI.V40;
using Jaeger.Aplication.Html.Contracts;

namespace Jaeger.Aplication.Html.Services {
    public partial class HtmlToPDFService : HtmlToPDF, IHtmlToPDFService {
        public string Procesar(Comprobante cfdi, string keyName) {
            var doc = new HtmlDocument();
            // cargamos el templete 
            string htmlContent = FileResource.GetResources("Resources." + this.templeteDefault);

            // debemos comprobar la ruta del templete, en el caso de que fuera asignado
            if (File.Exists(this.PathTemplete))
                htmlContent = FileExtensions.ReadFileText(new FileInfo(this.PathTemplete));

            if (cfdi.Complemento == null) {
                cfdi.Complemento = new ComprobanteComplemento();
                cfdi.Complemento.TimbreFiscalDigital = new SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital() { UUID = "00000000-0000-0000-0000-000000000000", FechaTimbrado = DateTime.Now, NoCertificadoSAT = "0000000000000", RfcProvCertif = "XXXXXXXXXX", SelloCFD = "XXXXXXXXXXXXXXXXXXXXXXXXXX=", SelloSAT = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=" };
            }

            if (cfdi.Complemento.TimbreFiscalDigital == null) {
                cfdi.Complemento.TimbreFiscalDigital = new SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital() { UUID = "00000000-0000-0000-0000-000000000000", FechaTimbrado = DateTime.Now, NoCertificadoSAT = "0000000000000", RfcProvCertif = "XXXXXXXXXX", SelloCFD = "XXXXXXXXXXXXXXXXXXXXXXXXXX=", SelloSAT = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=" };
            }

            htmlContent = this.LogoTipo(htmlContent);
            htmlContent = this.General(cfdi, htmlContent);
            htmlContent = this.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital, cfdi.CadenaOriginalOff, htmlContent);
            htmlContent = this.QR(cfdi.Emisor.Rfc, cfdi.Receptor.Rfc, cfdi.Total, cfdi.Complemento.TimbreFiscalDigital.UUID, cfdi.Complemento.TimbreFiscalDigital.SelloSAT, htmlContent);
            htmlContent = this.CFDIRelacionados(cfdi, htmlContent);
            htmlContent = this.Impuestos(cfdi.Impuestos, htmlContent);
            doc.LoadHtml(htmlContent);

            // conceptos del comprobante
            HtmlNode htmlNodeconceptos = doc.GetElementbyId("conceptos");
            this.Conceptos(cfdi, ref htmlNodeconceptos);

            // bloque informacion general en el caso de ser una factura de publico en general
            if (htmlContent.Contains("bloque-global")) {
                HtmlNode _nodeInformacionGlobal = doc.GetElementbyId("bloque-global");
                if (cfdi.InformacionGlobal != null) {
                    this.InformacionGlobal(cfdi.InformacionGlobal, doc);
                } else {
                    _nodeInformacionGlobal.Remove();
                }
            }

            // bloque de complementos pagos
            if (htmlContent.Contains("bloque-pagos10")) {
                HtmlNode nodeComplementoPagos10 = doc.GetElementbyId("bloque-pagos10");
                if (!(cfdi.Complemento.Pagos == null)) {
                    doc = this.ComplementoPagos20(cfdi.Complemento.Pagos, doc);
                } else {
                    nodeComplementoPagos10.Remove();
                    HtmlNode nodeCfdiRelacionado = doc.GetElementbyId("bloque-pagos10documentos");
                    nodeCfdiRelacionado.Remove();
                }
            }

            if (htmlContent.Contains("bloque-carta")) {
                HtmlNode nodecarta = doc.GetElementbyId("bloque-carta");
                if (!(cfdi.Complemento.CartaPorte == null)) {
                    doc = this.CartaPorte(cfdi.Complemento.CartaPorte, doc);
                } else {
                    nodecarta.Remove();


                }
            }

            if (cfdi.Complemento.Nomina != null) {
                this.Nomina12(cfdi, ref doc);
            }

            this.Finalizar(cfdi, ref doc);

            // convertir el archivo html temporal en formato PDF
            string archivotemporal = Path.Combine(this.PathTemporal, cfdi.Complemento.TimbreFiscalDigital.UUID + ".html");
            doc.Save(archivotemporal, System.Text.Encoding.UTF8);
            if (this.CrearPDF(archivotemporal, keyName))
                return this.PDF;
            return null;
        }

        private string General(Comprobante cfdi, string htmlContent) {

            // reemplazar datos generales
            htmlContent = htmlContent.Replace("{{EMISOR_RAZONSOCIAL}}", cfdi.Emisor.Nombre);
            htmlContent = htmlContent.Replace("{{EMISOR_RFC}}", cfdi.Emisor.Rfc);
            htmlContent = htmlContent.Replace("{{EMISOR_REGIMEN}}", this.catalogoRegimenFiscal.Search(cfdi.Emisor.RegimenFiscal).Descriptor);
            htmlContent = htmlContent.Replace("{{RECEPTOR_RAZONSOCIAL}}", cfdi.Receptor.Nombre);
            htmlContent = htmlContent.Replace("{{RECEPTOR_RFC}}", cfdi.Receptor.Rfc);
            htmlContent = htmlContent.Replace("{{RECEPTOR_DOMICILIOFISCAL}}", cfdi.Receptor.DomicilioFiscalReceptor);
            htmlContent = htmlContent.Replace("{{RECEPTOR_REGIMENFISCAL}}", this.catalogoRegimenFiscal.Search(cfdi.Receptor.RegimenFiscalReceptor).Descriptor);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TIPODECOMPROBANTE}}", this.TipoComprobante(cfdi.TipoDeComprobante));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_SERIE}}", cfdi.Serie);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FOLIO}}", cfdi.Folio);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_NUMERODECERTIFICADO}}", cfdi.NoCertificado);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FECHA}}", cfdi.Fecha.ToString());
            htmlContent = htmlContent.Replace("{{COMPROBANTE_LUGAREXPEDICION}}", cfdi.LugarExpedicion);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_METODOPAGO}}", cfdi.MetodoPago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FORMADEPAGO}}", this.catalogoFormaPago.Search(cfdi.FormaPago).Descriptor);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CONDICIONES}}", cfdi.CondicionesDePago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_MONEDA}}", cfdi.Moneda);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CONFIRMACION}}", cfdi.Confirmacion);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_SUBTOTAL}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(cfdi.SubTotal)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_DESCUENTO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(cfdi.Descuento)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TOTAL}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(cfdi.Total)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{RECEPTOR_USO_CFDI}}", this.catalogoUsoCFDI.Search(cfdi.Receptor.UsoCFDI).Descriptor);

            if (cfdi.Exportacion == "02")
                htmlContent = htmlContent.Replace("{{COMPROBANTE_EXPORTACION}}", "02 - Definitiva");
            else if (cfdi.Exportacion == "03")
                htmlContent = htmlContent.Replace("{{COMPROBANTE_EXPORTACION}}", "03 - Temporal");
            else
                htmlContent = htmlContent.Replace("{{COMPROBANTE_EXPORTACION}}", "01 - No Aplica");

            htmlContent = this.TotalEnLetra(cfdi.Total, htmlContent);
            return htmlContent;
        }

        private string CFDIRelacionados(Comprobante cfdi, string htmlContent) {
            if (cfdi.CfdiRelacionados == null) {
                htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_TIPORELACION}}", "");
                htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", "");
            } else {
                if (cfdi.CfdiRelacionados != null) {
                    foreach (var item in cfdi.CfdiRelacionados) {
                        htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_TIPORELACION}}", item.TipoRelacion);

                        if (item.CfdiRelacionado == null)
                            htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", "");
                        else {
                            string lista = string.Join("<br/>", (from x in item.CfdiRelacionado select x.UUID).ToArray<string>());
                            htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", lista);
                        }
                    }
                }
            }
            return htmlContent;
        }

        private string Impuestos(ComprobanteImpuestos impuestos, string htmlContent) {
            decimal IvaTraslado = 0;
            decimal iepsTraslado = 0;
            decimal IvaRetencion = 0;
            decimal IsrRetencion = 0;
            decimal IepsRetencion = 0;

            if (!(impuestos == null)) {
                if (!(impuestos.Traslados == null)) {
                    foreach (var itemTraslados in impuestos.Traslados) {
                        if (itemTraslados.Impuesto == "002") {
                            IvaTraslado = IvaTraslado + itemTraslados.Importe;
                        } else if (itemTraslados.Impuesto == "003") {
                            iepsTraslado = iepsTraslado + itemTraslados.Importe;
                        }
                    }
                }

                if (!(impuestos.Retenciones == null)) {
                    foreach (ComprobanteImpuestosRetencion itemRetencion in impuestos.Retenciones) {
                        if (itemRetencion.Impuesto == "002") {
                            IvaRetencion = IvaRetencion + itemRetencion.Importe;
                        } else if (itemRetencion.Impuesto == "001") {
                            IsrRetencion = IsrRetencion + itemRetencion.Importe;
                        } else if (itemRetencion.Impuesto == "003") {
                            IepsRetencion = IepsRetencion + itemRetencion.Importe;
                        }
                    }
                }
            }

            //htmlContent = htmlContent.Replace("{{COMPROBANTE_IVA}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IvaTraslado)));
            //htmlContent = htmlContent.Replace("{{COMPROBANTE_ISR}}", this.FormatoMoneda(DbConvert.ConvertDouble(basicComprobante.Isr)));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVARETENIDO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IvaRetencion)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_ISRRETENCION}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IsrRetencion)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IEPSRETENCION}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IepsRetencion)).Replace("$", ""));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVATRASLADO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IvaTraslado)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IEPSTRASLADO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(iepsTraslado)).Replace("$", ""));


            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            HtmlNode bloqueIEPST = doc.GetElementbyId("bloque-iepst");
            if (iepsTraslado <= 0) {
                if(bloqueIEPST != null) bloqueIEPST.Remove();
            }

            HtmlNode bloqueIVAR = doc.GetElementbyId("bloque-ivar");
            if (IvaRetencion <= 0)
                if(bloqueIVAR != null) bloqueIVAR.Remove();

            HtmlNode bloqueIEPSR = doc.GetElementbyId("bloque-ipesr");
            if (IepsRetencion <= 0)
                if (bloqueIEPSR != null) bloqueIEPSR.Remove();

            HtmlNode bloqueISRR = doc.GetElementbyId("bloque-isrr");
            if (IsrRetencion <= 0)
                if (bloqueISRR != null) bloqueISRR.Remove();

            return doc.DocumentNode.OuterHtml;
        }

        /// <summary>
        /// reemplazar informacion de los conceptos del comprobante
        /// </summary>
        private void Conceptos(Comprobante cfdi, ref HtmlNode htmlNode) {
            foreach (var item in cfdi.Conceptos) {
                HtmlNode temporal = htmlNode.Clone();
                temporal.InnerHtml = this.Concepto(item, temporal.InnerHtml);
                htmlNode.ParentNode.ChildNodes.Add(temporal);
                temporal = null;
            }
            // removemos el elemento con Id Conceptos, este solo es la base del html
            htmlNode.Remove();
        }

        /// <summary>
        /// reemplazar valores del concepto
        /// </summary>
        private string Concepto(ComprobanteConcepto concepto, string stringHtml) {
            stringHtml = stringHtml.Replace("{{CONCEPTO_CANTIDAD}}", ConvertExtensions.ConvertString(concepto.Cantidad));
            stringHtml = stringHtml.Replace("{{CONCEPTO_UNIDAD}}", concepto.Unidad);
            stringHtml = stringHtml.Replace("{{CONCEPTO_CLVUNIDAD}}", concepto.ClaveUnidad);
            stringHtml = stringHtml.Replace("{{CONCEPTO_CLVPRODSRV}}", concepto.ClaveProdServ);
            stringHtml = stringHtml.Replace("{{CONCEPTO_DESCRIPCION}}", concepto.Descripcion);
            stringHtml = stringHtml.Replace("{{CONCEPTO_OBJETOIMPUESTO}}", concepto.ObjetoImp);
            stringHtml = stringHtml.Replace("{{CONCEPTO_VALORUNITARIO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(concepto.ValorUnitario)).Replace("$", ""));
            stringHtml = stringHtml.Replace("{{CONCEPTO_DESCUENTO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(concepto.Descuento)).Replace("$", ""));
            stringHtml = stringHtml.Replace("{{CONCEPTO_IMPORTE}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(concepto.Importe)).Replace("$", ""));
            // en caso de que tenga cuenta predial
            if (concepto.CuentaPredial != null) {
                var _numero = concepto.CuentaPredial.Select(it => it.Numero).ToList();
                if (_numero != null) {
                    stringHtml = stringHtml.Replace("{{CONCEPTO_CTAPREDIAL}}", "CUENTA PREDIAL: " + string.Join(" ", _numero.ToArray()));
                }
            }
            stringHtml = stringHtml.Replace("{{CONCEPTO_CTAPREDIAL}}", "");
            return stringHtml;
        }

        protected virtual void Finalizar(Comprobante cfdi, ref HtmlDocument doc) {
            // confirmacion
            HtmlNode htmlNodeConfirmacion = doc.GetElementbyId("confirmacion");
            if (htmlNodeConfirmacion != null) {
                if (cfdi.Confirmacion == null) {
                    htmlNodeConfirmacion.Remove();
                }
            }

            if (cfdi.CfdiRelacionados == null) {
                HtmlNode htmlNodeTipoRelacion = doc.GetElementbyId("tiporelacion");
                if (htmlNodeTipoRelacion != null) {
                    htmlNodeTipoRelacion.Remove();
                    HtmlNode htmlNodeCfdiRelacionado = doc.GetElementbyId("cfdirelacionado");
                    if (htmlNodeCfdiRelacionado != null)
                        htmlNodeCfdiRelacionado.Remove();
                }
            }

            // metodo de pago
            HtmlNode htmlNodemetodopago = doc.GetElementbyId("metodopago");
            if (cfdi.MetodoPago == null) {
                htmlNodemetodopago.Remove();
            }

            // condiciones de pago
            HtmlNode htmlNodeconceptos1 = doc.GetElementbyId("condiciones");
            if (htmlNodeconceptos1 != null) {
                if (cfdi.CondicionesDePago == null) {
                    htmlNodeconceptos1.Remove();
                }
            }

            // forma de pago
            HtmlNode htmlNodeFormaPago = doc.GetElementbyId("formapago");
            if (htmlNodeFormaPago != null) {
                if (cfdi.FormaPago == null) {
                    htmlNodeFormaPago.Remove();
                }
            }

            // tipo de cambio
            HtmlNode htmlNodeTipoCambio = doc.GetElementbyId("tipocambio");
            if (htmlNodeTipoCambio != null) {
                if (cfdi.TipoCambioSpecified == false) {
                    htmlNodeTipoCambio.Remove();
                }
            }
        }

        private void Nomina12(Comprobante cfdi, ref HtmlDocument doc) {
            string htmlContent = doc.DocumentNode.OuterHtml;
            htmlContent = htmlContent.Replace("{{NOMINA_EMISOR_REGISTROPATRONAL}}", cfdi.Complemento.Nomina.Emisor.RegistroPatronal);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_NUMEMPLEADO}}", cfdi.Complemento.Nomina.Receptor.NumEmpleado);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_CURP}}", cfdi.Complemento.Nomina.Receptor.Curp);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_NUMSEGURIDADSOCIAL}}", cfdi.Complemento.Nomina.Receptor.NumSeguridadSocial);

            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_DEPARTAMENTO}}", cfdi.Complemento.Nomina.Receptor.Departamento);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_PUESTO}}", cfdi.Complemento.Nomina.Receptor.Puesto);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_TIPOREGIMEN}}", cfdi.Complemento.Nomina.Receptor.TipoRegimen);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_CLAVEENTFED}}", cfdi.Complemento.Nomina.Receptor.ClaveEntFed);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_TIPOJORNADA}}", cfdi.Complemento.Nomina.Receptor.TipoJornada);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_DESCUENTO}}", cfdi.Descuento.ToString());

            htmlContent = htmlContent.Replace("{{NOMINA_NUMDIASPAGADOS}}", cfdi.Complemento.Nomina.NumDiasPagados.ToString());
            htmlContent = htmlContent.Replace("{{NOMINA_FECHAPAGO}}", cfdi.Complemento.Nomina.FechaPago.ToShortDateString());
            htmlContent = htmlContent.Replace("{{NOMINA_FECHAINICIALPAGO}}", cfdi.Complemento.Nomina.FechaInicialPago.ToShortDateString());
            htmlContent = htmlContent.Replace("{{NOMINA_FECHAFINALPAGO}}", cfdi.Complemento.Nomina.FechaFinalPago.ToShortDateString());

            htmlContent = htmlContent.Replace("{{NOMINA_PERCEPCIONES_TOTALGRAVADO}}", cfdi.Complemento.Nomina.Percepciones.TotalGravado.ToString());
            htmlContent = htmlContent.Replace("{{NOMINA_PERCEPCIONES_TOTALEXENTO}}", cfdi.Complemento.Nomina.Percepciones.TotalExento.ToString());

            htmlContent = htmlContent.Replace("{{NOMINA_TOTALDEDUCCIONES}}", cfdi.Complemento.Nomina.Deducciones.TotalOtrasDeducciones.ToString());

            doc.LoadHtml(htmlContent);
            this.Nomina12Percepciones(cfdi.Complemento.Nomina, ref doc);
            this.Nomina12Deducciones(cfdi.Complemento.Nomina, ref doc);
            this.Nomina12Incapacidades(cfdi.Complemento.Nomina, ref doc);
            this.Nomina12OtrosPagos(cfdi.Complemento.Nomina, ref doc);
        }

        /// <summary>
        /// reemplazar el contenido del complemento de pagos10
        /// </summary>
        private HtmlDocument ComplementoPagos20(SAT.CFDI.Complemento.Pagos.V20.Pagos pagos, HtmlDocument bloqueComplemento) {
            HtmlNode nodeComplementoPagos10 = bloqueComplemento.GetElementbyId("bloque-pagos10");
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_FECHA}}", pagos.Pago[0].FechaPago.ToShortDateString());
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_EMISORCTAORDENANTE}}", pagos.Pago[0].RfcEmisorCtaOrd);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_CTAORDENANTE}}", pagos.Pago[0].CtaOrdenante);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_EMISORBENEFICIARIO}}", pagos.Pago[0].RfcEmisorCtaBen);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_CTABENEFICIARIO}}", pagos.Pago[0].CtaBeneficiario);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_FORMAPAGO}}", this.catalogoFormaPago.Search(pagos.Pago[0].FormaDePagoP).Descriptor);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_MONEDAP}}", pagos.Pago[0].MonedaP);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_TIPODECAMBIOP}}", pagos.Pago[0].TipoCambioP.ToString());
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_MONTOP}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(pagos.Pago[0].Monto)).Replace("$", ""));
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_NUMOPERACIONP}}", pagos.Pago[0].NumOperacion);

            if (!(pagos == null)) {
                HtmlNode nodeCfdiRelacionado = bloqueComplemento.GetElementbyId("pagos10_cfdirelacionado");
                foreach (var ddr in pagos.Pago[0].DoctoRelacionado) {
                    HtmlNode temporal = nodeCfdiRelacionado.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_IDDOCUMENTO}}", ddr.IdDocumento);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SERIE}}", ddr.Serie);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_FOLIO}}", ddr.Folio);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_MONEDA}}", ddr.MonedaDR);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_TIPODECAMBIOPP}}", ddr.EquivalenciaDR.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_PARCIALIDAD}}", ddr.NumParcialidad.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SALDOANTERIOR}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(ddr.ImpSaldoAnt)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_PAGADO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(ddr.ImpPagado)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SALDOINSOLUTO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(ddr.ImpSaldoInsoluto)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_METODODEPAGO}}", "");
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_OBJETOIMPUESTO}}", ddr.ObjetoImpDR);
                    nodeCfdiRelacionado.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
                // removemos el ultimo 
                nodeCfdiRelacionado.Remove();
            }
            return bloqueComplemento;
        }

        private HtmlDocument InformacionGlobal(SAT.CFDI.V40.ComprobanteInformacionGlobal global, HtmlDocument bloqueInformacionGlobal) {
            HtmlNode _nodeInformacionGlobal = bloqueInformacionGlobal.GetElementbyId("bloque-global");
            if (global != null) {
                _nodeInformacionGlobal.InnerHtml = _nodeInformacionGlobal.InnerHtml.Replace("{{GLOBAL_PERIODICIDAD}}", this.periodicidadCatalogo.Search(global.Periodicidad).Descriptor);
                _nodeInformacionGlobal.InnerHtml = _nodeInformacionGlobal.InnerHtml.Replace("{{GLOBAL_MESES}}", this.mesesCatalogo.Search(global.Meses).Descriptor);
                _nodeInformacionGlobal.InnerHtml = _nodeInformacionGlobal.InnerHtml.Replace("{{GLOBAL_ANIO}}", global.Año.ToString());
            } else {
                _nodeInformacionGlobal.Remove();
            }
            return bloqueInformacionGlobal;
        }

        private HtmlDocument CartaPorte(SAT.CFDI.Complemento.CartaPorte.V20.CartaPorte cartaPorte, HtmlDocument documentCartaPorte) {
            var contador = 1;
            var bloque = documentCartaPorte.GetElementbyId("bloque-ubicacion");
            foreach (var item in cartaPorte.Ubicaciones) {
                var clone = bloque.Clone();
                clone.InnerHtml = clone.InnerHtml.Replace("{{partidas}}", contador.ToString());
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_TIPOUBICACION}}", item.TipoUbicacion);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_IDUBICACION}}", item.IDUbicacion);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_RFCREMITENTEDESTINATARIO}}", item.RFCRemitenteDestinatario);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_NOMBREREMITENTEDESTINATARIO}}", item.NombreRemitenteDestinatario);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_FECHAHORASALIDALLEGADA}}", item.FechaHoraSalidaLlegada.ToString());
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_DISTANCIA}}", item.DistanciaRecorrida.ToString());
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_UBICACIONDOMICILIO}}", item.Domicilio.ToString());
                bloque.ParentNode.ChildNodes.Add(clone);
                contador += 1;
            }
            contador = 1;
            bloque.Remove();
            bloque = documentCartaPorte.GetElementbyId("bloque-figura");
            foreach (var item in cartaPorte.FiguraTransporte) {
                var clone = bloque.Clone();
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_PARTIDAS}}", contador.ToString());
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_TIPOFIGURA}}", item.TipoFigura);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_RFCFIGURA}}", item.RFCFigura);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_NOMBREFIGURA}}", item.NombreFigura);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_NUMLICENCIA}}", item.NumLicencia);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_DOMICILIO}}", item.Domicilio.ToString());

                if (item.PartesTransporte != null) {

                } else {

                }

                bloque.ParentNode.ChildNodes.Add(clone);
                contador = contador + 1;
            }
            contador = 1;
            bloque.Remove();
            bloque = documentCartaPorte.GetElementbyId("bloque-mercancias");
            bloque.InnerHtml = bloque.InnerHtml.Replace("{{CARTAP_PESOBRUTOTOTAL}}", cartaPorte.Mercancias.PesoBrutoTotal.ToString("N2"));
            bloque.InnerHtml = bloque.InnerHtml.Replace("{{CARTAP_UNIDADPESO}}", cartaPorte.Mercancias.UnidadPeso);
            bloque.InnerHtml = bloque.InnerHtml.Replace("{{CARTAP_PESONETOTOTAL}}", cartaPorte.Mercancias.PesoNetoTotalSpecified ? cartaPorte.Mercancias.PesoNetoTotal.ToString("N2") : "");
            bloque.InnerHtml = bloque.InnerHtml.Replace("{{CARTAP_NUMTOTALMERCANCIAS}}", cartaPorte.Mercancias.NumTotalMercancias.ToString());

            bloque = documentCartaPorte.GetElementbyId("bloque-mercancia");
            foreach (var item in cartaPorte.Mercancias.Mercancia) {
                var clone = bloque.Clone();
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_CANTIDAD}}", item.Cantidad.ToString("N2"));
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_UNIDAD}}", item.Unidad);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_CLAVEUNIDAD}}", item.ClaveUnidad);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_BIENESTRANSP}}", item.BienesTransp);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_DESCRIPCION}}", item.Descripcion);
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_PESOENKG}}", item.PesoEnKg.ToString("N2"));
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_VALORMERCANCIA}}", item.ValorMercanciaSpecified ? item.ValorMercancia.ToString("N2") : "");
                clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_MONEDA}}", item.Moneda);
                bloque.ParentNode.ChildNodes.Add(clone);
            }
            bloque.Remove();

            bloque = documentCartaPorte.GetElementbyId("bloque-autotransporte");
            if (cartaPorte.Mercancias.Autotransporte != null) {
                bloque.InnerHtml = bloque.InnerHtml.Replace("{{CARTAP_PERMSCT}}", cartaPorte.Mercancias.Autotransporte.PermSCT);
                bloque.InnerHtml = bloque.InnerHtml.Replace("{{CARTAP_NUMPERMISOSCT}}", cartaPorte.Mercancias.Autotransporte.NumPermisoSCT);

                var bloque1 = bloque.OwnerDocument.GetElementbyId("bloque-identificacion");
                if (cartaPorte.Mercancias.Autotransporte.IdentificacionVehicular != null) {
                    bloque1.InnerHtml = bloque1.InnerHtml.Replace("{{CARTAP_CONFIGVEHICULAR}}", cartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.ConfigVehicular);
                    bloque1.InnerHtml = bloque1.InnerHtml.Replace("{{CARTAP_PLACAVM}}", cartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.PlacaVM);
                    bloque1.InnerHtml = bloque1.InnerHtml.Replace("{{CARTAP_ANIOMODELOVM}}", cartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM.ToString("N0"));
                } else {
                    bloque1.Remove();
                }

                bloque1 = bloque.OwnerDocument.GetElementbyId("bloque-seguros");
                if (cartaPorte.Mercancias.Autotransporte.Seguros != null) {
                    bloque1.InnerHtml = bloque1.InnerHtml.Replace("{{CARTAP_ASEGURARESPCIVIL}}", cartaPorte.Mercancias.Autotransporte.Seguros.AseguraRespCivil);
                    bloque1.InnerHtml = bloque1.InnerHtml.Replace("{{CARTAP_POLIZARESPCIVIL}}", cartaPorte.Mercancias.Autotransporte.Seguros.PolizaRespCivil);
                } else {
                    bloque1.Remove();
                }

                bloque1 = bloque.OwnerDocument.GetElementbyId("bloque-remolques");
                if (cartaPorte.Mercancias.Autotransporte.Remolques != null) {
                    if (cartaPorte.Mercancias.Autotransporte.Remolques.Count() > 0) {
                        foreach (var item1 in cartaPorte.Mercancias.Autotransporte.Remolques) {
                            var clone = bloque1.Clone();
                            clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_SUBTIPOREM}}", item1.SubTipoRem);
                            clone.InnerHtml = clone.InnerHtml.Replace("{{CARTAP_PLACA}}", item1.Placa);
                            bloque1.ParentNode.ChildNodes.Add(clone);
                        }
                        bloque1.Remove();
                    } else {
                        bloque1.Remove();
                    }
                } else {
                    bloque1.Remove();
                }
            } else {
                bloque.Remove();
            }

            return documentCartaPorte;
        }
    }
}
