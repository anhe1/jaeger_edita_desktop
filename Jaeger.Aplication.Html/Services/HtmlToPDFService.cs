﻿using System;
using System.Linq;
using System.IO;
using HtmlAgilityPack;
using Jaeger.SAT.CFDI.V33;
using Jaeger.Catalogos;
using Jaeger.Catalogos.Repositories;
using Jaeger.Aplication.Html.Contracts;
using Jaeger.Aplication.Html.Abstractions;

namespace Jaeger.Aplication.Html.Services {
    /// <summary>
    /// clase para la conversion de templetes HTML a PDF
    /// </summary>
    public partial class HtmlToPDFService : HtmlToPDF, IHtmlToPDFService {
        public HtmlToPDFService() {
            this.catalogoRegimenFiscal = new RegimenesFiscalesCatalogo();
            this.catalogoUsoCFDI = new UsoCFDICatalogo();
            this.catalogoFormaPago = new FormaPagoCatalogo();
            this.periodicidadCatalogo = new PeriodicidadCatalogo();
            this.mesesCatalogo = new MesesCatalogo();
            this.PathTemporal = @"C:\Jaeger\Jaeger.Temporal";
            this.PathDefault = this.PathTemporal;
            this.catalogoUsoCFDI.Load();
            this.catalogoRegimenFiscal.Load();
            this.catalogoFormaPago.Load();
            this.mesesCatalogo.Load();
            this.periodicidadCatalogo.Load();
        }

        public string Procesar(object cfdi, string keyName) {
            if (cfdi.GetType() == typeof(SAT.CFDI.V33.Comprobante)) {
                return this.Procesar((SAT.CFDI.V33.Comprobante)cfdi, keyName);
            } else if (cfdi.GetType() == typeof(SAT.CFDI.V40.Comprobante)) {
                return this.Procesar((SAT.CFDI.V40.Comprobante)cfdi, keyName);
            }
            return string.Empty;
        }

        /// <summary>
        /// crear representacion impresa xml a PDF
        /// </summary>
        /// <param name="cfdi">objeto string XML valido</param>
        /// <param name="keyName">nombre del archivo</param>
        /// <returns>ruta del archivo creado (PDF)</returns>
        public string Procesar(string cfdi, string keyName) {
            var tcfdi = Comprobante.LoadXml(cfdi);
            if (tcfdi != null)
                return this.Procesar(tcfdi, keyName);
            return "";
        }

        /// <summary>
        /// crear representacion impresa xml a PDF
        /// </summary>
        /// <param name="cfdi">objeto Comprobante</param>
        /// <param name="keyName">nombre del archivo</param>
        /// <returns>ruta del archivo creado (PDF)</returns>
        public string Procesar(Comprobante cfdi, string keyName) {
            
            var doc = new HtmlDocument();
            // cargamos el templete 
            string htmlContent = FileResource.GetResources("Resources." + this.templeteDefault);

            // debemos comprobar la ruta del templete, en el caso de que fuera asignado
            if (File.Exists(this.PathTemplete))
                htmlContent = FileExtensions.ReadFileText(new FileInfo(this.PathTemplete));

            htmlContent = this.LogoTipo(htmlContent);
            htmlContent = this.General(cfdi, htmlContent);
            htmlContent = this.TimbreFiscal(cfdi.Complemento.TimbreFiscalDigital, cfdi.CadenaOriginalOff, htmlContent);
            htmlContent = this.QR(cfdi.Emisor.Rfc, cfdi.Receptor.Rfc, cfdi.Total, cfdi.Complemento.TimbreFiscalDigital.UUID, cfdi.Complemento.TimbreFiscalDigital.SelloSAT, htmlContent);
            htmlContent = this.CFDIRelacionados(cfdi, htmlContent);
            htmlContent = this.Impuestos(cfdi.Impuestos, htmlContent);
            doc.LoadHtml(htmlContent);

            // conceptos del comprobante
            HtmlNode htmlNodeconceptos = doc.GetElementbyId("conceptos");
            this.Conceptos(cfdi, ref htmlNodeconceptos);

            // bloque de complementos pagos
            if (htmlContent.Contains("bloque-pagos10")) {
                HtmlNode nodeComplementoPagos10 = doc.GetElementbyId("bloque-pagos10");
                if (!(cfdi.Complemento.Pagos == null)) {
                    doc = this.ComplementoPagos10(cfdi.Complemento.Pagos, doc);
                } else {
                    nodeComplementoPagos10.Remove();
                    HtmlNode nodeCfdiRelacionado = doc.GetElementbyId("bloque-pagos10documentos");
                    nodeCfdiRelacionado.Remove();
                }
            }

            if (cfdi.Complemento.Nomina12 != null) {
                this.Nomina12(cfdi, ref doc);
            }

            this.Finalizar(cfdi, ref doc);

            // convertir el archivo html temporal en formato PDF
            string archivotemporal = Path.Combine(this.PathTemporal, cfdi.Complemento.TimbreFiscalDigital.UUID + ".html");
            doc.Save(archivotemporal, System.Text.Encoding.UTF8);
            if (this.CrearPDF(archivotemporal, keyName))
                return this.PDF;
            return null;
        }

        /// <summary>
        /// reemplazar datos generales de la plantilla html
        /// </summary>
        private string General(Comprobante cfdi, string htmlContent) {
            // descripcion del regimen fiscal del emisor
            string regimenDescripcion = cfdi.Emisor.RegimenFiscal;
            try {
                var regimen = this.catalogoRegimenFiscal.Search(regimenDescripcion);
                if (regimen != null) {
                    regimenDescripcion = string.Format("{0} - {1}", regimen.Clave, regimen.Descripcion);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            // reemplazar datos generales
            htmlContent = htmlContent.Replace("{{EMISOR_RAZONSOCIAL}}", cfdi.Emisor.Nombre);
            htmlContent = htmlContent.Replace("{{EMISOR_RFC}}", cfdi.Emisor.Rfc);
            htmlContent = htmlContent.Replace("{{EMISOR_REGIMEN}}", regimenDescripcion);
            htmlContent = htmlContent.Replace("{{RECEPTOR_RAZONSOCIAL}}", cfdi.Receptor.Nombre);
            htmlContent = htmlContent.Replace("{{RECEPTOR_RFC}}", cfdi.Receptor.Rfc);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TIPODECOMPROBANTE}}", cfdi.TipoDeComprobante);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_SERIE}}", cfdi.Serie);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FOLIO}}", cfdi.Folio);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_NUMERODECERTIFICADO}}", cfdi.NoCertificado);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FECHA}}", cfdi.Fecha.ToString());
            htmlContent = htmlContent.Replace("{{COMPROBANTE_LUGAREXPEDICION}}", cfdi.LugarExpedicion);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_METODOPAGO}}", cfdi.MetodoPago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_FORMADEPAGO}}", cfdi.FormaPago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CONDICIONES}}", cfdi.CondicionesDePago);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_MONEDA}}", cfdi.Moneda);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CONFIRMACION}}", cfdi.Confirmacion);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_SUBTOTAL}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(cfdi.SubTotal)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_TOTAL}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(cfdi.Total)).Replace("$", ""));
            string uso = this.catalogoUsoCFDI.Search(cfdi.Receptor.UsoCFDI).Descripcion;
            htmlContent = htmlContent.Replace("{{RECEPTOR_USO_CFDI}}", string.Format("{0} {1}", cfdi.Receptor.UsoCFDI, uso));
            htmlContent = this.TotalEnLetra(cfdi.Total, htmlContent);
            return htmlContent;
        }

        private string Impuestos(ComprobanteImpuestos impuestos, string htmlContent) {
            decimal IvaTraslado = 0;
            decimal iepsTraslado = 0;
            decimal IvaRetencion = 0;
            decimal IsrRetencion = 0;
            decimal IepsRetencion = 0;

            if (!(impuestos == null)) {
                if (!(impuestos.Traslados == null)) {
                    foreach (var itemTraslados in impuestos.Traslados) {
                        if (itemTraslados.Impuesto == "002") {
                            IvaTraslado = IvaTraslado + itemTraslados.Importe;
                        } else if (itemTraslados.Impuesto == "003") {
                            iepsTraslado = iepsTraslado + itemTraslados.Importe;
                        }
                    }
                }

                if (!(impuestos.Retenciones == null)) {
                    foreach (ComprobanteImpuestosRetencion itemRetencion in impuestos.Retenciones) {
                        if (itemRetencion.Impuesto == "002") {
                            IvaRetencion = IvaRetencion + itemRetencion.Importe;
                        } else if (itemRetencion.Impuesto == "001") {
                            IsrRetencion = IsrRetencion + itemRetencion.Importe;
                        } else if (itemRetencion.Impuesto == "003") {
                            IepsRetencion = IepsRetencion + itemRetencion.Importe;
                        }
                    }
                }
            }

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVA}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IvaTraslado)));
            //htmlContent = htmlContent.Replace("{{COMPROBANTE_ISR}}", this.FormatoMoneda(DbConvert.ConvertDouble(basicComprobante.Isr)));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVARETENCION}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IvaRetencion)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_ISRRETENCION}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IsrRetencion)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IEPSRETENCION}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IepsRetencion)).Replace("$", ""));

            htmlContent = htmlContent.Replace("{{COMPROBANTE_IVATRASLADO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(IvaTraslado)).Replace("$", ""));
            htmlContent = htmlContent.Replace("{{COMPROBANTE_IEPSTRASLADO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(iepsTraslado)).Replace("$", ""));

            return htmlContent;
        }

        /// <summary>
        /// comprobantes relacionados
        /// </summary>
        private string CFDIRelacionados(Comprobante cfdi, string htmlContent) {
            if (cfdi.CfdiRelacionados == null) {
                htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_TIPORELACION}}", "");
                htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", "");
            } else {
                if (cfdi.CfdiRelacionados != null) {
                    htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_TIPORELACION}}", cfdi.CfdiRelacionados.TipoRelacion);
                    if (cfdi.CfdiRelacionados.CfdiRelacionado == null)
                        htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", "");
                    else {
                        string lista = string.Join("<br/>", (from x in cfdi.CfdiRelacionados.CfdiRelacionado select x.UUID).ToArray<string>());
                        htmlContent = htmlContent.Replace("{{CFDIRELACIONADOS_UIDSLIST}}", lista);
                    }
                }
            }
            return htmlContent;
        }

        /// <summary>
        /// reemplazar el contenido del complemento de pagos10
        /// </summary>
        private HtmlDocument ComplementoPagos10(SAT.CFDI.Complemento.Pagos.V10.Pagos pagos, HtmlDocument bloqueComplemento) {
            HtmlNode nodeComplementoPagos10 = bloqueComplemento.GetElementbyId("bloque-pagos10");
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_FECHA}}", pagos.Pago[0].FechaPago.ToShortDateString());
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_EMISORCTAORDENANTE}}", pagos.Pago[0].RfcEmisorCtaOrd);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_CTAORDENANTE}}", pagos.Pago[0].CtaOrdenante);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_EMISORBENEFICIARIO}}", pagos.Pago[0].RfcEmisorCtaBen);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_CTABENEFICIARIO}}", pagos.Pago[0].CtaBeneficiario);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_FORMAPAGO}}", pagos.Pago[0].FormaDePagoP);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_MONEDAP}}", pagos.Pago[0].MonedaP);
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_TIPODECAMBIOP}}", pagos.Pago[0].TipoCambioP.ToString());
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_MONTOP}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(pagos.Pago[0].Monto)).Replace("$", ""));
            nodeComplementoPagos10.InnerHtml = nodeComplementoPagos10.InnerHtml.Replace("{{PAGOS10_NUMOPERACIONP}}", pagos.Pago[0].NumOperacion);

            if (!(pagos == null)) {
                HtmlNode nodeCfdiRelacionado = bloqueComplemento.GetElementbyId("pagos10_cfdirelacionado");
                foreach (var ddr in pagos.Pago[0].DoctoRelacionado) {
                    HtmlNode temporal = nodeCfdiRelacionado.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_IDDOCUMENTO}}", ddr.IdDocumento);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SERIE}}", ddr.Serie);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_FOLIO}}", ddr.Folio);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_MONEDA}}", ddr.MonedaDR);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_TIPODECAMBIOPP}}", ddr.TipoCambioDR.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_PARCIALIDAD}}", ddr.NumParcialidad.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SALDOANTERIOR}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(ddr.ImpSaldoAnt)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_PAGADO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(ddr.ImpPagado)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_SALDOINSOLUTO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(ddr.ImpSaldoInsoluto)).Replace("$", ""));
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{PAGOS10_METODODEPAGO}}", ddr.MetodoDePagoDR);
                    nodeCfdiRelacionado.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
                // removemos el ultimo 
                nodeCfdiRelacionado.Remove();
            }
            return bloqueComplemento;
        }

        /// <summary>
        /// reemplazar informacion de los conceptos del comprobante
        /// </summary>
        private void Conceptos(Comprobante cfdi, ref HtmlNode htmlNode) {
            foreach (var item in cfdi.Conceptos) {
                HtmlNode temporal = htmlNode.Clone();
                temporal.InnerHtml = this.Concepto(item, temporal.InnerHtml);
                htmlNode.ParentNode.ChildNodes.Add(temporal);
                temporal = null;
            }
            // removemos el elemento con Id Conceptos, este solo es la base del html
            htmlNode.Remove();
        }

        /// <summary>
        /// reemplazar valores del concepto
        /// </summary>
        private string Concepto(ComprobanteConcepto concepto, string stringHtml) {
            stringHtml = stringHtml.Replace("{{CONCEPTO_CANTIDAD}}", ConvertExtensions.ConvertString(concepto.Cantidad));
            stringHtml = stringHtml.Replace("{{CONCEPTO_UNIDAD}}", concepto.Unidad);
            stringHtml = stringHtml.Replace("{{CONCEPTO_CLVUNIDAD}}", concepto.ClaveUnidad);
            stringHtml = stringHtml.Replace("{{CONCEPTO_CLVPRODSRV}}", concepto.ClaveProdServ);
            stringHtml = stringHtml.Replace("{{CONCEPTO_DESCRIPCION}}", concepto.Descripcion);
            stringHtml = stringHtml.Replace("{{CONCEPTO_VALORUNITARIO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(concepto.ValorUnitario)).Replace("$", ""));
            stringHtml = stringHtml.Replace("{{CONCEPTO_DESCUENTO}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(concepto.Descuento)).Replace("$", ""));
            stringHtml = stringHtml.Replace("{{CONCEPTO_IMPORTE}}", this.FormatoMoneda(ConvertExtensions.ConvertDouble(concepto.Importe)).Replace("$", ""));
            // en caso de que tenga cuenta predial
            if (concepto.CuentaPredial != null) {
                if (concepto.CuentaPredial.Numero != "") {
                    stringHtml = stringHtml.Replace("{{CONCEPTO_CTAPREDIAL}}", "CUENTA PREDIAL: " + concepto.CuentaPredial.Numero);
                }
            }
            stringHtml = stringHtml.Replace("{{CONCEPTO_CTAPREDIAL}}", "");
            return stringHtml;
        }

        protected virtual void Finalizar(Comprobante cfdi, ref HtmlDocument doc) {
            // confirmacion
            HtmlNode htmlNodeConfirmacion = doc.GetElementbyId("confirmacion");
            if (htmlNodeConfirmacion != null) {
                if (cfdi.Confirmacion == null) {
                    htmlNodeConfirmacion.Remove();
                }
            }

            if (cfdi.CfdiRelacionados == null) {
                HtmlNode htmlNodeTipoRelacion = doc.GetElementbyId("tiporelacion");
                if (htmlNodeTipoRelacion != null) {
                    htmlNodeTipoRelacion.Remove();
                    HtmlNode htmlNodeCfdiRelacionado = doc.GetElementbyId("cfdirelacionado");
                    if (htmlNodeCfdiRelacionado != null)
                        htmlNodeCfdiRelacionado.Remove();
                }
            }

            // metodo de pago
            HtmlNode htmlNodemetodopago = doc.GetElementbyId("metodopago");
            if (cfdi.MetodoPago == null) {
                htmlNodemetodopago.Remove();
            }

            // condiciones de pago
            HtmlNode htmlNodeconceptos1 = doc.GetElementbyId("condiciones");
            if (htmlNodeconceptos1 != null) {
                if (cfdi.CondicionesDePago == null) {
                    htmlNodeconceptos1.Remove();
                }
            }

            // forma de pago
            HtmlNode htmlNodeFormaPago = doc.GetElementbyId("formapago");
            if (htmlNodeFormaPago != null) {
                if (cfdi.FormaPago == null) {
                    htmlNodeFormaPago.Remove();
                }
            }

            // tipo de cambio
            HtmlNode htmlNodeTipoCambio = doc.GetElementbyId("tipocambio");
            if (htmlNodeTipoCambio != null) {
                if (cfdi.TipoCambioSpecified == false) {
                    htmlNodeTipoCambio.Remove();
                }
            }
        }

        private void Nomina12(Comprobante cfdi, ref HtmlDocument doc) {
            string htmlContent = doc.DocumentNode.OuterHtml;
            htmlContent = htmlContent.Replace("{{NOMINA_EMISOR_REGISTROPATRONAL}}", cfdi.Complemento.Nomina12.Emisor.RegistroPatronal);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_NUMEMPLEADO}}", cfdi.Complemento.Nomina12.Receptor.NumEmpleado);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_CURP}}", cfdi.Complemento.Nomina12.Receptor.Curp);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_NUMSEGURIDADSOCIAL}}", cfdi.Complemento.Nomina12.Receptor.NumSeguridadSocial);

            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_DEPARTAMENTO}}", cfdi.Complemento.Nomina12.Receptor.Departamento);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_PUESTO}}", cfdi.Complemento.Nomina12.Receptor.Puesto);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_TIPOREGIMEN}}", cfdi.Complemento.Nomina12.Receptor.TipoRegimen);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_CLAVEENTFED}}", cfdi.Complemento.Nomina12.Receptor.ClaveEntFed);
            htmlContent = htmlContent.Replace("{{NOMINA_RECEPTOR_TIPOJORNADA}}", cfdi.Complemento.Nomina12.Receptor.TipoJornada);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_DESCUENTO}}", cfdi.Descuento.ToString());

            htmlContent = htmlContent.Replace("{{NOMINA_NUMDIASPAGADOS}}", cfdi.Complemento.Nomina12.NumDiasPagados.ToString());
            htmlContent = htmlContent.Replace("{{NOMINA_FECHAPAGO}}", cfdi.Complemento.Nomina12.FechaPago.ToShortDateString());
            htmlContent = htmlContent.Replace("{{NOMINA_FECHAINICIALPAGO}}", cfdi.Complemento.Nomina12.FechaInicialPago.ToShortDateString());
            htmlContent = htmlContent.Replace("{{NOMINA_FECHAFINALPAGO}}", cfdi.Complemento.Nomina12.FechaFinalPago.ToShortDateString());

            htmlContent = htmlContent.Replace("{{NOMINA_PERCEPCIONES_TOTALGRAVADO}}", cfdi.Complemento.Nomina12.Percepciones.TotalGravado.ToString());
            htmlContent = htmlContent.Replace("{{NOMINA_PERCEPCIONES_TOTALEXENTO}}", cfdi.Complemento.Nomina12.Percepciones.TotalExento.ToString());

            htmlContent = htmlContent.Replace("{{NOMINA_TOTALDEDUCCIONES}}", cfdi.Complemento.Nomina12.Deducciones.TotalOtrasDeducciones.ToString());

            doc.LoadHtml(htmlContent);
            this.Nomina12Percepciones(cfdi.Complemento.Nomina12, ref doc);
            this.Nomina12Deducciones(cfdi.Complemento.Nomina12, ref doc);
            this.Nomina12Incapacidades(cfdi.Complemento.Nomina12, ref doc);
            this.Nomina12OtrosPagos(cfdi.Complemento.Nomina12, ref doc);
        }
    }
}
