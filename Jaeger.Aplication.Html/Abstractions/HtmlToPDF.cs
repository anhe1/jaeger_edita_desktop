﻿using System.Globalization;
using System;
using System.IO;
using System.Drawing.Printing;
using System.Collections.Generic;
using HtmlAgilityPack;
using Pechkin.Synchronized;
using Pechkin;
using Jaeger.Aplication.Html.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.Aplication.Html.Abstractions {
    public abstract class HtmlToPDF {
        public readonly string templeteDefault = "factura40-generic.html";

        protected IRegimenesFiscalesCatalogo catalogoRegimenFiscal;
        protected IUsoCFDICatalogo catalogoUsoCFDI;
        protected IFormaPagoCatalogo catalogoFormaPago;
        protected IMesesCatalogo mesesCatalogo = new MesesCatalogo();
        protected IPeriodicidadCatalogo periodicidadCatalogo;

        /// <summary>
        /// obtener o establecer el logotipo que se incluira en la representacion impresa
        /// </summary>
        public string Logo {
            get; set;
        }

        /// <summary>
        /// obtener o establcer la ruta del templete a utilizar, en caso de no existir se utiliza el templete por default almacenado en los recursos
        /// </summary>
        public string PathTemplete {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ruta de archivos temporales
        /// </summary>
        public string PathTemporal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ruta donde se almacenara la representacion impresa PDF
        /// </summary>
        public string PathDefault {
            get; set;
        }

        public string PDF {
            get; set;
        }

        protected string LogoTipo(string htmlContent) {
            if (!File.Exists(this.Logo)) {
                htmlContent = htmlContent.Replace("{{COMPROBANTE_LOGO}}", "");
            } else {
                htmlContent = htmlContent.Replace("{{COMPROBANTE_LOGO}}", string.Concat("<img id=\"empresa-logo\" align=\"middle\" src=\"data:image/png;base64, ", FileExtensions.ReadFileB64(this.Logo), "\"/>"));
            }
            return htmlContent;
        }

        protected virtual string TimbreFiscal(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbreFiscalDigital, string cadenaOriginalTFD, string htmlContent) {
            if (timbreFiscalDigital != null) {
                htmlContent = htmlContent.Replace("{{TIMBRE_UUID}}", timbreFiscalDigital.UUID);
                htmlContent = htmlContent.Replace("{{TIMBRE_CERTISAT}}", timbreFiscalDigital.NoCertificadoSAT);
                htmlContent = htmlContent.Replace("{{TIMBRE_FECHA}}", timbreFiscalDigital.FechaTimbrado.ToString());
                htmlContent = htmlContent.Replace("{{TIMBRE_PROVEEDOR}}", timbreFiscalDigital.RfcProvCertif);

                string cadenaOriginal = cadenaOriginalTFD;
                double largo = cadenaOriginal.Length / 5;
                int c = ConvertExtensions.ConvertInt32(Math.Ceiling(largo));
                htmlContent = htmlContent.Replace("{{COMPROBANTE_CADENAORIGINALTFD}}", string.Join(" ", this.SplitIntoColumns(cadenaOriginal, c + 1, "")));

                largo = timbreFiscalDigital.SelloSAT.Length / 5;
                c = ConvertExtensions.ConvertInt32(Math.Ceiling(largo));
                htmlContent = htmlContent.Replace("{{TIMBRE_SELLOSAT}}", string.Join(" ", this.SplitIntoColumns(timbreFiscalDigital.SelloSAT, c + 1, "")));

                largo = timbreFiscalDigital.SelloCFD.Length / 5;
                c = ConvertExtensions.ConvertInt32(Math.Ceiling(largo));
                htmlContent = htmlContent.Replace("{{TIMBRE_SELLOCFD}}", string.Join(" ", this.SplitIntoColumns(timbreFiscalDigital.SelloCFD, c + 1, "")));
            }
            return htmlContent;
        }

        protected virtual string QR(string emisorRFC, string receptorRFC, decimal total, string uuid, string selloSAT, string htmlContent) {
            byte[] qr = QRCodeExtensions.CopyImageToByteArray(
                            QRCodeExtensions.GetCodigoQR(
                                "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?",
                                emisorRFC, receptorRFC, total.ToString(), uuid, selloSAT));
            htmlContent = htmlContent.Replace("{{TIMBRE_QRCODE}}", string.Concat("<img align=\"middle\" src=\"data:image/png;base64, ", Convert.ToBase64String(qr), "\"/>"));
            return htmlContent;
        }

        /// <summary>
        /// obtener el total en letra
        /// </summary>
        protected virtual string TotalEnLetra(decimal total, string htmlContent) {
            string totalEnLetra = NumeroALetras.Convertir(Double.Parse(total.ToString()), 1);
            htmlContent = htmlContent.Replace("{{COMPROBANTE_CANTIDADCONLETRA}}", totalEnLetra);
            return htmlContent;
        }

        protected virtual bool CrearPDF(string archivotemporal, string keyName) {
            GlobalConfig conf = new GlobalConfig();
            conf.SetMargins(0, 0, 0, 0);
            conf.SetColorMode(true);
            conf.SetPaperOrientation(false);
            conf.SetDocumentTitle("Comprobante Fiscal");
            conf.SetPaperSize(PaperKind.Letter);
            conf.SetOutputDpi(200);

            IPechkin objetop = new SynchronizedPechkin(conf);
            ObjectConfig objeto2 = new ObjectConfig();
            objeto2.SetAllowLocalContent(true);
            objeto2.SetPrintBackground(true);
            objeto2.SetPageUri(archivotemporal);
            objeto2.SetIntelligentShrinking(false);
            objeto2.SetPrintBackground(true);

            // guardamos las conversión del archivo html a PDF en la carpeta de comprobantes
            Byte[] pdfBuffer = objetop.Convert(objeto2);
            this.PDF = Path.Combine(this.PathDefault, keyName + ".pdf");
            FileExtensions.WriteFileByte(pdfBuffer, this.PDF);
            return File.Exists(this.PDF);
        }

        #region nomina 12
        protected virtual void Nomina12Percepciones(SAT.CFDI.Complemento.Nomina.V12.Nomina cfdi, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("percepciones");
            List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra> horasExtras = new List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra>();
            List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos> accionesOTitulos = new List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos>();
            if (htmlNode != null) {
                foreach (var item in cfdi.Percepciones.Percepcion) {
                    HtmlNode temporal = htmlNode.Clone();
                    temporal.InnerHtml = this.Nomina12Percepcion(item, temporal.InnerHtml);
                    htmlNode.ParentNode.ChildNodes.Add(temporal);
                    if (item.HorasExtra != null)
                        horasExtras.AddRange(item.HorasExtra);
                    if (item.AccionesOTitulos != null)
                        accionesOTitulos.Add(item.AccionesOTitulos);
                    temporal = null;
                }
                // removemos el elemento con Id Conceptos, este solo es la base del html
                htmlNode.Remove();

                if (horasExtras.Count > 0) {
                    foreach (var item in horasExtras) {
                        this.Nomina12HorasExtra(item, ref doc);
                    }
                } else {
                    HtmlNode htmlNode1 = doc.GetElementbyId("bloque-horasextra");
                    htmlNode1.Remove();
                }
                this.Nomina12JubilacionPensionRetiro(cfdi.Percepciones, ref doc);
                this.Nomina12SeparacionIndemnizacion(cfdi.Percepciones, ref doc);
                this.Nomina12AccionesOTitulos(accionesOTitulos, ref doc);
            }
        }

        protected virtual void Nomina12Deducciones(SAT.CFDI.Complemento.Nomina.V12.Nomina cfdi, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("deducciones");
            if (htmlNode != null) {
                foreach (var item in cfdi.Deducciones.Deduccion) {
                    HtmlNode temporal = htmlNode.Clone();
                    temporal.InnerHtml = this.Nomina12Deduccion(item, temporal.InnerHtml);
                    htmlNode.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
                // removemos el elemento con Id Conceptos, este solo es la base del html
                htmlNode.Remove();
            }
        }

        protected virtual string Nomina12Percepcion(SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcion percepcion, string stringHtml) {
            stringHtml = stringHtml.Replace("{{NOMINA_PERCEPCION_TIPOPERCEPCION}}", percepcion.TipoPercepcion);
            stringHtml = stringHtml.Replace("{{NOMINA_PERCEPCION_CLAVE}}", percepcion.Clave);
            stringHtml = stringHtml.Replace("{{NOMINA_PERCEPCION_CONCEPTO}}", percepcion.Concepto);
            stringHtml = stringHtml.Replace("{{NOMINA_PERCEPCION_IMPORTEGRAVADO}}", percepcion.ImporteGravado.ToString());
            stringHtml = stringHtml.Replace("{{NOMINA_PERCEPCION_IMPORTEEXENTO}}", percepcion.ImporteExento.ToString());
            return stringHtml;
        }

        protected virtual string Nomina12Deduccion(SAT.CFDI.Complemento.Nomina.V12.NominaDeduccionesDeduccion percepcion, string stringHtml) {
            stringHtml = stringHtml.Replace("{{NOMINA_DEDUCCION_TIPODEDUCCION}}", percepcion.TipoDeduccion);
            stringHtml = stringHtml.Replace("{{NOMINA_DEDUCCION_CLAVE}}", percepcion.Clave);
            stringHtml = stringHtml.Replace("{{NOMINA_DEDUCCION_CONCEPTO}}", percepcion.Concepto);
            stringHtml = stringHtml.Replace("{{NOMINA_DEDUCCION_IMPORTE}}", percepcion.Importe.ToString());
            return stringHtml;
        }

        protected virtual void Nomina12Incapacidades(SAT.CFDI.Complemento.Nomina.V12.Nomina cfdi, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-incapacidades");
            if (htmlNode != null) {
                if (cfdi.Incapacidades != null) {
                    foreach (var item in cfdi.Incapacidades) {
                        var temporal = htmlNode.Clone();
                        temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_INCAPACIDAD_DIASINCAPACIDAD}}", item.DiasIncapacidad.ToString());
                        temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_INCAPACIDAD_TIPOINCAPACIDAD}}", item.ImporteMonetario.ToString());
                        temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_INCAPACIDAD_IMPORTEMONETARIO}}", item.TipoIncapacidad);
                        htmlNode.ParentNode.ChildNodes.Add(temporal);
                        temporal = null;
                    }
                }
                htmlNode.Remove();
            }
        }

        protected virtual void Nomina12HorasExtra(SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionHorasExtra cfdi, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-horasextra");
            HtmlNode temporal = htmlNode.Clone();
            temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_HORASEXTRA_DIAS}}", cfdi.Dias.ToString());
            temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_HORASEXTRA_HORASEXTRA}}", cfdi.HorasExtra.ToString());
            temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_HORASEXTRA_TIPOHORAS}}", cfdi.TipoHoras);
            temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_HORASEXTRA_IMPORTEPAGADO}}", cfdi.ImportePagado.ToString());
        }

        protected virtual void Nomina12JubilacionPensionRetiro(SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones percepciones, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-jubilacion");
            if (percepciones.JubilacionPensionRetiro != null) {

            } else {
                htmlNode.Remove();
            }
        }

        protected virtual void Nomina12SeparacionIndemnizacion(SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones percepciones, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-separacion");
            if (percepciones.SeparacionIndemnizacion != null) {

            } else {
                htmlNode.Remove();
            }
        }

        protected virtual void Nomina12CompensacionSaldosAFavor(List<SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor> lista, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-compensaciones");
            if (htmlNode != null) {
                foreach (var item in lista) {
                    var temporal = htmlNode.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_COMPENSALDO_ANO}}", item.Año.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_COMPENSALDO_SALDOAFAVOR}}", item.SaldoAFavor.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_COMPENSALDO_REMANENTESALFAV}}", item.RemanenteSalFav.ToString());
                    htmlNode.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
            }
            htmlNode.Remove();
        }

        protected virtual void Nomina12SubsidioAlEmpleo(List<SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoSubsidioAlEmpleo> subsidioAlEmpleo, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-subsidio");
            if (htmlNode != null) {
                foreach (var item in subsidioAlEmpleo) {
                    var temporal = htmlNode.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_SUBSIDIO_SUBSIDIOCAUSADO}}", item.SubsidioCausado.ToString());
                    htmlNode.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
            }
            htmlNode.Remove();
        }

        protected virtual void Nomina12AccionesOTitulos(List<SAT.CFDI.Complemento.Nomina.V12.NominaPercepcionesPercepcionAccionesOTitulos> accionesOTitulos, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-acciones");
            if (htmlNode != null) {
                foreach (var item in accionesOTitulos) {
                    var temporal = htmlNode.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_ACCITITU_VALORMERCADO}}", item.ValorMercado.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_ACCITITU_PRECIOALOTORGARSE}}", item.PrecioAlOtorgarse.ToString());
                    htmlNode.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                }
            }
            htmlNode.Remove();
        }

        protected virtual void Nomina12OtrosPagos(SAT.CFDI.Complemento.Nomina.V12.Nomina cfdi, ref HtmlDocument doc) {
            HtmlNode htmlNode = doc.GetElementbyId("bloque-otrospagos");
            var lista = new List<SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoCompensacionSaldosAFavor>();
            var subsidioAlEmpleos = new List<SAT.CFDI.Complemento.Nomina.V12.NominaOtroPagoSubsidioAlEmpleo>();
            if (htmlNode != null) {
                foreach (var item in cfdi.OtrosPagos) {
                    var temporal = htmlNode.Clone();
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_OTROPAGO_TIPOOTROPAGO}}", item.TipoOtroPago);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_OTROPAGO_CLAVE}}", item.Clave.ToString());
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_OTROPAGO_CONCEPTO}}", item.Concepto);
                    temporal.InnerHtml = temporal.InnerHtml.Replace("{{NOMINA_OTROPAGO_IMPORTE}}", item.Importe.ToString());
                    htmlNode.ParentNode.ChildNodes.Add(temporal);
                    temporal = null;
                    if (item.CompensacionSaldosAFavor != null) {
                        lista.Add(item.CompensacionSaldosAFavor);
                    }
                    if (item.SubsidioAlEmpleo != null) {
                        subsidioAlEmpleos.Add(item.SubsidioAlEmpleo);
                    }
                }

                this.Nomina12CompensacionSaldosAFavor(lista, ref doc);

                this.Nomina12SubsidioAlEmpleo(subsidioAlEmpleos, ref doc);

                htmlNode.Remove();
            }
        }
        #endregion

        /// <summary>
        /// formato moneda con decimales definidos
        /// </summary>
        protected decimal FormatoNumero(double numero, int decimales) {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        /// <summary>
        /// formato moneda con decimales definidos
        /// </summary>
        protected decimal FormatoNumero(decimal numero, int decimales) {
            string str = "".PadRight(decimales, '#');
            string str1 = string.Format(string.Concat("{0:0.", str, "}"), numero);
            return Convert.ToDecimal(str1);
        }

        /// <summary>
        /// formato de moneda
        /// </summary>
        protected string FormatoMoneda(double p) {
            return string.Format(CultureInfo.CreateSpecificCulture("es-MX"), "{0:C2}", new object[] { p });
        }

        /// <summary>
        /// convierte una cadena en un arreglo de tamaño definido
        /// </summary>
        protected string[] SplitIntoColumns(string inputString, int columns, string splitString) {
            string[] output = new string[checked(checked(columns - 1) + 1)];
            int colLength = checked((int)Math.Round((double)(inputString.Length) / (double)columns));
            int lastPos = 0;
            int num = columns;
            for (int colCount = 1; colCount <= num; colCount = checked(colCount + 1)) {
                int pos = -1;
                if (checked(lastPos + colLength) < inputString.Length)
                    pos = inputString.IndexOf(splitString, checked(lastPos + colLength));

                if (pos == -1)
                    pos = inputString.Length;

                output[checked(colCount - 1)] = inputString.Substring(checked(lastPos), checked(pos - lastPos));
                lastPos = pos;
            }
            return output;
        }

        protected string TipoComprobante(string TipoComprobanteText) {
            if (TipoComprobanteText == "Ingreso" || TipoComprobanteText == "I")
                return "I: Ingreso";
            else if (TipoComprobanteText == "Egreso" || TipoComprobanteText == "E")
                return "E: Egreso";
            else if (TipoComprobanteText == "Traslado" || TipoComprobanteText == "T")
                return "T: Traslado";
            else if (TipoComprobanteText == "Nomina" || TipoComprobanteText == "N")
                return "N: Nomina";
            else if (TipoComprobanteText == "Pagos" || TipoComprobanteText == "P")
                return "P - Pagos";
            else
                return "I - Ingreso";
        }
    }
}