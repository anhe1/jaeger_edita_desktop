﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;

namespace Jaeger.Aplication.Comprobante.Mappers {
    public class ComplementoNominaSerializeExtensions {
        #region complemento de nomina12
        public static IComplementoNominaDetailModel Create(SAT.CFDI.Complemento.Nomina.V12.Nomina nomina) {
            IComplementoNominaDetailModel complemento = new ComplementoNominaDetailModel {
                Activo = true,
                FechaFinalPago = nomina.FechaFinalPago,
                FechaInicialPago = nomina.FechaInicialPago,
                FechaPago = nomina.FechaPago,
                NumDiasPagados = nomina.NumDiasPagados,
                Version = nomina.Version,
                TipoNomina = nomina.TipoNomina,
            };
            complemento = Create(nomina.Receptor, ref complemento);
            complemento = Create(nomina.Emisor, ref complemento);
            var percepciones = Create(nomina.Percepciones);
            var deducciones = Create(nomina.Deducciones);
            var otrospagos = Create(nomina.OtrosPagos);
            var incapacidades = Create(nomina.Incapacidades);
            percepciones.AddRange(deducciones);
            percepciones.AddRange(otrospagos);
            complemento.Partes = new BindingList<ComplementoNominaParteDetailModel>(percepciones);
            complemento.PercepcionTotalExento = nomina.Percepciones.TotalExento;
            complemento.PercepcionTotalGravado = nomina.Percepciones.TotalGravado;
            complemento.DeduccionTotalExento = nomina.Deducciones.TotalOtrasDeducciones;
            
            return complemento;
        }

        private static IComplementoNominaDetailModel Create(SAT.CFDI.Complemento.Nomina.V12.NominaEmisor emisor, ref IComplementoNominaDetailModel nomina) {
            nomina.RegistroPatronal = nomina.RegistroPatronal;
            return nomina;
        }

        private static IComplementoNominaDetailModel Create(SAT.CFDI.Complemento.Nomina.V12.NominaReceptor receptor, ref IComplementoNominaDetailModel nomina) {
            nomina.Antiguedad = receptor.Antigüedad;
            nomina.Banco = receptor.Banco;
            nomina.EntidadFederativa = receptor.ClaveEntFed;
            nomina.CtaBanco = receptor.CuentaBancaria;
            nomina.CURP = receptor.Curp;
            nomina.Departamento = receptor.Departamento;
            nomina.FechaInicioRelLaboral = receptor.FechaInicioRelLaboral;
            nomina.NumEmpleado = receptor.NumEmpleado;
            nomina.NumSeguridadSocial = receptor.NumSeguridadSocial;
            nomina.PeriodicidadPago = receptor.PeriodicidadPago;
            nomina.Puesto = receptor.Puesto;
            nomina.RiesgoPuesto = receptor.RiesgoPuesto;
            nomina.SalarioBaseCotApor = receptor.SalarioBaseCotApor;
            nomina.SalarioDiarioIntegrado = receptor.SalarioDiarioIntegrado;
            nomina.TipoContrato = receptor.TipoContrato;
            nomina.TipoJornada = receptor.TipoJornada;
            nomina.TipoRegimen = receptor.TipoRegimen;
            return nomina;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaPercepciones percepciones) {
            var response = new List<ComplementoNominaParteDetailModel>();
            foreach (var percepcion in percepciones.Percepcion) {
                var d1 = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Clave = percepcion.Clave,
                    Concepto = percepcion.Concepto,
                    Tipo = percepcion.TipoPercepcion,
                    ImporteGravado = percepcion.ImporteGravado,
                    ImporteExento = percepcion.ImporteExento,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.Percepcion
                };
                response.Add(d1);
                if (percepcion.HorasExtra != null) {
                    foreach (var item in percepcion.HorasExtra) {
                        var horaExtra = new ComplementoNominaParteDetailModel {
                            HorasExtra = item.HorasExtra,
                            ImportePagadoHorasExtra = item.ImportePagado,
                            TipoHoras = item.TipoHoras,
                            Dias = item.Dias,
                            TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.HorasExtra,
                            Activo = true
                        };
                        response.Add(horaExtra);
                    }
                }

                if (percepcion.AccionesOTitulos != null) {
                    var accionesotitulos = new ComplementoNominaParteDetailModel {
                        Activo = true,
                        IdDoc = 5
                    };
                    response.Add(accionesotitulos);
                }

            }
            return response;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaDeducciones deducciones) {
            var response = new List<ComplementoNominaParteDetailModel>();
            foreach (var deduccion in deducciones.Deduccion) {
                var item = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Clave = deduccion.Clave,
                    Concepto = deduccion.Concepto,
                    Tipo = deduccion.TipoDeduccion,
                    Importe = deduccion.Importe,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.Deduccion
                };
                response.Add(item);
            }
            return response;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaOtroPago[] otroPagos) {
            var response = new List<ComplementoNominaParteDetailModel>();
            foreach (var otropago in otroPagos) {
                var item = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Tipo = otropago.TipoOtroPago,
                    Clave = otropago.Clave,
                    Concepto = otropago.Concepto,
                    Importe = otropago.Importe,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.OtrosPagos
                };
                response.Add(item);
            }
            return response;
        }

        private static List<ComplementoNominaParteDetailModel> Create(SAT.CFDI.Complemento.Nomina.V12.NominaIncapacidad[] incapacidades) {
            if (incapacidades == null) { return new List<ComplementoNominaParteDetailModel>(); }
            var response = new List<ComplementoNominaParteDetailModel>();
            foreach (var incapacidad in incapacidades) {
                var item = new ComplementoNominaParteDetailModel {
                    Activo = true,
                    Tipo = incapacidad.TipoIncapacidad,
                    DiasIncapacidad = incapacidad.DiasIncapacidad,
                    DescuentoIncapcidad = incapacidad.ImporteMonetario,
                    TipoElemento = Domain.Comprobante.ValueObjects.NominaElementoEnum.Incapacidad
                };
                response.Add(item);
            }
            return response;
        }
        #endregion
    }
}
