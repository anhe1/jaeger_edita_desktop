﻿using Jaeger.Domain.Comprobante.Entities.Complemento;

namespace Jaeger.Aplication.Comprobante.Mappers {
    /// <summary>
    /// Complemento de Timbre Fiscal
    /// </summary>
    public class ComplementoTimbreFiscalExtensions {
        #region
        /// <summary>
        /// convertir un objeto TimbreFiscal v11 al objeto comun 
        /// </summary>
        /// <returns>ComplementoTimbreFiscal</returns>
        public static ComplementoTimbreFiscal TimbreFiscal(SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital timbre11) {
            var response = new ComplementoTimbreFiscal {
                UUID = timbre11.UUID.ToUpper(),
                Version = timbre11.Version,
                FechaTimbrado = timbre11.FechaTimbrado,
                Leyenda = timbre11.Leyenda,
                NoCertificadoSAT = timbre11.NoCertificadoSAT,
                RFCProvCertif = timbre11.RfcProvCertif,
                SelloCFD = timbre11.SelloCFD,
                SelloSAT = timbre11.SelloSAT
            };
            return response;
        }

        /// <summary>
        /// convertir un objeto TimbreFiscal v10 al objeto comun 
        /// </summary>
        /// <returns>ComplementoTimbreFiscal</returns>
        public static ComplementoTimbreFiscal TimbreFiscal(SAT.CFDI.Complemento.TimbreFiscal.V10.TimbreFiscalDigital timbre10) {
            var response = new ComplementoTimbreFiscal {
                UUID = timbre10.UUID.ToUpper(),
                Version = timbre10.version,
                FechaTimbrado = timbre10.FechaTimbrado,
                Leyenda = string.Empty,
                NoCertificadoSAT = timbre10.noCertificadoSAT,
                RFCProvCertif = string.Empty,
                SelloCFD = timbre10.selloCFD,
                SelloSAT = timbre10.selloSAT
            };
            return response;
        }
        #endregion
    }
}
