﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Comprobante.Services {
    public static class GridComprobanteService0 {
        #region columnas para el comprobante
        /// <summary>
        /// obtener columna para version del comprobante
        /// </summary>
        public static GridViewTextBoxColumn Version {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Version",
                    HeaderText = "Ver.",
                    Name = "Version",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColID {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColTipoComprobante {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "TipoComprobante",
                    HeaderText = "TipoComprobante",
                    IsVisible = false,
                    Name = "TipoComprobante",
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColSubtipoText {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "SubTipoText",
                    HeaderText = "Tipo",
                    IsVisible = false,
                    Name = "SubTipoText",
                    ReadOnly = true
                };
            }
        }

        public static GridViewComboBoxColumn ColStatus {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "Status",
                    HeaderText = "Status",
                    Name = "Status",
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColFolio {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Folio",
                    HeaderText = "Folio",
                    Name = "Folio",
                    ReadOnly = true,
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColSerie {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Serie",
                    HeaderText = "Serie",
                    Name = "Serie",
                    ReadOnly = true,
                    Width = 65
                };
            }
        }

        /// <summary>
        /// obtener columna del RFC del emisor
        /// </summary>
        public static GridViewTextBoxColumn EmisorRFC {
            get {
                var rfc = GridTelerikCommon.ColRFC;
                rfc.Name = "EmisorRFC";
                rfc.FieldName = "EmisorRFC";
                rfc.HeaderText = "RFC Emisor";
                return rfc;
            }
        }

        public static GridViewTextBoxColumn EmisorNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "EmisorNombre",
                    HeaderText = "Nombre del Emisor",
                    Name = "EmisorNombre",
                    ReadOnly = true,
                    Width = 180
                };
            }
        }

        /// <summary>
        /// obtener columna del RFC del emisor
        /// </summary>
        public static GridViewTextBoxColumn ReceptorRFC {
            get {
                var rfc = GridTelerikCommon.ColRFC;
                rfc.Name = "ReceptorRFC";
                rfc.FieldName = "ReceptorRFC";
                rfc.HeaderText = "RFC Receptor";
                return rfc;
            }
        }

        /// <summary>
        /// obtener columna del nombre del receptor
        /// </summary>
        public static GridViewTextBoxColumn ReceptorNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorNombre",
                    HeaderText = "Nombre del Receptor",
                    Name = "ReceptorNombre",
                    ReadOnly = true,
                    Width = 220
                };
            }
        }

        public static GridViewTextBoxColumn ColIdDocumento {
            get {
                var uuid = GridTelerikCommon.ColIdDocumento;
                uuid.HeaderText = "Folio Fiscal (UUID)";
                return uuid;
            }
        }

        public static GridViewDateTimeColumn ColFechaEmision {
            get { return GridTelerikCommon.ColFechaEmision; }
        }

        public static GridViewDateTimeColumn ColFechaTimbre {
            get {
                var fecha = GridTelerikCommon.ColBaseDateTime;
                fecha.Name = "FechaTimbre";
                fecha.FieldName = "FechaTimbre";
                fecha.HeaderText = "Fecha de \r\n Certificación";
                return fecha;
            }
        }

        public static GridViewDateTimeColumn ColFechaCancela {
            get {
                var fecha = GridTelerikCommon.ColBaseDateTime;
                fecha.Name = "FechaCancela";
                fecha.FieldName = "FechaCancela";
                fecha.HeaderText = "Fecha de \r\n Cancelación";
                return fecha;
            }
        }

        public static GridViewDateTimeColumn ColFechaValidacion {
            get {
                var fecha = GridTelerikCommon.ColBaseDateTime;
                fecha.Name = "FechaValidacion";
                fecha.FieldName = "FechaValidacion";
                fecha.HeaderText = "Fecha de \r\n Validación";
                return fecha;
            }
        }

        public static GridViewDateTimeColumn ColFechaEntrega {
            get {
                var fecha = GridTelerikCommon.ColBaseDateTime;
                fecha.Name = "FechaEntrega";
                fecha.FieldName = "FechaEntrega";
                fecha.HeaderText = "Fecha de \r\n Entrega";
                return fecha;
            }
        }

        public static GridViewDateTimeColumn ColFechaUltPago {
            get {
                var fecha = GridTelerikCommon.ColBaseDateTime;
                fecha.Name = "FechaUltimoPago";
                fecha.FieldName = "FechaUltimoPago";
                fecha.HeaderText = "Ult. Pago";
                return fecha;
            }
        }

        public static GridViewDateTimeColumn ColFechaRecepcionPago {
            get {
                return new GridViewDateTimeColumn {
                    FieldName = "FechaRecepcionPago",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "C. Fecha Pago",
                    Name = "FechaRecepcionPago",
                    Width = 70,
                };
            }
        }

        public static GridViewTextBoxColumn ColNumParcialidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NumParcialidad",
                    HeaderText = "Núm. Par.",
                    IsVisible = false,
                    Name = "NumParcialidad",
                    WrapText = true,
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveMetodoPago {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveMetodoPago",
                    HeaderText = "Método de Pago",
                    IsVisible = false,
                    Name = "ClaveMetodoPago",
                    ReadOnly = true,
                    Width = 75,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColFormaPago {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveFormaPago",
                    HeaderText = "Forma de Pago",
                    Name = "ClaveFormaPago"
                };
            }
        }

        public static GridViewTextBoxColumn ColSubTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "SubTotal",
                    FormatString = "{0:n}",
                    HeaderText = "SubTotal",
                    IsVisible = false,
                    Name = "SubTotal",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTrasladoIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TrasladoIVA",
                    FormatString = "{0:n}",
                    HeaderText = "Iva",
                    IsVisible = false,
                    Name = "TrasladoIVA",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColDescuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Descuento",
                    FormatString = "{0:n}",
                    HeaderText = "Descuento",
                    Name = "Descuento",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 95
                };
            }
        }

        public static GridViewTextBoxColumn ColTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Total1",
                    FormatString = "{0:n}",
                    HeaderText = "Total",
                    Name = "Total",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColAumulado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(double),
                    FieldName = "Acumulado",
                    FormatString = "{0:n}",
                    HeaderText = "Cobrado",
                    Name = "Acumulado",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColSaldo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Saldo",
                    FormatString = "{0:n}",
                    HeaderText = "Saldo",
                    Name = "Saldo",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColEstado {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Estado",
                    HeaderText = "Estado",
                    Name = "Estado",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaEstado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(System.DateTime),
                    FieldName = "FechaEstado",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fecha del Estado",
                    IsVisible = false,
                    Name = "FechaEstado",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 70,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColFileXmlURL {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "UrlFileXml",
                    HeaderText = "XML",
                    Name = "UrlFileXml",
                    ReadOnly = true,
                    Width = 30
                };
            }
        }


        public static GridViewTextBoxColumn ColFilePdfURL {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "UrlFilePdf",
                    HeaderText = "PDF",
                    Name = "UrlFilePdf",
                    ReadOnly = true,
                    Width = 30
                };
            }
        }


        public static GridViewTextBoxColumn ColMoneda {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Moneda",
                    HeaderText = "Moneda",
                    IsVisible = false,
                    Name = "Moneda"
                };
            }
        }


        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Nota",
                    Name = "Nota",
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColDiasTranscurridos {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "DiasTranscurridos",
                    FormatString = "{0:N0}",
                    HeaderText = "Días Trans.",
                    Name = "DiasTranscurridos"
                };
            }
        }

        public static GridViewTextBoxColumn ColTotal1 {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Total1",
                    FormatString = "{0:n}",
                    HeaderText = "Total",
                    Name = "Total",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }


        public static GridViewTextBoxColumn ColImporteNota {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "ImporteNota",
                    FormatString = "{0:N2}",
                    HeaderText = "Imp. Nota",
                    IsVisible = false,
                    Name = "ImporteNota",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }


        public static GridViewTextBoxColumn ColImportePagado {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ImportePagado",
                    FormatString = "{0:n}",
                    HeaderText = "C. Pagos",
                    Name = "ImportePagado",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }


        public static GridViewTextBoxColumn ColSaldoPagos {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "SaldoPagos",
                    FormatString = "{0:n}",
                    HeaderText = "C. Saldo",
                    Name = "SaldoPagos",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        #endregion

        #region columas para conceptos

        public static GridViewTextBoxColumn ColCantidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Cantidad",
                    FormatString = "{0:n}",
                    HeaderText = "Cantidad",
                    Name = "Cantidad",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 70
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveUnidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveUnidad",
                    HeaderText = "Clv. Unidad",
                    Name = "ClaveUnidad",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColUnidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Unidad",
                    HeaderText = "Unidad",
                    Name = "Unidad",
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColNoOrden {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "NoOrden",
                    HeaderText = "No. Orden",
                    Name = "NoOrden",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 70
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveProducto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveProducto",
                    HeaderText = "Clv. Prod.",
                    Name = "ClaveProducto",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColConcepto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Concepto",
                    HeaderText = "Concepto",
                    Name = "Concepto",
                    Width = 285,
                    MaxLength = 255
                };
            }
        }

        public static GridViewTextBoxColumn ColUnitario {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Unitario",
                    FormatString = "{0:n2}",
                    HeaderText = "Unitario",
                    Name = "Unitario",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColImporte {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Importe",
                    FormatString = "{0:n2}",
                    HeaderText = "Importe",
                    Name = "Importe",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColValorUnitario {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "ValorUnitario",
                    HeaderText = "Unitario",
                    Name = "ValorUnitario",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColCuentaPredial {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "CtaPredial",
                    HeaderText = "Cta. Predial",
                    Name = "CtaPredial",
                    Width = 80
                };
            }
        }
        #endregion
        /// <summary>
        /// Grid para mostrar informacion de facturacion asociada al numero de orden utilizada en productos vendidos
        /// </summary>
        /// <returns></returns>
        public static GridViewDataColumn[] GetFacturaConceptos() {
            return new GridViewDataColumn[] {
              ColStatus, ColFolio, ColSerie, ColIdDocumento, ColFechaEmision, ColCantidad, ColClaveUnidad, ColUnidad, ColClaveProducto, ColNoOrden, ColConcepto, ColValorUnitario, ColImporte
            };
        }

        #region Complemento a cuenta de terceros
        public static GridViewDataColumn[] ComplementoCuentaTerceros() {
            return new GridViewDataColumn[] { GridTelerikCommon.ColRFC, NombreT, ColRegimenFiscal, ColDomicilioFiscal };
        }

        public static GridViewTextBoxColumn NombreT {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NombreACuentaTerceros",
                    HeaderText = "Denominación ó Razón Social",
                    Name = "NombreACuentaTerceros",
                    ReadOnly = true,
                    Width = 220
                };
            }
        }

        public static GridViewTextBoxColumn ColRegimenFiscal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RegimenFiscalACuentaTerceros",
                    HeaderText = "Rég. Fiscal",
                    Name = "RegimenFiscalACuentaTerceros",
                    ReadOnly = true,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColDomicilioFiscal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "DomicilioFiscal",
                    HeaderText = "Dom. Fiscal",
                    Name = "DomicilioFiscal",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 85
                };
            }
        }
        #endregion

        #region formatos condicionales
        /// <summary>
        /// formato condicional para los registros no activos
        /// </summary>
        public static ConditionalFormattingObject RegistroActivo() {
            return new ConditionalFormattingObject {
                ApplyToRow = true,
                CellBackColor = Color.Empty,
                CellFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Italic, GraphicsUnit.Point, ((byte)(0))),
                CellForeColor = Color.Empty,
                Name = "Activo",
                RowBackColor = Color.Empty,
                RowFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Italic, GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = Color.Gray,
                TValue1 = "False"
            };
        }

        /// <summary>
        /// formato condicional para mostrar si el comprobante relacionado en el complemento de pago existe en la tabla de comprobantes
        /// </summary>
        public static ConditionalFormattingObject ComplmentoPagoRelacionComprobante() {
            return new ConditionalFormattingObject {
                ApplyToRow = true,
                CellBackColor = Color.Empty,
                CellForeColor = Color.Empty,
                Name = "Relacion",
                RowBackColor = Color.Empty,
                RowFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Italic, GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = Color.DimGray,
                TValue1 = "0",
                TValue2 = "0"
            };
        }
        #endregion
    }
}
