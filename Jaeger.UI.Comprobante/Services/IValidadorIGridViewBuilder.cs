﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Services {
    public interface IValidadorGridViewBuilder : IValidadorGridViewColumnsBuilder, IDisposable {
    }

    public interface IValidadorGridViewTempleteBuilder {

    }

    public interface IValidadorGridViewColumnsBuilder {

    }

    public class ValidadorIGridViewBuilder : GridViewBuilder, IGridViewBuilder, IValidadorGridViewBuilder {
        public IValidadorGridViewColumnsBuilder AddColumnId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddTipoComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoComprobante",
                HeaderText = "Tipo Comprobante",
                Name = "TipoComprobante"
            });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddVersion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Version",
                HeaderText = "Versión",
                IsVisible = false,
                Name = "Version", });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddMetaData() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetaData",
                HeaderText = "Doc.",
                IsVisible = false,
                Name = "MetaData",
            });
            return this;
        }
        public IValidadorGridViewColumnsBuilder AddFolio() {
            this._Columns.Add(new
            GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio", });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddSerie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
            });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddEmisorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorRFC",
                HeaderText = "Emisor (RFC)",
                Name = "EmisorRFC",
                Width = 85, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddEmisorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorNombre",
                HeaderText = "Emisor",
                Name = "EmisorNombre",
                Width = 220, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "Receptor (RFC)",
                Name = "ReceptorRFC",
                Width = 85, });
            return this;
        }



        public IValidadorGridViewColumnsBuilder AddReceptorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Receptor",
                Name = "ReceptorNombre",
                Width = 220, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddFechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEmision",
                FormatString = "{0:d}",
                HeaderText = "Fecha",
                Name = "FechaEmision",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddFechaTimbre() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaTimbre",
                FormatString = "{0:d}",
                HeaderText = "Fec. Certifica",
                Name = "FechaTimbre",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75,
                WrapText = true, });
            return this;
        }


        public IValidadorGridViewColumnsBuilder AddIdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "UUID",
                Name = "IdDocumento",
                Width = 240, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddNoCertificado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoCertificado",
                HeaderText = "No. Certificado",
                IsVisible = false,
                Name = "NoCertificado",
                Width = 95, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddRFCProvCertif() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFCProvCertif",
                HeaderText = "Prov. Certif. (RFC)",
                IsVisible = false,
                Name = "RFCProvCertif",
                Width = 90, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado SAT",
                Name = "EstadoSAT",
                Width = 65, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddLugarExpedicion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "LugarExpedicion",
                HeaderText = "Expedición",
                IsVisible = false,
                Name = "LugarExpedicion", });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddUsoCFDI() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UsoCFDI",
                HeaderText = "Uso CFDI",
                Name = "ClaveUsoCFDI", });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddMetodoPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetodoPago",
                HeaderText = "Método Pago",
                Name = "MetodoPago"
            });
            return this;
        }
        public IValidadorGridViewColumnsBuilder AddFormaPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Forma de Pago",
                FieldName = "FormaPago",
                Name = "FormaPago",
                WrapText = true, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddMetodoVsForma() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetodoVsForma",
                HeaderText = "Met.Vs Pago",
                Name = "MetodoVsForma",
                Width = 85, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddSubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = "{0:n}",
                HeaderText = "SubTotal",
                Name = "SubTotal",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 85, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddDescuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:n}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 85, });
            return this;
        }
        public IValidadorGridViewColumnsBuilder AddRetencionISR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionISR",
                FormatString = "{0:n}",
                HeaderText = "ISR Ret.",
                Name = "RetencionISR",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75, });
            return this;
        }
        public IValidadorGridViewColumnsBuilder AddRetencionIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionIVA",
                FormatString = "{0:n}",
                HeaderText = "IVA Ret.",
                Name = "RetencionIVA",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddRetencionIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionIEPS",
                FormatString = "{0:n}",
                HeaderText = "IEPS Ret.",
                Name = "RetencionIEPS",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = "{0:n}",
                HeaderText = "IVA Tras.",
                Name = "TrasladoIVA",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddTrasladoIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIEPS",
                FormatString = "{0:n}",
                HeaderText = "IEPS Tras.",
                Name = "TrasladoIEPS",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 85, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddPathXML() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "PathXML",
                HeaderText = "XML",
                Name = "XML",
                Width = 30, });
            return this;
        }
        public IValidadorGridViewColumnsBuilder AddPathPDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "PathPDF",
                HeaderText = "PDF",
                Name = "PDF",
                Width = 30, });
            return this;
        }
        public IValidadorGridViewColumnsBuilder AddSituacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Situacion",
                HeaderText = "Situación",
                Name = "Situacion",
                Width = 85, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "Clave Unidad",
                Name = "ClaveUnidad",
                Width = 150, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddClaveConceptos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveConceptos",
                HeaderText = "Clave Conceptos",
                Multiline = true,
                Name = "ClaveConceptos",
                Width = 250, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddComplementos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Complementos",
                HeaderText = "Complementos",
                IsVisible = false,
                Name = "Complementos", });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddResultado() {
            this._Columns.Add(new GridViewCommandColumn {
                FieldName = "Resultado",
                HeaderText = "Resultado",
                IsPinned = true,
                Name = "Resultado",
                PinPosition = PinnedColumnPosition.Right,
                Width = 85, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddRegistrado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Registrado",
                HeaderText = "Registrado",
                IsVisible = false,
                Name = "Registrado",
                VisibleInColumnChooser = false, });
            return this;
        }

        public IValidadorGridViewColumnsBuilder AddInformacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Informacion",
                HeaderText = "Información",
                Name = "Informacion", });
            return this;
        }
    }
}
