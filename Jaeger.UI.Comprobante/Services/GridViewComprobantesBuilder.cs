﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Comprobante.Services {
    public class GridViewComprobantesBuilder : Common.Builder.GridViewBuilder, IGridViewComprobantesBuilder, IGridViewComprobantesTempletesBuilder, IGridViewComprobantesBuild {
        public GridViewComprobantesBuilder() : base() {
            
        }

        public IGridViewComprobantesBuild Columns() {
            return this;
        }

        public IGridViewComprobantesTempletesBuilder Templetes() {
            return this;
        }

        public IGridViewComprobantesBuild Emitidos() {
            this.AddId().AddVersion().AddActivo().AddTipoComprobante().AddSubTipoText().AddStatus().AddFolio().AddSerie().AddEmisorRFC().AddReceptorRFC().AddReceptorNombre().AddIdDocumento().AddTipoComprobanteText()
                .AddFechaEmision().AddFechaTimbre().AddFechaCancela().AddFechaEntrega().AddFechaUltimoPago("Últ. Cobro").AddFechaRecepcionPago().AddNumParcialidad().AddClaveMetodoPago().AddClaveFormaPago().AddSubTotal()
                .AddTrasladoIVA().AddTotal1().AddDescuento().AddImporteNota().AddAcumulado().AddSaldo().AddImportePagado().AddSaldoPagos().AddEstado().AddFechaEstado().AddFechaNuevo().AddCreo().AddFechaModifica()
                .AddModifica().AddUrlFileXml().AddUrlFilePdf().AddMoneda().AddNota().AddDiasTranscurridos();
            return this;
        }

        public IGridViewComprobantesBuild Nomina() {
            throw new NotImplementedException();
        }

        public IGridViewComprobantesBuild Recibidos() {
            this.AddId().AddVersion().AddActivo().AddTipoComprobante().AddSubTipoText().AddStatus().AddFolio().AddSerie().AddEmisorRFC().AddReceptorRFC().AddReceptorNombre().AddIdDocumento().AddTipoComprobanteText()
                .AddFechaEmision().AddFechaTimbre().AddFechaCancela().AddFechaEntrega().AddFechaUltimoPago("Últ. Pago").AddFechaRecepcionPago().AddNumParcialidad().AddClaveMetodoPago().AddClaveFormaPago().AddSubTotal()
                .AddTrasladoIVA().AddTotal1().AddDescuento().AddImporteNota().AddAcumulado().AddSaldo().AddImportePagado().AddSaldoPagos().AddEstado().AddFechaEstado().AddFechaNuevo().AddCreo().AddFechaModifica()
                .AddModifica().AddUrlFileXml().AddUrlFilePdf().AddMoneda().AddNota().AddDiasTranscurridos();
            return this;
        }

        public IGridViewComprobantesBuild Conceptos() {
            this.AddIdConcepto().AddSubId().AddNumPedido().AddCantidad().AddUnidad().AddClaveUnidad().AddNoIdentificacion().AddClaveProducto().AddObjImpuesto().AddDescripcion().AddValorUnitario().AddImporte().AddCtaPredial();
            return this;
        }

        public IGridViewComprobantesBuild ACuentaDeTerceros() {
            this.RfcACuentaTerceros().AddNombreACuentaTerceros().AddRegimenFiscalACuentaTerceros().AddDomicilioFiscalACuentaTerceros();
            return this;
        }

        public IGridViewComprobantesBuild Facturacionconceptos() {
            this.AddStatus().AddFolio().AddSerie().AddIdDocumento().AddFechaEmision().AddNumPedido().AddCantidad().AddClaveUnidad().AddUnidad().AddClaveProducto().AddDescripcion().AddValorUnitario().AddImporte();
            return this;
        }

        #region comprobante
        public IGridViewComprobantesBuild AddId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        /// <summary>
        /// obtener columna para version del comprobante
        /// </summary>
        public IGridViewComprobantesBuild AddVersion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Version",
                HeaderText = "Ver.",
                Name = "Version",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// columna de registro activo
        /// </summary>
        public IGridViewComprobantesBuild AddActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                IsVisible = false,
                Name = "Activo",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        /// <summary>
        /// columna de tipo de comprobante
        /// </summary>
        public IGridViewComprobantesBuild AddTipoComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "TipoComprobante",
                HeaderText = "TipoComprobante",
                IsVisible = false,
                Name = "TipoComprobante",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IGridViewComprobantesBuild AddSubTipoText() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "SubTipoText",
                HeaderText = "Tipo",
                IsVisible = false,
                Name = "SubTipoText",
                ReadOnly = true,
            });
            return this;
        }

        public IGridViewComprobantesBuild AddStatus() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Status",
                HeaderText = "Status",
                Name = "Status",
                Width = 80
            });
            return this;
        }

        /// <summary>
        /// columna de folio de control interno
        /// </summary>
        /// <returns></returns>
        public IGridViewComprobantesBuild AddFolio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                ReadOnly = true,
                Width = 65
            });
            return this;
        }

        /// <summary>
        /// columna de serie de control interno
        /// </summary>
        public IGridViewComprobantesBuild AddSerie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                ReadOnly = true,
                Width = 65
            });
            return this;
        }

        public IGridViewComprobantesBuild AddEmisorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorRFC",
                HeaderText = "RFC Emisor",
                Name = "EmisorRFC",
                ReadOnly = true,
                Width = 90
            });
            return this;
        }

        public IGridViewComprobantesBuild AddEmisorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorNombre",
                HeaderText = "Nombre del Emisor",
                Name = "EmisorNombre",
                ReadOnly = true,
                Width = 180
            });
            return this;
        }

        public IGridViewComprobantesBuild AddReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "RFC Receptor",
                Name = "ReceptorRFC",
                ReadOnly = true,
                Width = 90
            });
            return this;
        }

        public IGridViewComprobantesBuild AddReceptorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Nombre del Receptor",
                Name = "ReceptorNombre",
                ReadOnly = true,
                Width = 220
            });
            return this;
        }

        public IGridViewComprobantesBuild AddIdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "Folio Fiscal (uuid)",
                Name = "IdDocumento",
                ReadOnly = true,
                Width = 180
            });
            return this;
        }

        public IGridViewComprobantesBuild AddTipoComprobanteText() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoComprobanteText",
                HeaderText = "Tipo",
                Name = "TipoComprobanteText",
                ReadOnly = true,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEmision",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Emisión",
                Name = "FechaEmision",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaTimbre() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaTimbre",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Certificación",
                Name = "FechaTimbre",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaCancela() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaCancela",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Cancelación",
                Name = "FechaCancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaValidacion() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaValidacion",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Validación",
                Name = "FechaValidacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaEntrega() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEntrega",
                Format = System.Windows.Forms.DateTimePickerFormat.Short,
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Entrega",
                Name = "FechaEntrega",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaUltimoPago(string headerText = "Ültm. Pago") {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaUltimoPago",
                Format = System.Windows.Forms.DateTimePickerFormat.Short,
                FormatString = "{0:dd MMM yy}",
                HeaderText = headerText,
                Name = "FechaUltimoPago",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaRecepcionPago() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaRecepcionPago",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "C. Fecha Pago",
                Name = "FechaRecepcionPago",
                Width = 70
            });
            return this;
        }

        public IGridViewComprobantesBuild AddNumParcialidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumParcialidad",
                HeaderText = "Núm. Par.",
                IsVisible = false,
                Name = "NumParcialidad",
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddClaveMetodoPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveMetodoPago",
                HeaderText = "Método de Pago",
                IsVisible = false,
                Name = "ClaveMetodoPago",
                ReadOnly = true,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddClaveFormaPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveFormaPago",
                HeaderText = "Forma de Pago",
                Name = "ClaveFormaPago"
            });
            return this;
        }

        public IGridViewComprobantesBuild AddSubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = "{0:n}",
                HeaderText = "SubTotal",
                IsVisible = false,
                Name = "SubTotal",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewComprobantesBuild AddTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = "{0:n}",
                HeaderText = "Iva",
                IsVisible = false,
                Name = "TrasladoIVA",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewComprobantesBuild AddTotal1() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total1",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewComprobantesBuild AddDescuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:n}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 95
            });
            return this;
        }

        public IGridViewComprobantesBuild AddImporteNota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImporteNota",
                FormatString = "{0:N2}",
                HeaderText = "Imp. Nota",
                IsVisible = false,
                Name = "ImporteNota",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewComprobantesBuild AddAcumulado(string headerText = "Acumulado") {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "Acumulado",
                FormatString = "{0:n}",
                HeaderText = headerText,
                Name = "Acumulado",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewComprobantesBuild AddSaldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Saldo",
                FormatString = "{0:n}",
                HeaderText = "Saldo",
                Name = "Saldo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// columna del acumulado creado por los comprobantes de pago relacionados
        /// </summary>
        public IGridViewComprobantesBuild AddImportePagado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ImportePagado",
                FormatString = "{0:n}",
                HeaderText = "C. Pagos",
                Name = "ImportePagado",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// columna del saldo creado por los comprobantes de pago relacionados
        /// </summary>
        public IGridViewComprobantesBuild AddSaldoPagos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "SaldoPagos",
                FormatString = "{0:n}",
                HeaderText = "C. Saldo",
                Name = "SaldoPagos",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// estado SAT del comprobante fiscal
        /// </summary>
        public IGridViewComprobantesBuild AddEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                Name = "Estado",
                Width = 75
            });
            return this;
        }

        public IGridViewComprobantesBuild AddFechaEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaEstado",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha del Estado",
                IsVisible = false,
                Name = "FechaEstado",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public new IGridViewComprobantesBuild AddFechaNuevo() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaNuevo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Sistema",
                IsVisible = false,
                Name = "FechaNuevo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70
            });
            return this;
        }

        public new IGridViewComprobantesBuild AddCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                IsVisible = false,
                Name = "Creo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public new IGridViewComprobantesBuild AddModifica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Modifica",
                HeaderText = "Mod.",
                IsVisible = false,
                Name = "Modifica",
                ReadOnly = true,
                Width = 75
            });
            return this;
        }

        public new IGridViewComprobantesBuild AddFechaModifica() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaMod",
                Format = System.Windows.Forms.DateTimePickerFormat.Short,
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fecha de Modificación",
                IsVisible = false,
                Name = "FechaMod",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IGridViewComprobantesBuild AddUrlFileXml() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFileXml",
                HeaderText = "XML",
                Name = "UrlFileXml",
                ReadOnly = true,
                Width = 30
            });
            return this;
        }

        public IGridViewComprobantesBuild AddUrlFilePdf() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFilePdf",
                HeaderText = "PDF",
                Name = "UrlFilePdf",
                ReadOnly = true,
                Width = 30
            });
            return this;
        }

        public IGridViewComprobantesBuild AddMoneda() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Moneda",
                HeaderText = "Moneda",
                IsVisible = false,
                Name = "Moneda"
            });
            return this;
        }

        public IGridViewComprobantesBuild AddNota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                Width = 100
            });
            return this;
        }

        public IGridViewComprobantesBuild AddDiasTranscurridos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "DiasTranscurridos",
                FormatString = "{0:N0}",
                HeaderText = "Días Trans.",
                Name = "DiasTranscurridos"
            });
            return this;
        }

        #endregion

        #region conceptos
        public IGridViewComprobantesBuild AddIdConcepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                TextAlignment = ContentAlignment.MiddleRight,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IGridViewComprobantesBuild AddSubId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "SubId",
                HeaderText = "SubId",
                IsVisible = false,
                Name = "SubId",
                VisibleInColumnChooser = false,
            });
            return this;
        }

        public IGridViewComprobantesBuild AddNumPedido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumPedido",
                HeaderText = "# Orden",
                Name = "NumPedido",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 60
            });
            return this;
        }

        public IGridViewComprobantesBuild AddCantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = "{0:#,###0.00}",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IGridViewComprobantesBuild AddUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                Width = 80
            });
            return this;
        }

        public IGridViewComprobantesBuild AddClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "Clv. Unidad",
                Name = "ClaveUnidad",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 80
            });
            return this;
        }

        public IGridViewComprobantesBuild AddNoIdentificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoIdentificacion",
                HeaderText = "No. Ident.",
                Name = "NoIdentificacion",
                Width = 90
            });
            return this;
        }

        public IGridViewComprobantesBuild AddClaveProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveProdServ",
                HeaderText = "Clv. Producto",
                Name = "ClaveProducto",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 80
            });
            return this;
        }

        public IGridViewComprobantesBuild AddObjImpuesto() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                DisplayMember = "Id",
                FieldName = "ObjetoImp",
                HeaderText = "Obj. Imp.",
                Name = "ObjetoImp",
                TextAlignment = ContentAlignment.MiddleCenter,
                ValueMember = "Id"
            });
            return this;
        }

        public IGridViewComprobantesBuild AddDescripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Concepto",
                Name = "Descripcion",
                Width = 325
            });
            return this;
        }

        public IGridViewComprobantesBuild AddValorUnitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ValorUnitario",
                FormatString = "{0:N4}",
                HeaderText = "Unitario",
                Name = "ValorUnitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IGridViewComprobantesBuild AddImporte() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                EnableExpressionEditor = false,
                FormatString = this.FormatStringMoney,
                FieldName = "Importe",
                HeaderText = "Importe",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IGridViewComprobantesBuild AddCtaPredial() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CtaPredial",
                HeaderText = "Cta. Predial",
                Name = "CtaPredial",
                Width = 80
            });
            return this;
        }
        #endregion

        #region a cuenta de terceros
        public IGridViewComprobantesBuild RfcACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RfcACuentaTerceros",
                HeaderText = "RFC",
                Name = "RfcACuentaTerceros",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 110
            });
            return this;
        }

        public IGridViewComprobantesBuild AddNombreACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NombreACuentaTerceros",
                HeaderText = "Denominación ó Razón Social",
                Name = "NombreACuentaTerceros",
                ReadOnly = true,
                Width = 220
            });
            return this;
        }
        public IGridViewComprobantesBuild AddRegimenFiscalACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RegimenFiscalACuentaTerceros",
                HeaderText = "Rég. Fiscal",
                Name = "RegimenFiscalACuentaTerceros",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        public IGridViewComprobantesBuild AddDomicilioFiscalACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "DomicilioFiscalACuentaTerceros",
                HeaderText = "Dom. Fiscal",
                Name = "DomicilioFiscalACuentaTerceros",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }
        #endregion

        #region metodos estaticos
        public static GridViewRowInfo ConvertTo(ComprobanteFiscalConceptoView item, GridViewRowInfo newRow) {
            newRow.Cells["Status"].Value = item.Status;
            newRow.Cells["Folio"].Value = item.Folio;
            newRow.Cells["Serie"].Value = item.Serie;
            newRow.Cells["IdDocumento"].Value = item.IdDocumento;
            newRow.Cells["FechaEmision"].Value = item.FechaEmision;
            newRow.Cells["Cantidad"].Value = item.Cantidad;
            newRow.Cells["ClaveUnidad"].Value = item.ClaveUnidad;
            newRow.Cells["Unidad"].Value = item.Unidad;
            newRow.Cells["ClaveProducto"].Value = item.ClaveProdServ;
            newRow.Cells["NumPedido"].Value = item.NumPedido;
            newRow.Cells["Descripcion"].Value = item.Descripcion;
            newRow.Cells["ValorUnitario"].Value = item.ValorUnitario;
            newRow.Cells["Importe"].Value = item.Importe;
            return newRow;
        }
        #endregion
    }
}
