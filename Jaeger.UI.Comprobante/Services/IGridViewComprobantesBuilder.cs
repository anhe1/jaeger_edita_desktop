﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Comprobante.Services {
    public interface IGridViewComprobantesBuilder {
        IGridViewComprobantesTempletesBuilder Templetes();

        IGridViewComprobantesBuild Columns();
    }

    public interface IGridViewComprobantesTempletesBuilder {
        IGridViewComprobantesBuild Emitidos();
        IGridViewComprobantesBuild Recibidos();
        IGridViewComprobantesBuild Nomina();
        IGridViewComprobantesBuild Conceptos();
        IGridViewComprobantesBuild ACuentaDeTerceros();
        IGridViewComprobantesBuild Facturacionconceptos();
    }

    public interface IGridViewComprobantesBuild {
        #region comprobante
        IGridViewComprobantesBuild AddId();

        /// <summary>
        /// obtener columna para version del comprobante
        /// </summary>
        IGridViewComprobantesBuild AddVersion();

        IGridViewComprobantesBuild AddActivo();

        IGridViewComprobantesBuild AddTipoComprobante();

        IGridViewComprobantesBuild AddSubTipoText();

        IGridViewComprobantesBuild AddStatus();

        IGridViewComprobantesBuild AddFolio();

        IGridViewComprobantesBuild AddSerie();

        IGridViewComprobantesBuild AddEmisorRFC();

        IGridViewComprobantesBuild AddEmisorNombre();

        IGridViewComprobantesBuild AddReceptorRFC();

        IGridViewComprobantesBuild AddReceptorNombre();

        IGridViewComprobantesBuild AddIdDocumento();

        IGridViewComprobantesBuild AddTipoComprobanteText();

        IGridViewComprobantesBuild AddFechaEmision();

        IGridViewComprobantesBuild AddFechaTimbre();

        IGridViewComprobantesBuild AddFechaCancela();

        IGridViewComprobantesBuild AddFechaValidacion();

        IGridViewComprobantesBuild AddFechaEntrega();

        IGridViewComprobantesBuild AddFechaUltimoPago(string headerText = "Ültm. Pago");

        IGridViewComprobantesBuild AddFechaRecepcionPago();

        IGridViewComprobantesBuild AddNumParcialidad();

        IGridViewComprobantesBuild AddClaveMetodoPago();

        IGridViewComprobantesBuild AddClaveFormaPago();

        IGridViewComprobantesBuild AddSubTotal();

        IGridViewComprobantesBuild AddTrasladoIVA();

        IGridViewComprobantesBuild AddTotal1();

        IGridViewComprobantesBuild AddDescuento();

        IGridViewComprobantesBuild AddImporteNota();

        IGridViewComprobantesBuild AddAcumulado(string headerText = "Acumulado");

        IGridViewComprobantesBuild AddSaldo();

        IGridViewComprobantesBuild AddImportePagado();

        IGridViewComprobantesBuild AddSaldoPagos();

        IGridViewComprobantesBuild AddEstado();

        IGridViewComprobantesBuild AddFechaEstado();

        IGridViewComprobantesBuild AddFechaNuevo();

        IGridViewComprobantesBuild AddCreo();

        IGridViewComprobantesBuild AddModifica();

        IGridViewComprobantesBuild AddFechaModifica();

        IGridViewComprobantesBuild AddUrlFileXml();

        IGridViewComprobantesBuild AddUrlFilePdf();

        IGridViewComprobantesBuild AddMoneda();

        IGridViewComprobantesBuild AddNota();

        IGridViewComprobantesBuild AddDiasTranscurridos();

        #endregion

        #region conceptos
        IGridViewComprobantesBuild AddIdConcepto();

        IGridViewComprobantesBuild AddSubId();

        IGridViewComprobantesBuild AddNumPedido();

        IGridViewComprobantesBuild AddCantidad();

        IGridViewComprobantesBuild AddUnidad();

        IGridViewComprobantesBuild AddClaveUnidad();

        IGridViewComprobantesBuild AddNoIdentificacion();

        IGridViewComprobantesBuild AddClaveProducto();

        IGridViewComprobantesBuild AddObjImpuesto();

        IGridViewComprobantesBuild AddDescripcion();

        IGridViewComprobantesBuild AddValorUnitario();

        IGridViewComprobantesBuild AddImporte();

        IGridViewComprobantesBuild AddCtaPredial();
        #endregion

        #region a cuenta de terceros
        IGridViewComprobantesBuild RfcACuentaTerceros();

        IGridViewComprobantesBuild AddNombreACuentaTerceros();

        IGridViewComprobantesBuild AddRegimenFiscalACuentaTerceros();

        IGridViewComprobantesBuild AddDomicilioFiscalACuentaTerceros();
        #endregion

        GridViewDataColumn[] Build();
    }

    public class Class1 {
        public Class1() {
            IGridViewComprobantesBuilder d0 = new GridViewComprobantesBuilder();
            d0.Columns().AddActivo().Build();
            
        }
    }
}
