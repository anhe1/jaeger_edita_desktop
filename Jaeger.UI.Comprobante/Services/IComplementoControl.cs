﻿namespace Jaeger.UI.Comprobante.Services {
    interface IComplementoControl {
        string Caption { get; set; }

        bool Editable { get; set; }

        bool Validar();

        void Start();

        void CreateBinding();
    }
}
