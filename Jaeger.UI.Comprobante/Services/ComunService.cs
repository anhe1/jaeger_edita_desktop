﻿using System;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.Collections.Generic;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Comprobante.Services {
    public static class ComunService {
        public static void SetConceptoImpuestos(this RadGridView grid) {
            // impuestos de los conceptos
            GridViewComboBoxColumn comboTipo = (GridViewComboBoxColumn)grid.MasterTemplate.Columns["Tipo"];
            comboTipo.DataSource = Enum.GetNames(typeof(TipoImpuestoEnum));
            GridViewComboBoxColumn comboImpuesto = (GridViewComboBoxColumn)grid.MasterTemplate.Columns["Impuesto"];
            comboImpuesto.DataSource = Enum.GetNames(typeof(ImpuestoEnum));
            GridViewComboBoxColumn comboFactor = (GridViewComboBoxColumn)grid.MasterTemplate.Columns["TipoFactor"];
            comboFactor.DataSource = Enum.GetNames(typeof(FactorEnum));
        }

        public static void SetComboBoxConceptos(this RadMultiColumnComboBox comboBoxConceptos) {
            comboBoxConceptos.AutoFilter = true;
            comboBoxConceptos.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            comboBoxConceptos.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            comboBoxConceptos.AutoSizeDropDownToBestFit = true;
            comboBoxConceptos.DisplayMember = "Descripcion";
            comboBoxConceptos.ValueMember = "ClaveProdServ";
        }

        public static string ValidacionPDF(ComprobanteValidacionPrinter printer, string localFileName) {
            var horizon = new Util.EmbeddedResources("Jaeger.Domain.Comprobante");
            var pdf = new Common.Services.HelperReportToPDF {
                LocalFileName = localFileName,
                ReportDefinition = horizon.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobanteValidacion35Report.rdlc")
            };
            pdf.SetDisplayName("Reporte Validación");
            var d = DbConvert.ConvertToDataTable<ComprobanteValidacionPrinter>(new List<ComprobanteValidacionPrinter>() { printer });
            pdf.SetDataSource("Validacion", d);
            var d1 = DbConvert.ConvertToDataTable<PropertyValidate>(printer.Resultados);
            pdf.SetDataSource("Resultados", d1);
            pdf.Procesar();
            return pdf.LocalFileName;
        }

        public static string ComprobanteFiscalReporte(ComprobanteFiscalPrinter current, string localFileName, string pathLogo) {
            var horizon = new Util.EmbeddedResources("Jaeger.Domain.Comprobante");

            var pdf = new Common.Services.HelperReportToPDF {
                LocalFileName = localFileName,
                ReportDefinition = horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0Reporte.rdlc")
            };
            pdf.PathLogo = pathLogo;
            pdf.SetDisplayName("Comprobante");
            
            if (current.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                pdf.ReportDefinition = horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0PagoReporte.rdlc");
                var p1 = DbConvert.ConvertToDataTable<ComplementoPagoDetailModel>(current.RecepcionPago);
                var docs = DbConvert.ConvertToDataTable<PagosPagoDoctoRelacionadoDetailModel>(current.RecepcionPago[0].DoctoRelacionados);
                pdf.SetDataSource("Pago", p1);
                pdf.SetDataSource("DoctoRelacionado", docs);
            } else if (current.TipoComprobante == CFDITipoComprobanteEnum.Nomina) {

            } else if ((current.TipoComprobante == CFDITipoComprobanteEnum.Ingreso | current.TipoComprobante == CFDITipoComprobanteEnum.Egreso) && current.InformacionGlobal == null) {
                pdf.ReportDefinition = horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0Reporte.rdlc");
            } else if (current.TipoComprobante == CFDITipoComprobanteEnum.Ingreso && current.InformacionGlobal != null) {
                pdf.ReportDefinition = horizon.GetStream("Jaeger.Domain.Comprobante.Reports.CFDIv0GlobalReporte.rdlc");
                var g = DbConvert.ConvertToDataTable<ComprobanteInformacionGlobalDetailModel>(new List<ComprobanteInformacionGlobalDetailModel>() { current.InformacionGlobal });
                pdf.SetDataSource("InformacionGlobal", g);
            }
            var d1 = DbConvert.ConvertToDataTable<ComprobanteFiscalPrinter>(new List<ComprobanteFiscalPrinter>() { current });
            var d = DbConvert.ConvertToDataTable<ComprobanteConceptoDetailModel>(current.Conceptos);
            var d3 = DbConvert.ConvertToDataTable<ComplementoTimbreFiscal>(new List<ComplementoTimbreFiscal>() { current.TimbreFiscal });

            
            pdf.SetDataSource("Comprobante", d1);
            pdf.SetDataSource("Conceptos", d);
            pdf.SetDataSource("TimbreFiscal", d3);

            if (current.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {

            }
            pdf.Procesar();
            return pdf.LocalFileName;
        }
    }
}
