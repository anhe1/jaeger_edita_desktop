﻿using Jaeger.Catalogos.Entities;
using System.ComponentModel;

namespace Jaeger.UI.Comprobante {
    public interface IMonedaService {
        BindingList<ClaveMoneda> GetMonedas();
    }
}
