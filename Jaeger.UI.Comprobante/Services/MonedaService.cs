﻿using Jaeger.Catalogos.Entities;
using System.ComponentModel;

namespace Jaeger.UI.Comprobante {
    public class MonedaService : IMonedaService {
        public BindingList<ClaveMoneda> GetMonedas() {
            return new BindingList<ClaveMoneda> {
                new ClaveMoneda { Clave = "MXN", Descripcion = "Peso Mexicano", Decimales = 2 },
                new ClaveMoneda { Clave = "USD", Descripcion = "Dolar americano", Decimales = 2 },
                new ClaveMoneda { Clave = "XXX", Descripcion = "Los códigos asignados para las transacciones en que intervenga ninguna moneda", Decimales = 0 }};
        }
    }
}
