﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Jaeger.UI.Common.Builder;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Comprobante.Builder {
    public class ComprobanteFiscalGridBuilder : GridViewBuilder, IComprobanteFiscalGridBuilder, IComprobanteFiscalColumnsGridBuilder, IComprobanteFiscalTempleteGridBuilder, IGridViewColumnsBuild,
        IGridViewTempleteBuild, IComprobanteFiscalTempletesGridBuilder, IComprobanteFiscalRecibidoGridBuilder, IComprobanteFiscalEmitidoGridBuilder {

        public enum TempleteEnum {
            None,
            Standar,
            ComplementoNomina,
            ComplementoPago,
            EdoCuenta
        }

        private TempleteEnum m_templeteEnum;

        public ComprobanteFiscalGridBuilder() : base() { }

        public IComprobanteFiscalTempletesGridBuilder Templetes(TempleteEnum templete = TempleteEnum.Standar) {
            this.m_templeteEnum = templete;
            this._Columns.Clear();
            return this;
        }

        public IComprobanteFiscalRecibidoGridBuilder Recibido() {
            this._Columns.Clear();
            if (this.m_templeteEnum == TempleteEnum.EdoCuenta) {
                this.IdDirectorio().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre(false).FechaEmision().FechaUltimoPago().TotalRetencionISR().TotalRetencionIEPS().TotalRetencionIVA().TotalTrasladoIEPS().TotalTrasladoIVA()
                    .SubTotal(true).TotalDescuento().GTotal().Acumulado("Pagado").Tipocomprobante().Saldo();
            } else if (m_templeteEnum == TempleteEnum.ComplementoPago) {
                this.IdComprobante().Tipocomprobante().SubTipoText().Status().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre(false).IdDocumento().TipoComprobanteText().FechaEmision().FechaTimbre().FechaCancela().Estado().FechaEstado();
                this._Columns.AddRange(new ComplementoPagoGridBuilder().FechaPago().FormaDePagoP().MonedaP().Monto().Build());
                this.UrlFileXml().UrlFilePdf();
            } else if (m_templeteEnum == TempleteEnum.ComplementoNomina) {

            } else if (m_templeteEnum == TempleteEnum.Standar) {
                this.IdComprobante().Version().CheckActivo().Tipocomprobante().SubTipoText().Status().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre(false).IdDocumento().TipoComprobanteText().FechaEmision()
                    .FechaTimbre().FechaCancela().FechaValidacion().FechaEntrega().FechaUltimoPago().FechaRecepcionPago().NumParcialidad().ClaveMetodoPago().ClaveFormaPago().SubTotal().TotalTrasladoIVA().GTotal().TotalDescuento()
                    .ImporteNota().Acumulado("Pagado").Saldo().ImportePagado().SaldoPagos().Estado().FechaEstado().FechaNuevo().Creo().Modifica().FechaMod().UrlFileXml().UrlFilePdf().Moneda().Nota().DiasTranscurridos();
            }
            return this;
        }

        public IComprobanteFiscalEmitidoGridBuilder Emitido() {
            this._Columns.Clear();
            if (this.m_templeteEnum == TempleteEnum.EdoCuenta) {
                this.IdDirectorio().ReceptorRFC().ReceptorNombre().EmisorRFC().EmisorNombre(false).FechaEmision().FechaUltimoPago().TotalRetencionISR().TotalRetencionIEPS().TotalRetencionIVA().TotalTrasladoIEPS().TotalTrasladoIVA()
                    .SubTotal(true).TotalDescuento().GTotal().Acumulado("Cobrado").Tipocomprobante().Saldo();
            } else if (m_templeteEnum == TempleteEnum.ComplementoPago) {
                var d0 = new ComplementoPagoGridBuilder().FechaPago().FormaDePagoP().MonedaP().Monto().Build();
                this.IdComprobante().Tipocomprobante().SubTipoText().Status().Folio().Serie().ReceptorRFC().ReceptorNombre().EmisorRFC().EmisorNombre(false).IdDocumento().TipoComprobanteText().FechaEmision().FechaTimbre().FechaCancela().Estado().FechaEstado();
                this._Columns.AddRange(d0);
                this.UrlFileXml().UrlFilePdf();
            } else if (m_templeteEnum == TempleteEnum.ComplementoNomina) {

            } else if (m_templeteEnum == TempleteEnum.Standar) {
                this.IdComprobante().Version().CheckActivo().Tipocomprobante().SubTipoText().Status().Folio().Serie().ReceptorRFC().ReceptorNombre().EmisorRFC().EmisorNombre(false).IdDocumento().TipoComprobanteText().FechaEmision()
                    .FechaTimbre().FechaCancela().FechaValidacion().FechaEntrega().FechaUltimoPago().FechaRecepcionPago().NumParcialidad().ClaveMetodoPago().ClaveFormaPago().SubTotal().TotalTrasladoIVA().GTotal().TotalDescuento()
                    .ImporteNota().Acumulado("Cobrado").Saldo().ImportePagado().SaldoPagos().Estado().FechaEstado().FechaNuevo().Creo().Modifica().FechaMod().UrlFileXml().UrlFilePdf().Moneda().Nota().DiasTranscurridos();
            }
            return this;
        }

        #region templetes
        public IComprobanteFiscalTempleteGridBuilder Templetes() {
            return this;
        }

        public IComprobanteFiscalTempleteGridBuilder Master() {
            this._Columns.Clear();
            this.IdComprobante().Version().CheckActivo().Tipocomprobante().SubTipoText().Status().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre().IdDocumento().TipoComprobanteText().FechaEmision().
            FechaTimbre().FechaCancela().FechaValidacion().FechaEntrega().FechaUltimoPago().FechaRecepcionPago().NumParcialidad().ClaveMetodoPago().ClaveFormaPago()
            .SubTotal().TotalRetencionISR().TotalRetencionIEPS().TotalRetencionIVA().TotalTrasladoIEPS().TotalTrasladoIVA().GTotal().TotalDescuento().
            ImporteNota().Acumulado().Saldo().ImportePagado().SaldoPagos().Estado().FechaEstado().FechaNuevo().Creo().Modifica().FechaMod().UrlFileXml().UrlFilePdf().Moneda().Nota().DiasTranscurridos();
            return this;
        }

        /// <summary>
        /// vista simple de comprobantes
        /// </summary>
        public IComprobanteFiscalTempleteGridBuilder Single() {
            this._Columns.Clear();
            this.IdComprobante().SubTipo().Version().Tipocomprobante().Status().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre().IdDocumento().TipoComprobanteText().FechaEmision()
            .FechaCancela().FechaUltimoPago().DiasTranscurridos(true).GTotal().Acumulado().Saldo().Estado().UrlFileXml().UrlFilePdf().Moneda();
            return this;
        }

        public IComprobanteFiscalTempleteGridBuilder EdoCuenta() {
            this._Columns.Clear();
            this.IdDirectorio().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre(false).FechaEmision().FechaUltimoPago().TotalRetencionISR().TotalRetencionIEPS().TotalRetencionIVA().TotalTrasladoIEPS()
                .TotalTrasladoIVA().SubTotal(true).TotalDescuento().GTotal().Pagado().Tipocomprobante().Saldo();
            return this;
        }

        public IComprobanteFiscalTempleteGridBuilder Conceptos() {
            this._Columns.Clear();
            this._Columns.AddRange(new ConceptoGridBuilder().Master().Build());
            return this;
        }

        public IComprobanteFiscalTempleteGridBuilder ConceptosParte() {
            this._Columns.Clear();
            this._Columns.AddRange(new ConceptoGridBuilder().ConceptoParte().Build());
            return this;
        }

        /// <summary>
        /// master de factura y conceptos
        /// </summary>
        public IComprobanteFiscalTempleteGridBuilder MasterYConceptos() {
            this._Columns.Clear();
            this.Version().Tipocomprobante().SubTipoText().Status().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre().IdDocumento(false).TipoComprobanteText().FechaEmision()
            .FechaTimbre().FechaUltimoPago().NumParcialidad().ClaveMetodoPago().ClaveFormaPago();
            this._Columns.AddRange(new ConceptoGridBuilder().NumPedido().Cantidad().Unidad().ClaveUnidad().NoIdentificacion().ClaveProducto().ObjetoImp().Descripcion().ValorUnitario().Importe().Descuento().SubTotal(true).Build());
            return this;
        }

        /// <summary>
        /// templete para CFDI Relaciondo, solo tiene la clave de relacion que se carga con un metodo estatico
        /// </summary>
        public IComprobanteFiscalTempleteGridBuilder CFDIRelaciondos() {
            this._Columns.Clear();
            this.ClaveRelacion();
            return this;
        }

        /// <summary>
        /// templete de documentos relacionados del grid CFDI Relacionado
        /// </summary>
        public IComprobanteFiscalTempleteGridBuilder CFDIRelaciondosDoctos() {
            this._Columns.Clear();
            this.IdDocumento().EmisorRFC().EmisorNombre().Serie().Folio().GTotal().TipoComprobanteText().Tipocomprobante();
            return this;
        }

        /// <summary>
        /// templete del complemento de pagos
        /// </summary>
        public IComprobanteFiscalTempleteGridBuilder ComplementoPago() {
            this._Columns.Clear();
            this._Columns.AddRange(new ComplementoPagoGridBuilder().Templetes().Master().Build());
            return this;
        }

        //complemento de pago
        public IComprobanteFiscalTempleteGridBuilder DocumentoRelacionado() {
            this._Columns.Clear();
            this._Columns.AddRange(new ComplementoPagoGridBuilder().Templetes().DoctoRelacionados().Build());
            return this;
        }

        /// <summary>
        /// complemento de pago con informacion del comprobante
        /// </summary>
        public IComprobanteFiscalTempleteGridBuilder ComplementoPagoD() {
            this._Columns.Clear();
            var p = new ComplementoPagoGridBuilder();
            var d = new ComplementoPagoDoctoRelacionadoBuilder();
            this.Status().Folio().Serie().EmisorRFC().EmisorNombre().IdDocumento().FechaEmision();
            this._Columns.AddRange(p.FechaPago().FormaDePagoP().MonedaP().TipoCambioP().NumOperacion().Build());
            this._Columns.AddRange(d.NumParcialidad().ImpSaldoAnt().ImpPagado().ImpSaldoInsoluto().IdComprobanteR().Build());
            this.Estado();
            this.PrintColumns();
            return this;
        }

        public IComprobanteFiscalTempleteGridBuilder Impuestos() {
            this._Columns.Clear();
            this._Columns.AddRange(new ImpuestosGridBuilder().Master().Build());
            return this;
        }

        public IComprobanteFiscalTempleteGridBuilder ComprobanteVsComplementoPago() {
            this._Columns.Clear();
            this.IdComprobante().Version().Tipocomprobante().SubTipoText().Status().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre().IdDocumento().TipoComprobanteText().FechaEmision().
            FechaCancela().ClaveMetodoPago(true).ClaveFormaPago().GTotal();
            var complementoPago = new ComplementoPagoGridBuilder().Columns().IdDocumentoPG().FechaPago().FormaDePagoP().MonedaP().TipoCambioP(false).Monto().NumOperacion().RfcEmisorCtaOrd().NomBancoOrdExt().CtaOrdenante().RfcEmisorCtaBen()
                .CtaBeneficiario().Build();
            this._Columns.AddRange(complementoPago);
            var doctoRelacionado = new ComplementoPagoDoctoRelacionadoBuilder().Columns().NumParcialidad().ImpSaldoAnt().ImpPagado().ImpSaldoInsoluto().Build();
            this._Columns.AddRange(doctoRelacionado);
            this.Estado().UrlFileXml().UrlFilePdf();
            return this;
        }

        /// <summary>
        /// templete builder para conceptos
        /// </summary>
        /// <returns>Builder templete</returns>
        public IConceptoGridBuilder Concepto() {
            return new ConceptoGridBuilder();
        }
        #endregion

        #region columnas del comprobantes
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder IdComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdComprobante",
                HeaderText = "Id",
                IsVisible = false,
                Name = "IdComprobante",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer version del estandar bajo el que se encuentra expresado el comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder Version() {
            this._Columns.Add(new GridViewTextBoxColumn {
                ExcelExportType = DisplayFormatType.Text,
                FieldName = "Version",
                HeaderText = "Ver.",
                IsVisible = false,
                Name = "Version",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
            });
            return this;
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        public new IComprobanteFiscalColumnsGridBuilder CheckActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                IsVisible = false,
                Name = "Activo",
                ReadOnly = true,
                VisibleInColumnChooser = false,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Tipocomprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "TipoComprobante",
                HeaderText = "TipoComprobante",
                IsVisible = false,
                Name = "TipoComprobante",
                VisibleInColumnChooser = false,
            });
            return this;
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder TipoComprobanteText() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoComprobanteText",
                HeaderText = "Tipo",
                Name = "TipoComprobanteText",
                ReadOnly = true,
                WrapText = true,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder SubTipoText() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "SubTipoText",
                HeaderText = "Tipo",
                IsVisible = false,
                Name = "SubTipoText",
                ReadOnly = true,
            });
            return this;
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder SubTipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdSubTipo",
                HeaderText = "IdSubTipo",
                IsVisible = false,
                Name = "IdSubTipo",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        /// <summary>
        /// obtner o establecer status interno del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder Status() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Status",
                HeaderText = "Status",
                Name = "Status",
                Width = 80,
                ExcelExportType = DisplayFormatType.Text
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                ReadOnly = true,
                Width = 65,
                MaxLength = 40
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                ReadOnly = true,
                Width = 65
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder IdDirectorio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdDirectorio",
                HeaderText = "IdDirectorio",
                IsVisible = false,
                Name = "IdDirectorio",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder EmisorRFC(string headerText = "RFC Emisor") {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "EmisorRFC",
                HeaderText = "RFC Emisor",
                Name = "EmisorRFC",
                ReadOnly = true,
                Width = 90,
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder EmisorNombre(bool isVisible = true) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "EmisorNombre",
                HeaderText = "Nombre del Emisor",
                Name = "EmisorNombre",
                ReadOnly = true,
                Width = 180,
                IsVisible = isVisible
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder EmisorNombre(string headerText = "Nombre del Emisor") {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "EmisorNombre",
                HeaderText = headerText,
                Name = "EmisorNombre",
                ReadOnly = true,
                Width = 180,
                IsVisible = true
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder EmisorNombre(string headerText = "Nombre del Emisor", bool isVisible = true) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "EmisorNombre",
                HeaderText = headerText,
                Name = "EmisorNombre",
                ReadOnly = true,
                Width = 180,
                IsVisible = isVisible
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder ReceptorRFC(string headerText = "RFC Receptor") {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ReceptorRFC",
                HeaderText = headerText,
                Name = "ReceptorRFC",
                ReadOnly = true,
                Width = 90,
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder ReceptorNombre(bool isVisible = true) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ReceptorNombre",
                HeaderText = "Nombre del Receptor",
                Name = "ReceptorNombre",
                ReadOnly = true,
                Width = 220,
                IsVisible = isVisible
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder ReceptorNombre(string header = "Nombre del Receptor", bool isVisible = true) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ReceptorNombre",
                HeaderText = header,
                Name = "ReceptorNombre",
                ReadOnly = true,
                Width = 220,
                IsVisible = isVisible
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder ReceptorCodigoPostal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "DomicilioFiscal",
                HeaderText = "Codigo\r\nPostal",
                Name = "DomicilioFiscal",
                ReadOnly = true,
                Width = 90,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder IdDocumento(bool isVisible = true) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "IdDocumento",
                HeaderText = "Folio Fiscal (uuid)",
                Name = "IdDocumento",
                ReadOnly = true,
                Width = 180,
                IsVisible = isVisible
            });
            return this;
        }

        /// <summary>
        /// fecha de emision del comprobante
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEmision",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Emisión",
                Name = "FechaEmision",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaTimbre() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaTimbre",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Certificación",
                Name = "FechaTimbre",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaCancela() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaCancela",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Cancelación",
                Name = "FechaCancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaValidacion() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaValidacion",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Validación",
                Name = "FechaValidacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaEntrega() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEntrega",
                Format = System.Windows.Forms.DateTimePickerFormat.Short,
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Entrega",
                Name = "FechaEntrega",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaUltimoPago() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaUltimoPago",
                Format = System.Windows.Forms.DateTimePickerFormat.Short,
                FormatString = this.FormatStringDate,
                HeaderText = "Ult. Pago",
                Name = "FechaUltimoPago",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaRecepcionPago() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaRecepcionPago",
                FormatString = this.FormatStringDate,
                HeaderText = "C. Fecha Pago",
                Name = "FechaRecepcionPago",
                Width = 70,
                TextAlignment = ContentAlignment.MiddleCenter,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder NumParcialidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "NumParcialidad",
                HeaderText = "Núm. Par.",
                IsVisible = false,
                Name = "NumParcialidad",
                WrapText = true,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder ClaveMetodoPago(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ClaveMetodoPago",
                HeaderText = "Método\r\nde Pago",
                IsVisible = isVisible,
                Name = "ClaveMetodoPago",
                ReadOnly = true,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder ClaveFormaPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ClaveFormaPago",
                HeaderText = "Forma\r\nde Pago",
                Name = "ClaveFormaPago",
                ReadOnly = true,
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder ClaveExportacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "ClaveExportacion",
                HeaderText = "Cv. \r\nExportación",
                Name = "ClaveExportacion",
                ReadOnly = true,
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// Lugar de expedicion
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder LugarExpedicion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "LugarExpedicion",
                HeaderText = "Expedición",
                Name = "LugarExpedicion",
                ReadOnly = true,
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder SubTotal(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = this.FormatStringMoney,
                HeaderText = "SubTotal",
                IsVisible = isVisible,
                Name = "SubTotal",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalRetencionISR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionISR",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Ret. ISR",
                Name = "RetencionISR",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalRetencionIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionIVA",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Ret. IVA",
                Name = "RetencionIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = this.FormatStringMoney,
                HeaderText = "IVA",
                Name = "TrasladoIVA",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalTrasladoIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIEPS",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Traslado IEPS",
                Name = "TrasladoIEPS",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalRetencionIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionIEPS",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Ret. IEPS",
                Name = "RetencionIEPS",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder GTotal() {
            var gTotal = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = this.FormatStringMoney,
                HeaderText = "Total",
                Name = "Total",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            };
            // formato condicional
            var conditional = gTotal.ConditionalFormattingObjectList;
            conditional.Add(ConditionalEgreso());
            this._Columns.Add(gTotal);
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalDescuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = this.FormatStringMoney,
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 95,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder ImporteNota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImporteNota",
                FormatString = this.FormatStringMoney,
                HeaderText = "Imp. Nota",
                IsVisible = false,
                Name = "ImporteNota",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        /// <summary>
        /// acumulados
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder Acumulado(string hedearText = "Cobrado") {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "Acumulado",
                FormatString = this.FormatStringMoney,
                HeaderText = hedearText,
                Name = "Acumulado",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Cobrado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "Acumulado",
                FormatString = this.FormatStringMoney,
                HeaderText = "Cobrado",
                Name = "Acumulado",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Pagado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "Acumulado",
                FormatString = this.FormatStringMoney,
                HeaderText = "Pagado",
                Name = "Acumulado",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Saldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Saldo",
                FormatString = this.FormatStringMoney,
                HeaderText = "Saldo",
                Name = "Saldo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder ImportePagado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ImportePagado",
                FormatString = this.FormatStringMoney,
                HeaderText = "C. Pagos",
                Name = "ImportePagado",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder SaldoPagos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "SaldoPagos",
                FormatString = this.FormatStringMoney,
                HeaderText = "C. Saldo",
                Name = "SaldoPagos",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Estado() {
            var estado = new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                Name = "Estado",
                Width = 75,
                ReadOnly = true
            };
            // se agrega el formato condicional para el estado del comprobante
            var conditional = estado.ConditionalFormattingObjectList;
            conditional.Add(ConditionalEstado());
            conditional.Add(ConditionalVigente());
            this._Columns.Add(estado);
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEstado",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha del Estado",
                IsVisible = false,
                Name = "FechaEstado",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Sistema",
                IsVisible = false,
                Name = "FechaNuevo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                ExcelExportType = DisplayFormatType.ShortDate
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                IsVisible = false,
                Name = "Creo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Modifica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Modifica",
                HeaderText = "Mod.",
                IsVisible = false,
                Name = "Modifica",
                ReadOnly = true,
                Width = 75,
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder FechaMod() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaMod",
                Format = System.Windows.Forms.DateTimePickerFormat.Short,
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Modificación",
                IsVisible = false,
                Name = "FechaMod",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder UrlFileXml() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFileXml",
                HeaderText = "XML",
                Name = "UrlFileXml",
                ReadOnly = true,
                Width = 30
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder UrlFilePdf() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UrlFilePdf",
                HeaderText = "PDF",
                Name = "UrlFilePdf",
                ReadOnly = true,
                Width = 30
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Moneda() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Moneda",
                HeaderText = "Moneda",
                IsVisible = false,
                Name = "Moneda"
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                Width = 100
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder DiasTranscurridos(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "DiasTranscurridos",
                FormatString = "{0:N0}",
                HeaderText = "Días\r\nTrans.",
                Name = "DiasTranscurridos",
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = isVisible
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalPecepcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalPecepcion",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Percepciones",
                Name = "TotalPecepcion",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IComprobanteFiscalColumnsGridBuilder TotalDeduccion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalDeduccion",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Deducciones",
                Name = "TotalDeduccion",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }
        #endregion

        #region CFDI Relacionados
        /// <summary>
        /// clave de relacion de documentos relacionados (CFDI Relacionado)
        /// </summary>
        public IComprobanteFiscalColumnsGridBuilder ClaveRelacion() {
            var clave = new GridViewComboBoxColumn {
                FieldName = "Clave",
                HeaderText = "Tipo de relación",
                Name = "Clave",
                Width = 250,
                DisplayMember = "Descriptor",
                ValueMember = "Id"
            };
            clave.DataSource = ComprobanteFiscalService.GetCFDIRelaciones();
            this._Columns.Add(clave);
            return this;
        }
        #endregion

        #region impuestos

        #endregion

        #region formato condicional
        /// <summary>
        /// formato condicional para el estado del comprobante
        /// </summary>
        /// <returns>ExpressionFormattingObject</returns>
        public static ExpressionFormattingObject ConditionalEstado() {
            return new ExpressionFormattingObject("Estado del Comprobante", "Estado = 'NoEncontrado' OR Estado = 'Cancelado'", true) {
                RowForeColor = Color.DarkGray
            };
        }

        /// <summary>
        /// formato condicional para el estado del comprobante
        /// </summary>
        /// <returns>ExpressionFormattingObject</returns>
        public static ExpressionFormattingObject ConditionalVigente() {
            return new ExpressionFormattingObject("Vigente", "Estado = 'Vigente'", false) {
                CellForeColor = Color.Green
            };
        }

        /// <summary>
        /// formato condicional para el estado del comprobante
        /// </summary>
        /// <returns>ExpressionFormattingObject</returns>
        public static ExpressionFormattingObject ConditionalEgreso() {
            return new ExpressionFormattingObject("Egreso", "TipoComprobante = 1", false) {
                CellForeColor = Color.Red
            };
        }
        #endregion
    }
}
