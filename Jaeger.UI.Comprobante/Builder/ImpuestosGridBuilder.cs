﻿using Jaeger.UI.Common.Builder;
using System.Drawing;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Comprobante.Builder {
    public class ImpuestosGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IImpuestosGridBuilder, IImpuestosColumnsGridBuilder {
        public IImpuestosTempleteGridBuilder Templetes() {
            return this;
        }

        public IImpuestosTempleteGridBuilder Master() {
            this._Columns.Clear();
            this.Tipo().Impuesto().Base().TipoFactor().TasaOCuota().Importe();
            return this;
        }

        public IImpuestosColumnsGridBuilder Tipo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 80
            });
            return this;
        }

        public IImpuestosColumnsGridBuilder Impuesto() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Impuesto",
                HeaderText = "Impuesto",
                Name = "Impuesto",
                Width = 80
            });
            return this;
        }

        public IImpuestosColumnsGridBuilder Base() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Base",
                FormatString = "{0:#,###0.00}",
                HeaderText = "Base",
                Name = "Base",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IImpuestosColumnsGridBuilder TipoFactor() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "TipoFactor",
                HeaderText = "Tipo Factor",
                Name = "TipoFactor",
                Width = 80
            });
            return this;
        }

        public IImpuestosColumnsGridBuilder TasaOCuota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TasaOCuota",
                FormatString = "{0:#,###0.0000}",
                HeaderText = "Tasa ó Cuota",
                Name = "TasaOCuota",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IImpuestosColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = "{0:N2}",
                HeaderText = "Importe",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }
    }
}
