﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    public class ValidadorGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IValidadorGridBuilder, IValidadorColumnsGridBuilder, IValidadorTempleteGridBuilder,
        IDisposable {

        public ValidadorGridBuilder() : base() { }

        #region templetes
        public IValidadorTempleteGridBuilder Templetes() {
            return this;
        }

        public IValidadorTempleteGridBuilder Master() {
            this._Columns.Clear();
            this.Id().TipoComprobante().Version().MetaData().Folio().Serie().EmisorRFC().EmisorNombre().ReceptorRFC().ReceptorNombre().FechaEmision().FechaTimbre().IdDocumento().NoCertificado().RFCProvCertif()
                .Estado().LugarExpedicion().UsoCFDI().MetodoPago().FormaPago().MetodoVsForma().SubTotal().Descuento().RetencionISR().RetencionIVA().RetencionIEPS().TrasladoIVA().TrasladoIEPS().Total().PathXML()
                .PathPDF().Situacion().ClaveUnidad().ClaveConceptos().Complementos().Resultado().Registrado();
            return this;
        }

        public IConceptoGridBuilder Conceptos() {
            return new ConceptoGridBuilder();
        }
        #endregion

        #region columnas
        public IValidadorColumnsGridBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IValidadorColumnsGridBuilder TipoComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoComprobante",
                HeaderText = "Tipo Comprobante",
                Name = "TipoComprobante"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Version() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Version",
                HeaderText = "Versión",
                IsVisible = false,
                Name = "Version"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder MetaData() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetaData",
                HeaderText = "Doc.",
                IsVisible = false,
                Name = "MetaData"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder EmisorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorRFC",
                HeaderText = "Emisor (RFC)",
                Name = "EmisorRFC",
                Width = 85
            });
            return this;
        }

        public IValidadorColumnsGridBuilder EmisorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorNombre",
                HeaderText = "Emisor",
                Name = "EmisorNombre",
                Width = 220
            });
            return this;
        }

        public IValidadorColumnsGridBuilder ReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "Receptor (RFC)",
                Name = "ReceptorRFC",
                Width = 85
            });
            return this;
        }

        public IValidadorColumnsGridBuilder ReceptorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Receptor",
                Name = "ReceptorNombre",
                Width = 220
            });
            return this;
        }

        public IValidadorColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEmision",
                FormatString = "{0:d}",
                HeaderText = "Fecha",
                Name = "FechaEmision",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IValidadorColumnsGridBuilder FechaTimbre() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaTimbre",
                FormatString = "{0:d}",
                HeaderText = "Fec. Certifica",
                Name = "FechaTimbre",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public IValidadorColumnsGridBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "UUID",
                Name = "IdDocumento",
                Width = 240
            });
            return this;
        }

        public IValidadorColumnsGridBuilder NoCertificado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoCertificado",
                HeaderText = "No. Certificado",
                IsVisible = false,
                Name = "NoCertificado",
                Width = 95
            });
            return this;
        }

        public IValidadorColumnsGridBuilder RFCProvCertif() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFCProvCertif",
                HeaderText = "Prov. Certif. (RFC)",
                IsVisible = false,
                Name = "RFCProvCertif",
                Width = 90
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Estado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado SAT",
                Name = "EstadoSAT",
                Width = 65
            });
            return this;
        }

        public IValidadorColumnsGridBuilder LugarExpedicion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "LugarExpedicion",
                HeaderText = "Expedición",
                IsVisible = false,
                Name = "LugarExpedicion"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder UsoCFDI() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "UsoCFDI",
                HeaderText = "Uso CFDI",
                Name = "ClaveUsoCFDI"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder MetodoPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetodoPago",
                HeaderText = "Método Pago",
                Name = "MetodoPago"
            });
            return this;
        }

        public IValidadorColumnsGridBuilder FormaPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FormaPago",
                HeaderText = "Forma de Pago",
                Name = "FormaPago",
                WrapText = true
            });
            return this;
        }

        public IValidadorColumnsGridBuilder MetodoVsForma() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetodoVsForma",
                HeaderText = "Met.Vs Pago",
                Name = "MetodoVsForma",
                Width = 85
            });
            return this;
        }

        public IValidadorColumnsGridBuilder SubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = "{0:n}",
                HeaderText = "SubTotal",
                Name = "SubTotal",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Descuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:n}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IValidadorColumnsGridBuilder RetencionISR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionISR",
                FormatString = "{0:n}",
                HeaderText = "ISR Ret.",
                Name = "RetencionISR",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IValidadorColumnsGridBuilder RetencionIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionIVA",
                FormatString = "{0:n}",
                HeaderText = "IVA Ret.",
                Name = "RetencionIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IValidadorColumnsGridBuilder RetencionIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "RetencionIEPS",
                FormatString = "{0:n}",
                HeaderText = "IEPS Ret.",
                Name = "RetencionIEPS",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }


        public IValidadorColumnsGridBuilder TrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = "{0:n}",
                HeaderText = "IVA Tras.",
                Name = "TrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }


        public IValidadorColumnsGridBuilder TrasladoIEPS() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIEPS",
                FormatString = "{0:n}",
                HeaderText = "IEPS Tras.",
                Name = "TrasladoIEPS",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }


        public IValidadorColumnsGridBuilder Total() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }


        public IValidadorColumnsGridBuilder PathXML() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "PathXML",
                HeaderText = "XML",
                Name = "XML",
                Width = 30
            });
            return this;
        }

        public IValidadorColumnsGridBuilder PathPDF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "PathPDF",
                HeaderText = "PDF",
                Name = "PDF",
                Width = 30
            });
            return this;
        }


        public IValidadorColumnsGridBuilder Situacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Situacion",
                HeaderText = "Situación",
                Name = "Situacion",
                Width = 85
            });
            return this;
        }


        public IValidadorColumnsGridBuilder ClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "Clave Unidad",
                Name = "ClaveUnidad",
                Width = 150
            });
            return this;
        }


        public IValidadorColumnsGridBuilder ClaveConceptos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveConceptos",
                HeaderText = "Clave Conceptos",
                Multiline = true,
                Name = "ClaveConceptos",
                Width = 250
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Complementos() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Complementos",
                HeaderText = "Complementos",
                IsVisible = false,
                Name = "Complementos"
            });
            return this;
        }


        public IValidadorColumnsGridBuilder Resultado() {
            this._Columns.Add(new GridViewCommandColumn {
                FieldName = "Resultado",
                HeaderText = "Resultado",
                IsPinned = true,
                Name = "Resultado",
                PinPosition = PinnedColumnPosition.Right,
                Width = 85
            });
            return this;
        }

        public IValidadorColumnsGridBuilder Registrado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Registrado",
                HeaderText = "Registrado",
                IsVisible = false,
                Name = "Registrado",
                VisibleInColumnChooser = false
            });
            return this;
        }
        #endregion
    }
}
