﻿using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    public class ConceptoGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IConceptoGridBuilder, IConceptoTempletesGridBuilder,
        IConceptoColumnsGridBuilder, IConceptoParteTempletesGridBuilder {
        public ConceptoGridBuilder() : base() { }

        public IConceptoTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.NumPedido().Cantidad().Unidad().ClaveUnidad().NoIdentificacion().ClaveProducto().ObjetoImp().Descripcion().ValorUnitario().Descuento().Importe().SubTotal();
            return this;
        }

        public IConceptoParteTempletesGridBuilder ConceptoParte() {
            this._Columns.Clear();
            this.Cantidad().Unidad().ClaveUnidad().NoIdentificacion().ClaveProducto().Descripcion().ValorUnitario().Importe();
            return this;
        }

        #region columnas
        /// <summary>
        /// CheckBoxColumna para registro activo
        /// </summary>
        public new IConceptoColumnsGridBuilder CheckActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }

        /// <summary>
        /// asignar numero de pedido 
        /// </summary>
        public IConceptoColumnsGridBuilder NumPedido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "NumPedido",
                HeaderText = "# Orden",
                Name = "NumPedido",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 60
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la cantidad de bienes o servicios del tipo particular definido por el presente concepto
        /// </summary>
        public IConceptoColumnsGridBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = "{0:n}",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 70
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        public IConceptoColumnsGridBuilder ClaveUnidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveUnidad",
                HeaderText = "Clv. Unidad",
                Name = "ClaveUnidad",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        public IConceptoColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                Width = 65
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer el número de serie, número de parte del bien o identificador del producto o del servicio amparado por la presente parte. Opcionalmente se puede utilizar claves 
        /// del estándar GTIN.
        /// pattern value="[^|]{1,100}", Longitud: 100
        /// </summary>
        public IConceptoColumnsGridBuilder NoIdentificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "NoIdentificacion",
                HeaderText = "No. Ident.",
                Name = "NoIdentificacion",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 90
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        public IConceptoColumnsGridBuilder ObjetoImp() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                DisplayMember = "Id",
                FieldName = "ObjetoImp",
                HeaderText = "Obj. Imp.",
                Name = "ObjetoImp",
                TextAlignment = ContentAlignment.MiddleCenter,
                ValueMember = "Id"
            });
            return this;
        }

        public IConceptoColumnsGridBuilder NoOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "NoOrden",
                HeaderText = "No. Orden",
                Name = "NoOrden",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        public IConceptoColumnsGridBuilder ClaveProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveProdServ",
                HeaderText = "Clv. Prod.",
                Name = "ClaveProdServ",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la descripción del bien o servicio cubierto por el presente concepto
        /// </summary>
        public IConceptoColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Concepto",
                Name = "Descripcion",
                Width = 285
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer el valor o precio unitario del bien o servicio cubierto por el presente concepto
        /// </summary>
        public IConceptoColumnsGridBuilder ValorUnitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ValorUnitario",
                FormatString = "{0:N2}",
                HeaderText = "V. Unitario",
                Name = "ValorUnitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// obtener o establcer el importe total de los bienes o servicios del presente concepto. Debe ser equivalente al resultado de multiplicar la cantidad por el valor unitario expresado en el concepto. No se permiten valores negativos.
        /// </summary>
        public IConceptoColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = "{0:N2}",
                HeaderText = "Importe",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridBuilder Descuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:N2}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridBuilder SubTotal(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = this.FormatStringMoney,
                HeaderText = "SubTotal",
                IsVisible = isVisible,
                Name = "SubTotal",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
                ExcelExportType = DisplayFormatType.Standard
            });
            return this;
        }
        #endregion

        #region complemento a cuenta de tercero
        /// <summary>
        /// Registro Federal de Contribuyentes del contribuyente Tercero, a cuenta del que se realiza la operación.
        /// </summary>
        public IConceptoColumnsGridBuilder RfcACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RfcACuentaTerceros",
                HeaderText = "RFC",
                Name = "RfcACuentaTerceros",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 110
            });
            return this;
        }

        /// <summary>
        /// nombre, denominación o razón social del contribuyente Tercero correspondiente con el Rfc, a cuenta del que se realiza la operación.
        /// </summary>
        public IConceptoColumnsGridBuilder NombreACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NombreACuentaTerceros",
                HeaderText = "Denominación ó Razón Social",
                Name = "NombreACuentaTerceros",
                ReadOnly = true,
                Width = 220
            });
            return this;
        }

        /// <summary>
        /// clave del régimen del contribuyente Tercero, a cuenta del que se realiza la operación.
        /// </summary>
        public IConceptoColumnsGridBuilder RegimenFiscalACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RegimenFiscalACuentaTerceros",
                HeaderText = "Rég. Fiscal",
                Name = "RegimenFiscalACuentaTerceros",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// codigo postal del domicilio fiscal del Tercero, a cuenta del que se realiza la operación.
        /// </summary>
        public IConceptoColumnsGridBuilder DomicilioFiscalACuentaTerceros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "DomicilioFiscalACuentaTerceros",
                HeaderText = "Dom. Fiscal",
                Name = "DomicilioFiscalACuentaTerceros",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }
        #endregion

        #region numero de pedimento
        public IConceptoColumnsGridBuilder NumeroPedimento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumeroPedimento",
                HeaderText = "Número de Pedimento",
                Name = "NumeroPedimento",
                Width = 150
            });
            return this;
        }
        #endregion
    }
}
