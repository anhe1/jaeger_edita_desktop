﻿using Jaeger.UI.Common.Builder;
using static Jaeger.UI.Comprobante.Builder.ComprobanteFiscalGridBuilder;

namespace Jaeger.UI.Comprobante.Builder {
    public interface IComprobanteFiscalGridBuilder : IGridViewBuilder {
        IComprobanteFiscalTempleteGridBuilder Templetes();
        IComprobanteFiscalTempletesGridBuilder Templetes(TempleteEnum templete = TempleteEnum.Standar);
    }

    public interface IComprobanteFiscalTempletesGridBuilder {
        IComprobanteFiscalRecibidoGridBuilder Recibido();
        IComprobanteFiscalEmitidoGridBuilder Emitido();
    }

    public interface IComprobanteFiscalRecibidoGridBuilder : IGridViewBuilder {

    }

    public interface IComprobanteFiscalEmitidoGridBuilder : IGridViewBuilder {

    }

    public interface IComprobanteFiscalTempleteGridBuilder : IGridViewTempleteBuild, IComprobanteFiscalColumnsGridBuilder {
        IComprobanteFiscalTempleteGridBuilder Master();
        IComprobanteFiscalTempleteGridBuilder Single();
        //IComprobanteFiscalTempleteGridBuilder Master(TempleteEnum templete = TempleteEnum.Standar);
        IComprobanteFiscalTempleteGridBuilder EdoCuenta();
        IComprobanteFiscalTempleteGridBuilder Conceptos();
        IComprobanteFiscalTempleteGridBuilder ConceptosParte();

        /// <summary>
        /// templete builder para conceptos
        /// </summary>
        /// <returns>Builder templete</returns>
        IConceptoGridBuilder Concepto();

        /// <summary>
        /// master de factura y conceptos
        /// </summary>
        IComprobanteFiscalTempleteGridBuilder MasterYConceptos();

        /// <summary>
        /// templete para CFDI Relaciondo, solo tiene la clave de relacion que se carga con un metodo estatico
        /// </summary>
        IComprobanteFiscalTempleteGridBuilder CFDIRelaciondos();

        /// <summary>
        /// templete de documentos relacionados del grid CFDI Relacionado
        /// </summary>
        IComprobanteFiscalTempleteGridBuilder CFDIRelaciondosDoctos();
        IComprobanteFiscalTempleteGridBuilder ComplementoPago();
        IComprobanteFiscalTempleteGridBuilder ComplementoPagoD();
        IComprobanteFiscalTempleteGridBuilder DocumentoRelacionado();

        /// <summary>
        /// para la vista de comprobantes de ingreso y complementos de pago
        /// </summary>
        IComprobanteFiscalTempleteGridBuilder ComprobanteVsComplementoPago();
    }

    public interface IComprobanteFiscalColumnsGridBuilder : IGridViewBuilder, IGridViewColumnsBuild {
        #region comprobantes
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder IdComprobante();

        /// <summary>
        /// obtener o establecer version del estandar bajo el que se encuentra expresado el comprobante
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder Version();

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder CheckActivo();

        IComprobanteFiscalColumnsGridBuilder Tipocomprobante();

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder TipoComprobanteText();
        IComprobanteFiscalColumnsGridBuilder SubTipoText();
        IComprobanteFiscalColumnsGridBuilder SubTipo();
        IComprobanteFiscalColumnsGridBuilder Status();

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder Folio();

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder Serie();

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder IdDirectorio();

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder EmisorRFC(string headerText = "RFC Emisor");

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder EmisorNombre(bool isVisible = true);

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        //IComprobanteFiscalColumnsGridBuilder EmisorNombre(string headerText = "Nombre del Emisor");

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder ReceptorRFC(string headerText = "RFC Receptor");
        IComprobanteFiscalColumnsGridBuilder ReceptorNombre(bool isVisible = true);
        IComprobanteFiscalColumnsGridBuilder IdDocumento(bool isVisible = true);
        IComprobanteFiscalColumnsGridBuilder FechaEmision();
        IComprobanteFiscalColumnsGridBuilder FechaTimbre();
        IComprobanteFiscalColumnsGridBuilder FechaCancela();
        IComprobanteFiscalColumnsGridBuilder FechaValidacion();
        IComprobanteFiscalColumnsGridBuilder FechaEntrega();
        IComprobanteFiscalColumnsGridBuilder FechaUltimoPago();
        IComprobanteFiscalColumnsGridBuilder FechaRecepcionPago();
        IComprobanteFiscalColumnsGridBuilder NumParcialidad();
        IComprobanteFiscalColumnsGridBuilder ClaveMetodoPago(bool isVisible = false);
        IComprobanteFiscalColumnsGridBuilder ClaveFormaPago();
        IComprobanteFiscalColumnsGridBuilder SubTotal(bool isVisible = false);
        IComprobanteFiscalColumnsGridBuilder TotalTrasladoIVA();
        IComprobanteFiscalColumnsGridBuilder TotalTrasladoIEPS();
        IComprobanteFiscalColumnsGridBuilder TotalRetencionISR();
        IComprobanteFiscalColumnsGridBuilder TotalRetencionIVA();
        IComprobanteFiscalColumnsGridBuilder TotalRetencionIEPS();
        IComprobanteFiscalColumnsGridBuilder TotalPecepcion();
        IComprobanteFiscalColumnsGridBuilder TotalDeduccion();
        IComprobanteFiscalColumnsGridBuilder GTotal();
        IComprobanteFiscalColumnsGridBuilder TotalDescuento();
        IComprobanteFiscalColumnsGridBuilder ImporteNota();
        IComprobanteFiscalColumnsGridBuilder Acumulado(string hedearText = "Cobrado");
        IComprobanteFiscalColumnsGridBuilder Cobrado();
        IComprobanteFiscalColumnsGridBuilder Pagado();
        IComprobanteFiscalColumnsGridBuilder Saldo();
        IComprobanteFiscalColumnsGridBuilder ImportePagado();
        IComprobanteFiscalColumnsGridBuilder SaldoPagos();
        IComprobanteFiscalColumnsGridBuilder Estado();
        IComprobanteFiscalColumnsGridBuilder FechaEstado();
        IComprobanteFiscalColumnsGridBuilder FechaNuevo();
        IComprobanteFiscalColumnsGridBuilder Creo();
        IComprobanteFiscalColumnsGridBuilder Modifica();
        IComprobanteFiscalColumnsGridBuilder FechaMod();
        IComprobanteFiscalColumnsGridBuilder UrlFileXml();
        IComprobanteFiscalColumnsGridBuilder UrlFilePdf();
        IComprobanteFiscalColumnsGridBuilder Moneda();
        IComprobanteFiscalColumnsGridBuilder Nota();
        IComprobanteFiscalColumnsGridBuilder DiasTranscurridos(bool isVisible = false);
        #endregion

        #region CFDI Relacionados
        /// <summary>
        /// clave de relacion de documentos relacionados (CFDI Relacionado)
        /// </summary>
        IComprobanteFiscalColumnsGridBuilder ClaveRelacion();
        #endregion
    }

    
}
