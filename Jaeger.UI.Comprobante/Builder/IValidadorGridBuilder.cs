﻿using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Comprobante.Builder {
    public interface IValidadorGridBuilder : IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild {
        IValidadorTempleteGridBuilder Templetes();
    }

    public interface IValidadorColumnsGridBuilder : IGridViewBuilder, IGridViewColumnsBuild {
        #region columnas
         IValidadorColumnsGridBuilder Id();

         IValidadorColumnsGridBuilder TipoComprobante();

         IValidadorColumnsGridBuilder Version();

         IValidadorColumnsGridBuilder MetaData();

         IValidadorColumnsGridBuilder Folio();

         IValidadorColumnsGridBuilder Serie();

         IValidadorColumnsGridBuilder EmisorRFC();

         IValidadorColumnsGridBuilder EmisorNombre();

         IValidadorColumnsGridBuilder ReceptorRFC();

         IValidadorColumnsGridBuilder ReceptorNombre();

         IValidadorColumnsGridBuilder FechaEmision();

         IValidadorColumnsGridBuilder FechaTimbre();

         IValidadorColumnsGridBuilder IdDocumento();

         IValidadorColumnsGridBuilder NoCertificado();

         IValidadorColumnsGridBuilder RFCProvCertif();

         IValidadorColumnsGridBuilder Estado();

         IValidadorColumnsGridBuilder LugarExpedicion();

         IValidadorColumnsGridBuilder UsoCFDI();

         IValidadorColumnsGridBuilder MetodoPago();

         IValidadorColumnsGridBuilder FormaPago();

         IValidadorColumnsGridBuilder MetodoVsForma();

         IValidadorColumnsGridBuilder SubTotal();

         IValidadorColumnsGridBuilder Descuento();

         IValidadorColumnsGridBuilder RetencionISR();

         IValidadorColumnsGridBuilder RetencionIVA();

         IValidadorColumnsGridBuilder RetencionIEPS();


         IValidadorColumnsGridBuilder TrasladoIVA();

         IValidadorColumnsGridBuilder TrasladoIEPS();

         IValidadorColumnsGridBuilder Total();

         IValidadorColumnsGridBuilder PathXML();

         IValidadorColumnsGridBuilder PathPDF();

         IValidadorColumnsGridBuilder Situacion();

         IValidadorColumnsGridBuilder ClaveUnidad();

         IValidadorColumnsGridBuilder ClaveConceptos();

         IValidadorColumnsGridBuilder Complementos();

         IValidadorColumnsGridBuilder Resultado();

         IValidadorColumnsGridBuilder Registrado();
        #endregion
    }

    public interface IValidadorTempleteGridBuilder : IGridViewTempleteBuild, IValidadorColumnsGridBuilder {
        IValidadorTempleteGridBuilder Master();

        IConceptoGridBuilder Conceptos();
    }
}
