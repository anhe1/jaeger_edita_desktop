﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    public interface IImpuestosGridBuilder : IGridViewBuilder, IImpuestosTempleteGridBuilder, IImpuestosColumnsGridBuilder {
        IImpuestosTempleteGridBuilder Templetes();
    }

    public interface IImpuestosTempleteGridBuilder : IGridViewBuilder {
        IImpuestosTempleteGridBuilder Master();
    }

    public interface IImpuestosColumnsGridBuilder {
        IImpuestosColumnsGridBuilder Tipo();

        IImpuestosColumnsGridBuilder Impuesto();

        IImpuestosColumnsGridBuilder Base();

        IImpuestosColumnsGridBuilder TipoFactor();

        IImpuestosColumnsGridBuilder TasaOCuota();

        IImpuestosColumnsGridBuilder Importe();
    }
}
