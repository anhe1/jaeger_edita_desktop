﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    public interface IComplementoPagoDoctoRelacionadoGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IComplementoPagoDoctoRelacionadoTempleteGridBuilder Templetes();
        IComplementoPagoDoctoRelacionadoColumnsBuilder Columns();
    }

    public interface IComplementoPagoDoctoRelacionadoTempleteGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IComplementoPagoDoctoRelacionadoTempleteGridBuilder Master();
    }

    public interface IComplementoPagoDoctoRelacionadoColumnsBuilder : IGridViewBuilder, IGridViewColumnsBuild {
        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder IdDocumentoDR();

        /// <summary>
        /// obtener o establecer serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder SerieDR();

        /// <summary>
        /// obtener o establecer folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder Folio();

        /// <summary>
        /// obtener o establecer RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder RFC();

        /// <summary>
        /// obtener o establecer nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder Nombre();

        /// <summary>
        /// 
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder FechaEmision();

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder FormaPagoP();

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder MetodoPago();

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento 
        /// relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto”
        /// de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder MonedaDR();

        /// <summary>
        /// 
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder EquivalenciaDR();

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder NumParcialidad();

        /// <summary>
        /// obtener o establecer el importe total del comprobante relacionado
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder Total();

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder ImpSaldoAnt();

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder ImpPagado();

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder ImpSaldoInsoluto();

        /// <summary>
        /// obtener o establecer el pago del documento relacionado es objeto o no de impuesto.
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder ObjetoImpDR();

        /// <summary>
        /// 
        /// </summary>
        IComplementoPagoDoctoRelacionadoColumnsBuilder IdComprobanteR();
    }
}
