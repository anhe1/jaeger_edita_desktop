﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    /// <summary>
    /// Complemento de Pago
    /// </summary>
    public class ComplementoPagoGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IComplementoPagoGridBuilder, IComplementoPagoColumnsGridBuilder,
        IComplementoPagoTempletesGridBuilder {

        public ComplementoPagoGridBuilder() : base() { }

        #region templetes
        public IComplementoPagoTempletesGridBuilder Templetes() {
            return this;
        }

        public IComplementoPagoTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.Version(false).FechaPago().FormaDePagoP().MonedaP().TipoCambioP().Monto().NumOperacion().RfcEmisorCtaOrd().NomBancoOrdExt().CtaOrdenante().RfcEmisorCtaBen().TipoCadPago()
                .CtaBeneficiario().CadPago().CertPago().SelloPago();
            return this;
        }

        public IComplementoPagoColumnsGridBuilder Columns() {
            return this;
        }

        public IComplementoPagoTempletesGridBuilder DoctoRelacionados() {
            this._Columns.Clear();
            IComplementoPagoDoctoRelacionadoGridBuilder templeteBuilder = new ComplementoPagoDoctoRelacionadoBuilder();
            this._Columns.AddRange(templeteBuilder.Templetes().Master().Build());
            return this;
        }
        #endregion

        #region columnas
        /// <summary>
        /// columna para el indice de la tabla de pagos
        /// </summary>
        public IComplementoPagoColumnsGridBuilder IdPago(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdPago",
                HeaderText = "IdPago",
                Name = "IdPago",
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible,
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 50
            });
            return this;
        }

        /// <summary>
        /// columna version del complemento para recepcion de pagos.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder Version(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Version",
                HeaderText = "Ver.",
                Name = "Version",
                IsVisible = isVisible,
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// columna de indice de relacion con el comprobante de pagos (cfdi) al que pertenece el complemento
        /// </summary>
        public IComplementoPagoColumnsGridBuilder IdComprobanteP(bool isVisible = false) {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdComprobanteP",
                HeaderText = "IdComprobanteP",
                Name = "IdComprobanteP",
                IsVisible = isVisible,
                VisibleInColumnChooser = isVisible,
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IComplementoPagoColumnsGridBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                Name = "Activo",
                ReadOnly = true,
                VisibleInColumnChooser = false,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la 
        /// especificación ISO 8601.En caso de no contar con la hora se debe registrar 12:00:00.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder FechaPago() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaPagoP",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha \r\nde Pago",
                Name = "FechaPagoP",
                ReadOnly = true,
                DataType = typeof(DateTime),
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85,
            });
            return this;
        }

        /// <summary>
        /// clave de la forma en que se realiza el pago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder FormaDePagoP() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FormaDePagoP",
                HeaderText = "Forma \r\nde Pago",
                Name = "FormaDePagoP",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// clave de la moneda utilizada para realizar el pago conforme a la especificación ISO 4217. Cuando se usa moneda nacional se registra MXN. 
        /// El atributo Pagos:Pago:Monto debe ser expresado en la moneda registrada en este atributo.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder MonedaP() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MonedaP",
                HeaderText = "Moneda",
                Name = "MonedaP",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una 
        /// unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder TipoCambioP(bool isVisible = true) {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoCambioP",
                HeaderText = "Tipo \r\nCambio",
                Name = "TipoCambioP",
                DataType = typeof(decimal),
                IsVisible = isVisible,
                FormatString = this.FormatStringMoney,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// importe del pago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder Monto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Monto",
                HeaderText = "Monto",
                Name = "Monto",
                DataType = typeof(decimal),
                FormatString = this.FormatStringMoney,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 95
            });
            return this;
        }

        /// <summary>
        /// numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia 
        /// análogo que identifique la operación que ampara el pago efectuado.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder NumOperacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumOperacion",
                HeaderText = "Num. Operación",
                Name = "NumOperacion",
                DataType = typeof(string),
                FormatString = this.FormatStringMoney,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc.,
        /// en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder RfcEmisorCtaOrd() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RfcEmisorCtaOrd",
                HeaderText = "RFC Emisor \r\nCta. Ord.",
                Name = "RfcEmisorCtaOrd",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de 
        /// acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder NomBancoOrdExt() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NomBancoOrdExt",
                HeaderText = "Nom. Banco \r\nOrd. Ext.",
                Name = "NomBancoOrdExt",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// numero de la cuenta con la que se realizo el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con 
        /// el catálogo catCFDI:c_FormaPago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder CtaOrdenante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CtaOrdenante",
                HeaderText = "Cta. Ordenante",
                Name = "CtaOrdenante",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 125
            });
            return this;
        }

        /// <summary>
        /// clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. 
        /// Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder RfcEmisorCtaBen() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RfcEmisorCtaBen",
                HeaderText = "RFC Emisor \r\nCta. Ben.",
                Name = "RfcEmisorCtaBen",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// clave del tipo de cadena de pago que genera la entidad receptora del pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para 
        /// éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder TipoCadPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoCadPago",
                HeaderText = "Tipo Cad. Pago",
                Name = "TipoCadPago",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// numero de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo 
        /// con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder CtaBeneficiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CtaBeneficiario",
                HeaderText = "Cta. Beneficiario",
                Name = "CtaBeneficiario",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 120
            });
            return this;
        }

        /// <summary>
        /// la cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria. Es requerido en caso de que el atributo TipoCadPago 
        /// contenga información.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder CadPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CadPago",
                HeaderText = "CadPago",
                IsVisible = false,
                Name = "CadPago",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// certificado que ampara al pago, como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder CertPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CertPago",
                HeaderText = "CertPago",
                IsVisible = false,
                Name = "CertPago",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// sello digital que se asocie al pago. La entidad que emite el comprobante de pago, ingresa una cadena original y el sello digital en una sección de 
        /// dicho comprobante, este sello digital es el que se debe registrar en este atributo. Debe ser expresado como una cadena de texto en formato base 64. Es requerido en caso 
        /// de que el atributo TipoCadPago contenga información.
        /// </summary>
        public IComplementoPagoColumnsGridBuilder SelloPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "SelloPago",
                HeaderText = "SelloPago",
                IsVisible = false,
                Name = "SelloPago",
                DataType = typeof(string),
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 140
            });
            return this;
        }
        #endregion

        /// <summary>
        /// Folio Fiscal del complemento de pago, FieldName=IdDocumentoPG
        /// </summary>
        public IComplementoPagoColumnsGridBuilder IdDocumentoPG() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumentoPG",
                HeaderText = "Folio Fiscal \r\n(C. Pago)",
                Name = "IdDocumentoPG",
                ReadOnly = true,
                Width = 180
            });
            return this;
        }
    }
}
