﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    /// <summary>
    /// builder para templete de conceptos
    /// </summary>
    public interface IConceptoGridBuilder : IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IConceptoParteTempletesGridBuilder {
        IConceptoTempletesGridBuilder Master();
    }

    public interface IConceptoTempletesGridBuilder : IGridViewBuilder, IConceptoColumnsGridBuilder {
        IConceptoParteTempletesGridBuilder ConceptoParte();
    }

    public interface IConceptoParteTempletesGridBuilder : IGridViewBuilder, IConceptoColumnsGridBuilder {
    }

    public interface IConceptoColumnsGridBuilder : IGridViewColumnsBuild, IGridViewBuilder {
        IConceptoColumnsGridBuilder CheckActivo();
        IConceptoColumnsGridBuilder NumPedido();
        IConceptoColumnsGridBuilder Cantidad();
        IConceptoColumnsGridBuilder ClaveUnidad();
        IConceptoColumnsGridBuilder Unidad();
        IConceptoColumnsGridBuilder NoIdentificacion();
        IConceptoColumnsGridBuilder ObjetoImp();
        IConceptoColumnsGridBuilder NoOrden();
        IConceptoColumnsGridBuilder ClaveProducto();
        IConceptoColumnsGridBuilder Descripcion();
        IConceptoColumnsGridBuilder ValorUnitario();
        IConceptoColumnsGridBuilder Importe();
        IConceptoColumnsGridBuilder Descuento();
        IConceptoColumnsGridBuilder SubTotal(bool isVisible = false);
        IConceptoColumnsGridBuilder NumeroPedimento();
    }
}
