﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Comprobante.Builder {
    /// <summary>
    /// builder de documentos relacionados al complemento de pagos
    /// </summary>
    public class ComplementoPagoDoctoRelacionadoBuilder : GridViewBuilder, IGridViewBuilder, IGridViewColumnsBuild, IGridViewTempleteBuild, IComplementoPagoDoctoRelacionadoGridBuilder,
        IComplementoPagoDoctoRelacionadoColumnsBuilder, IComplementoPagoDoctoRelacionadoTempleteGridBuilder {
        public ComplementoPagoDoctoRelacionadoBuilder() : base() { }

        public IComplementoPagoDoctoRelacionadoTempleteGridBuilder Templetes() {
            return this;
        }

        public IComplementoPagoDoctoRelacionadoTempleteGridBuilder Master() {
            this.IdDocumentoDR().SerieDR().Folio().RFC().Nombre().FechaEmision().FormaPagoP().MetodoPago().MonedaDR().EquivalenciaDR().NumParcialidad().ImpSaldoAnt().ImpPagado().ImpSaldoInsoluto().ObjetoImpDR().IdComprobanteR();
            return this;
        }

        public IComplementoPagoDoctoRelacionadoColumnsBuilder Columns() { return this; }

        #region columnas
        /// <summary>
        /// FieldName=IdDocumento
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder IdDocumentoDR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumentoDR",
                HeaderText = "Folio Fiscal (uuid)",
                Name = "IdDocumentoDR",
                ReadOnly = true,
                Width = 180
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder SerieDR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "SerieDR",
                HeaderText = "Serie",
                Name = "SerieDR",
                ReadOnly = true,
                Width = 65
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                ReadOnly = true,
                Width = 65,
            }); return this;
        }

        /// <summary>
        /// obtener o establecer RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder RFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "RFC",
                ReadOnly = true,
                Width = 90
            }); return this;
        }

        /// <summary>
        /// obtener o establecer nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre",
                Name = "Nombre",
                ReadOnly = true,
                Width = 220
            }); return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEmision",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha de Emisión",
                Name = "FechaEmision",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 70,
                WrapText = true,
            });
            return this;
        }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder FormaPagoP() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FormaPagoP",
                HeaderText = "Forma \r\nde Pago",
                Name = "FormaPagoP",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// FieldName = "MetodoPago",
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder MetodoPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MetodoPago",
                HeaderText = "Método de Pago",
                IsVisible = false,
                Name = "MetodoPago",
                ReadOnly = true,
                Width = 75,
                WrapText = true
            });
            return this;
        }

        /// <summary>
        /// FieldName = "MonedaDR",
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder MonedaDR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "MonedaDR",
                HeaderText = "Moneda",
                Name = "MonedaDR",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        /// <summary>
        /// FieldName = "EquivalenciaDR",
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder EquivalenciaDR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EquivalenciaDR",
                HeaderText = "Equivalencia",
                Name = "EquivalenciaDR",
                TextAlignment = ContentAlignment.MiddleCenter,
                FormatString = this.FormatStringMoney,
                DataType = typeof(decimal)
            }); return this;
        }

        /// <summary>
        /// FieldName = "NumParcialidad",
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder NumParcialidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumParcialidad",
                HeaderText = "Parcialidad",
                Name = "NumParcialidad",
                TextAlignment = ContentAlignment.MiddleCenter,
                FormatString = this.FormatStringNumber
            }); return this;
        }

        /// <summary>
        /// obtener o establecer el importe total del comprobante relacionado
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder Total() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = this.FormatStringMoney,
                HeaderText = "Total",
                Name = "Total",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            }); return this;
        }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder ImpSaldoAnt() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImpSaldoAnt",
                FormatString = this.FormatStringMoney,
                HeaderText = "Saldo Ant.",
                Name = "ImpSaldoAnt",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            }); return this;
        }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder ImpPagado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImpPagado",
                FormatString = this.FormatStringMoney,
                HeaderText = "Imp. Pagado",
                Name = "ImpPagado",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            }); return this;
        }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder ImpSaldoInsoluto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ImpSaldoInsoluto",
                FormatString = this.FormatStringMoney,
                HeaderText = "Saldo Insoluto",
                Name = "ImpSaldoInsoluto",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            }); return this;
        }

        /// <summary>
        /// obtener o establecer el pago del documento relacionado es objeto o no de impuesto.
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder ObjetoImpDR() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                DisplayMember = "Id",
                FieldName = "ObjetoImpDR",
                HeaderText = "Obj. Imp.",
                Name = "ObjetoImpDR",
                TextAlignment = ContentAlignment.MiddleCenter,
                ValueMember = "Id"
            });
            return this;
        }

        /// <summary>
        /// columna del indice del comprobante relacionado
        /// </summary>
        public IComplementoPagoDoctoRelacionadoColumnsBuilder IdComprobanteR() {
            var columnIdComprobanteR = new GridViewTextBoxColumn {
                FieldName = "IdComprobanteR",
                HeaderText = "IdComprobanteR",
                Name = "IdComprobanteR",
                ReadOnly = true,
                VisibleInColumnChooser = false,
                IsVisible = false,
                Width = 80
            };
            var conditional = columnIdComprobanteR.ConditionalFormattingObjectList;
            conditional.Add(Conditional());
            this._Columns.Add(columnIdComprobanteR);
            return this;
        }
        #endregion

        #region formatos condicionales
        public static ConditionalFormattingObject Conditional() {
            return new ConditionalFormattingObject() {
                ApplyToRow = true,
                CellBackColor = Color.Empty,
                CellForeColor = Color.Empty,
                Name = "Relacion CFDi",
                RowBackColor = Color.Empty,
                RowFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Italic, GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = Color.DimGray,
                TValue1 = "0",
                TValue2 = "0",
            };
        }
        #endregion
    }
}
