﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TipoCambio = new Telerik.WinControls.UI.RadSpinEditor();
            this.TipoComprobante = new Telerik.WinControls.UI.RadDropDownList();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaCertifica = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaCertificacion = new Telerik.WinControls.UI.RadLabel();
            this.lblTipoComprobante = new Telerik.WinControls.UI.RadLabel();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.Serie = new Telerik.WinControls.UI.RadDropDownList();
            this.Documento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.MetodoPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.FormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Condiciones = new Telerik.WinControls.UI.RadDropDownList();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.lblSerie = new Telerik.WinControls.UI.RadLabel();
            this.lblLugarExpedicion = new Telerik.WinControls.UI.RadLabel();
            this.LugarExpedicion = new Telerik.WinControls.UI.RadTextBox();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.lblMetodoPago = new Telerik.WinControls.UI.RadLabel();
            this.lblCondiciones = new Telerik.WinControls.UI.RadLabel();
            this.lblFormaPago = new Telerik.WinControls.UI.RadLabel();
            this.lblExportacion = new Telerik.WinControls.UI.RadLabel();
            this.Exportacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.GroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.Receptor = new Jaeger.UI.Comprobante.Forms.ComprobanteContribuyenteControl();
            this.lblDecimales = new Telerik.WinControls.UI.RadLabel();
            this.Decimales = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaCertifica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaCertificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLugarExpedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCondiciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExportacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox)).BeginInit();
            this.GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).BeginInit();
            this.SuspendLayout();
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.DecimalPlaces = 2;
            this.TipoCambio.Location = new System.Drawing.Point(1013, 77);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.Size = new System.Drawing.Size(54, 20);
            this.TipoCambio.TabIndex = 38;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoComprobante.DisplayMember = "Descripcion";
            this.TipoComprobante.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoComprobante.Location = new System.Drawing.Point(1100, 7);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.NullText = "Tipo";
            this.TipoComprobante.Size = new System.Drawing.Size(93, 20);
            this.TipoComprobante.TabIndex = 19;
            this.TipoComprobante.ValueMember = "Id";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(521, 7);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(192, 20);
            this.FechaEmision.TabIndex = 7;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // FechaCertifica
            // 
            this.FechaCertifica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaCertifica.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaCertifica.Location = new System.Drawing.Point(828, 7);
            this.FechaCertifica.Name = "FechaCertifica";
            this.FechaCertifica.NullText = "--/--/----";
            this.FechaCertifica.ReadOnly = true;
            this.FechaCertifica.Size = new System.Drawing.Size(114, 20);
            this.FechaCertifica.TabIndex = 9;
            this.FechaCertifica.TabStop = false;
            this.FechaCertifica.Text = "05/12/2017";
            this.FechaCertifica.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(433, 8);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEmision.TabIndex = 6;
            this.lblFechaEmision.Text = "Fec. de Emisión:";
            // 
            // lblFechaCertificacion
            // 
            this.lblFechaCertificacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaCertificacion.Location = new System.Drawing.Point(717, 8);
            this.lblFechaCertificacion.Name = "lblFechaCertificacion";
            this.lblFechaCertificacion.Size = new System.Drawing.Size(108, 18);
            this.lblFechaCertificacion.TabIndex = 8;
            this.lblFechaCertificacion.Text = "Fec. de Certificación:";
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoComprobante.Location = new System.Drawing.Point(948, 8);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(103, 18);
            this.lblTipoComprobante.TabIndex = 18;
            this.lblTipoComprobante.Text = "Tipo Comprobante:";
            this.lblTipoComprobante.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(247, 7);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.Size = new System.Drawing.Size(121, 20);
            this.Folio.TabIndex = 5;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Serie
            // 
            this.Serie.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Serie.Location = new System.Drawing.Point(61, 7);
            this.Serie.Name = "Serie";
            this.Serie.NullText = "Serie";
            this.Serie.Size = new System.Drawing.Size(125, 20);
            this.Serie.TabIndex = 3;
            // 
            // Documento
            // 
            // 
            // Documento.NestedRadGridView
            // 
            this.Documento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Documento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Documento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Documento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Documento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Documento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Documento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "TipoDeComprobante";
            gridViewTextBoxColumn1.HeaderText = "Comprobante";
            gridViewTextBoxColumn1.Name = "TipoDeComprobante";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn3.FieldName = "Serie";
            gridViewTextBoxColumn3.HeaderText = "Serie";
            gridViewTextBoxColumn3.Name = "Serie";
            gridViewTextBoxColumn4.FieldName = "Folio";
            gridViewTextBoxColumn4.HeaderText = "Folio";
            gridViewTextBoxColumn4.Name = "Folio";
            gridViewTextBoxColumn5.FieldName = "Template";
            gridViewTextBoxColumn5.HeaderText = "Template";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Template";
            this.Documento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.Documento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Documento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Documento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Documento.EditorControl.Name = "NestedRadGridView";
            this.Documento.EditorControl.ReadOnly = true;
            this.Documento.EditorControl.ShowGroupPanel = false;
            this.Documento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Documento.EditorControl.TabIndex = 0;
            this.Documento.Location = new System.Drawing.Point(75, 132);
            this.Documento.Name = "Documento";
            this.Documento.NullText = "Documento";
            this.Documento.Size = new System.Drawing.Size(164, 20);
            this.Documento.TabIndex = 1;
            this.Documento.TabStop = false;
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn6.FieldName = "Clave";
            gridViewTextBoxColumn6.HeaderText = "Clave";
            gridViewTextBoxColumn6.Name = "Clave";
            gridViewTextBoxColumn7.FieldName = "Descripcion";
            gridViewTextBoxColumn7.HeaderText = "Descripción";
            gridViewTextBoxColumn7.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(1126, 77);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(67, 20);
            this.Moneda.TabIndex = 27;
            this.Moneda.TabStop = false;
            // 
            // MetodoPago
            // 
            this.MetodoPago.AutoSizeDropDownToBestFit = true;
            this.MetodoPago.DisplayMember = "Descriptor";
            this.MetodoPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MetodoPago.NestedRadGridView
            // 
            this.MetodoPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MetodoPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MetodoPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MetodoPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "Clave";
            gridViewTextBoxColumn8.HeaderText = "Clave";
            gridViewTextBoxColumn8.Name = "Clave";
            gridViewTextBoxColumn9.FieldName = "Descripcion";
            gridViewTextBoxColumn9.HeaderText = "Descripción";
            gridViewTextBoxColumn9.Name = "Descripcion";
            gridViewTextBoxColumn10.FieldName = "Descriptor";
            gridViewTextBoxColumn10.HeaderText = "Descriptor";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "Descriptor";
            this.MetodoPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.MetodoPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MetodoPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MetodoPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MetodoPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.MetodoPago.EditorControl.Name = "NestedRadGridView";
            this.MetodoPago.EditorControl.ReadOnly = true;
            this.MetodoPago.EditorControl.ShowGroupPanel = false;
            this.MetodoPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MetodoPago.EditorControl.TabIndex = 0;
            this.MetodoPago.Location = new System.Drawing.Point(103, 77);
            this.MetodoPago.Name = "MetodoPago";
            this.MetodoPago.NullText = "Método de Pago";
            this.MetodoPago.Size = new System.Drawing.Size(215, 20);
            this.MetodoPago.TabIndex = 32;
            this.MetodoPago.TabStop = false;
            this.MetodoPago.ValueMember = "Clave";
            // 
            // FormaPago
            // 
            this.FormaPago.AutoSizeDropDownToBestFit = true;
            this.FormaPago.DisplayMember = "Descriptor";
            this.FormaPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // FormaPago.NestedRadGridView
            // 
            this.FormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.FormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.FormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn11.FieldName = "Clave";
            gridViewTextBoxColumn11.HeaderText = "Clave";
            gridViewTextBoxColumn11.Name = "Clave";
            gridViewTextBoxColumn12.FieldName = "Descripcion";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descripcion";
            gridViewTextBoxColumn13.FieldName = "Descriptor";
            gridViewTextBoxColumn13.HeaderText = "Descriptor";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Descriptor";
            this.FormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.FormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.FormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.FormaPago.EditorControl.Name = "NestedRadGridView";
            this.FormaPago.EditorControl.ReadOnly = true;
            this.FormaPago.EditorControl.ShowGroupPanel = false;
            this.FormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.FormaPago.EditorControl.TabIndex = 0;
            this.FormaPago.Location = new System.Drawing.Point(415, 77);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.NullText = "Forma de pago";
            this.FormaPago.Size = new System.Drawing.Size(225, 20);
            this.FormaPago.TabIndex = 34;
            this.FormaPago.TabStop = false;
            this.FormaPago.ValueMember = "Clave";
            // 
            // Condiciones
            // 
            this.Condiciones.AutoSizeItems = true;
            radListDataItem1.Text = "Contado";
            radListDataItem2.Text = "Crédito";
            this.Condiciones.Items.Add(radListDataItem1);
            this.Condiciones.Items.Add(radListDataItem2);
            this.Condiciones.Location = new System.Drawing.Point(735, 77);
            this.Condiciones.Name = "Condiciones";
            this.Condiciones.NullText = "Condiciones";
            this.Condiciones.Size = new System.Drawing.Size(207, 20);
            this.Condiciones.TabIndex = 36;
            // 
            // lblFolio
            // 
            this.lblFolio.Location = new System.Drawing.Point(211, 8);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 4;
            this.lblFolio.Text = "Folio:";
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(22, 8);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(33, 18);
            this.lblSerie.TabIndex = 2;
            this.lblSerie.Text = "Serie:";
            // 
            // lblLugarExpedicion
            // 
            this.lblLugarExpedicion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLugarExpedicion.Location = new System.Drawing.Point(948, 31);
            this.lblLugarExpedicion.Name = "lblLugarExpedicion";
            this.lblLugarExpedicion.Size = new System.Drawing.Size(94, 18);
            this.lblLugarExpedicion.TabIndex = 28;
            this.lblLugarExpedicion.Text = "Lugar Expedición:";
            // 
            // LugarExpedicion
            // 
            this.LugarExpedicion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LugarExpedicion.Location = new System.Drawing.Point(1048, 30);
            this.LugarExpedicion.MaxLength = 5;
            this.LugarExpedicion.Name = "LugarExpedicion";
            this.LugarExpedicion.NullText = "00000";
            this.LugarExpedicion.Size = new System.Drawing.Size(46, 20);
            this.LugarExpedicion.TabIndex = 30;
            this.LugarExpedicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoCambio.Location = new System.Drawing.Point(948, 78);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(59, 18);
            this.lblTipoCambio.TabIndex = 37;
            this.lblTipoCambio.Text = "T. Cambio:";
            // 
            // lblMoneda
            // 
            this.lblMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneda.Location = new System.Drawing.Point(1070, 78);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 26;
            this.lblMoneda.Text = "Moneda:";
            // 
            // lblMetodoPago
            // 
            this.lblMetodoPago.Location = new System.Drawing.Point(4, 78);
            this.lblMetodoPago.Name = "lblMetodoPago";
            this.lblMetodoPago.Size = new System.Drawing.Size(93, 18);
            this.lblMetodoPago.TabIndex = 31;
            this.lblMetodoPago.Text = "Método de Pago:";
            // 
            // lblCondiciones
            // 
            this.lblCondiciones.Location = new System.Drawing.Point(649, 78);
            this.lblCondiciones.Name = "lblCondiciones";
            this.lblCondiciones.Size = new System.Drawing.Size(70, 18);
            this.lblCondiciones.TabIndex = 35;
            this.lblCondiciones.Text = "Condiciones:";
            // 
            // lblFormaPago
            // 
            this.lblFormaPago.Location = new System.Drawing.Point(325, 78);
            this.lblFormaPago.Name = "lblFormaPago";
            this.lblFormaPago.Size = new System.Drawing.Size(84, 18);
            this.lblFormaPago.TabIndex = 33;
            this.lblFormaPago.Text = "Forma de Pago:";
            // 
            // lblExportacion
            // 
            this.lblExportacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExportacion.Location = new System.Drawing.Point(948, 55);
            this.lblExportacion.Name = "lblExportacion";
            this.lblExportacion.Size = new System.Drawing.Size(67, 18);
            this.lblExportacion.TabIndex = 89;
            this.lblExportacion.Text = "Exportación:";
            // 
            // Exportacion
            // 
            this.Exportacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Exportacion.AutoSizeDropDownToBestFit = true;
            this.Exportacion.DisplayMember = "Descriptor";
            this.Exportacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Exportacion.NestedRadGridView
            // 
            this.Exportacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Exportacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exportacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Exportacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Exportacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Exportacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Exportacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn14.FieldName = "Clave";
            gridViewTextBoxColumn14.HeaderText = "Clave";
            gridViewTextBoxColumn14.Name = "Clave";
            gridViewTextBoxColumn15.FieldName = "Descripcion";
            gridViewTextBoxColumn15.HeaderText = "Descripción";
            gridViewTextBoxColumn15.Name = "Descripcion";
            gridViewTextBoxColumn16.FieldName = "Descriptor";
            gridViewTextBoxColumn16.HeaderText = "Descriptor";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Descriptor";
            this.Exportacion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
            this.Exportacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Exportacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Exportacion.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Exportacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.Exportacion.EditorControl.Name = "NestedRadGridView";
            this.Exportacion.EditorControl.ReadOnly = true;
            this.Exportacion.EditorControl.ShowGroupPanel = false;
            this.Exportacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Exportacion.EditorControl.TabIndex = 0;
            this.Exportacion.Location = new System.Drawing.Point(1021, 54);
            this.Exportacion.Name = "Exportacion";
            this.Exportacion.NullText = "Exportación";
            this.Exportacion.Size = new System.Drawing.Size(172, 20);
            this.Exportacion.TabIndex = 90;
            this.Exportacion.TabStop = false;
            this.Exportacion.ValueMember = "Clave";
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.Location = new System.Drawing.Point(1068, 8);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(26, 18);
            this.lblVersion.TabIndex = 91;
            this.lblVersion.Text = "Ver.";
            // 
            // GroupBox
            // 
            this.GroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBox.Controls.Add(this.Receptor);
            this.GroupBox.Controls.Add(this.lblVersion);
            this.GroupBox.Controls.Add(this.lblExportacion);
            this.GroupBox.Controls.Add(this.Exportacion);
            this.GroupBox.Controls.Add(this.MetodoPago);
            this.GroupBox.Controls.Add(this.lblDecimales);
            this.GroupBox.Controls.Add(this.TipoCambio);
            this.GroupBox.Controls.Add(this.lblFormaPago);
            this.GroupBox.Controls.Add(this.TipoComprobante);
            this.GroupBox.Controls.Add(this.lblCondiciones);
            this.GroupBox.Controls.Add(this.Decimales);
            this.GroupBox.Controls.Add(this.lblMetodoPago);
            this.GroupBox.Controls.Add(this.FechaEmision);
            this.GroupBox.Controls.Add(this.FechaCertifica);
            this.GroupBox.Controls.Add(this.lblMoneda);
            this.GroupBox.Controls.Add(this.lblFechaEmision);
            this.GroupBox.Controls.Add(this.lblFechaCertificacion);
            this.GroupBox.Controls.Add(this.lblTipoCambio);
            this.GroupBox.Controls.Add(this.lblTipoComprobante);
            this.GroupBox.Controls.Add(this.Folio);
            this.GroupBox.Controls.Add(this.LugarExpedicion);
            this.GroupBox.Controls.Add(this.Serie);
            this.GroupBox.Controls.Add(this.Documento);
            this.GroupBox.Controls.Add(this.lblLugarExpedicion);
            this.GroupBox.Controls.Add(this.Moneda);
            this.GroupBox.Controls.Add(this.lblSerie);
            this.GroupBox.Controls.Add(this.lblFolio);
            this.GroupBox.Controls.Add(this.FormaPago);
            this.GroupBox.Controls.Add(this.Condiciones);
            this.GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBox.HeaderText = "";
            this.GroupBox.Location = new System.Drawing.Point(0, 0);
            this.GroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.GroupBox.Name = "GroupBox";
            this.GroupBox.Size = new System.Drawing.Size(1200, 102);
            this.GroupBox.TabIndex = 92;
            this.GroupBox.TabStop = false;
            // 
            // Receptor
            // 
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.Location = new System.Drawing.Point(1, 30);
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = false;
            this.Receptor.Size = new System.Drawing.Size(941, 44);
            this.Receptor.TabIndex = 92;
            // 
            // lblDecimales
            // 
            this.lblDecimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDecimales.Location = new System.Drawing.Point(1100, 31);
            this.lblDecimales.Name = "lblDecimales";
            this.lblDecimales.Size = new System.Drawing.Size(30, 18);
            this.lblDecimales.TabIndex = 39;
            this.lblDecimales.Text = "Dec.:";
            // 
            // Decimales
            // 
            this.Decimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Decimales.Location = new System.Drawing.Point(1141, 30);
            this.Decimales.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.Decimales.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Name = "Decimales";
            this.Decimales.NullableValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Size = new System.Drawing.Size(52, 20);
            this.Decimales.TabIndex = 40;
            this.Decimales.TabStop = false;
            this.Decimales.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Decimales.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // ComprobanteGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GroupBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1200, 102);
            this.MinimumSize = new System.Drawing.Size(800, 102);
            this.Name = "ComprobanteGeneralControl";
            this.Size = new System.Drawing.Size(1200, 102);
            this.Load += new System.EventHandler(this.ComprobanteGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaCertifica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaCertificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Condiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLugarExpedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarExpedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCondiciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExportacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox)).EndInit();
            this.GroupBox.ResumeLayout(false);
            this.GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaCertifica;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaCertificacion;
        internal Telerik.WinControls.UI.RadLabel lblTipoComprobante;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadDropDownList Serie;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Documento;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MetodoPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox FormaPago;
        internal Telerik.WinControls.UI.RadDropDownList Condiciones;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadLabel lblSerie;
        internal Telerik.WinControls.UI.RadLabel lblLugarExpedicion;
        internal Telerik.WinControls.UI.RadTextBox LugarExpedicion;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblMoneda;
        internal Telerik.WinControls.UI.RadLabel lblMetodoPago;
        internal Telerik.WinControls.UI.RadLabel lblCondiciones;
        internal Telerik.WinControls.UI.RadLabel lblFormaPago;
        internal Telerik.WinControls.UI.RadSpinEditor TipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblExportacion;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Exportacion;
        protected internal Telerik.WinControls.UI.RadLabel lblVersion;
        private Telerik.WinControls.UI.RadGroupBox GroupBox;
        protected internal Telerik.WinControls.UI.RadDropDownList TipoComprobante;
        internal Telerik.WinControls.UI.RadLabel lblDecimales;
        internal Telerik.WinControls.UI.RadSpinEditor Decimales;
        public ComprobanteContribuyenteControl Receptor;
    }
}
