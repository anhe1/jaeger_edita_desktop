﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteRelacionadoControl : UserControl {
        protected IRelacionCFDICatalogo catalogoRelacionesCfdi;
        private bool editable = true;

        public event EventHandler<EventArgs> ButtonIncluir_CheckStateChanged;
        public void OnButtonIncluir_CheckStateChanged(object sender, EventArgs e) {
            if (this.ButtonIncluir_CheckStateChanged != null)
                this.ButtonIncluir_CheckStateChanged(this, e);
        }

        public ComprobanteRelacionadoControl() {
            InitializeComponent();
        }

        public bool Editable {
            get {
                return this.editable;
            }
            set {
                this.editable = value;
                this.SetEditable();
            }
        }

        public void Start() {
            this.catalogoRelacionesCfdi = new RelacionCFDICatalogo();
            this.catalogoRelacionesCfdi.Load();

            this.TipoRelacion.DataSource = this.catalogoRelacionesCfdi.Items;
            this.TipoRelacion.DisplayMember = "Descripcion";
            this.TipoRelacion.ValueMember = "Clave";
        }

        private void CFDIRelacionadoControl_Load(object sender, EventArgs e) {
            this.ToolBarHostItemIncluir.HostedItem = this.Incluir.ButtonElement;
        }

        private void ChkCfdiRelacionadoIncluir_CheckStateChanged(object sender, EventArgs e) {
            this.TipoRelacion.Enabled = this.Incluir.Checked;
            if (this.Incluir.Checked == false) {
                this.TipoRelacion.SelectedValue = null;
                this.TipoRelacion.SelectedIndex = -1;
                this.Comprobantes.Rows.Clear();
            }
            this.OnButtonIncluir_CheckStateChanged(sender, e);
        }

        private void Buscar_AgregarComprobante(object sender, Domain.Comprobante.Entities.ComprobanteFiscalDetailSingleModel e) {
            if (e != null) {
                if (ValidacionService.UUID(e.IdDocumento)) {
                    this.Comprobantes.Rows.AddNew();
                    this.Comprobantes.CurrentRow.Cells["IdDocumento"].Value = e.IdDocumento;
                    this.Comprobantes.CurrentRow.Cells["RFC"].Value = e.ReceptorRFC;
                    this.Comprobantes.CurrentRow.Cells["Nombre"].Value = e.ReceptorNombre;
                    this.Comprobantes.CurrentRow.Cells["Serie"].Value = e.Serie;
                    this.Comprobantes.CurrentRow.Cells["Folio"].Value = e.Folio;
                    this.Comprobantes.CurrentRow.Cells["Total"].Value = e.Total;
                    this.Comprobantes.CurrentRow.Cells["ImporteAplicado"].Value = e.ImportePagado;
                } else {
                    RadMessageBox.Show(this, Properties.Resources.Message_Objeto_NoValido, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void ToolBarButtonAgregarCFDI_Click(object sender, EventArgs e) {
            if (this.ReceptorRFC.Text.Length > 0) {
                var buscar = new ComprobantesBuscar0Form(this.ReceptorRFC.Text, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido);
                buscar.AgregarComprobante += Buscar_AgregarComprobante;
                buscar.ShowDialog(this.ParentForm);
            } else {
                RadMessageBox.Show(this, Properties.Resources.Message_Contribuyente_Seleciona, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void ToolBarButtonRemoverCFDI_Click(object sender, EventArgs e) {
            if (this.Comprobantes.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    try {
                        this.Comprobantes.Rows.Remove(this.Comprobantes.CurrentRow);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        private void SetEditable() {
            this.Incluir.Enabled = this.Editable;
            this.ToolBarHostItemIncluir.Enabled = this.Editable;
            this.TipoRelacion.SetEditable(this.Editable);
        }

        private void TipoRelacion_EnabledChanged(object sender, EventArgs e) {
            this.ToolBarButtonAgregarCFDI.Enabled = this.TipoRelacion.Enabled == this.Editable == true;
            this.ToolBarButtonRemoverCFDI.Enabled = this.TipoRelacion.Enabled == this.Editable == true;
        }
    }
}
