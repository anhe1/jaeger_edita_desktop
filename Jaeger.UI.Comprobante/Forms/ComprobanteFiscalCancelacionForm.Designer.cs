﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteFiscalCancelacionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.MotivoCancelacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.Relacionado = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.TCancelar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MotivoCancelacion
            // 
            this.MotivoCancelacion.AutoSizeDropDownHeight = true;
            this.MotivoCancelacion.AutoSizeDropDownToBestFit = true;
            this.MotivoCancelacion.DisplayMember = "Descriptor";
            this.MotivoCancelacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MotivoCancelacion.NestedRadGridView
            // 
            this.MotivoCancelacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MotivoCancelacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotivoCancelacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MotivoCancelacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Id";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Descripcion";
            gridViewTextBoxColumn9.FieldName = "Descriptor";
            gridViewTextBoxColumn9.HeaderText = "Descriptor";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Descriptor";
            this.MotivoCancelacion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.MotivoCancelacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.MotivoCancelacion.EditorControl.Name = "NestedRadGridView";
            this.MotivoCancelacion.EditorControl.ReadOnly = true;
            this.MotivoCancelacion.EditorControl.ShowGroupPanel = false;
            this.MotivoCancelacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MotivoCancelacion.EditorControl.TabIndex = 0;
            this.MotivoCancelacion.Location = new System.Drawing.Point(142, 79);
            this.MotivoCancelacion.Name = "MotivoCancelacion";
            this.MotivoCancelacion.NullText = "Selecciona";
            this.MotivoCancelacion.Size = new System.Drawing.Size(409, 20);
            this.MotivoCancelacion.TabIndex = 89;
            this.MotivoCancelacion.TabStop = false;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(15, 80);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(121, 18);
            this.radLabel6.TabIndex = 90;
            this.radLabel6.Text = "Motivo de cancelación:";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.Buscar);
            this.groupBox1.Controls.Add(this.Relacionado);
            this.groupBox1.Controls.Add(this.radLabel2);
            this.groupBox1.Controls.Add(this.IdDocumento);
            this.groupBox1.Controls.Add(this.radLabel1);
            this.groupBox1.Controls.Add(this.radLabel6);
            this.groupBox1.Controls.Add(this.MotivoCancelacion);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "Para cada CFDI que desea cancelar debe capturar el motivo de cancelación";
            this.groupBox1.Location = new System.Drawing.Point(0, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(571, 124);
            this.groupBox1.TabIndex = 91;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Para cada CFDI que desea cancelar debe capturar el motivo de cancelación";
            // 
            // Buscar
            // 
            this.Buscar.Location = new System.Drawing.Point(493, 53);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(58, 20);
            this.Buscar.TabIndex = 95;
            this.Buscar.Text = "Buscar";
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // Relacionado
            // 
            this.Relacionado.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Relacionado.Location = new System.Drawing.Point(142, 53);
            this.Relacionado.Name = "Relacionado";
            this.Relacionado.NullText = "Folio Fiscal";
            this.Relacionado.ReadOnly = true;
            this.Relacionado.Size = new System.Drawing.Size(345, 20);
            this.Relacionado.TabIndex = 94;
            this.Relacionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(39, 54);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(97, 18);
            this.radLabel2.TabIndex = 93;
            this.radLabel2.Text = "Folio Relacionado:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdDocumento.Location = new System.Drawing.Point(142, 27);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.NullText = "Folio Fiscal";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Size = new System.Drawing.Size(409, 20);
            this.IdDocumento.TabIndex = 92;
            this.IdDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(73, 28);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(63, 18);
            this.radLabel1.TabIndex = 91;
            this.radLabel1.Text = "Folio Fiscal:";
            // 
            // TCancelar
            // 
            this.TCancelar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCancelar.Etiqueta = "";
            this.TCancelar.Location = new System.Drawing.Point(0, 0);
            this.TCancelar.Name = "TCancelar";
            this.TCancelar.ShowActualizar = false;
            this.TCancelar.ShowAutorizar = true;
            this.TCancelar.ShowCerrar = true;
            this.TCancelar.ShowEditar = false;
            this.TCancelar.ShowExportarExcel = false;
            this.TCancelar.ShowFiltro = false;
            this.TCancelar.ShowGuardar = false;
            this.TCancelar.ShowHerramientas = false;
            this.TCancelar.ShowImagen = false;
            this.TCancelar.ShowImprimir = false;
            this.TCancelar.ShowNuevo = false;
            this.TCancelar.ShowRemover = false;
            this.TCancelar.Size = new System.Drawing.Size(571, 29);
            this.TCancelar.TabIndex = 71;
            this.TCancelar.Autorizar.Click += this.TCancelar_Autorizar_Click;
            this.TCancelar.Cerrar.Click += this.TCancelar_Cerrar_Click;
            // 
            // ComprobanteFiscalCancelacionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 153);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TCancelar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobanteFiscalCancelacionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cancelación";
            this.Load += new System.EventHandler(this.ComprobanteFiscalCancelacionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TCancelar;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MotivoCancelacion;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadTextBox Relacionado;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        internal Telerik.WinControls.UI.RadTextBox IdDocumento;
        internal Telerik.WinControls.UI.RadButton Buscar;
    }
}