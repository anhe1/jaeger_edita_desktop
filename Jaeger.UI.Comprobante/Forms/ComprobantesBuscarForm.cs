﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesBuscarForm : RadForm {
        protected IComprobantesFiscalesSearchService service;
        protected Domain.Base.ValueObjects.CFDISubTipoEnum subTipo;
        private BindingList<ComprobanteFiscalDetailSingleModel> datos;
        private string rfc;

        public void OnAgregar(ComprobanteFiscalDetailSingleModel e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public event EventHandler<ComprobanteFiscalDetailSingleModel> AgregarComprobante;

        public ComprobantesBuscarForm(string rfc, Domain.Base.ValueObjects.CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.rfc = rfc;
            this.subTipo = subTipo;
        }

        private void ComprobantesBuscarForm_Load(object sender, EventArgs e) {
            this.ToolBar.Nuevo.Text = "Agregar";
            this.service = new ComprobanteFiscalSearchService(this.subTipo);
        }

        private void TComprobante_Agregar_Click(object sender, EventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                var seleccionado = this.gridSearchResult.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    this.OnAgregar(seleccionado);
                }
            }
        }

        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            if (this.workerBuscar.IsBusy == false) {
                this.radWaitingBar1.StartWaiting();
                this.workerBuscar.RunWorkerAsync();
            }
        }

        private void TComprobante_Filtro_Click(object sender, EventArgs e) {
            this.gridSearchResult.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        private void GridSearchResult_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                this.ToolBar.Nuevo.PerformClick();
                this.Close();
            }
        }

        private void WorkerBuscar_DoWork(object sender, DoWorkEventArgs e) {
            this.datos = this.service.GetCFDI(this.rfc, this.ToolBar.GetEjercicio(), this.ToolBar.GetMes());
            e.Result = true;
        }

        private void WorkerBuscar_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            
        }

        private void WorkerBuscar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.gridSearchResult.DataSource = this.datos;
            this.radWaitingBar1.StopWaiting();
        }
    }
}
