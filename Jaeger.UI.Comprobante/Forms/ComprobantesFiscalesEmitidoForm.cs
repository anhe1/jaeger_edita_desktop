﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.UI.Comprobante.Forms {
    /// <summary>
    /// formulario de comprobantes fiscales emitidos
    /// </summary>
    public class ComprobantesFiscalesEmitidoForm : ComprobantesFiscalesForm {
        #region declaraciones
        protected RadMenuItem buttonProductos = new RadMenuItem { Text = "Productos" };
        protected internal RadMenuItem Factura = new RadMenuItem { Text = "Factura" };
        protected internal RadMenuItem CFDIGlobal = new RadMenuItem { Text = "CFDI Global" };
        protected internal RadMenuItem NotaCredito = new RadMenuItem { Text = "Nota de Crédito" };
        protected internal RadMenuItem RecepcionPago = new RadMenuItem { Text = "Recibo Electrónico de Pago" };
        protected internal RadMenuItem CartaPorte = new RadMenuItem { Text = "Carta Porte" };

        protected internal RadMenuItem MenuContextual_Clonar = new RadMenuItem { Text = "Clonar" };
        protected internal RadMenuItem Sustituir = new RadMenuItem { Text = "Sustituir" };
        protected internal RadMenuItem RecepcionPagoP = new RadMenuItem { Text = "Crear Recibo Electrónico de Pago", ToolTipText = "Crear recibo electrónico de pago del comprobante seleccionado." };
        protected internal RadMenuItem NotaCreditoP = new RadMenuItem { Text = "Crear Nota de Crédito", ToolTipText = "Crear nota de crédito del comprobante seleccionado." };
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobantesFiscalesEmitidoForm() : base(CFDISubTipoEnum.Emitido) {
            this.Text = "Clientes: Facturación";
            this.Load += ComprobantesFiscalesEmitidoForm_Load;
        }

        private void ComprobantesFiscalesEmitidoForm_Load(object sender, EventArgs e) {
            this.Icon = base.Icon;

            buttonProductos.Click += ButtonProductos_Click;
            //this.TComprobante.ShowHerramientas = true;
            this.TComprobante.Herramientas.Items.Add(buttonProductos);

            this.GridData.Columns["EmisorNombre"].IsVisible = false;
            this.GridData.Columns["FechaUltimoPago"].HeaderText = "Ult. Cobro";
            this.GridData.Columns["FechaEntrega"].HeaderText = "Fecha Revisión";
            this.GridData.Columns["FechaValidacion"].IsVisible = false;
            this.GridData.Columns["FechaValidacion"].VisibleInColumnChooser = false;

            // se aplica solo para los comprobantes emitidos a partir del 01/09/2018, solo aquellos con metodo de pago PPD
            var saldo = new ExpressionFormattingObject("Saldo", "ImportePagado<=0 AND ClaveMetodoPago = 'PPD' AND FechaEmision >= '2018/09/01' AND TipoComprobanteText = 'Ingreso'", false) { CellForeColor = System.Drawing.Color.Red };
            var saldoPagos = new ExpressionFormattingObject("Saldo de Pagos", "SaldoPagos<=0", false) { CellForeColor = System.Drawing.Color.Red };

            this.GridData.Columns["ImportePagado"].ConditionalFormattingObjectList.Add(saldo);
            this.GridData.Columns["SaldoPagos"].ConditionalFormattingObjectList.Add(saldoPagos);

            this.TComprobante.Nuevo.Items.AddRange(new RadMenuItem[] { this.Factura, this.CFDIGlobal, this.NotaCredito, this.RecepcionPago, this.CartaPorte });
            this.Factura.Click += this.TComprobante_Nuevo_Click;
            this.CFDIGlobal.Click += this.TComprobante_CFDIGlobal_Click;
            this.RecepcionPago.Click += this.TComprobante_RecepcionPago_Click;
            this.CartaPorte.Click += this.TComprobante_CartaPorte_Click;
            this.TComprobante.Editar.Click += this.TComprobante_Editar_Click;
            
            this.menuContextual.Items.AddRange(new RadMenuItem[] { MenuContextual_Clonar, Sustituir, this.RecepcionPagoP, this.NotaCreditoP });
            this.MenuContextual_Clonar.Click += this.MenuContextual_Clonar_Click;
            this.Sustituir.Click += this.MenuContextual_Sustituir_Click;
            this.RecepcionPagoP.Click += this.MenuContextual_CrearReciboElectronicoPago_Click;
            this.NotaCreditoP.Click += this.MenuContextual_CrearNotaCredito_Click;
            this.TComprobante.ShowImprimir = true;
        }

        #region barra de herramientas
        public virtual void TComprobante_Nuevo_Click(object sender, EventArgs e) {
            var form = new ComprobanteFiscalForm(CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
            form.Show();
        }

        public virtual void TComprobante_CFDIGlobal_Click(object sender, EventArgs e) {
            var form = new ComprobanteFiscalForm(CFDISubTipoEnum.Emitido, -3) { MdiParent = this.ParentForm };
            form.Show();
        }

        public virtual void TComprobante_NotaCredito_Click(object sender, EventArgs e) {
            var form = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
            form.Show();
        }

        public virtual void TComprobante_CartaPorte_Click(object sender, EventArgs e) {
            var form = new ComprobanteFiscalCartaPorteForm(CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
            form.Show();
        }

        public virtual void TComprobante_RecepcionPago_Click(object sender, EventArgs e) {
            var form = new ComprobanteFiscalRecepcionPagoForm(CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
            form.Show();
        }

        /// <summary>
        /// modo de edicion del comprobante
        /// </summary>
        public virtual void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    if (_seleccionado.IdDocumento != null) {
                        var _visualizar = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                        _visualizar.Show();
                    } else {
                        if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                            var _editar = new ComprobanteFiscalRecepcionPagoForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if (_seleccionado.Documento == "porte") {
                            var _editar = new ComprobanteFiscalCartaPorteForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Nomina) {

                        } else {
                            var _editar = new ComprobanteFiscalForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        }
                    }
                }
            }
        }

        public virtual void ButtonProductos_Click(object sender, EventArgs e) {
            var productos = new ComprobanteProductosForm() { MdiParent = this.ParentForm, Text = "Productos y Serv." };
            productos.Show();
        }
        #endregion

        #region menu contextual
        public virtual void MenuContextual_CrearReciboElectronicoPago_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccion = new List<ComprobanteFiscalDetailSingleModel>();
                seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ComprobanteFiscalDetailSingleModel));
                int c = seleccion.Where(it => it.TipoComprobante == CFDITipoComprobanteEnum.Ingreso).Count();
                int d = seleccion.Where(it => it.FechaTimbre == null).Count();
                if (seleccion.Count - c != 0 | d > 0)
                    RadMessageBox.Show(this, Properties.Resources.Message_REP_Error, "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                else {
                    if (seleccion.Count > 0) {
                        // crear el comprobante y luego la vista
                        //var cfdiV33 = this._Service.CrearReciboElectronicoPago(seleccion);
                        var cfdiV33 = ComprobanteFiscalService.Builder().Templete().RecepcionPago().Add(seleccion).Build() as ComprobanteFiscalDetailModel;
                        var nuevo = new ComprobanteFiscalRecepcionPagoForm(cfdiV33, CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                        nuevo.Show();
                    }
                }
            }
        }

        public virtual void MenuContextual_CrearNotaCredito_Click(object sender, EventArgs e) {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            var seleccion = new List<ComprobanteFiscalDetailSingleModel>();
            seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ComprobanteFiscalDetailSingleModel));
            int c = seleccion.Where(it => it.TipoComprobante == CFDITipoComprobanteEnum.Pagos).Count();
            if (c > 0)
                RadMessageBox.Show(this, Properties.Resources.Message_NCredito_Error, "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            else {
                // crear el comprobante y luego la vista
                //var nota = this._Service.CrearNotaCredito(seleccion);
                var nota = ComprobanteFiscalService.Builder().Templete().NotaCredito().With(seleccion).Build() as ComprobanteFiscalDetailModel;
                if (nota != null) {
                    var nuevo = new ComprobanteFiscalGeneralForm(nota, CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm, Text = "Nota Crédito" };
                    nuevo.Show();
                }
            }
        }

        public virtual void MenuContextual_Sustituir_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Comprobante_Sustituir)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }

            var seleccionado = (ComprobanteFiscalDetailModel)this.Tag;
            if (seleccionado != null) {
                if (seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                    var nuevo = new ComprobanteFiscalGeneralForm(seleccionado, CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante Pagos" };
                    nuevo.Show();
                } else if (seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Ingreso | seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Egreso) {
                    var nuevo = new ComprobanteFiscalGeneralForm(seleccionado, CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                    nuevo.Show();
                }
            }

            this.Tag = null;
        }

        public virtual void MenuContextual_Clonar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Comprobante_Clonar)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }

            var _seleccionado = (ComprobanteFiscalDetailModel)this.Tag;
            if (_seleccionado != null) {
                if (_seleccionado != null) {
                    if (_seleccionado.IdDocumento != null) {
                        var _visualizar = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                        _visualizar.Show();
                    } else {
                        if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                            var _editar = new ComprobanteFiscalRecepcionPagoForm(_seleccionado, CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if (_seleccionado.Documento == "porte") {
                            var _editar = new ComprobanteFiscalCartaPorteForm(_seleccionado, CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Nomina) {

                        } else {
                            var _editar = new ComprobanteFiscalForm(_seleccionado, CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        }
                    }
                }
            }

            this.Tag = null;
        }
        #endregion

        #region metodos privados
        public virtual void Comprobante_Clonar() {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            var duplicado = this._Service.GetComprobante(seleccionado.Id);
            if (duplicado != null) {
                duplicado.Clonar();
                if (duplicado.Version == "3.3")
                    duplicado.Version = "4.0";
                this.Tag = duplicado;
            }
        }

        public virtual void Comprobante_Sustituir() {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            var duplicado = this._Service.GetComprobante(seleccionado.Id);
            if (duplicado != null) {
                duplicado.Sustituir();
                this.Tag = duplicado;
            }
        }
        #endregion

        public override void Consultar() {
            var d0 = ComprobantesFiscalesService.Query().Emitido().WithYear(this.TComprobante.GetEjercicio()).WithMonth(this.TComprobante.GetMes()).Build();
            this._DataSource = new BindingList<IComprobanteFiscalDetailSingleModel>(this._Service.GetList<ComprobanteFiscalDetailSingleModel>(d0).ToList<IComprobanteFiscalDetailSingleModel>());
        }
    }
}
