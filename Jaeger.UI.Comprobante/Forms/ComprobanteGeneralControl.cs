﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteGeneralControl : UserControl {
        protected IFolioYSeriesService empresaService;
        protected IMetodoPagoCatalogo catalogoMetodoPago = new MetodoPagoCatalogo();
        protected IFormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        protected IMonedaService monedaService = new MonedaService();
        protected IExportacionCatalogo exportacionCatalogo = new ExportacionCatalogo();
        protected BindingList<ComprobanteContribuyenteModel> contribuyenteModels;
        protected BindingList<SerieFolioModel> series;

        public event EventHandler<CFDITipoComprobanteModel> TipoComprobante_Change;

        protected virtual void OnTipoChanged(CFDITipoComprobanteModel e) {
            if (TipoComprobante_Change != null) {
                this.TipoComprobante_Change(this, e);
            }
        }

        public ComprobanteGeneralControl() {
            InitializeComponent();
        }

        private void ComprobanteGeneralControl_Load(object sender, EventArgs e) {
            this.Enabled = false;
            // catalogo de monedas
            this.Moneda.DataSource = this.monedaService.GetMonedas();
            this.Moneda.DisplayMember = "Clave";
            this.Moneda.ValueMember = "Clave";

            // tipo de comprobantes
            this.TipoComprobante.DataSource = ComprobanteCommonService.GetTipoComprobantes();
            this.TipoComprobante.SelectedIndex = -1;
            this.TipoComprobante.SelectedValue = null;

            this.Folio.KeyPress += new KeyPressEventHandler(TelerikTextBoxExtension.TxbFolio_KeyPress);
            this.TipoComprobante.TextChanged += this.TipoComprobante_SelectedValueChanged;
        }

        public virtual void TipoComprobante_SelectedValueChanged(object sender, EventArgs e) {
            if (this.TipoComprobante.Text == "Ingreso" || this.TipoComprobante.Text == "I")
                this.OnTipoChanged(new CFDITipoComprobanteModel((int)CFDITipoComprobanteEnum.Ingreso, CFDITipoComprobanteEnum.Ingreso.ToString()));
            else if (this.TipoComprobante.Text == "Egreso" || this.TipoComprobante.Text == "E")
                this.OnTipoChanged(new CFDITipoComprobanteModel((int)CFDITipoComprobanteEnum.Egreso, CFDITipoComprobanteEnum.Egreso.ToString()));
            else if (this.TipoComprobante.Text == "Traslado" || this.TipoComprobante.Text == "T")
                this.OnTipoChanged(new CFDITipoComprobanteModel((int)CFDITipoComprobanteEnum.Traslado, CFDITipoComprobanteEnum.Traslado.ToString()));
            else if (this.TipoComprobante.Text == "Nomina" || this.TipoComprobante.Text == "N")
                this.OnTipoChanged(new CFDITipoComprobanteModel((int)CFDITipoComprobanteEnum.Nomina, CFDITipoComprobanteEnum.Nomina.ToString()));
            else if (this.TipoComprobante.Text == "Pagos" || this.TipoComprobante.Text == "P")
                this.OnTipoChanged(new CFDITipoComprobanteModel((int)CFDITipoComprobanteEnum.Pagos, CFDITipoComprobanteEnum.Pagos.ToString()));
            else
                this.OnTipoChanged(new CFDITipoComprobanteModel((int)CFDITipoComprobanteEnum.Ingreso, CFDITipoComprobanteEnum.Ingreso.ToString()));

            this.LugarExpedicion.TextBoxElement.ToolTipText = "Código postal del lugar de expedición del comprobante(domicilio de la matriz o de la sucursal)";
        }

        public virtual void Start() {
            this.empresaService = new FolioYSeriesService();
            this.Receptor.Start();
            // cargar catalogos
            this.catalogoMetodoPago.Load();
            this.catalogoFormaPago.Load();
            this.exportacionCatalogo.Load();
            this.series = this.empresaService.GetSeries(true);

            // catalogo formas de pago
            this.FormaPago.DataSource = this.catalogoFormaPago.Items;
            this.FormaPago.SelectedIndex = -1;
            this.FormaPago.SelectedValue = null;

            // catalogo metodos de pago
            this.MetodoPago.DataSource = this.catalogoMetodoPago.Items;
            this.MetodoPago.SelectedIndex = -1;
            this.MetodoPago.SelectedValue = null;

            // exportacion
            this.Exportacion.DataSource = this.exportacionCatalogo.Items;
            this.Exportacion.SelectedIndex = -1;
            this.Exportacion.SelectedValue = null;

            this.Serie.DataSource = this.series;
            this.Serie.DisplayMember = "Nombre";
            this.Serie.ValueMember = "Id";
            this.Serie.SelectedIndex = -1;
            this.Serie.SelectedValue = null;

            this.Enabled = true;
        }

        public void LoadContribuyente(CFDISubTipoEnum subTipo) {
            if (subTipo == CFDISubTipoEnum.Recibido) {
                this.Receptor.RelacionComerical = TipoRelacionComericalEnum.Proveedor;
            } else {
                this.Receptor.RelacionComerical = TipoRelacionComericalEnum.Cliente;
            }
            this.Receptor.OnLoad();
        }
    }
}
