﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteImpuestosControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Impuestos = new Telerik.WinControls.UI.RadGridView();
            this.TImpuestos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.Impuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Impuestos.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Impuestos
            // 
            this.Impuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Impuestos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.Impuestos.MasterTemplate.AllowAddNewRow = false;
            this.Impuestos.MasterTemplate.AllowCellContextMenu = false;
            this.Impuestos.MasterTemplate.AllowColumnChooser = false;
            this.Impuestos.MasterTemplate.AllowColumnReorder = false;
            this.Impuestos.MasterTemplate.AllowDragToGroup = false;
            this.Impuestos.MasterTemplate.AllowRowResize = false;
            this.Impuestos.MasterTemplate.AutoGenerateColumns = false;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Impuesto";
            gridViewComboBoxColumn2.HeaderText = "Impuesto";
            gridViewComboBoxColumn2.Name = "Impuesto";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Base";
            gridViewTextBoxColumn1.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn1.HeaderText = "Base";
            gridViewTextBoxColumn1.Name = "Base";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 80;
            gridViewComboBoxColumn3.FieldName = "TipoFactor";
            gridViewComboBoxColumn3.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn3.Name = "TipoFactor";
            gridViewComboBoxColumn3.Width = 80;
            gridViewTextBoxColumn2.DataType = typeof(decimal);
            gridViewTextBoxColumn2.FieldName = "TasaOCuota";
            gridViewTextBoxColumn2.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn2.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn2.Name = "TasaOCuota";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.DataType = typeof(decimal);
            gridViewTextBoxColumn3.FieldName = "Importe";
            gridViewTextBoxColumn3.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn3.HeaderText = "Importe";
            gridViewTextBoxColumn3.Name = "Importe";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn3.Width = 80;
            this.Impuestos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn1,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Impuestos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Impuestos.Name = "Impuestos";
            this.Impuestos.ShowGroupPanel = false;
            this.Impuestos.Size = new System.Drawing.Size(509, 102);
            this.Impuestos.TabIndex = 5;
            // 
            // TImpuestos
            // 
            this.TImpuestos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuestos.Etiqueta = "";
            this.TImpuestos.Location = new System.Drawing.Point(0, 0);
            this.TImpuestos.Name = "TImpuestos";
            this.TImpuestos.ShowActualizar = false;
            this.TImpuestos.ShowAutorizar = false;
            this.TImpuestos.ShowCerrar = false;
            this.TImpuestos.ShowEditar = false;
            this.TImpuestos.ShowExportarExcel = false;
            this.TImpuestos.ShowFiltro = true;
            this.TImpuestos.ShowGuardar = false;
            this.TImpuestos.ShowHerramientas = false;
            this.TImpuestos.ShowImagen = false;
            this.TImpuestos.ShowImprimir = false;
            this.TImpuestos.ShowNuevo = true;
            this.TImpuestos.ShowRemover = true;
            this.TImpuestos.Size = new System.Drawing.Size(509, 30);
            this.TImpuestos.TabIndex = 4;
            this.TImpuestos.Nuevo.Click += this.TImpuestos_Nuevo_Click;
            this.TImpuestos.Remover.Click += this.TImpuestos_Remover_Click;
            this.TImpuestos.Filtro.Click += this.TImpuestos_Filtro_Click;
            // 
            // ComprobanteImpuestosControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Impuestos);
            this.Controls.Add(this.TImpuestos);
            this.Name = "ComprobanteImpuestosControl";
            this.Size = new System.Drawing.Size(509, 132);
            this.Load += new System.EventHandler(this.ComprobanteImpuestosControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Impuestos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Impuestos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Telerik.WinControls.UI.RadGridView Impuestos;
        private Common.Forms.ToolBarStandarControl TImpuestos;
    }
}
