﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Comprobante.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public class ComprobanteFiscalGeneralForm : ComprobanteFiscalForm {
        protected internal RadMenuItem ComplementoRecepcionPago = new RadMenuItem { Text = "Recepción Pago", CheckOnClick = false };
        protected internal RadMenuItem ComplementoCartaPorte = new RadMenuItem { Text = "Carta Porte", CheckOnClick = false };

        public ComprobanteFiscalGeneralForm() : base() {

        }

        public ComprobanteFiscalGeneralForm(CFDISubTipoEnum subtTipo, int indice) : base(subtTipo, indice) {
            this.Load += this.ComprobanteFiscalGeneralForm_Load;
        }

        public ComprobanteFiscalGeneralForm(ComprobanteFiscalDetailModel comprobante, CFDISubTipoEnum subtTipo, int indice) : base(comprobante, subtTipo, indice) {
            this.Load += this.ComprobanteFiscalGeneralForm_Load;
        }

        private void ComprobanteFiscalGeneralForm_Load(object sender, EventArgs e) {
            this.ComprobanteControl.TComprobante.Complemento.Items.Clear();
            this.ComprobanteControl.TComprobante.Complemento.Enabled = true;
            this.ComprobanteControl.TComprobante.Complemento.Items.AddRange(new RadMenuItem[] { this.ComplementoRecepcionPago, this.ComplementoCartaPorte });
            this.ComprobanteControl.BindingCompleted += this.ComprobanteControl_BindingCompleted;
            this.ComplementoRecepcionPago.Click += this.ComplementoRecepcionPago_Click;
            this.ComplementoCartaPorte.Click += this.ComplementoCartaPorte_Click;
            this.ComprobanteControl.General.TipoComprobante_Change += this.General_TipoComprobante_Change;
            this.ComprobanteControl.TComprobante.Complemento.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        }

        private void General_TipoComprobante_Change(object sender, CFDITipoComprobanteModel e) {
            if (e.Id == (int)CFDITipoComprobanteEnum.Pagos) {
                this.ComplementoAgregar(this.ControlPago());
            }
        }

        private void ComplementoCartaPorte_Click(object sender, EventArgs e) {
        }

        private void ComplementoRecepcionPago_Click(object sender, EventArgs e) {
        }

        private void ComprobanteControl_BindingCompleted(object sender, EventArgs e) {

            foreach (var item in this.TabControl.Pages) {
                foreach (var item1 in item.Controls) {
                    if (item1.GetType() == typeof(ComplementoPagoControl)) {
                        if (this.ComprobanteControl.Comprobante.RecepcionPago == null) {
                            this.ComprobanteControl.Comprobante.RecepcionPago = new System.ComponentModel.BindingList<Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel>();
                        }

                        if (this.ComprobanteControl.Comprobante.RecepcionPago.Count == 0) {
                            this.ComprobanteControl.Comprobante.RecepcionPago.Add(new Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel());
                        }

                        ((ComplementoPagoControl)item1).Complemento = this.ComprobanteControl.Comprobante.RecepcionPago[0];
                    }

                    if (item1.GetType() == typeof(ComplementoCartaPorteControl) | item1.GetType() == typeof(ComplementoPagoControl)) {
                        ((IComplementoControl)item1).CreateBinding();
                        ((IComplementoControl)item1).Editable = this.ComprobanteControl.Comprobante.IsEditable;
                    }
                }
            }
        }

        private Control ControlPago() {
            var _complemento = new ComplementoPagoControl() { Dock = DockStyle.Fill };
            _complemento.Start();
            _complemento.Complemento = new Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel();
            return _complemento;
        }

        private Control ControlCartaPorte() {
            var _complemento = new ComplementoCartaPorteControl() { Dock = DockStyle.Fill };
            _complemento.Complemento = this.ComprobanteControl.Comprobante.CartaPorte;
            _complemento.Start();
            return _complemento;
        }

        private bool Existe(object o) {
            foreach (var item in this.TabControl.Pages) {
                foreach (var item1 in item.Controls) {
                    if (item1.GetType() == o.GetType()) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
