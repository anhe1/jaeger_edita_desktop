﻿using System;
using System.ComponentModel;
using Telerik.Pivot.Core;
using Telerik.Pivot.Core.Aggregates;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesFiscalesResumenForm : RadForm {
        protected CFDISubTipoEnum subTipo;
        private IComprobantesFiscalesService service;
        private BindingList<EstadoCuentaSingleView> _DataSource;
        private LocalDataSourceProvider dataProvider;

        public ComprobantesFiscalesResumenForm(CFDISubTipoEnum subTipo, UIMenuElement menuElement) {
            InitializeComponent();
            this.subTipo = subTipo;
        }

        private void ComprobantesFiscalesResumenForm_Load(object sender, EventArgs e) {
            var receptor = new PropertyGroupDescription() { PropertyName = "Receptor", GroupComparer = new GroupNameComparer() };
            var acumulado = new PropertyAggregateDescription() { PropertyName = "Acumulado", AggregateFunction = AggregateFunctions.Sum, CustomName = "Cobrado", StringFormat = "N2" };
            var total = new PropertyAggregateDescription() { PropertyName = "Total", AggregateFunction = AggregateFunctions.Sum, CustomName = "Por Cobrar", StringFormat = "N2" };

            if (this.subTipo == CFDISubTipoEnum.Recibido) {
                receptor.CustomName = "Emisor";
                acumulado.CustomName = "Pagado";
                total.CustomName = "Por Pagar";
            }

            this.dataProvider = new LocalDataSourceProvider();
            this.dataProvider.Culture = new System.Globalization.CultureInfo("es-MX");
            this.dataProvider.AggregateDescriptions.Add(total);
            this.dataProvider.AggregateDescriptions.Add(acumulado);
            this.dataProvider.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() { PropertyName = "FechaEmision", CustomName = "Año", Step = DateTimeStep.Year, GroupComparer = new GroupNameComparer() });
            this.dataProvider.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() { PropertyName = "FechaEmision", CustomName = "Mes", Step = DateTimeStep.Month, GroupComparer = new GroupNameComparer() });
            this.dataProvider.RowGroupDescriptions.Add(receptor);
            this.radPivotGrid1.DataProvider = this.dataProvider;
            this.service = new ComprobantesFiscalesService(this.subTipo);
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Obteniendo infomración ...";
                espera.ShowDialog(this);
            }
            this.radPivotGrid1.DataSource = this._DataSource;
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            this._DataSource = this.service.GetResumen(this.ToolBar.GetEjercicio());
        }
    }
}
