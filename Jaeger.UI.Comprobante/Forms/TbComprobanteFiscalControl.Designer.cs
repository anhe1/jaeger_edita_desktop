﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class TbComprobanteFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TbComprobanteFiscalControl));
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarComprobanteFiscal = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Emisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.Status = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Certificar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.UrlFilePDF = new Telerik.WinControls.UI.CommandBarButton();
            this.UrlFileXML = new Telerik.WinControls.UI.CommandBarButton();
            this.SendEmail = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Complemento = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.LabelUuid = new Telerik.WinControls.UI.CommandBarLabel();
            this.IdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(1345, 55);
            this.RadCommandBar1.TabIndex = 4;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarComprobanteFiscal});
            this.CommandBarRowElement1.Text = "";
            // 
            // ToolBarComprobanteFiscal
            // 
            this.ToolBarComprobanteFiscal.DisplayName = "Emision de Comprobante";
            this.ToolBarComprobanteFiscal.EnableFocusBorder = false;
            this.ToolBarComprobanteFiscal.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Emisor,
            this.Separador,
            this.ToolLabelStatus,
            this.Status,
            this.Nuevo,
            this.Duplicar,
            this.Guardar,
            this.Actualizar,
            this.Certificar,
            this.Cancelar,
            this.UrlFilePDF,
            this.UrlFileXML,
            this.SendEmail,
            this.Separator2,
            this.Complemento,
            this.CommandBarSeparator2,
            this.LabelUuid,
            this.IdDocumento,
            this.Cerrar});
            this.ToolBarComprobanteFiscal.Name = "ToolBarComprobanteFiscal";
            // 
            // 
            // 
            this.ToolBarComprobanteFiscal.OverflowButton.Enabled = true;
            this.ToolBarComprobanteFiscal.ShowHorizontalLine = false;
            this.ToolBarComprobanteFiscal.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarComprobanteFiscal.GetChildAt(2))).Enabled = true;
            // 
            // Emisor
            // 
            this.Emisor.DefaultItem = null;
            this.Emisor.DisplayName = "Emisor";
            this.Emisor.DrawText = true;
            this.Emisor.Image = global::Jaeger.UI.Comprobante.Properties.Resources.administrator_male_16px;
            this.Emisor.Name = "Emisor";
            this.Emisor.Text = "XXXXXXXXXXXXXX";
            this.Emisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            this.ToolLabelStatus.UseCompatibleTextRendering = false;
            // 
            // Status
            // 
            this.Status.AutoCompleteDisplayMember = "Descripcion";
            this.Status.AutoCompleteValueMember = "Id";
            this.Status.DisplayMember = "Descripcion";
            this.Status.DisplayName = "Descripcion";
            this.Status.DrawImage = false;
            this.Status.DrawText = true;
            this.Status.DropDownAnimationEnabled = true;
            this.Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Status.Image = ((System.Drawing.Image)(resources.GetObject("Status.Image")));
            this.Status.MaxDropDownItems = 0;
            this.Status.Name = "Status";
            this.Status.Text = "";
            this.Status.ValueMember = "Id";
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Comprobante.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Nuevo.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.copy_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Duplicar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Enabled = false;
            this.Guardar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.save_16px;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Certificar
            // 
            this.Certificar.DisplayName = "Timbrar";
            this.Certificar.DrawText = true;
            this.Certificar.Enabled = false;
            this.Certificar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.approval_16px;
            this.Certificar.Name = "Certificar";
            this.Certificar.Text = "Timbrar";
            this.Certificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Enabled = false;
            this.Cancelar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // UrlFilePDF
            // 
            this.UrlFilePDF.DisplayName = "Archivo PDF";
            this.UrlFilePDF.DrawText = true;
            this.UrlFilePDF.Enabled = false;
            this.UrlFilePDF.Image = global::Jaeger.UI.Comprobante.Properties.Resources.pdf_16px;
            this.UrlFilePDF.Name = "UrlFilePDF";
            this.UrlFilePDF.Text = "PDF";
            this.UrlFilePDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // UrlFileXML
            // 
            this.UrlFileXML.DisplayName = "Archivo XML";
            this.UrlFileXML.DrawText = true;
            this.UrlFileXML.Enabled = false;
            this.UrlFileXML.Image = global::Jaeger.UI.Comprobante.Properties.Resources.xml_file_16px;
            this.UrlFileXML.Name = "UrlFileXML";
            this.UrlFileXML.Text = "XML";
            this.UrlFileXML.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // SendEmail
            // 
            this.SendEmail.DisplayName = "Correo";
            this.SendEmail.DrawText = true;
            this.SendEmail.Enabled = false;
            this.SendEmail.Image = global::Jaeger.UI.Comprobante.Properties.Resources.email_document_16px;
            this.SendEmail.Name = "SendEmail";
            this.SendEmail.Text = "Envíar";
            this.SendEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Complemento
            // 
            this.Complemento.DisplayName = "Series y Folios";
            this.Complemento.DrawText = true;
            this.Complemento.Enabled = false;
            this.Complemento.Image = global::Jaeger.UI.Comprobante.Properties.Resources.plugin_16px;
            this.Complemento.Name = "Complemento";
            this.Complemento.Text = "Complementos";
            this.Complemento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // LabelUuid
            // 
            this.LabelUuid.DisplayName = "UUID";
            this.LabelUuid.Name = "LabelUuid";
            this.LabelUuid.Text = "UUID:";
            this.LabelUuid.VisibleInOverflowMenu = false;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DisplayName = "IdDocumento";
            this.IdDocumento.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.IdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Text = "";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbComprobanteFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RadCommandBar1);
            this.Name = "TbComprobanteFiscalControl";
            this.Size = new System.Drawing.Size(1345, 30);
            this.Load += new System.EventHandler(this.ToolBarComprobanteFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBarComprobanteFiscal;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.CommandBarLabel LabelUuid;
        protected internal Telerik.WinControls.UI.CommandBarSplitButton Emisor;
        protected internal Telerik.WinControls.UI.CommandBarDropDownList Status;
        protected internal Telerik.WinControls.UI.CommandBarButton Nuevo;
        protected internal Telerik.WinControls.UI.CommandBarButton Duplicar;
        protected internal Telerik.WinControls.UI.CommandBarButton Guardar;
        protected internal Telerik.WinControls.UI.CommandBarButton Certificar;
        protected internal Telerik.WinControls.UI.CommandBarButton Cancelar;
        protected internal Telerik.WinControls.UI.CommandBarButton UrlFilePDF;
        protected internal Telerik.WinControls.UI.CommandBarButton UrlFileXML;
        protected internal Telerik.WinControls.UI.CommandBarButton SendEmail;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Complemento;
        protected internal Telerik.WinControls.UI.CommandBarTextBox IdDocumento;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
    }
}
