﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Catalogos;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Contribuyentes.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteContribuyenteControl : UserControl {
        private bool _ReadOnly = false;
        private bool _BindingUsoCFDI = true;
        protected internal IUsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        protected internal IRegimenesFiscalesCatalogo catalogoRegimenFiscal = new RegimenesFiscalesCatalogo();
        protected internal List<ContribuyenteModel> _DataSource;
        public IDirectorioService Service;
        public TipoRelacionComericalEnum RelacionComerical = TipoRelacionComericalEnum.Cliente;

        #region eventos
        public event EventHandler<ContribuyenteModel> Seleccionado;
        public void OnSeleccionadoClompleted(ContribuyenteModel e) {
            if (this.Seleccionado != null)
                this.Seleccionado(this, e);
        }
        #endregion

        public ComprobanteContribuyenteControl() {
            InitializeComponent();
        }

        private void ComprobanteContribuyenteControl_Load(object sender, EventArgs e) {
            this.RFC.TextBoxElement.ToolTipText = "Clave del Registro Federal de Contribuyentes correspondiente al contribuyente";
            this.NumRegIdTrib.TextBoxElement.ToolTipText = "Número de registro de identidad fiscal cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.";
            this.RegimenFiscal.MultiColumnComboBoxElement.TextBoxElement.ToolTipText = "Clave del régimen del contribuyente";
            this.DomicilioFiscal.TextBoxElement.ToolTipText = "Registrar el código postal del domicilio fiscal.";

            this.Actualizar.Click += this.Actualizar_Click;
            this.Buscar.Click += this.Buscar_Click;
            this.Agregar.Click += this.Agregar_Click;
            this.Nombre.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.StartsWith, ""));
            this.Nombre.AutoSizeDropDownColumnMode = BestFitColumnMode.AllCells;
            this.Nombre.AutoFilter = true;
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set {
                this._ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        public bool BindingUsoCFDI {
            get { return this._BindingUsoCFDI; }
            set { this._BindingUsoCFDI = value; }
        }

        public virtual void Start() {
            this.Service = new DirectorioService(this.RelacionComerical);

            // catalogo uso de cfdi
            this.catalogoUsoCfdi.Load();
            this.UsoCFDI.DataSource = catalogoUsoCfdi.Items;
            this.UsoCFDI.SelectedIndex = -1;
            this.UsoCFDI.SelectedValue = null;

            // regimen fiscal
            this.catalogoRegimenFiscal.Load();
            this.RegimenFiscal.DataSource = this.catalogoRegimenFiscal.Items;
            this.RegimenFiscal.SelectedIndex = -1;
            this.RegimenFiscal.SelectedValue = null;

            this.Nombre.DropDownOpened += this.Nombre_DropDownOpened;
            this.Nombre.SelectedIndexChanged += this.Nombre_SelectedValueChanged;
        }

        public virtual void OnLoad() {
            this.Actualizar.PerformClick();
            this.Nombre.DataSource = this._DataSource;
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            this.Actualizar.Enabled = false;
            this.LoadDataSource();

            this.Actualizar.Enabled = true;
        }

        public virtual void Buscar_Click(object sender, EventArgs e) {

        }

        public virtual void Agregar_Click(object sender, EventArgs e) {

        }

        public virtual void Nombre_DropDownOpened(object sender, EventArgs e) {

        }

        public virtual void Nombre_SelectedValueChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = this.Nombre.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var receptor = temporal.DataBoundItem as ContribuyenteModel;
                if (receptor != null) {
                    this.RFC.Text = receptor.RFC;
                    this.Nombre.Text = receptor.Nombre;
                    this.IdDirectorio.Value = receptor.IdDirectorio;

                    if (!string.IsNullOrEmpty(receptor.DomicilioFiscal)) {
                        this.DomicilioFiscal.Text = receptor.DomicilioFiscal;
                    }

                    if (!string.IsNullOrEmpty(receptor.ClaveUsoCFDI)) {
                        if (this._BindingUsoCFDI)
                            this.UsoCFDI.SelectedValue = receptor.ClaveUsoCFDI;
                    }

                    if (!string.IsNullOrEmpty(receptor.RegimenFiscal)) {
                        this.RegimenFiscal.SelectedValue = receptor.RegimenFiscal;
                    }
                    this.OnSeleccionadoClompleted(receptor);
                } else {
                    RadMessageBox.Show(this, Properties.Resources.Message_Objeto_NoValido, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void EditorControl_DropDownOpening(object sender, CancelEventArgs e) {
            e.Cancel = this._ReadOnly;
        }

        public virtual void CreateReadOnly() {
            if (this._ReadOnly) {
                this.Nombre.DropDownOpening += EditorControl_DropDownOpening;
                this.Nombre.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.Nombre.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                this.ResidenciaFiscal.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.ResidenciaFiscal.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                this.ResidenciaFiscal.DropDownOpening += EditorControl_DropDownOpening;

                this.RegimenFiscal.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.RegimenFiscal.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                this.RegimenFiscal.DropDownOpening += EditorControl_DropDownOpening;
                this.UsoCFDI.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.UsoCFDI.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                this.UsoCFDI.DropDownOpening += EditorControl_DropDownOpening;

                this.DomicilioFiscal.ReadOnly = this._ReadOnly;
                this.NumRegIdTrib.ReadOnly = this._ReadOnly;
                this.Buscar.Enabled = false;
                this.Actualizar.Enabled = false;
            } else {
                this.Nombre.DropDownOpening -= EditorControl_DropDownOpening;
                this.Nombre.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = false;
                this.Nombre.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;

                this.Buscar.Enabled = true;
                this.Actualizar.Enabled = true;
            }
        }

        public virtual void LoadDataSource() {
            this._DataSource = this.Service.GetList<ContribuyenteModel>(true);
        }

        private void ComprobanteContribuyenteControl_Resize(object sender, EventArgs e) {
            Panel4.Size = PanelNombre.Size;
            PanelResidencia.Width = PanelRFC.Width + 64;
            PanelResidencia.Location = new System.Drawing.Point(PanelRFC.Location.X, PanelResidencia.Location.Y);
            var d2 = Panel4.Size.Width / 2;
            PanelRegimen.Width = d2;
            PanelUso.Location = new System.Drawing.Point(PanelRegimen.Width - 1, PanelUso.Location.Y);
            PanelUso.Width = d2 + 4;
        }
    }
}
