﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Comprobante.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Util;

namespace Jaeger.UI.Comprobante.Forms {
    public class ComprobanteFiscalGridControl : GridCommonControl {
        private readonly bool IsBegingExported = false;
        private CFDISubTipoEnum _TipoComprobante;

        public IComprobantesFiscalesService _Service;

        protected internal ComprobanteFiscalGridBuilder.TempleteEnum _Tipo;
        protected internal GridViewTemplate Comprobantes = new GridViewTemplate() { Caption = "Documentos" };
        protected internal GridViewTemplate Conceptos = new GridViewTemplate() { Caption = "Conceptos" };
        // para mostrar informacion del tipo de relacion
        protected internal GridViewTemplate CFDIRelacionado = new GridViewTemplate() { Caption = "CFDI Relacionado" };
        protected internal GridViewTemplate CFDIRelacionadoDoctos = new GridViewTemplate() { Caption = "CFDI Relacionado: Comprobantes" };
        // para el complemento de pagos
        protected internal GridViewTemplate ComplementoPago = new GridViewTemplate() { Caption = "Complemento de Pago" };
        protected internal GridViewTemplate ComplementoPagosDoctoRelacionado = new GridViewTemplate() { Caption = "Complemento de Pago: Comprobantes" };
        protected internal GridViewTemplate ComplementoPagoD = new GridViewTemplate() { Caption = "Complemento de Pago Relacionado" };
        // opciones del menu contextual
        protected internal RadMenuItem ContextEstado = new RadMenuItem { Text = "Estado SAT", ToolTipText = "Consulta el estado del comprobante fiscal." };
        protected internal RadMenuItem ContextCargarXML = new RadMenuItem { Text = "Cargar XML", Name = "_upxml", Enabled = true, ToolTipText = "Carga de archivo XML" };
        protected internal RadMenuItem ContextCargarPDF = new RadMenuItem { Text = "Cargar PDF", Name = "_uppdf", ToolTipText = "Representación impresa PDF" };
        protected internal RadMenuItem ContextCargarAcuse = new RadMenuItem { Text = "Cargar Acuse de cancelación", Name = "_uppdf", ToolTipText = "Acuse de cancelación (PDF)", Enabled = false };
        protected internal RadMenuItem ContextCrearPDF = new RadMenuItem { Text = "Crear representación impresa" };
        protected internal RadMenuItem ContextSerializar = new RadMenuItem { Text = "Serializar XML" };
        protected internal RadMenuItem ContextSerializar_Conceptos = new RadMenuItem { Text = "Conceptos" };
        protected internal RadMenuItem ContextSerializar_Complemento = new RadMenuItem { Text = "Complemento" };
        protected internal RadMenuItem ContextCPago_Relacionar = new RadMenuItem { Text = "Buscar relación" };

        public event EventHandler<GridViewRowSourceNeededEventArgs> RowsNeeded;
        public void OnRowsNeeded(GridViewRowSourceNeededEventArgs e) {
            if (RowsNeeded != null)
                RowsNeeded(this, e);
        }

        public ComprobanteFiscalGridControl() : base() {
            this.Load += ComprobanteFiscalGridControl_Load;
        }

        private void ComprobanteFiscalGridControl_Load(object sender, EventArgs e) {
            this._Tipo = ComprobanteFiscalGridBuilder.TempleteEnum.None;

            this.Comprobantes.Standard();
            this.Conceptos.Standard();
            this.CFDIRelacionado.Standard();
            this.CFDIRelacionadoDoctos.Standard();
            this.ComplementoPago.Standard();
            this.ComplementoPagosDoctoRelacionado.Standard();
            this.ComplementoPagoD.Standard();

            this.ContextSerializar.Items.AddRange(this.ContextSerializar_Conceptos, this.ContextSerializar_Complemento);
            this.ContextCargar.Items.AddRange(this.ContextCargarXML, this.ContextCargarPDF, this.ContextCargarAcuse);
            this.ContextCargar.Enabled = true;

            this.ContextAcciones.Visibility = ElementVisibility.Visible;
            this.ContextAcciones.Items.Add(ContextSerializar);
            this.ContextAcciones.Items.Add(this.ContextEstado);
            this.ContextAcciones.Items.Add(this.ContextCrearPDF);

            this.ContextEstado.Click += this.ContextEstadoSAT_Click;
            this.ContextCrearPDF.Click += this.ContextCrearPDF_Click;
            this.ContextCargarXML.Click += this.ContextSubirXML_Click;
            this.ContextCargarPDF.Click += this.ContextSubirPDF_Click;
            this.ContextDescargar.Click += this.ContextDescargar_Click;
            this.ContextSerializar_Complemento.Click += this.ContextSerializarComplemento_Click;
            this.ContextSerializar_Conceptos.Click += this.ContextSerializarConceptos_Click;
        }

        public ComprobanteFiscalGridBuilder.TempleteEnum Templete {
            get { return this._Tipo; }
            set {
                this._Tipo = value;
                this.CreateView();
            }
        }

        public CFDISubTipoEnum TipoComprobante {
            get { return this._TipoComprobante; }
            set {
                this._TipoComprobante = value;
                this.CreateView();
            }
        }

        protected override void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridRowHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridFilterCellElement) {
            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate.Caption == this.Comprobantes.Caption) {
                    e.ContextMenu = this.menuContextual.DropDown;
                }
            }
        }

        protected virtual void CreateView() {
            if (this._Tipo == ComprobanteFiscalGridBuilder.TempleteEnum.None) { this.Clear(); return; }
            this.Clear();
            using (IComprobanteFiscalGridBuilder view = new ComprobanteFiscalGridBuilder()) {
                this.Conceptos.Columns.AddRange(view.Templetes().Conceptos().Build());
                this.CFDIRelacionado.Columns.AddRange(view.Templetes().CFDIRelaciondos().Build());
                this.CFDIRelacionadoDoctos.Columns.AddRange(view.Templetes().CFDIRelaciondosDoctos().Build());
                this.ComplementoPago.Columns.AddRange(view.Templetes().ComplementoPago().Build());
                this.ComplementoPagosDoctoRelacionado.Columns.AddRange(view.Templetes().DocumentoRelacionado().Build());
                this.ComplementoPagoD.Columns.AddRange(view.Templetes().ComplementoPagoD().Build());

                if (this.Templete == ComprobanteFiscalGridBuilder.TempleteEnum.EdoCuenta) {
                    if (this.TipoComprobante == CFDISubTipoEnum.Recibido) {
                        this.GridData.Columns.AddRange(view.Templetes(ComprobanteFiscalGridBuilder.TempleteEnum.EdoCuenta).Recibido().Build());
                    } else {
                        this.GridData.Columns.AddRange(view.Templetes(ComprobanteFiscalGridBuilder.TempleteEnum.EdoCuenta).Emitido().Build());
                    }
                    this.Comprobantes.Columns.AddRange(view.Templetes().Single().Build());
                    this.GridData.MasterTemplate.Templates.AddRange(new GridViewTemplate[] { this.Comprobantes });
                    this.Comprobantes.Templates.AddRange(new GridViewTemplate[] { this.Conceptos, this.CFDIRelacionado, this.ComplementoPago, this.ComplementoPagoD });
                } else if (this.Templete == ComprobanteFiscalGridBuilder.TempleteEnum.ComplementoPago) {
                    if (this.TipoComprobante == CFDISubTipoEnum.Recibido) {
                        this.GridData.Columns.AddRange(view.Templetes(ComprobanteFiscalGridBuilder.TempleteEnum.ComplementoPago).Recibido().Build());
                    } else {
                        this.GridData.Columns.AddRange(view.Templetes(ComprobanteFiscalGridBuilder.TempleteEnum.ComplementoPago).Emitido().Build());
                    }
                    this.GridData.MasterTemplate.Templates.AddRange(new GridViewTemplate[] { this.Conceptos, this.CFDIRelacionado, this.ComplementoPago, this.ComplementoPagoD });
                } else {
                    if (this.TipoComprobante == CFDISubTipoEnum.Recibido) {
                        this.GridData.Columns.AddRange(view.Templetes(ComprobanteFiscalGridBuilder.TempleteEnum.Standar).Recibido().Build());
                    } else {
                        this.GridData.Columns.AddRange(view.Templetes(ComprobanteFiscalGridBuilder.TempleteEnum.Standar).Emitido().Build());
                    }
                    this.GridData.MasterTemplate.Templates.AddRange(new GridViewTemplate[] { this.Conceptos, this.CFDIRelacionado, this.ComplementoPago, this.ComplementoPagoD });
                }

            }

            this.ContextSerializar.Visibility = ElementVisibility.Collapsed;
            this.ContextCPago_Relacionar.Visibility = ElementVisibility.Collapsed;
            this.CFDIRelacionado.Templates.Add(this.CFDIRelacionadoDoctos);
            this.ComplementoPago.Templates.Add(this.ComplementoPagosDoctoRelacionado);
            this.Comprobantes.HierarchyDataProvider = new GridViewEventDataProvider(this.Comprobantes);
            this.CFDIRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.CFDIRelacionado);
            this.CFDIRelacionadoDoctos.HierarchyDataProvider = new GridViewEventDataProvider(this.CFDIRelacionadoDoctos);
            this.Conceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.Conceptos);
            this.ComplementoPago.HierarchyDataProvider = new GridViewEventDataProvider(this.ComplementoPago);
            this.ComplementoPagosDoctoRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.ComplementoPagosDoctoRelacionado);
            this.ComplementoPagoD.HierarchyDataProvider = new GridViewEventDataProvider(this.ComplementoPagoD);
        }

        protected virtual void Clear() {
            this.GridData.Columns.Clear();
            this.Conceptos.Columns.Clear();
            this.CFDIRelacionado.Columns.Clear();
            this.CFDIRelacionadoDoctos.Columns.Clear();
            this.ComplementoPago.Columns.Clear();
            this.ComplementoPagosDoctoRelacionado.Columns.Clear();
            this.ComplementoPagoD.Columns.Clear();
            this.Comprobantes.Columns.Clear();
            this.GridData.Templates.Clear();
            this.Comprobantes.Templates.Clear();
        }

        #region grid
        protected override void GridData_CellEditorInitialized(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    RadDropDownListEditor editor = (RadDropDownListEditor)e.ActiveEditor;
                    RadDropDownListEditorElement editorElement = (RadDropDownListEditorElement)editor.EditorElement;
                    editorElement.DisplayMember = "Descripcion";
                    editorElement.DataSource = ComprobanteFiscalService.GetStatus(this._Service.GetSubTipo);
                }
            }
        }

        protected override void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    var seleccionado = (Domain.Comprobante.Contracts.IComprobanteSingle)this.GridData.ReturnRowSelected();
                    // en caso de no tener ningun tipo de status
                    if (seleccionado.Status == null) {
                        e.Cancel = true;
                        this.GridData.CancelEdit();
                        return;
                    }

                    if (seleccionado.SubTipo == CFDISubTipoEnum.Emitido) {
                        e.Cancel = true;
                        return;
                    } else if (seleccionado.SubTipo == CFDISubTipoEnum.Recibido) {
                        var _status = (CFDIStatusRecibidoEnum)Enum.Parse(typeof(CFDIStatusRecibidoEnum), seleccionado.Status, true);
                        if (_status != CFDIStatusRecibidoEnum.Importado) {
                            e.Cancel = true;
                        }
                    } else {
                        e.Cancel = true;
                    }
                }
            }
        }

        protected override void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if ((string)e.Column.Tag == "Actualizar") {
                        using (var espera = new Waiting1Form(this.ActualizaStatus)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.Tag != null) {
                            if ((bool)this.Tag == false) {
                                MessageBox.Show("Error");
                                this.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        protected override void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Column == null) { return; }
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                            this.GridData.CancelEdit();
                        } else {
                            var seleccionado = (Domain.Comprobante.Contracts.IComprobanteSingle)this.GridData.ReturnRowSelected();
                            if (seleccionado.SubTipo == CFDISubTipoEnum.Recibido) {
                                var _status = (CFDIStatusRecibidoEnum)Enum.Parse(typeof(CFDIStatusRecibidoEnum), e.Value.ToString(), true);
                                if (_status == CFDIStatusRecibidoEnum.PorPagar) {

                                    e.Cancel = false;
                                    e.Column.Tag = "Actualizar";
                                } else {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (this.IsBegingExported)
                return;
            this.OnRowsNeeded(e);
            Console.WriteLine("Dos");
            if (e.Template.Caption == this.Conceptos.Caption) {
                var indice = this.GetIdComprobante(e.ParentRow);
                if (indice > 0) {
                    using (var espera = new Waiting1Form(this.GetConceptos)) {
                        espera.ShowDialog(this);
                    }
                    var tabla = this.GridData.Tag as BindingList<ComprobanteConceptoDetailModel>;
                    if (tabla != null) {
                        foreach (var item in tabla) {
                            var newRow = e.Template.Rows.NewRow();
                            newRow.Cells["NumPedido"].Value = item.NumPedido;
                            newRow.Cells["Cantidad"].Value = item.Cantidad;
                            newRow.Cells["ClaveUnidad"].Value = item.ClaveUnidad;
                            newRow.Cells["Unidad"].Value = item.Unidad;
                            newRow.Cells["ClaveProdServ"].Value = item.ClaveProdServ;
                            newRow.Cells["Descripcion"].Value = item.Descripcion;
                            newRow.Cells["ValorUnitario"].Value = item.ValorUnitario;
                            newRow.Cells["Descuento"].Value = item.Descuento;
                            newRow.Cells["SubTotal"].Value = item.SubTotal;
                            newRow.Cells["Importe"].Value = item.Importe;
                            newRow.Cells["ObjetoImp"].Value = item.ObjetoImp;
                            e.SourceCollection.Add(newRow);
                        }
                    }
                    this.GridData.Tag = null;
                }
            } else if (e.Template.Caption == this.CFDIRelacionado.Caption) {
                var rowView = e.ParentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                if (rowView != null) {
                    var d = rowView.CfdiRelacionados;
                    if (d != null) {
                        if (d.TipoRelacion.Clave != null) {
                            var newRow = e.Template.Rows.NewRow();
                            newRow.Cells["Clave"].Value = d.TipoRelacion.Clave;
                            e.SourceCollection.Add(newRow);
                        }
                    }
                }
            } else if (e.Template.Caption == this.CFDIRelacionadoDoctos.Caption) {
                // comprobantes relacionados al CFDI
                var otro = e.ParentRow.Parent as GridViewHierarchyRowInfo;
                var rowView = otro.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                if (rowView != null) {
                    var d = rowView.CfdiRelacionados;
                    if (d != null) {
                        if (d.CfdiRelacionado.Count > 0) {
                            if (d.CfdiRelacionado != null) {
                                foreach (var item in d.CfdiRelacionado) {
                                    var newRow = e.Template.Rows.NewRow();
                                    newRow.Cells["IdDocumento"].Value = item.IdDocumento;
                                    newRow.Cells["Folio"].Value = item.Folio;
                                    newRow.Cells["Serie"].Value = item.Serie;
                                    newRow.Cells["EmisorRFC"].Value = item.RFC;
                                    newRow.Cells["EmisorNombre"].Value = item.Nombre;
                                    newRow.Cells["Total"].Value = item.Total;
                                    e.SourceCollection.Add(newRow);
                                }
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.ComplementoPago.Caption) {
                //var rowView = e.ParentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                if (this.GridData.CurrentRow != null) {
                    var rowView = this.GetTipoComprobante(e.ParentRow);
                    if (rowView == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                        using (var espera = new Waiting1Form(this.GetComplementoPago)) {
                            espera.Text = "Complemento de pago ...";
                            espera.ShowDialog(this);
                        }

                        if (this.GridData.Tag != null) {
                            var p0 = this.GridData.Tag as List<ComplementoPagoDetailModel>;
                            if (p0 != null) {
                                if (p0.Count > 0) {
                                    foreach (var item in p0) {
                                        GridViewRowInfo row = e.Template.Rows.NewRow();
                                        row.Cells["FechaPagoP"].Value = item.FechaPagoP;
                                        row.Cells["FormaDePagoP"].Value = item.FormaDePagoP;
                                        row.Cells["MonedaP"].Value = item.MonedaP;
                                        row.Cells["TipoCambioP"].Value = item.TipoCambioP;
                                        row.Cells["CtaBeneficiario"].Value = item.CtaBeneficiario;
                                        row.Cells["CtaOrdenante"].Value = item.CtaOrdenante;
                                        row.Cells["NomBancoOrdExt"].Value = item.NomBancoOrdExt;
                                        row.Cells["NumOperacion"].Value = item.NumOperacion;
                                        row.Cells["Monto"].Value = item.Monto;
                                        row.Cells["TipoCadPago"].Value = item.TipoCadPago;
                                        row.Cells["RfcEmisorCtaBen"].Value = item.RfcEmisorCtaBen;
                                        row.Cells["RfcEmisorCtaOrd"].Value = item.RfcEmisorCtaOrd;
                                        row.Tag = item;
                                        e.SourceCollection.Add(row);
                                    }
                                }
                            }
                        }
                        this.GridData.Tag = null;
                    }
                }
            } else if (e.Template.Caption == this.ComplementoPagosDoctoRelacionado.Caption) {
                // documentos relacionados en el complemento de pagos 
                var rowView = e.ParentRow.Tag as ComplementoPagoDetailModel;
                if (!(rowView == null)) {
                    var pagos10 = rowView;
                    if (!(pagos10 == null)) {
                        if (pagos10.DoctoRelacionados != null) {
                            foreach (var item in pagos10.DoctoRelacionados) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["Folio"].Value = item.Folio;
                                row.Cells["SerieDR"].Value = item.SerieDR;
                                row.Cells["RFC"].Value = item.RFC;
                                row.Cells["Nombre"].Value = item.Nombre;
                                row.Cells["FechaEmision"].Value = item.FechaEmision;
                                row.Cells["ObjetoImpDR"].Value = item.ObjetoImpDR;
                                row.Cells["IdDocumentoDR"].Value = item.IdDocumentoDR;
                                row.Cells["FormaPagoP"].Value = item.FormaDePagoP;
                                row.Cells["MonedaDR"].Value = item.MonedaDR;
                                row.Cells["EquivalenciaDR"].Value = item.EquivalenciaDR;
                                row.Cells["MetodoPago"].Value = item.MetodoPago;
                                row.Cells["NumParcialidad"].Value = item.NumParcialidad;
                                row.Cells["ImpSaldoAnt"].Value = item.ImpSaldoAnt;
                                row.Cells["ImpPagado"].Value = item.ImpPagado;
                                row.Cells["ImpSaldoInsoluto"].Value = item.ImpSaldoInsoluto;
                                row.Cells["IdComprobanteR"].Value = item.IdComprobanteR;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.ComplementoPagoD.Caption) {
                var rowView = e.ParentRow; //.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                if (rowView != null) {
                    using (var espera = new Waiting1Form(this.GetComplementoPagoD)) {
                        espera.Text = "CFDI relacionados ...";
                        espera.ShowDialog(this);
                    }
                    var _cPago = (List<ComplementoPagoMergeDoctoModel>)this.GridData.Tag;
                    if (_cPago != null) {
                        if (_cPago.Count > 0) {
                            foreach (var item in _cPago) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["IdComprobanteR"].Value = item.IdComprobanteR;
                                row.Cells["Status"].Value = item.StatusP;
                                row.Cells["Folio"].Value = item.FolioP;
                                row.Cells["Serie"].Value = item.SerieP;
                                row.Cells["FechaEmision"].Value = item.FechaEmisionP;
                                row.Cells["FechaPago"].Value = item.FechaPagoP;
                                row.Cells["IdDocumento"].Value = item.IdDocumentoP;
                                row.Cells["FormaDePagoP"].Value = item.FormaDePagoP;
                                row.Cells["MonedaP"].Value = item.MonedaP;
                                row.Cells["TipoCambioP"].Value = item.TipoCambioP;
                                row.Cells["NumOperacion"].Value = item.NumOperacion;
                                row.Cells["NumParcialidad"].Value = item.NumParcialidad;
                                row.Cells["ImpSaldoAnt"].Value = item.ImpSaldoAnt;
                                row.Cells["ImpPagado"].Value = item.ImpPagado;
                                row.Cells["ImpSaldoInsoluto"].Value = item.ImpSaldoInsoluto;
                                row.Cells["Estado"].Value = item.EstadoP;
                                if (item.SubTipo == CFDISubTipoEnum.Emitido) {
                                    row.Cells["EmisorRFC"].Value = item.ReceptorRFCP;
                                    row.Cells["EmisorNombre"].Value = item.ReceptorNombreP;
                                } else {
                                    row.Cells["EmisorRFC"].Value = item.EmisorRFCP;
                                    row.Cells["EmisorNombre"].Value = item.EmisorNombreP;
                                }
                                row.Tag = item;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            }
        }

        protected override void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name == "UrlFileXml" || e.Column.Name == "UrlFilePdf") {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = this.Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name != "Status") {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// descarga de archivos 
        /// </summary>
        protected override void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    // solo para columnas con url del archivo PDF o XML
                    if (e.Column.Name.ToLower() == "urlfilexml" || e.Column.Name.ToLower() == "urlfilepdf") {
                        string link = (string)e.Value;
                        // validar la liga de descarga
                        if (ValidacionService.URL(link)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = Path.GetFileName(link),
                                Filter = string.Format("*{0}|*{0}", Path.GetExtension(link)),
                                DefaultExt = Path.GetExtension(link),
                                Title = "Descarga",
                            };
                            // por si cancela el usuario
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            // proceder a descargar
                            if (FileService.DownloadFile(link, savefiledialog.FileName)) {
                                if (RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region menu contextual
        protected virtual void ContextEstadoSAT_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.GetEstadoSAT)) {
                espera.Text = "Consultando estado, un momento por favor...";
                espera.ShowDialog(this);
            }
            var response = (string)this.ContextEstado.Tag;
            if (response != null)
                RadMessageBox.Show(this, response, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            this.ContextEstado.Tag = null;
        }

        protected virtual void ContextCrearPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                if (_seleccionado != null) {
                    if (_seleccionado.SubTipo == CFDISubTipoEnum.Recibido) {
                        var d = this._Service.Printer(_seleccionado);
                        var reporte = new ReporteForm(d);
                        reporte.Show();
                    } else {
                        if (ValidacionService.UUID(_seleccionado.IdDocumento)) {
                            this._Service.ToHtml(_seleccionado);
                        } else {
                            var r0 = this._Service.Printer(_seleccionado);
                            var reporte = new ReporteForm(r0);
                            reporte.Show();
                        }
                    }
                } else {
                    RadMessageBox.Show("No se selecciono un objeto válido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        protected virtual void ContextSubirXML_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() { Filter = "*.xml|*.XML", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            if (seleccionado != null) {
                if (ValidacionService.URL(seleccionado.UrlFileXML)) {
                    if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName, ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                }

                if (File.Exists(openFileDialog.FileName) == false) {
                    RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
            }
        }

        protected virtual void ContextSubirPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var openFileDialog = new OpenFileDialog() { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
                if (openFileDialog.ShowDialog() != DialogResult.OK) {
                    return;
                }

                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    if (ValidacionService.URL(seleccionado.UrlFilePDF)) {
                        if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName,
                            ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                            return;
                    }

                    if (File.Exists(openFileDialog.FileName) == false) {
                        RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                    this.ContextCargarPDF.Tag = openFileDialog.FileName;
                    using (var espera = new Waiting2Form(this.ReemplazaPDF)) {
                        espera.Text = "Reemplazando, espere un momento...";
                        espera.ShowDialog(this);
                    }

                    if (this.ContextCargarPDF.Tag != null) {
                        if ((bool)this.ContextCargarPDF.Tag == false) {
                            RadMessageBox.Show(this, "Ocurrio un error al procesar el archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                    }
                }
            }
        }

        protected virtual void ContextDescargar_Click(object sender, EventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
                if (folder.ShowDialog(this) != DialogResult.OK)
                    return;

                if (Directory.Exists(folder.SelectedPath) == false) {
                    RadMessageBox.Show(this, "No se encontro una ruta de descarga valida!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                this.ContextDescargar.Tag = folder.SelectedPath;

                using (var espera = new Waiting1Form(this.Descargar)) {
                    espera.Text = "Procesando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        protected virtual void ContextSerializarComplemento_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                using (var espera = new Waiting1Form(this.SerializarComplementoPagos)) {
                    espera.Text = "Creando ...";
                    espera.ShowDialog(this);
                }
                var row = ((GridViewHierarchyRowInfo)this.GridData.CurrentRow);
                foreach (var item in row.Views) {
                    item.Refresh();
                }
            }
        }

        protected virtual void ContextSerializarConceptos_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                using (var espera = new Waiting1Form(this.SerializarConceptos)) {
                    espera.Text = "Creando ...";
                    espera.ShowDialog(this);
                }
                var row = ((GridViewHierarchyRowInfo)this.GridData.CurrentRow);
                foreach (var item in row.Views) {
                    item.Refresh();
                }
            }
        }
        #endregion

        #region metodos
        protected virtual void ReemplazaPDF() {
            var f1 = (string)this.ContextCargarPDF.Tag;
            var fi = new FileInfo(f1);
            if (fi.Exists) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                var d0 = FileService.ReadFileB64(fi);
                var d1 = this._Service.Upload(d0, fi.FullName, seleccionado.KeyName() + ".pdf");
                if (ValidacionService.UUID(d1)) {
                    this.ContextCargarPDF.Tag = this._Service.UpdateUrlPdf(seleccionado.Id, d1);
                }
            }
        }

        protected virtual void GetEstadoSAT() {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                if (seleccionado != null) {
                    var response = this._Service.EstadoSAT(seleccionado.Id, seleccionado.EmisorRFC, seleccionado.ReceptorRFC, seleccionado.Total, seleccionado.IdDocumento);
                    seleccionado.Estado = response;
                    seleccionado.FechaEstado = DateTime.Now;
                    if (response.ToLower().Equals("cancelado")) {
                        seleccionado.Status = response;
                    }
                }
            }
        }

        protected virtual void GetComplementoPago() {
            var _seleccion = this.GridData.CurrentRow;//.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (_seleccion != null) {
                var q0 = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().WithIdComprobante(this.GetIdComprobante(this.GridData.CurrentRow)).OnlyActive().Build();
                this.GridData.Tag = this._Service.GetList<ComplementoPagoDetailModel>(q0).ToList();
            }
        }

        /// <summary>
        /// obtener listado de complemento de pago relacionado a un comprobante fiscal
        /// </summary>
        protected virtual void GetComplementoPagoD() {
            //var seleccion = this.GridData.CurrentRow;//.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            var id = this.GetIdComprobante(this.GridData.CurrentRow);
            var idDocumento = this.IdDocumento(this.GridData.CurrentRow);
            if (id > 0 && idDocumento != string.Empty) {
                var query = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().Vista().GetComplementoPagoD(id, idDocumento).Build();
                this.GridData.Tag = this._Service.GetList<ComplementoPagoMergeDoctoModel>(query).ToList();
            }
        }

        protected virtual void GetConceptos() {
            var indice = this.GetIdComprobante(this.GridData.CurrentRow);
            if (indice > 0) {
                var tabla = this._Service.GetConceptos(indice);
                this.GridData.Tag = tabla;
                return;
            }
            this.GridData.Tag = null;
        }

        protected virtual void Descargar() {
            var _carpeta = (string)this.ContextDescargar.Tag;
            var _seleccion = this.GetSelected();

            foreach (var item in _seleccion) {
                if (ValidacionService.URL(item.UrlFileXML)) {
                    FileService.DownloadFile(item.UrlFileXML, Path.Combine(_carpeta, Path.GetFileName(item.UrlFileXML)));
                }

                if (ValidacionService.URL(item.UrlFilePDF)) {
                    FileService.DownloadFile(item.UrlFilePDF, Path.Combine(_carpeta, Path.GetFileName(item.UrlFilePDF)));
                }
            }
        }

        private void SerializarComplementoPagos() {
            if (this.GridData.CurrentRow != null) {
                var seleccion = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
                //this._Service.Serializar(seleccion);
            }
        }

        private void SerializarConceptos() {
            var seleccion = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (seleccion != null) {
                //this._Service.SerializarConceptos(seleccion);
            }
        }

        private void ActualizaStatus() {
            var singleC = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (singleC != null) {
                if (singleC.Estado == "Cancelado")
                    singleC.Status = "Cancelado";
                if (this._Service.UpdateStatus(singleC.Id, singleC.Status)) {
                    //singleC.FechaModifica = DateTime.Now;
                    //singleC.FechaEntrega = DateTime.Now;
                }
                this.GridData.CurrentRow.Tag = "";
            }
        }

        protected virtual int GetIdComprobante(GridViewRowInfo rowInfo, string columnName = "IdComprobante") {
            var single = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (single != null) {
                return single.Id;
            }

            if (this.Search(rowInfo, columnName) == false)
                return -1;

            try {
                var d1 = rowInfo.Cells[columnName].Value;
                return (int)d1;
            } catch (Exception) {
                return -1;
            }
        }

        protected virtual int GetSubTipo(GridViewRowInfo rowInfo, string columnName = "IdSubTipo") {
            var single = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (single != null) {
                return (int)single.SubTipo;
            }
            try {
                var d1 = rowInfo.Cells[columnName].Value;
                return (int)d1;
            } catch (Exception) {
                return -1;
            }
        }

        protected virtual string IdDocumento(GridViewRowInfo rowInfo, string columnName = "IdDocumento") {
            var single = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (single != null) {
                return single.IdDocumento;
            }

            try {
                var d1 = rowInfo.Cells[columnName].Value;
                return (string)d1;
            } catch (Exception) {
                return string.Empty;
            }
        }

        protected virtual Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum GetTipoComprobante(GridViewRowInfo rowInfo, string columnName = "TipoComprobante") {
            var single = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (single != null) {
                return single.TipoComprobante;
            }

            try {
                var d1 = rowInfo.Cells[columnName].Value;
                return (Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum)d1;
            } catch (Exception) {
                return 0;
            }
        }

        protected virtual int IdDirectorio(GridViewRowInfo rowInfo, string columnName = "IdDirectorio") {
            var single = this.GridData.CurrentRow.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle;
            if (single != null) {
                return single.IdDirectorio;
            }

            try {
                var d1 = rowInfo.Cells[columnName].Value;
                return (int)d1;
            } catch (Exception) {
            }
            return -1;
        }

        protected virtual bool Search(GridViewRowInfo rowInfo, string columnName) {
            foreach (GridViewCellInfo cellInfo in rowInfo.Cells) {
                if (cellInfo.ColumnInfo.Name == columnName) {
                    return true;
                }
            }
            return false;
        }

        protected virtual List<Domain.Comprobante.Contracts.IComprobanteSingle> GetSelected() {
            return this.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(x => x.DataBoundItem as Domain.Comprobante.Contracts.IComprobanteSingle).ToList();
        }
        #endregion
    }
}
