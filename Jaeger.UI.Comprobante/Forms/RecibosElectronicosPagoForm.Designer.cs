﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class RecibosElectronicosPagoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TComprobantes = new Jaeger.UI.Comprobante.Forms.ComprobanteFiscalGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TComprobantes
            // 
            this.TComprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TComprobantes.Location = new System.Drawing.Point(0, 0);
            this.TComprobantes.Name = "TComprobantes";
            this.TComprobantes.PDF = "UrlFilePdf";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TComprobantes.Permisos = uiAction1;
            this.TComprobantes.ShowActualizar = true;
            this.TComprobantes.ShowAutosuma = true;
            this.TComprobantes.ShowCancelar = false;
            this.TComprobantes.ShowCerrar = true;
            this.TComprobantes.ShowEditar = false;
            this.TComprobantes.ShowEjercicio = true;
            this.TComprobantes.ShowExportarExcel = false;
            this.TComprobantes.ShowFiltro = true;
            this.TComprobantes.ShowHerramientas = false;
            this.TComprobantes.ShowImprimir = false;
            this.TComprobantes.ShowItem = false;
            this.TComprobantes.ShowNuevo = false;
            this.TComprobantes.ShowPeriodo = true;
            this.TComprobantes.ShowSeleccionMultiple = true;
            this.TComprobantes.Size = new System.Drawing.Size(1428, 450);
            this.TComprobantes.TabIndex = 0;
            this.TComprobantes.Templete = Jaeger.UI.Comprobante.Builder.ComprobanteFiscalGridBuilder.TempleteEnum.None;
            // 
            // RecibosElectronicosPagoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1428, 450);
            this.Controls.Add(this.TComprobantes);
            this.Name = "RecibosElectronicosPagoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RecibosEletronicosPagoForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RecibosElectronicosPagoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected ComprobanteFiscalGridControl TComprobantes;
    }
}