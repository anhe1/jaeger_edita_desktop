﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobantesFiscalesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition14 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn82 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn83 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn84 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn85 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn86 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn87 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn88 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn89 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn90 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn91 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn92 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn93 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn94 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn95 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn96 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn97 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn98 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject4 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition13 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobantesFiscalesForm));
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.gridConcepto = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridCFDIRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComprobanteRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComplementoPagos = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComplementoPagosDoctoRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComplementoPagoD = new Telerik.WinControls.UI.GridViewTemplate();
            this.menuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.Iconos = new System.Windows.Forms.ImageList(this.components);
            this.TComprobante = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.mContextualComplementoP = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComprobanteRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagosDoctoRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 31);
            // 
            // 
            // 
            this.GridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridConcepto,
            this.gridCFDIRelacionado,
            this.gridComplementoPagos,
            this.gridComplementoPagoD});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition14;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(1297, 617);
            this.GridData.TabIndex = 14;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.RowSourceNeeded += new Telerik.WinControls.UI.GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.GridData.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            // 
            // gridConcepto
            // 
            this.gridConcepto.Caption = "Conceptos";
            this.gridConcepto.ViewDefinition = tableViewDefinition8;
            // 
            // gridCFDIRelacionado
            // 
            this.gridCFDIRelacionado.Caption = "CFDI Relacionado";
            gridViewComboBoxColumn2.FieldName = "Clave";
            gridViewComboBoxColumn2.HeaderText = "Tipo de relación";
            gridViewComboBoxColumn2.Name = "Clave";
            gridViewComboBoxColumn2.Width = 250;
            this.gridCFDIRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn2});
            this.gridCFDIRelacionado.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridComprobanteRelacionado});
            this.gridCFDIRelacionado.ViewDefinition = tableViewDefinition10;
            // 
            // gridComprobanteRelacionado
            // 
            this.gridComprobanteRelacionado.Caption = "Comprobantes";
            gridViewTextBoxColumn50.FieldName = "UUID";
            gridViewTextBoxColumn50.HeaderText = "IdDocumento";
            gridViewTextBoxColumn50.Name = "UUID";
            gridViewTextBoxColumn50.Width = 220;
            gridViewTextBoxColumn51.FieldName = "RFC";
            gridViewTextBoxColumn51.HeaderText = "RFC";
            gridViewTextBoxColumn51.Name = "RFC";
            gridViewTextBoxColumn51.Width = 110;
            gridViewTextBoxColumn52.FieldName = "Nombre";
            gridViewTextBoxColumn52.HeaderText = "Nombre";
            gridViewTextBoxColumn52.Name = "Nombre";
            gridViewTextBoxColumn52.Width = 250;
            gridViewTextBoxColumn53.FieldName = "Serie";
            gridViewTextBoxColumn53.HeaderText = "Serie";
            gridViewTextBoxColumn53.Name = "Serie";
            gridViewTextBoxColumn53.Width = 85;
            gridViewTextBoxColumn54.FieldName = "Folio";
            gridViewTextBoxColumn54.HeaderText = "Folio";
            gridViewTextBoxColumn54.Name = "Folio";
            gridViewTextBoxColumn54.Width = 85;
            gridViewTextBoxColumn55.DataType = typeof(decimal);
            gridViewTextBoxColumn55.FieldName = "Total";
            gridViewTextBoxColumn55.FormatString = "{0:N2}";
            gridViewTextBoxColumn55.HeaderText = "Total";
            gridViewTextBoxColumn55.Name = "Total";
            gridViewTextBoxColumn55.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn55.Width = 85;
            this.gridComprobanteRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52,
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54,
            gridViewTextBoxColumn55});
            this.gridComprobanteRelacionado.ViewDefinition = tableViewDefinition9;
            // 
            // gridComplementoPagos
            // 
            this.gridComplementoPagos.Caption = "Complemento: Pagos";
            gridViewTextBoxColumn56.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn56.FieldName = "FechaPago";
            gridViewTextBoxColumn56.FormatString = "{0:d}";
            gridViewTextBoxColumn56.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn56.Name = "FechaPago";
            gridViewTextBoxColumn56.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn56.Width = 85;
            gridViewTextBoxColumn57.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn57.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn57.Name = "FormaDePagoP";
            gridViewTextBoxColumn58.FieldName = "MonedaP";
            gridViewTextBoxColumn58.HeaderText = "Moneda";
            gridViewTextBoxColumn58.Name = "MonedaP";
            gridViewTextBoxColumn59.DataType = typeof(decimal);
            gridViewTextBoxColumn59.FieldName = "TipoCambioP";
            gridViewTextBoxColumn59.FormatString = "{0:N2}";
            gridViewTextBoxColumn59.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn59.Name = "TipoCambioP";
            gridViewTextBoxColumn59.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn60.DataType = typeof(decimal);
            gridViewTextBoxColumn60.FieldName = "Monto";
            gridViewTextBoxColumn60.FormatString = "{0:N2}";
            gridViewTextBoxColumn60.HeaderText = "Monto";
            gridViewTextBoxColumn60.Name = "Monto";
            gridViewTextBoxColumn60.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn60.Width = 85;
            gridViewTextBoxColumn61.FieldName = "NumOperacion";
            gridViewTextBoxColumn61.HeaderText = "Num. Operación";
            gridViewTextBoxColumn61.Name = "NumOperacion";
            gridViewTextBoxColumn61.Width = 80;
            gridViewTextBoxColumn62.FieldName = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn62.HeaderText = "RFC Emisor Cta. Ord.";
            gridViewTextBoxColumn62.Name = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn62.Width = 80;
            gridViewTextBoxColumn63.FieldName = "NomBancoOrdExt";
            gridViewTextBoxColumn63.HeaderText = "Nom. Banco Ord. Ext.";
            gridViewTextBoxColumn63.Name = "NomBancoOrdExt";
            gridViewTextBoxColumn63.Width = 120;
            gridViewTextBoxColumn64.FieldName = "CtaOrdenante";
            gridViewTextBoxColumn64.HeaderText = "Cta. Ordenante";
            gridViewTextBoxColumn64.Name = "CtaOrdenante";
            gridViewTextBoxColumn64.Width = 100;
            gridViewTextBoxColumn65.FieldName = "RfcEmisorCtaBen";
            gridViewTextBoxColumn65.HeaderText = "RFC Emisor Cta. Ben.";
            gridViewTextBoxColumn65.Name = "RfcEmisorCtaBen";
            gridViewTextBoxColumn65.Width = 85;
            gridViewTextBoxColumn66.FieldName = "CtaBeneficiario";
            gridViewTextBoxColumn66.HeaderText = "Cta. Beneficiario";
            gridViewTextBoxColumn66.Name = "CtaBeneficiario";
            gridViewTextBoxColumn66.Width = 80;
            gridViewTextBoxColumn67.FieldName = "TipoCadPago";
            gridViewTextBoxColumn67.HeaderText = "Tipo Cad. Pago";
            gridViewTextBoxColumn67.Name = "TipoCadPago";
            gridViewTextBoxColumn67.Width = 80;
            this.gridComplementoPagos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewTextBoxColumn67});
            this.gridComplementoPagos.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridComplementoPagosDoctoRelacionado});
            this.gridComplementoPagos.ViewDefinition = tableViewDefinition12;
            // 
            // gridComplementoPagosDoctoRelacionado
            // 
            this.gridComplementoPagosDoctoRelacionado.Caption = "Documentos relacionados";
            gridViewTextBoxColumn68.FieldName = "Folio";
            gridViewTextBoxColumn68.HeaderText = "Folio";
            gridViewTextBoxColumn68.Name = "Folio";
            gridViewTextBoxColumn68.Width = 75;
            gridViewTextBoxColumn69.FieldName = "Serie";
            gridViewTextBoxColumn69.HeaderText = "Serie";
            gridViewTextBoxColumn69.Name = "Serie";
            gridViewTextBoxColumn69.Width = 75;
            gridViewTextBoxColumn70.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn70.FieldName = "FechaEmision";
            gridViewTextBoxColumn70.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn70.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn70.Name = "FechaEmision";
            gridViewTextBoxColumn70.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn70.Width = 70;
            gridViewTextBoxColumn71.FieldName = "IdDocumento";
            gridViewTextBoxColumn71.HeaderText = "IdDocumento";
            gridViewTextBoxColumn71.Name = "IdDocumento";
            gridViewTextBoxColumn71.Width = 225;
            gridViewTextBoxColumn72.FieldName = "MonedaDR";
            gridViewTextBoxColumn72.HeaderText = "Moneda";
            gridViewTextBoxColumn72.Name = "MonedaDR";
            gridViewTextBoxColumn73.DataType = typeof(decimal);
            gridViewTextBoxColumn73.FieldName = "EquivalenciaDR";
            gridViewTextBoxColumn73.FormatString = "{0:N2}";
            gridViewTextBoxColumn73.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn73.Name = "EquivalenciaDR";
            gridViewTextBoxColumn73.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn74.FieldName = "MetodoPago";
            gridViewTextBoxColumn74.HeaderText = "Método de Pago";
            gridViewTextBoxColumn74.Name = "MetodoPago";
            gridViewTextBoxColumn75.FieldName = "NumParcialidad";
            gridViewTextBoxColumn75.HeaderText = "Num. Parcialidad";
            gridViewTextBoxColumn75.Name = "NumParcialidad";
            gridViewTextBoxColumn75.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn76.DataType = typeof(decimal);
            gridViewTextBoxColumn76.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn76.FormatString = "{0:N2}";
            gridViewTextBoxColumn76.HeaderText = "Saldo Anterior";
            gridViewTextBoxColumn76.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn76.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn76.Width = 85;
            gridViewTextBoxColumn77.DataType = typeof(decimal);
            gridViewTextBoxColumn77.FieldName = "ImpPagado";
            gridViewTextBoxColumn77.FormatString = "{0:N2}";
            gridViewTextBoxColumn77.HeaderText = "Imp. Pagado";
            gridViewTextBoxColumn77.Name = "ImpPagado";
            gridViewTextBoxColumn77.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn77.Width = 85;
            gridViewTextBoxColumn78.DataType = typeof(decimal);
            gridViewTextBoxColumn78.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn78.FormatString = "{0:N2}";
            gridViewTextBoxColumn78.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn78.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn78.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn78.Width = 85;
            gridViewTextBoxColumn79.FieldName = "ObjetoImpDR";
            gridViewTextBoxColumn79.HeaderText = "Obj. Impuesto";
            gridViewTextBoxColumn79.Name = "ObjetoImpDR";
            gridViewTextBoxColumn79.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            conditionalFormattingObject3.ApplyToRow = true;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.Name = "Relacion";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.DimGray;
            conditionalFormattingObject3.TValue1 = "0";
            conditionalFormattingObject3.TValue2 = "0";
            gridViewTextBoxColumn80.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn80.DataType = typeof(int);
            gridViewTextBoxColumn80.FieldName = "IdComprobante";
            gridViewTextBoxColumn80.HeaderText = "IdComprobante";
            gridViewTextBoxColumn80.IsVisible = false;
            gridViewTextBoxColumn80.Name = "IdComprobante";
            gridViewTextBoxColumn80.VisibleInColumnChooser = false;
            this.gridComplementoPagosDoctoRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn68,
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74,
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79,
            gridViewTextBoxColumn80});
            this.gridComplementoPagosDoctoRelacionado.ViewDefinition = tableViewDefinition11;
            // 
            // gridComplementoPagoD
            // 
            this.gridComplementoPagoD.Caption = "Complemento: Pagos Relacionado";
            gridViewTextBoxColumn81.FieldName = "StatusP";
            gridViewTextBoxColumn81.HeaderText = "Status";
            gridViewTextBoxColumn81.Name = "StatusP";
            gridViewTextBoxColumn81.Width = 75;
            gridViewTextBoxColumn82.FieldName = "FolioP";
            gridViewTextBoxColumn82.HeaderText = "Folio";
            gridViewTextBoxColumn82.Name = "FolioP";
            gridViewTextBoxColumn83.FieldName = "SerieP";
            gridViewTextBoxColumn83.HeaderText = "Serie";
            gridViewTextBoxColumn83.Name = "SerieP";
            gridViewTextBoxColumn84.FieldName = "EmisorRFCP";
            gridViewTextBoxColumn84.HeaderText = "RFC";
            gridViewTextBoxColumn84.Name = "EmisorRFCP";
            gridViewTextBoxColumn84.Width = 95;
            gridViewTextBoxColumn85.FieldName = "EmisorNombreP";
            gridViewTextBoxColumn85.HeaderText = "Nombre";
            gridViewTextBoxColumn85.Name = "EmisorNombreP";
            gridViewTextBoxColumn85.Width = 200;
            gridViewTextBoxColumn86.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn86.FieldName = "FechaEmisionP";
            gridViewTextBoxColumn86.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn86.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn86.Name = "FechaEmisionP";
            gridViewTextBoxColumn86.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn86.Width = 75;
            gridViewTextBoxColumn87.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn87.FieldName = "FechaPago";
            gridViewTextBoxColumn87.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn87.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn87.Name = "FechaPago";
            gridViewTextBoxColumn87.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn87.Width = 75;
            gridViewTextBoxColumn88.FieldName = "IdDocumentoP";
            gridViewTextBoxColumn88.HeaderText = "IdDocumento";
            gridViewTextBoxColumn88.Name = "IdDocumentoP";
            gridViewTextBoxColumn88.Width = 220;
            gridViewTextBoxColumn89.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn89.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn89.Name = "FormaDePagoP";
            gridViewTextBoxColumn89.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn90.FieldName = "MonedaP";
            gridViewTextBoxColumn90.HeaderText = "Moneda";
            gridViewTextBoxColumn90.Name = "MonedaP";
            gridViewTextBoxColumn90.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn91.DataType = typeof(decimal);
            gridViewTextBoxColumn91.FieldName = "TipoCambioP";
            gridViewTextBoxColumn91.FormatString = "{0:N2}";
            gridViewTextBoxColumn91.HeaderText = "T. Cambio";
            gridViewTextBoxColumn91.Name = "TipoCambioP";
            gridViewTextBoxColumn91.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn92.FieldName = "NumOperacion";
            gridViewTextBoxColumn92.HeaderText = "Núm. Operación";
            gridViewTextBoxColumn92.Name = "NumOperacion";
            gridViewTextBoxColumn92.Width = 140;
            gridViewTextBoxColumn93.DataType = typeof(decimal);
            gridViewTextBoxColumn93.FieldName = "NumParcialidad";
            gridViewTextBoxColumn93.FormatString = "{0:N0}";
            gridViewTextBoxColumn93.HeaderText = "Parcialidad";
            gridViewTextBoxColumn93.Name = "NumParcialidad";
            gridViewTextBoxColumn93.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn94.DataType = typeof(decimal);
            gridViewTextBoxColumn94.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn94.FormatString = "{0:N2}";
            gridViewTextBoxColumn94.HeaderText = "Saldo Ant.";
            gridViewTextBoxColumn94.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn94.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn94.Width = 85;
            gridViewTextBoxColumn95.DataType = typeof(decimal);
            gridViewTextBoxColumn95.FieldName = "ImpPagado";
            gridViewTextBoxColumn95.FormatString = "{0:N2}";
            gridViewTextBoxColumn95.HeaderText = "Imp. Pagado";
            gridViewTextBoxColumn95.Name = "ImpPagado";
            gridViewTextBoxColumn95.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn95.Width = 85;
            gridViewTextBoxColumn96.DataType = typeof(decimal);
            gridViewTextBoxColumn96.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn96.FormatString = "{0:N2}";
            gridViewTextBoxColumn96.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn96.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn96.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn96.Width = 85;
            gridViewTextBoxColumn97.FieldName = "EstadoP";
            gridViewTextBoxColumn97.HeaderText = "Estado";
            gridViewTextBoxColumn97.Name = "EstadoP";
            gridViewTextBoxColumn97.Width = 75;
            conditionalFormattingObject4.ApplyToRow = true;
            conditionalFormattingObject4.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.Name = "Relacionado";
            conditionalFormattingObject4.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            conditionalFormattingObject4.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.TValue1 = "0";
            conditionalFormattingObject4.TValue2 = "0";
            gridViewTextBoxColumn98.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            gridViewTextBoxColumn98.DataType = typeof(int);
            gridViewTextBoxColumn98.FieldName = "IdComprobanteR";
            gridViewTextBoxColumn98.HeaderText = "Relacionado";
            gridViewTextBoxColumn98.IsVisible = false;
            gridViewTextBoxColumn98.Name = "IdComprobanteR";
            gridViewTextBoxColumn98.VisibleInColumnChooser = false;
            this.gridComplementoPagoD.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn81,
            gridViewTextBoxColumn82,
            gridViewTextBoxColumn83,
            gridViewTextBoxColumn84,
            gridViewTextBoxColumn85,
            gridViewTextBoxColumn86,
            gridViewTextBoxColumn87,
            gridViewTextBoxColumn88,
            gridViewTextBoxColumn89,
            gridViewTextBoxColumn90,
            gridViewTextBoxColumn91,
            gridViewTextBoxColumn92,
            gridViewTextBoxColumn93,
            gridViewTextBoxColumn94,
            gridViewTextBoxColumn95,
            gridViewTextBoxColumn96,
            gridViewTextBoxColumn97,
            gridViewTextBoxColumn98});
            this.gridComplementoPagoD.ViewDefinition = tableViewDefinition13;
            // 
            // Iconos
            // 
            this.Iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Iconos.ImageStream")));
            this.Iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.Iconos.Images.SetKeyName(0, "UrlFileXML");
            this.Iconos.Images.SetKeyName(1, "UrlFilePDF");
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.ShowActualizar = true;
            this.TComprobante.ShowAutosuma = true;
            this.TComprobante.ShowCancelar = true;
            this.TComprobante.ShowCerrar = true;
            this.TComprobante.ShowEditar = true;
            this.TComprobante.ShowEjercicio = true;
            this.TComprobante.ShowExportarExcel = true;
            this.TComprobante.ShowFiltro = true;
            this.TComprobante.ShowHerramientas = false;
            this.TComprobante.ShowImprimir = false;
            this.TComprobante.ShowItem = false;
            this.TComprobante.ShowNuevo = true;
            this.TComprobante.ShowPeriodo = true;
            this.TComprobante.Size = new System.Drawing.Size(1297, 31);
            this.TComprobante.TabIndex = 1;
            // 
            // ComprobantesFiscalesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1297, 648);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.TComprobante);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobantesFiscalesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Comprobantes Fiscales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobantesFiscalesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComprobanteRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagosDoctoRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList Iconos;
        public Common.Forms.ToolBarCommonControl TComprobante;
        private Telerik.WinControls.UI.GridViewTemplate gridConcepto;
        private Telerik.WinControls.UI.GridViewTemplate gridCFDIRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate gridComprobanteRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate gridComplementoPagos;
        private Telerik.WinControls.UI.GridViewTemplate gridComplementoPagosDoctoRelacionado;
        protected internal Telerik.WinControls.UI.RadContextMenu menuContextual;
        protected internal Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.GridViewTemplate gridComplementoPagoD;
        private Telerik.WinControls.UI.RadContextMenu mContextualComplementoP;
    }
}
