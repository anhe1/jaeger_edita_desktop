﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesBackupForm : RadForm {
        protected internal BindingList<ComprobanteBackup> datos;
        protected internal RadButtonElement bttnSelectPath = new RadButtonElement();
        protected internal IComprobantesFiscalesService service;
        private bool showCellBorders = false;
        private DashStyle lineStyle = DashStyle.Dot;
        private Color lineColor = Color.Black;

        public ComprobantesBackupForm() {
            InitializeComponent();
        }

        public ComprobantesBackupForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ComprobantesBackupForm_Load(object sender, EventArgs e) {
            this.datos = new BindingList<ComprobanteBackup>();
            this.service = new ComprobantesFiscalesService();
            this.GridData.AllowAddNewRow = false;
            this.GridData.TableElement.RowHeight = 40;
            this.GridData.ShowGroupPanel = false;
            this.GridData.ReadOnly = true;
            this.GridData.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.GridData.Relations.AddSelfReference(this.GridData.MasterTemplate, "Index", "Parent");
            this.GridData.TableElement.ShowSelfReferenceLines = true;
            this.GridData.EnableSorting = false;

            this.SubTipo.DisplayName = "Descripcion";
            this.SubTipo.ValueMember = "Id";
            this.SubTipo.DataSource = ConfigService.CFDISubTipo();

            this.TxbPath.CrearBoton(bttnSelectPath, "Seleccione");
            this.bttnSelectPath.Click += new EventHandler(this.SelectPath_Click);

            this.Descargar.Click += this.TBackup_Descargar_Click;
            this.Cerrar.Click += this.TBackup_Cerrar_Click;
        }

        private void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            GridDataCellElement dataCell = e.CellElement as GridDataCellElement;

            if (dataCell.ColumnInfo.Name == "Name") {
                GridViewDataRowInfo dataRow = dataCell.RowInfo as GridViewDataRowInfo;
                dataCell.ImageAlignment = ContentAlignment.MiddleLeft;
                if (dataRow == null)
                    return;
                string valueType = Convert.ToString(dataRow.Cells["Type"].Value).ToUpperInvariant();

                if (valueType.Contains("FILE") || valueType.Contains("DOCUMENT"))
                    dataCell.Image = this.ImageItems.Images[2];
                else if (dataRow.IsExpanded)
                    dataCell.Image = this.ImageItems.Images[1];
                else
                    dataCell.Image = this.ImageItems.Images[0];

                dataCell.TextImageRelation = TextImageRelation.ImageBeforeText;
            } else {
                dataCell.ResetValue(LightVisualElement.ImageProperty, ValueResetFlags.Local);
                dataCell.ResetValue(LightVisualElement.ImageAlignmentProperty, ValueResetFlags.Local);
                dataCell.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                dataCell.ResetValue(LightVisualElement.ImageLayoutProperty, ValueResetFlags.Local);
            }
        }

        private void GridData_ViewCellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Row is GridViewDataRowInfo) {
                if (showCellBorders)
                    e.CellElement.ResetValue(GridDataCellElement.DrawBorderProperty, ValueResetFlags.Local);
                else
                    e.CellElement.DrawBorder = false;

                GridDataCellElement cell = e.CellElement as GridDataCellElement;
                if (cell != null && cell.SelfReferenceLayout != null) {
                    foreach (RadElement element in cell.SelfReferenceLayout.StackLayoutElement.Children) {
                        GridLinkItem linkItem = element as GridLinkItem;
                        if (linkItem != null) {
                            linkItem.LineStyle = lineStyle;
                            linkItem.ForeColor = lineColor;
                        }
                        GridExpanderItem expanderItem = element as GridExpanderItem;
                        if (expanderItem != null) {
                            expanderItem.LinkLineStyle = lineStyle;
                            expanderItem.LinkLineColor = lineColor;
                        }
                    }
                }
            }
        }

        private void SelectPath_Click(object sender, EventArgs e) {
            this.TxbPath.Text = Extensions.DialogFolderOpen(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
        }

        private void TBackup_Descargar_Click(object sender, EventArgs eventArgs) {
            if (this.SubTipo.SelectedValue != null) {
                this.datos = this.service.GetBackups((CFDISubTipoEnum)this.SubTipo.SelectedValue);
                this.GridData.DataSource = this.datos;
            }
        }

        private void TBackup_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
