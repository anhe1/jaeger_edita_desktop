﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Aplication.Almacen.Contracts;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ProdServCatalogoBuscarForm : RadForm {
        protected IProdServsCatalogo catalogo;
        protected ICatalogoProductoService catalogoProductoService;
        protected ComprobanteConceptoDetailModel seleccionado;
        private BindingList<ProductoServicioModeloModel> resultados = new BindingList<ProductoServicioModeloModel>();

        public void OnAgregar(ComprobanteConceptoDetailModel e) {
            if (this.Agregar != null) {
                this.Agregar(this, e);
            }
        }

        public event EventHandler<ComprobanteConceptoDetailModel> Agregar;

        public ProdServCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new ProdServsCatalogo();
        }

        private void CatalogoProdServBuscarForm_Load(object sender, EventArgs e) {
            this.gridProductoCatalogo.Standard();
            this.gridProductoSAT.Standard();
            this.ToolBar.Autorizar.Text = "Seleccionar";
            this.ToolBar.Actualizar.Text = "Buscar";
            this.catalogo.Load();
            this.catalogoProductoService = new CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
        }

        #region catalogo de productos SAT
        private void TextBoxProductoSAT_TextChanged(object sender, EventArgs e) {
            if (this.textBoxProductoSAT.Text.Length >= 3) {
                this.gridProductoSAT.DataSource = this.catalogo.Productos(this.textBoxProductoSAT.Text);
            }
        }

        private void ComboBoxProducto_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {
            if (this.comboBoxProducto.Text == "Productos") {
                this.comboBoxDivision.DataSource = this.catalogo.Productos();
            } else if (this.comboBoxProducto.Text == "Servicios") {
                this.comboBoxDivision.DataSource = this.catalogo.Servicios();
            }
        }

        private void ComboBoxDivision_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.comboBoxDivision.SelectedItem != null) {
                var temporal = this.comboBoxDivision.SelectedItem as GridViewRowInfo;
                if (temporal != null) {
                    var item = temporal.DataBoundItem as ClaveProdServ;
                    if (item != null)
                        this.comboBoxGrupo.DataSource = this.catalogo.Grupo(item.Clave);
                }
            }
        }

        private void ComboBoxGrupo_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.comboBoxGrupo.SelectedItem != null) {
                this.comboBoxClase.DataSource = this.catalogo.Clases(this.comboBoxGrupo.SelectedValue.ToString());
            }
        }

        private void ComboBoxClase_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.comboBoxGrupo.SelectedItem != null) {
                this.comboBoxSubClase.DataSource = this.catalogo.Clases(this.comboBoxGrupo.SelectedValue.ToString());
            }
        }
        #endregion

        public virtual void Asignar_Click(object sender, EventArgs e) {
            if (this.radPageView.SelectedPage == this.PageBuscarTexto) {
                if (this.gridProductoSAT.CurrentRow != null) {
                    var s = this.gridProductoSAT.CurrentRow.DataBoundItem as ClaveProdServ;
                    if (s != null) {
                        this.seleccionado = new ComprobanteConceptoDetailModel {
                            Activo = true,
                            Cantidad = 0,
                            ClaveProdServ = s.Clave,
                            Descripcion = s.Descripcion
                        };
                        this.OnAgregar(seleccionado);
                    }
                }
            } else if (this.radPageView.SelectedPage == this.PageBuscarPorCatalogo) {
                var s = this.gridProductoCatalogo.CurrentRow.DataBoundItem as ProductoServicioModeloModel;
                if (s != null) {
                    this.seleccionado = new ComprobanteConceptoDetailModel {
                        Activo = true,
                        Cantidad = 0,
                        ClaveProdServ = s.ClaveProdServ,
                        ClaveUnidad = s.ClaveUnidad,
                        Descripcion = s.Descripcion,
                        ValorUnitario = s.Unitario,
                        Unidad = s.Unidad
                    };
                    this.OnAgregar(seleccionado);
                }
            }
        }

        private void ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TextBoxPorCatalogo_TextChanged(object sender, EventArgs e) {
            if (this.textBoxPorCatalogo.TextLength > 4) {
                if (this.Consulta.IsBusy == false) {
                    this.Consulta.RunWorkerAsync();
                }
            }
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e) {
            var d0 = CatalogoModeloService.Query().ByIdAlmacen(this.catalogoProductoService.Almacen).Activo().Search(this.textBoxPorCatalogo.Text).Build();
            this.resultados = new BindingList<ProductoServicioModeloModel>(this.catalogoProductoService.GetList<ProductoServicioModeloModel>(d0).ToList());
        }

        private void Consulta_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e) {
            this.gridProductoCatalogo.DataSource = resultados;
        }
    }
}
