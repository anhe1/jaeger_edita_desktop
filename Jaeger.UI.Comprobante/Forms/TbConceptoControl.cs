﻿using System;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class TbConceptoControl : UserControl {
        private bool isEditable = true;

        public bool IsEditable {
            get {
                return this.isEditable;
            }
            set {
                this.isEditable = value;
                this.SetEditable();
            }
        }

        public TbConceptoControl() {
            InitializeComponent();
        }
        
        private void ToolBarConceptoControl_Load(object sender, EventArgs e) {
            this.HostConceptos.HostedItem = this.ComboConceptos.MultiColumnComboBoxElement;
        }

        private void SetEditable() {
            this.ComboConceptos.SetEditable(this.IsEditable);
            this.Agregar.Enabled = this.isEditable;
            this.Unidades.Enabled = this.isEditable;
            this.Duplicar.Enabled = this.isEditable;
            this.Nuevo.Enabled = this.isEditable;
            this.Productos.Enabled = this.isEditable;
            this.Remover.Enabled = this.isEditable;
            this.Complementos.Enabled = this.isEditable;
        }
    }
}
