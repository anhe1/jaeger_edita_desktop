﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.UI.Comprobante.Forms {
    /// <summary>
    /// formulario de comprobantes fiscales recibidos
    /// </summary>
    public class ComprobantesFiscalesRecibidoForm : ComprobantesFiscalesForm {
        #region declaraciones
        protected internal RadMenuItem TComprobante_ValidacionImprimir = new RadMenuItem { Text = "Validación", ToolTipText = "Imprimir validación del comprobante" };
        protected internal RadMenuItem TComprobante_ValidacionDescargar = new RadMenuItem { Text = "Descargar validación" };
        protected internal RadMenuItem MContextual_ValidacionDescargar = new RadMenuItem { Text = "Descargar validación" };
        protected internal RadMenuItem MContextual_SerializarReceptor = new RadMenuItem { Text = "Obtener información del receptor", Name = "Receptor" };
        #endregion

        public ComprobantesFiscalesRecibidoForm() : base(CFDISubTipoEnum.Recibido) {
            this.Text = "Proveedores: Facturación";
            this.Load += ComprobantesFiscalesRecibidoForm_Load;
        }

        private void ComprobantesFiscalesRecibidoForm_Load(object sender, EventArgs e) {
            this.TComprobante.Editar.Text = "Ver";
            this.TComprobante.Cancelar.Text = "Rechazado";

            this.GridData.Columns["ReceptorNombre"].IsVisible = false;
            this.GridData.Columns["FechaUltimoPago"].HeaderText = "Ult. Pago";
            this.GridData.Columns["FechaEntrega"].HeaderText = "Fecha Recepción";
            this.GridData.Columns["Acumulado"].HeaderText = "Pagado";

            this.TComprobante.Nuevo.Visibility = ElementVisibility.Collapsed;
            this.TComprobante.Editar.Click += new EventHandler(this.TComprobante_Editar_Click);
            TComprobante_ValidacionDescargar.Click += TComprobante_ValidacionDescargar_Click;
            TComprobante_ValidacionImprimir.Click += TComprobante_ValidacionImprimir_Click;
            MContextual_ValidacionDescargar.Click += TComprobante_ValidacionDescargar_Click;
            MContextual_SerializarReceptor.Click += MContextual_SerializarReceptor_Click;

            this.TComprobante.Imprimir.Items.Add(TComprobante_ValidacionImprimir);
            this.TComprobante.Imprimir.Items.Add(TComprobante_ValidacionDescargar);
            this.TComprobante.ShowImprimir = true;
            this.menuContextual.Items.Add(MContextual_ValidacionDescargar);
            this.MContextual_Serializar.Items.Add(this.MContextual_SerializarReceptor);
        }

        #region barra de herramientas
        public virtual void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    var view = new ComprobanteFiscalGeneralForm(seleccionado.SubTipo, seleccionado.Id) { MdiParent = this.ParentForm };
                    view.Show();
                }
            }
        }

        protected virtual void TComprobante_ValidacionImprimir_Click(object sender, EventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                var seleccion =                this.GetSelected();
                if (seleccion != null) {
                    if (!string.IsNullOrEmpty(seleccion.JValidacion)) {
                        var val = ComprobanteValidacionPrinter.Json(seleccion.JValidacion);
                        if (val != null) {
                            var reporte = new ReporteForm(val);
                            reporte.ShowDialog(this);
                        }
                    } else {
                        RadMessageBox.Show(this, "No existe validación para el comprobante seleccionado.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                    }
                }
            }
        }

        public virtual void TComprobante_ValidacionDescargar_Click(object sender, EventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
                if (folder.ShowDialog(this) != DialogResult.OK)
                    return;

                if (Directory.Exists(folder.SelectedPath) == false) {
                    RadMessageBox.Show(this, Properties.Resources.Message_Error_RutaNoValida, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }

                // almacenamos la ruta
                this.Tag = folder.SelectedPath;

                using (var espera = new Waiting2Form(this.ValidacionPDF)) {
                    espera.Text = "Procesando ...";
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        #region menu contextual
        public virtual void MContextual_SerializarReceptor_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var d0 = this._Service.Serializar(this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel);
                if (d0 != null) {
                 
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void ValidacionPDF() {
            var seleccion = new List<ComprobanteFiscalDetailSingleModel>();
            // obtener la seleccion
            seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ComprobanteFiscalDetailSingleModel));
            if (seleccion != null) {
                foreach (var item in seleccion) {
                    var val = ComprobanteValidacionPrinter.Json(item.JValidacion);
                    if (val != null) {
                        Services.ComunService.ValidacionPDF(val, Path.Combine((string)this.Tag, val.KeyName() + ".pdf"));
                    }
                }
            }
        }

        public override void Consultar() {
            var d0 = ComprobantesFiscalesService.Query().Recibido().WithYear(this.TComprobante.GetEjercicio()).WithMonth(this.TComprobante.GetMes()).Build();
            this._DataSource = new BindingList<IComprobanteFiscalDetailSingleModel>(this._Service.GetList<ComprobanteFiscalDetailSingleModel>(d0).ToList<IComprobanteFiscalDetailSingleModel>());
        }
        #endregion
    }
}
