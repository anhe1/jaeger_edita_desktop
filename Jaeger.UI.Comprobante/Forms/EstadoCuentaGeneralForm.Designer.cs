﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class EstadoCuentaGeneralForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TComprobante = new Jaeger.UI.Comprobante.Forms.ComprobanteFiscalGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TComprobante.Permisos = uiAction1;
            this.TComprobante.ShowActualizar = true;
            this.TComprobante.ShowAutosuma = false;
            this.TComprobante.ShowCancelar = false;
            this.TComprobante.ShowCerrar = true;
            this.TComprobante.ShowEditar = true;
            this.TComprobante.ShowEjercicio = true;
            this.TComprobante.ShowExportarExcel = false;
            this.TComprobante.ShowFiltro = true;
            this.TComprobante.ShowHerramientas = false;
            this.TComprobante.ShowImprimir = false;
            this.TComprobante.ShowItem = false;
            this.TComprobante.ShowNuevo = true;
            this.TComprobante.ShowPeriodo = true;
            this.TComprobante.ShowSeleccionMultiple = true;
            this.TComprobante.Size = new System.Drawing.Size(1319, 569);
            this.TComprobante.TabIndex = 0;
            this.TComprobante.Templete = Jaeger.UI.Comprobante.Builder.ComprobanteFiscalGridBuilder.TempleteEnum.ComplementoPago;
            this.TComprobante.TipoComprobante = Jaeger.Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido;
            // 
            // EstadoCuentaGeneralForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1319, 569);
            this.Controls.Add(this.TComprobante);
            this.Name = "EstadoCuentaGeneralForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Estado de Cuenta";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EstadoCuentaGeneralForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected ComprobanteFiscalGridControl TComprobante;
    }
}