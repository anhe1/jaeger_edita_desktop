﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Util;

namespace Jaeger.UI.Comprobante.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources localResource = new EmbeddedResources("Jaeger.Domain.Comprobante");

        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) { }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(ComprobanteValidacionPrinter)) {
                this.CrearReporteValidacion();
            } else if (this.CurrentObject.GetType() == typeof(ComprobanteListadoPrinter)) {
                this.ComprobantesListadoReporte();
            } else if (this.CurrentObject.GetType() == typeof(ComprobanteFiscalPrinter)) {
                this.ComprobanteClasifica();
            }
        }

        private void CrearReporteValidacion() {
            var current = (ComprobanteValidacionPrinter)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobanteValidacion35Report.rdlc");
            this.SetDisplayName("Reporte Validación");
            var d = DbConvert.ConvertToDataTable<ComprobanteValidacionPrinter>(new List<ComprobanteValidacionPrinter>() { current });
            this.SetDataSource("Validacion", d);
            var d1 = DbConvert.ConvertToDataTable<PropertyValidate>(current.Resultados);
            this.SetDataSource("Resultados", d1);
            this.Finalizar();
        }

        private void ComprobantesListadoReporte() {
            var current = (ComprobanteListadoPrinter)this.CurrentObject;
            if (current.Vertical) {
                this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobantesListadoVReporte.rdlc");
            } else {
                this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Comprobante.Reports.ComprobantesListadoHReporte.rdlc");
            }
            this.SetDisplayName("Reporte 1");
            var d = DbConvert.ConvertToDataTable<ComprobanteFiscalDetailSingleModel>(current.Conceptos);
            this.Procesar();
            this.SetDataSource("Comprobantes", d);
            this.Finalizar();
        }

        private void ComprobanteClasifica() {
            var current = (ComprobanteFiscalPrinter)this.CurrentObject;
            if (current.EmisorRFC == this.RFC) {
                current.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido;
            } else {
                current.SubTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido;
            }
            ComprobanteFiscalReporte(ComprobanteTemplete());
        }

        private string ComprobanteTemplete() {
            var current = (ComprobanteFiscalPrinter)this.CurrentObject;
            if (current.SubTipo == Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) {
                if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Nomina) {
                    return "Jaeger.Domain.Comprobante.Reports.CFDIv0NominaReporte.rdlc";
                } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso | current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Egreso) {
                    if (current.InformacionGlobal != null) {
                        return "Jaeger.Domain.Comprobante.Reports.CFDIv0GlobalReporte.rdlc";
                    } else if (current.CartaPorte != null && current.Documento.ToLower() == "porte") {
                        return "Jaeger.Domain.Comprobante.Reports.CFDIv0CartaPorteReporte.rdlc";
                    } else {
                        return "Jaeger.Domain.Comprobante.Reports.CFDIv0Reporte.rdlc";
                    }
                } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                    return "Jaeger.Domain.Comprobante.Reports.CFDIv0PagoReporte.rdlc";
                }
            } else {
                if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Nomina) {
                    return "Jaeger.Domain.Comprobante.Reports.CFDIv0NominaReporte.rdlc";
                } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso | current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Egreso) {
                    if (current.InformacionGlobal != null) {
                        return "Jaeger.Domain.Comprobante.Reports.CFDIv1GlobalReporte.rdlc";
                    } else if (current.CartaPorte != null && current.Documento.ToLower() == "porte") {
                        return "Jaeger.Domain.Comprobante.Reports.CFDIv1CartaPorteReporte.rdlc";
                    } else {
                        return "Jaeger.Domain.Comprobante.Reports.CFDIv1Reporte.rdlc";
                    }
                } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                    return "Jaeger.Domain.Comprobante.Reports.CFDIv1PagoReporte.rdlc";
                }
            }
            return "Jaeger.Domain.Comprobante.Reports.CFDIv1Reporte.rdlc";
        }

        private void ComprobanteFiscalReporte(string templete) {
            var current = (ComprobanteFiscalPrinter)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream(templete);
            this.SetDisplayName("Comprobante");
            if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                var pago = DbConvert.ConvertToDataTable<ComplementoPagoDetailModel>(current.RecepcionPago);
                var doctosRelacionados = DbConvert.ConvertToDataTable<PagosPagoDoctoRelacionadoDetailModel>(current.RecepcionPago[0].DoctoRelacionados);
                this.SetDataSource("Pago", pago);
                this.SetDataSource("DoctoRelacionado", doctosRelacionados);
            } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Nomina) {
                this.SetDataSource("Nomina", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNomina>(new List<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNomina> { current.Nomina }));
                this.SetDataSource("Emisor", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaEmisor>(new List<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaEmisor> { current.Nomina.Emisor }));
                this.SetDataSource("Receptor", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaReceptor>(new List<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaReceptor> { current.Nomina.Receptor }));
                this.SetDataSource("Percepcion", DbConvert.ConvertToDataTable(current.Nomina.Percepciones.Percepcion));
                this.SetDataSource("Percepciones", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaPercepciones>(new List<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaPercepciones> { current.Nomina.Percepciones }));
                this.SetDataSource("Deduccion", DbConvert.ConvertToDataTable(current.Nomina.Deducciones.Deduccion));
                this.SetDataSource("Deducciones", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaDeducciones>(new List<Domain.Comprobante.Entities.Complemento.Nomina.ComplementoNominaDeducciones> { current.Nomina.Deducciones }));
                this.SetDataSource("OtroPago", DbConvert.ConvertToDataTable(current.Nomina.OtrosPagos));

            } else if ((current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso | current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Egreso) && current.InformacionGlobal == null && current.CartaPorte == null) {
            } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && current.InformacionGlobal != null && current.Documento != "porte") {
                var informacionGeneral = DbConvert.ConvertToDataTable<ComprobanteInformacionGlobalDetailModel>(new List<ComprobanteInformacionGlobalDetailModel>() { current.InformacionGlobal });
                this.SetDataSource("InformacionGlobal", informacionGeneral);
            } else if (current.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso && current.Documento == "porte") {
                var cartaPorte = DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel>(new List<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel>() { current.CartaPorte });
                this.SetDataSource("CartaPorte", cartaPorte);
                this.SetDataSource("Ubicaciones", DbConvert.ConvertToDataTable(current.CartaPorte.Ubicaciones));
                this.SetDataSource("FigurasTransporte", DbConvert.ConvertToDataTable(current.CartaPorte.FiguraTransporte));
                this.SetDataSource("Mercancia", DbConvert.ConvertToDataTable<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteMercancias>(new List<Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteMercancias> { current.CartaPorte.Mercancias }));
                this.SetDataSource("MercanciasMercancia", DbConvert.ConvertToDataTable(current.CartaPorte.Mercancias.Mercancia));

                this.SetParameter("ConfigVehicular", current.CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.ConfigVehicular);
                this.SetParameter("PlacaVM", current.CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.PlacaVM);
                this.SetParameter("AnioModeloVM", current.CartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM.ToString());

                this.SetParameter("PermSCT", current.CartaPorte.Mercancias.Autotransporte.PermSCT);
                this.SetParameter("NumPermisoSCT", current.CartaPorte.Mercancias.Autotransporte.NumPermisoSCT);

                this.SetParameter("AseguraRespCivil", current.CartaPorte.Mercancias.Autotransporte.Seguros.AseguraRespCivil);
                this.SetParameter("PolizaRespCivil", current.CartaPorte.Mercancias.Autotransporte.Seguros.PolizaRespCivil);
                this.SetParameter("AseguraMedAmbiente", current.CartaPorte.Mercancias.Autotransporte.Seguros.AseguraMedAmbiente);
                this.SetParameter("PolizaMedAmbiente", current.CartaPorte.Mercancias.Autotransporte.Seguros.PolizaMedAmbiente);
                this.SetParameter("AseguraCarga", current.CartaPorte.Mercancias.Autotransporte.Seguros.AseguraCarga);
                this.SetParameter("PolizaCarga", current.CartaPorte.Mercancias.Autotransporte.Seguros.PolizaCarga);
            }

            var comprobante = DbConvert.ConvertToDataTable<ComprobanteFiscalPrinter>(new List<ComprobanteFiscalPrinter>() { current });
            var conceptos = DbConvert.ConvertToDataTable<ComprobanteConceptoDetailModel>(current.Conceptos);
            var timbreFiscal = DbConvert.ConvertToDataTable<ComplementoTimbreFiscal>(new List<ComplementoTimbreFiscal>() { current.TimbreFiscal });

            this.Procesar();
            this.SetDataSource("Comprobante", comprobante);
            this.SetDataSource("Conceptos", conceptos);
            this.SetDataSource("TimbreFiscal", timbreFiscal);
            this.Finalizar();
        }
    }
}