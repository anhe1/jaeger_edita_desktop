﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteFiscalSerializerForm : RadForm {
        protected internal IComprobanteFiscalSerializerService Service;

        public ComprobanteFiscalSerializerForm() {
            InitializeComponent();
        }

        private void ComprobanteFiscalSerializerForm_Load(object sender, EventArgs e) {
            this.Service = new ComprobanteFiscalSerializerService();
            this.Ejercicio.Minimum = 2013;
            this.Ejercicio.Maximum = DateTime.Now.Year;
            this.Ejercicio.Value = DateTime.Now.Year;

            this.Periodo.DisplayMember = "Descripcion";
            this.Periodo.ValueMember = "Id";
            this.Periodo.DataSource = ConfigService.GetMeses();
            this.Periodo.SelectedIndex = DateTime.Now.Month;

            this.TipoComprobante.DisplayMember = "Descripcion";
            this.TipoComprobante.ValueMember = "Id";
            this.TipoComprobante.DataSource = ConfigService.CFDISubTipo();
        }

        private void Start_Click(object sender, EventArgs e) {
            this.Service.Serializar(this.GetEjercicio(), this.GetPeriodo());
        }

        private void Close_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected int GetPeriodo() {
            if (this.Periodo.SelectedValue != null) {
                return (int)this.Periodo.SelectedValue;
            } else {
                return (int)Enum.Parse(typeof(Domain.Base.ValueObjects.MesesEnum), this.Periodo.Text);
            }
        }

        protected int GetEjercicio() {
            return int.Parse(this.Ejercicio.Value.ToString());
        }
    }
}
