﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ProdServCatalogoCPBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.CatalogoSAT = new Jaeger.UI.Comprobante.Forms.ProdServCatalogoCPSATBuscarControl();
            this.SuspendLayout();
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "Buscar:";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowAgregar = true;
            this.TCatalogo.ShowBuscar = true;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowExistencia = false;
            this.TCatalogo.ShowFiltro = false;
            this.TCatalogo.Size = new System.Drawing.Size(494, 31);
            this.TCatalogo.TabIndex = 1;
            this.TCatalogo.ButtonAgregar_Click += new System.EventHandler<System.EventArgs>(this.Agregar_Click);
            this.TCatalogo.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Cerrar_Click);
            // 
            // CatalogoSAT
            // 
            this.CatalogoSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CatalogoSAT.Location = new System.Drawing.Point(0, 31);
            this.CatalogoSAT.Name = "CatalogoSAT";
            this.CatalogoSAT.Size = new System.Drawing.Size(494, 259);
            this.CatalogoSAT.TabIndex = 0;
            // 
            // ProdServCatalogoCPBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 290);
            this.Controls.Add(this.CatalogoSAT);
            this.Controls.Add(this.TCatalogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProdServCatalogoCPBuscarForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Productos y Servicios";
            this.Load += new System.EventHandler(this.ProdServCatalogoCPBuscarForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ProdServCatalogoCPSATBuscarControl CatalogoSAT;
        private Common.Forms.ToolBarStandarBuscarControl TCatalogo;
    }
}