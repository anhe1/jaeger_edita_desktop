﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Comprobante.Forms {
    public class ComprobanteFiscalRecepcionPagoForm : ComprobanteFiscalForm {
        private readonly ComplementoPagoControl Complemento = new ComplementoPagoControl() { Dock = DockStyle.Fill };

        public ComprobanteFiscalRecepcionPagoForm(CFDISubTipoEnum subtTipo, int indice) : base(subtTipo, indice) {
            this.Load += this.ComprobanteRecepcionPago_Load;
        }

        public ComprobanteFiscalRecepcionPagoForm(ComprobanteFiscalDetailModel comprobante, CFDISubTipoEnum subtTipo, int indice) : base(comprobante, subtTipo, indice) {
            this.Load += this.ComprobanteRecepcionPago_Load;
        }

        private void ComprobanteRecepcionPago_Load(object sender, EventArgs e) {
            this.ComprobanteControl.TipoComprobante = Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos;
            this.ComprobanteControl.General.Receptor.BindingUsoCFDI = false;
            this.Complemento.Start();
            this.ComplementoAgregar(this.Complemento);
            this.ComprobanteControl.BindingCompleted += this.General_BindingCompleted;
            this.TabControl.Controls.Remove(this.PageConceptos);
            this.ComprobanteControl.TabControl.Controls.Remove(this.ComprobanteControl.PageInformacionGeneral);
        }

        #region barra de herramientas
        public override void TComprobante_Guardar_Click(object sender, EventArgs e) {
            if (this.Complemento.Validar() == true) {
                base.TComprobante_Guardar_Click(sender, e);
            } else {
                RadMessageBox.Show(this, Properties.Resources.Message_Comprobante_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }
        #endregion

        private void General_BindingCompleted(object sender, EventArgs e) {
            this.Complemento.ReceptorRFC.DataBindings.Clear();
            this.Complemento.ReceptorRFC.DataBindings.Add("Text", this.ComprobanteControl.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
            if (this.ComprobanteControl.Comprobante.RecepcionPago == null) {
                RadMessageBox.Show(this, "El comprobante no cuenta con un complemento de pago asociado.", "Complemento de Pago", MessageBoxButtons.OK, RadMessageIcon.Error);
                this.ComprobanteControl.Comprobante.RecepcionPago = new BindingList<Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel> {
                    new Domain.Comprobante.Entities.Complemento.Pagos.ComplementoPagoDetailModel()
                };
            }
            this.Complemento.Complemento = this.ComprobanteControl.Comprobante.RecepcionPago[0];
            this.Complemento.CreateBinding();
            this.Complemento.Editable = this.ComprobanteControl.Comprobante.IsEditable;
        }
    }
}
