﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class ContribuyenteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContribuyenteForm));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.checkBoxExtranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.comboRegimen = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxRelacionComercial = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.textBoxClave = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNotas = new Telerik.WinControls.UI.RadTextBox();
            this.textBoxRFC = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.CboResidenciaFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.textBoxCURP = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.TxbNombre = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.CboRegimenFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.TxbTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.TxbSitioWeb = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.TxbCorreo = new Telerik.WinControls.UI.RadTextBox();
            this.CboUsoCFDI = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.textBoxNumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExtranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboRegimen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxRelacionComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSitioWeb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCFDI.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCFDI.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.checkBoxExtranjero);
            this.radPanel1.Controls.Add(this.comboRegimen);
            this.radPanel1.Controls.Add(this.radLabel5);
            this.radPanel1.Controls.Add(this.comboBoxRelacionComercial);
            this.radPanel1.Controls.Add(this.textBoxClave);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.radLabel19);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.TxbNotas);
            this.radPanel1.Controls.Add(this.textBoxRFC);
            this.radPanel1.Controls.Add(this.radLabel18);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.CboResidenciaFiscal);
            this.radPanel1.Controls.Add(this.textBoxCURP);
            this.radPanel1.Controls.Add(this.radLabel17);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Controls.Add(this.radLabel16);
            this.radPanel1.Controls.Add(this.TxbNombre);
            this.radPanel1.Controls.Add(this.radLabel15);
            this.radPanel1.Controls.Add(this.CboRegimenFiscal);
            this.radPanel1.Controls.Add(this.radLabel14);
            this.radPanel1.Controls.Add(this.TxbTelefono);
            this.radPanel1.Controls.Add(this.radLabel13);
            this.radPanel1.Controls.Add(this.TxbSitioWeb);
            this.radPanel1.Controls.Add(this.radLabel12);
            this.radPanel1.Controls.Add(this.TxbCorreo);
            this.radPanel1.Controls.Add(this.CboUsoCFDI);
            this.radPanel1.Controls.Add(this.textBoxNumRegIdTrib);
            this.radPanel1.Controls.Add(this.radLabel6);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 29);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(707, 205);
            this.radPanel1.TabIndex = 68;
            // 
            // checkBoxExtranjero
            // 
            this.checkBoxExtranjero.Location = new System.Drawing.Point(608, 58);
            this.checkBoxExtranjero.Name = "checkBoxExtranjero";
            this.checkBoxExtranjero.Size = new System.Drawing.Size(70, 18);
            this.checkBoxExtranjero.TabIndex = 68;
            this.checkBoxExtranjero.Text = "Extranjero";
            // 
            // comboRegimen
            // 
            this.comboRegimen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "NoDefinido";
            radListDataItem2.Text = "Fisica";
            radListDataItem3.Text = "Moral";
            this.comboRegimen.Items.Add(radListDataItem1);
            this.comboRegimen.Items.Add(radListDataItem2);
            this.comboRegimen.Items.Add(radListDataItem3);
            this.comboRegimen.Location = new System.Drawing.Point(413, 30);
            this.comboRegimen.Name = "comboRegimen";
            this.comboRegimen.NullText = "Selecciona";
            this.comboRegimen.Size = new System.Drawing.Size(118, 20);
            this.comboRegimen.TabIndex = 1;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(421, 6);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(50, 18);
            this.radLabel5.TabIndex = 67;
            this.radLabel5.Text = "Régimen";
            // 
            // comboBoxRelacionComercial
            // 
            this.comboBoxRelacionComercial.Location = new System.Drawing.Point(512, 83);
            this.comboBoxRelacionComercial.Name = "comboBoxRelacionComercial";
            this.comboBoxRelacionComercial.NullText = "Relación Comercial";
            this.comboBoxRelacionComercial.Size = new System.Drawing.Size(166, 20);
            this.comboBoxRelacionComercial.TabIndex = 48;
            // 
            // textBoxClave
            // 
            this.textBoxClave.Location = new System.Drawing.Point(41, 31);
            this.textBoxClave.Name = "textBoxClave";
            this.textBoxClave.NullText = "Clave";
            this.textBoxClave.Size = new System.Drawing.Size(83, 20);
            this.textBoxClave.TabIndex = 2;
            this.textBoxClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(41, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(35, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Clave:";
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(6, 163);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(81, 18);
            this.radLabel19.TabIndex = 53;
            this.radLabel19.Text = "Observaciones:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(130, 7);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(131, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "Reg. Fed. Contribuyentes";
            // 
            // TxbNotas
            // 
            this.TxbNotas.Location = new System.Drawing.Point(96, 162);
            this.TxbNotas.Name = "TxbNotas";
            this.TxbNotas.NullText = "Observaciones";
            this.TxbNotas.Size = new System.Drawing.Size(582, 20);
            this.TxbNotas.TabIndex = 43;
            // 
            // textBoxRFC
            // 
            this.textBoxRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxRFC.Location = new System.Drawing.Point(130, 31);
            this.textBoxRFC.MaxLength = 14;
            this.textBoxRFC.Name = "textBoxRFC";
            this.textBoxRFC.NullText = "RFC";
            this.textBoxRFC.Size = new System.Drawing.Size(131, 20);
            this.textBoxRFC.TabIndex = 4;
            this.textBoxRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(425, 110);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(68, 18);
            this.radLabel18.TabIndex = 64;
            this.radLabel18.Text = "Resid. Fiscal:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(275, 7);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(132, 18);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Clv. Única Reg. Población";
            // 
            // CboResidenciaFiscal
            // 
            // 
            // CboResidenciaFiscal.NestedRadGridView
            // 
            this.CboResidenciaFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboResidenciaFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboResidenciaFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboResidenciaFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboResidenciaFiscal.EditorControl.Name = "NestedRadGridView";
            this.CboResidenciaFiscal.EditorControl.ReadOnly = true;
            this.CboResidenciaFiscal.EditorControl.ShowGroupPanel = false;
            this.CboResidenciaFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboResidenciaFiscal.EditorControl.TabIndex = 0;
            this.CboResidenciaFiscal.Location = new System.Drawing.Point(512, 108);
            this.CboResidenciaFiscal.Name = "CboResidenciaFiscal";
            this.CboResidenciaFiscal.Size = new System.Drawing.Size(166, 20);
            this.CboResidenciaFiscal.TabIndex = 63;
            this.CboResidenciaFiscal.TabStop = false;
            // 
            // textBoxCURP
            // 
            this.textBoxCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxCURP.Location = new System.Drawing.Point(275, 31);
            this.textBoxCURP.Name = "textBoxCURP";
            this.textBoxCURP.NullText = "CURP";
            this.textBoxCURP.Size = new System.Drawing.Size(132, 20);
            this.textBoxCURP.TabIndex = 6;
            this.textBoxCURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(537, 8);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(106, 18);
            this.radLabel17.TabIndex = 62;
            this.radLabel17.Text = "Núm. Reg. Id. Fiscal:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(6, 59);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(86, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "Identidad Fiscal:";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(6, 110);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(68, 18);
            this.radLabel16.TabIndex = 61;
            this.radLabel16.Text = "Uso de CFDI";
            // 
            // TxbNombre
            // 
            this.TxbNombre.Location = new System.Drawing.Point(96, 57);
            this.TxbNombre.Name = "TxbNombre";
            this.TxbNombre.NullText = "Identidad Fiscal";
            this.TxbNombre.Size = new System.Drawing.Size(506, 20);
            this.TxbNombre.TabIndex = 8;
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(427, 84);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(79, 18);
            this.radLabel15.TabIndex = 60;
            this.radLabel15.Text = "Rel. Comercial:";
            // 
            // CboRegimenFiscal
            // 
            // 
            // CboRegimenFiscal.NestedRadGridView
            // 
            this.CboRegimenFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboRegimenFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboRegimenFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboRegimenFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboRegimenFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboRegimenFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboRegimenFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            this.CboRegimenFiscal.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.CboRegimenFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboRegimenFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboRegimenFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CboRegimenFiscal.EditorControl.Name = "NestedRadGridView";
            this.CboRegimenFiscal.EditorControl.ReadOnly = true;
            this.CboRegimenFiscal.EditorControl.ShowGroupPanel = false;
            this.CboRegimenFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboRegimenFiscal.EditorControl.TabIndex = 0;
            this.CboRegimenFiscal.Location = new System.Drawing.Point(96, 83);
            this.CboRegimenFiscal.Name = "CboRegimenFiscal";
            this.CboRegimenFiscal.NullText = "Régimen Fiscal";
            this.CboRegimenFiscal.Size = new System.Drawing.Size(323, 20);
            this.CboRegimenFiscal.TabIndex = 37;
            this.CboRegimenFiscal.TabStop = false;
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(425, 137);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(43, 18);
            this.radLabel14.TabIndex = 59;
            this.radLabel14.Text = "Correo:";
            // 
            // TxbTelefono
            // 
            this.TxbTelefono.Location = new System.Drawing.Point(62, 136);
            this.TxbTelefono.Name = "TxbTelefono";
            this.TxbTelefono.NullText = "Teléfono";
            this.TxbTelefono.Size = new System.Drawing.Size(114, 20);
            this.TxbTelefono.TabIndex = 44;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(182, 137);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(31, 18);
            this.radLabel13.TabIndex = 58;
            this.radLabel13.Text = "Sitio:";
            // 
            // TxbSitioWeb
            // 
            this.TxbSitioWeb.Location = new System.Drawing.Point(219, 136);
            this.TxbSitioWeb.Name = "TxbSitioWeb";
            this.TxbSitioWeb.NullText = "Sitio WEB";
            this.TxbSitioWeb.Size = new System.Drawing.Size(200, 20);
            this.TxbSitioWeb.TabIndex = 45;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(6, 137);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(52, 18);
            this.radLabel12.TabIndex = 57;
            this.radLabel12.Text = "Teléfono:";
            // 
            // TxbCorreo
            // 
            this.TxbCorreo.Location = new System.Drawing.Point(474, 136);
            this.TxbCorreo.Name = "TxbCorreo";
            this.TxbCorreo.NullText = "Correo Electrónico";
            this.TxbCorreo.Size = new System.Drawing.Size(204, 20);
            this.TxbCorreo.TabIndex = 46;
            // 
            // CboUsoCFDI
            // 
            // 
            // CboUsoCFDI.NestedRadGridView
            // 
            this.CboUsoCFDI.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboUsoCFDI.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboUsoCFDI.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboUsoCFDI.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboUsoCFDI.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboUsoCFDI.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboUsoCFDI.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            this.CboUsoCFDI.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.CboUsoCFDI.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboUsoCFDI.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboUsoCFDI.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.CboUsoCFDI.EditorControl.Name = "NestedRadGridView";
            this.CboUsoCFDI.EditorControl.ReadOnly = true;
            this.CboUsoCFDI.EditorControl.ShowGroupPanel = false;
            this.CboUsoCFDI.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboUsoCFDI.EditorControl.TabIndex = 0;
            this.CboUsoCFDI.Location = new System.Drawing.Point(96, 110);
            this.CboUsoCFDI.Name = "CboUsoCFDI";
            this.CboUsoCFDI.NullText = "Uso de CFDI";
            this.CboUsoCFDI.Size = new System.Drawing.Size(323, 20);
            this.CboUsoCFDI.TabIndex = 47;
            this.CboUsoCFDI.TabStop = false;
            // 
            // textBoxNumRegIdTrib
            // 
            this.textBoxNumRegIdTrib.Location = new System.Drawing.Point(537, 30);
            this.textBoxNumRegIdTrib.Name = "textBoxNumRegIdTrib";
            this.textBoxNumRegIdTrib.Size = new System.Drawing.Size(141, 20);
            this.textBoxNumRegIdTrib.TabIndex = 49;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(6, 84);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(80, 18);
            this.radLabel6.TabIndex = 50;
            this.radLabel6.Text = "Régimen Fiscal";
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = false;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowExportarExcel = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(707, 29);
            this.ToolBar.TabIndex = 69;
            // 
            // ContribuyenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 234);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.ToolBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContribuyenteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nuevo Receptor";
            this.Load += new System.EventHandler(this.ContribuyenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExtranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboRegimen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxRelacionComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboRegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSitioWeb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCFDI.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCFDI.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboUsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadCheckBox checkBoxExtranjero;
        private Telerik.WinControls.UI.RadDropDownList comboRegimen;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadCheckedDropDownList comboBoxRelacionComercial;
        private Telerik.WinControls.UI.RadTextBox textBoxClave;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox TxbNotas;
        private Telerik.WinControls.UI.RadTextBox textBoxRFC;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboResidenciaFiscal;
        private Telerik.WinControls.UI.RadTextBox textBoxCURP;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadTextBox TxbNombre;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboRegimenFiscal;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadTextBox TxbTelefono;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox TxbSitioWeb;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTextBox TxbCorreo;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CboUsoCFDI;
        private Telerik.WinControls.UI.RadTextBox textBoxNumRegIdTrib;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}
