﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteFiscalForm : RadForm {
        #region declaraciones
        protected internal RadMenuItem ComplementoACuentaTercero = new RadMenuItem { Text = "Complemento A Cuenta Tercero" };
        protected internal RadContextMenu menuContextualConceptos = new RadContextMenu();
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        private bool IsActiveEditorObjImpuesto = false;
        #endregion

        #region constructor
        public ComprobanteFiscalForm() {
            InitializeComponent();
        }

        public ComprobanteFiscalForm(CFDISubTipoEnum subtTipo, int indice) {
            InitializeComponent();
            this.ComprobanteControl.SubtTipo = subtTipo;
            this.ComprobanteControl.Indice = indice;
        }

        public ComprobanteFiscalForm(ComprobanteFiscalDetailModel comprobante, CFDISubTipoEnum subtTipo, int indice) {
            InitializeComponent();
            this.ComprobanteControl.Comprobante = comprobante;
            this.ComprobanteControl.SubtTipo = subtTipo;
            this.ComprobanteControl.Indice = indice;
        }
        
        #endregion

        private void ComprobanteForm_Load(object sender, EventArgs e) {
            this.OnStart.RunWorkerAsync();
            this.menuContextDuplicar.Image = Properties.Resources.copy_16px;
            this.menuContextualConceptos.Items.AddRange(this.menuContextAplicar, this.menuContextDuplicar);
            this.menuContextAplicar.Click += this.MenuContextAplicar_Click;
            this.menuContextDuplicar.Click += this.TConcepto_Duplicar_Click;
        }

        public virtual void TComprobante_Guardar_Click(object sender, EventArgs e) {
            this.ComprobanteControl.TComprobante_Guardar_Click(sender, e);
        }

        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region barra de herramientas conceptos del comprobante
        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.Conceptos.Count == 0) {
                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<ComprobanteConceptoDetailModel>();
                this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoInformacionAduanera.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoParte.DataMember = "Parte";
                this.GridConceptoImpuestos.DataMember = "Impuestos";
                this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";
            }

            this.ComprobanteControl.Comprobante.Conceptos.Add(new ComprobanteConceptoDetailModel { Activo = true });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                    if (seleccionado.Id == 0) {
                        try {
                            //this.GridConceptos.Rows.Remove(this.GridConceptos.CurrentRow);
                            this.ComprobanteControl.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.GridConceptos.Rows.Count == 0) {
                                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<ComprobanteConceptoDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.GridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.ComprobanteControl.Comprobante.Conceptos.Add(duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            using (var _catalogoProductos = new ProdServCatalogoBuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Agregar += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        public virtual void TConceptos_Agregar_Producto(object sender, ComprobanteConceptoDetailModel e) {
            if (this.ComprobanteControl.Comprobante.Conceptos.Count == 0) {
                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<ComprobanteConceptoDetailModel>();
                this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoInformacionAduanera.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoParte.DataMember = "Parte";
                this.GridConceptoImpuestos.DataMember = "Impuestos";
                this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";
            }
            if (e != null) {
                this.ComprobanteControl.Comprobante.Conceptos.Add(e);
            }
        }

        public virtual void TConceptos_Unidades_Click(object sender, EventArgs e) {
            using (var _unidad = new UnidadCatalogoBuscarForm()) {
                _unidad.StartPosition = FormStartPosition.CenterParent;
                _unidad.Selected += this.TConceptos_Agregar_Unidad_Click;
                _unidad.ShowDialog(this);
            }
        }

        public virtual void TConceptos_Agregar_Unidad_Click(object sender, Catalogos.Entities.ClaveUnidad e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (e != null) {
                    this.GridConceptos.CurrentRow.Cells["ClaveUnidad"].Value = e.Clave;
                }
            }
        }

        public virtual void TImpuestos_Agregar_Click(object sender, EventArgs e) {
            if (!(this.GridConceptos.CurrentRow == null)) {
                int index = this.GridConceptos.Rows.IndexOf(this.GridConceptos.CurrentRow);
                this.ComprobanteControl.Comprobante.Conceptos[index].Impuestos.Add(Aplication.Comprobante.Builder.ComprobanteFiscalBuilder.GetImpuestoTrasladoIVA(this.ComprobanteControl.Comprobante.Conceptos[index].Importe));
            }
        }

        public virtual void TImpuestos_Remover_Click(object sender, EventArgs e) {
            if (!(this.GridConceptoImpuestos.CurrentRow == null)) {
                if (this.GridConceptos.CurrentRow != null) {
                    var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoModel;
                    if (seleccionado != null) {
                        var impuesto = this.GridConceptoImpuestos.CurrentRow.DataBoundItem as ComprobanteConceptoImpuesto;
                        if (impuesto != null) {
                            try {
                                seleccionado.Impuestos.Remove(impuesto);
                            } catch (Exception ex) {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region menu contextual
        public virtual void MenuContextAplicar_Click(object sender, EventArgs e) {
            var d = this.GridConceptos.CurrentCell.Value;
            foreach (var item in this.GridConceptos.Rows) {
                item.Cells[this.GridConceptos.CurrentColumn.Index].Value = d;
            }
        }
        #endregion

        #region acciones del grid
        public virtual void GridConceptos_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (this.ComprobanteControl.Comprobante.IsEditable == false) {
                return;
            }
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridConceptos.CurrentRow.ViewInfo.ViewTemplate == this.GridConceptos.MasterTemplate) {
                    this.menuContextAplicar.Enabled = this.GridConceptos.CurrentColumn.Name == "ClaveUnidad" |
                        this.GridConceptos.CurrentColumn.Name == "Unidad" |
                        this.GridConceptos.CurrentColumn.Name == "NumPedido" |
                        this.GridConceptos.CurrentColumn.Name == "Cantidad" |
                        this.GridConceptos.CurrentColumn.Name == "ValorUnitario" |
                        this.GridConceptos.CurrentColumn.Name == "ClaveProducto" | 
                        this.GridConceptos.CurrentColumn.Name == "ObjetoImp";
                }
                e.ContextMenu = this.menuContextualConceptos.DropDown;
            }
        }
        #endregion

        #region conceptos
        public virtual void GridConceptos_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var _seleccionado1 = e.ParentRow.DataBoundItem as ComprobanteConceptoDetailModel;
            if (_seleccionado1 != null) {
                if (e.Template.Caption == this.GridACuentaTercero.Caption) {
                    var _seleccionado = _seleccionado1.ACuentaTerceros;
                    if (_seleccionado != null) {
                        var _newRow = e.Template.Rows.NewRow();
                        _newRow.Cells["RfcACuentaTerceros"].Value = _seleccionado.RfcACuentaTerceros;
                        _newRow.Cells["NombreACuentaTerceros"].Value = _seleccionado.NombreACuentaTerceros;
                        _newRow.Cells["RegimenFiscalACuentaTerceros"].Value = _seleccionado.RegimenFiscalACuentaTerceros;
                        _newRow.Cells["DomicilioFiscalACuentaTerceros"].Value = _seleccionado.DomicilioFiscalACuentaTerceros;
                        e.SourceCollection.Add(_newRow);
                    }
                }
            }
        }

        public virtual void GridConceptos_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Column.Name == "ObjetoImp") {
                if (this.IsActiveEditorObjImpuesto == false) {
                    var editor = (RadMultiColumnComboBoxElement)this.GridConceptos.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    var _col1 = new GridViewTextBoxColumn() { Name = "Id", HeaderText = "Clave", FieldName = "Id", Width = 40, TextAlignment = ContentAlignment.MiddleCenter };
                    var _col2 = new GridViewTextBoxColumn() { Name = "Descripcion", HeaderText = "Descripción", FieldName = "Descripcion", Width = 150 };
                    editor.EditorControl.Columns.AddRange(new GridViewDataColumn[] { _col1, _col2 });
                    editor.EditorControl.ShowRowHeaderColumn = false;
                    editor.AutoSizeDropDownToBestFit = true;
                    this.IsActiveEditorObjImpuesto = true;
                }
            }
        }
        #endregion

        #region A cuenta de terceros
        public virtual void ComplementoACuentaTercero_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                var _seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                if (_seleccionado != null) {
                    var _Complemento = new ACuentaTerceroForm(_seleccionado.ACuentaTerceros);
                    _Complemento.AgregarComplemento += Complemento_AgregarComplemento;
                    _Complemento.ShowDialog(this);
                } else {
                    RadMessageBox.Show(this, "No se selecciono un concepto válido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        public virtual void Complemento_AgregarComplemento(object sender, ComprobanteConceptoACuentaTerceros e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (e != null) {
                    var _seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ComprobanteConceptoDetailModel;
                    if (_seleccionado != null) {
                        _seleccionado.ACuentaTerceros = e;
                    }
                }
            }
        }
        #endregion

        #region eventos
        public virtual void TabControl_PageAdded(object sender, RadPageViewEventArgs e) {
            if (this.TabControl.Pages[this.TabControl.Pages.Count - 1].Name != null) {

            }
            //if (this.TabControl.Pages.Count == 1) {
            //    (this.TabControl.ViewElement as RadPageViewStripElement).ItemContainer.Visibility = ElementVisibility.Collapsed;
            //} else {
            //    (this.TabControl.ViewElement as RadPageViewStripElement).ItemContainer.Visibility = ElementVisibility.Visible;
            //}
        }

        public virtual void Comprobante_BindingCompleted(object sender, EventArgs e) {
            this.Text = "Comprobante: " + this.ComprobanteControl.Text;
            this.TConceptos.IsEditable = this.ComprobanteControl.Comprobante.IsEditable;
            this.ToolBarConceptoParte.IsEditable = this.ComprobanteControl.Comprobante.IsEditable;

            this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.GridConceptos.AllowEditRow = this.ComprobanteControl.Comprobante.IsEditable;

            this.GridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.GridConceptoParte.DataMember = "Parte";
            this.GridConceptoParte.AllowEditRow = this.ComprobanteControl.Comprobante.IsEditable;

            this.GridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.GridConceptoImpuestos.DataMember = "Impuestos";
            this.GridConceptoImpuestos.AllowEditRow = this.ComprobanteControl.Comprobante.IsEditable;

            this.GridConceptoInformacionAduanera.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";
            this.GridConceptoInformacionAduanera.AllowEditRow = this.ComprobanteControl.Comprobante.IsEditable;

            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "Descuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.ComprobanteControl.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "TrasladoIva", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIEPS.DataBindings.Clear();
            this.TrasladoIEPS.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "TrasladoIeps", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RetencionIva.DataBindings.Clear();
            this.RetencionIva.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "RetencionIva", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RetencionISR.DataBindings.Clear();
            this.RetencionISR.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "RetencionIsr", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TImpuestos.ReadOnly = !this.ComprobanteControl.Comprobante.IsEditable;
            this.TAduana.ReadOnly = !this.ComprobanteControl.Comprobante.IsEditable;
            this.TPredial.ReadOnly = !this.ComprobanteControl.Comprobante.IsEditable;
        }
        #endregion

        #region OnLoad
        private void OnStart_DoWork(object sender, DoWorkEventArgs e) {
            // crear el servicio
            this.ComprobanteControl.Start();
            this.ComprobanteControl.CFDIRelacionado.Start();
        }

        private void OnStart_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.ComprobanteControl.TComprobante.Cerrar.Click += this.TComprobante_Cerrar_Click;
            this.TConceptos.Complementos.Items.Add(this.ComplementoACuentaTercero);
            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;
            this.TConceptos.Unidades.Click += this.TConceptos_Unidades_Click;

            this.TImpuestos.Nuevo.Click += this.TImpuestos_Agregar_Click;
            this.TImpuestos.Remover.Click += this.TImpuestos_Remover_Click;

            this.ComplementoACuentaTercero.Click += ComplementoACuentaTercero_Click;
            this.GridConceptos.RowSourceNeeded += GridConceptos_RowSourceNeeded;
            this.GridACuentaTercero.Standard();
            this.GridACuentaTercero.HierarchyDataProvider = new GridViewEventDataProvider(this.GridACuentaTercero);
            this.TabControl.PageAdded += this.TabControl_PageAdded;
            this.TabControl.PageRemoved += this.TabControl_PageAdded;

            var _ObjetoImp = this.GridConceptos.Columns["ObjetoImp"] as GridViewMultiComboBoxColumn;
            _ObjetoImp.Tag = false;
            _ObjetoImp.DataSource = ComprobanteCommonService.GetObjetoImpuesto();
            this.ComprobanteControl.TComprobante.Actualizar.PerformClick();
        }
        #endregion

        public virtual void ComplementoAgregar(Control sender) {
            this.TabControl_PageAdded(sender, null);
            this.ComprobanteControl.TComprobante.Guardar.Click += this.TComprobante_Guardar_Click;
            var _pageView = new RadPageViewPage();
            _pageView.Controls.Add(sender);
            _pageView.Text = "Complemento: " + ((Services.IComplementoControl)sender).Caption;

            this.TabControl.Controls.Add(_pageView);
            this.TabControl.Refresh();
        }
        private bool autosuma = false;
        private void GridConceptos_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F11) {
                autosuma = !autosuma;
                this.GridConceptos.AutoSum(autosuma);
            }
        }
    }
}
