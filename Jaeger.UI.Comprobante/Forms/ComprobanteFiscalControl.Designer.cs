﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.PageComprobante = new Telerik.WinControls.UI.RadPageViewPage();
            this.PageCFDIRelacionado = new Telerik.WinControls.UI.RadPageViewPage();
            this.PageInformacionGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.General = new Jaeger.UI.Comprobante.Forms.ComprobanteGeneralControl();
            this.CFDIRelacionado = new Jaeger.UI.Comprobante.Forms.ComprobanteRelacionadoControl();
            this.InformacionGeneral = new Jaeger.UI.Comprobante.Forms.ComprobanteFiscalInformacionGeneralControl();
            this.TComprobante = new Jaeger.UI.Comprobante.Forms.TbComprobanteFiscalControl();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.PageComprobante.SuspendLayout();
            this.PageCFDIRelacionado.SuspendLayout();
            this.PageInformacionGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.PageComprobante);
            this.TabControl.Controls.Add(this.PageCFDIRelacionado);
            this.TabControl.Controls.Add(this.PageInformacionGeneral);
            this.TabControl.DefaultPage = this.PageComprobante;
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 30);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.PageComprobante;
            this.TabControl.Size = new System.Drawing.Size(1150, 150);
            this.TabControl.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).ItemAlignment = Telerik.WinControls.UI.StripViewItemAlignment.Near;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).ItemFitMode = Telerik.WinControls.UI.StripViewItemFitMode.Shrink;
            // 
            // PageComprobante
            // 
            this.PageComprobante.Controls.Add(this.General);
            this.PageComprobante.ItemSize = new System.Drawing.SizeF(81F, 24F);
            this.PageComprobante.Location = new System.Drawing.Point(10, 33);
            this.PageComprobante.Name = "PageComprobante";
            this.PageComprobante.Size = new System.Drawing.Size(1129, 106);
            this.PageComprobante.Text = "Comprobante";
            // 
            // PageCFDIRelacionado
            // 
            this.PageCFDIRelacionado.Controls.Add(this.CFDIRelacionado);
            this.PageCFDIRelacionado.ItemSize = new System.Drawing.SizeF(100F, 24F);
            this.PageCFDIRelacionado.Location = new System.Drawing.Point(10, 37);
            this.PageCFDIRelacionado.Name = "PageCFDIRelacionado";
            this.PageCFDIRelacionado.Size = new System.Drawing.Size(1329, 103);
            this.PageCFDIRelacionado.Text = "CFDI Relacionado";
            // 
            // PageInformacionGeneral
            // 
            this.PageInformacionGeneral.Controls.Add(this.InformacionGeneral);
            this.PageInformacionGeneral.ItemSize = new System.Drawing.SizeF(114F, 24F);
            this.PageInformacionGeneral.Location = new System.Drawing.Point(10, 37);
            this.PageInformacionGeneral.Name = "PageInformacionGeneral";
            this.PageInformacionGeneral.Size = new System.Drawing.Size(1329, 103);
            this.PageInformacionGeneral.Text = "Información General";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // General
            // 
            this.General.Dock = System.Windows.Forms.DockStyle.Fill;
            this.General.Location = new System.Drawing.Point(0, 0);
            this.General.Margin = new System.Windows.Forms.Padding(4);
            this.General.MaximumSize = new System.Drawing.Size(0, 102);
            this.General.MinimumSize = new System.Drawing.Size(0, 102);
            this.General.Name = "General";
            this.General.Size = new System.Drawing.Size(1129, 102);
            this.General.TabIndex = 1;
            // 
            // CFDIRelacionado
            // 
            this.CFDIRelacionado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CFDIRelacionado.Editable = true;
            this.CFDIRelacionado.Location = new System.Drawing.Point(0, 0);
            this.CFDIRelacionado.Name = "CFDIRelacionado";
            this.CFDIRelacionado.Size = new System.Drawing.Size(1329, 103);
            this.CFDIRelacionado.TabIndex = 0;
            this.CFDIRelacionado.ButtonIncluir_CheckStateChanged += new System.EventHandler<System.EventArgs>(this.TCFDIRelacionado_Incluir);
            // 
            // InformacionGeneral
            // 
            this.InformacionGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InformacionGeneral.Editable = false;
            this.InformacionGeneral.Location = new System.Drawing.Point(0, 0);
            this.InformacionGeneral.MinimumSize = new System.Drawing.Size(1200, 102);
            this.InformacionGeneral.Name = "InformacionGeneral";
            this.InformacionGeneral.Size = new System.Drawing.Size(1329, 103);
            this.InformacionGeneral.TabIndex = 0;
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.Size = new System.Drawing.Size(1150, 30);
            this.TComprobante.TabIndex = 2;
            // 
            // ComprobanteFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.TComprobante);
            this.MinimumSize = new System.Drawing.Size(1050, 180);
            this.Name = "ComprobanteFiscalControl";
            this.Size = new System.Drawing.Size(1150, 180);
            this.Load += new System.EventHandler(this.ComprobanteFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.PageComprobante.ResumeLayout(false);
            this.PageCFDIRelacionado.ResumeLayout(false);
            this.PageInformacionGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal ComprobanteGeneralControl General;
        internal ComprobanteRelacionadoControl CFDIRelacionado;
        private ComprobanteFiscalInformacionGeneralControl InformacionGeneral;
        protected internal TbComprobanteFiscalControl TComprobante;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageComprobante;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageCFDIRelacionado;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageInformacionGeneral;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}
