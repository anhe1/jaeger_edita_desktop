﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteBuscarControl : UserControl {
        protected IComprobantesFiscalesService _Service;
        private List<ComprobanteFiscalDetailSingleModel> _DataSource;
        private int idDirectorio = 0;
        private string rfc;
        private string status;

        public event EventHandler<ComprobanteFiscalDetailSingleModel> AgregarComprobante;
        public void OnAgregar(ComprobanteFiscalDetailSingleModel e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
            }
        }

        public void SetService(IComprobantesFiscalesService service) {
            this._Service = service;
        }

        public void SetFilter(ToggleState toggleState) {
            this.gridSearchResult.ActivateFilter(toggleState);
        }

        public void Search(int iddirectorio, string rfc, string status) {
            this.idDirectorio = iddirectorio;
            this.rfc = rfc;
            this.status = status;
            if (this.WorkerBuscar.IsBusy == false) {
                this.WorkerBuscar.RunWorkerAsync();
            }
        }

        public ComprobanteFiscalDetailSingleModel GetSelected() {
            if (this.gridSearchResult.CurrentRow != null) {
                var seleccionado = this.gridSearchResult.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    return seleccionado;
                }
            }
            return null;
        }

        public ComprobanteBuscarControl() {
            InitializeComponent();
        }

        private void ComprobanteBuscarControl_Load(object sender, EventArgs e) {
            this.Espera.AssociatedControl = this.gridSearchResult;
        }

        private void WorkerBuscar_DoWork(object sender, DoWorkEventArgs e) {
            if (this._Service.GetSubTipo == Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) {
                var q0 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Emitido().WithStatus(new string[] { this.status }).WithRFC(this.rfc).Build();
                this._DataSource = this._Service.GetList<ComprobanteFiscalDetailSingleModel>(q0).ToList();
            } else if (this._Service.GetSubTipo == Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido) {
                var q0 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Emitido().WithStatus(new string[] { this.status }).WithRFC(this.rfc).Build();
                this._DataSource = this._Service.GetList<ComprobanteFiscalDetailSingleModel>(q0).ToList();
            }
            //this._DataSource = this._Service.GetSearch(this.idDirectorio, this.rfc, new string[] { this.status });
            e.Result = true;
        }

        private void WorkerBuscar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.gridSearchResult.DataSource = this._DataSource;
            this.Espera.StopWaiting();
        }
    }
}
