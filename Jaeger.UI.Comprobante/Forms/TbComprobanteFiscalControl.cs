﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class TbComprobanteFiscalControl : UserControl {

        public TbComprobanteFiscalControl() {
            InitializeComponent();
        }

        private void ToolBarComprobanteFiscalControl_Load(object sender, EventArgs e) {
            this.ToolBarComprobanteFiscal.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        }
    }
}
