﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesFiscalesResumen2 : RadForm {
        private IComprobantesFiscalesService Service;
        private CFDISubTipoEnum Subtipo;
        private DataTable Datasource;
        private BackgroundWorker Preparar;
        private BackgroundWorker Consulta;
        private string[] _status;

        public ComprobantesFiscalesResumen2(CFDISubTipoEnum subtipo, UIMenuElement menuElement) {
            InitializeComponent();
            this.TReporte.ItemHost.HostedItem = this.RazonSocial.MultiColumnComboBoxElement;
            this.TReporte.ItemHost.MinSize = new System.Drawing.Size(355, 20);
            this.TReporte.ItemHost.MaxSize = new System.Drawing.Size(355, 20);
            this.TReporte.ItemHost.Size = new System.Drawing.Size(355, 20);
            this.Subtipo = subtipo;
        }

        private void ViewEstadoCredito_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            this.Consulta = new BackgroundWorker();
            this.Consulta.DoWork += Consulta_DoWork;
            this.Consulta.RunWorkerCompleted += Consulta_RunWorkerCompleted;

            this.Preparar = new BackgroundWorker();
            this.Preparar.DoWork += PrepararDoWork;
            this.Preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.Preparar.RunWorkerAsync();

            this.TReporte.Actualizar.Click += TReporte_Actualizar_Click;
            this.TReporte.Cerrar.Click += TReporte_Cerrar_Click;
        }

        private void PrepararDoWork(object sender, DoWorkEventArgs e) {
            this.Service = new ComprobantesFiscalesService(this.Subtipo);
            this.RazonSocial.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            this.RazonSocial.AutoFilter = true;
            this.RazonSocial.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.RazonSocial.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.GridData.AutoSum();
            this.TReporte.Periodo.DataSource = ComprobanteFiscalService.GetStatus(this.Subtipo);

            if (this.Subtipo == CFDISubTipoEnum.Emitido) {
                this.ChartView.Title = "Facturación vs Cobranza";
                var d0 = ComprobantesFiscalesService.Query().Emitido().Clientes().Build();

                this.RazonSocial.DataSource = this.Service.GetList<ComprobanteContribuyenteModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("_drctr_rlcn", "Cliente", Domain.Base.ValueObjects.ConditionalTypeEnum.In) });
                this._status = new string[] { CFDIStatusEmitidoEnum.PorCobrar.ToString(), CFDIStatusEmitidoEnum.Cobrado.ToString() };
            } else {
                this.ChartView.Title = "Fact. Proveedor vs Pagos";
                this.GridData.Columns["Acumulado"].HeaderText = "Pagado";

                var d0 = ComprobantesFiscalesService.Query().Recibido().Proveedores().Build();
                this.RazonSocial.DataSource = this.Service.GetList<ComprobanteContribuyenteModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("_drctr_rlcn", "Proveedor", Domain.Base.ValueObjects.ConditionalTypeEnum.In) });
                this._status = new string[] { CFDIStatusRecibidoEnum.PorPagar.ToString(), CFDIStatusRecibidoEnum.Pagado.ToString() };
            }
        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.RazonSocial.Enabled = true;
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e) {
            if (this.RazonSocial.SelectedItem != null) {
                var seleccionado = this.RazonSocial.SelectedItem as GridViewRowInfo;
                if (seleccionado != null) {
                    var d = seleccionado.DataBoundItem as ComprobanteContribuyenteModel;
                    this.Datasource = this.Service.GetResumen(this.TReporte.GetEjercicio(), _status, d.RFC);
                }
            }
        }

        private void Consulta_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = this.Datasource;

            var facturado = new BarSeries();
            facturado.ValueMember = "Ingreso";
            facturado.CategoryMember = "Mes";
            facturado.LegendTitle = "Facturado";
            facturado.LabelFormat = "{0:n}";
            facturado.ShowLabels = true;

            var cobrado = new BarSeries();
            cobrado.ValueMember = "Acumulado";
            cobrado.CategoryMember = "Mes";
            cobrado.LegendTitle = "Cobrado";
            cobrado.LabelFormat = "{0:n}";
            cobrado.ShowLabels = true;

            var saldo = new BarSeries();
            saldo.ValueMember = "Saldo";
            saldo.CategoryMember = "Mes";
            saldo.LegendTitle = "Saldo";
            saldo.LabelFormat = "{0:n}";
            saldo.ShowLabels = true;

            this.ChartView.Axes.Purge();
            this.ChartView.Series.Clear();
            this.ChartView.Invalidate();
            this.ChartView.Series.Add(facturado);
            this.ChartView.Series.Add(cobrado);
            this.ChartView.Series.Add(saldo);
            this.ChartView.ShowLegend = true;
            this.ChartView.ShowTitle = true;

            facturado.DataSource = this.Datasource;
            cobrado.DataSource = this.Datasource;
            saldo.DataSource = this.Datasource;

            this.ChartView.Refresh();
        }

        private void TReporte_Actualizar_Click(object sender, EventArgs e) {
            this.Consulta.RunWorkerAsync();
        }

        private void TReporte_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
