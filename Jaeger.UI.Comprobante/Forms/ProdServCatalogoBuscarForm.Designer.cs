﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ProdServCatalogoBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProdServCatalogoBuscarForm));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.textBoxProductoSAT = new Telerik.WinControls.UI.RadTextBox();
            this.gridProductoSAT = new Telerik.WinControls.UI.RadGridView();
            this.comboBoxDivision = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxGrupo = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxClase = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radPageView = new Telerik.WinControls.UI.RadPageView();
            this.PageBuscarTexto = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.ButtonBuscar = new Telerik.WinControls.UI.RadButton();
            this.PageBuscarClasificacion = new Telerik.WinControls.UI.RadPageViewPage();
            this.comboBoxProducto = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.TxbClaveDeClase = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxSubClase = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PageBuscarPorCatalogo = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridProductoCatalogo = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.textBoxPorCatalogo = new Telerik.WinControls.UI.RadTextBox();
            this.Consulta = new System.ComponentModel.BackgroundWorker();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxProductoSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDivision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDivision.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDivision.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxGrupo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxGrupo.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxGrupo.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClase.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClase.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView)).BeginInit();
            this.radPageView.SuspendLayout();
            this.PageBuscarTexto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).BeginInit();
            this.PageBuscarClasificacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClaveDeClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSubClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSubClase.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSubClase.EditorControl.MasterTemplate)).BeginInit();
            this.PageBuscarPorCatalogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoCatalogo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPorCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(4, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Descripción:";
            // 
            // textBoxProductoSAT
            // 
            this.textBoxProductoSAT.Location = new System.Drawing.Point(77, 7);
            this.textBoxProductoSAT.Name = "textBoxProductoSAT";
            this.textBoxProductoSAT.Size = new System.Drawing.Size(497, 20);
            this.textBoxProductoSAT.TabIndex = 1;
            this.textBoxProductoSAT.TextChanged += new System.EventHandler(this.TextBoxProductoSAT_TextChanged);
            // 
            // gridProductoSAT
            // 
            this.gridProductoSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProductoSAT.Location = new System.Drawing.Point(0, 34);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 450;
            this.gridProductoSAT.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.gridProductoSAT.MasterTemplate.EnableGrouping = false;
            this.gridProductoSAT.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridProductoSAT.Name = "gridProductoSAT";
            this.gridProductoSAT.Size = new System.Drawing.Size(692, 183);
            this.gridProductoSAT.TabIndex = 2;
            // 
            // comboBoxDivision
            // 
            this.comboBoxDivision.AutoFilter = true;
            this.comboBoxDivision.DisplayMember = "Descripcion";
            // 
            // comboBoxDivision.NestedRadGridView
            // 
            this.comboBoxDivision.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxDivision.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDivision.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxDivision.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxDivision.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxDivision.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxDivision.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.Width = 85;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Name";
            gridViewTextBoxColumn4.Width = 280;
            this.comboBoxDivision.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.comboBoxDivision.EditorControl.MasterTemplate.EnableFiltering = true;
            this.comboBoxDivision.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxDivision.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxDivision.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.comboBoxDivision.EditorControl.Name = "NestedRadGridView";
            this.comboBoxDivision.EditorControl.ReadOnly = true;
            this.comboBoxDivision.EditorControl.ShowGroupPanel = false;
            this.comboBoxDivision.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxDivision.EditorControl.TabIndex = 0;
            this.comboBoxDivision.Location = new System.Drawing.Point(64, 33);
            this.comboBoxDivision.Name = "comboBoxDivision";
            this.comboBoxDivision.Size = new System.Drawing.Size(625, 20);
            this.comboBoxDivision.TabIndex = 21;
            this.comboBoxDivision.TabStop = false;
            this.comboBoxDivision.ValueMember = "Clave";
            this.comboBoxDivision.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDivision_SelectedIndexChanged);
            // 
            // RadLabel4
            // 
            this.RadLabel4.Location = new System.Drawing.Point(3, 87);
            this.RadLabel4.Name = "RadLabel4";
            this.RadLabel4.Size = new System.Drawing.Size(35, 18);
            this.RadLabel4.TabIndex = 27;
            this.RadLabel4.Text = "Clase:";
            // 
            // comboBoxGrupo
            // 
            this.comboBoxGrupo.DisplayMember = "Descripcion";
            // 
            // comboBoxGrupo.NestedRadGridView
            // 
            this.comboBoxGrupo.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxGrupo.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxGrupo.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxGrupo.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxGrupo.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxGrupo.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxGrupo.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.Name = "Clave";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "Descripcion";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "Name";
            gridViewTextBoxColumn6.Width = 290;
            this.comboBoxGrupo.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.comboBoxGrupo.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxGrupo.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxGrupo.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.comboBoxGrupo.EditorControl.Name = "NestedRadGridView";
            this.comboBoxGrupo.EditorControl.ReadOnly = true;
            this.comboBoxGrupo.EditorControl.ShowGroupPanel = false;
            this.comboBoxGrupo.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxGrupo.EditorControl.TabIndex = 0;
            this.comboBoxGrupo.Location = new System.Drawing.Point(64, 59);
            this.comboBoxGrupo.Name = "comboBoxGrupo";
            this.comboBoxGrupo.Size = new System.Drawing.Size(625, 20);
            this.comboBoxGrupo.TabIndex = 22;
            this.comboBoxGrupo.TabStop = false;
            this.comboBoxGrupo.ValueMember = "Clave";
            this.comboBoxGrupo.SelectedIndexChanged += new System.EventHandler(this.ComboBoxGrupo_SelectedIndexChanged);
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(3, 61);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(40, 18);
            this.RadLabel3.TabIndex = 26;
            this.RadLabel3.Text = "Grupo:";
            // 
            // comboBoxClase
            // 
            this.comboBoxClase.DisplayMember = "Descripcion";
            // 
            // comboBoxClase.NestedRadGridView
            // 
            this.comboBoxClase.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxClase.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxClase.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxClase.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxClase.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxClase.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxClase.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Clave";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Name";
            gridViewTextBoxColumn8.Width = 290;
            this.comboBoxClase.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.comboBoxClase.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxClase.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxClase.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.comboBoxClase.EditorControl.Name = "NestedRadGridView";
            this.comboBoxClase.EditorControl.ReadOnly = true;
            this.comboBoxClase.EditorControl.ShowGroupPanel = false;
            this.comboBoxClase.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxClase.EditorControl.TabIndex = 0;
            this.comboBoxClase.Location = new System.Drawing.Point(64, 85);
            this.comboBoxClase.Name = "comboBoxClase";
            this.comboBoxClase.Size = new System.Drawing.Size(625, 20);
            this.comboBoxClase.TabIndex = 23;
            this.comboBoxClase.TabStop = false;
            this.comboBoxClase.ValueMember = "Clave";
            this.comboBoxClase.SelectedIndexChanged += new System.EventHandler(this.ComboBoxClase_SelectedIndexChanged);
            // 
            // RadLabel2
            // 
            this.RadLabel2.Location = new System.Drawing.Point(3, 35);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(48, 18);
            this.RadLabel2.TabIndex = 25;
            this.RadLabel2.Text = "División:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(3, 9);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(31, 18);
            this.radLabel5.TabIndex = 24;
            this.radLabel5.Text = "Tipo:";
            // 
            // radPageView
            // 
            this.radPageView.Controls.Add(this.PageBuscarTexto);
            this.radPageView.Controls.Add(this.PageBuscarClasificacion);
            this.radPageView.Controls.Add(this.PageBuscarPorCatalogo);
            this.radPageView.DefaultPage = this.PageBuscarTexto;
            this.radPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView.ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
            this.radPageView.Location = new System.Drawing.Point(0, 30);
            this.radPageView.Name = "radPageView";
            this.radPageView.SelectedPage = this.PageBuscarPorCatalogo;
            this.radPageView.Size = new System.Drawing.Size(702, 390);
            this.radPageView.TabIndex = 1;
            this.radPageView.ViewMode = Telerik.WinControls.UI.PageViewMode.Outlook;
            // 
            // PageBuscarTexto
            // 
            this.PageBuscarTexto.Controls.Add(this.gridProductoSAT);
            this.PageBuscarTexto.Controls.Add(this.radPanel2);
            this.PageBuscarTexto.ItemSize = new System.Drawing.SizeF(704F, 32F);
            this.PageBuscarTexto.Location = new System.Drawing.Point(5, 31);
            this.PageBuscarTexto.Name = "PageBuscarTexto";
            this.PageBuscarTexto.Size = new System.Drawing.Size(692, 217);
            this.PageBuscarTexto.Text = "Búsqueda por texto";
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radLabel1);
            this.radPanel2.Controls.Add(this.ButtonBuscar);
            this.radPanel2.Controls.Add(this.textBoxProductoSAT);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel2.Location = new System.Drawing.Point(0, 0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(692, 34);
            this.radPanel2.TabIndex = 5;
            this.radPanel2.Text = "radPanel2";
            // 
            // ButtonBuscar
            // 
            this.ButtonBuscar.Location = new System.Drawing.Point(580, 7);
            this.ButtonBuscar.Name = "ButtonBuscar";
            this.ButtonBuscar.Size = new System.Drawing.Size(110, 20);
            this.ButtonBuscar.TabIndex = 4;
            this.ButtonBuscar.Text = "Buscar";
            // 
            // PageBuscarClasificacion
            // 
            this.PageBuscarClasificacion.Controls.Add(this.comboBoxProducto);
            this.PageBuscarClasificacion.Controls.Add(this.radLabel8);
            this.PageBuscarClasificacion.Controls.Add(this.radLabel7);
            this.PageBuscarClasificacion.Controls.Add(this.radTextBox3);
            this.PageBuscarClasificacion.Controls.Add(this.TxbClaveDeClase);
            this.PageBuscarClasificacion.Controls.Add(this.radLabel6);
            this.PageBuscarClasificacion.Controls.Add(this.RadLabel2);
            this.PageBuscarClasificacion.Controls.Add(this.radLabel5);
            this.PageBuscarClasificacion.Controls.Add(this.comboBoxSubClase);
            this.PageBuscarClasificacion.Controls.Add(this.comboBoxDivision);
            this.PageBuscarClasificacion.Controls.Add(this.comboBoxClase);
            this.PageBuscarClasificacion.Controls.Add(this.RadLabel4);
            this.PageBuscarClasificacion.Controls.Add(this.RadLabel3);
            this.PageBuscarClasificacion.Controls.Add(this.comboBoxGrupo);
            this.PageBuscarClasificacion.ItemSize = new System.Drawing.SizeF(704F, 32F);
            this.PageBuscarClasificacion.Location = new System.Drawing.Point(5, 31);
            this.PageBuscarClasificacion.Name = "PageBuscarClasificacion";
            this.PageBuscarClasificacion.Size = new System.Drawing.Size(692, 217);
            this.PageBuscarClasificacion.Text = "Clasificación";
            // 
            // comboBoxProducto
            // 
            this.comboBoxProducto.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "Productos";
            radListDataItem2.Text = "Servicios";
            this.comboBoxProducto.Items.Add(radListDataItem1);
            this.comboBoxProducto.Items.Add(radListDataItem2);
            this.comboBoxProducto.Location = new System.Drawing.Point(64, 7);
            this.comboBoxProducto.Name = "comboBoxProducto";
            this.comboBoxProducto.Size = new System.Drawing.Size(125, 20);
            this.comboBoxProducto.TabIndex = 34;
            this.comboBoxProducto.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.ComboBoxProducto_SelectedIndexChanged);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(170, 146);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(64, 18);
            this.radLabel8.TabIndex = 33;
            this.radLabel8.Text = "Descripción";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(64, 146);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(55, 18);
            this.radLabel7.TabIndex = 32;
            this.radLabel7.Text = "Clave SAT";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(170, 170);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(519, 20);
            this.radTextBox3.TabIndex = 31;
            // 
            // TxbClaveDeClase
            // 
            this.TxbClaveDeClase.Location = new System.Drawing.Point(64, 170);
            this.TxbClaveDeClase.Name = "TxbClaveDeClase";
            this.TxbClaveDeClase.Size = new System.Drawing.Size(100, 20);
            this.TxbClaveDeClase.TabIndex = 30;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(3, 113);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(54, 18);
            this.radLabel6.TabIndex = 29;
            this.radLabel6.Text = "SubClase:";
            // 
            // comboBoxSubClase
            // 
            this.comboBoxSubClase.DisplayMember = "Descripcion";
            // 
            // comboBoxSubClase.NestedRadGridView
            // 
            this.comboBoxSubClase.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxSubClase.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSubClase.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxSubClase.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxSubClase.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxSubClase.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxSubClase.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Name";
            gridViewTextBoxColumn10.Width = 290;
            this.comboBoxSubClase.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.comboBoxSubClase.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxSubClase.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxSubClase.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.comboBoxSubClase.EditorControl.Name = "NestedRadGridView";
            this.comboBoxSubClase.EditorControl.ReadOnly = true;
            this.comboBoxSubClase.EditorControl.ShowGroupPanel = false;
            this.comboBoxSubClase.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxSubClase.EditorControl.TabIndex = 0;
            this.comboBoxSubClase.Location = new System.Drawing.Point(64, 111);
            this.comboBoxSubClase.Name = "comboBoxSubClase";
            this.comboBoxSubClase.Size = new System.Drawing.Size(625, 20);
            this.comboBoxSubClase.TabIndex = 28;
            this.comboBoxSubClase.TabStop = false;
            this.comboBoxSubClase.ValueMember = "Clave";
            // 
            // PageBuscarPorCatalogo
            // 
            this.PageBuscarPorCatalogo.Controls.Add(this.gridProductoCatalogo);
            this.PageBuscarPorCatalogo.Controls.Add(this.radPanel1);
            this.PageBuscarPorCatalogo.ItemSize = new System.Drawing.SizeF(704F, 32F);
            this.PageBuscarPorCatalogo.Location = new System.Drawing.Point(5, 31);
            this.PageBuscarPorCatalogo.Name = "PageBuscarPorCatalogo";
            this.PageBuscarPorCatalogo.Size = new System.Drawing.Size(692, 217);
            this.PageBuscarPorCatalogo.Text = "Catálogo";
            // 
            // gridProductoCatalogo
            // 
            this.gridProductoCatalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProductoCatalogo.Location = new System.Drawing.Point(0, 33);
            // 
            // 
            // 
            this.gridProductoCatalogo.MasterTemplate.AllowAddNewRow = false;
            this.gridProductoCatalogo.MasterTemplate.AllowDeleteRow = false;
            this.gridProductoCatalogo.MasterTemplate.AllowEditRow = false;
            this.gridProductoCatalogo.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "IdModelo";
            gridViewTextBoxColumn11.FormatString = "{0:PT00000#}";
            gridViewTextBoxColumn11.HeaderText = "Id";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "IdModelo";
            gridViewTextBoxColumn11.Width = 65;
            gridViewTextBoxColumn12.FieldName = "Tipo";
            gridViewTextBoxColumn12.HeaderText = "Tipo";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Tipo";
            gridViewTextBoxColumn12.Width = 80;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn13.FieldName = "ProductoDescripcion";
            gridViewTextBoxColumn13.HeaderText = "Descripción";
            gridViewTextBoxColumn13.Name = "ProductoDescripcion";
            gridViewTextBoxColumn13.Width = 350;
            gridViewTextBoxColumn14.FieldName = "Marca";
            gridViewTextBoxColumn14.HeaderText = "Marca";
            gridViewTextBoxColumn14.Name = "Marca";
            gridViewTextBoxColumn14.Width = 200;
            gridViewTextBoxColumn15.FieldName = "Especificacion";
            gridViewTextBoxColumn15.HeaderText = "Especificación";
            gridViewTextBoxColumn15.Name = "Especificacion";
            gridViewTextBoxColumn15.Width = 95;
            gridViewTextBoxColumn16.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn16.HeaderText = "Clave\r\nProd. Serv.";
            gridViewTextBoxColumn16.Name = "ClaveProdServ";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn16.Width = 80;
            gridViewComboBoxColumn1.FieldName = "ClaveUnidad";
            gridViewComboBoxColumn1.HeaderText = "Clave\r\nUnidad";
            gridViewComboBoxColumn1.Name = "ClaveUnidad";
            gridViewComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn1.Width = 80;
            gridViewTextBoxColumn17.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn17.HeaderText = "Identificador";
            gridViewTextBoxColumn17.Name = "NoIdentificacion";
            gridViewTextBoxColumn17.Width = 95;
            gridViewComboBoxColumn2.FieldName = "Unidad";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 75;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "Unitario";
            gridViewTextBoxColumn18.FormatString = "{0:n}";
            gridViewTextBoxColumn18.HeaderText = "Unitario";
            gridViewTextBoxColumn18.Name = "Unitario";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn18.Width = 85;
            gridViewTextBoxColumn19.FieldName = "CtaPredial";
            gridViewTextBoxColumn19.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn19.Name = "CtaPredial";
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.FieldName = "NumRequerimiento";
            gridViewTextBoxColumn20.HeaderText = "Núm Req.";
            gridViewTextBoxColumn20.Name = "NumRequerimiento";
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.FieldName = "Creo";
            gridViewTextBoxColumn21.HeaderText = "Creó";
            gridViewTextBoxColumn21.IsVisible = false;
            gridViewTextBoxColumn21.Name = "Creo";
            gridViewTextBoxColumn21.Width = 75;
            gridViewTextBoxColumn22.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn22.FieldName = "FechaNuevo";
            gridViewTextBoxColumn22.FormatString = "{0:d}";
            gridViewTextBoxColumn22.HeaderText = "Fec. Sistema";
            gridViewTextBoxColumn22.IsVisible = false;
            gridViewTextBoxColumn22.Name = "FechaNuevo";
            gridViewTextBoxColumn22.Width = 75;
            this.gridProductoCatalogo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn17,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22});
            this.gridProductoCatalogo.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.gridProductoCatalogo.Name = "gridProductoCatalogo";
            this.gridProductoCatalogo.ShowGroupPanel = false;
            this.gridProductoCatalogo.Size = new System.Drawing.Size(692, 184);
            this.gridProductoCatalogo.TabIndex = 5;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radLabel9);
            this.radPanel1.Controls.Add(this.radButton1);
            this.radPanel1.Controls.Add(this.textBoxPorCatalogo);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(692, 33);
            this.radPanel1.TabIndex = 9;
            this.radPanel1.Text = "radPanel1";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(3, 7);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(67, 18);
            this.radLabel9.TabIndex = 6;
            this.radLabel9.Text = "Descripción:";
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(579, 6);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(110, 20);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "Buscar";
            // 
            // textBoxPorCatalogo
            // 
            this.textBoxPorCatalogo.Location = new System.Drawing.Point(76, 6);
            this.textBoxPorCatalogo.Name = "textBoxPorCatalogo";
            this.textBoxPorCatalogo.Size = new System.Drawing.Size(497, 20);
            this.textBoxPorCatalogo.TabIndex = 7;
            this.textBoxPorCatalogo.TextChanged += new System.EventHandler(this.TextBoxPorCatalogo_TextChanged);
            // 
            // Consulta
            // 
            this.Consulta.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Consulta_DoWork);
            this.Consulta.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Consulta_RunWorkerCompleted);
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Productos y Servicios";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ReadOnly = false;
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutorizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowExportarExcel = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(702, 30);
            this.ToolBar.TabIndex = 5;
            this.ToolBar.Autorizar.Click += this.Asignar_Click;
            this.ToolBar.Cerrar.Click += this.ButtonCerrar_Click;
            // 
            // ProdServCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 420);
            this.Controls.Add(this.radPageView);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProdServCatalogoBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Buscar Clave de Producto/Servicio SAT";
            this.Load += new System.EventHandler(this.CatalogoProdServBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxProductoSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDivision.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDivision.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDivision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxGrupo.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxGrupo.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxGrupo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClase.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClase.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView)).EndInit();
            this.radPageView.ResumeLayout(false);
            this.PageBuscarTexto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).EndInit();
            this.PageBuscarClasificacion.ResumeLayout(false);
            this.PageBuscarClasificacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClaveDeClase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSubClase.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSubClase.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSubClase)).EndInit();
            this.PageBuscarPorCatalogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoCatalogo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPorCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox textBoxProductoSAT;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView gridProductoSAT;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxDivision;
        internal Telerik.WinControls.UI.RadLabel RadLabel4;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxGrupo;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxClase;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadPageViewPage PageBuscarTexto;
        private Telerik.WinControls.UI.RadPageViewPage PageBuscarClasificacion;
        internal Telerik.WinControls.UI.RadLabel radLabel8;
        internal Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox TxbClaveDeClase;
        internal Telerik.WinControls.UI.RadLabel radLabel6;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxSubClase;
        private Telerik.WinControls.UI.RadButton ButtonBuscar;
        private Telerik.WinControls.UI.RadPageViewPage PageBuscarPorCatalogo;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox textBoxPorCatalogo;
        private Telerik.WinControls.UI.RadGridView gridProductoCatalogo;
        private Telerik.WinControls.UI.RadDropDownList comboBoxProducto;
        private System.ComponentModel.BackgroundWorker Consulta;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        public Telerik.WinControls.UI.RadPageView radPageView;
        public Common.Forms.ToolBarStandarControl ToolBar;
    }
}
