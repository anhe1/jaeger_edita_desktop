﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteFiscalInformacionGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.ClavePeriodicidad = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ClaveMeses = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.RadLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.Incluir = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClavePeriodicidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClavePeriodicidad.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClavePeriodicidad.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveMeses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveMeses.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveMeses.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Incluir)).BeginInit();
            this.SuspendLayout();
            // 
            // RadLabel12
            // 
            this.RadLabel12.Location = new System.Drawing.Point(12, 27);
            this.RadLabel12.Name = "RadLabel12";
            this.RadLabel12.Size = new System.Drawing.Size(71, 18);
            this.RadLabel12.TabIndex = 18;
            this.RadLabel12.Text = "Periodicidad:";
            // 
            // ClavePeriodicidad
            // 
            this.ClavePeriodicidad.AutoSizeDropDownToBestFit = true;
            this.ClavePeriodicidad.DisplayMember = "Descriptor";
            this.ClavePeriodicidad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ClavePeriodicidad.NestedRadGridView
            // 
            this.ClavePeriodicidad.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ClavePeriodicidad.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClavePeriodicidad.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClavePeriodicidad.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ClavePeriodicidad.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ClavePeriodicidad.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ClavePeriodicidad.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Descriptor";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.ClavePeriodicidad.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.ClavePeriodicidad.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ClavePeriodicidad.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ClavePeriodicidad.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.ClavePeriodicidad.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.ClavePeriodicidad.EditorControl.Name = "NestedRadGridView";
            this.ClavePeriodicidad.EditorControl.ReadOnly = true;
            this.ClavePeriodicidad.EditorControl.ShowGroupPanel = false;
            this.ClavePeriodicidad.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ClavePeriodicidad.EditorControl.TabIndex = 0;
            this.ClavePeriodicidad.Enabled = false;
            this.ClavePeriodicidad.Location = new System.Drawing.Point(94, 26);
            this.ClavePeriodicidad.Name = "ClavePeriodicidad";
            this.ClavePeriodicidad.NullText = "Periodicidad";
            this.ClavePeriodicidad.Size = new System.Drawing.Size(207, 20);
            this.ClavePeriodicidad.TabIndex = 19;
            this.ClavePeriodicidad.TabStop = false;
            this.ClavePeriodicidad.ValueMember = "Clave";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 53);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(40, 18);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "Meses:";
            // 
            // ClaveMeses
            // 
            this.ClaveMeses.AutoSizeDropDownToBestFit = true;
            this.ClaveMeses.DisplayMember = "Descriptor";
            this.ClaveMeses.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ClaveMeses.NestedRadGridView
            // 
            this.ClaveMeses.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ClaveMeses.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClaveMeses.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClaveMeses.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ClaveMeses.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ClaveMeses.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ClaveMeses.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn4.FieldName = "Clave";
            gridViewTextBoxColumn4.HeaderText = "Clave";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "Clave";
            gridViewTextBoxColumn4.VisibleInColumnChooser = false;
            gridViewTextBoxColumn5.FieldName = "Descriptor";
            gridViewTextBoxColumn5.HeaderText = "Descripción";
            gridViewTextBoxColumn5.Name = "Descripcion";
            gridViewTextBoxColumn6.FieldName = "Descriptor";
            gridViewTextBoxColumn6.HeaderText = "Descriptor";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Descriptor";
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            this.ClaveMeses.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.ClaveMeses.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ClaveMeses.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ClaveMeses.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.ClaveMeses.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.ClaveMeses.EditorControl.Name = "NestedRadGridView";
            this.ClaveMeses.EditorControl.ReadOnly = true;
            this.ClaveMeses.EditorControl.ShowGroupPanel = false;
            this.ClaveMeses.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ClaveMeses.EditorControl.TabIndex = 0;
            this.ClaveMeses.Enabled = false;
            this.ClaveMeses.Location = new System.Drawing.Point(94, 52);
            this.ClaveMeses.Name = "ClaveMeses";
            this.ClaveMeses.NullText = "Meses";
            this.ClaveMeses.Size = new System.Drawing.Size(207, 20);
            this.ClaveMeses.TabIndex = 21;
            this.ClaveMeses.TabStop = false;
            this.ClaveMeses.ValueMember = "Clave";
            // 
            // Ejercicio
            // 
            this.Ejercicio.Enabled = false;
            this.Ejercicio.Location = new System.Drawing.Point(94, 78);
            this.Ejercicio.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(72, 20);
            this.Ejercicio.TabIndex = 40;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RadLabel20
            // 
            this.RadLabel20.Location = new System.Drawing.Point(12, 79);
            this.RadLabel20.Name = "RadLabel20";
            this.RadLabel20.Size = new System.Drawing.Size(29, 18);
            this.RadLabel20.TabIndex = 39;
            this.RadLabel20.Text = "Año:";
            // 
            // Incluir
            // 
            this.Incluir.Location = new System.Drawing.Point(12, 3);
            this.Incluir.Name = "Incluir";
            this.Incluir.Size = new System.Drawing.Size(155, 18);
            this.Incluir.TabIndex = 64;
            this.Incluir.Text = "Incluir Información General";
            this.Incluir.CheckStateChanged += new System.EventHandler(this.Incluir_CheckStateChanged);
            // 
            // ComprobanteFiscalInformacionGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Incluir);
            this.Controls.Add(this.Ejercicio);
            this.Controls.Add(this.RadLabel20);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.ClaveMeses);
            this.Controls.Add(this.RadLabel12);
            this.Controls.Add(this.ClavePeriodicidad);
            this.MinimumSize = new System.Drawing.Size(1200, 102);
            this.Name = "ComprobanteFiscalInformacionGeneralControl";
            this.Size = new System.Drawing.Size(1200, 103);
            this.Load += new System.EventHandler(this.ComprobanteFiscalInformacionGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClavePeriodicidad.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClavePeriodicidad.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClavePeriodicidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveMeses.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveMeses.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveMeses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Incluir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadLabel RadLabel12;
        internal Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadLabel RadLabel20;
        protected internal Telerik.WinControls.UI.RadCheckBox Incluir;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox ClavePeriodicidad;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox ClaveMeses;
        protected internal Telerik.WinControls.UI.RadSpinEditor Ejercicio;
    }
}
