﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteProductosForm : Telerik.WinControls.UI.RadForm {
        private ICatalogoProductoService service;
        private BindingList<ModeloDetailModel> datos;

        public ComprobanteProductosForm() {
            InitializeComponent();
        }

        private void ViewComprobanteProductos_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            this.service = new CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
        }

        #region barra de herramientas

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            this.ToolBar.Actualizar.Enabled = false;
            System.Threading.Thread.Sleep(10);
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando productos...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = datos;
            this.ToolBar.Actualizar.Enabled = true;
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            var nuevo = new ComprobanteProductoForm(null) { StartPosition = FormStartPosition.CenterParent };
            nuevo.ShowDialog(this);
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var producto = this.GridData.CurrentRow.DataBoundItem as ModeloDetailModel;
                if (producto != null) {
                    var editar = new ComprobanteProductoForm(producto) { StartPosition = FormStartPosition.CenterParent };
                    editar.ShowDialog(this);
                }
            }
        }

        private void ToolBarButtonEliminar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null)
                if (RadMessageBox.Show("¿Esta seguro de eliminar este producto del catálogo?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    using (var espera = new Waiting2Form(this.Eliminar)) {
                        espera.Text = "Eliminando ...";
                        espera.ShowDialog(this);
                    }
                }
        }

        private void ToolBarButtonFiltrar_Click(object sender, EventArgs e) {
            this.GridData.ShowFilteringRow = this.ToolBar.Filtro.ToggleState != ToggleState.On;
            if (this.GridData.ShowFilteringRow == false)
                this.GridData.FilterDescriptors.Clear();
        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {
            TelerikGridExportForm exportar = new TelerikGridExportForm(this.GridData) { StartPosition = FormStartPosition.CenterParent };
            exportar.ShowDialog();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #endregion

        #region consulta de datos

        private void Consultar() {
            this.datos = new BindingList<ModeloDetailModel>(this.service.GetListConceptos().ToList());
        }

        
        #endregion

        private void Eliminar() {
            var producto = this.GridData.CurrentRow.DataBoundItem as ModeloDetailModel;
            if (producto != null) {
                producto.Activo = false;
                //this.service.Save(producto);
                this.GridData.CurrentRow.IsVisible = false;
            }
        }

        private void GridData_RowsChanged(object sender, Telerik.WinControls.UI.GridViewCollectionChangedEventArgs e) {
            this.ToolBar.Editar.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBar.Remover.Enabled = this.GridData.Rows.Count > 0;
            this.ToolBar.Filtro.Enabled = this.GridData.Rows.Count > 0;
        }
    }
}
