﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class Articulo69BForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.ToolBarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblCarpeta = new Telerik.WinControls.UI.CommandBarLabel();
            this.Separador1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Separador2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.TAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.TActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.TFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            this.commandBarTextBox1 = new Telerik.WinControls.UI.CommandBarTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBarRowElement});
            this.RadCommandBar1.Size = new System.Drawing.Size(1092, 30);
            this.RadCommandBar1.TabIndex = 6;
            // 
            // ToolBarRowElement
            // 
            this.ToolBarRowElement.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBarRowElement.Name = "ToolBarRowElement";
            this.ToolBarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.ToolBarRowElement.Text = "";
            this.ToolBarRowElement.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarRowElement.UseCompatibleTextRendering = false;
            // 
            // ToolBar
            // 
            this.ToolBar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.DisplayName = "Emision de Comprobante";
            this.ToolBar.EnableFocusBorder = false;
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblCarpeta,
            this.commandBarTextBox1,
            this.Separador1,
            this.TAgregar,
            this.Separador2,
            this.TActualizar,
            this.TFiltro,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            // 
            // 
            // 
            this.ToolBar.OverflowButton.Enabled = true;
            this.ToolBar.ShowHorizontalLine = false;
            this.ToolBar.StretchHorizontally = true;
            this.ToolBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.UseCompatibleTextRendering = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBar.GetChildAt(2))).Enabled = true;
            // 
            // lblCarpeta
            // 
            this.lblCarpeta.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblCarpeta.DisplayName = "Etiqueta: Carpeta";
            this.lblCarpeta.Name = "lblCarpeta";
            this.lblCarpeta.Text = "Carpeta:";
            this.lblCarpeta.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblCarpeta.UseCompatibleTextRendering = false;
            // 
            // Separador1
            // 
            this.Separador1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador1.DisplayName = "Separador 1";
            this.Separador1.Name = "Separador1";
            this.Separador1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador1.UseCompatibleTextRendering = false;
            this.Separador1.VisibleInOverflowMenu = false;
            // 
            // Separador2
            // 
            this.Separador2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador2.DisplayName = "Separador 2";
            this.Separador2.Name = "Separador2";
            this.Separador2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador2.UseCompatibleTextRendering = false;
            this.Separador2.VisibleInOverflowMenu = false;
            // 
            // TAgregar
            // 
            this.TAgregar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TAgregar.DisplayName = "Agregar";
            this.TAgregar.DrawText = true;
            this.TAgregar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.add_16px;
            this.TAgregar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TAgregar.Name = "TAgregar";
            this.TAgregar.Text = "Agregar";
            this.TAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TAgregar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TAgregar.UseCompatibleTextRendering = false;
            // 
            // TActualizar
            // 
            this.TActualizar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TActualizar.DisplayName = "Actualizar";
            this.TActualizar.DrawText = true;
            this.TActualizar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.refresh_16px;
            this.TActualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TActualizar.Name = "TActualizar";
            this.TActualizar.Text = "Actualizar";
            this.TActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TActualizar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TActualizar.UseCompatibleTextRendering = false;
            // 
            // TFiltro
            // 
            this.TFiltro.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TFiltro.DisplayName = "Filtro";
            this.TFiltro.DrawText = true;
            this.TFiltro.Image = global::Jaeger.UI.Comprobante.Properties.Resources.filter_16px;
            this.TFiltro.Name = "TFiltro";
            this.TFiltro.Text = "Filtro";
            this.TFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TFiltro.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TFiltro.UseCompatibleTextRendering = false;
            // 
            // Herramientas
            // 
            this.Herramientas.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Comprobante.Properties.Resources.toolbox_16px;
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Herramientas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Herramientas.UseCompatibleTextRendering = false;
            // 
            // Cerrar
            // 
            this.Cerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.UseCompatibleTextRendering = false;
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridData.MasterTemplate.AllowAddNewRow = false;
            this.gridData.MasterTemplate.AllowDeleteRow = false;
            this.gridData.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.FieldName = "No";
            gridViewTextBoxColumn1.HeaderText = "No";
            gridViewTextBoxColumn1.Name = "No";
            gridViewTextBoxColumn2.FieldName = "RFC";
            gridViewTextBoxColumn2.HeaderText = "RFC";
            gridViewTextBoxColumn2.Name = "RFC";
            gridViewTextBoxColumn2.Width = 110;
            gridViewTextBoxColumn3.HeaderText = "Nombre del Contribuyente";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 250;
            gridViewTextBoxColumn4.HeaderText = "Situación del contribuyente";
            gridViewTextBoxColumn4.Name = "column1";
            gridViewTextBoxColumn4.Width = 150;
            gridViewTextBoxColumn5.HeaderText = "Número y fecha de oficio global de presunción SAT";
            gridViewTextBoxColumn5.Name = "column2";
            gridViewTextBoxColumn5.Width = 200;
            gridViewTextBoxColumn6.HeaderText = "Publicación página SAT presuntos";
            gridViewTextBoxColumn6.Name = "column3";
            gridViewTextBoxColumn6.Width = 250;
            this.gridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.gridData.MasterTemplate.EnableGrouping = false;
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(1092, 461);
            this.gridData.TabIndex = 7;
            // 
            // commandBarTextBox1
            // 
            this.commandBarTextBox1.DisplayName = "commandBarTextBox1";
            this.commandBarTextBox1.Name = "commandBarTextBox1";
            this.commandBarTextBox1.Text = "commandBarTextBox1";
            // 
            // Articulo69BForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 491);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.RadCommandBar1);
            this.Name = "Articulo69BForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Articulo69BForm";
            this.Load += new System.EventHandler(this.Articulo69BForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement ToolBarRowElement;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel lblCarpeta;
        private Telerik.WinControls.UI.CommandBarSeparator Separador1;
        private Telerik.WinControls.UI.CommandBarSeparator Separador2;
        protected internal Telerik.WinControls.UI.CommandBarButton TAgregar;
        protected internal Telerik.WinControls.UI.CommandBarButton TActualizar;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton TFiltro;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        private Telerik.WinControls.UI.RadGridView gridData;
        private Telerik.WinControls.UI.CommandBarTextBox commandBarTextBox1;
    }
}