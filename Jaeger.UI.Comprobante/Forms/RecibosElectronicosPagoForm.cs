﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Comprobante.Builder;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class RecibosElectronicosPagoForm : RadForm {
        #region declaraciones
        protected internal IComprobantesFiscalesService _Service;
        protected internal BindingList<ComprobanteReciboEletronicoPagoSingle> _DataSource;
        #endregion

        public RecibosElectronicosPagoForm(IComprobantesFiscalesService service) {
            InitializeComponent();
            this._Service = service;
        }

        private void RecibosElectronicosPagoForm_Load(object sender, EventArgs e) {
            this.TComprobantes.Templete = ComprobanteFiscalGridBuilder.TempleteEnum.ComplementoPago;
            this.TComprobantes.Actualizar.Click += TComprobantes_Actualizar_Click;
            this.TComprobantes.Cerrar.Click += TComprobantes_Cerrar_Click;
            this.TComprobantes._Service = this._Service;
        }

        private void TComprobantes_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando datos ...";
                espera.ShowDialog(this);
            }
            this.TComprobantes.GridData.DataSource = this._DataSource;
        }

        private void TComprobantes_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            var query = ComprobantesFiscalesService.Query().Recibido().WithYear(this.TComprobantes.GetEjercicio()).WithTipo(Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos).Build();
            this._DataSource = new BindingList<ComprobanteReciboEletronicoPagoSingle>(this._Service.GetList<ComprobanteReciboEletronicoPagoSingle>(query).ToList());
        }
    }
}
