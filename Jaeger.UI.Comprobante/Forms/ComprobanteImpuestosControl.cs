﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteImpuestosControl : UserControl {
        public ComprobanteImpuestosControl() {
            InitializeComponent();
        }

        private void ComprobanteImpuestosControl_Load(object sender, EventArgs e) {
            var _tipo = this.Impuestos.Columns["Tipo"] as GridViewComboBoxColumn;
            _tipo.DisplayMember = "Descripcion";
            _tipo.ValueMember = "Id";
            _tipo.DataSource = ConfigService.GetImpuestoTipo();

            var _impuesto = this.Impuestos.Columns["Impuesto"] as GridViewComboBoxColumn;
            _impuesto.DisplayMember = "Descripcion";
            _impuesto.ValueMember = "Id";
            _impuesto.DataSource = ConfigService.GetImpuestos();

            var _factor = this.Impuestos.Columns["TipoFactor"] as GridViewComboBoxColumn;
            _factor.DisplayMember = "Descripcion";
            _factor.ValueMember = "Id";
            _factor.DataSource = ConfigService.GetImpuestoTipoFactor();
        }

        private void TImpuestos_Nuevo_Click(object sender, EventArgs e) {
            this.Impuestos.Rows.AddNew();
        }

        private void TImpuestos_Remover_Click(object sender, EventArgs e) {
            if (this.Impuestos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    try {
                        this.Impuestos.Rows.Remove(this.Impuestos.CurrentRow);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        private void TImpuestos_Filtro_Click(object sender, EventArgs e) {
            this.Impuestos.ShowFilteringRow = this.TImpuestos.Filtro.ToggleState == ToggleState.Off;
            if (this.Impuestos.ShowFilteringRow == false) {
                this.Impuestos.FilterDescriptors.Clear();
            }
        }
    }
}
