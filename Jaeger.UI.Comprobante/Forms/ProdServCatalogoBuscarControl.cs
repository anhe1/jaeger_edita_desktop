﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ProdServCatalogoBuscarControl : UserControl {
        protected ICatalogoProductoService service;
        private BindingList<ProductoServicioModeloModel> resultados = new BindingList<ProductoServicioModeloModel>();
        private string descripcion;

        public event EventHandler<ProductoServicioModeloModel> Selected;
        public void OnSelected(ProductoServicioModeloModel e) {
            if (this.Selected != null) {
                this.Selected(this, e);
            }
        }

        public ProdServCatalogoBuscarControl() {
            InitializeComponent();
        }

        public void SetService(ICatalogoProductoService service) {
            this.service = service;
        }

        public void Search(string descripcion) {
            this.descripcion = descripcion;
            if (this.WorkerSearch.IsBusy == false) {
                this.Espera.Visible = true;
                this.Espera.StartWaiting();
                this.WorkerSearch.RunWorkerAsync();
            }
        }

        public void SetFilter(ToggleState toggleState) {
            this.gridSearchResult.ActivateFilter(toggleState);
        }

        public ProductoServicioModeloModel GetSelected() {
            var seleccionado = this.gridSearchResult.CurrentRow.DataBoundItem as ProductoServicioModeloModel;
            if (seleccionado != null)
                return seleccionado;
            return null;
        }

        private void ProdServCatalogoBuscarControl_Load(object sender, EventArgs e) {
            this.Espera.AssociatedControl = this.gridSearchResult;
        }

        private void gridProductoCatalogo_DoubleClick(object sender, EventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                var seleccionado = this.GetSelected();
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }

        private void WorkerSearch_DoWork(object sender, DoWorkEventArgs e) {
            var d0 = CatalogoModeloService.Query().ByIdAlmacen(this.service.Almacen).Activo().Search(this.descripcion).Build();
            this.resultados = new BindingList<ProductoServicioModeloModel>(this.service.GetList<ProductoServicioModeloModel>(d0).ToList());
        }

        private void WorkerSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.gridSearchResult.DataSource = resultados;
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
        }
    }
}
