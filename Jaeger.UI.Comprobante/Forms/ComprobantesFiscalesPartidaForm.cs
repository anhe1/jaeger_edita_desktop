﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Comprobante.Builder;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesFiscalesPartidaForm : RadForm {
        #region declaraciones
        protected internal IComprobantesFiscalesService _Service;
        protected internal BindingList<ComprobanteFiscalConceptoView> _DataSource;
        #endregion

        public ComprobantesFiscalesPartidaForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TComprobante.Permisos = new UIAction(menuElement.Permisos);
        }

        private void ComprobantesFiscalesPartidaForm_Load(object sender, EventArgs e) {
            this.Text = "Facturación: Por Partida";
            using (IComprobanteFiscalGridBuilder view = new ComprobanteFiscalGridBuilder()) {
                this.TComprobante.GridData.Columns.AddRange(view.Templetes().MasterYConceptos().Build());
            }

            this.TComprobante.Actualizar.Click += Actualizar_Click;
            this.TComprobante.Cerrar.Click += Cerrar_Click;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.TComprobante.GridData.DataSource = this._DataSource;
        }

        public virtual void Consulta() {
            var q0 = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Emitido().WithYear(this.TComprobante.GetEjercicio()).WithMonth(this.TComprobante.GetPeriodo()).Build();
            this._DataSource = new BindingList<ComprobanteFiscalConceptoView>(this._Service.GetList<ComprobanteFiscalConceptoView>(q0).ToList<ComprobanteFiscalConceptoView>());
        }
    }
}
