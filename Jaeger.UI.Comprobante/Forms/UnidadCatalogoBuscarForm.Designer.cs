﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class UnidadCatalogoBuscarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnidadCatalogoBuscarForm));
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.TUnidad = new Telerik.WinControls.UI.CommandBarStripElement();
            this.TUnidad_Asignar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(584, 30);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.TUnidad});
            // 
            // TUnidad
            // 
            this.TUnidad.DisplayName = "commandBarStripElement1";
            this.TUnidad.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.TUnidad_Asignar,
            this.ToolBarButtonCerrar});
            this.TUnidad.Name = "TUnidad";
            this.TUnidad.StretchHorizontally = true;
            // 
            // ToolBarButtonAsignar
            // 
            this.TUnidad_Asignar.DisplayName = "Asignar";
            this.TUnidad_Asignar.DrawText = true;
            this.TUnidad_Asignar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.add_16px;
            this.TUnidad_Asignar.Name = "ToolBarButtonAsignar";
            this.TUnidad_Asignar.Text = "Asignar";
            this.TUnidad_Asignar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TUnidad_Asignar.Click += new System.EventHandler(this.TUnidad_Asignar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.close_window_16px;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.TUnidad_Cerrar_Click);
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Tipo";
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn3.Width = 300;
            this.gridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(584, 240);
            this.gridData.TabIndex = 1;
            this.gridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            // 
            // UnidadCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 270);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.radCommandBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnidadCatalogoBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Catálogo de Unidades";
            this.Load += new System.EventHandler(this.UnidadCatalogoBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement TUnidad;
        private Telerik.WinControls.UI.RadGridView gridData;
        private Telerik.WinControls.UI.CommandBarButton TUnidad_Asignar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
    }
}
