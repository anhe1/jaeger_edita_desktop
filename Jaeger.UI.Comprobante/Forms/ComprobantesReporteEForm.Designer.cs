﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobantesReporteEForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn93 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn94 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn95 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn96 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn97 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn98 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn99 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn100 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn101 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn102 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn103 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn104 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn10 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn11 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn12 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn13 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn14 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn15 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn16 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn105 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn106 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn107 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn108 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn109 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn110 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn111 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn112 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn113 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn114 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn115 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn116 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn117 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn118 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn17 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn119 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn120 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn18 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn121 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn122 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn123 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn124 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn125 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition14 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn126 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn127 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn128 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn129 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn130 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn131 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn132 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn133 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn134 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn135 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition10 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn136 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn137 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn138 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn139 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn140 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn141 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn142 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn143 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn144 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn145 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn146 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn147 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn148 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn149 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn150 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn151 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn152 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn153 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition12 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn154 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn155 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn156 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn157 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn158 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn159 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn160 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn161 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn162 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn163 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn164 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn165 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn166 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition11 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn167 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn168 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn169 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn170 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn171 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn172 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn173 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn174 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn175 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn176 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn177 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn178 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn179 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn180 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn181 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn182 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn183 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn184 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject4 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition13 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            this.gridConcepto = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridCFDIRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComprobanteRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComplementoPagos = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComplementoPagosDoctoRelacionado = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridComplementoPagoD = new Telerik.WinControls.UI.GridViewTemplate();
            this.TReporte = new Jaeger.UI.Common.Forms.TBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComprobanteRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagosDoctoRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn93.DataType = typeof(int);
            gridViewTextBoxColumn93.FieldName = "Id";
            gridViewTextBoxColumn93.HeaderText = "Id";
            gridViewTextBoxColumn93.Name = "Id";
            gridViewTextBoxColumn93.ReadOnly = true;
            gridViewTextBoxColumn93.VisibleInColumnChooser = false;
            gridViewTextBoxColumn94.FieldName = "Version";
            gridViewTextBoxColumn94.HeaderText = "Ver.";
            gridViewTextBoxColumn94.Name = "Version";
            gridViewTextBoxColumn94.ReadOnly = true;
            gridViewTextBoxColumn94.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "Activo";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.ReadOnly = true;
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn95.DataType = typeof(int);
            gridViewTextBoxColumn95.FieldName = "TipoComprobante";
            gridViewTextBoxColumn95.HeaderText = "TipoComprobante";
            gridViewTextBoxColumn95.IsVisible = false;
            gridViewTextBoxColumn95.Name = "TipoComprobante";
            gridViewTextBoxColumn95.VisibleInColumnChooser = false;
            gridViewTextBoxColumn96.DataType = typeof(int);
            gridViewTextBoxColumn96.FieldName = "SubTipoText";
            gridViewTextBoxColumn96.HeaderText = "Tipo";
            gridViewTextBoxColumn96.IsVisible = false;
            gridViewTextBoxColumn96.Name = "SubTipoText";
            gridViewTextBoxColumn96.ReadOnly = true;
            gridViewComboBoxColumn2.FieldName = "Status";
            gridViewComboBoxColumn2.HeaderText = "Status";
            gridViewComboBoxColumn2.Name = "Status";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn97.FieldName = "Folio";
            gridViewTextBoxColumn97.HeaderText = "Folio";
            gridViewTextBoxColumn97.Name = "Folio";
            gridViewTextBoxColumn97.ReadOnly = true;
            gridViewTextBoxColumn97.Width = 65;
            gridViewTextBoxColumn98.FieldName = "Serie";
            gridViewTextBoxColumn98.HeaderText = "Serie";
            gridViewTextBoxColumn98.Name = "Serie";
            gridViewTextBoxColumn98.ReadOnly = true;
            gridViewTextBoxColumn98.Width = 65;
            gridViewTextBoxColumn99.FieldName = "EmisorRFC";
            gridViewTextBoxColumn99.HeaderText = "RFC Emisor";
            gridViewTextBoxColumn99.Name = "EmisorRFC";
            gridViewTextBoxColumn99.ReadOnly = true;
            gridViewTextBoxColumn99.Width = 90;
            gridViewTextBoxColumn100.FieldName = "EmisorNombre";
            gridViewTextBoxColumn100.HeaderText = "Nombre del Emisor";
            gridViewTextBoxColumn100.Name = "EmisorNombre";
            gridViewTextBoxColumn100.ReadOnly = true;
            gridViewTextBoxColumn100.Width = 180;
            gridViewTextBoxColumn101.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn101.HeaderText = "RFC Receptor";
            gridViewTextBoxColumn101.Name = "ReceptorRFC";
            gridViewTextBoxColumn101.ReadOnly = true;
            gridViewTextBoxColumn101.Width = 90;
            gridViewTextBoxColumn102.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn102.HeaderText = "Nombre del Receptor";
            gridViewTextBoxColumn102.Name = "ReceptorNombre";
            gridViewTextBoxColumn102.ReadOnly = true;
            gridViewTextBoxColumn102.Width = 220;
            gridViewTextBoxColumn103.FieldName = "IdDocumento";
            gridViewTextBoxColumn103.HeaderText = "Folios Fiscal (uuid)";
            gridViewTextBoxColumn103.Name = "IdDocumento";
            gridViewTextBoxColumn103.ReadOnly = true;
            gridViewTextBoxColumn103.Width = 180;
            gridViewTextBoxColumn104.FieldName = "TipoComprobanteText";
            gridViewTextBoxColumn104.HeaderText = "Tipo";
            gridViewTextBoxColumn104.Name = "TipoComprobanteText";
            gridViewTextBoxColumn104.ReadOnly = true;
            gridViewTextBoxColumn104.WrapText = true;
            gridViewDateTimeColumn10.FieldName = "FechaEmision";
            gridViewDateTimeColumn10.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn10.HeaderText = "Fecha de Emisión";
            gridViewDateTimeColumn10.Name = "FechaEmision";
            gridViewDateTimeColumn10.ReadOnly = true;
            gridViewDateTimeColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn10.Width = 70;
            gridViewDateTimeColumn10.WrapText = true;
            gridViewDateTimeColumn11.FieldName = "FechaTimbre";
            gridViewDateTimeColumn11.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn11.HeaderText = "Fecha de Certificación";
            gridViewDateTimeColumn11.Name = "FechaTimbre";
            gridViewDateTimeColumn11.ReadOnly = true;
            gridViewDateTimeColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn11.Width = 70;
            gridViewDateTimeColumn11.WrapText = true;
            gridViewDateTimeColumn12.FieldName = "FechaCancela";
            gridViewDateTimeColumn12.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn12.HeaderText = "Fecha de Cancelación";
            gridViewDateTimeColumn12.Name = "FechaCancela";
            gridViewDateTimeColumn12.ReadOnly = true;
            gridViewDateTimeColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn12.Width = 70;
            gridViewDateTimeColumn12.WrapText = true;
            gridViewDateTimeColumn13.FieldName = "FechaValidacion";
            gridViewDateTimeColumn13.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn13.HeaderText = "Fecha de Validación";
            gridViewDateTimeColumn13.Name = "FechaValidacion";
            gridViewDateTimeColumn13.ReadOnly = true;
            gridViewDateTimeColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn13.Width = 70;
            gridViewDateTimeColumn13.WrapText = true;
            gridViewDateTimeColumn14.FieldName = "FechaEntrega";
            gridViewDateTimeColumn14.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn14.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn14.HeaderText = "Fecha de Entrega";
            gridViewDateTimeColumn14.Name = "FechaEntrega";
            gridViewDateTimeColumn14.ReadOnly = true;
            gridViewDateTimeColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn14.Width = 70;
            gridViewDateTimeColumn14.WrapText = true;
            gridViewDateTimeColumn15.FieldName = "FechaUltimoPago";
            gridViewDateTimeColumn15.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn15.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn15.HeaderText = "Ult. Pago";
            gridViewDateTimeColumn15.Name = "FechaUltimoPago";
            gridViewDateTimeColumn15.ReadOnly = true;
            gridViewDateTimeColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn15.Width = 70;
            gridViewDateTimeColumn16.FieldName = "FechaRecepcionPago";
            gridViewDateTimeColumn16.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn16.HeaderText = "C. Fecha Pago";
            gridViewDateTimeColumn16.Name = "FechaRecepcionPago";
            gridViewDateTimeColumn16.Width = 70;
            gridViewTextBoxColumn105.FieldName = "NumParcialidad";
            gridViewTextBoxColumn105.HeaderText = "Núm. Par.";
            gridViewTextBoxColumn105.IsVisible = false;
            gridViewTextBoxColumn105.Name = "NumParcialidad";
            gridViewTextBoxColumn105.WrapText = true;
            gridViewTextBoxColumn106.FieldName = "ClaveMetodoPago";
            gridViewTextBoxColumn106.HeaderText = "Método de Pago";
            gridViewTextBoxColumn106.IsVisible = false;
            gridViewTextBoxColumn106.Name = "ClaveMetodoPago";
            gridViewTextBoxColumn106.ReadOnly = true;
            gridViewTextBoxColumn106.Width = 75;
            gridViewTextBoxColumn106.WrapText = true;
            gridViewTextBoxColumn107.FieldName = "ClaveFormaPago";
            gridViewTextBoxColumn107.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn107.Name = "ClaveFormaPago";
            gridViewTextBoxColumn108.DataType = typeof(decimal);
            gridViewTextBoxColumn108.FieldName = "SubTotal";
            gridViewTextBoxColumn108.FormatString = "{0:n}";
            gridViewTextBoxColumn108.HeaderText = "SubTotal";
            gridViewTextBoxColumn108.IsVisible = false;
            gridViewTextBoxColumn108.Name = "SubTotal";
            gridViewTextBoxColumn108.ReadOnly = true;
            gridViewTextBoxColumn108.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn108.Width = 85;
            gridViewTextBoxColumn109.DataType = typeof(decimal);
            gridViewTextBoxColumn109.FieldName = "TrasladoIVA";
            gridViewTextBoxColumn109.FormatString = "{0:n}";
            gridViewTextBoxColumn109.HeaderText = "Iva";
            gridViewTextBoxColumn109.IsVisible = false;
            gridViewTextBoxColumn109.Name = "TrasladoIVA";
            gridViewTextBoxColumn109.ReadOnly = true;
            gridViewTextBoxColumn109.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn109.Width = 85;
            gridViewTextBoxColumn110.DataType = typeof(decimal);
            gridViewTextBoxColumn110.FieldName = "Total1";
            gridViewTextBoxColumn110.FormatString = "{0:n}";
            gridViewTextBoxColumn110.HeaderText = "Total";
            gridViewTextBoxColumn110.Name = "Total";
            gridViewTextBoxColumn110.ReadOnly = true;
            gridViewTextBoxColumn110.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn110.Width = 85;
            gridViewTextBoxColumn111.DataType = typeof(decimal);
            gridViewTextBoxColumn111.FieldName = "Descuento";
            gridViewTextBoxColumn111.FormatString = "{0:n}";
            gridViewTextBoxColumn111.HeaderText = "Descuento";
            gridViewTextBoxColumn111.Name = "Descuento";
            gridViewTextBoxColumn111.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn111.Width = 95;
            gridViewTextBoxColumn112.DataType = typeof(decimal);
            gridViewTextBoxColumn112.FieldName = "ImporteNota";
            gridViewTextBoxColumn112.FormatString = "{0:N2}";
            gridViewTextBoxColumn112.HeaderText = "Imp. Nota";
            gridViewTextBoxColumn112.IsVisible = false;
            gridViewTextBoxColumn112.Name = "ImporteNota";
            gridViewTextBoxColumn112.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn112.Width = 85;
            gridViewTextBoxColumn113.DataType = typeof(double);
            gridViewTextBoxColumn113.FieldName = "Acumulado";
            gridViewTextBoxColumn113.FormatString = "{0:n}";
            gridViewTextBoxColumn113.HeaderText = "Cobrado";
            gridViewTextBoxColumn113.Name = "Acumulado";
            gridViewTextBoxColumn113.ReadOnly = true;
            gridViewTextBoxColumn113.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn113.Width = 85;
            gridViewTextBoxColumn114.DataType = typeof(decimal);
            gridViewTextBoxColumn114.FieldName = "Saldo";
            gridViewTextBoxColumn114.FormatString = "{0:n}";
            gridViewTextBoxColumn114.HeaderText = "Saldo";
            gridViewTextBoxColumn114.Name = "Saldo";
            gridViewTextBoxColumn114.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn114.Width = 85;
            gridViewTextBoxColumn115.FieldName = "ImportePagado";
            gridViewTextBoxColumn115.FormatString = "{0:n}";
            gridViewTextBoxColumn115.HeaderText = "C. Pagos";
            gridViewTextBoxColumn115.Name = "ImportePagado";
            gridViewTextBoxColumn115.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn115.Width = 85;
            gridViewTextBoxColumn116.FieldName = "SaldoPagos";
            gridViewTextBoxColumn116.FormatString = "{0:n}";
            gridViewTextBoxColumn116.HeaderText = "C. Saldo";
            gridViewTextBoxColumn116.Name = "SaldoPagos";
            gridViewTextBoxColumn116.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn116.Width = 85;
            gridViewTextBoxColumn117.FieldName = "Estado";
            gridViewTextBoxColumn117.HeaderText = "Estado";
            gridViewTextBoxColumn117.Name = "Estado";
            gridViewTextBoxColumn117.Width = 75;
            gridViewTextBoxColumn118.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn118.FieldName = "FechaEstado";
            gridViewTextBoxColumn118.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn118.HeaderText = "Fecha del Estado";
            gridViewTextBoxColumn118.IsVisible = false;
            gridViewTextBoxColumn118.Name = "FechaEstado";
            gridViewTextBoxColumn118.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn118.Width = 70;
            gridViewTextBoxColumn118.WrapText = true;
            gridViewDateTimeColumn17.FieldName = "FechaNuevo";
            gridViewDateTimeColumn17.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn17.HeaderText = "Fecha de Sistema";
            gridViewDateTimeColumn17.IsVisible = false;
            gridViewDateTimeColumn17.Name = "FechaNuevo";
            gridViewDateTimeColumn17.ReadOnly = true;
            gridViewDateTimeColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn17.Width = 70;
            gridViewTextBoxColumn119.FieldName = "Creo";
            gridViewTextBoxColumn119.HeaderText = "Creó";
            gridViewTextBoxColumn119.IsVisible = false;
            gridViewTextBoxColumn119.Name = "Creo";
            gridViewTextBoxColumn119.ReadOnly = true;
            gridViewTextBoxColumn119.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn119.Width = 75;
            gridViewTextBoxColumn120.FieldName = "Modifica";
            gridViewTextBoxColumn120.HeaderText = "Mod.";
            gridViewTextBoxColumn120.IsVisible = false;
            gridViewTextBoxColumn120.Name = "Modifica";
            gridViewTextBoxColumn120.ReadOnly = true;
            gridViewTextBoxColumn120.Width = 75;
            gridViewDateTimeColumn18.FieldName = "FechaMod";
            gridViewDateTimeColumn18.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn18.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn18.HeaderText = "Fecha de Modificación";
            gridViewDateTimeColumn18.IsVisible = false;
            gridViewDateTimeColumn18.Name = "FechaMod";
            gridViewDateTimeColumn18.ReadOnly = true;
            gridViewDateTimeColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn18.Width = 70;
            gridViewDateTimeColumn18.WrapText = true;
            gridViewTextBoxColumn121.FieldName = "UrlFileXml";
            gridViewTextBoxColumn121.HeaderText = "XML";
            gridViewTextBoxColumn121.Name = "UrlFileXml";
            gridViewTextBoxColumn121.ReadOnly = true;
            gridViewTextBoxColumn121.Width = 30;
            gridViewTextBoxColumn122.FieldName = "UrlFilePdf";
            gridViewTextBoxColumn122.HeaderText = "PDF";
            gridViewTextBoxColumn122.Name = "UrlFilePdf";
            gridViewTextBoxColumn122.ReadOnly = true;
            gridViewTextBoxColumn122.Width = 30;
            gridViewTextBoxColumn123.FieldName = "Moneda";
            gridViewTextBoxColumn123.HeaderText = "Moneda";
            gridViewTextBoxColumn123.IsVisible = false;
            gridViewTextBoxColumn123.Name = "Moneda";
            gridViewTextBoxColumn124.FieldName = "Nota";
            gridViewTextBoxColumn124.HeaderText = "Nota";
            gridViewTextBoxColumn124.Name = "Nota";
            gridViewTextBoxColumn124.Width = 100;
            gridViewTextBoxColumn125.DataType = typeof(int);
            gridViewTextBoxColumn125.FieldName = "DiasTranscurridos";
            gridViewTextBoxColumn125.FormatString = "{0:N0}";
            gridViewTextBoxColumn125.HeaderText = "Días Trans.";
            gridViewTextBoxColumn125.Name = "DiasTranscurridos";
            this.gridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn93,
            gridViewTextBoxColumn94,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn95,
            gridViewTextBoxColumn96,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn97,
            gridViewTextBoxColumn98,
            gridViewTextBoxColumn99,
            gridViewTextBoxColumn100,
            gridViewTextBoxColumn101,
            gridViewTextBoxColumn102,
            gridViewTextBoxColumn103,
            gridViewTextBoxColumn104,
            gridViewDateTimeColumn10,
            gridViewDateTimeColumn11,
            gridViewDateTimeColumn12,
            gridViewDateTimeColumn13,
            gridViewDateTimeColumn14,
            gridViewDateTimeColumn15,
            gridViewDateTimeColumn16,
            gridViewTextBoxColumn105,
            gridViewTextBoxColumn106,
            gridViewTextBoxColumn107,
            gridViewTextBoxColumn108,
            gridViewTextBoxColumn109,
            gridViewTextBoxColumn110,
            gridViewTextBoxColumn111,
            gridViewTextBoxColumn112,
            gridViewTextBoxColumn113,
            gridViewTextBoxColumn114,
            gridViewTextBoxColumn115,
            gridViewTextBoxColumn116,
            gridViewTextBoxColumn117,
            gridViewTextBoxColumn118,
            gridViewDateTimeColumn17,
            gridViewTextBoxColumn119,
            gridViewTextBoxColumn120,
            gridViewDateTimeColumn18,
            gridViewTextBoxColumn121,
            gridViewTextBoxColumn122,
            gridViewTextBoxColumn123,
            gridViewTextBoxColumn124,
            gridViewTextBoxColumn125});
            this.gridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridConcepto,
            this.gridCFDIRelacionado,
            this.gridComplementoPagos,
            this.gridComplementoPagoD});
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition14;
            this.gridData.Name = "gridData";
            this.gridData.ShowGroupPanel = false;
            this.gridData.Size = new System.Drawing.Size(1103, 537);
            this.gridData.TabIndex = 15;
            // 
            // gridConcepto
            // 
            this.gridConcepto.Caption = "Conceptos";
            gridViewTextBoxColumn126.DataType = typeof(decimal);
            gridViewTextBoxColumn126.FieldName = "Cantidad";
            gridViewTextBoxColumn126.FormatString = "{0:n}";
            gridViewTextBoxColumn126.HeaderText = "Cantidad";
            gridViewTextBoxColumn126.Name = "Cantidad";
            gridViewTextBoxColumn126.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn126.Width = 70;
            gridViewTextBoxColumn127.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn127.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn127.Name = "ClaveUnidad";
            gridViewTextBoxColumn127.Width = 75;
            gridViewTextBoxColumn128.FieldName = "Unidad";
            gridViewTextBoxColumn128.HeaderText = "Unidad";
            gridViewTextBoxColumn128.Name = "Unidad";
            gridViewTextBoxColumn128.Width = 65;
            gridViewTextBoxColumn129.DataType = typeof(int);
            gridViewTextBoxColumn129.FieldName = "NoOrden";
            gridViewTextBoxColumn129.HeaderText = "No. Orden";
            gridViewTextBoxColumn129.Name = "NoOrden";
            gridViewTextBoxColumn129.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn129.Width = 75;
            gridViewTextBoxColumn130.FieldName = "ClaveProducto";
            gridViewTextBoxColumn130.HeaderText = "Clv. Prod.";
            gridViewTextBoxColumn130.Name = "ClaveProducto";
            gridViewTextBoxColumn130.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn130.Width = 75;
            gridViewTextBoxColumn131.FieldName = "Concepto";
            gridViewTextBoxColumn131.HeaderText = "Concepto";
            gridViewTextBoxColumn131.Name = "Concepto";
            gridViewTextBoxColumn131.Width = 285;
            gridViewTextBoxColumn132.DataType = typeof(decimal);
            gridViewTextBoxColumn132.FieldName = "Unitario";
            gridViewTextBoxColumn132.FormatString = "{0:n2}";
            gridViewTextBoxColumn132.HeaderText = "Unitario";
            gridViewTextBoxColumn132.Name = "Unitario";
            gridViewTextBoxColumn132.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn132.Width = 75;
            gridViewTextBoxColumn133.DataType = typeof(decimal);
            gridViewTextBoxColumn133.FieldName = "SubTotal";
            gridViewTextBoxColumn133.FormatString = "{0:N2}";
            gridViewTextBoxColumn133.HeaderText = "SubTotal";
            gridViewTextBoxColumn133.Name = "SubTotal";
            gridViewTextBoxColumn133.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn133.Width = 75;
            this.gridConcepto.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn126,
            gridViewTextBoxColumn127,
            gridViewTextBoxColumn128,
            gridViewTextBoxColumn129,
            gridViewTextBoxColumn130,
            gridViewTextBoxColumn131,
            gridViewTextBoxColumn132,
            gridViewTextBoxColumn133});
            this.gridConcepto.ViewDefinition = tableViewDefinition8;
            // 
            // gridCFDIRelacionado
            // 
            this.gridCFDIRelacionado.Caption = "CFDI Relacionado";
            gridViewTextBoxColumn134.FieldName = "Clave";
            gridViewTextBoxColumn134.HeaderText = "Clave";
            gridViewTextBoxColumn134.Name = "Clave";
            gridViewTextBoxColumn135.FieldName = "Descripcion";
            gridViewTextBoxColumn135.HeaderText = "Descripción";
            gridViewTextBoxColumn135.Name = "Descripcion";
            gridViewTextBoxColumn135.Width = 200;
            this.gridCFDIRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn134,
            gridViewTextBoxColumn135});
            this.gridCFDIRelacionado.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridComprobanteRelacionado});
            this.gridCFDIRelacionado.ViewDefinition = tableViewDefinition10;
            // 
            // gridComprobanteRelacionado
            // 
            this.gridComprobanteRelacionado.Caption = "Comprobantes";
            gridViewTextBoxColumn136.FieldName = "UUID";
            gridViewTextBoxColumn136.HeaderText = "IdDocumento";
            gridViewTextBoxColumn136.Name = "UUID";
            gridViewTextBoxColumn136.Width = 220;
            gridViewTextBoxColumn137.FieldName = "RFC";
            gridViewTextBoxColumn137.HeaderText = "RFC";
            gridViewTextBoxColumn137.Name = "RFC";
            gridViewTextBoxColumn137.Width = 110;
            gridViewTextBoxColumn138.FieldName = "Nombre";
            gridViewTextBoxColumn138.HeaderText = "Nombre";
            gridViewTextBoxColumn138.Name = "Nombre";
            gridViewTextBoxColumn138.Width = 250;
            gridViewTextBoxColumn139.FieldName = "Serie";
            gridViewTextBoxColumn139.HeaderText = "Serie";
            gridViewTextBoxColumn139.Name = "Serie";
            gridViewTextBoxColumn139.Width = 85;
            gridViewTextBoxColumn140.FieldName = "Folio";
            gridViewTextBoxColumn140.HeaderText = "Folio";
            gridViewTextBoxColumn140.Name = "Folio";
            gridViewTextBoxColumn140.Width = 85;
            gridViewTextBoxColumn141.DataType = typeof(decimal);
            gridViewTextBoxColumn141.FieldName = "Total";
            gridViewTextBoxColumn141.FormatString = "{0:N2}";
            gridViewTextBoxColumn141.HeaderText = "Total";
            gridViewTextBoxColumn141.Name = "Total";
            gridViewTextBoxColumn141.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn141.Width = 85;
            this.gridComprobanteRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn136,
            gridViewTextBoxColumn137,
            gridViewTextBoxColumn138,
            gridViewTextBoxColumn139,
            gridViewTextBoxColumn140,
            gridViewTextBoxColumn141});
            this.gridComprobanteRelacionado.ViewDefinition = tableViewDefinition9;
            // 
            // gridComplementoPagos
            // 
            this.gridComplementoPagos.Caption = "Complemento: Pagos";
            gridViewTextBoxColumn142.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn142.FieldName = "FechaPago";
            gridViewTextBoxColumn142.FormatString = "{0:d}";
            gridViewTextBoxColumn142.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn142.Name = "FechaPago";
            gridViewTextBoxColumn142.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn142.Width = 85;
            gridViewTextBoxColumn143.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn143.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn143.Name = "FormaDePagoP";
            gridViewTextBoxColumn144.FieldName = "MonedaP";
            gridViewTextBoxColumn144.HeaderText = "Moneda";
            gridViewTextBoxColumn144.Name = "MonedaP";
            gridViewTextBoxColumn145.DataType = typeof(decimal);
            gridViewTextBoxColumn145.FieldName = "TipoCambioP";
            gridViewTextBoxColumn145.FormatString = "{0:N2}";
            gridViewTextBoxColumn145.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn145.Name = "TipoCambioP";
            gridViewTextBoxColumn145.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn146.DataType = typeof(decimal);
            gridViewTextBoxColumn146.FieldName = "Monto";
            gridViewTextBoxColumn146.FormatString = "{0:N2}";
            gridViewTextBoxColumn146.HeaderText = "Monto";
            gridViewTextBoxColumn146.Name = "Monto";
            gridViewTextBoxColumn146.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn146.Width = 85;
            gridViewTextBoxColumn147.FieldName = "NumOperacion";
            gridViewTextBoxColumn147.HeaderText = "Num. Operación";
            gridViewTextBoxColumn147.Name = "NumOperacion";
            gridViewTextBoxColumn147.Width = 80;
            gridViewTextBoxColumn148.FieldName = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn148.HeaderText = "RFC Emisor Cta. Ord.";
            gridViewTextBoxColumn148.Name = "RfcEmisorCtaOrd";
            gridViewTextBoxColumn148.Width = 80;
            gridViewTextBoxColumn149.FieldName = "NomBancoOrdExt";
            gridViewTextBoxColumn149.HeaderText = "Nom. Banco Ord. Ext.";
            gridViewTextBoxColumn149.Name = "NomBancoOrdExt";
            gridViewTextBoxColumn149.Width = 120;
            gridViewTextBoxColumn150.FieldName = "CtaOrdenante";
            gridViewTextBoxColumn150.HeaderText = "Cta. Ordenante";
            gridViewTextBoxColumn150.Name = "CtaOrdenante";
            gridViewTextBoxColumn150.Width = 100;
            gridViewTextBoxColumn151.FieldName = "RfcEmisorCtaBen";
            gridViewTextBoxColumn151.HeaderText = "RFC Emisor Cta. Ben.";
            gridViewTextBoxColumn151.Name = "RfcEmisorCtaBen";
            gridViewTextBoxColumn151.Width = 85;
            gridViewTextBoxColumn152.FieldName = "CtaBeneficiario";
            gridViewTextBoxColumn152.HeaderText = "Cta. Beneficiario";
            gridViewTextBoxColumn152.Name = "CtaBeneficiario";
            gridViewTextBoxColumn152.Width = 80;
            gridViewTextBoxColumn153.FieldName = "TipoCadPago";
            gridViewTextBoxColumn153.HeaderText = "Tipo Cad. Pago";
            gridViewTextBoxColumn153.Name = "TipoCadPago";
            gridViewTextBoxColumn153.Width = 80;
            this.gridComplementoPagos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn142,
            gridViewTextBoxColumn143,
            gridViewTextBoxColumn144,
            gridViewTextBoxColumn145,
            gridViewTextBoxColumn146,
            gridViewTextBoxColumn147,
            gridViewTextBoxColumn148,
            gridViewTextBoxColumn149,
            gridViewTextBoxColumn150,
            gridViewTextBoxColumn151,
            gridViewTextBoxColumn152,
            gridViewTextBoxColumn153});
            this.gridComplementoPagos.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridComplementoPagosDoctoRelacionado});
            this.gridComplementoPagos.ViewDefinition = tableViewDefinition12;
            // 
            // gridComplementoPagosDoctoRelacionado
            // 
            this.gridComplementoPagosDoctoRelacionado.Caption = "Documentos relacionados";
            gridViewTextBoxColumn154.FieldName = "Folio";
            gridViewTextBoxColumn154.HeaderText = "Folio";
            gridViewTextBoxColumn154.Name = "Folio";
            gridViewTextBoxColumn154.Width = 75;
            gridViewTextBoxColumn155.FieldName = "Serie";
            gridViewTextBoxColumn155.HeaderText = "Serie";
            gridViewTextBoxColumn155.Name = "Serie";
            gridViewTextBoxColumn155.Width = 75;
            gridViewTextBoxColumn156.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn156.FieldName = "FechaEmision";
            gridViewTextBoxColumn156.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn156.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn156.Name = "FechaEmision";
            gridViewTextBoxColumn156.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn156.Width = 70;
            gridViewTextBoxColumn157.FieldName = "IdDocumento";
            gridViewTextBoxColumn157.HeaderText = "IdDocumento";
            gridViewTextBoxColumn157.Name = "IdDocumento";
            gridViewTextBoxColumn157.Width = 225;
            gridViewTextBoxColumn158.FieldName = "MonedaDR";
            gridViewTextBoxColumn158.HeaderText = "Moneda";
            gridViewTextBoxColumn158.Name = "MonedaDR";
            gridViewTextBoxColumn159.DataType = typeof(decimal);
            gridViewTextBoxColumn159.FieldName = "EquivalenciaDR";
            gridViewTextBoxColumn159.FormatString = "{0:N2}";
            gridViewTextBoxColumn159.HeaderText = "Tipo de Cambio";
            gridViewTextBoxColumn159.Name = "EquivalenciaDR";
            gridViewTextBoxColumn159.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn160.FieldName = "MetodoPago";
            gridViewTextBoxColumn160.HeaderText = "Método de Pago";
            gridViewTextBoxColumn160.Name = "MetodoPago";
            gridViewTextBoxColumn161.FieldName = "NumParcialidad";
            gridViewTextBoxColumn161.HeaderText = "Num. Parcialidad";
            gridViewTextBoxColumn161.Name = "NumParcialidad";
            gridViewTextBoxColumn161.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn162.DataType = typeof(decimal);
            gridViewTextBoxColumn162.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn162.FormatString = "{0:N2}";
            gridViewTextBoxColumn162.HeaderText = "Saldo Anterior";
            gridViewTextBoxColumn162.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn162.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn162.Width = 85;
            gridViewTextBoxColumn163.DataType = typeof(decimal);
            gridViewTextBoxColumn163.FieldName = "ImpPagado";
            gridViewTextBoxColumn163.FormatString = "{0:N2}";
            gridViewTextBoxColumn163.HeaderText = "Imp. Pagado";
            gridViewTextBoxColumn163.Name = "ImpPagado";
            gridViewTextBoxColumn163.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn163.Width = 85;
            gridViewTextBoxColumn164.DataType = typeof(decimal);
            gridViewTextBoxColumn164.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn164.FormatString = "{0:N2}";
            gridViewTextBoxColumn164.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn164.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn164.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn164.Width = 85;
            gridViewTextBoxColumn165.FieldName = "ObjetoImpDR";
            gridViewTextBoxColumn165.HeaderText = "Obj. Impuesto";
            gridViewTextBoxColumn165.Name = "ObjetoImpDR";
            gridViewTextBoxColumn165.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            conditionalFormattingObject3.ApplyToRow = true;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.Name = "Relacion";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.DimGray;
            conditionalFormattingObject3.TValue1 = "0";
            conditionalFormattingObject3.TValue2 = "0";
            gridViewTextBoxColumn166.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn166.DataType = typeof(int);
            gridViewTextBoxColumn166.FieldName = "IdComprobante";
            gridViewTextBoxColumn166.HeaderText = "IdComprobante";
            gridViewTextBoxColumn166.IsVisible = false;
            gridViewTextBoxColumn166.Name = "IdComprobante";
            gridViewTextBoxColumn166.VisibleInColumnChooser = false;
            this.gridComplementoPagosDoctoRelacionado.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn154,
            gridViewTextBoxColumn155,
            gridViewTextBoxColumn156,
            gridViewTextBoxColumn157,
            gridViewTextBoxColumn158,
            gridViewTextBoxColumn159,
            gridViewTextBoxColumn160,
            gridViewTextBoxColumn161,
            gridViewTextBoxColumn162,
            gridViewTextBoxColumn163,
            gridViewTextBoxColumn164,
            gridViewTextBoxColumn165,
            gridViewTextBoxColumn166});
            this.gridComplementoPagosDoctoRelacionado.ViewDefinition = tableViewDefinition11;
            // 
            // gridComplementoPagoD
            // 
            this.gridComplementoPagoD.Caption = "Complemento: Pagos Relacionado";
            gridViewTextBoxColumn167.FieldName = "StatusP";
            gridViewTextBoxColumn167.HeaderText = "Status";
            gridViewTextBoxColumn167.Name = "StatusP";
            gridViewTextBoxColumn167.Width = 75;
            gridViewTextBoxColumn168.FieldName = "FolioP";
            gridViewTextBoxColumn168.HeaderText = "Folio";
            gridViewTextBoxColumn168.Name = "FolioP";
            gridViewTextBoxColumn169.FieldName = "SerieP";
            gridViewTextBoxColumn169.HeaderText = "Serie";
            gridViewTextBoxColumn169.Name = "SerieP";
            gridViewTextBoxColumn170.FieldName = "EmisorRFCP";
            gridViewTextBoxColumn170.HeaderText = "RFC";
            gridViewTextBoxColumn170.Name = "EmisorRFCP";
            gridViewTextBoxColumn170.Width = 95;
            gridViewTextBoxColumn171.FieldName = "EmisorNombreP";
            gridViewTextBoxColumn171.HeaderText = "Nombre";
            gridViewTextBoxColumn171.Name = "EmisorNombreP";
            gridViewTextBoxColumn171.Width = 200;
            gridViewTextBoxColumn172.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn172.FieldName = "FechaEmisionP";
            gridViewTextBoxColumn172.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn172.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn172.Name = "FechaEmisionP";
            gridViewTextBoxColumn172.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn172.Width = 75;
            gridViewTextBoxColumn173.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn173.FieldName = "FechaPago";
            gridViewTextBoxColumn173.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn173.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn173.Name = "FechaPago";
            gridViewTextBoxColumn173.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn173.Width = 75;
            gridViewTextBoxColumn174.FieldName = "IdDocumentoP";
            gridViewTextBoxColumn174.HeaderText = "IdDocumento";
            gridViewTextBoxColumn174.Name = "IdDocumentoP";
            gridViewTextBoxColumn174.Width = 220;
            gridViewTextBoxColumn175.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn175.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn175.Name = "FormaDePagoP";
            gridViewTextBoxColumn175.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn176.FieldName = "MonedaP";
            gridViewTextBoxColumn176.HeaderText = "Moneda";
            gridViewTextBoxColumn176.Name = "MonedaP";
            gridViewTextBoxColumn176.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn177.DataType = typeof(decimal);
            gridViewTextBoxColumn177.FieldName = "TipoCambioP";
            gridViewTextBoxColumn177.FormatString = "{0:N2}";
            gridViewTextBoxColumn177.HeaderText = "T. Cambio";
            gridViewTextBoxColumn177.Name = "TipoCambioP";
            gridViewTextBoxColumn177.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn178.FieldName = "NumOperacion";
            gridViewTextBoxColumn178.HeaderText = "Núm. Operación";
            gridViewTextBoxColumn178.Name = "NumOperacion";
            gridViewTextBoxColumn178.Width = 140;
            gridViewTextBoxColumn179.DataType = typeof(decimal);
            gridViewTextBoxColumn179.FieldName = "NumParcialidad";
            gridViewTextBoxColumn179.FormatString = "{0:N0}";
            gridViewTextBoxColumn179.HeaderText = "Parcialidad";
            gridViewTextBoxColumn179.Name = "NumParcialidad";
            gridViewTextBoxColumn179.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn180.DataType = typeof(decimal);
            gridViewTextBoxColumn180.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn180.FormatString = "{0:N2}";
            gridViewTextBoxColumn180.HeaderText = "Saldo Ant.";
            gridViewTextBoxColumn180.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn180.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn180.Width = 85;
            gridViewTextBoxColumn181.DataType = typeof(decimal);
            gridViewTextBoxColumn181.FieldName = "ImpPagado";
            gridViewTextBoxColumn181.FormatString = "{0:N2}";
            gridViewTextBoxColumn181.HeaderText = "Imp. Pagado";
            gridViewTextBoxColumn181.Name = "ImpPagado";
            gridViewTextBoxColumn181.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn181.Width = 85;
            gridViewTextBoxColumn182.DataType = typeof(decimal);
            gridViewTextBoxColumn182.FieldName = "ImpSaldoInsoluto";
            gridViewTextBoxColumn182.FormatString = "{0:N2}";
            gridViewTextBoxColumn182.HeaderText = "Saldo Insoluto";
            gridViewTextBoxColumn182.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn182.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn182.Width = 85;
            gridViewTextBoxColumn183.FieldName = "EstadoP";
            gridViewTextBoxColumn183.HeaderText = "Estado";
            gridViewTextBoxColumn183.Name = "EstadoP";
            gridViewTextBoxColumn183.Width = 75;
            conditionalFormattingObject4.ApplyToRow = true;
            conditionalFormattingObject4.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.Name = "Relacionado";
            conditionalFormattingObject4.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            conditionalFormattingObject4.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.TValue1 = "0";
            conditionalFormattingObject4.TValue2 = "0";
            gridViewTextBoxColumn184.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            gridViewTextBoxColumn184.DataType = typeof(int);
            gridViewTextBoxColumn184.FieldName = "IdComprobanteR";
            gridViewTextBoxColumn184.HeaderText = "Relacionado";
            gridViewTextBoxColumn184.IsVisible = false;
            gridViewTextBoxColumn184.Name = "IdComprobanteR";
            gridViewTextBoxColumn184.VisibleInColumnChooser = false;
            this.gridComplementoPagoD.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn167,
            gridViewTextBoxColumn168,
            gridViewTextBoxColumn169,
            gridViewTextBoxColumn170,
            gridViewTextBoxColumn171,
            gridViewTextBoxColumn172,
            gridViewTextBoxColumn173,
            gridViewTextBoxColumn174,
            gridViewTextBoxColumn175,
            gridViewTextBoxColumn176,
            gridViewTextBoxColumn177,
            gridViewTextBoxColumn178,
            gridViewTextBoxColumn179,
            gridViewTextBoxColumn180,
            gridViewTextBoxColumn181,
            gridViewTextBoxColumn182,
            gridViewTextBoxColumn183,
            gridViewTextBoxColumn184});
            this.gridComplementoPagoD.ViewDefinition = tableViewDefinition13;
            // 
            // TReporte
            // 
            this.TReporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TReporte.Location = new System.Drawing.Point(0, 0);
            this.TReporte.Name = "TReporte";
            this.TReporte.ShowActualizar = true;
            this.TReporte.ShowAutosuma = false;
            this.TReporte.ShowCancelar = false;
            this.TReporte.ShowCerrar = true;
            this.TReporte.ShowEditar = true;
            this.TReporte.ShowEjercicio = true;
            this.TReporte.ShowExportarExcel = false;
            this.TReporte.ShowFiltro = true;
            this.TReporte.ShowHerramientas = false;
            this.TReporte.ShowImprimir = false;
            this.TReporte.ShowItem = false;
            this.TReporte.ShowNuevo = true;
            this.TReporte.ShowPeriodo = true;
            this.TReporte.Size = new System.Drawing.Size(1103, 30);
            this.TReporte.TabIndex = 1;
            // 
            // ComprobantesReporteEForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 567);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.TReporte);
            this.Name = "ComprobantesReporteEForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Comprobantes Reporte";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobantesReporteEForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCFDIRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComprobanteRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagosDoctoRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplementoPagoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal Telerik.WinControls.UI.RadGridView gridData;
        private Telerik.WinControls.UI.GridViewTemplate gridConcepto;
        private Telerik.WinControls.UI.GridViewTemplate gridCFDIRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate gridComprobanteRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate gridComplementoPagos;
        private Telerik.WinControls.UI.GridViewTemplate gridComplementoPagosDoctoRelacionado;
        private Telerik.WinControls.UI.GridViewTemplate gridComplementoPagoD;
        private Common.Forms.TBarCommonControl TReporte;
    }
}