﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class CertificadoForm : RadForm {
        private Color colorInvalido;
        private Color colorValido;
        private Color colorEditable;
        private string _pathOpenSSL = @"D:\bitbucket\jaeger_edita_desktop\documentos";

        public CertificadoForm() {
            InitializeComponent();
        }

        private void CertificadoForm_Load(object sender, EventArgs e) {
            this.colorInvalido = Color.Bisque;
            this.colorValido = Color.MintCream;
            this.colorEditable = Color.LemonChiffon;
        }

        private void ButtonCER_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.cer",
                DefaultExt = ".cer",
                Filter = "Archivo CER (*.cer)|*.cer",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.txtArchivoCertificado.Text = openFileDialog.FileName;
                this.VerificacionDeArchivos();
                this.LeerCertificado();
            }
        }

        private void ButtonKEY_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo de la llave privada *.key",
                DefaultExt = ".key",
                Filter = "Archivo KEY (*.key)|*.key",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.txtLlavePrivada.Text = openFileDialog.FileName;
            }
        }

        private void VerificacionDeArchivos() {
            if (!File.Exists(this.txtArchivoCertificado.Text)) {
                this.txtArchivoCertificado.BackColor = this.colorInvalido;
            } else {
                this.txtArchivoCertificado.BackColor = this.colorValido;
            }
            if (!File.Exists(this.txtLlavePrivada.Text)) {
                this.txtLlavePrivada.BackColor = this.colorInvalido;
            } else {
                this.txtLlavePrivada.BackColor = this.colorValido;
            }
            this.VerificarContrasenas();
        }

        private void VerificarContrasenas() {
            if (this.txtConfirmacion.Text == "") {
                this.txtContrasena.BackColor = this.colorEditable;
                this.txtConfirmacion.BackColor = this.colorEditable;
            } else if (this.txtContrasena.Text == this.txtConfirmacion.Text) {
                this.txtContrasena.BackColor = this.colorEditable;
                this.txtConfirmacion.BackColor = this.colorEditable;
            } else {
                this.txtContrasena.BackColor = this.colorInvalido;
                this.txtConfirmacion.BackColor = this.colorInvalido;
            }
        }

        private void LeerCertificado() {
            string text = this.txtArchivoCertificado.Text;
            if (!File.Exists(text)) {
                this.txtArchivoCertificado.BackColor = this.colorInvalido;
                this.txtCertificadoB64.Text = "";
            } else {

                if (LeerCertificado(text)) {
                    this.txtNumeroDeSerieCertificado.BackColor = this.colorValido;
                } else {
                    RadMessageBox.Show("no se puede leer certificado, intente nuevamente.", "Error", MessageBoxButtons.OK);
                    this.txtArchivoCertificado.BackColor = this.colorInvalido;
                    this.txtCertificadoB64.Text = "";
                }
            }
        }

        private bool LeerCertificado(string fileName) {
            var informacion = Aplication.Comprobante.Services.CertificadoService.Certificado(fileName);
            if (informacion != null) {
                txtValidoDesde.Text = informacion.NotAfter.ToString("yyyy/MM/dd HH:mm:ss");
                txtValidoHasta.Text = informacion.NotBefore.ToString("yyyy/MM/dd HH:mm:ss");
                this.RFC.Text = informacion.RFC;
                this.TipoCertificado.Text = informacion.Tipo;
                this.txtNumeroDeSerieCertificado.Text = informacion.Serie;
                this.txtCertificadoB64.Text = informacion.CerB64;
                this.txtRazonSocial.Text = informacion.Receptor;
                return true;
            }
            RadMessageBox.Show("Ocurrió un error al leer el certificado.\r\nCertificado no válido. ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            return false;
        }

        private void CheckBox1_CheckStateChanged(object sender, EventArgs e) {
            if (!this.checkBox1.Checked) {
                this.txtContrasena.PasswordChar = '*';
                this.txtConfirmacion.PasswordChar = '*';
            } else {
                this.txtContrasena.PasswordChar = '\0';
                this.txtConfirmacion.PasswordChar = (char)0;
            }
        }

        private void ButtonVerificar_Click(object sender, EventArgs e) {
            if (this.EsInformacionValida()) {
                var _resultado = Aplication.Comprobante.Services.CertificadoService.ComprobanteKey(this.txtArchivoCertificado.Text, this.txtLlavePrivada.Text, this.txtContrasena.Text);
                RadMessageBox.Show(this, _resultado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        private bool EsInformacionValida() {
            bool flag;
            Convert.ToDateTime(this.txtValidoHasta.Text.Substring(0, 10));
            if (this.txtArchivoCertificado.Text == "") {
                RadMessageBox.Show("No ha seleccionado el archivo del certificado", "Sin certificado", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                flag = false;
            } else if (File.Exists(this.txtArchivoCertificado.Text)) {
                if (this.txtCertificadoB64.Text == "") {
                    RadMessageBox.Show("El certificado se encuentra vacío", "Certificado vacío", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
                if (this.txtLlavePrivada.Text != "") {
                    if (!File.Exists(this.txtLlavePrivada.Text)) {
                        RadMessageBox.Show("El archivo de llave privada no existe", "Archivo inválido", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                    if (this.txtContrasena.Text == "") {
                        RadMessageBox.Show("La contrseña no puede estar vacía", "Sin contraseña", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        flag = false;
                    } else if (this.txtContrasena.Text != this.txtConfirmacion.Text) {
                        RadMessageBox.Show("La contraseña y su confirmación son inconsistentes", "Confirmación erronea", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        flag = false;
                    } else if (DateTime.Compare(DateTime.Now.Date, Convert.ToDateTime(this.txtValidoDesde.Text.Substring(0, 10))) < 0) {
                        RadMessageBox.Show("La vigencia del certificado no ha entrado en vigor", "Vigencia sin vigor", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        flag = false;
                    } else if (DateTime.Compare(DateTime.Now.Date, Convert.ToDateTime(this.txtValidoHasta.Text.Substring(0, 10))) <= 0) {
                        flag = true;
                    } else {
                        RadMessageBox.Show("La vigencia del certificado ha expirado", "Vigencia expirada", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        flag = false;
                    }
                } else {
                    RadMessageBox.Show("No ha seleccionado el archivo de la llave privada", "Sin llave privada", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    flag = false;
                }
            } else {
                MessageBox.Show("El archivo de certificado seleccionado no existe", "Archivo inválido", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                flag = false;
            }
            return flag;
        }

        private void GenerarArchivoPFX() {
            
            string _pathDestino = this.txtArchivoPFX.Text;
            Process paso1 = new Process();
            paso1.StartInfo.FileName = Path.Combine(this._pathOpenSSL, "openssl.exe");
            string[] text = new string[] { "x509 -inform DER -outform PEM -in \"", this.txtArchivoCertificado.Text, "\" -pubkey -out \"", _pathDestino.ToString(), "\\", this.RFC.Text, "Cer.pem\"" };
            string str2 = string.Concat(text);
            paso1.StartInfo.Arguments = str2;
            paso1.StartInfo.WorkingDirectory = _pathOpenSSL;
            paso1.StartInfo.UseShellExecute = false;
            paso1.StartInfo.RedirectStandardOutput = true;
            paso1.Start();
            paso1.WaitForExit();

            Process paso2 = new Process();
            paso2.StartInfo.FileName = Path.Combine(_pathOpenSSL, "openssl.exe");
            text = new string[] { "pkcs8 -inform DER -in \"", this.txtLlavePrivada.Text, "\" -passin pass:\"", this.txtContrasena.Text, "\" -out \"", _pathDestino.ToString(), "\\", this.RFC.Text, "key.pem\"" };
            string str3 = string.Concat(text);
            paso2.StartInfo.Arguments = str3;
            paso2.StartInfo.WorkingDirectory = _pathOpenSSL;
            paso2.StartInfo.UseShellExecute = false;
            paso2.StartInfo.RedirectStandardOutput = true;
            paso2.Start();
            paso2.WaitForExit();
            
            Process paso3 = new Process();
            paso3.StartInfo.FileName = Path.Combine(_pathOpenSSL, "openssl.exe");
            text = new string[] { "pkcs12 -export -inkey \"", _pathDestino.ToString(), "\\", this.RFC.Text, "key.pem\" -in \"", _pathDestino.ToString(), "\\", this.RFC.Text, "Cer.pem\" -out \"", _pathDestino.ToString(), "\\", this.RFC.Text, ".pfx\" -passout pass:\"", this.txtContrasena.Text, "\"" };
            string str4 = string.Concat(text);
            paso3.StartInfo.Arguments = str4;
            paso3.StartInfo.WorkingDirectory = this._pathOpenSSL;
            paso3.StartInfo.UseShellExecute = false;
            paso3.StartInfo.RedirectStandardOutput = true;
            paso3.Start();
            paso3.WaitForExit();
            paso3.Close();
            this.txtArchivoPFX.Text = string.Concat(_pathDestino, "\\", this.RFC.Text, ".pfx");
        }

        private void ButtonGeneraPFX_Click(object sender, EventArgs e) {
            GenerarArchivoPFX();
        }

        private void ButtonOpenSSL_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.exe",
                DefaultExt = ".exe",
                Filter = "Archivo CER (*.exe)|*.exe",
                FilterIndex = 1,
                FileName = "openssl.exe"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.label14.Text = openFileDialog.FileName;
            }
        }

        private void ButtonPathPFX_Click(object sender, EventArgs e) {
            var openFolrderDialog = new FolderBrowserDialog();
            if (openFolrderDialog.ShowDialog(this) == DialogResult.OK) {
                this.txtArchivoPFX.Text = openFolrderDialog.SelectedPath;
            }
        }
    }
}
