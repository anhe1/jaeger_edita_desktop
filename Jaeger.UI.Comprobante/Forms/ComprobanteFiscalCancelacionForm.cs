﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Contracts;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteFiscalCancelacionForm : RadForm {
        #region declaraciones
        protected internal IComprobanteFiscalSingleModel _Single;
        protected internal IComprobantesFiscalesService _Service;
        #endregion

        public ComprobanteFiscalCancelacionForm(IComprobanteFiscalSingleModel model, IComprobantesFiscalesService service) {
            InitializeComponent();
            this._Single = model;
            this._Service = service;
        }

        private void ComprobanteFiscalCancelacionForm_Load(object sender, EventArgs e) {
            this.MotivoCancelacion.DataSource = ComprobanteCommonService.GetMotivoCancelacion();
            this.IdDocumento.Text = this._Single.IdDocumento;
        }

        private void TCancelar_Autorizar_Click(object sender, EventArgs e) {
            var _seleccionado = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as MotivoCancelacionModel;
            if (_seleccionado != null) {
                using (var espera = new Waiting1Form(this.Cancelar)) {
                    espera.Text = "Solicitando cancelación ...";
                    espera.ShowDialog(this);
                }
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Buscar_Click(object sender, EventArgs e) {
            var buscar = new ComprobantesBuscarForm("", Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido);
            buscar.AgregarComprobante += this.Buscar_AgregarComprobante;
            buscar.ShowDialog(this);
        }

        private void Buscar_AgregarComprobante(object sender, IComprobanteFiscalSingleModel e) {
            throw new NotImplementedException();
        }

        private void Cancelar() {
            var _seleccionado = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as MotivoCancelacionModel;
            if (_seleccionado != null) {
                this._Service.Cancelar(this._Single, _seleccionado.Id);
            }
        }
    }
}
