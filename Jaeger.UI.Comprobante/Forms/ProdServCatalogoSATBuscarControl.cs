﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ProdServCatalogoSATBuscarControl : UserControl {
        protected IProdServsCatalogo catalogo;
        private string descripcion;

        public event EventHandler<ComprobanteConceptoDetailModel> Selected;
        public void OnSelected(ComprobanteConceptoDetailModel e) {
            if (this.Selected != null) {
                this.Selected(this, e);
            }
        }

        public ProdServCatalogoSATBuscarControl() {
            InitializeComponent();
        }

        private void ProdServCatalogoBuscarControl_Load(object sender, EventArgs e) {

        }

        private void gridProductoSAT_DoubleClick(object sender, EventArgs e) {
            if (this.gridProductoSAT.CurrentRow != null) {
                var seleccionado = this.GetSelected();
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }

        public void SetService() {
            this.catalogo = new ProdServsCatalogo();
            if (this.WorkerService.IsBusy == false) {
                this.WorkerService.RunWorkerAsync();
            }
        }

        public void Search(string descripcion) {
            this.descripcion = descripcion;
            this.gridProductoSAT.DataSource = this.catalogo.Productos(this.descripcion);
        }

        public void SetFilter(ToggleState toggleState) {
            this.gridProductoSAT.ActivateFilter(toggleState);
        }

        public ComprobanteConceptoDetailModel GetSelected() {
            if (this.gridProductoSAT.CurrentRow != null) {
                var s = this.gridProductoSAT.CurrentRow.DataBoundItem as ClaveProdServ;
                if (s != null) {
                    var seleccionado = new ComprobanteConceptoDetailModel {
                        Activo = true,
                        Cantidad = 0,
                        ClaveProdServ = s.Clave,
                        Descripcion = s.Descripcion
                    };
                    return seleccionado;
                }
            }
            return null;
        }

        private void WorkerService_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e) {
            this.catalogo.Load();
        }
    }
}
