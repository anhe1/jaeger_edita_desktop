﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteContribuyenteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RegimenFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblRegimenFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblDomicilioFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblNumRegTrib = new Telerik.WinControls.UI.RadLabel();
            this.lblResidenciaFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblUsoCFDI = new Telerik.WinControls.UI.RadLabel();
            this.DomicilioFiscal = new Telerik.WinControls.UI.RadTextBox();
            this.ResidenciaFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.UsoCFDI = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.NumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.Actualizar = new Telerik.WinControls.UI.RadButton();
            this.Agregar = new Telerik.WinControls.UI.RadButton();
            this.RFC = new Telerik.WinControls.UI.RadTextBox();
            this.Nombre = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.PanelNombre = new Telerik.WinControls.UI.RadPanel();
            this.PanelRFC = new Telerik.WinControls.UI.RadPanel();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.IdDirectorio = new Telerik.WinControls.UI.RadSpinEditor();
            this.PanelRegimen = new Telerik.WinControls.UI.RadPanel();
            this.PanelUso = new Telerik.WinControls.UI.RadPanel();
            this.PanelResidencia = new Telerik.WinControls.UI.RadPanel();
            this.Panel4 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilioFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumRegTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DomicilioFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelNombre)).BeginInit();
            this.PanelNombre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRFC)).BeginInit();
            this.PanelRFC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRegimen)).BeginInit();
            this.PanelRegimen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelUso)).BeginInit();
            this.PanelUso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelResidencia)).BeginInit();
            this.PanelResidencia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel4)).BeginInit();
            this.Panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // RegimenFiscal
            // 
            this.RegimenFiscal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RegimenFiscal.DisplayMember = "Descriptor";
            this.RegimenFiscal.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // RegimenFiscal.NestedRadGridView
            // 
            this.RegimenFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.RegimenFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegimenFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RegimenFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn12.FieldName = "Clave";
            gridViewTextBoxColumn12.HeaderText = "Clave";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Clave";
            gridViewTextBoxColumn13.FieldName = "Descripcion";
            gridViewTextBoxColumn13.HeaderText = "Descripción";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Descripcion";
            gridViewTextBoxColumn14.FieldName = "Descriptor";
            gridViewTextBoxColumn14.HeaderText = "Descriptor";
            gridViewTextBoxColumn14.Name = "Descriptor";
            gridViewTextBoxColumn14.Width = 250;
            this.RegimenFiscal.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.RegimenFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.RegimenFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.RegimenFiscal.EditorControl.Name = "NestedRadGridView";
            this.RegimenFiscal.EditorControl.ReadOnly = true;
            this.RegimenFiscal.EditorControl.ShowGroupPanel = false;
            this.RegimenFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.RegimenFiscal.EditorControl.TabIndex = 0;
            this.RegimenFiscal.Location = new System.Drawing.Point(90, 0);
            this.RegimenFiscal.Name = "RegimenFiscal";
            this.RegimenFiscal.NullText = "Régimen Fiscal";
            this.RegimenFiscal.Size = new System.Drawing.Size(183, 20);
            this.RegimenFiscal.TabIndex = 103;
            this.RegimenFiscal.TabStop = false;
            // 
            // lblRegimenFiscal
            // 
            this.lblRegimenFiscal.Location = new System.Drawing.Point(2, 2);
            this.lblRegimenFiscal.Name = "lblRegimenFiscal";
            this.lblRegimenFiscal.Size = new System.Drawing.Size(83, 18);
            this.lblRegimenFiscal.TabIndex = 104;
            this.lblRegimenFiscal.Text = "Régimen Fiscal:";
            // 
            // lblDomicilioFiscal
            // 
            this.lblDomicilioFiscal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDomicilioFiscal.Location = new System.Drawing.Point(720, 1);
            this.lblDomicilioFiscal.Name = "lblDomicilioFiscal";
            this.lblDomicilioFiscal.Size = new System.Drawing.Size(86, 18);
            this.lblDomicilioFiscal.TabIndex = 101;
            this.lblDomicilioFiscal.Text = "Domicilio Fiscal:";
            // 
            // lblNumRegTrib
            // 
            this.lblNumRegTrib.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumRegTrib.Location = new System.Drawing.Point(720, 25);
            this.lblNumRegTrib.Name = "lblNumRegTrib";
            this.lblNumRegTrib.Size = new System.Drawing.Size(85, 18);
            this.lblNumRegTrib.TabIndex = 99;
            this.lblNumRegTrib.Text = "Núm. Reg. Trib.:";
            // 
            // lblResidenciaFiscal
            // 
            this.lblResidenciaFiscal.Location = new System.Drawing.Point(2, 2);
            this.lblResidenciaFiscal.Name = "lblResidenciaFiscal";
            this.lblResidenciaFiscal.Size = new System.Drawing.Size(59, 18);
            this.lblResidenciaFiscal.TabIndex = 97;
            this.lblResidenciaFiscal.Text = "Res. Fiscal:";
            // 
            // lblUsoCFDI
            // 
            this.lblUsoCFDI.Location = new System.Drawing.Point(3, 2);
            this.lblUsoCFDI.Name = "lblUsoCFDI";
            this.lblUsoCFDI.Size = new System.Drawing.Size(70, 18);
            this.lblUsoCFDI.TabIndex = 95;
            this.lblUsoCFDI.Text = "Uso de CFDI:";
            // 
            // DomicilioFiscal
            // 
            this.DomicilioFiscal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DomicilioFiscal.Location = new System.Drawing.Point(810, 0);
            this.DomicilioFiscal.MaxLength = 5;
            this.DomicilioFiscal.Name = "DomicilioFiscal";
            this.DomicilioFiscal.NullText = "C. Postal";
            this.DomicilioFiscal.Size = new System.Drawing.Size(87, 20);
            this.DomicilioFiscal.TabIndex = 102;
            this.DomicilioFiscal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ResidenciaFiscal
            // 
            this.ResidenciaFiscal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResidenciaFiscal.AutoSizeDropDownHeight = true;
            this.ResidenciaFiscal.AutoSizeDropDownToBestFit = true;
            // 
            // ResidenciaFiscal.NestedRadGridView
            // 
            this.ResidenciaFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ResidenciaFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResidenciaFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ResidenciaFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ResidenciaFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.ResidenciaFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.ResidenciaFiscal.EditorControl.Name = "NestedRadGridView";
            this.ResidenciaFiscal.EditorControl.ReadOnly = true;
            this.ResidenciaFiscal.EditorControl.ShowGroupPanel = false;
            this.ResidenciaFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ResidenciaFiscal.EditorControl.TabIndex = 0;
            this.ResidenciaFiscal.Location = new System.Drawing.Point(67, 1);
            this.ResidenciaFiscal.Name = "ResidenciaFiscal";
            this.ResidenciaFiscal.NullText = "Residencia Fiscal";
            this.ResidenciaFiscal.Size = new System.Drawing.Size(132, 20);
            this.ResidenciaFiscal.TabIndex = 98;
            this.ResidenciaFiscal.TabStop = false;
            // 
            // UsoCFDI
            // 
            this.UsoCFDI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsoCFDI.AutoSizeDropDownHeight = true;
            this.UsoCFDI.AutoSizeDropDownToBestFit = true;
            this.UsoCFDI.DisplayMember = "Descriptor";
            this.UsoCFDI.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // UsoCFDI.NestedRadGridView
            // 
            this.UsoCFDI.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.UsoCFDI.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsoCFDI.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UsoCFDI.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.UsoCFDI.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn15.FieldName = "Clave";
            gridViewTextBoxColumn15.HeaderText = "Clave";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "Clave";
            gridViewTextBoxColumn16.FieldName = "Descripcion";
            gridViewTextBoxColumn16.HeaderText = "Descripción";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Descripcion";
            gridViewTextBoxColumn16.Width = 200;
            gridViewTextBoxColumn17.FieldName = "Descriptor";
            gridViewTextBoxColumn17.HeaderText = "Descriptor";
            gridViewTextBoxColumn17.Name = "Descriptor";
            gridViewTextBoxColumn17.Width = 250;
            this.UsoCFDI.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17});
            this.UsoCFDI.EditorControl.MasterTemplate.EnableGrouping = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.UsoCFDI.EditorControl.Name = "NestedRadGridView";
            this.UsoCFDI.EditorControl.ReadOnly = true;
            this.UsoCFDI.EditorControl.ShowGroupPanel = false;
            this.UsoCFDI.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.UsoCFDI.EditorControl.TabIndex = 0;
            this.UsoCFDI.Location = new System.Drawing.Point(79, 0);
            this.UsoCFDI.Name = "UsoCFDI";
            this.UsoCFDI.NullText = "Uso de CFDI";
            this.UsoCFDI.Size = new System.Drawing.Size(160, 20);
            this.UsoCFDI.TabIndex = 96;
            this.UsoCFDI.TabStop = false;
            this.UsoCFDI.ValueMember = "Clave";
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumRegIdTrib.Location = new System.Drawing.Point(810, 24);
            this.NumRegIdTrib.MaxLength = 32;
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.NullText = "Núm. Registro Trib.";
            this.NumRegIdTrib.Size = new System.Drawing.Size(87, 20);
            this.NumRegIdTrib.TabIndex = 100;
            // 
            // Actualizar
            // 
            this.Actualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Actualizar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Actualizar.Enabled = false;
            this.Actualizar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.refresh_16px;
            this.Actualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Actualizar.Location = new System.Drawing.Point(652, 0);
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(20, 20);
            this.Actualizar.TabIndex = 377;
            this.Actualizar.Text = "radButton1";
            // 
            // Agregar
            // 
            this.Agregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Agregar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Agregar.Enabled = false;
            this.Agregar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.add_user_male_16px;
            this.Agregar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Agregar.Location = new System.Drawing.Point(696, 0);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(20, 20);
            this.Agregar.TabIndex = 378;
            this.Agregar.Text = "radButton1";
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(35, 0);
            this.RFC.MaxLength = 14;
            this.RFC.Name = "RFC";
            this.RFC.NullText = "Registro Federal";
            this.RFC.ReadOnly = true;
            this.RFC.Size = new System.Drawing.Size(100, 20);
            this.RFC.TabIndex = 15;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Nombre
            // 
            this.Nombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nombre.AutoSizeDropDownHeight = true;
            this.Nombre.AutoSizeDropDownToBestFit = true;
            this.Nombre.DisplayMember = "Nombre";
            // 
            // Nombre.NestedRadGridView
            // 
            this.Nombre.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nombre.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nombre.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nombre.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nombre.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nombre.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn18.DataType = typeof(int);
            gridViewTextBoxColumn18.FieldName = "IdDirectorio";
            gridViewTextBoxColumn18.HeaderText = "ID";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "IdDirectorio";
            gridViewTextBoxColumn18.VisibleInColumnChooser = false;
            gridViewTextBoxColumn19.FieldName = "Clave";
            gridViewTextBoxColumn19.HeaderText = "Clave";
            gridViewTextBoxColumn19.Name = "Clave";
            gridViewTextBoxColumn19.Width = 75;
            gridViewTextBoxColumn20.FieldName = "RFC";
            gridViewTextBoxColumn20.HeaderText = "RFC";
            gridViewTextBoxColumn20.Name = "RFC";
            gridViewTextBoxColumn20.Width = 110;
            gridViewTextBoxColumn21.FieldName = "Nombre";
            gridViewTextBoxColumn21.HeaderText = "Denominación o Razón Social";
            gridViewTextBoxColumn21.Name = "Nombre";
            gridViewTextBoxColumn21.Width = 250;
            gridViewTextBoxColumn22.FieldName = "NombreComercial";
            gridViewTextBoxColumn22.HeaderText = "Nombre Comercial";
            gridViewTextBoxColumn22.Name = "NombreComercial";
            gridViewTextBoxColumn22.Width = 150;
            this.Nombre.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22});
            this.Nombre.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nombre.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nombre.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nombre.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.Nombre.EditorControl.Name = "NestedRadGridView";
            this.Nombre.EditorControl.ReadOnly = true;
            this.Nombre.EditorControl.ShowGroupPanel = false;
            this.Nombre.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Nombre.EditorControl.TabIndex = 0;
            this.Nombre.Location = new System.Drawing.Point(59, 0);
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Denominación o Razon Social ";
            this.Nombre.Size = new System.Drawing.Size(455, 20);
            this.Nombre.TabIndex = 11;
            this.Nombre.TabStop = false;
            this.Nombre.ValueMember = "Id";
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(2, 1);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(50, 18);
            this.lblNombre.TabIndex = 10;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(4, 1);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 14;
            this.lblRFC.Text = "RFC:";
            // 
            // PanelNombre
            // 
            this.PanelNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelNombre.Controls.Add(this.Nombre);
            this.PanelNombre.Controls.Add(this.lblNombre);
            this.PanelNombre.Location = new System.Drawing.Point(1, 0);
            this.PanelNombre.Name = "PanelNombre";
            // 
            // 
            // 
            this.PanelNombre.RootElement.Opacity = 0D;
            this.PanelNombre.Size = new System.Drawing.Size(514, 20);
            this.PanelNombre.TabIndex = 376;
            // 
            // PanelRFC
            // 
            this.PanelRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelRFC.Controls.Add(this.RFC);
            this.PanelRFC.Controls.Add(this.lblRFC);
            this.PanelRFC.Location = new System.Drawing.Point(516, 0);
            this.PanelRFC.Name = "PanelRFC";
            // 
            // 
            // 
            this.PanelRFC.RootElement.Opacity = 0D;
            this.PanelRFC.Size = new System.Drawing.Size(137, 20);
            this.PanelRFC.TabIndex = 384;
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Buscar.Enabled = false;
            this.Buscar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.Buscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Buscar.Location = new System.Drawing.Point(674, 0);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(20, 20);
            this.Buscar.TabIndex = 382;
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(89, 65);
            this.IdDirectorio.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.ShowBorder = false;
            this.IdDirectorio.ShowUpDownButtons = false;
            this.IdDirectorio.Size = new System.Drawing.Size(37, 20);
            this.IdDirectorio.TabIndex = 381;
            this.IdDirectorio.TabStop = false;
            // 
            // PanelRegimen
            // 
            this.PanelRegimen.Controls.Add(this.lblRegimenFiscal);
            this.PanelRegimen.Controls.Add(this.RegimenFiscal);
            this.PanelRegimen.Location = new System.Drawing.Point(-1, 0);
            this.PanelRegimen.Name = "PanelRegimen";
            // 
            // 
            // 
            this.PanelRegimen.RootElement.Opacity = 0D;
            this.PanelRegimen.Size = new System.Drawing.Size(276, 21);
            this.PanelRegimen.TabIndex = 385;
            // 
            // PanelUso
            // 
            this.PanelUso.Controls.Add(this.lblUsoCFDI);
            this.PanelUso.Controls.Add(this.UsoCFDI);
            this.PanelUso.Location = new System.Drawing.Point(275, 0);
            this.PanelUso.Name = "PanelUso";
            // 
            // 
            // 
            this.PanelUso.RootElement.Opacity = 0D;
            this.PanelUso.Size = new System.Drawing.Size(242, 21);
            this.PanelUso.TabIndex = 386;
            // 
            // PanelResidencia
            // 
            this.PanelResidencia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelResidencia.Controls.Add(this.ResidenciaFiscal);
            this.PanelResidencia.Controls.Add(this.lblResidenciaFiscal);
            this.PanelResidencia.Location = new System.Drawing.Point(516, 23);
            this.PanelResidencia.Name = "PanelResidencia";
            // 
            // 
            // 
            this.PanelResidencia.RootElement.BorderHighlightThickness = 0;
            this.PanelResidencia.RootElement.Opacity = 0D;
            this.PanelResidencia.Size = new System.Drawing.Size(200, 21);
            this.PanelResidencia.TabIndex = 387;
            // 
            // Panel4
            // 
            this.Panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel4.Controls.Add(this.PanelRegimen);
            this.Panel4.Controls.Add(this.PanelUso);
            this.Panel4.Location = new System.Drawing.Point(2, 23);
            this.Panel4.Name = "Panel4";
            // 
            // 
            // 
            this.Panel4.RootElement.Opacity = 0D;
            this.Panel4.ShowItemToolTips = false;
            this.Panel4.Size = new System.Drawing.Size(514, 21);
            this.Panel4.TabIndex = 388;
            // 
            // ComprobanteContribuyenteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.PanelResidencia);
            this.Controls.Add(this.Actualizar);
            this.Controls.Add(this.Agregar);
            this.Controls.Add(this.PanelNombre);
            this.Controls.Add(this.PanelRFC);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.IdDirectorio);
            this.Controls.Add(this.lblDomicilioFiscal);
            this.Controls.Add(this.lblNumRegTrib);
            this.Controls.Add(this.DomicilioFiscal);
            this.Controls.Add(this.NumRegIdTrib);
            this.Name = "ComprobanteContribuyenteControl";
            this.Size = new System.Drawing.Size(898, 45);
            this.Load += new System.EventHandler(this.ComprobanteContribuyenteControl_Load);
            this.Resize += new System.EventHandler(this.ComprobanteContribuyenteControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegimenFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilioFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumRegTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DomicilioFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelNombre)).EndInit();
            this.PanelNombre.ResumeLayout(false);
            this.PanelNombre.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRFC)).EndInit();
            this.PanelRFC.ResumeLayout(false);
            this.PanelRFC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRegimen)).EndInit();
            this.PanelRegimen.ResumeLayout(false);
            this.PanelRegimen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelUso)).EndInit();
            this.PanelUso.ResumeLayout(false);
            this.PanelUso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelResidencia)).EndInit();
            this.PanelResidencia.ResumeLayout(false);
            this.PanelResidencia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel4)).EndInit();
            this.Panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Telerik.WinControls.UI.RadMultiColumnComboBox RegimenFiscal;
        public Telerik.WinControls.UI.RadLabel lblRegimenFiscal;
        public Telerik.WinControls.UI.RadLabel lblDomicilioFiscal;
        public Telerik.WinControls.UI.RadLabel lblNumRegTrib;
        public Telerik.WinControls.UI.RadLabel lblResidenciaFiscal;
        public Telerik.WinControls.UI.RadLabel lblUsoCFDI;
        public Telerik.WinControls.UI.RadTextBox DomicilioFiscal;
        public Telerik.WinControls.UI.RadMultiColumnComboBox ResidenciaFiscal;
        public Telerik.WinControls.UI.RadMultiColumnComboBox UsoCFDI;
        public Telerik.WinControls.UI.RadTextBox NumRegIdTrib;
        public Telerik.WinControls.UI.RadButton Actualizar;
        public Telerik.WinControls.UI.RadButton Agregar;
        public Telerik.WinControls.UI.RadTextBox RFC;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Nombre;
        public Telerik.WinControls.UI.RadLabel lblNombre;
        public Telerik.WinControls.UI.RadLabel lblRFC;
        protected internal Telerik.WinControls.UI.RadPanel PanelNombre;
        protected internal Telerik.WinControls.UI.RadPanel PanelRFC;
        public Telerik.WinControls.UI.RadButton Buscar;
        public Telerik.WinControls.UI.RadSpinEditor IdDirectorio;
        private Telerik.WinControls.UI.RadPanel PanelRegimen;
        private Telerik.WinControls.UI.RadPanel PanelUso;
        private Telerik.WinControls.UI.RadPanel PanelResidencia;
        private Telerik.WinControls.UI.RadPanel Panel4;
    }
}
