﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.UI.Comprobante.Forms {
    /// <summary>
    /// Comprobantes fiscales por Receptor o Emisor
    /// </summary>
    public partial class ComprobantesFiscalesContribuyenteForm : RadForm {
        //protected internal IComprobantesFiscalesService _Service;
        protected internal BindingList<IComprobanteFiscalDetailSingleModel> _DataSource;
        protected internal BackgroundWorker _ClientesLoad = new BackgroundWorker();

        public ComprobantesFiscalesContribuyenteForm(IComprobantesFiscalesService service, UIMenuElement menuElement) {
            InitializeComponent();
            this.TComprobante._Service = service;
        }

        private void ComprobantesFiscalesContribuyenteForm_Load(object sender, EventArgs e) {
            this.TComprobante.Templete = Builder.ComprobanteFiscalGridBuilder.TempleteEnum.Standar;
            //this.TComprobante._Service = this._Service;
            this.TComprobante.Enabled = false;
            this.TComprobante.ShowPeriodo = false;
            this.TComprobante.ShowNuevo = false;
            this.TComprobante.ShowEditar = false;
            this.TComprobante.ShowCancelar = false;
            this.TComprobante.ShowItem = true;
            this.TComprobante.ItemHost.HostedItem = this.Nombre.MultiColumnComboBoxElement;
            this.TComprobante.GridData.AllowEditRow = true;
            this.TComprobante.ShowAutosuma = true;
            this.TComprobante.ShowFiltro = true;
            this.TComprobante.ItemLbl.Text = "Cliente: ";

            this.Nombre.AutoFilter = true;
            this.Nombre.EditorControl.AllowRowResize = false;
            this.Nombre.EditorControl.AllowSearchRow = true;
            this.Nombre.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            this._ClientesLoad.DoWork += LoadClientes_DoWork;
            this._ClientesLoad.RunWorkerCompleted += LoadClientes_RunWorkerCompleted;
        //    this._ClientesLoad.RunWorkerAsync();

            this.TComprobante.Actualizar.Click += TComprobante_Actualizar_Click;
            this.TComprobante.Cerrar.Click += TComprobante_Cerrar_Click;
        }

        protected void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.ShowDialog(this);
            }
            this.TComprobante.GridData.DataSource = this._DataSource;
        }

        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        /// <summary>
        /// actualizar el listado
        /// </summary>
        protected virtual void Actualizar() {
            if (this.Nombre.SelectedItem != null) {
                var seleccionado = ((GridViewDataRowInfo)this.Nombre.SelectedItem).DataBoundItem as ComprobanteContribuyenteModel;
                if (seleccionado != null) {
                    this._DataSource = new BindingList<IComprobanteFiscalDetailSingleModel>(
                        this.TComprobante._Service.GetList<ComprobanteFiscalDetailSingleModel>(
                           Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().IdDirectorio(seleccionado.IdDirectorio).WithYear(this.TComprobante.GetEjercicio()).Build()).ToList<IComprobanteFiscalDetailSingleModel>()
                        );
                }
            }
        }

        #region metodos privados
        protected virtual void LoadClientes_DoWork(object sender, DoWorkEventArgs e) {
            if (this.TComprobante._Service != null) {
                var d0 = Aplication.Contribuyentes.Services.DirectorioService.Query().WithRelacionComercial(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente).RegistroActivo().Build();
                this.Nombre.DataSource = this.TComprobante._Service.GetList<ComprobanteContribuyenteModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("_drctr_rlcn", "Cliente", Domain.Base.ValueObjects.ConditionalTypeEnum.In) }).ToList();
            }
        }

        protected virtual void LoadClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (this.TComprobante._Service != null) {
                this.TComprobante.Enabled = true;
                this.Nombre.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
                this.Nombre.AutoFilter = true;
            }
        }
        #endregion
    }
}
