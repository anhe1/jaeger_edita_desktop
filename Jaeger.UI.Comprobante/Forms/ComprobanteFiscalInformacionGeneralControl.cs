﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteFiscalInformacionGeneralControl : UserControl {
        protected IMesesCatalogo mesesCatalogo = new MesesCatalogo();
        protected IPeriodicidadCatalogo periodicidadCatalogo = new PeriodicidadCatalogo();
        private bool _Editable = false;

        public ComprobanteFiscalInformacionGeneralControl() {
            InitializeComponent();
        }

        public bool Editable {
            get { return this._Editable; }
            set { this._Editable = value;
                this.SetEditable();
            }
        }

        private void ComprobanteFiscalInformacionGeneralControl_Load(object sender, EventArgs e) {
            
        }

        private void Incluir_CheckStateChanged(object sender, EventArgs e) {
            this.ClavePeriodicidad.Enabled = this.Incluir.Checked;
            this.ClaveMeses.Enabled = this.Incluir.Checked;
            this.Ejercicio.Enabled = this.Incluir.Checked;
        }

        public virtual void Start() {
            this.mesesCatalogo.Load();
            this.periodicidadCatalogo.Load();
            this.Ejercicio.Maximum = DateTime.Now.Year;
            this.Ejercicio.Minimum = 2021;
            this.ClavePeriodicidad.DataSource = this.periodicidadCatalogo.Items;
            this.ClaveMeses.DataSource = this.mesesCatalogo.Items;
            this.ClavePeriodicidad.SelectedIndex = -1;
            this.ClavePeriodicidad.SelectedValue = null;
            this.ClaveMeses.SelectedIndex = -1;
            this.ClaveMeses.SelectedValue = null;
        }

        private void SetEditable() {
            this.Ejercicio.ReadOnly = !this._Editable;
            //this.ClavePeriodicidad.SetEditable(this.Editable);
            //this.ClaveMeses.SetEditable(this.Editable);
        }
    }
}
