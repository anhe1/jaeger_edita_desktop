﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ProdServCatalogoSATBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridProductoSAT = new Telerik.WinControls.UI.RadGridView();
            this.WorkerService = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // gridProductoSAT
            // 
            this.gridProductoSAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProductoSAT.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridProductoSAT.MasterTemplate.AllowAddNewRow = false;
            this.gridProductoSAT.MasterTemplate.AllowDeleteRow = false;
            this.gridProductoSAT.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 450;
            this.gridProductoSAT.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.gridProductoSAT.MasterTemplate.EnableGrouping = false;
            this.gridProductoSAT.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridProductoSAT.Name = "gridProductoSAT";
            this.gridProductoSAT.Size = new System.Drawing.Size(696, 201);
            this.gridProductoSAT.TabIndex = 3;
            this.gridProductoSAT.DoubleClick += new System.EventHandler(this.gridProductoSAT_DoubleClick);
            // 
            // WorkerService
            // 
            this.WorkerService.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerService_DoWork);
            // 
            // ProdServCatalogoBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridProductoSAT);
            this.Name = "ProdServCatalogoBuscarControl";
            this.Size = new System.Drawing.Size(696, 201);
            this.Load += new System.EventHandler(this.ProdServCatalogoBuscarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductoSAT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView gridProductoSAT;
        private System.ComponentModel.BackgroundWorker WorkerService;
    }
}
