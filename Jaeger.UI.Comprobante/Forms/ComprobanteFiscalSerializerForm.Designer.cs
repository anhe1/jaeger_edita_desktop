﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteFiscalSerializerForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteFiscalSerializerForm));
            this.Header = new System.Windows.Forms.PictureBox();
            this.HeaderTitle = new Telerik.WinControls.UI.RadLabel();
            this.Start = new Telerik.WinControls.UI.RadButton();
            this.Close = new Telerik.WinControls.UI.RadButton();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.EjercicioLabel = new System.Windows.Forms.Label();
            this.Periodo = new Telerik.WinControls.UI.RadDropDownList();
            this.PeriodoLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TipoComprobante = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Periodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.White;
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(460, 40);
            this.Header.TabIndex = 0;
            this.Header.TabStop = false;
            // 
            // HeaderTitle
            // 
            this.HeaderTitle.BackColor = System.Drawing.Color.White;
            this.HeaderTitle.Location = new System.Drawing.Point(12, 12);
            this.HeaderTitle.Name = "HeaderTitle";
            this.HeaderTitle.Size = new System.Drawing.Size(254, 18);
            this.HeaderTitle.TabIndex = 1;
            this.HeaderTitle.Text = "Servicio de serialización de comprobantes fiscales";
            // 
            // Start
            // 
            this.Start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Start.Location = new System.Drawing.Point(373, 208);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(75, 23);
            this.Start.TabIndex = 2;
            this.Start.Text = "Iniciar";
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Close
            // 
            this.Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close.Location = new System.Drawing.Point(295, 207);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(72, 24);
            this.Close.TabIndex = 3;
            this.Close.Text = "Cancelar";
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(80, 61);
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(55, 20);
            this.Ejercicio.TabIndex = 4;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EjercicioLabel
            // 
            this.EjercicioLabel.AutoSize = true;
            this.EjercicioLabel.Location = new System.Drawing.Point(17, 65);
            this.EjercicioLabel.Name = "EjercicioLabel";
            this.EjercicioLabel.Size = new System.Drawing.Size(52, 13);
            this.EjercicioLabel.TabIndex = 5;
            this.EjercicioLabel.Text = "Ejercicio:";
            // 
            // Periodo
            // 
            this.Periodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Periodo.Location = new System.Drawing.Point(214, 61);
            this.Periodo.Name = "Periodo";
            this.Periodo.NullText = "Selecciona";
            this.Periodo.Size = new System.Drawing.Size(118, 20);
            this.Periodo.TabIndex = 8;
            // 
            // PeriodoLabel
            // 
            this.PeriodoLabel.AutoSize = true;
            this.PeriodoLabel.Location = new System.Drawing.Point(151, 65);
            this.PeriodoLabel.Name = "PeriodoLabel";
            this.PeriodoLabel.Size = new System.Drawing.Size(47, 13);
            this.PeriodoLabel.TabIndex = 9;
            this.PeriodoLabel.Text = "Periodo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Comprobantes";
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoComprobante.Location = new System.Drawing.Point(110, 87);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.NullText = "Selecciona";
            this.TipoComprobante.Size = new System.Drawing.Size(118, 20);
            this.TipoComprobante.TabIndex = 10;
            // 
            // ComprobanteFiscalSerializerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Close;
            this.ClientSize = new System.Drawing.Size(460, 243);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TipoComprobante);
            this.Controls.Add(this.PeriodoLabel);
            this.Controls.Add(this.Periodo);
            this.Controls.Add(this.EjercicioLabel);
            this.Controls.Add(this.Ejercicio);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.HeaderTitle);
            this.Controls.Add(this.Header);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobanteFiscalSerializerForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Comprobante Serializer";
            this.Load += new System.EventHandler(this.ComprobanteFiscalSerializerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Periodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Header;
        private Telerik.WinControls.UI.RadLabel HeaderTitle;
        private Telerik.WinControls.UI.RadButton Start;
        private Telerik.WinControls.UI.RadButton Close;
        private Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        private System.Windows.Forms.Label EjercicioLabel;
        public Telerik.WinControls.UI.RadDropDownList Periodo;
        private System.Windows.Forms.Label PeriodoLabel;
        private System.Windows.Forms.Label label1;
        public Telerik.WinControls.UI.RadDropDownList TipoComprobante;
    }
}