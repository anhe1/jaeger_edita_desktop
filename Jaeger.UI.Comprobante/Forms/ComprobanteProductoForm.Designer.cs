﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class ComprobanteProductoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            this.buttonProductoServicio = new Telerik.WinControls.UI.RadSplitButton();
            this.buttonProducto = new Telerik.WinControls.UI.RadMenuItem();
            this.buttonServicio = new Telerik.WinControls.UI.RadMenuItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txbMarca = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.txbDescripcion = new Telerik.WinControls.UI.RadTextBox();
            this.labelProducto = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txbNoIdentificacion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.txbUnidad = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.txbCtaPredial = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.txbNumRequerimiento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbClaveProdServ = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbClaveUnidad = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbUnitario = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cboTrasladoIVA = new Telerik.WinControls.UI.RadDropDownList();
            this.txbTrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbTrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.cboTrasladoIEPS = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.txbRetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbRetencionIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txbRetencionIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.chkRentencionISR = new Telerik.WinControls.UI.RadCheckBox();
            this.chkRetnecionIVA = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.cboRetencionIEPS = new Telerik.WinControls.UI.RadDropDownList();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.buttonProductoServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbMarca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNoIdentificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCtaPredial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNumRequerimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbClaveProdServ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbClaveUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbUnitario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRentencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetnecionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRetencionIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonProductoServicio
            // 
            this.buttonProductoServicio.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonProducto,
            this.buttonServicio});
            this.buttonProductoServicio.Location = new System.Drawing.Point(497, 34);
            this.buttonProductoServicio.Name = "buttonProductoServicio";
            this.buttonProductoServicio.Size = new System.Drawing.Size(122, 20);
            this.buttonProductoServicio.TabIndex = 3;
            this.buttonProductoServicio.Text = "Selecciona";
            // 
            // buttonProducto
            // 
            this.buttonProducto.Name = "buttonProducto";
            this.buttonProducto.Text = "Producto";
            this.buttonProducto.UseCompatibleTextRendering = false;
            this.buttonProducto.Click += new System.EventHandler(this.ProductoServicioItem_Click);
            // 
            // buttonServicio
            // 
            this.buttonServicio.Name = "buttonServicio";
            this.buttonServicio.Text = "Servicio";
            this.buttonServicio.UseCompatibleTextRendering = false;
            this.buttonServicio.Click += new System.EventHandler(this.ProductoServicioItem_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(460, 35);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(31, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Tipo:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 130);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(39, 18);
            this.radLabel4.TabIndex = 10;
            this.radLabel4.Text = "Marca:";
            // 
            // txbMarca
            // 
            this.txbMarca.Location = new System.Drawing.Point(57, 129);
            this.txbMarca.Name = "txbMarca";
            this.txbMarca.NullText = "Marca";
            this.txbMarca.ShowClearButton = true;
            this.txbMarca.Size = new System.Drawing.Size(562, 20);
            this.txbMarca.TabIndex = 11;
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(6, 21);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(82, 18);
            this.radLabel18.TabIndex = 12;
            this.radLabel18.Text = "Precio Unitario:";
            // 
            // txbDescripcion
            // 
            this.txbDescripcion.AutoSize = false;
            this.txbDescripcion.Location = new System.Drawing.Point(12, 60);
            this.txbDescripcion.MaxLength = 1000;
            this.txbDescripcion.Multiline = true;
            this.txbDescripcion.Name = "txbDescripcion";
            this.txbDescripcion.NullText = "Descripción";
            this.txbDescripcion.ShowClearButton = true;
            this.txbDescripcion.Size = new System.Drawing.Size(607, 64);
            this.txbDescripcion.TabIndex = 15;
            // 
            // labelProducto
            // 
            this.labelProducto.Location = new System.Drawing.Point(12, 36);
            this.labelProducto.Name = "labelProducto";
            this.labelProducto.Size = new System.Drawing.Size(67, 18);
            this.labelProducto.TabIndex = 14;
            this.labelProducto.Text = "Descripción:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(253, 182);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(107, 18);
            this.radLabel2.TabIndex = 16;
            this.radLabel2.Text = "Clave Producto SAT:";
            // 
            // txbNoIdentificacion
            // 
            this.txbNoIdentificacion.Location = new System.Drawing.Point(510, 181);
            this.txbNoIdentificacion.Name = "txbNoIdentificacion";
            this.txbNoIdentificacion.NullText = "SKU";
            this.txbNoIdentificacion.Size = new System.Drawing.Size(109, 20);
            this.txbNoIdentificacion.TabIndex = 19;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(446, 182);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(58, 18);
            this.radLabel3.TabIndex = 18;
            this.radLabel3.Text = "No. Ident.:";
            // 
            // txbUnidad
            // 
            this.txbUnidad.Location = new System.Drawing.Point(510, 155);
            this.txbUnidad.MaxLength = 20;
            this.txbUnidad.Name = "txbUnidad";
            this.txbUnidad.NullText = "Unidad";
            this.txbUnidad.ShowClearButton = true;
            this.txbUnidad.Size = new System.Drawing.Size(109, 20);
            this.txbUnidad.TabIndex = 25;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(446, 156);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(45, 18);
            this.radLabel5.TabIndex = 24;
            this.radLabel5.Text = "Unidad:";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(253, 156);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(113, 18);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "Clave de Unidad SAT:";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(12, 182);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(109, 18);
            this.radLabel7.TabIndex = 20;
            this.radLabel7.Text = "Núm. de pedimento:";
            // 
            // txbCtaPredial
            // 
            this.txbCtaPredial.Location = new System.Drawing.Point(127, 155);
            this.txbCtaPredial.Name = "txbCtaPredial";
            this.txbCtaPredial.NullText = "Cta. Predial";
            this.txbCtaPredial.ShowClearButton = true;
            this.txbCtaPredial.Size = new System.Drawing.Size(116, 20);
            this.txbCtaPredial.TabIndex = 27;
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(11, 156);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(110, 18);
            this.radLabel8.TabIndex = 26;
            this.radLabel8.Text = "Núm. cuenta predial:";
            // 
            // txbNumRequerimiento
            // 
            this.txbNumRequerimiento.Location = new System.Drawing.Point(127, 181);
            this.txbNumRequerimiento.Mask = "##-##-####-######";
            this.txbNumRequerimiento.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.txbNumRequerimiento.Name = "txbNumRequerimiento";
            this.txbNumRequerimiento.Size = new System.Drawing.Size(116, 20);
            this.txbNumRequerimiento.TabIndex = 28;
            this.txbNumRequerimiento.TabStop = false;
            this.txbNumRequerimiento.Text = "__-__-____-______";
            this.txbNumRequerimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbClaveProdServ
            // 
            this.txbClaveProdServ.Location = new System.Drawing.Point(368, 181);
            this.txbClaveProdServ.Mask = "########";
            this.txbClaveProdServ.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.txbClaveProdServ.Name = "txbClaveProdServ";
            this.txbClaveProdServ.Size = new System.Drawing.Size(72, 20);
            this.txbClaveProdServ.TabIndex = 33;
            this.txbClaveProdServ.TabStop = false;
            this.txbClaveProdServ.Text = "________";
            this.txbClaveProdServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbClaveUnidad
            // 
            this.txbClaveUnidad.Location = new System.Drawing.Point(368, 155);
            this.txbClaveUnidad.Name = "txbClaveUnidad";
            this.txbClaveUnidad.Size = new System.Drawing.Size(72, 20);
            this.txbClaveUnidad.TabIndex = 34;
            this.txbClaveUnidad.TabStop = false;
            this.txbClaveUnidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txbUnitario
            // 
            this.txbUnitario.Location = new System.Drawing.Point(94, 21);
            this.txbUnitario.Mask = "N4";
            this.txbUnitario.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbUnitario.Name = "txbUnitario";
            this.txbUnitario.Size = new System.Drawing.Size(71, 20);
            this.txbUnitario.TabIndex = 35;
            this.txbUnitario.TabStop = false;
            this.txbUnitario.Text = "0.0000";
            this.txbUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.cboTrasladoIVA);
            this.radGroupBox1.Controls.Add(this.txbTrasladoIEPS);
            this.radGroupBox1.Controls.Add(this.txbTrasladoIVA);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.cboTrasladoIEPS);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.HeaderText = "Impuestos Traslado";
            this.radGroupBox1.Location = new System.Drawing.Point(190, 207);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(212, 105);
            this.radGroupBox1.TabIndex = 36;
            this.radGroupBox1.Text = "Impuestos Traslado";
            // 
            // cboTrasladoIVA
            // 
            radListDataItem1.Text = "N/A";
            radListDataItem2.Text = "Tasa";
            radListDataItem3.Text = "Exento";
            this.cboTrasladoIVA.Items.Add(radListDataItem1);
            this.cboTrasladoIVA.Items.Add(radListDataItem2);
            this.cboTrasladoIVA.Items.Add(radListDataItem3);
            this.cboTrasladoIVA.Location = new System.Drawing.Point(37, 21);
            this.cboTrasladoIVA.Name = "cboTrasladoIVA";
            this.cboTrasladoIVA.Size = new System.Drawing.Size(66, 20);
            this.cboTrasladoIVA.TabIndex = 37;
            this.cboTrasladoIVA.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cboTrasladoIVA_SelectedIndexChanged);
            // 
            // txbTrasladoIEPS
            // 
            this.txbTrasladoIEPS.Location = new System.Drawing.Point(109, 47);
            this.txbTrasladoIEPS.Mask = "n6";
            this.txbTrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbTrasladoIEPS.Name = "txbTrasladoIEPS";
            this.txbTrasladoIEPS.Size = new System.Drawing.Size(68, 20);
            this.txbTrasladoIEPS.TabIndex = 45;
            this.txbTrasladoIEPS.TabStop = false;
            this.txbTrasladoIEPS.Text = "0.000000";
            this.txbTrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txbTrasladoIEPS.Validated += new System.EventHandler(this.txbTrasladoIEPS_Validated);
            // 
            // txbTrasladoIVA
            // 
            this.txbTrasladoIVA.Location = new System.Drawing.Point(109, 20);
            this.txbTrasladoIVA.Mask = "n6";
            this.txbTrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbTrasladoIVA.Name = "txbTrasladoIVA";
            this.txbTrasladoIVA.Size = new System.Drawing.Size(68, 20);
            this.txbTrasladoIVA.TabIndex = 44;
            this.txbTrasladoIVA.TabStop = false;
            this.txbTrasladoIVA.Text = "0.000000";
            this.txbTrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txbTrasladoIVA.Validated += new System.EventHandler(this.txbTrasladoIVA_Validated);
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(4, 47);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(27, 18);
            this.radLabel12.TabIndex = 35;
            this.radLabel12.Text = "IEPS";
            // 
            // cboTrasladoIEPS
            // 
            radListDataItem4.Text = "N/A";
            radListDataItem5.Text = "Tasa";
            radListDataItem6.Text = "Cuota";
            this.cboTrasladoIEPS.Items.Add(radListDataItem4);
            this.cboTrasladoIEPS.Items.Add(radListDataItem5);
            this.cboTrasladoIEPS.Items.Add(radListDataItem6);
            this.cboTrasladoIEPS.Location = new System.Drawing.Point(37, 47);
            this.cboTrasladoIEPS.Name = "cboTrasladoIEPS";
            this.cboTrasladoIEPS.Size = new System.Drawing.Size(66, 20);
            this.cboTrasladoIEPS.TabIndex = 34;
            this.cboTrasladoIEPS.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cboTrasladoIEPS_SelectedIndexChanged);
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(6, 22);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(24, 18);
            this.radLabel11.TabIndex = 33;
            this.radLabel11.Text = "IVA";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.txbRetencionISR);
            this.radGroupBox2.Controls.Add(this.txbRetencionIVA);
            this.radGroupBox2.Controls.Add(this.txbRetencionIEPS);
            this.radGroupBox2.Controls.Add(this.chkRentencionISR);
            this.radGroupBox2.Controls.Add(this.chkRetnecionIVA);
            this.radGroupBox2.Controls.Add(this.radLabel15);
            this.radGroupBox2.Controls.Add(this.cboRetencionIEPS);
            this.radGroupBox2.HeaderText = "Impuestos Retenidos";
            this.radGroupBox2.Location = new System.Drawing.Point(406, 207);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(212, 105);
            this.radGroupBox2.TabIndex = 37;
            this.radGroupBox2.Text = "Impuestos Retenidos";
            // 
            // txbRetencionISR
            // 
            this.txbRetencionISR.Location = new System.Drawing.Point(48, 47);
            this.txbRetencionISR.Mask = "n6";
            this.txbRetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionISR.Name = "txbRetencionISR";
            this.txbRetencionISR.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionISR.TabIndex = 47;
            this.txbRetencionISR.TabStop = false;
            this.txbRetencionISR.Text = "0.000000";
            this.txbRetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txbRetencionISR.Validated += new System.EventHandler(this.txbRetencionISR_Validated);
            // 
            // txbRetencionIVA
            // 
            this.txbRetencionIVA.Location = new System.Drawing.Point(48, 21);
            this.txbRetencionIVA.Mask = "n6";
            this.txbRetencionIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionIVA.Name = "txbRetencionIVA";
            this.txbRetencionIVA.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionIVA.TabIndex = 46;
            this.txbRetencionIVA.TabStop = false;
            this.txbRetencionIVA.Text = "0.000000";
            this.txbRetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txbRetencionIVA.Validated += new System.EventHandler(this.txbRetencionIVA_Validated);
            // 
            // txbRetencionIEPS
            // 
            this.txbRetencionIEPS.Location = new System.Drawing.Point(122, 70);
            this.txbRetencionIEPS.Mask = "n6";
            this.txbRetencionIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txbRetencionIEPS.Name = "txbRetencionIEPS";
            this.txbRetencionIEPS.Size = new System.Drawing.Size(68, 20);
            this.txbRetencionIEPS.TabIndex = 43;
            this.txbRetencionIEPS.TabStop = false;
            this.txbRetencionIEPS.Text = "0.000000";
            this.txbRetencionIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txbRetencionIEPS.Validated += new System.EventHandler(this.txbRetencionIEPS_Validated);
            // 
            // chkRentencionISR
            // 
            this.chkRentencionISR.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRentencionISR.Location = new System.Drawing.Point(7, 46);
            this.chkRentencionISR.Name = "chkRentencionISR";
            this.chkRentencionISR.Size = new System.Drawing.Size(36, 18);
            this.chkRentencionISR.TabIndex = 42;
            this.chkRentencionISR.Text = "ISR";
            this.chkRentencionISR.CheckStateChanged += new System.EventHandler(this.chkRentencionISR_CheckStateChanged);
            // 
            // chkRetnecionIVA
            // 
            this.chkRetnecionIVA.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRetnecionIVA.Location = new System.Drawing.Point(7, 21);
            this.chkRetnecionIVA.Name = "chkRetnecionIVA";
            this.chkRetnecionIVA.Size = new System.Drawing.Size(38, 18);
            this.chkRetnecionIVA.TabIndex = 41;
            this.chkRetnecionIVA.Text = "IVA";
            this.chkRetnecionIVA.CheckStateChanged += new System.EventHandler(this.chkRetnecionIVA_CheckStateChanged);
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(7, 72);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(27, 18);
            this.radLabel15.TabIndex = 40;
            this.radLabel15.Text = "IEPS";
            // 
            // cboRetencionIEPS
            // 
            radListDataItem7.Text = "N/A";
            radListDataItem8.Text = "Tasa";
            radListDataItem9.Text = "Cuota";
            this.cboRetencionIEPS.Items.Add(radListDataItem7);
            this.cboRetencionIEPS.Items.Add(radListDataItem8);
            this.cboRetencionIEPS.Items.Add(radListDataItem9);
            this.cboRetencionIEPS.Location = new System.Drawing.Point(48, 71);
            this.cboRetencionIEPS.Name = "cboRetencionIEPS";
            this.cboRetencionIEPS.Size = new System.Drawing.Size(68, 20);
            this.cboRetencionIEPS.TabIndex = 39;
            this.cboRetencionIEPS.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cboRetencionIEPS_SelectedIndexChanged);
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.txbUnitario);
            this.radGroupBox3.Controls.Add(this.radLabel18);
            this.radGroupBox3.HeaderText = "Precio";
            this.radGroupBox3.Location = new System.Drawing.Point(11, 207);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(173, 105);
            this.radGroupBox3.TabIndex = 38;
            this.radGroupBox3.Text = "Precio";
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(630, 30);
            this.ToolBar.TabIndex = 39;
            this.ToolBar.Nuevo.Click += this.ToolBarButtonNuevo_Click;
            this.ToolBar.Guardar.Click += this.ToolBarButtonGuardar_Click;
            this.ToolBar.Actualizar.Click += this.ToolBarButtonActualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBarButtonCerrar_Click;
            // 
            // ComprobanteProductoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 329);
            this.Controls.Add(this.ToolBar);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.txbClaveUnidad);
            this.Controls.Add(this.txbClaveProdServ);
            this.Controls.Add(this.txbNumRequerimiento);
            this.Controls.Add(this.txbCtaPredial);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.txbUnidad);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.txbNoIdentificacion);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.txbDescripcion);
            this.Controls.Add(this.labelProducto);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.txbMarca);
            this.Controls.Add(this.buttonProductoServicio);
            this.Controls.Add(this.radLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobanteProductoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Producto ó Servicio";
            this.Load += new System.EventHandler(this.ViewComprobanteProducto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.buttonProductoServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbMarca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNoIdentificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbCtaPredial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNumRequerimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbClaveProdServ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbClaveUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbUnitario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbTrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbRetencionIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRentencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRetnecionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRetencionIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitButton buttonProductoServicio;
        private Telerik.WinControls.UI.RadMenuItem buttonProducto;
        private Telerik.WinControls.UI.RadMenuItem buttonServicio;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txbMarca;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadTextBox txbDescripcion;
        private Telerik.WinControls.UI.RadLabel labelProducto;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txbNoIdentificacion;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox txbUnidad;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox txbCtaPredial;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadMaskedEditBox txbNumRequerimiento;
        private Telerik.WinControls.UI.RadMaskedEditBox txbClaveProdServ;
        private Telerik.WinControls.UI.RadMaskedEditBox txbClaveUnidad;
        private Telerik.WinControls.UI.RadMaskedEditBox txbUnitario;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadDropDownList cboTrasladoIEPS;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadDropDownList cboRetencionIEPS;
        private Telerik.WinControls.UI.RadDropDownList cboTrasladoIVA;
        private Telerik.WinControls.UI.RadCheckBox chkRentencionISR;
        private Telerik.WinControls.UI.RadCheckBox chkRetnecionIVA;
        private Telerik.WinControls.UI.RadMaskedEditBox txbRetencionIEPS;
        private Telerik.WinControls.UI.RadMaskedEditBox txbTrasladoIEPS;
        private Telerik.WinControls.UI.RadMaskedEditBox txbTrasladoIVA;
        private Telerik.WinControls.UI.RadMaskedEditBox txbRetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox txbRetencionIVA;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}
