﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Domain.Certificado.Entities;

namespace Jaeger.UI.Comprobante.Forms {
    public class CertificadosForm : Certificado.Forms.CSD.CatalogoForm {
        #region declaraciones
        protected internal ICertificadoService _Service;
        protected List<CertificadoModel> _DataSource;
        protected Domain.Base.Abstractions.UIMenuElement MenuElement;
        #endregion

        public CertificadosForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.MenuElement = menuElement;
            this.Load += CertificadosForm_Load;
        }

        private void CertificadosForm_Load(object sender, EventArgs e) {
            this.TCertificado.Permisos = this.MenuElement.Action;
            this.Actualizar_Click(sender, e);
        }

        protected override void Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new CertificadoForm(this._Service)) {
                nuevo.Text = "Nuevo CSD";
                nuevo.ShowDialog(this);
            }
        }

        protected override void Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.TCertificado.GridData.ReturnRowSelected<CertificadoModel>();
            if (seleccionado != null) {
                if (Telerik.WinControls.RadMessageBox.Show(this, $"¿Esta seguro de eliminar el certificado con número de serie {seleccionado.Serie}?", "Atención", System.Windows.Forms.MessageBoxButtons.YesNo, Telerik.WinControls.RadMessageIcon.Question) == System.Windows.Forms.DialogResult.Yes) {

                }
            }
        }

        protected override void Actualizar_Click(object sender, EventArgs e) {
            base.Actualizar_Click(sender, e);
            this.TCertificado.GridData.DataSource = this._DataSource;
        }

        protected override void Consulta() {
            var query = Aplication.Comprobante.Services.CertificadoService.Query().OnlyActive();
            this._DataSource = this._Service.GetList<CertificadoModel>(query.Build()).ToList<CertificadoModel>();
        }
    }
}
