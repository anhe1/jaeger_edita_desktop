﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobantesFiscalesPartidaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TComprobante = new Jaeger.UI.Common.Forms.GridCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.PDF = null;
            this.TComprobante.ShowActualizar = true;
            this.TComprobante.ShowAutosuma = false;
            this.TComprobante.ShowCancelar = false;
            this.TComprobante.ShowCerrar = true;
            this.TComprobante.ShowEditar = false;
            this.TComprobante.ShowEjercicio = true;
            this.TComprobante.ShowExportarExcel = false;
            this.TComprobante.ShowFiltro = true;
            this.TComprobante.ShowHerramientas = false;
            this.TComprobante.ShowImprimir = false;
            this.TComprobante.ShowItem = false;
            this.TComprobante.ShowNuevo = false;
            this.TComprobante.ShowPeriodo = true;
            this.TComprobante.ShowSeleccionMultiple = true;
            this.TComprobante.Size = new System.Drawing.Size(800, 450);
            this.TComprobante.TabIndex = 0;
            // 
            // ComprobantesFiscalesPartidaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TComprobante);
            this.Name = "ComprobantesFiscalesPartidaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ComprobantesFiscalesPartidaForm";
            this.Load += new System.EventHandler(this.ComprobantesFiscalesPartidaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.GridCommonControl TComprobante;
    }
}