﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobanteBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridSearchResult = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.WorkerBuscar = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult.MasterTemplate)).BeginInit();
            this.gridSearchResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            this.SuspendLayout();
            // 
            // gridSearchResult
            // 
            this.gridSearchResult.Controls.Add(this.Espera);
            this.gridSearchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSearchResult.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridSearchResult.MasterTemplate.AllowAddNewRow = false;
            this.gridSearchResult.MasterTemplate.AllowDeleteRow = false;
            this.gridSearchResult.MasterTemplate.AllowDragToGroup = false;
            this.gridSearchResult.MasterTemplate.AllowEditRow = false;
            this.gridSearchResult.MasterTemplate.AllowRowResize = false;
            this.gridSearchResult.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "TipoComprobante";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "TipoComprobante";
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.Width = 65;
            gridViewTextBoxColumn5.FieldName = "Status";
            gridViewTextBoxColumn5.HeaderText = "Status";
            gridViewTextBoxColumn5.Name = "Status";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn6.HeaderText = "Receptor";
            gridViewTextBoxColumn6.Name = "Receptor";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.Name = "ReceptorRFC";
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.FieldName = "IdDocumento";
            gridViewTextBoxColumn8.HeaderText = "IdDocumento";
            gridViewTextBoxColumn8.Name = "IdDocumento";
            gridViewTextBoxColumn8.Width = 190;
            gridViewTextBoxColumn9.FieldName = "FechaEmision";
            gridViewTextBoxColumn9.FormatString = "{0:d}";
            gridViewTextBoxColumn9.HeaderText = "Fecha Emision";
            gridViewTextBoxColumn9.Name = "FechaEmision";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Total";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Total";
            gridViewTextBoxColumn10.Name = "Total";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            this.gridSearchResult.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.gridSearchResult.MasterTemplate.EnableFiltering = true;
            this.gridSearchResult.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridSearchResult.Name = "gridSearchResult";
            this.gridSearchResult.ShowGroupPanel = false;
            this.gridSearchResult.Size = new System.Drawing.Size(677, 412);
            this.gridSearchResult.TabIndex = 3;
            // 
            // Espera
            // 
            this.Espera.Location = new System.Drawing.Point(309, 171);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // WorkerBuscar
            // 
            this.WorkerBuscar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerBuscar_DoWork);
            this.WorkerBuscar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerBuscar_RunWorkerCompleted);
            // 
            // ComprobanteBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridSearchResult);
            this.Name = "ComprobanteBuscarControl";
            this.Size = new System.Drawing.Size(677, 412);
            this.Load += new System.EventHandler(this.ComprobanteBuscarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult)).EndInit();
            this.gridSearchResult.ResumeLayout(false);
            this.gridSearchResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView gridSearchResult;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private System.ComponentModel.BackgroundWorker WorkerBuscar;
    }
}
