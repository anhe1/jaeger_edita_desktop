﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Comprobante;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Catalogos.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteFiscalControl : UserControl {
        protected internal ComprobanteFiscalDetailModel _Comprobante;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public ComprobanteFiscalControl() {
            InitializeComponent();
        }
        #region propiedades
        public ComprobanteFiscalDetailModel Comprobante {
            get { return this._Comprobante; }
            set { this._Comprobante = value; }
        }

        public IComprobanteFiscalService Service;

        public CFDISubTipoEnum SubtTipo;

        public CFDITipoComprobanteEnum TipoComprobante;

        public int Indice;
        #endregion

        public virtual void Start() {
            if (this.Indice <= 0) {
                //this.Comprobante = null;
            } else {
                this.Comprobante = new ComprobanteFiscalDetailModel { Id = this.Indice };
            }

            this.Service = new ComprobantesFiscalesService(this.SubtTipo);
            this.OnLoad_DoWork();
        }

        private void ComprobanteFiscalControl_Load(object sender, EventArgs e) {
            this.TComprobante.IdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.TComprobante.IdDocumento.TextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Center;
            this.InformacionGeneral.Incluir.CheckStateChanged += Incluir_InformacionGeneral_CheckStateChanged;
            this.CFDIRelacionado.Incluir.CheckStateChanged += this.Incluir_CheckStateChanged;
            this.TComprobante.Actualizar.Click += this.TComprobante_Actualizar_Click;
            this.TComprobante.Nuevo.Click += this.TComprobante_Nuevo_Click;
            this.TComprobante.Duplicar.Click += this.TComprobante_Duplicar_Click;
            this.TComprobante.Guardar.Click += this.TComprobante_Guardar_Click;
            this.TComprobante.Certificar.Click += this.TComprobante_Certificar_Click;
            this.TComprobante.UrlFilePDF.Click += this.TComprobante_PDF_Click;
            this.TComprobante.UrlFileXML.Click += this.TComprobante_XML_Click;
        }

        #region barra de herramientas
        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            this.Text = Properties.Resources.Message_ComprobanteNew;
            if (this.Comprobante == null) {
                if (this.Indice == -3) {
                    this.Comprobante = ComprobanteFiscalService.Builder().Templete().Global().Build() as ComprobanteFiscalDetailModel;// this.Service.GetNewCFDIGlobal();
                } else {
                    this.Comprobante = ComprobanteFiscalService.Builder().New(this.TipoComprobante).Build() as ComprobanteFiscalDetailModel;// this.Service.GetNew(this.TipoComprobante);
                }
            } else if (this.Comprobante.Id > 0) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.Text = Properties.Resources.Message_ComprobanteGet;
                    espera.ShowDialog(this);
                    if (!string.IsNullOrEmpty(this.Comprobante.Serie) | string.IsNullOrEmpty(this.Comprobante.Folio)) {
                        this.Text = string.Concat(this.Comprobante.Serie, " ", this.Comprobante.Folio);
                    } else if (!string.IsNullOrEmpty(this.Comprobante.IdDocumento)) {
                        this.Text = this.Comprobante.IdDocumento;
                    }
                }
            }

            if (this.Comprobante.IsEditable) {
                this.General.LoadContribuyente(this.SubtTipo);
            }
            this.CreateBinding();

        }

        public virtual void TComprobante_Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.TComprobante.Actualizar.PerformClick();
        }

        public virtual void TComprobante_Duplicar_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, Properties.Resources.msg_NoImplementado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
        }

        protected internal void TComprobante_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, Properties.Resources.Message_Comprobante_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (this.Comprobante.CfdiRelacionados != null) {
                if (this.Comprobante.CfdiRelacionados.CfdiRelacionado == null) {
                    this.Comprobante.CfdiRelacionados = null;
                } else if (this.Comprobante.CfdiRelacionados.CfdiRelacionado.Count <= 0) {
                    this.Comprobante.CfdiRelacionados = null;
                }
            }

            using (var espera = new Waiting1Form(this.SaveComprobante)) {
                espera.Text = Properties.Resources.Message_ComprobanteSave;
                espera.ShowDialog(this);
            }
        }

        protected virtual void TComprobante_Certificar_Click(object sender, EventArgs e) {
            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Modo_Prueba_EsCorrecto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                    return;
                }
            }

            using (var espera = new Waiting1Form(this.OnCertificar)) {
                espera.Text = "Certificando, espere un momento ...";
                espera.ShowDialog(this);
            }
            if (this.Tag != null) {
                RadMessageBox.Show(this, (string)this.Tag, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            this.TComprobante.Actualizar.PerformClick();
            this.Tag = null;
        }

        public virtual void TComprobante_Cancelar_Click(object sender, EventArgs e) {
            if (ValidacionService.UUID(this.Comprobante.IdDocumento)) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Comprobante_Cancelar, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {


                }
            }
        }

        public virtual void TComprobante_PDF_Click(object sender, EventArgs e) {
            if (ValidacionService.URL(this.Comprobante.UrlFilePDF)) {
                var saveDialog = new SaveFileDialog { AddExtension = true, DefaultExt = "pdf", FileName = System.IO.Path.GetFileName(this.Comprobante.UrlFilePDF) };
                if (saveDialog.ShowDialog(this) == DialogResult.OK) {
                    if (Util.Downloads.File(this.Comprobante.UrlFilePDF, saveDialog.FileName) == false) {
                        RadMessageBox.Show(this, Properties.Resources.Message_Descarga_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    } else {
                        RadMessageBox.Show(this, string.Format(Properties.Resources.Message_Descarga_Success, saveDialog.FileName), "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                    }
                }
            }
        }

        public virtual void TComprobante_XML_Click(object sender, EventArgs e) {
            if (ValidacionService.URL(this.Comprobante.UrlFileXML)) {
                var saveDialog = new SaveFileDialog { AddExtension = true, DefaultExt = "xml", FileName = System.IO.Path.GetFileName(this.Comprobante.UrlFileXML) };
                if (saveDialog.ShowDialog(this) == DialogResult.OK) {
                    if (Util.Downloads.File(this.Comprobante.UrlFileXML, saveDialog.FileName) == false) {
                        RadMessageBox.Show(this, Properties.Resources.Message_Descarga_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    } else {
                        RadMessageBox.Show(this, string.Format(Properties.Resources.Message_Descarga_Success, saveDialog.FileName), "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                    }
                }
            }
        }

        public virtual void TCFDIRelacionado_Incluir(object sender, EventArgs e) {
            if (this.Comprobante.CfdiRelacionados == null) {
                this.Comprobante.CfdiRelacionados = new ComprobanteCfdiRelacionadosModel();
                this.CFDIRelacionado.TipoRelacion.DataBindings.Clear();
                this.CFDIRelacionado.TipoRelacion.DataBindings.Add("SelectedValue", this.Comprobante.CfdiRelacionados.TipoRelacion, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
                this.CFDIRelacionado.Comprobantes.DataSource = this.Comprobante.CfdiRelacionados.CfdiRelacionado;
            }
            this.CFDIRelacionado.TipoRelacion.SelectedIndexChanged += TipoRelacion_SelectedIndexChanged;
        }

        private void TipoRelacion_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {
            RadListDataItem temporal = this.CFDIRelacionado.TipoRelacion.SelectedItem;
            if (!(temporal == null)) {
                var seleccionado = temporal.DataBoundItem as ClaveTipoRelacionCFDI;
                if (seleccionado != null) {
                    if (this.Comprobante.CfdiRelacionados == null)
                        this.Comprobante.CfdiRelacionados = new ComprobanteCfdiRelacionadosModel();

                    this.Comprobante.CfdiRelacionados.TipoRelacion.Clave = seleccionado.Clave;
                    this.Comprobante.CfdiRelacionados.TipoRelacion.Descripcion = seleccionado.Descripcion;
                }
            }
        }
        #endregion

        #region metodos privados
        private void OnLoad_DoWork() {
            this.General.Start();
            this.InformacionGeneral.Start();
            this.TComprobante.Status.DataSource = ComprobanteFiscalService.GetStatus(this.SubtTipo);

            if (this.SubtTipo == CFDISubTipoEnum.Recibido) {
                this.TComprobante.ToolBarComprobanteFiscal.Items.Remove(this.TComprobante.Nuevo);
                this.TComprobante.ToolBarComprobanteFiscal.Items.Remove(this.TComprobante.Duplicar);
                this.TComprobante.ToolBarComprobanteFiscal.Items.Remove(this.TComprobante.Certificar);
                this.TComprobante.ToolBarComprobanteFiscal.Items.Remove(this.TComprobante.Cancelar);
                this.TComprobante.ToolBarComprobanteFiscal.Items.Remove(this.TComprobante.SendEmail);
                this.TComprobante.ToolBarComprobanteFiscal.Items.Remove(this.TComprobante.Complemento);
            }

            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false) {
                this.TComprobante.Emisor.ToolTipText = Properties.Resources.Message_Modo_Prueba;
                this.TComprobante.Emisor.Image = Properties.Resources.more_info_16px;
            }
            this.TComprobante.Emisor.Text = ConfigService.Synapsis.Empresa.RFC;
        }

        private void CreateBinding() {
            this.TComprobante.Status.DataBindings.Clear();
            this.TComprobante.Status.DataBindings.Add("Text", this.Comprobante, "Status", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CFDIRelacionado.ReceptorRFC.DataBindings.Clear();

            this.General.lblVersion.DataBindings.Clear();
            this.General.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TComprobante.IdDocumento.DataBindings.Clear();
            this.TComprobante.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Documento.DataBindings.Clear();
            this.General.Documento.DataBindings.Add("SelectedValue", this.Comprobante, "Documento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.Documento.SetEditable(this.Comprobante.IsEditable);

            this.General.Folio.DataBindings.Clear();
            this.General.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Serie.DataBindings.Clear();
            this.General.Serie.SetEditable(this.Comprobante.IsEditable);
            this.General.Serie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Receptor.IdDirectorio.DataBindings.Clear();
            this.General.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.Receptor.Nombre.DataBindings.Clear();
            this.General.Receptor.RFC.DataBindings.Clear();
            if (this.Comprobante.SubTipo == CFDISubTipoEnum.Emitido) {
                this.General.Receptor.lblNombre.Text = "Receptor";
                this.General.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);
                this.General.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
                this.CFDIRelacionado.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
            } else {
                this.General.Receptor.lblNombre.Text = "Emisor";
                this.General.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "EmisorNombre", true, DataSourceUpdateMode.OnPropertyChanged);
                this.General.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "EmisorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
            }

            this.General.Receptor.ResidenciaFiscal.DataBindings.Clear();
            this.General.Receptor.ResidenciaFiscal.DataBindings.Add("Text", this.Comprobante, "ResidenciaFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.NumRegIdTrib.DataBindings.Clear();
            this.General.Receptor.NumRegIdTrib.DataBindings.Add("Text", this.Comprobante, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.DomicilioFiscal.DataBindings.Clear();
            this.General.Receptor.DomicilioFiscal.DataBindings.Add("Text", this.Comprobante, "DomicilioFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.RegimenFiscal.DataBindings.Clear();
            this.General.Receptor.RegimenFiscal.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveRegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.UsoCFDI.DataBindings.Clear();
            this.General.Receptor.UsoCFDI.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveUsoCFDI", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Exportacion.DataBindings.Clear();
            this.General.Exportacion.SetEditable(this.Comprobante.IsEditable);
            this.General.Exportacion.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveExportacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.TipoCambio.DataBindings.Clear();
            this.General.TipoCambio.SetEditable(this.Comprobante.IsEditable);
            this.General.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Decimales.DataBindings.Clear();
            this.General.Decimales.SetEditable(this.Comprobante.IsEditable);
            this.General.Decimales.DataBindings.Add("Value", this.Comprobante, "PrecisionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.TipoComprobante.DataBindings.Clear();
            this.General.TipoComprobante.SetEditable(this.Comprobante.IsEditable);
            this.General.TipoComprobante.DataBindings.Add("Text", this.Comprobante, "TipoComprobanteText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.MetodoPago.DataBindings.Clear();
            this.General.MetodoPago.SetEditable(this.Comprobante.IsEditable);
            this.General.MetodoPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FormaPago.DataBindings.Clear();
            this.General.FormaPago.SetEditable(this.Comprobante.IsEditable);
            this.General.FormaPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Condiciones.DataBindings.Clear();
            this.General.Condiciones.SetEditable(this.Comprobante.IsEditable);
            this.General.Condiciones.DataBindings.Add("Text", this.Comprobante, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Moneda.DataBindings.Clear();
            this.General.Moneda.SetEditable(this.Comprobante.IsEditable);
            this.General.Moneda.DataBindings.Add("Text", this.Comprobante, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaEmision.DataBindings.Clear();
            this.General.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.General.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaCertifica.DataBindings.Clear();
            this.General.FechaCertifica.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            this.General.FechaCertifica.DateTimePickerElement.ReadOnly = true;
            this.General.FechaCertifica.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.General.FechaCertifica.DataBindings.Add("Value", this.Comprobante, "FechaTimbre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.FechaCertifica.SetEditable(this.Comprobante.IsEditable);

            this.General.LugarExpedicion.DataBindings.Clear();
            this.General.LugarExpedicion.ReadOnly = true;
            this.General.LugarExpedicion.DataBindings.Add("Text", this.Comprobante, "LugarExpedicion", true, DataSourceUpdateMode.OnPropertyChanged);


            this.BindingInfoGeneral();

            this.CFDIRelacionado.Editable = this.Comprobante.IsEditable;

            this.BindingCFDIRelacionado();

            this.General.TipoComprobante.EndUpdate();
            this.InformacionGeneral.Editable = this.Comprobante.IsEditable;

            // esta timbrado?
            if (this.Comprobante.IdDocumento == null) {
                if (CertificacionService.ValidaFechaEmisionMenor72H(this.Comprobante.FechaEmision, 1) == false) {
                    RadMessageBox.Show(this, Properties.Resources.msg_Fecha_Emision_72hrs, "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    this.General.FechaEmision.MinDate = DateTime.Now.AddDays(-1);
                    this.Comprobante.FechaEmision = DateTime.Now;
                }
            }

            this.Inabilitar();
            this.OnBindingClompleted(null);
        }

        private void Incluir_InformacionGeneral_CheckStateChanged(object sender, EventArgs e) {
            if (this.Comprobante.IsEditable) {
                if (this.Comprobante.InformacionGlobal == null && this.InformacionGeneral.Incluir.Checked) {
                    this.Comprobante.InformacionGlobal = new ComprobanteInformacionGlobalDetailModel() { Anio = DateTime.Now.Year };
                    this.BindingInfoGeneral();
                } else {
                    if (this.Comprobante.InformacionGlobal != null && this.InformacionGeneral.Incluir.Checked == false) {
                        this.Comprobante.InformacionGlobal = null;
                    }
                }
            }
        }

        private void BindingInfoGeneral() {
            if (this.Comprobante.InformacionGlobal != null) {
                this.InformacionGeneral.Incluir.Checked = true;
                this.InformacionGeneral.ClavePeriodicidad.DataBindings.Clear();
                this.InformacionGeneral.ClavePeriodicidad.DataBindings.Add("SelectedValue", this.Comprobante.InformacionGlobal, "Periodicidad", true, DataSourceUpdateMode.OnPropertyChanged);
                this.InformacionGeneral.ClaveMeses.DataBindings.Clear();
                this.InformacionGeneral.ClaveMeses.DataBindings.Add("SelectedValue", this.Comprobante.InformacionGlobal, "ClaveMeses", true, DataSourceUpdateMode.OnPropertyChanged);
                this.InformacionGeneral.Ejercicio.DataBindings.Clear();
                this.InformacionGeneral.Ejercicio.DataBindings.Add("Value", this.Comprobante.InformacionGlobal, "Anio", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void Incluir_CheckStateChanged(object sender, EventArgs e) {
            if (this.Comprobante.IsEditable) {
                if (this.Comprobante.CfdiRelacionados == null && this.CFDIRelacionado.Incluir.Checked) {
                    this.Comprobante.CfdiRelacionados = new ComprobanteCfdiRelacionadosModel();
                } else {
                    if (this.Comprobante.CfdiRelacionados != null && this.CFDIRelacionado.Incluir.Checked == false) {
                        this.Comprobante.CfdiRelacionados = null;
                    }
                }
            }
        }

        private void BindingCFDIRelacionado() {
            if (this.Comprobante.CfdiRelacionados != null) {
                this.CFDIRelacionado.Incluir.Checked = true;
                this.CFDIRelacionado.TipoRelacion.DataBindings.Clear();
                this.CFDIRelacionado.TipoRelacion.DataBindings.Add("SelectedValue", this.Comprobante.CfdiRelacionados.TipoRelacion, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
                this.CFDIRelacionado.Comprobantes.DataSource = this.Comprobante.CfdiRelacionados.CfdiRelacionado;
            }
        }

        private void Inabilitar() {
            this.TComprobante.UrlFileXML.Enabled = ValidacionService.URL(this.Comprobante.UrlFileXML);
            this.TComprobante.UrlFilePDF.Enabled = ValidacionService.URL(this.Comprobante.UrlFilePDF);
            this.TComprobante.Certificar.Enabled = this.Comprobante.IsEditable;
            this.TComprobante.Guardar.Enabled = this.Comprobante.IsEditable;

            this.General.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Serie.ReadOnly = !this.Comprobante.IsEditable;
            this.InformacionGeneral.Incluir.Enabled = this.Comprobante.IsEditable;

            if (this.Comprobante.IsEditable == false) {
                this.TComprobante.Certificar.Visibility = ElementVisibility.Collapsed;
            } else {
                this.TComprobante.Certificar.Visibility = ElementVisibility.Visible;
            }

            // si es un comprobante de pago
            if (this.Comprobante.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                this.General.MetodoPago.Enabled = false;
                this.General.FormaPago.Enabled = false;
                this.General.Condiciones.Enabled = false;
                this.General.TipoComprobante.Enabled = false;
                this.General.Moneda.Enabled = false;
                this.General.TipoCambio.Enabled = false;
                this.General.Receptor.UsoCFDI.Enabled = false;
            }
        }

        private void GetComprobante() {
            this.Comprobante = this.Service.GetComprobante(this.Comprobante.Id);
        }

        private void SaveComprobante() {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        private void OnCertificar() {
            this.Tag = null;
            int codigo = 0;
            string mensaje = string.Empty;
            this.Comprobante = this.Service.Procesar(this.Comprobante, ref codigo, ref mensaje);
            if (codigo > 0) {
                this.Tag = mensaje;
            }
        }

        private bool Validar() {
            this.errorProvider1.Clear();

            if (this.Comprobante.Serie == null || this.Comprobante.Serie.Trim() == "") {
                this.errorProvider1.SetError(this.General.Serie, "Por favor aplica una serie válida al comprobante.");
                return false;
            }

            if (this.Comprobante.IdDirectorio == 0) {
                this.errorProvider1.SetError(this.General.Receptor, "Selecciona a un receptor");
                return false;
            }

            if (this.Comprobante.ClaveUsoCFDI == "P01" && this.Comprobante.IsEditable == true) {
                this.errorProvider1.SetError(this.General.Receptor.UsoCFDI, "Clave de uso de CFDI no válido.");
            }

            return true;
        }
        #endregion
    }
}
