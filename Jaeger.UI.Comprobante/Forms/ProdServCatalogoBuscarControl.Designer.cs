﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ProdServCatalogoBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridSearchResult = new Telerik.WinControls.UI.RadGridView();
            this.WorkerSearch = new System.ComponentModel.BackgroundWorker();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult.MasterTemplate)).BeginInit();
            this.gridSearchResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            this.SuspendLayout();
            // 
            // gridProductoCatalogo
            // 
            this.gridSearchResult.Controls.Add(this.Espera);
            this.gridSearchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSearchResult.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridSearchResult.MasterTemplate.AllowAddNewRow = false;
            this.gridSearchResult.MasterTemplate.AllowDeleteRow = false;
            this.gridSearchResult.MasterTemplate.AllowEditRow = false;
            this.gridSearchResult.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdModelo";
            gridViewTextBoxColumn1.FormatString = "{0:PT00000#}";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdModelo";
            gridViewTextBoxColumn1.Width = 65;
            gridViewTextBoxColumn2.FieldName = "Tipo";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Tipo";
            gridViewTextBoxColumn2.Width = 80;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn3.FieldName = "ProductoDescripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "ProductoDescripcion";
            gridViewTextBoxColumn3.Width = 350;
            gridViewTextBoxColumn4.FieldName = "Marca";
            gridViewTextBoxColumn4.HeaderText = "Marca";
            gridViewTextBoxColumn4.Name = "Marca";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.FieldName = "Especificacion";
            gridViewTextBoxColumn5.HeaderText = "Especificación";
            gridViewTextBoxColumn5.Name = "Especificacion";
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn6.HeaderText = "Clave\r\nProd. Serv.";
            gridViewTextBoxColumn6.Name = "ClaveProdServ";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 80;
            gridViewComboBoxColumn1.FieldName = "ClaveUnidad";
            gridViewComboBoxColumn1.HeaderText = "Clave\r\nUnidad";
            gridViewComboBoxColumn1.Name = "ClaveUnidad";
            gridViewComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn1.Width = 80;
            gridViewTextBoxColumn7.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn7.HeaderText = "Identificador";
            gridViewTextBoxColumn7.Name = "NoIdentificacion";
            gridViewTextBoxColumn7.Width = 95;
            gridViewComboBoxColumn2.FieldName = "Unidad";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 75;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "Unitario";
            gridViewTextBoxColumn8.FormatString = "{0:n}";
            gridViewTextBoxColumn8.HeaderText = "Unitario";
            gridViewTextBoxColumn8.Name = "Unitario";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.FieldName = "CtaPredial";
            gridViewTextBoxColumn9.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn9.Name = "CtaPredial";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.FieldName = "NumRequerimiento";
            gridViewTextBoxColumn10.HeaderText = "Núm Req.";
            gridViewTextBoxColumn10.Name = "NumRequerimiento";
            gridViewTextBoxColumn10.Width = 85;
            gridViewTextBoxColumn11.FieldName = "Creo";
            gridViewTextBoxColumn11.HeaderText = "Creó";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Creo";
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn12.FieldName = "FechaNuevo";
            gridViewTextBoxColumn12.FormatString = "{0:d}";
            gridViewTextBoxColumn12.HeaderText = "Fec. Sistema";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "FechaNuevo";
            gridViewTextBoxColumn12.Width = 75;
            this.gridSearchResult.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn7,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.gridSearchResult.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridSearchResult.Name = "gridProductoCatalogo";
            this.gridSearchResult.ShowGroupPanel = false;
            this.gridSearchResult.Size = new System.Drawing.Size(1246, 222);
            this.gridSearchResult.TabIndex = 6;
            this.gridSearchResult.DoubleClick += new System.EventHandler(this.gridProductoCatalogo_DoubleClick);
            // 
            // WorkerSearch
            // 
            this.WorkerSearch.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerSearch_DoWork);
            this.WorkerSearch.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerSearch_RunWorkerCompleted);
            // 
            // Espera
            // 
            this.Espera.Location = new System.Drawing.Point(364, 60);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // ProdServCatalogoBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridSearchResult);
            this.Name = "ProdServCatalogoBuscarControl";
            this.Size = new System.Drawing.Size(1246, 222);
            this.Load += new System.EventHandler(this.ProdServCatalogoBuscarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult)).EndInit();
            this.gridSearchResult.ResumeLayout(false);
            this.gridSearchResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView gridSearchResult;
        private System.ComponentModel.BackgroundWorker WorkerSearch;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
    }
}
