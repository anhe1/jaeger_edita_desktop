﻿using System;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobanteProductoForm : RadForm {
        private ICatalogoProductoService service;
        private ModeloDetailModel producto;

        public ComprobanteProductoForm(ModeloDetailModel modelo) {
            InitializeComponent();
            this.producto = modelo;
        }

        private void ViewComprobanteProducto_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.service = new CatalogoProductoService(AlmacenEnum.PT);
            using (var espera = new Waiting2Form(this.Cargar)) {
                espera.Text = "Cargando ...";
                espera.ShowDialog(this);
            }
            this.ToolBar.Actualizar.PerformClick();
        }

        private void ProductoServicioItem_Click(object sender, EventArgs e) {
            if (sender as RadMenuItem is RadMenuItem) {
                RadMenuItem button = (RadMenuItem)sender;
                this.buttonProductoServicio.DefaultItem = button;
                if (button.Text == this.buttonServicio.Text)
                    this.producto.Tipo = ProdServTipoEnum.Servicio;
                else
                    this.producto.Tipo = ProdServTipoEnum.Producto;
            }
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            this.producto = null;
            this.ToolBar.Actualizar.PerformClick();
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando";
                espera.ShowDialog(this);
            }
            this.Close();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            if (this.producto == null) {
                this.producto = new ModeloDetailModel();
                this.producto.Creo = ConfigService.Piloto.Clave;
                this.producto.FechaNuevo = DateTime.Now;
                this.producto.Almacen = AlmacenEnum.PT;
                this.producto.Tipo = ProdServTipoEnum.Producto;
            } else if (this.producto.IdProducto > 0) {
                // this.producto = this.data.GetById(this.producto.IdProducto);
            }
            this.CreateBinding();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void cboTrasladoIVA_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {
            if (this.producto.FactorTrasladoIVA == null) {
                this.txbTrasladoIVA.Enabled = false;
            } else if (this.producto.FactorTrasladoIVA == "Exento") {
                this.txbTrasladoIVA.Enabled = false;
                this.txbTrasladoIVA.Text = "0.0";
            } else {
                this.txbTrasladoIVA.Enabled = true;
                this.txbTrasladoIVA.Text = "0.0";
            }
        }

        private void txbTrasladoIVA_Validated(object sender, EventArgs e) {
            if (this.cboTrasladoIVA.Text == "Exento")
                this.txbTrasladoIVA.Text = "0.0";
            else {
                decimal[] vs = new decimal[] { new decimal(0), new decimal(0.08), new decimal(.16) };
                if (this.producto.ValorTrasladoIVA != null)
                    if (!(vs.Contains(decimal.Parse(this.producto.ValorTrasladoIVA.ToString()))))
                        RadMessageBox.Show(this, "El valor para el impuesto trasladado IVA se encuentra fuera del valor permitido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void cboTrasladoIEPS_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {
            this.txbTrasladoIEPS.Enabled = !(this.producto.FactorTrasladoIEPS == null);
        }

        private void txbTrasladoIEPS_Validated(object sender, EventArgs e) {
            // si el factor es cuota entonces debe esta en el rango
            if (this.cboTrasladoIEPS.Text == "Cuota") {
                if (!(decimal.Parse(this.txbTrasladoIEPS.Text) >= 0 && decimal.Parse(this.txbTrasladoIEPS.Text) <= new decimal(43.77)))
                    RadMessageBox.Show(this, "El valor para el impuesto trasladado IEPS se encuentra fuera del valor permitido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            } else {
                // en caso de ser tasa los valores son fijos y debe contener alguno de los valores constantes
                decimal[] vs = new decimal[] { new decimal(0.265), new decimal(0.3), new decimal(0.53), new decimal(0.5), new decimal(1.6), new decimal(0.304), new decimal(0.25), new decimal(0.09), new decimal(0.08), new decimal(0.07), new decimal(0.06), new decimal(0.03), new decimal(0) };
                if (!(vs.Contains(decimal.Parse(this.txbTrasladoIEPS.Text)))) {
                    RadMessageBox.Show(this, "El valor para el impuesto trasladado IEPS se encuentra fuera del valor permitido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void txbRetencionIVA_Validated(object sender, EventArgs e) {
            // en el caso de un iva retenido debe estar en el rango y el factor sera TASA
            if (!(decimal.Parse(this.txbRetencionIVA.Text) >= 0 && decimal.Parse(this.txbRetencionIVA.Text) <= new decimal(.16)))
                RadMessageBox.Show(this, "El valor del impuesto retenido IVA se encuentra fuera del valor permitido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }

        private void cboRetencionIEPS_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {
            if (this.producto.FactorRetencionIEPS == null)
                this.txbRetencionIEPS.Enabled = false;
            else
                this.txbRetencionIEPS.Enabled = true;
        }

        private void txbRetencionIEPS_Validated(object sender, EventArgs e) {
            // si el factor del IEPS es una TASA entonces debe contener algunos de los valores definidos
            //if(this.cboRetencionIEPS.SelectedItem.Text == "Tasa")
            if (this.producto.FactorRetencionIEPS == "Tasa") {
                decimal[] vs = new decimal[] { new decimal(0.265), new decimal(0.3), new decimal(0.53), new decimal(0.5), new decimal(1.6), new decimal(0.304), new decimal(0.25), new decimal(0.09), new decimal(0.08), new decimal(0.07), new decimal(0.06) };
                if (!vs.Contains<decimal>(decimal.Parse(this.producto.ValorRetencionIEPS.ToString())))
                    RadMessageBox.Show(this, "El valor del impuesto retenido IEPS se encuentra fuera de los valores fijos permitidos.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            } else if (this.cboRetencionIEPS.SelectedItem.Text == "Cuota") {
                // si el factor es CUOTA entonces el valor debe estar dentro del rango permitido
                if (!(decimal.Parse(this.txbRetencionIEPS.Text) >= 0 && decimal.Parse(this.txbRetencionIEPS.Text) <= new decimal(43.77)))
                    RadMessageBox.Show(this, "El valor del impuesto retenido IEPS se encuentra fuera del rango permitido.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void txbRetencionISR_Validated(object sender, EventArgs e) {
            if (!(decimal.Parse(this.txbRetencionISR.Text) >= 0 && decimal.Parse(this.txbRetencionISR.Text) <= new decimal(.35)))
                RadMessageBox.Show(this, "El valor del impuesto retenido ISR se encuentra fuera del rango permitido.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }

        private void chkRetnecionIVA_CheckStateChanged(object sender, EventArgs e) {
            if (!(this.chkRetnecionIVA.CheckState == CheckState.Checked)) {
                this.txbRetencionIVA.Enabled = false;
                this.producto.ValorRetencionIVA = null;
            } else {
                this.txbRetencionIVA.Enabled = true;
            }
        }

        private void chkRentencionISR_CheckStateChanged(object sender, EventArgs e) {
            if (!(this.chkRentencionISR.CheckState == CheckState.Checked)) {
                this.txbRetencionISR.Enabled = false;
                this.producto.ValorRetecionISR = null;
            } else {
                this.txbRetencionISR.Enabled = true;
            }
        }

        private void CreateBinding() {
            this.buttonProductoServicio.DataBindings.Clear();
            this.buttonProductoServicio.DataBindings.Add("Text", this.producto, "Tipo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbDescripcion.DataBindings.Clear();
            this.txbDescripcion.DataBindings.Add("Text", this.producto, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbMarca.DataBindings.Clear();
            this.txbMarca.DataBindings.Add("Text", this.producto, "Marca", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbUnidad.DataBindings.Clear();
            this.txbUnidad.DataBindings.Add("Text", this.producto, "Unidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbClaveProdServ.DataBindings.Clear();
            this.txbClaveProdServ.DataBindings.Add("Text", this.producto, "ClaveProdServ", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbClaveUnidad.DataBindings.Clear();
            this.txbClaveUnidad.DataBindings.Add("Text", this.producto, "ClaveUnidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbNoIdentificacion.DataBindings.Clear();
            this.txbNoIdentificacion.DataBindings.Add("Text", this.producto, "NoIdentificacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbCtaPredial.DataBindings.Clear();
            this.txbCtaPredial.DataBindings.Add("Text", this.producto, "CtaPredial", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbNumRequerimiento.DataBindings.Clear();
            this.txbNumRequerimiento.DataBindings.Add("Text", this.producto, "NumRequerimiento", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbUnitario.DataBindings.Clear();
            this.txbUnitario.DataBindings.Add("Text", this.producto, "Unitario", true, DataSourceUpdateMode.OnPropertyChanged);
            // impuestos trasladados
            this.cboTrasladoIVA.DataBindings.Clear();
            this.cboTrasladoIVA.DataBindings.Add("Text", this.producto, "FactorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cboTrasladoIVA.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            this.txbTrasladoIVA.DataBindings.Clear();
            this.txbTrasladoIVA.DataBindings.Add("Value", this.producto, "ValorTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbTrasladoIVA.Enabled = !(this.producto.ValorTrasladoIVA == null);

            this.cboTrasladoIEPS.DataBindings.Clear();
            this.cboTrasladoIEPS.DataBindings.Add("Text", this.producto, "FactorTrasladoIEPS", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cboTrasladoIEPS.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            this.txbTrasladoIEPS.DataBindings.Clear();
            this.txbTrasladoIEPS.DataBindings.Add("Value", this.producto, "ValorTrasladoIEPS", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbTrasladoIEPS.Enabled = !(this.producto.ValorTrasladoIEPS == null);
            // impuestos retenidos
            this.txbRetencionIVA.DataBindings.Clear();
            this.txbRetencionIVA.DataBindings.Add("Value", this.producto, "ValorRetencionIVA", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbRetencionIVA.Enabled = !(this.producto.ValorRetencionIVA == null);
            this.chkRetnecionIVA.Checked = !(this.producto.ValorRetencionIVA == null);

            this.txbRetencionISR.DataBindings.Clear();
            this.txbRetencionISR.DataBindings.Add("Value", this.producto, "ValorRetecionISR", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbRetencionISR.Enabled = !(this.producto.ValorRetecionISR == null);
            this.chkRentencionISR.Checked = !(this.producto.ValorRetecionISR == null);

            this.cboRetencionIEPS.DataBindings.Clear();
            this.cboRetencionIEPS.DataBindings.Add("Text", this.producto, "FactorRetencionIEPS", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cboRetencionIEPS.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            this.txbRetencionIEPS.DataBindings.Clear();
            this.txbRetencionIEPS.DataBindings.Add("Value", this.producto, "ValorRetencionIEPS", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txbRetencionIEPS.Enabled = !(this.producto.ValorRetencionIEPS == null);
        }

        private void Cargar() {

        }

        private void Guardar() {
            this.producto.IdModelo = this.service.Save(this.producto).IdModelo;
        }
    }
}
