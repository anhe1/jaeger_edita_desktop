﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ACuentaTerceroForm : RadForm {
        protected IContribuyenteService service;
        protected IRegimenesFiscalesCatalogo regimenesfiscales = new RegimenesFiscalesCatalogo();
        protected BindingList<ComprobanteContribuyenteModel> contribuyenteModels;
        private ComprobanteConceptoACuentaTerceros CurrentComplemento;

        public void OnAgregarComplemento(ComprobanteConceptoACuentaTerceros e) {
            if (this.AgregarComplemento != null)
                this.AgregarComplemento(this, e);
        }

        public event EventHandler<ComprobanteConceptoACuentaTerceros> AgregarComplemento;

        public ACuentaTerceroForm(ComprobanteConceptoACuentaTerceros complemento) {
            InitializeComponent();
            this.CurrentComplemento = complemento;
        }

        private void ACuentaTerceroForm_Load(object sender, EventArgs e) {
            this.service = new ContribuyenteService(Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido);
            this.CargarReceptores.RunWorkerAsync();
            
        }

        private void CreateBinding() {
            if (this.CurrentComplemento == null) {
                this.CurrentComplemento = new ComprobanteConceptoACuentaTerceros();
            }

            this.RfcACuentaTerceros.DataBindings.Clear();
            this.RfcACuentaTerceros.DataBindings.Add("Text", this.CurrentComplemento, "RfcACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NombreACuentaTerceros.DataBindings.Clear();
            this.NombreACuentaTerceros.DataBindings.Add("Text", this.CurrentComplemento, "NombreACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RegimenFiscalACuentaTerceros.DataBindings.Clear();
            this.RegimenFiscalACuentaTerceros.DataBindings.Add("SelectedValue", this.CurrentComplemento, "RegimenFiscalACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
            this.DomicilioFiscalACuentaTerceros.DataBindings.Clear();
            this.DomicilioFiscalACuentaTerceros.DataBindings.Add("Text", this.CurrentComplemento, "DomicilioFiscalACuentaTerceros", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Receptor_SelectedValueChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = this.NombreACuentaTerceros.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var receptor = temporal.DataBoundItem as ComprobanteContribuyenteModel;
                if (receptor != null) {
                    this.RfcACuentaTerceros.Text = receptor.RFC;
                    this.NombreACuentaTerceros.Text = receptor.Nombre;
                    
                    this.DomicilioFiscalACuentaTerceros.Text = receptor.DomicilioFiscal;

                    if (receptor.RegimenFiscal != null) {
                        this.RegimenFiscalACuentaTerceros.SelectedValue = receptor.RegimenFiscal;
                    }
                } else {
                    RadMessageBox.Show(this, Properties.Resources.Message_Objeto_NoValido, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void Receptor_DropDownOpened(object sender, EventArgs e) {
            this.NombreACuentaTerceros.DataSource = this.contribuyenteModels;
        }

        private void TComplemento_Guardar_Click(object sender, EventArgs e) {
            this.OnAgregarComplemento(this.CurrentComplemento);
            this.Close();
        }

        private void TComplemento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CargarReceptores_DoWork(object sender, DoWorkEventArgs e) {
            this.regimenesfiscales.Load();
            this.buttonActualizarDirectorio.Enabled = false;
            this.NombreACuentaTerceros.Enabled = false;
            this.contribuyenteModels = this.service.GetContribuyentes();

        }

        private void CargarReceptores_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // regimen fiscal
            this.RegimenFiscalACuentaTerceros.DataSource = this.regimenesfiscales.Items;
            this.RegimenFiscalACuentaTerceros.SelectedIndex = -1;
            this.RegimenFiscalACuentaTerceros.SelectedValue = null;

            this.NombreACuentaTerceros.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.NombreACuentaTerceros.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.NombreACuentaTerceros.EditorControl.TableElement.MinSize = new Size(300, 200);
            this.NombreACuentaTerceros.AutoSizeDropDownToBestFit = true;
            this.NombreACuentaTerceros.SelectedIndex = -1;
            this.NombreACuentaTerceros.SelectedValue = null;
            this.NombreACuentaTerceros.Enabled = true;
            this.buttonActualizarDirectorio.Enabled = true;
            this.NombreACuentaTerceros.Enabled = true;
            this.NombreACuentaTerceros.SelectedValueChanged += new EventHandler(this.Receptor_SelectedValueChanged);
            this.NombreACuentaTerceros.DropDownOpened += new EventHandler(this.Receptor_DropDownOpened);
            this.CreateBinding();
        }
    }
}
