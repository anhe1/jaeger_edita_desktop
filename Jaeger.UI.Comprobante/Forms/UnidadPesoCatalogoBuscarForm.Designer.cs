﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class UnidadPesoCatalogoBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.gridUnidades = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidades.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "Producto / Servicio:";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowAgregar = true;
            this.TCatalogo.ShowBuscar = true;
            this.TCatalogo.ShowCerrar = false;
            this.TCatalogo.ShowExistencia = false;
            this.TCatalogo.ShowFiltro = true;
            this.TCatalogo.Size = new System.Drawing.Size(583, 30);
            this.TCatalogo.TabIndex = 0;
            this.TCatalogo.ButtonBuscar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Buscar_Click);
            this.TCatalogo.ButtonAgregar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Seleccionar_Click);
            this.TCatalogo.ButtonFiltro_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Filtro_Click);
            this.TCatalogo.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Cerrar_Click);
            // 
            // gridUnidades
            // 
            this.gridUnidades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUnidades.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridUnidades.MasterTemplate.AllowAddNewRow = false;
            this.gridUnidades.MasterTemplate.AllowCellContextMenu = false;
            this.gridUnidades.MasterTemplate.AllowColumnChooser = false;
            this.gridUnidades.MasterTemplate.AllowColumnReorder = false;
            this.gridUnidades.MasterTemplate.AllowDragToGroup = false;
            this.gridUnidades.MasterTemplate.AllowRowResize = false;
            this.gridUnidades.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.Name = "Clave";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.FieldName = "Descripcion";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "Descripcion";
            gridViewTextBoxColumn6.Width = 280;
            this.gridUnidades.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.gridUnidades.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridUnidades.Name = "gridUnidades";
            this.gridUnidades.ShowGroupPanel = false;
            this.gridUnidades.Size = new System.Drawing.Size(583, 361);
            this.gridUnidades.TabIndex = 7;
            // 
            // UnidadPesoCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 391);
            this.Controls.Add(this.gridUnidades);
            this.Controls.Add(this.TCatalogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnidadPesoCatalogoBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "UnidadPesoCatalogoBuscarForm";
            this.Load += new System.EventHandler(this.CatalogoBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidades.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUnidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarBuscarControl TCatalogo;
        public Telerik.WinControls.UI.RadGridView gridUnidades;
    }
}