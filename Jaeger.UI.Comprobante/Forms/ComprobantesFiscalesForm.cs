﻿using System;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Comprobante.Builder;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Util;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesFiscalesForm : RadForm {
        #region declaraciones
        private bool IsBegingExported = false;
        private BackgroundWorker _Preparar;
        protected readonly CFDISubTipoEnum _SubTipo;
        protected internal IComprobantesFiscalesService _Service;
        protected internal BindingList<IComprobanteFiscalDetailSingleModel> _DataSource;

        protected internal RadMenuItem MContextual_Copiar = new RadMenuItem { Text = "Copiar" };
        protected internal RadMenuItem MContextual_MultiSeleccion = new RadMenuItem { Text = "Selección múltiple" };
        protected internal RadMenuItem MContextual_EstadoSAT = new RadMenuItem { Text = "Estado SAT" };
        protected internal RadMenuItem MContextual_Adicional = new RadMenuItem { Text = "Acciones" };
        protected internal RadMenuItem MContextual_Cargar = new RadMenuItem { Text = "Cargar", Name = "_upload" };
        protected internal RadMenuItem MContextual_CargarXML = new RadMenuItem { Text = "Cargar XML", Name = "_upxml", Enabled = true, ToolTipText = "Carga de archivo XML" };
        protected internal RadMenuItem MContextual_CargarPDF = new RadMenuItem { Text = "Cargar PDF", Name = "_uppdf", ToolTipText = "Representación impresa PDF" };
        protected internal RadMenuItem MContextual_CargarAcuse = new RadMenuItem { Text = "Cargar Acuse de cancelación", Name = "_uppdf", ToolTipText = "Acuse de cancelación (PDF)", Enabled = false };
        protected internal RadMenuItem MContextual_Descarga = new RadMenuItem { Text = "Descargar", Name = "_download" };
        protected internal RadMenuItem MContextual_CrearPDF = new RadMenuItem { Text = "Crear representación impresa" };
        protected internal RadMenuItem MContextual_Serializar = new RadMenuItem { Text = "Serializar XML" };
        protected internal RadMenuItem MContextual_Serializar_Conceptos = new RadMenuItem { Text = "Conceptos" };
        protected internal RadMenuItem MContextual_Serializar_Complemento = new RadMenuItem { Text = "Complemento" };
        protected internal RadMenuItem MContextual_CPago_Relacionar = new RadMenuItem { Text = "Buscar relación" };

        protected internal RadMenuItem TComplementoPagoRevisar = new RadMenuItem { Text = "Complemento de pagos (serializar)", ToolTipText = "Revisar complmento de pagos del mes seleccionado." };
        protected internal RadMenuItem TComprobante_Imprimir_Listado = new RadMenuItem { Text = "Listado" };
        protected internal RadMenuItem TComprobante_Imprimir_ListadoV = new RadMenuItem { Text = "Vertical" };
        protected internal RadMenuItem TComprobante_Imprimir_ListadoH = new RadMenuItem { Text = "Horizontal" };
        #endregion

        public ComprobantesFiscalesForm(CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this._SubTipo = subTipo;
        }

        private void ComprobantesFiscalesForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            using (IComprobanteFiscalGridBuilder view = new ComprobanteFiscalGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Master().Build());
                this.gridConcepto.Columns.AddRange(view.Templetes().Conceptos().Build());
            }

            this._Service = new ComprobantesFiscalesService(this._SubTipo);
            this.GridData.Standard();
            this.gridConcepto.Standard();
            this.gridCFDIRelacionado.Standard();
            this.gridComprobanteRelacionado.Standard();
            this.gridComplementoPagos.Standard();
            this.gridComplementoPagosDoctoRelacionado.Standard();
            this.gridComplementoPagoD.Standard();

            this._Preparar = new BackgroundWorker();
            this._Preparar.DoWork += Preparar_DoWork;
            this._Preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this._Preparar.RunWorkerAsync();

            // regla para cambiar de color todos los comprobantes con un estado CANCELADO o NO ENCONTRADO
            var estado = new ExpressionFormattingObject("Estado del Comprobante", "Estado = 'NoEncontrado' OR Estado = 'Cancelado'", true) { RowForeColor = System.Drawing.Color.DarkGray };
            var vigente = new ExpressionFormattingObject("Vigente", "Estado = 'Vigente'", false) { CellForeColor = System.Drawing.Color.Green };
            var egreso = new ExpressionFormattingObject("Egreso", "TipoComprobante = 1", false) { CellForeColor = System.Drawing.Color.Red };

            this.GridData.Columns["Total"].ConditionalFormattingObjectList.Add(egreso);
            this.GridData.Columns["Estado"].ConditionalFormattingObjectList.Add(estado);
            this.GridData.Columns["Estado"].ConditionalFormattingObjectList.Add(vigente);

            this.GridData.CellBeginEdit += GridData_CellBeginEdit;
            this.GridData.CellEndEdit += GridData_CellEndEdit;
            this.GridData.CellValidating += GridData_CellValidating;
            this.GridData.EditorRequired += GridData_EditorRequired;
            this.gridConcepto.HierarchyDataProvider = new GridViewEventDataProvider(this.gridConcepto);
            this.gridCFDIRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.gridCFDIRelacionado);
            this.gridComprobanteRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.gridComprobanteRelacionado);
            this.gridComplementoPagos.HierarchyDataProvider = new GridViewEventDataProvider(this.gridComplementoPagos);
            this.gridComplementoPagoD.HierarchyDataProvider = new GridViewEventDataProvider(this.gridComplementoPagoD);
            this.gridComplementoPagosDoctoRelacionado.HierarchyDataProvider = new GridViewEventDataProvider(this.gridComplementoPagosDoctoRelacionado);

            this.TComprobante.Herramientas.Items.Add(this.TComplementoPagoRevisar);

            this.MContextual_Cargar.Items.AddRange(new RadMenuItem[] { this.MContextual_CargarXML, this.MContextual_CargarPDF, this.MContextual_CargarAcuse });
            this.MContextual_Serializar.Items.AddRange(new RadMenuItem[] { this.MContextual_Serializar_Conceptos, this.MContextual_Serializar_Complemento });
            this.MContextual_Adicional.Items.AddRange(new RadMenuItem[] { this.MContextual_Cargar, this.MContextual_Descarga, this.MContextual_CrearPDF, this.MContextual_Serializar });
            this.menuContextual.Items.AddRange(new RadMenuItem[] { this.MContextual_Copiar, this.MContextual_MultiSeleccion, this.MContextual_EstadoSAT, this.MContextual_Adicional });

            this.MContextual_Copiar.Click += this.MContextual_Copiar_Click;
            this.MContextual_MultiSeleccion.Click += this.MContextual_Seleccion_Click;
            this.MContextual_EstadoSAT.Click += this.MContextual_EstadoSAT_Click;
            this.MContextual_Serializar_Conceptos.Click += this.MContextual_Serializar_Conceptos_Click;
            this.MContextual_Serializar_Complemento.Click += this.MContextual_Serializar_Complemento_Click;
            this.MContextual_Descarga.Click += this.MContextual_Descargar_Click;
            this.MContextual_CrearPDF.Click += this.MContextual_CrearPDF_Click;
            this.MContextual_CargarXML.Click += this.MContextual_SubirXML_Click;
            this.MContextual_CargarPDF.Click += this.MContextual_SubirPDF_Click;

            this.MContextual_CPago_Relacionar.Click += MContextual_CPago_Relacionar_Click;
            this.TComprobante.Actualizar.Click += this.TComprobante_Actualizar_Click;
            this.TComprobante.Cancelar.Click += TComprobante_Cancelar_Click;
            this.TComprobante.ExportarExcel.Click += this.TComprobante_ExportarExcel_Click;
            this.TComprobante.AutoSuma.Click += TComprobante_AutoSuma_Click;
            this.TComprobante.Filtro.Click += TComprobante_Filtro_Click;
            this.TComprobante_Imprimir_Listado.Items.AddRange(this.TComprobante_Imprimir_ListadoV, this.TComprobante_Imprimir_ListadoH);
            this.TComprobante.Imprimir.Items.Add(this.TComprobante_Imprimir_Listado);
            this.TComprobante_Imprimir_ListadoV.Click += this.TComprobante_Imprimir_Listado_Click;
            this.TComprobante_Imprimir_ListadoH.Click += this.TComprobante_Imprimir_Listado_Click;
            this.TComprobante.Cerrar.Click += this.TComprobante_Cerrar_Click;
            this.TComplementoPagoRevisar.Click += TComprobante_ComplementoPagoRevisar_Click;
        }

        #region barra de herramientas
        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = Properties.Resources.Message_Consulta;
                espera.ShowDialog(this);
                this.GridData.DataSource = this._DataSource;
            }
        }

        public virtual void TComprobante_Cancelar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as IComprobanteFiscalSingleModel;
                if (_seleccionado != null) {
                    if (_seleccionado.IdSubTipo == (int)CFDISubTipoEnum.Emitido) {
                        var _motivo = new ComprobanteFiscalCancelacionForm(_seleccionado, this._Service);
                        _motivo.ShowDialog(this);
                    }
                } else {
                    RadMessageBox.Show("No se selecciono un objeto válido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        public virtual void TComprobante_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        public virtual void TComprobante_Imprimir_Listado_Click(object sender, EventArgs e) {
            if (this.GridData.ChildRows.Count > 0) {
                var _seleccion = new List<ComprobanteFiscalDetailSingleModel>();
                _seleccion.AddRange(this.GridData.ChildRows.Select(it => it.DataBoundItem as ComprobanteFiscalDetailSingleModel));
                var vertical = ((RadMenuItem)sender).Text == this.TComprobante_Imprimir_ListadoV.Text;
                var reporte = new ReporteForm(new ComprobanteListadoPrinter(_seleccion, vertical));
                reporte.Show();
            }
        }

        public virtual void TComprobante_ExportarExcel_Click(object sender, EventArgs e) {
            this.IsBegingExported = true;
            var exportar = new TelerikGridExportForm(this.GridData);
            exportar.ShowDialog(this);
            this.IsBegingExported = false;
        }

        public virtual void TComprobante_AutoSuma_Click(object sender, EventArgs e) {
            this.GridData.AutoSum(((CommandBarToggleButton)sender).ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off);
        }

        protected virtual void TComprobante_ComplementoPagoRevisar_Click(object sender, EventArgs e) {
           using (var espera = new Waiting2Form(this.ComplementoPagoSerializarT)) {
                espera.Text = "Espera un momento";
                espera.ShowDialog(this);
            }
        }

        public virtual void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this._Service = null;
            this.Close();
        }
        #endregion

        #region menu contextual
        public virtual void MContextual_Seleccion_Click(object sender, EventArgs e) {
            this.MContextual_MultiSeleccion.IsChecked = !this.MContextual_MultiSeleccion.IsChecked;
            this.GridData.MultiSelect = this.MContextual_MultiSeleccion.IsChecked;
        }

        public virtual void MContextual_Copiar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentCell != null) { Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString()); }
        }

        public virtual void MContextual_EstadoSAT_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.EstadoSAT)) {
                espera.Text = "Consultando estado, un momento por favor...";
                espera.ShowDialog(this);
            }
            var response = (string)this.MContextual_EstadoSAT.Tag;
            if (response != null)
                RadMessageBox.Show(this, response, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            this.MContextual_EstadoSAT.Tag = null;
        }

        public virtual void MContextual_CrearPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var _seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    if (_seleccionado.SubTipo == CFDISubTipoEnum.Recibido) {
                        var d = this._Service.Printer(_seleccionado);
                        var reporte = new ReporteForm(d);
                        reporte.Show();
                    } else {
                        if (ValidacionService.UUID(_seleccionado.IdDocumento)) {
                            this._Service.ToHtml(_seleccionado);
                        } else {
                            var r0 = this._Service.ToPrinter(_seleccionado);
                            var reporte = new ReporteForm(r0);
                            reporte.ShowDialog(this);
                        }
                    }
                } else {
                    RadMessageBox.Show("No se selecciono un objeto válido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        public virtual void MContextual_SubirXML_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() { Filter = "*.xml|*.XML", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            if (seleccionado != null) {
                if (ValidacionService.URL(seleccionado.UrlFileXML)) {
                    if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName, ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        return;
                }

                if (File.Exists(openFileDialog.FileName) == false) {
                    RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
            }
        }

        public virtual void MContextual_SubirPDF_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var openFileDialog = new OpenFileDialog() { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
                if (openFileDialog.ShowDialog() != DialogResult.OK) {
                    return;
                }

                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    if (ValidacionService.URL(seleccionado.UrlFilePDF)) {
                        if (RadMessageBox.Show(this, string.Concat("Se reemplazara el archivo actualmente cargado por: ", openFileDialog.FileName,
                            ", esta acción no se puede revertir ¿Esta seguro?"), "Reemplazar archivo", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                            return;
                    }

                    if (File.Exists(openFileDialog.FileName) == false) {
                        RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                    this.MContextual_CargarPDF.Tag = openFileDialog.FileName;
                    using (var espera = new Waiting2Form(this.ReemplazaPDF)) {
                        espera.Text = "Reemplazando, espere un momento...";
                        espera.ShowDialog(this);
                    }

                    if (this.MContextual_CargarPDF.Tag != null) {
                        if ((bool)this.MContextual_CargarPDF.Tag == false) {
                            RadMessageBox.Show(this, "Ocurrio un error al procesar el archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                        }
                    }
                }
            }
        }

        public virtual void MContextual_Serializar_Complemento_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                using (var espera = new Waiting1Form(this.ComplementoPagoSerializar)) {
                    espera.Text = "Creando ...";
                    espera.ShowDialog(this);
                }
                var row = ((GridViewHierarchyRowInfo)this.GridData.CurrentRow);
                foreach (var item in row.Views) {
                    item.Refresh();
                }
            }
        }

        public virtual void MContextual_Serializar_Conceptos_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                using (var espera = new Waiting1Form(this.ConceptosSerializar)) {
                    espera.Text = "Creando ...";
                    espera.ShowDialog(this);
                }
                var row = ((GridViewHierarchyRowInfo)this.GridData.CurrentRow);
                foreach (var item in row.Views) {
                    item.Refresh();
                }
            }
        }

        public virtual void MContextual_Descargar_Click(object sender, EventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                var folder = new FolderBrowserDialog() { Description = "Selecciona ruta de descarga" };
                if (folder.ShowDialog(this) != DialogResult.OK)
                    return;

                if (Directory.Exists(folder.SelectedPath) == false) {
                    RadMessageBox.Show(this, "No se encontro una ruta de descarga valida!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                MContextual_Descarga.Tag = folder.SelectedPath;

                using (var espera = new Waiting1Form(this.Descargar)) {
                    espera.Text = "Procesando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void MContextual_CPago_Relacionar_Click(object sender, EventArgs e) {
            this.BuscarRelacion();
        }
        #endregion

        #region acciones del grid
        private void GridData_EditorRequired(object sender, EditorRequiredEventArgs e) {
            if (this.GridData.CurrentRow is GridViewFilteringRowInfo) {
                if (e.EditorType == typeof(RadDropDownListEditor)) {
                    e.EditorType = typeof(RadTextBoxEditor);
                }
            }
        }

        public virtual void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    var seleccionado = (ComprobanteFiscalDetailSingleModel)this.GridData.ReturnRowSelected();
                    // en caso de no tener ningun tipo de status
                    if (seleccionado.Status == null) {
                        e.Cancel = true;
                        this.GridData.CancelEdit();
                        return;
                    }
                    if (this._SubTipo == CFDISubTipoEnum.Emitido) {
                        e.Cancel = true;
                        return;
                    } else if (this._SubTipo == CFDISubTipoEnum.Recibido) {
                        var _status = (CFDIStatusRecibidoEnum)Enum.Parse(typeof(CFDIStatusRecibidoEnum), seleccionado.Status, true);
                        if (_status != CFDIStatusRecibidoEnum.Importado) {
                            e.Cancel = true;
                        }
                    } else {
                        e.Cancel = true;
                    }
                }
            }
        }

        protected virtual void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Column == null) {
                return;
            }
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                            this.GridData.CancelEdit();
                        } else {
                            if (this._SubTipo == CFDISubTipoEnum.Recibido) {
                                var _status = (CFDIStatusRecibidoEnum)Enum.Parse(typeof(CFDIStatusRecibidoEnum), e.Value.ToString(), true);
                                if (_status == CFDIStatusRecibidoEnum.PorPagar) {

                                    e.Cancel = false;
                                    e.Column.Tag = "Actualizar";
                                } else {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if ((string)e.Column.Tag == "Actualizar") {
                        using (var espera = new Waiting1Form(this.ActualizaStatus)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.TComprobante.Tag != null) {
                            if ((bool)this.TComprobante.Tag == false) {
                                MessageBox.Show("Error");
                                this.TComprobante.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        public virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {

            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate) {
                    e.ContextMenu = this.menuContextual.DropDown;
                } else if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.gridComplementoPagosDoctoRelacionado) {
                    if ((int)this.GridData.CurrentRow.ViewInfo.CurrentRow.Cells["IdComprobante"].Value == 0)
                        e.ContextMenu = this.mContextualComplementoP.DropDown;
                }
            }
        }

        public virtual void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name.ToLower() == "UrlFileXml".ToLower() || e.Column.Name.ToLower() == "UrlFilePdf".ToLower()) {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name.ToLower() != "Status".ToLower()) {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public virtual void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name.ToLower() == "UrlFileXml".ToLower() || e.Column.Name.ToLower() == "UrlFilePdf".ToLower()) {
                        var single = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                        string liga = "";
                        if (e.Column.Name.ToLower() == "UrlFileXml".ToLower())
                            liga = single.UrlFileXML;
                        else
                            if (e.Column.Name.ToLower() == "UrlFilePdf".ToLower())
                            liga = single.UrlFilePDF;
                        // validar la liga de descarga
                        if (ValidacionService.URL(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (FileService.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (this.IsBegingExported)
                return;

            if (e.Template.Caption == this.gridConcepto.Caption) {
                // lista de conceptos
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    Console.WriteLine("Conceptos");
                    var tabla = this._Service.GetConceptos(rowView.Id);
                    foreach (var item in tabla) {
                        var newRow = e.Template.Rows.NewRow();
                        newRow.Cells["NumPedido"].Value = item.NumPedido;
                        newRow.Cells["Cantidad"].Value = item.Cantidad;
                        newRow.Cells["ClaveUnidad"].Value = item.ClaveUnidad;
                        newRow.Cells["Unidad"].Value = item.Unidad;
                        newRow.Cells["ClaveProdServ"].Value = item.ClaveProdServ;
                        newRow.Cells["Descripcion"].Value = item.Descripcion;
                        newRow.Cells["ValorUnitario"].Value = item.ValorUnitario;
                        newRow.Cells["Descuento"].Value = item.Descuento;
                        newRow.Cells["SubTotal"].Value = item.SubTotal;
                        newRow.Cells["Importe"].Value = item.Importe;
                        e.SourceCollection.Add(newRow);
                    }
                }
            } else if (e.Template.Caption == this.gridCFDIRelacionado.Caption) {
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    var d = rowView.CfdiRelacionados;
                    if (d != null) {
                        if (d.TipoRelacion.Clave != null) {
                            var newRow = e.Template.Rows.NewRow();
                            newRow.Cells["Clave"].Value = d.TipoRelacion.Clave;// (d.TipoRelacion.Descripcion == null ? "" : d.TipoRelacion.Descripcion);
                            e.SourceCollection.Add(newRow);
                        }
                    }
                }
            } else if (e.Template.Caption == this.gridComprobanteRelacionado.Caption) {
                // comprobantes relacionados al CFDI
                var otro = e.ParentRow.Parent as GridViewHierarchyRowInfo;
                var rowView = otro.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    var d = rowView.CfdiRelacionados;
                    if (d != null) {
                        if (d.CfdiRelacionado.Count > 0) {
                            if (d.CfdiRelacionado != null) {
                                foreach (var item in d.CfdiRelacionado) {
                                    var newRow = e.Template.Rows.NewRow();
                                    newRow.Cells["UUID"].Value = item.IdDocumento;
                                    newRow.Cells["folio"].Value = item.Folio;
                                    newRow.Cells["serie"].Value = item.Serie;
                                    newRow.Cells["rfc"].Value = item.RFC;
                                    newRow.Cells["nombre"].Value = item.Nombre;
                                    newRow.Cells["total"].Value = item.Total;
                                    e.SourceCollection.Add(newRow);
                                }
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.gridComplementoPagos.Caption) {
                // en caso de ser un comprobante de recepcion de pago
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    if (rowView.TipoComprobante == Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Pagos) {
                        using (var espera = new Waiting1Form(this.ComplementoPago)) {
                            espera.Text = "Complemento de pago ...";
                            espera.ShowDialog(this);
                        }
                        if (rowView.RecepcionPago != null) {
                            if (rowView.RecepcionPago.Count > 0) {
                                foreach (var item in rowView.RecepcionPago) {
                                    GridViewRowInfo row = e.Template.Rows.NewRow();
                                    row.Cells["FechaPago"].Value = item.FechaPagoP;
                                    row.Cells["FormaDePagoP"].Value = item.FormaDePagoP;
                                    row.Cells["MonedaP"].Value = item.MonedaP;
                                    row.Cells["TipoCambioP"].Value = item.TipoCambioP;
                                    row.Cells["CtaBeneficiario"].Value = item.CtaBeneficiario;
                                    row.Cells["CtaOrdenante"].Value = item.CtaOrdenante;
                                    row.Cells["NomBancoOrdExt"].Value = item.NomBancoOrdExt;
                                    row.Cells["NumOperacion"].Value = item.NumOperacion;
                                    row.Cells["Monto"].Value = item.Monto;
                                    row.Cells["TipoCadPago"].Value = item.TipoCadPago;
                                    row.Cells["RfcEmisorCtaBen"].Value = item.RfcEmisorCtaBen;
                                    row.Cells["RfcEmisorCtaOrd"].Value = item.RfcEmisorCtaOrd;
                                    row.Tag = item;
                                    e.SourceCollection.Add(row);
                                }
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.gridComplementoPagosDoctoRelacionado.Caption) {
                // documentos relacionados en el complemento de pagos 
                var rowView = e.ParentRow.Tag as ComplementoPagoDetailModel;
                if (!(rowView == null)) {
                    var pagos10 = rowView;
                    if (!(pagos10 == null)) {
                        if (pagos10.DoctoRelacionados != null) {
                            foreach (var item in pagos10.DoctoRelacionados) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["Folio"].Value = item.Folio;
                                row.Cells["Serie"].Value = item.SerieDR;
                                row.Cells["FechaEmision"].Value = item.FechaEmision;
                                row.Cells["ObjetoImpDR"].Value = item.ObjetoImpDR;
                                row.Cells["IdDocumento"].Value = item.IdDocumentoDR;
                                row.Cells["MonedaDR"].Value = item.MonedaDR;
                                row.Cells["EquivalenciaDR"].Value = item.EquivalenciaDR;
                                row.Cells["MetodoPago"].Value = item.MetodoPago;
                                row.Cells["NumParcialidad"].Value = item.NumParcialidad;
                                row.Cells["ImpSaldoAnt"].Value = item.ImpSaldoAnt;
                                row.Cells["ImpPagado"].Value = item.ImpPagado;
                                row.Cells["ImpSaldoInsoluto"].Value = item.ImpSaldoInsoluto;
                                row.Cells["IdComprobante"].Value = item.IdComprobanteR;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.gridComplementoPagoD.Caption) {
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    using (var espera = new Waiting1Form(this.ComplementoPagoD)) {
                        espera.Text = "CFDI relacionados ...";
                        espera.ShowDialog(this);
                    }
                    var _cPago = (BindingList<ComplementoPagoMergeDoctoModel>)rowView.Complemento;
                    if (_cPago != null) {
                        if (_cPago.Count > 0) {
                            foreach (var item in _cPago) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["IdComprobanteR"].Value = item.IdComprobanteR;
                                row.Cells["StatusP"].Value = item.StatusP;
                                row.Cells["FolioP"].Value = item.FolioP;
                                row.Cells["SerieP"].Value = item.SerieP;
                                row.Cells["FechaEmisionP"].Value = item.FechaEmisionP;
                                row.Cells["FechaPago"].Value = item.FechaPagoP;
                                row.Cells["IdDocumentoP"].Value = item.IdDocumentoP;
                                row.Cells["FormaDePagoP"].Value = item.FormaDePagoP;
                                row.Cells["MonedaP"].Value = item.MonedaP;
                                row.Cells["TipoCambioP"].Value = item.TipoCambioP;
                                row.Cells["NumOperacion"].Value = item.NumOperacion;
                                row.Cells["NumParcialidad"].Value = item.NumParcialidad;
                                row.Cells["ImpSaldoAnt"].Value = item.ImpSaldoAnt;
                                row.Cells["ImpPagado"].Value = item.ImpPagado;
                                row.Cells["ImpSaldoInsoluto"].Value = item.ImpSaldoInsoluto;
                                row.Cells["EstadoP"].Value = item.EstadoP;
                                if (rowView.SubTipo == CFDISubTipoEnum.Emitido) {
                                    row.Cells["EmisorRFCP"].Value = item.ReceptorRFCP;
                                    row.Cells["EmisorNombreP"].Value = item.ReceptorNombreP;
                                } else {
                                    row.Cells["EmisorRFCP"].Value = item.EmisorRFCP;
                                    row.Cells["EmisorNombreP"].Value = item.EmisorNombreP;
                                }
                                row.Tag = item;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region preparar
        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            var combo = this.GridData.Columns["Status"] as GridViewComboBoxColumn;
            combo.DisplayMember = "Descripcion";
            combo.DataSource = ComprobanteFiscalService.GetStatus(this._SubTipo);
            this.GridData.AllowEditRow = true;

            var combo1 = this.gridCFDIRelacionado.Columns["Clave"] as GridViewComboBoxColumn;
            combo1.DisplayMember = "Descriptor";
            combo1.ValueMember = "Id";
            combo1.DataSource = ComprobanteFiscalService.GetCFDIRelaciones();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }
        #endregion

        #region metodos privados
        public virtual void Consultar() { }

        public virtual void ReemplazaPDF() {
            var f1 = (string)this.MContextual_CargarPDF.Tag;
            var fi = new FileInfo(f1);
            if (fi.Exists) {
                var seleccionado = this.GetSelected();
                var d0 = FileService.ReadFileB64(fi);
                var d1 = this._Service.Upload(d0, fi.FullName, seleccionado.KeyName() + ".pdf");
                if (ValidacionService.UUID(d1)) {
                    this.MContextual_CargarPDF.Tag = this._Service.UpdateUrlPdf(seleccionado.Id, d1);
                }
            }
        }

        private void EstadoSAT() {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    var response = this._Service.EstadoSAT(seleccionado.Id, seleccionado.EmisorRFC, seleccionado.ReceptorRFC, seleccionado.Total, seleccionado.IdDocumento);
                    seleccionado.Estado = response;
                    seleccionado.FechaEstado = DateTime.Now;
                    if (response.ToLower().Equals("cancelado")) {
                        seleccionado.Status = response;
                    }
                }
            }
        }

        private void ActualizaStatus() {
            var singleC = this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            if (singleC != null) {
                if (singleC.Estado == "Cancelado")
                    singleC.Status = "Cancelado";
                if (this._Service.UpdateStatus(singleC.Id, singleC.Status)) {
                    singleC.FechaModifica = DateTime.Now;
                    singleC.FechaEntrega = DateTime.Now;
                }
                this.GridData.CurrentRow.Tag = "";
            }
        }

        private void Descargar() {
            var _carpeta = (string)MContextual_Descarga.Tag;
            var _seleccion = this.GetSelection();

            foreach (var item in _seleccion) {
                if (ValidacionService.URL(item.UrlFileXML)) {
                    FileService.DownloadFile(item.UrlFileXML, Path.Combine(_carpeta, Path.GetFileName(item.UrlFileXML)));
                }

                if (ValidacionService.URL(item.UrlFilePDF)) {
                    FileService.DownloadFile(item.UrlFilePDF, Path.Combine(_carpeta, Path.GetFileName(item.UrlFilePDF)));
                }
            }
        }

        private void ComplementoPago() {
            var _seleccion = this.GetSelected();
            if (_seleccion != null) {
                var q0 = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().WithIdComprobante(_seleccion.Id).OnlyActive().Build();
                _seleccion.RecepcionPago = new BindingList<ComplementoPagoDetailModel>(this._Service.GetList<ComplementoPagoDetailModel>(q0).ToList());
            }
        }

        /// <summary>
        /// obtener listado de complemento de pago relacionado a un comprobante fiscal
        /// </summary>
        private void ComplementoPagoD() {
            var seleccion = this.GetSelected();
            if (seleccion != null) {
                var query = ComprobantesFiscalesService.Query().ComplementoPagoBuilder().Vista().GetComplementoPagoD(seleccion.Id, seleccion.IdDocumento).Build();
                seleccion.Complemento = new BindingList<ComplementoPagoMergeDoctoModel>(this._Service.GetList<ComplementoPagoMergeDoctoModel>(query).ToList());
            }
        }

        /// <summary>
        /// Serializar complemento de pago del comprobante seleccionado
        /// </summary>
        private void ComplementoPagoSerializar() {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = GetSelected();
                this._Service.Serializar(seleccionado);
            }
        }

        /// <summary>
        /// serializar complemento de pago por año
        /// </summary>
        private void ComplementoPagoSerializarT() {
            this._Service.ParcheComplemento(this.TComprobante.GetEjercicio());
        }

        private void ConceptosSerializar() {
            var _seleccion = this.GetSelected();
            if (_seleccion != null) {
                this._Service.SerializarConceptos(_seleccion);
            }
        }

        private void BuscarRelacion() {
            if (this.GridData.CurrentRow != null) {
                var d0 = this.GridData.CurrentRow.ViewTemplate;
                if (d0.Caption == this.gridComplementoPagosDoctoRelacionado.Caption) {
                    var d2 = this.GridData.CurrentRow;
                    if (((int)this.GridData.CurrentRow.Cells["IdComprobante"].Value) == 0) {
                        var d1 = ComprobantesFiscalesService.Query().WithIdDocumento(d2.Cells["IdDocumento"].Value.ToString()).Build();
                        var d3 = this._Service.GetList<ComprobanteFiscalDetailSingleModel>(d1).FirstOrDefault();
                        if (d3 != null) {
                            d2.Cells["IdComprobante"].Value = d3.Id;
                        }
                    }
                }
            }
            var seleccionado = this.GetSelected();
            if (seleccionado != null) {
                //if (seleccionado.RecepcionPago)
            }
        }

        public ComprobanteFiscalDetailSingleModel GetSelected() {
            if (this.GridData.CurrentRow != null) {
                return this.GridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
            }
            return null;
        }

        /// <summary>
        /// obtener listado de comprobantes seleccionados
        /// </summary>
        public virtual List<ComprobanteFiscalDetailSingleModel> GetSelection() {
            var seleccion = new List<ComprobanteFiscalDetailSingleModel>();
            seleccion.AddRange(this.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as ComprobanteFiscalDetailSingleModel));
            return seleccion;
        }
        #endregion
    }
}
