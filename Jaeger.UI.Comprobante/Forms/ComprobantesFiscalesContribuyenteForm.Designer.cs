﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobantesFiscalesContribuyenteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.Nombre = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TComprobante = new Jaeger.UI.Comprobante.Forms.ComprobanteFiscalGridControl();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Nombre
            // 
            this.Nombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nombre.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.Nombre.AutoSizeDropDownHeight = true;
            this.Nombre.AutoSizeDropDownToBestFit = true;
            this.Nombre.DisplayMember = "Nombre";
            // 
            // Nombre.NestedRadGridView
            // 
            this.Nombre.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nombre.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nombre.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nombre.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nombre.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nombre.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Denominación o Razón Social";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 250;
            gridViewTextBoxColumn3.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn3.HeaderText = "RFC";
            gridViewTextBoxColumn3.Name = "ReceptorRFC";
            gridViewTextBoxColumn3.Width = 105;
            this.Nombre.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Nombre.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nombre.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nombre.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nombre.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Nombre.EditorControl.Name = "NestedRadGridView";
            this.Nombre.EditorControl.ReadOnly = true;
            this.Nombre.EditorControl.ShowGroupPanel = false;
            this.Nombre.EditorControl.Size = new System.Drawing.Size(380, 150);
            this.Nombre.EditorControl.TabIndex = 0;
            this.Nombre.Location = new System.Drawing.Point(590, 5);
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Denominación o Razon Social ";
            this.Nombre.Size = new System.Drawing.Size(210, 20);
            this.Nombre.TabIndex = 13;
            this.Nombre.TabStop = false;
            this.Nombre.ValueMember = "Id";
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TComprobante.Permisos = uiAction1;
            this.TComprobante.ShowActualizar = true;
            this.TComprobante.ShowAutosuma = false;
            this.TComprobante.ShowCancelar = false;
            this.TComprobante.ShowCerrar = true;
            this.TComprobante.ShowEditar = true;
            this.TComprobante.ShowEjercicio = true;
            this.TComprobante.ShowExportarExcel = false;
            this.TComprobante.ShowFiltro = true;
            this.TComprobante.ShowHerramientas = false;
            this.TComprobante.ShowImprimir = false;
            this.TComprobante.ShowItem = false;
            this.TComprobante.ShowNuevo = true;
            this.TComprobante.ShowPeriodo = true;
            this.TComprobante.ShowSeleccionMultiple = true;
            this.TComprobante.Size = new System.Drawing.Size(800, 450);
            this.TComprobante.TabIndex = 14;
            this.TComprobante.Templete = Jaeger.UI.Comprobante.Builder.ComprobanteFiscalGridBuilder.TempleteEnum.None;
            // 
            // ComprobantesFiscalesContribuyenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.TComprobante);
            this.Name = "ComprobantesFiscalesContribuyenteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Por";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobantesFiscalesContribuyenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadMultiColumnComboBox Nombre;
        protected ComprobanteFiscalGridControl TComprobante;
    }
}