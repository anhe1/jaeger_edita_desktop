﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class TbComprobanteReceptorControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.Documento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarRowElement();
            this.DoctoRelacionado = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Label = new Telerik.WinControls.UI.CommandBarLabel();
            this.RazonSocialItem = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Autosuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            this.CommandBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Controls.Add(this.Documento);
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBar});
            this.CommandBar.Size = new System.Drawing.Size(1037, 55);
            this.CommandBar.TabIndex = 2;
            // 
            // Documento
            // 
            this.Documento.AutoSizeDropDownToBestFit = true;
            // 
            // Documento.NestedRadGridView
            // 
            this.Documento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Documento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Documento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Documento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Documento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Documento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Documento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "SubId";
            gridViewTextBoxColumn2.HeaderText = "SubId";
            gridViewTextBoxColumn2.Name = "SubId";
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn3.FieldName = "Status";
            gridViewTextBoxColumn3.HeaderText = "Status";
            gridViewTextBoxColumn3.Name = "Status";
            gridViewTextBoxColumn4.FieldName = "Folio";
            gridViewTextBoxColumn4.HeaderText = "Folio";
            gridViewTextBoxColumn4.Name = "Folio";
            gridViewTextBoxColumn5.FieldName = "Serie";
            gridViewTextBoxColumn5.HeaderText = "Serie";
            gridViewTextBoxColumn5.Name = "Serie";
            gridViewTextBoxColumn6.FieldName = "IdDocumento";
            gridViewTextBoxColumn6.HeaderText = "UUID";
            gridViewTextBoxColumn6.Name = "IdDocumento";
            gridViewTextBoxColumn7.FieldName = "EmisorRfc";
            gridViewTextBoxColumn7.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn7.Name = "RfcEmisor";
            gridViewTextBoxColumn8.FieldName = "Emisor";
            gridViewTextBoxColumn8.HeaderText = "Emisor";
            gridViewTextBoxColumn8.Name = "Emisor";
            gridViewTextBoxColumn9.FieldName = "ReceptorRfc";
            gridViewTextBoxColumn9.HeaderText = "Receptor (RFC)";
            gridViewTextBoxColumn9.Name = "RfcReceptor";
            gridViewTextBoxColumn10.FieldName = "Receptor";
            gridViewTextBoxColumn10.HeaderText = "Receptor";
            gridViewTextBoxColumn10.Name = "Receptor";
            gridViewTextBoxColumn11.FieldName = "Estado";
            gridViewTextBoxColumn11.HeaderText = "Estado";
            gridViewTextBoxColumn11.Name = "Estado";
            gridViewTextBoxColumn12.FieldName = "NumParcialidad";
            gridViewTextBoxColumn12.HeaderText = "Núm. Par.";
            gridViewTextBoxColumn12.Name = "NumParcialidad";
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Total";
            gridViewTextBoxColumn13.FormatString = "{0:$#,###0.00}";
            gridViewTextBoxColumn13.HeaderText = "Total";
            gridViewTextBoxColumn13.Name = "Total";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "Acumulado";
            gridViewTextBoxColumn14.FormatString = "{0:$#,###0.00}";
            gridViewTextBoxColumn14.HeaderText = "Acumulado";
            gridViewTextBoxColumn14.Name = "Acumulado";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.Documento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.Documento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Documento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Documento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Documento.EditorControl.Name = "NestedRadGridView";
            this.Documento.EditorControl.ReadOnly = true;
            this.Documento.EditorControl.ShowGroupPanel = false;
            this.Documento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Documento.EditorControl.TabIndex = 0;
            this.Documento.Location = new System.Drawing.Point(93, 4);
            this.Documento.Name = "Documento";
            this.Documento.NullText = "Selecciona";
            this.Documento.Size = new System.Drawing.Size(405, 20);
            this.Documento.TabIndex = 0;
            this.Documento.TabStop = false;
            // 
            // ToolBar
            // 
            this.ToolBar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.DoctoRelacionado});
            this.ToolBar.Text = "";
            this.ToolBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.UseCompatibleTextRendering = false;
            // 
            // DoctoRelacionado
            // 
            this.DoctoRelacionado.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DoctoRelacionado.DisplayName = "commandBarStripElement1";
            this.DoctoRelacionado.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.Label,
            this.RazonSocialItem,
            this.Separator1,
            this.Actualizar,
            this.Filtro,
            this.Autosuma,
            this.Imprimir,
            this.Cerrar});
            this.DoctoRelacionado.Name = "DoctoRelacionado";
            // 
            // 
            // 
            this.DoctoRelacionado.OverflowButton.Enabled = false;
            this.DoctoRelacionado.StretchHorizontally = true;
            this.DoctoRelacionado.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DoctoRelacionado.UseCompatibleTextRendering = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.DoctoRelacionado.GetChildAt(2))).Enabled = false;
            // 
            // Label
            // 
            this.Label.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Label.DisplayName = "Razón Social";
            this.Label.Name = "Label";
            this.Label.Text = "Razón Social:";
            this.Label.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Label.UseCompatibleTextRendering = false;
            // 
            // RazonSocialItem
            // 
            this.RazonSocialItem.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.RazonSocialItem.DisplayName = "Razón Social";
            this.RazonSocialItem.MinSize = new System.Drawing.Size(420, 0);
            this.RazonSocialItem.Name = "RazonSocialItem";
            this.RazonSocialItem.Text = "Comprobantes";
            this.RazonSocialItem.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.RazonSocialItem.UseCompatibleTextRendering = false;
            // 
            // Separator1
            // 
            this.Separator1.DisplayName = "Separator1";
            this.Separator1.Name = "Separator1";
            this.Separator1.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "commandBarButton1";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Comprobante.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Autosuma
            // 
            this.Autosuma.DisplayName = "Auto suma";
            this.Autosuma.DrawText = true;
            this.Autosuma.Image = global::Jaeger.UI.Comprobante.Properties.Resources.sigma_16px;
            this.Autosuma.Name = "Autosuma";
            this.Autosuma.Text = "Auto Suma";
            this.Autosuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Comprobante.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbComprobanteReceptorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CommandBar);
            this.Name = "TbComprobanteReceptorControl";
            this.Size = new System.Drawing.Size(1037, 30);
            this.Load += new System.EventHandler(this.TbComprobanteReceptorControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            this.CommandBar.ResumeLayout(false);
            this.CommandBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Documento;
        private Telerik.WinControls.UI.CommandBarRowElement ToolBar;
        private Telerik.WinControls.UI.CommandBarStripElement DoctoRelacionado;
        private Telerik.WinControls.UI.CommandBarLabel Label;
        private Telerik.WinControls.UI.CommandBarHostItem RazonSocialItem;
        private Telerik.WinControls.UI.CommandBarSeparator Separator1;
        protected internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Autosuma;
        protected internal Telerik.WinControls.UI.CommandBarButton Imprimir;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
    }
}
