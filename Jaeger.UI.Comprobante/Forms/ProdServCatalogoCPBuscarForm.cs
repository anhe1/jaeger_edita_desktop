﻿using System;
using Jaeger.Domain.Comprobante.Entities;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ProdServCatalogoCPBuscarForm : RadForm {

        public void OnAgregar(ComprobanteConceptoDetailModel e) {
            if (this.Agregar != null) {
                this.Agregar(this, e);
            }
        }

        public event EventHandler<ComprobanteConceptoDetailModel> Agregar;

        public ProdServCatalogoCPBuscarForm() {
            InitializeComponent();
        }

        private void ProdServCatalogoCPBuscarForm_Load(object sender, EventArgs e) {
            this.CatalogoSAT.SetService();
            this.TCatalogo.Descripcion.TextChanged += Descripcion_TextChanged;
            this.TCatalogo.Buscar.Click += Actualizar_Click;
        }

        private void Agregar_Click(object sender, EventArgs e) {
            var seleccionado = this.CatalogoSAT.GetSelected();
            this.OnAgregar(seleccionado);
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            this.CatalogoSAT.Search(this.TCatalogo.Descripcion.Text);
        }

        private void Descripcion_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Descripcion.Text.Length > 0) { 
                this.TCatalogo.Buscar.PerformClick();
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
