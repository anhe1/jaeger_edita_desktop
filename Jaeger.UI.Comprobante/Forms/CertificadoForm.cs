﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    /// <summary>
    /// certificado de sello digital
    /// </summary>
    public class CertificadoForm : Certificado.Forms.UploadForm {
        #region declaraciones
        protected internal ICertificadoService Service;
        protected internal Domain.Certificado.Entities.Response Response;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="service"></param>
        public CertificadoForm(ICertificadoService service) : base() {
            this.Header.Text = "Certificado de Sello Digital";
            this.Service = service;
            this.GeneraPFX.Visible = false;
            this.Load += CertificadoForm_Load;
        }

        private void CertificadoForm_Load(object sender, EventArgs e) {
            
        }

        protected override void TVerificarButton_Click(object sender, EventArgs eventArgs) {
            base.TVerificarButton_Click(sender, eventArgs);
            if (this.Response != null) {
                if (this.Response.IsOk) {
                    RadMessageBox.Show(this, this.Response.Message, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                    if (this.Response.Certificado.Tipo != "SELLO") {
                        RadMessageBox.Show(this, "No es un Certificado de Sello Digital", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                        this.Guardar.Enabled = false;
                    }
                } else {
                    RadMessageBox.Show(this, this.Response.Message, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            } else {
                RadMessageBox.Show(this, "Error", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        protected override void TSaveButton_Click(object sender, EventArgs e) {
            if (this.Response.Certificado.RFC.ToLower() != ConfigService.Synapsis.Empresa.RFC.ToLower()) {
                RadMessageBox.Show(this, "El certificado no pertenece a la empresa registrada.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production) {
                    RadMessageBox.Show(this, "No se guardo.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
            }
            base.TSaveButton_Click(sender, e);
        }

        protected override void TVerificar() {
            this.Response = Aplication.Comprobante.Services.CertificadoService.Verificar(this.Certificado.PathCer.Text, this.Certificado.PathKey.Text, this.Certificado.Password.Text);
            this.Guardar.Enabled = this.Response.IsOk;
        }

        protected override void Salveable() {
            this.Service.Save(this.Response.Certificado);
        }
    }
}
