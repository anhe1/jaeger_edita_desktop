﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class ComprobanteFiscalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteFiscalForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.GridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.GridACuentaTercero = new Telerik.WinControls.UI.GridViewTemplate();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.TabConceptoParte = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoParte = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoParte = new Telerik.WinControls.UI.RadGridView();
            this.PageConceptoImpuesto = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoImpuestos = new Telerik.WinControls.UI.RadGridView();
            this.TImpuestos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PageConceptoAduana = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoInformacionAduanera = new Telerik.WinControls.UI.RadGridView();
            this.TAduana = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PageConceptoCtaPredial = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridCtaPredial = new Telerik.WinControls.UI.RadGridView();
            this.TPredial = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PanelTotales = new Telerik.WinControls.UI.RadPanel();
            this.Total = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Descuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionIva = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SubTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.TrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.OnStart = new System.ComponentModel.BackgroundWorker();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptos = new Telerik.WinControls.UI.RadPageViewPage();
            this.TConceptos = new Jaeger.UI.Comprobante.Forms.TbConceptoControl();
            this.ToolBarConceptoParte = new Jaeger.UI.Comprobante.Forms.TbConceptoControl();
            this.ComprobanteControl = new Jaeger.UI.Comprobante.Forms.ComprobanteFiscalControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridACuentaTercero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).BeginInit();
            this.TabConceptoParte.SuspendLayout();
            this.PageConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).BeginInit();
            this.PageConceptoImpuesto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos.MasterTemplate)).BeginInit();
            this.PageConceptoAduana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera.MasterTemplate)).BeginInit();
            this.PageConceptoCtaPredial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtaPredial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtaPredial.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).BeginInit();
            this.PanelTotales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.PageConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1193, 451);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.GridConceptos);
            this.splitPanel1.Controls.Add(this.TConceptos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1193, 282);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1226415F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 59);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // GridConceptos
            // 
            this.GridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptos.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptos.MasterTemplate.AllowDeleteRow = false;
            this.GridConceptos.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "SubId";
            gridViewTextBoxColumn2.HeaderText = "SubId";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "SubId";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "Activo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject1.TValue1 = "False";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "NumPedido";
            gridViewTextBoxColumn3.HeaderText = "# Orden";
            gridViewTextBoxColumn3.Name = "NumPedido";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 60;
            gridViewTextBoxColumn4.DataType = typeof(decimal);
            gridViewTextBoxColumn4.FieldName = "Cantidad";
            gridViewTextBoxColumn4.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn4.HeaderText = "Cantidad";
            gridViewTextBoxColumn4.Name = "Cantidad";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "Unidad";
            gridViewTextBoxColumn5.HeaderText = "Unidad";
            gridViewTextBoxColumn5.Name = "Unidad";
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn6.HeaderText = "Clv. Unidad";
            gridViewTextBoxColumn6.Name = "ClaveUnidad";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn7.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn7.HeaderText = "No. Ident.";
            gridViewTextBoxColumn7.Name = "NoIdentificacion";
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn8.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn8.Name = "ClaveProducto";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 80;
            gridViewMultiComboBoxColumn1.DisplayMember = "Id";
            gridViewMultiComboBoxColumn1.FieldName = "ObjetoImp";
            gridViewMultiComboBoxColumn1.HeaderText = "Obj. Imp.";
            gridViewMultiComboBoxColumn1.Name = "ObjetoImp";
            gridViewMultiComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewMultiComboBoxColumn1.ValueMember = "Id";
            gridViewTextBoxColumn9.FieldName = "Descripcion";
            gridViewTextBoxColumn9.HeaderText = "Concepto";
            gridViewTextBoxColumn9.Name = "Descripcion";
            gridViewTextBoxColumn9.Width = 325;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "ValorUnitario";
            gridViewTextBoxColumn10.FormatString = "{0:N4}";
            gridViewTextBoxColumn10.HeaderText = "Unitario";
            gridViewTextBoxColumn10.Name = "ValorUnitario";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.EnableExpressionEditor = false;
            gridViewTextBoxColumn11.Expression = "";
            gridViewTextBoxColumn11.FieldName = "Importe";
            gridViewTextBoxColumn11.FormatString = "{0:N2}";
            gridViewTextBoxColumn11.HeaderText = "Importe";
            gridViewTextBoxColumn11.Name = "Importe";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 80;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "Descuento";
            gridViewTextBoxColumn12.FormatString = "{0:N2}";
            gridViewTextBoxColumn12.HeaderText = "Descuento";
            gridViewTextBoxColumn12.Name = "Descuento";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 80;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "SubTotal";
            gridViewTextBoxColumn13.FormatString = "{0:N2}";
            gridViewTextBoxColumn13.HeaderText = "Sub Total";
            gridViewTextBoxColumn13.Name = "SubTotal";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn14.FieldName = "CtaPredial";
            gridViewTextBoxColumn14.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn14.Name = "CtaPredial";
            gridViewTextBoxColumn14.Width = 80;
            this.GridConceptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.GridConceptos.MasterTemplate.EnableGrouping = false;
            this.GridConceptos.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridACuentaTercero});
            this.GridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridConceptos.Name = "GridConceptos";
            this.GridConceptos.Size = new System.Drawing.Size(1193, 252);
            this.GridConceptos.TabIndex = 194;
            this.GridConceptos.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridConceptos_CellBeginEdit);
            this.GridConceptos.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridConceptos_ContextMenuOpening);
            this.GridConceptos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridConceptos_KeyDown);
            // 
            // GridACuentaTercero
            // 
            this.GridACuentaTercero.Caption = "Complento A Cuenta Tercero";
            gridViewTextBoxColumn15.FieldName = "RfcACuentaTerceros";
            gridViewTextBoxColumn15.HeaderText = "RFC";
            gridViewTextBoxColumn15.Name = "RfcACuentaTerceros";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn15.Width = 110;
            gridViewTextBoxColumn16.FieldName = "NombreACuentaTerceros";
            gridViewTextBoxColumn16.HeaderText = "Denominación ó Razón Social";
            gridViewTextBoxColumn16.Name = "NombreACuentaTerceros";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.Width = 220;
            gridViewTextBoxColumn17.FieldName = "RegimenFiscalACuentaTerceros";
            gridViewTextBoxColumn17.HeaderText = "Rég. Fiscal";
            gridViewTextBoxColumn17.Name = "RegimenFiscalACuentaTerceros";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.FieldName = "DomicilioFiscalACuentaTerceros";
            gridViewTextBoxColumn18.HeaderText = "Dom. Fiscal";
            gridViewTextBoxColumn18.Name = "DomicilioFiscalACuentaTerceros";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn18.Width = 85;
            this.GridACuentaTercero.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.GridACuentaTercero.ViewDefinition = tableViewDefinition1;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.TabConceptoParte);
            this.splitPanel2.Controls.Add(this.PanelTotales);
            this.splitPanel2.Location = new System.Drawing.Point(0, 286);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1193, 165);
            this.splitPanel2.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 165);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1226415F);
            this.splitPanel2.SizeInfo.MaximumSize = new System.Drawing.Size(0, 165);
            this.splitPanel2.SizeInfo.MinimumSize = new System.Drawing.Size(0, 165);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -59);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // TabConceptoParte
            // 
            this.TabConceptoParte.Controls.Add(this.PageConceptoParte);
            this.TabConceptoParte.Controls.Add(this.PageConceptoImpuesto);
            this.TabConceptoParte.Controls.Add(this.PageConceptoAduana);
            this.TabConceptoParte.Controls.Add(this.PageConceptoCtaPredial);
            this.TabConceptoParte.DefaultPage = this.PageConceptoParte;
            this.TabConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.TabConceptoParte.Name = "TabConceptoParte";
            this.TabConceptoParte.SelectedPage = this.PageConceptoParte;
            this.TabConceptoParte.Size = new System.Drawing.Size(961, 165);
            this.TabConceptoParte.TabIndex = 169;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemCloseButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            // 
            // PageConceptoParte
            // 
            this.PageConceptoParte.Controls.Add(this.GridConceptoParte);
            this.PageConceptoParte.Controls.Add(this.ToolBarConceptoParte);
            this.PageConceptoParte.ItemSize = new System.Drawing.SizeF(92F, 24F);
            this.PageConceptoParte.Location = new System.Drawing.Point(7, 29);
            this.PageConceptoParte.Name = "PageConceptoParte";
            this.PageConceptoParte.Size = new System.Drawing.Size(947, 129);
            this.PageConceptoParte.Text = "Concepto: Parte";
            // 
            // GridConceptoParte
            // 
            this.GridConceptoParte.AutoGenerateHierarchy = true;
            this.GridConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoParte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoParte.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptoParte.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "Cantidad";
            gridViewTextBoxColumn19.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn19.HeaderText = "Cantidad";
            gridViewTextBoxColumn19.Name = "Cantidad";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 80;
            gridViewTextBoxColumn20.FieldName = "Unidad";
            gridViewTextBoxColumn20.HeaderText = "Unidad";
            gridViewTextBoxColumn20.Name = "Unidad";
            gridViewTextBoxColumn20.Width = 80;
            gridViewTextBoxColumn21.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn21.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn21.Name = "ClaveProdServ";
            gridViewTextBoxColumn21.Width = 80;
            gridViewTextBoxColumn22.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn22.HeaderText = "No. Ident.";
            gridViewTextBoxColumn22.Name = "NoIdentificacion";
            gridViewTextBoxColumn22.Width = 90;
            gridViewTextBoxColumn23.FieldName = "Descripcion";
            gridViewTextBoxColumn23.HeaderText = "Descripción";
            gridViewTextBoxColumn23.Name = "Descripcion";
            gridViewTextBoxColumn23.Width = 250;
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.FieldName = "ValorUnitario";
            gridViewTextBoxColumn24.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn24.HeaderText = "Valor \n\rUnitario";
            gridViewTextBoxColumn24.Name = "ValorUnitario";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 80;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "Importe";
            gridViewTextBoxColumn25.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn25.HeaderText = "Importe";
            gridViewTextBoxColumn25.Name = "Importe";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 80;
            this.GridConceptoParte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25});
            this.GridConceptoParte.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridConceptoParte.Name = "GridConceptoParte";
            this.GridConceptoParte.ShowGroupPanel = false;
            this.GridConceptoParte.Size = new System.Drawing.Size(947, 99);
            this.GridConceptoParte.TabIndex = 0;
            // 
            // PageConceptoImpuesto
            // 
            this.PageConceptoImpuesto.Controls.Add(this.GridConceptoImpuestos);
            this.PageConceptoImpuesto.Controls.Add(this.TImpuestos);
            this.PageConceptoImpuesto.ItemSize = new System.Drawing.SizeF(118F, 24F);
            this.PageConceptoImpuesto.Location = new System.Drawing.Point(10, 37);
            this.PageConceptoImpuesto.Name = "PageConceptoImpuesto";
            this.PageConceptoImpuesto.Size = new System.Drawing.Size(1114, 137);
            this.PageConceptoImpuesto.Text = "Concepto: Impuestos";
            // 
            // GridConceptoImpuestos
            // 
            this.GridConceptoImpuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoImpuestos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoImpuestos.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Impuesto";
            gridViewComboBoxColumn2.HeaderText = "Impuesto";
            gridViewComboBoxColumn2.Name = "Impuesto";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "Base";
            gridViewTextBoxColumn26.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn26.HeaderText = "Base";
            gridViewTextBoxColumn26.Name = "Base";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 80;
            gridViewComboBoxColumn3.FieldName = "TipoFactor";
            gridViewComboBoxColumn3.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn3.Name = "TipoFactor";
            gridViewComboBoxColumn3.Width = 80;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "TasaOCuota";
            gridViewTextBoxColumn27.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn27.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn27.Name = "TasaOCuota";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 80;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "Importe";
            gridViewTextBoxColumn28.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn28.HeaderText = "Importe";
            gridViewTextBoxColumn28.Name = "Importe";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 80;
            this.GridConceptoImpuestos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn26,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28});
            this.GridConceptoImpuestos.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.GridConceptoImpuestos.Name = "GridConceptoImpuestos";
            this.GridConceptoImpuestos.ShowGroupPanel = false;
            this.GridConceptoImpuestos.Size = new System.Drawing.Size(1114, 107);
            this.GridConceptoImpuestos.TabIndex = 1;
            // 
            // TImpuestos
            // 
            this.TImpuestos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuestos.Etiqueta = "Impuestos";
            this.TImpuestos.Location = new System.Drawing.Point(0, 0);
            this.TImpuestos.Name = "TImpuestos";
            this.TImpuestos.ReadOnly = false;
            this.TImpuestos.ShowActualizar = false;
            this.TImpuestos.ShowAutorizar = false;
            this.TImpuestos.ShowCerrar = false;
            this.TImpuestos.ShowEditar = false;
            this.TImpuestos.ShowExportarExcel = false;
            this.TImpuestos.ShowFiltro = false;
            this.TImpuestos.ShowGuardar = false;
            this.TImpuestos.ShowHerramientas = false;
            this.TImpuestos.ShowImagen = false;
            this.TImpuestos.ShowImprimir = false;
            this.TImpuestos.ShowNuevo = true;
            this.TImpuestos.ShowRemover = true;
            this.TImpuestos.Size = new System.Drawing.Size(1114, 30);
            this.TImpuestos.TabIndex = 7;
            // 
            // PageConceptoAduana
            // 
            this.PageConceptoAduana.Controls.Add(this.GridConceptoInformacionAduanera);
            this.PageConceptoAduana.Controls.Add(this.TAduana);
            this.PageConceptoAduana.ItemSize = new System.Drawing.SizeF(104F, 24F);
            this.PageConceptoAduana.Location = new System.Drawing.Point(7, 33);
            this.PageConceptoAduana.Name = "PageConceptoAduana";
            this.PageConceptoAduana.Size = new System.Drawing.Size(1105, 125);
            this.PageConceptoAduana.Text = "Concepto: Aduana";
            // 
            // GridConceptoInformacionAduanera
            // 
            this.GridConceptoInformacionAduanera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoInformacionAduanera.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoInformacionAduanera.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptoInformacionAduanera.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn29.FieldName = "NumeroPedimento";
            gridViewTextBoxColumn29.HeaderText = "Número de Pedimento";
            gridViewTextBoxColumn29.Name = "NumeroPedimento";
            gridViewTextBoxColumn29.Width = 150;
            this.GridConceptoInformacionAduanera.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn29});
            this.GridConceptoInformacionAduanera.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.GridConceptoInformacionAduanera.Name = "GridConceptoInformacionAduanera";
            this.GridConceptoInformacionAduanera.ShowGroupPanel = false;
            this.GridConceptoInformacionAduanera.Size = new System.Drawing.Size(1105, 95);
            this.GridConceptoInformacionAduanera.TabIndex = 1;
            // 
            // TAduana
            // 
            this.TAduana.Dock = System.Windows.Forms.DockStyle.Top;
            this.TAduana.Etiqueta = "Aduana:";
            this.TAduana.Location = new System.Drawing.Point(0, 0);
            this.TAduana.Name = "TAduana";
            this.TAduana.ReadOnly = false;
            this.TAduana.ShowActualizar = false;
            this.TAduana.ShowAutorizar = false;
            this.TAduana.ShowCerrar = false;
            this.TAduana.ShowEditar = false;
            this.TAduana.ShowExportarExcel = false;
            this.TAduana.ShowFiltro = false;
            this.TAduana.ShowGuardar = false;
            this.TAduana.ShowHerramientas = false;
            this.TAduana.ShowImagen = false;
            this.TAduana.ShowImprimir = false;
            this.TAduana.ShowNuevo = true;
            this.TAduana.ShowRemover = true;
            this.TAduana.Size = new System.Drawing.Size(1105, 30);
            this.TAduana.TabIndex = 7;
            // 
            // PageConceptoCtaPredial
            // 
            this.PageConceptoCtaPredial.Controls.Add(this.gridCtaPredial);
            this.PageConceptoCtaPredial.Controls.Add(this.TPredial);
            this.PageConceptoCtaPredial.ItemSize = new System.Drawing.SizeF(139F, 24F);
            this.PageConceptoCtaPredial.Location = new System.Drawing.Point(7, 33);
            this.PageConceptoCtaPredial.Name = "PageConceptoCtaPredial";
            this.PageConceptoCtaPredial.Size = new System.Drawing.Size(1105, 125);
            this.PageConceptoCtaPredial.Text = "Concepto: Cuenta Predial";
            // 
            // gridCtaPredial
            // 
            this.gridCtaPredial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCtaPredial.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridCtaPredial.MasterTemplate.AllowAddNewRow = false;
            this.gridCtaPredial.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn30.FieldName = "Numero";
            gridViewTextBoxColumn30.HeaderText = "Número de Predial";
            gridViewTextBoxColumn30.Name = "Numero";
            gridViewTextBoxColumn30.Width = 150;
            this.gridCtaPredial.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn30});
            this.gridCtaPredial.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.gridCtaPredial.Name = "gridCtaPredial";
            this.gridCtaPredial.ShowGroupPanel = false;
            this.gridCtaPredial.Size = new System.Drawing.Size(1105, 95);
            this.gridCtaPredial.TabIndex = 8;
            // 
            // TPredial
            // 
            this.TPredial.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPredial.Etiqueta = "Cuenta Predial:";
            this.TPredial.Location = new System.Drawing.Point(0, 0);
            this.TPredial.Name = "TPredial";
            this.TPredial.ReadOnly = false;
            this.TPredial.ShowActualizar = false;
            this.TPredial.ShowAutorizar = false;
            this.TPredial.ShowCerrar = false;
            this.TPredial.ShowEditar = false;
            this.TPredial.ShowExportarExcel = false;
            this.TPredial.ShowFiltro = false;
            this.TPredial.ShowGuardar = false;
            this.TPredial.ShowHerramientas = false;
            this.TPredial.ShowImagen = false;
            this.TPredial.ShowImprimir = false;
            this.TPredial.ShowNuevo = true;
            this.TPredial.ShowRemover = true;
            this.TPredial.Size = new System.Drawing.Size(1105, 30);
            this.TPredial.TabIndex = 9;
            // 
            // PanelTotales
            // 
            this.PanelTotales.Controls.Add(this.Total);
            this.PanelTotales.Controls.Add(this.RetencionISR);
            this.PanelTotales.Controls.Add(this.Descuento);
            this.PanelTotales.Controls.Add(this.RetencionIva);
            this.PanelTotales.Controls.Add(this.SubTotal);
            this.PanelTotales.Controls.Add(this.TrasladoIEPS);
            this.PanelTotales.Controls.Add(this.RadLabel28);
            this.PanelTotales.Controls.Add(this.TrasladoIVA);
            this.PanelTotales.Controls.Add(this.RadLabel31);
            this.PanelTotales.Controls.Add(this.RadLabel24);
            this.PanelTotales.Controls.Add(this.RadLabel27);
            this.PanelTotales.Controls.Add(this.RadLabel30);
            this.PanelTotales.Controls.Add(this.RadLabel26);
            this.PanelTotales.Controls.Add(this.RadLabel29);
            this.PanelTotales.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotales.Location = new System.Drawing.Point(961, 0);
            this.PanelTotales.Name = "PanelTotales";
            this.PanelTotales.Size = new System.Drawing.Size(232, 165);
            this.PanelTotales.TabIndex = 170;
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(97, 134);
            this.Total.Mask = "n4";
            this.Total.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Total.Name = "Total";
            this.Total.NullText = "SubTotal";
            this.Total.Size = new System.Drawing.Size(125, 20);
            this.Total.TabIndex = 7;
            this.Total.TabStop = false;
            this.Total.Text = "0.0000";
            this.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionISR
            // 
            this.RetencionISR.Location = new System.Drawing.Point(97, 113);
            this.RetencionISR.Mask = "n4";
            this.RetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.NullText = "SubTotal";
            this.RetencionISR.Size = new System.Drawing.Size(125, 20);
            this.RetencionISR.TabIndex = 6;
            this.RetencionISR.TabStop = false;
            this.RetencionISR.Text = "0.0000";
            this.RetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Descuento
            // 
            this.Descuento.Location = new System.Drawing.Point(97, 29);
            this.Descuento.Mask = "n4";
            this.Descuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Descuento.Name = "Descuento";
            this.Descuento.NullText = "SubTotal";
            this.Descuento.Size = new System.Drawing.Size(125, 20);
            this.Descuento.TabIndex = 2;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "0.0000";
            this.Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionIva
            // 
            this.RetencionIva.Location = new System.Drawing.Point(97, 92);
            this.RetencionIva.Mask = "n4";
            this.RetencionIva.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionIva.Name = "RetencionIva";
            this.RetencionIva.NullText = "SubTotal";
            this.RetencionIva.Size = new System.Drawing.Size(125, 20);
            this.RetencionIva.TabIndex = 5;
            this.RetencionIva.TabStop = false;
            this.RetencionIva.Text = "0.0000";
            this.RetencionIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SubTotal
            // 
            this.SubTotal.Location = new System.Drawing.Point(97, 8);
            this.SubTotal.Mask = "n4";
            this.SubTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.NullText = "SubTotal";
            this.SubTotal.Size = new System.Drawing.Size(125, 20);
            this.SubTotal.TabIndex = 1;
            this.SubTotal.TabStop = false;
            this.SubTotal.Text = "0.0000";
            this.SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.Location = new System.Drawing.Point(97, 71);
            this.TrasladoIEPS.Mask = "n4";
            this.TrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.NullText = "SubTotal";
            this.TrasladoIEPS.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIEPS.TabIndex = 4;
            this.TrasladoIEPS.TabStop = false;
            this.TrasladoIEPS.Text = "0.0000";
            this.TrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel28
            // 
            this.RadLabel28.Location = new System.Drawing.Point(16, 9);
            this.RadLabel28.Name = "RadLabel28";
            this.RadLabel28.Size = new System.Drawing.Size(50, 18);
            this.RadLabel28.TabIndex = 179;
            this.RadLabel28.Text = "SubTotal";
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.Location = new System.Drawing.Point(97, 50);
            this.TrasladoIVA.Mask = "n4";
            this.TrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.NullText = "SubTotal";
            this.TrasladoIVA.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIVA.TabIndex = 3;
            this.TrasladoIVA.TabStop = false;
            this.TrasladoIVA.Text = "0.0000";
            this.TrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel31
            // 
            this.RadLabel31.Location = new System.Drawing.Point(16, 135);
            this.RadLabel31.Name = "RadLabel31";
            this.RadLabel31.Size = new System.Drawing.Size(31, 18);
            this.RadLabel31.TabIndex = 182;
            this.RadLabel31.Text = "Total";
            // 
            // RadLabel24
            // 
            this.RadLabel24.Location = new System.Drawing.Point(16, 30);
            this.RadLabel24.Name = "RadLabel24";
            this.RadLabel24.Size = new System.Drawing.Size(59, 18);
            this.RadLabel24.TabIndex = 176;
            this.RadLabel24.Text = "Descuento";
            // 
            // RadLabel27
            // 
            this.RadLabel27.Location = new System.Drawing.Point(16, 72);
            this.RadLabel27.Name = "RadLabel27";
            this.RadLabel27.Size = new System.Drawing.Size(27, 18);
            this.RadLabel27.TabIndex = 178;
            this.RadLabel27.Text = "IEPS";
            // 
            // RadLabel30
            // 
            this.RadLabel30.Location = new System.Drawing.Point(16, 114);
            this.RadLabel30.Name = "RadLabel30";
            this.RadLabel30.Size = new System.Drawing.Size(75, 18);
            this.RadLabel30.TabIndex = 181;
            this.RadLabel30.Text = "Retención ISR";
            // 
            // RadLabel26
            // 
            this.RadLabel26.Location = new System.Drawing.Point(16, 51);
            this.RadLabel26.Name = "RadLabel26";
            this.RadLabel26.Size = new System.Drawing.Size(24, 18);
            this.RadLabel26.TabIndex = 177;
            this.RadLabel26.Text = "IVA";
            // 
            // RadLabel29
            // 
            this.RadLabel29.Location = new System.Drawing.Point(16, 93);
            this.RadLabel29.Name = "RadLabel29";
            this.RadLabel29.Size = new System.Drawing.Size(77, 18);
            this.RadLabel29.TabIndex = 180;
            this.RadLabel29.Text = "Retención IVA";
            // 
            // OnStart
            // 
            this.OnStart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnStart_DoWork);
            this.OnStart.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnStart_RunWorkerCompleted);
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.PageConceptos);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 180);
            this.TabControl.Margin = new System.Windows.Forms.Padding(1);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.PageConceptos;
            this.TabControl.Size = new System.Drawing.Size(1209, 484);
            this.TabControl.TabIndex = 1;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            // 
            // PageConceptos
            // 
            this.PageConceptos.Controls.Add(this.radSplitContainer1);
            this.PageConceptos.ItemSize = new System.Drawing.SizeF(65F, 24F);
            this.PageConceptos.Location = new System.Drawing.Point(8, 28);
            this.PageConceptos.Name = "PageConceptos";
            this.PageConceptos.Size = new System.Drawing.Size(1193, 451);
            this.PageConceptos.Text = "Conceptos";
            // 
            // TConceptos
            // 
            this.TConceptos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptos.IsEditable = true;
            this.TConceptos.Location = new System.Drawing.Point(0, 0);
            this.TConceptos.Margin = new System.Windows.Forms.Padding(1);
            this.TConceptos.Name = "TConceptos";
            this.TConceptos.Size = new System.Drawing.Size(1193, 30);
            this.TConceptos.TabIndex = 195;
            // 
            // ToolBarConceptoParte
            // 
            this.ToolBarConceptoParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBarConceptoParte.IsEditable = true;
            this.ToolBarConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.ToolBarConceptoParte.Name = "ToolBarConceptoParte";
            this.ToolBarConceptoParte.Size = new System.Drawing.Size(947, 30);
            this.ToolBarConceptoParte.TabIndex = 1;
            // 
            // ComprobanteControl
            // 
            this.ComprobanteControl.Comprobante = null;
            this.ComprobanteControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ComprobanteControl.Location = new System.Drawing.Point(0, 0);
            this.ComprobanteControl.MinimumSize = new System.Drawing.Size(1150, 180);
            this.ComprobanteControl.Name = "ComprobanteControl";
            this.ComprobanteControl.Size = new System.Drawing.Size(1209, 180);
            this.ComprobanteControl.TabIndex = 0;
            this.ComprobanteControl.BindingCompleted += new System.EventHandler<System.EventArgs>(this.Comprobante_BindingCompleted);
            // 
            // ComprobanteFiscalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1209, 664);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.ComprobanteControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobanteFiscalForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CFDI - ";
            this.Load += new System.EventHandler(this.ComprobanteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridACuentaTercero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).EndInit();
            this.TabConceptoParte.ResumeLayout(false);
            this.PageConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).EndInit();
            this.PageConceptoImpuesto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoImpuestos)).EndInit();
            this.PageConceptoAduana.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoInformacionAduanera)).EndInit();
            this.PageConceptoCtaPredial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCtaPredial.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCtaPredial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            this.PanelTotales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.PageConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        internal Telerik.WinControls.UI.RadPageView TabConceptoParte;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoParte;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoImpuesto;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoAduana;
        private Telerik.WinControls.UI.RadPanel PanelTotales;
        private Telerik.WinControls.UI.RadMaskedEditBox Total;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox Descuento;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionIva;
        private Telerik.WinControls.UI.RadMaskedEditBox SubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIEPS;
        internal Telerik.WinControls.UI.RadLabel RadLabel28;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIVA;
        internal Telerik.WinControls.UI.RadLabel RadLabel31;
        internal Telerik.WinControls.UI.RadLabel RadLabel24;
        internal Telerik.WinControls.UI.RadLabel RadLabel27;
        internal Telerik.WinControls.UI.RadLabel RadLabel30;
        internal Telerik.WinControls.UI.RadLabel RadLabel26;
        internal Telerik.WinControls.UI.RadLabel RadLabel29;
        protected internal TbConceptoControl ToolBarConceptoParte;
        protected internal TbConceptoControl TConceptos;
        protected internal ComprobanteFiscalControl ComprobanteControl;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptos;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptoParte;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptoImpuestos;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptoInformacionAduanera;
        protected internal Common.Forms.ToolBarStandarControl TImpuestos;
        protected internal Common.Forms.ToolBarStandarControl TAduana;
        protected internal Telerik.WinControls.UI.GridViewTemplate GridACuentaTercero;
        protected internal System.ComponentModel.BackgroundWorker OnStart;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageConceptos;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageConceptoCtaPredial;
        protected internal Telerik.WinControls.UI.RadGridView gridCtaPredial;
        protected internal Common.Forms.ToolBarStandarControl TPredial;
    }
}
