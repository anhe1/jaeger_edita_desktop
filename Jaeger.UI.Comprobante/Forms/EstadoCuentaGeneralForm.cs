﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class EstadoCuentaGeneralForm : RadForm {
        protected internal BindingList<EstadoCuenta> _DataSource;
        public EstadoCuentaGeneralForm() {
            InitializeComponent();
        }

        protected virtual void EstadoCuentaGeneralForm_Load(object sender, EventArgs e) {
            this.TComprobante.TipoComprobante = Domain.Base.ValueObjects.CFDISubTipoEnum.Recibido;
            this.TComprobante.Templete = Builder.ComprobanteFiscalGridBuilder.TempleteEnum.EdoCuenta;
            this.TComprobante.ShowNuevo = false;
            this.TComprobante.ShowEditar = false;
            this.TComprobante.ShowAutosuma = true;
            this.TComprobante.ShowExportarExcel = true;
            this.TComprobante.ShowHerramientas = true;
            this.TComprobante.ShowImprimir = true;
            this.TComprobante.ExportarExcel.Enabled = true;
            this.TComprobante.Actualizar.Click += TComprobante_Actualizar_Click;
            this.TComprobante.Cerrar.Click += TComprobante_Cerrar_Click;
            this.TComprobante.RowsNeeded += TComprobante_RowsNeeded;
        }

        protected virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.ShowDialog(this);
            }
            this.TComprobante.GridData.DataSource = this._DataSource;
        }

        protected virtual void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void TComprobante_RowsNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (this.TComprobante.GridData.CurrentRow is null)
                return;
            if (e.Template.Caption == this.TComprobante.Comprobantes.Caption) {
                this.GetComprobantes();
                var d1 = this.TComprobante.GridData.CurrentRow.DataBoundItem as EstadoCuenta;
                if (d1 != null) {
                    if (d1.Tag != null) {
                        var d2 = (List<ComprobanteFiscalDetailSingleModel>)d1.Tag;
                        foreach (var item in d2) {
                            var newRow = e.Template.Rows.NewRow();
                            newRow.Cells["IdComprobante"].Value = item.Id;
                            newRow.Cells["Status"].Value = item.Status;
                            newRow.Cells["Version"].Value = item.Version;
                            newRow.Cells["Tipocomprobante"].Value = item.TipoComprobante;
                            newRow.Cells["Folio"].Value = item.Folio;
                            newRow.Cells["Serie"].Value = item.Serie;
                            newRow.Cells["EmisorRFC"].Value = item.EmisorRFC;
                            newRow.Cells["EmisorNombre"].Value = item.EmisorNombre;
                            newRow.Cells["ReceptorRFC"].Value = item.ReceptorRFC;
                            newRow.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
                            newRow.Cells["IdDocumento"].Value = item.IdDocumento;
                            newRow.Cells["TipoComprobanteText"].Value = item.TipoComprobanteText;
                            newRow.Cells["FechaEmision"].Value = item.FechaEmision;
                            newRow.Cells["FechaCancela"].Value = item.FechaCancela;
                            newRow.Cells["FechaUltimoPago"].Value = item.FechaUltimoPago;
                            newRow.Cells["Total"].Value = item.Total;
                            newRow.Cells["Acumulado"].Value = item.Acumulado;
                            newRow.Cells["Saldo"].Value = item.Saldo;
                            newRow.Cells["Estado"].Value = item.Estado;
                            newRow.Cells["UrlFileXml"].Value = item.UrlFileXML;
                            newRow.Cells["UrlFilePdf"].Value = item.UrlFilePDF;
                            newRow.Cells["Moneda"].Value = item.ClaveMoneda;
                            newRow.Cells["DiasTranscurridos"].Value = item.DiasTranscurridos;
                            var d0 = newRow.DataBoundItem;
                            d0 = item;
                            e.SourceCollection.Add(newRow);
                        }
                    }
                }
            }
        }

        protected virtual void Actualizar() {
            this._DataSource = new BindingList<EstadoCuenta>(
                this.TComprobante._Service.GetList<EstadoCuenta>(
                   Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Recibido().WithStatus(Domain.Comprobante.ValueObjects.CFDStatusEgresoEnum.PorPagar).WithYear(this.TComprobante.GetEjercicio()).Build()).ToList<EstadoCuenta>()
                );
        }

        protected virtual void GetComprobantes() {
            var selected = this.TComprobante.GridData.CurrentRow.DataBoundItem as EstadoCuenta;
            if (selected != null) {
                var query = Aplication.Comprobante.Services.ComprobantesFiscalesService.Query().Recibido().WithStatus(Domain.Comprobante.ValueObjects.CFDStatusEgresoEnum.PorPagar).WithYear(this.TComprobante.GetEjercicio()).WithIdDirectorio(selected.IdDirectorio).Build();
                var data = this.TComprobante._Service.GetList<ComprobanteFiscalDetailSingleModel>(query);
                selected.Tag = data.ToList();
            }
        }
    }
}