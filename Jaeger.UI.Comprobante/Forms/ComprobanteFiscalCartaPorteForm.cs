﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Comprobante.Forms {
    public class ComprobanteFiscalCartaPorteForm : ComprobanteFiscalForm {
        private readonly ComplementoCartaPorteControl Complemento = new ComplementoCartaPorteControl() { Dock = DockStyle.Fill };

        public ComprobanteFiscalCartaPorteForm(CFDISubTipoEnum subtTipo, int indice) : base(subtTipo, indice) {
            this.Load += this.ComprobanteFiscalCartaPorteForm_Load;
        }

        public ComprobanteFiscalCartaPorteForm(ComprobanteFiscalDetailModel comprobante, CFDISubTipoEnum subtTipo, int indice) : base(comprobante, subtTipo, indice) {
            this.Load += this.ComprobanteFiscalCartaPorteForm_Load;
        }

        private void ComprobanteFiscalCartaPorteForm_Load(object sender, EventArgs e) {
            this.ComprobanteControl.TipoComprobante = Domain.Comprobante.ValueObjects.CFDITipoComprobanteEnum.Ingreso;
            this.Complemento.Start();
            this.ComplementoAgregar(this.Complemento);
            this.ComprobanteControl.BindingCompleted += this.General_BindingCompleted;
            this.ComprobanteControl.TabControl.Controls.Remove(this.ComprobanteControl.PageInformacionGeneral);
        }

        public override void TComprobante_Guardar_Click(object sender, EventArgs e) {
            if (this.Complemento.Validar() == true) {
                base.TComprobante_Guardar_Click(sender, e);
            } else {
                RadMessageBox.Show(this, Properties.Resources.Message_Comprobante_ErrorCaptura, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void General_BindingCompleted(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.Id == 0) {
                if (this.ComprobanteControl.Comprobante.CartaPorte == null) {
                    this.ComprobanteControl.Comprobante.CartaPorte = new Domain.Comprobante.Entities.Complemento.CartaPorte.CartaPorteDetailModel();
                }
            }

            this.Complemento.Complemento = this.ComprobanteControl.Comprobante.CartaPorte;
            this.Complemento.CreateBinding();
            this.Complemento.Editable = this.ComprobanteControl.Comprobante.IsEditable;
        }
    }
}
