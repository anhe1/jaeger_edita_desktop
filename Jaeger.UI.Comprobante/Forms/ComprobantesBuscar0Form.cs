﻿using System;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComprobantesBuscar0Form : RadForm {
        protected IComprobantesFiscalesSearchService service;
        protected Domain.Base.ValueObjects.CFDISubTipoEnum subTipo;
        private BindingList<ComprobanteFiscalDetailSingleModel> datos;
        private string rfc;

        public void OnAgregar(ComprobanteFiscalDetailSingleModel e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public event EventHandler<ComprobanteFiscalDetailSingleModel> AgregarComprobante;

        public ComprobantesBuscar0Form(string rfc, Domain.Base.ValueObjects.CFDISubTipoEnum subTipo) {
            InitializeComponent();
            this.rfc = rfc;
            this.subTipo = subTipo;
        }

        private void ComprobantesBuscarForm_Load(object sender, EventArgs e) {
            this.ToolBarButtonActualizar.Text = "Buscar";
            this.service = new ComprobanteFiscalSearchService(this.subTipo);
        }

        private void ToolBarButtonAgregar_Click(object sender, EventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                var seleccionado = this.gridSearchResult.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (seleccionado != null) {
                    this.OnAgregar(seleccionado);
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            if (this.workerBuscar.IsBusy == false) {
                this.radWaitingBar1.StartWaiting();
                this.workerBuscar.RunWorkerAsync();
            }
            this.gridSearchResult.DataSource = this.datos;
        }

        private void gridSearchResult_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                this.ToolBarButtonAgregar.PerformClick();
                this.Close();
            }
        }

        private void workerBuscar_DoWork(object sender, DoWorkEventArgs e) {
            this.datos = this.service.GetSearch(this.rfc, this.Folio.Text);
            e.Result = true;
        }

        private void workerBuscar_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            
        }

        private void workerBuscar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.gridSearchResult.DataSource = this.datos;
            this.radWaitingBar1.StopWaiting();
        }
    }
}
