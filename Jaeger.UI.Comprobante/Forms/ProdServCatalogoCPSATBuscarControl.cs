﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ProdServCatalogoCPSATBuscarControl : UserControl {
        protected ICveProdServCPCatalogo catalogo;
        private string descripcion;

        public event EventHandler<ComprobanteConceptoDetailModel> Selected;

        public void OnSelected(ComprobanteConceptoDetailModel e) {
            if (this.Selected != null) {
                this.Selected(this, e);
            }
        }
        public ProdServCatalogoCPSATBuscarControl() {
            InitializeComponent();
        }

        private void ProdServCatalogoSATBuscarControl_Load(object sender, EventArgs e) {
            this.gridProductoSAT.Standard();
        }

        public void SetService() {
            this.catalogo = new CveProdServCPCatalogo();
            if (this.WorkerService.IsBusy == false) {
                this.WorkerService.RunWorkerAsync();
            }
        }

        public virtual void Search(string descripcion) {
            this.descripcion = descripcion;
            this.gridProductoSAT.DataSource = this.catalogo.Productos(this.descripcion);
        }

        public virtual ComprobanteConceptoDetailModel GetSelected() {
            if (this.gridProductoSAT.CurrentRow != null) {
                var s = this.gridProductoSAT.CurrentRow.DataBoundItem as CveProdServCP;
                if (s != null) {
                    var seleccionado = new ComprobanteConceptoDetailModel {
                        Activo = true,
                        Cantidad = 0,
                        ClaveProdServ = s.Clave,
                        Descripcion = s.Descripcion
                    };
                    return seleccionado;
                }
            }
            return null;
        }

        private void WorkerService_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e) {
            this.catalogo.Load();
        }

        private void gridProductoSAT_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (this.gridProductoSAT.CurrentRow != null) {
                var seleccionado = this.GetSelected();
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }
    }
}
