﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobantesFiscalesResumen2 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.RazonSocial = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ChartView = new Telerik.WinControls.UI.RadChartView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.TReporte = new Jaeger.UI.Common.Forms.TBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartView)).BeginInit();
            this.ChartView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "RFC";
            gridViewTextBoxColumn1.HeaderText = "RFC";
            gridViewTextBoxColumn1.Name = "RFC";
            gridViewTextBoxColumn1.Width = 90;
            gridViewTextBoxColumn2.FieldName = "Mes";
            gridViewTextBoxColumn2.HeaderText = "Mes";
            gridViewTextBoxColumn2.Name = "Mes";
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "DiasCobro";
            gridViewTextBoxColumn3.FormatString = "{0:n}";
            gridViewTextBoxColumn3.HeaderText = "Días Cobro";
            gridViewTextBoxColumn3.Name = "DiasCobro";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.FieldName = "TotalComprobantes";
            gridViewTextBoxColumn4.FormatString = "{0:n}";
            gridViewTextBoxColumn4.HeaderText = "Comp.";
            gridViewTextBoxColumn4.Name = "TotalComprobantes";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.DataType = typeof(decimal);
            gridViewTextBoxColumn5.FieldName = "TotalFacturado";
            gridViewTextBoxColumn5.FormatString = "{0:n}";
            gridViewTextBoxColumn5.HeaderText = "T. Facturado";
            gridViewTextBoxColumn5.Name = "TotalFacturado";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.DataType = typeof(decimal);
            gridViewTextBoxColumn6.FieldName = "Ingreso";
            gridViewTextBoxColumn6.FormatString = "{0:N2}";
            gridViewTextBoxColumn6.HeaderText = "Facturado";
            gridViewTextBoxColumn6.Name = "Ingreso";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Egreso";
            gridViewTextBoxColumn7.FormatString = "{0:N2}";
            gridViewTextBoxColumn7.HeaderText = "N. Crédito";
            gridViewTextBoxColumn7.Name = "Egreso";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "Acumulado";
            gridViewTextBoxColumn8.FormatString = "{0:n}";
            gridViewTextBoxColumn8.HeaderText = "Cobrado";
            gridViewTextBoxColumn8.Name = "Acumulado";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Saldo";
            gridViewTextBoxColumn9.FormatString = "{0:n}";
            gridViewTextBoxColumn9.HeaderText = "Saldo";
            gridViewTextBoxColumn9.Name = "Saldo";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "PromedioFacturado";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Prom. Facturado";
            gridViewTextBoxColumn10.Name = "PromedioFacturado";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "PromedioCobrado";
            gridViewTextBoxColumn11.FormatString = "{0:n}";
            gridViewTextBoxColumn11.HeaderText = "Prom. Cobrado";
            gridViewTextBoxColumn11.Name = "PromedioCobrado";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 85;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1161, 307);
            this.GridData.TabIndex = 1;
            // 
            // RazonSocial
            // 
            this.RazonSocial.AutoSizeDropDownHeight = true;
            this.RazonSocial.DisplayMember = "Nombre";
            // 
            // RazonSocial.NestedRadGridView
            // 
            this.RazonSocial.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.RazonSocial.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RazonSocial.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RazonSocial.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RazonSocial.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.RazonSocial.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.RazonSocial.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.RazonSocial.EditorControl.MasterTemplate.AllowSearchRow = true;
            gridViewTextBoxColumn12.FieldName = "Id";
            gridViewTextBoxColumn12.HeaderText = "Id";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Id";
            gridViewTextBoxColumn13.FieldName = "Nombre";
            gridViewTextBoxColumn13.HeaderText = "Nombre";
            gridViewTextBoxColumn13.Name = "Nombre";
            gridViewTextBoxColumn13.Width = 220;
            gridViewTextBoxColumn14.FieldName = "RFC";
            gridViewTextBoxColumn14.HeaderText = "RFC";
            gridViewTextBoxColumn14.Name = "RFC";
            gridViewTextBoxColumn14.Width = 80;
            this.RazonSocial.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.RazonSocial.EditorControl.MasterTemplate.EnableGrouping = false;
            this.RazonSocial.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.RazonSocial.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RazonSocial.EditorControl.Name = "NestedRadGridView";
            this.RazonSocial.EditorControl.ReadOnly = true;
            this.RazonSocial.EditorControl.ShowGroupPanel = false;
            this.RazonSocial.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.RazonSocial.EditorControl.TabIndex = 0;
            this.RazonSocial.Location = new System.Drawing.Point(12, 54);
            this.RazonSocial.Name = "RazonSocial";
            this.RazonSocial.NullText = "Nombre o Razon Social del Receptor";
            this.RazonSocial.Size = new System.Drawing.Size(355, 20);
            this.RazonSocial.TabIndex = 150;
            this.RazonSocial.TabStop = false;
            this.RazonSocial.ValueMember = "Id";
            // 
            // ChartView
            // 
            this.ChartView.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.TickOrigin = null;
            this.ChartView.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.ChartView.Controls.Add(this.RazonSocial);
            this.ChartView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartView.Location = new System.Drawing.Point(0, 0);
            this.ChartView.Name = "ChartView";
            this.ChartView.ShowGrid = false;
            this.ChartView.ShowPanZoom = true;
            this.ChartView.ShowToolTip = true;
            this.ChartView.ShowTrackBall = true;
            this.ChartView.Size = new System.Drawing.Size(1161, 308);
            this.ChartView.TabIndex = 2;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1161, 619);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.ChartView);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1161, 308);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.GridData);
            this.splitPanel2.Location = new System.Drawing.Point(0, 312);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1161, 307);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // TReporte
            // 
            this.TReporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TReporte.Location = new System.Drawing.Point(0, 0);
            this.TReporte.Name = "TReporte";
            this.TReporte.ShowActualizar = true;
            this.TReporte.ShowAutosuma = false;
            this.TReporte.ShowCancelar = false;
            this.TReporte.ShowCerrar = true;
            this.TReporte.ShowEditar = true;
            this.TReporte.ShowEjercicio = true;
            this.TReporte.ShowExportarExcel = false;
            this.TReporte.ShowFiltro = true;
            this.TReporte.ShowHerramientas = false;
            this.TReporte.ShowImprimir = false;
            this.TReporte.ShowItem = true;
            this.TReporte.ShowNuevo = true;
            this.TReporte.ShowPeriodo = false;
            this.TReporte.Size = new System.Drawing.Size(1161, 30);
            this.TReporte.TabIndex = 151;
            // 
            // ComprobantesFiscalesResumen2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 649);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TReporte);
            this.Name = "ComprobantesFiscalesResumen2";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Facturación vs";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ViewEstadoCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RazonSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartView)).EndInit();
            this.ChartView.ResumeLayout(false);
            this.ChartView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.RadChartView ChartView;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox RazonSocial;
        private Common.Forms.TBarCommonControl TReporte;
    }
}
