﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class ComprobantesBuscar0Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobantesBuscar0Form));
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelBuscar = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.gridSearchResult = new Telerik.WinControls.UI.RadGridView();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.workerBuscar = new System.ComponentModel.BackgroundWorker();
            this.Folio = new Telerik.WinControls.UI.CommandBarTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult.MasterTemplate)).BeginInit();
            this.gridSearchResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(680, 30);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Herramientas";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelBuscar,
            this.Folio,
            this.commandBarSeparator1,
            this.ToolBarButtonAgregar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // ToolBarLabelBuscar
            // 
            this.ToolBarLabelBuscar.DisplayName = "Etiqueta: Buscar";
            this.ToolBarLabelBuscar.Name = "ToolBarLabelBuscar";
            this.ToolBarLabelBuscar.Text = "Folio:";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.DisplayName = "Agregar";
            this.ToolBarButtonAgregar.DrawText = true;
            this.ToolBarButtonAgregar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.add_16px;
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar";
            this.ToolBarButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonAgregar.Click += new System.EventHandler(this.ToolBarButtonAgregar_Click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.refresh_16px;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.ToolBarButtonActualizar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.Comprobante.Properties.Resources.filter_16px;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.close_window_16px;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.ToolBarButtonCerrar_Click);
            // 
            // gridSearchResult
            // 
            this.gridSearchResult.Controls.Add(this.radWaitingBar1);
            this.gridSearchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSearchResult.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridSearchResult.MasterTemplate.AllowAddNewRow = false;
            this.gridSearchResult.MasterTemplate.AllowDeleteRow = false;
            this.gridSearchResult.MasterTemplate.AllowDragToGroup = false;
            this.gridSearchResult.MasterTemplate.AllowEditRow = false;
            this.gridSearchResult.MasterTemplate.AllowRowResize = false;
            this.gridSearchResult.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "TipoComprobante";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "TipoComprobante";
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.Width = 65;
            gridViewTextBoxColumn5.FieldName = "Status";
            gridViewTextBoxColumn5.HeaderText = "Status";
            gridViewTextBoxColumn5.Name = "Status";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn6.HeaderText = "Receptor";
            gridViewTextBoxColumn6.Name = "Receptor";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.Name = "ReceptorRFC";
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.FieldName = "IdDocumento";
            gridViewTextBoxColumn8.HeaderText = "IdDocumento";
            gridViewTextBoxColumn8.Name = "IdDocumento";
            gridViewTextBoxColumn8.Width = 190;
            gridViewTextBoxColumn9.FieldName = "FechaEmision";
            gridViewTextBoxColumn9.FormatString = "{0:d}";
            gridViewTextBoxColumn9.HeaderText = "Fecha Emision";
            gridViewTextBoxColumn9.Name = "FechaEmision";
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Total";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Total";
            gridViewTextBoxColumn10.Name = "Total";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            this.gridSearchResult.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.gridSearchResult.MasterTemplate.EnableFiltering = true;
            this.gridSearchResult.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridSearchResult.Name = "gridSearchResult";
            this.gridSearchResult.ShowGroupPanel = false;
            this.gridSearchResult.Size = new System.Drawing.Size(680, 226);
            this.gridSearchResult.TabIndex = 2;
            this.gridSearchResult.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridSearchResult_CellDoubleClick);
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.AssociatedControl = this.gridSearchResult;
            this.radWaitingBar1.Location = new System.Drawing.Point(379, 115);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(130, 24);
            this.radWaitingBar1.TabIndex = 1;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingSpeed = 80;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // workerBuscar
            // 
            this.workerBuscar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerBuscar_DoWork);
            this.workerBuscar.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.workerBuscar_ProgressChanged);
            this.workerBuscar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerBuscar_RunWorkerCompleted);
            // 
            // Folio
            // 
            this.Folio.DisplayName = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.Text = "";
            // 
            // ComprobantesBuscar0Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 256);
            this.Controls.Add(this.gridSearchResult);
            this.Controls.Add(this.radCommandBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobantesBuscar0Form";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar";
            this.Load += new System.EventHandler(this.ComprobantesBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSearchResult)).EndInit();
            this.gridSearchResult.ResumeLayout(false);
            this.gridSearchResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.RadGridView gridSearchResult;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelBuscar;
        private System.ComponentModel.BackgroundWorker workerBuscar;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarTextBox Folio;
    }
}
