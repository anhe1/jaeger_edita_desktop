﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class ComprobantesFiscalesResumenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.radPivotGrid1 = new Telerik.WinControls.UI.RadPivotGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radPivotGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutosuma = false;
            this.ToolBar.ShowCancelar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowEjercicio = true;
            this.ToolBar.ShowExportarExcel = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowPeriodo = false;
            this.ToolBar.Size = new System.Drawing.Size(1086, 30);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Actualizar.Click += this.ToolBar_ButtonActualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_ButtonCerrar_Click;
            // 
            // radPivotGrid1
            // 
            this.radPivotGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPivotGrid1.Location = new System.Drawing.Point(0, 30);
            this.radPivotGrid1.Name = "radPivotGrid1";
            this.radPivotGrid1.Size = new System.Drawing.Size(1086, 487);
            this.radPivotGrid1.TabIndex = 1;
            // 
            // ComprobantesFiscalesResumenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 517);
            this.Controls.Add(this.radPivotGrid1);
            this.Controls.Add(this.ToolBar);
            this.Name = "ComprobantesFiscalesResumenForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Resumen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobantesFiscalesResumenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPivotGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarCommonControl ToolBar;
        private Telerik.WinControls.UI.RadPivotGrid radPivotGrid1;
    }
}
