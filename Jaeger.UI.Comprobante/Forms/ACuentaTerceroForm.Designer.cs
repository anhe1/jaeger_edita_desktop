﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ACuentaTerceroForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor2 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TComplemento = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.RegimenFiscalACuentaTerceros = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.buttonNuevoContribuyente = new Telerik.WinControls.UI.RadButton();
            this.buttonActualizarDirectorio = new Telerik.WinControls.UI.RadButton();
            this.RadLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.LabelContribuyente = new Telerik.WinControls.UI.RadLabel();
            this.DomicilioFiscalACuentaTerceros = new Telerik.WinControls.UI.RadTextBox();
            this.NombreACuentaTerceros = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RfcACuentaTerceros = new Telerik.WinControls.UI.RadTextBox();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.CargarReceptores = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscalACuentaTerceros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscalACuentaTerceros.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonNuevoContribuyente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonActualizarDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelContribuyente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DomicilioFiscalACuentaTerceros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreACuentaTerceros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreACuentaTerceros.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreACuentaTerceros.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RfcACuentaTerceros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TComplemento
            // 
            this.TComplemento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComplemento.Etiqueta = "";
            this.TComplemento.Location = new System.Drawing.Point(0, 0);
            this.TComplemento.Name = "TComplemento";
            this.TComplemento.ShowActualizar = false;
            this.TComplemento.ShowAutorizar = false;
            this.TComplemento.ShowCerrar = true;
            this.TComplemento.ShowEditar = false;
            this.TComplemento.ShowExportarExcel = false;
            this.TComplemento.ShowFiltro = false;
            this.TComplemento.ShowGuardar = true;
            this.TComplemento.ShowHerramientas = false;
            this.TComplemento.ShowImagen = false;
            this.TComplemento.ShowImprimir = false;
            this.TComplemento.ShowNuevo = false;
            this.TComplemento.ShowRemover = false;
            this.TComplemento.Size = new System.Drawing.Size(675, 29);
            this.TComplemento.TabIndex = 70;
            this.TComplemento.Guardar.Click += this.TComplemento_Guardar_Click;
            this.TComplemento.Cerrar.Click += this.TComplemento_Cerrar_Click;
            // 
            // RegimenFiscalACuentaTerceros
            // 
            this.RegimenFiscalACuentaTerceros.DisplayMember = "Descriptor";
            this.RegimenFiscalACuentaTerceros.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // RegimenFiscalACuentaTerceros.NestedRadGridView
            // 
            this.RegimenFiscalACuentaTerceros.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.RegimenFiscalACuentaTerceros.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegimenFiscalACuentaTerceros.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RegimenFiscalACuentaTerceros.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Descripcion";
            gridViewTextBoxColumn11.FieldName = "Descriptor";
            gridViewTextBoxColumn11.HeaderText = "Descriptor";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Descriptor";
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.EnableGrouping = false;
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.RegimenFiscalACuentaTerceros.EditorControl.Name = "NestedRadGridView";
            this.RegimenFiscalACuentaTerceros.EditorControl.ReadOnly = true;
            this.RegimenFiscalACuentaTerceros.EditorControl.ShowGroupPanel = false;
            this.RegimenFiscalACuentaTerceros.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.RegimenFiscalACuentaTerceros.EditorControl.TabIndex = 0;
            this.RegimenFiscalACuentaTerceros.Location = new System.Drawing.Point(105, 55);
            this.RegimenFiscalACuentaTerceros.Name = "RegimenFiscalACuentaTerceros";
            this.RegimenFiscalACuentaTerceros.NullText = "Régimen Fiscal";
            this.RegimenFiscalACuentaTerceros.Size = new System.Drawing.Size(341, 20);
            this.RegimenFiscalACuentaTerceros.TabIndex = 97;
            this.RegimenFiscalACuentaTerceros.TabStop = false;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(19, 56);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(83, 18);
            this.radLabel6.TabIndex = 98;
            this.radLabel6.Text = "Régimen Fiscal:";
            // 
            // buttonNuevoContribuyente
            // 
            this.buttonNuevoContribuyente.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.buttonNuevoContribuyente.Enabled = false;
            this.buttonNuevoContribuyente.Image = global::Jaeger.UI.Comprobante.Properties.Resources.add_user_male_16px;
            this.buttonNuevoContribuyente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonNuevoContribuyente.Location = new System.Drawing.Point(426, 29);
            this.buttonNuevoContribuyente.Name = "buttonNuevoContribuyente";
            this.buttonNuevoContribuyente.Size = new System.Drawing.Size(20, 20);
            this.buttonNuevoContribuyente.TabIndex = 92;
            this.buttonNuevoContribuyente.Text = "radButton1";
            // 
            // buttonActualizarDirectorio
            // 
            this.buttonActualizarDirectorio.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.buttonActualizarDirectorio.Enabled = false;
            this.buttonActualizarDirectorio.Image = global::Jaeger.UI.Comprobante.Properties.Resources.refresh_16px;
            this.buttonActualizarDirectorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonActualizarDirectorio.Location = new System.Drawing.Point(403, 29);
            this.buttonActualizarDirectorio.Name = "buttonActualizarDirectorio";
            this.buttonActualizarDirectorio.Size = new System.Drawing.Size(20, 20);
            this.buttonActualizarDirectorio.TabIndex = 91;
            this.buttonActualizarDirectorio.Text = "radButton1";
            // 
            // RadLabel18
            // 
            this.RadLabel18.Location = new System.Drawing.Point(457, 56);
            this.RadLabel18.Name = "RadLabel18";
            this.RadLabel18.Size = new System.Drawing.Size(86, 18);
            this.RadLabel18.TabIndex = 95;
            this.RadLabel18.Text = "Domicilio Fiscal:";
            // 
            // RadLabel11
            // 
            this.RadLabel11.Location = new System.Drawing.Point(457, 30);
            this.RadLabel11.Name = "RadLabel11";
            this.RadLabel11.Size = new System.Drawing.Size(28, 18);
            this.RadLabel11.TabIndex = 93;
            this.RadLabel11.Text = "RFC:";
            // 
            // LabelContribuyente
            // 
            this.LabelContribuyente.Location = new System.Drawing.Point(19, 30);
            this.LabelContribuyente.Name = "LabelContribuyente";
            this.LabelContribuyente.Size = new System.Drawing.Size(54, 18);
            this.LabelContribuyente.TabIndex = 89;
            this.LabelContribuyente.Text = "Receptor:";
            // 
            // DomicilioFiscalACuentaTerceros
            // 
            this.DomicilioFiscalACuentaTerceros.Location = new System.Drawing.Point(549, 55);
            this.DomicilioFiscalACuentaTerceros.Name = "DomicilioFiscalACuentaTerceros";
            this.DomicilioFiscalACuentaTerceros.NullText = "C. Postal";
            this.DomicilioFiscalACuentaTerceros.Size = new System.Drawing.Size(91, 20);
            this.DomicilioFiscalACuentaTerceros.TabIndex = 96;
            // 
            // NombreACuentaTerceros
            // 
            this.NombreACuentaTerceros.AutoSizeDropDownHeight = true;
            this.NombreACuentaTerceros.AutoSizeDropDownToBestFit = true;
            this.NombreACuentaTerceros.DisplayMember = "Nombre";
            // 
            // NombreACuentaTerceros.NestedRadGridView
            // 
            this.NombreACuentaTerceros.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.NombreACuentaTerceros.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreACuentaTerceros.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.NombreACuentaTerceros.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn12.FieldName = "Id";
            gridViewTextBoxColumn12.HeaderText = "Id";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Id";
            gridViewTextBoxColumn13.FieldName = "Nombre";
            gridViewTextBoxColumn13.HeaderText = "Nombre";
            gridViewTextBoxColumn13.Name = "Nombre";
            gridViewTextBoxColumn13.Width = 220;
            gridViewTextBoxColumn14.FieldName = "RFC";
            gridViewTextBoxColumn14.HeaderText = "RFC";
            gridViewTextBoxColumn14.Name = "_drctr_rfc";
            gridViewTextBoxColumn15.FieldName = "ResidenciaFiscal";
            gridViewTextBoxColumn15.HeaderText = "Residencia Fiscal";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "_drctr_resfis";
            gridViewTextBoxColumn16.FieldName = "ClaveUsoCFDI";
            gridViewTextBoxColumn16.HeaderText = "Uso CFDI";
            gridViewTextBoxColumn16.Name = "ClaveUsoCFDI";
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.EnableGrouping = false;
            filterDescriptor2.Operator = Telerik.WinControls.Data.FilterOperator.StartsWith;
            filterDescriptor2.PropertyName = "Nombre";
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor2});
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.NombreACuentaTerceros.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.NombreACuentaTerceros.EditorControl.Name = "NestedRadGridView";
            this.NombreACuentaTerceros.EditorControl.ReadOnly = true;
            this.NombreACuentaTerceros.EditorControl.ShowGroupPanel = false;
            this.NombreACuentaTerceros.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.NombreACuentaTerceros.EditorControl.TabIndex = 0;
            this.NombreACuentaTerceros.Location = new System.Drawing.Point(91, 29);
            this.NombreACuentaTerceros.Name = "NombreACuentaTerceros";
            this.NombreACuentaTerceros.NullText = "Denominación o Razon Social del Receptor";
            this.NombreACuentaTerceros.Size = new System.Drawing.Size(306, 20);
            this.NombreACuentaTerceros.TabIndex = 90;
            this.NombreACuentaTerceros.TabStop = false;
            this.NombreACuentaTerceros.ValueMember = "Id";
            // 
            // RfcACuentaTerceros
            // 
            this.RfcACuentaTerceros.Location = new System.Drawing.Point(505, 29);
            this.RfcACuentaTerceros.Name = "RfcACuentaTerceros";
            this.RfcACuentaTerceros.NullText = "Registro Federal";
            this.RfcACuentaTerceros.ReadOnly = true;
            this.RfcACuentaTerceros.Size = new System.Drawing.Size(135, 20);
            this.RfcACuentaTerceros.TabIndex = 94;
            this.RfcACuentaTerceros.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.LabelContribuyente);
            this.groupBox1.Controls.Add(this.RegimenFiscalACuentaTerceros);
            this.groupBox1.Controls.Add(this.RfcACuentaTerceros);
            this.groupBox1.Controls.Add(this.radLabel6);
            this.groupBox1.Controls.Add(this.NombreACuentaTerceros);
            this.groupBox1.Controls.Add(this.buttonNuevoContribuyente);
            this.groupBox1.Controls.Add(this.DomicilioFiscalACuentaTerceros);
            this.groupBox1.Controls.Add(this.buttonActualizarDirectorio);
            this.groupBox1.Controls.Add(this.RadLabel11);
            this.groupBox1.Controls.Add(this.RadLabel18);
            this.groupBox1.HeaderText = "Información:";
            this.groupBox1.Location = new System.Drawing.Point(0, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(666, 98);
            this.groupBox1.TabIndex = 99;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información:";
            // 
            // CargarReceptores
            // 
            this.CargarReceptores.DoWork += new System.ComponentModel.DoWorkEventHandler(this.CargarReceptores_DoWork);
            this.CargarReceptores.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.CargarReceptores_RunWorkerCompleted);
            // 
            // ACuentaTerceroForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 135);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TComplemento);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ACuentaTerceroForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Complemento: A Cuenta Tercero";
            this.Load += new System.EventHandler(this.ACuentaTerceroForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscalACuentaTerceros.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscalACuentaTerceros.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegimenFiscalACuentaTerceros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonNuevoContribuyente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonActualizarDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelContribuyente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DomicilioFiscalACuentaTerceros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreACuentaTerceros.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreACuentaTerceros.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreACuentaTerceros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RfcACuentaTerceros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TComplemento;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox RegimenFiscalACuentaTerceros;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        internal Telerik.WinControls.UI.RadButton buttonNuevoContribuyente;
        internal Telerik.WinControls.UI.RadButton buttonActualizarDirectorio;
        internal Telerik.WinControls.UI.RadLabel RadLabel18;
        internal Telerik.WinControls.UI.RadLabel RadLabel11;
        internal Telerik.WinControls.UI.RadLabel LabelContribuyente;
        internal Telerik.WinControls.UI.RadTextBox DomicilioFiscalACuentaTerceros;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox NombreACuentaTerceros;
        internal Telerik.WinControls.UI.RadTextBox RfcACuentaTerceros;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private System.ComponentModel.BackgroundWorker CargarReceptores;
    }
}