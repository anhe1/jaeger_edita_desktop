﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class UnidadCatalogoBuscarForm : RadForm {
        protected IUnidadesCatalogo catalogo;

        public event EventHandler<ClaveUnidad> Selected;
        public void OnSelected(ClaveUnidad e) {
            if (this.Selected != null) {
                this.Selected(this, e);
            }
        }

        public UnidadCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new UnidadesCatalogo();
        }

        private void UnidadCatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.gridData.Standard();
            this.gridData.ShowFilteringRow = true;
            this.catalogo.Load();
            this.gridData.DataSource = this.catalogo.Items;
        }

        private void TUnidad_Asignar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as ClaveUnidad;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                    this.Close();
                }
            }
        }

        private void TUnidad_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            this.TUnidad_Asignar.PerformClick();
        }
    }
}
