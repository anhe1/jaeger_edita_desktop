﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComprobantesBackupForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobantesBackupForm));
            this.ChkCrearLog = new Telerik.WinControls.UI.RadCheckBox();
            this.DotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.CommandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLog = new Telerik.WinControls.UI.CommandBarHostItem();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.CommandBarLabel2 = new Telerik.WinControls.UI.CommandBarLabel();
            this.CommandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarLabelTipoComprobante = new Telerik.WinControls.UI.CommandBarLabel();
            this.CommandBarDescarga = new Telerik.WinControls.UI.CommandBarStripElement();
            this.SubTipo = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.ToolBarPorFecha = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ButtonFechaEmision = new Telerik.WinControls.UI.RadMenuItem();
            this.ButtonFechaTimbre = new Telerik.WinControls.UI.RadMenuItem();
            this.ButtonFechaValidacion = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.Descargar = new Telerik.WinControls.UI.RadButton();
            this.ChkNormaliza = new Telerik.WinControls.UI.RadCheckBox();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TxbPath = new Telerik.WinControls.UI.RadTextBox();
            this.DotsSpinnerWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.ImageItems = new System.Windows.Forms.ImageList(this.components);
            this.LabelResult = new Telerik.WinControls.UI.RadLabelElement();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCrearLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descargar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNormaliza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkCrearLog
            // 
            this.ChkCrearLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkCrearLog.Location = new System.Drawing.Point(616, 36);
            this.ChkCrearLog.Name = "ChkCrearLog";
            this.ChkCrearLog.Size = new System.Drawing.Size(68, 18);
            this.ChkCrearLog.TabIndex = 22;
            this.ChkCrearLog.Text = "Crear Log";
            this.ChkCrearLog.TextWrap = true;
            this.ChkCrearLog.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // DotsLineWaitingBarIndicatorElement1
            // 
            this.DotsLineWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsLineWaitingBarIndicatorElement1.Name = "DotsLineWaitingBarIndicatorElement1";
            this.DotsLineWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsLineWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // CommandBarSeparator3
            // 
            this.CommandBarSeparator3.DisplayName = "CommandBarSeparator3";
            this.CommandBarSeparator3.Name = "CommandBarSeparator3";
            this.CommandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // ToolBarLog
            // 
            this.ToolBarLog.DisplayName = "CommandBarHostItem1";
            this.ToolBarLog.Name = "ToolBarLog";
            this.ToolBarLog.Text = "";
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // CommandBarLabel2
            // 
            this.CommandBarLabel2.DisplayName = "CommandBarLabel2";
            this.CommandBarLabel2.Name = "CommandBarLabel2";
            this.CommandBarLabel2.Text = "Obtener por fecha:";
            // 
            // CommandBarSeparator1
            // 
            this.CommandBarSeparator1.DisplayName = "CommandBarSeparator1";
            this.CommandBarSeparator1.Name = "CommandBarSeparator1";
            this.CommandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarLabelTipoComprobante
            // 
            this.ToolBarLabelTipoComprobante.DisplayName = "CommandBarLabel1";
            this.ToolBarLabelTipoComprobante.Name = "ToolBarLabelTipoComprobante";
            this.ToolBarLabelTipoComprobante.Text = "Tipo de Comprobante:";
            // 
            // CommandBarDescarga
            // 
            this.CommandBarDescarga.DisplayName = "CommandBarStripElement1";
            this.CommandBarDescarga.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelTipoComprobante,
            this.SubTipo,
            this.CommandBarSeparator1,
            this.CommandBarLabel2,
            this.ToolBarPorFecha,
            this.CommandBarSeparator2,
            this.ToolBarLog,
            this.CommandBarSeparator3,
            this.Cerrar});
            this.CommandBarDescarga.Name = "CommandBarDescarga";
            // 
            // SubTipo
            // 
            this.SubTipo.AutoCompleteDisplayMember = "Descripcion";
            this.SubTipo.AutoCompleteValueMember = "Id";
            this.SubTipo.DisplayMember = "Descripcion";
            this.SubTipo.DropDownAnimationEnabled = true;
            this.SubTipo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.SubTipo.MaxDropDownItems = 0;
            this.SubTipo.Name = "SubTipo";
            this.SubTipo.NullText = "Sub Tipo";
            this.SubTipo.Text = "";
            this.SubTipo.ValueMember = "Id";
            // 
            // ToolBarPorFecha
            // 
            this.ToolBarPorFecha.DefaultItem = null;
            this.ToolBarPorFecha.DisplayName = "CommandBarSplitButton2";
            this.ToolBarPorFecha.DrawImage = false;
            this.ToolBarPorFecha.DrawText = true;
            this.ToolBarPorFecha.Image = null;
            this.ToolBarPorFecha.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ButtonFechaEmision,
            this.ButtonFechaTimbre,
            this.ButtonFechaValidacion});
            this.ToolBarPorFecha.MinSize = new System.Drawing.Size(120, 26);
            this.ToolBarPorFecha.Name = "ToolBarPorFecha";
            this.ToolBarPorFecha.Text = "Selecciona";
            // 
            // ButtonFechaEmision
            // 
            this.ButtonFechaEmision.Name = "ButtonFechaEmision";
            this.ButtonFechaEmision.Tag = "FechaEmision";
            this.ButtonFechaEmision.Text = "Fecha Emisión";
            this.ButtonFechaEmision.UseCompatibleTextRendering = false;
            // 
            // ButtonFechaTimbre
            // 
            this.ButtonFechaTimbre.Name = "ButtonFechaTimbre";
            this.ButtonFechaTimbre.Tag = "FechaTimbre";
            this.ButtonFechaTimbre.Text = "Fecha de Timbre";
            this.ButtonFechaTimbre.UseCompatibleTextRendering = false;
            // 
            // ButtonFechaValidacion
            // 
            this.ButtonFechaValidacion.Name = "ButtonFechaValidacion";
            this.ButtonFechaValidacion.Tag = "FechaValidacion";
            this.ButtonFechaValidacion.Text = "Fecha Validación";
            this.ButtonFechaValidacion.UseCompatibleTextRendering = false;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "CommandBarButton1";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.CommandBarDescarga});
            this.CommandBarRowElement1.Text = "";
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(599, 55);
            this.RadCommandBar1.TabIndex = 21;
            // 
            // Descargar
            // 
            this.Descargar.Location = new System.Drawing.Point(455, 38);
            this.Descargar.Name = "Descargar";
            this.Descargar.Size = new System.Drawing.Size(137, 24);
            this.Descargar.TabIndex = 20;
            this.Descargar.Text = "Descargar";
            // 
            // ChkNormaliza
            // 
            this.ChkNormaliza.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkNormaliza.Location = new System.Drawing.Point(75, 41);
            this.ChkNormaliza.Name = "ChkNormaliza";
            this.ChkNormaliza.Size = new System.Drawing.Size(183, 18);
            this.ChkNormaliza.TabIndex = 19;
            this.ChkNormaliza.Text = "Normalizar nombres de carpetas";
            this.ChkNormaliza.TextWrap = true;
            this.ChkNormaliza.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // RadLabel3
            // 
            this.RadLabel3.Location = new System.Drawing.Point(5, 13);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(64, 18);
            this.RadLabel3.TabIndex = 18;
            this.RadLabel3.Text = "Guardar en:";
            // 
            // TxbPath
            // 
            this.TxbPath.Location = new System.Drawing.Point(75, 12);
            this.TxbPath.Name = "TxbPath";
            this.TxbPath.NullText = "Selecciona ...";
            this.TxbPath.Size = new System.Drawing.Size(517, 20);
            this.TxbPath.TabIndex = 17;
            // 
            // DotsSpinnerWaitingBarIndicatorElement1
            // 
            this.DotsSpinnerWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsSpinnerWaitingBarIndicatorElement1.Name = "DotsSpinnerWaitingBarIndicatorElement1";
            this.DotsSpinnerWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DotsSpinnerWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(246, 140);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(70, 70);
            this.Espera.TabIndex = 23;
            this.Espera.Text = "RadWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.DotsSpinnerWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 100;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.Espera.GetChildAt(0))).WaitingSpeed = 100;
            ((Telerik.WinControls.UI.WaitingBarContentElement)(this.Espera.GetChildAt(0).GetChildAt(0))).WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsSpinner;
            ((Telerik.WinControls.UI.WaitingBarSeparatorElement)(this.Espera.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Dash = false;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.ImageList = this.ImageItems;
            this.GridData.Location = new System.Drawing.Point(0, 55);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "Index";
            gridViewTextBoxColumn1.HeaderText = "Index";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Index";
            gridViewTextBoxColumn2.FieldName = "Parent";
            gridViewTextBoxColumn2.HeaderText = "Parent";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Parent";
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "Año / Mes";
            gridViewTextBoxColumn3.Name = "Name";
            gridViewTextBoxColumn4.FieldName = "Type";
            gridViewTextBoxColumn4.HeaderText = "Tipo";
            gridViewTextBoxColumn4.Name = "Type";
            gridViewTextBoxColumn5.FieldName = "Count";
            gridViewTextBoxColumn5.FormatString = "{0:#,###0} elemento(s)";
            gridViewTextBoxColumn5.HeaderText = "Cantidad";
            gridViewTextBoxColumn5.Name = "Count";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.FieldName = "Year";
            gridViewTextBoxColumn6.HeaderText = "Año";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Year";
            gridViewTextBoxColumn7.FieldName = "Month";
            gridViewTextBoxColumn7.HeaderText = "Mes";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "Month";
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(599, 382);
            this.GridData.TabIndex = 16;
            this.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.GridData.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_ViewCellFormatting);
            // 
            // ImageItems
            // 
            this.ImageItems.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageItems.ImageStream")));
            this.ImageItems.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageItems.Images.SetKeyName(0, "folder");
            this.ImageItems.Images.SetKeyName(1, "folderOpened");
            this.ImageItems.Images.SetKeyName(2, "foldeOpen");
            // 
            // LabelResult
            // 
            this.LabelResult.Name = "LabelResult";
            this.BarraEstado.SetSpring(this.LabelResult, false);
            this.LabelResult.Text = "";
            this.LabelResult.TextWrap = true;
            this.LabelResult.UseCompatibleTextRendering = false;
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LabelResult});
            this.BarraEstado.Location = new System.Drawing.Point(0, 507);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(599, 26);
            this.BarraEstado.SizingGrip = false;
            this.BarraEstado.TabIndex = 15;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox1.Location = new System.Drawing.Point(278, 41);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(130, 18);
            this.radCheckBox1.TabIndex = 24;
            this.radCheckBox1.Text = "Método no sincrónico";
            this.radCheckBox1.TextWrap = true;
            this.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.radCheckBox1);
            this.groupBox1.Controls.Add(this.TxbPath);
            this.groupBox1.Controls.Add(this.RadLabel3);
            this.groupBox1.Controls.Add(this.ChkNormaliza);
            this.groupBox1.Controls.Add(this.Descargar);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 437);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(599, 70);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // ComprobantesBackupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 533);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ChkCrearLog);
            this.Controls.Add(this.RadCommandBar1);
            this.Controls.Add(this.Espera);
            this.Controls.Add(this.BarraEstado);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobantesBackupForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Backup de Comprobantes";
            this.Load += new System.EventHandler(this.ComprobantesBackupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ChkCrearLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descargar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNormaliza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Telerik.WinControls.UI.RadCheckBox ChkCrearLog;
        internal Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement DotsLineWaitingBarIndicatorElement1;
        internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator3;
        internal Telerik.WinControls.UI.CommandBarHostItem ToolBarLog;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.RadMenuItem ButtonFechaValidacion;
        internal Telerik.WinControls.UI.RadMenuItem ButtonFechaTimbre;
        internal Telerik.WinControls.UI.RadMenuItem ButtonFechaEmision;
        internal Telerik.WinControls.UI.CommandBarSplitButton ToolBarPorFecha;
        internal Telerik.WinControls.UI.CommandBarLabel CommandBarLabel2;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator1;
        internal Telerik.WinControls.UI.CommandBarLabel ToolBarLabelTipoComprobante;
        internal Telerik.WinControls.UI.CommandBarStripElement CommandBarDescarga;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.RadButton Descargar;
        internal Telerik.WinControls.UI.RadCheckBox ChkNormaliza;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadTextBox TxbPath;
        internal Telerik.WinControls.UI.DotsSpinnerWaitingBarIndicatorElement DotsSpinnerWaitingBarIndicatorElement1;
        internal Telerik.WinControls.UI.RadWaitingBar Espera;
        internal Telerik.WinControls.UI.RadGridView GridData;
        internal Telerik.WinControls.UI.RadLabelElement LabelResult;
        internal Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        internal Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private System.Windows.Forms.ImageList ImageItems;
        private Telerik.WinControls.UI.CommandBarDropDownList SubTipo;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
    }
}