﻿namespace Jaeger.UI.Comprobante.Forms
{
    partial class ComprobanteProductosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobanteProductosForm));
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoValeEntrada = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoProducto = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarNuevoValeSalida = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ToolBarButtonCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonDuplicar = new Telerik.WinControls.UI.RadMenuItem();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar ubicación";
            this.ToolBarButtonAgregar.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevoValeEntrada
            // 
            this.ToolBarNuevoValeEntrada.Name = "ToolBarNuevoValeEntrada";
            this.ToolBarNuevoValeEntrada.Text = "Vale de Entrada";
            this.ToolBarNuevoValeEntrada.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevoProducto
            // 
            this.ToolBarNuevoProducto.Name = "ToolBarNuevoProducto";
            this.ToolBarNuevoProducto.Text = "Producto";
            this.ToolBarNuevoProducto.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevo
            // 
            this.ToolBarNuevo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarNuevoProducto,
            this.ToolBarNuevoValeEntrada,
            this.ToolBarNuevoValeSalida});
            this.ToolBarNuevo.Name = "ToolBarNuevo";
            this.ToolBarNuevo.Text = "Nuevo";
            this.ToolBarNuevo.UseCompatibleTextRendering = false;
            // 
            // ToolBarNuevoValeSalida
            // 
            this.ToolBarNuevoValeSalida.Name = "ToolBarNuevoValeSalida";
            this.ToolBarNuevoValeSalida.Text = "Vale de Salida";
            this.ToolBarNuevoValeSalida.UseCompatibleTextRendering = false;
            // 
            // MenuContextual
            // 
            this.MenuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCopiar,
            this.ToolBarButtonDuplicar,
            this.ToolBarNuevo,
            this.ToolBarButtonAgregar});
            // 
            // ToolBarButtonCopiar
            // 
            this.ToolBarButtonCopiar.Name = "ToolBarButtonCopiar";
            this.ToolBarButtonCopiar.Text = "Copiar";
            this.ToolBarButtonCopiar.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonDuplicar
            // 
            this.ToolBarButtonDuplicar.Name = "ToolBarButtonDuplicar";
            this.ToolBarButtonDuplicar.Text = "Duplicar";
            this.ToolBarButtonDuplicar.UseCompatibleTextRendering = false;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "IdModelo";
            gridViewTextBoxColumn13.FormatString = "{0:PT00000#}";
            gridViewTextBoxColumn13.HeaderText = "Id";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "IdModelo";
            gridViewTextBoxColumn13.Width = 65;
            gridViewTextBoxColumn14.FieldName = "Tipo";
            gridViewTextBoxColumn14.HeaderText = "Tipo";
            gridViewTextBoxColumn14.Name = "Tipo";
            gridViewTextBoxColumn14.Width = 80;
            gridViewTextBoxColumn15.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn15.HeaderText = "Identificador";
            gridViewTextBoxColumn15.Name = "NoIdentificacion";
            gridViewTextBoxColumn15.Width = 95;
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewTextBoxColumn16.FieldName = "Descripcion";
            gridViewTextBoxColumn16.HeaderText = "Descripción";
            gridViewTextBoxColumn16.Name = "Modelo";
            gridViewTextBoxColumn16.Width = 350;
            gridViewTextBoxColumn17.FieldName = "Marca";
            gridViewTextBoxColumn17.HeaderText = "Marca";
            gridViewTextBoxColumn17.Name = "Marca";
            gridViewTextBoxColumn17.Width = 200;
            gridViewTextBoxColumn18.FieldName = "Especificacion";
            gridViewTextBoxColumn18.HeaderText = "Especificación";
            gridViewTextBoxColumn18.Name = "Especificacion";
            gridViewTextBoxColumn18.Width = 115;
            gridViewTextBoxColumn19.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn19.HeaderText = "Clave\r\nProd. Serv.";
            gridViewTextBoxColumn19.Name = "ClaveProdServ";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn19.Width = 80;
            gridViewComboBoxColumn3.FieldName = "ClaveUnidad";
            gridViewComboBoxColumn3.HeaderText = "Clave\r\nUnidad";
            gridViewComboBoxColumn3.Name = "ClaveUnidad";
            gridViewComboBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn3.Width = 80;
            gridViewComboBoxColumn4.FieldName = "Unidad";
            gridViewComboBoxColumn4.HeaderText = "Unidad";
            gridViewComboBoxColumn4.Name = "Unidad";
            gridViewComboBoxColumn4.Width = 75;
            gridViewTextBoxColumn20.DataType = typeof(decimal);
            gridViewTextBoxColumn20.FieldName = "Unitario";
            gridViewTextBoxColumn20.FormatString = "{0:n}";
            gridViewTextBoxColumn20.HeaderText = "Unitario";
            gridViewTextBoxColumn20.Name = "Unitario";
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.FieldName = "CtaPredial";
            gridViewTextBoxColumn21.HeaderText = "Cta. Predial";
            gridViewTextBoxColumn21.Name = "CtaPredial";
            gridViewTextBoxColumn21.Width = 85;
            gridViewTextBoxColumn22.FieldName = "NumRequerimiento";
            gridViewTextBoxColumn22.HeaderText = "Núm Req.";
            gridViewTextBoxColumn22.Name = "NumRequerimiento";
            gridViewTextBoxColumn22.Width = 85;
            gridViewTextBoxColumn23.FieldName = "Creo";
            gridViewTextBoxColumn23.HeaderText = "Creó";
            gridViewTextBoxColumn23.Name = "Creo";
            gridViewTextBoxColumn23.Width = 75;
            gridViewTextBoxColumn24.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn24.FieldName = "FechaNuevo";
            gridViewTextBoxColumn24.FormatString = "{0:d}";
            gridViewTextBoxColumn24.HeaderText = "Fec. Sistema";
            gridViewTextBoxColumn24.Name = "FechaNuevo";
            gridViewTextBoxColumn24.Width = 75;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewComboBoxColumn3,
            gridViewComboBoxColumn4,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1139, 542);
            this.GridData.TabIndex = 4;
            this.GridData.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridData_RowsChanged);
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowFiltro = true;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = true;
            this.ToolBar.Size = new System.Drawing.Size(1139, 30);
            this.ToolBar.TabIndex = 5;
            this.ToolBar.Nuevo.Click += this.ToolBarButtonNuevo_Click;
            this.ToolBar.Editar.Click += this.ToolBarButtonEditar_Click;
            this.ToolBar.Remover.Click += this.ToolBarButtonEliminar_Click;
            this.ToolBar.Actualizar.Click += this.ToolBarButtonActualizar_Click;
            this.ToolBar.Filtro.Click += this.ToolBarButtonFiltrar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBarButtonCerrar_Click;
            // 
            // ComprobanteProductosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 572);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComprobanteProductosForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Catálogo Productos";
            this.Load += new System.EventHandler(this.ViewComprobanteProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonAgregar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoValeEntrada;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoProducto;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevo;
        private Telerik.WinControls.UI.RadMenuItem ToolBarNuevoValeSalida;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonDuplicar;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCopiar;
        private Telerik.WinControls.UI.RadContextMenu MenuContextual;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}
