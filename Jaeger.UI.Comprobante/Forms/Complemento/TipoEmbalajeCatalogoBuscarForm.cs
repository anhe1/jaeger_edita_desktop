﻿using System;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class TipoEmbalajeCatalogoBuscarForm : RadForm {
        protected IClaveTipoEmbalajeCatalogo catalogo;
        private CveTipoEmbalaje seleccionado = null;
        public event EventHandler<CveTipoEmbalaje> Seleted;

        public void OnSelected(CveTipoEmbalaje e) {
            if (this.Seleted != null) {
                this.Seleted(this, e);
            }
        }
        public TipoEmbalajeCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new TipoEmbalajeCatalogo();
        }

        private void TipoEmbalajeCatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.catalogo.Load();
            this.gridCatalogo.DataSource = this.catalogo.Items;
            this.TCatalogo.Buscar.TextChanged += Buscar_TextChanged;
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Buscar.Text.Length > 3) {
                this.TCatalogo.Buscar.PerformClick();
            }
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            this.gridCatalogo.DataSource = this.catalogo.Search(this.TCatalogo.Buscar.Text);
        }

        private void TCatalogo_Imprimir_Click(object sender, EventArgs e) {
            if (this.gridCatalogo.CurrentRow != null) {
                this.seleccionado = this.gridCatalogo.CurrentRow.DataBoundItem as CveTipoEmbalaje;
                if (seleccionado != null) {
                    this.OnSelected(this.seleccionado);
                    this.Close();
                }
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
