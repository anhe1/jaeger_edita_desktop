﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class CartaPorteMercanciaControl : RadForm {
        protected bool editar = false;
        protected CartaPorteMercanciasMercancia Mercancia;
        protected ICveProdServCPCatalogo cveProdServCPCatalogo;
        protected IClaveMaterialPeligrosoCatalogo claveMaterialPeligrosoCatalogo;
        protected IClaveTipoEmbalajeCatalogo claveTipoEmbalajeCatalogo;

        public event EventHandler<CartaPorteMercanciasMercancia> Add;
        public event EventHandler<CartaPorteMercanciasMercancia> Edit;

        public void OnAdd(CartaPorteMercanciasMercancia mercancia) {
            if (this.Add != null) {
                this.Add(this, mercancia);
            }
        }

        public void OnEdit(CartaPorteMercanciasMercancia mercancia) {
            if (this.Edit != null) {
                this.Edit(this, mercancia);
            }
        }

        public CartaPorteMercanciaControl() {
            InitializeComponent();
        }

        public CartaPorteMercanciaControl(CartaPorteMercanciasMercancia model) {
            InitializeComponent();
            if (model == null) {
                this.Mercancia = new CartaPorteMercanciasMercancia();
            } else {
                this.editar = true;
                this.Mercancia = model;
            }
        }

        private void CartaPorteMercanciaControl_Load(object sender, EventArgs e) {
            cveProdServCPCatalogo = new CveProdServCPCatalogo();
            cveProdServCPCatalogo.Load();

            this.claveMaterialPeligrosoCatalogo = new MaterialPeligrosoCatalogo();
            this.claveMaterialPeligrosoCatalogo.Load();

            this.claveTipoEmbalajeCatalogo = new TipoEmbalajeCatalogo();
            this.claveTipoEmbalajeCatalogo.Load();

            this.CreateBinding();

            //this.MercanciaControl.DetalleMercanciaSpecified.CheckedChanged += DetalleMercanciaSpecified_CheckedChanged;

            this.MercanciaControl.TCantidad.Nuevo.Click += TCantidad_Nuevo_Click;

            this.MercanciaControl.TGuia.Nuevo.Click += TGuia_Nuevo_Click;
        }

        private void TGuia_Nuevo_Click(object sender, EventArgs e) {
            this.Mercancia.GuiasIdentificacion.AddNew();
        }

        private void TGuia_Remover_Click(object sender, EventArgs e) {

        }

        private void TCantidad_Nuevo_Click(object sender, EventArgs e) {
            this.Mercancia.CantidadTransporta.AddNew();
        }

        public void CreateBinding() {
            this.MercanciaControl.BienesTransp.DataBindings.Clear();
            this.MercanciaControl.BienesTransp.DataBindings.Add("Text", this.Mercancia, "BienesTransp", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.Descripcion.DataBindings.Clear();
            this.MercanciaControl.Descripcion.DataBindings.Add("Text", this.Mercancia, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.Cantidad.DataBindings.Clear();
            this.MercanciaControl.Cantidad.DataBindings.Add("Text", this.Mercancia, "Cantidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.ClaveUnidad.DataBindings.Clear();
            this.MercanciaControl.ClaveUnidad.DataBindings.Add("Text", this.Mercancia, "ClaveUnidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.Unidad.DataBindings.Clear();
            this.MercanciaControl.Unidad.DataBindings.Add("Text", this.Mercancia, "Unidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.PesoEnKg.DataBindings.Clear();
            this.MercanciaControl.PesoEnKg.DataBindings.Add("Text", this.Mercancia, "PesoEnKg", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.ValorMercancia.DataBindings.Clear();
            this.MercanciaControl.ValorMercancia.DataBindings.Add("Text", this.Mercancia, "ValorMercancia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.Moneda.DataBindings.Clear();
            this.MercanciaControl.Moneda.DataBindings.Add("Text", this.Mercancia, "Moneda", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.MaterialPeligroso.DataBindings.Clear();
            this.MercanciaControl.MaterialPeligroso.DataBindings.Add("Checked", this.Mercancia, "MaterialPeligroso", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.CveMaterialPeligroso.DataBindings.Clear();
            this.MercanciaControl.CveMaterialPeligroso.DataBindings.Add("SelectedValue", this.Mercancia, "CveMaterialPeligroso", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.Dimensiones.DataBindings.Clear();
            this.MercanciaControl.Dimensiones.DataBindings.Add("Text", this.Mercancia, "Dimensiones", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.ClaveSTCC.DataBindings.Clear();
            this.MercanciaControl.ClaveSTCC.DataBindings.Add("Text", this.Mercancia, "ClaveSTCC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.FraccionArancelaria.DataBindings.Clear();
            this.MercanciaControl.FraccionArancelaria.DataBindings.Add("SelectedValue", this.Mercancia, "FraccionArancelaria", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.Embalaje.DataBindings.Clear();
            this.MercanciaControl.Embalaje.DataBindings.Add("Text", this.Mercancia, "Embalaje", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.DescripEmbalaje.DataBindings.Clear();
            this.MercanciaControl.DescripEmbalaje.DataBindings.Add("Text", this.Mercancia, "DescripEmbalaje", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.UUIDComercioExt.DataBindings.Clear();
            this.MercanciaControl.UUIDComercioExt.DataBindings.Add("Text", this.Mercancia, "UUIDComercioExt", true, DataSourceUpdateMode.OnPropertyChanged);

            this.MercanciaControl.gridCantidadTransporte.DataSource = this.Mercancia.CantidadTransporta;
            this.MercanciaControl.gridGuias.DataSource = this.Mercancia.GuiasIdentificacion;
            this.MercanciaControl.gridPedimentos.DataSource = this.Mercancia.Pedimentos;
            if (this.Mercancia.DetalleMercancia != null) {
                this.MercanciaControl.DetalleMercanciaSpecified.Checked = true;
                this.CreateBinding2();
            }
        }

        private void CreateBinding2() {
            this.MercanciaControl.UnidadPesoMerc.DataBindings.Clear();
            this.MercanciaControl.PesoBruto.DataBindings.Clear();
            this.MercanciaControl.PesoNeto.DataBindings.Clear();
            this.MercanciaControl.PesoTara.DataBindings.Clear();
            this.MercanciaControl.NumPiezas.DataBindings.Clear();

            this.MercanciaControl.UnidadPesoMerc.DataBindings.Add("SelectedValue", this.Mercancia.DetalleMercancia, "UnidadPesoMerc", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.PesoBruto.DataBindings.Add("Text", this.Mercancia.DetalleMercancia, "PesoBruto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.PesoNeto.DataBindings.Add("Text", this.Mercancia.DetalleMercancia, "PesoNeto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.PesoTara.DataBindings.Add("Text", this.Mercancia.DetalleMercancia, "PesoTara", true, DataSourceUpdateMode.OnPropertyChanged);
            this.MercanciaControl.NumPiezas.DataBindings.Add("Text", this.Mercancia.DetalleMercancia, "NumPiezas", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void DetalleMercanciaSpecified_CheckedChanged(object sender, EventArgs e) {
            if (this.Mercancia != null) {
                if (MercanciaControl.DetalleMercanciaSpecified.Checked) {
                    this.Mercancia.DetalleMercancia = new CartaPorteMercanciasMercanciaDetalleMercancia();
                    this.CreateBinding2();
                } else this.Mercancia.DetalleMercancia = null;
            }
        }

        private void TMercancia_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TMercancia_Guardar_Click(object sender, EventArgs e) {
            if (this.Verificar() == false)
                return;

            if (this.editar == false) {
                this.OnAdd(this.Mercancia);
            } else {
                this.OnEdit(this.Mercancia);
            }
        }

        private void TMercancia_Nuevo_Click(object sender, EventArgs e) {
            this.Mercancia = new CartaPorteMercanciasMercancia();
            this.CreateBinding();
        }

        private bool Verificar() {
            this.MercanciaControl.ProviderError.Clear();
            if (string.IsNullOrEmpty(this.Mercancia.BienesTransp)) {
                this.MercanciaControl.ProviderError.SetError(this.MercanciaControl.BienesTransp, "Clave de producto no vaílida.");
                return false;
            }

            if (!string.IsNullOrEmpty(this.Mercancia.Descripcion)) {
                if (!ValidacionService.RegexValid("[^|]{1,1000}", this.Mercancia.Descripcion)) {
                    this.MercanciaControl.ProviderError.SetError(this.MercanciaControl.Descripcion, "Descripción no válido");
                    return false;
                }
            } 

            if (this.Mercancia.Cantidad <= 0) {
                this.MercanciaControl.ProviderError.SetError(this.MercanciaControl.Cantidad, "La cantidad no es válido");
                return false;
            }

            if (!string.IsNullOrEmpty(this.Mercancia.ClaveSTCC)) {
                if (!ValidacionService.RegexValid("[0-9|]{6,7}", this.Mercancia.ClaveSTCC)) {
                    this.MercanciaControl.ProviderError.SetError(this.MercanciaControl.ClaveSTCC, "Clave de producto de la STCC no válido");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(this.Mercancia.DescripEmbalaje)) {
                if (!ValidacionService.RegexValid("[^|]{1,100}", this.Mercancia.DescripEmbalaje)) {
                    this.MercanciaControl.ProviderError.SetError(this.MercanciaControl.DescripEmbalaje, "Descripción del embalaje de los bienes y/o mercancías no válido");
                    return false;
                }
            }

            if (this.Mercancia.PesoEnKg <= 0) {
                this.MercanciaControl.ProviderError.SetError(this.MercanciaControl.PesoEnKg, "Kilogramos del peso estimado de los bienes y/o mercancías que se trasladan es un dato no válido.");
                return false;
            }
            return true;
        }
    }
}
