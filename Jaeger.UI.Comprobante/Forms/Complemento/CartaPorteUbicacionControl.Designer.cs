﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class CartaPorteUbicacionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CartaPorteUbicacionControl));
            this.gUbicacion = new Telerik.WinControls.UI.RadGroupBox();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.TipoUbicacion = new Telerik.WinControls.UI.RadDropDownList();
            this.TotalDistRec = new Telerik.WinControls.UI.RadSpinEditor();
            this.ResidenciaFiscalFigura = new Telerik.WinControls.UI.RadDropDownList();
            this.label12 = new Telerik.WinControls.UI.RadLabel();
            this.NumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.FechaSalida = new Telerik.WinControls.UI.RadDateTimePicker();
            this.TipoEstacion = new Telerik.WinControls.UI.RadDropDownList();
            this.NombreEstacion = new Telerik.WinControls.UI.RadTextBox();
            this.NombreRemitenteDestinatario = new Telerik.WinControls.UI.RadTextBox();
            this.NavegacionTrafico = new Telerik.WinControls.UI.RadDropDownList();
            this.NumEstacion = new Telerik.WinControls.UI.RadDropDownList();
            this.RFCRemitenteDestinatario = new Telerik.WinControls.UI.RadTextBox();
            this.IDUbicacion = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.TUbicacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.label13 = new Telerik.WinControls.UI.RadLabel();
            this.CodigoPostal = new Telerik.WinControls.UI.RadTextBox();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.Calle = new Telerik.WinControls.UI.RadTextBox();
            this.Pais = new Telerik.WinControls.UI.RadDropDownList();
            this.NumeroExterior = new Telerik.WinControls.UI.RadTextBox();
            this.label16 = new Telerik.WinControls.UI.RadLabel();
            this.label17 = new Telerik.WinControls.UI.RadLabel();
            this.Estado = new Telerik.WinControls.UI.RadDropDownList();
            this.NumeroInterior = new Telerik.WinControls.UI.RadTextBox();
            this.label18 = new Telerik.WinControls.UI.RadLabel();
            this.label19 = new Telerik.WinControls.UI.RadLabel();
            this.Municipio = new Telerik.WinControls.UI.RadDropDownList();
            this.label20 = new Telerik.WinControls.UI.RadLabel();
            this.Colonia = new Telerik.WinControls.UI.RadDropDownList();
            this.label21 = new Telerik.WinControls.UI.RadLabel();
            this.Referencia = new Telerik.WinControls.UI.RadTextBox();
            this.Localidad = new Telerik.WinControls.UI.RadDropDownList();
            this.label22 = new Telerik.WinControls.UI.RadLabel();
            this.providerError = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gUbicacion)).BeginInit();
            this.gUbicacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoUbicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscalFigura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoEstacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreEstacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreRemitenteDestinatario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavegacionTrafico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumEstacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCRemitenteDestinatario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDUbicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroInterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Municipio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Colonia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Localidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gUbicacion
            // 
            this.gUbicacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gUbicacion.Controls.Add(this.label10);
            this.gUbicacion.Controls.Add(this.TipoUbicacion);
            this.gUbicacion.Controls.Add(this.TotalDistRec);
            this.gUbicacion.Controls.Add(this.ResidenciaFiscalFigura);
            this.gUbicacion.Controls.Add(this.label12);
            this.gUbicacion.Controls.Add(this.NumRegIdTrib);
            this.gUbicacion.Controls.Add(this.label11);
            this.gUbicacion.Controls.Add(this.FechaSalida);
            this.gUbicacion.Controls.Add(this.TipoEstacion);
            this.gUbicacion.Controls.Add(this.NombreEstacion);
            this.gUbicacion.Controls.Add(this.NombreRemitenteDestinatario);
            this.gUbicacion.Controls.Add(this.NavegacionTrafico);
            this.gUbicacion.Controls.Add(this.NumEstacion);
            this.gUbicacion.Controls.Add(this.RFCRemitenteDestinatario);
            this.gUbicacion.Controls.Add(this.IDUbicacion);
            this.gUbicacion.Controls.Add(this.label5);
            this.gUbicacion.Controls.Add(this.label8);
            this.gUbicacion.Controls.Add(this.label7);
            this.gUbicacion.Controls.Add(this.label9);
            this.gUbicacion.Controls.Add(this.label6);
            this.gUbicacion.Controls.Add(this.label4);
            this.gUbicacion.Controls.Add(this.label3);
            this.gUbicacion.Controls.Add(this.label2);
            this.gUbicacion.Controls.Add(this.label1);
            this.gUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.gUbicacion.HeaderText = "Generales";
            this.gUbicacion.Location = new System.Drawing.Point(0, 31);
            this.gUbicacion.Name = "gUbicacion";
            this.gUbicacion.Size = new System.Drawing.Size(681, 193);
            this.gUbicacion.TabIndex = 0;
            this.gUbicacion.TabStop = false;
            this.gUbicacion.Text = "Generales";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(12, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 18);
            this.label10.TabIndex = 1;
            this.label10.Text = "Tipo de ubicación:*";
            // 
            // TipoUbicacion
            // 
            this.TipoUbicacion.BackColor = System.Drawing.SystemColors.Window;
            this.TipoUbicacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "Origen";
            radListDataItem2.Text = "Destino";
            this.TipoUbicacion.Items.Add(radListDataItem1);
            this.TipoUbicacion.Items.Add(radListDataItem2);
            this.TipoUbicacion.Location = new System.Drawing.Point(117, 18);
            this.TipoUbicacion.Name = "TipoUbicacion";
            this.TipoUbicacion.Size = new System.Drawing.Size(134, 20);
            this.TipoUbicacion.TabIndex = 0;
            // 
            // TotalDistRec
            // 
            this.TotalDistRec.Location = new System.Drawing.Point(547, 160);
            this.TotalDistRec.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.TotalDistRec.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TotalDistRec.Name = "TotalDistRec";
            this.TotalDistRec.NullableValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TotalDistRec.Size = new System.Drawing.Size(121, 20);
            this.TotalDistRec.TabIndex = 11;
            this.TotalDistRec.TabStop = false;
            this.TotalDistRec.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.TotalDistRec.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // ResidenciaFiscalFigura
            // 
            this.ResidenciaFiscalFigura.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ResidenciaFiscalFigura.Location = new System.Drawing.Point(547, 68);
            this.ResidenciaFiscalFigura.MaxLength = 40;
            this.ResidenciaFiscalFigura.Name = "ResidenciaFiscalFigura";
            this.ResidenciaFiscalFigura.Size = new System.Drawing.Size(121, 20);
            this.ResidenciaFiscalFigura.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(545, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 18);
            this.label12.TabIndex = 30;
            this.label12.Text = "Residencia Fiscal";
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.Location = new System.Drawing.Point(420, 68);
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Size = new System.Drawing.Size(121, 20);
            this.NumRegIdTrib.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(418, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 18);
            this.label11.TabIndex = 28;
            this.label11.Text = "Núm. Reg Id Tributario";
            // 
            // FechaSalida
            // 
            this.FechaSalida.CustomFormat = "dddd, dd/MMMM/yyyy hh:mm";
            this.FechaSalida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaSalida.Location = new System.Drawing.Point(12, 160);
            this.FechaSalida.Name = "FechaSalida";
            this.FechaSalida.Size = new System.Drawing.Size(265, 20);
            this.FechaSalida.TabIndex = 9;
            this.FechaSalida.TabStop = false;
            this.FechaSalida.Text = "lunes, 11/abril/2022 01:47";
            this.FechaSalida.Value = new System.DateTime(2022, 4, 11, 13, 47, 19, 604);
            // 
            // TipoEstacion
            // 
            this.TipoEstacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoEstacion.Location = new System.Drawing.Point(283, 160);
            this.TipoEstacion.Name = "TipoEstacion";
            this.TipoEstacion.Size = new System.Drawing.Size(258, 20);
            this.TipoEstacion.TabIndex = 10;
            // 
            // NombreEstacion
            // 
            this.NombreEstacion.Location = new System.Drawing.Point(283, 113);
            this.NombreEstacion.MaxLength = 50;
            this.NombreEstacion.Name = "NombreEstacion";
            this.NombreEstacion.Size = new System.Drawing.Size(258, 20);
            this.NombreEstacion.TabIndex = 7;
            // 
            // NombreRemitenteDestinatario
            // 
            this.NombreRemitenteDestinatario.Location = new System.Drawing.Point(136, 68);
            this.NombreRemitenteDestinatario.MaxLength = 254;
            this.NombreRemitenteDestinatario.Name = "NombreRemitenteDestinatario";
            this.NombreRemitenteDestinatario.Size = new System.Drawing.Size(278, 20);
            this.NombreRemitenteDestinatario.TabIndex = 3;
            // 
            // NavegacionTrafico
            // 
            this.NavegacionTrafico.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.NavegacionTrafico.Location = new System.Drawing.Point(547, 113);
            this.NavegacionTrafico.Name = "NavegacionTrafico";
            this.NavegacionTrafico.Size = new System.Drawing.Size(121, 20);
            this.NavegacionTrafico.TabIndex = 8;
            // 
            // NumEstacion
            // 
            this.NumEstacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.NumEstacion.Location = new System.Drawing.Point(12, 113);
            this.NumEstacion.Name = "NumEstacion";
            this.NumEstacion.Size = new System.Drawing.Size(263, 20);
            this.NumEstacion.TabIndex = 6;
            // 
            // RFCRemitenteDestinatario
            // 
            this.RFCRemitenteDestinatario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RFCRemitenteDestinatario.Location = new System.Drawing.Point(12, 68);
            this.RFCRemitenteDestinatario.MaxLength = 14;
            this.RFCRemitenteDestinatario.Name = "RFCRemitenteDestinatario";
            this.RFCRemitenteDestinatario.Size = new System.Drawing.Size(118, 20);
            this.RFCRemitenteDestinatario.TabIndex = 2;
            this.RFCRemitenteDestinatario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IDUbicacion
            // 
            this.IDUbicacion.Location = new System.Drawing.Point(591, 18);
            this.IDUbicacion.MaxLength = 8;
            this.IDUbicacion.Name = "IDUbicacion";
            this.IDUbicacion.Size = new System.Drawing.Size(77, 20);
            this.IDUbicacion.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(280, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tipo estación:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(280, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nombre estación:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(134, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Nombre remitente:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(12, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 18);
            this.label9.TabIndex = 8;
            this.label9.Text = "Fecha salida ó llegada:*";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(544, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Distancia recorrida:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(544, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Navegación tráfico:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Núm. estación:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "RFC remitente:*";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(513, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Ubicación:";
            // 
            // TUbicacion
            // 
            this.TUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUbicacion.Etiqueta = "Ubicación:";
            this.TUbicacion.Location = new System.Drawing.Point(0, 0);
            this.TUbicacion.Name = "TUbicacion";
            this.TUbicacion.ShowActualizar = false;
            this.TUbicacion.ShowAutorizar = false;
            this.TUbicacion.ShowCerrar = true;
            this.TUbicacion.ShowEditar = false;
            this.TUbicacion.ShowExportarExcel = false;
            this.TUbicacion.ShowFiltro = false;
            this.TUbicacion.ShowGuardar = true;
            this.TUbicacion.ShowHerramientas = false;
            this.TUbicacion.ShowImagen = false;
            this.TUbicacion.ShowImprimir = false;
            this.TUbicacion.ShowNuevo = false;
            this.TUbicacion.ShowRemover = false;
            this.TUbicacion.Size = new System.Drawing.Size(681, 31);
            this.TUbicacion.TabIndex = 15;
            this.TUbicacion.Guardar.Click += this.TUbicacion_Guardar_Click;
            this.TUbicacion.Cerrar.Click += this.TUbicacion_Cerrar_Click;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.CodigoPostal);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.Calle);
            this.groupBox1.Controls.Add(this.Pais);
            this.groupBox1.Controls.Add(this.NumeroExterior);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Estado);
            this.groupBox1.Controls.Add(this.NumeroInterior);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.Municipio);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.Colonia);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.Referencia);
            this.groupBox1.Controls.Add(this.Localidad);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.HeaderText = "Domicilio";
            this.groupBox1.Location = new System.Drawing.Point(0, 224);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 173);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Domicilio";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(10, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 18);
            this.label13.TabIndex = 0;
            this.label13.Text = "Calle:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(10, 140);
            this.CodigoPostal.MaxLength = 5;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(100, 20);
            this.CodigoPostal.TabIndex = 7;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(348, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 18);
            this.label14.TabIndex = 1;
            this.label14.Text = "Núm. Exterior:";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(12, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 18);
            this.label15.TabIndex = 18;
            this.label15.Text = "Codigo Postal:*";
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(10, 40);
            this.Calle.MaxLength = 100;
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(333, 20);
            this.Calle.TabIndex = 0;
            // 
            // Pais
            // 
            this.Pais.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Pais.Location = new System.Drawing.Point(560, 40);
            this.Pais.MaxLength = 30;
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(105, 20);
            this.Pais.TabIndex = 3;
            // 
            // NumeroExterior
            // 
            this.NumeroExterior.Location = new System.Drawing.Point(348, 40);
            this.NumeroExterior.MaxLength = 55;
            this.NumeroExterior.Name = "NumeroExterior";
            this.NumeroExterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroExterior.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(560, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 18);
            this.label16.TabIndex = 16;
            this.label16.Text = "País:*";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(454, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 18);
            this.label17.TabIndex = 4;
            this.label17.Text = "Núm. Interior:";
            // 
            // Estado
            // 
            this.Estado.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Estado.Location = new System.Drawing.Point(10, 90);
            this.Estado.MaxLength = 30;
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(206, 20);
            this.Estado.TabIndex = 4;
            // 
            // NumeroInterior
            // 
            this.NumeroInterior.Location = new System.Drawing.Point(454, 40);
            this.NumeroInterior.MaxLength = 55;
            this.NumeroInterior.Name = "NumeroInterior";
            this.NumeroInterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroInterior.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(10, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 18);
            this.label18.TabIndex = 14;
            this.label18.Text = "Estado:*";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(117, 116);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 18);
            this.label19.TabIndex = 6;
            this.label19.Text = "Colonia:";
            // 
            // Municipio
            // 
            this.Municipio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Municipio.Location = new System.Drawing.Point(222, 90);
            this.Municipio.MaxLength = 120;
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(205, 20);
            this.Municipio.TabIndex = 5;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(219, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 18);
            this.label20.TabIndex = 12;
            this.label20.Text = "Municipio:";
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(117, 140);
            this.Colonia.MaxLength = 120;
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(208, 20);
            this.Colonia.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(430, 66);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 18);
            this.label21.TabIndex = 8;
            this.label21.Text = "Localidad:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(330, 140);
            this.Referencia.MaxLength = 250;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(335, 20);
            this.Referencia.TabIndex = 9;
            // 
            // Localidad
            // 
            this.Localidad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Localidad.Location = new System.Drawing.Point(433, 90);
            this.Localidad.MaxLength = 120;
            this.Localidad.Name = "Localidad";
            this.Localidad.Size = new System.Drawing.Size(232, 20);
            this.Localidad.TabIndex = 6;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(330, 116);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 18);
            this.label22.TabIndex = 10;
            this.label22.Text = "Referencia:";
            // 
            // providerError
            // 
            this.providerError.ContainerControl = this;
            // 
            // CartaPorteUbicacionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 398);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gUbicacion);
            this.Controls.Add(this.TUbicacion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CartaPorteUbicacionControl";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ubicación";
            this.Load += new System.EventHandler(this.CartaPorteUbicacionControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gUbicacion)).EndInit();
            this.gUbicacion.ResumeLayout(false);
            this.gUbicacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoUbicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscalFigura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoEstacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreEstacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreRemitenteDestinatario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NavegacionTrafico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumEstacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCRemitenteDestinatario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDUbicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroInterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Municipio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Colonia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Localidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.providerError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox gUbicacion;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel label7;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label11;
        private Telerik.WinControls.UI.RadLabel label12;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel label13;
        private Telerik.WinControls.UI.RadLabel label14;
        private Telerik.WinControls.UI.RadLabel label15;
        private Telerik.WinControls.UI.RadLabel label16;
        private Telerik.WinControls.UI.RadLabel label17;
        private Telerik.WinControls.UI.RadLabel label18;
        private Telerik.WinControls.UI.RadLabel label19;
        private Telerik.WinControls.UI.RadLabel label20;
        private Telerik.WinControls.UI.RadLabel label21;
        private Telerik.WinControls.UI.RadLabel label22;
        private System.Windows.Forms.ErrorProvider providerError;
        protected internal Telerik.WinControls.UI.RadDateTimePicker FechaSalida;
        protected internal Telerik.WinControls.UI.RadTextBox NombreEstacion;
        protected internal Telerik.WinControls.UI.RadTextBox NombreRemitenteDestinatario;
        protected internal Telerik.WinControls.UI.RadDropDownList TipoEstacion;
        protected internal Telerik.WinControls.UI.RadDropDownList NavegacionTrafico;
        protected internal Telerik.WinControls.UI.RadDropDownList NumEstacion;
        protected internal Telerik.WinControls.UI.RadTextBox RFCRemitenteDestinatario;
        protected internal Telerik.WinControls.UI.RadTextBox IDUbicacion;
        protected internal Telerik.WinControls.UI.RadDropDownList TipoUbicacion;
        protected internal Telerik.WinControls.UI.RadTextBox NumRegIdTrib;
        protected internal Telerik.WinControls.UI.RadDropDownList ResidenciaFiscalFigura;
        protected internal Telerik.WinControls.UI.RadSpinEditor TotalDistRec;
        protected internal Common.Forms.ToolBarStandarControl TUbicacion;
        protected internal Telerik.WinControls.UI.RadTextBox CodigoPostal;
        protected internal Telerik.WinControls.UI.RadTextBox Calle;
        protected internal Telerik.WinControls.UI.RadDropDownList Pais;
        protected internal Telerik.WinControls.UI.RadTextBox NumeroExterior;
        protected internal Telerik.WinControls.UI.RadDropDownList Estado;
        protected internal Telerik.WinControls.UI.RadTextBox NumeroInterior;
        protected internal Telerik.WinControls.UI.RadDropDownList Municipio;
        protected internal Telerik.WinControls.UI.RadDropDownList Colonia;
        protected internal Telerik.WinControls.UI.RadTextBox Referencia;
        protected internal Telerik.WinControls.UI.RadDropDownList Localidad;
    }
}
