﻿using System;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class CartaPorteUbicacionControl : RadForm {
        protected bool editar = false;
        protected CartaPorteUbicacion _Ubicacion;
        protected ICveTipoEstacionCatalogo cveTipoEstacion;
        protected IClaveEstacionesCatalogo claveEstacionesCatalogo;
        protected ICveEstadoCatalogo cveEstadoCatalogo;
        protected IClaveMunicipioCatalogo claveMunicipioCatalogo;
        protected IClaveLocalidadCatalogo claveLocalidadCatalogo;
        protected IClaveColoniaCatalogo claveColoniaCatalogo;
        protected System.Collections.Generic.List<Catalogos.Entities.ClavePais> Paises;
        private string _cveViaEntradaSalida = "01";
        public event EventHandler<CartaPorteUbicacion> CartaPorteUbicacionEdit;
        public event EventHandler<CartaPorteUbicacion> CartaPorteUbicacionAdd;
        private bool bSupressEstadoCode;

        public void OnAdd(CartaPorteUbicacion ubicacion) {
            if (this.CartaPorteUbicacionAdd != null)
                this.CartaPorteUbicacionAdd(this, ubicacion);
        }

        public void OnEdit(CartaPorteUbicacion ubicacion) {
            if (this.CartaPorteUbicacionEdit != null)
                this.CartaPorteUbicacionEdit(this, ubicacion);
        }

        public CartaPorteUbicacionControl(CartaPorteUbicacion cartaPorteUbicacion) {
            InitializeComponent();
            if (cartaPorteUbicacion == null) {
                this._Ubicacion = new CartaPorteUbicacion();
            } else {
                this.editar = true;
                this._Ubicacion = cartaPorteUbicacion;
            }
        }

        public string ViaEntradaSalida {
            get { return this._cveViaEntradaSalida; }
            set {
                this._cveViaEntradaSalida = value;
                this.SetUbicacion(value);
            }
        }

        private void CartaPorteUbicacionControl_Load(object sender, EventArgs e) {
            this.Paises = new System.Collections.Generic.List<Catalogos.Entities.ClavePais>() {
                new Catalogos.Entities.ClavePais { Clave = "MEX" }, 
                new Catalogos.Entities.ClavePais { Clave = "USA" }, 
                new Catalogos.Entities.ClavePais { Clave = "CAN" } 
            };

            this.cveEstadoCatalogo = new CartaPorteEstadosCatalogo();
            this.cveEstadoCatalogo.Load();

            this.claveMunicipioCatalogo = new MunicipioCatalogo();
            this.claveMunicipioCatalogo.Load();

            this.claveLocalidadCatalogo = new LocalidadCatalogo();
            this.claveLocalidadCatalogo.Load();

            this.claveColoniaCatalogo = new ColoniaCatalogo();
            this.claveColoniaCatalogo.Load();

            this.cveTipoEstacion = new TipoEstacionCatalogo();
            this.cveTipoEstacion.Load();
            this.TipoEstacion.DisplayMember = "Descriptor";
            this.TipoEstacion.ValueMember = "Clave";
            this.TipoEstacion.DataSource = this.cveTipoEstacion.Items;

            this.claveEstacionesCatalogo = new EstacionesCatalogo();
            this.claveEstacionesCatalogo.Load();
            this.NumEstacion.DisplayMember = "Descriptor";
            this.NumEstacion.ValueMember = "Clave";
            this.NumEstacion.DataSource = this.claveEstacionesCatalogo.Items;

            this.Pais.SelectedValueChanged += this.Pais_SelectedValueChanged;
            this.Pais.ValueMember = "Clave";
            this.Pais.DisplayMember = "Clave";
            this.Pais.DataSource = this.Paises;

            this.Estado.DropDownListElement.SyncSelectionWithText = false;
            this.Estado.DisplayMember = "Descriptor";
            this.Estado.ValueMember = "Clave";
            this.bSupressEstadoCode = true;
            this.Estado.SelectedValueChanged += this.Estado_SelectedValueChanged;
            this.bSupressEstadoCode = false;

            this.Municipio.DisplayMember = "Descriptor";
            this.Municipio.ValueMember = "Clave";

            this.Localidad.DisplayMember = "Descriptor";
            this.Localidad.ValueMember = "Clave";

            this.Colonia.ValueMember = "Clave";
            this.Colonia.DisplayMember = "Descriptor";

            this.CodigoPostal.TextChanged += this.CodigoPostal_TextChanged;
            this.CreateBinding();
        }

        private void CodigoPostal_TextChanged(object sender, EventArgs e) {
            if (ValidacionService.CodigoPostal(this.CodigoPostal.Text)) {
                this._Ubicacion.Domicilio.CodigoPostal = this.CodigoPostal.Text;
                this.Colonia.DataSource = this.claveColoniaCatalogo.Items.Where(it => it.CodigoPostal == this.CodigoPostal.Text);
            }
        }

        private void Pais_SelectedValueChanged(object sender, EventArgs e) {
            this.bSupressEstadoCode = true;
            this.Estado.DataSource = this.cveEstadoCatalogo.Items.Where(it => it.ClavePais == this.Pais.SelectedValue.ToString());
            this.Estado.DisplayMember = "Descriptor";
            this.Estado.ValueMember = "Clave";
            this.bSupressEstadoCode = false;
        }

        private void Estado_SelectedValueChanged(object sender, EventArgs e) {
            if (this.bSupressEstadoCode == false) {
                var c = (Telerik.WinControls.UI.Data.ValueChangedEventArgs)e;
                if (c.OldValue == null && c.NewValue == null)
                    return;
                var f = c.OldValue == null ? c.NewValue.ToString() : c.OldValue.ToString();
                if (c.NewValue != null) {
                    f = c.NewValue.ToString();
                }
                this._Ubicacion.Domicilio.Estado = f;
                this.Municipio.DataSource = this.claveMunicipioCatalogo.Items.Where(it => it.Estado == f);
                this.Localidad.DataSource = this.claveLocalidadCatalogo.Items.Where(it => it.Estado == f);
            }
        }

        private void CreateBinding() {
            this.TipoUbicacion.DataBindings.Clear();
            this.TipoUbicacion.DataBindings.Add("Text", this._Ubicacion, "TipoUbicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IDUbicacion.DataBindings.Clear();
            this.IDUbicacion.DataBindings.Add("Text", this._Ubicacion, "IDUbicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RFCRemitenteDestinatario.DataBindings.Clear();
            this.RFCRemitenteDestinatario.DataBindings.Add("Text", this._Ubicacion, "RFCRemitenteDestinatario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NombreRemitenteDestinatario.DataBindings.Clear();
            this.NombreRemitenteDestinatario.DataBindings.Add("Text", this._Ubicacion, "NombreRemitenteDestinatario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumRegIdTrib.DataBindings.Clear();
            this.NumRegIdTrib.DataBindings.Add("Text", this._Ubicacion, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ResidenciaFiscalFigura.DataBindings.Clear();
            this.ResidenciaFiscalFigura.DataBindings.Add("SelectedValue", this._Ubicacion, "TipoUbicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaSalida.DataBindings.Clear();
            this.FechaSalida.DataBindings.Add("Value", this._Ubicacion, "FechaHoraSalidaLlegada", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumEstacion.DataBindings.Clear();
            this.NumEstacion.DataBindings.Add("SelectedValue", this._Ubicacion, "NumEstacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NombreEstacion.DataBindings.Clear();
            this.NombreEstacion.DataBindings.Add("Text", this._Ubicacion, "NombreEstacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NavegacionTrafico.DataBindings.Clear();
            this.NavegacionTrafico.DataBindings.Add("Text", this._Ubicacion, "NavegacionTrafico", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoEstacion.DataBindings.Clear();
            this.TipoEstacion.DataBindings.Add("SelectedValue", this._Ubicacion, "TipoEstacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TotalDistRec.DataBindings.Clear();
            this.TotalDistRec.DataBindings.Add("Value", this._Ubicacion, "DistanciaRecorrida", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Calle.DataBindings.Clear();
            this.Calle.DataBindings.Add("Text", this._Ubicacion.Domicilio, "Calle", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumeroExterior.DataBindings.Clear();
            this.NumeroExterior.DataBindings.Add("Text", this._Ubicacion.Domicilio, "NumeroExterior", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumeroInterior.DataBindings.Clear();
            this.NumeroInterior.DataBindings.Add("Text", this._Ubicacion.Domicilio, "NumeroInterior", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Pais.DataBindings.Clear();
            this.Pais.DataBindings.Add("Text", this._Ubicacion.Domicilio, "Pais", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Estado.DataBindings.Clear();
            this.Estado.DataBindings.Add("SelectedValue", this._Ubicacion.Domicilio, "Estado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Municipio.DataBindings.Clear();
            this.Municipio.DataBindings.Add("SelectedValue", this._Ubicacion.Domicilio, "Municipio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Localidad.DataBindings.Clear();
            this.Localidad.DataBindings.Add("SelectedValue", this._Ubicacion.Domicilio, "Localidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CodigoPostal.DataBindings.Clear();
            this.CodigoPostal.DataBindings.Add("Text", this._Ubicacion.Domicilio, "CodigoPostal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Colonia.DataBindings.Clear();
            this.Colonia.DataBindings.Add("SelectedValue", this._Ubicacion.Domicilio, "Colonia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Referencia.DataBindings.Clear();
            this.Referencia.DataBindings.Add("Text", this._Ubicacion.Domicilio, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void TUbicacion_Guardar_Click(object sender, EventArgs e) {
            if (this.Verifica()) {
                if (this.editar == false)
                    this.OnAdd(this._Ubicacion);
                else {
                    this.OnEdit(this._Ubicacion);
                }
            }
        }

        private void TUbicacion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual bool Verifica() {
            this.providerError.Clear();
            if (string.IsNullOrEmpty(this._Ubicacion.TipoUbicacion)) {
                this.providerError.SetError(this.TipoUbicacion, "No se especifico un tipo de ubicación");
                return false;
            }

            if (string.IsNullOrEmpty(this._Ubicacion.RFCRemitenteDestinatario)) {
                this.providerError.SetError(this.RFCRemitenteDestinatario, "El RFC es un dato requerido");
                return false;
            }

            if (ValidacionService.RFC(this._Ubicacion.RFCRemitenteDestinatario) == false) {
                this.providerError.SetError(this.RFCRemitenteDestinatario, "El RFC es un dato no válido");
                return false;
            }

            if (string.IsNullOrEmpty(this._Ubicacion.Domicilio.Pais)) {
                this.providerError.SetError(this.Pais, "Requerido registrar la clave del país en donde se encuentra ubicado el domicilio del origen y/o destino de los bienes y/o mercancías que se trasladan");
                return false;
            }

            if (string.IsNullOrEmpty(this._Ubicacion.Domicilio.CodigoPostal)) {
                this.providerError.SetError(this.CodigoPostal, "Requerido registrar el código postal en donde se encuentra el domicilio del origen y/o destino de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.");
                return false;
            }

            if (string.IsNullOrEmpty(this._Ubicacion.Domicilio.Estado)) {
                this.providerError.SetError(this.Estado, "Requerido registrar el estado, entidad, región, comunidad, o dato análogo en donde se encuentra ubicado el domicilio del origen y/o destino de los bienes y/o mercancías que se trasladan en los distintos medios de transporte");
                return false;
            }
            return true;
        }

        public virtual void SetUbicacion(string value) {
            switch (value) {
                case "01":
                    this.NumEstacion.Enabled = false;
                    this.NombreEstacion.Enabled = false;
                    this.TipoEstacion.Enabled = false;
                    this.NavegacionTrafico.Enabled = false;
                    break;
                default:
                    break;
            }
        }
    }
}
