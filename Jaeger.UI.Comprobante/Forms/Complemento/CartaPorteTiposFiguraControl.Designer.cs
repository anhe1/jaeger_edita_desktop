﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class CartaPorteTiposFiguraControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CartaPorteTiposFiguraControl));
            this.TipoFigura = new Telerik.WinControls.UI.RadDropDownList();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.RFCFigura = new Telerik.WinControls.UI.RadTextBox();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.NumLicencia = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.NombreFigura = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.NumRegIdTribFigura = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.ResidenciaFiscalFigura = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.TFigura = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabControl1 = new Telerik.WinControls.UI.RadPageView();
            this.tabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Domicilio = new Jaeger.UI.Comprobante.Forms.DomicilioFiscalControl();
            this.tabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.TParte = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.TipoFigura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCFigura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumLicencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreFigura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTribFigura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscalFigura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TipoFigura
            // 
            this.TipoFigura.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoFigura.Location = new System.Drawing.Point(6, 32);
            this.TipoFigura.Name = "TipoFigura";
            this.TipoFigura.Size = new System.Drawing.Size(121, 20);
            this.TipoFigura.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tipo:";
            // 
            // RFCFigura
            // 
            this.RFCFigura.Location = new System.Drawing.Point(387, 32);
            this.RFCFigura.Name = "RFCFigura";
            this.RFCFigura.Size = new System.Drawing.Size(121, 20);
            this.RFCFigura.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(387, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 18);
            this.label11.TabIndex = 20;
            this.label11.Text = "RFC";
            // 
            // NumLicencia
            // 
            this.NumLicencia.Location = new System.Drawing.Point(260, 73);
            this.NumLicencia.Name = "NumLicencia";
            this.NumLicencia.Size = new System.Drawing.Size(121, 20);
            this.NumLicencia.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(260, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 18);
            this.label1.TabIndex = 22;
            this.label1.Text = "Núm. Licencia:";
            // 
            // NombreFigura
            // 
            this.NombreFigura.Location = new System.Drawing.Point(133, 32);
            this.NombreFigura.Name = "NombreFigura";
            this.NombreFigura.Size = new System.Drawing.Size(248, 20);
            this.NombreFigura.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(133, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 18);
            this.label3.TabIndex = 24;
            this.label3.Text = "Nombre:";
            // 
            // NumRegIdTribFigura
            // 
            this.NumRegIdTribFigura.Location = new System.Drawing.Point(514, 32);
            this.NumRegIdTribFigura.Name = "NumRegIdTribFigura";
            this.NumRegIdTribFigura.Size = new System.Drawing.Size(121, 20);
            this.NumRegIdTribFigura.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(514, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 18);
            this.label4.TabIndex = 26;
            this.label4.Text = "Núm. Reg Id Tributario";
            // 
            // ResidenciaFiscalFigura
            // 
            this.ResidenciaFiscalFigura.Location = new System.Drawing.Point(133, 73);
            this.ResidenciaFiscalFigura.Name = "ResidenciaFiscalFigura";
            this.ResidenciaFiscalFigura.Size = new System.Drawing.Size(121, 20);
            this.ResidenciaFiscalFigura.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(133, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 18);
            this.label5.TabIndex = 28;
            this.label5.Text = "Residencia Fiscal";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ResidenciaFiscalFigura);
            this.groupBox1.Controls.Add(this.TipoFigura);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.NumRegIdTribFigura);
            this.groupBox1.Controls.Add(this.RFCFigura);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NombreFigura);
            this.groupBox1.Controls.Add(this.NumLicencia);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.HeaderText = "Figura de Transporte";
            this.groupBox1.Location = new System.Drawing.Point(0, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(702, 101);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Figura de Transporte";
            // 
            // TFigura
            // 
            this.TFigura.Dock = System.Windows.Forms.DockStyle.Top;
            this.TFigura.Etiqueta = "";
            this.TFigura.Location = new System.Drawing.Point(0, 0);
            this.TFigura.Name = "TFigura";
            this.TFigura.ShowActualizar = false;
            this.TFigura.ShowAutorizar = false;
            this.TFigura.ShowCerrar = true;
            this.TFigura.ShowEditar = false;
            this.TFigura.ShowExportarExcel = false;
            this.TFigura.ShowFiltro = false;
            this.TFigura.ShowGuardar = true;
            this.TFigura.ShowHerramientas = false;
            this.TFigura.ShowImagen = false;
            this.TFigura.ShowImprimir = false;
            this.TFigura.ShowNuevo = false;
            this.TFigura.ShowRemover = false;
            this.TFigura.Size = new System.Drawing.Size(702, 31);
            this.TFigura.TabIndex = 31;
            this.TFigura.Guardar.Click += this.TFigura_ButtonGuardar_Click;
            this.TFigura.Cerrar.Click += this.TFigura_ButtonCerrar_Click;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.DefaultPage = this.tabPage1;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 132);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedPage = this.tabPage1;
            this.tabControl1.Size = new System.Drawing.Size(702, 218);
            this.tabControl1.TabIndex = 33;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Domicilio);
            this.tabPage1.ItemSize = new System.Drawing.SizeF(63F, 28F);
            this.tabPage1.Location = new System.Drawing.Point(10, 37);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(681, 170);
            this.tabPage1.Text = "Domicilio";
            // 
            // Domicilio
            // 
            this.Domicilio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Domicilio.Location = new System.Drawing.Point(3, 3);
            this.Domicilio.Name = "Domicilio";
            this.Domicilio.Size = new System.Drawing.Size(675, 164);
            this.Domicilio.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.TParte);
            this.tabPage2.ItemSize = new System.Drawing.SizeF(47F, 28F);
            this.tabPage2.Location = new System.Drawing.Point(10, 37);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(681, 170);
            this.tabPage2.Text = "Partes";
            // 
            // TParte
            // 
            this.TParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TParte.Etiqueta = "";
            this.TParte.Location = new System.Drawing.Point(3, 3);
            this.TParte.Name = "TParte";
            this.TParte.ShowActualizar = false;
            this.TParte.ShowAutorizar = false;
            this.TParte.ShowCerrar = false;
            this.TParte.ShowEditar = false;
            this.TParte.ShowExportarExcel = false;
            this.TParte.ShowFiltro = true;
            this.TParte.ShowGuardar = false;
            this.TParte.ShowHerramientas = false;
            this.TParte.ShowImagen = false;
            this.TParte.ShowImprimir = false;
            this.TParte.ShowNuevo = false;
            this.TParte.ShowRemover = false;
            this.TParte.Size = new System.Drawing.Size(675, 30);
            this.TParte.TabIndex = 0;
            // 
            // CartaPorteTiposFiguraControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 350);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TFigura);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CartaPorteTiposFiguraControl";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tipos de Figura";
            this.Load += new System.EventHandler(this.CartaPorteTiposFiguraControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TipoFigura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCFigura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumLicencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreFigura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRegIdTribFigura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResidenciaFiscalFigura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadDropDownList TipoFigura;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadTextBox RFCFigura;
        private Telerik.WinControls.UI.RadLabel label11;
        private Telerik.WinControls.UI.RadTextBox NumLicencia;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadTextBox NombreFigura;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadTextBox NumRegIdTribFigura;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadTextBox ResidenciaFiscalFigura;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Common.Forms.ToolBarStandarControl TFigura;
        private Telerik.WinControls.UI.RadPageView tabControl1;
        private Telerik.WinControls.UI.RadPageViewPage tabPage1;
        private Telerik.WinControls.UI.RadPageViewPage tabPage2;
        private Common.Forms.ToolBarStandarControl TParte;
        private DomicilioFiscalControl Domicilio;
    }
}
