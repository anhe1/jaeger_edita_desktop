﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class CartaPorteMercanciasMercanciaControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.tabControl1 = new Telerik.WinControls.UI.RadPageView();
            this.tabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.BuscarUnidadPeso = new Telerik.WinControls.UI.RadButton();
            this.DetalleMercanciaSpecified = new Telerik.WinControls.UI.RadCheckBox();
            this.NumPiezas = new Telerik.WinControls.UI.RadSpinEditor();
            this.label17 = new Telerik.WinControls.UI.RadLabel();
            this.label21 = new Telerik.WinControls.UI.RadLabel();
            this.label22 = new Telerik.WinControls.UI.RadLabel();
            this.label23 = new Telerik.WinControls.UI.RadLabel();
            this.label24 = new Telerik.WinControls.UI.RadLabel();
            this.UnidadPesoMerc = new Telerik.WinControls.UI.RadDropDownList();
            this.PesoNeto = new Telerik.WinControls.UI.RadSpinEditor();
            this.PesoBruto = new Telerik.WinControls.UI.RadSpinEditor();
            this.PesoTara = new Telerik.WinControls.UI.RadSpinEditor();
            this.tabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridCantidadTransporte = new Telerik.WinControls.UI.RadGridView();
            this.TCantidad = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridGuias = new Telerik.WinControls.UI.RadGridView();
            this.TGuia = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.tabPage9 = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridPedimentos = new Telerik.WinControls.UI.RadGridView();
            this.TPedimento = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.BuscarMaterialPeligroso = new Telerik.WinControls.UI.RadButton();
            this.BuscarEmbalaje = new Telerik.WinControls.UI.RadButton();
            this.BuscarUnidad = new Telerik.WinControls.UI.RadButton();
            this.BuscarProducto = new Telerik.WinControls.UI.RadButton();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.CveMaterialPeligroso = new Telerik.WinControls.UI.RadDropDownList();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.label16 = new Telerik.WinControls.UI.RadLabel();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.ClaveSTCC = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.UUIDComercioExt = new Telerik.WinControls.UI.RadTextBox();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.FraccionArancelaria = new Telerik.WinControls.UI.RadDropDownList();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.DescripEmbalaje = new Telerik.WinControls.UI.RadTextBox();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.Embalaje = new Telerik.WinControls.UI.RadTextBox();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.Dimensiones = new Telerik.WinControls.UI.RadTextBox();
            this.Moneda = new Telerik.WinControls.UI.RadDropDownList();
            this.label13 = new Telerik.WinControls.UI.RadLabel();
            this.ValorMercancia = new Telerik.WinControls.UI.RadSpinEditor();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.PesoEnKg = new Telerik.WinControls.UI.RadSpinEditor();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.MaterialPeligroso = new Telerik.WinControls.UI.RadCheckBox();
            this.BienesTransp = new Telerik.WinControls.UI.RadTextBox();
            this.Unidad = new Telerik.WinControls.UI.RadTextBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.ClaveUnidad = new Telerik.WinControls.UI.RadTextBox();
            this.Cantidad = new Telerik.WinControls.UI.RadSpinEditor();
            this.ProviderError = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarUnidadPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetalleMercanciaSpecified)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPiezas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnidadPesoMerc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoNeto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoTara)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCantidadTransporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCantidadTransporte.MasterTemplate)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGuias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridGuias.MasterTemplate)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPedimentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPedimentos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarMaterialPeligroso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarEmbalaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CveMaterialPeligroso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSTCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UUIDComercioExt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FraccionArancelaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripEmbalaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embalaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dimensiones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValorMercancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoEnKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialPeligroso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BienesTransp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProviderError)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.DefaultPage = this.tabPage1;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 216);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedPage = this.tabPage1;
            this.tabControl1.Size = new System.Drawing.Size(580, 198);
            this.tabControl1.TabIndex = 38;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabControl1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.BuscarUnidadPeso);
            this.tabPage1.Controls.Add(this.DetalleMercanciaSpecified);
            this.tabPage1.Controls.Add(this.NumPiezas);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.UnidadPesoMerc);
            this.tabPage1.Controls.Add(this.PesoNeto);
            this.tabPage1.Controls.Add(this.PesoBruto);
            this.tabPage1.Controls.Add(this.PesoTara);
            this.tabPage1.ItemSize = new System.Drawing.SizeF(56F, 28F);
            this.tabPage1.Location = new System.Drawing.Point(10, 37);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(559, 150);
            this.tabPage1.Text = "Detalles";
            // 
            // BuscarUnidadPeso
            // 
            this.BuscarUnidadPeso.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.BuscarUnidadPeso.Location = new System.Drawing.Point(405, 33);
            this.BuscarUnidadPeso.Name = "BuscarUnidadPeso";
            this.BuscarUnidadPeso.Size = new System.Drawing.Size(23, 23);
            this.BuscarUnidadPeso.TabIndex = 42;
            this.BuscarUnidadPeso.Click += new System.EventHandler(this.BuscarUnidadPeso_Click);
            // 
            // DetalleMercanciaSpecified
            // 
            this.DetalleMercanciaSpecified.Location = new System.Drawing.Point(451, 13);
            this.DetalleMercanciaSpecified.Name = "DetalleMercanciaSpecified";
            this.DetalleMercanciaSpecified.Size = new System.Drawing.Size(87, 18);
            this.DetalleMercanciaSpecified.TabIndex = 41;
            this.DetalleMercanciaSpecified.Text = "Incluir detalle";
            this.DetalleMercanciaSpecified.CheckStateChanged += new System.EventHandler(this.DetalleMercanciaSpecified_CheckedChanged);
            // 
            // NumPiezas
            // 
            this.NumPiezas.Enabled = false;
            this.NumPiezas.Location = new System.Drawing.Point(350, 77);
            this.NumPiezas.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.NumPiezas.Name = "NumPiezas";
            this.NumPiezas.Size = new System.Drawing.Size(77, 20);
            this.NumPiezas.TabIndex = 40;
            this.NumPiezas.TabStop = false;
            this.NumPiezas.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(347, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 18);
            this.label17.TabIndex = 39;
            this.label17.Text = "Núm. de Piezas";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(123, 58);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 18);
            this.label21.TabIndex = 31;
            this.label21.Text = "Peso Neto";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(8, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(87, 18);
            this.label22.TabIndex = 32;
            this.label22.Text = "Unidad de Peso:";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(237, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 18);
            this.label23.TabIndex = 33;
            this.label23.Text = "Peso Tara";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(8, 58);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 18);
            this.label24.TabIndex = 34;
            this.label24.Text = "Peso Bruto";
            // 
            // UnidadPesoMerc
            // 
            this.UnidadPesoMerc.Enabled = false;
            this.UnidadPesoMerc.Location = new System.Drawing.Point(8, 34);
            this.UnidadPesoMerc.Name = "UnidadPesoMerc";
            this.UnidadPesoMerc.Size = new System.Drawing.Size(395, 20);
            this.UnidadPesoMerc.TabIndex = 38;
            // 
            // PesoNeto
            // 
            this.PesoNeto.DecimalPlaces = 2;
            this.PesoNeto.Enabled = false;
            this.PesoNeto.Location = new System.Drawing.Point(123, 77);
            this.PesoNeto.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PesoNeto.Name = "PesoNeto";
            this.PesoNeto.Size = new System.Drawing.Size(105, 20);
            this.PesoNeto.TabIndex = 37;
            this.PesoNeto.TabStop = false;
            this.PesoNeto.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PesoBruto
            // 
            this.PesoBruto.DecimalPlaces = 2;
            this.PesoBruto.Enabled = false;
            this.PesoBruto.Location = new System.Drawing.Point(8, 77);
            this.PesoBruto.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PesoBruto.Name = "PesoBruto";
            this.PesoBruto.Size = new System.Drawing.Size(107, 20);
            this.PesoBruto.TabIndex = 36;
            this.PesoBruto.TabStop = false;
            this.PesoBruto.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PesoTara
            // 
            this.PesoTara.DecimalPlaces = 2;
            this.PesoTara.Enabled = false;
            this.PesoTara.Location = new System.Drawing.Point(237, 77);
            this.PesoTara.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PesoTara.Name = "PesoTara";
            this.PesoTara.Size = new System.Drawing.Size(107, 20);
            this.PesoTara.TabIndex = 35;
            this.PesoTara.TabStop = false;
            this.PesoTara.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridCantidadTransporte);
            this.tabPage2.Controls.Add(this.TCantidad);
            this.tabPage2.ItemSize = new System.Drawing.SizeF(118F, 28F);
            this.tabPage2.Location = new System.Drawing.Point(10, 37);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(559, 147);
            this.tabPage2.Text = "Cantidad Transporte";
            // 
            // gridCantidadTransporte
            // 
            this.gridCantidadTransporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCantidadTransporte.Location = new System.Drawing.Point(0, 25);
            // 
            // 
            // 
            this.gridCantidadTransporte.MasterTemplate.AllowAddNewRow = false;
            this.gridCantidadTransporte.MasterTemplate.AllowCellContextMenu = false;
            this.gridCantidadTransporte.MasterTemplate.AllowColumnChooser = false;
            this.gridCantidadTransporte.MasterTemplate.AllowColumnReorder = false;
            this.gridCantidadTransporte.MasterTemplate.AllowDragToGroup = false;
            this.gridCantidadTransporte.MasterTemplate.AllowRowResize = false;
            this.gridCantidadTransporte.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Cantidad";
            gridViewTextBoxColumn1.HeaderText = "Cantidad";
            gridViewTextBoxColumn1.Name = "Cantidad";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "IDOrigen";
            gridViewTextBoxColumn2.HeaderText = "IDOrigen";
            gridViewTextBoxColumn2.Name = "IDOrigen";
            gridViewTextBoxColumn2.Width = 85;
            gridViewTextBoxColumn3.FieldName = "IDDestino";
            gridViewTextBoxColumn3.HeaderText = "IDDestino";
            gridViewTextBoxColumn3.Name = "IDDestino";
            gridViewTextBoxColumn3.Width = 85;
            gridViewTextBoxColumn4.FieldName = "CvesTransporte";
            gridViewTextBoxColumn4.HeaderText = "CvesTransporte";
            gridViewTextBoxColumn4.Name = "CvesTransporte";
            gridViewTextBoxColumn4.Width = 90;
            this.gridCantidadTransporte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.gridCantidadTransporte.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridCantidadTransporte.Name = "gridCantidadTransporte";
            this.gridCantidadTransporte.ShowGroupPanel = false;
            this.gridCantidadTransporte.Size = new System.Drawing.Size(559, 122);
            this.gridCantidadTransporte.TabIndex = 35;
            // 
            // TCantidad
            // 
            this.TCantidad.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCantidad.Etiqueta = "";
            this.TCantidad.Location = new System.Drawing.Point(0, 0);
            this.TCantidad.Name = "TCantidad";
            this.TCantidad.ShowActualizar = false;
            this.TCantidad.ShowAutorizar = false;
            this.TCantidad.ShowCerrar = false;
            this.TCantidad.ShowEditar = false;
            this.TCantidad.ShowExportarExcel = false;
            this.TCantidad.ShowFiltro = true;
            this.TCantidad.ShowGuardar = false;
            this.TCantidad.ShowHerramientas = false;
            this.TCantidad.ShowImagen = false;
            this.TCantidad.ShowImprimir = false;
            this.TCantidad.ShowNuevo = true;
            this.TCantidad.ShowRemover = true;
            this.TCantidad.Size = new System.Drawing.Size(559, 25);
            this.TCantidad.TabIndex = 34;
            this.TCantidad.Remover.Click += this.TCantidad_Remover_Click;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.gridGuias);
            this.tabPage3.Controls.Add(this.TGuia);
            this.tabPage3.ItemSize = new System.Drawing.SizeF(44F, 28F);
            this.tabPage3.Location = new System.Drawing.Point(10, 37);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(559, 147);
            this.tabPage3.Text = "Guías";
            // 
            // gridGuias
            // 
            this.gridGuias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridGuias.Location = new System.Drawing.Point(0, 25);
            // 
            // 
            // 
            this.gridGuias.MasterTemplate.AllowAddNewRow = false;
            this.gridGuias.MasterTemplate.AllowCellContextMenu = false;
            this.gridGuias.MasterTemplate.AllowColumnChooser = false;
            this.gridGuias.MasterTemplate.AllowColumnReorder = false;
            this.gridGuias.MasterTemplate.AllowDragToGroup = false;
            this.gridGuias.MasterTemplate.AllowRowResize = false;
            this.gridGuias.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn5.FieldName = "NumeroGuiaIdentificacion";
            gridViewTextBoxColumn5.HeaderText = "NumeroGuiaIdentificacion";
            gridViewTextBoxColumn5.Name = "NumeroGuiaIdentificacion";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "DescripGuiaIdentificacion";
            gridViewTextBoxColumn6.HeaderText = "DescripGuiaIdentificacion";
            gridViewTextBoxColumn6.Name = "DescripGuiaIdentificacion";
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.FieldName = "PesoGuiaIdentificacion";
            gridViewTextBoxColumn7.HeaderText = "PesoGuiaIdentificacion";
            gridViewTextBoxColumn7.Name = "PesoGuiaIdentificacion";
            gridViewTextBoxColumn7.Width = 85;
            this.gridGuias.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.gridGuias.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridGuias.Name = "gridGuias";
            this.gridGuias.ShowGroupPanel = false;
            this.gridGuias.Size = new System.Drawing.Size(559, 122);
            this.gridGuias.TabIndex = 37;
            // 
            // TGuia
            // 
            this.TGuia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TGuia.Etiqueta = "";
            this.TGuia.Location = new System.Drawing.Point(0, 0);
            this.TGuia.Name = "TGuia";
            this.TGuia.ShowActualizar = false;
            this.TGuia.ShowAutorizar = false;
            this.TGuia.ShowCerrar = false;
            this.TGuia.ShowEditar = false;
            this.TGuia.ShowExportarExcel = false;
            this.TGuia.ShowFiltro = false;
            this.TGuia.ShowGuardar = false;
            this.TGuia.ShowHerramientas = false;
            this.TGuia.ShowImagen = false;
            this.TGuia.ShowImprimir = false;
            this.TGuia.ShowNuevo = true;
            this.TGuia.ShowRemover = true;
            this.TGuia.Size = new System.Drawing.Size(559, 25);
            this.TGuia.TabIndex = 36;
            this.TGuia.Remover.Click += this.TGuia_Remover_Click;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.gridPedimentos);
            this.tabPage9.Controls.Add(this.TPedimento);
            this.tabPage9.ItemSize = new System.Drawing.SizeF(75F, 28F);
            this.tabPage9.Location = new System.Drawing.Point(10, 37);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(559, 147);
            this.tabPage9.Text = "Pedimentos";
            // 
            // gridPedimentos
            // 
            this.gridPedimentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPedimentos.Location = new System.Drawing.Point(0, 25);
            // 
            // 
            // 
            this.gridPedimentos.MasterTemplate.AllowAddNewRow = false;
            this.gridPedimentos.MasterTemplate.AllowCellContextMenu = false;
            this.gridPedimentos.MasterTemplate.AllowColumnChooser = false;
            this.gridPedimentos.MasterTemplate.AllowColumnReorder = false;
            this.gridPedimentos.MasterTemplate.AllowDragToGroup = false;
            this.gridPedimentos.MasterTemplate.AllowRowResize = false;
            this.gridPedimentos.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn8.FieldName = "Pedimentos";
            gridViewTextBoxColumn8.HeaderText = "Pedimentos";
            gridViewTextBoxColumn8.Name = "Pedimentos";
            this.gridPedimentos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8});
            this.gridPedimentos.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridPedimentos.Name = "gridPedimentos";
            this.gridPedimentos.ShowGroupPanel = false;
            this.gridPedimentos.Size = new System.Drawing.Size(559, 122);
            this.gridPedimentos.TabIndex = 37;
            // 
            // TPedimento
            // 
            this.TPedimento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPedimento.Etiqueta = "";
            this.TPedimento.Location = new System.Drawing.Point(0, 0);
            this.TPedimento.Name = "TPedimento";
            this.TPedimento.ShowActualizar = false;
            this.TPedimento.ShowAutorizar = false;
            this.TPedimento.ShowCerrar = false;
            this.TPedimento.ShowEditar = false;
            this.TPedimento.ShowExportarExcel = false;
            this.TPedimento.ShowFiltro = false;
            this.TPedimento.ShowGuardar = false;
            this.TPedimento.ShowHerramientas = false;
            this.TPedimento.ShowImagen = false;
            this.TPedimento.ShowImprimir = false;
            this.TPedimento.ShowNuevo = true;
            this.TPedimento.ShowRemover = true;
            this.TPedimento.Size = new System.Drawing.Size(559, 25);
            this.TPedimento.TabIndex = 36;
            this.TPedimento.Remover.Click += this.TPedimento_Remover_Click;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.BuscarMaterialPeligroso);
            this.groupBox1.Controls.Add(this.BuscarEmbalaje);
            this.groupBox1.Controls.Add(this.BuscarUnidad);
            this.groupBox1.Controls.Add(this.BuscarProducto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.CveMaterialPeligroso);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ClaveSTCC);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.UUIDComercioExt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.FraccionArancelaria);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.DescripEmbalaje);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.Embalaje);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Dimensiones);
            this.groupBox1.Controls.Add(this.Moneda);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.ValorMercancia);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.PesoEnKg);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.MaterialPeligroso);
            this.groupBox1.Controls.Add(this.BienesTransp);
            this.groupBox1.Controls.Add(this.Unidad);
            this.groupBox1.Controls.Add(this.Descripcion);
            this.groupBox1.Controls.Add(this.ClaveUnidad);
            this.groupBox1.Controls.Add(this.Cantidad);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 216);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // BuscarMaterialPeligroso
            // 
            this.BuscarMaterialPeligroso.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.BuscarMaterialPeligroso.Location = new System.Drawing.Point(316, 119);
            this.BuscarMaterialPeligroso.Name = "BuscarMaterialPeligroso";
            this.BuscarMaterialPeligroso.Size = new System.Drawing.Size(23, 23);
            this.BuscarMaterialPeligroso.TabIndex = 36;
            this.BuscarMaterialPeligroso.Click += new System.EventHandler(this.BuscarMaterialPeligroso_Click);
            // 
            // BuscarEmbalaje
            // 
            this.BuscarEmbalaje.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.BuscarEmbalaje.Location = new System.Drawing.Point(316, 160);
            this.BuscarEmbalaje.Name = "BuscarEmbalaje";
            this.BuscarEmbalaje.Size = new System.Drawing.Size(23, 23);
            this.BuscarEmbalaje.TabIndex = 35;
            this.BuscarEmbalaje.Click += new System.EventHandler(this.BuscarEmbalaje_Click);
            // 
            // BuscarUnidad
            // 
            this.BuscarUnidad.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.BuscarUnidad.Location = new System.Drawing.Point(92, 72);
            this.BuscarUnidad.Name = "BuscarUnidad";
            this.BuscarUnidad.Size = new System.Drawing.Size(23, 23);
            this.BuscarUnidad.TabIndex = 34;
            this.BuscarUnidad.Click += new System.EventHandler(this.BuscarUnidad_Click);
            // 
            // BuscarProducto
            // 
            this.BuscarProducto.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.BuscarProducto.Location = new System.Drawing.Point(92, 30);
            this.BuscarProducto.Name = "BuscarProducto";
            this.BuscarProducto.Size = new System.Drawing.Size(23, 23);
            this.BuscarProducto.TabIndex = 33;
            this.BuscarProducto.Click += new System.EventHandler(this.BuscarProducto_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(2, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Clave*:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(119, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descripción*:";
            // 
            // CveMaterialPeligroso
            // 
            this.CveMaterialPeligroso.Enabled = false;
            this.CveMaterialPeligroso.Location = new System.Drawing.Point(46, 120);
            this.CveMaterialPeligroso.Name = "CveMaterialPeligroso";
            this.CveMaterialPeligroso.Size = new System.Drawing.Size(269, 20);
            this.CveMaterialPeligroso.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Clave unidad*:";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(3, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 18);
            this.label16.TabIndex = 31;
            this.label16.Text = "Clave:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(348, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dimensiones:";
            // 
            // ClaveSTCC
            // 
            this.ClaveSTCC.Location = new System.Drawing.Point(461, 120);
            this.ClaveSTCC.MaxLength = 7;
            this.ClaveSTCC.Name = "ClaveSTCC";
            this.ClaveSTCC.Size = new System.Drawing.Size(107, 20);
            this.ClaveSTCC.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(232, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Embalaje:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(461, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 18);
            this.label9.TabIndex = 29;
            this.label9.Text = "Clave STCC:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(236, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Peso Kg*:";
            // 
            // UUIDComercioExt
            // 
            this.UUIDComercioExt.Location = new System.Drawing.Point(112, 188);
            this.UUIDComercioExt.MaxLength = 36;
            this.UUIDComercioExt.Name = "UUIDComercioExt";
            this.UUIDComercioExt.Size = new System.Drawing.Size(227, 20);
            this.UUIDComercioExt.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(463, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Moneda:";
            // 
            // FraccionArancelaria
            // 
            this.FraccionArancelaria.Location = new System.Drawing.Point(6, 161);
            this.FraccionArancelaria.Name = "FraccionArancelaria";
            this.FraccionArancelaria.Size = new System.Drawing.Size(220, 20);
            this.FraccionArancelaria.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "Fracción arancelaria:";
            // 
            // DescripEmbalaje
            // 
            this.DescripEmbalaje.Location = new System.Drawing.Point(348, 161);
            this.DescripEmbalaje.MaxLength = 100;
            this.DescripEmbalaje.Name = "DescripEmbalaje";
            this.DescripEmbalaje.Size = new System.Drawing.Size(220, 20);
            this.DescripEmbalaje.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(461, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 18);
            this.label10.TabIndex = 9;
            this.label10.Text = "Cantidad*:";
            // 
            // Embalaje
            // 
            this.Embalaje.Location = new System.Drawing.Point(232, 161);
            this.Embalaje.Name = "Embalaje";
            this.Embalaje.Size = new System.Drawing.Size(83, 20);
            this.Embalaje.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(121, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 18);
            this.label11.TabIndex = 10;
            this.label11.Text = "Unidad:";
            // 
            // Dimensiones
            // 
            this.Dimensiones.Location = new System.Drawing.Point(348, 120);
            this.Dimensiones.Name = "Dimensiones";
            this.Dimensiones.Size = new System.Drawing.Size(107, 20);
            this.Dimensiones.TabIndex = 24;
            // 
            // Moneda
            // 
            radListDataItem1.Text = "MXN";
            radListDataItem2.Text = "USD";
            this.Moneda.Items.Add(radListDataItem1);
            this.Moneda.Items.Add(radListDataItem2);
            this.Moneda.Location = new System.Drawing.Point(461, 75);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(107, 20);
            this.Moneda.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(345, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 18);
            this.label13.TabIndex = 12;
            this.label13.Text = "Descripción embalaje:";
            // 
            // ValorMercancia
            // 
            this.ValorMercancia.DecimalPlaces = 2;
            this.ValorMercancia.Location = new System.Drawing.Point(348, 75);
            this.ValorMercancia.Name = "ValorMercancia";
            this.ValorMercancia.Size = new System.Drawing.Size(107, 20);
            this.ValorMercancia.TabIndex = 21;
            this.ValorMercancia.TabStop = false;
            this.ValorMercancia.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(347, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 18);
            this.label14.TabIndex = 13;
            this.label14.Text = "Valor mercancia:";
            // 
            // PesoEnKg
            // 
            this.PesoEnKg.DecimalPlaces = 3;
            this.PesoEnKg.Location = new System.Drawing.Point(234, 75);
            this.PesoEnKg.Name = "PesoEnKg";
            this.PesoEnKg.Size = new System.Drawing.Size(105, 20);
            this.PesoEnKg.TabIndex = 21;
            this.PesoEnKg.TabStop = false;
            this.PesoEnKg.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(6, 191);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 18);
            this.label15.TabIndex = 14;
            this.label15.Text = "UUID comercio ext:";
            // 
            // MaterialPeligroso
            // 
            this.MaterialPeligroso.Location = new System.Drawing.Point(6, 101);
            this.MaterialPeligroso.Name = "MaterialPeligroso";
            this.MaterialPeligroso.Size = new System.Drawing.Size(111, 18);
            this.MaterialPeligroso.TabIndex = 20;
            this.MaterialPeligroso.Text = "Material peligroso";
            this.MaterialPeligroso.CheckStateChanged += new System.EventHandler(this.MaterialPeligrosoSpecified_CheckedChanged);
            // 
            // BienesTransp
            // 
            this.BienesTransp.Location = new System.Drawing.Point(6, 32);
            this.BienesTransp.MaxLength = 8;
            this.BienesTransp.Name = "BienesTransp";
            this.BienesTransp.Size = new System.Drawing.Size(85, 20);
            this.BienesTransp.TabIndex = 15;
            this.BienesTransp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Unidad
            // 
            this.Unidad.Location = new System.Drawing.Point(119, 75);
            this.Unidad.MaxLength = 20;
            this.Unidad.Name = "Unidad";
            this.Unidad.Size = new System.Drawing.Size(107, 20);
            this.Unidad.TabIndex = 19;
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(119, 32);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(337, 20);
            this.Descripcion.TabIndex = 16;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.Location = new System.Drawing.Point(6, 74);
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.Size = new System.Drawing.Size(85, 20);
            this.ClaveUnidad.TabIndex = 18;
            this.ClaveUnidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Cantidad
            // 
            this.Cantidad.DecimalPlaces = 6;
            this.Cantidad.Location = new System.Drawing.Point(461, 33);
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Size = new System.Drawing.Size(107, 20);
            this.Cantidad.TabIndex = 17;
            this.Cantidad.TabStop = false;
            this.Cantidad.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ProviderError
            // 
            this.ProviderError.ContainerControl = this;
            // 
            // CartaPorteMercanciasMercanciaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "CartaPorteMercanciasMercanciaControl";
            this.Size = new System.Drawing.Size(580, 414);
            this.Load += new System.EventHandler(this.CartaPorteMercanciaControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarUnidadPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetalleMercanciaSpecified)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPiezas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnidadPesoMerc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoNeto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoTara)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCantidadTransporte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCantidadTransporte)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridGuias.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridGuias)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPedimentos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPedimentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarMaterialPeligroso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarEmbalaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuscarProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CveMaterialPeligroso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSTCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UUIDComercioExt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FraccionArancelaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripEmbalaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embalaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dimensiones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValorMercancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoEnKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialPeligroso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BienesTransp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProviderError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView tabControl1;
        private Telerik.WinControls.UI.RadPageViewPage tabPage1;
        private Telerik.WinControls.UI.RadLabel label17;
        private Telerik.WinControls.UI.RadLabel label21;
        private Telerik.WinControls.UI.RadLabel label22;
        private Telerik.WinControls.UI.RadLabel label23;
        private Telerik.WinControls.UI.RadLabel label24;
        private Telerik.WinControls.UI.RadPageViewPage tabPage2;
        private Telerik.WinControls.UI.RadPageViewPage tabPage3;
        private Telerik.WinControls.UI.RadPageViewPage tabPage9;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadLabel label16;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadLabel label7;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label11;
        private Telerik.WinControls.UI.RadLabel label13;
        private Telerik.WinControls.UI.RadLabel label14;
        private Telerik.WinControls.UI.RadLabel label15;
        protected internal Telerik.WinControls.UI.RadSpinEditor NumPiezas;
        protected internal Telerik.WinControls.UI.RadDropDownList UnidadPesoMerc;
        protected internal Telerik.WinControls.UI.RadSpinEditor PesoNeto;
        protected internal Telerik.WinControls.UI.RadSpinEditor PesoBruto;
        protected internal Telerik.WinControls.UI.RadSpinEditor PesoTara;
        protected internal Telerik.WinControls.UI.RadDropDownList CveMaterialPeligroso;
        protected internal Telerik.WinControls.UI.RadTextBox ClaveSTCC;
        protected internal Telerik.WinControls.UI.RadTextBox UUIDComercioExt;
        protected internal Telerik.WinControls.UI.RadDropDownList FraccionArancelaria;
        protected internal Telerik.WinControls.UI.RadTextBox DescripEmbalaje;
        protected internal Telerik.WinControls.UI.RadTextBox Embalaje;
        protected internal Telerik.WinControls.UI.RadTextBox Dimensiones;
        protected internal Telerik.WinControls.UI.RadDropDownList Moneda;
        protected internal Telerik.WinControls.UI.RadSpinEditor ValorMercancia;
        protected internal Telerik.WinControls.UI.RadSpinEditor PesoEnKg;
        protected internal Telerik.WinControls.UI.RadCheckBox MaterialPeligroso;
        protected internal Telerik.WinControls.UI.RadTextBox BienesTransp;
        protected internal Telerik.WinControls.UI.RadTextBox Unidad;
        protected internal Telerik.WinControls.UI.RadTextBox Descripcion;
        protected internal Telerik.WinControls.UI.RadTextBox ClaveUnidad;
        protected internal Telerik.WinControls.UI.RadSpinEditor Cantidad;
        protected internal Common.Forms.ToolBarStandarControl TCantidad;
        protected internal Common.Forms.ToolBarStandarControl TGuia;
        protected internal Common.Forms.ToolBarStandarControl TPedimento;
        protected internal Telerik.WinControls.UI.RadCheckBox DetalleMercanciaSpecified;
        private Telerik.WinControls.UI.RadButton BuscarProducto;
        private Telerik.WinControls.UI.RadButton BuscarUnidad;
        private Telerik.WinControls.UI.RadButton BuscarEmbalaje;
        private Telerik.WinControls.UI.RadButton BuscarMaterialPeligroso;
        private Telerik.WinControls.UI.RadButton BuscarUnidadPeso;
        protected internal System.Windows.Forms.ErrorProvider ProviderError;
        protected internal Telerik.WinControls.UI.RadGridView gridCantidadTransporte;
        protected internal Telerik.WinControls.UI.RadGridView gridGuias;
        protected internal Telerik.WinControls.UI.RadGridView gridPedimentos;
    }
}
