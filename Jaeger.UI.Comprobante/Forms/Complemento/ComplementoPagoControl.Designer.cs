﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class ComplementoPagoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn3 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Monto = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.ButtonSPEI = new Telerik.WinControls.UI.RadButton();
            this.FormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.NomBancoOrdExt = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.SelloPago = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.CadenaPago = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.CertificadoPago = new Telerik.WinControls.UI.RadTextBox();
            this.TipoCadenaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RadLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.CuentaBeneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.RFCCtaBeneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.CtaOrdenante = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.RFCEmisorCtaOrigen = new Telerik.WinControls.UI.RadTextBox();
            this.labelNumOperacion = new Telerik.WinControls.UI.RadLabel();
            this.labelMonto = new Telerik.WinControls.UI.RadLabel();
            this.labelFechaPago = new Telerik.WinControls.UI.RadLabel();
            this.NumOperacion = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.labelFormaPago = new Telerik.WinControls.UI.RadLabel();
            this.labelTipoOperacion = new Telerik.WinControls.UI.RadLabel();
            this.TipoCambio = new Telerik.WinControls.UI.RadSpinEditor();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.FechaPago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ReceptorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.DoctoRelacionado = new Telerik.WinControls.UI.RadGridView();
            this.TComprobante = new Jaeger.UI.Comprobante.Forms.TbComplementoPagoDoctoControl();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radPageView2 = new Telerik.WinControls.UI.RadPageView();
            this.PageImpuestosTralados = new Telerik.WinControls.UI.RadPageViewPage();
            this.Traslados = new Telerik.WinControls.UI.RadGridView();
            this.TTraslado = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PageImpuestosRetenidos = new Telerik.WinControls.UI.RadPageViewPage();
            this.Retenciones = new Telerik.WinControls.UI.RadGridView();
            this.TRetencion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Monto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSPEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomBancoOrdExt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomBancoOrdExt.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomBancoOrdExt.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelloPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CadenaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertificadoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCadenaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCadenaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCadenaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCCtaBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CtaOrdenante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCEmisorCtaOrigen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelNumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFechaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTipoOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DoctoRelacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoctoRelacionado.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView2)).BeginInit();
            this.radPageView2.SuspendLayout();
            this.PageImpuestosTralados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Traslados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Traslados.MasterTemplate)).BeginInit();
            this.PageImpuestosRetenidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Retenciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Retenciones.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // Monto
            // 
            this.Monto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Monto.Location = new System.Drawing.Point(1070, 6);
            this.Monto.Mask = "N2";
            this.Monto.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Monto.Name = "Monto";
            this.Monto.Size = new System.Drawing.Size(104, 20);
            this.Monto.TabIndex = 9;
            this.Monto.TabStop = false;
            this.Monto.Text = "0.00";
            this.Monto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ButtonSPEI
            // 
            this.ButtonSPEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonSPEI.Location = new System.Drawing.Point(1075, 58);
            this.ButtonSPEI.Name = "ButtonSPEI";
            this.ButtonSPEI.Size = new System.Drawing.Size(99, 20);
            this.ButtonSPEI.TabIndex = 22;
            this.ButtonSPEI.Text = "SPEI";
            this.ButtonSPEI.Click += new System.EventHandler(this.ButtonSPEI_Click);
            // 
            // FormaPago
            // 
            this.FormaPago.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FormaPago.AutoSizeDropDownToBestFit = true;
            this.FormaPago.DisplayMember = "Descriptor";
            this.FormaPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // FormaPago.NestedRadGridView
            // 
            this.FormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.FormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.FormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Codigo";
            gridViewTextBoxColumn1.Name = "Code";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Name";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.FormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.FormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.FormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.FormaPago.EditorControl.Name = "NestedRadGridView";
            this.FormaPago.EditorControl.ReadOnly = true;
            this.FormaPago.EditorControl.ShowGroupPanel = false;
            this.FormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.FormaPago.EditorControl.TabIndex = 0;
            this.FormaPago.Location = new System.Drawing.Point(328, 6);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.NullText = "Forma de pago";
            this.FormaPago.Size = new System.Drawing.Size(294, 20);
            this.FormaPago.TabIndex = 3;
            this.FormaPago.TabStop = false;
            // 
            // NomBancoOrdExt
            // 
            this.NomBancoOrdExt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NomBancoOrdExt.AutoSizeDropDownToBestFit = true;
            // 
            // NomBancoOrdExt.NestedRadGridView
            // 
            this.NomBancoOrdExt.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.NomBancoOrdExt.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NomBancoOrdExt.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.NomBancoOrdExt.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.NomBancoOrdExt.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.NomBancoOrdExt.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.NomBancoOrdExt.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn4.FieldName = "RazonSocial";
            gridViewTextBoxColumn4.HeaderText = "Razon Social";
            gridViewTextBoxColumn4.Name = "RazonSocial";
            gridViewTextBoxColumn4.Width = 220;
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.Name = "Clave";
            gridViewTextBoxColumn6.FieldName = "Descripcion";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "Descripcion";
            gridViewTextBoxColumn6.Width = 165;
            this.NomBancoOrdExt.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.NomBancoOrdExt.EditorControl.MasterTemplate.EnableGrouping = false;
            this.NomBancoOrdExt.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.NomBancoOrdExt.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.NomBancoOrdExt.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.NomBancoOrdExt.EditorControl.Name = "NestedRadGridView";
            this.NomBancoOrdExt.EditorControl.ReadOnly = true;
            this.NomBancoOrdExt.EditorControl.ShowGroupPanel = false;
            this.NomBancoOrdExt.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.NomBancoOrdExt.EditorControl.TabIndex = 0;
            this.NomBancoOrdExt.Location = new System.Drawing.Point(492, 32);
            this.NomBancoOrdExt.Name = "NomBancoOrdExt";
            this.NomBancoOrdExt.NullText = "Nombre del Banco";
            this.NomBancoOrdExt.Size = new System.Drawing.Size(238, 20);
            this.NomBancoOrdExt.TabIndex = 13;
            this.NomBancoOrdExt.TabStop = false;
            // 
            // RadLabel4
            // 
            this.RadLabel4.Location = new System.Drawing.Point(603, 85);
            this.RadLabel4.Name = "RadLabel4";
            this.RadLabel4.Size = new System.Drawing.Size(77, 18);
            this.RadLabel4.TabIndex = 29;
            this.RadLabel4.Text = "Sello de pago:";
            // 
            // SelloPago
            // 
            this.SelloPago.Location = new System.Drawing.Point(686, 84);
            this.SelloPago.Name = "SelloPago";
            this.SelloPago.NullText = "Sello de pago:";
            this.SelloPago.Size = new System.Drawing.Size(152, 20);
            this.SelloPago.TabIndex = 30;
            // 
            // RadLabel5
            // 
            this.RadLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadLabel5.Location = new System.Drawing.Point(844, 85);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(91, 18);
            this.RadLabel5.TabIndex = 25;
            this.RadLabel5.Text = "Cadena de pago:";
            // 
            // CadenaPago
            // 
            this.CadenaPago.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CadenaPago.Location = new System.Drawing.Point(941, 84);
            this.CadenaPago.Name = "CadenaPago";
            this.CadenaPago.NullText = "Cadena de pago";
            this.CadenaPago.Size = new System.Drawing.Size(232, 20);
            this.CadenaPago.TabIndex = 26;
            // 
            // RadLabel6
            // 
            this.RadLabel6.Location = new System.Drawing.Point(305, 85);
            this.RadLabel6.Name = "RadLabel6";
            this.RadLabel6.Size = new System.Drawing.Size(107, 18);
            this.RadLabel6.TabIndex = 27;
            this.RadLabel6.Text = "Certificado de pago:";
            // 
            // CertificadoPago
            // 
            this.CertificadoPago.Location = new System.Drawing.Point(418, 84);
            this.CertificadoPago.Name = "CertificadoPago";
            this.CertificadoPago.NullText = "Certificado de pago";
            this.CertificadoPago.Size = new System.Drawing.Size(180, 20);
            this.CertificadoPago.TabIndex = 28;
            // 
            // TipoCadenaPago
            // 
            this.TipoCadenaPago.AutoSizeDropDownToBestFit = true;
            // 
            // TipoCadenaPago.NestedRadGridView
            // 
            this.TipoCadenaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.TipoCadenaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TipoCadenaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TipoCadenaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.TipoCadenaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.TipoCadenaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.TipoCadenaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Clave";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Descripcion";
            this.TipoCadenaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.TipoCadenaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.TipoCadenaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.TipoCadenaPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.TipoCadenaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.TipoCadenaPago.EditorControl.Name = "NestedRadGridView";
            this.TipoCadenaPago.EditorControl.ReadOnly = true;
            this.TipoCadenaPago.EditorControl.ShowGroupPanel = false;
            this.TipoCadenaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.TipoCadenaPago.EditorControl.TabIndex = 0;
            this.TipoCadenaPago.Location = new System.Drawing.Point(123, 84);
            this.TipoCadenaPago.Name = "TipoCadenaPago";
            this.TipoCadenaPago.NullText = "Cadena de Pago";
            this.TipoCadenaPago.Size = new System.Drawing.Size(173, 20);
            this.TipoCadenaPago.TabIndex = 24;
            this.TipoCadenaPago.TabStop = false;
            // 
            // RadLabel22
            // 
            this.RadLabel22.Location = new System.Drawing.Point(3, 85);
            this.RadLabel22.Name = "RadLabel22";
            this.RadLabel22.Size = new System.Drawing.Size(114, 18);
            this.RadLabel22.TabIndex = 23;
            this.RadLabel22.Text = "Tipo cadena de pago:";
            // 
            // RadLabel23
            // 
            this.RadLabel23.Location = new System.Drawing.Point(306, 59);
            this.RadLabel23.Name = "RadLabel23";
            this.RadLabel23.Size = new System.Drawing.Size(105, 18);
            this.RadLabel23.TabIndex = 20;
            this.RadLabel23.Text = "Cuenta beneficiario:";
            // 
            // CuentaBeneficiario
            // 
            this.CuentaBeneficiario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CuentaBeneficiario.Location = new System.Drawing.Point(418, 58);
            this.CuentaBeneficiario.MaxLength = 50;
            this.CuentaBeneficiario.Name = "CuentaBeneficiario";
            this.CuentaBeneficiario.NullText = "Cuenta beneficiario";
            this.CuentaBeneficiario.Size = new System.Drawing.Size(181, 20);
            this.CuentaBeneficiario.TabIndex = 21;
            // 
            // RadLabel24
            // 
            this.RadLabel24.Location = new System.Drawing.Point(4, 59);
            this.RadLabel24.Name = "RadLabel24";
            this.RadLabel24.Size = new System.Drawing.Size(163, 18);
            this.RadLabel24.TabIndex = 18;
            this.RadLabel24.Text = "RFC Emisor Cuenta Beneficiaria:";
            // 
            // RFCCtaBeneficiario
            // 
            this.RFCCtaBeneficiario.Location = new System.Drawing.Point(173, 58);
            this.RFCCtaBeneficiario.MaxLength = 13;
            this.RFCCtaBeneficiario.Name = "RFCCtaBeneficiario";
            this.RFCCtaBeneficiario.NullText = "RFC Emisor Cuenta Beneficiaria:";
            this.RFCCtaBeneficiario.Size = new System.Drawing.Size(124, 20);
            this.RFCCtaBeneficiario.TabIndex = 19;
            // 
            // RadLabel25
            // 
            this.RadLabel25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadLabel25.Location = new System.Drawing.Point(736, 33);
            this.RadLabel25.Name = "RadLabel25";
            this.RadLabel25.Size = new System.Drawing.Size(99, 18);
            this.RadLabel25.TabIndex = 14;
            this.RadLabel25.Text = "Cuenta ordenante:";
            // 
            // CtaOrdenante
            // 
            this.CtaOrdenante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CtaOrdenante.Location = new System.Drawing.Point(841, 32);
            this.CtaOrdenante.MaxLength = 50;
            this.CtaOrdenante.Name = "CtaOrdenante";
            this.CtaOrdenante.NullText = "Cuenta ordenante";
            this.CtaOrdenante.Size = new System.Drawing.Size(179, 20);
            this.CtaOrdenante.TabIndex = 15;
            // 
            // RadLabel26
            // 
            this.RadLabel26.Location = new System.Drawing.Point(306, 33);
            this.RadLabel26.Name = "RadLabel26";
            this.RadLabel26.Size = new System.Drawing.Size(180, 18);
            this.RadLabel26.TabIndex = 12;
            this.RadLabel26.Text = "Nombre del Banco Ord. Extranjero:";
            // 
            // RadLabel27
            // 
            this.RadLabel27.Location = new System.Drawing.Point(4, 33);
            this.RadLabel27.Name = "RadLabel27";
            this.RadLabel27.Size = new System.Drawing.Size(156, 18);
            this.RadLabel27.TabIndex = 10;
            this.RadLabel27.Text = "RFC Emisor de Cuenta Origen:";
            // 
            // RFCEmisorCtaOrigen
            // 
            this.RFCEmisorCtaOrigen.Location = new System.Drawing.Point(173, 32);
            this.RFCEmisorCtaOrigen.MaxLength = 13;
            this.RFCEmisorCtaOrigen.Name = "RFCEmisorCtaOrigen";
            this.RFCEmisorCtaOrigen.NullText = "RFC emisor de cuenta origen:";
            this.RFCEmisorCtaOrigen.Size = new System.Drawing.Size(124, 20);
            this.RFCEmisorCtaOrigen.TabIndex = 11;
            // 
            // labelNumOperacion
            // 
            this.labelNumOperacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumOperacion.Location = new System.Drawing.Point(628, 7);
            this.labelNumOperacion.Name = "labelNumOperacion";
            this.labelNumOperacion.Size = new System.Drawing.Size(106, 18);
            this.labelNumOperacion.TabIndex = 4;
            this.labelNumOperacion.Text = "Núm. de Operación:";
            // 
            // labelMonto
            // 
            this.labelMonto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMonto.Location = new System.Drawing.Point(1023, 7);
            this.labelMonto.Name = "labelMonto";
            this.labelMonto.Size = new System.Drawing.Size(42, 18);
            this.labelMonto.TabIndex = 8;
            this.labelMonto.Text = "Monto:";
            // 
            // labelFechaPago
            // 
            this.labelFechaPago.Location = new System.Drawing.Point(4, 7);
            this.labelFechaPago.Name = "labelFechaPago";
            this.labelFechaPago.Size = new System.Drawing.Size(82, 18);
            this.labelFechaPago.TabIndex = 0;
            this.labelFechaPago.Text = "Fecha de Pago:";
            // 
            // NumOperacion
            // 
            this.NumOperacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumOperacion.Location = new System.Drawing.Point(736, 6);
            this.NumOperacion.MaxLength = 100;
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.NullText = "Número de Operación";
            this.NumOperacion.Size = new System.Drawing.Size(145, 20);
            this.NumOperacion.TabIndex = 5;
            // 
            // RadLabel31
            // 
            this.RadLabel31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadLabel31.Location = new System.Drawing.Point(1023, 33);
            this.RadLabel31.Name = "RadLabel31";
            this.RadLabel31.Size = new System.Drawing.Size(50, 18);
            this.RadLabel31.TabIndex = 16;
            this.RadLabel31.Text = "Moneda:";
            // 
            // labelFormaPago
            // 
            this.labelFormaPago.Location = new System.Drawing.Point(238, 7);
            this.labelFormaPago.Name = "labelFormaPago";
            this.labelFormaPago.Size = new System.Drawing.Size(84, 18);
            this.labelFormaPago.TabIndex = 2;
            this.labelFormaPago.Text = "Forma de Pago:";
            // 
            // labelTipoOperacion
            // 
            this.labelTipoOperacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTipoOperacion.Location = new System.Drawing.Point(887, 7);
            this.labelTipoOperacion.Name = "labelTipoOperacion";
            this.labelTipoOperacion.Size = new System.Drawing.Size(74, 18);
            this.labelTipoOperacion.TabIndex = 6;
            this.labelTipoOperacion.Text = "T. de Cambio:";
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.DecimalPlaces = 2;
            this.TipoCambio.Location = new System.Drawing.Point(967, 6);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.Size = new System.Drawing.Size(53, 20);
            this.TipoCambio.TabIndex = 7;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.AutoSizeDropDownToBestFit = true;
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(1075, 32);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(99, 20);
            this.Moneda.TabIndex = 17;
            this.Moneda.TabStop = false;
            // 
            // FechaPago
            // 
            this.FechaPago.CustomFormat = "dd/MM/yyyy hh:mm tt";
            this.FechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaPago.Location = new System.Drawing.Point(93, 6);
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.NullText = "--/--/---- --:-- -- --";
            this.FechaPago.Size = new System.Drawing.Size(137, 20);
            this.FechaPago.TabIndex = 1;
            this.FechaPago.TabStop = false;
            this.FechaPago.Text = "06/10/2017 12:18 a. m.";
            this.FechaPago.Value = new System.DateTime(2017, 10, 6, 0, 18, 34, 554);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.labelFechaPago);
            this.radGroupBox1.Controls.Add(this.Monto);
            this.radGroupBox1.Controls.Add(this.FechaPago);
            this.radGroupBox1.Controls.Add(this.ButtonSPEI);
            this.radGroupBox1.Controls.Add(this.Moneda);
            this.radGroupBox1.Controls.Add(this.FormaPago);
            this.radGroupBox1.Controls.Add(this.TipoCambio);
            this.radGroupBox1.Controls.Add(this.NomBancoOrdExt);
            this.radGroupBox1.Controls.Add(this.labelTipoOperacion);
            this.radGroupBox1.Controls.Add(this.RadLabel4);
            this.radGroupBox1.Controls.Add(this.labelFormaPago);
            this.radGroupBox1.Controls.Add(this.SelloPago);
            this.radGroupBox1.Controls.Add(this.RadLabel31);
            this.radGroupBox1.Controls.Add(this.RadLabel5);
            this.radGroupBox1.Controls.Add(this.NumOperacion);
            this.radGroupBox1.Controls.Add(this.CadenaPago);
            this.radGroupBox1.Controls.Add(this.labelMonto);
            this.radGroupBox1.Controls.Add(this.RadLabel6);
            this.radGroupBox1.Controls.Add(this.labelNumOperacion);
            this.radGroupBox1.Controls.Add(this.CertificadoPago);
            this.radGroupBox1.Controls.Add(this.RFCEmisorCtaOrigen);
            this.radGroupBox1.Controls.Add(this.TipoCadenaPago);
            this.radGroupBox1.Controls.Add(this.RadLabel27);
            this.radGroupBox1.Controls.Add(this.RadLabel22);
            this.radGroupBox1.Controls.Add(this.RadLabel26);
            this.radGroupBox1.Controls.Add(this.RadLabel23);
            this.radGroupBox1.Controls.Add(this.CtaOrdenante);
            this.radGroupBox1.Controls.Add(this.CuentaBeneficiario);
            this.radGroupBox1.Controls.Add(this.RadLabel25);
            this.radGroupBox1.Controls.Add(this.RadLabel24);
            this.radGroupBox1.Controls.Add(this.RFCCtaBeneficiario);
            this.radGroupBox1.Controls.Add(this.ReceptorRFC);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            this.radGroupBox1.HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Right;
            this.radGroupBox1.HeaderText = "Complemento Pago";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.MinimumSize = new System.Drawing.Size(1196, 110);
            this.radGroupBox1.Name = "radGroupBox1";
            // 
            // 
            // 
            this.radGroupBox1.RootElement.MinSize = new System.Drawing.Size(1196, 110);
            this.radGroupBox1.Size = new System.Drawing.Size(1196, 110);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Complemento Pago";
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Location = new System.Drawing.Point(772, 6);
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.Size = new System.Drawing.Size(100, 20);
            this.ReceptorRFC.TabIndex = 2;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 110);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1196, 456);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.DoctoRelacionado);
            this.splitPanel1.Controls.Add(this.TComprobante);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1196, 226);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // DoctoRelacionado
            // 
            this.DoctoRelacionado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DoctoRelacionado.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.DoctoRelacionado.MasterTemplate.AllowAddNewRow = false;
            this.DoctoRelacionado.MasterTemplate.AllowDeleteRow = false;
            this.DoctoRelacionado.MasterTemplate.AllowRowHeaderContextMenu = false;
            this.DoctoRelacionado.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "IdSubTipo";
            gridViewTextBoxColumn11.HeaderText = "IdSubTipo";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "IdSubTipo";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.VisibleInColumnChooser = false;
            gridViewTextBoxColumn12.FieldName = "IdDocumentoDR";
            gridViewTextBoxColumn12.HeaderText = "IdDocumento";
            gridViewTextBoxColumn12.Name = "IdDocumentoDR";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 230;
            gridViewTextBoxColumn13.FieldName = "SerieDR";
            gridViewTextBoxColumn13.HeaderText = "Serie";
            gridViewTextBoxColumn13.Name = "SerieDR";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.Width = 65;
            gridViewTextBoxColumn14.FieldName = "Folio";
            gridViewTextBoxColumn14.HeaderText = "Folio";
            gridViewTextBoxColumn14.Name = "Folio";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.Width = 65;
            gridViewTextBoxColumn15.FieldName = "RFC";
            gridViewTextBoxColumn15.HeaderText = "RFC";
            gridViewTextBoxColumn15.Name = "RFC";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 85;
            gridViewTextBoxColumn16.FieldName = "Nombre";
            gridViewTextBoxColumn16.HeaderText = "Receptor";
            gridViewTextBoxColumn16.Name = "Nombre";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.Width = 200;
            gridViewTextBoxColumn17.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn17.FieldName = "FechaEmision";
            gridViewTextBoxColumn17.FormatString = "{0:d}";
            gridViewTextBoxColumn17.HeaderText = "Fecha \r\nEmision";
            gridViewTextBoxColumn17.Name = "FechaEmision";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn17.Width = 85;
            gridViewMultiComboBoxColumn1.FieldName = "MonedaDR";
            gridViewMultiComboBoxColumn1.HeaderText = "Moneda";
            gridViewMultiComboBoxColumn1.Name = "Moneda";
            gridViewMultiComboBoxColumn1.ReadOnly = true;
            gridViewMultiComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewMultiComboBoxColumn1.Width = 65;
            gridViewTextBoxColumn18.FieldName = "EquivalenciaDR";
            gridViewTextBoxColumn18.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn18.HeaderText = "Tipo \r\nCambio";
            gridViewTextBoxColumn18.Name = "EquivalenciaDR";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn19.FieldName = "FormaDePagoP";
            gridViewTextBoxColumn19.HeaderText = "Forma de Pago";
            gridViewTextBoxColumn19.Name = "FormaDePagoP";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn19.WrapText = true;
            gridViewMultiComboBoxColumn2.FieldName = "MetodoPago";
            gridViewMultiComboBoxColumn2.HeaderText = "Método \r\nde Pago";
            gridViewMultiComboBoxColumn2.Name = "MetodoPago";
            gridViewMultiComboBoxColumn2.ReadOnly = true;
            gridViewMultiComboBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewMultiComboBoxColumn2.Width = 80;
            gridViewMultiComboBoxColumn3.DisplayMember = "Id";
            gridViewMultiComboBoxColumn3.FieldName = "ObjetoImpDR";
            gridViewMultiComboBoxColumn3.HeaderText = "Objeto Imp.";
            gridViewMultiComboBoxColumn3.Name = "ObjetoImpDR";
            gridViewMultiComboBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn20.DataType = typeof(int);
            gridViewTextBoxColumn20.FieldName = "NumParcialidad";
            gridViewTextBoxColumn20.HeaderText = "Núm. \r\n Parcialidad";
            gridViewTextBoxColumn20.Name = "NumParcialidad";
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn21.DataType = typeof(decimal);
            gridViewTextBoxColumn21.FieldName = "Total";
            gridViewTextBoxColumn21.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn21.HeaderText = "Total";
            gridViewTextBoxColumn21.Name = "Total";
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn21.Width = 75;
            gridViewTextBoxColumn22.FieldName = "ImpSaldoAnt";
            gridViewTextBoxColumn22.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn22.HeaderText = "Imp. Saldo \r\nAnterior";
            gridViewTextBoxColumn22.Name = "ImpSaldoAnt";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 75;
            gridViewTextBoxColumn23.FieldName = "ImpPagado";
            gridViewTextBoxColumn23.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn23.HeaderText = "Importe \r\n Pagado";
            gridViewTextBoxColumn23.Name = "ImpPagado";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.Width = 75;
            gridViewTextBoxColumn24.FieldName = "SaldoInsoluto";
            gridViewTextBoxColumn24.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn24.HeaderText = "Imp. Saldo\r\nInsoluto";
            gridViewTextBoxColumn24.Name = "ImpSaldoInsoluto";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 75;
            gridViewTextBoxColumn25.FieldName = "Estado";
            gridViewTextBoxColumn25.HeaderText = "Estado";
            gridViewTextBoxColumn25.IsVisible = false;
            gridViewTextBoxColumn25.Name = "Estado";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn25.Width = 65;
            this.DoctoRelacionado.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewMultiComboBoxColumn2,
            gridViewMultiComboBoxColumn3,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25});
            this.DoctoRelacionado.MasterTemplate.EnableGrouping = false;
            this.DoctoRelacionado.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.DoctoRelacionado.Name = "DoctoRelacionado";
            this.DoctoRelacionado.Size = new System.Drawing.Size(1196, 196);
            this.DoctoRelacionado.TabIndex = 1;
            this.DoctoRelacionado.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.DoctoRelacionado_CellBeginEdit);
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.Size = new System.Drawing.Size(1196, 30);
            this.TComprobante.TabIndex = 2;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radPageView2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 230);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1196, 226);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radPageView2
            // 
            this.radPageView2.Controls.Add(this.PageImpuestosTralados);
            this.radPageView2.Controls.Add(this.PageImpuestosRetenidos);
            this.radPageView2.DefaultPage = this.PageImpuestosTralados;
            this.radPageView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView2.Location = new System.Drawing.Point(0, 0);
            this.radPageView2.Name = "radPageView2";
            this.radPageView2.SelectedPage = this.PageImpuestosTralados;
            this.radPageView2.Size = new System.Drawing.Size(1196, 226);
            this.radPageView2.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView2.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageImpuestosTralados
            // 
            this.PageImpuestosTralados.Controls.Add(this.Traslados);
            this.PageImpuestosTralados.Controls.Add(this.TTraslado);
            this.PageImpuestosTralados.ItemSize = new System.Drawing.SizeF(133F, 28F);
            this.PageImpuestosTralados.Location = new System.Drawing.Point(10, 37);
            this.PageImpuestosTralados.Name = "PageImpuestosTralados";
            this.PageImpuestosTralados.Size = new System.Drawing.Size(1175, 178);
            this.PageImpuestosTralados.Text = "Impuestos: Trasladados";
            // 
            // Traslados
            // 
            this.Traslados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Traslados.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.Traslados.MasterTemplate.AllowAddNewRow = false;
            this.Traslados.MasterTemplate.AllowCellContextMenu = false;
            this.Traslados.MasterTemplate.AllowColumnChooser = false;
            this.Traslados.MasterTemplate.AllowColumnReorder = false;
            this.Traslados.MasterTemplate.AllowDragToGroup = false;
            this.Traslados.MasterTemplate.AllowRowResize = false;
            this.Traslados.MasterTemplate.AutoGenerateColumns = false;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 80;
            gridViewComboBoxColumn2.FieldName = "Impuesto";
            gridViewComboBoxColumn2.HeaderText = "Impuesto";
            gridViewComboBoxColumn2.Name = "Impuesto";
            gridViewComboBoxColumn2.Width = 80;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.FieldName = "Base";
            gridViewTextBoxColumn26.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn26.HeaderText = "Base";
            gridViewTextBoxColumn26.Name = "Base";
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 80;
            gridViewComboBoxColumn3.FieldName = "TipoFactor";
            gridViewComboBoxColumn3.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn3.Name = "TipoFactor";
            gridViewComboBoxColumn3.Width = 80;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.FieldName = "TasaOCuota";
            gridViewTextBoxColumn27.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn27.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn27.Name = "TasaOCuota";
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 80;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.FieldName = "Importe";
            gridViewTextBoxColumn28.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn28.HeaderText = "Importe";
            gridViewTextBoxColumn28.Name = "Importe";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 80;
            this.Traslados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn26,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28});
            this.Traslados.MasterTemplate.DataMember = "Traslados";
            this.Traslados.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.Traslados.Name = "Traslados";
            this.Traslados.ShowGroupPanel = false;
            this.Traslados.Size = new System.Drawing.Size(1175, 148);
            this.Traslados.TabIndex = 6;
            // 
            // TTraslado
            // 
            this.TTraslado.Dock = System.Windows.Forms.DockStyle.Top;
            this.TTraslado.Etiqueta = "";
            this.TTraslado.Location = new System.Drawing.Point(0, 0);
            this.TTraslado.Name = "TTraslado";
            this.TTraslado.ReadOnly = false;
            this.TTraslado.ShowActualizar = false;
            this.TTraslado.ShowAutorizar = false;
            this.TTraslado.ShowCerrar = false;
            this.TTraslado.ShowEditar = false;
            this.TTraslado.ShowExportarExcel = false;
            this.TTraslado.ShowFiltro = true;
            this.TTraslado.ShowGuardar = false;
            this.TTraslado.ShowHerramientas = false;
            this.TTraslado.ShowImagen = false;
            this.TTraslado.ShowImprimir = false;
            this.TTraslado.ShowNuevo = true;
            this.TTraslado.ShowRemover = true;
            this.TTraslado.Size = new System.Drawing.Size(1175, 30);
            this.TTraslado.TabIndex = 7;
            // 
            // PageImpuestosRetenidos
            // 
            this.PageImpuestosRetenidos.Controls.Add(this.Retenciones);
            this.PageImpuestosRetenidos.Controls.Add(this.TRetencion);
            this.PageImpuestosRetenidos.ItemSize = new System.Drawing.SizeF(123F, 28F);
            this.PageImpuestosRetenidos.Location = new System.Drawing.Point(10, 37);
            this.PageImpuestosRetenidos.Name = "PageImpuestosRetenidos";
            this.PageImpuestosRetenidos.Size = new System.Drawing.Size(1175, 178);
            this.PageImpuestosRetenidos.Text = "Impuestos: Retenidos";
            // 
            // Retenciones
            // 
            this.Retenciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Retenciones.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.Retenciones.MasterTemplate.AllowAddNewRow = false;
            this.Retenciones.MasterTemplate.AllowCellContextMenu = false;
            this.Retenciones.MasterTemplate.AllowColumnChooser = false;
            this.Retenciones.MasterTemplate.AllowColumnReorder = false;
            this.Retenciones.MasterTemplate.AllowDragToGroup = false;
            this.Retenciones.MasterTemplate.AllowRowResize = false;
            this.Retenciones.MasterTemplate.AutoGenerateColumns = false;
            gridViewComboBoxColumn4.FieldName = "Tipo";
            gridViewComboBoxColumn4.HeaderText = "Tipo";
            gridViewComboBoxColumn4.Name = "Tipo";
            gridViewComboBoxColumn4.Width = 80;
            gridViewComboBoxColumn5.FieldName = "Impuesto";
            gridViewComboBoxColumn5.HeaderText = "Impuesto";
            gridViewComboBoxColumn5.Name = "Impuesto";
            gridViewComboBoxColumn5.Width = 80;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.FieldName = "Base";
            gridViewTextBoxColumn29.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn29.HeaderText = "Base";
            gridViewTextBoxColumn29.Name = "Base";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.Width = 80;
            gridViewComboBoxColumn6.FieldName = "TipoFactor";
            gridViewComboBoxColumn6.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn6.Name = "TipoFactor";
            gridViewComboBoxColumn6.Width = 80;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.FieldName = "TasaOCuota";
            gridViewTextBoxColumn30.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn30.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn30.Name = "TasaOCuota";
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.FieldName = "Importe";
            gridViewTextBoxColumn31.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn31.HeaderText = "Importe";
            gridViewTextBoxColumn31.Name = "Importe";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 80;
            this.Retenciones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn4,
            gridViewComboBoxColumn5,
            gridViewTextBoxColumn29,
            gridViewComboBoxColumn6,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31});
            this.Retenciones.MasterTemplate.DataMember = "Retenciones";
            this.Retenciones.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.Retenciones.Name = "Retenciones";
            this.Retenciones.ShowGroupPanel = false;
            this.Retenciones.Size = new System.Drawing.Size(1175, 148);
            this.Retenciones.TabIndex = 8;
            // 
            // TRetencion
            // 
            this.TRetencion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRetencion.Etiqueta = "";
            this.TRetencion.Location = new System.Drawing.Point(0, 0);
            this.TRetencion.Name = "TRetencion";
            this.TRetencion.ReadOnly = false;
            this.TRetencion.ShowActualizar = false;
            this.TRetencion.ShowAutorizar = false;
            this.TRetencion.ShowCerrar = false;
            this.TRetencion.ShowEditar = false;
            this.TRetencion.ShowExportarExcel = false;
            this.TRetencion.ShowFiltro = true;
            this.TRetencion.ShowGuardar = false;
            this.TRetencion.ShowHerramientas = false;
            this.TRetencion.ShowImagen = false;
            this.TRetencion.ShowImprimir = false;
            this.TRetencion.ShowNuevo = true;
            this.TRetencion.ShowRemover = true;
            this.TRetencion.Size = new System.Drawing.Size(1175, 30);
            this.TRetencion.TabIndex = 9;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ComplementoPagoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "ComplementoPagoControl";
            this.Size = new System.Drawing.Size(1196, 566);
            this.Load += new System.EventHandler(this.ComplementoPagoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Monto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSPEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomBancoOrdExt.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomBancoOrdExt.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NomBancoOrdExt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelloPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CadenaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertificadoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCadenaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCadenaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCadenaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCCtaBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CtaOrdenante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCEmisorCtaOrigen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelNumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFechaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTipoOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DoctoRelacionado.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoctoRelacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView2)).EndInit();
            this.radPageView2.ResumeLayout(false);
            this.PageImpuestosTralados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Traslados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Traslados)).EndInit();
            this.PageImpuestosRetenidos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Retenciones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Retenciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadButton ButtonSPEI;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox FormaPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox NomBancoOrdExt;
        internal Telerik.WinControls.UI.RadTextBox SelloPago;
        internal Telerik.WinControls.UI.RadTextBox CadenaPago;
        internal Telerik.WinControls.UI.RadTextBox CertificadoPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox TipoCadenaPago;
        internal Telerik.WinControls.UI.RadTextBox CuentaBeneficiario;
        internal Telerik.WinControls.UI.RadTextBox RFCCtaBeneficiario;
        internal Telerik.WinControls.UI.RadTextBox CtaOrdenante;
        internal Telerik.WinControls.UI.RadTextBox RFCEmisorCtaOrigen;
        internal Telerik.WinControls.UI.RadTextBox NumOperacion;
        internal Telerik.WinControls.UI.RadSpinEditor TipoCambio;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaPago;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        internal Telerik.WinControls.UI.RadMaskedEditBox Monto;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadPageView radPageView2;
        private Telerik.WinControls.UI.RadPageViewPage PageImpuestosTralados;
        private Telerik.WinControls.UI.RadPageViewPage PageImpuestosRetenidos;
        internal Telerik.WinControls.UI.RadGridView DoctoRelacionado;
        internal Telerik.WinControls.UI.RadTextBox ReceptorRFC;
        private Telerik.WinControls.UI.RadLabel RadLabel4;
        private Telerik.WinControls.UI.RadLabel RadLabel5;
        private Telerik.WinControls.UI.RadLabel RadLabel6;
        private Telerik.WinControls.UI.RadLabel RadLabel22;
        private Telerik.WinControls.UI.RadLabel RadLabel23;
        private Telerik.WinControls.UI.RadLabel RadLabel24;
        private Telerik.WinControls.UI.RadLabel RadLabel25;
        private Telerik.WinControls.UI.RadLabel RadLabel26;
        private Telerik.WinControls.UI.RadLabel RadLabel27;
        private Telerik.WinControls.UI.RadLabel labelNumOperacion;
        private Telerik.WinControls.UI.RadLabel labelMonto;
        private Telerik.WinControls.UI.RadLabel labelFechaPago;
        private Telerik.WinControls.UI.RadLabel RadLabel31;
        private Telerik.WinControls.UI.RadLabel labelFormaPago;
        private Telerik.WinControls.UI.RadLabel labelTipoOperacion;
        public Telerik.WinControls.UI.RadGridView Traslados;
        private Common.Forms.ToolBarStandarControl TTraslado;
        public Telerik.WinControls.UI.RadGridView Retenciones;
        private Common.Forms.ToolBarStandarControl TRetencion;
        protected internal TbComplementoPagoDoctoControl TComprobante;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}
