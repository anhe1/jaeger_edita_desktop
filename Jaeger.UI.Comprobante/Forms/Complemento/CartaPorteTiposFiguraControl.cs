﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class CartaPorteTiposFiguraControl : RadForm {
        protected bool modeEdit = false;
        protected CartaPorteTiposFigura _TiposFigura = null;
        protected ICveFiguraTransporteCatalogo figuraTransporteCatalogo;

        public event EventHandler<CartaPorteTiposFigura> Add;
        public event EventHandler<CartaPorteTiposFigura> Edit; 

        public void OnAdd(CartaPorteTiposFigura e) {
            if (Add != null)
                this.Add(this, e);
        }

        public void OnEdit(CartaPorteTiposFigura e) {
            if (Edit != null)
                this.Edit(this, e);
        }

        public CartaPorteTiposFiguraControl() {
            InitializeComponent();
            this._TiposFigura = new CartaPorteTiposFigura();
        }

        public CartaPorteTiposFiguraControl(CartaPorteTiposFigura model) {
            InitializeComponent();
            this._TiposFigura = model;
            this.modeEdit = true; 
        }

        private void CartaPorteTiposFiguraControl_Load(object sender, EventArgs e) {
            this.figuraTransporteCatalogo = new CveFiguraTransporteCatalogo();
            this.figuraTransporteCatalogo.Load();

            this.TipoFigura.DataSource = this.figuraTransporteCatalogo.Items;
            this.TipoFigura.ValueMember = "Clave";
            this.TipoFigura.DisplayMember = "Descriptor";

            this.Domicilio.ClavePais += this.Domicilio_ClavePais;
            this.Domicilio.d2 += this.Domicilio_d2;
            this.Domicilio.ClaveCodigoPostal += this.Domicilio_ClaveCodigoPostal;
            this.CreateBinding();
        }

        private void Domicilio_ClavePais(object sender, Catalogos.Entities.ClavePais e) {
            this._TiposFigura.Domicilio.Pais = e.Clave;
        }

        private void Domicilio_ClaveCodigoPostal(object sender, Catalogos.Entities.ClaveCodigoPostal e) {
            this._TiposFigura.Domicilio.CodigoPostal = e.Clave;
        }

        private void Domicilio_d2(object sender, Catalogos.Entities.CveEstado e) {
            this._TiposFigura.Domicilio.Estado = e.Clave;
        }

        private void TFigura_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TFigura_ButtonGuardar_Click(object sender, EventArgs e) {
            if (this.modeEdit) {
                this.OnEdit(this._TiposFigura);
            } else {
                this.OnAdd(this._TiposFigura);
            }
            this.Close();
        }

        private void CreateBinding() {
            this.TipoFigura.DataBindings.Add("SelectedValue", this._TiposFigura, "TipoFigura", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NombreFigura.DataBindings.Add("Text", this._TiposFigura, "NombreFigura", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RFCFigura.DataBindings.Add("Text", this._TiposFigura, "RFCFigura", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumRegIdTribFigura.DataBindings.Add("Text", this._TiposFigura, "NumRegIdTribFigura", true, DataSourceUpdateMode.OnPropertyChanged);
            this.ResidenciaFiscalFigura.DataBindings.Add("Text", this._TiposFigura, "ResidenciaFiscalFigura", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumLicencia.DataBindings.Add("Text", this._TiposFigura, "NumLicencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Domicilio.Calle.DataBindings.Add("Text", this._TiposFigura.Domicilio, "Calle", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.NumeroExterior.DataBindings.Add("Text", this._TiposFigura.Domicilio, "NumeroExterior", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.NumeroInterior.DataBindings.Add("Text", this._TiposFigura.Domicilio, "NumeroInterior", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.Pais.DataBindings.Add("SelectedValue", this._TiposFigura.Domicilio, "Pais", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.Estado.DataBindings.Add("SelectedValue", this._TiposFigura.Domicilio, "Estado", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.Municipio.DataBindings.Add("SelectedValue", this._TiposFigura.Domicilio, "Municipio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.Localidad.DataBindings.Add("SelectedValue", this._TiposFigura.Domicilio, "Localidad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.CodigoPostal.DataBindings.Add("Text", this._TiposFigura.Domicilio, "CodigoPostal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.Colonia.DataBindings.Add("SelectedValue", this._TiposFigura.Domicilio, "Colonia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Domicilio.Referencia.DataBindings.Add("Text", this._TiposFigura.Domicilio, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
