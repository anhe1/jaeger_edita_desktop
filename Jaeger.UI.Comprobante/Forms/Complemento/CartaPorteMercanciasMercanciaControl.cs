﻿using System;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Entities;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class CartaPorteMercanciasMercanciaControl : UserControl {
        protected bool editar = false;
        protected CartaPorteMercanciasMercancia Mercancia;
        protected IClaveMaterialPeligrosoCatalogo claveMaterialPeligrosoCatalogo;
        protected IClaveClaveUnidadPesoCatalogo claveClaveUnidadPesoCatalogo;

        public CartaPorteMercanciasMercanciaControl() {
            InitializeComponent();
        }

        public CartaPorteMercanciasMercanciaControl(CartaPorteMercanciasMercancia model) {
            InitializeComponent();
            if (model == null) {
                this.Mercancia = new CartaPorteMercanciasMercancia();
            } else {
                this.editar = true;
                this.Mercancia = model;
            }
        }

        private void CartaPorteMercanciaControl_Load(object sender, EventArgs e) {
            this.gridCantidadTransporte.Standard();
            this.gridCantidadTransporte.ReadOnly = false;
            this.gridGuias.Standard();
            this.gridGuias.ReadOnly = false;
            this.gridPedimentos.Standard();
            this.gridPedimentos.ReadOnly = false;

            this.TCantidad.ShowNuevo = true;
            this.TCantidad.ShowRemover = true;

            this.TGuia.ShowNuevo = true;
            this.TGuia.ShowRemover = true;

            this.TPedimento.ShowNuevo = true;
            this.TPedimento.ShowRemover = true;

            this.claveMaterialPeligrosoCatalogo = new MaterialPeligrosoCatalogo();
            this.claveMaterialPeligrosoCatalogo.Load();

            this.claveClaveUnidadPesoCatalogo = new UnidadPesoCatalogo();
            this.claveClaveUnidadPesoCatalogo.Load();

            CveMaterialPeligroso.DisplayMember = "Descriptor";
            CveMaterialPeligroso.ValueMember = "Clave";
            CveMaterialPeligroso.DataSource = this.claveMaterialPeligrosoCatalogo.Items;

            this.UnidadPesoMerc.DisplayMember = "Descriptor";
            this.UnidadPesoMerc.ValueMember = "Clave";
            this.UnidadPesoMerc.DataSource = this.claveClaveUnidadPesoCatalogo.Items;
        }

        public virtual void MaterialPeligrosoSpecified_CheckedChanged(object sender, EventArgs e) {
            this.CveMaterialPeligroso.Enabled = this.MaterialPeligroso.Checked;
        }

        public virtual void DetalleMercanciaSpecified_CheckedChanged(object sender, EventArgs e) {
            this.UnidadPesoMerc.Enabled = this.DetalleMercanciaSpecified.Checked;
            this.PesoBruto.Enabled = this.DetalleMercanciaSpecified.Checked;
            this.PesoNeto.Enabled = this.DetalleMercanciaSpecified.Checked;
            this.PesoTara.Enabled = this.DetalleMercanciaSpecified.Checked;
            this.NumPiezas.Enabled = this.DetalleMercanciaSpecified.Checked;
        }

        public virtual void TCantidad_Remover_Click(object sender, EventArgs e) {
            if (this.gridCantidadTransporte.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) { 
                    this.gridCantidadTransporte.Rows.Remove(this.gridCantidadTransporte.CurrentRow);
                }
            }
        }

        public virtual void TGuia_Remover_Click(object sender, EventArgs e) {
            if (this.gridGuias.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    this.gridGuias.Rows.Remove(this.gridGuias.CurrentRow);
                }
            }
        }

        public virtual void TPedimento_Remover_Click(object sender, EventArgs e) {
            if (this.gridPedimentos.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    this.gridPedimentos.Rows.Remove(this.gridPedimentos.CurrentRow);
                }
            }
        }

        private void BuscarProducto_Click(object sender, EventArgs e) {
            var _buscar = new ProdServCatalogoCPBuscarForm();
            _buscar.Agregar += Producto_Agregar;
            _buscar.ShowDialog(this);
        }

        private void Producto_Agregar(object sender, Domain.Comprobante.Entities.ComprobanteConceptoDetailModel e) {
            if (e != null) {
                this.BienesTransp.Text = e.ClaveProdServ;
                this.Descripcion.Text = e.Descripcion;
            }
        }

        private void BuscarUnidad_Click(object sender, EventArgs e) {
            var _buscar = new UnidadCatalogoBuscarForm();
            _buscar.Selected += Unidad_Selected;
            _buscar.ShowDialog(this);
        }

        private void Unidad_Selected(object sender, ClaveUnidad e) {
            if (e != null) {
                this.ClaveUnidad.Text = e.Clave;
                this.Unidad.Text = e.Nombre;
            }
        }

        private void BuscarEmbalaje_Click(object sender, EventArgs e) {
            var _buscar = new TipoEmbalajeCatalogoBuscarForm();
            _buscar.Seleted += Embalaje_Selected;
            _buscar.ShowDialog(this);
        }

        private void Embalaje_Selected(object sender, CveTipoEmbalaje e) {
            if (e != null) {
                this.Embalaje.Text = e.Clave;
                this.DescripEmbalaje.Text = e.Descripcion;
            }
        }

        private void BuscarMaterialPeligroso_Click(object sender, EventArgs e) {
            var _buscar = new MaterialPeligrosoCatalogoBuscarForm();
            _buscar.Seleted += MaterialPeligroso_Selected;
            _buscar.ShowDialog(this);
        }

        private void MaterialPeligroso_Selected(object sender, CveMaterialPeligroso e) {
            if (e != null) {
                this.Embalaje.Text = e.Clave;
                this.DescripEmbalaje.Text = e.Descripcion;
            }
        }

        private void BuscarUnidadPeso_Click(object sender, EventArgs e) {
            var _buscar = new UnidadPesoCatalogoBuscarForm();
            _buscar.Seleted += UnidadPeso_Seleted;
            _buscar.ShowDialog(this);
        }

        private void UnidadPeso_Seleted(object sender, CveClaveUnidadPeso e) {
            if (e != null) {
                
            }
        }
    }
}
