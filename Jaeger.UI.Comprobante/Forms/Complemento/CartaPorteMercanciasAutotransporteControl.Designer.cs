﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class CartaPorteMercanciasAutotransporteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.PermSCT = new Telerik.WinControls.UI.RadDropDownList();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.NumPermisoSCT = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.ConfigVehicular = new Telerik.WinControls.UI.RadDropDownList();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.PlacaVM = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.AnioModeloVM = new Telerik.WinControls.UI.RadSpinEditor();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.AseguraRespCivil = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.PolizaRespCivil = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.AseguraMedAmbiente = new Telerik.WinControls.UI.RadTextBox();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.PolizaMedAmbiente = new Telerik.WinControls.UI.RadTextBox();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.AseguraCarga = new Telerik.WinControls.UI.RadTextBox();
            this.label11 = new Telerik.WinControls.UI.RadLabel();
            this.PolizaCarga = new Telerik.WinControls.UI.RadTextBox();
            this.label12 = new Telerik.WinControls.UI.RadLabel();
            this.PrimaSeguro = new Telerik.WinControls.UI.RadSpinEditor();
            this.Incluir = new Telerik.WinControls.UI.RadCheckBox();
            this.TRemolque = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TabControlAutoTransporte = new Telerik.WinControls.UI.RadPageView();
            this.TabGeneral = new Telerik.WinControls.UI.RadPageViewPage();
            this.TabRemolque = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridRemolques = new Telerik.WinControls.UI.RadGridView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermSCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPermisoSCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfigVehicular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlacaVM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnioModeloVM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AseguraRespCivil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolizaRespCivil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AseguraMedAmbiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolizaMedAmbiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AseguraCarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolizaCarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaSeguro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Incluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlAutoTransporte)).BeginInit();
            this.TabControlAutoTransporte.SuspendLayout();
            this.TabGeneral.SuspendLayout();
            this.TabRemolque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemolques)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemolques.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(6, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 18);
            this.label7.TabIndex = 23;
            this.label7.Text = "Permiso SCT:";
            // 
            // PermSCT
            // 
            this.PermSCT.Enabled = false;
            this.PermSCT.Location = new System.Drawing.Point(93, 26);
            this.PermSCT.Name = "PermSCT";
            this.PermSCT.Size = new System.Drawing.Size(348, 20);
            this.PermSCT.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(448, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 18);
            this.label10.TabIndex = 25;
            this.label10.Text = "Núm. Permiso:*";
            // 
            // NumPermisoSCT
            // 
            this.NumPermisoSCT.Enabled = false;
            this.NumPermisoSCT.Location = new System.Drawing.Point(533, 26);
            this.NumPermisoSCT.MaxLength = 50;
            this.NumPermisoSCT.Name = "NumPermisoSCT";
            this.NumPermisoSCT.Size = new System.Drawing.Size(141, 20);
            this.NumPermisoSCT.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 18);
            this.label1.TabIndex = 27;
            this.label1.Text = "Configuración Vehicular:";
            // 
            // ConfigVehicular
            // 
            this.ConfigVehicular.Enabled = false;
            this.ConfigVehicular.Location = new System.Drawing.Point(140, 52);
            this.ConfigVehicular.Name = "ConfigVehicular";
            this.ConfigVehicular.Size = new System.Drawing.Size(301, 20);
            this.ConfigVehicular.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(448, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 18);
            this.label2.TabIndex = 29;
            this.label2.Text = "Placa:";
            // 
            // PlacaVM
            // 
            this.PlacaVM.Enabled = false;
            this.PlacaVM.Location = new System.Drawing.Point(491, 52);
            this.PlacaVM.MaxLength = 7;
            this.PlacaVM.Name = "PlacaVM";
            this.PlacaVM.Size = new System.Drawing.Size(70, 20);
            this.PlacaVM.TabIndex = 30;
            this.PlacaVM.TextChanged += new System.EventHandler(this.PlacaVM_TextChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(567, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 18);
            this.label3.TabIndex = 31;
            this.label3.Text = "Año:";
            // 
            // AnioModeloVM
            // 
            this.AnioModeloVM.Enabled = false;
            this.AnioModeloVM.Location = new System.Drawing.Point(602, 52);
            this.AnioModeloVM.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.AnioModeloVM.Minimum = new decimal(new int[] {
            1899,
            0,
            0,
            0});
            this.AnioModeloVM.Name = "AnioModeloVM";
            this.AnioModeloVM.NullableValue = new decimal(new int[] {
            1899,
            0,
            0,
            0});
            this.AnioModeloVM.Size = new System.Drawing.Size(72, 20);
            this.AnioModeloVM.TabIndex = 32;
            this.AnioModeloVM.TabStop = false;
            this.AnioModeloVM.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.AnioModeloVM.Value = new decimal(new int[] {
            1899,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 18);
            this.label4.TabIndex = 33;
            this.label4.Text = "Aseguradora de responsabilidad civil :";
            // 
            // AseguraRespCivil
            // 
            this.AseguraRespCivil.Enabled = false;
            this.AseguraRespCivil.Location = new System.Drawing.Point(217, 81);
            this.AseguraRespCivil.MaxLength = 50;
            this.AseguraRespCivil.Name = "AseguraRespCivil";
            this.AseguraRespCivil.Size = new System.Drawing.Size(242, 20);
            this.AseguraRespCivil.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(465, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 18);
            this.label5.TabIndex = 35;
            this.label5.Text = "Núm. de póliza:";
            // 
            // PolizaRespCivil
            // 
            this.PolizaRespCivil.Enabled = false;
            this.PolizaRespCivil.Location = new System.Drawing.Point(551, 81);
            this.PolizaRespCivil.MaxLength = 30;
            this.PolizaRespCivil.Name = "PolizaRespCivil";
            this.PolizaRespCivil.Size = new System.Drawing.Size(123, 20);
            this.PolizaRespCivil.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(218, 18);
            this.label6.TabIndex = 37;
            this.label6.Text = "Aseguradora de daños al medio ambiente:";
            // 
            // AseguraMedAmbiente
            // 
            this.AseguraMedAmbiente.Enabled = false;
            this.AseguraMedAmbiente.Location = new System.Drawing.Point(230, 107);
            this.AseguraMedAmbiente.MaxLength = 50;
            this.AseguraMedAmbiente.Name = "AseguraMedAmbiente";
            this.AseguraMedAmbiente.Size = new System.Drawing.Size(229, 20);
            this.AseguraMedAmbiente.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(465, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 18);
            this.label8.TabIndex = 39;
            this.label8.Text = "Núm. de póliza:";
            // 
            // PolizaMedAmbiente
            // 
            this.PolizaMedAmbiente.Enabled = false;
            this.PolizaMedAmbiente.Location = new System.Drawing.Point(551, 107);
            this.PolizaMedAmbiente.Name = "PolizaMedAmbiente";
            this.PolizaMedAmbiente.Size = new System.Drawing.Size(123, 20);
            this.PolizaMedAmbiente.TabIndex = 40;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(6, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(197, 18);
            this.label9.TabIndex = 41;
            this.label9.Text = "Aseguradora de la carga transportada:";
            // 
            // AseguraCarga
            // 
            this.AseguraCarga.Enabled = false;
            this.AseguraCarga.Location = new System.Drawing.Point(217, 133);
            this.AseguraCarga.Name = "AseguraCarga";
            this.AseguraCarga.Size = new System.Drawing.Size(138, 20);
            this.AseguraCarga.TabIndex = 42;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(361, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 18);
            this.label11.TabIndex = 43;
            this.label11.Text = "Núm. de póliza:";
            // 
            // PolizaCarga
            // 
            this.PolizaCarga.Enabled = false;
            this.PolizaCarga.Location = new System.Drawing.Point(447, 133);
            this.PolizaCarga.Name = "PolizaCarga";
            this.PolizaCarga.Size = new System.Drawing.Size(68, 20);
            this.PolizaCarga.TabIndex = 44;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(521, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 18);
            this.label12.TabIndex = 45;
            this.label12.Text = "Prima Seguro:";
            // 
            // PrimaSeguro
            // 
            this.PrimaSeguro.DecimalPlaces = 2;
            this.PrimaSeguro.Location = new System.Drawing.Point(603, 133);
            this.PrimaSeguro.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.PrimaSeguro.Name = "PrimaSeguro";
            this.PrimaSeguro.Size = new System.Drawing.Size(71, 20);
            this.PrimaSeguro.TabIndex = 46;
            this.PrimaSeguro.TabStop = false;
            this.PrimaSeguro.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Incluir
            // 
            this.Incluir.Location = new System.Drawing.Point(613, 4);
            this.Incluir.Name = "Incluir";
            this.Incluir.Size = new System.Drawing.Size(51, 18);
            this.Incluir.TabIndex = 47;
            this.Incluir.Text = "Incluir";
            this.Incluir.CheckStateChanged += new System.EventHandler(this.Incluir_CheckedChanged);
            // 
            // TRemolque
            // 
            this.TRemolque.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRemolque.Etiqueta = "Remolques";
            this.TRemolque.Location = new System.Drawing.Point(3, 3);
            this.TRemolque.Name = "TRemolque";
            this.TRemolque.ShowActualizar = false;
            this.TRemolque.ShowAutorizar = false;
            this.TRemolque.ShowCerrar = false;
            this.TRemolque.ShowEditar = false;
            this.TRemolque.ShowExportarExcel = false;
            this.TRemolque.ShowFiltro = true;
            this.TRemolque.ShowGuardar = false;
            this.TRemolque.ShowHerramientas = false;
            this.TRemolque.ShowImagen = false;
            this.TRemolque.ShowImprimir = false;
            this.TRemolque.ShowNuevo = true;
            this.TRemolque.ShowRemover = true;
            this.TRemolque.Size = new System.Drawing.Size(671, 31);
            this.TRemolque.TabIndex = 50;
            this.TRemolque.Remover.Click += this.TRemolque_Remover_Click;
            // 
            // TabControlAutoTransporte
            // 
            this.TabControlAutoTransporte.Controls.Add(this.TabGeneral);
            this.TabControlAutoTransporte.Controls.Add(this.TabRemolque);
            this.TabControlAutoTransporte.DefaultPage = this.TabGeneral;
            this.TabControlAutoTransporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlAutoTransporte.Location = new System.Drawing.Point(0, 0);
            this.TabControlAutoTransporte.Name = "TabControlAutoTransporte";
            this.TabControlAutoTransporte.SelectedPage = this.TabRemolque;
            this.TabControlAutoTransporte.Size = new System.Drawing.Size(698, 208);
            this.TabControlAutoTransporte.TabIndex = 52;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControlAutoTransporte.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // TabGeneral
            // 
            this.TabGeneral.Controls.Add(this.Incluir);
            this.TabGeneral.Controls.Add(this.AseguraRespCivil);
            this.TabGeneral.Controls.Add(this.ConfigVehicular);
            this.TabGeneral.Controls.Add(this.label4);
            this.TabGeneral.Controls.Add(this.PolizaRespCivil);
            this.TabGeneral.Controls.Add(this.PermSCT);
            this.TabGeneral.Controls.Add(this.label12);
            this.TabGeneral.Controls.Add(this.label1);
            this.TabGeneral.Controls.Add(this.label5);
            this.TabGeneral.Controls.Add(this.PlacaVM);
            this.TabGeneral.Controls.Add(this.PrimaSeguro);
            this.TabGeneral.Controls.Add(this.label7);
            this.TabGeneral.Controls.Add(this.AseguraMedAmbiente);
            this.TabGeneral.Controls.Add(this.label2);
            this.TabGeneral.Controls.Add(this.label11);
            this.TabGeneral.Controls.Add(this.NumPermisoSCT);
            this.TabGeneral.Controls.Add(this.label6);
            this.TabGeneral.Controls.Add(this.AnioModeloVM);
            this.TabGeneral.Controls.Add(this.PolizaCarga);
            this.TabGeneral.Controls.Add(this.label10);
            this.TabGeneral.Controls.Add(this.PolizaMedAmbiente);
            this.TabGeneral.Controls.Add(this.label3);
            this.TabGeneral.Controls.Add(this.label9);
            this.TabGeneral.Controls.Add(this.AseguraCarga);
            this.TabGeneral.Controls.Add(this.label8);
            this.TabGeneral.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.TabGeneral.Location = new System.Drawing.Point(10, 37);
            this.TabGeneral.Name = "TabGeneral";
            this.TabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TabGeneral.Size = new System.Drawing.Size(677, 160);
            this.TabGeneral.Text = "General";
            // 
            // TabRemolque
            // 
            this.TabRemolque.Controls.Add(this.gridRemolques);
            this.TabRemolque.Controls.Add(this.TRemolque);
            this.TabRemolque.ItemSize = new System.Drawing.SizeF(72F, 28F);
            this.TabRemolque.Location = new System.Drawing.Point(10, 37);
            this.TabRemolque.Name = "TabRemolque";
            this.TabRemolque.Padding = new System.Windows.Forms.Padding(3);
            this.TabRemolque.Size = new System.Drawing.Size(677, 160);
            this.TabRemolque.Text = "Remolques";
            // 
            // gridRemolques
            // 
            this.gridRemolques.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRemolques.Location = new System.Drawing.Point(3, 34);
            // 
            // 
            // 
            this.gridRemolques.MasterTemplate.AllowAddNewRow = false;
            this.gridRemolques.MasterTemplate.AllowCellContextMenu = false;
            this.gridRemolques.MasterTemplate.AllowColumnChooser = false;
            this.gridRemolques.MasterTemplate.AllowColumnReorder = false;
            this.gridRemolques.MasterTemplate.AllowDeleteRow = false;
            this.gridRemolques.MasterTemplate.AllowDragToGroup = false;
            this.gridRemolques.MasterTemplate.AllowEditRow = false;
            this.gridRemolques.MasterTemplate.AllowRowResize = false;
            this.gridRemolques.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "SubTipoRem";
            gridViewTextBoxColumn1.HeaderText = "SubTipoRem";
            gridViewTextBoxColumn1.Name = "SubTipoRem";
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "Placa";
            gridViewTextBoxColumn2.HeaderText = "Placa";
            gridViewTextBoxColumn2.Name = "Placa";
            gridViewTextBoxColumn2.Width = 85;
            this.gridRemolques.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.gridRemolques.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridRemolques.Name = "gridRemolques";
            this.gridRemolques.ShowGroupPanel = false;
            this.gridRemolques.Size = new System.Drawing.Size(671, 123);
            this.gridRemolques.TabIndex = 51;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // CartaPorteMercanciasAutotransporteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControlAutoTransporte);
            this.Name = "CartaPorteMercanciasAutotransporteControl";
            this.Size = new System.Drawing.Size(698, 208);
            this.Load += new System.EventHandler(this.CartaPorteMercanciasAutotransporteControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermSCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPermisoSCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfigVehicular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlacaVM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnioModeloVM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AseguraRespCivil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolizaRespCivil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AseguraMedAmbiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolizaMedAmbiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AseguraCarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolizaCarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaSeguro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Incluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlAutoTransporte)).EndInit();
            this.TabControlAutoTransporte.ResumeLayout(false);
            this.TabGeneral.ResumeLayout(false);
            this.TabGeneral.PerformLayout();
            this.TabRemolque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridRemolques.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemolques)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel label7;
        private Telerik.WinControls.UI.RadLabel label10;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadLabel label6;
        private Telerik.WinControls.UI.RadLabel label8;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label11;
        private Telerik.WinControls.UI.RadLabel label12;
        protected internal Telerik.WinControls.UI.RadDropDownList PermSCT;
        protected internal Telerik.WinControls.UI.RadTextBox NumPermisoSCT;
        protected internal Telerik.WinControls.UI.RadDropDownList ConfigVehicular;
        protected internal Telerik.WinControls.UI.RadTextBox PlacaVM;
        protected internal Telerik.WinControls.UI.RadSpinEditor AnioModeloVM;
        protected internal Telerik.WinControls.UI.RadTextBox AseguraRespCivil;
        protected internal Telerik.WinControls.UI.RadTextBox PolizaRespCivil;
        protected internal Telerik.WinControls.UI.RadTextBox AseguraMedAmbiente;
        protected internal Telerik.WinControls.UI.RadTextBox PolizaMedAmbiente;
        protected internal Telerik.WinControls.UI.RadTextBox AseguraCarga;
        protected internal Telerik.WinControls.UI.RadTextBox PolizaCarga;
        protected internal Telerik.WinControls.UI.RadSpinEditor PrimaSeguro;
        protected internal Common.Forms.ToolBarStandarControl TRemolque;
        private Telerik.WinControls.UI.RadPageViewPage TabGeneral;
        private Telerik.WinControls.UI.RadPageViewPage TabRemolque;
        protected internal Telerik.WinControls.UI.RadPageView TabControlAutoTransporte;
        protected internal Telerik.WinControls.UI.RadCheckBox Incluir;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        protected internal Telerik.WinControls.UI.RadGridView gridRemolques;
    }
}
