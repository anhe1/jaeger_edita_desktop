﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class TbComplementoPagoDoctoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.CommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.Documento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarRowElement();
            this.DoctoRelacionado = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblComprobante = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostComprobante = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Separador1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ImportesPagadosLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ImportePagado = new Telerik.WinControls.UI.CommandBarLabel();
            this.Total = new Telerik.WinControls.UI.CommandBarMaskedEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).BeginInit();
            this.CommandBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandBar
            // 
            this.CommandBar.Controls.Add(this.Documento);
            this.CommandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandBar.Location = new System.Drawing.Point(0, 0);
            this.CommandBar.Name = "CommandBar";
            this.CommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBar});
            this.CommandBar.Size = new System.Drawing.Size(1069, 55);
            this.CommandBar.TabIndex = 1;
            // 
            // Documento
            // 
            this.Documento.AutoSizeDropDownToBestFit = true;
            // 
            // Documento.NestedRadGridView
            // 
            this.Documento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Documento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Documento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Documento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Documento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Documento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Documento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "SubId";
            gridViewTextBoxColumn2.HeaderText = "SubId";
            gridViewTextBoxColumn2.Name = "SubId";
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn3.FieldName = "Status";
            gridViewTextBoxColumn3.HeaderText = "Status";
            gridViewTextBoxColumn3.Name = "Status";
            gridViewTextBoxColumn4.FieldName = "Folio";
            gridViewTextBoxColumn4.HeaderText = "Folio";
            gridViewTextBoxColumn4.Name = "Folio";
            gridViewTextBoxColumn5.FieldName = "Serie";
            gridViewTextBoxColumn5.HeaderText = "Serie";
            gridViewTextBoxColumn5.Name = "Serie";
            gridViewTextBoxColumn6.FieldName = "IdDocumento";
            gridViewTextBoxColumn6.HeaderText = "UUID";
            gridViewTextBoxColumn6.Name = "IdDocumento";
            gridViewTextBoxColumn7.FieldName = "EmisorRfc";
            gridViewTextBoxColumn7.HeaderText = "Emisor (RFC)";
            gridViewTextBoxColumn7.Name = "RfcEmisor";
            gridViewTextBoxColumn8.FieldName = "Emisor";
            gridViewTextBoxColumn8.HeaderText = "Emisor";
            gridViewTextBoxColumn8.Name = "Emisor";
            gridViewTextBoxColumn9.FieldName = "ReceptorRfc";
            gridViewTextBoxColumn9.HeaderText = "Receptor (RFC)";
            gridViewTextBoxColumn9.Name = "RfcReceptor";
            gridViewTextBoxColumn10.FieldName = "Receptor";
            gridViewTextBoxColumn10.HeaderText = "Receptor";
            gridViewTextBoxColumn10.Name = "Receptor";
            gridViewTextBoxColumn11.FieldName = "Estado";
            gridViewTextBoxColumn11.HeaderText = "Estado";
            gridViewTextBoxColumn11.Name = "Estado";
            gridViewTextBoxColumn12.FieldName = "NumParcialidad";
            gridViewTextBoxColumn12.HeaderText = "Núm. Par.";
            gridViewTextBoxColumn12.Name = "NumParcialidad";
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Total";
            gridViewTextBoxColumn13.FormatString = "{0:$#,###0.00}";
            gridViewTextBoxColumn13.HeaderText = "Total";
            gridViewTextBoxColumn13.Name = "Total";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "Acumulado";
            gridViewTextBoxColumn14.FormatString = "{0:$#,###0.00}";
            gridViewTextBoxColumn14.HeaderText = "Acumulado";
            gridViewTextBoxColumn14.Name = "Acumulado";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.Documento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.Documento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Documento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Documento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Documento.EditorControl.Name = "NestedRadGridView";
            this.Documento.EditorControl.ReadOnly = true;
            this.Documento.EditorControl.ShowGroupPanel = false;
            this.Documento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Documento.EditorControl.TabIndex = 0;
            this.Documento.Location = new System.Drawing.Point(93, 4);
            this.Documento.Name = "Documento";
            this.Documento.NullText = "Selecciona";
            this.Documento.Size = new System.Drawing.Size(405, 20);
            this.Documento.TabIndex = 0;
            this.Documento.TabStop = false;
            // 
            // ToolBar
            // 
            this.ToolBar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.DoctoRelacionado});
            this.ToolBar.Text = "";
            this.ToolBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBar.UseCompatibleTextRendering = false;
            // 
            // DoctoRelacionado
            // 
            this.DoctoRelacionado.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DoctoRelacionado.DisplayName = "commandBarStripElement1";
            this.DoctoRelacionado.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblComprobante,
            this.HostComprobante,
            this.Agregar,
            this.Remover,
            this.Buscar,
            this.Separador1,
            this.ImportesPagadosLabel,
            this.ImportePagado,
            this.Total});
            this.DoctoRelacionado.Name = "DoctoRelacionado";
            // 
            // 
            // 
            this.DoctoRelacionado.OverflowButton.Enabled = false;
            this.DoctoRelacionado.StretchHorizontally = true;
            this.DoctoRelacionado.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.DoctoRelacionado.UseCompatibleTextRendering = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.DoctoRelacionado.GetChildAt(2))).Enabled = false;
            // 
            // lblComprobante
            // 
            this.lblComprobante.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblComprobante.DisplayName = "commandBarLabel1";
            this.lblComprobante.Name = "lblComprobante";
            this.lblComprobante.Text = "Comprobante:";
            this.lblComprobante.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblComprobante.UseCompatibleTextRendering = false;
            // 
            // HostComprobante
            // 
            this.HostComprobante.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.HostComprobante.DisplayName = "Comprobantes";
            this.HostComprobante.MinSize = new System.Drawing.Size(420, 0);
            this.HostComprobante.Name = "HostComprobante";
            this.HostComprobante.Text = "Comprobantes";
            this.HostComprobante.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.HostComprobante.UseCompatibleTextRendering = false;
            // 
            // Agregar
            // 
            this.Agregar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Agregar.DisplayName = "Agregar";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Agregar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Agregar.UseCompatibleTextRendering = false;
            // 
            // Remover
            // 
            this.Remover.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Remover.DisplayName = "Quitar";
            this.Remover.DrawText = true;
            this.Remover.Image = global::Jaeger.UI.Comprobante.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Quitar";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Remover.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Remover.UseCompatibleTextRendering = false;
            // 
            // Buscar
            // 
            this.Buscar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Buscar.DisplayName = "Buscar";
            this.Buscar.DrawText = true;
            this.Buscar.Image = global::Jaeger.UI.Comprobante.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Buscar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Buscar.UseCompatibleTextRendering = false;
            // 
            // Separador1
            // 
            this.Separador1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador1.DisplayName = "commandBarSeparator1";
            this.Separador1.Name = "Separador1";
            this.Separador1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separador1.UseCompatibleTextRendering = false;
            this.Separador1.VisibleInOverflowMenu = false;
            // 
            // ImportesPagadosLabel
            // 
            this.ImportesPagadosLabel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ImportesPagadosLabel.DisplayName = "commandBarLabel2";
            this.ImportesPagadosLabel.Name = "ImportesPagadosLabel";
            this.ImportesPagadosLabel.Text = "Importes Pagados:";
            this.ImportesPagadosLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ImportesPagadosLabel.UseCompatibleTextRendering = false;
            // 
            // ImportePagado
            // 
            this.ImportePagado.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ImportePagado.DisplayName = "commandBarLabel1";
            this.ImportePagado.Name = "ImportePagado";
            this.ImportePagado.Text = "";
            this.ImportePagado.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ImportePagado.UseCompatibleTextRendering = false;
            // 
            // Total
            // 
            this.Total.DisplayName = "Total";
            this.Total.MaxSize = new System.Drawing.Size(85, 22);
            this.Total.MinSize = new System.Drawing.Size(85, 22);
            this.Total.Name = "Total";
            this.Total.Text = "0";
            this.Total.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.Total.GetChildAt(0))).Text = "0";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.Total.GetChildAt(0))).MinSize = new System.Drawing.Size(85, 22);
            // 
            // TbComplementoPagoDoctoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CommandBar);
            this.Name = "TbComplementoPagoDoctoControl";
            this.Size = new System.Drawing.Size(1069, 30);
            this.Load += new System.EventHandler(this.TbComplementoPagoDoctoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CommandBar)).EndInit();
            this.CommandBar.ResumeLayout(false);
            this.CommandBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar CommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement ToolBar;
        private Telerik.WinControls.UI.CommandBarStripElement DoctoRelacionado;
        private Telerik.WinControls.UI.CommandBarLabel lblComprobante;
        private Telerik.WinControls.UI.CommandBarHostItem HostComprobante;
        private Telerik.WinControls.UI.CommandBarSeparator Separador1;
        private Telerik.WinControls.UI.CommandBarLabel ImportePagado;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Documento;
        protected internal Telerik.WinControls.UI.CommandBarButton Agregar;
        protected internal Telerik.WinControls.UI.CommandBarButton Remover;
        protected internal Telerik.WinControls.UI.CommandBarButton Buscar;
        public Telerik.WinControls.UI.CommandBarLabel ImportesPagadosLabel;
        protected internal Telerik.WinControls.UI.CommandBarMaskedEditBox Total;
    }
}
