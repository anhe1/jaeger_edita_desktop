﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class TbComplementoPagoDoctoControl : UserControl {
        public TbComplementoPagoDoctoControl() {
            InitializeComponent();
        }

        private void TbComplementoPagoDoctoControl_Load(object sender, EventArgs e) {
            this.HostComprobante.HostedItem = this.Documento.MultiColumnComboBoxElement;
            this.Documento.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.Documento.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.Total.CommandBarMaskedTextBoxElement.TextBoxItem.ReadOnly = true;
            this.Total.CommandBarMaskedTextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Right;
            this.Total.CommandBarMaskedTextBoxElement.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Total.CommandBarMaskedTextBoxElement.Mask = "N2";
        }
    }
}
