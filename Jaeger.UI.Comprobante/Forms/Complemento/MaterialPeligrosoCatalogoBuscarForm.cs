﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class MaterialPeligrosoCatalogoBuscarForm : RadForm {
        protected IClaveMaterialPeligrosoCatalogo catalogo;
        private CveMaterialPeligroso seleccionado = null;
        public event EventHandler<CveMaterialPeligroso> Seleted;

        public void OnSelected(CveMaterialPeligroso e) {
            if (this.Seleted != null) {
                this.Seleted(this, e);
            }
        }
        public MaterialPeligrosoCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new MaterialPeligrosoCatalogo();
        }

        private void CatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.gridUnidades.Standard();
            this.catalogo.Load();
            this.gridUnidades.DataSource = this.catalogo.Items;
            this.TCatalogo.Descripcion.TextChanged += Descripcion_TextChanged;
        }

        private void Descripcion_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Descripcion.Text.Length > 3) {
                this.TCatalogo.Buscar.PerformClick();
            }
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            this.gridUnidades.DataSource = this.catalogo.GetSearch(this.TCatalogo.Buscar.Text);
        }

        private void TCatalogo_Agregar_Click(object sender, EventArgs e) {
            if (this.gridUnidades.CurrentRow != null) {
                this.seleccionado = this.gridUnidades.CurrentRow.DataBoundItem as CveMaterialPeligroso;
                if (seleccionado != null) {
                    this.OnSelected(this.seleccionado);
                    this.Close();
                }
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
