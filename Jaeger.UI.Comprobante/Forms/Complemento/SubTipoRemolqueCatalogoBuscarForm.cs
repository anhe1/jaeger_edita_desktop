﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Repositories;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class SubTipoRemolqueCatalogoBuscarForm : RadForm {
        protected IClaveSubTipoRemCatalogo catalogo;
        private CveSubTipoRem seleccionado = null;
        public event EventHandler<CveSubTipoRem> Seleted;

        public void OnSelected(CveSubTipoRem e) {
            if (this.Seleted != null) {
                this.Seleted(this, e);
            }
        }
        public SubTipoRemolqueCatalogoBuscarForm() {
            InitializeComponent();
            this.catalogo = new SubTipoRemCatalogo();
        }

        private void TipoEmbalajeCatalogoBuscarForm_Load(object sender, EventArgs e) {
            this.gridCatalogo.Standard();
            this.catalogo.Load();
            this.gridCatalogo.DataSource = this.catalogo.Items;
            this.TCatalogo.Buscar.Click += this.TCatalogo_Actualizar_Click;
            this.TCatalogo.Agregar.Click += this.TCatalogo_Selaccionar_Click;
            this.TCatalogo.Descripcion.TextChanged += Buscar_TextChanged;
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.TCatalogo.Descripcion.Text.Length > 3) {
                this.TCatalogo.Buscar.PerformClick();
            }
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            this.gridCatalogo.DataSource = this.catalogo.GetSearchBy(this.TCatalogo.Descripcion.Text);
        }

        private void TCatalogo_Selaccionar_Click(object sender, EventArgs e) {
            if (this.gridCatalogo.CurrentRow != null) {
                this.seleccionado = this.gridCatalogo.CurrentRow.DataBoundItem as CveSubTipoRem;
                if (seleccionado != null) {
                    this.OnSelected(this.seleccionado);
                    this.Close();
                }
            }
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
