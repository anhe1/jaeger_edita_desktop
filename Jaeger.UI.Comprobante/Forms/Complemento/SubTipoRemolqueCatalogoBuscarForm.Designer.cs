﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class SubTipoRemolqueCatalogoBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.gridCatalogo = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCatalogo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "Buscar:";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowAgregar = true;
            this.TCatalogo.ShowBuscar = true;
            this.TCatalogo.ShowCerrar = false;
            this.TCatalogo.ShowExistencia = false;
            this.TCatalogo.ShowFiltro = true;
            this.TCatalogo.Size = new System.Drawing.Size(481, 30);
            this.TCatalogo.TabIndex = 0;
            this.TCatalogo.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TCatalogo_Cerrar_Click);
            // 
            // gridCatalogo
            // 
            this.gridCatalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCatalogo.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.Width = 75;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 250;
            this.gridCatalogo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.gridCatalogo.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridCatalogo.Name = "gridCatalogo";
            this.gridCatalogo.Size = new System.Drawing.Size(481, 285);
            this.gridCatalogo.TabIndex = 5;
            // 
            // TipoEmbalajeCatalogoBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 315);
            this.Controls.Add(this.gridCatalogo);
            this.Controls.Add(this.TCatalogo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TipoEmbalajeCatalogoBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catálogo: Tipo de Embalaje";
            this.Load += new System.EventHandler(this.TipoEmbalajeCatalogoBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridCatalogo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarBuscarControl TCatalogo;
        private Telerik.WinControls.UI.RadGridView gridCatalogo;
    }
}