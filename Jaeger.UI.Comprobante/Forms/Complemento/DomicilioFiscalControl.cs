﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class DomicilioFiscalControl : UserControl {
        private bool bSupressEstadoCode;
        protected ICveTipoEstacionCatalogo cveTipoEstacion;
        protected IClaveEstacionesCatalogo claveEstacionesCatalogo;
        protected ICveEstadoCatalogo cveEstadoCatalogo;
        protected IClaveMunicipioCatalogo claveMunicipioCatalogo;
        protected IClaveLocalidadCatalogo claveLocalidadCatalogo;
        protected IClaveColoniaCatalogo claveColoniaCatalogo;
        protected System.Collections.Generic.List<Catalogos.Entities.ClavePais> Paises;
        public event EventHandler<Catalogos.Entities.ClavePais> ClavePais;
        public event EventHandler<Catalogos.Entities.CveEstado> d2;
        //public event EventHandler<Catalogos.Entities.CveMunicipio> d3;
        public event EventHandler<Catalogos.Entities.ClaveCodigoPostal> ClaveCodigoPostal;

        public void OnClavePaisChange(Catalogos.Entities.ClavePais e) {
            if (this.ClavePais != null) {
                this.ClavePais(this, e);
            }
        }

        public void OnEstadoChange(Catalogos.Entities.CveEstado e) {
            if (this.d2 != null) {
                this.d2(this, e);
            }
        }

        public void OnCodigoPostalChange(Catalogos.Entities.ClaveCodigoPostal codigoPostal) {
            if (this.ClaveCodigoPostal != null) {
                this.ClaveCodigoPostal(this, codigoPostal);
            }
        }

        public DomicilioFiscalControl() {
            InitializeComponent();
        }

        private void DomicilioFiscalControl_Load(object sender, EventArgs e) {
            this.Paises = new System.Collections.Generic.List<Catalogos.Entities.ClavePais>() {
                new Catalogos.Entities.ClavePais { Clave = "MEX" },
                new Catalogos.Entities.ClavePais { Clave = "USA" },
                new Catalogos.Entities.ClavePais { Clave = "CAN" }
            };

            this.cveEstadoCatalogo = new CartaPorteEstadosCatalogo();
            this.cveEstadoCatalogo.Load();

            this.claveMunicipioCatalogo = new MunicipioCatalogo();
            this.claveMunicipioCatalogo.Load();

            this.claveLocalidadCatalogo = new LocalidadCatalogo();
            this.claveLocalidadCatalogo.Load();

            this.claveColoniaCatalogo = new ColoniaCatalogo();
            this.claveColoniaCatalogo.Load();

            this.claveEstacionesCatalogo = new EstacionesCatalogo();
            this.claveEstacionesCatalogo.Load();

            this.Pais.SelectedValueChanged += this.Pais_SelectedValueChanged;
            this.Pais.ValueMember = "Clave";
            this.Pais.DisplayMember = "Clave";
            this.Pais.DataSource = this.Paises;

            this.Estado.DropDownListElement.SyncSelectionWithText = false;
            this.Estado.DisplayMember = "Descriptor";
            this.Estado.ValueMember = "Clave";
            this.bSupressEstadoCode = true;
            this.Estado.SelectedValueChanged += this.Estado_SelectedValueChanged;
            this.bSupressEstadoCode = false;

            this.Municipio.DisplayMember = "Descriptor";
            this.Municipio.ValueMember = "Clave";

            this.Localidad.DisplayMember = "Descriptor";
            this.Localidad.ValueMember = "Clave";

            this.Colonia.ValueMember = "Clave";
            this.Colonia.DisplayMember = "Descriptor";

            this.CodigoPostal.TextChanged += this.CodigoPostal_TextChanged;
        }

        public virtual void Pais_SelectedValueChanged(object sender, EventArgs e) {
            this.bSupressEstadoCode = true;
            if (this.Pais.SelectedValue != null) {
                this.OnClavePaisChange(new Catalogos.Entities.ClavePais() { Clave = this.Pais.SelectedValue.ToString() });
                this.Estado.DataSource = this.cveEstadoCatalogo.Items.Where(it => it.ClavePais == this.Pais.SelectedValue.ToString());
            }
            this.Estado.DisplayMember = "Descriptor";
            this.Estado.ValueMember = "Clave";
            this.bSupressEstadoCode = false;
        }

        public virtual void Estado_SelectedValueChanged(object sender, EventArgs e) {
            if (this.bSupressEstadoCode == false) {
                var c = (Telerik.WinControls.UI.Data.ValueChangedEventArgs)e;
                if (c.OldValue == null && c.NewValue == null)
                    return;
                var f = c.OldValue == null ? c.NewValue.ToString() : c.OldValue.ToString();
                if (c.NewValue != null) {
                    f = c.NewValue.ToString();
                }

                if (this.Estado.SelectedItem != null)
                    this.OnEstadoChange(this.Estado.SelectedItem.DataBoundItem as Catalogos.Entities.CveEstado);
                else
                    this.OnEstadoChange(new Catalogos.Entities.CveEstado { Clave = f });

                this.Municipio.DataSource = this.claveMunicipioCatalogo.Items.Where(it => it.Estado == f);
                this.Localidad.DataSource = this.claveLocalidadCatalogo.Items.Where(it => it.Estado == f);
            }
        }

        public virtual void CodigoPostal_TextChanged(object sender, EventArgs e) {
            if (ValidacionService.CodigoPostal(this.CodigoPostal.Text)) {
                this.OnCodigoPostalChange(new Catalogos.Entities.ClaveCodigoPostal { Clave = this.CodigoPostal.Text });
                this.Colonia.DataSource = this.claveColoniaCatalogo.Items.Where(it => it.CodigoPostal == this.CodigoPostal.Text);
            }
        }
    }
}
