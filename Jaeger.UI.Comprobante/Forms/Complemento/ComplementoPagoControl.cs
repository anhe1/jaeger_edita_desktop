﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.UI.Comprobante.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Comprobante.Forms {
    /// <summary>
    /// complemento de recepcion de pagos
    /// </summary>
    public partial class ComplementoPagoControl : UserControl, IComplementoControl {
        #region declaraciones
        protected IFormaPagoCatalogo catalogoFormaPagoP;
        protected ITipoCadenaPagoCatalogo catalogoTipoCadenaPago;
        protected IBancosCatalogo catalgoBancos;
        protected IMonedaService monedaService = new MonedaService();
        private bool editable = true;
        #endregion

        [Bindable(true)]
        public ComplementoPagoDetailModel Complemento {
            get; set;
        }

        public ComplementoPagoControl() {
            InitializeComponent();
        }

        public string Caption { get; set; }

        public bool Editable {
            get {
                return this.editable;
            }
            set {
                this.editable = value;
                this.SetEditable();
            }
        }

        public virtual void Start() {
            this.catalogoFormaPagoP = new FormaPagoCatalogo();
            this.catalogoFormaPagoP.Load();
            this.catalgoBancos = new BancosCatalogo();
            this.catalgoBancos.Load();
            this.catalogoTipoCadenaPago = new TipoCadenaPagoCatalogo();
            this.catalogoTipoCadenaPago.Load();

            // catalogo formas de pago
            this.FormaPago.DataSource = this.catalogoFormaPagoP.Items;
            this.FormaPago.DisplayMember = "Descriptor";
            this.FormaPago.ValueMember = "Clave";
            this.FormaPago.SelectedIndex = -1;

            this.NomBancoOrdExt.DataSource = this.catalgoBancos.Items;
            this.NomBancoOrdExt.DisplayMember = "Descripcion";
            this.NomBancoOrdExt.ValueMember = "Clave";

            this.TipoCadenaPago.DataSource = this.catalogoTipoCadenaPago.Items;
            this.TipoCadenaPago.DisplayMember = "Clave";
            this.TipoCadenaPago.ValueMember = "Clave";

            this.Moneda.DataSource = this.monedaService.GetMonedas();
            this.Moneda.SelectedIndex = -1;
            this.Moneda.DisplayMember = "Clave";
            this.Moneda.ValueMember = "Clave";

            var _ObjetoImp = this.DoctoRelacionado.Columns["ObjetoImpDR"] as GridViewMultiComboBoxColumn;
            _ObjetoImp.Tag = false;
            _ObjetoImp.DataSource = ComprobanteCommonService.GetObjetoImpuesto();
        }

        private void ComplementoPagoControl_Load(object sender, EventArgs e) {
            this.Caption = "Complemento: Recepción de Pagos";
            this.FechaPago.DateTimePickerElement.ToolTipText = Properties.Resources.ComplementoPago_ToolTip_FechaPago;
            this.FormaPago.MultiColumnComboBoxElement.ToolTipText = "Clave de la forma en que se realiza el pago.";
            this.Moneda.MultiColumnComboBoxElement.ToolTipText = "Clave de la moneda utilizada para realizar el pago, cuando se usa moneda nacional se registra MXN. El atributo Pagos:Pago:Monto y los atributos TotalImpuestosRetenidos, TotalImpuestosTrasladados, Traslados:Traslado:Importe y Retenciones:Retencion:Importe del nodo Pago:Impuestos deben ser expresados en esta moneda. Conforme con la especificación ISO 4217.";
            this.TipoCambio.SpinElement.ToolTipText = Properties.Resources.ComplementoPago_ToolTip_TipoCambioDR;
            this.Monto.MaskedEditBoxElement.ToolTipText = "Importe del pago";
            this.NumOperacion.TextBoxElement.ToolTipText = "Para expresar el número de cheque, número de autorización, número de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia análogo que identifique la operación que ampara el pago efectuado";
            
            this.TComprobante.Agregar.Click += this.TComprobante_Agrega_Click;
            this.TComprobante.Buscar.Click += this.TComprobante_Buscar_Click;
            this.TComprobante.Remover.Click += this.TComprobante_Quitar_Click;

            this.TTraslado.Nuevo.Click += this.TTraslados_Nuevo_Click;
            this.TTraslado.Remover.Click += this.TTraslados_Remover_Click;
            this.TTraslado.Filtro.Click += this.TTraslados_Filtro_Click;

            this.TRetencion.Nuevo.Click += this.TRetencion_Nuevo_Click;
            this.TRetencion.Remover.Click += this.TRetencion_Remover_Click;
            this.TRetencion.Filtro.Click += this.TRetencion_Filtro_Click;

            var _traslados_tipo = this.Traslados.Columns["Tipo"] as GridViewComboBoxColumn;
            _traslados_tipo.DisplayMember = "Descripcion";
            _traslados_tipo.ValueMember = "Id";
            _traslados_tipo.DataSource = ConfigService.GetImpuestoTipo();

            var _traslados_impuesto = this.Traslados.Columns["Impuesto"] as GridViewComboBoxColumn;
            _traslados_impuesto.DisplayMember = "Descripcion";
            _traslados_impuesto.ValueMember = "Id";
            _traslados_impuesto.DataSource = ConfigService.GetImpuestos();

            var _traslados_factor = this.Traslados.Columns["TipoFactor"] as GridViewComboBoxColumn;
            _traslados_factor.DisplayMember = "Descripcion";
            _traslados_factor.ValueMember = "Id";
            _traslados_factor.DataSource = ConfigService.GetImpuestoTipoFactor();

            var _retenciones_tipo = this.Retenciones.Columns["Tipo"] as GridViewComboBoxColumn;
            _retenciones_tipo.DisplayMember = "Descripcion";
            _retenciones_tipo.ValueMember = "Id";
            _retenciones_tipo.DataSource = ConfigService.GetImpuestoTipo();

            var _retenciones_impuesto = this.Retenciones.Columns["Impuesto"] as GridViewComboBoxColumn;
            _retenciones_impuesto.DisplayMember = "Descripcion";
            _retenciones_impuesto.ValueMember = "Id";
            _retenciones_impuesto.DataSource = ConfigService.GetImpuestos();

            var _retenciones_factor = this.Retenciones.Columns["TipoFactor"] as GridViewComboBoxColumn;
            _retenciones_factor.DisplayMember = "Descripcion";
            _retenciones_factor.ValueMember = "Id";
            _retenciones_factor.DataSource = ConfigService.GetImpuestoTipoFactor();

            var totales = new ExpressionFormattingObject("Importes", "ImpPagado > Total OR ImpSaldoAnt > Total", true) { RowForeColor = Color.Red };
            this.DoctoRelacionado.Columns["Estado"].ConditionalFormattingObjectList.Add(totales);
        }

        #region barra de herramientas
        public virtual void TComprobante_Agrega_Click(object sender, EventArgs e) {
            this.DoctoRelacionado.Rows.AddNew();
        }

        public virtual void TComprobante_Quitar_Click(object sender, EventArgs e) {
            if (this.DoctoRelacionado.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var _seleccionado = this.DoctoRelacionado.CurrentRow.DataBoundItem as PagosPagoDoctoRelacionadoDetailModel;
                    if (_seleccionado.IdRelacion == 0) {
                        try {
                            this.DoctoRelacionado.Rows.Remove(this.DoctoRelacionado.CurrentRow);
                        } catch (Exception ex) {
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        _seleccionado.Activo = false;
                        this.DoctoRelacionado.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TComprobante_Buscar_Click(object sender, EventArgs e) {
            if (this.ReceptorRFC.Text.Length > 0) {
                var buscar = new ComprobantesBuscarForm(this.ReceptorRFC.Text, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido);
                buscar.AgregarComprobante += Buscar_AgregarComprobante;
                buscar.ShowDialog(this.ParentForm);
            } else {
                RadMessageBox.Show(this, Properties.Resources.Message_Contribuyente_Seleciona, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        public virtual void TTraslados_Nuevo_Click(object sender, EventArgs e) {
            if (this.DoctoRelacionado.CurrentRow != null) {
                var seleccionado = this.DoctoRelacionado.CurrentRow.DataBoundItem as PagosPagoDoctoRelacionadoModel;
                seleccionado.Traslados.Add(new PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR() { 
                    Base = seleccionado.ImpPagado / new decimal(1.16), TasaOCuota = new decimal(0.16) 
                });
            }
            //this.Traslados.Rows.AddNew();
        }

        public virtual void TTraslados_Remover_Click(object sender, EventArgs e) {
            if (this.Traslados.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    try {
                        this.Traslados.Rows.Remove(this.Traslados.CurrentRow);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public virtual void TTraslados_Filtro_Click(object sender, EventArgs e) {
            this.Traslados.ShowFilteringRow = this.TTraslado.Filtro.ToggleState == ToggleState.Off;
            if (this.Traslados.ShowFilteringRow == false) {
                this.Traslados.FilterDescriptors.Clear();
            }
        }

        public virtual void TRetencion_Nuevo_Click(object sender, EventArgs e) {
            this.Retenciones.Rows.AddNew();
        }

        public virtual void TRetencion_Remover_Click(object sender, EventArgs e) {
            if (this.Retenciones.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    try {
                        this.Retenciones.Rows.Remove(this.Retenciones.CurrentRow);
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public virtual void TRetencion_Filtro_Click(object sender, EventArgs e) {
            this.Retenciones.ShowFilteringRow = this.TRetencion.Filtro.ToggleState == ToggleState.Off;
            if (this.Retenciones.ShowFilteringRow == false) {
                this.Retenciones.FilterDescriptors.Clear();
            }
        }

        public virtual void ButtonSPEI_Click(object sender, EventArgs e) {
            var dialogoAbrirArchivo = new OpenFileDialog() {
                Title = "Archivo",
                Multiselect = false,
                Filter = "*.xml|*.XML"
            };

            if (dialogoAbrirArchivo.ShowDialog() == DialogResult.OK) {

            }
        }
        #endregion

        #region acciones del grid
        /// <summary>
        /// Agregar el documento seleccionado a la lista 
        /// </summary>
        private void Buscar_AgregarComprobante(object sender, Domain.Comprobante.Entities.ComprobanteFiscalDetailSingleModel e) {
            if (e != null) {
                if (this.Complemento.Search(e.IdDocumento) == null) {
                    var _nuevo = new PagosPagoDoctoRelacionadoDetailModel {
                        Activo = true,
                        FechaEmision = e.FechaEmision,
                        Folio = e.Folio,
                        EquivalenciaDR = e.TipoCambio,
                        FormaDePagoP = e.ClaveFormaPago,
                        IdComprobanteR = e.Id,
                        IdDocumentoDR = e.IdDocumento,
                        IdSubTipo = e.IdSubTipo,
                        MetodoPago = e.ClaveMetodoPago,
                        MonedaDR = e.ClaveMoneda,
                        NumParcialidad = e.NumParcialidad + 1,
                        Nombre = e.ReceptorNombre,
                        ImpSaldoAnt = e.ImportePagado,
                        SerieDR = e.Serie,
                        ImpPagado = e.ImportePagado,
                        ImpSaldoInsoluto = 0,
                        RFC = e.ReceptorRFC,
                        Total = e.Total
                    };
                    this.Complemento.DoctoRelacionados.Add(_nuevo);
                } else {
                    RadMessageBox.Show(this, Properties.Resources.complementoPago_Existe_UUID, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void DoctoRelacionado_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Column.Name == "ObjetoImpDR") {
                if ((bool)e.Column.Tag == false) {
                    var editor = (RadMultiColumnComboBoxElement)this.DoctoRelacionado.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    var _col1 = new GridViewTextBoxColumn() { Name = "Id", HeaderText = "Clave", FieldName = "Id", Width = 40, TextAlignment = ContentAlignment.MiddleCenter };
                    var _col2 = new GridViewTextBoxColumn() { Name = "Descripcion", HeaderText = "Descripción", FieldName = "Descripcion", Width = 150 };
                    editor.EditorControl.Columns.AddRange(new GridViewDataColumn[] { _col1, _col2 });
                    editor.EditorControl.ShowRowHeaderColumn = false;
                    editor.AutoSizeDropDownToBestFit = true;
                    e.Column.Tag = true;
                }
            }
        }
        #endregion

        #region metodos 
        public virtual void SetEditable() {
            this.FechaPago.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = !this.Editable;
            this.FechaPago.DateTimePickerElement.ArrowButton.Visibility = (this.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FormaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.Editable;
            this.FormaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.NumOperacion.ReadOnly = !this.Editable;

            this.TipoCambio.ReadOnly = !this.Editable;

            this.Monto.ReadOnly = !this.Editable;

            this.Moneda.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.Editable;
            this.Moneda.MultiColumnComboBoxElement.ArrowButton.Visibility = this.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible;

            this.RFCEmisorCtaOrigen.ReadOnly = !this.Editable;

            this.NomBancoOrdExt.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.Editable;
            this.NomBancoOrdExt.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CtaOrdenante.ReadOnly = !this.Editable;

            this.RFCCtaBeneficiario.ReadOnly = !this.Editable;

            this.CuentaBeneficiario.ReadOnly = !this.Editable;

            this.TipoCadenaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this.Editable;
            this.TipoCadenaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.Editable == false ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.CadenaPago.ReadOnly = !this.Editable;

            this.CertificadoPago.ReadOnly = !this.Editable;

            this.SelloPago.ReadOnly = !this.Editable;

            this.DoctoRelacionado.AllowEditRow = this.Editable;
            this.Retenciones.AllowEditRow = this.Editable;
            this.Traslados.AllowEditRow = this.Editable;

            this.TTraslado.Enabled = this.Editable;
            this.TRetencion.Enabled = this.Editable;
            this.TComprobante.Enabled = this.Editable;

            this.ButtonSPEI.Enabled = this.Editable;
        }

        public virtual void CreateBinding() {
            this.FechaPago.DataBindings.Clear();
            this.FechaPago.DataBindings.Add("Value", this.Complemento, "FechaPagoP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FormaPago.DataBindings.Clear();
            this.FormaPago.DataBindings.Add("SelectedValue", this.Complemento, "FormaDePagoP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumOperacion.DataBindings.Clear();
            this.NumOperacion.DataBindings.Add("Text", this.Complemento, "NumOperacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoCambio.DataBindings.Clear();
            this.TipoCambio.DataBindings.Add("Value", this.Complemento, "TipoCambioP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Monto.DataBindings.Clear();
            this.Monto.DataBindings.Add("Text", this.Complemento, "Monto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.DataBindings.Add("Text", this.Complemento, "MonedaP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RFCEmisorCtaOrigen.DataBindings.Clear();
            this.RFCEmisorCtaOrigen.DataBindings.Add("Text", this.Complemento, "RfcEmisorCtaOrd", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NomBancoOrdExt.DataBindings.Clear();
            this.NomBancoOrdExt.DataBindings.Add("Text", this.Complemento, "NomBancoOrdExt", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CtaOrdenante.DataBindings.Clear();
            this.CtaOrdenante.DataBindings.Add("Text", this.Complemento, "CtaOrdenante", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RFCCtaBeneficiario.DataBindings.Clear();
            this.RFCCtaBeneficiario.DataBindings.Add("Text", this.Complemento, "RfcEmisorCtaBen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CuentaBeneficiario.DataBindings.Clear();
            this.CuentaBeneficiario.DataBindings.Add("Text", this.Complemento, "CtaBeneficiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoCadenaPago.DataBindings.Clear();
            this.TipoCadenaPago.DataBindings.Add("Text", this.Complemento, "TipoCadPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CadenaPago.DataBindings.Clear();
            this.CadenaPago.DataBindings.Add("Text", this.Complemento, "CadPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CertificadoPago.DataBindings.Clear();
            this.CertificadoPago.DataBindings.Add("Text", this.Complemento, "CertPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SelloPago.DataBindings.Clear();
            this.SelloPago.DataBindings.Add("Text", this.Complemento, "SelloPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DoctoRelacionado.DataSource = this.Complemento.DoctoRelacionados;
            this.Traslados.DataSource = this.Complemento.DoctoRelacionados;
            this.Retenciones.DataSource = this.Complemento.DoctoRelacionados;

            this.TComprobante.Total.DataBindings.Clear();
            this.TComprobante.Total.DataBindings.Add("Text", this.Complemento, "Total", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public virtual bool Validar() {
            this.errorProvider1.Clear();
            if (this.Complemento.MonedaP == null) {
                this.errorProvider1.SetError(this.Moneda, "Es necesario seleccionar la clave de moneda");
                return false;
            }

            if (this.Complemento.FormaDePagoP == null) {
                this.errorProvider1.SetError(this.FormaPago, "Es necesario seleccionar la forma de pago.");
                return false;
            }

            if (this.Complemento.Monto <= 0) {
                this.errorProvider1.SetError(this.Monto, "Debes ingresar el monto del pago.");
                return false;
            }

            if (this.Complemento.DoctoRelacionados.Count <= 0) {
                this.errorProvider1.SetError(this.DoctoRelacionado, "No se relacionaron comprobantes a este comprobante.");
                return false;
            }
            return true;
        }
        #endregion
    }
}
