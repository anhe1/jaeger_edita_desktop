﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class ComplementoCartaPorteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Generales = new Telerik.WinControls.UI.RadGroupBox();
            this.PaisOrigenDestino = new Telerik.WinControls.UI.RadDropDownList();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.EntradaSalidaMerc = new Telerik.WinControls.UI.RadDropDownList();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.TotalDistRec = new Telerik.WinControls.UI.RadSpinEditor();
            this.ViaEntradaSalida = new Telerik.WinControls.UI.RadDropDownList();
            this.TranspInternac = new Telerik.WinControls.UI.RadDropDownList();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.pageUbicaciones = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridUbicaciones = new Telerik.WinControls.UI.RadGridView();
            this.pageMercancia = new Telerik.WinControls.UI.RadPageViewPage();
            this.TabMercancias = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridMercancias = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PesoNetoTotal = new Telerik.WinControls.UI.RadTextBox();
            this.label10 = new Telerik.WinControls.UI.RadLabel();
            this.PesoBrutoTotal = new Telerik.WinControls.UI.RadTextBox();
            this.CargoPorTasacion = new Telerik.WinControls.UI.RadTextBox();
            this.UnidadPeso = new Telerik.WinControls.UI.RadDropDownList();
            this.label9 = new Telerik.WinControls.UI.RadLabel();
            this.label8 = new Telerik.WinControls.UI.RadLabel();
            this.NumTotalMercancias = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.pageAutoTransporte = new Telerik.WinControls.UI.RadPageViewPage();
            this.pageTransporte = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridTransporte = new Telerik.WinControls.UI.RadGridView();
            this.TUbicacion = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TMercancia = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TTransporte = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.AutotransporteControl = new Jaeger.UI.Comprobante.Forms.CartaPorteMercanciasAutotransporteControl();
            ((System.ComponentModel.ISupportInitialize)(this.Generales)).BeginInit();
            this.Generales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PaisOrigenDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntradaSalidaMerc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViaEntradaSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TranspInternac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.pageUbicaciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridUbicaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUbicaciones.MasterTemplate)).BeginInit();
            this.pageMercancia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabMercancias)).BeginInit();
            this.TabMercancias.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMercancias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMercancias.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PesoNetoTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoBrutoTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoPorTasacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnidadPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumTotalMercancias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            this.pageAutoTransporte.SuspendLayout();
            this.pageTransporte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTransporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTransporte.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Generales
            // 
            this.Generales.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Generales.Controls.Add(this.PaisOrigenDestino);
            this.Generales.Controls.Add(this.label7);
            this.Generales.Controls.Add(this.EntradaSalidaMerc);
            this.Generales.Controls.Add(this.label6);
            this.Generales.Controls.Add(this.TotalDistRec);
            this.Generales.Controls.Add(this.ViaEntradaSalida);
            this.Generales.Controls.Add(this.TranspInternac);
            this.Generales.Controls.Add(this.label3);
            this.Generales.Controls.Add(this.label2);
            this.Generales.Controls.Add(this.label1);
            this.Generales.Dock = System.Windows.Forms.DockStyle.Top;
            this.Generales.HeaderText = "Generales:";
            this.Generales.Location = new System.Drawing.Point(0, 0);
            this.Generales.Name = "Generales";
            this.Generales.Size = new System.Drawing.Size(1117, 47);
            this.Generales.TabIndex = 1;
            this.Generales.TabStop = false;
            this.Generales.Text = "Generales:";
            // 
            // PaisOrigenDestino
            // 
            this.PaisOrigenDestino.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "MXN";
            radListDataItem2.Text = "USA";
            this.PaisOrigenDestino.Items.Add(radListDataItem1);
            this.PaisOrigenDestino.Items.Add(radListDataItem2);
            this.PaisOrigenDestino.Location = new System.Drawing.Point(1017, 17);
            this.PaisOrigenDestino.Name = "PaisOrigenDestino";
            this.PaisOrigenDestino.Size = new System.Drawing.Size(88, 20);
            this.PaisOrigenDestino.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(904, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "País Origen/Destino:";
            // 
            // EntradaSalidaMerc
            // 
            this.EntradaSalidaMerc.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.EntradaSalidaMerc.Location = new System.Drawing.Point(813, 17);
            this.EntradaSalidaMerc.Name = "EntradaSalidaMerc";
            this.EntradaSalidaMerc.Size = new System.Drawing.Size(85, 20);
            this.EntradaSalidaMerc.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(661, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "mercancías Entrada / Salida:";
            // 
            // TotalDistRec
            // 
            this.TotalDistRec.DecimalPlaces = 2;
            this.TotalDistRec.Location = new System.Drawing.Point(572, 17);
            this.TotalDistRec.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.TotalDistRec.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.TotalDistRec.Name = "TotalDistRec";
            this.TotalDistRec.NullableValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.TotalDistRec.Size = new System.Drawing.Size(77, 20);
            this.TotalDistRec.TabIndex = 5;
            this.TotalDistRec.TabStop = false;
            this.TotalDistRec.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.TotalDistRec.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // ViaEntradaSalida
            // 
            this.ViaEntradaSalida.Location = new System.Drawing.Point(275, 17);
            this.ViaEntradaSalida.Name = "ViaEntradaSalida";
            this.ViaEntradaSalida.Size = new System.Drawing.Size(161, 20);
            this.ViaEntradaSalida.TabIndex = 4;
            // 
            // TranspInternac
            // 
            this.TranspInternac.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TranspInternac.Location = new System.Drawing.Point(103, 17);
            this.TranspInternac.Name = "TranspInternac";
            this.TranspInternac.Size = new System.Drawing.Size(121, 20);
            this.TranspInternac.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(442, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total distancia recorrida:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(238, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vía:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de Traslado:";
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.pageUbicaciones);
            this.TabControl.Controls.Add(this.pageMercancia);
            this.TabControl.Controls.Add(this.pageTransporte);
            this.TabControl.DefaultPage = this.pageUbicaciones;
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 47);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.pageMercancia;
            this.TabControl.Size = new System.Drawing.Size(1117, 311);
            this.TabControl.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pageUbicaciones
            // 
            this.pageUbicaciones.Controls.Add(this.gridUbicaciones);
            this.pageUbicaciones.Controls.Add(this.TUbicacion);
            this.pageUbicaciones.ItemSize = new System.Drawing.SizeF(72F, 24F);
            this.pageUbicaciones.Location = new System.Drawing.Point(10, 33);
            this.pageUbicaciones.Name = "pageUbicaciones";
            this.pageUbicaciones.Size = new System.Drawing.Size(1096, 267);
            this.pageUbicaciones.Text = "Ubicaciones";
            // 
            // gridUbicaciones
            // 
            this.gridUbicaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUbicaciones.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridUbicaciones.MasterTemplate.AllowAddNewRow = false;
            this.gridUbicaciones.MasterTemplate.AllowCellContextMenu = false;
            this.gridUbicaciones.MasterTemplate.AllowColumnChooser = false;
            this.gridUbicaciones.MasterTemplate.AllowColumnReorder = false;
            this.gridUbicaciones.MasterTemplate.AllowDragToGroup = false;
            this.gridUbicaciones.MasterTemplate.AllowRowResize = false;
            this.gridUbicaciones.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "TipoUbicacion";
            gridViewTextBoxColumn1.HeaderText = "T. Ubicación";
            gridViewTextBoxColumn1.Name = "TipoUbicacion";
            gridViewTextBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "IDUbicacion";
            gridViewTextBoxColumn2.HeaderText = "ID Ubicación";
            gridViewTextBoxColumn2.Name = "IDUbicacion";
            gridViewTextBoxColumn2.Width = 90;
            gridViewTextBoxColumn3.FieldName = "RFCRemitenteDestinatario";
            gridViewTextBoxColumn3.HeaderText = "RFC";
            gridViewTextBoxColumn3.Name = "RFCRemitenteDestinatario";
            gridViewTextBoxColumn3.Width = 110;
            gridViewTextBoxColumn4.FieldName = "NombreRemitenteDestinatario";
            gridViewTextBoxColumn4.HeaderText = "Nombre ó Razon Social";
            gridViewTextBoxColumn4.Name = "NombreRemitenteDestinatario";
            gridViewTextBoxColumn4.Width = 240;
            gridViewTextBoxColumn5.FieldName = "NumRegIdTrib";
            gridViewTextBoxColumn5.HeaderText = "NumRegIdTrib";
            gridViewTextBoxColumn5.Name = "NumRegIdTrib";
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldName = "ResidenciaFiscal";
            gridViewTextBoxColumn6.HeaderText = "ResidenciaFiscal";
            gridViewTextBoxColumn6.Name = "ResidenciaFiscal";
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.FieldName = "NumEstacion";
            gridViewTextBoxColumn7.HeaderText = "Núm Estación";
            gridViewTextBoxColumn7.Name = "NumEstacion";
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.FieldName = "DomicilioToString";
            gridViewTextBoxColumn8.HeaderText = "Domicilio";
            gridViewTextBoxColumn8.Name = "DomicilioToString";
            gridViewTextBoxColumn8.Width = 350;
            this.gridUbicaciones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.gridUbicaciones.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridUbicaciones.Name = "gridUbicaciones";
            this.gridUbicaciones.ShowGroupPanel = false;
            this.gridUbicaciones.Size = new System.Drawing.Size(1096, 237);
            this.gridUbicaciones.TabIndex = 8;
            // 
            // pageMercancia
            // 
            this.pageMercancia.Controls.Add(this.TabMercancias);
            this.pageMercancia.ItemSize = new System.Drawing.SizeF(68F, 24F);
            this.pageMercancia.Location = new System.Drawing.Point(10, 33);
            this.pageMercancia.Name = "pageMercancia";
            this.pageMercancia.Size = new System.Drawing.Size(1096, 267);
            this.pageMercancia.Text = "Mercancias";
            // 
            // TabMercancias
            // 
            this.TabMercancias.Controls.Add(this.radPageViewPage1);
            this.TabMercancias.Controls.Add(this.pageAutoTransporte);
            this.TabMercancias.DefaultPage = this.radPageViewPage1;
            this.TabMercancias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabMercancias.Location = new System.Drawing.Point(0, 0);
            this.TabMercancias.Name = "TabMercancias";
            this.TabMercancias.SelectedPage = this.radPageViewPage1;
            this.TabMercancias.Size = new System.Drawing.Size(1096, 267);
            this.TabMercancias.TabIndex = 3;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabMercancias.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.gridMercancias);
            this.radPageViewPage1.Controls.Add(this.panel1);
            this.radPageViewPage1.Controls.Add(this.TMercancia);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(68F, 24F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 33);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1075, 223);
            this.radPageViewPage1.Text = "Mercancias";
            // 
            // gridMercancias
            // 
            this.gridMercancias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMercancias.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridMercancias.MasterTemplate.AllowAddNewRow = false;
            this.gridMercancias.MasterTemplate.AllowCellContextMenu = false;
            this.gridMercancias.MasterTemplate.AllowColumnChooser = false;
            this.gridMercancias.MasterTemplate.AllowColumnReorder = false;
            this.gridMercancias.MasterTemplate.AllowDragToGroup = false;
            this.gridMercancias.MasterTemplate.AllowRowResize = false;
            this.gridMercancias.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Cantidad";
            gridViewTextBoxColumn9.FormatString = "{0:N2}";
            gridViewTextBoxColumn9.HeaderText = "Cantidad";
            gridViewTextBoxColumn9.Name = "Cantidad";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn10.FormatString = "{0:N2}";
            gridViewTextBoxColumn10.HeaderText = "Cve. Unidad";
            gridViewTextBoxColumn10.Name = "ClaveUnidad";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.FieldName = "Unidad";
            gridViewTextBoxColumn11.HeaderText = "Unidad";
            gridViewTextBoxColumn11.Name = "Unidad";
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.FieldName = "BienesTransp";
            gridViewTextBoxColumn12.HeaderText = "BienesTransp";
            gridViewTextBoxColumn12.Name = "BienesTransp";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.FieldName = "Descripcion";
            gridViewTextBoxColumn13.HeaderText = "Descripción";
            gridViewTextBoxColumn13.Name = "Descripcion";
            gridViewTextBoxColumn13.Width = 250;
            gridViewTextBoxColumn14.FieldName = "Dimensiones";
            gridViewTextBoxColumn14.HeaderText = "Dimensiones";
            gridViewTextBoxColumn14.Name = "Dimensiones";
            gridViewTextBoxColumn14.Width = 95;
            gridViewCheckBoxColumn1.FieldName = "MaterialPeligroso";
            gridViewCheckBoxColumn1.HeaderText = "Mat. Peligroso";
            gridViewCheckBoxColumn1.Name = "MaterialPeligroso";
            gridViewTextBoxColumn15.FieldName = "CveMaterialPeligroso";
            gridViewTextBoxColumn15.HeaderText = "Cve. Mat. Peligroso";
            gridViewTextBoxColumn15.Name = "CveMaterialPeligroso";
            gridViewTextBoxColumn15.Width = 90;
            gridViewTextBoxColumn16.FieldName = "ClaveSTCC";
            gridViewTextBoxColumn16.HeaderText = "Cve. STCC";
            gridViewTextBoxColumn16.Name = "ClaveSTCC";
            gridViewTextBoxColumn16.Width = 85;
            gridViewTextBoxColumn17.FieldName = "Embalaje";
            gridViewTextBoxColumn17.HeaderText = "Embalaje";
            gridViewTextBoxColumn17.Name = "Embalaje";
            gridViewTextBoxColumn17.Width = 85;
            gridViewTextBoxColumn18.FieldName = "DescripEmbalaje";
            gridViewTextBoxColumn18.HeaderText = "Descripción del Embalaje";
            gridViewTextBoxColumn18.Name = "DescripEmbalaje";
            gridViewTextBoxColumn18.Width = 100;
            gridViewTextBoxColumn19.FieldName = "PesoEnKg";
            gridViewTextBoxColumn19.FormatString = "{0:N2}";
            gridViewTextBoxColumn19.HeaderText = "Peso Kg.";
            gridViewTextBoxColumn19.Name = "PesoEnKg";
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn20.DataType = typeof(decimal);
            gridViewTextBoxColumn20.FieldName = "ValorMercancia";
            gridViewTextBoxColumn20.FormatString = "{0:N2}";
            gridViewTextBoxColumn20.HeaderText = "Valor Mercancia";
            gridViewTextBoxColumn20.Name = "ValorMercancia";
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.FieldName = "Moneda";
            gridViewTextBoxColumn21.FormatString = "{0:N2}";
            gridViewTextBoxColumn21.HeaderText = "Moneda";
            gridViewTextBoxColumn21.Name = "Moneda";
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn21.Width = 85;
            gridViewTextBoxColumn22.FieldName = "FraccionArancelaria";
            gridViewTextBoxColumn22.HeaderText = "Fracción Arancelaria";
            gridViewTextBoxColumn22.Name = "FraccionArancelaria";
            gridViewTextBoxColumn22.Width = 85;
            gridViewTextBoxColumn23.FieldName = "UUIDComercioExt";
            gridViewTextBoxColumn23.HeaderText = "UUID ComercioExt";
            gridViewTextBoxColumn23.Name = "UUIDComercioExt";
            gridViewTextBoxColumn23.Width = 210;
            this.gridMercancias.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23});
            this.gridMercancias.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridMercancias.Name = "gridMercancias";
            this.gridMercancias.ShowGroupPanel = false;
            this.gridMercancias.Size = new System.Drawing.Size(1075, 155);
            this.gridMercancias.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PesoNetoTotal);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.PesoBrutoTotal);
            this.panel1.Controls.Add(this.CargoPorTasacion);
            this.panel1.Controls.Add(this.UnidadPeso);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.NumTotalMercancias);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 185);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1075, 38);
            this.panel1.TabIndex = 1;
            // 
            // PesoNetoTotal
            // 
            this.PesoNetoTotal.Location = new System.Drawing.Point(440, 9);
            this.PesoNetoTotal.Name = "PesoNetoTotal";
            this.PesoNetoTotal.Size = new System.Drawing.Size(72, 20);
            this.PesoNetoTotal.TabIndex = 19;
            this.PesoNetoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(347, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 18);
            this.label10.TabIndex = 18;
            this.label10.Text = "Peso Neto Total:";
            // 
            // PesoBrutoTotal
            // 
            this.PesoBrutoTotal.Location = new System.Drawing.Point(798, 9);
            this.PesoBrutoTotal.Name = "PesoBrutoTotal";
            this.PesoBrutoTotal.Size = new System.Drawing.Size(84, 20);
            this.PesoBrutoTotal.TabIndex = 17;
            this.PesoBrutoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CargoPorTasacion
            // 
            this.CargoPorTasacion.Location = new System.Drawing.Point(625, 9);
            this.CargoPorTasacion.Name = "CargoPorTasacion";
            this.CargoPorTasacion.Size = new System.Drawing.Size(72, 20);
            this.CargoPorTasacion.TabIndex = 16;
            this.CargoPorTasacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // UnidadPeso
            // 
            this.UnidadPeso.Location = new System.Drawing.Point(96, 9);
            this.UnidadPeso.Name = "UnidadPeso";
            this.UnidadPeso.Size = new System.Drawing.Size(245, 20);
            this.UnidadPeso.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(518, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 18);
            this.label9.TabIndex = 14;
            this.label9.Text = "Cargo Por Tasación";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(4, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 18);
            this.label8.TabIndex = 13;
            this.label8.Text = "Unidad de Peso:";
            // 
            // NumTotalMercancias
            // 
            this.NumTotalMercancias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumTotalMercancias.Location = new System.Drawing.Point(999, 9);
            this.NumTotalMercancias.Name = "NumTotalMercancias";
            this.NumTotalMercancias.Size = new System.Drawing.Size(72, 20);
            this.NumTotalMercancias.TabIndex = 12;
            this.NumTotalMercancias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(890, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Núm. de Mercancias";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(703, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 18);
            this.label4.TabIndex = 10;
            this.label4.Text = "Peso Bruto Total:";
            // 
            // pageAutoTransporte
            // 
            this.pageAutoTransporte.Controls.Add(this.AutotransporteControl);
            this.pageAutoTransporte.ItemSize = new System.Drawing.SizeF(88F, 24F);
            this.pageAutoTransporte.Location = new System.Drawing.Point(10, 37);
            this.pageAutoTransporte.Name = "pageAutoTransporte";
            this.pageAutoTransporte.Size = new System.Drawing.Size(1075, 215);
            this.pageAutoTransporte.Text = "Autotransporte";
            // 
            // pageTransporte
            // 
            this.pageTransporte.Controls.Add(this.gridTransporte);
            this.pageTransporte.Controls.Add(this.TTransporte);
            this.pageTransporte.ItemSize = new System.Drawing.SizeF(66F, 24F);
            this.pageTransporte.Location = new System.Drawing.Point(10, 37);
            this.pageTransporte.Name = "pageTransporte";
            this.pageTransporte.Size = new System.Drawing.Size(1096, 263);
            this.pageTransporte.Text = "Transporte";
            // 
            // gridTransporte
            // 
            this.gridTransporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTransporte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridTransporte.MasterTemplate.AllowAddNewRow = false;
            this.gridTransporte.MasterTemplate.AllowCellContextMenu = false;
            this.gridTransporte.MasterTemplate.AllowColumnChooser = false;
            this.gridTransporte.MasterTemplate.AllowColumnReorder = false;
            this.gridTransporte.MasterTemplate.AllowDragToGroup = false;
            this.gridTransporte.MasterTemplate.AllowRowResize = false;
            this.gridTransporte.MasterTemplate.AutoGenerateColumns = false;
            gridViewComboBoxColumn1.FieldName = "TipoFigura";
            gridViewComboBoxColumn1.HeaderText = "T. Figura";
            gridViewComboBoxColumn1.Name = "TipoFigura";
            gridViewComboBoxColumn1.Width = 85;
            gridViewTextBoxColumn24.FieldName = "RFCFigura";
            gridViewTextBoxColumn24.HeaderText = "RFC";
            gridViewTextBoxColumn24.Name = "RFCFigura";
            gridViewTextBoxColumn24.Width = 110;
            gridViewTextBoxColumn25.FieldName = "NumLicencia";
            gridViewTextBoxColumn25.HeaderText = "Núm. Licencia";
            gridViewTextBoxColumn25.Name = "NumLicencia";
            gridViewTextBoxColumn25.Width = 85;
            gridViewTextBoxColumn26.FieldName = "NombreFigura";
            gridViewTextBoxColumn26.HeaderText = "Nombre";
            gridViewTextBoxColumn26.Name = "NombreFigura";
            gridViewTextBoxColumn26.Width = 185;
            gridViewTextBoxColumn27.FieldName = "NumRegIdTribFigura";
            gridViewTextBoxColumn27.HeaderText = "Reg. Id. Trib.";
            gridViewTextBoxColumn27.Name = "NumRegIdTribFigura";
            gridViewTextBoxColumn27.Width = 85;
            gridViewTextBoxColumn28.FieldName = "ResidenciaFiscalFigura";
            gridViewTextBoxColumn28.HeaderText = "Residencia Fiscal";
            gridViewTextBoxColumn28.Name = "ResidenciaFiscalFigura";
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn28.Width = 85;
            gridViewTextBoxColumn29.FieldName = "DomicilioToString";
            gridViewTextBoxColumn29.HeaderText = "Domicilio";
            gridViewTextBoxColumn29.Name = "DomicilioToString";
            gridViewTextBoxColumn29.Width = 350;
            this.gridTransporte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29});
            this.gridTransporte.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridTransporte.Name = "gridTransporte";
            this.gridTransporte.ShowGroupPanel = false;
            this.gridTransporte.Size = new System.Drawing.Size(1096, 233);
            this.gridTransporte.TabIndex = 10;
            // 
            // TUbicacion
            // 
            this.TUbicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUbicacion.Etiqueta = "";
            this.TUbicacion.Location = new System.Drawing.Point(0, 0);
            this.TUbicacion.Name = "TUbicacion";
            this.TUbicacion.ShowActualizar = false;
            this.TUbicacion.ShowAutorizar = false;
            this.TUbicacion.ShowCerrar = false;
            this.TUbicacion.ShowEditar = true;
            this.TUbicacion.ShowExportarExcel = false;
            this.TUbicacion.ShowFiltro = true;
            this.TUbicacion.ShowGuardar = false;
            this.TUbicacion.ShowHerramientas = false;
            this.TUbicacion.ShowImagen = false;
            this.TUbicacion.ShowImprimir = false;
            this.TUbicacion.ShowNuevo = true;
            this.TUbicacion.ShowRemover = true;
            this.TUbicacion.Size = new System.Drawing.Size(1096, 30);
            this.TUbicacion.TabIndex = 0;
            this.TUbicacion.Nuevo.Click += this.TUbicacion_Nuevo_Click;
            this.TUbicacion.Editar.Click += this.TUbicacion_Editar_Click;
            this.TUbicacion.Remover.Click += this.TUbicacion_Remover_Click;
            // 
            // TMercancia
            // 
            this.TMercancia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMercancia.Etiqueta = "";
            this.TMercancia.Location = new System.Drawing.Point(0, 0);
            this.TMercancia.Name = "TMercancia";
            this.TMercancia.ShowActualizar = false;
            this.TMercancia.ShowAutorizar = false;
            this.TMercancia.ShowCerrar = false;
            this.TMercancia.ShowEditar = true;
            this.TMercancia.ShowExportarExcel = false;
            this.TMercancia.ShowFiltro = true;
            this.TMercancia.ShowGuardar = false;
            this.TMercancia.ShowHerramientas = false;
            this.TMercancia.ShowImagen = false;
            this.TMercancia.ShowImprimir = false;
            this.TMercancia.ShowNuevo = true;
            this.TMercancia.ShowRemover = true;
            this.TMercancia.Size = new System.Drawing.Size(1075, 30);
            this.TMercancia.TabIndex = 0;
            this.TMercancia.Nuevo.Click += this.TMercancia_Nuevo_Click;
            this.TMercancia.Editar.Click += this.TMercancia_Editar_Click;
            this.TMercancia.Remover.Click += this.TMercancia_Remover_Click;
            // 
            // TTransporte
            // 
            this.TTransporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TTransporte.Etiqueta = "";
            this.TTransporte.Location = new System.Drawing.Point(0, 0);
            this.TTransporte.Name = "TTransporte";
            this.TTransporte.ShowActualizar = false;
            this.TTransporte.ShowAutorizar = false;
            this.TTransporte.ShowCerrar = false;
            this.TTransporte.ShowEditar = true;
            this.TTransporte.ShowExportarExcel = false;
            this.TTransporte.ShowFiltro = true;
            this.TTransporte.ShowGuardar = false;
            this.TTransporte.ShowHerramientas = false;
            this.TTransporte.ShowImagen = false;
            this.TTransporte.ShowImprimir = false;
            this.TTransporte.ShowNuevo = true;
            this.TTransporte.ShowRemover = true;
            this.TTransporte.Size = new System.Drawing.Size(1096, 30);
            this.TTransporte.TabIndex = 9;
            this.TTransporte.Nuevo.Click += this.TTransporte_Nuevo_Click;
            this.TTransporte.Editar.Click += this.TTransporte_Editar_Click;
            this.TTransporte.Remover.Click += this.TTransporte_Remover_Click;
            // 
            // AutotransporteControl
            // 
            this.AutotransporteControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AutotransporteControl.Editable = false;
            this.AutotransporteControl.Location = new System.Drawing.Point(0, 0);
            this.AutotransporteControl.Name = "AutotransporteControl";
            this.AutotransporteControl.Size = new System.Drawing.Size(1075, 215);
            this.AutotransporteControl.TabIndex = 0;
            // 
            // ComplementoCartaPorteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.Generales);
            this.Name = "ComplementoCartaPorteControl";
            this.Size = new System.Drawing.Size(1117, 358);
            this.Load += new System.EventHandler(this.ComplementoCartaPorteControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Generales)).EndInit();
            this.Generales.ResumeLayout(false);
            this.Generales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PaisOrigenDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntradaSalidaMerc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDistRec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViaEntradaSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TranspInternac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.pageUbicaciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridUbicaciones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUbicaciones)).EndInit();
            this.pageMercancia.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabMercancias)).EndInit();
            this.TabMercancias.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMercancias.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMercancias)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PesoNetoTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PesoBrutoTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoPorTasacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnidadPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumTotalMercancias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            this.pageAutoTransporte.ResumeLayout(false);
            this.pageTransporte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTransporte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTransporte)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox Generales;
        protected internal Telerik.WinControls.UI.RadDropDownList PaisOrigenDestino;
        private Telerik.WinControls.UI.RadLabel label7;
        protected internal Telerik.WinControls.UI.RadDropDownList EntradaSalidaMerc;
        private Telerik.WinControls.UI.RadLabel label6;
        protected internal Telerik.WinControls.UI.RadSpinEditor TotalDistRec;
        protected internal Telerik.WinControls.UI.RadDropDownList ViaEntradaSalida;
        protected internal Telerik.WinControls.UI.RadDropDownList TranspInternac;
        private Telerik.WinControls.UI.RadLabel label3;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadPageView TabControl;
        private Telerik.WinControls.UI.RadPageViewPage pageUbicaciones;
        private Common.Forms.ToolBarStandarControl TUbicacion;
        private Telerik.WinControls.UI.RadPageViewPage pageMercancia;
        private Telerik.WinControls.UI.RadPageView TabMercancias;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Common.Forms.ToolBarStandarControl TMercancia;
        private Telerik.WinControls.UI.RadPageViewPage pageAutoTransporte;
        private Telerik.WinControls.UI.RadPageViewPage pageTransporte;
        protected internal Telerik.WinControls.UI.RadGridView gridUbicaciones;
        private System.Windows.Forms.Panel panel1;
        public Telerik.WinControls.UI.RadGridView gridMercancias;
        protected internal Telerik.WinControls.UI.RadTextBox PesoNetoTotal;
        private Telerik.WinControls.UI.RadLabel label10;
        protected internal Telerik.WinControls.UI.RadTextBox PesoBrutoTotal;
        protected internal Telerik.WinControls.UI.RadTextBox CargoPorTasacion;
        private Telerik.WinControls.UI.RadDropDownList UnidadPeso;
        private Telerik.WinControls.UI.RadLabel label9;
        private Telerik.WinControls.UI.RadLabel label8;
        protected internal Telerik.WinControls.UI.RadTextBox NumTotalMercancias;
        private Telerik.WinControls.UI.RadLabel label5;
        private Telerik.WinControls.UI.RadLabel label4;
        public Telerik.WinControls.UI.RadGridView gridTransporte;
        private Common.Forms.ToolBarStandarControl TTransporte;
        private CartaPorteMercanciasAutotransporteControl AutotransporteControl;
    }
}
