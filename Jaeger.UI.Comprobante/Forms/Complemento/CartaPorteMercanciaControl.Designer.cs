﻿namespace Jaeger.UI.Comprobante.Forms {
    partial class CartaPorteMercanciaControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CartaPorteMercanciaControl));
            this.TMercancia = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.MercanciaControl = new Jaeger.UI.Comprobante.Forms.CartaPorteMercanciasMercanciaControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TMercancia
            // 
            this.TMercancia.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMercancia.Etiqueta = "Mercancia";
            this.TMercancia.Location = new System.Drawing.Point(0, 0);
            this.TMercancia.Name = "TMercancia";
            this.TMercancia.ShowActualizar = false;
            this.TMercancia.ShowAutorizar = false;
            this.TMercancia.ShowCerrar = true;
            this.TMercancia.ShowEditar = false;
            this.TMercancia.ShowExportarExcel = false;
            this.TMercancia.ShowFiltro = true;
            this.TMercancia.ShowGuardar = true;
            this.TMercancia.ShowHerramientas = false;
            this.TMercancia.ShowImagen = false;
            this.TMercancia.ShowImprimir = false;
            this.TMercancia.ShowNuevo = true;
            this.TMercancia.ShowRemover = false;
            this.TMercancia.Size = new System.Drawing.Size(598, 31);
            this.TMercancia.TabIndex = 33;
            this.TMercancia.Nuevo.Click +=this.TMercancia_Nuevo_Click;
            this.TMercancia.Guardar.Click += this.TMercancia_Guardar_Click;
            this.TMercancia.Cerrar.Click += this.TMercancia_Cerrar_Click;
            // 
            // MercanciaControl
            // 
            this.MercanciaControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MercanciaControl.Location = new System.Drawing.Point(0, 31);
            this.MercanciaControl.Name = "MercanciaControl";
            this.MercanciaControl.Size = new System.Drawing.Size(598, 405);
            this.MercanciaControl.TabIndex = 0;
            // 
            // CartaPorteMercanciaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 436);
            this.Controls.Add(this.MercanciaControl);
            this.Controls.Add(this.TMercancia);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CartaPorteMercanciaControl";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mercancia";
            this.Load += new System.EventHandler(this.CartaPorteMercanciaControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Common.Forms.ToolBarStandarControl TMercancia;
        private CartaPorteMercanciasMercanciaControl MercanciaControl;
    }
}
