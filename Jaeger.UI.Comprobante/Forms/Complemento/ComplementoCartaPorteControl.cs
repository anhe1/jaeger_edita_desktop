﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Comprobante.Services;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Comprobante.Forms {
    public partial class ComplementoCartaPorteControl : UserControl, IComplementoControl {
        protected IClaveTransporteCatalogo transporteCatalogo;
        protected IClaveClaveUnidadPesoCatalogo claveClaveUnidadPesoCatalogo;
        protected ICveFiguraTransporteCatalogo figuraTransporteCatalogo;
        public CartaPorteDetailModel Complemento;
        private bool _Editable = false;

        public ComplementoCartaPorteControl() {
            InitializeComponent();
        }

        public ComplementoCartaPorteControl(CartaPorteDetailModel model) {
            InitializeComponent();
            if (model == null) {
                this.Complemento = new CartaPorteDetailModel();
            } else {
                this.Complemento = model;
            }
        }

        private void ComplementoCartaPorteControl_Load(object sender, EventArgs e) {
            this.Caption = "Carta Porte";

            this.gridUbicaciones.Standard();
            this.gridMercancias.Standard();
            this.gridTransporte.Standard();

            this.TranspInternac.DataSource = ComprobanteCommonService.GetTipoTranporte();
            this.TranspInternac.DisplayMember = "Descripcion";
            this.TranspInternac.ValueMember = "Id";

            this.EntradaSalidaMerc.DisplayMember = "Descripcion";
            this.EntradaSalidaMerc.ValueMember = "Id";
            this.EntradaSalidaMerc.DataSource = ComprobanteCommonService.GetCartaPorteEntradaSalidaMercs();

            this.transporteCatalogo = new TransporteCatalogo();
            this.transporteCatalogo.Load();

            this.ViaEntradaSalida.DisplayMember = "Descriptor";
            this.ViaEntradaSalida.ValueMember = "Clave";
            this.ViaEntradaSalida.DataSource = this.transporteCatalogo.Items;

            this.AutotransporteControl.TRemolque.Nuevo.Click += this.TRemolque_Nuevo_Click;
        }

        public virtual bool Editable {
            get { return this._Editable; }
            set {
                this._Editable = value;
                this.SetEditable();
                this.AutotransporteControl.Editable = this._Editable;
            }
        }

        public virtual string Caption { get; set; }

        public virtual void Start() {
            this.Caption = "Carta Porte";
            this.figuraTransporteCatalogo = new CveFiguraTransporteCatalogo();
            this.figuraTransporteCatalogo.Load();
            var d = this.gridTransporte.Columns["TipoFigura"] as GridViewComboBoxColumn;
            d.ValueMember = "Clave";
            d.DisplayMember = "Descriptor";
            d.DataSource = this.figuraTransporteCatalogo.Items;
        }

        private void TMercancia_Nuevo_Click(object sender, EventArgs e) {
            var _Mercancia = new CartaPorteMercanciaControl(null);
            _Mercancia.Add += Mercancia_Add;
            _Mercancia.ShowDialog(this);
        }

        private void Mercancia_Add(object sender, CartaPorteMercanciasMercancia e) {
            if (e != null) {
                this.Complemento.Mercancias.Mercancia.Add(e);
            }
        }

        private void TMercancia_Editar_Click(object sender, EventArgs e) {
            if (this.gridMercancias.CurrentRow != null) {
                var _seleccionado = this.gridMercancias.CurrentRow.DataBoundItem as CartaPorteMercanciasMercancia;
                if (_seleccionado != null) {
                    var _editar = new CartaPorteMercanciaControl(_seleccionado);
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TMercancia_Remover_Click(object sender, EventArgs e) {
            if (this.gridMercancias.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    var _seleccionado = this.gridMercancias.CurrentRow.DataBoundItem as CartaPorteMercanciasMercancia;
                    if (_seleccionado != null) {
                        this.Complemento.Mercancias.Mercancia.Remove(_seleccionado);
                    }
                }
            }
        }

        private void TRemolque_Nuevo_Click(object sender, EventArgs e) {
            var _tipo = new SubTipoRemolqueCatalogoBuscarForm();
            _tipo.Seleted += this.TRemolque_Selected_New;
            _tipo.ShowDialog(this);
        }

        private void TRemolque_Selected_New(object sender, Catalogos.Entities.CveSubTipoRem e) {
            if (e != null) {
                if (this.Complemento.Mercancias.Autotransporte.Remolques == null) {
                    this.Complemento.Mercancias.Autotransporte.Remolques = new System.ComponentModel.BindingList<CartaPorteMercanciasAutotransporteRemolque>();
                }
                this.Complemento.Mercancias.Autotransporte.Remolques.Add(new CartaPorteMercanciasAutotransporteRemolque { SubTipoRem = e.Clave, Placa = string.Empty });
            }
            this.AutotransporteControl.gridRemolques.AllowEditRow = this.Editable;
        }

        private void TUbicacion_Nuevo_Click(object sender, EventArgs e) {
            var _ubicacion = new CartaPorteUbicacionControl(null) {
                ViaEntradaSalida = this.Complemento.ViaEntradaSalida
            };
            _ubicacion.CartaPorteUbicacionAdd += Ubicacion_UbicacionAdd;
            _ubicacion.ShowDialog(this);
        }

        private void TUbicacion_Editar_Click(object sender, EventArgs e) {
            if (this.gridUbicaciones.CurrentRow != null) {
                var _seleccionado = this.gridUbicaciones.CurrentRow.DataBoundItem as CartaPorteUbicacion;
                if (_seleccionado != null) {
                    using (var _editar = new CartaPorteUbicacionControl(_seleccionado)) {
                        _editar.ViaEntradaSalida = this.Complemento.ViaEntradaSalida;
                        _editar.CartaPorteUbicacionEdit += Ubicacion_UbicacionEdit;
                        _editar.ShowDialog(this);
                    }
                }
            }
        }

        private void TUbicacion_Remover_Click(object sender, EventArgs e) {
            if (this.gridUbicaciones.CurrentRow != null) {
                if (MessageBox.Show(this, "¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    var _seleccionado = this.gridUbicaciones.CurrentRow.DataBoundItem as CartaPorteUbicacion;
                    if (_seleccionado != null) {
                        this.Complemento.Ubicaciones.Remove(_seleccionado);
                    }
                }
            }
        }

        private void Ubicacion_UbicacionEdit(object sender, CartaPorteUbicacion e) {
            if (e != null) {
                this.gridUbicaciones.BeginUpdate();
                var _seleccionado = this.gridUbicaciones.CurrentRow.DataBoundItem as CartaPorteUbicacion;
                _seleccionado = e;
                _seleccionado.NombreRemitenteDestinatario = e.NombreRemitenteDestinatario;
                this.gridUbicaciones.EndUpdate();
            }
        }

        private void Ubicacion_UbicacionAdd(object sender, CartaPorteUbicacion e) {
            if (e != null) {
                this.Complemento.Ubicaciones.Add(e);
            }
        }

        private void TTransporte_Nuevo_Click(object sender, EventArgs e) {
            var _figuraTransporte = new CartaPorteTiposFiguraControl();
            _figuraTransporte.Add += FiguraTransporte_Add;
            _figuraTransporte.ShowDialog(this);
        }

        private void FiguraTransporte_Add(object sender, CartaPorteTiposFigura e) {
            if (e != null) {
                this.Complemento.FiguraTransporte.Add(e);
            }
        }

        private void TTransporte_Editar_Click(object sender, EventArgs e) {
            if (this.gridTransporte.CurrentRow != null) {
                var _seleccionado = this.gridTransporte.CurrentRow.DataBoundItem as CartaPorteTiposFigura;
                if (_seleccionado != null) {
                    var _figuraTransporte = new CartaPorteTiposFiguraControl(_seleccionado);
                    _figuraTransporte.Add += FiguraTransporte_Add;
                    _figuraTransporte.ShowDialog(this);
                }
            }
        }

        private void TTransporte_Remover_Click(object sender, EventArgs e) {
            if (this.gridTransporte.CurrentRow != null) {
                this.gridTransporte.Rows.Remove(this.gridTransporte.CurrentRow);
            }
        }

        public virtual void CreateBinding() {
            this.TranspInternac.DataBindings.Clear();
            this.TranspInternac.DataBindings.Add("SelectedValue", this.Complemento, "TranspInternac", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ViaEntradaSalida.DataBindings.Clear();
            this.ViaEntradaSalida.DataBindings.Add("SelectedValue", this.Complemento, "ViaEntradaSalida", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TotalDistRec.DataBindings.Clear();
            this.TotalDistRec.DataBindings.Add("Value", this.Complemento, "TotalDistRec", true, DataSourceUpdateMode.OnPropertyChanged);

            this.EntradaSalidaMerc.DataBindings.Clear();
            this.EntradaSalidaMerc.DataBindings.Add("SelectedValue", this.Complemento, "EntradaSalidaMerc", true, DataSourceUpdateMode.OnPropertyChanged);
            this.EntradaSalidaMerc.DataBindings.Add("Enabled", this.Complemento, "TranspInternac", true, DataSourceUpdateMode.OnPropertyChanged);
            this.PaisOrigenDestino.DataBindings.Clear();
            this.PaisOrigenDestino.DataBindings.Add("Text", this.Complemento, "PaisOrigenDestino", true, DataSourceUpdateMode.OnPropertyChanged);
            this.PaisOrigenDestino.DataBindings.Add("Enabled", this.Complemento, "TranspInternac", true, DataSourceUpdateMode.OnPropertyChanged);

            this.UnidadPeso.DataBindings.Clear();
            this.UnidadPeso.DataBindings.Add("SelectedValue", this.Complemento.Mercancias, "UnidadPeso", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PesoBrutoTotal.DataBindings.Clear();
            this.PesoBrutoTotal.DataBindings.Add("Text", this.Complemento.Mercancias, "PesoBrutoTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PesoNetoTotal.DataBindings.Clear();
            this.PesoNetoTotal.DataBindings.Add("Text", this.Complemento.Mercancias, "PesoNetoTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CargoPorTasacion.DataBindings.Clear();
            this.CargoPorTasacion.DataBindings.Add("Text", this.Complemento.Mercancias, "CargoPorTasacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumTotalMercancias.DataBindings.Clear();
            this.NumTotalMercancias.DataBindings.Add("Text", this.Complemento.Mercancias, "NumTotalMercancias", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridUbicaciones.DataSource = this.Complemento;
            this.gridMercancias.DataSource = this.Complemento.Mercancias;
            this.gridTransporte.DataSource = this.Complemento;
            this.gridUbicaciones.DataMember = "Ubicaciones";
            this.gridMercancias.DataMember = "Mercancia";
            this.gridTransporte.DataMember = "FiguraTransporte";


            if (this.Complemento.Mercancias.Autotransporte != null) {
                this.AutotransporteControl.Incluir.Checked = true;
                this.CreateBindingAutoTransporte();
            }
        }

        public virtual void CreateBindingAutoTransporte() {
            this.AutotransporteControl.PermSCT.DataBindings.Clear();
            this.AutotransporteControl.PermSCT.DataBindings.Add("SelectedValue", this.Complemento.Mercancias.Autotransporte, "PermSCT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.NumPermisoSCT.DataBindings.Clear();
            this.AutotransporteControl.NumPermisoSCT.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte, "NumPermisoSCT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.ConfigVehicular.DataBindings.Clear();
            this.AutotransporteControl.ConfigVehicular.DataBindings.Add("SelectedValue", this.Complemento.Mercancias.Autotransporte.IdentificacionVehicular, "ConfigVehicular", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PlacaVM.DataBindings.Clear();
            this.AutotransporteControl.PlacaVM.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.IdentificacionVehicular, "PlacaVM", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.AnioModeloVM.DataBindings.Clear();
            this.AutotransporteControl.AnioModeloVM.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.IdentificacionVehicular, "AnioModeloVM", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.AseguraRespCivil.DataBindings.Clear();
            this.AutotransporteControl.AseguraRespCivil.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "AseguraRespCivil", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PolizaRespCivil.DataBindings.Clear();
            this.AutotransporteControl.PolizaRespCivil.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "PolizaRespCivil", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.AseguraMedAmbiente.DataBindings.Clear();
            this.AutotransporteControl.AseguraMedAmbiente.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "AseguraMedAmbiente", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PolizaMedAmbiente.DataBindings.Clear();
            this.AutotransporteControl.PolizaMedAmbiente.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "PolizaMedAmbiente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.AseguraCarga.DataBindings.Clear();
            this.AutotransporteControl.AseguraCarga.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "AseguraCarga", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AutotransporteControl.PolizaCarga.DataBindings.Clear();
            this.AutotransporteControl.PolizaCarga.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "PolizaCarga", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.PrimaSeguro.DataBindings.Clear();
            this.AutotransporteControl.PrimaSeguro.DataBindings.Add("Text", this.Complemento.Mercancias.Autotransporte.Seguros, "PrimaSeguro", true, DataSourceUpdateMode.OnPropertyChanged);

            this.AutotransporteControl.gridRemolques.DataSource = this.Complemento.Mercancias.Autotransporte.Remolques;
        }

        private void Incluir_CheckedChanged(object sender, EventArgs e) {
            if (this.Complemento.Mercancias.Autotransporte == null) {
                this.Complemento.Mercancias.Autotransporte = new CartaPorteMercanciasAutotransporte();
                this.CreateBindingAutoTransporte();
            }
        }

        private void SetEditable() {
            this.TranspInternac.SetEditable(this._Editable);
            this.ViaEntradaSalida.SetEditable(this._Editable);
            this.TotalDistRec.SetEditable(this._Editable);
            this.PaisOrigenDestino.SetEditable(this._Editable);

            this.TUbicacion.Nuevo.Enabled = this._Editable;
            this.TUbicacion.Editar.Enabled = this._Editable;
            this.TUbicacion.Remover.Enabled = this._Editable;
            this.gridUbicaciones.AllowEditRow = this._Editable;

            this.TMercancia.Nuevo.Enabled = this._Editable;
            this.TMercancia.Editar.Enabled = this._Editable;
            this.TMercancia.Remover.Enabled = this._Editable;
            this.gridMercancias.AllowEditRow = this._Editable;

            this.TTransporte.Nuevo.Enabled = this._Editable;
            this.TTransporte.Editar.Enabled = this._Editable;
            this.TTransporte.Remover.Enabled = this._Editable;
            this.gridTransporte.AllowEditRow = this._Editable;

            this.UnidadPeso.SetEditable(this._Editable);
            this.PesoNetoTotal.ReadOnly = this._Editable;
            this.CargoPorTasacion.ReadOnly = this._Editable;
            this.PesoBrutoTotal.ReadOnly = this._Editable;
            this.NumTotalMercancias.ReadOnly = this._Editable;
        }

        public virtual bool Validar() {
            return true;
        }
    }
}
