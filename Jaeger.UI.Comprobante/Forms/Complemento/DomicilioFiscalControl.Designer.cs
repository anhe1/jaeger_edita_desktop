﻿
namespace Jaeger.UI.Comprobante.Forms {
    partial class DomicilioFiscalControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.label13 = new Telerik.WinControls.UI.RadLabel();
            this.CodigoPostal = new Telerik.WinControls.UI.RadTextBox();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.Calle = new Telerik.WinControls.UI.RadTextBox();
            this.Pais = new Telerik.WinControls.UI.RadDropDownList();
            this.NumeroExterior = new Telerik.WinControls.UI.RadTextBox();
            this.label16 = new Telerik.WinControls.UI.RadLabel();
            this.label17 = new Telerik.WinControls.UI.RadLabel();
            this.Estado = new Telerik.WinControls.UI.RadDropDownList();
            this.NumeroInterior = new Telerik.WinControls.UI.RadTextBox();
            this.label18 = new Telerik.WinControls.UI.RadLabel();
            this.label19 = new Telerik.WinControls.UI.RadLabel();
            this.Municipio = new Telerik.WinControls.UI.RadDropDownList();
            this.label20 = new Telerik.WinControls.UI.RadLabel();
            this.Colonia = new Telerik.WinControls.UI.RadDropDownList();
            this.label21 = new Telerik.WinControls.UI.RadLabel();
            this.Referencia = new Telerik.WinControls.UI.RadTextBox();
            this.Localidad = new Telerik.WinControls.UI.RadDropDownList();
            this.label22 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroInterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Municipio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Colonia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Localidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.CodigoPostal);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.Calle);
            this.groupBox1.Controls.Add(this.Pais);
            this.groupBox1.Controls.Add(this.NumeroExterior);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Estado);
            this.groupBox1.Controls.Add(this.NumeroInterior);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.Municipio);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.Colonia);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.Referencia);
            this.groupBox1.Controls.Add(this.Localidad);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "Domicilio";
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(674, 166);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Domicilio";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(10, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 18);
            this.label13.TabIndex = 0;
            this.label13.Text = "Calle:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(10, 134);
            this.CodigoPostal.MaxLength = 5;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(100, 20);
            this.CodigoPostal.TabIndex = 7;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(348, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 18);
            this.label14.TabIndex = 1;
            this.label14.Text = "Núm. Exterior:";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(10, 113);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 18);
            this.label15.TabIndex = 18;
            this.label15.Text = "Codigo Postal:*";
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(10, 40);
            this.Calle.MaxLength = 100;
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(333, 20);
            this.Calle.TabIndex = 0;
            // 
            // Pais
            // 
            this.Pais.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Pais.Location = new System.Drawing.Point(560, 40);
            this.Pais.MaxLength = 30;
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(105, 20);
            this.Pais.TabIndex = 3;
            // 
            // NumeroExterior
            // 
            this.NumeroExterior.Location = new System.Drawing.Point(348, 40);
            this.NumeroExterior.MaxLength = 55;
            this.NumeroExterior.Name = "NumeroExterior";
            this.NumeroExterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroExterior.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(560, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 18);
            this.label16.TabIndex = 16;
            this.label16.Text = "País:*";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(454, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 18);
            this.label17.TabIndex = 4;
            this.label17.Text = "Núm. Interior:";
            // 
            // Estado
            // 
            this.Estado.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Estado.Location = new System.Drawing.Point(10, 87);
            this.Estado.MaxLength = 30;
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(206, 20);
            this.Estado.TabIndex = 4;
            // 
            // NumeroInterior
            // 
            this.NumeroInterior.Location = new System.Drawing.Point(454, 40);
            this.NumeroInterior.MaxLength = 55;
            this.NumeroInterior.Name = "NumeroInterior";
            this.NumeroInterior.Size = new System.Drawing.Size(100, 20);
            this.NumeroInterior.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(10, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 18);
            this.label18.TabIndex = 14;
            this.label18.Text = "Estado:*";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(117, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 18);
            this.label19.TabIndex = 6;
            this.label19.Text = "Colonia:";
            // 
            // Municipio
            // 
            this.Municipio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Municipio.Location = new System.Drawing.Point(222, 87);
            this.Municipio.MaxLength = 120;
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(205, 20);
            this.Municipio.TabIndex = 5;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(222, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 18);
            this.label20.TabIndex = 12;
            this.label20.Text = "Municipio:";
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(117, 134);
            this.Colonia.MaxLength = 120;
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(208, 20);
            this.Colonia.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(433, 66);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 18);
            this.label21.TabIndex = 8;
            this.label21.Text = "Localidad:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(330, 134);
            this.Referencia.MaxLength = 250;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(335, 20);
            this.Referencia.TabIndex = 9;
            // 
            // Localidad
            // 
            this.Localidad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Localidad.Location = new System.Drawing.Point(433, 87);
            this.Localidad.MaxLength = 120;
            this.Localidad.Name = "Localidad";
            this.Localidad.Size = new System.Drawing.Size(232, 20);
            this.Localidad.TabIndex = 6;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(330, 113);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 18);
            this.label22.TabIndex = 10;
            this.label22.Text = "Referencia:";
            // 
            // DomicilioFiscalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "DomicilioFiscalControl";
            this.Size = new System.Drawing.Size(674, 166);
            this.Load += new System.EventHandler(this.DomicilioFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Calle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroInterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Municipio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Colonia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Localidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel label13;
        protected internal Telerik.WinControls.UI.RadTextBox CodigoPostal;
        private Telerik.WinControls.UI.RadLabel label14;
        private Telerik.WinControls.UI.RadLabel label15;
        protected internal Telerik.WinControls.UI.RadTextBox Calle;
        protected internal Telerik.WinControls.UI.RadDropDownList Pais;
        protected internal Telerik.WinControls.UI.RadTextBox NumeroExterior;
        private Telerik.WinControls.UI.RadLabel label16;
        private Telerik.WinControls.UI.RadLabel label17;
        protected internal Telerik.WinControls.UI.RadDropDownList Estado;
        protected internal Telerik.WinControls.UI.RadTextBox NumeroInterior;
        private Telerik.WinControls.UI.RadLabel label18;
        private Telerik.WinControls.UI.RadLabel label19;
        protected internal Telerik.WinControls.UI.RadDropDownList Municipio;
        private Telerik.WinControls.UI.RadLabel label20;
        protected internal Telerik.WinControls.UI.RadDropDownList Colonia;
        private Telerik.WinControls.UI.RadLabel label21;
        protected internal Telerik.WinControls.UI.RadTextBox Referencia;
        protected internal Telerik.WinControls.UI.RadDropDownList Localidad;
        private Telerik.WinControls.UI.RadLabel label22;
    }
}
