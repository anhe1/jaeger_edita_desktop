﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Contracts;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Comprobante.Builder;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Entities;
using System.Windows.Forms;
using Telerik.WinControls;

namespace Jaeger.UI.Comprobante.Forms {
    /// <summary>
    /// formulario donde se presenta el comprobante de ingreso con el complemento de pago
    /// </summary>
    public partial class ComprobanteVsComplementoPagoForm : RadForm {
        #region declaraciones
        protected internal IComprobantesFiscalesService _Service;
        protected internal BindingList<ComplementoVsPagoMergeDoctoModel> _DataSource;
        protected internal UIMenuElement menuElement;
        #endregion

        public ComprobanteVsComplementoPagoForm() {
            InitializeComponent();
        }

        private void ComprobanteVsComplementoPago_Load(object sender, EventArgs e) {
            using (IComprobanteFiscalGridBuilder gridView = new ComprobanteFiscalGridBuilder()) {
                this.TComprobantes.GridData.Columns.AddRange(gridView.Templetes().ComprobanteVsComplementoPago().Build());
            }
            this.TComprobantes.GridData.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.GridData_CellFormatting);
            this.TComprobantes.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            this.TComprobantes.Actualizar.Click += this.TComprobantes_Actualizar_Click;
            this.TComprobantes.Cerrar.Click += TComprobante_Cerrar_Click;
        }

        private void TComprobantes_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando datos ...";
                espera.ShowDialog(this);
            }
            this.TComprobantes.GridData.DataSource = this._DataSource;
        }

        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TComprobantes_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void GridData_CellFormatting(object sender, CellFormattingEventArgs e) {
            if (e.Column.Name.ToLower() == "UrlFileXml".ToLower() || e.Column.Name.ToLower() == "UrlFilePdf".ToLower()) {
                if (DbConvert.ConvertString(e.CellElement.Value) != "") {
                    e.CellElement.Image = Iconos.Images[e.Column.Name];
                    e.CellElement.DrawText = false;
                } else {
                    e.CellElement.Image = null;
                    e.CellElement.DrawText = true;
                    e.CellElement.Children.Clear();
                }
            } else if (e.Column.Name.ToLower() != "Status".ToLower()) {
                // aqui tenemos que agregar las columnas que no debemos limpiar para evitar borrar controles adicionales
                e.CellElement.Image = null;
                e.CellElement.DrawText = true;
                try {
                    e.CellElement.Children.Clear();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public virtual void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (!(this.TComprobantes.GridData.CurrentRow == null)) {
                if (!(e.Column == null)) {
                    if (e.Column.Name.ToLower() == "UrlFileXml".ToLower() || e.Column.Name.ToLower() == "UrlFilePdf".ToLower()) {
                        var single = this.TComprobantes.GridData.CurrentRow.DataBoundItem as ComplementoVsPagoMergeDoctoModel;
                        string liga = "";
                        if (e.Column.Name.ToLower() == "UrlFileXml".ToLower())
                            liga = single.UrlFileXML;
                        else
                            if (e.Column.Name.ToLower() == "UrlFilePdf".ToLower())
                            liga = single.UrlFilePDF;
                        // validar la liga de descarga
                        if (ValidacionService.URL(liga)) {
                            var savefiledialog = new SaveFileDialog {
                                FileName = System.IO.Path.GetFileName(liga)
                            };
                            if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                                return;
                            if (Jaeger.Util.FileService.DownloadFile(liga, savefiledialog.FileName)) {
                                DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (dr == DialogResult.Yes) {
                                    try {
                                        System.Diagnostics.Process.Start(savefiledialog.FileName);
                                    } catch (Exception ex) {
                                        string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                        RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void Consultar() {
            var d0 = ComprobantesFiscalesService.Query().Recibido().WithYear(this.TComprobantes.GetEjercicio()).Build();
            this._DataSource = new BindingList<ComplementoVsPagoMergeDoctoModel>(this._Service.GetList<ComplementoVsPagoMergeDoctoModel>(d0).ToList());
        }
    }
}
