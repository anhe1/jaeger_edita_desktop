﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Kaiju.Builder {
    public interface IUserGridBuilder : IGridViewBuilder, IDisposable {
        IUserTempleteGridBuilder Templetes();
    }

    public interface IUserTempleteGridBuilder : IGridViewBuilder {
        IUserTempleteGridBuilder Master();
    }

    public interface IUserColumnsGridBuilder : IGridViewBuilder {
        IUserColumnsGridBuilder Id();
        IUserColumnsGridBuilder Activo();
        IUserColumnsGridBuilder IdPerfil();
        IUserColumnsGridBuilder Clave();
        IUserColumnsGridBuilder Nombre();
        IUserColumnsGridBuilder Correo();
        IUserColumnsGridBuilder FechaNuevo();
        IUserColumnsGridBuilder Creo();
    }
}
