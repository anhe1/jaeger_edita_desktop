﻿using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Kaiju.Builder {
    public class UserGridBuilder : GridViewBuilder, IUserGridBuilder, IGridViewBuilder, IUserTempleteGridBuilder, IUserColumnsGridBuilder {
        public UserGridBuilder() : base() { }

        public IUserTempleteGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public IUserTempleteGridBuilder Master() {
            this.Id().Activo().Clave().Nombre().Correo().FechaNuevo().Creo();
            return this;
        }

        public IUserColumnsGridBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IUserColumnsGridBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo"
            });
            return this;
        }

        public IUserColumnsGridBuilder IdPerfil() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdPerfil",
                HeaderText = "IdPerfil",
                Name = "IdPerfil",
                Width = 200
            });
            return this;
        }

        public IUserColumnsGridBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 85
            });
            return this;
        }

        public IUserColumnsGridBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre",
                Name = "_user_nom",
                Width = 200
            });
            return this;
        }

        public IUserColumnsGridBuilder Correo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Correo",
                HeaderText = "Correo electrónico",
                Name = "Correo",
                Width = 220
            });
            return this;
        }

        public IUserColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaNuevo",
                HeaderText = "Fec. Sist.",
                Name = "Fechanuevo",
                Width = 75
            });
            return this;
        }

        public IUserColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                Width = 85
            });
            return this;
        }
    }
}
