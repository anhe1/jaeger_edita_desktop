﻿namespace Jaeger.UI.Kaiju.Forms {
    partial class UsersCatalogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TUser = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TUser
            // 
            this.TUser.Caption = "";
            this.TUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TUser.Location = new System.Drawing.Point(0, 0);
            this.TUser.Name = "TUser";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TUser.Permisos = uiAction1;
            this.TUser.ReadOnly = false;
            this.TUser.ShowActualizar = true;
            this.TUser.ShowAutorizar = false;
            this.TUser.ShowAutosuma = false;
            this.TUser.ShowCerrar = true;
            this.TUser.ShowEditar = true;
            this.TUser.ShowExportarExcel = false;
            this.TUser.ShowFiltro = true;
            this.TUser.ShowHerramientas = false;
            this.TUser.ShowImagen = false;
            this.TUser.ShowImprimir = false;
            this.TUser.ShowItem = false;
            this.TUser.ShowNuevo = true;
            this.TUser.ShowRemover = true;
            this.TUser.ShowSeleccionMultiple = true;
            this.TUser.Size = new System.Drawing.Size(703, 446);
            this.TUser.TabIndex = 0;
            // 
            // UsersCatalogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 446);
            this.Controls.Add(this.TUser);
            this.Name = "UsersCatalogForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Empresa: Usuarios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UsersCatalog_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Common.Forms.GridStandarControl TUser;
    }
}
