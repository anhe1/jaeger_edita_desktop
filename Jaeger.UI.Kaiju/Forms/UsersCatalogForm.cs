﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Kaiju.Builder;
using Jaeger.Aplication.Kaiju.Services;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.UI.Kaiju.Forms {
    /// <summary>
    /// Formulario de catálogo de usuarios.
    /// </summary>
    public partial class UsersCatalogForm : RadForm {
        protected Aplication.Kaiju.Contracts.IProfileService Service;
        protected BindingList<UserModel> DataSource;

        /// <summary>
        /// constructor
        /// </summary>
        public UsersCatalogForm() {
            InitializeComponent();
        }

        private void UsersCatalog_Load(object sender, EventArgs e) {
            using (IUserGridBuilder view = new UserGridBuilder()) {
                this.TUser.GridData.Columns.AddRange(view.Templetes().Master().Build());
            }

            this.TUser.Nuevo.Click += this.TUsers_Nuevo_Click;
            this.TUser.Editar.Click += this.TUsers_Editar_Click;
            this.TUser.Remover.Click += this.TUsers_Remover_Click;
            this.TUser.Actualizar.Click += this.TUsers_Actualizar_Click;
            this.TUser.Cerrar.Click += this.TUsers_Cerrar_Click;
        }

        #region barra de herramientas
        private void TUsers_Actualizar_Click(object sender, EventArgs e) {
            this.TUser.Actualizar.Enabled = false;
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando usuarios ...";
                espera.ShowDialog(this);
            }
            this.TUser.GridData.DataSource = this.DataSource;
            this.TUser.Actualizar.Enabled = true;
        }

        private void TUsers_Nuevo_Click(object sender, EventArgs e) {
            using (UserForm userForm = new UserForm(null, this.Service) {
                StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            }) {
                userForm.ShowDialog(this);
            }
        }

        private void TUsers_Editar_Click(object sender, EventArgs e) {
            if (!(this.TUser.GridData.CurrentRow == null)) {
                var seleccionado = this.TUser.GridData.CurrentRow.DataBoundItem as UserModel;
                if (seleccionado != null) {
                    using (var form = new UserForm(seleccionado, this.Service)) {
                        form.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        form.ShowDialog(this);
                    }
                }
            }
        }

        private void TUsers_Remover_Click(object sender, EventArgs e) {
            if (this.TUser.GridData.CurrentRow != null) {
                var usuario = this.TUser.GridData.CurrentRow.DataBoundItem as UserModel;
                if (usuario != null) {
                        if (RadMessageBox.Show("", "Atención!", System.Windows.Forms.MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, "Este usuario ya no podra acceder incluso al sistema web.") == System.Windows.Forms.DialogResult.Yes) { 
                            if ((this.Service as Aplication.Kaiju.Contracts.IUserService).Desactiva(usuario) == false)
                                RadMessageBox.Show("");
                    }
                }
            }
        }

        private void TUsers_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Consultar() {
            this.DataSource = new BindingList<UserModel>(this.Service.GetList<UserModel>(UserService.Query().OnlyActive().Build()).ToList());
        }
    }
}
