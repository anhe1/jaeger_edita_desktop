﻿namespace Jaeger.UI.Kaiju.Forms {
    partial class UserForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserForm));
            this.Clave = new Telerik.WinControls.UI.RadTextBox();
            this.Nombre = new Telerik.WinControls.UI.RadTextBox();
            this.Correo = new Telerik.WinControls.UI.RadTextBox();
            this.ButtonClose = new Telerik.WinControls.UI.RadButton();
            this.ButtonSave = new Telerik.WinControls.UI.RadButton();
            this.Psw = new Telerik.WinControls.UI.RadTextBox();
            this.PswConfirma = new Telerik.WinControls.UI.RadTextBox();
            this.ClaveLabel = new Telerik.WinControls.UI.RadLabel();
            this.NameLabel = new Telerik.WinControls.UI.RadLabel();
            this.MailLabel = new Telerik.WinControls.UI.RadLabel();
            this.GroupPassword = new Telerik.WinControls.UI.RadGroupBox();
            this.Message1Label = new Telerik.WinControls.UI.RadLabel();
            this.Password2Label = new Telerik.WinControls.UI.RadLabel();
            this.Password1Label = new Telerik.WinControls.UI.RadLabel();
            this.CheckPassword = new Telerik.WinControls.UI.RadCheckBox();
            this.Perfiles = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PerfilLabel = new Telerik.WinControls.UI.RadLabel();
            this.SearchPicture = new System.Windows.Forms.PictureBox();
            this.UserPicture = new System.Windows.Forms.PictureBox();
            this.HeaderImage = new System.Windows.Forms.PictureBox();
            this.HeaderLabel = new Telerik.WinControls.UI.RadLabel();
            this.InformationBox = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Correo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Psw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PswConfirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MailLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupPassword)).BeginInit();
            this.GroupPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Message1Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password2Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password1Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Perfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Perfiles.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Perfiles.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PerfilLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformationBox)).BeginInit();
            this.InformationBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Clave
            // 
            this.Clave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Clave.Location = new System.Drawing.Point(73, 32);
            this.Clave.MaxLength = 14;
            this.Clave.Name = "Clave";
            this.Clave.NullText = "Clave";
            this.Clave.Size = new System.Drawing.Size(100, 20);
            this.Clave.TabIndex = 0;
            this.Clave.Validating += new System.ComponentModel.CancelEventHandler(this.TxbClave_Validating);
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(73, 58);
            this.Nombre.MaxLength = 128;
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Nombre completo del usuario";
            this.Nombre.Size = new System.Drawing.Size(334, 20);
            this.Nombre.TabIndex = 1;
            // 
            // Correo
            // 
            this.Correo.Location = new System.Drawing.Point(73, 84);
            this.Correo.MaxLength = 128;
            this.Correo.Name = "Correo";
            this.Correo.NullText = "Correo electrónico";
            this.Correo.Size = new System.Drawing.Size(334, 20);
            this.Correo.TabIndex = 2;
            this.Correo.Validated += new System.EventHandler(this.TxbCorreo_Validated);
            // 
            // ButtonClose
            // 
            this.ButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonClose.Location = new System.Drawing.Point(432, 274);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(110, 24);
            this.ButtonClose.TabIndex = 4;
            this.ButtonClose.Text = "&Cerrar";
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // ButtonSave
            // 
            this.ButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonSave.Location = new System.Drawing.Point(431, 244);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(110, 24);
            this.ButtonSave.TabIndex = 3;
            this.ButtonSave.Text = "&Guardar";
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // Psw
            // 
            this.Psw.Location = new System.Drawing.Point(110, 22);
            this.Psw.MaxLength = 128;
            this.Psw.Name = "Psw";
            this.Psw.NullText = "Contraseña";
            this.Psw.PasswordChar = '*';
            this.Psw.Size = new System.Drawing.Size(212, 20);
            this.Psw.TabIndex = 5;
            // 
            // PswConfirma
            // 
            this.PswConfirma.Location = new System.Drawing.Point(110, 48);
            this.PswConfirma.MaxLength = 128;
            this.PswConfirma.Name = "PswConfirma";
            this.PswConfirma.NullText = "Confirmación de Contraseña";
            this.PswConfirma.PasswordChar = '*';
            this.PswConfirma.Size = new System.Drawing.Size(212, 20);
            this.PswConfirma.TabIndex = 6;
            this.PswConfirma.Validating += new System.ComponentModel.CancelEventHandler(this.TxbPswConfirma_Validating);
            // 
            // ClaveLabel
            // 
            this.ClaveLabel.Location = new System.Drawing.Point(12, 32);
            this.ClaveLabel.Name = "ClaveLabel";
            this.ClaveLabel.Size = new System.Drawing.Size(35, 18);
            this.ClaveLabel.TabIndex = 7;
            this.ClaveLabel.Text = "Clave:";
            // 
            // NameLabel
            // 
            this.NameLabel.Location = new System.Drawing.Point(12, 59);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(50, 18);
            this.NameLabel.TabIndex = 8;
            this.NameLabel.Text = "Nombre:";
            // 
            // MailLabel
            // 
            this.MailLabel.Location = new System.Drawing.Point(12, 85);
            this.MailLabel.Name = "MailLabel";
            this.MailLabel.Size = new System.Drawing.Size(43, 18);
            this.MailLabel.TabIndex = 9;
            this.MailLabel.Text = "Correo:";
            // 
            // GroupPassword
            // 
            this.GroupPassword.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupPassword.Controls.Add(this.Message1Label);
            this.GroupPassword.Controls.Add(this.Password2Label);
            this.GroupPassword.Controls.Add(this.Password1Label);
            this.GroupPassword.Controls.Add(this.Psw);
            this.GroupPassword.Controls.Add(this.PswConfirma);
            this.GroupPassword.HeaderText = "Contraseña";
            this.GroupPassword.Location = new System.Drawing.Point(12, 204);
            this.GroupPassword.Name = "GroupPassword";
            this.GroupPassword.Size = new System.Drawing.Size(406, 93);
            this.GroupPassword.TabIndex = 10;
            this.GroupPassword.Text = "Contraseña";
            // 
            // Message1Label
            // 
            this.Message1Label.Location = new System.Drawing.Point(110, 70);
            this.Message1Label.Name = "Message1Label";
            this.Message1Label.Size = new System.Drawing.Size(154, 18);
            this.Message1Label.TabIndex = 12;
            this.Message1Label.Text = "Las contraseñas no coinciden.";
            this.Message1Label.Visible = false;
            // 
            // Password2Label
            // 
            this.Password2Label.Location = new System.Drawing.Point(18, 49);
            this.Password2Label.Name = "Password2Label";
            this.Password2Label.Size = new System.Drawing.Size(75, 18);
            this.Password2Label.TabIndex = 11;
            this.Password2Label.Text = "Confirmación:";
            // 
            // Password1Label
            // 
            this.Password1Label.Location = new System.Drawing.Point(18, 23);
            this.Password1Label.Name = "Password1Label";
            this.Password1Label.Size = new System.Drawing.Size(65, 18);
            this.Password1Label.TabIndex = 10;
            this.Password1Label.Text = "Contraseña:";
            // 
            // CheckPassword
            // 
            this.CheckPassword.Location = new System.Drawing.Point(432, 204);
            this.CheckPassword.Name = "CheckPassword";
            this.CheckPassword.Size = new System.Drawing.Size(116, 18);
            this.CheckPassword.TabIndex = 11;
            this.CheckPassword.Text = "Asignar contraseña";
            this.CheckPassword.CheckStateChanged += new System.EventHandler(this.CheckPassword_CheckStateChanged);
            // 
            // Perfiles
            // 
            // 
            // Perfiles.NestedRadGridView
            // 
            this.Perfiles.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Perfiles.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Perfiles.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Perfiles.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Perfiles.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowColumnReorder = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowDeleteRow = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowEditRow = false;
            this.Perfiles.EditorControl.MasterTemplate.AllowRowResize = false;
            this.Perfiles.EditorControl.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdUserRol";
            gridViewTextBoxColumn1.HeaderText = "IdUserRol";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdUserRol";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Perfil";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 239;
            this.Perfiles.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Perfiles.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Perfiles.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Perfiles.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Perfiles.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Perfiles.EditorControl.Name = "NestedRadGridView";
            this.Perfiles.EditorControl.ReadOnly = true;
            this.Perfiles.EditorControl.ShowCellErrors = false;
            this.Perfiles.EditorControl.ShowGroupPanel = false;
            this.Perfiles.EditorControl.ShowRowErrors = false;
            this.Perfiles.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Perfiles.EditorControl.TabIndex = 0;
            this.Perfiles.Location = new System.Drawing.Point(73, 110);
            this.Perfiles.Name = "Perfiles";
            this.Perfiles.NullText = "Perfil asignado";
            this.Perfiles.Size = new System.Drawing.Size(334, 20);
            this.Perfiles.TabIndex = 13;
            this.Perfiles.TabStop = false;
            this.Perfiles.ValueMember = "IdUserRol";
            // 
            // PerfilLabel
            // 
            this.PerfilLabel.Location = new System.Drawing.Point(12, 112);
            this.PerfilLabel.Name = "PerfilLabel";
            this.PerfilLabel.Size = new System.Drawing.Size(34, 18);
            this.PerfilLabel.TabIndex = 14;
            this.PerfilLabel.Text = "Perfil:";
            // 
            // SearchPicture
            // 
            this.SearchPicture.Location = new System.Drawing.Point(179, 32);
            this.SearchPicture.Name = "SearchPicture";
            this.SearchPicture.Size = new System.Drawing.Size(21, 20);
            this.SearchPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SearchPicture.TabIndex = 15;
            this.SearchPicture.TabStop = false;
            // 
            // UserPicture
            // 
            this.UserPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UserPicture.Image = global::Jaeger.UI.Kaiju.Properties.Resources.user_32;
            this.UserPicture.Location = new System.Drawing.Point(419, 26);
            this.UserPicture.Name = "UserPicture";
            this.UserPicture.Size = new System.Drawing.Size(110, 104);
            this.UserPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.UserPicture.TabIndex = 12;
            this.UserPicture.TabStop = false;
            // 
            // HeaderImage
            // 
            this.HeaderImage.BackColor = System.Drawing.Color.White;
            this.HeaderImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HeaderImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderImage.Location = new System.Drawing.Point(0, 0);
            this.HeaderImage.Name = "HeaderImage";
            this.HeaderImage.Padding = new System.Windows.Forms.Padding(5);
            this.HeaderImage.Size = new System.Drawing.Size(554, 42);
            this.HeaderImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.HeaderImage.TabIndex = 16;
            this.HeaderImage.TabStop = false;
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.BackColor = System.Drawing.Color.White;
            this.HeaderLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.HeaderLabel.Location = new System.Drawing.Point(9, 11);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(47, 18);
            this.HeaderLabel.TabIndex = 17;
            this.HeaderLabel.Text = "Usuario";
            this.HeaderLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // InformationBox
            // 
            this.InformationBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.InformationBox.Controls.Add(this.ClaveLabel);
            this.InformationBox.Controls.Add(this.Clave);
            this.InformationBox.Controls.Add(this.Nombre);
            this.InformationBox.Controls.Add(this.SearchPicture);
            this.InformationBox.Controls.Add(this.Correo);
            this.InformationBox.Controls.Add(this.PerfilLabel);
            this.InformationBox.Controls.Add(this.NameLabel);
            this.InformationBox.Controls.Add(this.Perfiles);
            this.InformationBox.Controls.Add(this.MailLabel);
            this.InformationBox.Controls.Add(this.UserPicture);
            this.InformationBox.HeaderText = "Información del usuario:";
            this.InformationBox.Location = new System.Drawing.Point(12, 48);
            this.InformationBox.Name = "InformationBox";
            this.InformationBox.Size = new System.Drawing.Size(542, 150);
            this.InformationBox.TabIndex = 18;
            this.InformationBox.TabStop = false;
            this.InformationBox.Text = "Información del usuario:";
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonClose;
            this.ClientSize = new System.Drawing.Size(554, 310);
            this.Controls.Add(this.InformationBox);
            this.Controls.Add(this.HeaderLabel);
            this.Controls.Add(this.HeaderImage);
            this.Controls.Add(this.CheckPassword);
            this.Controls.Add(this.GroupPassword);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.ButtonSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Datos del Usuario";
            this.Load += new System.EventHandler(this.ViewUsuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Correo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Psw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PswConfirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MailLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupPassword)).EndInit();
            this.GroupPassword.ResumeLayout(false);
            this.GroupPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Message1Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password2Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password1Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Perfiles.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Perfiles.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Perfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PerfilLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InformationBox)).EndInit();
            this.InformationBox.ResumeLayout(false);
            this.InformationBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox Clave;
        private Telerik.WinControls.UI.RadTextBox Nombre;
        private Telerik.WinControls.UI.RadTextBox Correo;
        private Telerik.WinControls.UI.RadButton ButtonClose;
        private Telerik.WinControls.UI.RadButton ButtonSave;
        private Telerik.WinControls.UI.RadTextBox Psw;
        private Telerik.WinControls.UI.RadTextBox PswConfirma;
        private Telerik.WinControls.UI.RadLabel ClaveLabel;
        private Telerik.WinControls.UI.RadLabel NameLabel;
        private Telerik.WinControls.UI.RadLabel MailLabel;
        private Telerik.WinControls.UI.RadGroupBox GroupPassword;
        private Telerik.WinControls.UI.RadLabel Password2Label;
        private Telerik.WinControls.UI.RadLabel Password1Label;
        private Telerik.WinControls.UI.RadCheckBox CheckPassword;
        private System.Windows.Forms.PictureBox UserPicture;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Perfiles;
        private Telerik.WinControls.UI.RadLabel PerfilLabel;
        private Telerik.WinControls.UI.RadLabel Message1Label;
        private System.Windows.Forms.PictureBox SearchPicture;
        private System.Windows.Forms.PictureBox HeaderImage;
        private Telerik.WinControls.UI.RadLabel HeaderLabel;
        private Telerik.WinControls.UI.RadGroupBox InformationBox;
    }
}
