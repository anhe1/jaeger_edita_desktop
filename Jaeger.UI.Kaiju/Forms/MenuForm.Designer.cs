﻿
namespace Jaeger.UI.Kaiju.Forms {
    partial class MenuForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.dataGridRelacion = new Telerik.WinControls.UI.RadGridView();
            this.TMenu = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRelacion.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridRelacion
            // 
            this.dataGridRelacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridRelacion.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.dataGridRelacion.MasterTemplate.AllowAddNewRow = false;
            this.dataGridRelacion.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.FormatString = "{0:000}";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.ReadOnly = true;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "ParentID = 0";
            expressionFormattingObject1.Name = "Tab";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Blue;
            gridViewTextBoxColumn2.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "ParentID";
            gridViewTextBoxColumn2.FormatString = "{0:000}";
            gridViewTextBoxColumn2.HeaderText = "Parent ID";
            gridViewTextBoxColumn2.Name = "ParentID";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn1.FieldName = "Default";
            gridViewCheckBoxColumn1.HeaderText = "Default";
            gridViewCheckBoxColumn1.Name = "Default";
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "Name";
            gridViewTextBoxColumn3.MaxLength = 25;
            gridViewTextBoxColumn3.Name = "Name";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.FieldName = "Label";
            gridViewTextBoxColumn4.HeaderText = "Label";
            gridViewTextBoxColumn4.MaxLength = 50;
            gridViewTextBoxColumn4.Name = "Label";
            gridViewTextBoxColumn4.Width = 200;
            gridViewComboBoxColumn1.FieldName = "Assembly";
            gridViewComboBoxColumn1.HeaderText = "Assembly";
            gridViewComboBoxColumn1.Name = "Assembly";
            gridViewTextBoxColumn5.FieldName = "Form";
            gridViewTextBoxColumn5.HeaderText = "Form";
            gridViewTextBoxColumn5.MaxLength = 100;
            gridViewTextBoxColumn5.Name = "Form";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "TypeOf";
            gridViewTextBoxColumn6.HeaderText = "TypeOf";
            gridViewTextBoxColumn6.Name = "TypeOf";
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.FieldName = "Rol";
            gridViewTextBoxColumn7.HeaderText = "Rol";
            gridViewTextBoxColumn7.Name = "Rol";
            gridViewTextBoxColumn8.FieldName = "Action";
            gridViewTextBoxColumn8.HeaderText = "Permisos";
            gridViewTextBoxColumn8.MaxLength = 9;
            gridViewTextBoxColumn8.Name = "Action";
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.FieldName = "ToolTipText";
            gridViewTextBoxColumn9.HeaderText = "ToolTipText";
            gridViewTextBoxColumn9.MaxLength = 100;
            gridViewTextBoxColumn9.Name = "ToolTipText";
            gridViewTextBoxColumn9.Width = 200;
            this.dataGridRelacion.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.dataGridRelacion.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridRelacion.Name = "dataGridRelacion";
            this.dataGridRelacion.ShowGroupPanel = false;
            this.dataGridRelacion.Size = new System.Drawing.Size(1113, 496);
            this.dataGridRelacion.TabIndex = 1;
            // 
            // TMenu
            // 
            this.TMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMenu.Etiqueta = "";
            this.TMenu.Location = new System.Drawing.Point(0, 0);
            this.TMenu.Name = "TMenu";
            this.TMenu.ShowActualizar = true;
            this.TMenu.ShowAutorizar = false;
            this.TMenu.ShowCerrar = true;
            this.TMenu.ShowEditar = true;
            this.TMenu.ShowExportarExcel = true;
            this.TMenu.ShowFiltro = true;
            this.TMenu.ShowGuardar = true;
            this.TMenu.ShowHerramientas = true;
            this.TMenu.ShowImagen = false;
            this.TMenu.ShowImprimir = false;
            this.TMenu.ShowNuevo = true;
            this.TMenu.ShowRemover = true;
            this.TMenu.Size = new System.Drawing.Size(1113, 30);
            this.TMenu.TabIndex = 7;
            this.TMenu.Nuevo.Click += this.TMenu_Nuevo_Click;
            this.TMenu.Guardar.Click += this.TMenu_Guardar_Click;
            this.TMenu.Actualizar.Click += this.TMenu_Actualizar_Click;
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 526);
            this.Controls.Add(this.dataGridRelacion);
            this.Controls.Add(this.TMenu);
            this.Name = "MenuForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Menú";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MenuForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRelacion.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView dataGridRelacion;
        private Common.Forms.ToolBarStandarControl TMenu;
    }
}