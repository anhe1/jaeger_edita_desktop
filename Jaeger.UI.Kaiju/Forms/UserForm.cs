﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Kaiju.Contracts;

namespace Jaeger.UI.Kaiju.Forms {
    public partial class UserForm : RadForm {
        protected IProfileService service;
        protected UserModel usuario = new UserModel();

        public UserForm(UserModel usuario, IProfileService service) {
            InitializeComponent();
            this.usuario = usuario;
            this.service = service;
        }

        private void ViewUsuario_Load(object sender, EventArgs e) {

            this.SearchPicture.Visible = false;

            /// eventos
            this.GroupPassword.Enabled = false;
            this.Perfiles.DisplayMember = "Nombre";
            this.Perfiles.ValueMember = "IdUserRol";
            this.Perfiles.DataSource = this.service.GetList<UserRolModel>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>());

            if (this.usuario == null) {
                usuario = new UserModel();
            }
            SetBindings();
            this.Perfiles.SelectedIndexChanged += CboPerfiles_SelectedIndexChanged;
        }

        private void TxbClave_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
            if (usuario.IdUser == 0) {
                if ((this.service as IUserService).Search(this.Clave.Text) > 0) {
                    this.SearchPicture.Visible = true;
                } else {
                    this.SearchPicture.Visible = false;
                }
            }
        }

        private void SetBindings() {
            this.Clave.DataBindings.Add("Text", usuario, "Clave", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            this.Nombre.DataBindings.Add("Text", usuario, "Nombre", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            this.Correo.DataBindings.Add("Text", usuario, "Correo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            this.Perfiles.DataBindings.Add("SelectedValue", this.usuario, "IdUserRol", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged);
            if (!(ConfigService.Piloto.Clave.Contains("ANHE1")))
                this.Clave.ReadOnly = usuario.IdUser != 0;
        }

        private void TxbPswConfirma_Validating(object sender, System.ComponentModel.CancelEventArgs e) {
            if (this.Psw.Text != this.PswConfirma.Text) {
                this.Message1Label.Visible = true;
                this.ButtonSave.Enabled = false;
            } else {
                this.Message1Label.Visible = false;
                this.ButtonSave.Enabled = true;
            }
        }

        private void CheckPassword_CheckStateChanged(object sender, EventArgs e) {
            this.GroupPassword.Enabled = this.CheckPassword.CheckState == System.Windows.Forms.CheckState.Checked;
        }

        private void TxbCorreo_Validated(object sender, EventArgs e) {
            //if (Jaeger.Domain.Helpers.HelperValidacion.EsCorreo(TxbCorreo.Text) == false) {
            //    RadMessageBox.Show("No es un correo válido!");
            //}
        }

        private void CboPerfiles_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRowInfo temporal = Perfiles.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
            }
        }

        private void ButtonClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ButtonSave_Click(object sender, EventArgs e) {
            //if (this.CheckPassword.Checked == true)
            //    this.usuario.Password = Jaeger.Loggin.Helpers.HelperPassword.Password(this.TxbPsw.Text);
            //if (this.usuario.Id == 0)
            //    this.usuario.Creo = ConfigService.Piloto.Clave;
            //this.service.Save(this.usuario);
        }

        private void Consultar() {
            //usuario = this.service.GetUser(this.usuario.IdUser);
        }

    }
}
