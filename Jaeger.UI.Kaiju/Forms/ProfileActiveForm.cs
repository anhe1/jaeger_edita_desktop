﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Kaiju.Contracts;
using Jaeger.Aplication.Kaiju.Services;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Kaiju.Forms {
    /// <summary>
    /// Formulario de perfiles activos.
    /// </summary>
    public partial class ProfileActiveForm : RadForm {
        #region declaraciones
        protected internal IProfileService Service;
        protected internal BindingList<UserRolDetailModel> DataRol;
        protected internal BindingList<UserModel> DataUsers;
        protected internal RadMenuItem CrearTabla = new RadMenuItem { Text = "Crear tabla" };
        protected internal UIAction _permisos;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="menuElement">MenuElement</param>
        public ProfileActiveForm(UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new UIAction(menuElement.Permisos);
        }

        private void ProfileActiveForm_Load(object sender, EventArgs e) {
            this.gridPerfil.Relations.AddSelfReference(this.gridPerfil.MasterTemplate, "Name", "Parent");
            this.gridPerfil.AllowEditRow = true;

            this.TPerfil.Nuevo.Click += this.TPerfil_Nuevo_Click;
            this.TPerfil.Actualizar.Click += this.TPerfil_Actualizar_Click;
            this.TPerfil.Guardar.Click += this.TPerfil_Guardar_Click;
            this.TPerfil.Cerrar.Click += this.TPerfil_Cerrar_Click;

            this.TUsuario.Nuevo.Click += this.TUsuario_Nuevo_Click;
            this.TUsuario.Remover.Click += this.TUsuario_Remover_Click;
            this.TUsuario.Filtro.Click += this.TUsuario_Filtro_Click;
        }

        #region barra de herramientas Perfil
        public virtual void TPerfil_Nuevo_Click(object sender, EventArgs e) {
            var _result = new UserRolDetailModel();
            if (this.DataRol == null)
                this.DataRol = new BindingList<UserRolDetailModel>();
            this.DataRol.Add(_result);
        }

        public virtual void TPerfil_Guardar_Click(object sender, EventArgs e) {
            if (this.gridUserRol.CurrentRow != null) {
                var _seleccionado = this.gridUserRol.CurrentRow.DataBoundItem as UserRolDetailModel;
                if (_seleccionado != null) {
                    this.gridPerfil.BeginUpdate();
                    using (var espera = new Waiting1Form(this.Guardar)) {
                        espera.Text = "Almacenando...";
                        espera.ShowDialog(this);
                    }
                    this.gridPerfil.EndUpdate();
                }
            }
        }

        public virtual void TPerfil_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "...";
                espera.ShowDialog(this);
            }

            var cbUsuarios = (GridViewMultiComboBoxColumn)this.gridUsuarios.MasterTemplate.Columns["IdUser"];
            cbUsuarios.DisplayMember = "Nombre";
            cbUsuarios.ValueMember = "IdUser";
            cbUsuarios.DataSource = this.DataUsers;

            this.gridUserRol.DataSource = this.DataRol;
            this.gridPerfil.DataMember = "Perfil";
            this.gridPerfil.DataSource = this.DataRol;
            this.gridUsuarios.DataMember = "Relacion";
            this.gridUsuarios.DataSource = this.DataRol;
        }

        public virtual void TPerfil_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de herramientas usuarios
        public virtual void TUsuario_Nuevo_Click(object sender, EventArgs e) {
            if (this.gridUserRol.CurrentRow != null) {
                var _seleccionado = this.gridUserRol.CurrentRow.DataBoundItem as UserRolDetailModel;
                if (_seleccionado != null) {
                    _seleccionado.Relacion.Add(new UIMenuRolRelacionModel { IdUserRol = _seleccionado.IdUserRol });
                }
            }
        }

        public virtual void TUsuario_Remover_Click(object sender, EventArgs e) {

        }

        public virtual void TUsuario_Filtro_Click(object sender, EventArgs e) {

        }
        #endregion

        #region grid usuarios
        public virtual void GridUsuarios_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (this.gridUsuarios.CurrentColumn is GridViewMultiComboBoxColumn) {
                var editor = (RadMultiColumnComboBoxElement)this.gridUsuarios.ActiveEditor;
                if ((string)editor.Tag == "listo") { return; }
                editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "IdUser", Name = "IdUser", FieldName = "IdUser", IsVisible = false });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Clave", Name = "Clave", FieldName = "Clave" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Usuario", Name = "Nombre", FieldName = "Nombre" });
                editor.AutoSizeDropDownToBestFit = true;
                editor.Tag = "listo";
            }
        }

        public virtual void GridPerfil_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "Activo") {
                if (this.gridPerfil.CurrentRow != null) {
                    var _selccionado = this.gridPerfil.CurrentRow.DataBoundItem as UIMenuProfileDetailModel;
                    var _rol = this.gridUserRol.CurrentRow.DataBoundItem as UserRolDetailModel;
                    if (_selccionado != null) {
                        _selccionado.IdPerfilMenu = _selccionado.Id;
                        _selccionado.IdRol = _rol.IdUserRol;
                        _selccionado.Key = _selccionado.Name;
                        _selccionado.SetModified = true;
                    }
                }
            }
        }
        #endregion

        #region grid perfiles
        public virtual void GridPerfil_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            var _seleccionado = e.Row.DataBoundItem as UIMenuProfileDetailModel;
            if (_seleccionado != null) {
                if (_seleccionado.Default == true) {
                    e.Cancel = true;
                }
            }
        }

        public virtual void GridPerfil_CommandCellClick(object sender, GridViewCellEventArgs e) {
            if (this.gridPerfil.CurrentRow != null) {
                var _seleccionado = this.gridPerfil.CurrentRow.DataBoundItem as UIMenuProfileDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Form != null) {
                        var _permisos = new AccionesForm();
                        var _result = _permisos.ShowForm(_seleccionado);
                        if (_result != null) {
                            _seleccionado.Action2 = _result;
                            _seleccionado.SetModified = true;
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void Consultar() {
            this.DataRol = new BindingList<UserRolDetailModel>(this.Service.GetList<UserRolDetailModel>(ProfileService.Query().OnlyActive().Build()).ToList());
            if (this.DataRol == null) {
                this.DataRol = new BindingList<UserRolDetailModel>();
            }

            this.DataUsers = new BindingList<UserModel>(this.Service.GetList<UserModel>(UserService.Query().OnlyActive().Build()).ToList());
            if (this.DataUsers == null) {
                this.DataUsers = new BindingList<UserModel>();
            }
        }

        public virtual void Guardar() {
            if (this.gridUserRol.CurrentRow != null) {
                var _seleccionado = this.gridUserRol.CurrentRow.DataBoundItem as UserRolDetailModel;
                if (_seleccionado != null) {
                    _seleccionado = this.Service.Save(_seleccionado);
                }
            }
        }
        #endregion
    }
}
