﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.UI.Kaiju.Forms {
    /// <summary>
    /// Formulario de asignación de acciones a un perfil.
    /// </summary>
    public partial class AccionesForm : RadForm {
        #region declaraciones
        private UIAction _action;
        private UIAction _permisos;
        private bool _return = false;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public AccionesForm() {
            InitializeComponent();
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="action">UIAction</param>
        public AccionesForm(UIAction action) {
            InitializeComponent();
            _action = action;
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="uIMenu">UIMenuProfileDetailModel</param>
        public AccionesForm(UIMenuProfileDetailModel uIMenu) {
            InitializeComponent();
            this._action = new UIAction(uIMenu.Action2);
            this._permisos = new UIAction(uIMenu.Permisos);
        }

        private void AccionesForm_Load(object sender, EventArgs e) {
            this.Cancelar.Checked = this._action.Cancelar;
            this.Agregar.Checked = this._action.Agregar;
            this.Remover.Checked = this._action.Remover;
            this.Editar.Checked = this._action.Editar;
            this.Autorizar.Checked = this._action.Autorizar;
            this.Imprimir.Checked = this._action.Imprimir;
            this.Exportar.Checked = this._action.Exportar;
            this.Importar.Checked = this._action.Importar;
            this.Status.Checked = this._action.Status;
            this.Reportes.Checked = this._action.Reporte;

            this.Cancelar.Enabled = this._permisos.Cancelar;
            this.Agregar.Enabled = this._permisos.Agregar;
            this.Remover.Enabled = this._permisos.Remover;
            this.Editar.Enabled = this._permisos.Editar;
            this.Autorizar.Enabled = this._permisos.Autorizar;
            this.Imprimir.Enabled = this._permisos.Imprimir;
            this.Exportar.Enabled = this._permisos.Exportar;
            this.Importar.Enabled = this._permisos.Importar;
            this.Status.Enabled = this._permisos.Status;
            this.Reportes.Enabled = this._permisos.Reporte;
        }

        private void BAceptar_Click(object sender, EventArgs e) {
            this._action.Cancelar = this.Cancelar.Checked;
            this._action.Agregar = this.Agregar.Checked;
            this._action.Remover = this.Remover.Checked;
            this._action.Editar = this.Editar.Checked;
            this._action.Autorizar = this.Autorizar.Checked;
            this._action.Imprimir = this.Imprimir.Checked;
            this._action.Exportar = this.Exportar.Checked;
            this._action.Importar = this.Importar.Checked;
            this._action.Status = this.Status.Checked;
            this._action.Reporte = this.Reportes.Checked;
            this._return = true;
            this.Close();
        }

        private void BCancelar_Click(object sender, EventArgs e) {
            this._return = false;
            this.Close();
        }

        public string ShowForm(UIMenuProfileDetailModel uIMenu) {
            this._action = new UIAction(uIMenu.Action2);
            this._permisos = new UIAction(uIMenu.Permisos);
            this.ShowDialog();
            if (this._return) {
                return this._action.Acciones;
            }
            return null;
        }
    }
}
