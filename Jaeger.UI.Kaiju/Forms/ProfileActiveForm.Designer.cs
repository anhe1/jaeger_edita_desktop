﻿namespace Jaeger.UI.Kaiju.Forms {
    partial class ProfileActiveForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject2 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfileActiveForm));
            this.gridPerfil = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.gridUserRol = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.gridUsuarios = new Telerik.WinControls.UI.RadGridView();
            this.TUsuario = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.TPerfil = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridPerfil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPerfil.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridUserRol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUserRol.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUsuarios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPerfil
            // 
            this.gridPerfil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPerfil.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridPerfil.MasterTemplate.AllowAddNewRow = false;
            this.gridPerfil.MasterTemplate.AllowDeleteRow = false;
            this.gridPerfil.MasterTemplate.AllowRowResize = false;
            this.gridPerfil.MasterTemplate.AutoGenerateColumns = false;
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "Nuevo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.LawnGreen;
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "0";
            gridViewTextBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.FormatString = "{0:000}";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            conditionalFormattingObject2.ApplyToRow = true;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.Name = "NewCondition";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Blue;
            conditionalFormattingObject2.TValue1 = "0";
            conditionalFormattingObject2.TValue2 = "0";
            gridViewTextBoxColumn2.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "ParentID";
            gridViewTextBoxColumn2.FormatString = "{0:000}";
            gridViewTextBoxColumn2.HeaderText = "Parent ID";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "ParentID";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Name";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 65;
            gridViewTextBoxColumn4.FieldName = "Parent";
            gridViewTextBoxColumn4.HeaderText = "Parent";
            gridViewTextBoxColumn4.Name = "Parent";
            gridViewTextBoxColumn5.FieldName = "Label";
            gridViewTextBoxColumn5.HeaderText = "Menu";
            gridViewTextBoxColumn5.Name = "Label";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 220;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "IdPerfil";
            gridViewTextBoxColumn6.FormatString = "{0:000}";
            gridViewTextBoxColumn6.HeaderText = "Perfil ID";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "IdPerfil";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "=((ACTIVO = FALSE) AND (DEFAULT = FALSE))";
            expressionFormattingObject1.Name = "Registro Activo";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic);
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn7.DataType = typeof(int);
            gridViewTextBoxColumn7.FieldName = "IdPerfilMenu";
            gridViewTextBoxColumn7.FormatString = "{0:000}";
            gridViewTextBoxColumn7.HeaderText = "Menu ID";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "IdPerfilMenu";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "IdRol";
            gridViewTextBoxColumn8.FormatString = "{0:000}";
            gridViewTextBoxColumn8.HeaderText = "Rol ID";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "IdRol";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCommandColumn1.FieldName = "Action2";
            gridViewCommandColumn1.HeaderText = "Permisos";
            gridViewCommandColumn1.Name = "Action";
            gridViewCommandColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCommandColumn1.Width = 85;
            gridViewTextBoxColumn9.FieldName = "ToolTipText";
            gridViewTextBoxColumn9.HeaderText = "ToolTipText";
            gridViewTextBoxColumn9.Name = "ToolTipText";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 250;
            gridViewTextBoxColumn10.FieldName = "Permisos";
            gridViewTextBoxColumn10.HeaderText = "Default";
            gridViewTextBoxColumn10.Name = "Permisos";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.Width = 85;
            gridViewCheckBoxColumn2.FieldName = "Default";
            gridViewCheckBoxColumn2.HeaderText = "Default";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Default";
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            expressionFormattingObject2.ApplyToRow = true;
            expressionFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.Expression = "SetModified = True";
            expressionFormattingObject2.Name = "SetModified";
            expressionFormattingObject2.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            expressionFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            gridViewTextBoxColumn11.ConditionalFormattingObjectList.Add(expressionFormattingObject2);
            gridViewTextBoxColumn11.DataType = typeof(bool);
            gridViewTextBoxColumn11.FieldName = "SetModified";
            gridViewTextBoxColumn11.HeaderText = "Modified";
            gridViewTextBoxColumn11.Name = "SetModified";
            this.gridPerfil.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewCommandColumn1,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn11});
            this.gridPerfil.MasterTemplate.EnableFiltering = true;
            this.gridPerfil.MasterTemplate.EnableHierarchyFiltering = true;
            this.gridPerfil.MasterTemplate.ShowFilteringRow = false;
            this.gridPerfil.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridPerfil.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridPerfil.Name = "gridPerfil";
            this.gridPerfil.ShowGroupPanel = false;
            this.gridPerfil.Size = new System.Drawing.Size(746, 630);
            this.gridPerfil.TabIndex = 7;
            this.gridPerfil.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridPerfil_CellBeginEdit);
            this.gridPerfil.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridPerfil_CellEndEdit);
            this.gridPerfil.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.GridPerfil_CommandCellClick);
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1120, 630);
            this.radSplitContainer2.TabIndex = 9;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radSplitContainer1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(370, 630);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.1684588F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(-188, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(370, 630);
            this.radSplitContainer1.TabIndex = 9;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.gridUserRol);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(370, 313);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // gridUserRol
            // 
            this.gridUserRol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUserRol.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridUserRol.MasterTemplate.AllowAddNewRow = false;
            this.gridUserRol.MasterTemplate.AllowRowResize = false;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.Name = "NewCondition";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.Lime;
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.TValue1 = "0";
            conditionalFormattingObject3.TValue2 = "0";
            gridViewTextBoxColumn12.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "IdUserRol";
            gridViewTextBoxColumn12.FormatString = "{0:0000}";
            gridViewTextBoxColumn12.HeaderText = "ID";
            gridViewTextBoxColumn12.Name = "IdUserRol";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn13.FieldName = "Clave";
            gridViewTextBoxColumn13.HeaderText = "Clave";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Clave";
            gridViewTextBoxColumn13.Width = 110;
            gridViewTextBoxColumn14.FieldName = "Nombre";
            gridViewTextBoxColumn14.HeaderText = "Descripción";
            gridViewTextBoxColumn14.Name = "Nombre";
            gridViewTextBoxColumn14.Width = 210;
            gridViewTextBoxColumn15.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn15.FieldName = "FechaNuevo";
            gridViewTextBoxColumn15.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn15.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "FechaNuevo";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn15.Width = 75;
            gridViewTextBoxColumn16.FieldName = "Creo";
            gridViewTextBoxColumn16.HeaderText = "Creó";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Creo";
            gridViewTextBoxColumn16.Width = 65;
            this.gridUserRol.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
            this.gridUserRol.MasterTemplate.EnableFiltering = true;
            this.gridUserRol.MasterTemplate.ShowFilteringRow = false;
            this.gridUserRol.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridUserRol.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridUserRol.Name = "gridUserRol";
            this.gridUserRol.ShowGroupPanel = false;
            this.gridUserRol.Size = new System.Drawing.Size(370, 313);
            this.gridUserRol.TabIndex = 2;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.gridUsuarios);
            this.splitPanel2.Controls.Add(this.TUsuario);
            this.splitPanel2.Location = new System.Drawing.Point(0, 317);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(370, 313);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // gridUsuarios
            // 
            this.gridUsuarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridUsuarios.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridUsuarios.MasterTemplate.AllowAddNewRow = false;
            this.gridUsuarios.MasterTemplate.AllowRowResize = false;
            gridViewMultiComboBoxColumn1.FieldName = "IdUser";
            gridViewMultiComboBoxColumn1.HeaderText = "Usuario";
            gridViewMultiComboBoxColumn1.Name = "IdUser";
            gridViewMultiComboBoxColumn1.Width = 250;
            gridViewTextBoxColumn17.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn17.FieldName = "FechaNuevo";
            gridViewTextBoxColumn17.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn17.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "FechaNuevo";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn17.Width = 75;
            gridViewTextBoxColumn18.FieldName = "Creo";
            gridViewTextBoxColumn18.HeaderText = "Creó";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "Creo";
            gridViewTextBoxColumn18.Width = 65;
            this.gridUsuarios.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.gridUsuarios.MasterTemplate.EnableFiltering = true;
            this.gridUsuarios.MasterTemplate.ShowFilteringRow = false;
            this.gridUsuarios.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridUsuarios.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridUsuarios.Name = "gridUsuarios";
            this.gridUsuarios.ShowGroupPanel = false;
            this.gridUsuarios.Size = new System.Drawing.Size(370, 283);
            this.gridUsuarios.TabIndex = 3;
            this.gridUsuarios.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridUsuarios_CellBeginEdit);
            // 
            // TUsuario
            // 
            this.TUsuario.Dock = System.Windows.Forms.DockStyle.Top;
            this.TUsuario.Etiqueta = "";
            this.TUsuario.Location = new System.Drawing.Point(0, 0);
            this.TUsuario.Name = "TUsuario";
            this.TUsuario.ReadOnly = false;
            this.TUsuario.ShowActualizar = false;
            this.TUsuario.ShowAutorizar = false;
            this.TUsuario.ShowCerrar = false;
            this.TUsuario.ShowEditar = false;
            this.TUsuario.ShowExportarExcel = true;
            this.TUsuario.ShowFiltro = true;
            this.TUsuario.ShowGuardar = false;
            this.TUsuario.ShowHerramientas = false;
            this.TUsuario.ShowImagen = false;
            this.TUsuario.ShowImprimir = false;
            this.TUsuario.ShowNuevo = true;
            this.TUsuario.ShowRemover = true;
            this.TUsuario.Size = new System.Drawing.Size(370, 30);
            this.TUsuario.TabIndex = 7;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.gridPerfil);
            this.splitPanel4.Location = new System.Drawing.Point(374, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(746, 630);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.1684588F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(188, 0);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // TPerfil
            // 
            this.TPerfil.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPerfil.Etiqueta = "";
            this.TPerfil.Location = new System.Drawing.Point(0, 0);
            this.TPerfil.Name = "TPerfil";
            this.TPerfil.ReadOnly = false;
            this.TPerfil.ShowActualizar = true;
            this.TPerfil.ShowAutorizar = false;
            this.TPerfil.ShowCerrar = true;
            this.TPerfil.ShowEditar = false;
            this.TPerfil.ShowExportarExcel = true;
            this.TPerfil.ShowFiltro = false;
            this.TPerfil.ShowGuardar = true;
            this.TPerfil.ShowHerramientas = false;
            this.TPerfil.ShowImagen = false;
            this.TPerfil.ShowImprimir = false;
            this.TPerfil.ShowNuevo = true;
            this.TPerfil.ShowRemover = true;
            this.TPerfil.Size = new System.Drawing.Size(1120, 30);
            this.TPerfil.TabIndex = 6;
            // 
            // ProfileActiveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 660);
            this.Controls.Add(this.radSplitContainer2);
            this.Controls.Add(this.TPerfil);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProfileActiveForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Perfiles";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProfileActiveForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPerfil.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPerfil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridUserRol.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUserRol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridUsuarios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView gridPerfil;
        private Common.Forms.ToolBarStandarControl TPerfil;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadGridView gridUserRol;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView gridUsuarios;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Common.Forms.ToolBarStandarControl TUsuario;
    }
}