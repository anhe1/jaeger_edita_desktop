﻿namespace Jaeger.UI.Kaiju.Forms {
    partial class AccionesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Cancelar = new Telerik.WinControls.UI.RadCheckBox();
            this.Agregar = new Telerik.WinControls.UI.RadCheckBox();
            this.Remover = new Telerik.WinControls.UI.RadCheckBox();
            this.Editar = new Telerik.WinControls.UI.RadCheckBox();
            this.Autorizar = new Telerik.WinControls.UI.RadCheckBox();
            this.Imprimir = new Telerik.WinControls.UI.RadCheckBox();
            this.Exportar = new Telerik.WinControls.UI.RadCheckBox();
            this.Importar = new Telerik.WinControls.UI.RadCheckBox();
            this.Status = new Telerik.WinControls.UI.RadCheckBox();
            this.Reportes = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox10 = new Telerik.WinControls.UI.RadGroupBox();
            this.BAceptar = new Telerik.WinControls.UI.RadButton();
            this.BCancelar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Remover)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Editar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Autorizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Reportes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox10)).BeginInit();
            this.radCheckBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Cancelar
            // 
            this.Cancelar.Location = new System.Drawing.Point(17, 21);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(63, 18);
            this.Cancelar.TabIndex = 64;
            this.Cancelar.Text = "Cancelar";
            // 
            // Agregar
            // 
            this.Agregar.Location = new System.Drawing.Point(17, 45);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(60, 18);
            this.Agregar.TabIndex = 65;
            this.Agregar.Text = "Agregar";
            // 
            // Remover
            // 
            this.Remover.Location = new System.Drawing.Point(17, 69);
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(65, 18);
            this.Remover.TabIndex = 66;
            this.Remover.Text = "Remover";
            // 
            // Editar
            // 
            this.Editar.Location = new System.Drawing.Point(17, 93);
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(49, 18);
            this.Editar.TabIndex = 67;
            this.Editar.Text = "Editar";
            // 
            // Autorizar
            // 
            this.Autorizar.Location = new System.Drawing.Point(17, 117);
            this.Autorizar.Name = "Autorizar";
            this.Autorizar.Size = new System.Drawing.Size(66, 18);
            this.Autorizar.TabIndex = 68;
            this.Autorizar.Text = "Autorizar";
            // 
            // Imprimir
            // 
            this.Imprimir.Location = new System.Drawing.Point(17, 141);
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Size = new System.Drawing.Size(63, 18);
            this.Imprimir.TabIndex = 69;
            this.Imprimir.Text = "Imprimir";
            // 
            // Exportar
            // 
            this.Exportar.Location = new System.Drawing.Point(17, 165);
            this.Exportar.Name = "Exportar";
            this.Exportar.Size = new System.Drawing.Size(62, 18);
            this.Exportar.TabIndex = 70;
            this.Exportar.Text = "Exportar";
            // 
            // Importar
            // 
            this.Importar.Location = new System.Drawing.Point(17, 189);
            this.Importar.Name = "Importar";
            this.Importar.Size = new System.Drawing.Size(64, 18);
            this.Importar.TabIndex = 71;
            this.Importar.Text = "Importar";
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(17, 213);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(51, 18);
            this.Status.TabIndex = 72;
            this.Status.Text = "Status";
            // 
            // Reportes
            // 
            this.Reportes.Location = new System.Drawing.Point(17, 237);
            this.Reportes.Name = "Reportes";
            this.Reportes.Size = new System.Drawing.Size(65, 18);
            this.Reportes.TabIndex = 73;
            this.Reportes.Text = "Reportes";
            // 
            // radCheckBox10
            // 
            this.radCheckBox10.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radCheckBox10.Controls.Add(this.Cancelar);
            this.radCheckBox10.Controls.Add(this.Reportes);
            this.radCheckBox10.Controls.Add(this.Agregar);
            this.radCheckBox10.Controls.Add(this.Status);
            this.radCheckBox10.Controls.Add(this.Remover);
            this.radCheckBox10.Controls.Add(this.Importar);
            this.radCheckBox10.Controls.Add(this.Editar);
            this.radCheckBox10.Controls.Add(this.Exportar);
            this.radCheckBox10.Controls.Add(this.Autorizar);
            this.radCheckBox10.Controls.Add(this.Imprimir);
            this.radCheckBox10.HeaderImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radCheckBox10.HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Right;
            this.radCheckBox10.HeaderText = "Permitir";
            this.radCheckBox10.Location = new System.Drawing.Point(12, 12);
            this.radCheckBox10.Name = "radCheckBox10";
            this.radCheckBox10.Size = new System.Drawing.Size(113, 273);
            this.radCheckBox10.TabIndex = 74;
            this.radCheckBox10.Text = "Permitir";
            // 
            // BAceptar
            // 
            this.BAceptar.Location = new System.Drawing.Point(12, 295);
            this.BAceptar.Name = "BAceptar";
            this.BAceptar.Size = new System.Drawing.Size(113, 23);
            this.BAceptar.TabIndex = 75;
            this.BAceptar.Text = "Aceptar";
            this.BAceptar.Click += new System.EventHandler(this.BAceptar_Click);
            // 
            // BCancelar
            // 
            this.BCancelar.Location = new System.Drawing.Point(12, 324);
            this.BCancelar.Name = "BCancelar";
            this.BCancelar.Size = new System.Drawing.Size(113, 23);
            this.BCancelar.TabIndex = 75;
            this.BCancelar.Text = "Cancelar";
            this.BCancelar.Click += new System.EventHandler(this.BCancelar_Click);
            // 
            // AccionesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(137, 357);
            this.Controls.Add(this.BCancelar);
            this.Controls.Add(this.BAceptar);
            this.Controls.Add(this.radCheckBox10);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccionesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Acciones";
            this.Load += new System.EventHandler(this.AccionesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Remover)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Editar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Autorizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Exportar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Reportes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox10)).EndInit();
            this.radCheckBox10.ResumeLayout(false);
            this.radCheckBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal Telerik.WinControls.UI.RadCheckBox Cancelar;
        internal Telerik.WinControls.UI.RadCheckBox Agregar;
        internal Telerik.WinControls.UI.RadCheckBox Remover;
        internal Telerik.WinControls.UI.RadCheckBox Editar;
        internal Telerik.WinControls.UI.RadCheckBox Autorizar;
        internal Telerik.WinControls.UI.RadCheckBox Imprimir;
        internal Telerik.WinControls.UI.RadCheckBox Exportar;
        internal Telerik.WinControls.UI.RadCheckBox Importar;
        internal Telerik.WinControls.UI.RadCheckBox Status;
        internal Telerik.WinControls.UI.RadCheckBox Reportes;
        internal Telerik.WinControls.UI.RadGroupBox radCheckBox10;
        internal Telerik.WinControls.UI.RadButton BAceptar;
        internal Telerik.WinControls.UI.RadButton BCancelar;
    }
}