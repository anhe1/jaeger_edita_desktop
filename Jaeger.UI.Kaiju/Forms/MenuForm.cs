﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Kaiju.Services;
using Jaeger.Domain.Kaiju.Entities;
using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Kaiju.Forms {
    /// <summary>
    /// Formulario para el mantenimiento de menús.
    /// </summary>
    public partial class MenuForm : RadForm {
        protected UIMenuService service;
        private BindingList<UIMenuElement> models;

        public MenuForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void MenuForm_Load(object sender, EventArgs e) {
            this.service = new UIMenuService();
            this.dataGridRelacion.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.dataGridRelacion.Relations.AddSelfReference(this.dataGridRelacion.MasterTemplate, "ID", "ParentID");
            var combo1 = new List<string> { "Jaeger.UI.Contribuyentes", "Jaeger.UI.Banco" };
            var combo = this.dataGridRelacion.Columns["Assembly"] as GridViewComboBoxColumn;
            combo.DataSource = combo1;
        }

        private void TMenu_Nuevo_Click(object sender, EventArgs e) {
            //if (this.dataGridRelacion.CurrentRow != null) {
            //    var _selccionado = this.dataGridRelacion.CurrentRow.DataBoundItem as UIMenuModel;
            //    if (_selccionado != null) {
            //        var _nuevo = new UIMenuModel();
            //        _nuevo.Id = _selccionado.Id * 1000;
            //        _nuevo.ParentId = _selccionado.Id;
            //        this.models.Add(_nuevo);
            //    }
            //}
        }

        private void TMenu_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "...";
                espera.ShowDialog(this);
            }
            this.dataGridRelacion.DataSource = this.models;
        }

        private void TMenu_Guardar_Click(object sender, EventArgs e) {
            this.Save();
            //if (this.dataGridRelacion.CurrentRow != null) {
            //    var _selccionado = this.dataGridRelacion.CurrentRow.DataBoundItem as UIMenuModel;
            //    if (_selccionado != null) {
            //        _selccionado = this.service.Save(_selccionado);
            //    }
            //}
        }

        private void Consultar() {
            this.models = new BindingList<UIMenuElement>(ConfigService.Menus);//this.service.GetList();
        }

        public void Save() {
            foreach (var item in this.models) {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
