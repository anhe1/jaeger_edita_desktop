﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class SolicitudCotizacionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SolicitudCotizacionControl));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarComprobanteFiscal = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonEmisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.Status = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.FilePDF = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Series = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelUuid = new Telerik.WinControls.UI.CommandBarLabel();
            this.IdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Receptor = new Jaeger.UI.Contribuyentes.Forms.DirectorioControl();
            this.Documento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.Serie = new Telerik.WinControls.UI.RadDropDownList();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.MetodoEnvio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Observaciones = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Contacto = new Telerik.WinControls.UI.RadDropDownList();
            this.FechaRequerida = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.Embarque = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaRequerida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(1200, 30);
            this.RadCommandBar1.TabIndex = 3;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarComprobanteFiscal});
            this.CommandBarRowElement1.Text = "";
            // 
            // ToolBarComprobanteFiscal
            // 
            this.ToolBarComprobanteFiscal.DisplayName = "Emision de Comprobante";
            this.ToolBarComprobanteFiscal.EnableFocusBorder = false;
            this.ToolBarComprobanteFiscal.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonEmisor,
            this.Separador,
            this.ToolLabelStatus,
            this.Status,
            this.Nuevo,
            this.Duplicar,
            this.Guardar,
            this.Actualizar,
            this.Cancelar,
            this.Imprimir,
            this.FilePDF,
            this.Separator2,
            this.Series,
            this.CommandBarSeparator2,
            this.ToolLabelUuid,
            this.IdDocumento,
            this.Cerrar});
            this.ToolBarComprobanteFiscal.Name = "ToolBarComprobanteFiscal";
            // 
            // 
            // 
            this.ToolBarComprobanteFiscal.OverflowButton.Enabled = true;
            this.ToolBarComprobanteFiscal.ShowHorizontalLine = false;
            this.ToolBarComprobanteFiscal.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarComprobanteFiscal.GetChildAt(2))).Enabled = true;
            // 
            // ToolBarButtonEmisor
            // 
            this.ToolBarButtonEmisor.DefaultItem = null;
            this.ToolBarButtonEmisor.DisplayName = "Emisor";
            this.ToolBarButtonEmisor.DrawText = true;
            this.ToolBarButtonEmisor.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.administrator_male_16px;
            this.ToolBarButtonEmisor.Name = "ToolBarButtonEmisor";
            this.ToolBarButtonEmisor.Text = "XXXXXXXXXXXXXX";
            this.ToolBarButtonEmisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            // 
            // Status
            // 
            this.Status.AutoCompleteDisplayMember = "Descripcion";
            this.Status.AutoCompleteValueMember = "Id";
            this.Status.DisplayMember = "Descripcion";
            this.Status.DisplayName = "Descripcion";
            this.Status.DrawImage = false;
            this.Status.DrawText = true;
            this.Status.DropDownAnimationEnabled = true;
            this.Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Status.Image = ((System.Drawing.Image)(resources.GetObject("Status.Image")));
            this.Status.MaxDropDownItems = 0;
            this.Status.Name = "Status";
            this.Status.Text = "";
            this.Status.ValueMember = "Id";
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Nuevo.Click += new System.EventHandler(this.Nuevo_Click);
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.documents_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Duplicar.Click += new System.EventHandler(this.Duplicar_Click);
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Enabled = false;
            this.Guardar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.save_16;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.refresh_16;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Actualizar.Click += new System.EventHandler(this.Actualizar_Click);
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Enabled = false;
            this.Cancelar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.Click += new System.EventHandler(this.Imprimir_Click);
            // 
            // FilePDF
            // 
            this.FilePDF.DisplayName = "Archivo PDF";
            this.FilePDF.DrawText = true;
            this.FilePDF.Enabled = false;
            this.FilePDF.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.export_pdf_16px;
            this.FilePDF.Name = "FilePDF";
            this.FilePDF.Text = "PDF";
            this.FilePDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Series
            // 
            this.Series.DisplayName = "Series y Folios";
            this.Series.DrawText = true;
            this.Series.Enabled = false;
            this.Series.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.counter_16px;
            this.Series.Name = "Series";
            this.Series.Text = "Series y Folios";
            this.Series.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolLabelUuid
            // 
            this.ToolLabelUuid.DisplayName = "UUID";
            this.ToolLabelUuid.Name = "ToolLabelUuid";
            this.ToolLabelUuid.Text = "UUID:";
            this.ToolLabelUuid.VisibleInOverflowMenu = false;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DisplayName = "IdDocumento";
            this.IdDocumento.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.IdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Text = "";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.Receptor);
            this.groupBox1.Controls.Add(this.Documento);
            this.groupBox1.Controls.Add(this.Folio);
            this.groupBox1.Controls.Add(this.Serie);
            this.groupBox1.Controls.Add(this.RadLabel8);
            this.groupBox1.Controls.Add(this.RadLabel7);
            this.groupBox1.Controls.Add(this.RadLabel1);
            this.groupBox1.Controls.Add(this.MetodoEnvio);
            this.groupBox1.Controls.Add(this.Observaciones);
            this.groupBox1.Controls.Add(this.radLabel3);
            this.groupBox1.Controls.Add(this.radLabel6);
            this.groupBox1.Controls.Add(this.RadLabel18);
            this.groupBox1.Controls.Add(this.FechaEmision);
            this.groupBox1.Controls.Add(this.Contacto);
            this.groupBox1.Controls.Add(this.FechaRequerida);
            this.groupBox1.Controls.Add(this.RadLabel13);
            this.groupBox1.Controls.Add(this.radLabel4);
            this.groupBox1.Controls.Add(this.Embarque);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1200, 80);
            this.groupBox1.TabIndex = 239;
            this.groupBox1.TabStop = false;
            // 
            // Receptor
            // 
            this.Receptor.AllowAddNew = false;
            this.Receptor.LDomicilio = "Domicilio:";
            this.Receptor.LNombre = "Proveedor:";
            this.Receptor.Location = new System.Drawing.Point(2, 30);
            this.Receptor.Margin = new System.Windows.Forms.Padding(4);
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = false;
            this.Receptor.Relacion = Jaeger.Domain.Base.ValueObjects.TipoRelacionComericalEnum.None;
            this.Receptor.ShowDomicilio = false;
            this.Receptor.ShowRFC = true;
            this.Receptor.Size = new System.Drawing.Size(778, 20);
            this.Receptor.TabIndex = 238;
            // 
            // Documento
            // 
            // 
            // Documento.NestedRadGridView
            // 
            this.Documento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Documento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Documento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Documento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Documento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Documento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Documento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "TipoDeComprobante";
            gridViewTextBoxColumn1.HeaderText = "Comprobante";
            gridViewTextBoxColumn1.Name = "TipoDeComprobante";
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn3.FieldName = "Serie";
            gridViewTextBoxColumn3.HeaderText = "Serie";
            gridViewTextBoxColumn3.Name = "Serie";
            gridViewTextBoxColumn4.FieldName = "Folio";
            gridViewTextBoxColumn4.HeaderText = "Folio";
            gridViewTextBoxColumn4.Name = "Folio";
            gridViewTextBoxColumn5.FieldName = "Template";
            gridViewTextBoxColumn5.HeaderText = "Template";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Template";
            this.Documento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.Documento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Documento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Documento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Documento.EditorControl.Name = "NestedRadGridView";
            this.Documento.EditorControl.ReadOnly = true;
            this.Documento.EditorControl.ShowGroupPanel = false;
            this.Documento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Documento.EditorControl.TabIndex = 0;
            this.Documento.Location = new System.Drawing.Point(68, 5);
            this.Documento.Name = "Documento";
            this.Documento.NullText = "Documento";
            this.Documento.Size = new System.Drawing.Size(171, 20);
            this.Documento.TabIndex = 231;
            this.Documento.TabStop = false;
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(453, 5);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.ReadOnly = true;
            this.Folio.Size = new System.Drawing.Size(121, 20);
            this.Folio.TabIndex = 235;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Serie
            // 
            this.Serie.Location = new System.Drawing.Point(285, 5);
            this.Serie.Name = "Serie";
            this.Serie.NullText = "Serie";
            this.Serie.Size = new System.Drawing.Size(125, 20);
            this.Serie.TabIndex = 233;
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(246, 6);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(33, 18);
            this.RadLabel8.TabIndex = 232;
            this.RadLabel8.Text = "Serie:";
            // 
            // RadLabel7
            // 
            this.RadLabel7.Location = new System.Drawing.Point(414, 6);
            this.RadLabel7.Name = "RadLabel7";
            this.RadLabel7.Size = new System.Drawing.Size(33, 18);
            this.RadLabel7.TabIndex = 234;
            this.RadLabel7.Text = "Folio:";
            // 
            // RadLabel1
            // 
            this.RadLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadLabel1.Location = new System.Drawing.Point(591, 6);
            this.RadLabel1.Name = "RadLabel1";
            this.RadLabel1.Size = new System.Drawing.Size(85, 18);
            this.RadLabel1.TabIndex = 193;
            this.RadLabel1.Text = "Fec. de Emisión:";
            // 
            // MetodoEnvio
            // 
            // 
            // MetodoEnvio.NestedRadGridView
            // 
            this.MetodoEnvio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MetodoEnvio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoEnvio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MetodoEnvio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn6.FieldName = "Id";
            gridViewTextBoxColumn6.HeaderText = "Clave";
            gridViewTextBoxColumn6.Name = "Id";
            gridViewTextBoxColumn7.FieldName = "Descripcion";
            gridViewTextBoxColumn7.HeaderText = "Descripción";
            gridViewTextBoxColumn7.Name = "Descripcion";
            gridViewTextBoxColumn8.FieldName = "Descriptor";
            gridViewTextBoxColumn8.HeaderText = "Descriptor";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Descriptor";
            gridViewTextBoxColumn8.VisibleInColumnChooser = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.MetodoEnvio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.MetodoEnvio.EditorControl.Name = "NestedRadGridView";
            this.MetodoEnvio.EditorControl.ReadOnly = true;
            this.MetodoEnvio.EditorControl.ShowGroupPanel = false;
            this.MetodoEnvio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MetodoEnvio.EditorControl.TabIndex = 0;
            this.MetodoEnvio.Location = new System.Drawing.Point(575, 54);
            this.MetodoEnvio.Name = "MetodoEnvio";
            this.MetodoEnvio.NullText = "Envío";
            this.MetodoEnvio.Size = new System.Drawing.Size(187, 20);
            this.MetodoEnvio.TabIndex = 224;
            this.MetodoEnvio.TabStop = false;
            // 
            // Observaciones
            // 
            this.Observaciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Observaciones.Location = new System.Drawing.Point(852, 54);
            this.Observaciones.Name = "Observaciones";
            this.Observaciones.NullText = "Observaciones";
            this.Observaciones.Size = new System.Drawing.Size(335, 20);
            this.Observaciones.TabIndex = 194;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(768, 55);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(81, 18);
            this.radLabel3.TabIndex = 226;
            this.radLabel3.Text = "Observaciones:";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(511, 55);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(61, 18);
            this.radLabel6.TabIndex = 225;
            this.radLabel6.Text = "Mét. Envío:";
            // 
            // RadLabel18
            // 
            this.RadLabel18.Location = new System.Drawing.Point(787, 31);
            this.RadLabel18.Name = "RadLabel18";
            this.RadLabel18.Size = new System.Drawing.Size(54, 18);
            this.RadLabel18.TabIndex = 214;
            this.RadLabel18.Text = "Contacto:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(683, 5);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(227, 20);
            this.FechaEmision.TabIndex = 192;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // Contacto
            // 
            this.Contacto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Contacto.Location = new System.Drawing.Point(852, 30);
            this.Contacto.Name = "Contacto";
            this.Contacto.NullText = "Contacto";
            this.Contacto.Size = new System.Drawing.Size(335, 20);
            this.Contacto.TabIndex = 198;
            // 
            // FechaRequerida
            // 
            this.FechaRequerida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaRequerida.CustomFormat = "dd/MMMM/yyyy";
            this.FechaRequerida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaRequerida.Location = new System.Drawing.Point(1015, 5);
            this.FechaRequerida.Name = "FechaRequerida";
            this.FechaRequerida.NullText = "--/--/----";
            this.FechaRequerida.ReadOnly = true;
            this.FechaRequerida.Size = new System.Drawing.Size(172, 20);
            this.FechaRequerida.TabIndex = 219;
            this.FechaRequerida.TabStop = false;
            this.FechaRequerida.Text = "05/diciembre/2017";
            this.FechaRequerida.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(6, 55);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(59, 18);
            this.RadLabel13.TabIndex = 209;
            this.RadLabel13.Text = "Embarque:";
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel4.Location = new System.Drawing.Point(916, 6);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(97, 18);
            this.radLabel4.TabIndex = 221;
            this.radLabel4.Text = "Fec. de Requerida:";
            // 
            // Embarque
            // 
            this.Embarque.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.Embarque.AutoSizeDropDownToBestFit = true;
            this.Embarque.DisplayMember = "Domicilio";
            // 
            // Embarque.NestedRadGridView
            // 
            this.Embarque.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Embarque.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Embarque.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Embarque.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Embarque.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Embarque.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Embarque.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Domicilio";
            gridViewTextBoxColumn9.HeaderText = "Domicilio";
            gridViewTextBoxColumn9.Name = "Domicilio";
            this.Embarque.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9});
            this.Embarque.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Embarque.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Embarque.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.Embarque.EditorControl.Name = "NestedRadGridView";
            this.Embarque.EditorControl.ReadOnly = true;
            this.Embarque.EditorControl.ShowGroupPanel = false;
            this.Embarque.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Embarque.EditorControl.TabIndex = 0;
            this.Embarque.Location = new System.Drawing.Point(68, 54);
            this.Embarque.Name = "Embarque";
            this.Embarque.NullText = "Lugar de Embarque";
            this.Embarque.Size = new System.Drawing.Size(437, 20);
            this.Embarque.TabIndex = 203;
            this.Embarque.TabStop = false;
            this.Embarque.ValueMember = "Id";
            // 
            // SolicitudCotizacionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.RadCommandBar1);
            this.Name = "SolicitudCotizacionControl";
            this.Size = new System.Drawing.Size(1200, 110);
            this.Load += new System.EventHandler(this.SolicitudCotizacionControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaRequerida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBarComprobanteFiscal;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        private Telerik.WinControls.UI.CommandBarButton Cancelar;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelUuid;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEmisor;
        private Telerik.WinControls.UI.CommandBarDropDownList Status;
        private Telerik.WinControls.UI.CommandBarButton Nuevo;
        private Telerik.WinControls.UI.CommandBarButton Duplicar;
        private Telerik.WinControls.UI.CommandBarButton Guardar;
        private Telerik.WinControls.UI.CommandBarButton FilePDF;
        private Telerik.WinControls.UI.CommandBarButton Series;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        private Telerik.WinControls.UI.CommandBarTextBox IdDocumento;
        private Telerik.WinControls.UI.CommandBarButton Imprimir;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        public Contribuyentes.Forms.DirectorioControl Receptor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Documento;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadDropDownList Serie;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadLabel RadLabel7;
        internal Telerik.WinControls.UI.RadLabel RadLabel1;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MetodoEnvio;
        internal Telerik.WinControls.UI.RadTextBox Observaciones;
        internal Telerik.WinControls.UI.RadLabel radLabel3;
        internal Telerik.WinControls.UI.RadLabel radLabel6;
        internal Telerik.WinControls.UI.RadLabel RadLabel18;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadDropDownList Contacto;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaRequerida;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        internal Telerik.WinControls.UI.RadLabel radLabel4;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Embarque;
    }
}
