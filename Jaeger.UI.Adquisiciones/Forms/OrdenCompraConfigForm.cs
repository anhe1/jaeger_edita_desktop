﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class OrdenCompraConfigForm : RadForm {
        protected IOrdenCompraService service;
        private OrdenCompraConfig config;

        public OrdenCompraConfigForm() {
            InitializeComponent();
        }

        private void ViewOrdenCompraConfig_Load(object sender, EventArgs e) {
            this.service = new OrdenCompraService();
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.Text = "";
                espera.ShowDialog(this);
            }
        }

        private void ToolBar_ButtonGuardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.txbConcepto.DataBindings.Clear();
            this.txbConcepto.DataBindings.Add("Text", this.config, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbPiePagina.DataBindings.Clear();
            this.txbPiePagina.DataBindings.Add("Text", this.config, "LeyendaPiePagina", true, DataSourceUpdateMode.OnPropertyChanged);

            this.GridDireccion.DataSource = this.config.Domicilios;
        }

        private void Actualizar() {
            this.config = this.service.Configuracion;
            this.CreateBinding();
        }

        private void Guardar() {
            this.service.Save(this.config);
        }
    }
}
