﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class OrdenCompraControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrdenCompraControl));
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.general = new Jaeger.UI.Adquisiciones.Forms.OrdenCompraProveedorControl();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Relacionado = new Jaeger.UI.Adquisiciones.Forms.OrdenCompraRelacionadoControl();
            this.RadCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CommandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarComprobanteFiscal = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonEmisor = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.Separador = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.Status = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.FilePDF = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Series = new Telerik.WinControls.UI.CommandBarButton();
            this.CommandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolLabelUuid = new Telerik.WinControls.UI.CommandBarLabel();
            this.IdDocumento = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.OnLoad1 = new System.ComponentModel.BackgroundWorker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 30);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1442, 150);
            this.radPageView1.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).ItemAlignment = Telerik.WinControls.UI.StripViewItemAlignment.Near;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).ItemFitMode = Telerik.WinControls.UI.StripViewItemFitMode.Shrink;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.general);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(97F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1421, 102);
            this.radPageViewPage1.Text = "Datos Generales";
            // 
            // general
            // 
            this.general.Dock = System.Windows.Forms.DockStyle.Fill;
            this.general.Location = new System.Drawing.Point(0, 0);
            this.general.Margin = new System.Windows.Forms.Padding(4);
            this.general.MaximumSize = new System.Drawing.Size(0, 102);
            this.general.MinimumSize = new System.Drawing.Size(0, 102);
            this.general.Name = "general";
            this.general.ReadOnly = false;
            this.general.Size = new System.Drawing.Size(1421, 102);
            this.general.TabIndex = 1;
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.Relacionado);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(144F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(1421, 102);
            this.radPageViewPage2.Text = "Ord. Compra Relacionada";
            // 
            // Relacionado
            // 
            this.Relacionado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Relacionado.Editable = false;
            this.Relacionado.Location = new System.Drawing.Point(0, 0);
            this.Relacionado.Name = "Relacionado";
            this.Relacionado.Size = new System.Drawing.Size(1421, 102);
            this.Relacionado.TabIndex = 0;
            // 
            // RadCommandBar1
            // 
            this.RadCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.RadCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.RadCommandBar1.Name = "RadCommandBar1";
            this.RadCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.CommandBarRowElement1});
            this.RadCommandBar1.Size = new System.Drawing.Size(1442, 30);
            this.RadCommandBar1.TabIndex = 3;
            // 
            // CommandBarRowElement1
            // 
            this.CommandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.CommandBarRowElement1.Name = "CommandBarRowElement1";
            this.CommandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarComprobanteFiscal});
            this.CommandBarRowElement1.Text = "";
            // 
            // ToolBarComprobanteFiscal
            // 
            this.ToolBarComprobanteFiscal.DisplayName = "Emision de Comprobante";
            this.ToolBarComprobanteFiscal.EnableFocusBorder = false;
            this.ToolBarComprobanteFiscal.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonEmisor,
            this.Separador,
            this.ToolLabelStatus,
            this.Status,
            this.Nuevo,
            this.Duplicar,
            this.Guardar,
            this.Actualizar,
            this.Cancelar,
            this.Imprimir,
            this.FilePDF,
            this.Separator2,
            this.Series,
            this.CommandBarSeparator2,
            this.ToolLabelUuid,
            this.IdDocumento,
            this.Cerrar});
            this.ToolBarComprobanteFiscal.Name = "ToolBarComprobanteFiscal";
            // 
            // 
            // 
            this.ToolBarComprobanteFiscal.OverflowButton.Enabled = true;
            this.ToolBarComprobanteFiscal.ShowHorizontalLine = false;
            this.ToolBarComprobanteFiscal.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.ToolBarComprobanteFiscal.GetChildAt(2))).Enabled = true;
            // 
            // ToolBarButtonEmisor
            // 
            this.ToolBarButtonEmisor.DefaultItem = null;
            this.ToolBarButtonEmisor.DisplayName = "Emisor";
            this.ToolBarButtonEmisor.DrawText = true;
            this.ToolBarButtonEmisor.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.administrator_male_16px;
            this.ToolBarButtonEmisor.Name = "ToolBarButtonEmisor";
            this.ToolBarButtonEmisor.Text = "XXXXXXXXXXXXXX";
            this.ToolBarButtonEmisor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separador
            // 
            this.Separador.DisplayName = "Separador 2";
            this.Separador.Name = "Separador";
            this.Separador.VisibleInOverflowMenu = false;
            // 
            // ToolLabelStatus
            // 
            this.ToolLabelStatus.DisplayName = "Etiqueta Status";
            this.ToolLabelStatus.Name = "ToolLabelStatus";
            this.ToolLabelStatus.Text = "Status:";
            // 
            // Status
            // 
            this.Status.AutoCompleteDisplayMember = "Descripcion";
            this.Status.AutoCompleteValueMember = "Id";
            this.Status.DisplayMember = "Descripcion";
            this.Status.DisplayName = "Descripcion";
            this.Status.DrawImage = false;
            this.Status.DrawText = true;
            this.Status.DropDownAnimationEnabled = true;
            this.Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Status.Image = ((System.Drawing.Image)(resources.GetObject("Status.Image")));
            this.Status.MaxDropDownItems = 0;
            this.Status.Name = "Status";
            this.Status.Text = "";
            this.Status.ValueMember = "Id";
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Nuevo.Click += new System.EventHandler(this.Nuevo_Click);
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.documents_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Duplicar.Click += new System.EventHandler(this.Duplicar_Click);
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Enabled = false;
            this.Guardar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.save_16;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.refresh_16;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Actualizar.Click += new System.EventHandler(this.Actualizar_Click);
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Enabled = false;
            this.Cancelar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cancelar.Click += new System.EventHandler(this.Cancelar_Click);
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.Click += new System.EventHandler(this.Imprimir_Click);
            // 
            // FilePDF
            // 
            this.FilePDF.DisplayName = "Archivo PDF";
            this.FilePDF.DrawText = true;
            this.FilePDF.Enabled = false;
            this.FilePDF.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.export_pdf_16px;
            this.FilePDF.Name = "FilePDF";
            this.FilePDF.Text = "PDF";
            this.FilePDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.FilePDF.Click += new System.EventHandler(this.FilePDF_Click);
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "CommandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Series
            // 
            this.Series.DisplayName = "Series y Folios";
            this.Series.DrawText = true;
            this.Series.Enabled = false;
            this.Series.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.counter_16px;
            this.Series.Name = "Series";
            this.Series.Text = "Series y Folios";
            this.Series.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Series.Click += new System.EventHandler(this.Series_Click);
            // 
            // CommandBarSeparator2
            // 
            this.CommandBarSeparator2.DisplayName = "CommandBarSeparator2";
            this.CommandBarSeparator2.Name = "CommandBarSeparator2";
            this.CommandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // ToolLabelUuid
            // 
            this.ToolLabelUuid.DisplayName = "UUID";
            this.ToolLabelUuid.Name = "ToolLabelUuid";
            this.ToolLabelUuid.Text = "UUID:";
            this.ToolLabelUuid.VisibleInOverflowMenu = false;
            // 
            // IdDocumento
            // 
            this.IdDocumento.DisplayName = "IdDocumento";
            this.IdDocumento.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.IdDocumento.MinSize = new System.Drawing.Size(240, 22);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.Text = "";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // OnLoad1
            // 
            this.OnLoad1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnLoad1_DoWork);
            this.OnLoad1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnLoad1_RunWorkerCompleted);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // OrdenCompraControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.RadCommandBar1);
            this.MinimumSize = new System.Drawing.Size(1350, 180);
            this.Name = "OrdenCompraControl";
            this.Size = new System.Drawing.Size(1442, 180);
            this.Load += new System.EventHandler(this.OrdenCompraToolBarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        public OrdenCompraProveedorControl general;
        internal OrdenCompraRelacionadoControl Relacionado;
        internal Telerik.WinControls.UI.RadCommandBar RadCommandBar1;
        internal Telerik.WinControls.UI.CommandBarRowElement CommandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBarComprobanteFiscal;
        internal Telerik.WinControls.UI.CommandBarSeparator Separador;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelStatus;
        private Telerik.WinControls.UI.CommandBarButton Cancelar;
        internal Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarSeparator CommandBarSeparator2;
        internal Telerik.WinControls.UI.CommandBarLabel ToolLabelUuid;
        private System.ComponentModel.BackgroundWorker OnLoad1;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonEmisor;
        private Telerik.WinControls.UI.CommandBarDropDownList Status;
        private Telerik.WinControls.UI.CommandBarButton Nuevo;
        private Telerik.WinControls.UI.CommandBarButton Duplicar;
        private Telerik.WinControls.UI.CommandBarButton Guardar;
        private Telerik.WinControls.UI.CommandBarButton FilePDF;
        private Telerik.WinControls.UI.CommandBarButton Series;
        private Telerik.WinControls.UI.CommandBarButton Cerrar;
        internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        private Telerik.WinControls.UI.CommandBarTextBox IdDocumento;
        private Telerik.WinControls.UI.CommandBarButton Imprimir;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}
