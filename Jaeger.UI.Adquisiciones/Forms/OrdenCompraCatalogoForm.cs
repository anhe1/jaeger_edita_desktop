﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Adquisiciones.Services;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Adquisiciones.Forms {
    public class OrdenCompraCatalogoForm : OrdenCompraBaseForm {
        protected internal RadMenuItem Solicitud = new RadMenuItem { Text = "Solicitud de cotización", Name = "tadm_gprv_solcotiza" };
        public OrdenCompraCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.OrdenCompraCatalogoForm_Load;
        }

        private void OrdenCompraCatalogoForm_Load(object sender, EventArgs e) {
            this.GOrdenCompra.GridData.Columns.AddRange(GridOrdenCompraService.GetColumnsTotales());
            this.GOrdenCompra.ShowHerramientas = true;
            this.GOrdenCompra.Nuevo.Items.Add(Solicitud);

            this.Solicitud.Click += this.TSolicitud_Nuevo_Click;
        }

        public virtual void TSolicitud_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new SolicitudCotizacionForm() { MdiParent = this.ParentForm };
            nuevo.Show();
        }
    }
}
