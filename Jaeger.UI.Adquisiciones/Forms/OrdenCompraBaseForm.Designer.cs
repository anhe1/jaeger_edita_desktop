﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class OrdenCompraBaseForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GOrdenCompra = new Jaeger.UI.Common.Forms.GridCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GOrdenCompra
            // 
            this.GOrdenCompra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GOrdenCompra.Location = new System.Drawing.Point(0, 0);
            this.GOrdenCompra.Name = "GOrdenCompra";
            this.GOrdenCompra.ShowActualizar = true;
            this.GOrdenCompra.ShowAutosuma = false;
            this.GOrdenCompra.ShowCancelar = true;
            this.GOrdenCompra.ShowCerrar = true;
            this.GOrdenCompra.ShowEditar = true;
            this.GOrdenCompra.ShowEjercicio = true;
            this.GOrdenCompra.ShowExportarExcel = false;
            this.GOrdenCompra.ShowFiltro = true;
            this.GOrdenCompra.ShowHerramientas = false;
            this.GOrdenCompra.ShowImprimir = true;
            this.GOrdenCompra.ShowNuevo = true;
            this.GOrdenCompra.ShowPeriodo = true;
            this.GOrdenCompra.ShowSeleccionMultiple = true;
            this.GOrdenCompra.Size = new System.Drawing.Size(1197, 654);
            this.GOrdenCompra.TabIndex = 0;
            // 
            // OrdenCompraBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 654);
            this.Controls.Add(this.GOrdenCompra);
            this.Name = "OrdenCompraBaseForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Proveedor: Ordenes de Compra";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrdenCompraCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal Common.Forms.GridCommonControl GOrdenCompra;
    }
}
