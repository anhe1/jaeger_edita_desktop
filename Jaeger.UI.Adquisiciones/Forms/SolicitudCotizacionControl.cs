﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Aplication.Adquisiciones.Contracts;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class SolicitudCotizacionControl : UserControl {
        #region eventos
        private BackgroundWorker bgWorker = new BackgroundWorker();
        public event EventHandler<EventArgs> BindingCompleted;

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        protected ISolicitudCotizacionService Service;

        public SolicitudCotizacionDetailModel Comprobante;

        public SolicitudCotizacionControl() {
            InitializeComponent();
        }

        private void SolicitudCotizacionControl_Load(object sender, EventArgs e) {
            this.bgWorker.DoWork += this.BgWorker_DoWork;
            this.bgWorker.RunWorkerCompleted += this.BgWorker_RunWorkerCompleted;
            this.IdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.IdDocumento.TextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Center;

            this.MetodoEnvio.AutoSizeDropDownToBestFit = true;
            this.MetodoEnvio.DropDownStyle = RadDropDownStyle.DropDownList;
            this.MetodoEnvio.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowRowResize = false;
        }

        public virtual void Init() {
            this.Service = new SolicitudCotizacionService();
            this.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor;
            this.bgWorker.RunWorkerAsync();
        }

        #region  barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.Actualizar.PerformClick();
        }

        public virtual void Duplicar_Click(object sender, EventArgs e) {

        }

        public virtual void Guardar_Click(object sender, EventArgs e) {
            if (Comprobante.IdProveedor == 0) {
                RadMessageBox.Show(this, Properties.Resources.Message_ErrorSeleccionaProveedor, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            if (Comprobante.Conceptos.Count == 0) {
                RadMessageBox.Show(this, Properties.Resources.Message_ErrorNoConceptos, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            using (var espera = new Waiting1Form(this.SetGuardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Text = Properties.Resources.Message_ComprobanteNew;
                this.Comprobante = this.Service.GetNew();
            } else if (this.Comprobante.IdSolicitud > 0) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.Text = Properties.Resources.Message_ComprobanteGet;
                    espera.ShowDialog(this);
                    this.Text = string.Concat(this.Comprobante.Serie, " ", this.Comprobante.Folio);
                }
            }

            if (this.Comprobante.IsEditable) {
                this.Receptor.Init();
            }
            this.CreateBinding();
            this.OnBindingClompleted(e);
        }

        public virtual void Imprimir_Click(object sender, EventArgs e) {
            if (this.Comprobante.IdSolicitud > 0) {
                var _imprimir = new ReporteForm(new SolicitudCotizacionPrinter(this.Comprobante));
                _imprimir.Show();
            } else {
                RadMessageBox.Show(this, Properties.Resources.Message_ErrorComprobanteNoAutorizado, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }
        #endregion

        #region metodos privados
        public virtual void CreateBinding() {
            this.Status.DataBindings.Clear();
            this.Status.CommandBarDropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            this.Status.CommandBarDropDownListElement.ArrowButton.Visibility = ElementVisibility.Collapsed;
            this.Status.CommandBarDropDownListElement.ListElement.Visibility = ElementVisibility.Collapsed;
            this.Status.CommandBarDropDownListElement.PopupOpening += CommandBarDropDownListElement_PopupOpening;
            this.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdDocumento.DataBindings.Clear();
            //this.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Folio.DataBindings.Clear();
            this.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Serie.DataBindings.Clear();
            this.Serie.SetEditable(this.Comprobante.IsEditable);
            this.Serie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaRequerida.DataBindings.Clear();
            this.FechaRequerida.SetEditable(this.Comprobante.IsEditable);
            this.FechaRequerida.DataBindings.Add("Value", this.Comprobante, "FechaRequerida", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Nombre.DataBindings.Clear();
            this.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.RFC.DataBindings.Clear();
            this.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDirectorio.DataBindings.Clear();
            this.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdProveedor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contacto.DataBindings.Clear();
            this.Contacto.SetEditable(this.Comprobante.IsEditable);
            this.Contacto.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.MetodoEnvio.DataBindings.Clear();
            this.MetodoEnvio.SetEditable(this.Comprobante.IsEditable);
            this.MetodoEnvio.DataBindings.Add("Text", this.Comprobante, "MetodoEnvio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Observaciones.DataBindings.Clear();
            this.Observaciones.ReadOnly = !this.Comprobante.IsEditable;
            this.Observaciones.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Embarque.DataBindings.Clear();
            this.Embarque.SetEditable(this.Comprobante.IsEditable);
            this.Embarque.DataBindings.Add("Text", this.Comprobante, "EntregarEnText", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Receptor.ReadOnly = !this.Comprobante.IsEditable;
            this.Guardar.Enabled = this.Comprobante.IsEditable;
        }

        public virtual void CommandBarDropDownListElement_PopupOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        public virtual void GetComprobante() {
            this.Comprobante = this.Service.GetById(this.Comprobante.IdSolicitud);
        }

        public virtual void SetGuardar() {
            this.Comprobante = this.Service.Save(this.Comprobante);
            this.Actualizar.PerformClick();
        }
        #endregion
        private void BgWorker_DoWork(object sender, DoWorkEventArgs e) {
            this.MetodoEnvio.DisplayMember = "Descriptor";
            this.MetodoEnvio.DataSource = SolicitudCotizacionService.GetMetodoEnvio();
        }

        private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Embarque.DisplayMember = "Domicilio";
            this.Embarque.ValueMember = "Domicilio";
            this.Embarque.DataSource = this.Service.Configuracion.Domicilios;
            this.Embarque.SelectedValue = null;
            this.Embarque.SelectedIndex = -1;
            this.Actualizar.PerformClick();
        }
    }
}
