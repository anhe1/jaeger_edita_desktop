﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Adquisiciones.Services;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class SolicitudCotizacionesBaseForm : RadForm {
        protected internal RadMenuItem Crear = new RadMenuItem { Text = "Crear tablas" };
        protected internal ISolicitudCotizacionService Service;
        protected internal BindingList<SolicitudCotizacionSingleModel> data;
        protected internal GridViewTemplate GridConcepto = new GridViewTemplate() { Caption = "Conceptos" };

        public SolicitudCotizacionesBaseForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.GSolicitud.Permisos = new UIAction(menuElement.Permisos);
        }

        private void SolicitudCotizacionesForm_Load(object sender, EventArgs e) {
            this.GSolicitud.GridData.Columns.AddRange(GridSolicitudCotizacion.GetColumnsBase());
            this.GSolicitud.GridData.MasterTemplate.Templates.Add(this.GridConcepto);

            this.GridConcepto.Standard();
            this.GridConcepto.Columns.AddRange(GridSolicitudCotizacion.GetConceptos());
            this.GridConcepto.HierarchyDataProvider = new GridViewEventDataProvider(this.GridConcepto);
            this.GSolicitud.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);

            this.Service = new SolicitudCotizacionService();
            this.GSolicitud.Nuevo.Click += this.Nuevo_Click;
            this.GSolicitud.Editar.Click += this.Editar_Click;
            this.GSolicitud.Actualizar.Click += this.Actualizar_Click;
            this.GSolicitud.Imprimir.Click += this.Imprimir_Click;
            this.GSolicitud.Cerrar.Click += this.Cerrar_Click;
            this.GSolicitud.Herramientas.Items.Add(this.Crear);
            this.Crear.Click += this.Crear_Click;
        }

        #region barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new SolicitudCotizacionForm() { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        public virtual void Editar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetComprobante)) {
                espera.Text = "Complemento de pago ...";
                espera.ShowDialog(this);
            }
            var seleccionado = this.GSolicitud.GetCurrent<SolicitudCotizacionSingleModel>();
            if (seleccionado.Tag != null) {
                var rowView = (SolicitudCotizacionDetailModel)seleccionado.Tag;
                if (rowView != null) {
                    var nuevo = new SolicitudCotizacionForm(rowView) { MdiParent = this.ParentForm };
                    nuevo.Show();
                }
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetActualizar)) {
                espera.Text = "Actualizando datos ...";
                espera.ShowDialog(this);
            }
            this.GSolicitud.GridData.DataSource = this.data;
        }

        public virtual void Imprimir_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetComprobante)) {
                espera.ShowDialog(this);
            }
            var seleccionado = this.GSolicitud.GetCurrent<SolicitudCotizacionSingleModel>();
            if (seleccionado != null) {
                var comprobante = new SolicitudCotizacionPrinter((SolicitudCotizacionDetailModel)seleccionado.Tag);
                if (comprobante.IdSolicitud > 0) {
                    var _imprimir = new ReporteForm(new SolicitudCotizacionPrinter(comprobante));
                    _imprimir.Show();
                } else {
                    RadMessageBox.Show(this, Properties.Resources.Message_ErrorComprobanteNoAutorizado, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Crear_Click(object sender, EventArgs e) {
            this.Service.CrearTablas();
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.GridConcepto.Caption) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.Text = "Complemento de pago ...";
                    espera.ShowDialog(this);
                }
                var seleccionado = this.GSolicitud.GetCurrent<SolicitudCotizacionSingleModel>();
                if (seleccionado.Tag != null) {
                    var rowView = (SolicitudCotizacionDetailModel)seleccionado.Tag;
                    foreach (var item in rowView.Conceptos) {
                        GridViewRowInfo row = e.Template.Rows.NewRow();
                        row.Cells[GridTelerikCommon.ColCantidad.Name].Value = item.Cantidad;
                        row.Cells[GridSolicitudCotizacion.ColCboUnidad.Name].Value = item.IdUnidad;
                        row.Cells[GridSolicitudCotizacion.ColProducto.Name].Value = item.Producto;
                        row.Cells[GridSolicitudCotizacion.ColDescripcion.Name].Value = item.Descripcion;
                        row.Cells[GridSolicitudCotizacion.ColEspecificacion.Name].Value = item.Especificacion;
                        row.Cells[GridSolicitudCotizacion.ColMarca.Name].Value = item.Marca;
                        row.Cells[GridSolicitudCotizacion.ColNota.Name].Value = item.Nota;
                        e.SourceCollection.Add(row);
                    }
                }
            }
        }
        #endregion

        #region metodos virtuales
        public virtual void GetActualizar() {
            this.data = this.Service.GetList(this.GSolicitud.GetEjercicio(), this.GSolicitud.GetPeriodo());
        }

        public virtual void GetComprobante() {
            var seleccionado = this.GSolicitud.GetCurrent<SolicitudCotizacionSingleModel>();
            if (seleccionado != null) {
                seleccionado.Tag = this.Service.GetById(seleccionado.IdSolicitud);
            }
        }
        #endregion
    }
}
