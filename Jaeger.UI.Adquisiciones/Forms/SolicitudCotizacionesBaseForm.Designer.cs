﻿
namespace Jaeger.UI.Adquisiciones.Forms {
    partial class SolicitudCotizacionesBaseForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.GSolicitud = new Jaeger.UI.Common.Forms.GridCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GSolicitud
            // 
            this.GSolicitud.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GSolicitud.Location = new System.Drawing.Point(0, 0);
            this.GSolicitud.Name = "GSolicitud";
            this.GSolicitud.ShowActualizar = true;
            this.GSolicitud.ShowAutosuma = false;
            this.GSolicitud.ShowCancelar = false;
            this.GSolicitud.ShowCerrar = true;
            this.GSolicitud.ShowEditar = true;
            this.GSolicitud.ShowEjercicio = true;
            this.GSolicitud.ShowExportarExcel = false;
            this.GSolicitud.ShowFiltro = true;
            this.GSolicitud.ShowHerramientas = true;
            this.GSolicitud.ShowImprimir = true;
            this.GSolicitud.ShowNuevo = true;
            this.GSolicitud.ShowPeriodo = true;
            this.GSolicitud.ShowSeleccionMultiple = true;
            this.GSolicitud.Size = new System.Drawing.Size(800, 450);
            this.GSolicitud.TabIndex = 0;
            // 
            // SolicitudCotizacionesBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GSolicitud);
            this.Name = "SolicitudCotizacionesBaseForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "OC: Solicitud de Cotización";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SolicitudCotizacionesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.GridCommonControl GSolicitud;
    }
}