﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Telerik.WinControls;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Adquisiciones.Forms {
    /// <summary>
    /// formulario de orden de compra
    /// </summary>
    public partial class OrdenCompraForm : RadForm {
        public OrdenCompraForm() {
            InitializeComponent();
            this.TOrden.Comprobante = null;
        }

        public OrdenCompraForm(OrdenCompraDetailModel model) {
            InitializeComponent();
            this.TOrden.Comprobante = model;
        }

        private void OrdenCompraForm_Load(object sender, EventArgs e) {
            this.Preparar.RunWorkerAsync();
            this.gridConceptos.Standard();
            this.gridConceptos.AllowEditRow = true;
            this.gridConceptoImpuestos.Standard();
            this.gridConceptoImpuestos.AllowEditRow = true;
            this.gridValeAlmacen.Standard();

            this.TConcepto.Agregar.Click += this.TConceptos_Agregar_Click;
            this.TConcepto.Buscar.Click += this.TConceptos_Buscar_Click;
            this.TConcepto.Remover.Click += this.TConceptos_Remover_Click;
            this.TConcepto.Nuevo.Click += this.TConceptos_Nuevo_Click;
        }

        #region barra de herramientas
        private void TOrden_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TOrden_BindingCompleted(object sender, EventArgs e) {
            this.gridConceptos.DataSource = this.TOrden.Comprobante.Conceptos;
            this.gridConceptoImpuestos.DataSource = this.TOrden.Comprobante.Conceptos;
            this.gridConceptoImpuestos.DataMember = "Impuestos";

            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.TOrden.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.TOrden.Comprobante, "Descuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.TOrden.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.TOrden.Comprobante, "TrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIEPS.DataBindings.Clear();
            this.TrasladoIEPS.DataBindings.Add("Value", this.TOrden.Comprobante, "TrasladoIEPS", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RetencionIVA.DataBindings.Clear();
            this.RetencionIVA.DataBindings.Add("Value", this.TOrden.Comprobante, "RetencionIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.RetencionISR.DataBindings.Clear();
            this.RetencionISR.DataBindings.Add("Value", this.TOrden.Comprobante, "RetencionISR", true, DataSourceUpdateMode.OnPropertyChanged);

            this.gridConceptos.AllowEditRow = this.TOrden.Comprobante.IsEditable;
            this.gridConceptoImpuestos.AllowEditRow = this.TOrden.Comprobante.IsEditable;
            this.TConcepto.ReadOnly = this.TOrden.Comprobante.IsEditable;
            this.TImpuestos.ReadOnly = !this.TOrden.Comprobante.IsEditable;
        }

        private void TConceptos_Agregar_Click(object sender, EventArgs e) {
            
        }

        private void TConceptos_Nuevo_Click(object sender, EventArgs e) {
            this.TOrden.Comprobante.Conceptos.Add(new OrdenCompraConceptoDetailModel { Activo = true, Descripcion = "Nada" });
        }

        private void TConceptos_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.gridConceptos.CurrentRow.DataBoundItem as OrdenCompraConceptoDetailModel;
                    if (seleccionado.Id == 0) {
                        try {
                            this.TOrden.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.gridConceptos.Rows.Count == 0) {
                                this.TOrden.Comprobante.Conceptos = new BindingList<OrdenCompraConceptoDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.gridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        private void TConceptos_Buscar_Click(object sender, EventArgs e) {
            var _buscar = new Almacen.Forms.ProductoModeloBuscarForm(Domain.Base.ValueObjects.AlmacenEnum.MP);
            _buscar.Seleccionar += Buscar_Seleccionar;
            _buscar.ShowDialog(this);
        }

        private void TImpuestos_Nuevo_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                int index = this.gridConceptos.Rows.IndexOf(this.gridConceptos.CurrentRow);
                this.TOrden.Comprobante.Conceptos[index].Impuestos.Add(new OrdenCompraConceptoImpuesto() {
                    Base = this.TOrden.Comprobante.Conceptos[index].SubTotal,
                    Tipo = ImpuestoTipoEnum.Traslado,
                    Impuesto = ImpuestoEnum.IVA,
                    TipoFactor = ImpuestoTipoFactorEnum.Tasa,
                    TasaOCuota = new decimal(0.16)
                });
            }
        }

        private void TImpuestos_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptoImpuestos.CurrentRow != null) {
                var seleccionado = this.gridConceptoImpuestos.CurrentRow.DataBoundItem as OrdenCompraConceptoImpuesto;
                if (seleccionado != null) {
                    this.gridConceptoImpuestos.Rows.Remove(this.gridConceptoImpuestos.CurrentRow);
                }
            }
        }

        private void Buscar_Seleccionar(object sender, Domain.Almacen.Entities.ProductoServicioModeloModel e) {
            if (e != null) {
                var nuevo = new OrdenCompraConceptoDetailModel {
                    Activo = true,
                    IdAlmacen = e.IdAlmacen,
                    IdModelo = e.IdModelo,
                    IdProducto = e.IdProducto,
                    Creo = ConfigService.Piloto.Clave,
                    ClaveProdServ = e.ClaveProdServ,
                    ClaveUnidad = e.ClaveUnidad,
                    Descripcion = e.Descripcion,
                    NoIdentificacion = e.NoIdentificacion,
                    Marca = e.Marca,
                    Unidad = e.Unidad,
                    Especificacion = e.Especificacion,
                    IdUnidad = e.IdUnidad,
                    FechaNuevo = DateTime.Now,
                    Nombre = e.Nombre,
                    UnitarioBase = e.Unitario,
                    Unitario = e.Unitario
                };

                if (e.ValorTrasladoIVA != null) {
                    if (e.ValorTrasladoIVA > 0) {
                        nuevo.Impuestos.Add(new OrdenCompraConceptoImpuesto {
                            Base = nuevo.SubTotal,
                            Tipo = ImpuestoTipoEnum.Traslado,
                            Impuesto = ImpuestoEnum.IVA,
                            TipoFactor = ImpuestoTipoFactorEnum.Tasa,
                            TasaOCuota = (decimal)e.ValorTrasladoIVA
                        });
                    }
                }
                this.TOrden.Comprobante.Conceptos.Add(nuevo);
            }
        }
        #endregion

        #region acciones del grid
        private void GridConceptos_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.gridConceptos.CurrentRow.ViewInfo.ViewTemplate == this.gridConceptos.MasterTemplate) {
                    this.menuContextAplicar.Enabled = this.gridConceptos.CurrentColumn.Name == "ClaveUnidad" | this.gridConceptos.CurrentColumn.Name == "Unidad" | this.gridConceptos.CurrentColumn.Name == "NumPedido" | this.gridConceptos.CurrentColumn.Name == "Cantidad" | this.gridConceptos.CurrentColumn.Name == "ValorUnitario" | this.gridConceptos.CurrentColumn.Name == "ClaveProdServ";
                }
                e.ContextMenu = this.menuContextual.DropDown;
            }
        }
        #endregion

        #region menu contextual
        private void menuContextAplicar_Click(object sender, EventArgs e) {
            var d = this.gridConceptos.CurrentCell.Value;
            foreach (var item in this.gridConceptos.Rows) {
                item.Cells[this.gridConceptos.CurrentColumn.Index].Value = d;
            }
        }
        #endregion

        #region metodos privados
        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            this.TOrden.CreateService();
            var combo1 = this.gridConceptoImpuestos.Columns["Tipo"] as GridViewComboBoxColumn;
            combo1.DisplayMember = "Descripcion";
            combo1.ValueMember = "Id";
            combo1.DataSource = ConfigService.GetImpuestoTipo();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.TOrden.Actualizar.PerformClick();
        }
        #endregion
    }
}
