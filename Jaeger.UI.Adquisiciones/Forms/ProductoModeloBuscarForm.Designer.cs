﻿namespace Jaeger.UI.Adquisiciones.Forms
{
    partial class ProductoModeloBuscarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarTextBuscar = new Telerik.WinControls.UI.CommandBarTextBox();
            this.ToolBarButtonSeleccionar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolHostItem = new Telerik.WinControls.UI.CommandBarHostItem();
            this.ToolBarButtoCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.checkSoloExtencias = new Telerik.WinControls.UI.RadCheckBox();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSoloExtencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(806, 55);
            this.radCommandBar1.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            this.commandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.UseCompatibleTextRendering = false;
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabel,
            this.ToolBarTextBuscar,
            this.ToolBarButtonSeleccionar,
            this.ToolBarButtonFiltro,
            this.ToolHostItem,
            this.ToolBarButtoCerrar});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.UseCompatibleTextRendering = false;
            // 
            // ToolBarLabel
            // 
            this.ToolBarLabel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarLabel.Name = "ToolBarLabel";
            this.ToolBarLabel.Text = "Producto ó Servicio:";
            this.ToolBarLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarLabel.UseCompatibleTextRendering = false;
            // 
            // ToolBarTextBuscar
            // 
            this.ToolBarTextBuscar.DisplayName = "Buscar";
            this.ToolBarTextBuscar.MaxSize = new System.Drawing.Size(295, 20);
            this.ToolBarTextBuscar.MinSize = new System.Drawing.Size(294, 20);
            this.ToolBarTextBuscar.Name = "ToolBarTextBuscar";
            this.ToolBarTextBuscar.Text = "";
            // 
            // ToolBarButtonSeleccionar
            // 
            this.ToolBarButtonSeleccionar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonSeleccionar.DisplayName = "Seleccionar";
            this.ToolBarButtonSeleccionar.DrawText = true;
            this.ToolBarButtonSeleccionar.Name = "ToolBarButtonSeleccionar";
            this.ToolBarButtonSeleccionar.Text = "Seleccionar";
            this.ToolBarButtonSeleccionar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonSeleccionar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtonSeleccionar.UseCompatibleTextRendering = false;
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolHostItem
            // 
            this.ToolHostItem.DisplayName = "commandBarHostItem1";
            this.ToolHostItem.Enabled = false;
            this.ToolHostItem.Name = "ToolHostItem";
            this.ToolHostItem.Text = "";
            // 
            // ToolBarButtoCerrar
            // 
            this.ToolBarButtoCerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtoCerrar.DisplayName = "Cerrar";
            this.ToolBarButtoCerrar.DrawText = true;
            this.ToolBarButtoCerrar.MinSize = new System.Drawing.Size(0, 0);
            this.ToolBarButtoCerrar.Name = "ToolBarButtoCerrar";
            this.ToolBarButtoCerrar.Text = "Cerrar";
            this.ToolBarButtoCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtoCerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ToolBarButtoCerrar.UseCompatibleTextRendering = false;
            // 
            // checkSoloExtencias
            // 
            this.checkSoloExtencias.Location = new System.Drawing.Point(631, 64);
            this.checkSoloExtencias.Name = "checkSoloExtencias";
            this.checkSoloExtencias.Size = new System.Drawing.Size(99, 18);
            this.checkSoloExtencias.TabIndex = 2;
            this.checkSoloExtencias.Text = "Solo existencias";
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.checkSoloExtencias);
            this.GridData.Controls.Add(this.Espera);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 55);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "SubId";
            gridViewTextBoxColumn2.HeaderText = "SubId";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "SubId";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "Producto";
            gridViewTextBoxColumn3.HeaderText = "Producto";
            gridViewTextBoxColumn3.Name = "Producto";
            gridViewTextBoxColumn3.Width = 200;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn4.Width = 180;
            gridViewTextBoxColumn5.FieldName = "Especificacion";
            gridViewTextBoxColumn5.HeaderText = "Especificacion";
            gridViewTextBoxColumn5.Name = "Especificacion";
            gridViewTextBoxColumn5.Width = 180;
            gridViewTextBoxColumn6.FieldName = "Marca";
            gridViewTextBoxColumn6.HeaderText = "Marca";
            gridViewTextBoxColumn6.Name = "Marca";
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "Unitario";
            gridViewTextBoxColumn7.FormatString = "{0:N2}";
            gridViewTextBoxColumn7.HeaderText = "Unitario";
            gridViewTextBoxColumn7.Name = "Unitario";
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "Largo";
            gridViewTextBoxColumn8.HeaderText = "Largo";
            gridViewTextBoxColumn8.Name = "Largo";
            gridViewTextBoxColumn9.FieldName = "Ancho";
            gridViewTextBoxColumn9.HeaderText = "Ancho";
            gridViewTextBoxColumn9.Name = "Ancho";
            gridViewTextBoxColumn10.FieldName = "Alto";
            gridViewTextBoxColumn10.HeaderText = "Alto";
            gridViewTextBoxColumn10.Name = "Alto";
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(806, 296);
            this.GridData.TabIndex = 4;
            // 
            // Espera
            // 
            this.Espera.AssociatedControl = this.GridData;
            this.Espera.Location = new System.Drawing.Point(361, 124);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(130, 24);
            this.Espera.TabIndex = 1;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // ProductoModeloBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 351);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProductoModeloBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Materia Prima Buscar";
            this.Load += new System.EventHandler(this.MateriaPrimaBuscar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSoloExtencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabel;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonSeleccionar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtoCerrar;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarTextBox ToolBarTextBuscar;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarHostItem ToolHostItem;
        private Telerik.WinControls.UI.RadCheckBox checkSoloExtencias;
    }
}
