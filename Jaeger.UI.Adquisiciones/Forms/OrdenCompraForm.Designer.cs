﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class OrdenCompraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn2 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Contenedor = new Telerik.WinControls.UI.RadSplitContainer();
            this.PanelConceptos = new Telerik.WinControls.UI.SplitPanel();
            this.gridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.TConcepto = new Jaeger.UI.Adquisiciones.Forms.TbProductoServicioBuscarControl();
            this.PanelTotales = new Telerik.WinControls.UI.SplitPanel();
            this.PageViewConceptoParte = new Telerik.WinControls.UI.RadPageView();
            this.ViewPageConceptoImpuestos = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridConceptoImpuestos = new Telerik.WinControls.UI.RadGridView();
            this.TImpuestos = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.ViewPageValesEntrada = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridValeAlmacen = new Telerik.WinControls.UI.RadGridView();
            this.toolBarStandarControl2 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Total = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionISR = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Descuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RetencionIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SubTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.TrasladoIEPS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.TrasladoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            this.menuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.menuContextAplicar = new Telerik.WinControls.UI.RadMenuItem();
            this.TOrden = new Jaeger.UI.Adquisiciones.Forms.OrdenCompraControl();
            ((System.ComponentModel.ISupportInitialize)(this.Contenedor)).BeginInit();
            this.Contenedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelConceptos)).BeginInit();
            this.PanelConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).BeginInit();
            this.PanelTotales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageViewConceptoParte)).BeginInit();
            this.PageViewConceptoParte.SuspendLayout();
            this.ViewPageConceptoImpuestos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoImpuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoImpuestos.MasterTemplate)).BeginInit();
            this.ViewPageValesEntrada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridValeAlmacen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridValeAlmacen.MasterTemplate)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Controls.Add(this.PanelConceptos);
            this.Contenedor.Controls.Add(this.PanelTotales);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 180);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.Contenedor.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Contenedor.Size = new System.Drawing.Size(1402, 522);
            this.Contenedor.TabIndex = 3;
            this.Contenedor.TabStop = false;
            // 
            // PanelConceptos
            // 
            this.PanelConceptos.Controls.Add(this.gridConceptos);
            this.PanelConceptos.Controls.Add(this.TConcepto);
            this.PanelConceptos.Location = new System.Drawing.Point(0, 0);
            this.PanelConceptos.Name = "PanelConceptos";
            // 
            // 
            // 
            this.PanelConceptos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.PanelConceptos.Size = new System.Drawing.Size(1402, 338);
            this.PanelConceptos.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1745027F);
            this.PanelConceptos.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 96);
            this.PanelConceptos.TabIndex = 0;
            this.PanelConceptos.TabStop = false;
            this.PanelConceptos.Text = "splitPanel1";
            // 
            // gridConceptos
            // 
            this.gridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn33.DataType = typeof(int);
            gridViewTextBoxColumn33.FieldName = "Id";
            gridViewTextBoxColumn33.HeaderText = "Id";
            gridViewTextBoxColumn33.IsVisible = false;
            gridViewTextBoxColumn33.Name = "Id";
            gridViewTextBoxColumn33.ReadOnly = true;
            gridViewTextBoxColumn33.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.ReadOnly = true;
            gridViewCheckBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn34.DataType = typeof(int);
            gridViewTextBoxColumn34.FieldName = "SubId";
            gridViewTextBoxColumn34.HeaderText = "SubId";
            gridViewTextBoxColumn34.IsVisible = false;
            gridViewTextBoxColumn34.Name = "SubId";
            gridViewTextBoxColumn34.ReadOnly = true;
            gridViewTextBoxColumn34.VisibleInColumnChooser = false;
            gridViewTextBoxColumn35.FieldName = "Almacen";
            gridViewTextBoxColumn35.HeaderText = "Almacén";
            gridViewTextBoxColumn35.IsVisible = false;
            gridViewTextBoxColumn35.Name = "Almacen";
            gridViewTextBoxColumn35.ReadOnly = true;
            gridViewTextBoxColumn35.VisibleInColumnChooser = false;
            gridViewTextBoxColumn36.FieldName = "Tipo";
            gridViewTextBoxColumn36.HeaderText = "Tipo";
            gridViewTextBoxColumn36.IsVisible = false;
            gridViewTextBoxColumn36.Name = "Tipo";
            gridViewTextBoxColumn36.ReadOnly = true;
            gridViewTextBoxColumn36.VisibleInColumnChooser = false;
            gridViewTextBoxColumn37.DataType = typeof(int);
            gridViewTextBoxColumn37.FieldName = "IdProducto";
            gridViewTextBoxColumn37.HeaderText = "IdProducto";
            gridViewTextBoxColumn37.IsVisible = false;
            gridViewTextBoxColumn37.Name = "IdProducto";
            gridViewTextBoxColumn37.ReadOnly = true;
            gridViewTextBoxColumn37.VisibleInColumnChooser = false;
            gridViewTextBoxColumn38.DataType = typeof(int);
            gridViewTextBoxColumn38.FieldName = "IdModelo";
            gridViewTextBoxColumn38.HeaderText = "IdModelo";
            gridViewTextBoxColumn38.IsVisible = false;
            gridViewTextBoxColumn38.Name = "IdModelo";
            gridViewTextBoxColumn38.ReadOnly = true;
            gridViewTextBoxColumn38.VisibleInColumnChooser = false;
            gridViewTextBoxColumn39.DataType = typeof(int);
            gridViewTextBoxColumn39.FieldName = "IdUnidad";
            gridViewTextBoxColumn39.HeaderText = "IdUnidad";
            gridViewTextBoxColumn39.IsVisible = false;
            gridViewTextBoxColumn39.Name = "IdUnidad";
            gridViewTextBoxColumn39.ReadOnly = true;
            gridViewTextBoxColumn39.VisibleInColumnChooser = false;
            gridViewTextBoxColumn39.Width = 85;
            gridViewTextBoxColumn40.DataType = typeof(int);
            gridViewTextBoxColumn40.FieldName = "OrdenProduccion";
            gridViewTextBoxColumn40.HeaderText = "O. P";
            gridViewTextBoxColumn40.Name = "OrdenProduccion";
            gridViewTextBoxColumn41.DataType = typeof(decimal);
            gridViewTextBoxColumn41.FieldName = "Cantidad";
            gridViewTextBoxColumn41.FormatString = "{0:n}";
            gridViewTextBoxColumn41.HeaderText = "Cantidad";
            gridViewTextBoxColumn41.Name = "Cantidad";
            gridViewTextBoxColumn41.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn41.Width = 85;
            gridViewTextBoxColumn42.FieldName = "Unidad";
            gridViewTextBoxColumn42.HeaderText = "Unidad";
            gridViewTextBoxColumn42.Name = "Unidad";
            gridViewTextBoxColumn42.ReadOnly = true;
            gridViewTextBoxColumn42.Width = 85;
            gridViewTextBoxColumn43.FieldName = "Nombre";
            gridViewTextBoxColumn43.HeaderText = "Nombre";
            gridViewTextBoxColumn43.Name = "Nombre";
            gridViewTextBoxColumn43.ReadOnly = true;
            gridViewTextBoxColumn43.Width = 200;
            gridViewTextBoxColumn44.FieldName = "Descripcion";
            gridViewTextBoxColumn44.HeaderText = "Descripción";
            gridViewTextBoxColumn44.Name = "Descripcion";
            gridViewTextBoxColumn44.ReadOnly = true;
            gridViewTextBoxColumn44.Width = 200;
            gridViewTextBoxColumn45.FieldName = "Especificacion";
            gridViewTextBoxColumn45.HeaderText = "Especificación";
            gridViewTextBoxColumn45.Name = "Especificacion";
            gridViewTextBoxColumn45.ReadOnly = true;
            gridViewTextBoxColumn45.Width = 100;
            gridViewTextBoxColumn46.FieldName = "Marca";
            gridViewTextBoxColumn46.HeaderText = "Marca";
            gridViewTextBoxColumn46.Name = "Marca";
            gridViewTextBoxColumn46.ReadOnly = true;
            gridViewTextBoxColumn46.Width = 100;
            gridViewTextBoxColumn47.FieldName = "Nota";
            gridViewTextBoxColumn47.HeaderText = "Observaciones";
            gridViewTextBoxColumn47.MaxLength = 128;
            gridViewTextBoxColumn47.Name = "Nora";
            gridViewTextBoxColumn47.Width = 150;
            gridViewTextBoxColumn48.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn48.HeaderText = "NoIdentificacion";
            gridViewTextBoxColumn48.Name = "NoIdentificacion";
            gridViewTextBoxColumn48.ReadOnly = true;
            gridViewTextBoxColumn48.Width = 100;
            gridViewTextBoxColumn49.DataType = typeof(decimal);
            gridViewTextBoxColumn49.FieldName = "UnitarioBase";
            gridViewTextBoxColumn49.FormatString = "{0:n}";
            gridViewTextBoxColumn49.HeaderText = "Unit. Auto";
            gridViewTextBoxColumn49.Name = "UnitarioBase";
            gridViewTextBoxColumn49.ReadOnly = true;
            gridViewTextBoxColumn49.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn49.Width = 85;
            gridViewCalculatorColumn2.FieldName = "Unitario";
            gridViewCalculatorColumn2.FormatString = "{0:n}";
            gridViewCalculatorColumn2.HeaderText = "Unitario";
            gridViewCalculatorColumn2.Name = "Unitario";
            gridViewCalculatorColumn2.Width = 85;
            gridViewTextBoxColumn50.DataType = typeof(decimal);
            gridViewTextBoxColumn50.FieldName = "Importe";
            gridViewTextBoxColumn50.FormatString = "{0:n}";
            gridViewTextBoxColumn50.HeaderText = "Importe";
            gridViewTextBoxColumn50.IsVisible = false;
            gridViewTextBoxColumn50.Name = "Importe";
            gridViewTextBoxColumn50.ReadOnly = true;
            gridViewTextBoxColumn50.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn50.Width = 85;
            gridViewTextBoxColumn51.DataType = typeof(decimal);
            gridViewTextBoxColumn51.FieldName = "Descuento";
            gridViewTextBoxColumn51.FormatString = "{0:n}";
            gridViewTextBoxColumn51.HeaderText = "Descuento";
            gridViewTextBoxColumn51.Name = "Descuento";
            gridViewTextBoxColumn51.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn51.Width = 85;
            gridViewTextBoxColumn52.DataType = typeof(decimal);
            gridViewTextBoxColumn52.FieldName = "SubTotal";
            gridViewTextBoxColumn52.FormatString = "{0:n}";
            gridViewTextBoxColumn52.HeaderText = "SubTotal";
            gridViewTextBoxColumn52.Name = "SubTotal";
            gridViewTextBoxColumn52.ReadOnly = true;
            gridViewTextBoxColumn52.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn52.Width = 85;
            this.gridConceptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn33,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46,
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48,
            gridViewTextBoxColumn49,
            gridViewCalculatorColumn2,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52});
            this.gridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.gridConceptos.Name = "gridConceptos";
            this.gridConceptos.ShowGroupPanel = false;
            this.gridConceptos.Size = new System.Drawing.Size(1402, 308);
            this.gridConceptos.TabIndex = 0;
            this.gridConceptos.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridConceptos_ContextMenuOpening);
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Location = new System.Drawing.Point(0, 0);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ReadOnly = false;
            this.TConcepto.Size = new System.Drawing.Size(1402, 30);
            this.TConcepto.TabIndex = 1;
            // 
            // PanelTotales
            // 
            this.PanelTotales.Controls.Add(this.PageViewConceptoParte);
            this.PanelTotales.Controls.Add(this.panel2);
            this.PanelTotales.Location = new System.Drawing.Point(0, 342);
            this.PanelTotales.Name = "PanelTotales";
            // 
            // 
            // 
            this.PanelTotales.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.PanelTotales.Size = new System.Drawing.Size(1402, 180);
            this.PanelTotales.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 180);
            this.PanelTotales.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1745027F);
            this.PanelTotales.SizeInfo.MinimumSize = new System.Drawing.Size(0, 180);
            this.PanelTotales.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -96);
            this.PanelTotales.TabIndex = 1;
            this.PanelTotales.TabStop = false;
            this.PanelTotales.Text = "splitPanel2";
            // 
            // PageViewConceptoParte
            // 
            this.PageViewConceptoParte.Controls.Add(this.ViewPageConceptoImpuestos);
            this.PageViewConceptoParte.Controls.Add(this.ViewPageValesEntrada);
            this.PageViewConceptoParte.DefaultPage = this.ViewPageConceptoImpuestos;
            this.PageViewConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageViewConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.PageViewConceptoParte.Name = "PageViewConceptoParte";
            this.PageViewConceptoParte.SelectedPage = this.ViewPageConceptoImpuestos;
            this.PageViewConceptoParte.Size = new System.Drawing.Size(1167, 180);
            this.PageViewConceptoParte.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageViewConceptoParte.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // ViewPageConceptoImpuestos
            // 
            this.ViewPageConceptoImpuestos.Controls.Add(this.gridConceptoImpuestos);
            this.ViewPageConceptoImpuestos.Controls.Add(this.TImpuestos);
            this.ViewPageConceptoImpuestos.ItemSize = new System.Drawing.SizeF(122F, 28F);
            this.ViewPageConceptoImpuestos.Location = new System.Drawing.Point(10, 37);
            this.ViewPageConceptoImpuestos.Name = "ViewPageConceptoImpuestos";
            this.ViewPageConceptoImpuestos.Size = new System.Drawing.Size(1146, 132);
            this.ViewPageConceptoImpuestos.Text = "Concepto: Impuestos";
            // 
            // gridConceptoImpuestos
            // 
            this.gridConceptoImpuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptoImpuestos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridConceptoImpuestos.MasterTemplate.AllowAddNewRow = false;
            gridViewComboBoxColumn4.FieldName = "Tipo";
            gridViewComboBoxColumn4.HeaderText = "Tipo";
            gridViewComboBoxColumn4.Name = "Tipo";
            gridViewComboBoxColumn4.Width = 80;
            gridViewComboBoxColumn5.FieldName = "Impuesto";
            gridViewComboBoxColumn5.HeaderText = "Impuesto";
            gridViewComboBoxColumn5.Name = "Impuesto";
            gridViewComboBoxColumn5.Width = 80;
            gridViewTextBoxColumn53.DataType = typeof(decimal);
            gridViewTextBoxColumn53.FieldName = "Base";
            gridViewTextBoxColumn53.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn53.HeaderText = "Base";
            gridViewTextBoxColumn53.Name = "Base";
            gridViewTextBoxColumn53.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn53.Width = 80;
            gridViewComboBoxColumn6.FieldName = "TipoFactor";
            gridViewComboBoxColumn6.HeaderText = "Tipo Factor";
            gridViewComboBoxColumn6.Name = "TipoFactor";
            gridViewComboBoxColumn6.Width = 80;
            gridViewTextBoxColumn54.DataType = typeof(decimal);
            gridViewTextBoxColumn54.FieldName = "TasaOCuota";
            gridViewTextBoxColumn54.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn54.HeaderText = "Tasa ó Cuota";
            gridViewTextBoxColumn54.Name = "TasaOCuota";
            gridViewTextBoxColumn54.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn54.Width = 80;
            gridViewTextBoxColumn55.DataType = typeof(decimal);
            gridViewTextBoxColumn55.FieldName = "Importe";
            gridViewTextBoxColumn55.FormatString = "{0:#,###0.0000}";
            gridViewTextBoxColumn55.HeaderText = "Importe";
            gridViewTextBoxColumn55.Name = "Importe";
            gridViewTextBoxColumn55.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn55.Width = 80;
            this.gridConceptoImpuestos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn4,
            gridViewComboBoxColumn5,
            gridViewTextBoxColumn53,
            gridViewComboBoxColumn6,
            gridViewTextBoxColumn54,
            gridViewTextBoxColumn55});
            this.gridConceptoImpuestos.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.gridConceptoImpuestos.Name = "gridConceptoImpuestos";
            this.gridConceptoImpuestos.ShowGroupPanel = false;
            this.gridConceptoImpuestos.Size = new System.Drawing.Size(1146, 102);
            this.gridConceptoImpuestos.TabIndex = 4;
            // 
            // TImpuestos
            // 
            this.TImpuestos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TImpuestos.Etiqueta = "";
            this.TImpuestos.Location = new System.Drawing.Point(0, 0);
            this.TImpuestos.Name = "TImpuestos";
            this.TImpuestos.ReadOnly = false;
            this.TImpuestos.ShowActualizar = false;
            this.TImpuestos.ShowAutorizar = false;
            this.TImpuestos.ShowCerrar = false;
            this.TImpuestos.ShowEditar = false;
            this.TImpuestos.ShowExportarExcel = false;
            this.TImpuestos.ShowFiltro = true;
            this.TImpuestos.ShowGuardar = false;
            this.TImpuestos.ShowHerramientas = false;
            this.TImpuestos.ShowImagen = false;
            this.TImpuestos.ShowImprimir = false;
            this.TImpuestos.ShowNuevo = true;
            this.TImpuestos.ShowRemover = true;
            this.TImpuestos.Size = new System.Drawing.Size(1146, 30);
            this.TImpuestos.TabIndex = 3;
            this.TImpuestos.Nuevo.Click += this.TImpuestos_Nuevo_Click;
            this.TImpuestos.Remover.Click += this.TImpuestos_Remover_Click;
            // 
            // ViewPageValesEntrada
            // 
            this.ViewPageValesEntrada.Controls.Add(this.gridValeAlmacen);
            this.ViewPageValesEntrada.Controls.Add(this.toolBarStandarControl2);
            this.ViewPageValesEntrada.ItemSize = new System.Drawing.SizeF(91F, 28F);
            this.ViewPageValesEntrada.Location = new System.Drawing.Point(10, 37);
            this.ViewPageValesEntrada.Name = "ViewPageValesEntrada";
            this.ViewPageValesEntrada.Size = new System.Drawing.Size(1146, 132);
            this.ViewPageValesEntrada.Text = "Almacén: Vales";
            // 
            // gridValeAlmacen
            // 
            this.gridValeAlmacen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridValeAlmacen.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn56.DataType = typeof(int);
            gridViewTextBoxColumn56.FieldName = "Id";
            gridViewTextBoxColumn56.HeaderText = "Id";
            gridViewTextBoxColumn56.IsVisible = false;
            gridViewTextBoxColumn56.Name = "Id";
            gridViewTextBoxColumn56.VisibleInColumnChooser = false;
            gridViewTextBoxColumn57.FieldName = "EfectoText";
            gridViewTextBoxColumn57.HeaderText = "Mov.";
            gridViewTextBoxColumn57.Name = "EfectoText";
            gridViewTextBoxColumn57.Width = 85;
            gridViewTextBoxColumn58.DataType = typeof(int);
            gridViewTextBoxColumn58.FieldName = "Folio";
            gridViewTextBoxColumn58.FormatString = "{0:0000#}";
            gridViewTextBoxColumn58.HeaderText = "Folio";
            gridViewTextBoxColumn58.Name = "Folio";
            gridViewTextBoxColumn58.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn58.Width = 85;
            gridViewTextBoxColumn59.FieldName = "Serie";
            gridViewTextBoxColumn59.HeaderText = "Serie";
            gridViewTextBoxColumn59.Name = "Serie";
            gridViewTextBoxColumn59.Width = 85;
            gridViewTextBoxColumn60.FieldName = "FechaEmision";
            gridViewTextBoxColumn60.FormatString = "{0:d}";
            gridViewTextBoxColumn60.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn60.Name = "FechaEmision";
            gridViewTextBoxColumn60.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn60.Width = 85;
            gridViewTextBoxColumn61.FieldName = "OrdenCompra";
            gridViewTextBoxColumn61.FormatString = "{0:0000#}";
            gridViewTextBoxColumn61.HeaderText = "O. C.";
            gridViewTextBoxColumn61.Name = "OrdenCompra";
            gridViewTextBoxColumn61.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn61.Width = 85;
            gridViewTextBoxColumn62.FieldName = "Receptor";
            gridViewTextBoxColumn62.HeaderText = "Receptor";
            gridViewTextBoxColumn62.Name = "Receptor";
            gridViewTextBoxColumn62.Width = 250;
            gridViewTextBoxColumn63.FieldName = "Contacto";
            gridViewTextBoxColumn63.HeaderText = "Contacto";
            gridViewTextBoxColumn63.Name = "Contacto";
            gridViewTextBoxColumn63.Width = 200;
            gridViewTextBoxColumn64.FieldName = "Cancela";
            gridViewTextBoxColumn64.HeaderText = "Cancela";
            gridViewTextBoxColumn64.Name = "Cancela";
            gridViewTextBoxColumn64.Width = 85;
            this.gridValeAlmacen.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64});
            this.gridValeAlmacen.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.gridValeAlmacen.Name = "gridValeAlmacen";
            this.gridValeAlmacen.ShowGroupPanel = false;
            this.gridValeAlmacen.Size = new System.Drawing.Size(1146, 102);
            this.gridValeAlmacen.TabIndex = 1;
            // 
            // toolBarStandarControl2
            // 
            this.toolBarStandarControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl2.Etiqueta = "";
            this.toolBarStandarControl2.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl2.Name = "toolBarStandarControl2";
            this.toolBarStandarControl2.ReadOnly = false;
            this.toolBarStandarControl2.ShowActualizar = false;
            this.toolBarStandarControl2.ShowAutorizar = false;
            this.toolBarStandarControl2.ShowCerrar = false;
            this.toolBarStandarControl2.ShowEditar = false;
            this.toolBarStandarControl2.ShowExportarExcel = false;
            this.toolBarStandarControl2.ShowFiltro = true;
            this.toolBarStandarControl2.ShowGuardar = false;
            this.toolBarStandarControl2.ShowHerramientas = false;
            this.toolBarStandarControl2.ShowImagen = false;
            this.toolBarStandarControl2.ShowImprimir = false;
            this.toolBarStandarControl2.ShowNuevo = true;
            this.toolBarStandarControl2.ShowRemover = true;
            this.toolBarStandarControl2.Size = new System.Drawing.Size(1146, 30);
            this.toolBarStandarControl2.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Total);
            this.panel2.Controls.Add(this.RetencionISR);
            this.panel2.Controls.Add(this.Descuento);
            this.panel2.Controls.Add(this.RetencionIVA);
            this.panel2.Controls.Add(this.SubTotal);
            this.panel2.Controls.Add(this.TrasladoIEPS);
            this.panel2.Controls.Add(this.RadLabel28);
            this.panel2.Controls.Add(this.TrasladoIVA);
            this.panel2.Controls.Add(this.RadLabel31);
            this.panel2.Controls.Add(this.RadLabel24);
            this.panel2.Controls.Add(this.RadLabel27);
            this.panel2.Controls.Add(this.RadLabel30);
            this.panel2.Controls.Add(this.RadLabel26);
            this.panel2.Controls.Add(this.RadLabel29);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1167, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(235, 180);
            this.panel2.TabIndex = 3;
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(95, 154);
            this.Total.Mask = "n2";
            this.Total.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Total.Name = "Total";
            this.Total.NullText = "SubTotal";
            this.Total.Size = new System.Drawing.Size(125, 20);
            this.Total.TabIndex = 189;
            this.Total.TabStop = false;
            this.Total.Text = "0.00";
            this.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionISR
            // 
            this.RetencionISR.Location = new System.Drawing.Point(95, 130);
            this.RetencionISR.Mask = "n2";
            this.RetencionISR.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionISR.Name = "RetencionISR";
            this.RetencionISR.NullText = "SubTotal";
            this.RetencionISR.Size = new System.Drawing.Size(125, 20);
            this.RetencionISR.TabIndex = 188;
            this.RetencionISR.TabStop = false;
            this.RetencionISR.Text = "0.00";
            this.RetencionISR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Descuento
            // 
            this.Descuento.Location = new System.Drawing.Point(96, 34);
            this.Descuento.Mask = "n2";
            this.Descuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Descuento.Name = "Descuento";
            this.Descuento.NullText = "SubTotal";
            this.Descuento.Size = new System.Drawing.Size(125, 20);
            this.Descuento.TabIndex = 184;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "0.00";
            this.Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RetencionIVA
            // 
            this.RetencionIVA.Location = new System.Drawing.Point(96, 106);
            this.RetencionIVA.Mask = "n2";
            this.RetencionIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.RetencionIVA.Name = "RetencionIVA";
            this.RetencionIVA.NullText = "SubTotal";
            this.RetencionIVA.Size = new System.Drawing.Size(125, 20);
            this.RetencionIVA.TabIndex = 187;
            this.RetencionIVA.TabStop = false;
            this.RetencionIVA.Text = "0.00";
            this.RetencionIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SubTotal
            // 
            this.SubTotal.Location = new System.Drawing.Point(96, 10);
            this.SubTotal.Mask = "n2";
            this.SubTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.NullText = "SubTotal";
            this.SubTotal.Size = new System.Drawing.Size(125, 20);
            this.SubTotal.TabIndex = 183;
            this.SubTotal.TabStop = false;
            this.SubTotal.Text = "0.00";
            this.SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TrasladoIEPS
            // 
            this.TrasladoIEPS.Location = new System.Drawing.Point(96, 82);
            this.TrasladoIEPS.Mask = "n2";
            this.TrasladoIEPS.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIEPS.Name = "TrasladoIEPS";
            this.TrasladoIEPS.NullText = "SubTotal";
            this.TrasladoIEPS.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIEPS.TabIndex = 186;
            this.TrasladoIEPS.TabStop = false;
            this.TrasladoIEPS.Text = "0.00";
            this.TrasladoIEPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel28
            // 
            this.RadLabel28.Location = new System.Drawing.Point(14, 11);
            this.RadLabel28.Name = "RadLabel28";
            this.RadLabel28.Size = new System.Drawing.Size(50, 18);
            this.RadLabel28.TabIndex = 193;
            this.RadLabel28.Text = "SubTotal";
            // 
            // TrasladoIVA
            // 
            this.TrasladoIVA.Location = new System.Drawing.Point(96, 58);
            this.TrasladoIVA.Mask = "n2";
            this.TrasladoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TrasladoIVA.Name = "TrasladoIVA";
            this.TrasladoIVA.NullText = "SubTotal";
            this.TrasladoIVA.Size = new System.Drawing.Size(125, 20);
            this.TrasladoIVA.TabIndex = 185;
            this.TrasladoIVA.TabStop = false;
            this.TrasladoIVA.Text = "0.00";
            this.TrasladoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel31
            // 
            this.RadLabel31.Location = new System.Drawing.Point(14, 155);
            this.RadLabel31.Name = "RadLabel31";
            this.RadLabel31.Size = new System.Drawing.Size(31, 18);
            this.RadLabel31.TabIndex = 196;
            this.RadLabel31.Text = "Total";
            // 
            // RadLabel24
            // 
            this.RadLabel24.Location = new System.Drawing.Point(14, 35);
            this.RadLabel24.Name = "RadLabel24";
            this.RadLabel24.Size = new System.Drawing.Size(59, 18);
            this.RadLabel24.TabIndex = 190;
            this.RadLabel24.Text = "Descuento";
            // 
            // RadLabel27
            // 
            this.RadLabel27.Location = new System.Drawing.Point(14, 83);
            this.RadLabel27.Name = "RadLabel27";
            this.RadLabel27.Size = new System.Drawing.Size(27, 18);
            this.RadLabel27.TabIndex = 192;
            this.RadLabel27.Text = "IEPS";
            // 
            // RadLabel30
            // 
            this.RadLabel30.Location = new System.Drawing.Point(14, 131);
            this.RadLabel30.Name = "RadLabel30";
            this.RadLabel30.Size = new System.Drawing.Size(75, 18);
            this.RadLabel30.TabIndex = 195;
            this.RadLabel30.Text = "Retención ISR";
            // 
            // RadLabel26
            // 
            this.RadLabel26.Location = new System.Drawing.Point(14, 59);
            this.RadLabel26.Name = "RadLabel26";
            this.RadLabel26.Size = new System.Drawing.Size(24, 18);
            this.RadLabel26.TabIndex = 191;
            this.RadLabel26.Text = "IVA";
            // 
            // RadLabel29
            // 
            this.RadLabel29.Location = new System.Drawing.Point(14, 107);
            this.RadLabel29.Name = "RadLabel29";
            this.RadLabel29.Size = new System.Drawing.Size(77, 18);
            this.RadLabel29.TabIndex = 194;
            this.RadLabel29.Text = "Retención IVA";
            // 
            // Preparar
            // 
            this.Preparar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.Preparar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // menuContextual
            // 
            this.menuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.menuContextAplicar});
            // 
            // menuContextAplicar
            // 
            this.menuContextAplicar.Name = "menuContextAplicar";
            this.menuContextAplicar.Text = "Aplicar a todos";
            this.menuContextAplicar.UseCompatibleTextRendering = false;
            this.menuContextAplicar.Click += new System.EventHandler(this.menuContextAplicar_Click);
            // 
            // TOrden
            // 
            this.TOrden.Dock = System.Windows.Forms.DockStyle.Top;
            this.TOrden.Location = new System.Drawing.Point(0, 0);
            this.TOrden.MinimumSize = new System.Drawing.Size(1350, 180);
            this.TOrden.Name = "TOrden";
            this.TOrden.Size = new System.Drawing.Size(1402, 180);
            this.TOrden.TabIndex = 4;
            this.TOrden.BindingCompleted += new System.EventHandler<System.EventArgs>(this.TOrden_BindingCompleted);
            this.TOrden.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.TOrden_Cerrar_Click);
            // 
            // OrdenCompraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1402, 702);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.TOrden);
            this.Name = "OrdenCompraForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "Orden de Compra";
            this.Load += new System.EventHandler(this.OrdenCompraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Contenedor)).EndInit();
            this.Contenedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelConceptos)).EndInit();
            this.PanelConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PageViewConceptoParte)).EndInit();
            this.PageViewConceptoParte.ResumeLayout(false);
            this.ViewPageConceptoImpuestos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoImpuestos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptoImpuestos)).EndInit();
            this.ViewPageValesEntrada.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridValeAlmacen.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridValeAlmacen)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionISR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetencionIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrasladoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer Contenedor;
        private Telerik.WinControls.UI.SplitPanel PanelConceptos;
        private Telerik.WinControls.UI.SplitPanel PanelTotales;
        private Telerik.WinControls.UI.RadPageView PageViewConceptoParte;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadMaskedEditBox Total;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionISR;
        private Telerik.WinControls.UI.RadMaskedEditBox Descuento;
        private Telerik.WinControls.UI.RadMaskedEditBox RetencionIVA;
        private Telerik.WinControls.UI.RadMaskedEditBox SubTotal;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIEPS;
        internal Telerik.WinControls.UI.RadLabel RadLabel28;
        private Telerik.WinControls.UI.RadMaskedEditBox TrasladoIVA;
        internal Telerik.WinControls.UI.RadLabel RadLabel31;
        internal Telerik.WinControls.UI.RadLabel RadLabel24;
        internal Telerik.WinControls.UI.RadLabel RadLabel27;
        internal Telerik.WinControls.UI.RadLabel RadLabel30;
        internal Telerik.WinControls.UI.RadLabel RadLabel26;
        internal Telerik.WinControls.UI.RadLabel RadLabel29;
        private Telerik.WinControls.UI.RadPageViewPage ViewPageConceptoImpuestos;
        private Telerik.WinControls.UI.RadPageViewPage ViewPageValesEntrada;
        private Telerik.WinControls.UI.RadGridView gridConceptos;
        private Telerik.WinControls.UI.RadGridView gridValeAlmacen;
        private Common.Forms.ToolBarStandarControl TImpuestos;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl2;
        public OrdenCompraControl TOrden;
        private System.ComponentModel.BackgroundWorker Preparar;
        private TbProductoServicioBuscarControl TConcepto;
        internal Telerik.WinControls.UI.RadGridView gridConceptoImpuestos;
        private Telerik.WinControls.UI.RadContextMenu menuContextual;
        private Telerik.WinControls.UI.RadMenuItem menuContextAplicar;
    }
}
