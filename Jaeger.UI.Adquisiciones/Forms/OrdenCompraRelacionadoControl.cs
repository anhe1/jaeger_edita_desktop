﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Aplication.Adquisiciones.Contracts;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class OrdenCompraRelacionadoControl : UserControl {
        public IOrdenCompraService service;
        private bool editable = false;

        public OrdenCompraRelacionadoControl() {
            InitializeComponent();
        }

        private void CFDIRelacionadoControl_Load(object sender, EventArgs e) {

        }

        public bool Editable {
            get {
                return this.editable;
            }
            set {
                this.ToolBar.TipoRelacion.DataSource = OrdenCompraService.GetTipoRelacion();
                this.ToolBar.TipoRelacion.DisplayMember = "Descripcion";
                this.ToolBar.TipoRelacion.ValueMember = "Id";
                this.editable = value;
                this.ToolBar.SetEditable(value);
            }
        }

        private void ToolBar_ButtonBuscar_Click(object sender, EventArgs e) {
            var _buscar = new OrdenCompraBuscarForm(this.RFC.Text);
            _buscar.Agregar += _buscar_Agregar;
            _buscar.ShowDialog(this);
        }

        private void _buscar_Agregar(object sender, Domain.Adquisiciones.Entities.OrdenCompraSingleModel e) {
            if (e != null) {
                this.Comprobantes.Rows.AddNew();
                this.Comprobantes.CurrentRow.Cells["Folio"].Value = e.Folio;
                this.Comprobantes.CurrentRow.Cells["Serie"].Value = e.Serie;
                this.Comprobantes.CurrentRow.Cells["ReceptorNombre"].Value = e.ReceptorNombre;
                this.Comprobantes.CurrentRow.Cells["FechaEmision"].Value = e.FechaEmision;
                this.Comprobantes.CurrentRow.Cells["Total"].Value = e.Total;
                this.Comprobantes.CurrentRow.Cells["Id"].Value = e.Id;
                this.Comprobantes.CurrentRow.Cells["IdProveedor"].Value = e.IdProveedor;
                this.Comprobantes.CurrentRow.Cells["IdStatus"].Value = e.IdStatus;
                this.Comprobantes.CurrentRow.Cells["Creo"].Value = e.Creo;
                this.Comprobantes.CurrentRow.Cells["ReceptorRFC"].Value = e.ReceptorRFC;
                this.Comprobantes.CurrentRow.Cells["Contacto"].Value = e.Contacto;
            }
        }

        private void ToolBar_ButtonRemover_Click(object sender, EventArgs e) {
            if (this.Comprobantes.CurrentRow != null) {
                this.Comprobantes.Rows.Remove(this.Comprobantes.CurrentRow);
            }
        }
    }
}
