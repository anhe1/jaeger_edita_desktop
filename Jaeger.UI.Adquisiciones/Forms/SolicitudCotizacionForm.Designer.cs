﻿
namespace Jaeger.UI.Adquisiciones.Forms {
    partial class SolicitudCotizacionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TSolicitud = new Jaeger.UI.Adquisiciones.Forms.SolicitudCotizacionControl();
            this.gridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.TConcepto = new Jaeger.UI.Adquisiciones.Forms.TbProductoServicioBuscarControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TSolicitud
            // 
            this.TSolicitud.Dock = System.Windows.Forms.DockStyle.Top;
            this.TSolicitud.Location = new System.Drawing.Point(0, 0);
            this.TSolicitud.MinimumSize = new System.Drawing.Size(1350, 115);
            this.TSolicitud.Name = "TSolicitud";
            this.TSolicitud.Size = new System.Drawing.Size(1351, 115);
            this.TSolicitud.TabIndex = 0;
            this.TSolicitud.BindingCompleted += new System.EventHandler<System.EventArgs>(this.TSolicitud_BindingCompleted);
            // 
            // gridConceptos
            // 
            this.gridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridConceptos.Location = new System.Drawing.Point(0, 145);
            // 
            // 
            // 
            this.gridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridConceptos.Name = "gridConceptos";
            this.gridConceptos.ShowGroupPanel = false;
            this.gridConceptos.Size = new System.Drawing.Size(1351, 305);
            this.gridConceptos.TabIndex = 2;
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Location = new System.Drawing.Point(0, 115);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.Size = new System.Drawing.Size(1351, 30);
            this.TConcepto.TabIndex = 3;
            // 
            // SolicitudCotizacionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1351, 450);
            this.Controls.Add(this.gridConceptos);
            this.Controls.Add(this.TConcepto);
            this.Controls.Add(this.TSolicitud);
            this.Name = "SolicitudCotizacionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Solicitud de Cotización";
            this.Load += new System.EventHandler(this.SolicitudCotizacionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public SolicitudCotizacionControl TSolicitud;
        private Telerik.WinControls.UI.RadGridView gridConceptos;
        private TbProductoServicioBuscarControl TConcepto;
    }
}