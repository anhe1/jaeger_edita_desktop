﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class OrdenCompraProveedorControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            this.MetodoEnvio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.MotivoDescuento = new Telerik.WinControls.UI.RadTextBox();
            this.lblDescuento = new Telerik.WinControls.UI.RadLabel();
            this.lblMetodoEnvio = new Telerik.WinControls.UI.RadLabel();
            this.FechaEntrada = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEntrada = new Telerik.WinControls.UI.RadLabel();
            this.FechaRequerida = new Telerik.WinControls.UI.RadDateTimePicker();
            this.NumPedido = new Telerik.WinControls.UI.RadTextBox();
            this.lblNumPedido = new Telerik.WinControls.UI.RadLabel();
            this.Vendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblFechaRequerida = new Telerik.WinControls.UI.RadLabel();
            this.lblVendedor = new Telerik.WinControls.UI.RadLabel();
            this.Embarque = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblEmbarque = new Telerik.WinControls.UI.RadLabel();
            this.Contacto = new Telerik.WinControls.UI.RadDropDownList();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblContacto = new Telerik.WinControls.UI.RadLabel();
            this.MetodoPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.FechaAutorizacion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.CondicionPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.Decimales = new Telerik.WinControls.UI.RadSpinEditor();
            this.FormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaAutoriza = new Telerik.WinControls.UI.RadLabel();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.TipoCambio = new Telerik.WinControls.UI.RadTextBox();
            this.lblMetodoPago = new Telerik.WinControls.UI.RadLabel();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCondicionPago = new Telerik.WinControls.UI.RadLabel();
            this.lblFormaPago = new Telerik.WinControls.UI.RadLabel();
            this.IdDirectorio = new Telerik.WinControls.UI.RadSpinEditor();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.Serie = new Telerik.WinControls.UI.RadDropDownList();
            this.Documento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblSerie = new Telerik.WinControls.UI.RadLabel();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.lblUsoCFDI = new Telerik.WinControls.UI.RadLabel();
            this.UsoCFDI = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            this.Grupo = new Telerik.WinControls.UI.RadGroupBox();
            this.Receptor = new Jaeger.UI.Contribuyentes.Forms.DirectorioControl();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaRequerida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaRequerida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmbarque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaAutorizacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CondicionPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CondicionPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CondicionPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAutoriza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCondicionPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grupo)).BeginInit();
            this.Grupo.SuspendLayout();
            this.SuspendLayout();
            // 
            // MetodoEnvio
            // 
            this.MetodoEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // MetodoEnvio.NestedRadGridView
            // 
            this.MetodoEnvio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MetodoEnvio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoEnvio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MetodoEnvio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn4.FieldName = "Descriptor";
            gridViewTextBoxColumn4.HeaderText = "Descripcion";
            gridViewTextBoxColumn4.Name = "Descriptor";
            this.MetodoEnvio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.MetodoEnvio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.MetodoEnvio.EditorControl.Name = "NestedRadGridView";
            this.MetodoEnvio.EditorControl.ReadOnly = true;
            this.MetodoEnvio.EditorControl.ShowGroupPanel = false;
            this.MetodoEnvio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MetodoEnvio.EditorControl.TabIndex = 0;
            this.MetodoEnvio.Location = new System.Drawing.Point(500, 53);
            this.MetodoEnvio.Name = "MetodoEnvio";
            this.MetodoEnvio.NullText = "Envío";
            this.MetodoEnvio.Size = new System.Drawing.Size(93, 20);
            this.MetodoEnvio.TabIndex = 224;
            this.MetodoEnvio.TabStop = false;
            // 
            // MotivoDescuento
            // 
            this.MotivoDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MotivoDescuento.Location = new System.Drawing.Point(333, 78);
            this.MotivoDescuento.Name = "MotivoDescuento";
            this.MotivoDescuento.NullText = "Motivo del Descuento";
            this.MotivoDescuento.Size = new System.Drawing.Size(260, 20);
            this.MotivoDescuento.TabIndex = 194;
            // 
            // lblDescuento
            // 
            this.lblDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescuento.Location = new System.Drawing.Point(265, 79);
            this.lblDescuento.Name = "lblDescuento";
            this.lblDescuento.Size = new System.Drawing.Size(62, 18);
            this.lblDescuento.TabIndex = 226;
            this.lblDescuento.Text = "Descuento:";
            // 
            // lblMetodoEnvio
            // 
            this.lblMetodoEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMetodoEnvio.Location = new System.Drawing.Point(433, 54);
            this.lblMetodoEnvio.Name = "lblMetodoEnvio";
            this.lblMetodoEnvio.Size = new System.Drawing.Size(61, 18);
            this.lblMetodoEnvio.TabIndex = 225;
            this.lblMetodoEnvio.Text = "Mét. Envío:";
            // 
            // FechaEntrada
            // 
            this.FechaEntrada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEntrada.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEntrada.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaEntrada.Location = new System.Drawing.Point(885, 78);
            this.FechaEntrada.Name = "FechaEntrada";
            this.FechaEntrada.NullText = "--/--/----";
            this.FechaEntrada.ReadOnly = true;
            this.FechaEntrada.Size = new System.Drawing.Size(199, 20);
            this.FechaEntrada.TabIndex = 222;
            this.FechaEntrada.TabStop = false;
            this.FechaEntrada.Text = "05/diciembre/2017";
            this.FechaEntrada.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // lblFechaEntrada
            // 
            this.lblFechaEntrada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEntrada.Location = new System.Drawing.Point(785, 79);
            this.lblFechaEntrada.Name = "lblFechaEntrada";
            this.lblFechaEntrada.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEntrada.TabIndex = 223;
            this.lblFechaEntrada.Text = "Fec. de Entrada:";
            // 
            // FechaRequerida
            // 
            this.FechaRequerida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaRequerida.CustomFormat = "dd/MMMM/yyyy";
            this.FechaRequerida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaRequerida.Location = new System.Drawing.Point(885, 28);
            this.FechaRequerida.Name = "FechaRequerida";
            this.FechaRequerida.NullText = "--/--/----";
            this.FechaRequerida.ReadOnly = true;
            this.FechaRequerida.Size = new System.Drawing.Size(199, 20);
            this.FechaRequerida.TabIndex = 219;
            this.FechaRequerida.TabStop = false;
            this.FechaRequerida.Text = "05/diciembre/2017";
            this.FechaRequerida.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // NumPedido
            // 
            this.NumPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumPedido.Location = new System.Drawing.Point(676, 3);
            this.NumPedido.Name = "NumPedido";
            this.NumPedido.NullText = "Núm. Pedido";
            this.NumPedido.Size = new System.Drawing.Size(103, 20);
            this.NumPedido.TabIndex = 202;
            // 
            // lblNumPedido
            // 
            this.lblNumPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumPedido.Location = new System.Drawing.Point(596, 4);
            this.lblNumPedido.Name = "lblNumPedido";
            this.lblNumPedido.Size = new System.Drawing.Size(74, 18);
            this.lblNumPedido.TabIndex = 210;
            this.lblNumPedido.Text = "Núm. Pedido:";
            // 
            // Vendedor
            // 
            this.Vendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Vendedor.AutoFilter = true;
            this.Vendedor.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.Vendedor.AutoSizeDropDownToBestFit = true;
            // 
            // Vendedor.NestedRadGridView
            // 
            this.Vendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Vendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Vendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Vendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Vendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Vendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.FieldName = "IdVendedor";
            gridViewTextBoxColumn5.HeaderText = "IdVendedor";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "IdVendedor";
            gridViewTextBoxColumn6.FieldName = "Clave";
            gridViewTextBoxColumn6.HeaderText = "Clave";
            gridViewTextBoxColumn6.Name = "Clave";
            gridViewTextBoxColumn7.FieldName = "Nombre";
            gridViewTextBoxColumn7.HeaderText = "Nombre";
            gridViewTextBoxColumn7.Name = "Nombre";
            this.Vendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.Vendedor.EditorControl.MasterTemplate.EnableFiltering = true;
            this.Vendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Vendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Vendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Vendedor.EditorControl.Name = "NestedRadGridView";
            this.Vendedor.EditorControl.ReadOnly = true;
            this.Vendedor.EditorControl.ShowGroupPanel = false;
            this.Vendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Vendedor.EditorControl.TabIndex = 0;
            this.Vendedor.Location = new System.Drawing.Point(660, 28);
            this.Vendedor.Name = "Vendedor";
            this.Vendedor.NullText = "Vendedor";
            this.Vendedor.Size = new System.Drawing.Size(119, 20);
            this.Vendedor.TabIndex = 201;
            this.Vendedor.TabStop = false;
            // 
            // lblFechaRequerida
            // 
            this.lblFechaRequerida.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaRequerida.Location = new System.Drawing.Point(785, 29);
            this.lblFechaRequerida.Name = "lblFechaRequerida";
            this.lblFechaRequerida.Size = new System.Drawing.Size(97, 18);
            this.lblFechaRequerida.TabIndex = 221;
            this.lblFechaRequerida.Text = "Fec. de Requerida:";
            // 
            // lblVendedor
            // 
            this.lblVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVendedor.Location = new System.Drawing.Point(596, 29);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(58, 18);
            this.lblVendedor.TabIndex = 208;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // Embarque
            // 
            this.Embarque.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Embarque.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.Embarque.AutoSizeDropDownToBestFit = true;
            // 
            // Embarque.NestedRadGridView
            // 
            this.Embarque.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Embarque.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Embarque.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Embarque.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Embarque.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Embarque.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Embarque.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "Domicilio";
            gridViewTextBoxColumn8.HeaderText = "Domicilio";
            gridViewTextBoxColumn8.Name = "Domicilio";
            this.Embarque.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8});
            this.Embarque.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Embarque.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Embarque.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.Embarque.EditorControl.Name = "NestedRadGridView";
            this.Embarque.EditorControl.ReadOnly = true;
            this.Embarque.EditorControl.ShowGroupPanel = false;
            this.Embarque.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Embarque.EditorControl.TabIndex = 0;
            this.Embarque.Location = new System.Drawing.Point(67, 53);
            this.Embarque.Name = "Embarque";
            this.Embarque.NullText = "Lugar de Embarque";
            this.Embarque.Size = new System.Drawing.Size(362, 20);
            this.Embarque.TabIndex = 203;
            this.Embarque.TabStop = false;
            // 
            // lblEmbarque
            // 
            this.lblEmbarque.Location = new System.Drawing.Point(2, 54);
            this.lblEmbarque.Name = "lblEmbarque";
            this.lblEmbarque.Size = new System.Drawing.Size(59, 18);
            this.lblEmbarque.TabIndex = 209;
            this.lblEmbarque.Text = "Embarque:";
            // 
            // Contacto
            // 
            this.Contacto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Contacto.Location = new System.Drawing.Point(67, 78);
            this.Contacto.Name = "Contacto";
            this.Contacto.NullText = "Contacto";
            this.Contacto.Size = new System.Drawing.Size(192, 20);
            this.Contacto.TabIndex = 198;
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(885, 3);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(199, 20);
            this.FechaEmision.TabIndex = 192;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // lblContacto
            // 
            this.lblContacto.Location = new System.Drawing.Point(2, 79);
            this.lblContacto.Name = "lblContacto";
            this.lblContacto.Size = new System.Drawing.Size(54, 18);
            this.lblContacto.TabIndex = 214;
            this.lblContacto.Text = "Contacto:";
            // 
            // MetodoPago
            // 
            this.MetodoPago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // MetodoPago.NestedRadGridView
            // 
            this.MetodoPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MetodoPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MetodoPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MetodoPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Descripción";
            gridViewTextBoxColumn10.Name = "Descripcion";
            this.MetodoPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.MetodoPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MetodoPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MetodoPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.MetodoPago.EditorControl.Name = "NestedRadGridView";
            this.MetodoPago.EditorControl.ReadOnly = true;
            this.MetodoPago.EditorControl.ShowGroupPanel = false;
            this.MetodoPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MetodoPago.EditorControl.TabIndex = 0;
            this.MetodoPago.Location = new System.Drawing.Point(1191, 28);
            this.MetodoPago.Name = "MetodoPago";
            this.MetodoPago.NullText = "Método de Pago";
            this.MetodoPago.Size = new System.Drawing.Size(217, 20);
            this.MetodoPago.TabIndex = 197;
            this.MetodoPago.TabStop = false;
            // 
            // FechaAutorizacion
            // 
            this.FechaAutorizacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaAutorizacion.CustomFormat = "dd/MMMM/yyyy";
            this.FechaAutorizacion.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FechaAutorizacion.Location = new System.Drawing.Point(885, 53);
            this.FechaAutorizacion.Name = "FechaAutorizacion";
            this.FechaAutorizacion.NullText = "--/--/----";
            this.FechaAutorizacion.ReadOnly = true;
            this.FechaAutorizacion.Size = new System.Drawing.Size(199, 20);
            this.FechaAutorizacion.TabIndex = 217;
            this.FechaAutorizacion.TabStop = false;
            this.FechaAutorizacion.Text = "05/diciembre/2017";
            this.FechaAutorizacion.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // CondicionPago
            // 
            this.CondicionPago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // CondicionPago.NestedRadGridView
            // 
            this.CondicionPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CondicionPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CondicionPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CondicionPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CondicionPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CondicionPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CondicionPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CondicionPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CondicionPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CondicionPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.CondicionPago.EditorControl.Name = "NestedRadGridView";
            this.CondicionPago.EditorControl.ReadOnly = true;
            this.CondicionPago.EditorControl.ShowGroupPanel = false;
            this.CondicionPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CondicionPago.EditorControl.TabIndex = 0;
            this.CondicionPago.Location = new System.Drawing.Point(1191, 3);
            this.CondicionPago.Name = "CondicionPago";
            this.CondicionPago.NullText = "Condiciones";
            this.CondicionPago.Size = new System.Drawing.Size(217, 20);
            this.CondicionPago.TabIndex = 200;
            this.CondicionPago.TabStop = false;
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(785, 4);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEmision.TabIndex = 193;
            this.lblFechaEmision.Text = "Fec. de Emisión:";
            // 
            // Decimales
            // 
            this.Decimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Decimales.Location = new System.Drawing.Point(733, 78);
            this.Decimales.Name = "Decimales";
            this.Decimales.Size = new System.Drawing.Size(46, 20);
            this.Decimales.TabIndex = 220;
            this.Decimales.TabStop = false;
            this.Decimales.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FormaPago
            // 
            this.FormaPago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // FormaPago.NestedRadGridView
            // 
            this.FormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.FormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.FormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn11.FieldName = "Clave";
            gridViewTextBoxColumn11.HeaderText = "Clave";
            gridViewTextBoxColumn11.Name = "Clave";
            gridViewTextBoxColumn12.FieldName = "Descripcion";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descripcion";
            this.FormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.FormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.FormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.FormaPago.EditorControl.Name = "NestedRadGridView";
            this.FormaPago.EditorControl.ReadOnly = true;
            this.FormaPago.EditorControl.ShowGroupPanel = false;
            this.FormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.FormaPago.EditorControl.TabIndex = 0;
            this.FormaPago.Location = new System.Drawing.Point(1191, 53);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.NullText = "Forma de pago";
            this.FormaPago.Size = new System.Drawing.Size(217, 20);
            this.FormaPago.TabIndex = 199;
            this.FormaPago.TabStop = false;
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoCambio.Location = new System.Drawing.Point(596, 79);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(74, 18);
            this.lblTipoCambio.TabIndex = 216;
            this.lblTipoCambio.Text = "T. de Cambio:";
            // 
            // lblFechaAutoriza
            // 
            this.lblFechaAutoriza.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaAutoriza.Location = new System.Drawing.Point(785, 54);
            this.lblFechaAutoriza.Name = "lblFechaAutoriza";
            this.lblFechaAutoriza.Size = new System.Drawing.Size(94, 18);
            this.lblFechaAutoriza.TabIndex = 218;
            this.lblFechaAutoriza.Text = "Fec. Autorización:";
            // 
            // lblMoneda
            // 
            this.lblMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneda.Location = new System.Drawing.Point(596, 54);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 215;
            this.lblMoneda.Text = "Moneda:";
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.Location = new System.Drawing.Point(685, 78);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.NullText = "Tipo de Cambio";
            this.TipoCambio.Size = new System.Drawing.Size(49, 20);
            this.TipoCambio.TabIndex = 196;
            // 
            // lblMetodoPago
            // 
            this.lblMetodoPago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMetodoPago.Location = new System.Drawing.Point(1092, 29);
            this.lblMetodoPago.Name = "lblMetodoPago";
            this.lblMetodoPago.Size = new System.Drawing.Size(93, 18);
            this.lblMetodoPago.TabIndex = 211;
            this.lblMetodoPago.Text = "Método de Pago:";
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn13.FieldName = "Clave";
            gridViewTextBoxColumn13.HeaderText = "Clave";
            gridViewTextBoxColumn13.Name = "Clave";
            gridViewTextBoxColumn14.FieldName = "Descripcion";
            gridViewTextBoxColumn14.HeaderText = "Descripción";
            gridViewTextBoxColumn14.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(660, 53);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(119, 20);
            this.Moneda.TabIndex = 195;
            this.Moneda.TabStop = false;
            // 
            // lblCondicionPago
            // 
            this.lblCondicionPago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCondicionPago.Location = new System.Drawing.Point(1092, 4);
            this.lblCondicionPago.Name = "lblCondicionPago";
            this.lblCondicionPago.Size = new System.Drawing.Size(70, 18);
            this.lblCondicionPago.TabIndex = 213;
            this.lblCondicionPago.Text = "Condiciones:";
            // 
            // lblFormaPago
            // 
            this.lblFormaPago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFormaPago.Location = new System.Drawing.Point(1092, 54);
            this.lblFormaPago.Name = "lblFormaPago";
            this.lblFormaPago.Size = new System.Drawing.Size(84, 18);
            this.lblFormaPago.TabIndex = 212;
            this.lblFormaPago.Text = "Forma de Pago:";
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(1419, 9);
            this.IdDirectorio.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.ShowBorder = false;
            this.IdDirectorio.ShowUpDownButtons = false;
            this.IdDirectorio.Size = new System.Drawing.Size(37, 20);
            this.IdDirectorio.TabIndex = 229;
            this.IdDirectorio.TabStop = false;
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(472, 3);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.ReadOnly = true;
            this.Folio.Size = new System.Drawing.Size(121, 20);
            this.Folio.TabIndex = 235;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Serie
            // 
            this.Serie.Location = new System.Drawing.Point(304, 3);
            this.Serie.Name = "Serie";
            this.Serie.NullText = "Serie";
            this.Serie.Size = new System.Drawing.Size(125, 20);
            this.Serie.TabIndex = 233;
            // 
            // Documento
            // 
            // 
            // Documento.NestedRadGridView
            // 
            this.Documento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Documento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Documento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Documento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Documento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Documento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Documento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn15.FieldName = "TipoDeComprobante";
            gridViewTextBoxColumn15.HeaderText = "Comprobante";
            gridViewTextBoxColumn15.Name = "TipoDeComprobante";
            gridViewTextBoxColumn16.FieldName = "Nombre";
            gridViewTextBoxColumn16.HeaderText = "Nombre";
            gridViewTextBoxColumn16.Name = "Nombre";
            gridViewTextBoxColumn17.FieldName = "Serie";
            gridViewTextBoxColumn17.HeaderText = "Serie";
            gridViewTextBoxColumn17.Name = "Serie";
            gridViewTextBoxColumn18.FieldName = "Folio";
            gridViewTextBoxColumn18.HeaderText = "Folio";
            gridViewTextBoxColumn18.Name = "Folio";
            gridViewTextBoxColumn19.FieldName = "Template";
            gridViewTextBoxColumn19.HeaderText = "Template";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Template";
            this.Documento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19});
            this.Documento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Documento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Documento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.Documento.EditorControl.Name = "NestedRadGridView";
            this.Documento.EditorControl.ReadOnly = true;
            this.Documento.EditorControl.ShowGroupPanel = false;
            this.Documento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Documento.EditorControl.TabIndex = 0;
            this.Documento.Location = new System.Drawing.Point(67, 3);
            this.Documento.Name = "Documento";
            this.Documento.NullText = "Documento";
            this.Documento.Size = new System.Drawing.Size(171, 20);
            this.Documento.TabIndex = 231;
            this.Documento.TabStop = false;
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(265, 4);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(33, 18);
            this.lblSerie.TabIndex = 232;
            this.lblSerie.Text = "Serie:";
            // 
            // lblFolio
            // 
            this.lblFolio.Location = new System.Drawing.Point(433, 4);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 234;
            this.lblFolio.Text = "Folio:";
            // 
            // lblUsoCFDI
            // 
            this.lblUsoCFDI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUsoCFDI.Location = new System.Drawing.Point(1094, 79);
            this.lblUsoCFDI.Name = "lblUsoCFDI";
            this.lblUsoCFDI.Size = new System.Drawing.Size(70, 18);
            this.lblUsoCFDI.TabIndex = 236;
            this.lblUsoCFDI.Text = "Uso de CFDI:";
            // 
            // UsoCFDI
            // 
            this.UsoCFDI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UsoCFDI.DisplayMember = "Descripcion";
            this.UsoCFDI.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // UsoCFDI.NestedRadGridView
            // 
            this.UsoCFDI.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.UsoCFDI.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsoCFDI.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UsoCFDI.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.UsoCFDI.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn20.FieldName = "Clave";
            gridViewTextBoxColumn20.HeaderText = "Clave";
            gridViewTextBoxColumn20.Name = "Clave";
            gridViewTextBoxColumn21.FieldName = "Descripcion";
            gridViewTextBoxColumn21.HeaderText = "Descripción";
            gridViewTextBoxColumn21.Name = "Descripcion";
            this.UsoCFDI.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21});
            this.UsoCFDI.EditorControl.MasterTemplate.EnableGrouping = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.UsoCFDI.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.UsoCFDI.EditorControl.Name = "NestedRadGridView";
            this.UsoCFDI.EditorControl.ReadOnly = true;
            this.UsoCFDI.EditorControl.ShowGroupPanel = false;
            this.UsoCFDI.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.UsoCFDI.EditorControl.TabIndex = 0;
            this.UsoCFDI.Location = new System.Drawing.Point(1191, 78);
            this.UsoCFDI.Name = "UsoCFDI";
            this.UsoCFDI.NullText = "Uso de CFDI";
            this.UsoCFDI.Size = new System.Drawing.Size(217, 20);
            this.UsoCFDI.TabIndex = 237;
            this.UsoCFDI.TabStop = false;
            this.UsoCFDI.ValueMember = "Clave";
            // 
            // Preparar
            // 
            this.Preparar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.Preparar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // Grupo
            // 
            this.Grupo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Grupo.Controls.Add(this.Receptor);
            this.Grupo.Controls.Add(this.Documento);
            this.Grupo.Controls.Add(this.lblUsoCFDI);
            this.Grupo.Controls.Add(this.IdDirectorio);
            this.Grupo.Controls.Add(this.lblFormaPago);
            this.Grupo.Controls.Add(this.UsoCFDI);
            this.Grupo.Controls.Add(this.lblCondicionPago);
            this.Grupo.Controls.Add(this.Moneda);
            this.Grupo.Controls.Add(this.Folio);
            this.Grupo.Controls.Add(this.lblMetodoPago);
            this.Grupo.Controls.Add(this.Serie);
            this.Grupo.Controls.Add(this.TipoCambio);
            this.Grupo.Controls.Add(this.lblMoneda);
            this.Grupo.Controls.Add(this.lblSerie);
            this.Grupo.Controls.Add(this.lblFechaAutoriza);
            this.Grupo.Controls.Add(this.lblFolio);
            this.Grupo.Controls.Add(this.lblTipoCambio);
            this.Grupo.Controls.Add(this.FormaPago);
            this.Grupo.Controls.Add(this.Decimales);
            this.Grupo.Controls.Add(this.lblFechaEmision);
            this.Grupo.Controls.Add(this.MetodoEnvio);
            this.Grupo.Controls.Add(this.CondicionPago);
            this.Grupo.Controls.Add(this.MotivoDescuento);
            this.Grupo.Controls.Add(this.FechaAutorizacion);
            this.Grupo.Controls.Add(this.lblDescuento);
            this.Grupo.Controls.Add(this.MetodoPago);
            this.Grupo.Controls.Add(this.lblMetodoEnvio);
            this.Grupo.Controls.Add(this.lblContacto);
            this.Grupo.Controls.Add(this.FechaEntrada);
            this.Grupo.Controls.Add(this.FechaEmision);
            this.Grupo.Controls.Add(this.lblFechaEntrada);
            this.Grupo.Controls.Add(this.Contacto);
            this.Grupo.Controls.Add(this.FechaRequerida);
            this.Grupo.Controls.Add(this.lblEmbarque);
            this.Grupo.Controls.Add(this.NumPedido);
            this.Grupo.Controls.Add(this.lblNumPedido);
            this.Grupo.Controls.Add(this.Vendedor);
            this.Grupo.Controls.Add(this.lblFechaRequerida);
            this.Grupo.Controls.Add(this.lblVendedor);
            this.Grupo.Controls.Add(this.Embarque);
            this.Grupo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grupo.HeaderText = "";
            this.Grupo.Location = new System.Drawing.Point(0, 0);
            this.Grupo.Name = "Grupo";
            this.Grupo.Size = new System.Drawing.Size(1419, 102);
            this.Grupo.TabIndex = 238;
            this.Grupo.TabStop = false;
            // 
            // Receptor
            // 
            this.Receptor.AllowAddNew = false;
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.LDomicilio = "Domicilio:";
            this.Receptor.LNombre = "Proveedor:";
            this.Receptor.Location = new System.Drawing.Point(0, 28);
            this.Receptor.Margin = new System.Windows.Forms.Padding(4);
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = false;
            this.Receptor.Relacion = Jaeger.Domain.Base.ValueObjects.TipoRelacionComericalEnum.None;
            this.Receptor.ShowDomicilio = false;
            this.Receptor.ShowRFC = true;
            this.Receptor.Size = new System.Drawing.Size(593, 20);
            this.Receptor.TabIndex = 239;
            // 
            // OrdenCompraProveedorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Grupo);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "OrdenCompraProveedorControl";
            this.Size = new System.Drawing.Size(1419, 102);
            this.Load += new System.EventHandler(this.OrdenCompraProveedorControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaRequerida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaRequerida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Embarque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmbarque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaAutorizacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CondicionPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CondicionPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CondicionPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAutoriza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCondicionPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Serie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grupo)).EndInit();
            this.Grupo.ResumeLayout(false);
            this.Grupo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Telerik.WinControls.UI.RadMultiColumnComboBox MetodoEnvio;
        internal Telerik.WinControls.UI.RadTextBox MotivoDescuento;
        internal Telerik.WinControls.UI.RadLabel lblDescuento;
        internal Telerik.WinControls.UI.RadLabel lblMetodoEnvio;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEntrada;
        internal Telerik.WinControls.UI.RadLabel lblFechaEntrada;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaRequerida;
        internal Telerik.WinControls.UI.RadTextBox NumPedido;
        internal Telerik.WinControls.UI.RadLabel lblNumPedido;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Vendedor;
        internal Telerik.WinControls.UI.RadLabel lblFechaRequerida;
        internal Telerik.WinControls.UI.RadLabel lblVendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Embarque;
        internal Telerik.WinControls.UI.RadLabel lblEmbarque;
        internal Telerik.WinControls.UI.RadDropDownList Contacto;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblContacto;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MetodoPago;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaAutorizacion;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CondicionPago;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox FormaPago;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblFechaAutoriza;
        internal Telerik.WinControls.UI.RadLabel lblMoneda;
        internal Telerik.WinControls.UI.RadTextBox TipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblMetodoPago;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadLabel lblCondicionPago;
        internal Telerik.WinControls.UI.RadLabel lblFormaPago;
        internal Telerik.WinControls.UI.RadSpinEditor IdDirectorio;
        internal Telerik.WinControls.UI.RadSpinEditor Decimales;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadDropDownList Serie;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Documento;
        internal Telerik.WinControls.UI.RadLabel lblSerie;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadLabel lblUsoCFDI;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox UsoCFDI;
        public System.ComponentModel.BackgroundWorker Preparar;
        private Telerik.WinControls.UI.RadGroupBox Grupo;
        public Contribuyentes.Forms.DirectorioControl Receptor;
    }
}
