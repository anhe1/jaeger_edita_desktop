﻿using System;
using System.Collections.Generic;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class OrdenCompraBuscarForm : Telerik.WinControls.UI.RadForm {
        protected IOrdenCompraService service;
        private List<OrdenCompraSingleModel> datos;
        private string _rfc;

        public OrdenCompraBuscarForm(string rfc) {
            InitializeComponent();
            this._rfc = rfc;
        }

        public event EventHandler<OrdenCompraSingleModel> Agregar;
        public void OnAgregar(OrdenCompraSingleModel e) {
            if (this.Agregar != null)
                this.Agregar(this, e);
        }

        private void OrdenCompraBuscarForm_Load(object sender, EventArgs e) {
            this.ToolBar.Nuevo.Text = "Agregar";
            this.ToolBar.Actualizar.Text = "Buscar";
            this.service = new OrdenCompraService();
            this.GridData.Standard();
        }

        private void ToolBar_ButtonNuevo_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as OrdenCompraSingleModel;
                if (seleccionado != null) {
                    this.Agregar(sender, seleccionado);
                }
            }
        }

        private void ToolBar_ButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Buscar)) {
                espera.Text = "Buscando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.datos;
        }

        private void ToolBar_ButtonFiltro_Click(object sender, EventArgs e) {

        }

        private void ToolBar_ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Buscar() {
            this.datos = this.service.GetRelacionado(this._rfc, DateTime.Now.Year);
        }

        private void ToolBar_ButtonAutorizar_Click(object sender, EventArgs e) {

        }
    }
}
