﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class OrdenCompraRelacionadoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Comprobantes = new Telerik.WinControls.UI.RadGridView();
            this.RFC = new Telerik.WinControls.UI.RadTextBox();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarDoctoRelacionadoControl();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            this.SuspendLayout();
            // 
            // Comprobantes
            // 
            this.Comprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Comprobantes.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.Comprobantes.MasterTemplate.AllowAddNewRow = false;
            this.Comprobantes.MasterTemplate.AllowEditRow = false;
            this.Comprobantes.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "Folio";
            gridViewTextBoxColumn1.HeaderText = "Folio";
            gridViewTextBoxColumn1.Name = "Folio";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 75;
            gridViewTextBoxColumn2.FieldName = "Serie";
            gridViewTextBoxColumn2.HeaderText = "Serie";
            gridViewTextBoxColumn2.Name = "Serie";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "FechaEmision";
            gridViewTextBoxColumn3.FormatString = "{0:d}";
            gridViewTextBoxColumn3.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn3.Name = "FechaEmision";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn4.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn4.Name = "ReceptorNombre";
            gridViewTextBoxColumn4.Width = 250;
            gridViewTextBoxColumn5.DataType = typeof(decimal);
            gridViewTextBoxColumn5.FieldName = "Total";
            gridViewTextBoxColumn5.FormatString = "{0:n}";
            gridViewTextBoxColumn5.HeaderText = "Total";
            gridViewTextBoxColumn5.Name = "Total";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldName = "Id";
            gridViewTextBoxColumn6.HeaderText = "Id";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "Id";
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "IdProveedor";
            gridViewTextBoxColumn7.HeaderText = "IdProveedor";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "IdProveedor";
            gridViewTextBoxColumn7.VisibleInColumnChooser = false;
            gridViewTextBoxColumn8.FieldName = "IdStatus";
            gridViewTextBoxColumn8.HeaderText = "IdStatus";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "IdStatus";
            gridViewTextBoxColumn8.VisibleInColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Creo";
            gridViewTextBoxColumn9.HeaderText = "Creo";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Creo";
            gridViewTextBoxColumn9.VisibleInColumnChooser = false;
            gridViewTextBoxColumn10.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn10.HeaderText = "ReceptorRFC";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "ReceptorRFC";
            gridViewTextBoxColumn10.VisibleInColumnChooser = false;
            gridViewTextBoxColumn11.FieldName = "Contacto";
            gridViewTextBoxColumn11.HeaderText = "Contacto";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Contacto";
            gridViewTextBoxColumn11.VisibleInColumnChooser = false;
            this.Comprobantes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.Comprobantes.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Comprobantes.Name = "Comprobantes";
            this.Comprobantes.ShowGroupPanel = false;
            this.Comprobantes.Size = new System.Drawing.Size(969, 146);
            this.Comprobantes.TabIndex = 188;
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(886, 112);
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(100, 20);
            this.RFC.TabIndex = 1;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(969, 30);
            this.ToolBar.TabIndex = 189;
            this.ToolBar.ButtonBuscar_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonBuscar_Click);
            this.ToolBar.ButtonRemover_Click += new System.EventHandler<System.EventArgs>(this.ToolBar_ButtonRemover_Click);
            // 
            // OrdenCompraRelacionadoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Comprobantes);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.ToolBar);
            this.Name = "OrdenCompraRelacionadoControl";
            this.Size = new System.Drawing.Size(969, 176);
            this.Load += new System.EventHandler(this.CFDIRelacionadoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.ComponentModel.BackgroundWorker Preparar;
        internal Telerik.WinControls.UI.RadGridView Comprobantes;
        internal Telerik.WinControls.UI.RadTextBox RFC;
        internal Common.Forms.ToolBarDoctoRelacionadoControl ToolBar;
    }
}
