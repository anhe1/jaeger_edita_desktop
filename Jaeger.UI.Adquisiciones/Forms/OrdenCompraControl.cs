﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Domain.Adquisiciones.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class OrdenCompraControl : UserControl {
        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public event EventHandler<EventArgs> ButtonCerrar_Click;

        public void OnButtonCerrarClick(object sender, EventArgs e) {
            if (this.ButtonCerrar_Click != null) {
                this.ButtonCerrar_Click(sender, e);
            }
        }

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion
        public IOrdenCompraService Service;

        public OrdenCompraDetailModel Comprobante;

        public OrdenCompraControl() {
            InitializeComponent();
        }

        public void CreateService() {
            this.Service = new OrdenCompraService();
            this.OnLoad1.RunWorkerAsync();
        }

        private void OrdenCompraToolBarControl_Load(object sender, EventArgs e) {
            this.IdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.IdDocumento.TextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Center;
            this.Cerrar.Click += new EventHandler(this.OnButtonCerrarClick);
            this.general.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor;
        }

        #region  barra de herramientas
        private void Nuevo_Click(object sender, EventArgs e) {

        }

        private void Duplicar_Click(object sender, EventArgs e) {

        }

        private void Guardar_Click(object sender, EventArgs e) {
            if (Comprobante.IdProveedor == 0) {
                RadMessageBox.Show(this, Properties.Resources.Message_ErrorSeleccionaProveedor, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            if (Comprobante.Conceptos.Count == 0) {
                RadMessageBox.Show(this, Properties.Resources.Message_ErrorNoConceptos, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            if (Comprobante.TrasladoIVA == 0) {
                if (RadMessageBox.Show(this, "No existe iva trasladado, es correcto?", "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == DialogResult.No)
                    return;
            }

            if (Comprobante.Autorizacion()) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_ErrorAutorizacion, "Atención!", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;
            } else {
                if (this.Comprobante.Status != OrdenCompraStatusEnum.Cancelado | this.Comprobante.Status != OrdenCompraStatusEnum.Autorizada | this.Comprobante.Status != OrdenCompraStatusEnum.Ingresada) {
                    this.Comprobante.Status = OrdenCompraStatusEnum.Autorizada;
                    this.Comprobante.FechaAutoriza = DateTime.Now;
                    this.Comprobante.Autoriza = ConfigService.Sysdba;
                }
            }

            using (var espera = new Waiting1Form(this.SetGuardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Text = Properties.Resources.Message_ComprobanteNew;
                this.Comprobante = this.Service.GetNew();
            } else if (this.Comprobante.Id > 0) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.Text = Properties.Resources.Message_ComprobanteGet;
                    espera.ShowDialog(this);
                    this.Text = string.Concat(this.Comprobante.Serie, " ", this.Comprobante.Folio);
                }
            }
            if (this.Comprobante.IsEditable) {
                this.general.Receptor.Init();
                this.Relacionado.Editable = this.Comprobante.IsEditable;
            }
            this.CreateBinding();
            this.OnBindingClompleted(e);
        }

        private void Cancelar_Click(object sender, EventArgs e) {

        }

        private void Imprimir_Click(object sender, EventArgs e) {
            if (this.Comprobante.Id > 0 && this.Comprobante.Status != OrdenCompraStatusEnum.Requerida) {
                var _imprimir = new ReporteForm(new OrdenCompraPrinter(this.Comprobante));
                _imprimir.Show();
            } else {
                RadMessageBox.Show(this, Properties.Resources.Message_ErrorComprobanteNoAutorizado, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void FilePDF_Click(object sender, EventArgs e) {

        }

        private void Series_Click(object sender, EventArgs e) {

        }
        #endregion

        #region metodos privados
        private void CreateBinding() {
            this.Status.DataBindings.Clear();
            this.Status.CommandBarDropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            this.Status.CommandBarDropDownListElement.ArrowButton.Visibility = ElementVisibility.Collapsed;
            this.Status.CommandBarDropDownListElement.ListElement.Visibility = ElementVisibility.Collapsed;
            this.Status.CommandBarDropDownListElement.PopupOpening += CommandBarDropDownListElement_PopupOpening;
            this.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdDocumento.DataBindings.Clear();
            //this.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Folio.DataBindings.Clear();
            this.general.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Serie.DataBindings.Clear();
            this.general.Serie.SetEditable(this.Comprobante.IsEditable);
            this.general.Serie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.NumPedido.DataBindings.Clear();
            this.general.NumPedido.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.FechaEmision.DataBindings.Clear();
            this.general.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.general.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.FechaAutorizacion.DataBindings.Clear();
            this.general.FechaAutorizacion.SetEditable(this.Comprobante.IsEditable);
            this.general.FechaAutorizacion.DataBindings.Add("Value", this.Comprobante, "FechaAutoriza", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.FechaEntrada.DataBindings.Clear();
            this.general.FechaEntrada.SetEditable(this.Comprobante.IsEditable);
            this.general.FechaEntrada.DataBindings.Add("Value", this.Comprobante, "FechaRecepcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.FechaRequerida.DataBindings.Clear();
            this.general.FechaRequerida.SetEditable(this.Comprobante.IsEditable);
            this.general.FechaRequerida.DataBindings.Add("Value", this.Comprobante, "FechaRequerida", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Receptor.Nombre.DataBindings.Clear();
            this.general.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Receptor.RFC.DataBindings.Clear();
            this.general.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Receptor.IdDirectorio.DataBindings.Clear();
            this.general.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdProveedor", true, DataSourceUpdateMode.OnPropertyChanged);
            
            //this.general.CboVendedor.DataBindings.Clear();
            //this.general.CboVendedor.DataBindings.Add("Text", this.Comprobante, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.general.CboVendedor.DataBindings.Add("SelectedValue", this.Comprobante, "IdVendedor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Contacto.DataBindings.Clear();
            this.general.Contacto.SetEditable(this.Comprobante.IsEditable);
            this.general.Contacto.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.MetodoEnvio.DataBindings.Clear();
            this.general.MetodoEnvio.SetEditable(this.Comprobante.IsEditable);
            this.general.MetodoEnvio.DataBindings.Add("Text", this.Comprobante, "MetodoEnvio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.MotivoDescuento.DataBindings.Clear();
            this.general.MotivoDescuento.ReadOnly = true;
            //this.general.MotivoDescuento.DataBindings.Add("Text", this.Comprobante.Descuento, "Motivo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.TipoCambio.DataBindings.Clear();
            this.general.TipoCambio.ReadOnly = !this.Comprobante.IsEditable;
            //this.general.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.Decimales.DataBindings.Clear();
            this.general.Decimales.SetEditable(this.Comprobante.IsEditable);
            this.general.Decimales.DataBindings.Add("Value", this.Comprobante, "Decimales", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.CondicionPago.DataBindings.Clear();
            this.general.CondicionPago.DataBindings.Add("Text", this.Comprobante, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.FormaPago.DataBindings.Clear();
            this.general.FormaPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.MetodoPago.DataBindings.Clear();
            this.general.MetodoPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.general.UsoCFDI.DataBindings.Clear();
            this.general.UsoCFDI.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveUsoCFDI", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.general.Vendedor.DataBindings.Clear();
            this.general.Vendedor.Enabled = false;

            this.general.Moneda.DataBindings.Clear();
            this.general.Moneda.Enabled = false;

            this.general.Embarque.DataBindings.Clear();
            this.general.Embarque.SetEditable(this.Comprobante.IsEditable);
            this.general.Embarque.DataBindings.Add("Text", this.Comprobante, "EntregarEnText", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Relacionado.RFC.DataBindings.Clear();
            //this.Relacionado.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Relacionado.ToolBar.TipoRelacion.DataBindings.Clear();
            this.Relacionado.ToolBar.TipoRelacion.DataBindings.Add("SelectedValue", this.Comprobante.OrdenRelacionada, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Relacionado.ToolBar.TipoRelacion.DataBindings.Add("Text", this.Comprobante.OrdenRelacionada, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Relacionado.Comprobantes.DataSource = this.Comprobante.OrdenRelacionada.Relacionado;

            this.general.ReadOnly = !this.Comprobante.IsEditable;
            this.Guardar.Enabled = this.Comprobante.IsEditable;
        }

        private void CommandBarDropDownListElement_PopupOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        private void CboDireccion_SelectedValueChanged(object sender, EventArgs e) {
            //var rowInfo = this.general.Embarque.SelectedItem as GridViewRowInfo;
            //if (!(rowInfo == null)) {
            //    var seleccionado = rowInfo.DataBoundItem as DomicilioFiscal;
            //    if (seleccionado != null) {
            //        this.Comprobante.EntregarEn = seleccionado;
            //    }
            //}
        }

        private void GetComprobante() {
            this.Comprobante = this.Service.GetComprobante(this.Comprobante.Id);
        }

        private void SetGuardar() {
            this.Comprobante = this.Service.Save(this.Comprobante);
            this.Actualizar.PerformClick();
        }

        private void OnLoad1_DoWork(object sender, DoWorkEventArgs e) {
            this.Status.DataSource = OrdenCompraService.GetStatus();
            this.general.Preparar.RunWorkerAsync();
        }

        private void OnLoad1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.general.Embarque.DisplayMember = "Domicilio";
            this.general.Embarque.ValueMember = "Domicilio";
            this.general.Embarque.DataSource = this.Service.Configuracion.Domicilios;
            this.general.Embarque.SelectedValue = null;
            this.general.Embarque.SelectedIndex = -1;
            this.general.Embarque.SelectedValueChanged += CboDireccion_SelectedValueChanged;
        }
        #endregion
    }
}
