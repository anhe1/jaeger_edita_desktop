﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class ProductoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn1 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewImageColumn gridViewImageColumn1 = new Telerik.WinControls.UI.GridViewImageColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductoCatalogoForm));
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarProducto = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonNuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonRemover = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarSplitButton();
            this.ToolBarButtonActualizar_Activos = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonExportar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonHerramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ToolBarButtonCatalogoFix = new Telerik.WinControls.UI.RadMenuItem();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.radGridProductos = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridModelos = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarModelo = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelModelos = new Telerik.WinControls.UI.CommandBarLabel();
            this.ToolBarSeparadorModelos = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonQuitar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltrar = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonAutorizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonImagen = new Telerik.WinControls.UI.CommandBarButton();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.ragGridMedios = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridProveedor = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar3 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBarProveedor = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelProveedores = new Telerik.WinControls.UI.CommandBarLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProductos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridModelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridModelos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProveedor.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1325, 30);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarProducto});
            // 
            // ToolBarProducto
            // 
            this.ToolBarProducto.DisplayName = "Productos";
            this.ToolBarProducto.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonNuevo,
            this.ToolBarButtonRemover,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonExportar,
            this.ToolBarButtonHerramientas,
            this.ToolBarButtonCerrar});
            this.ToolBarProducto.Name = "ToolBarProducto";
            this.ToolBarProducto.StretchHorizontally = true;
            // 
            // ToolBarButtonNuevo
            // 
            this.ToolBarButtonNuevo.DisplayName = "Nuevo";
            this.ToolBarButtonNuevo.DrawText = true;
            this.ToolBarButtonNuevo.Name = "ToolBarButtonNuevo";
            this.ToolBarButtonNuevo.Text = "Nuevo";
            this.ToolBarButtonNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonRemover
            // 
            this.ToolBarButtonRemover.DisplayName = "Remover";
            this.ToolBarButtonRemover.DrawText = true;
            this.ToolBarButtonRemover.Name = "ToolBarButtonRemover";
            this.ToolBarButtonRemover.Text = "Remover";
            this.ToolBarButtonRemover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DefaultItem = null;
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonActualizar_Activos});
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonActualizar_Activos
            // 
            this.ToolBarButtonActualizar_Activos.CheckOnClick = true;
            this.ToolBarButtonActualizar_Activos.IsChecked = true;
            this.ToolBarButtonActualizar_Activos.Name = "ToolBarButtonActualizar_Activos";
            this.ToolBarButtonActualizar_Activos.Text = "Activos";
            this.ToolBarButtonActualizar_Activos.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonExportar
            // 
            this.ToolBarButtonExportar.DisplayName = "Exportar";
            this.ToolBarButtonExportar.DrawText = true;
            this.ToolBarButtonExportar.Name = "ToolBarButtonExportar";
            this.ToolBarButtonExportar.Text = "Exportar";
            this.ToolBarButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonHerramientas
            // 
            this.ToolBarButtonHerramientas.DisplayName = "Herramientas";
            this.ToolBarButtonHerramientas.DrawText = false;
            this.ToolBarButtonHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ToolBarButtonCatalogoFix});
            this.ToolBarButtonHerramientas.Name = "ToolBarButtonHerramientas";
            this.ToolBarButtonHerramientas.Text = "Herramientas";
            this.ToolBarButtonHerramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCatalogoFix
            // 
            this.ToolBarButtonCatalogoFix.Name = "ToolBarButtonCatalogoFix";
            this.ToolBarButtonCatalogoFix.Text = "Corregir";
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGridProductos
            // 
            this.radGridProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridProductos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewMultiComboBoxColumn1.FieldName = "IdCategoria";
            gridViewMultiComboBoxColumn1.HeaderText = "Categoría";
            gridViewMultiComboBoxColumn1.Name = "IdCategoria";
            gridViewMultiComboBoxColumn1.Width = 250;
            gridViewTextBoxColumn1.FieldName = "Nombre";
            gridViewTextBoxColumn1.HeaderText = "Producto o Servicio";
            gridViewTextBoxColumn1.Name = "Nombre";
            gridViewTextBoxColumn1.Width = 300;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.FieldName = "FechaNuevo";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn1.Name = "FechaNuevo";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 75;
            gridViewTextBoxColumn2.FieldName = "Creo";
            gridViewTextBoxColumn2.HeaderText = "Creó";
            gridViewTextBoxColumn2.Name = "Creo";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 65;
            this.radGridProductos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn2});
            this.radGridProductos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridProductos.Name = "radGridProductos";
            this.radGridProductos.Size = new System.Drawing.Size(1325, 205);
            this.radGridProductos.TabIndex = 1;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel4);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1325, 636);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radGridProductos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1325, 205);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.006900222F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -5);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 209);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1325, 216);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.0106157F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 7);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1325, 216);
            this.radSplitContainer2.TabIndex = 3;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radGridModelos);
            this.splitPanel3.Controls.Add(this.radCommandBar2);
            this.splitPanel3.Controls.Add(this.radCollapsiblePanel1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(1325, 216);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2201414F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(311, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radGridModelos
            // 
            this.radGridModelos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridModelos.Location = new System.Drawing.Point(0, 55);
            // 
            // 
            // 
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = false";
            expressionFormattingObject1.Name = "NewCondition";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCheckBoxColumn2.Width = 40;
            gridViewComboBoxColumn1.FieldName = "Tipo";
            gridViewComboBoxColumn1.HeaderText = "Tipo";
            gridViewComboBoxColumn1.Name = "Tipo";
            gridViewComboBoxColumn1.Width = 85;
            gridViewTextBoxColumn3.FieldName = "Codigo";
            gridViewTextBoxColumn3.HeaderText = "Codigo";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Codigo";
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.FieldName = "Marca";
            gridViewTextBoxColumn5.HeaderText = "Marca";
            gridViewTextBoxColumn5.Name = "Marca";
            gridViewTextBoxColumn5.Width = 200;
            gridViewTextBoxColumn6.FieldName = "Especificacion";
            gridViewTextBoxColumn6.HeaderText = "Especificación";
            gridViewTextBoxColumn6.Name = "Especificacion";
            gridViewTextBoxColumn6.Width = 150;
            gridViewComboBoxColumn2.FieldName = "Unidad";
            gridViewComboBoxColumn2.HeaderText = "Unidad";
            gridViewComboBoxColumn2.Name = "Unidad";
            gridViewComboBoxColumn2.Width = 95;
            gridViewCalculatorColumn1.FieldName = "Unitario";
            gridViewCalculatorColumn1.HeaderText = "Unitario";
            gridViewCalculatorColumn1.Name = "Unitario";
            gridViewCalculatorColumn1.Width = 75;
            gridViewComboBoxColumn3.FieldName = "FactorTrasladoIVA";
            gridViewComboBoxColumn3.HeaderText = "Fac. IVA";
            gridViewComboBoxColumn3.Name = "FactorTrasladoIVA";
            gridViewComboBoxColumn3.Width = 65;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "ValorTrasladoIVA";
            gridViewTextBoxColumn7.HeaderText = "% IVA";
            gridViewTextBoxColumn7.Name = "ValorTrasladoIVA";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewCommandColumn1.FieldName = "AutorizadoText";
            gridViewCommandColumn1.HeaderText = "Autorizado";
            gridViewCommandColumn1.Name = "AutorizadoText";
            gridViewCommandColumn1.Width = 75;
            gridViewComboBoxColumn4.DataType = typeof(int);
            gridViewComboBoxColumn4.FieldName = "UnidadXY";
            gridViewComboBoxColumn4.HeaderText = "UnidadXY";
            gridViewComboBoxColumn4.Name = "UnidadXY";
            gridViewComboBoxColumn4.Width = 75;
            gridViewComboBoxColumn5.FieldName = "UnidadZ";
            gridViewComboBoxColumn5.HeaderText = "UnidadZ";
            gridViewComboBoxColumn5.Name = "UnidadZ";
            gridViewComboBoxColumn5.Width = 75;
            gridViewComboBoxColumn6.FieldName = "IdUnidad";
            gridViewComboBoxColumn6.HeaderText = "IdUnidad";
            gridViewComboBoxColumn6.Name = "IdUnidad";
            gridViewComboBoxColumn6.Width = 75;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "Minimo";
            gridViewTextBoxColumn8.HeaderText = "Minimo";
            gridViewTextBoxColumn8.Name = "Minimo";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 65;
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "Maximo";
            gridViewTextBoxColumn9.HeaderText = "Maximo";
            gridViewTextBoxColumn9.Name = "Maximo";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 65;
            gridViewTextBoxColumn10.DataType = typeof(int);
            gridViewTextBoxColumn10.FieldName = "ReOrden";
            gridViewTextBoxColumn10.HeaderText = "ReOrden";
            gridViewTextBoxColumn10.Name = "ReOrden";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 65;
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "Existencia";
            gridViewTextBoxColumn11.HeaderText = "Existencia";
            gridViewTextBoxColumn11.Name = "Existencia";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 65;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "Largo";
            gridViewTextBoxColumn12.HeaderText = "Largo";
            gridViewTextBoxColumn12.Name = "Largo";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 65;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Ancho";
            gridViewTextBoxColumn13.HeaderText = "Ancho";
            gridViewTextBoxColumn13.Name = "Ancho";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 65;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "Alto";
            gridViewTextBoxColumn14.HeaderText = "Alto";
            gridViewTextBoxColumn14.Name = "Alto";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 65;
            gridViewTextBoxColumn15.FieldName = "ClaveUnidad";
            gridViewTextBoxColumn15.HeaderText = "ClaveUnidad";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "ClaveUnidad";
            gridViewTextBoxColumn16.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn16.HeaderText = "ClaveProdServ";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "ClaveProdServ";
            gridViewTextBoxColumn17.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn17.HeaderText = "NoIdentificacion";
            gridViewTextBoxColumn17.Name = "NoIdentificacion";
            gridViewTextBoxColumn17.Width = 120;
            gridViewTextBoxColumn18.FieldName = "Creo";
            gridViewTextBoxColumn18.HeaderText = "Creó";
            gridViewTextBoxColumn18.Name = "Creo";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.Width = 65;
            gridViewDateTimeColumn2.FieldName = "FechaNuevo";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn2.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn2.Name = "FechaNuevo";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 75;
            gridViewTextBoxColumn19.FieldName = "Modifica";
            gridViewTextBoxColumn19.HeaderText = "Modifica";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Modifica";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn20.FieldName = "FechaModifica";
            gridViewTextBoxColumn20.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn20.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "FechaModifica";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGridModelos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn2,
            gridViewCalculatorColumn1,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn7,
            gridViewCommandColumn1,
            gridViewComboBoxColumn4,
            gridViewComboBoxColumn5,
            gridViewComboBoxColumn6,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20});
            this.radGridModelos.MasterTemplate.MultiSelect = true;
            this.radGridModelos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridModelos.Name = "radGridModelos";
            this.radGridModelos.Size = new System.Drawing.Size(953, 161);
            this.radGridModelos.TabIndex = 2;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.radCommandBar2.Size = new System.Drawing.Size(953, 55);
            this.radCommandBar2.TabIndex = 0;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarModelo});
            // 
            // ToolBarModelo
            // 
            this.ToolBarModelo.DisplayName = "Modelos";
            this.ToolBarModelo.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelModelos,
            this.ToolBarSeparadorModelos,
            this.ToolBarButtonAgregar,
            this.ToolBarButtonQuitar,
            this.ToolBarButtonFiltrar,
            this.ToolBarButtonAutorizar,
            this.ToolBarButtonImagen});
            this.ToolBarModelo.Name = "ToolBarModelo";
            this.ToolBarModelo.StretchHorizontally = true;
            // 
            // ToolBarLabelModelos
            // 
            this.ToolBarLabelModelos.DisplayName = "Modelos";
            this.ToolBarLabelModelos.Name = "ToolBarLabelModelos";
            this.ToolBarLabelModelos.Text = "Modelos:";
            // 
            // ToolBarSeparadorModelos
            // 
            this.ToolBarSeparadorModelos.DisplayName = "Separador Modelos";
            this.ToolBarSeparadorModelos.Name = "ToolBarSeparadorModelos";
            this.ToolBarSeparadorModelos.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.DisplayName = "Agregar";
            this.ToolBarButtonAgregar.DrawText = true;
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar";
            this.ToolBarButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonQuitar
            // 
            this.ToolBarButtonQuitar.DisplayName = "Quitar";
            this.ToolBarButtonQuitar.DrawText = true;
            this.ToolBarButtonQuitar.Name = "ToolBarButtonQuitar";
            this.ToolBarButtonQuitar.Text = "Remover";
            this.ToolBarButtonQuitar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonFiltrar
            // 
            this.ToolBarButtonFiltrar.DisplayName = "Filtro";
            this.ToolBarButtonFiltrar.DrawText = true;
            this.ToolBarButtonFiltrar.Name = "ToolBarButtonFiltrar";
            this.ToolBarButtonFiltrar.Text = "Filtro";
            this.ToolBarButtonFiltrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonAutorizar
            // 
            this.ToolBarButtonAutorizar.DisplayName = "Autorizar";
            this.ToolBarButtonAutorizar.DrawText = true;
            this.ToolBarButtonAutorizar.Name = "ToolBarButtonAutorizar";
            this.ToolBarButtonAutorizar.Text = "Autoriza";
            this.ToolBarButtonAutorizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonAutorizar.ToolTipText = "Autorizar precio";
            // 
            // ToolBarButtonImagen
            // 
            this.ToolBarButtonImagen.DisplayName = "Imagen";
            this.ToolBarButtonImagen.DrawText = true;
            this.ToolBarButtonImagen.Name = "ToolBarButtonImagen";
            this.ToolBarButtonImagen.Text = "Imagen";
            this.ToolBarButtonImagen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Left;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(953, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(1304, 0, 372, 216);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.ragGridMedios);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(344, 214);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(372, 216);
            this.radCollapsiblePanel1.TabIndex = 1;
            // 
            // ragGridMedios
            // 
            this.ragGridMedios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ragGridMedios.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ragGridMedios.MasterTemplate.AllowAddNewRow = false;
            this.ragGridMedios.MasterTemplate.AutoGenerateColumns = false;
            gridViewImageColumn1.FieldName = "Imagen";
            gridViewImageColumn1.HeaderText = "Imagen";
            gridViewImageColumn1.ImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            gridViewImageColumn1.Name = "column1";
            gridViewImageColumn1.Width = 300;
            this.ragGridMedios.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewImageColumn1});
            this.ragGridMedios.MasterTemplate.ShowFilteringRow = false;
            this.ragGridMedios.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.ragGridMedios.Name = "ragGridMedios";
            this.ragGridMedios.ShowGroupPanel = false;
            this.ragGridMedios.Size = new System.Drawing.Size(344, 214);
            this.ragGridMedios.TabIndex = 0;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.radGridProveedor);
            this.splitPanel4.Controls.Add(this.radCommandBar3);
            this.splitPanel4.Location = new System.Drawing.Point(0, 429);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(1325, 207);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.003715509F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -2);
            this.splitPanel4.TabIndex = 2;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // radGridProveedor
            // 
            this.radGridProveedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridProveedor.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn21.DataType = typeof(int);
            gridViewTextBoxColumn21.FieldName = "Id";
            gridViewTextBoxColumn21.HeaderText = "Id";
            gridViewTextBoxColumn21.IsVisible = false;
            gridViewTextBoxColumn21.Name = "Id";
            gridViewTextBoxColumn21.VisibleInColumnChooser = false;
            gridViewMultiComboBoxColumn2.FieldName = "IdProveedor";
            gridViewMultiComboBoxColumn2.HeaderText = "Proveedor";
            gridViewMultiComboBoxColumn2.Name = "IdProveedor";
            gridViewMultiComboBoxColumn2.Width = 280;
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.FieldName = "Unitario";
            gridViewTextBoxColumn22.HeaderText = "Unitario";
            gridViewTextBoxColumn22.Name = "Unitario";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 85;
            gridViewTextBoxColumn23.FieldName = "IdModelo";
            gridViewTextBoxColumn23.HeaderText = "IdModelo";
            gridViewTextBoxColumn23.IsVisible = false;
            gridViewTextBoxColumn23.Name = "IdModelo";
            gridViewTextBoxColumn23.VisibleInColumnChooser = false;
            gridViewTextBoxColumn24.FieldName = "Nota";
            gridViewTextBoxColumn24.HeaderText = "Nota";
            gridViewTextBoxColumn24.Name = "Nota";
            gridViewTextBoxColumn24.Width = 150;
            gridViewTextBoxColumn25.FieldName = "FechaNuevo";
            gridViewTextBoxColumn25.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn25.Name = "FechaNuevo";
            gridViewTextBoxColumn25.Width = 85;
            gridViewTextBoxColumn26.FieldName = "Creo";
            gridViewTextBoxColumn26.HeaderText = "Creo";
            gridViewTextBoxColumn26.Name = "Creo";
            gridViewTextBoxColumn26.Width = 85;
            this.radGridProveedor.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn21,
            gridViewMultiComboBoxColumn2,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26});
            this.radGridProveedor.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.radGridProveedor.Name = "radGridProveedor";
            this.radGridProveedor.Size = new System.Drawing.Size(1325, 177);
            this.radGridProveedor.TabIndex = 2;
            // 
            // radCommandBar3
            // 
            this.radCommandBar3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar3.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar3.Name = "radCommandBar3";
            this.radCommandBar3.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar3.Size = new System.Drawing.Size(1325, 30);
            this.radCommandBar3.TabIndex = 3;
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Name = "commandBarRowElement3";
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBarProveedor});
            // 
            // ToolBarProveedor
            // 
            this.ToolBarProveedor.DisplayName = "commandBarStripElement3";
            this.ToolBarProveedor.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelProveedores});
            this.ToolBarProveedor.Name = "ToolBarProveedor";
            this.ToolBarProveedor.StretchHorizontally = true;
            // 
            // ToolBarLabelProveedores
            // 
            this.ToolBarLabelProveedores.DisplayName = "Proveedores";
            this.ToolBarLabelProveedores.Name = "ToolBarLabelProveedores";
            this.ToolBarLabelProveedores.Text = "Proveedores";
            // 
            // ProductoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1325, 666);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radCommandBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Almacén MP: Catálogo de Productos";
            this.Load += new System.EventHandler(this.ProductoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProductos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            this.splitPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridModelos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridModelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ragGridMedios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            this.splitPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProveedor.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarProducto;
        private Telerik.WinControls.UI.RadGridView radGridProductos;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonNuevo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonRemover;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonExportar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarSplitButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView radGridModelos;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarModelo;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonQuitar;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltrar;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadGridView ragGridMedios;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonAutorizar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonImagen;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonActualizar_Activos;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadGridView radGridProveedor;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelModelos;
        private Telerik.WinControls.UI.CommandBarSeparator ToolBarSeparadorModelos;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar3;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBarProveedor;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelProveedores;
        private Telerik.WinControls.UI.CommandBarDropDownButton ToolBarButtonHerramientas;
        private Telerik.WinControls.UI.RadMenuItem ToolBarButtonCatalogoFix;
    }
}