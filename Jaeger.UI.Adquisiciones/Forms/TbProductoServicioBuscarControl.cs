﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class TbProductoServicioBuscarControl : UserControl {
        private bool _ReadOnly = false;

        public TbProductoServicioBuscarControl() {
            InitializeComponent();
        }

        private void ProductoServicioBuscarToolBarControl_Load(object sender, EventArgs e) {
            this.HConcepto.HostedItem = this.Productos.MultiColumnComboBoxElement;
            this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
        }

        [Bindable(true)]
        public bool ReadOnly {
            get { return this._ReadOnly; }
            set { this._ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        private void CreateReadOnly() {
            this.Agregar.Enabled = this._ReadOnly;
            this.Buscar.Enabled = this._ReadOnly;
            this.Remover.Enabled = this._ReadOnly;
            this.Nuevo.Enabled = this._ReadOnly;

            this.Productos.DropDownOpening += EditorControl_DropDownOpening;
            this.Productos.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = !this._ReadOnly;
            this.Productos.MultiColumnComboBoxElement.ArrowButton.Visibility = (this._ReadOnly ? ElementVisibility.Hidden : ElementVisibility.Collapsed);
        }

        private void EditorControl_DropDownOpening(object sender, CancelEventArgs args) {
            args.Cancel = true;
        }
    }
}
