﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.QRCode.Helpers;
using Jaeger.Util;

namespace Jaeger.UI.Adquisiciones.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        protected internal EmbeddedResources localResource = new EmbeddedResources("Jaeger.Domain.Adquisiciones");
        public ReporteForm() : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(OrdenCompraPrinter)) {
                this.CrearOrdenCompra();
            } else if (this.CurrentObject.GetType() == typeof(SolicitudCotizacionPrinter)) {
                this.CrearSolicitudCotizacion();
            }
        }

        private void CrearOrdenCompra() {
            var current = (OrdenCompraPrinter)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Adquisiciones.Reports.OrdenComprav10Reporte.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText());
            this.Procesar();
            this.SetDisplayName("Orden de Compra");
            this.SetParameter("LeyendaPiePagina", current.Configuracion.LeyendaPiePagina);
            this.SetParameter("EntregarEn", current.EntregarEnText);
            this.SetParameter("Concepto", current.Concepto);
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(new List<OrdenCompraPrinter>() { current }));
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearSolicitudCotizacion() {
            var current = (SolicitudCotizacionPrinter)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.Adquisiciones.Reports.SolicitudCotizacionv10Reporte.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("OC: Solicitud de Cotización");
            this.SetParameter("LeyendaPiePagina", current.LeyendaPiePagina);
            this.SetParameter("EntregarEn", current.EntregarEnText);
            this.SetParameter("Concepto", current.Concepto);
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(new List<SolicitudCotizacionPrinter>() { current }));
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }
    }
}
