﻿namespace Jaeger.UI.Adquisiciones.Forms
{
    partial class OrdenCompraBuscarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrdenCompraBuscarForm));
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowFiltro = true;
            this.ToolBar.ShowGuardar = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(881, 30);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Nuevo.Click += this.ToolBar_ButtonNuevo_Click;
            this.ToolBar.Actualizar.Click += this.ToolBar_ButtonActualizar_Click;
            this.ToolBar.Filtro.Click += this.ToolBar_ButtonFiltro_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_ButtonCerrar_Click;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "Folio";
            gridViewTextBoxColumn2.FormatString = "{0:0000#}";
            gridViewTextBoxColumn2.HeaderText = "# Orden";
            gridViewTextBoxColumn2.Name = "Folio";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 65;
            gridViewComboBoxColumn1.FieldName = "Status";
            gridViewComboBoxColumn1.HeaderText = "Status";
            gridViewComboBoxColumn1.Name = "Status";
            gridViewComboBoxColumn1.Width = 75;
            gridViewTextBoxColumn3.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn3.HeaderText = "Proveedor";
            gridViewTextBoxColumn3.Name = "ReceptorNombre";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 240;
            gridViewTextBoxColumn4.FieldName = "FechaEmision";
            gridViewTextBoxColumn4.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn4.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn4.Name = "FechaEmision";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "FechaAutoriza";
            gridViewTextBoxColumn5.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn5.HeaderText = "Fec. Autoriza";
            gridViewTextBoxColumn5.Name = "FechaAutoriza";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.FieldName = "Autoriza";
            gridViewTextBoxColumn6.HeaderText = "Autoriza";
            gridViewTextBoxColumn6.Name = "Autoriza";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.FieldName = "FechaCancela";
            gridViewTextBoxColumn7.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn7.HeaderText = "Fec. Cancela";
            gridViewTextBoxColumn7.Name = "FechaCancela";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.FieldName = "Cancela";
            gridViewTextBoxColumn8.HeaderText = "Cancela";
            gridViewTextBoxColumn8.Name = "Cancela";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Total";
            gridViewTextBoxColumn9.FormatString = "{0:n2}";
            gridViewTextBoxColumn9.HeaderText = "Total";
            gridViewTextBoxColumn9.Name = "Total";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.FieldName = "Creo";
            gridViewTextBoxColumn10.HeaderText = "Creó";
            gridViewTextBoxColumn10.Name = "Creo";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 85;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(881, 344);
            this.GridData.TabIndex = 17;
            // 
            // OrdenCompraBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 374);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrdenCompraBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar Orden de Compra";
            this.Load += new System.EventHandler(this.OrdenCompraBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl ToolBar;
        public Telerik.WinControls.UI.RadGridView GridData;
    }
}
