﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class TbProductoServicioBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.commandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.Productos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ToolBarRow = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.LabelProducto = new Telerik.WinControls.UI.CommandBarLabel();
            this.HConcepto = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.commandBar)).BeginInit();
            this.commandBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Productos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBar
            // 
            this.commandBar.Controls.Add(this.Productos);
            this.commandBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.commandBar.Location = new System.Drawing.Point(0, 0);
            this.commandBar.Name = "commandBar";
            this.commandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.ToolBarRow});
            this.commandBar.Size = new System.Drawing.Size(759, 30);
            this.commandBar.TabIndex = 0;
            // 
            // Productos
            // 
            // 
            // Productos.NestedRadGridView
            // 
            this.Productos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Productos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Productos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Productos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Productos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Productos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Productos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Productos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Productos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Productos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Productos.EditorControl.Name = "NestedRadGridView";
            this.Productos.EditorControl.ReadOnly = true;
            this.Productos.EditorControl.ShowGroupPanel = false;
            this.Productos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Productos.EditorControl.TabIndex = 0;
            this.Productos.Location = new System.Drawing.Point(132, 4);
            this.Productos.Name = "Productos";
            this.Productos.Size = new System.Drawing.Size(331, 20);
            this.Productos.TabIndex = 1;
            this.Productos.TabStop = false;
            // 
            // ToolBarRow
            // 
            this.ToolBarRow.MinSize = new System.Drawing.Size(25, 25);
            this.ToolBarRow.Name = "ToolBarRow";
            this.ToolBarRow.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.LabelProducto,
            this.HConcepto,
            this.Agregar,
            this.Buscar,
            this.Remover,
            this.Nuevo});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // LabelProducto
            // 
            this.LabelProducto.DisplayName = "Etiqueta: Producto / Servicio";
            this.LabelProducto.Name = "LabelProducto";
            this.LabelProducto.Text = "Producto / Servicio:";
            // 
            // HConcepto
            // 
            this.HConcepto.DisplayName = "Producto Servicio";
            this.HConcepto.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.HConcepto.MinSize = new System.Drawing.Size(350, 0);
            this.HConcepto.Name = "HConcepto";
            this.HConcepto.Text = "Combo";
            // 
            // Agregar
            // 
            this.Agregar.DisplayName = "commandBarButton1";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Buscar
            // 
            this.Buscar.DisplayName = "Buscar";
            this.Buscar.DrawText = true;
            this.Buscar.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Remover
            // 
            this.Remover.DisplayName = "Remover";
            this.Remover.DrawText = true;
            this.Remover.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Adquisiciones.Properties.Resources.add_new_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbProductoServicioBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.commandBar);
            this.Name = "TbProductoServicioBuscarControl";
            this.Size = new System.Drawing.Size(759, 30);
            this.Load += new System.EventHandler(this.ProductoServicioBuscarToolBarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.commandBar)).EndInit();
            this.commandBar.ResumeLayout(false);
            this.commandBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Productos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar commandBar;
        private Telerik.WinControls.UI.CommandBarRowElement ToolBarRow;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel LabelProducto;
        private Telerik.WinControls.UI.CommandBarHostItem HConcepto;
        public Telerik.WinControls.UI.CommandBarButton Agregar;
        public Telerik.WinControls.UI.CommandBarButton Buscar;
        public Telerik.WinControls.UI.CommandBarButton Remover;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Productos;
        public Telerik.WinControls.UI.CommandBarButton Nuevo;
    }
}
