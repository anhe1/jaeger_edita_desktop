﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Telerik.WinControls;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class OrdenCompraProveedorControl : UserControl {
        protected IMetodoPagoCatalogo catalogoMetodoPago = new MetodoPagoCatalogo();
        protected IFormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();
        protected IUsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        private bool _ReadOnly = false;

        public OrdenCompraProveedorControl() {
            InitializeComponent();
        }

        private void OrdenCompraProveedorControl_Load(object sender, EventArgs e) {
            this.FechaEmision.SetToNullValue();
            this.FechaRequerida.SetToNullValue();
            this.FechaEntrada.SetToNullValue();
            this.FechaAutorizacion.SetToNullValue();

            this.MetodoPago.AutoSizeDropDownToBestFit = true;
            this.MetodoPago.DropDownStyle = RadDropDownStyle.DropDownList;
            this.MetodoPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MetodoPago.EditorControl.MasterTemplate.AllowRowResize = false;

            this.FormaPago.AutoSizeDropDownToBestFit = true;
            this.FormaPago.DropDownStyle = RadDropDownStyle.DropDownList;
            this.FormaPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowRowResize = false;

            this.UsoCFDI.AutoSizeDropDownToBestFit = true;
            this.UsoCFDI.DropDownStyle = RadDropDownStyle.DropDownList;
            this.UsoCFDI.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.UsoCFDI.EditorControl.MasterTemplate.AllowRowResize = false;

            this.CondicionPago.AutoSizeDropDownToBestFit = true;
            this.CondicionPago.DropDownStyle = RadDropDownStyle.DropDownList;
            this.CondicionPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.CondicionPago.EditorControl.MasterTemplate.AllowRowResize = false;

            this.MetodoEnvio.AutoSizeDropDownToBestFit = true;
            this.MetodoEnvio.DropDownStyle = RadDropDownStyle.DropDownList;
            this.MetodoEnvio.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MetodoEnvio.EditorControl.MasterTemplate.AllowRowResize = false;
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set { this._ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        private void CreateReadOnly() {
            this.Receptor.ReadOnly = this._ReadOnly;
            this.NumPedido.ReadOnly = this._ReadOnly;
            if (this._ReadOnly) {
                this.FormaPago.DropDownOpening += EditorControl_DropDownOpening;
                this.FormaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.FormaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;

                this.MetodoPago.DropDownOpening += EditorControl_DropDownOpening;
                this.MetodoPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.MetodoPago.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;

                this.UsoCFDI.DropDownOpening += EditorControl_DropDownOpening;
                this.UsoCFDI.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.UsoCFDI.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;

                this.CondicionPago.DropDownOpening += EditorControl_DropDownOpening;
                this.CondicionPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.CondicionPago.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            } else {
                this.FormaPago.DropDownOpening -= EditorControl_DropDownOpening;
                this.FormaPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = false;
                this.FormaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;

                this.MetodoPago.DropDownOpening -= EditorControl_DropDownOpening;
                this.MetodoPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.MetodoPago.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;

                this.UsoCFDI.DropDownOpening -= EditorControl_DropDownOpening;
                this.UsoCFDI.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.UsoCFDI.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;

                this.CondicionPago.DropDownOpening -= EditorControl_DropDownOpening;
                this.CondicionPago.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = false;
                this.CondicionPago.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;
            }
        }

        private void EditorControl_DropDownOpening(object sender, CancelEventArgs args) {
            args.Cancel = true;
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            this.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor;
            this.FormaPago.Enabled = false;
            this.MetodoPago.Enabled = false;
            this.UsoCFDI.Enabled = false;
            this.MetodoEnvio.Enabled = false;
            this.catalogoMetodoPago.Load();
            this.catalogoFormaPago.Load();
            this.catalogoUsoCfdi.Load();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // catalogo formas de pago
            this.FormaPago.DataSource = this.catalogoFormaPago.Items;
            this.FormaPago.SelectedIndex = -1;
            this.FormaPago.SelectedValue = null;

            // catalogo metodos de pago
            this.MetodoPago.DataSource = this.catalogoMetodoPago.Items;
            this.MetodoPago.SelectedIndex = -1;
            this.MetodoPago.SelectedValue = null;

            // catalogo uso de cfdi
            this.UsoCFDI.DataSource = catalogoUsoCfdi.Items;

            this.MetodoEnvio.DisplayMember = "Descriptor";
            this.MetodoEnvio.DataSource = SolicitudCotizacionService.GetMetodoEnvio();

            this.CondicionPago.DisplayMember = "Descriptor";
            this.CondicionPago.DataSource = SolicitudCotizacionService.GetCondiciones();

            this.FormaPago.Enabled = true;
            this.MetodoPago.Enabled = true;
            this.UsoCFDI.Enabled = true;
            this.MetodoEnvio.Enabled = true;
        }
    }
}
