﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class SolicitudCotizacionForm : RadForm {
        public SolicitudCotizacionForm() {
            InitializeComponent();
        }

        public SolicitudCotizacionForm(SolicitudCotizacionDetailModel model) {
            InitializeComponent();
            this.TSolicitud.Comprobante = model;
        }

        private void SolicitudCotizacionForm_Load(object sender, EventArgs e) {
            this.gridConceptos.Standard();
            this.gridConceptos.Columns.AddRange(GridSolicitudCotizacion.GetConceptos());
            this.TConcepto.Nuevo.Click += this.TConceptos_Nuevo_Click;
            this.TConcepto.Remover.Click += this.TConceptos_Remover_Click;
            this.TConcepto.Buscar.Click += this.TConceptos_Buscar_Click;
            this.TSolicitud.Cerrar.Click += this.Cerrar_Click;
            this.TSolicitud.Init();
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TConceptos_Nuevo_Click(object sender, EventArgs e) {
            if (this.TSolicitud.Comprobante.Conceptos == null) {
                this.TSolicitud.Comprobante.Conceptos = new BindingList<SolicitudCotizacionConceptoModel>();
            }
            this.TSolicitud.Comprobante.Conceptos.Add(new SolicitudCotizacionConceptoModel());
        }

        private void TConceptos_Remover_Click(object sender, EventArgs e) {
            if (this.gridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Message_Objeto_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.gridConceptos.CurrentRow.DataBoundItem as SolicitudCotizacionConceptoModel;
                    if (seleccionado.IdConcepto == 0) {
                        try {
                            this.TSolicitud.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.gridConceptos.Rows.Count == 0) {
                                this.TSolicitud.Comprobante.Conceptos = new BindingList<SolicitudCotizacionConceptoModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.gridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        private void TConceptos_Buscar_Click(object sender, EventArgs e) {
            var _buscar = new Almacen.Forms.ProductoModeloBuscarForm(AlmacenEnum.MP);
            _buscar.Seleccionar += Buscar_Seleccionar;
            _buscar.ShowDialog(this);
        }

        private void Buscar_Seleccionar(object sender, Domain.Almacen.Entities.ProductoServicioModeloModel e) {
            if (e != null) {
                var nuevo = new SolicitudCotizacionConceptoModel {
                    Activo = true,
                    IdAlmacen = e.IdAlmacen,
                    IdModelo = e.IdModelo,
                    IdProducto = e.IdProducto,
                    Creo = ConfigService.Piloto.Clave,
                    Producto = e.Nombre,
                    Descripcion = e.Descripcion,
                    Marca = e.Marca,
                    Unidad = e.Unidad,
                    Especificacion = e.Especificacion,
                    IdUnidad = e.IdUnidad,
                    FechaNuevo = DateTime.Now,
                };

                this.TSolicitud.Comprobante.Conceptos.Add(nuevo);
            }
        }

        private void TSolicitud_BindingCompleted(object sender, EventArgs e) {
            this.gridConceptos.DataSource = this.TSolicitud.Comprobante.Conceptos;
            this.TConcepto.ReadOnly = this.TSolicitud.Comprobante.IsEditable;
            this.gridConceptos.AllowEditRow = this.TSolicitud.Comprobante.IsEditable;
        }
    }
}
