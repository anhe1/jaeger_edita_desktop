﻿namespace Jaeger.UI.Adquisiciones.Forms {
    partial class OrdenCompraConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrdenCompraConfigForm));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txbConcepto = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txbPiePagina = new Telerik.WinControls.UI.RadTextBox();
            this.checkImporteConLetra = new Telerik.WinControls.UI.RadCheckBox();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridDireccion = new Telerik.WinControls.UI.RadGridView();
            this.toolBarStandarControl2 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPiePagina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkImporteConLetra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDireccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDireccion.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(57, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Concepto:";
            // 
            // txbConcepto
            // 
            this.txbConcepto.AutoSize = false;
            this.txbConcepto.Location = new System.Drawing.Point(3, 27);
            this.txbConcepto.Multiline = true;
            this.txbConcepto.Name = "txbConcepto";
            this.txbConcepto.Size = new System.Drawing.Size(611, 82);
            this.txbConcepto.TabIndex = 2;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(0, 115);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(76, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Pie de Página:";
            // 
            // txbPiePagina
            // 
            this.txbPiePagina.AutoSize = false;
            this.txbPiePagina.Location = new System.Drawing.Point(3, 139);
            this.txbPiePagina.Multiline = true;
            this.txbPiePagina.Name = "txbPiePagina";
            this.txbPiePagina.Size = new System.Drawing.Size(611, 91);
            this.txbPiePagina.TabIndex = 2;
            // 
            // checkImporteConLetra
            // 
            this.checkImporteConLetra.Location = new System.Drawing.Point(3, 236);
            this.checkImporteConLetra.Name = "checkImporteConLetra";
            this.checkImporteConLetra.Size = new System.Drawing.Size(117, 18);
            this.checkImporteConLetra.TabIndex = 3;
            this.checkImporteConLetra.Text = "Incluir total en letra";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 30);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(638, 305);
            this.radPageView1.TabIndex = 4;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radLabel1);
            this.radPageViewPage1.Controls.Add(this.checkImporteConLetra);
            this.radPageViewPage1.Controls.Add(this.txbConcepto);
            this.radPageViewPage1.Controls.Add(this.txbPiePagina);
            this.radPageViewPage1.Controls.Add(this.radLabel2);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(55F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(617, 257);
            this.radPageViewPage1.Text = "General";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.GridDireccion);
            this.radPageViewPage2.Controls.Add(this.toolBarStandarControl2);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(125F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(617, 257);
            this.radPageViewPage2.Text = "Domicilios de Entrega";
            // 
            // GridDireccion
            // 
            this.GridDireccion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDireccion.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridDireccion.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn1.FieldName = "IsActive";
            gridViewCheckBoxColumn1.HeaderText = "IsActive";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "IsActive";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Calle";
            gridViewTextBoxColumn2.HeaderText = "Calle";
            gridViewTextBoxColumn2.Name = "Calle";
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.FieldName = "noExterior";
            gridViewTextBoxColumn3.HeaderText = "No. Exterior";
            gridViewTextBoxColumn3.Name = "noExterior";
            gridViewTextBoxColumn4.FieldName = "NoInterior";
            gridViewTextBoxColumn4.HeaderText = "No. Interior";
            gridViewTextBoxColumn4.Name = "NoInterior";
            gridViewTextBoxColumn5.FieldName = "Colonia";
            gridViewTextBoxColumn5.HeaderText = "Colonia";
            gridViewTextBoxColumn5.Name = "Colonia";
            gridViewTextBoxColumn5.Width = 150;
            gridViewTextBoxColumn6.FieldName = "CodigoPostal";
            gridViewTextBoxColumn6.HeaderText = "CodigoPostal";
            gridViewTextBoxColumn6.Name = "CodigoPostal";
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.FieldName = "Municipio";
            gridViewTextBoxColumn7.HeaderText = "Municipio";
            gridViewTextBoxColumn7.Name = "Municipio";
            gridViewTextBoxColumn7.Width = 150;
            gridViewTextBoxColumn8.FieldName = "Ciudad";
            gridViewTextBoxColumn8.HeaderText = "Ciudad";
            gridViewTextBoxColumn8.Name = "Ciudad";
            gridViewTextBoxColumn8.Width = 150;
            gridViewTextBoxColumn9.FieldName = "Estado";
            gridViewTextBoxColumn9.HeaderText = "Estado";
            gridViewTextBoxColumn9.Name = "Estado";
            gridViewTextBoxColumn9.Width = 150;
            gridViewTextBoxColumn10.FieldName = "Pais";
            gridViewTextBoxColumn10.HeaderText = "País";
            gridViewTextBoxColumn10.Name = "Pais";
            gridViewTextBoxColumn11.FieldName = "Localidad";
            gridViewTextBoxColumn11.HeaderText = "Localidad";
            gridViewTextBoxColumn11.Name = "Localidad";
            gridViewTextBoxColumn11.Width = 85;
            gridViewTextBoxColumn12.FieldName = "Referencia";
            gridViewTextBoxColumn12.HeaderText = "Referencia";
            gridViewTextBoxColumn12.Name = "Referencia";
            gridViewTextBoxColumn12.Width = 85;
            gridViewTextBoxColumn13.FieldName = "Telefono";
            gridViewTextBoxColumn13.HeaderText = "Teléfono";
            gridViewTextBoxColumn13.Name = "Telefono";
            gridViewTextBoxColumn13.Width = 95;
            this.GridDireccion.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.GridDireccion.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridDireccion.Name = "GridDireccion";
            this.GridDireccion.ShowGroupPanel = false;
            this.GridDireccion.Size = new System.Drawing.Size(617, 227);
            this.GridDireccion.TabIndex = 2;
            // 
            // toolBarStandarControl2
            // 
            this.toolBarStandarControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl2.Etiqueta = "";
            this.toolBarStandarControl2.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl2.Name = "toolBarStandarControl2";
            this.toolBarStandarControl2.ShowActualizar = false;
            this.toolBarStandarControl2.ShowAutorizar = false;
            this.toolBarStandarControl2.ShowCerrar = false;
            this.toolBarStandarControl2.ShowEditar = false;
            this.toolBarStandarControl2.ShowFiltro = true;
            this.toolBarStandarControl2.ShowGuardar = false;
            this.toolBarStandarControl2.ShowHerramientas = false;
            this.toolBarStandarControl2.ShowImagen = false;
            this.toolBarStandarControl2.ShowImprimir = false;
            this.toolBarStandarControl2.ShowNuevo = true;
            this.toolBarStandarControl2.ShowRemover = true;
            this.toolBarStandarControl2.Size = new System.Drawing.Size(617, 30);
            this.toolBarStandarControl2.TabIndex = 3;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = false;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(638, 30);
            this.ToolBar.TabIndex = 5;
            this.ToolBar.Guardar.Click += this.ToolBar_ButtonGuardar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_ButtonCerrar_Click;
            // 
            // OrdenCompraConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 335);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.ToolBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrdenCompraConfigForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Orden de Compra: Configuración ";
            this.Load += new System.EventHandler(this.ViewOrdenCompraConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbPiePagina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkImporteConLetra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage1.PerformLayout();
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDireccion.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDireccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox txbConcepto;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txbPiePagina;
        private Telerik.WinControls.UI.RadCheckBox checkImporteConLetra;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        internal Telerik.WinControls.UI.RadGridView GridDireccion;
        private Common.Forms.ToolBarStandarControl ToolBar;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl2;
    }
}
