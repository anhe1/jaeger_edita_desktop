﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Adquisiciones.Services;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Adquisiciones.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.UI.Adquisiciones.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Adquisiciones.Forms {
    public partial class OrdenCompraBaseForm : RadForm {
        protected IOrdenCompraService service;
        protected internal RadMenuItem Nuevo = new RadMenuItem { Text = "Orden de Compra" };
        
        protected internal RadMenuItem Configuracion = new RadMenuItem { Text = "Configuración" };
        private BindingList<OrdenCompraSingleModel> datos;

        public OrdenCompraBaseForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.GOrdenCompra.Permisos = new UIAction(menuElement.Permisos);
        }

        private void OrdenCompraCatalogoForm_Load(object sender, EventArgs e) {
            this.GOrdenCompra.GridData.AllowEditRow = true;
            this.GOrdenCompra.GridData.Columns.AddRange(GridOrdenCompraService.GetColumnsBase());
            this.service = new OrdenCompraService();

            this.Nuevo.Click += Nuevo_Click;
            this.Configuracion.Click += Configuracion_Click;

            this.GOrdenCompra.Nuevo.Items.Add(Nuevo);
            
            this.GOrdenCompra.Herramientas.Items.Add(Configuracion);

            this.GOrdenCompra.Actualizar.Click += this.Actualizar_Click;
            this.GOrdenCompra.Editar.Click += this.Editar_Click;
            this.GOrdenCompra.Cancelar.Click += this.Cancelar_Click;
            this.GOrdenCompra.ContextEditar.Click += this.Editar_Click;
            this.GOrdenCompra.Imprimir.Click += this.Imprimir_Click;
            this.GOrdenCompra.ContextImprimir.Click += this.Imprimir_Click;
            this.GOrdenCompra.Cerrar.Click += this.Cerrar_Click;

            this.GOrdenCompra.GridData.CellBeginEdit += this.GridData_CellBeginEdit;
            this.GOrdenCompra.GridData.CellEndEdit += this.GridData_CellEndEdit;
            this.GOrdenCompra.GridData.CellValidating += this.GridData_CellValidating;
        }

        #region barra de herramientas
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new OrdenCompraForm() { MdiParent = this.ParentForm };
            _nuevo.Show();
        }

        private void Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.GOrdenCompra.GridData.CurrentRow.DataBoundItem as OrdenCompraSingleModel;
            if (seleccionado != null) {
                var _form = new OrdenCompraForm(new OrdenCompraDetailModel { Id = seleccionado.Id }) { MdiParent = this.ParentForm };
                _form.Show();
            }
        }

        private void Cancelar_Click(object sender, EventArgs eventArgs) {
            var seleccionado = this.GOrdenCompra.GetCurrent<OrdenCompraSingleModel>();
            if (seleccionado != null) {
                if (seleccionado.IdStatus != 0) {
                    if (RadMessageBox.Show(this, string.Format(Properties.Resources.Message_Cancelar, seleccionado.Folio), "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        using (var espera = new Waiting2Form(this.Cancelar)) {
                            espera.ShowDialog(this);
                        }
                    }
                }
            }
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultado ...";
                espera.ShowDialog(this);
            }
            this.GOrdenCompra.GridData.DataSource = this.datos;
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            if (this.GOrdenCompra.GridData.CurrentRow != null) {
                var seleccion = this.GOrdenCompra.GridData.CurrentRow.DataBoundItem as OrdenCompraSingleModel;
                if (seleccion != null) {
                    if (seleccion.Status != OrdenCompraStatusEnum.Requerida) {
                        using (var espera = new Waiting1Form(this.GetComprobante)) {
                            espera.Text = "Obteniendo comprobante ...";
                            espera.ShowDialog(this);
                        }

                        if (this.Tag != null) {
                            var seleccionado = this.Tag as OrdenCompraDetailModel;
                            if (seleccionado != null) {
                                var _imprimir = new ReporteForm(new OrdenCompraPrinter(seleccionado));
                                _imprimir.Show();
                            }
                        }
                    } else {
                        RadMessageBox.Show(this, Properties.Resources.Message_ErrorComprobanteNoAutorizado, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                }
            }
            this.Tag = null;
        }

        private void Configuracion_Click(object sender, EventArgs e) {
            var _configuracion = new OrdenCompraConfigForm();
            _configuracion.ShowDialog(this);
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        private void ContextMenuDuplicar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.GetDuplicar)) {
                espera.Text = "Duplciando comprobante ...";
                espera.ShowDialog(this);
            }

            if (this.Tag != null) {
                var seleccionado = this.Tag as OrdenCompraDetailModel;
                if (seleccionado != null) {
                    var _form = new OrdenCompraForm(seleccionado) { MdiParent = this.ParentForm };
                    _form.Show();
                }
            }
            this.Tag = null;
        }

        #endregion

        #region acciones del grid
        private void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    var seleccionado = this.GOrdenCompra.GridData.ReturnRowSelected() as OrdenCompraSingleModel;
                    if (seleccionado != null) {
                        switch (seleccionado.Status) {
                            case OrdenCompraStatusEnum.Cancelado:
                                e.Cancel = true;
                                break;
                        }
                    }
                }
            }
        }

        private void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if (e.Column.Tag != null) {
                        if ((OrdenCompraStatusEnum)e.Column.Tag == OrdenCompraStatusEnum.Cancelado) {
                            using (var espera = new Waiting1Form(this.Cancelar)) {
                                espera.Text = "Aplicando cambios ...";
                                espera.ShowDialog(this);
                            }
                            if (this.GOrdenCompra.Tag != null) {
                                if ((bool)this.GOrdenCompra.Tag == false) {
                                    MessageBox.Show("Error");
                                    this.GOrdenCompra.Tag = null;
                                }
                            }
                        } else if ((OrdenCompraStatusEnum)e.Column.Tag == OrdenCompraStatusEnum.Autorizada) {
                            using (var espera = new Waiting1Form(this.Autorizar)) {
                                espera.Text = "Autorizando ...";
                                espera.ShowDialog(this);
                            }
                        }
                        e.Column.Tag = null;
                    }
                }
            }
        }

        private void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                        } else {
                            if ((int)e.Value == (int)OrdenCompraStatusEnum.Cancelado) {
                                if (RadMessageBox.Show(this, Properties.Resources.Message_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                                    e.Cancel = true;
                                    this.GOrdenCompra.GridData.CancelEdit();
                                    return;
                                } else {
                                    if ((int)e.OldValue != (int)OrdenCompraStatusEnum.Cancelado) {
                                        e.Cancel = false;
                                        e.Column.Tag = OrdenCompraStatusEnum.Cancelado;
                                    }
                                }
                            } else if ((int)e.Value == (int)OrdenCompraStatusEnum.Autorizada) {
                                e.Cancel = false;
                                e.Column.Tag = OrdenCompraStatusEnum.Autorizada;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        private void Consultar() {
            this.datos = this.service.GetList(this.GOrdenCompra.GetEjercicio(), this.GOrdenCompra.GetPeriodo());
        }

        private void Cancelar() {
            var seleccionado = this.GOrdenCompra.GetCurrent<OrdenCompraModel>();
            if (seleccionado != null) {
                this.service.Cancelar(seleccionado);
            }
        }

        private void Autorizar() {
            var seleccionado = this.GOrdenCompra.GetCurrent<OrdenCompraSingleModel>();
            if (seleccionado != null) {
                seleccionado.Autoriza = ConfigService.Piloto.Clave;
                seleccionado.FechaAutoriza = DateTime.Now;
                this.service.Save(seleccionado);
            }
        }

        private void GetComprobante() {
            var seleccionado = this.GOrdenCompra.GetCurrent<OrdenCompraSingleModel>();
            var editar = this.service.GetComprobante(seleccionado.Id);
            if (editar != null) {
                this.Tag = editar;
            } else {
                this.Tag = null;
            }
        }

        private void GetDuplicar() {
            var seleccionado = this.GOrdenCompra.GetCurrent<OrdenCompraSingleModel>();
            var editar = this.service.GetComprobante(seleccionado.Id);
            if (editar != null) {
                editar.Clonar();
                this.Tag = editar;
            } else {
                this.Tag = null;
            }
        }
    }
}
