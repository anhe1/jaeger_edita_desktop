﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Adquisiciones.Services {
    public static class GridSolicitudCotizacion {
        public static GridViewDataColumn[] GetColumnsBase() {
            return new GridViewDataColumn[] {
                GridTelerikCommon.ColVersion,
                GridTelerikCommon.ColSerie,
                GridTelerikCommon.ColFolio, GridTelerikCommon.ColStatus, ColReceptorNombre, ColReceptorRFC, ColContacto, ColFechaEmision, ColFechaRequerida, ColMetodoEnvio, ColNota, ColEntregarEnText, GridTelerikCommon.ColCreo
            };
        }

        public static GridViewDataColumn[] GetConceptos() {
            return new GridViewDataColumn[] {
                GridTelerikCommon.ColCheckActivo,
                GridTelerikCommon.ColCantidad,
                ColCboUnidad, ColProducto,
                ColDescripcion, ColEspecificacion, ColMarca, ColNota
            };
        }

        #region columnas para el grid de solicitud de cotizacion
        public static GridViewTextBoxColumn ColIdSolicitud {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdSolicitud",
                    HeaderText = "# Folio",
                    IsVisible = true,
                    Name = "IdSolicitud",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColFolio {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FormatString = GridTelerikCommon.FormarStringFolio,
                    FieldName = "Folio",
                    HeaderText = "# Folio",
                    IsVisible = true,
                    Name = "Folio",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaRequerida {
            get {
                return new GridViewTextBoxColumn {
                    FormatString = Common.Services.GridTelerikCommon.FormatStringDate,
                    FieldName = "FechaRequerida",
                    HeaderText = "Fecha\r\nRequerida",
                    Name = "FechaRequerida",
                    ReadOnly = true,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColReceptorNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorNombre",
                    HeaderText = "Proveedor",
                    Name = "ReceptorNombre",
                    ReadOnly = true,
                    Width = 240
                };
            }
        }

        public static GridViewTextBoxColumn ColReceptorRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorRFC",
                    HeaderText = "RFC",
                    Name = "ReceptorRFC",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaEmision {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "FechaEmision",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Emisión",
                    Name = "FechaEmision",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColMetodoEnvio {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "MetodoEnvio",
                    HeaderText = "Método de envío",
                    Name = "MetodoEnvio",
                    ReadOnly = true,
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColContacto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Contacto",
                    HeaderText = "Contacto",
                    Name = "Contacto",
                    ReadOnly = true,
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Nota",
                    Name = "Nota",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColEntregarEnText {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "EntregarEnText",
                    Name = "EntregarEnText",
                    HeaderText = "Entregar en",
                    Width = 200
                };
            }
        }
        #endregion

        #region conceptos para la solicitud de cotizacion

        /// <summary>
        /// Columna ComboBox para unidades (IdUnidad)
        /// </summary>
        public static GridViewComboBoxColumn ColCboUnidad {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdUnidad",
                    HeaderText = "Unidad",
                    Name = "IdUnidad",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 80, DisplayMember = "Descripcion"
                };
            }
        }

        public static GridViewTextBoxColumn ColIdConcepto {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    Name = "IdConcepto",
                    HeaderText = "IdConcepto",
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColIdProducto {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    Name = "IdProducto",
                    HeaderText = "IdProducto",
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColIdModelo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    Name = "IdModelo",
                    HeaderText = "IdModelo",
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColProducto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Producto",
                    HeaderText = "Producto",
                    Name = "Producto",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColDescripcion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Descripcion",
                    HeaderText = "Descripción",
                    Name = "Descripcion",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColEspecificacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Especificacion",
                    HeaderText = "Especificación",
                    Name = "Especificacion",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColMarca {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Marca",
                    HeaderText = "Marca",
                    Name = "Marca",
                    Width = 200
                };
            }
        }
        #endregion
    }
}
