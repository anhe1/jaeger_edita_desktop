﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Adquisiciones.Services;

namespace Jaeger.UI.Adquisiciones.Services {
    public static class GridOrdenCompraService {
        public static GridViewDataColumn[] GetColumnsBase() {
            return new GridViewDataColumn[] { ColFolio, ColComboStatus, ColReceptorNombre, ColReceptorRFC, ColFechaEmision, ColFechaAutoriza, ColAutoriza, ColFechaRecepcion, ColFechaCancela, ColCancela };
        }

        public static GridViewDataColumn[] GetColumnsTotales() {
            return new GridViewDataColumn[] { ColSubTotal, ColDescuento, ColTrasladoIVA, ColTrasladoIEPS, ColRetencionIVA, ColRetenicionIEPS, ColRetencionISR, ColTotal, ColAcumulado, ColSaldo };
        }

        public static GridViewDataColumn[] GetConceptos() {
            return new GridViewDataColumn[] {
                ColId, ColActivo, ColAlmacen, ColTipo, ColIdProducto, ColIdModelo, ColIdUnidad, ColOrdenProduccion, ColCantidad, ColUnidad, ColNombre, ColDescripcion, ColEspecificacion, ColMarca, ColNota, ColNoIdentificacion,
                ColUnitarioBase, ColUnitario, ColImporte, ColDescuento, ColSubTotal
            };
        }

        #region orden de compra
        public static GridViewTextBoxColumn ColId {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColFolio {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Folio",
                    FormatString = "{0:0000#}",
                    HeaderText = "# Orden",
                    Name = "Folio",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 65
                };
            }
        }

        public static GridViewComboBoxColumn ColComboStatus {
            get {
                var cboStatus = new GridViewComboBoxColumn {
                    FieldName = "Status",
                    HeaderText = "Status",
                    Name = "Status",
                    Width = 75
                };

                cboStatus.DataSource = OrdenCompraService.GetStatus();
                cboStatus.DisplayMember = "Descripcion";
                cboStatus.ValueMember = "Id";
                return cboStatus;
            }
        }

        public static GridViewTextBoxColumn ColReceptorNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorNombre",
                    HeaderText = "Proveedor",
                    Name = "ReceptorNombre",
                    ReadOnly = true,
                    Width = 240
                };
            }
        }

        public static GridViewTextBoxColumn ColReceptorRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorRFC",
                    HeaderText = "RFC",
                    Name = "ReceptorRFC",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaEmision {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "FechaEmision",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Emisión",
                    Name = "FechaEmision",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaAutoriza {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "FechaAutoriza",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Autoriza",
                    Name = "FechaAutoriza",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColAutoriza {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Autoriza",
                    HeaderText = "Autoriza",
                    Name = "Autoriza",
                    ReadOnly = true,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaRecepcion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "FechaRecepcion",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Recepción",
                    Name = "FechaRecepcion",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaCancela {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "FechaCancela",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Cancela",
                    Name = "FechaCancela",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColCancela {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Cancela",
                    HeaderText = "Cancela",
                    Name = "Cancela",
                    ReadOnly = true,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColDescuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Descuento",
                    FormatString = "{0:n2}",
                    HeaderText = "Descuento",
                    Name = "Descuento",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColSubTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "SubTotal",
                    FormatString = "{0:n2}",
                    HeaderText = "SubTotal",
                    Name = "SubTotal",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTrasladoIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TrasladoIVA",
                    FormatString = "{0:n2}",
                    HeaderText = "Tras. IVA",
                    Name = "TrasladoIVA",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTrasladoIEPS {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TrasladoIEPS",
                    FormatString = "{0:n2}",
                    HeaderText = "Tras. IEPS",
                    IsVisible = false,
                    Name = "TrasladoIEPS",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColRetencionIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "RetencionIVA",
                    FormatString = "{0:n2}",
                    HeaderText = "Ret. IVA",
                    IsVisible = false,
                    Name = "RetencionIVA",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColRetenicionIEPS {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "RetencionIEPS",
                    FormatString = "{0:n2}",
                    HeaderText = "Ret. IEPS",
                    IsVisible = false,
                    Name = "RetencionIEPS",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColRetencionISR {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "RetencionISR",
                    FormatString = "{0:n2}",
                    HeaderText = "Ret. ISR",
                    IsVisible = false,
                    Name = "RetencionISR",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Total",
                    FormatString = "{0:n2}",
                    HeaderText = "Total",
                    Name = "Total",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColAcumulado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Acumulado",
                    FormatString = "{0:n2}",
                    HeaderText = "Pago",
                    Name = "Acumulado",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColSaldo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Saldo",
                    FormatString = "{0:n2}",
                    HeaderText = "Saldo",
                    Name = "Saldo",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }
        #endregion

        #region conceptos
        public static GridViewCheckBoxColumn ColActivo {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "Activo",
                    HeaderText = "A",
                    IsVisible = false,
                    Name = "Activo",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColSubId {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "SubId",
                    HeaderText = "SubId",
                    IsVisible = false,
                    Name = "SubId",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColAlmacen {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Almacen",
                    HeaderText = "Almacén",
                    IsVisible = false,
                    Name = "Almacen",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColTipo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Tipo",
                    HeaderText = "Tipo",
                    IsVisible = false,
                    Name = "Tipo",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColIdProducto {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdProducto",
                    HeaderText = "IdProducto",
                    IsVisible = false,
                    Name = "IdProducto",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColIdModelo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdModelo",
                    HeaderText = "IdModelo",
                    IsVisible = false,
                    Name = "IdModelo",
                    ReadOnly = true,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColIdUnidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdUnidad",
                    HeaderText = "IdUnidad",
                    IsVisible = false,
                    Name = "IdUnidad",
                    ReadOnly = true,
                    VisibleInColumnChooser = false,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColOrdenProduccion {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "OrdenProduccion",
                    HeaderText = "O. P",
                    Name = "OrdenProduccion"
                };
            }
        }

        public static GridViewTextBoxColumn ColCantidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Cantidad",
                    FormatString = "{0:n}",
                    HeaderText = "Cantidad",
                    Name = "Cantidad",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColUnidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Unidad",
                    HeaderText = "Unidad",
                    Name = "Unidad",
                    ReadOnly = true,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nombre",
                    HeaderText = "Nombre",
                    Name = "Nombre",
                    ReadOnly = true,
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColDescripcion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Descripcion",
                    HeaderText = "Descripción",
                    Name = "Descripcion",
                    ReadOnly = true,
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColEspecificacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Especificacion",
                    HeaderText = "Especificación",
                    Name = "Especificacion",
                    ReadOnly = true,
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColMarca {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Marca",
                    HeaderText = "Marca",
                    Name = "Marca",
                    ReadOnly = true,
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Observaciones",
                    MaxLength = 128,
                    Name = "Nora",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColNoIdentificacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NoIdentificacion",
                    HeaderText = "NoIdentificacion",
                    Name = "NoIdentificacion",
                    ReadOnly = true,
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColUnitarioBase {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "UnitarioBase",
                    FormatString = "{0:n}",
                    HeaderText = "Unit. Auto",
                    Name = "UnitarioBase",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85,
                };
            }
        }

        public static GridViewCalculatorColumn ColUnitario {
            get {
                return new GridViewCalculatorColumn {
                    FieldName = "Unitario",
                    FormatString = "{0:n}",
                    HeaderText = "Unitario",
                    Name = "Unitario",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColImporte {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Importe",
                    FormatString = "{0:n}",
                    HeaderText = "Importe",
                    IsVisible = false,
                    Name = "Importe",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }
        #endregion
    }
}
