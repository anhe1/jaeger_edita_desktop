﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    /// <summary>
    /// Repositorio para notas de descuento (NTDSC)
    /// </summary>
    public interface ISqlNotaDescuentoRepository : IGenericRepository<NotaDescuentoModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        NotaDescuentoDetailModel Save(NotaDescuentoDetailModel model);
        bool Cancelar(NotaDescuentoDetailModel model);
    }
}
