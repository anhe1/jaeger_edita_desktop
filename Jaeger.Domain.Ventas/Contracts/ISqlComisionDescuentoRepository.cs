﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    /// <summary>
    /// comision por descuento al cliente
    /// </summary>
    public interface ISqlComisionDescuentoRepository : IGenericRepository<ComisionDescuentoModel> {
        /// <summary>
        /// lista con condicionales
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1: class, new();

        /// <summary>
        /// almacernar objeto ComisionDescuentoModel
        /// </summary>
        ComisionDescuentoModel Save(ComisionDescuentoModel model);
    }
}
