﻿using System;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface IComisionMultaModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla (CMSNM_ID)
        /// </summary>
        int IdDescuento { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con el catalogo de comisiones
        /// </summary>
        int IdComision { get; set; }

        /// <summary>
        /// obtener o establecer el rango inicial
        /// </summary>
        decimal Inicio { get; set; }

        /// <summary>
        /// obtener o establecer el rango final
        /// </summary>
        decimal Final { get; set; }

        /// <summary>
        /// obtener o establecer el factor a utilizar
        /// </summary>
        decimal Factor { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
    }
}
