﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    /// <summary>
    /// repositorio de status de notas descuento
    /// </summary>
    public interface ISqlNotaDescuentoStatusRepository : IGenericRepository<NotaDescuentoStatusModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        bool Save(NotaDescuentoStatusModel model);
    }
}
