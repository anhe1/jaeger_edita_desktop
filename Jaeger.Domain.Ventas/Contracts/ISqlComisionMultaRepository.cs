﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    /// <summary>
    /// comision por multas a cartera vencida
    /// </summary>
    public interface ISqlComisionMultaRepository : IGenericRepository<ComisionMultaModel> {
        /// <summary>
        /// lista con condicionales
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1: class, new();

        /// <summary>
        /// almacenar objeto ComisionMultaModel
        /// </summary>
        ComisionMultaModel Save(ComisionMultaModel model);
    }
}
