﻿using System;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface INotaDescuentoRelacionModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla (ntdscr_id)
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdNota { get; set; }

        /// <summary>
        /// obtener o establecer indice del departamento (NTDSCR_ALMPT_ID)
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer indice del pedido
        /// </summary>
        int IdPedido { get; set; }

        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        int Folio { get; set; }

        /// <summary>
        /// obtener o establecer serie de control interno
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer fecha de emision 
        /// </summary>
        DateTime FechaEmision { get; set; }

        string Tipo { get; set; }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor del comprobante
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer nombre del receptor
        /// </summary>
        string Receptor { get; set; }

        /// <summary>
        /// obtener o establecer registro federeal de contribuyentes
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer sub total
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer importe del descuento
        /// </summary>
        decimal Descuento { get; set; }

        /// <summary>
        /// obtener o establecer importe total del traslado de IVA
        /// </summary>
        decimal TotalTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el gran total del comprobante (subTotal + Traslado IVA)
        /// </summary>
        decimal GTotal { get; set; }

        /// <summary>
        /// obtener o establecer id de documento
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision y tambien fecha de creacion del registro (_FN)
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_N)
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_M)
        /// </summary>
        string Modifica { get; set; }
    }
}
