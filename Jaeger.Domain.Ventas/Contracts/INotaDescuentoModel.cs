﻿using System;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface INotaDescuentoModel {
        /// <summary>
        /// obtener o establecer indice de tabla (ntdsc_id)
        /// </summary>
        int IdNota { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        int Activo { get; set; }

        int IdStatus { get; set; }

        int IdDirectorio { get; set; }

        int IdMotivo { get; set; }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        int Folio { get; set; }

        string Clave { get; set; }

        string RFC { get; set; }

        string Nombre { get; set; }

        /// <summary>
        /// obtener o establcer el importe de la nota de descuento
        /// </summary>
        decimal Importe { get; set; }

        string Contacto { get; set; }

        string Referencia { get; set; }

        /// <summary>
        /// obtener o establecer la clave y descripcion del motivo de la nota de descuento
        /// </summary>
        string CvMotivo { get; set; }

        decimal SubTotal { get; set; }

        decimal Descuento { get; set; }

        decimal TotalTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el gran total del comprobante (subTotal + Traslado IVA)
        /// </summary>
        decimal GTotal { get; set; }

        #region cancelacion
        string Cancela { get; set; }

        /// <summary>
        /// obtener o establecer fecha de cancelacion
        /// </summary>
        DateTime? FechaCancela { get; set; }
        #endregion

        /// <summary>
        /// obtener o establecer id de documento
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_OBSRV)
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision y tambien fecha de creacion del registro (_FN)
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_N)
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_M)
        /// </summary>
        string Modifica { get; set; }

        DateTime FechaEmision { get; }
    }
}
