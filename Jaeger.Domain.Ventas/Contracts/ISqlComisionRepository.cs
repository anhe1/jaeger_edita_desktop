﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    /// <summary>
    /// catalogo de comisiones
    /// </summary>
    public interface ISqlComisionRepository : IGenericRepository<ComisionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals)where T1 : class, new();

        ComisionDetailModel Save(ComisionDetailModel model);
    }
}
