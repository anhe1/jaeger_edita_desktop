﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface ISqlNotaDescuentoRelacionRepository : IGenericRepository<NotaDescuentoRelacionModel> {
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();

        NotaDescuentoRelacionDetailModel Save(NotaDescuentoRelacionDetailModel model);

        int Saveable(NotaDescuentoRelacionModel item);
    }
}
