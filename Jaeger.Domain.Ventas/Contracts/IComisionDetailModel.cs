﻿using System.ComponentModel;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface IComisionDetailModel : IComisionModel {
        BindingList<ComisionDescuentoModel> Descuento { get; set; }
        BindingList<ComisionMultaModel> Multa { get; set; }

        bool IsVigente { get; }
    }
}
