﻿using System;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface IOrdenClienteModel {
        int Id { get; set; }

        int IdCliente { get; set; }

        int IdStatus { get; set; }

        DateTime? FechaPedido { get; set; }

        string Cliente { get; set; }

        string Contacto { get; set; }

        string Correo { get; set; }

        string Telefono { get; set; }

        string NoPedido { get; set; }

        string CondicionPago { get; set; }

        string MetodoPago { get; set; }

        DateTime? FechaEntrega { get; set; }

        DateTime? FechaRequerida { get; set; }

        DateTime? FechaVoBo { get; set; }

        decimal Importe { get; set; }

        bool Facturar { get; set; }

        string Embarcar { get; set; }

        string Nota { get; set; }

        int IdVendedor { get; set; }

        DateTime FechaNuevo { get; set; }

        string Creo { get; set; }

        DateTime? FechaModifica { get; set; }

        string Modifica { get; set; }
    }
}
