﻿using System;

namespace Jaeger.Domain.Ventas.Contracts {
    public interface IComisionModel {
        int IdComision { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>

        bool Activo { get; set; }

        int Orden { get; set; }

        /// <summary>
        /// obtener o establecer venta minima para la aplicacion de la comision
        /// </summary>
        decimal VentaMinima { get; set; }

        /// <summary>
        /// obtener o establecer venta maxima para la aplicacion de la comision
        /// </summary>
        decimal VentaMaxima { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la comision
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer observaciones 
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer fecha de incio de la vigencia de la comision
        /// </summary>
        DateTime? FechaInicio { get; set; }

        /// <summary>
        /// obtener o establecer fecha de final de la vigencia de la comision
        /// </summary>
        DateTime? FechaFin { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
    }
}
