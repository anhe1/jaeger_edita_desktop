﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Domain.Ventas.Contracts {
    /// <summary>
    /// 
    /// </summary>
    public interface ISqlOrdenClienteRepository : IGenericRepository<OrdenClienteModel> {

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        OrdenClienteDetailModel GetOrden(int index);

        /// <summary>
        /// almacenar Orden de Cliente (orden de compra de cliente)
        /// </summary>
        OrdenClienteDetailModel Save(OrdenClienteDetailModel model);

        bool Create();
    }
}
