﻿namespace Jaeger.Domain.Ventas.Entities {
    public class VentasResumenTipo : Base.Abstractions.BaseSingleModel {
        public VentasResumenTipo() {
        }

        public VentasResumenTipo(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
