﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Ventas.Entities {
    public class NotaDescuentoPrinter : NotaDescuentoDetailModel {
        public NotaDescuentoPrinter() : base() { }

        public NotaDescuentoPrinter(NotaDescuentoDetailModel model) : base() {
            model.MatchAndMap(this);
        }

        public string[] QrText {
            get {
                return new string[] { "||NT", Folio.ToString("#000000"), "|", FechaNuevo.ToShortDateString(), "|", Clave, "|", Nombre, "|", Importe.ToString("0.00"), "|", Creo, "|", IdDocumento, "||" };
            }
        }
    }
}
