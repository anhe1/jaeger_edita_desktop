﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Ventas.Contracts;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// Multas por cartera vencida
    /// </summary>
    public class ComisionMultaModel : BasePropertyChangeImplementation, IComisionMultaModel {
        #region declaraciones
        private int index;
        private bool activo;
        private int idCatalogo;
        private decimal rango1;
        private decimal rango2;
        private decimal factor;
        private string descripcion;
        private DateTime fechaNuevo;
        private string creo;
        private DateTime? fechaModifica;
        private string modifica;
        #endregion

        public ComisionMultaModel() {
            fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla (CMSNM_ID)
        /// </summary>
        [DataNames("CMSNM_ID")]
        public int IdDescuento {
            get { return index; }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("CMSNM_A")]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el catalogo de comisiones
        /// </summary>
        [DataNames("CMSNM_CMSNT_ID")]
        public int IdComision {
            get { return idCatalogo; }
            set {
                idCatalogo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el rango inicial
        /// </summary>
        [DataNames("CMSNM_RNG1")]
        public decimal Inicio {
            get { return rango1; }
            set {
                rango1 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el rango final
        /// </summary>
        [DataNames("CMSNM_RNG2")]
        public decimal Final {
            get { return rango2; }
            set {
                rango2 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor a utilizar
        /// </summary>
        [DataNames("CMSNM_FACTOR")]
        public decimal Factor {
            get { return factor; }
            set {
                factor = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion
        /// </summary>
        [DataNames("CMSNM_NOM")]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CMSNM_FN")]
        public DateTime FechaNuevo {
            get { return fechaNuevo; }
            set {
                fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("CMSNM_USU_N")]
        public string Creo {
            get { return creo; }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("CMSNM_FM")]
        public DateTime? FechaModifica {
            get {
                if (fechaModifica >= new DateTime(1900, 1, 1))
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("CMSND_USU_M")]
        public string Modifica {
            get { return modifica; }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }
    }
}
