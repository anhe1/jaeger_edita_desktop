﻿using System;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Ventas.Contracts;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// relacion de documentos de almacen de notas de descuento
    /// </summary>
    public class NotaDescuentoRelacionModel : BasePropertyChangeImplementation, INotaDescuentoRelacionModel {
        #region declaraciones
        private int _Indice;
        private int idNota;
        private int idNotaAlmacen;
        private int _Folio;
        private int _IdPedido;
        private bool activo;
        private string tipo;
        private string _Clave;
        private string _RFC;
        private string _Receptor;
        private string creo;
        private string modifica;
        private string _IdDocumento;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private decimal _SubTotal;
        private decimal _TrasladoIVA;
        private decimal _GTotal;
        private decimal _Descuento;
        private int _IdTipoComprobante;
        private DateTime _FechaEmision;
        private string _Serie;
        private int _IdDirectorio;
        #endregion

        public NotaDescuentoRelacionModel() {
            activo = true;
            fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla (NTDSCP_id)
        /// </summary>
        [DataNames("NTDSCP_ID")]
        public int Id {
            get { return _Indice; }
            set {
                _Indice = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("NTDSCP_A")]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("NTDSCP_NTDSC_ID")]
        public int IdNota {
            get { return idNota; }
            set {
                idNota = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento (NTDSCP_ALMPT_ID)
        /// </summary>
        [DataNames("NTDSCP_ALMPT_ID")]
        public int IdComprobante {
            get { return idNotaAlmacen; }
            set {
                idNotaAlmacen = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("NTDSCP_DOC_ID")]
        public int IdTipo {
            get { return _IdTipoComprobante; }
            set {
                _IdTipoComprobante = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del pedido
        /// </summary>
        [DataNames("NTDSCP_PDCLN_ID")]
        public int IdPedido {
            get { return _IdPedido; }
            set {
                _IdPedido = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSCP_DRCTR_ID")]
        public int IdDirectorio {
            get { return _IdDirectorio; }
            set {
                _IdDirectorio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        [DataNames("NTDSCP_FOLIO")]
        public int Folio {
            get { return _Folio; }
            set {
                _Folio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer serie de control interno
        /// </summary>
        [DataNames("NTDSCP_SERIE")]
        public string Serie {
            get { return _Serie; }
            set {
                _Serie = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de emision 
        /// </summary>
        [DataNames("NTDSCP_FECEMS")]
        public DateTime FechaEmision {
            get { return _FechaEmision; }
            set {
                _FechaEmision = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSCP_TIPO")]
        public string Tipo {
            get { return tipo; }
            set {
                tipo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor del comprobante
        /// </summary>
        [DataNames("NTDSCP_CLV")]
        public string Clave {
            get { return _Clave; }
            set {
                _Clave = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del receptor
        /// </summary>
        [DataNames("NTDSCP_NOM")]
        public string Receptor {
            get { return _Receptor; }
            set {
                _Receptor = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federeal de contribuyentes
        /// </summary>
        [DataNames("NTDSCP_RFC")]
        public string RFC {
            get { return _RFC; }
            set {
                _RFC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub total
        /// </summary>
        [DataNames("NTDSCP_SBTTL")]
        public decimal SubTotal {
            get { return _SubTotal; }
            set {
                _SubTotal = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe del descuento
        /// </summary>
        [DataNames("NTDSCP_DSCNT")]
        public decimal Descuento {
            get { return _Descuento; }
            set {
                _Descuento = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe total del traslado de IVA
        /// </summary>
        [DataNames("NTDSCP_TRSIVA")]
        public decimal TotalTrasladoIVA {
            get { return _TrasladoIVA; }
            set {
                _TrasladoIVA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el gran total del comprobante (subTotal + Traslado IVA)
        /// </summary>
        [DataNames("NTDSCP_GTOTAL")]
        public decimal GTotal {
            get { return _GTotal; }
            set {
                _GTotal = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer id de documento
        /// </summary>
        [DataNames("NTDSCP_UUID")]
        public string IdDocumento {
            get { return _IdDocumento; }
            set {
                _IdDocumento = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision y tambien fecha de creacion del registro (_FN)
        /// </summary>
        [DataNames("NTDSCP_FN")]
        public DateTime FechaNuevo {
            get { return fechaNuevo; }
            set {
                fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_N)
        /// </summary>
        [DataNames("NTDSCP_USU_N")]
        public string Creo {
            get { return creo; }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_FM)
        /// </summary>
        [DataNames("NTDSCP_FM")]
        public DateTime? FechaModifica {
            get { return fechaModifica; }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_M)
        /// </summary>
        [DataNames("NTDSCP_USU_M")]
        public string Modifica {
            get { return modifica; }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }
    }
}
