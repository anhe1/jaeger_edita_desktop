﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Ventas.Entities {
    [SugarTable("_ordclio", "ventas: ordenes de produccion relacionadas a los conceptos de la orden de cliente")]
    public class OrdenClienteOrdenProduccionModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private int ordenProduccion;
        private bool activo;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;

        /// <summary>
        /// 
        /// </summary>
        public OrdenClienteOrdenProduccionModel() {
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [SugarColumn(ColumnName = "_ordclio_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, IsNullable = false)]
        public int Id {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [SugarColumn(ColumnName = "_ordclio_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set { this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indece del concepto
        /// </summary>
        [SugarColumn(ColumnName = "_ordclio_sbid", ColumnDescription = "indice de relacion con el concepto de orden de cliente", IsNullable = false)]
        public int IdConcepto {
            get { return this.subIndex; }
            set { this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden relacionado 
        /// </summary>
        [SugarColumn(ColumnName = "_ordclio_odp", ColumnDescription = "orden de produccion", DefaultValue = "0", IsNullable = false)]
        public int OrdenProduccion {
            get { return this.ordenProduccion; }
            set { this.ordenProduccion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnDescription = "clave del usuario que crea el registro", ColumnName = "_ordclio_usr_n", Length = 10, IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [SugarColumn(ColumnDescription = "clave del ultimo usuario que modifica el registro", ColumnName = "_ordclio_usr_m", Length = 10, IsNullable = false, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ordclio_fn", ColumnDescription = "fecha del registro en el sistema", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_ordclio_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica > new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
