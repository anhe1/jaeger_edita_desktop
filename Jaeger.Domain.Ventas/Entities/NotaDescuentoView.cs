﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Ventas.Entities {
    public class NotaDescuentoView : NotaDescuentoRelacionDetailModel {
        #region declaraciones
        private int idStatus;
        private int folio;
        private int idMotivo;
        #endregion

        public NotaDescuentoView() : base() { }

        [DataNames("NTDSC_STTS_ID")]
        public int IdStatus {
            get { return idStatus; }
            set {
                idStatus = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSC_CTMTV_ID")]
        public int IdMotivo {
            get { return idMotivo; }
            set {
                idMotivo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        [DataNames("NTDSC_FOLIO")]
        public int NFolio {
            get { return folio; }
            set {
                folio = value;
                OnPropertyChanged();
            }
        }
    }
}
