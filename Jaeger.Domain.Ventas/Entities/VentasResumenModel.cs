﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Ventas.Entities {

    public class VentasResumenModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _PDCLN_VNDDR_ID;
        private int _PDCLN_STTSDCS_ID;
        private int _PDCLN_PDD_ID;
        private decimal _PDCLN_IVA;
        private decimal _PDCLN_SBTTL;
        private decimal _PDCLN_TTL;
        private string _PDCLN_USU_CNCL;
        private DateTime? _PDCLN_FECPED;
        private DateTime? _PDCLN_FCCNCL;
        private DateTime? _PDCLN_FCENTRG;
        private string _MotivoCancelacion;
        private decimal _TotalDescuento;
        private int _RLCPDD_ID;
        private int _RLCPDD_A;
        private int _RLCPDD_CATPRD_ID;
        private int _RLCPDD_CTLGPRDCTS_ID;
        private int _RLCPDD_CTLGMDLS_ID;
        private int _RLCPDD_TAM_ID;
        private int _RLCPDD_UNDD_ID;
        private decimal _RLCPDD_UNDD;
        private decimal _RLCPDD_CNTDD_PDD;
        private decimal _RLCPDD_UNTR;
        private decimal _RLCPDD_COST;
        private decimal _RLCPDD_SBTT;
        private int _IdDirectorio;
        private int _IdPrecio;
        private string _Unidad;
        private string _Catalogo;
        private string _Producto;
        private string _Modelo;
        private string _Marca;
        private string _Especificacion;
        private string _Tamanio;
        private decimal _CostoUnidad;
        private decimal _ValorUnitario;
        private decimal _Descuento;
        private decimal _Importe;
        private decimal _TrasladoIVA;
        private decimal _Total;
        private decimal _TasaIVA;
        private string _Identificador;
        private int folio;
        #endregion

        #region informacion del cliente
        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("PDCLN_ID", "RMSN_FOLIO")]
        public int Folio {
            get { return folio; }
            set {
                folio = value;
                OnPropertyChanged();
            }
        }

        [DataNames("DRCTR_NOM", "RMSN_NOMR")]
        public string Cliente { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del pedido (PDCLN_STTSDCS_ID)
        /// </summary>
        [DataNames("PDCLN_STTS_ID", "RMSN_STTS_ID")]
        public int IdStatus {
            get { return _PDCLN_STTSDCS_ID; }
            set {
                _PDCLN_STTSDCS_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del domicilio de entrega (PDCLN_PDD_ID)
        /// </summary>
        [DataNames("PDCLN_DRCCN_ID", "RMSN_DRCCN_ID")]
        public int IdDireccion {
            get { return _PDCLN_PDD_ID; }
            set {
                _PDCLN_PDD_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_VNDDR_ID)
        /// </summary>
        [DataNames("PDCLN_VNDR_ID", "RMSN_VNDDR_ID")]
        public int IdVendedor {
            get { return _PDCLN_VNDDR_ID; }
            set {
                _PDCLN_VNDDR_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_VNDDR_ID)
        /// </summary>
        [DataNames("PDCLN_USU_N", "RMSN_VNDDR")]
        public string Vendedor { get; set; }

        /// <summary>
        /// obtener o establecer (PDCLN_IVA$)
        /// </summary>
        [DataNames("PDCLN_IVA", "RMSN_TRSIVA")]
        public decimal TotalTrasladoIVA {
            get { return _PDCLN_IVA; }
            set {
                _PDCLN_IVA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_SBTTL$)
        /// </summary>
        [DataNames("PDCLN_SBTTL", "RMSN_SBTTL")]
        public decimal SubTotalPedido {
            get { return _PDCLN_SBTTL; }
            set {
                _PDCLN_SBTTL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer total del descuento aplicado antes de los impuestos
        /// </summary>
        [DataNames("PDCLN_DSCNT", "RMSN_DSCNT")]
        public decimal TotalDescuento {
            get { return _TotalDescuento; }
            set {
                _TotalDescuento = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_TTL$)
        /// </summary>
        [DataNames("PDCLN_TTL", "RMSN_GTOTAL")]
        public decimal TotalPedido {
            get { return _PDCLN_TTL; }
            set {
                _PDCLN_TTL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FECPED)
        /// </summary>
        [DataNames("PDCLN_FCPED", "RMSN_FECEMS")]
        public DateTime? FechaPedido {
            get { return _PDCLN_FECPED; }
            set {
                _PDCLN_FECPED = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_FCENTRG)
        /// </summary>
        [DataNames("PDCLN_FCENTR", "RMSN_FECENT")]
        public DateTime? FechaEntrega {
            get { return _PDCLN_FCENTRG; }
            set {
                _PDCLN_FCENTRG = value;
                OnPropertyChanged();
            }
        }

        #region informacion de la cancelacion
        /// <summary>
        /// obtener o establecer (PDCLN_FCCNCL)
        /// </summary>
        [DataNames("PDCLN_FCCNCL", "RMSN_FCCNCL")]
        public DateTime? FechaCancela {
            get { return _PDCLN_FCCNCL; }
            set {
                _PDCLN_FCCNCL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave y descripcion del motivo de la cancelacion (catalogo)
        /// </summary>
        [DataNames("PDCLN_CLMTV", "RMSN_CVCAN")]
        public string ClaveCancelacion {
            get { return _MotivoCancelacion; }
            set {
                _MotivoCancelacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PDCLN_USU_CNCL)
        /// </summary>
        [DataNames("PDCLN_USU_CNCL", "RMSN_USR_C")]
        public string Cancela {
            get { return _PDCLN_USU_CNCL; }
            set {
                _PDCLN_USU_CNCL = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #endregion

        #region informacion de la partida
        /// <summary>
        /// obtener o establecer indice de la tabla (RLCPDD_ID)
        /// </summary>
        [DataNames("PDCLP_ID", "MVAPT_ID")]
        public int IdPartida {
            get { return _RLCPDD_ID; }
            set {
                _RLCPDD_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_A)
        /// </summary>
        [DataNames("PDCLP_A", "MVAPT_A")]
        public int Activo {
            get { return _RLCPDD_A; }
            set {
                _RLCPDD_A = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_CATPRD_ID)
        /// </summary>
        [DataNames("PDCLP_PDCLN_ID", "RMSN_PDD_ID")]
        public int IdPedido {
            get { return _RLCPDD_CATPRD_ID; }
            set {
                _RLCPDD_CATPRD_ID = value;
                OnPropertyChanged();
            }
        }

        [DataNames("PDCLP_DRCTR_ID", "RMSN_DRCTR_ID")]
        public int IdDirectorio {
            get { return _IdDirectorio; }
            set {
                _IdDirectorio = value;
                OnPropertyChanged();
            }
        }

        [DataNames("PDCLP_CTDPT_ID", "MVAPT_CTDPT_ID")]
        public int IdDepartamento { get; set; }

        [DataNames("PDCLP_CTALM_ID", "MVAPT_CTALM_ID")]
        public int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer (RLCPDD_CTLGPRDCTS_ID)
        /// </summary>
        [DataNames("PDCLP_CTPRD_ID", "MVAPT_CTPRD_ID")]
        public int IdProducto {
            get { return _RLCPDD_CTLGPRDCTS_ID; }
            set {
                _RLCPDD_CTLGPRDCTS_ID = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer (RLCPDD_CTLGMDLS_ID)
        /// </summary>
        [DataNames("PDCLP_CTMDL_ID", "MVAPT_CTMDL_ID")]
        public int IdModelo {
            get { return _RLCPDD_CTLGMDLS_ID; }
            set {
                _RLCPDD_CTLGMDLS_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_TAM_ID)
        /// </summary>
        [DataNames("PDCLP_CTESPC_ID", "MVAPT_CTESPC_ID")]
        public int IdEspecificacion {
            get { return _RLCPDD_TAM_ID; }
            set {
                _RLCPDD_TAM_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_UNDD_ID)
        /// </summary>
        [DataNames("PDCLP_CTUND_ID", "MVAPT_CTUND_ID")]
        public int IdUnidad {
            get { return _RLCPDD_UNDD_ID; }
            set {
                _RLCPDD_UNDD_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de precios asignado
        /// </summary>
        [DataNames("PDCLP_CTPRC_ID", "MVAPT_CTPRC_ID")]
        public int IdPrecio {
            get { return _IdPrecio; }
            set {
                _IdPrecio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_UNDD)
        /// </summary>
        [DataNames("PDCLP_UNDF", "MVAPT_UNDF")]
        public decimal FactorUnidad {
            get { return _RLCPDD_UNDD; }
            set {
                _RLCPDD_UNDD = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre de la unidad
        /// </summary>
        [DataNames("PDCLP_UNDN", "MVAPT_UNDN")]
        public string Unidad {
            get { return _Unidad; }
            set {
                _Unidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_CNTDD_PDD)
        /// </summary>
        [DataNames("PDCLP_CNTD", "MVAPT_CANTS")]
        public decimal Cantidad {
            get { return _RLCPDD_CNTDD_PDD; }
            set {
                _RLCPDD_CNTDD_PDD = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del catalogo de productos
        /// </summary>
        [DataNames("PDCLP_CTCLS", "MVAPT_CTCLS")]
        public string Catalogo {
            get { return _Catalogo; }
            set {
                _Catalogo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del producto
        /// </summary>
        [DataNames("PDCLP_PRDN", "MVAPT_PRDN")]
        public string Producto {
            get { return _Producto; }
            set {
                _Producto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del modelo
        /// </summary>
        [DataNames("PDCLP_MDLN", "MVAPT_MDLN")]
        public string Modelo {
            get { return _Modelo; }
            set {
                _Modelo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la marca
        /// </summary>
        [DataNames("PDCLP_MRC", "MVAPT_MRC")]
        public string Marca {
            get { return _Marca; }
            set {
                _Marca = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la especificacion
        /// </summary>
        [DataNames("PDCLP_ESPC", "MVAPT_ESPC")]
        public string Especificacion {
            get { return _Especificacion; }
            set {
                _Especificacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del tamanio
        /// </summary>
        [DataNames("PDCLP_ESPN", "MVAPT_ESPN")]
        public string Tamanio {
            get { return _Tamanio; }
            set {
                _Tamanio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_COST$)
        /// </summary>
        [DataNames("PDCLP_UNTC", "MVAPT_UNTC")]
        public decimal CostoUnitario {
            get { return _RLCPDD_COST; }
            set {
                _RLCPDD_COST = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo de la unidad
        /// </summary>
        [DataNames("PDCLP_UNDC", "MVAPT_UNDC")]
        public decimal CostoUnidad {
            get { return _CostoUnidad; }
            set {
                _CostoUnidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario por pieza
        /// </summary>
        [DataNames("PDCLP_UNTR", "MVAPT_UNTR")]
        public decimal ValorUnitario {
            get { return _ValorUnitario; }
            set {
                _ValorUnitario = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_UNTR$)
        /// </summary>
        [DataNames("PDCLP_UNTR2", "MVAPT_UNTR2")]
        public decimal Unitario {
            get { return _RLCPDD_UNTR; }
            set {
                _RLCPDD_UNTR = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (RLCPDD_SBTT$)
        /// </summary>
        [DataNames("PDCLP_SBTTL", "MVAPT_SBTTL")]
        public decimal SubTotal {
            get { return _RLCPDD_SBTT; }
            set {
                _RLCPDD_SBTT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estsablecer el importe del descuento aplicable al producto o sevicio 
        /// </summary>
        [DataNames("PDCLP_DESC", "MVAPT_DESC")]
        public decimal Descuento {
            get { return _Descuento; }
            set {
                _Descuento = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del importe de la partida (SubTotal - Descuento)
        /// </summary>
        [DataNames("PDCLP_IMPRT", "MVAPT_IMPRT")]
        public decimal Importe {
            get { return _Importe; }
            set {
                _Importe = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tasa del IVA
        /// </summary>
        [DataNames("PDCLP_TSIVA", "MVAPT_TSIVA")]
        public decimal TasaIVA {
            get { return _TasaIVA; }
            set {
                _TasaIVA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe trasladado del impuesto IVA
        /// </summary>
        [DataNames("PDCLP_TRIVA", "MVAPT_TRIVA")]
        public decimal TrasladoIVA {
            get { return _TrasladoIVA; }
            set {
                _TrasladoIVA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de la partida (Importe + TrasladoIVA)
        /// </summary>
        [DataNames("PDCLP_TOTAL", "MVAPT_TOTAL")]
        public decimal Total {
            get { return _Total; }
            set {
                _Total = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("PDCLP_SKU", "MVAPT_SKU")]
        public string Identificador {
            get { return _Identificador; }
            set {
                _Identificador = value;
                OnPropertyChanged();
            }
        }
        #endregion
    }
}
