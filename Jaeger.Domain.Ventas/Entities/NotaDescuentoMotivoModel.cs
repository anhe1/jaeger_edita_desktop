﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Ventas.Entities {
    public class NotaDescuentoMotivoModel : BaseSingleModel {
        public NotaDescuentoMotivoModel() {
        }

        public NotaDescuentoMotivoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }

    public class NotaDescuentoMotivoCancelacion : BaseSingleModel {
        public NotaDescuentoMotivoCancelacion() { }

        public NotaDescuentoMotivoCancelacion(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
