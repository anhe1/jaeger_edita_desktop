﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// concepto de orden de cliente (_ordclip)
    /// </summary>
    [SugarTable("_ordclip", "ventas: conceptos de la orden de cliente")]
    public class OrdenClienteConceptoModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private int secuencia;
        private int prioridad;
        private bool activo;
        private string noIdentificacion;
        private string unidadMedida;
        private string concepto;
        private string nota;
        private string creo;
        private decimal cantidad;
        private decimal valorUnitario;
        private decimal subTotal;
        private DateTime? fechaEntrega;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;

        public OrdenClienteConceptoModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
            this.fechaEntrega = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice del concepto de la orden de cliente
        /// </summary>
        [SugarColumn(ColumnDescription = "indice de la tabla", ColumnName = "_ordclip_id", IsPrimaryKey = true, IsIdentity = true)]
        public int IdConcepto {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [SugarColumn(ColumnDescription = "registro activo", ColumnName = "_ordclip_a", IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set { this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion de la orden de cliente
        /// </summary>
        [SugarColumn(ColumnDescription = "indice de relacion con la tabla de orden de cliente", ColumnName = "_ordclip_iddir", IsNullable = false)]
        public int IdOrdenCliente {
            get { return this.subIndex; }
            set { this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "prioridad del concepto", ColumnName = "_ordclip_sec", DefaultValue = "0", IsNullable = false)]
        public int Secuencia {
            get { return this.secuencia; }
            set { this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "prioridad del concepto", ColumnName = "_ordclip_pri", DefaultValue = "0", IsNullable = false)]
        public int Prioridad {
            get { return this.prioridad; }
            set {
                this.prioridad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad
        /// </summary>
        [SugarColumn(ColumnDescription = "cantidad del producto o servicio", ColumnName = "_ordclip_cntd", IsNullable = false)]
        public decimal Cantidad {
            get { return this.cantidad; }
            set {
                this.cantidad = value;
                this.ValorUnitario = this.valorUnitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor unitario
        /// </summary>
        [SugarColumn(ColumnDescription = "valor unitario del concepto", ColumnName = "_ordclip_untr", IsNullable = true)]
        public decimal ValorUnitario {
            get { return this.valorUnitario; }
            set {
                this.valorUnitario = value;
                this.SubTotal = this.Cantidad * this.valorUnitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor neto del pedido
        /// </summary>
        [SugarColumn(ColumnDescription = "subtotal del concepto unitario x cantidad", ColumnName = "_ordclip_sbttl", IsNullable = false)]
        public decimal SubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador establecido por el cliente
        /// </summary>
        [SugarColumn(ColumnDescription = "identificador establecido por el cliente", ColumnName = "_ordclip_noident", Length = 21, IsNullable = true)]
        public string NoIdentificacion {
            get { return this.noIdentificacion; }
            set { this.noIdentificacion = value; 
                this.OnPropertyChanged(); 
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida
        /// </summary>
        [SugarColumn(ColumnDescription = "unidad del producto o servicio", ColumnName = "_ordclip_undm", Length = 21, IsNullable = true)]
        public string UnidadMedida {
            get { return this.unidadMedida; }
            set {
                this.unidadMedida = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del concepto
        /// </summary>
        [SugarColumn(ColumnDescription = "concepto o descripcion del producto o servicio", ColumnName = "_ordclip_nom", Length = 512, IsNullable = false)]
        public string Concepto {
            get { return this.concepto; }
            set { this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del concepto
        /// </summary>
        [SugarColumn(ColumnDescription = "notas", ColumnName = "_ordclip_nota", Length = 64, IsNullable = true)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnDescription = "clave del usuario que crea el registro", ColumnName = "_ordclip_usr_n", Length = 10, IsNullable = false)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega
        /// </summary>
        [SugarColumn(ColumnDescription = "fecha de entrega del concepto", ColumnName = "_ordclip_fcentr", IsNullable = true)]
        public DateTime? FechaEntrega {
            get { return this.fechaEntrega; }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del registro
        /// </summary>
        [SugarColumn(ColumnDescription = "fecha del registro en el sistema", ColumnName = "_ordclip_fn", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set { this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "ultima fecha de modificacion del registro", ColumnName = "_ordclip_fm", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica > new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
