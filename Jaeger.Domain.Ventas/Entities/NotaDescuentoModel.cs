﻿using System;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Ventas.Contracts;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// clase modelo para nota de descuento
    /// </summary>
    public class NotaDescuentoModel : BasePropertyChangeImplementation, INotaDescuentoModel {
        #region declaraciones
        private int idNota;
        private int idStatus;
        private int folio;
        private int idDirectorio;
        private int idMotivo;
        private int activo;
        private string creo;
        private string modifica;
        private string _observaciones;
        private string referencia;
        private string contacto;
        private decimal importe;
        private string _IdDocumento;
        private string _Clave;
        private string _Nombre;
        private string _RFC;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private decimal _GTotal;
        private decimal _TrasladoIVA;
        private decimal _Descuento;
        private decimal _SubTotal;
        private string _Cancela;
        private DateTime? _FechaCancela;
        private int _ClaveCancela;
        private string _MotivoCancelacion;
        private string _NotaCancelacion;
        private string _CTMotivo;
        private int _Anio;
        private int _Mes;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public NotaDescuentoModel() {
            fechaNuevo = DateTime.Now;
            activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de tabla (ntdsc_id)
        /// </summary>
        [DataNames("NTDSC_ID")]
        public int IdNota {
            get { return idNota; }
            set {
                idNota = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("NTDSC_A")]
        public int Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del status
        /// </summary>
        [DataNames("NTDSC_STTS_ID")]
        public int IdStatus {
            get { return idStatus; }
            set {
                idStatus = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("NTDSC_DRCTR_ID")]
        public int IdDirectorio {
            get { return idDirectorio; }
            set {
                idDirectorio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del motivo de la nota de descuento
        /// </summary>
        [DataNames("NTDSC_CTMTV_ID")]
        public int IdMotivo {
            get { return idMotivo; }
            set {
                idMotivo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        [DataNames("NTDSC_FOLIO")]
        public int Folio {
            get { return folio; }
            set {
                folio = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("NTDSC_CLV")]
        public string Clave {
            get { return _Clave; }
            set {
                _Clave = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del receptor
        /// </summary>
        [DataNames("NTDSC_RFC")]
        public string RFC {
            get { return _RFC; }
            set {
                _RFC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del receptor
        /// </summary>
        [DataNames("NTDSC_NOM")]
        public string Nombre {
            get { return _Nombre; }
            set {
                _Nombre = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el importe de la nota de descuento
        /// </summary>
        [DataNames("NTDSC_IMPORTE")]
        public decimal Importe {
            get { return importe; }
            set {
                importe = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSC_CNTCT")]
        public string Contacto {
            get { return contacto; }
            set {
                contacto = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSC_REF")]
        public string Referencia {
            get { return referencia; }
            set {
                referencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave y descripcion del motivo de la nota de descuento
        /// </summary>
        [DataNames("NTDSC_CTMTV")]
        public string CvMotivo {
            get { return _CTMotivo; }
            set {
                _CTMotivo = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSC_SBTTL")]
        public decimal SubTotal {
            get { return _SubTotal; }
            set {
                _SubTotal = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSC_DSCNT")]
        public decimal Descuento {
            get { return _Descuento; }
            set {
                _Descuento = value;
                OnPropertyChanged();
            }
        }

        [DataNames("NTDSC_TRSIVA")]
        public decimal TotalTrasladoIVA {
            get { return _TrasladoIVA; }
            set {
                _TrasladoIVA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el gran total del comprobante (subTotal + Traslado IVA)
        /// </summary>
        [DataNames("NTDSC_GTOTAL")]
        public decimal GTotal {
            get { return _GTotal; }
            set {
                _GTotal = value;
                OnPropertyChanged();
            }
        }

        #region cancelacion
        /// <summary>
        /// obtener o establecer clave del usuario que cancela
        /// </summary>
        [DataNames("_ntdsc_usu_c", "NTDSC_USU_C")]
        public string Cancela {
            get { return _Cancela; }
            set {
                _Cancela = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de cancelacion
        /// </summary>
        [DataNames("_ntdsc_fccncl", "NTDSC_FCCNCL")]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (_FechaCancela >= firstGoodDate)
                    return _FechaCancela;
                return null;
            }
            set {
                _FechaCancela = value;
                OnPropertyChanged();
            }
        }
        #endregion

        /// <summary>
        /// obtener o establecer id de documento
        /// </summary>
        [DataNames("NTDSC_UUID")]
        public string IdDocumento {
            get { return _IdDocumento; }
            set {
                _IdDocumento = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_OBSRV)
        /// </summary>
        [DataNames("NTDSC_OBSRV")]
        public string Nota {
            get { return _observaciones; }
            set {
                _observaciones = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision y tambien fecha de creacion del registro (_FN)
        /// </summary>
        [DataNames("NTDSC_FN")]
        public DateTime FechaNuevo {
            get { return fechaNuevo; }
            set {
                fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_FM)
        /// </summary>
        [DataNames("NTDSC_FM")]
        public DateTime? FechaModifica {
            get { return fechaModifica; }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_N)
        /// </summary>
        [DataNames("NTDSC_USU_N")]
        public string Creo {
            get { return creo; }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (CTLRMS_USU_M)
        /// </summary>
        [DataNames("NTDSC_USU_M")]
        public string Modifica {
            get { return modifica; }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }

        public DateTime FechaEmision {
            get { return fechaNuevo; }
        }

        public int Anio {
            get { return this._Anio; }
            set {
                this._Anio = value;
                this.OnPropertyChanged();
            }
        }

        public int Mes {
            get { return this._Mes; }
            set { this._Mes = value;
                this.OnPropertyChanged();
            }
        }
    }
}