﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Domain.Ventas.Entities {
    [SugarTable("_ordclid", "ventas: documentos relacionados")]
    public class OrdenClienteDoctoModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private bool isActiveField;
        private string noIndetField;
        private string tituloField;
        private string descripcionField;
        private string contentField;
        private DateTime fechaNuevoField;
        private string creoField;
        private string urlDescargaField;

        /// <summary>
        /// cosntructor
        /// </summary>
        public OrdenClienteDoctoModel() {

        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("_ordclid_id")]
        [SugarColumn(ColumnName = "_ordclid_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, IsNullable = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_ordclid_a")]
        [SugarColumn(ColumnName = "_ordclid_a", ColumnDescription = "registro activo", IsNullable = true)]
        public bool IsActive {
            get {
                return this.isActiveField;
            }
            set {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ordclid_id")]
        [SugarColumn(ColumnName = "_ordclid_subId", ColumnDescription = "indice de relacion con la tabla _cntbl3", IsNullable = true)]
        public int IdOrdenCliente {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero o clave de indentificacion
        /// </summary>
        [DataNames("_ordclid_noiden")]
        [SugarColumn(ColumnName = "_ordclid_noiden", ColumnDescription = "identificador de la prepoliza", Length = 11, IsNullable = true)]
        public string NoIndet {
            get {
                return this.noIndetField;
            }
            set {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del archivo
        /// </summary>
        [DataNames("_ordclid_nom")]
        [SugarColumn(ColumnName = "_ordclid_nom", ColumnDescription = "nombre del archivo", Length = 128, IsNullable = true)]
        public string Titulo {
            get {
                return this.tituloField;
            }
            set {
                this.tituloField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion del documento
        /// </summary>
        [DataNames("_ordclid_desc")]
        [SugarColumn(ColumnName = "_ordclid_desc", ColumnDescription = "descripcion del documento anexo", Length = 128, IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de contenido de archivo
        /// </summary>
        [DataNames("_ordclid_cont")]
        [SugarColumn(ColumnName = "_ordclid_cont", ColumnDescription = "tipo de archivo", Length = 32, IsNullable = true)]
        public string Content {
            get {
                return this.contentField;
            }
            set {
                this.contentField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del nuevo registro
        /// </summary>
        [DataNames("_ordclid_fn")]
        [SugarColumn(ColumnDataType = "TIMESTAMP", ColumnName = "_ordclid_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevoField;
            }
            set {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("_ordclid_usr_n")]
        [SugarColumn(ColumnName = "_ordclid_usr_n", ColumnDescription = "clave del usuario quer crea el registro", Length = 10, IsNullable = true)]
        public string Creo {
            get {
                return this.creoField;
            }
            set {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url valida del archivo 
        /// </summary>
        [DataNames("_ordclid_url")]
        [SugarColumn(ColumnName = "_ordclid_url", ColumnDescription = "url", ColumnDataType = "TEXT", IsNullable = true)]
        public string URL {
            get {
                return this.urlDescargaField;
            }
            set {
                this.urlDescargaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ruta completa del archivo en el equipo
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Tag { get; set; }

        public string KeyName() {

            //use MD5 hash to get a 16-byte hash of the string:

            var provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Concat(this.noIndetField));

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            var hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();

        }
    }
}
