﻿using System.ComponentModel;
using Jaeger.Domain.Ventas.Contracts;

namespace Jaeger.Domain.Ventas.Entities {
    public class ComisionDetailModel : ComisionModel, IComisionDetailModel, IComisionModel {
        #region declaraciones
        private BindingList<ComisionDescuentoModel> descuento;
        private BindingList<ComisionMultaModel> multa;
        #endregion

        public ComisionDetailModel() : base() {
            descuento = new BindingList<ComisionDescuentoModel>();
            multa = new BindingList<ComisionMultaModel>();
        }

        public BindingList<ComisionDescuentoModel> Descuento {
            get { return descuento; }
            set {
                descuento = value;
                OnPropertyChanged();
            }
        }

        public BindingList<ComisionMultaModel> Multa {
            get { return multa; }
            set {
                multa = value;
                OnPropertyChanged();
            }
        }

        public bool IsVigente {
            get {
                if (System.DateTime.Now >= FechaInicio && System.DateTime.Now <= FechaFin) {
                    return true;
                }
                return false;
            }
        }

        public string Error {
            get {
                if (IsVigente == false) {
                    return "La lista de precios esta fuera de su vigencia";
                }
                return string.Empty;
            }
        }

        public string this[string columnName] {
            get {
                if (columnName == "FechaInicio" | columnName == "FechaFin" && IsVigente == false) {
                    return "Fecha de inicio o fin de vigencia no válida.";
                }
                return string.Empty;
            }
        }
    }
}
