﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// clase de orden de cliente
    /// </summary>
    [SugarTable("_ordcli", "ventas: orden de cliente")]
    public class OrdenClienteDetailModel : OrdenClienteModel {
        private BindingList<OrdenClienteConceptoDetailModel> conceptos;
        private BindingList<OrdenClienteDoctoModel> documentos;
        private BindingList<OrdenClienteOrdenProduccionModel> ordenProduccion;

        public OrdenClienteDetailModel() {
            //this.Status = "Pendiente";
            this.FechaPedido = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.conceptos = new BindingList<OrdenClienteConceptoDetailModel>() { RaiseListChangedEvents = true };
            this.conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
            this.conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
            this.documentos = new BindingList<OrdenClienteDoctoModel>();
            this.ordenProduccion = new BindingList<OrdenClienteOrdenProduccionModel>();
        }

        /// <summary>
        /// obtener solo indentificador etiqueta+numero de control interno
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Identificador {
            get { return this.Id.ToString("OC0000#"); }
        }

        /// <summary>
        /// obtener o establecer conceptos de la orden de cliente
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<OrdenClienteConceptoDetailModel> Conceptos {
            get { return this.conceptos; }
            set {
                if (this.conceptos != null) {
                    this.conceptos.AddingNew -= new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.conceptos = value;
                if (this.conceptos != null) {
                    this.conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer documentos relacionados a la orden de cliente
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<OrdenClienteDoctoModel> Documentos {
            get { return this.documentos; }
            set {
                if (this.documentos != null) {
                    this.documentos.AddingNew -= new AddingNewEventHandler(this.Documento_AddingNew);
                    this.documentos.ListChanged -= new ListChangedEventHandler(this.Documento_ListChanged);
                }
                this.documentos = value;
                if (this.documentos != null) {
                    this.documentos.AddingNew += new AddingNewEventHandler(this.Documento_AddingNew);
                    this.documentos.ListChanged += new ListChangedEventHandler(this.Documento_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<OrdenClienteOrdenProduccionModel> OrdenProduccion {
            get { return this.ordenProduccion; }
            set { this.ordenProduccion = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos privados

        private void Documento_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new OrdenClienteDoctoModel { IdOrdenCliente = this.Id };
        }

        private void Documento_ListChanged(object sender, ListChangedEventArgs e) {
            
        }

        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new OrdenClienteConceptoDetailModel { IdOrdenCliente = this.Id };
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.Importe = this.conceptos.Where((OrdenClienteConceptoDetailModel p) => p.Activo == true).Sum((OrdenClienteConceptoDetailModel p) => p.SubTotal);
        }

        #endregion
    }
}
