﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Ventas.Contracts;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// comisiones a vendedor
    /// </summary>
    public class ComisionModel : BasePropertyChangeImplementation, IComisionModel {
        #region declaraciones
        private int idComision;
        private bool activo;
        private int secuencia;
        private decimal ventaMinima;
        private decimal ventaMaxima;
        private string descripcion;
        private string nota;
        private DateTime? fechaInicioVigencia;
        private DateTime? fechaFinVigencia;
        private DateTime fechaNuevo;
        private string creo;
        private DateTime? fechaModifica;
        private string modifica;
        #endregion

        /// <summary>
        /// obtener o establecer el indice de la tabla (CMSNM_ID)
        /// </summary>
        public ComisionModel() {
            FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla (CMSNT_ID)
        /// </summary>
        [DataNames("CMSNT_ID")]
        public int IdComision {
            get { return idComision; }
            set {
                idComision = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>

        [DataNames("CMSNT_A")]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        [DataNames("CMSNT_SEC_ID")]
        public int Orden {
            get { return secuencia; }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer venta minima para la aplicacion de la comision
        /// </summary>
        [DataNames("CMSNT_VMIN")]
        public decimal VentaMinima {
            get { return ventaMinima; }
            set {
                ventaMinima = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer venta maxima para la aplicacion de la comision
        /// </summary>
        [DataNames("CMSNT_VMAX")]
        public decimal VentaMaxima {
            get { return ventaMaxima; }
            set {
                ventaMaxima = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la comision
        /// </summary>
        [DataNames("CMSNT_NOM")]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones 
        /// </summary>
        [DataNames("CMSNT_NOTA")]
        public string Nota {
            get { return nota; }
            set {
                nota = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de incio de la vigencia de la comision
        /// </summary>
        [DataNames("CMSNT_FCHINI")]
        public DateTime? FechaInicio {
            get {
                if (fechaInicioVigencia >= new DateTime(1900, 1, 1))
                    return fechaInicioVigencia;
                return null;
            }
            set {
                fechaInicioVigencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de final de la vigencia de la comision
        /// </summary>
        [DataNames("CMSNT_FCHVIG")]
        public DateTime? FechaFin {
            get {
                if (fechaFinVigencia >= new DateTime(1900, 1, 1))
                    return fechaFinVigencia;
                return null;
            }
            set {
                fechaFinVigencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CMSNT_FN")]
        public DateTime FechaNuevo {
            get { return fechaNuevo; }
            set {
                fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("CMSNT_USU_N")]
        public string Creo {
            get { return creo; }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("CMSNT_FM")]
        public DateTime? FechaModifica {
            get {
                if (fechaModifica >= new DateTime(1900, 1, 1))
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("CMSNT_USU_M")]
        public string Modifica {
            get { return modifica; }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }
    }
}
