﻿using System.ComponentModel;
using SqlSugar;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// obtener o establecer conceptos o partidas de la orden de cliente
    /// </summary>
    [SugarTable("_ordclip", "ventas: conceptos de la orden de cliente")]
    public class OrdenClienteConceptoDetailModel : OrdenClienteConceptoModel {
        private BindingList<OrdenClienteOrdenProduccionModel> ordenProduccion;

        /// <summary>
        /// constructor
        /// </summary>
        public OrdenClienteConceptoDetailModel() {
            this.ordenProduccion = new BindingList<OrdenClienteOrdenProduccionModel>();
        }

        /// <summary>
        /// obtener o establecer ordenes de produccion relacionadas a la partida o concepto de la orden de cliente
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<OrdenClienteOrdenProduccionModel> OrdenProduccion {
            get { return this.ordenProduccion; }
            set { this.ordenProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
