﻿using System.ComponentModel;
using Jaeger.Domain.Contribuyentes.Entities;
using SqlSugar;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// cliente
    /// </summary>
    public class ClienteDetailModel : Contribuyentes.Entities.ContribuyenteModel {
        private BindingList<Contribuyentes.Entities.DomicilioFiscalModel> domicilios;
        private BindingList<VendedorClienteDetailModel> vendedores;

        public ClienteDetailModel() {
    
        }

        /// <summary>
        /// obtener o establecer domiclios registrados
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<Contribuyentes.Entities.DomicilioFiscalModel> Domicilios {
            get {
                return this.domicilios;
            }
            set {
                this.domicilios = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de vendedeores asociados al cliente o vendedor
        /// </summary> 
        [SugarColumn(IsIgnore = true)]
        public BindingList<VendedorClienteDetailModel> Vendedores {
            get {
                return this.vendedores;
            }
            set {
                this.vendedores = value;
                this.OnPropertyChanged();
            }
        }
    }
}
