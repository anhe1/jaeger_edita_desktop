﻿using System.ComponentModel;
using Jaeger.Domain.Ventas.ValueObjects;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// Nota de descuento
    /// </summary>
    public class NotaDescuentoDetailModel : NotaDescuentoModel {
        #region declaraciones
        private BindingList<NotaDescuentoRelacionDetailModel> partidas;
        private BindingList<NotaDescuentoStatusModel> autorizaciones;
        #endregion

        public NotaDescuentoDetailModel() : base() {
            Status = NotaDescuentoStatusEnum.Pendiente;
            Motivo = NotaDescuentoMotivoEnum.NoDefinido;
            autorizaciones = new BindingList<NotaDescuentoStatusModel>();
            partidas = new BindingList<NotaDescuentoRelacionDetailModel>();
        }

        public NotaDescuentoStatusEnum Status {
            get { return (NotaDescuentoStatusEnum)IdStatus; }
            set {
                IdStatus = (int)value;
                OnPropertyChanged();
            }
        }

        public NotaDescuentoMotivoEnum Motivo {
            get { return (NotaDescuentoMotivoEnum)IdMotivo; }
            set {
                IdMotivo = (int)value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// verdadero si el status es Pendiente
        /// </summary>
        public bool IsEditable {
            get { return IdStatus == (int)NotaDescuentoStatusEnum.Pendiente; }
        }

        public BindingList<NotaDescuentoRelacionDetailModel> Partidas {
            get { return partidas; }
            set {
                partidas = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer listado de autorizaciones
        /// </summary>
        public BindingList<NotaDescuentoStatusModel> Autorizaciones {
            get { return autorizaciones; }
            set {
                autorizaciones = value;
                OnPropertyChanged();
            }
        }

        public int NPartida {
            get { return Partidas.Count; }
        }

        public NotaDescuentoStatusModel TagStatus { get; set; }
    }
}
