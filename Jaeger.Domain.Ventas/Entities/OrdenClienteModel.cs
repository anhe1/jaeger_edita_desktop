﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Ventas.Contracts;

namespace Jaeger.Domain.Ventas.Entities {
    public class OrdenClienteModel : BasePropertyChangeImplementation, IOrdenClienteModel {
        #region declaraciones
        private int idOrdenCliente;
        private int _IdCliente;
        private int _IdStatus;
        private DateTime? fechaRequerida;
        private DateTime? _FechaVoBo;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private DateTime? _FechaPedido;
        private string _Cliente;
        private string _Contacto;
        private string _Correo;
        private string _Telefono;
        private string _NoPedido;
        private string _CondicionPago;
        private string _MetodoPago;
        private DateTime? _FechaEntrega;
        private decimal _Importe;
        private bool _Facturar;
        private string _Embarcar;
        private string _Nota;
        private int _IdVendedor;
        #endregion

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_ID")]
        public int Id {
            get { return this.idOrdenCliente; }
            set {
                this.idOrdenCliente = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_DRCTR_ID")]
        public int IdCliente {
            get { return this._IdCliente; }
            set {
                this._IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_STTS_ID")]
        public int IdStatus {
            get { return this._IdStatus; }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_FCPED")]
        public DateTime? FechaPedido {
            get { return this._FechaPedido; }
            set {
                this._FechaPedido = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_NOM")]
        public string Cliente {
            get { return this._Cliente; }
            set {
                this._Cliente = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_NOM")]
        public string Contacto {
            get { return this._Contacto; }
            set {
                this._Contacto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_MAIL")]
        public string Correo {
            get { return _Correo; }
            set {
                this._Correo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_TEL")]
        public string Telefono {
            get { return this._Telefono; }
            set {
                this._Telefono = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_NOPED")]
        public string NoPedido {
            get { return this._NoPedido; }
            set {
                this._NoPedido = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_CNDPG")]
        public string CondicionPago {
            get { return this._CondicionPago; }
            set {
                this._CondicionPago = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_MTDPG")]
        public string MetodoPago {
            get { return this._MetodoPago; }
            set {
                this._MetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_FENTR")]
        public DateTime? FechaEntrega {
            get { return this._FechaEntrega; }
            set {
                this._FechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_FCREQ")]
        public DateTime? FechaRequerida {
            get { return this.fechaRequerida; }
            set {
                this.fechaRequerida = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_FVOBO")]
        public DateTime? FechaVoBo {
            get {
                if (this._FechaVoBo >= new DateTime(1900, 1, 1))
                    return this._FechaVoBo;
                return null;
            }
            set {
                this._FechaVoBo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_IMPRT")]
        public decimal Importe {
            get { return this._Importe; }
            set {
                this._Importe = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_FAC")]
        public bool Facturar {
            get { return this._Facturar; }
            set {
                this._Facturar = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_EMBR")]
        public string Embarcar {
            get { return this._Embarcar; }
            set {
                this._Embarcar = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_NOTA")]
        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "ORDCL_VNDR_ID")]
        public int IdVendedor {
            get { return this._IdVendedor; }
            set {
                this._IdVendedor = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_USR_N")]
        public string Creo { get; set; }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_FN")]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_USR_M")]
        public string Modifica { get; set; }

        [SugarColumn(ColumnDescription = "", ColumnName = "ORDCL_FM")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
