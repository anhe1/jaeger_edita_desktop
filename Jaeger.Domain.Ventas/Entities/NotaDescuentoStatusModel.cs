﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// modelo de status de remision
    /// </summary>
    public class NotaDescuentoStatusModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int _Index;
        private bool _Activo;
        private int _IdRemision;
        private int _IdCliente;
        private int _IdStatusA;
        private int _IdStatusB;
        private string _CvMotivo;
        private string _Nota;
        private DateTime _FechaNuevo;
        private string _User;
        #endregion

        public NotaDescuentoStatusModel() : base() {
            _Activo = true;
        }

        /// <summary>
        /// obtener o estableecr indice 
        /// </summary>
        [DataNames("NTDSCS_ID")]
        public int IdAuto {
            get { return _Index; }
            set {
                _Index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("NTDSCS_A")]
        public bool Activo {
            get { return _Activo; }
            set {
                _Activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con remisiones
        /// </summary>
        [DataNames("NTDSCS_NTDSC_ID")]
        public int IdNota {
            get { return _IdRemision; }
            set {
                _IdRemision = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de clientes
        /// </summary>
        [DataNames("NTDSCS_DRCTR_ID")]
        public int IdCliente {
            get { return _IdCliente; }
            set {
                _IdCliente = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del status sin autorizar
        /// </summary>
        [DataNames("NTDSCS_NTDSC_STTS_ID")]
        public int IdStatusA {
            get { return _IdStatusA; }
            set {
                _IdStatusA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del status autorizado
        /// </summary>
        [DataNames("NTDSCS_NTDSC_STTSA_ID")]
        public int IdStatusB {
            get { return _IdStatusB; }
            set {
                _IdStatusB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del motivo del cambio de status
        /// </summary>
        [DataNames("NTDSCS_CVMTV")]
        public string CvMotivo {
            get { return _CvMotivo; }
            set {
                _CvMotivo = value;
                OnPropertyChanged();
            }
        }

        public int IdCVMotivo { get; set; }

        /// <summary>
        /// obtener o establecer observaciones del registro
        /// </summary>
        [DataNames("NTDSCS_NOTA")]
        public string Nota {
            get { return _Nota; }
            set {
                _Nota = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estalecer fecha de creacion
        /// </summary>
        [DataNames("NTDSCS_FN")]
        public DateTime FechaNuevo {
            get { return _FechaNuevo; }
            set {
                _FechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer usuario
        /// </summary>
        [DataNames("NTDSCS_USR_N")]
        public string User {
            get { return _User; }
            set {
                _User = value;
                OnPropertyChanged();
            }
        }
    }
}
