﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Domain.Ventas.Entities {
    /// <summary>
    /// objeto para imprimir lista de comisiones con remisiones
    /// </summary>
    public class RemisionComisionPrinter {

        public RemisionComisionPrinter() {

        }

        public string Cliente { get; set; }

        public string Vendedor { get; set; }

        public List<RemisionComisionDetailModel> Conceptos { get; set; }

        public string[] QrText {
            get {
                return new string[] { "||Vendedores||" };
            }
        }
    }
}
