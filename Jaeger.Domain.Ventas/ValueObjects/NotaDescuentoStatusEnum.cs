﻿using System.ComponentModel;

namespace Jaeger.Domain.Ventas.ValueObjects {
    public enum NotaDescuentoStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Pendiente")]
        Pendiente = 1,
        [Description("Impreso")]
        Impreso = 2,
        [Description("Autorizado")]
        Autoizado = 3,
        [Description("Aplicado")]
        Aplicado = 4
    }
}
