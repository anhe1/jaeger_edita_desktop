﻿using System.ComponentModel;

namespace Jaeger.Domain.Ventas.ValueObjects {
    public enum NotaDescuentoMotivoCancelacionEnum {
        [Description("No Definido")]
        NoDefinido = 0,
        [Description("Autorizado por Administrador")]
        Autorizado = 1,
        [Description("Error de sistema")]
        ErrorSistema = 99
    }
}
