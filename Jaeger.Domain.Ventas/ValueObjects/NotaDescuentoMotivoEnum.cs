﻿using System.ComponentModel;

namespace Jaeger.Domain.Ventas.ValueObjects {
    public enum NotaDescuentoMotivoEnum {
        [Description("No Definido")]
        NoDefinido = 0,
        [Description("Autorizado por Administrador")]
        Autorizado = 1,
        [Description("Negociación con Cliente")]
        NegociacionCliente = 2,
        [Description("Negociación con Vendedor")]
        NegociacionVendedor = 3,
        [Description("Descuento Especial")]
        DescuentoEspecial = 4,
        [Description("Descuento por pronto pago")]
        DescuentoPorProntoPago = 5,
        [Description("Intercambio")]
        Intercambio = 6,
        [Description("Corregir montos")]
        CorregirMontos = 7,
        [Description("Devolución de producto")]
        DevolucionProducto = 8,
        [Description("Cambio de productos")]
        CambioProductos = 9,
        [Description("Producto defectuoso")]
        ProductoDefectuoso = 10,
        [Description("El cliente no solicita material")]
        ClienteNoSolicita = 11,
        [Description("Error en pedido del cliente")]
        ErrorPedido = 12,
        [Description("Error de sistema")]
        ErrorSistema = 99
    }
}
