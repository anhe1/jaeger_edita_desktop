﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Ventas.Builder {
    public interface IOrdenClienteQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IOrdenClienteYearQueryBuilder Year(int year);
    }

    public interface IOrdenClienteYearQueryBuilder : IConditionalBuilder {
        IOrdenClienteMonthQueryBuilder Month(int month);
    }

    public interface IOrdenClienteMonthQueryBuilder : IConditionalBuilder {

    }
}
