﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Ventas.Builder {
    public class NotaDescuentoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild ,INotaDescuentoQueryBuilder, INotaDescuentoYearQueryBuilder, INotaDescuentoMonthQueryBuilder {
        public NotaDescuentoQueryBuilder() : base() { }

        public INotaDescuentoYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("NTDSC_ANIO", year.ToString()));
            return this;
        }
        public INotaDescuentoMonthQueryBuilder Month(int month) {
            if (month> 0) { this._Conditionals.Add(new Conditional("NTDSC_MES", month.ToString())); }
            return this;
        }
    }
}
