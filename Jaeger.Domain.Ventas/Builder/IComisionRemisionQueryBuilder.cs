﻿using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Ventas.Builder {
    /// <summary>
    /// builder para la creacion de consulta de comisiones a remisiones
    /// </summary>
    public interface IComisionRemisionQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IComisionRemisionIdVendedorQueryBuilder Year(int year);
        IComisionRemisionIdVendedorQueryBuilder YearGreaterThan(int year);
        IComisionRemisionIdVendedorQueryBuilder Month(int month);
    }

    public interface IComisionRemisionIdVendedorQueryBuilder : IConditionalBuilder {
        IComisionRemisionIdVendedorQueryBuilder IdVendedor(int idVendedor);
        IComisionRemisionIdVendedorQueryBuilder Month(int month);
        /// <summary>
        /// status de la remision
        /// </summary>
        IComisionRemisionIdVendedorQueryBuilder IdStatus(RemisionStatusEnum idStatus);
        /// <summary>
        /// disponibilidad de la remision para el pago de comisión
        /// </summary>
        IComisionRemisionIdVendedorQueryBuilder IsDisponible(RemisionComisionStatusEnum idStatus);
    }
}
