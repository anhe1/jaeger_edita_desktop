﻿using System;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Ventas.Builder {
    public class OrdenClienteQueryBuilder : ConditionalBuilder, IOrdenClienteQueryBuilder, IOrdenClienteYearQueryBuilder, IOrdenClienteMonthQueryBuilder {
        public OrdenClienteQueryBuilder() { }

        public IOrdenClienteMonthQueryBuilder Month(int month) {
            throw new NotImplementedException();
        }

        public IOrdenClienteYearQueryBuilder Year(int year) {
            throw new NotImplementedException();
        }
    }
}
