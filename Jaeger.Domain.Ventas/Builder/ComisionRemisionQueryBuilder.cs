﻿using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Ventas.Builder {
    public class ComisionRemisionQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IComisionRemisionQueryBuilder, IComisionRemisionIdVendedorQueryBuilder {
        public ComisionRemisionQueryBuilder() : base() { }

        public IComisionRemisionIdVendedorQueryBuilder Year(int year) {
            if (year > 0) {
                this._Conditionals.Add(new Conditional("RMSN_ANIO", year.ToString()));
            }
            return this;
        }

        public IComisionRemisionIdVendedorQueryBuilder YearGreaterThan(int year) {
            this._Conditionals.Add(new Conditional("RMSN_ANIO", year.ToString(), ConditionalTypeEnum.GreaterThanOrEqual));
            return this;
        }

        public IComisionRemisionIdVendedorQueryBuilder Month(int month) {
            this._Conditionals.Add(new Conditional("RMSN_MES", month.ToString()));
            return this;
        }

        /// <summary>
        /// indice del vendedor
        /// </summary>
        public IComisionRemisionIdVendedorQueryBuilder IdVendedor(int idVendedor) {
            this._Conditionals.Add(new Conditional("RMSN_VNDDR_ID", idVendedor.ToString()));
            return this;
        }

        /// <summary>
        /// status de la remision
        /// </summary>
        public IComisionRemisionIdVendedorQueryBuilder IdStatus(RemisionStatusEnum idStatus) {
            var indice = (int)idStatus;
            this._Conditionals.Add(new Conditional("RMSN_STTS_ID", indice.ToString()));
            return this;
        }

        /// <summary>
        /// disponibilidad de la remision para el pago de comisión
        /// </summary>
        public IComisionRemisionIdVendedorQueryBuilder IsDisponible(RemisionComisionStatusEnum idStatus) {
            var indice = (int)idStatus;
            if (indice == 0) {
                this._Conditionals.Add(new Conditional("RMSNC_CTDIS_ID", "0", ConditionalTypeEnum.IsNullOrValue));
            } else {
                this._Conditionals.Add(new Conditional("RMSNC_CTDIS_ID", indice.ToString()));
            }
            return this;
        }
    }
}
