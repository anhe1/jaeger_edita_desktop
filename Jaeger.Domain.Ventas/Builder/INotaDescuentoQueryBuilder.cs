﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Ventas.Builder {
    public interface INotaDescuentoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        INotaDescuentoYearQueryBuilder Year(int  year);
    }

    public interface INotaDescuentoYearQueryBuilder : IConditionalBuilder, IConditionalBuild {
        INotaDescuentoMonthQueryBuilder Month(int month);
    }

    public interface INotaDescuentoMonthQueryBuilder : IConditionalBuilder, IConditionalBuild {
    }
}
