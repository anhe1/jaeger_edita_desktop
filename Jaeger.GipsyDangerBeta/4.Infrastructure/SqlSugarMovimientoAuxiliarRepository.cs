﻿using Jaeger.Beta.Domain.Banco.Contracts;
using Jaeger.Beta.Domain.Banco.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Beta.DataAccess.Repositories {
    public class SqlSugarMovimientoAuxiliarRepository : MySqlSugarContext<MovimientoBancarioAuxiliarModel>, ISqlMovimientoAuxiliarRepository {
        public SqlSugarMovimientoAuxiliarRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public MovimientoBancarioAuxiliarDetailModel Save(MovimientoBancarioAuxiliarDetailModel item) {
            if (item.IdAuxiliar == 0) {
                var _insert = this.Db.Insertable(item);
                item.IdAuxiliar = this.Execute(_insert);
            } else {
                var _update = this.Db.Updateable(item);
                this.Execute(_update);
            }
            return item;
        }

        public bool CreateTables() {
            return this.CreateTable();
        }
    }
}
