﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Beta.Domain.AlmacenPT.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Beta.DataAccess.Repositories {
    /// <summary>
    /// 
    /// </summary>
    public class SqlSugarRemisionFiscalRepository : MySqlSugarContext<RemisionFiscalModel> {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de base de datos</param>
        public SqlSugarRemisionFiscalRepository(DataBaseConfiguracion configuracion)
            : base(configuracion) {
        }

        /// <summary>
        /// obtener listado de remisiones con detalles
        /// </summary>
        /// <param name="year">ejercicio</param>
        /// <param name="month">periodo</param>
        public IEnumerable<RemisionFiscalDetailModel> GetList(int year, int month) {
            //Manual mode
            var result = this.Db.Queryable<RemisionFiscalDetailModel>()
                .Where(it => it.FechaEmision.Year == year)
                .WhereIF(month != 0, it => it.FechaEmision.Month == month).Mapper((itemModel, cache) => {
                    var allItems = cache.Get(remisionList => {
                        var allIds = remisionList.Select(it => it.IdRemision).ToList();
                        return this.Db.Queryable<RemisionConceptoDetailModel>().Where(it => it.Activo == true).Where(it => allIds.Contains(it.IdRemision)).ToList();
                    });
                    var descuento = cache.Get(remId => {
                        var ids = remId.Select(it => it.IdRemision).ToList();
                        return this.Db.Queryable<RemisionFiscalDescuentoModel>().Where(it => it.Activo == true).Where(it => ids.Contains(it.IdRemision)).ToList();
                    });
                    if (descuento != null)
                        itemModel.Descuento = descuento.Where(it => it.IdRemision == itemModel.IdRemision).SingleOrDefault();

                    itemModel.Conceptos = new BindingList<RemisionConceptoDetailModel>(allItems.Where(it => it.IdRemision == itemModel.IdRemision).ToList());
            }).ToList();
            return result;
        }

        /// <summary>
        /// obtener remision detalle
        /// </summary>
        /// <param name="index">indice de la remision</param>
        public new RemisionFiscalDetailModel GetById(int index) {
            //Manual mode
            var result = this.Db.Queryable<RemisionFiscalDetailModel>().Where(it => it.IdRemision == index).Mapper((itemModel, cache) => {
                var allItems = cache.Get(remisionList => {
                    var allIds = remisionList.Select(it => it.IdRemision).ToList();
                    return this.Db.Queryable<RemisionConceptoDetailModel>().Where(it => it.Activo == true).Where(it => allIds.Contains(it.IdRemision)).ToList();
                });
                itemModel.Conceptos = new BindingList<RemisionConceptoDetailModel>(allItems.Where(it => it.IdRemision == itemModel.IdRemision).ToList());
            }).Single();
            return result;
        }

        public IEnumerable<RemisionFiscalDetailModel> GetById(int[] index) {
            //Manual mode
            var result = this.Db.Queryable<RemisionFiscalDetailModel>().Where(it => index.Contains(it.IdRemision)).Mapper((itemModel, cache) => {
                var allItems = cache.Get(remisionList => {
                    var allIds = remisionList.Select(it => it.IdRemision).ToList();
                    return this.Db.Queryable<RemisionConceptoDetailModel>().Where(it => it.Activo == true).Where(it => allIds.Contains(it.IdRemision)).ToList();
                });
                itemModel.Conceptos = new BindingList<RemisionConceptoDetailModel>(allItems.Where(it => it.IdRemision == itemModel.IdRemision).ToList());
            }).ToList();
            return result;
        }

        public RemisionFiscalDetailModel GetByFolio(int folio, string serie = "") {
            var response = this.Db.Queryable<RemisionFiscalDetailModel>().Where(it => it.Folio == folio).WhereIF(serie != "", it => it.Serie == serie).Single();

            if (response != null) {
                response.Conceptos = this.GetListBy(response.IdRemision, true);
            }
            return response;
        }

        /// <summary>
        /// almacenar remision fiscal
        /// </summary>
        public RemisionFiscalDetailModel Save(RemisionFiscalDetailModel item) {
            if (item.IdRemision == 0) {
                if (item.Folio == 0) item.Folio = this.GetFolio(item.EmisorRFC, item.Serie);
                item.IdDocumento = this.CreateGuid(new string[] { item.Folio.ToString("000000#"), item.EmisorRFC, item.ReceptorRFC, item.FechaEmision.ToString() });
                item.IdRemision = this.Insert(item);
            }
            else {
                item.FechaModifica = DateTime.Now;
                this.Update(item);
            }

            if (item.Descuento != null) {
                if (item.Descuento.Id == 0) {
                    item.Descuento.IdRemision = item.IdRemision;
                    //item.Descuento.Id = this.Insert(item.Descuento);
                }
                else {
                    this.Update(item.Descuento);
                }
            }

            for (int i = 0; i < item.Conceptos.Count; i++) {
                if (item.Conceptos[i].IdConcepto == 0) {
                    item.Conceptos[i].IdRemision = item.IdRemision;
                    item.Conceptos[i].FechaNuevo = DateTime.Now;
                    item.Conceptos[i].IdConcepto = this.Insert(item.Conceptos[i]);
                }
                else {
                    item.Conceptos[i].IdRemision = item.IdRemision;
                    item.Conceptos[i].FechaModifica = DateTime.Now;
                    this.Update(item.Conceptos[i]);
                }
            }
            return item;
        }

        /// <summary>
        /// almacenar remision fiscal
        /// </summary>
        public RemisionFiscalDetailModel Import(RemisionFiscalDetailModel item) {
            if (item.IdRemision == 0) {
                if (item.Folio == 0)
                    item.Folio = this.GetFolio(item.EmisorRFC, item.Serie);
                item.IdDocumento = this.CreateGuid(new string[] { item.Folio.ToString("000000#"), item.EmisorRFC, item.ReceptorRFC, item.FechaEmision.ToString() });
                item.IdRemision = this.Insert(item);
            } else {
                item.FechaModifica = DateTime.Now;
                this.Update(item);
            }

            if (item.Descuento != null) {
                if (item.Descuento.Id == 0) {
                    item.Descuento.IdRemision = item.IdRemision;
                    //item.Descuento.Id = this.Insert(item.Descuento);
                } else {
                    this.Update(item.Descuento);
                }
            }

            for (int i = 0; i < item.Conceptos.Count; i++) {
                if (item.Conceptos[i].IdConcepto == 0) {
                    item.Conceptos[i].IdRemision = item.IdRemision;
                    //item.Conceptos[i].FechaNuevo = DateTime.Now;
                    item.Conceptos[i].IdConcepto = this.Insert(item.Conceptos[i]);
                } else {
                    item.Conceptos[i].IdRemision = item.IdRemision;
                    //item.Conceptos[i].FechaModifica = DateTime.Now;
                    this.Update(item.Conceptos[i]);
                }
            }
            return item;
        }

        public IEnumerable<ReceptorDetailModel> GetReceptor(TipoRelacionComericalEnum tipo) {
            return this.Db.Queryable<ReceptorDetailModel>().Where(it => it.Activo == true)
                .Where(it => it.Relacion.Contains(Enum.GetName(typeof(TipoRelacionComericalEnum), tipo)))
                .Select((receptor) => new ReceptorDetailModel {
                Id = receptor.Id,
                Nombre = receptor.Nombre,
                RFC = receptor.RFC
            }).ToList();
        }

        public int Insert(RemisionConceptoModel item) {
            return this.Db.Insertable<RemisionConceptoModel>(item).ExecuteReturnIdentity();
        }

        public int Update(RemisionConceptoModel item) {
            return this.Db.Updateable<RemisionConceptoModel>(item).ExecuteCommand();
        }

        public int Insert(RemisionFiscalDescuentoModel item) {
            return this.Db.Insertable<RemisionFiscalDescuentoModel>(item).ExecuteReturnIdentity();
        }

        public int Update(RemisionFiscalDescuentoModel item) {
            return this.Db.Updateable<RemisionFiscalDescuentoModel>(item).ExecuteCommand();
        }

        public BindingList<RemisionConceptoDetailModel> GetListBy(int index, bool activo = true) {
            return new BindingList<RemisionConceptoDetailModel>(this.Db.Queryable<RemisionConceptoDetailModel>()
                .Where(it => it.IdRemision == index).WhereIF(activo == true, it => it.Activo == true).ToList());
        }

        public bool CrearTablas() {
            try {
                this.Db.CodeFirst.InitTables<RemisionFiscalModel, RemisionConceptoModel, RemisionFiscalDescuentoModel>();
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        ///  Generar folio para un comprobante
        /// </summary>
        /// <param name="rfc">RFC del emisor del comprobante</param>
        /// <param name="serie">Serie del comprobante fiscal</param>
        /// <returns>numero entero convertido en cadena</returns>
        private int GetFolio(string rfc, string serie) {
            try {
                int folio;
                if (serie != null)
                    folio = this.Db.Ado.GetInt("select (Max(cast(_aptrms._aptrms_folio as UNSIGNED)) + 1) as folio from _aptrms where _aptrms_rfce = @rfc and _aptrms._aptrms_serie = @serie", new { rfc = rfc, serie = serie });
                else
                    folio = this.Db.Ado.GetInt("select (Max(cast(_aptrms._aptrms_folio as UNSIGNED)) + 1) as folio from _aptrms where _aptrms_rfce = @rfc ", new { rfc = rfc });
                return folio;
            }
            catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
    }
}
