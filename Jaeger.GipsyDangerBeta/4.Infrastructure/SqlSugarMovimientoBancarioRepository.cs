﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Beta.Domain.Banco.Contracts;
using Jaeger.Beta.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Beta.DataAccess.Repositories {
    /// <summary>
    /// repositorio de movimientos bancarios
    /// </summary>
    public class SqlSugarMovimientoBancarioRepository : MySqlSugarContext<MovimientoBancarioModel>, ISqlMovimientoBancarioRepository {
        protected ISqlMovmientoComprobanteRepository comprobanteRepository;
        protected ISqlMovimientoAuxiliarRepository auxiliarRepository;

        public SqlSugarMovimientoBancarioRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.comprobanteRepository = new SqlSugarMovimientoComprobanteRepository(configuracion);
            this.auxiliarRepository = new SqlSugarMovimientoAuxiliarRepository(configuracion);
        }

        /// <summary>
        /// 
        /// </summary>
        public new MovimientoBancarioDetailModel GetById(int index) {
            var result = this.Db.Queryable<MovimientoBancarioDetailModel>().Where(it => it.Id == index).Single();
            if (result != null) {
                var j = MovimientoBancarioDetailModel.Json(result.InfoAuxiliar);
                result.CuentaPropia = j.CuentaPropia;
                
                return result;
            }
            return null;
        }

        /// <summary>
        /// obtener listado de movimientos por cuenta
        /// </summary>
        /// <param name="idCuenta">indice de la cuenta, si es menor que o igual a cero devuelve todos los movimientos</param>
        /// <param name="month">mes</param>
        /// <param name="year">año</param>
        public IEnumerable<MovimientoBancarioDetailModel> GetList(int idCuenta, int month, int year, int idTipo) {
            var result = this.Db.Queryable<MovimientoBancarioDetailModel>()
                .Where(it => it.Activo == true)
                .Where(it => it.FechaEmision.Year == year)
                .WhereIF(month > 0, it => it.FechaEmision.Month == month)
                .WhereIF(idTipo > 0, it => it.IdTipo == idTipo)
                .WhereIF(idCuenta > 0, it => it.IdCuentaP == idCuenta).Mapper((mov, cache) => {
                    var allItems = cache.Get(comp => {
                        var allIds = comp.Select(it => it.Id).ToList();
                        return this.Db.Queryable<MovimientoBancarioComprobanteDetailModel>().Where(it => allIds.Contains(it.IdMovimiento)).Where(it => it.Activo == true).ToList();
                    });
                    var allItems2 = cache.Get(aux => {
                        var allIds = aux.Select(it => it.Id).ToList();
                        return this.Db.Queryable<MovimientoBancarioAuxiliarDetailModel>().Where(it => allIds.Contains(it.IdMovimiento)).Where(it => it.Activo == true).ToList();
                    });
                    mov.Comprobantes = new System.ComponentModel.BindingList<MovimientoBancarioComprobanteDetailModel>(allItems.Where(it => it.IdMovimiento == mov.Id).ToList());
                    mov.Auxiliar = new System.ComponentModel.BindingList<MovimientoBancarioAuxiliarDetailModel>(allItems2.Where(it => it.IdMovimiento == mov.Id).ToList());
                })
                .ToList();
            return result;
        }

        public IEnumerable<MovimientoBancarioDetailModel> GetList(int idCuenta, int idTipo, DateTime startDate, DateTime? endDate, bool onlyActive) {
            var conModels = new List<IConditionalModel>();

            if (idCuenta > 0)
                conModels.Add(new ConditionalModel() { FieldName = "_bncmov_idcnta1", FieldValue = idCuenta.ToString(), ConditionalType = ConditionalType.Equal });
            if (idTipo != 0)
                conModels.Add(new ConditionalModel() { FieldName = "_bncmov_idtp", FieldValue = idTipo.ToString(), ConditionalType = ConditionalType.Equal });

            if (endDate != null) {
                conModels.Add(new ConditionalModel() { FieldName = "_bncmov_fecems", FieldValue = startDate.ToString(), ConditionalType = ConditionalType.GreaterThanOrEqual });
                conModels.Add(new ConditionalModel() { FieldName = "_bncmov_fecems", FieldValue = endDate.ToString(), ConditionalType = ConditionalType.LessThanOrEqual });
            } else {
                conModels.Add(new ConditionalModel() { FieldName = "_bncmov_fecems", FieldValue = startDate.ToString(), ConditionalType = ConditionalType.Equal });
            }

            var result = this.Db.Queryable<MovimientoBancarioDetailModel>()
                .WhereIF(onlyActive == true, it => it.Activo == true)
                .Where(conModels).Mapper((mov, cache) => {
                    var allItems = cache.Get(comp => {
                        var allIds = comp.Select(it => it.Id).ToList();
                        return this.Db.Queryable<MovimientoBancarioComprobanteDetailModel>().Where(it => allIds.Contains(it.IdMovimiento)).Where(it => it.Activo == true).ToList();
                    });
                    var allItems2 = cache.Get(aux => {
                        var allIds = aux.Select(it => it.Id).ToList();
                        return this.Db.Queryable<MovimientoBancarioAuxiliarDetailModel>().Where(it => allIds.Contains(it.IdMovimiento)).Where(it => it.Activo == true).ToList();
                    });
                    mov.Comprobantes = new System.ComponentModel.BindingList<MovimientoBancarioComprobanteDetailModel>(allItems.Where(it => it.IdMovimiento == mov.Id).ToList());
                    mov.Auxiliar = new System.ComponentModel.BindingList<MovimientoBancarioAuxiliarDetailModel>(allItems2.Where(it => it.IdMovimiento == mov.Id).ToList());
                })
                .ToList();
            return result;
        }

        /// <summary>
        /// obtener listado de movimientos por cuenta
        /// </summary>
        /// <param name="idCuenta">indice de la cuenta</param>
        /// <param name="month">mes</param>
        /// <param name="year">año</param>
        //public IEnumerable<MovimientoBancarioDetailModel> GetList(int tipoMovimiento, int idCuenta, int month, int year) {
        //    var result = this.Db.Queryable<MovimientoBancarioDetailModel>()
        //        .Where(it => it.Activo == true)
        //        .Where(it => it.FechaEmision.Month == month)
        //        .Where(it => it.FechaEmision.Year == year)
        //        .Where(it => it.IdCuentaP == idCuenta).Mapper((mov, cache) => {
        //            var allItems = cache.Get(comp => {
        //                var allIds = comp.Select(it => it.Id).ToList();
        //                return this.Db.Queryable<MovimientoBancarioComprobanteDetailModel>().Where(it => allIds.Contains(it.IdMovimiento)).Where(it => it.Activo == true).ToList();
        //            });
        //            mov.Comprobantes = new System.ComponentModel.BindingList<MovimientoBancarioComprobanteDetailModel>(allItems.Where(it => it.IdMovimiento == mov.Id).ToList());
        //        })
        //        .ToList();
        //    return result;
        //}

        /// <summary>
        /// almacenar
        /// </summary>
        public MovimientoBancarioDetailModel Save(MovimientoBancarioDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Identificador = this.NoIndetificacion(model);
                model.InfoAuxiliar = model.Json();
                var _insert = this.Db.Insertable(model);
                model.Id = this.Execute(_insert);
            } else {
                model.FechaModifica = DateTime.Now;
                model.InfoAuxiliar = model.Json();
                var _update = this.Db.Updateable(model);
                this.Execute(_update);
            }

            if (model.Comprobantes != null) {
                if (model.Comprobantes.Count > 0) {
                    for (int i = 0; i < model.Comprobantes.Count; i++) {
                        model.Comprobantes[i].IdMovimiento = model.Id;
                        model.Comprobantes[i].Identificador = model.Identificador;
                        model.Comprobantes[i] = this.comprobanteRepository.Save(model.Comprobantes[i]);
                    }
                }
            }

            if (model.Auxiliar != null) {
                if (model.Auxiliar.Count > 0) {
                    for (int i = 0; i < model.Auxiliar.Count; i++) {
                        if (model.Auxiliar[i].IdAuxiliar == 0) {
                            model.Auxiliar[i].IdMovimiento = model.Id;
                            model.Auxiliar[i] = this.auxiliarRepository.Save(model.Auxiliar[i]);
                        }
                    }
                }
            }

            return model;
        }

        public bool Json(MovimientoBancarioDetailModel model) {
            model.InfoAuxiliar = model.Json();
            var _update = this.Db.Updateable(model).UpdateColumns(it => new { it.InfoAuxiliar }).Where(it => it.Id == model.Id);
            if (this.Execute(_update) > 0) {

                return true;
            }
            return false;
        }


        public bool Aplicar(MovimientoBancarioDetailModel model) {

            //var  d = this.Db.Updateable(model).UpdateColumns(it => new { it.IdStatus, it.FechaModifica, it.Modifica }).ToSql();
            //var sqlCommand = new MySqlCommand {
            //    CommandText = d.Key
            //};

            //foreach (var item in d.Value) {
            //    sqlCommand.Parameters.AddWithValue(item.ParameterName, item.Value);
            //}
            //var d1 = this.Db.Ado.GetCommand(d.Key, d.Value.ToArray());
            //return d1.ExecuteNonQuery() > 0;
            var _update = this.Db.Updateable(model).UpdateColumns(it => new { it.IdStatus, it.FechaModifica, it.Modifica });
            if (this.Execute(_update) > 0) {
                if (this.Aplicar(model.Id) == false) {
                   // Service.LogErrorService.LogWrite("No se actualizaron las partidas: " + model.Identificador);
                }
            }
            return false;
            //if (this.Db.Updateable(model).UpdateColumns(it => new { it.IdStatus, it.FechaModifica, it.Modifica }).ExecuteCommand() > 0) {
            //    if (this.Aplicar(model.Id) == false) {
            //        Service.LogErrorService.LogWrite("No se actualizaron las partidas: " + model.Identificador);
            //    }
            //    return true;
            //}
            //return false;
        }

        public bool Aplicar(int model) {
            var sql = @"SELECT a._bncmov_idtp, b._bnccmp_tipo as documento, b._bnccmp_idcom as idcomprobante, b._bnccmp_efecto as efecto, b._bnccmp_uuid as uuid, b._bnccmp_total as total, b._bnccmp_sbtp,
                        (SELECT SUM(c._bnccmp_cargo) FROM _bnccmp c, _bncmov d where c._bnccmp_subid = d._bncmov_id AND c._bnccmp_idcom = b._bnccmp_idcom AND (d._bncmov_idstts = @status1 OR d._bncmov_idstts = @status2)) AS cargo,
                        (SELECT SUM(c._bnccmp_abono) FROM _bnccmp c, _bncmov d where c._bnccmp_subid = d._bncmov_id AND c._bnccmp_idcom = b._bnccmp_idcom AND (d._bncmov_idstts = @status1 OR d._bncmov_idstts = @status2)) AS abono,
                        (SELECT MAX(d._bncmov_fccbr) FROM _bnccmp c, _bncmov d where c._bnccmp_subid = d._bncmov_id AND c._bnccmp_idcom = b._bnccmp_idcom AND (d._bncmov_idstts = @status1 OR d._bncmov_idstts = @status2)) AS aplicacion
                        FROM _bncmov a, _bnccmp b
                        WHERE b._bnccmp_subid = a._bncmov_id
                        AND a._bncmov_id = @index";

            var tabla = this.Db.Ado.GetDataTable(sql,
                new SugarParameter[] {
                    new SugarParameter("@index", model),
                    new SugarParameter("@status1", (int)MovimientoBancarioStatusEnum.Aplicado),
                    new SugarParameter("@status2", (int)MovimientoBancarioStatusEnum.Aplicado)
                    });

            
            var result = this.GetMapper<MovimientoBancarioModelView>(tabla).ToList();
            int contador = 0;
            if (result.Count > 0) {
                //foreach (var item in result) {
                //    if (item.TipoDocumento == MovimientoBancarioTipoComprobanteEnum.CFDI) {
                //        try {
                //            var _update  = this.Db.Updateable<ComprobanteFiscalModel>().SetColumns(it => new ComprobanteFiscalModel() { FechaUltimoPago = item.FechaAplicacion, Status = item.Status, Acumulado = item.Acumulado }).Where(it => it.IdComprobante == item.IdComprobante);
                //            this.Execute(_update);
                //        } catch (Exception ex) {
                //            Service.LogErrorService.LogWrite(ex.Message + "| No se actualizaron las partidas: " + item.IdDocumento);
                //        }
                //    }
                //    contador++;
                //}
            }
            return result.Count == contador;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfc">RFC del emisor o receptor</param>
        /// <param name="status">status del comprobante</param>
        /// <param name="SubTipoComprobante">representa el subtipo del comprobante fiscal (Emitido, Recibido, Nomina)</param>
        /// <returns></returns>
        public IEnumerable<MovimientoBancarioComprobanteDetailModel> GetComprobantes(string rfc, string status, CFDISubTipoEnum SubTipoComprobante) {
            var cual = "";
            if (SubTipoComprobante == CFDISubTipoEnum.Emitido) {
                cual = "_cfdi_rfcr";
            } else if (SubTipoComprobante == CFDISubTipoEnum.Recibido) {
                cual = "_cfdi_rfce";
            } else if (SubTipoComprobante == CFDISubTipoEnum.Nomina) {
                cual = "_cfdi_rfcr";
            }
            var sql = @"select _cfdi_ver, _cfdi_efecto, _cfdi_doc_id, _cfdi_id, _cfdi_folio,_cfdi_serie,_cfdi_status,_cfdi_rfce,_cfdi_nome,_cfdi_rfcr,_cfdi_nomr,_cfdi_fecems,_cfdi_mtdpg,_cfdi_frmpg,_cfdi_moneda,_cfdi_uuid,_cfdi_par,_cfdi_total,_cfdi_cbrd,_cfdi_estado 
                        from _cfdi 
                        where @cual1 = @rfc1 and _cfdi_status = @status1 and _cfdi_doc_id = @subtipo and _cfdi_uuid is not null 
                        order by _cfdi_fecems desc";

            sql = sql.Replace("@cual1", cual);
            var tabla = this.Db.Ado.GetDataTable(sql, new {
                rfc1 = rfc, status1 = status, subTipo = (int)SubTipoComprobante, cual1 = cual
            });
            
            var response = this.GetMapper<MovimientoBancarioComprobanteDetailModel>(tabla).ToList();
            return response;
        }

        public bool CreateTables() {
            this.comprobanteRepository.Create();
            return base.CreateTable();
        }

        /// <summary>
        /// calcular el numero de indentifiacion de la prepoliza
        /// </summary>
        private string NoIndetificacion(MovimientoBancarioDetailModel modelo) {
            int mes = modelo.FechaEmision.Month;
            int anio = modelo.FechaEmision.Year;
            int indice = 0;

            try {
                indice = this.Db.Queryable<MovimientoBancarioDetailModel>()
                    .Where(it => it.FechaEmision.Year == anio)
                    .Where(it => it.FechaEmision.Month == mes)
                    .Where(it => it.IdTipo == modelo.IdTipo)
                    .Select(it => new { maximus = SqlFunc.AggregateCount(it.FechaEmision) }).Single().maximus + 1;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                indice = 0;
            }

            return string.Format("{0}{1}{2}",
                modelo.Tipo.ToString()[0],
                modelo.FechaEmision.ToString("yyMM"),
                indice.ToString("000#"));
        }
    }
}
