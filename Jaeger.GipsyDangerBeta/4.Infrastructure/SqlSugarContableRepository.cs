﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Beta.Domain.Contable.Entities;
using Jaeger.Beta.Domain.Banco.Contracts;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Beta.DataAccess.Repositories {
    /// <summary>
    /// repositorio prepolizas para recibos de cobranza y pago
    /// </summary>
    public class SqlSugarContableRepository : SqlSugarContext<PrePolizaModel>, ISqlContableRepository {
        public SqlSugarContableRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        /// <summary>
        /// obtener listado de prepoilizas
        /// </summary>
        /// <param name="tipo">tipo de prepoliza</param>
        /// <param name="month">mes</param>
        /// <param name="year">año</param>
        /// <param name="cuenta">numero de cuenta</param>
        /// <returns></returns>
        public IEnumerable<PrePolizaDetailModel> GetList(PrePolizaModel.PolizaTipoEnum tipo, int month, int year, string cuenta) {
            var result = this.Db.Queryable<PrePolizaDetailModel>()
                .Where(it => it.IsActive == true)
                .WhereIF(cuenta != "todos", it => it.EmisorNumCta.Contains(cuenta))
                .WhereIF(month > 0, it => it.FechaEmision.Month == (int)month)
                .WhereIF(year > 0, it => it.FechaEmision.Year == year)
                .WhereIF(tipo != PrePolizaModel.PolizaTipoEnum.Ninguno, it => it.TipoText == Enum.GetName(typeof(PrePolizaModel.PolizaTipoEnum), tipo))
                .OrderBy(it => it.Id, OrderByType.Desc)
                .Select<PrePolizaDetailModel>().Mapper((itemModel, cache) => {
                    var allItems = cache.Get(prepoliza => {
                        var allIds = prepoliza.Select(it => it.Id).ToList();
                        return this.Db.Queryable<PrePolizaComprobanteModel>().Where(it => allIds.Contains(it.SubId)).ToList();//Execute only once
                    });
                    itemModel.Comprobantes = new BindingList<PrePolizaComprobanteModel>(allItems.Where(it => it.SubId == itemModel.Id).ToList());//Every time it's executed
                }).ToList();
            return result;
        }

        public IEnumerable<BancoCuentaModel> GetCuentasDeBanco() {
            return this.Db.Queryable<BancoCuentaModel>().ToList();
        }
    }
}
