﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Beta.Domain.Banco.Contracts;
using Jaeger.Beta.Domain.Banco.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.Beta.DataAccess.Repositories {
    /// <summary>
    /// repositorio de auxiliares de movimientos bancarios
    /// </summary>
    public class SqlSugarMovimientoComprobanteRepository : MySqlSugarContext<MovimientoBancarioComprobanteModel>, ISqlMovmientoComprobanteRepository {
        
        public SqlSugarMovimientoComprobanteRepository(DataBaseConfiguracion configuracion) : base(configuracion) {

        }

        public MovimientoBancarioComprobanteDetailModel Save(MovimientoBancarioComprobanteDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                //model.Id = this.Insert(model);
                var _insert = this.Db.Insertable(model);
                model.Id = this.Execute(_insert);
            } else {
                model.FechaModifica = DateTime.Now;
                //this.Update(model);
                var _update = this.Db.Updateable(model);
                this.Execute(_update);
            }
            return model;
        }

        public IEnumerable<MovimientoBancarioComprobanteDetailModel> Save(List<MovimientoBancarioComprobanteDetailModel> models) {
            for (int i = 0; i < models.Count(); i++) {
                models[i] = this.Save(models[i]);
            }
            return models;
        }

        public void Create() {
            base.CreateTable();
        }
    }
}
