﻿using System.ComponentModel;
using Jaeger.Domain.CP.Remisionado.Entities;

namespace Jaeger.Beta.Aplication {
    public interface IRemisiondoCPService {
        /// <summary>
        /// obtener listado de remisiones con partidas por un periodo
        /// </summary>
        /// <param name="year">año</param>
        /// <param name="month">mes</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns>vista de remisiones</returns>
        BindingList<RemisionDetailModelView> GetList(int year, int month, bool onlyActive);

        void Importar(int year, int month);
    }
}
