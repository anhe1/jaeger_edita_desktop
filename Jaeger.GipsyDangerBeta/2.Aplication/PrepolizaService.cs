﻿using System;
using System.Linq;
using Jaeger.Aplication.Base;
using Jaeger.Beta.DataAccess.Repositories;
using Jaeger.Beta.Domain.Banco.Contracts;
using Jaeger.Beta.Domain.Banco.Entities;

namespace Jaeger.Beta.Aplication.Banco {
    public class PrepolizaService : IPrepolizaService {
        protected ISqlContableRepository contableRepository;
        protected ISqlMovimientoBancarioRepository movimientoBancarioRepository;
        protected ISqlMovmientoComprobanteRepository comprobanteRepository;
        protected Jaeger.Domain.Banco.Contracts.ISqlCuentaBancariaRepository cuentaBancariaRepository;

        public PrepolizaService() {
            this.contableRepository = new SqlSugarContableRepository(ConfigService.Synapsis.RDS.Edita);
            this.movimientoBancarioRepository = new SqlSugarMovimientoBancarioRepository(ConfigService.Synapsis.RDS.Edita);
            this.comprobanteRepository = new SqlSugarMovimientoComprobanteRepository(ConfigService.Synapsis.RDS.Edita);
            this.cuentaBancariaRepository = new Jaeger.DataAccess.Repositories.SqlSugarCuentaBancariaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public int Importar(int year, int month) {
            var c = 0;
            var datos1 = this.contableRepository.GetList(Domain.Contable.Entities.PrePolizaModel.PolizaTipoEnum.Ninguno, month, year, "todos").OrderBy(it => it.Id);
            foreach (var item in datos1) {
                var n = new MovimientoBancarioDetailModel {
                    Activo = item.IsActive,
                    Id = item.Id,
                    FechaEmision = item.FechaEmision, FechaAplicacion = item.FechaPago,
                    FechaBoveda = item.FechaBoveda,
                    FechaCancela = item.FechaCancela,
                    FechaDocto = item.FechaDocto,
                    FechaModifica = item.FechaModifica,
                    Creo = item.Creo,
                    Modifica = item.Modifica,
                    FechaNuevo = item.FechaNuevo,
                    Identificador = item.NoIndet,
                    Autoriza = item.Autoriza,
                    Cancela = item.Cancela,
                    Abono = item.Abono,
                    Cargo = item.Cargo,
                    ClaveMoneda = item.Moneda,
                    ClaveFormaPago = (item.FormaDePagoText == null ? "" : item.FormaDePagoText),
                    FormaPagoDescripcion = item.FormaPagoDescripcion,
                    PorComprobar = item.PorJustificar,
                    Concepto = item.Concepto,
                    NumAutorizacion = item.NumAutorizacion,
                    NumDocto = item.NumDocto,
                    NumOperacion = item.NumOperacion,
                    Nota = item.Notas,
                    Referencia = item.Referencia,
                    IdCuentaP = item.IdCuentaBanco,
                    BeneficiarioP = item.Emisor.Nombre,
                    BeneficiarioT = item.ReceptorBeneficiario,
                    ClaveBancoT = item.ReceptorCodigo,
                    BeneficiarioRFCT = item.ReceptorRFC,
                    NumeroCuentaT = item.ReceptorNumCta
                };

                var c1 = new MovimientoBancarioCuentaModel(item.Emisor.Nombre, item.Emisor.RFC, item.Emisor.NumCta, item.Emisor.Codigo, item.Emisor.Banco, item.Emisor.Sucursal, item.Emisor.Clabe, item.Emisor.Clave);
                n.CuentaPropia = c1;

                if (item._prePolizaModel != null) {
                    n.ClaveBancoP = item._prePolizaModel.EmisorCodigo;
                    n.NumeroCuentaP = item._prePolizaModel.EmisorNumCta;
                } else {
                    Console.WriteLine("Sin json: " + n.Identificador);
                    n.ClaveBancoP = "E";
                    n.NumeroCuentaP = "E";
                }

                if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.Aplicado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Aplicado;
                } else if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.Auditado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Auditado;
                } else if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.Cancelado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Cancelado;
                } else if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.NoAplicado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Transito;
                }

                if (item.Tipo == Domain.Contable.Entities.PrePolizaModel.PolizaTipoEnum.Ingreso) {
                    n.Tipo = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Ingreso;
                    n.IdConcepto = 2;
                } else if (item.Tipo == Domain.Contable.Entities.PrePolizaModel.PolizaTipoEnum.Egreso) {
                    n.Tipo = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso;
                    n.IdConcepto = 1;
                }

                foreach (var item1 in item.Comprobantes) {
                    var d = new MovimientoBancarioComprobanteDetailModel {
                        Id = item1.Id,
                        Abono = item1.Abono,
                        Activo = item1.IsActive,
                        Folio = (item1.Folio == null ? "" : item1.Folio),
                        Cargo = item1.Cargo,
                        Creo = (item1.Creo == null ? "" : item1.Creo), 
                        EmisorNombre = item1.Emisor,
                        EmisorRFC = item1.EmisorRFC,
                        Estado = item1.Estado,
                        FechaEmision = (DateTime)item1.FechaEmision,
                        IdComprobante = item1.IdComprobante,
                        FechaModifica = item1.FechaMod,
                        Modifica = item1.Modifica,
                        IdDocumento = (item1.UUID == null ? "": item1.UUID),
                        NumParcialidad = item1.NumParcialidad,
                        ReceptorNombre = (item1.Receptor == null ? "" : item1.Receptor),
                        ReceptorRFC = item1.ReceptorRFC,
                        Serie = (item1.Serie == null ? "" : item1.Serie),
                        Version = (item1.Version == "V33" ? "3.3" : item1.Version),
                        TipoComprobanteText = item1.TipoComprobanteText,
                        IdSubTipoComprobante = item1.SubTipoComprobanteInt,
                        FechaNuevo = item1.FechaNuevo,
                        Total = item1.Total,
                        TipoDocumento = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.CFDI,
                        Status = item1.Status,
                        Identificador = item.NoIndet,
                        TipoDocumentoText = item1.TipoDocumentoText,
                        Acumulado = item1.Acumulado,
                        ClaveFormaPago = (item1.ClaveFormaPago == null ? "" : item1.ClaveFormaPago),
                        ClaveMetodoPago = (item1.ClaveMetodoPago == null ? "" : item1.ClaveMetodoPago),
                        ClaveMoneda = item1.ClaveMoneda,
                        IdMovimiento = item.Id
                    };
                    n.Comprobantes.Add(d);
                }

                c++;
                n.InfoAuxiliar = n.Json();
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine(n.Json());
                Console.WriteLine("-----------------------------------------");
                //this.Save3(n);
            }
            return c;
        }

        public int Importar1(int year, int month) {
            var c = 0;
            var datos1 = this.contableRepository.GetList(Domain.Contable.Entities.PrePolizaModel.PolizaTipoEnum.Ninguno, month, year, "todos").OrderBy(it => it.Id);
            foreach (var item in datos1) {
                var n = new MovimientoBancarioDetailModel {
                    Activo = item.IsActive,
                    Id = item.Id,
                    FechaEmision = item.FechaEmision,
                    FechaAplicacion = item.FechaPago,
                    FechaBoveda = item.FechaBoveda,
                    FechaCancela = item.FechaCancela,
                    FechaDocto = item.FechaDocto,
                    FechaModifica = item.FechaModifica,
                    Creo = item.Creo,
                    Modifica = item.Modifica,
                    FechaNuevo = item.FechaNuevo,
                    Identificador = item.NoIndet,
                    Autoriza = item.Autoriza,
                    Cancela = item.Cancela,
                    Abono = item.Abono,
                    Cargo = item.Cargo,
                    ClaveMoneda = item.Moneda,
                    ClaveFormaPago = (item.FormaDePagoText == null ? "" : item.FormaDePagoText),
                    FormaPagoDescripcion = item.FormaPagoDescripcion,
                    PorComprobar = item.PorJustificar,
                    Concepto = item.Concepto,
                    NumAutorizacion = item.NumAutorizacion,
                    NumDocto = item.NumDocto,
                    NumOperacion = item.NumOperacion,
                    Nota = item.Notas,
                    Referencia = item.Referencia,
                    IdCuentaP = item.IdCuentaBanco,
                    BeneficiarioP = item.Emisor.Nombre,
                    BeneficiarioT = item.ReceptorBeneficiario,
                    ClaveBancoT = item.ReceptorCodigo,
                    BeneficiarioRFCT = item.ReceptorRFC,

                    NumeroCuentaT = item.ReceptorNumCta
                };

                if (item._prePolizaModel != null) {
                    n.ClaveBancoP = item._prePolizaModel.EmisorCodigo;
                    n.NumeroCuentaP = item._prePolizaModel.EmisorNumCta;
                } else {
                    Console.WriteLine("Sin json: " + n.Identificador);
                    n.ClaveBancoP = "E";
                    n.NumeroCuentaP = "E";
                }

                if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.Aplicado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Aplicado;
                } else if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.Auditado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Auditado;
                } else if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.Cancelado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Cancelado;
                } else if (item.Estado == Domain.Contable.Entities.PrePolizaModel.PrePolizaStatusEnum.NoAplicado) {
                    n.Status = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioStatusEnum.Transito;
                }

                if (item.Tipo == Domain.Contable.Entities.PrePolizaModel.PolizaTipoEnum.Ingreso) {
                    n.Tipo = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Ingreso;
                    n.IdConcepto = 2;
                } else if (item.Tipo == Domain.Contable.Entities.PrePolizaModel.PolizaTipoEnum.Egreso) {
                    n.Tipo = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso;
                    n.IdConcepto = 1;
                }

                foreach (var item1 in item.Comprobantes) {
                    var d = new MovimientoBancarioComprobanteDetailModel {
                        Id = item1.Id,
                        Abono = item1.Abono,
                        Activo = item1.IsActive,
                        Folio = (item1.Folio == null ? "" : item1.Folio),
                        Cargo = item1.Cargo,
                        Creo = (item1.Creo == null ? "" : item1.Creo),
                        EmisorNombre = item1.Emisor,
                        EmisorRFC = item1.EmisorRFC,
                        Estado = item1.Estado,
                        FechaEmision = (DateTime)item1.FechaEmision,
                        IdComprobante = item1.IdComprobante,
                        FechaModifica = item1.FechaMod,
                        Modifica = item1.Modifica,
                        IdDocumento = (item1.UUID == null ? "" : item1.UUID),
                        NumParcialidad = item1.NumParcialidad,
                        ReceptorNombre = (item1.Receptor == null ? "" : item1.Receptor),
                        ReceptorRFC = item1.ReceptorRFC,
                        Serie = (item1.Serie == null ? "" : item1.Serie),
                        Version = (item1.Version == "V33" ? "3.3" : item1.Version),
                        TipoComprobanteText = item1.TipoComprobanteText,
                        IdSubTipoComprobante = item1.SubTipoComprobanteInt,
                        FechaNuevo = item1.FechaNuevo,
                        Total = item1.Total,
                        TipoDocumento = Jaeger.Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.CFDI,
                        Status = item1.Status,
                        Identificador = item.NoIndet,
                        TipoDocumentoText = item1.TipoDocumentoText,
                        Acumulado = item1.Acumulado,
                        ClaveFormaPago = (item1.ClaveFormaPago == null ? "" : item1.ClaveFormaPago),
                        ClaveMetodoPago = (item1.ClaveMetodoPago == null ? "" : item1.ClaveMetodoPago),
                        ClaveMoneda = item1.ClaveMoneda,
                        IdMovimiento = item.Id
                    };
                    n.Comprobantes.Add(d);
                }

                c++;
                n.InfoAuxiliar = n.Json();
                this.Save1(n);
            }
            return c;
        }

        public void ImportarCuentasBancarias() {
            var cuentas = this.contableRepository.GetCuentasDeBanco();
            foreach (var item in cuentas) {
                var n = new Jaeger.Domain.Banco.Entities.BancoCuentaDetailModel {
                    Activo = item.IsActive,
                    IdCuenta = item.Id,
                    Alias = item.Alias,
                    Banco = item.Banco,
                    Beneficiario = item.Beneficiario,
                    CLABE = item.Clabe,
                    ClaveBanco = item.Clave,
                    Creo = ConfigService.Sysdba,
                    CuentaContable = "",
                    DiaCorte = item.DiaCorte,
                    Extranjero = false,
                    FechaModifica = item.FechaModifica,
                    FechaNuevo = item.FechaNuevo,
                    Modifica = item.Modifica,
                    Funcionario = "",
                    NoCheque = 0,
                    Moneda = item.Moneda,
                    NumCliente = item.NumCliente,
                    NumCuenta = item.NumeroDeCuenta,
                    Plaza = "",
                    Sucursal = item.Sucursal,
                    RFCBenficiario = item.RFC,
                    SaldoInicial = 0
                };
                this.cuentaBancariaRepository.Insert(n);
            }
        }

        public MovimientoBancarioDetailModel Save(MovimientoBancarioDetailModel model) {
            throw new NotImplementedException();
        }

        public MovimientoBancarioDetailModel Save1(MovimientoBancarioDetailModel model) {
            var id = this.movimientoBancarioRepository.Insert(model);

            for (int i = 0; i < model.Comprobantes.Count; i++) {
                model.Comprobantes[i].IdMovimiento = id;
                model.Id = this.comprobanteRepository.Insert(model.Comprobantes[i]);
            }

            return model;
        }

        public void Save3(MovimientoBancarioDetailModel model) {
            this.movimientoBancarioRepository.Json(model);
        }
    }
}
