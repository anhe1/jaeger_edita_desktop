﻿using Jaeger.Beta.Domain.Banco.Entities;

namespace Jaeger.Beta.Aplication.Banco {
    public interface IPrepolizaService {
        MovimientoBancarioDetailModel Save(MovimientoBancarioDetailModel model);

        int Importar(int year, int month);

        void ImportarCuentasBancarias();
    }
}
