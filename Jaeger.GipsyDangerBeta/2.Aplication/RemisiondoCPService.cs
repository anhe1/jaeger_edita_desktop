﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Base;
using Jaeger.Domain.CP.Remisionado.Entities;

namespace Jaeger.Beta.Aplication {
    public class RemisiondoCPService : IRemisiondoCPService {
        protected Jaeger.Domain.CP.Remisionado.Contracts.ISqlRemisionRepository remisionRepository;
        protected DataAccess.Repositories.SqlSugarRemisionFiscalRepository remisionFiscalRepository;

        public RemisiondoCPService() {
            //this.remisionRepository = new Jaeger.DataAccess.CP.Repositories.SqlFbRemRepository(ConfigService.Synapsis.RDS.CP);
            this.remisionFiscalRepository = new DataAccess.Repositories.SqlSugarRemisionFiscalRepository(ConfigService.Synapsis.RDS.Edita);
        }

        /// <summary>
        /// obtener listado de remisiones con partidas por un periodo
        /// </summary>
        /// <param name="year">año</param>
        /// <param name="month">mes</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns>vista de remisiones</returns>
        public BindingList<RemisionDetailModelView> GetList(int year, int month, bool onlyActive) {
            if (month == 0) {
                return new BindingList<RemisionDetailModelView>(this.remisionRepository.GetList(new DateTime(year, 1, 1), new DateTime(year, 12, 31), null, null, onlyActive).ToList());
            } else {
                var periodo = new DateTime(year, month, 1);
                return new BindingList<RemisionDetailModelView>(this.remisionRepository.GetList(periodo, null, null, null, onlyActive).ToList());
            }
        }

        public void Importar(int year, int month) {
            var datos = this.GetList(year, month, true);
            if (datos != null) {
                if (datos.Count > 0) {
                    foreach (var item in datos) {
                        var _remision = this.Importar(item);
                        this.remisionFiscalRepository.Save(_remision);
                    }
                }
            }
        }

        public Domain.AlmacenPT.Entities.RemisionFiscalDetailModel Importar(RemisionDetailModelView remision) {
            var _remision = new Domain.AlmacenPT.Entities.RemisionFiscalDetailModel {
                Activo = remision.Activo == 1, Version = "1.0",
                Acumulado = 0,
                ClaveFormaPago = "",
                ClaveMetodoPago = "",
                ClaveMoneda = "MXN",
                Contacto = remision.Contacto,
                Creo = remision.Creo,
                Modifica = remision.Modifica,
                FechaEmision = remision.FechaEmision.Value,
                FechaModifica = remision.FechaModifica,
                Folio = remision.IdRemision,
                EmisorNombre = ConfigService.Synapsis.Empresa.RazonSocial,
                EmisorRFC = ConfigService.Synapsis.Empresa.RFC,
                ReceptorNombre = remision.Cliente,
                ReceptorRFC = remision.Clave
            };

            foreach (var _partida in remision.Partidas) {
                var _concepto = new Domain.AlmacenPT.Entities.RemisionConceptoDetailModel {
                    CantidadSalida = _partida.Cantidad, NoOrden = _partida.IdPedPrd,
                    ValorUnitario = _partida.Unitario,
                    Unidad = "Pieza",
                    NoIdentificacion = "",
                    Descripcion = _partida.Producto,
                    ClaveUnidad = "H87",
                    ClaveProdServ = "82121511"
                };
                _remision.Conceptos.Add(_concepto);
            }
            
            if (remision.IdStatus == 20) {
                _remision.Status = Jaeger.Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Cancelado;
            } else if (remision.IdStatus == 1) {
                _remision.Status = Jaeger.Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Pendiente;
            } else if (remision.IdStatus == 5) {
                _remision.Status = Jaeger.Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Entregado;
            } else if (remision.IdStatus == 10) {
                _remision.Status = Jaeger.Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Cobrado;
            } else if (remision.IdStatus == 3) {
                _remision.Status = Jaeger.Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Pendiente;
            } else if (remision.IdStatus == 21) {
                _remision.Status = Jaeger.Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Cobrado;
            }
                return _remision;
        }
    }
}
