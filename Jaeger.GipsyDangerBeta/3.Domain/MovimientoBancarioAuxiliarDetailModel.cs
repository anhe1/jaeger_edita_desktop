﻿using SqlSugar;

namespace Jaeger.Beta.Domain.Banco.Entities {
    [SugarTable("_bncaux", "documentos relacionados a movmientos bancarios")]
    public class MovimientoBancarioAuxiliarDetailModel : MovimientoBancarioAuxiliarModel {
        private string auxiliarB64;

        public MovimientoBancarioAuxiliarDetailModel() : base() {
            this.auxiliarB64 = null;    
        }

        [SugarColumn(IsIgnore = true)]
        public string Base64 {
            get {
                return this.auxiliarB64;
            }
            set {
                this.auxiliarB64 = value;
                this.OnPropertyChanged();
            }
        }

        

        [SugarColumn(IsIgnore = true)]
        public string Button {
            get {
                return "No disponible";
            }
        }
    }
}
