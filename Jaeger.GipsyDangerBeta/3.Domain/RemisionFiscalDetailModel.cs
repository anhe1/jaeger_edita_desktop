﻿using System;
using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Services;
using Jaeger.Domain.Almacen.PT.ValueObjects;

namespace Jaeger.Beta.Domain.AlmacenPT.Entities {
    [SugarTable("_aptrms", "apt: entradas, remisiones y devoluciones del almacen de producto terminado")]
    public class RemisionFiscalDetailModel : RemisionFiscalModel {
        private string texto;
        private BindingList<RemisionConceptoDetailModel> conceptos;
        private RemisionFiscalDescuentoModel objetoDescuento;

        public RemisionFiscalDetailModel() : base() {
            this.FechaEmision = DateTime.Now;
            this.ClaveMoneda = "MXN";
            this.PrecisionDecimal = 2;
            this.TipoCambio = 1;
            this.Conceptos = new BindingList<RemisionConceptoDetailModel> { RaiseListChangedEvents = true };
            this.Conceptos.AddingNew += Conceptos_AddingNew;
            this.Conceptos.ListChanged += Conceptos_ListChanged;
            this.Descuento = new RemisionFiscalDescuentoModel();
            this.Domicilio = new Jaeger.Domain.Almacen.PT.Entities.DomicilioEntregaModel();
        }

        [SugarColumn(IsIgnore = true)]
        public string StatusText {
            get {
                return Enum.GetName(typeof(RemisionStatusEnum), this.IdStatus);
            }
            set {
                this.IdStatus = (int)Enum.Parse(typeof(RemisionStatusEnum), value);
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public RemisionStatusEnum Status {
            get {
                return (RemisionStatusEnum)this.IdStatus;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionConceptoDetailModel> Conceptos {
            get {
                return this.conceptos;
            }
            set {
                if (this.conceptos != null) {
                    this.conceptos.AddingNew -= new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.conceptos = value;
                if (this.conceptos != null) {
                    this.conceptos.AddingNew += new AddingNewEventHandler(this.Conceptos_AddingNew);
                    this.conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }
        
        [SugarColumn(IsIgnore = true)]
        public string DomicilioText {
            get { if (base.Domicilio != null)
                    return base.Domicilio.ToString();
                return string.Empty;
            }
            set {
                this.texto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool Editable {
            get {
                return !ValidacionService.UUID(this.IdDocumento);
            }
        }

        [SugarColumn(IsIgnore = true)]
        public RemisionFiscalDescuentoModel Descuento {
            get { return this.objetoDescuento; }
            set { this.objetoDescuento = value;
                this.OnPropertyChanged();
            }
        }

        public void Clonar() {
            this.IdRemision = 0;
            this.Folio = 0;
            this.FechaEmision = DateTime.Now;
            this.FechaCancela = null;
            this.FechaCobranza = null;
            this.FechaEntrega = null;
            this.FechaModifica = null;
            this.FechaUltPago = null;
            this.Creo = null;

            if (this.conceptos != null) {
                for (int i = 0; i < this.conceptos.Count; i++) {
                    if (this.conceptos[i].Activo) {
                        this.conceptos[i].IdConcepto = 0;
                        this.conceptos[i].Modifica = null;
                        this.conceptos[i].FechaNuevo = DateTime.Now;
                        this.conceptos[i].FechaModifica = null;
                        this.conceptos[i].Creo = null;
                    }
                }
            }
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.Importe);
            this.ImporteDescuento = this.conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.Descuento);
            this.TrasladoIVA = this.conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.TrasladoIVA);
            this.Total = (this.SubTotal - this.ImporteDescuento) + (this.TrasladoIVA + this.TrasladoIEPS) - (this.RetencionIVA + this.RetencionISR);
        }

        private void Conceptos_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new RemisionConceptoDetailModel { IdRemision = this.IdRemision };
        }
    }
}
