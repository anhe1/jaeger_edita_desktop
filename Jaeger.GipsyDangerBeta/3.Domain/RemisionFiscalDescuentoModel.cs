﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Beta.Domain.AlmacenPT.Entities {
    /// <summary>
    /// modelo para descuento en remision
    /// </summary>
    [SugarTable("_aptrmsds", "descuentos aplicables a remision de cliente")]
    public class RemisionFiscalDescuentoModel : BasePropertyChangeImplementation {
        private int index;
        private bool activo;
        private int idRemision;
        private string motivoDescuento;
        private decimal factor;
        private decimal descuento;
        private string creo;
        private DateTime fechaNuevo;

        public RemisionFiscalDescuentoModel() {
            this.Activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establcer el indice de la tabla
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_id", ColumnDescription = "indice de la tabla", IsNullable = false)]
        public int Id {
            get { return this.index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_a", ColumnDescription = "registro activo", IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set { this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la remision
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_idrem", ColumnDescription = "indice de relacion con remisiones", IsNullable = false)]
        public int IdRemision {
            get { return this.idRemision; }
            set { this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el motivo del descuento
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_mtv", ColumnDescription = "motivo del descuento", IsNullable = true)]
        public string Motivo {
            get { return this.motivoDescuento; }
            set { this.motivoDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del descuento aplicable
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_fctr", ColumnDescription = "factor del descuento aplicable", Length = 14, DecimalDigits = 4, IsNullable = false)]
        public decimal Factor {
            get { return this.factor; }
            set { this.factor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el mondo del descuento
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_sbttl", ColumnDescription = "monto del descuento", Length = 14, DecimalDigits = 4, IsNullable = false)]
        public decimal Importe {
            get { return this.descuento; }
            set { this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_fn", ColumnDescription = "fecha de creacion del registro", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [SugarColumn(ColumnName = "_aptrmsds_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
