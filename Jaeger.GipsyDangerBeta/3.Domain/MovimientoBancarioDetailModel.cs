﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.Banco.Entities {
    [SugarTable("_bncmov", "movimientos bancarios")]
    public class MovimientoBancarioDetailModel : MovimientoBancarioModel, ICloneable {
        private MovimientoBancarioCuentaModel cuentaPropia;
        private BindingList<MovimientoBancarioComprobanteDetailModel> comprobantes;
        private BindingList<MovimientoBancarioAuxiliarDetailModel> auxiliar;
        private decimal saldo;

        public MovimientoBancarioDetailModel() : base() {
            this.Status = MovimientoBancarioStatusEnum.Transito;
            this.cuentaPropia = new MovimientoBancarioCuentaModel();

            this.comprobantes = new BindingList<MovimientoBancarioComprobanteDetailModel>() { 
                RaiseListChangedEvents = true
            };
            this.comprobantes.AddingNew += ComprobantesField_AddingNew;
            this.comprobantes.ListChanged += Comprobantes_ListChanged;

            this.auxiliar = new BindingList<MovimientoBancarioAuxiliarDetailModel>();
        }

        /// <summary>
        /// Ingreso = Deposito, Egreso = Retiro
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioEfectoEnum Tipo {
            get {
                return (MovimientoBancarioEfectoEnum)this.IdTipo;
            }
            set {
                this.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de operacion Transferencia, Cuentas etc
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BancoTipoOperacionEnum TipoOperacion {
            get {
                return (BancoTipoOperacionEnum)this.IdTipoOperacion;
            }
            set {
                this.IdTipoOperacion = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// status del movimiento
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioStatusEnum Status {
            get {
                return (MovimientoBancarioStatusEnum)this.IdStatus;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        #region propiedades de la cuenta propia
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string AliasP {
            get {
                return this.cuentaPropia.Alias;
            }
            set {
                this.cuentaPropia.Alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public new string ClaveBancoP {
            get {
                return base.ClaveBancoP;
            }
            set {
                base.ClaveBancoP = value;
                this.cuentaPropia.ClaveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes del banco
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string BeneficiarioRFCP {
            get {
                return this.cuentaPropia.RFC;
            }
            set {
                this.cuentaPropia.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string BeneficiarioP {
            get {
                return this.cuentaPropia.Beneficiario;
            }
            set {
                this.cuentaPropia.Beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonIgnore]
        [DataNames("_bnccta_numcta")]
        [SugarColumn(ColumnName = "_bncmov_numctap", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public new string NumeroCuentaP {
            get {
                return base.NumeroCuentaP;
            }
            set {
                base.NumeroCuentaP = value;
                this.cuentaPropia.NumCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string SucursalP {
            get {
                return this.cuentaPropia.Sucursal;
            }
            set {
                this.cuentaPropia.Sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string BancoP {
            get {
                return this.cuentaPropia.Banco;
            }
            set {
                this.cuentaPropia.Banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string CuentaCLABEP {
            get {
                return this.cuentaPropia.CLABE;
            }
            set {
                this.cuentaPropia.CLABE = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        [JsonProperty("cuenta1", Order = 99)]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioCuentaModel CuentaPropia {
            get {
                return this.cuentaPropia;
            }
            set {
                this.cuentaPropia = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("comp", Order = 100)]
        [SugarColumn(IsIgnore = true)]
        public BindingList<MovimientoBancarioComprobanteDetailModel> Comprobantes {
            get {
                return this.comprobantes;
            }
            set {
                this.comprobantes = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("aux", Order = 200)]
        [SugarColumn(IsIgnore = true)]
        public BindingList<MovimientoBancarioAuxiliarDetailModel> Auxiliar {
            get {
                return this.auxiliar;
            }
            set {
                this.auxiliar = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal Saldo {
            get {
                return this.saldo;
            }
            set {
                this.saldo = value;
                this.OnPropertyChanged();
            }
        }
        public static MovimientoBancarioDetailModel Json(string json) {
            try {
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                if (json != null)
                return JsonConvert.DeserializeObject<MovimientoBancarioDetailModel>(json, conf);
            } catch (JsonException ex) {
                Console.WriteLine(ex.Message);
            }
            return new MovimientoBancarioDetailModel();
        }

        public string Json() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, conf);
        }

        public void Clonar() {
            this.Identificador = null;
            this.Id = 0;
            this.FechaEmision = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.Status = MovimientoBancarioStatusEnum.Transito;
            this.FechaAplicacion = null;
            this.FechaCancela = null;
            this.FechaBoveda = null;
            this.FechaModifica = null;
            this.FechaVence = null;
            foreach (var item in this.comprobantes) {
                item.Id = 0;
                item.IdMovimiento = 0;
                item.FechaNuevo = DateTime.Now;
                item.FechaModifica = null;
                item.Modifica = null;
                item.Creo = null;
                if (item.Activo == false)
                    this.comprobantes.Remove(item);
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }

        public bool Agregar(MovimientoBancarioComprobanteDetailModel model) {
            bool existe = false;
            try {
                var buscar = this.Comprobantes.Where(it => it.IdDocumento == model.IdDocumento).ToList();
                existe = buscar.Count > 0;
            } catch (Exception) {
                existe = false;
            }
            if (!existe) {
                if (this.Tipo == MovimientoBancarioEfectoEnum.Egreso) {
                    model.Abono = model.Total - model.Acumulado;
                } else {
                    model.Cargo = model.Total - model.Acumulado;
                }
                this.Comprobantes.Add(model);
            }
            return existe;
        }

        public bool Remover(MovimientoBancarioComprobanteDetailModel model) {
            if (model.Id == 0) {
                this.Comprobantes.Remove(model);
                return true;
            } else {
                model.Activo = false;
            }
            return false;
        }

        private void ComprobantesField_AddingNew(object sender, AddingNewEventArgs e) {
        }
        private decimal totalComprobantesField;
        private void Comprobantes_ListChanged(object sender, ListChangedEventArgs e) {
            this.totalComprobantesField = this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Abono);
            if (this.PorComprobar == false)
                this.Cargo = this.totalComprobantesField;

            this.totalComprobantesField = this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true && p.TipoComprobante == TipoCFDIEnum.Ingreso).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Cargo);
            if (this.PorComprobar == false)
                this.Abono = this.totalComprobantesField;
        }
    }
}
