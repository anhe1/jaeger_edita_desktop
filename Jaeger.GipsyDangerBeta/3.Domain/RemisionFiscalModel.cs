﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.AlmacenPT.Entities {
    [SugarTable("_aptrms", "apt: comprobante remisiones fiscales")]
    public class RemisionFiscalModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int idRemision;
        private bool activo;
        private int precisionDecimal;
        private int idDirectorio;
        private int noOrden;
        private int folio;
        private int idVendedor;
        private int status;
        private int tipo;
        private DateTime fechaEmision;
        private DateTime? fechaCancela;
        private DateTime? fechaEntrega;
        private DateTime? fechaUltPago;
        private DateTime? fechaModifica;
        private DateTime? fechaCobranza;
        private string version;
        private string serie;
        private string idDocumento;
        private string emisorRFC;
        private string emisorNombre;
        private string receptorRFC;
        private string receptorNombre;
        private decimal tipoCambio;
        private decimal retencionISR;
        private decimal retencionIVA;
        private decimal trasladoIVA;
        private decimal retencionIEPS;
        private decimal trasladoIEPS;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal porCobrar;
        private decimal acumulado;
        private decimal importePagado;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveMoneda;
        private string vendedor;
        private string creo;
        private string modifica;
        private string nota;
        private string noOrdenCliente;
        private string contacto;
        private Jaeger.Domain.Almacen.PT.Entities.DomicilioEntregaModel domicilio;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public RemisionFiscalModel() {
            this.activo = true;
            this.FechaEmision = DateTime.Now;
            this.ClaveMoneda = "MXN";
            this.PrecisionDecimal = 2;
            this.TipoCambio = 1;
            this.version = "2.0";
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("_aptrms_id")]
        [SugarColumn(ColumnName = "_aptrms_id", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this.idRemision; }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("_aptrms_a")]
        [SugarColumn(ColumnName = "_aptrms_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("_aptrms_status")]
        [SugarColumn(ColumnName = "_aptrms_status", ColumnDescription = "status del comprobante")]
        public int IdStatus {
            get {
                return this.status;
            }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// precision decimal
        /// </summary>
        [DataNames("_aptrms_deci")]
        [SugarColumn(ColumnName = "_aptrms_deci", ColumnDescription = "precision decimal", DefaultValue = "2")]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set {
                this.precisionDecimal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_aptrms_drctr_id")]
        [SugarColumn(ColumnName = "_aptrms_drctr_id", ColumnDescription = "indice del directorio", IsNullable = true)]
        public int IdDirectorio {
            get { return this.idDirectorio; }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del vendedor
        /// </summary>
        [DataNames("_aptrms_vnddr_id")]
        [SugarColumn(ColumnName = "_aptrms_vnddr_id", ColumnDescription = "id del vendedor", IsNullable = true)]
        public int IdVendedor {
            get { return this.idVendedor; }
            set { this.idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_aptrms_tipo", ColumnDescription = "tipo de movimiento")]
        public int IdTipoDocumento {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_aptrms_norden")]
        [SugarColumn(ColumnName = "_aptrms_norden", ColumnDescription = "numero orden de produccion", IsNullable = true)]
        public int NoOrden {
            get { return this.noOrden; }
            set {
                this.noOrden = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("_aptrms_folio")]
        [SugarColumn(ColumnName = "_aptrms_folio", ColumnDescription = "folio de control interno del comprobante")]
        public int Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [DataNames("_aptrms_ver")]
        [SugarColumn(ColumnName = "_aptrms_ver", ColumnDescription = "version del comprobante", Length = 3, DefaultValue = "2.0")]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        [DataNames("_aptrms_serie")]
        [SugarColumn(ColumnName = "_aptrms_serie", ColumnDescription = "serie de control interno del comprobante", Length = 10, IsNullable = true)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden del cliente
        /// </summary>
        [DataNames("_aptrms_ordcli")]
        [SugarColumn(ColumnName = "_aptrms_ordcli", ColumnDescription = "numero de orden del cliente", IsNullable = true, Length = 21)]
        public string NoOrdenCliente {
            get { return this.noOrdenCliente; }
            set {
                this.noOrdenCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("_aptrms_uuid")]
        [SugarColumn(ColumnName = "_aptrms_uuid", ColumnDescription = "identificador de la remision", Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptrms_rfce")]
        [SugarColumn(ColumnName = "_aptrms_rfce", ColumnDescription = "registro federal de contribuyentes del emisor", Length = 14, IsNullable = true)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptrms_nome")]
        [SugarColumn(ColumnName = "_aptrms_nome", ColumnDescription = "nombre o razon social del emisor", Length = 255)]
        public string EmisorNombre {
            get { return this.emisorNombre; }
            set {
                this.emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptrms_rfcr")]
        [SugarColumn(ColumnName = "_aptrms_rfcr", ColumnDescription = "registro federal de contribuyentes del receptor", Length = 14, IsNullable = true)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "");
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("_aptrms_nomr")]
        [SugarColumn(ColumnName = "_aptrms_nomr", ColumnDescription = "nombre o razon social del receptor", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptrms_nomr")]
        [SugarColumn(ColumnName = "_aptrms_cntct", ColumnDescription = "nombre del contacto", Length = 255)]
        public string Contacto {
            get { return this.contacto; }
            set { this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("_aptrms_fecems")]
        [SugarColumn(ColumnName = "_aptrms_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega
        /// </summary>
        [DataNames("_aptrms_fecent")]
        [SugarColumn(ColumnName = "_aptrms_fecent", ColumnDescription = "fecha de entrega al cliente", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate)
                    return this.fechaEntrega;
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de pago del comprobante
        /// </summary>
        [DataNames("_aptrms_fecupc")]
        [SugarColumn(ColumnName = "_aptrms_fecupc", ColumnDescription = "fecha del ultimo pago del comprobante", IsNullable = true)]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltPago >= firstGoodDate)
                    return this.fechaUltPago;
                return null;
            }
            set {
                this.fechaUltPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_aptrms_feccnc")]
        [SugarColumn(ColumnName = "_aptrms_feccnc", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de recepcion de cobranza
        /// </summary>
        [DataNames("_aptrms_feccbr")]
        [SugarColumn(ColumnName = "_aptrms_feccbr", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaCobranza {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCobranza >= firstGoodDate)
                    return this.fechaCobranza;
                return null;
            }
            set {
                this.fechaCobranza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_aptrms_fm")]
        [SugarColumn(ColumnName = "_aptrms_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        [DataNames("_aptrms_tpcmb")]
        [SugarColumn(ColumnName = "_aptrms_tpcmb", ColumnDescription = "tipo de cambio", IsNullable = true)]
        public decimal TipoCambio {
            get { return this.tipoCambio; }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de retencion del impuesto ISR
        /// </summary>
        [DataNames("_aptrms_retisr")]
        [SugarColumn(ColumnName = "_aptrms_retisr", ColumnDescription = "total de retencion del impuesto ISR", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionISR {
            get { return this.retencionISR; }
            set {
                this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IVA
        /// </summary>
        [DataNames("_aptrms_retiva")]
        [SugarColumn(ColumnName = "_aptrms_retiva", ColumnDescription = "total del impuesto retenido IVA", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionIVA {
            get { return this.retencionIVA; }
            set {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA
        /// </summary>
        [DataNames("_aptrms_trsiva")]
        [SugarColumn(ColumnName = "_aptrms_trsiva", ColumnDescription = "total del impuesto trasladado IVA", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIVA {
            get { return this.trasladoIVA; }
            set { this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IEPS
        /// </summary>
        [DataNames("_aptrms_retieps")]
        [SugarColumn(ColumnName = "_aptrms_retieps", ColumnDescription = "total del impuesto retenido IEPS", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionIEPS {
            get { return this.retencionIEPS; }
            set { this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto traslado IEPS
        /// </summary>
        [DataNames("_aptrms_trsieps")]
        [SugarColumn(ColumnName = "_aptrms_trsieps", ColumnDescription = "total del impuesto traslado IEPS", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIEPS {
            get { return this.trasladoIEPS; }
            set { trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subtotal del comprobante
        /// </summary>
        [DataNames("_aptrms_sbttl")]
        [SugarColumn(ColumnName = "_aptrms_sbttl", ColumnDescription = "subtotal del comprobante", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal SubTotal {
            get { return this.subTotal; }
            set { this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del descuento
        /// </summary>
        [DataNames("_aptrms_dscnt")]
        [SugarColumn(ColumnName = "_aptrms_dscnt", ColumnDescription = "importe del descuento aplicado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal ImporteDescuento {
            get { return this.descuento; }
            set { this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("_aptrms_total")]
        [SugarColumn(ColumnName = "_aptrms_total", ColumnDescription = "total del comprobante", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Total {
            get { return this.total; }
            set { this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe por cobrar del comprobante
        /// </summary>
        [DataNames("_aptrms_xcbrd")]
        [SugarColumn(ColumnName = "_aptrms_xcbrd", ColumnDescription = "importe del comprobante por cobrar", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal PorCobrar {
            get { return this.porCobrar; }
            set { this.porCobrar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acumulado de la cobranza del comprobante
        /// </summary>
        [DataNames("_aptrms_cbrd")]
        [SugarColumn(ColumnName = "_aptrms_cbrd", ColumnDescription = "acumulado de la cobranza del comprobante", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado {
            get { return this.acumulado; }
            set { this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe pagado del comprobante
        /// </summary>
        [DataNames("_aptrms_cbrdp")]
        [SugarColumn(ColumnName = "_aptrms_cbrdp", ColumnDescription = "importe pagado del comprobante", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal ImportePagado {
            get { return this.importePagado; }
            set { this.importePagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_aptrms_mtdpg")]
        [SugarColumn(ColumnName = "_aptrms_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3, IsNullable = true)]
        public string ClaveMetodoPago {
            get { return this.claveMetodoPago; }
            set { this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_aptrms_frmpg")]
        [SugarColumn(ColumnName = "_aptrms_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2, IsNullable = true)]
        public string ClaveFormaPago {
            get { return this.claveFormaPago; }
            set { this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_aptrms_moneda")]
        [SugarColumn(ColumnName = "_aptrms_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3, IsNullable = true)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set { this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociada
        /// </summary>
        [DataNames("_aptrms_vnddr")]
        [SugarColumn(ColumnName = "_aptrms_vnddr", ColumnDescription = "clave del vendedor", Length = 10, IsNullable = true)]
        public string Vendedor {
            get { return this.vendedor; }
            set { this.vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("_aptrms_usr_n")]
        [SugarColumn(ColumnName = "_aptrms_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10, IsNullable = true)]
        public string Creo {
            get { return this.creo; }
            set { this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        [DataNames("_aptrms_usr_m")]
        [SugarColumn(ColumnName = "_aptrms_usr_m", ColumnDescription = "clave del ultimo usuario que modifica", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = false)]
        public string Modifica {
            get { return this.modifica; }
            set { this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer una nota
        /// </summary>
        [DataNames("_aptrms_nota")]
        [SugarColumn(ColumnName = "_aptrms_nota", ColumnDescription = "nota adicional", IsNullable = true, Length = 255)]
        public string Nota {
            get { return this.nota; }
            set { this.nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptrms_dom")]
        [SugarColumn(ColumnName = "_aptrms_dom", ColumnDescription = "domicilio fiscal o de entrega al cliente", ColumnDataType = "TEXT", IsNullable = true)]
        public string DomicilioJson {
            get { return this.domicilio.Json(); }
            set { this.domicilio = Jaeger.Domain.Almacen.PT.Entities.DomicilioEntregaModel.Json(value);
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public Jaeger.Domain.Almacen.PT.Entities.DomicilioEntregaModel Domicilio {
            get { return this.domicilio; }
            set { this.domicilio = value;
                this.OnPropertyChanged();
            }
        }
    }
}
