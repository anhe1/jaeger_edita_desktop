﻿using System;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.Banco.Entities {
    public class MovimientoBancarioModelView {
        private DateTime? fechaAplicacion;

        public MovimientoBancarioModelView() {
        }

        [DataNames("documento")]
        public string TipoDocumentoText {
            get;set;
        }

        public MovimientoBancarioTipoComprobanteEnum TipoDocumento {
            get {
                return (MovimientoBancarioTipoComprobanteEnum)Enum.Parse(typeof(MovimientoBancarioTipoComprobanteEnum), this.TipoDocumentoText);
            }
            set {
                this.TipoDocumentoText = Enum.GetName(typeof(MovimientoBancarioTipoComprobanteEnum), value);
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        [DataNames("_bnccmp_sbtp", "_cfdi_doc_id")]
        public int SubTipoComprobanteInt {
            get;set;
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante fiscal, emitido, recibido, nomina, etc
        /// </summary>           
        public CFDISubTipoEnum SubTipoComprobante {
            get {
                if (this.SubTipoComprobanteInt == 1)
                    return CFDISubTipoEnum.Emitido;
                else if (this.SubTipoComprobanteInt == 2)
                    return CFDISubTipoEnum.Recibido;
                else if (this.SubTipoComprobanteInt == 3)
                    return CFDISubTipoEnum.Nomina;
                else
                    return CFDISubTipoEnum.None;
            }
            set {
                this.SubTipoComprobanteInt = (int)value;
            }
        }

        /// <summary>
        /// obtener o establecer el efecto del comprobante
        /// </summary>
        [DataNames("efecto")]
        public string EfectoComprobante {
            get;set;
        }

        [DataNames("idcomprobante")]
        public int IdComprobante {
            get;set;
        }

        /// <summary>
        /// obtener o establecer el folio fiscal del comprobante (uuid)
        /// </summary>
        [DataNames("uuid")]
        public string IdDocumento {
            get;set;
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("total")]
        public decimal Total {
            get;set;
        }

        [DataNames("cargo")]
        public decimal Cargo {
            get;set;
        }

        [DataNames("abono")]
        public decimal Abono {
            get;set;
        }

        /// <summary>
        /// obtener o establecer el monto cobrado o pagado del comprobante
        /// </summary>
        public decimal Acumulado {
            get;set;
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de aplicacion
        /// </summary>
        [DataNames("aplicacion")]
        public DateTime? FechaAplicacion {
            get {
                if (this.fechaAplicacion >= new DateTime(1900, 1, 1))
                    return this.fechaAplicacion;
                return null;
            }
            set {
                this.fechaAplicacion = value;
            }
        }

        public string Status {
            get {
                if (this.TipoDocumento == MovimientoBancarioTipoComprobanteEnum.CFDI) {
                    if (this.SubTipoComprobante == CFDISubTipoEnum.Emitido | this.SubTipoComprobante == CFDISubTipoEnum.Nomina) {
                        this.Acumulado = this.Cargo;
                        if (this.Acumulado + new decimal(0.05) >= this.Total) {
                            return CFDIStatusEmitidoEnum.Cobrado.ToString();
                        } else {
                            return CFDIStatusEmitidoEnum.PorCobrar.ToString();
                        }
                    } else if (this.SubTipoComprobante == CFDISubTipoEnum.Recibido) {
                        this.Acumulado = this.Abono;
                        if (this.Acumulado >= this.Total) {
                            return CFDIStatusRecibidoEnum.Pagado.ToString();
                        } else {
                            return CFDIStatusRecibidoEnum.PorPagar.ToString();
                        }
                    }
                }
                return null;
            }
        }
    }
}
