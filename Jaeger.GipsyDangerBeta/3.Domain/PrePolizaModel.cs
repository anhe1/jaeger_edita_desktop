﻿using System;
using SqlSugar;
using System.Linq;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.Contable.Entities {
    [SugarTable("_cntbl3", "prepolizas")]
    public partial class PrePolizaModel : BasePropertyChangeImplementation {

        #region declaraciones

        private DateTime? fechaDoctoField;
        private DateTime? fechaPagoField;
        private DateTime? fechaVenceField;
        private DateTime? fechaBovedaField;
        private DateTime? fechaCancelaField;
        private DateTime? fechaModificaField;
        private int index;
        private bool isActive;
        private int folio;
        private int subIndex;
        private bool porJustificar;
        private DateTime fechaEmision;
        private DateTime fechaNuevo;
        private decimal cargo;
        private decimal abono;
        private string formaDePagoText;
        private PrePolizaCuenta emisor;
        private PrePolizaCuenta receptor;
        private string tipoText;
        private string noIndet;
        private string serie;
        private string estadoText;
        private string creo;
        private string modifica;
        private string cancela;
        private string autoriza;
        private string numDocto;
        private string formaDePagoDescripcion;
        private string referencia;
        private string numAutorizacion;
        private string notas;
        private string concepto;

        #endregion

        #region declaraciones

        private string moneda;
        private decimal tipoCambio;
        private string numOperacion;
        private PrePolizaRelacionados prePolizasRelacionadasField;
        private decimal totalComprobantesField;
        private bool paraAbonoField;
        private bool multiPagoField;
        private string recibeField;
        private BindingList<PrePolizaComprobanteModel> comprobantes;

        private string referenciaNumerica;

        #endregion

        public enum PrePolizaStatusEnum {
            [Description("Cancelado")]
            Cancelado,
            [Description("No Aplicado")]
            NoAplicado,
            [Description("Aplicado")]
            Aplicado,
            [Description("Auditado")]
            Auditado
        }

        public enum PolizaTipoEnum {
            Ninguno,
            Ingreso,
            Egreso,
            Diario
        }

        /// <summary>
        /// constructor
        /// </summary>
        public PrePolizaModel() {
            this.IsActive = true;
            this.FechaEmision = DateTime.Now;
            this.FechaDocto = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.FechaPago = null;
            this.Moneda = "MXN";
            this.TipoCambio = 1;
            this.Estado = PrePolizaStatusEnum.NoAplicado;

            this.comprobantes = new BindingList<PrePolizaComprobanteModel>() {
                RaiseListChangedEvents = true
            };
            this.comprobantes.AddingNew += ComprobantesField_AddingNew;
            this.comprobantes.ListChanged += Comprobantes_ListChanged;
            this.prePolizasRelacionadasField = new PrePolizaRelacionados();
            this.Emisor = new PrePolizaCuenta();
            this.Receptor = new PrePolizaCuenta();
        }

        /// <summary>
        /// constructor
        /// </summary>
        public PrePolizaModel(string creo) {
            this.IsActive = true;
            this.FechaEmision = DateTime.Now;
            this.FechaDocto = DateTime.Now;
            this.FechaPago = null;
            this.Moneda = "MXN";
            this.TipoCambio = 1;
            this.Estado = PrePolizaStatusEnum.NoAplicado;
            this.Creo = creo;
            this.comprobantes = new BindingList<PrePolizaComprobanteModel>() {
                RaiseListChangedEvents = true
            };
            this.comprobantes.AddingNew += ComprobantesField_AddingNew;
            this.comprobantes.ListChanged += Comprobantes_ListChanged;
            this.prePolizasRelacionadasField = new PrePolizaRelacionados();
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>
        [JsonIgnore]
        [DataNames("_cntbl3_id")]
        [SugarColumn(ColumnName = "_cntbl3_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_a")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cntbl3_a", ColumnDescription = "registro activo", IsNullable = false, Length = 1)]
        public bool IsActive {
            get {
                return this.isActive;
            }
            set {
                this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con las cuentas de banco
        /// </summary>
        [JsonIgnore]
        [DataNames("_cntbl3_sbid")]
        [SugarColumn(ColumnName = "_cntbl3_sbid", ColumnDescription = "indice de relacion con la cuenta bancaria (_cntbl3b)", IsNullable = false)]
        public int IdCuentaBanco {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        [JsonProperty("noIdent")]
        [DataNames("_cntbl3_noiden")]
        [SugarColumn(ColumnName = "_cntbl3_noiden", ColumnDescription = "identificador de la prepoliza", IsNullable = true, Length = 11)]
        public string NoIndet {
            get {
                return this.noIndet;
            }
            set {
                this.noIndet = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:status del documento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("estado")]
        [DataNames("_cntbl3_status")]
        [SugarColumn(ColumnName = "_cntbl3_status", ColumnDescription = "status del documento", IsNullable = true, Length = 10)]
        public string EstadoText {
            get {
                return this.estadoText;
            }
            set {
                this.estadoText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:folio de control
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("folio")]
        [DataNames("_cntbl3_folio")]
        [SugarColumn(ColumnName = "_cntbl3_folio", IsNullable = true, Length = 11)]
        public int Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de la prepoliza
        /// Default:current_timestamp()
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecEmision")]
        [DataNames("_cntbl3_fecems")]
        [SugarColumn(ColumnName = "_cntbl3_fecems", ColumnDescription = "fecha de la prepoliza", IsNullable = true)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha del documento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecDocto")]
        [DataNames("_cntbl3_fecdoc")]
        [SugarColumn(ColumnName = "_cntbl3_fecdoc", ColumnDescription = "fecha del documento", IsNullable = true)]
        public DateTime? FechaDocto {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaDoctoField >= firstGoodDate)
                    return this.fechaDoctoField;
                else
                    return null;
            }
            set {
                this.fechaDoctoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de vencimiento
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [JsonProperty("fecVence")]
        [DataNames("_cntbl3_fcvnc")]
        [SugarColumn(ColumnName = "_cntbl3_fcvnc", ColumnDescription = "fecha de vencimiento", IsNullable = true)]
        public DateTime? FechaVence {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaVenceField >= firstGooDate)
                    return this.fechaVenceField;
                else
                    return null;
            }
            set {
                this.fechaVenceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cobro o pago
        /// </summary>           
        [JsonProperty("fecPago")]
        [DataNames("_cntbl3_fccbr")]
        [SugarColumn(ColumnName = "_cntbl3_fccbr", ColumnDescription = "fecha de cobro o pago", IsNullable = true)]
        public DateTime? FechaPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                    return this.fechaPagoField;
                else
                    return null;
            }
            set {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de liberación (transmición al banco, por default es null) 
        /// </summary>           
        [JsonProperty("fecBoveda")]
        [DataNames("_cntbl3_fclib")]
        [SugarColumn(ColumnName = "_cntbl3_fclib", ColumnDescription = "fecha de liberación (transmición al banco)", IsNullable = true)]
        public DateTime? FechaBoveda {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaBovedaField >= firstGooDate)
                    return this.fechaBovedaField;
                return null;
            }
            set {
                this.fechaBovedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion (default null)
        /// </summary>           
        [DataNames("_cntbl3_fccncl")]
        [SugarColumn(ColumnName = "_cntbl3_fccncl", ColumnDescription = "fecha de cancelacion", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaCancelaField >= firstGooDate)
                    return this.fechaCancelaField;
                return null;
            }
            set {
                this.fechaCancelaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda (json)
        /// </summary>
        [JsonProperty("claveMoneda")]
        [SugarColumn(IsIgnore = true)]
        public string Moneda {
            get {
                return this.moneda;
            }
            set {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio (json)
        /// </summary>
        [JsonProperty("tipoCambio")]
        [SugarColumn(IsIgnore = true)]
        public decimal TipoCambio {
            get {
                return this.tipoCambio;
            }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro, (aqui la fecha actua como fecha de emision del comprobante de pago o cobro, default current_timestamp())
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fn")]
        [SugarColumn(ColumnDataType = "TIMESTAMP", ColumnName = "_cntbl3_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion, del registro (default null)
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fm")]
        [SugarColumn(ColumnName = "_cntbl3_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaModificaField >= firstGooDate)
                    return this.fechaModificaField;
                return null;
            }
            set {
                this.fechaModificaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el cargo al documento (default 0)
        /// </summary>           
        [JsonProperty("cargo")]
        [DataNames("_cntbl3_cargo")]
        [SugarColumn(ColumnName = "_cntbl3_cargo", ColumnDescription = "cargo", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el abono al documento (default 0)
        /// </summary>           
        [JsonProperty("abono")]
        [DataNames("_cntbl3_abono")]
        [SugarColumn(ColumnName = "_cntbl3_abono", ColumnDescription = "abono", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:bandera solo para indicar si el gasto es por justificar
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [JsonProperty("porJustificar")]
        [DataNames("_cntbl3_por")]
        [SugarColumn(ColumnName = "_cntbl3_por", ColumnDescription = "bandera solo para indicar si el gasto es por justificar", IsNullable = false, Length = 1)]
        public bool PorJustificar {
            get {
                return this.porJustificar;
            }
            set {
                this.porJustificar = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("paraAbono")]
        [SugarColumn(IsIgnore = true)]
        public bool ParaAbono {
            get {
                return this.paraAbonoField;
            }
            set {
                this.paraAbonoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (json)
        /// </summary>
        [JsonProperty("multiPago")]
        [SugarColumn(IsIgnore = true)]
        public bool MultiPago {
            get {
                return this.multiPagoField;
            }
            set {
                this.multiPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// informacion del emisor del recibo
        /// </summary>
        [JsonProperty("emisor")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaCuenta Emisor {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// informacion del receptor del recibo
        /// </summary>
        [JsonProperty("receptor")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaCuenta Receptor {
            get {
                return this.receptor;
            }
            set {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la forma de pago (default null)
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_frmclv")]
        [SugarColumn(ColumnName = "_cntbl3_frmclv", ColumnDescription = "clave de la forma de pago", Length = 2)]
        public string FormaDePagoText {
            get {
                return this.formaDePagoText;
            }
            set {
                this.formaDePagoText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de banco emisor segun catalogo SAT
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_bncclve")]
        [SugarColumn(ColumnName = "_cntbl3_bncclve", ColumnDescription = "clave de banco emisor segun catalogo SAT", Length = 3)]
        public string EmisorCodigo {
            get {
                return this.Emisor.Codigo;
            }
            set {
                this.Emisor.Codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de banco receptor segun catalogo SAT
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_bncclvr")]
        [SugarColumn(ColumnName = "_cntbl3_bncclvr", ColumnDescription = "clave del banco receptor segun catalogo SAT", Length = 3)]
        public string ReceptorCodigo {
            get {
                return this.Receptor.Codigo;
            }
            set {
                this.Receptor.Codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del documento (I - Ingreso, E-Egreso, D-Diario)
        /// </summary>           
        [JsonProperty("tipo")]
        [DataNames("_cntbl3_tipo")]
        [SugarColumn(ColumnName = "_cntbl3_tipo", ColumnDescription = "clave del documento (I - Ingreso, E-Egreso)", IsNullable = true, Length = 10)]
        public string TipoText {
            get {
                return this.tipoText;
            }
            set {
                this.tipoText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer serie de control interno
        /// </summary>           
        [JsonProperty("serie")]
        [DataNames("_cntbl3_serie")]
        [SugarColumn(ColumnName = "_cntbl3_serie", ColumnDescription = "serie de control interno", IsNullable = true, Length = 10)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_n")]
        [SugarColumn(ColumnName = "_cntbl3_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = true, Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_m")]
        [SugarColumn(ColumnName = "_cntbl3_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", IsNullable = true, Length = 10)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario cancela el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_c")]
        [SugarColumn(ColumnName = "_cntbl3_usr_c", IsOnlyIgnoreInsert = true)]
        public string Cancela {
            get {
                return this.cancela;
            }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estblecer la clave del usuario que autoriza el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_usr_a")]
        [SugarColumn(ColumnName = "_cntbl3_usr_a", IsOnlyIgnoreInsert = true)]
        public string Autoriza {
            get {
                return this.autoriza;
            }
            set {
                this.autoriza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de control interno del emisor
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_clve")]
        [SugarColumn(ColumnName = "_cntbl3_clve", ColumnDescription = "clave de control interno del emisor", IsNullable = true, Length = 14)]
        public string EmisorClave {
            get {
                return this.Emisor.Clave;
            }
            set {
                this.Emisor.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de control interno del receptor
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_clvr")]
        [SugarColumn(ColumnName = "_cntbl3_clvr", ColumnDescription = "obtener o establecer la clave de control interno del receptor", IsNullable = true, Length = 14)]
        public string ReceptorClave {
            get {
                return this.Receptor.Clave;
            }
            set {
                this.Receptor.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del receptor del documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_rfcr")]
        [SugarColumn(ColumnName = "_cntbl3_rfcr")]
        public string ReceptorRFC {
            get {
                return this.Receptor.RFC;
            }
            set {
                this.Receptor.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del emisor del documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_rfce")]
        [SugarColumn(ColumnName = "_cntbl3_rfce", ColumnDescription = "rfc del beneficiario", IsNullable = true, Length = 14)]
        public string EmisorRFC {
            get {
                return this.Emisor.RFC;
            }
            set {
                this.Emisor.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta del emisor
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_nmctae")]
        [SugarColumn(ColumnName = "_cntbl3_nmctae", ColumnDescription = "numero de cuenta del emisor", IsNullable = true, Length = 14)]
        public string EmisorNumCta {
            get {
                return this.Emisor.NumCta;
            }
            set {
                this.Emisor.NumCta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta del receptor
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_nmctar")]
        [SugarColumn(ColumnName = "_cntbl3_nmctar", ColumnDescription = "numero de cuenta del receptor", IsNullable = true, Length = 14)]
        public string ReceptorNumCta {
            get {
                return this.Receptor.NumCta;
            }
            set {
                this.Receptor.NumCta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de documento cheque transferencia etc
        /// </summary>           
        [JsonProperty("numDocto")]
        [DataNames("_cntbl3_nodocto")]
        [SugarColumn(ColumnName = "_cntbl3_nodocto", ColumnDescription = "numero de documento", IsNullable = true, Length = 20)]
        public string NumDocto {
            get {
                return this.numDocto;
            }
            set {
                this.numDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la sucursal de banco
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_scrslr")]
        [SugarColumn(ColumnName = "_cntbl3_scrslr", ColumnDescription = "numero de la sucursal del banco", IsNullable = true, Length = 20)]
        public string ReceptorSucursal {
            get {
                return this.Receptor.Sucursal;
            }
            set {
                this.Receptor.Sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cuenta clabe del receptor
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_clbr")]
        [SugarColumn(ColumnName = "_cntbl3_clbr", ColumnDescription = "cuenta clabe del receptor", IsNullable = true, Length = 20)]
        public string ReceptorCLABE {
            get {
                return this.Receptor.Clabe;
            }
            set {
                this.Receptor.Clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la forma de pago o cobro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_frmpg")]
        [SugarColumn(ColumnName = "_cntbl3_frmpg", ColumnDescription = "descripcion de la forma de pago o cobro", IsNullable = true, Length = 36)]
        public string FormaPagoDescripcion {
            get {
                return this.formaDePagoDescripcion;
            }
            set {
                this.formaDePagoDescripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del banco del receptor
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_bancor")]
        [SugarColumn(ColumnName = "_cntbl3_bancor", ColumnDescription = "nombre del banco del receptor", IsNullable = true, Length = 64)]
        public string ReceptorBanco {
            get {
                return this.Receptor.Banco;
            }
            set {
                this.Receptor.Banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia alfanumerica
        /// </summary>           
        [JsonProperty("ref")]
        [DataNames("_cntbl3_ref")]
        [SugarColumn(ColumnName = "_cntbl3_ref", ColumnDescription = "referencia alfanumerica", IsNullable = true, Length = 64)]
        public string Referencia {
            get {
                return this.referencia;
            }
            set {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de autorizacion bancaria
        /// </summary>           
        [JsonProperty("numAuto")]
        [DataNames("_cntbl3_numauto")]
        [SugarColumn(ColumnName = "_cntbl3_numauto", ColumnDescription = "numero de autorizacion", IsNullable = true, Length = 64)]
        public string NumAutorizacion {
            get {
                return this.numAutorizacion;
            }
            set {
                this.numAutorizacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota para este documento
        /// </summary>           
        [JsonProperty("nota")]
        [DataNames("_cntbl3_nota")]
        [SugarColumn(ColumnName = "_cntbl3_nota", ColumnDescription = "notas", IsNullable = true, Length = 64)]
        public string Notas {
            get {
                return this.notas;
            }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el concepto del documento 
        /// </summary>           
        [JsonProperty("concepto")]
        [DataNames("_cntbl3_cncpt")]
        [SugarColumn(ColumnName = "_cntbl3_cncpt", ColumnDescription = "concepto", IsNullable = true, Length = 256)]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del beneficiario
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_benef")]
        [SugarColumn(ColumnName = "_cntbl3_benef", ColumnDescription = "nombre del beneficiario", IsNullable = true, Length = 256)]
        public string ReceptorBeneficiario {
            get {
                return this.Receptor.Nombre;
            }
            set {
                this.Receptor.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        // [JsonIgnore]
        // [DataNames("_cntbl3_json")]
        // [SugarColumn(ColumnName = "_cntbl3_json", ColumnDataType = "Text", IsNullable = true)]
        // public string JPrepoliza { get; set; }

        /// <summary>
        /// obtener o establecer informacion de las autorizaciones para el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_jauto")]
        [SugarColumn(ColumnName = "_cntbl3_jauto", ColumnDataType = "Text", IsIgnore = true)]
        public string AutorizacioJson {
            get; set;
        }

        #region propiedades

        /// <summary>
        /// obtener o establecer el estado del comprobante
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaStatusEnum Estado {
            get {
                return (PrePolizaStatusEnum)Enum.Parse(typeof(PrePolizaStatusEnum), this.EstadoText);
            }
            set {
                this.EstadoText = Enum.GetName(typeof(PrePolizaStatusEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de poliza ingreso, egreso, diario
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public PolizaTipoEnum Tipo {
            get {
                return (PolizaTipoEnum)Enum.Parse(typeof(PolizaTipoEnum), this.TipoText);
            }
            set {
                this.TipoText = Enum.GetName(typeof(PolizaTipoEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer forma de pago
        /// </summary>
        [JsonProperty("formaPago")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaFormaPago FormaDePago {
            get {
                if (this.FormaDePagoText != null)
                    return new PrePolizaFormaPago { Clave = this.FormaDePagoText, Descripcion = this.FormaPagoDescripcion };
                return new PrePolizaFormaPago();
            }
            set {
                this.FormaDePagoText = value.Clave;
                this.FormaPagoDescripcion = value.Descripcion;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objeto de las prepolizas relacionadas a la instancia
        /// </summary>
        [JsonProperty("prePolizaRelacionada")]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaRelacionados Relacion {
            get {
                return this.prePolizasRelacionadasField;
            }
            set {
                this.prePolizasRelacionadasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de operacion (json)
        /// </summary>
        [JsonProperty("numOperacion")]
        [SugarColumn(IsIgnore = true)]
        public string NumOperacion {
            get {
                return this.numOperacion;
            }
            set {
                this.numOperacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia numerica, este dato pertenece al json
        /// </summary>
        [JsonProperty("refnum")]
        [SugarColumn(IsIgnore = true)]
        public string ReferenciaNumerica {
            get {
                return this.referenciaNumerica;
            }
            set {
                this.referenciaNumerica = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cntbl3_json")]
        [SugarColumn(ColumnName = "_cntbl3_json", ColumnDataType = "Text", IsNullable = true)]
        public string JPrepoliza {
            get {
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                return JsonConvert.SerializeObject(this, conf);
            }
            set {
                var item = JsonConvert.DeserializeObject<PrePolizaDetailModel>(value);
                if (item != null) {
                    this.Emisor = item.Emisor;
                    this.Receptor = item.Receptor;
                    this.Comprobantes = item.Comprobantes;
                    this._prePolizaModel = item;
                }
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("recibe")]
        [SugarColumn(IsIgnore = true)]
        public string Recibe {
            get {
                return this.recibeField;
            }
            set {
                this.recibeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// listado de comprobantes relacionados a la prepoliza
        /// </summary>
        [JsonProperty("comprobantes")]
        [SugarColumn(IsIgnore = true)]
        public BindingList<PrePolizaComprobanteModel> Comprobantes {
            get {
                return this.comprobantes;
            }
            set {
                if (this.comprobantes != null) {
                    this.comprobantes.ListChanged -= Comprobantes_ListChanged;
                }
                this.comprobantes = value;
                if (this.comprobantes != null) {
                    this.comprobantes.ListChanged += Comprobantes_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public bool Editable {
            get {
                if (this.Estado == PrePolizaStatusEnum.Aplicado | this.Estado == PrePolizaStatusEnum.Cancelado && this.PorJustificar == false) {
                    return false;
                }
                return !(this.Id > 0);
            }
        }

        #endregion

        #region metodos

        public object Clone() {
            return this.MemberwiseClone();
        }

        public void Clonar() {
            if (this.Tipo == PolizaTipoEnum.Ingreso || this.Tipo == PolizaTipoEnum.Egreso) {
                this.Id = 0;
                this.FechaEmision = DateTime.Now;
                this.FechaNuevo = DateTime.Now;
                this.FechaModifica = null;
                this.Modifica = null;
                this.NoIndet = "";
                // eliminar el id relacionado en las partidas
                foreach (PrePolizaComprobanteModel item in this.comprobantes) {
                    if (item.IsActive == false) {
                        this.comprobantes.Remove(item);
                    } else {
                        item.Id = 0;
                        item.SubId = 0;
                    }
                }
            }
        }

        private void ComprobantesField_AddingNew(object sender, AddingNewEventArgs e) {
        }

        private void Comprobantes_ListChanged(object sender, ListChangedEventArgs e) {
            this.totalComprobantesField = this.Comprobantes.Where((PrePolizaComprobanteModel p) => p.IsActive == true).Sum((PrePolizaComprobanteModel p) => p.Abono);
            if (this.PorJustificar == false)
                this.Cargo = this.totalComprobantesField;

            this.totalComprobantesField = this.Comprobantes.Where((PrePolizaComprobanteModel p) => p.IsActive == true && p.TipoComprobante == PrePolizaComprobanteModel.EnumCfdiType.Ingreso).Sum((PrePolizaComprobanteModel p) => p.Cargo);
            if (this.PorJustificar == false)
                this.Abono = this.totalComprobantesField;
        }

        public void Sustituir() {
            this.Relacion = new PrePolizaRelacionados();
            this.Relacion.TipoRelacion = "03 Sustitución";
            this.Relacion.PrePolizas.Add(new PrePolizaRelacionado { NoIndet = this.NoIndet, Emisor = this.Emisor, Receptor = this.Receptor, FechaEmision = this.FechaEmision, FormaDePago = this.FormaDePago, Abono = this.Abono, Cargo = this.Cargo });
            this.Clonar();
        }

        /// <summary>
        /// buscar un uuid en la lista de comprobantes para evitar duplicados
        /// </summary>
        public PrePolizaComprobanteModel Buscar(string uuid) {
            return this.Comprobantes.FirstOrDefault<PrePolizaComprobanteModel>((PrePolizaComprobanteModel p) => p.UUID == uuid);
        }

        public bool AplicarStatus() {
            if (this.Estado == PrePolizaStatusEnum.Cancelado)
                return false;

            if (this.Estado == PrePolizaStatusEnum.NoAplicado) {
                if (this.Comprobantes != null) {
                    if (this.Comprobantes.Count == 0) {
                        return false;
                    }
                } else {
                    return true;
                }
            }

            if (this.Estado == PrePolizaStatusEnum.Aplicado) {
            }
            return false;
        }

        public static PrePolizaDetailModel Json(string valueJson) {
            try {
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                return JsonConvert.DeserializeObject<PrePolizaDetailModel>(valueJson, conf);
            } catch (JsonException ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion

        [Browsable(false)]
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public PrePolizaModel _prePolizaModel {
            get; set;
        }

        [Browsable(false)]
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (this.comprobantes != null) {
                    if (this.Cargo != this.Abono) {
                        return "No coincide";
                    }
                }
                return string.Empty;
            }
        }
        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                return string.Empty;
            }
        }
    }

    ///<summary>
    /// clase que representa la relacion de comprobantes y prepoliza
    ///</summary>
    [SugarTable("_cntbl3c")]
    public partial class PrePolizaComprobanteModel : BasePropertyChangeImplementation {

        private decimal retencionIepsField;
        private decimal trasladoIepsField;
        private decimal saldoField;
        private decimal retencionIsrField;
        private decimal retencionIvaField;
        private int idComprobante;
        private string numPedido;
        private string noIndet;
        private decimal cargo;
        private decimal abono;
        private string vendedor;
        private string tipoDocumentoText;
        private int index;
        private int subId;
        private bool isActive;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private DateTime? fechaPago;
        private DateTime? fechaEmisionField;
        private DateTime? fechaTimbreField;
        private string version;
        private int subTipoComprobanteInt;
        private string folio;
        private string serie;
        private string status;
        private string estado;
        private string emisor;
        private string emisorRFC;
        private string emisorClave;
        private string receptor;
        private string receptorRFC;
        private string receptorClave;
        private string uUID;
        private decimal trasladoIVA;
        private decimal subtotal;
        private decimal descuento;
        private decimal total;
        private decimal acumulado;
        private DateTime? fechaEntregaField;
        private EnumCfdiType tipoComprobanteField;

        public enum EnumCfdiType {
            Ingreso,
            Egreso,
            Traslado,
            Nomina,
            Pagos
        }

        public enum MetadataTypeEnum {
            NA,
            CFD,
            CFDI,
            Certificate,
            Metadata,
            Acuse,
            TimbreFiscal,
            ValidationResult,
            Other,
            Polizas,
            Balanza,
            CatalogoCuentas,
            Retenciones,
            AuxiliarFolios,
            AuxiliarCtas,
            DPIVA,
            [Description("Remisión")]
            Remision,
            Devolucion,
            NotaDescuento
        }

        /// <summary>
        /// representa el subtipo del comprobante fiscal
        /// </summary>
        public enum EnumCfdiSubType {
            None,
            Emitido,
            Recibido,
            Nomina
        }

        public PrePolizaComprobanteModel() {
            this.IsActive = true;
            this.FechaNuevo = DateTime.Now;
            this.TipoDocumento = MetadataTypeEnum.CFDI;
        }


        /// <summary>
        /// obtener o establecer el indice de relacion con la tabla de comprobantes
        /// </summary>           
        [JsonProperty("idCom", Order = 0)]
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cntbl3c_idcom", ColumnDescription = "indice del comprobante", Length = 11, IsNullable = true)]
        public int IdComprobante {
            get {
                return this.idComprobante;
            }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de pedido relacionada al comprobante
        /// </summary>
        [JsonProperty("numPedido", Order = 1)]
        [DataNames("_cntbl3c_nmpdd")]
        [SugarColumn(ColumnName = "_cntbl3c_nmpdd", ColumnDescription = "numero de pedido", Length = 11, IsNullable = true)]
        public string NumPedido {
            get {
                return this.numPedido;
            }
            set {
                this.numPedido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:identificador de la prepoliza poliza
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("noIdent", Order = 2)]
        [DataNames("_cntbl3c_noiden")]
        [SugarColumn(ColumnName = "_cntbl3c_noiden", ColumnDescription = "identificador de la prepoliza", Length = 11, IsNullable = true)]
        public string NoIndet {
            get {
                return this.noIndet;
            }
            set {
                this.noIndet = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:cargo
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("cargo", Order = 3)]
        [DataNames("_cntbl3c_cargo")]
        [SugarColumn(ColumnName = "_cntbl3c_cargo", ColumnDescription = "cargo", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:abono
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [JsonProperty("abono", Order = 4)]
        [DataNames("_cntbl3c_abono")]
        [SugarColumn(ColumnName = "_cntbl3c_abono", ColumnDescription = "abono", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del vendedor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("vendedor", Order = 5)]
        [DataNames("_cntbl3c_vnddr")]
        [SugarColumn(ColumnName = "_cntbl3c_vnddr", ColumnDescription = "clave de vendedor", Length = 10, IsNullable = true)]
        public string Vendedor {
            get {
                return this.vendedor;
            }
            set {
                this.vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc
        /// </summary>           
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MetadataTypeEnum TipoDocumento {
            get {
                return (MetadataTypeEnum)Enum.Parse(typeof(MetadataTypeEnum), this.TipoDocumentoText);
            }
            set {
                this.TipoDocumentoText = Enum.GetName(typeof(MetadataTypeEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:tipo de comprobante
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonProperty("tipo", Order = 6)]
        [DataNames("_cntbl3c_tipo")]
        [SugarColumn(ColumnName = "_cntbl3c_tipo", ColumnDescription = "tipo de comprobante", Length = 14, IsNullable = true)]
        public string TipoDocumentoText {
            get {
                return this.tipoDocumentoText;
            }
            set {
                this.tipoDocumentoText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_id")]
        [SugarColumn(ColumnName = "_cntbl3c_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:indice de relacion con cntbl3
        /// Default:
        /// Nullable:True
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_subId")]
        [SugarColumn(ColumnName = "_cntbl3c_subId", ColumnDescription = "indice de relacion con cntbl3", Length = 11, IsNullable = true)]
        public int SubId {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_a")]
        [SugarColumn(ColumnDataType = "SMALINT", ColumnName = "_cntbl3c_a", ColumnDescription = "registro activo", DefaultValue = "1", Length = 1, IsNullable = true)]
        public bool IsActive {
            get {
                return this.isActive;
            }
            set {
                this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>  
        [JsonIgnore]
        [DataNames("_cntbl3c_usr_n")]
        [SugarColumn(ColumnName = "_cntbl3c_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_cntbl3c_usr_m")]
        [SugarColumn(ColumnName = "_cntbl3c_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 10, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del sistema (aqui la fecha actua como fecha de emision del documento)
        /// </summary>
        [JsonIgnore]
        [DataNames("_cntbl3c_fn")]
        [SugarColumn(ColumnName = "_cntbl3c_fn", ColumnDescription = "fec. sist. (aqui la fecha actua como fecha de emision del documento)", IsNullable = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecerla ultima fecha de modificacion del registro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3c_fm")]
        [SugarColumn(ColumnName = "_cntbl3c_fm", ColumnDescription = "fecha de modificacion", IsNullable = true)]
        public DateTime? FechaMod {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante fiscal
        /// </summary>
        [JsonProperty("ver", Order = 7)]
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cntbl3c_ver", ColumnDescription = "version del comprobante", Length = 3, IsNullable = true)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumCfdiType TipoComprobante {
            get {
                return this.tipoComprobanteField;
            }
            set {
                this.tipoComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (efecto: ingreso, egreso, traslado, nomina, pagos)
        /// </summary>
        [JsonProperty("tipoComprobante", Order = 8)]
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cntbl3c_efecto", ColumnDescription = "efecto del comprobante", Length = 10, IsIgnore = false)]
        public string TipoComprobanteText {
            get {
                return Enum.GetName(typeof(EnumCfdiType), this.TipoComprobante);
            }
            set {
                if (value == "Ingreso" | value == "I")
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                else if (value == "Egreso" | value == "E")
                    this.TipoComprobante = EnumCfdiType.Egreso;
                else if (value == "Traslado" | value == "T")
                    this.TipoComprobante = EnumCfdiType.Traslado;
                else if (value == "Nomina" | value == "N")
                    this.TipoComprobante = EnumCfdiType.Nomina;
                else if (value == "Pagos" | value == "P")
                    this.TipoComprobante = EnumCfdiType.Pagos;
                else
                    this.TipoComprobante = EnumCfdiType.Ingreso;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc
        /// </summary>           
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public EnumCfdiSubType SubTipoComprobante {
            get {
                if (this.SubTipoComprobanteInt == 1)
                    return EnumCfdiSubType.Emitido;
                else if (this.SubTipoComprobanteInt == 2)
                    return EnumCfdiSubType.Recibido;
                else if (this.SubTipoComprobanteInt == 3)
                    return EnumCfdiSubType.Nomina;
                else
                    return EnumCfdiSubType.None;
            }
            set {
                this.SubTipoComprobanteInt = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        [JsonProperty("subTipoComprobante", Order = 9)]
        [SugarColumn(IsIgnore = true)]
        public string SubTipoComprobanteText {
            get {
                return Enum.GetName(typeof(EnumCfdiSubType), this.SubTipoComprobante);
            }
            set {
                this.SubTipoComprobante = (EnumCfdiSubType)Enum.Parse(typeof(EnumCfdiSubType), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base)
        /// </summary>           
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cntbl3c_sbtp", ColumnDescription = "subtipo del comprobante", Length = 1, IsNullable = true)]
        public int SubTipoComprobanteInt {
            get {
                return this.subTipoComprobanteInt;
            }
            set {
                this.subTipoComprobanteInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el folio del comprobante fiscal
        /// </summary> 
        [JsonProperty("folio", Order = 10)]
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cntbl3c_folio", ColumnDescription = "folio de control interno", Length = 10, IsNullable = true)]
        public string Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie del comprobante fiscal
        /// </summary> 
        [JsonProperty("serie", Order = 11)]
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cntbl3c_serie", ColumnDescription = "serie de control interno", Length = 10, IsNullable = true)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [JsonProperty("fecEmision", Order = 12)]
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cntbl3c_fecems", ColumnDescription = "fecha de emision del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public DateTime? FechaEmision {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEmisionField >= firstGoodDate)
                    return this.fechaEmisionField;
                return null;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de pago del comprobante
        /// </summary>
        [JsonProperty("fecPago", Order = 13)]
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cntbl3c_feculpg", ColumnDescription = "fecha del ultimo pago", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public DateTime? FechaPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPago >= firstGoodDate)
                    return this.fechaPago;
                return null;
            }
            set {
                this.fechaPago = value;
                this.OnPropertyChanged();
            }
        }

        private decimal tipoDeCambioField;
        [JsonProperty("tipoCambio", Order = 14)]
        [SugarColumn(IsIgnore = true)]
        public decimal TipoCambio {
            get {
                return this.tipoDeCambioField;
            }
            set {
                this.tipoDeCambioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante fiscal
        /// </summary> 
        [JsonProperty("status", Order = 15)]
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cntbl3c_status", ColumnDescription = "status del documento", Length = 10, IsNullable = true)]
        public string Status {
            get {
                return this.status;
            }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ultimo estado del comprobante
        /// </summary>
        [JsonProperty("estado", Order = 16)]
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cntbl3c_estado", ColumnDescription = "ultimo estado del comprobante", Length = 10, IsNullable = true)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        private int numParcialidadField;
        /// <summary>
        /// Atributo condicional para expresar el número de parcialidad que corresponde al pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.
        /// </summary>
        [JsonProperty("numPar", Order = 17)]
        [DataNames("_cfdi_par")]
        [SugarColumn(IsIgnore = true)]
        public int NumParcialidad {
            get {
                return this.numParcialidadField;
            }
            set {

                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        private int precisionDecimalField;
        [JsonProperty("presc", Order = 18)]
        [DataNames("_cfdi_prec")]
        [SugarColumn(IsIgnore = true)]
        public int PrecisionDecimal {
            get {
                return this.precisionDecimalField;
            }
            set {
                this.precisionDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del emisor
        /// </summary> 
        [JsonProperty("emisor", Order = 19)]
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cntbl3c_nome", ColumnDescription = "nombre del emisor del comprobante", Length = 256, IsNullable = true)]
        public string Emisor {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del emisor
        /// </summary>
        [JsonProperty("emisorRFC", Order = 20)]
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cntbl3c_rfce", ColumnDescription = "rfc del emisor ", Length = 14, IsNullable = true)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del receptor
        /// </summary>
        [JsonProperty("emisorClave", Order = 21)]
        [SugarColumn(ColumnName = "_cntbl3c_clve", ColumnDescription = "clave del receptor", Length = 14, IsNullable = true)]
        public string EmisorClave {
            get {
                return this.emisorClave;
            }
            set {
                this.emisorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor
        /// </summary>
        [JsonProperty("receptor", Order = 22)]
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cntbl3c_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 256, IsNullable = true)]
        public string Receptor {
            get {
                return this.receptor;
            }
            set {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del recceptor del comprobante
        /// </summary>
        [JsonProperty("receptorRFC", Order = 23)]
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cntbl3c_rfcr", ColumnDescription = "rfc del receptor", Length = 14, IsNullable = true)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del receptor
        /// </summary>
        [JsonProperty("receptorClave", Order = 24)]
        [SugarColumn(ColumnName = "_cntbl3c_clvr", ColumnDescription = "clave del receptor", Length = 14, IsNullable = true)]
        public string ReceptorClave {
            get {
                return this.receptorClave;
            }
            set {
                this.receptorClave = value;
                this.OnPropertyChanged();
            }
        }

        private string claveMonedaField;
        /// <summary>
        /// Atributo requerido para identificar la clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("claveMoneda", Order = 25)]
        [DataNames("_cfdi_moneda")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveMoneda {
            get {
                return this.claveMonedaField;
            }
            set {
                this.claveMonedaField = value;
                this.OnPropertyChanged();
            }
        }

        private string claveFormaPagoField;
        [JsonProperty("claveFormaPago", Order = 26)]
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPagoField;
            }
            set {
                this.claveFormaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        private string claveMetodoPagoField;
        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [JsonProperty("claveMetodoPago", Order = 27)]
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(IsIgnore = true)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPagoField;
            }
            set {
                this.claveMetodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("fecTimbre", Order = 28)]
        [DataNames("_cfdi_feccert")]
        [SugarColumn(IsIgnore = true)]
        public DateTime? FechaTimbre {
            get {
                return this.fechaTimbreField;
            }
            set {
                this.fechaTimbreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio fiscal (uuid)
        /// </summary>  
        [JsonProperty("uuid", Order = 29)]
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cntbl3c_uuid", ColumnDescription = "folio fiscal", Length = 36, IsNullable = true)]
        public string UUID {
            get {
                return this.uUID;
            }
            set {
                this.uUID = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("retencionISR", Order = 30)]
        [DataNames("_cfdi_retisr")]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionISR {
            get {
                return this.retencionIsrField;
            }
            set {
                this.retencionIsrField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("retencionIVA", Order = 31)]
        [DataNames("_cfdi_retiva")]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionIVA {
            get {
                return this.retencionIvaField;
            }
            set {
                this.retencionIvaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del iva trasladado
        /// </summary>
        [JsonProperty("trasladoIVA", Order = 32)]
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cntbl3c_ivat", ColumnDescription = "total del iva trasladado", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIVA {
            get {
                return this.trasladoIVA;
            }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("retencionIEPS", Order = 33)]
        [DataNames("_cfdi_retieps")]
        [SugarColumn(IsIgnore = true)]
        public decimal RetencionIEPS {
            get {
                return this.retencionIepsField;
            }
            set {
                this.retencionIepsField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("trasladoIEPS", Order = 34)]
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(IsIgnore = true)]
        public decimal TrasladoIEPS {
            get {
                return this.trasladoIepsField;
            }
            set {
                this.trasladoIepsField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el sub total de comprobante
        /// </summary>
        [JsonProperty("subTotal", Order = 35)]
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cntbl3c_subtotal", ColumnDescription = "subtotal del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Subtotal {
            get {
                return this.subtotal;
            }
            set {
                this.subtotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto del descuento aplicable
        /// </summary>
        [JsonProperty("descuento", Order = 36)]
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cntbl3c_desc", ColumnDescription = "monto de descuento aplicable", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Descuento {
            get {
                return this.descuento;
            }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>  
        [JsonProperty("total", Order = 37)]
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cntbl3c_total", ColumnDescription = "total del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Total {
            get {
                return this.total;
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acumulado del comprobante
        /// </summary>
        [JsonProperty("acumulado", Order = 38)]
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cntbl3c_acumulado", ColumnDescription = "acumulado del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("saldo", Order = 39)]
        [DataNames("_cfdi_saldo")]
        [SugarColumn(IsIgnore = true)]
        public decimal Saldo {
            get {
                return this.saldoField;
            }
            set {
                this.saldoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de entrega del comprobante
        /// </summary>
        [JsonProperty("fecEntrega", Order = 40)]
        [SugarColumn(ColumnName = "_cntbl3c_fecent", ColumnDescription = "fecha de entrega del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntregaField >= firstGoodDate)
                    return this.fechaEntregaField;
                return null;
            }
            set {
                this.fechaEntregaField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject]
    public partial class PrePolizaCuenta : BasePropertyChangeImplementation {
        private int lngIndex;
        private int lngSubIndex;
        private bool blnIsActive;
        private string beneficiarioField;
        private string nombresField;
        private string primerApellidoField;
        private string segundoApellidoField;
        private string claveField;
        private string rfcField;
        private string numCtaField;
        private string codigoBancoField;
        private string nombreBancoField;
        private string sucursalField;
        private string clableField;
        private string tipoCuentaField;
        private string numClienteField;
        private string refNumericaField;
        private string refAlfaNumericaField;

        [JsonIgnore]
        public int Id {
            get {
                return this.lngIndex;
            }
            set {
                if (value != this.lngIndex) {
                    this.lngIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public bool IsActive {
            get {
                return this.blnIsActive;
            }
            set {
                if (value != this.blnIsActive) {
                    this.blnIsActive = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public int SubId {
            get {
                return this.lngSubIndex;
            }
            set {
                if (this.lngSubIndex != value) {
                    this.lngSubIndex = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// clave de control interno del beneficiario
        /// </summary>
        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre o razon social del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("benef")]
        public string Nombre {
            get {
                return this.beneficiarioField;
            }
            set {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre o nombres del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("nombres")]
        public string Nombres {
            get {
                return this.nombresField;
            }
            set {
                this.nombresField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("primerApellido")]
        public string PrimerApellido {
            get {
                return this.primerApellidoField;
            }
            set {
                this.primerApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("segundoApellido")]
        public string SegundoApellido {
            get {
                return this.segundoApellidoField;
            }
            set {
                this.segundoApellidoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de contribuyentes
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta
        /// </summary>
        [JsonProperty("numcta")]
        public string NumCta {
            get {
                return this.numCtaField;
            }
            set {
                this.numCtaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave de banco
        /// </summary>
        [JsonProperty("codigo")]
        public string Codigo {
            get {
                return this.codigoBancoField;
            }
            set {
                this.codigoBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre dela institucion bancaria
        /// </summary>
        [JsonProperty("banco")]
        public string Banco {
            get {
                return this.nombreBancoField;
            }
            set {
                this.nombreBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [JsonProperty("suc")]
        public string Sucursal {
            get {
                return this.sucursalField;
            }
            set {
                this.sucursalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta clabe para movimientos interbancarios
        /// </summary>
        [JsonProperty("clabe")]
        public string Clabe {
            get {
                return this.clableField;
            }
            set {
                this.clableField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("tipo")]
        public string TipoCuenta {
            get {
                return this.tipoCuentaField;
            }
            set {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cliente, este utilizando normalmente para layout de bancos
        /// </summary>
        [JsonProperty("numCliente")]
        public string NumCliente {
            get {
                return this.numClienteField;
            }
            set {
                this.numClienteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("refNumerica")]
        public string RefNumerica {
            get {
                return this.refNumericaField;
            }
            set {
                this.refNumericaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("refAlfaNumerica")]
        public string RefAlfaNumerica {
            get {
                return this.refAlfaNumericaField;
            }
            set {
                this.refAlfaNumericaField = value;
                this.OnPropertyChanged();
            }
        }
    }

    [JsonObject]
    public partial class PrePolizaRelacionados : BasePropertyChangeImplementation {
        private string tipoRelacionField;
        BindingList<PrePolizaRelacionado> prepolizasField;

        public PrePolizaRelacionados() {
            this.prepolizasField = new BindingList<PrePolizaRelacionado>() { RaiseListChangedEvents = true };
            this.prepolizasField.AddingNew += PrePolizasField_AddingNew;
            this.prepolizasField.ListChanged += PrePolizasField_ListChanged;
        }

        [JsonProperty("tipoRelacion")]
        public string TipoRelacion {
            get {
                return this.tipoRelacionField;
            }
            set {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("prePolizas")]
        public BindingList<PrePolizaRelacionado> PrePolizas {
            get {
                return this.prepolizasField;
            }
            set {
                if (this.prepolizasField != null) {
                    this.prepolizasField.AddingNew -= PrePolizasField_AddingNew;
                    this.prepolizasField.ListChanged -= PrePolizasField_ListChanged;
                }
                this.prepolizasField = value;
                if (this.prepolizasField != null) {
                    this.prepolizasField.AddingNew += PrePolizasField_AddingNew;
                    this.prepolizasField.ListChanged += PrePolizasField_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        private void PrePolizasField_AddingNew(object sender, AddingNewEventArgs e) {
        }

        private void PrePolizasField_ListChanged(object sender, ListChangedEventArgs e) {
        }
    }

    [JsonObject]
    public partial class PrePolizaRelacionado : BasePropertyChangeImplementation {
        private string noIndetField;
        private DateTime fechaEmisionField;
        private PrePolizaCuenta emisorField;
        private PrePolizaCuenta receptorField;
        private PrePolizaFormaPago formaDePagoField;
        private decimal cargoField;
        private decimal abonoField;

        public string NoIndet {
            get {
                return this.noIndetField;
            }
            set {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FechaEmision {
            get {
                return this.fechaEmisionField;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        public PrePolizaCuenta Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        public PrePolizaCuenta Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        public PrePolizaFormaPago FormaDePago {
            get {
                return this.formaDePagoField;
            }
            set {
                this.formaDePagoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Cargo {
            get {
                return this.cargoField;
            }
            set {
                this.cargoField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Abono {
            get {
                return this.abonoField;
            }
            set {
                this.abonoField = value;
                this.OnPropertyChanged();
            }
        }
    }

    public partial class PrePolizaFormaPago {
        private string clave;
        private string descripcion;

        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
            }
        }

        [JsonProperty("descrip")]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
            }
        }
    }

    /// <summary>
    /// clase de modelo de prepoliza
    /// </summary>
    public partial class PrePolizaDetailModel : PrePolizaModel, ICloneable {
        private BindingList<PrePolizaDoctoRelacionadoModel> doctosRelacionados;

        /// <summary>
        /// constructor
        /// </summary>
        public PrePolizaDetailModel() : base() {
            this.doctosRelacionados = new BindingList<PrePolizaDoctoRelacionadoModel>() {
                RaiseListChangedEvents = true
            };
            this.doctosRelacionados.AddingNew += DoctosRelacionados_AddingNew;
            this.doctosRelacionados.ListChanged += DoctosRelacionados_ListChanged;
        }

        /// <summary>
        /// constructor
        /// </summary>
        public PrePolizaDetailModel(string creo) : base(creo) {
            this.doctosRelacionados = new BindingList<PrePolizaDoctoRelacionadoModel>() {
                RaiseListChangedEvents = true
            };
            this.doctosRelacionados.AddingNew += DoctosRelacionados_AddingNew;
            this.doctosRelacionados.ListChanged += DoctosRelacionados_ListChanged;
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<PrePolizaDoctoRelacionadoModel> Doctos {
            get {
                return this.doctosRelacionados;
            }
            set {
                if (this.doctosRelacionados != null) {
                }
                this.doctosRelacionados = value;
                if (this.doctosRelacionados != null) {
                }
                this.OnPropertyChanged();
            }
        }

        private void DoctosRelacionados_AddingNew(object sender, AddingNewEventArgs e) {
            throw new NotImplementedException();
        }

        private void DoctosRelacionados_ListChanged(object sender, ListChangedEventArgs e) {
            //throw new NotImplementedException();
        }
    }

    [SugarTable("_cntbl3d", "contable: documentos relacionados")]
    public partial class PrePolizaDoctoRelacionadoModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private bool isActiveField;
        private string noIndetField;
        private string tituloField;
        private string descripcionField;
        private string contentField;
        private DateTime fechaNuevoField;
        private string creoField;
        private string urlDescargaField;

        /// <summary>
        /// cosntructor
        /// </summary>
        public PrePolizaDoctoRelacionadoModel() {
            this.isActiveField = true;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("_cntbl3d_id")]
        [SugarColumn(ColumnName = "_cntbl3d_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true, IsNullable = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_cntbl3d_a")]
        [SugarColumn(ColumnName = "_cntbl3d_a", ColumnDescription = "registro activo", IsNullable = true)]
        public bool IsActive {
            get {
                return this.isActiveField;
            }
            set {
                this.isActiveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla _cntbl3
        /// </summary>
        [DataNames("_cntbl3d_subid")]
        [SugarColumn(ColumnName = "_cntbl3d_subId", ColumnDescription = "indice de relacion con la tabla _cntbl3", IsNullable = true)]
        public int SubId {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero o clave de indentificacion
        /// </summary>
        [DataNames("_cntbl3d_noiden")]
        [SugarColumn(ColumnName = "_cntbl3d_noiden", ColumnDescription = "identificador de la prepoliza", Length = 11, IsNullable = true)]
        public string NoIndet {
            get {
                return this.noIndetField;
            }
            set {
                this.noIndetField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del archivo
        /// </summary>
        [DataNames("_cntbl3d_nom")]
        [SugarColumn(ColumnName = "_cntbl3d_nom", ColumnDescription = "nombre del archivo", Length = 128, IsNullable = true)]
        public string Titulo {
            get {
                return this.tituloField;
            }
            set {
                this.tituloField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion del documento
        /// </summary>
        [DataNames("_cntbl3d_desc")]
        [SugarColumn(ColumnName = "_cntbl3d_desc", ColumnDescription = "descripcion del documento anexo", Length = 128, IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de contenido de archivo
        /// </summary>
        [DataNames("_cntbl3d_cont")]
        [SugarColumn(ColumnName = "_cntbl3d_cont", ColumnDescription = "tipo de archivo", Length = 32, IsNullable = true)]
        public string Content {
            get {
                return this.contentField;
            }
            set {
                this.contentField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del nuevo registro
        /// </summary>
        [DataNames("_cntbl3d_fn")]
        [SugarColumn(ColumnDataType = "TIMESTAMP", ColumnName = "_cntbl3d_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevoField;
            }
            set {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("_cntbl3d_usr_n")]
        [SugarColumn(ColumnName = "_cntbl3d_usr_n", ColumnDescription = "clave del usuario quer crea el registro", Length = 10, IsNullable = true)]
        public string Creo {
            get {
                return this.creoField;
            }
            set {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url valida del archivo 
        /// </summary>
        [DataNames("_cntbl3d_url")]
        [SugarColumn(ColumnName = "_cntbl3d_url", ColumnDescription = "url", ColumnDataType = "TEXT", IsNullable = true)]
        public string URL {
            get {
                return this.urlDescargaField;
            }
            set {
                this.urlDescargaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ruta completa del archivo en el equipo
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Tag {
            get; set;
        }

        public string KeyName() {

            //use MD5 hash to get a 16-byte hash of the string:

            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Concat(this.noIndetField));

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();

        }
    }

    [SugarTable("_cntbl3b", "informacion de cuentas bancarias propias")]
    public partial class BancoCuentaModel : BasePropertyChangeImplementation {
        #region declaraciones

        private DateTime? fechaModifica;
        private int index;
        private bool isActive;
        private int diaCorte;
        private decimal cargoMaximo;
        private DateTime fechaNuevo;
        private string clave;
        private string moneda;
        private string creo;
        private string modifica;
        private string rfc;
        private string numCliente;
        private string clabe;
        private string alias;
        private string numeroDeCuenta;
        private string sucursal;
        private string banco;
        private string beneficiario;
        private string insitucionBancaria;

        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public BancoCuentaModel() {
            this.IsActive = true;
            this.FechaNuevo = DateTime.Now;
            this.Moneda = "MXN";
        }

        #region propiedades

        [DataNames("_cntbl3b_id")]
        [SugarColumn(ColumnName = "_cntbl3b_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("_cntbl3b_a")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_cntbl3b_a", ColumnDescription = "registro activo", Length = 1, IsNullable = false)]
        public bool IsActive {
            get {
                return this.isActive;
            }
            set {
                this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el dia de corte de la cuenta
        /// </summary>
        [DataNames("_cntbl3b_dcrt")]
        [SugarColumn(IsIgnore = true, ColumnDataType = "SMALLINT", ColumnName = "_cntbl3b_dcrt", ColumnDescription = "dia del corte", Length = 2, IsNullable = false)]
        public int DiaCorte {
            get {
                return this.diaCorte;
            }
            set {
                this.diaCorte = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        [JsonProperty("max")]
        [DataNames("_cntbl3b_cmax")]
        [SugarColumn(ColumnName = "_cntbl3b_cmax", ColumnDescription = "cargo maximo que se puede hacer a la cuenta", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal CargoMaximo {
            get {
                return this.cargoMaximo;
            }
            set {
                this.cargoMaximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_cntbl3b_fn")]
        [SugarColumn(ColumnName = "_cntbl3b_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [DataNames("_cntbl3b_fm")]
        [SugarColumn(ColumnName = "_cntbl3b_fm", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonProperty("clave")]
        [DataNames("_cntbl3b_clvb")]
        [SugarColumn(ColumnName = "_cntbl3b_clvb", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda a utilizar segun catalgo SAT
        /// </summary>
        [JsonProperty("moneda")]
        [DataNames("_cntbl3b_clv")]
        [SugarColumn(ColumnName = "_cntbl3b_clv", ColumnDescription = "clave de la moneda a utilizar segun catalgo SAT", Length = 3, IsNullable = true)]
        public string Moneda {
            get {
                return this.moneda;
            }
            set {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("_cntbl3b_usr_n")]
        [SugarColumn(ColumnName = "_cntbl3b_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_cntbl3b_usr_m")]
        [SugarColumn(ColumnName = "_cntbl3b_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes
        /// </summary>
        [JsonProperty("rfc")]
        [DataNames("_cntbl3b_rfc")]
        [SugarColumn(ColumnName = "_cntbl3b_rfc", ColumnDescription = "rfc del beneficiario de la cuenta de banco", Length = 14, IsNullable = true)]
        public string RFC {
            get {
                return this.rfc;
            }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de identificación del cliente, numero de contrato
        /// </summary>
        [JsonProperty("numCliente")]
        [DataNames("_cntbl3b_numcli")]
        [SugarColumn(ColumnName = "_cntbl3b_numcli", ColumnDescription = "numero de identificación del cliente, numero de contrato", Length = 14, IsNullable = true)]
        public string NumCliente {
            get {
                return this.numCliente;
            }
            set {
                this.numCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [JsonProperty("clabe")]
        [DataNames("_cntbl3b_clabe")]
        [SugarColumn(ColumnName = "_cntbl3b_clabe", ColumnDescription = "cuenta clabe", Length = 18, IsNullable = true)]
        public string Clabe {
            get {
                return this.clabe;
            }
            set {
                this.clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// alias de referencia para la cuenta bancaria
        /// </summary>
        [JsonProperty("alias")]
        [DataNames("_cntbl3b_alias")]
        [SugarColumn(ColumnName = "_cntbl3b_alias", ColumnDescription = "alias de la cuenta", Length = 20, IsNullable = true)]
        public string Alias {
            get {
                return this.alias;
            }
            set {
                this.alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonProperty("numCta")]
        [DataNames("_cntbl3b_numcta")]
        [SugarColumn(ColumnName = "_cntbl3b_numcta", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumeroDeCuenta {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [JsonProperty("sucursal")]
        [DataNames("_cntbl3b_scrsl")]
        [SugarColumn(ColumnName = "_cntbl3b_scrsl", ColumnDescription = "sucursal bancaria", Length = 20, IsNullable = true)]
        public string Sucursal {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [JsonProperty("banco")]
        [DataNames("_cntbl3b_banco")]
        [SugarColumn(ColumnName = "_cntbl3b_banco", ColumnDescription = "nombre del banco", Length = 64, IsNullable = true)]
        public string Banco {
            get {
                return this.banco;
            }
            set {
                this.banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("benef")]
        [DataNames("_cntbl3b_benef")]
        [SugarColumn(ColumnName = "_cntbl3b_benef", ColumnDescription = "nombre del beneficiario de la cuenta de banco", Length = 256, IsNullable = true)]
        public string Beneficiario {
            get {
                return this.beneficiario;
            }
            set {
                this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre corto de la institución bancaria
        /// </summary>
        [JsonProperty("descr")]
        [DataNames("_cntbl3b_nom")]
        [SugarColumn(ColumnName = "_cntbl3b_nom", ColumnDescription = "nombre de la institucion bancaria", Length = 256, IsNullable = true)]
        public string InsitucionBancaria {
            get {
                return this.insitucionBancaria;
            }
            set {
                this.insitucionBancaria = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static BancoCuentaModel Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<BancoCuentaModel>(inputJson);
            } catch (JsonException ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
