﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jaeger.Beta.Domain.AlmacenPT.Entities {
    public class RemisionConceptoDetailModel : RemisionConceptoModel {
        public RemisionConceptoDetailModel() {
            base.TasaTrasladoIVA = new decimal(.16);
        }

        public new RemisionConceptoDetailModel Clone() {
            var duplicado = (RemisionConceptoDetailModel)base.Clone();
            duplicado.IdConcepto = 0;
            // concepto parte sin referencia anterior
            var partes = new List<RemisionConceptoParte>(duplicado.Parte);
            duplicado.Parte = new BindingList<RemisionConceptoParte>();
            partes.ForEach((item) => duplicado.Parte.Add(item));
            return duplicado;
        }
    }
}
