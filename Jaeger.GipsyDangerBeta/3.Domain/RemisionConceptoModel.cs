﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.AlmacenPT.Entities {
    [SugarTable("_aptmov", "partidas o Conceptos del comprobante de remision")]
    public class RemisionConceptoModel : BasePropertyChangeImplementation, ICloneable {
        #region
        private int index;
        private bool activo;
        private int tipoComprobante;
        private int idRemision;
        private int noOrden;
        private int idModelo;
        private int idProducto;
        private int idPrecio;
        private int idCategoria;
        private int idUnidad;
        private decimal cantidadEntrada;
        private decimal cantidadSalida;
        private decimal unitario;
        private string claveUnidad;
        private string claveProdServ;
        private string codigo;
        private string unidad;
        private string numRequerimiento;
        private string noIdentificacion;
        private string marca;
        private string especificacion;
        private string descripcion;
        private string creo;
        private string modifica;
        private DateTime? fechaModifica;
        private DateTime fechaNuevo;
        private string noOrdenCliente;
        private decimal importe;
        private decimal subTotal;
        private decimal descuento;
        private BindingList<RemisionConceptoParte> parteField;
        private decimal trasladoIVA;
        private decimal tasaTrasladoIVA;
        private decimal tasaRetencionIVA;
        private decimal retencionIVA;
        private decimal tasaRetencionIEPS;
        private decimal retencionIEPS;
        private decimal tasaTrasladoIEPS;
        private decimal trasladoIEPS;
        private decimal tasaRetencionISR;
        private decimal retencionISR;
        #endregion

        public RemisionConceptoModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
            this.parteField = new BindingList<RemisionConceptoParte>() { RaiseListChangedEvents = true };
            this.parteField.ListChanged += Parte_ListChanged;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla de las partidas de la remision
        /// </summary>
        [DataNames("_aptmov_id")]
        [SugarColumn(ColumnName = "_aptmov_id", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdConcepto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("_aptmov_a")]
        [SugarColumn(ColumnName = "_aptmov_a", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_tp")]
        [SugarColumn(ColumnName = "_aptmov_tp", ColumnDescription = "define el tipo de comprobante al que se relacionada la partida (1 = remision, 2 = vale de entrada, 3 = vale de salida)", Length = 1, DefaultValue = "0", IsNullable = false)]
        public int TipoComprobante {
            get { return this.tipoComprobante; }
            set { this.tipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_rmsn_id")]
        [SugarColumn(ColumnName = "_aptmov_rmsn_id", ColumnDescription = "indice de relacion con la remision o devolucion del almacen de producto terminado", Length = 1, DefaultValue = "1", IsNullable = false)]
        public int IdRemision {
            get {
                return this.idRemision;
            }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la categoria del producto
        /// </summary>           
        [DataNames("_aptmov_idcat")]
        [SugarColumn(ColumnName = "_aptmov_idcat", ColumnDescription = "indice de relacion con la tabla de categorias", IsNullable = false)]
        public int IdCategoria {
            get {
                return this.idCategoria;
            }
            set {
                this.idCategoria = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        [DataNames("_aptmov_idpro")]
        [SugarColumn(ColumnName = "_aptmov_idpro", ColumnDescription = "indice de relación con la tabla de productos", IsNullable = false)]
        public int IdProducto {
            get {
                return this.idProducto;
            }
            set {
                this.idProducto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_idmod")]
        [SugarColumn(ColumnName = "_aptmov_idmod", ColumnDescription = "indice principal de la tabla", IsNullable = true)]
        public int IdModelo {
            get {
                return this.idModelo;
            }
            set {
                this.idModelo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_idpre")]
        [SugarColumn(ColumnName = "_aptmov_idpre", ColumnDescription = "indice del catalogo de precios", IsNullable = true)]
        public int IdPrecio {
            get {
                return this.idPrecio;
            }
            set {
                this.idPrecio = value;
                this.OnPropertyChanged();
            }
        }

        /// obtener o establecer la unidad de almacen
        /// </summary>
        [DataNames("_aptmov_undda")]
        [SugarColumn(ColumnName = "_aptmov_idund", ColumnDescription = "indice de la unidad de almacenamiento en el el almacen", IsNullable = false)]
        public int IdUnidad {
            get {
                return this.idUnidad;
            }
            set {
                this.idUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de pedido asociado al concepto de la remision
        /// </summary>
        [DataNames("_aptmov_noord")]
        [SugarColumn(ColumnName = "_aptmov_noord", ColumnDescription = "numero de orden o pedido asociado", IsNullable = false)]
        public int NoOrden {
            get {
                return this.noOrden;
            }
            set {
                this.noOrden = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_cante")]
        [SugarColumn(ColumnName = "_aptmov_cante", ColumnDescription = "cantidad entrante", Length = 14, DecimalDigits = 4, IsNullable = false)]
        public decimal CantidadEntrada {
            get {
                return this.cantidadEntrada;
            }
            set {
                this.cantidadEntrada = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_cants")]
        [SugarColumn(ColumnName = "_aptmov_cants", ColumnDescription = "cantidad saliente", Length = 14, DecimalDigits = 4, IsNullable = false)]
        public decimal CantidadSalida {
            get {
                return this.cantidadSalida;
            }
            set {
                this.cantidadSalida = value;
                this.ValorUnitario = this.ValorUnitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        [DataNames("_aptmov_untr")]
        [SugarColumn(ColumnName = "_aptmov_untr", ColumnDescription = "precio unitario", Length = 11, DecimalDigits = 6)]
        public decimal ValorUnitario {
            get {
                return this.unitario;
            }
            set {
                this.unitario = value;
                this.Importe = (this.CantidadSalida * this.ValorUnitario);
                this.TasaTrasladoIVA = this.tasaTrasladoIVA;
                this.SubTotal = (this.Importe - this.Descuento) + this.TrasladoIVA;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la tasa del impuesto del traslado de IVA
        /// </summary>
        [DataNames("_aptmov_ttiva")]
        [SugarColumn(ColumnName = "_aptmov_ttiva", ColumnDescription = "tasa del impuesto IVA trasladado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TasaTrasladoIVA {
            get { return this.tasaTrasladoIVA; }
            set { 
                if (value > new decimal(.16))
                    this.tasaTrasladoIVA = new decimal(.16);
                else
                    this.tasaTrasladoIVA = value;
                this.TrasladoIVA = this.Importe * this.tasaTrasladoIVA;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA
        /// </summary>
        [DataNames("_aptmov_itiva")]
        [SugarColumn(ColumnName = "_aptmov_itiva", ColumnDescription = "importe del impuesto IVA trasladado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_triva")]
        [SugarColumn(ColumnName = "_aptmov_triva", ColumnDescription = "tasa del impuesto IVA retenido", Length = 14, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal TasaRetencionIVA {
            get { return this.tasaRetencionIVA; }
            set { this.tasaRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_iriva")]
        [SugarColumn(ColumnName = "_aptmov_iriva", ColumnDescription = "importe del impuesto IVA retenido", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionIVA {
            get { return this.retencionIVA; }
            set { this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_tripes")]
        [SugarColumn(ColumnName = "_aptmov_tripes", ColumnDescription = "tasa del impuesto IEPS retenido", Length = 14, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal TasaRetencionIEPS {
            get { return this.tasaRetencionIEPS; }
            set { this.tasaRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_irieps")]
        [SugarColumn(ColumnName = "_aptmov_irieps", ColumnDescription = "importe del impuesto IEPS retenido", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionIEPS {
            get { return this.retencionIEPS; }
            set { this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_ttieps")]
        [SugarColumn(ColumnName = "_aptmov_ttieps", ColumnDescription = "tasa del impuesto IEPS trasladado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TasaTrasladoIEPS {
            get { return this.tasaTrasladoIEPS; }
            set { this.tasaTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_itieps")]
        [SugarColumn(ColumnName = "_aptmov_itieps", ColumnDescription = "importe del impuesto IEPS trasladado", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TrasladoIEPS {
            get { return this.trasladoIEPS; }
            set { this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_trisr")]
        [SugarColumn(ColumnName = "_aptmov_trisr", ColumnDescription = "tasa del impuesto ISR retenido", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal TasaRetencionISR {
            get { return this.tasaRetencionISR; }
            set { this.tasaRetencionISR = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_irisr")]
        [SugarColumn(ColumnName = "_aptmov_irisr", ColumnDescription = "importe del impuesto ISR retenido", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal RetencionISR {
            get { return this.retencionISR; }
            set { this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_imprt")]
        [SugarColumn(ColumnName = "_aptmov_imprt", ColumnDescription = "importe", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Importe {
            get {
                return this.importe;
            }
            set {
                this.importe = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_sbttl")]
        [SugarColumn(ColumnName = "_aptmov_sbttl", ColumnDescription = "subtotal", Length = 14, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal SubTotal {
            get {
                return this.subTotal;
            }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_dscnt")]
        [SugarColumn(ColumnName = "_aptmov_dscnt", ColumnDescription = "importe del descuento aplicado al concepto", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal Descuento {
            get {
                return this.descuento;
            }
            set {
                this.descuento = value;
                this.ValorUnitario = this.ValorUnitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de pedido del cliente hasta 21 caracteres
        /// </summary>
        [DataNames("_aptmov_noped")]
        [SugarColumn(ColumnName = "_aptmov_noped", ColumnDescription = "numero de pedido o orden de compra del cliente hasta 21 caracteres", Length = 21, IsNullable = true)]
        public string NoPedido {
            get { return this.noOrdenCliente; }
            set {
                this.noOrdenCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DataNames("_aptmov_clvund")]
        [SugarColumn(ColumnName = "_aptmov_clvund", ColumnDescription = "alm:|mp,pt,tw| desc: clave de unidad SAT", Length = 3, IsNullable = true)]
        public string ClaveUnidad {
            get {
                return this.claveUnidad;
            }
            set {
                this.claveUnidad = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [DataNames("_aptmov_clvprds")]
        [SugarColumn(ColumnName = "_aptmov_clvprds", ColumnDescription = "alm:|mp,pt,tw| desc: clave del producto o del servicio SAT", Length = 8, IsNullable = true)]
        public string ClaveProdServ {
            get {
                return this.claveProdServ;
            }
            set {
                if (value != null) {
                    this.claveProdServ = Regex.Replace(value.ToString(), "[^\\d]", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el codigo de barras asociado al producto
        /// </summary>           
        [DataNames("_aptmov_cdg")]
        [SugarColumn(ColumnName = "_aptmov_cdg", ColumnDescription = "alm:|mp,pt,tw| desc: codigo de barras", Length = 12, IsNullable = true)]
        public string Codigo {
            get {
                return this.codigo;
            }
            set {
                this.codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        [DataNames("_aptmov_undd")]
        [SugarColumn(ColumnName = "_aptmov_undd", ColumnDescription = "alm:|mp,pt,tw| desc: unidad personalizada", Length = 20, IsNullable = true)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número del pedimento que ampara la importación del bien que se expresa en el siguiente formato: 
        /// últimos 2 dígitos del año de validación seguidos por dos espacios, 2 dígitos de la aduana de despacho seguidos por dos espacios, 
        /// 4 dígitos del número de la patente seguidos por dos espacios, 1 dígito que corresponde al último dígito del año en curso, salvo 
        /// que se trate de un pedimento consolidado iniciado en el año inmediato anterior o del pedimento original de una rectificación, 
        /// seguido de 6 dígitos de la numeración progresiva por aduana.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}" longitud: 21
        /// </summary>
        [DataNames("_aptmov_numreq")]
        [SugarColumn(ColumnName = "_aptmov_numreq", ColumnDescription = "alm:|mp,pt,tw| desc: numero de requerimiento de aduana", Length = 50, IsNullable = true)]
        public string NumRequerimiento {
            get {
                return this.numRequerimiento;
            }
            set {
                this.numRequerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del estándar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,100}"
        [DataNames("_aptmov_sku")]
        [SugarColumn(ColumnName = "_aptmov_sku", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor (en cfdi: NoIdentificacion)", Length = 100, IsNullable = true)]
        public string NoIdentificacion {
            get {
                return this.noIdentificacion;
            }
            set {
                this.noIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:marca del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_aptmov_mrc")]
        [SugarColumn(ColumnName = "_aptmov_mrc", ColumnDescription = "alm:|mp,pt| desc: marca o fabricante", Length = 128, IsNullable = true)]
        public string Marca {
            get {
                return this.marca;
            }
            set {
                this.marca = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_espc")]
        [SugarColumn(ColumnName = "_aptmov_espc", ColumnDescription = "alm:|mp,pt| desc: espcificaciones", IsNullable = true, Length = 128)]
        public string Especificacion {
            get {
                return this.especificacion;
            }
            set {
                this.especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion corta del producto
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_aptmov_dscrc")]
        [SugarColumn(ColumnName = "_aptmov_dscrc", ColumnDescription = "alm:|mp,pt,tw| desc: descripcion corta", Length = 1000, IsNullable = true)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del usuario que crea el registro
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_aptmov_usr_n")]
        [SugarColumn(ColumnName = "_aptmov_usr_n", ColumnDescription = "clave del usuario que crea el registro", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del usuario que modifica
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_aptmov_usr_m")]
        [SugarColumn(ColumnName = "_aptmov_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener objeto json en formato string de la lista de la lista de ConceptoParte del Concepto
        /// </summary>
        [DataNames("_cfdcnp_parte")]
        public static string JsonParte(BindingList<RemisionConceptoParte> objeto) {
            return JsonConvert.SerializeObject(objeto);
        }

        [DataNames("_aptmov_fn")]
        [SugarColumn(ColumnName = "_aptmov_fn", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_aptmov_fm")]
        [SugarColumn(ColumnName = "_aptmov_fm", ColumnDescription = "fecha de creacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer lista de parte del conepto
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionConceptoParte> Parte {
            get {
                return this.parteField;
            }
            set {
                if (this.parteField != null)
                    this.parteField.ListChanged += new ListChangedEventHandler(Parte_ListChanged);
                this.parteField = value;
                if (this.parteField != null)
                    this.parteField.ListChanged += new ListChangedEventHandler(Parte_ListChanged);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener lista del objeto ConceptoParte para el concepto del comprobante
        /// </summary>
        public static BindingList<RemisionConceptoParte> JsonParte(string jsonIn) {
            if (jsonIn != "" && jsonIn != null) {
                return new BindingList<RemisionConceptoParte>(JsonConvert.DeserializeObject<List<RemisionConceptoParte>>(jsonIn));
            }
            else {
                return new BindingList<RemisionConceptoParte>();
            }
        }

        private void Parte_ListChanged(object sender, ListChangedEventArgs e) {
            if (this.parteField.Count > 0) {
                this.ValorUnitario = this.parteField.Sum((p) => p.ValorUnitario);
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
