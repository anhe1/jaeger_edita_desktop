﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.Banco.Entities {
    /// <summary>
    /// documento relacionado a movimiento bancario
    /// </summary>
    [SugarTable("_bncaux", "documentos relacionados a movmientos bancarios")]
    public class MovimientoBancarioAuxiliarModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private bool activo;
        private string descripcion;
        private string tipo;
        private string url;
        private DateTime fechaNuevo;
        private string creo;
        private string filename;

        public MovimientoBancarioAuxiliarModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// indice del auxiliar
        /// </summary>
        [DataNames("_bncaux_id")]
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "_bncaux_id", ColumnDescription = "indice del movimiento", IsNullable = false)]
        public int IdAuxiliar {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_bncaux_a")]
        [SugarColumn(ColumnName = "_bncaux_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el movimiento bancario
        /// </summary>
        [DataNames("_bncaux_sbid")]
        [SugarColumn(ColumnName = "_bncaux_sbid", ColumnDescription = "indice de relacion con la tabla de movimientos", DefaultValue = "0")]
        public int IdMovimiento {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descrpcion del auxiliar
        /// </summary>
        [DataNames("_bncaux_desc")]
        [SugarColumn(ColumnName = "_bncaux_desc", ColumnDescription = "descripcion del auxiliar", IsNullable = false, Length = 256)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del archivo, de forma temporal
        /// </summary>
        [DataNames("_bncaux_nom")]
        [SugarColumn(ColumnName = "_bncaux_nom", ColumnDescription = "nombre del archivo", IsNullable = false, Length = 64)]
        public string FileName {
            get {
                return this.filename;
            }
            set {
                this.filename = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de archivo
        /// </summary>
        [DataNames("_bncaux_tp")]
        [SugarColumn(ColumnName = "_bncaux_tp", ColumnDescription = "descripcion del movimiento", IsNullable = true, Length = 32)]
        public string Tipo {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer URL del archivo 
        /// </summary>
        [DataNames("_bncaux_url")]
        [SugarColumn(ColumnName = "_bncaux_url", ColumnDescription = "url de descarga para el archivo", IsNullable = true, ColumnDataType = "Text")]
        public string URL {
            get {
                return this.url;
            }
            set {
                this.url = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_bncaux_fn")]
        [SugarColumn(ColumnName = "_bncaux_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("_bncaux_usr_n")]
        [SugarColumn(ColumnName = "_bncaux_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
