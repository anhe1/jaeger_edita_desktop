﻿using System;
using System.Collections.Generic;
using Jaeger.Beta.Domain.Banco.Entities;
using Jaeger.Beta.Domain.Contable.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Beta.Domain.Banco.Contracts {
    public interface ISqlContableRepository : IGenericRepository<PrePolizaModel> {
        /// <summary>
        /// obtener listado de prepoilizas
        /// </summary>
        /// <param name="tipo">tipo de prepoliza</param>
        /// <param name="month">mes</param>
        /// <param name="year">año</param>
        /// <param name="cuenta">numero de cuenta</param>
        /// <returns></returns>
        IEnumerable<PrePolizaDetailModel> GetList(PrePolizaModel.PolizaTipoEnum tipo, int month, int year, string cuenta);

        IEnumerable<BancoCuentaModel> GetCuentasDeBanco();
    }

    public interface ISqlMovmientoComprobanteRepository : IGenericRepository<MovimientoBancarioComprobanteModel> {
        MovimientoBancarioComprobanteDetailModel Save(MovimientoBancarioComprobanteDetailModel model);
        IEnumerable<MovimientoBancarioComprobanteDetailModel> Save(List<MovimientoBancarioComprobanteDetailModel> models);
        void Create();
    }

    public interface ISqlMovimientoBancarioRepository : IGenericRepository<MovimientoBancarioModel> {
        IEnumerable<MovimientoBancarioDetailModel> GetList(int idCuenta, int month, int year, int idTipo);

        IEnumerable<MovimientoBancarioDetailModel> GetList(int idCuenta, int idTipo, DateTime startDate, DateTime? endDate, bool onlyActive);

        MovimientoBancarioDetailModel Save(MovimientoBancarioDetailModel model);

        bool Json(MovimientoBancarioDetailModel model);

        bool Aplicar(MovimientoBancarioDetailModel model);

        IEnumerable<MovimientoBancarioComprobanteDetailModel> GetComprobantes(string rfc, string status, Jaeger.Domain.Base.ValueObjects.CFDISubTipoEnum SubTipoComprobante);

        bool CreateTable();

        /// <summary>
        /// crear tablas de movimientos y comprobantes
        /// </summary>
        bool CreateTables();
    }

    public interface ISqlMovimientoAuxiliarRepository : IGenericRepository<MovimientoBancarioAuxiliarModel> {
        MovimientoBancarioAuxiliarDetailModel Save(MovimientoBancarioAuxiliarDetailModel item);

        /// <summary>
        /// crear tablas de movimientos y comprobantes
        /// </summary>
        bool CreateTables();
    }
}
