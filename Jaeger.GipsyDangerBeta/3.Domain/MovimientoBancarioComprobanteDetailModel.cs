﻿using System;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Beta.Domain.Banco.Entities {
    [SugarTable("_bnccmp", "comprobantes relacionados a movmientos bancarios")]
    public class MovimientoBancarioComprobanteDetailModel : MovimientoBancarioComprobanteModel {
        public MovimientoBancarioComprobanteDetailModel() : base() {
            this.TipoDocumento = MovimientoBancarioTipoComprobanteEnum.CFDI;
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante fiscal, si es ingreso, egreso, etc
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public TipoCFDIEnum TipoComprobante {
            get {
                switch (this.TipoComprobanteText.ToLower()) {
                    case "ingreso":
                    case "i":
                        return TipoCFDIEnum.Ingreso;
                    case "egreso":
                    case "e":
                        return TipoCFDIEnum.Egreso;
                    case "traslado":
                    case "t":
                        return TipoCFDIEnum.Traslado;
                    case "pagos":
                    case "p":
                        return TipoCFDIEnum.Pagos;
                    case "nomina":
                    case "n":
                        return TipoCFDIEnum.Nomina;
                    default:
                        return TipoCFDIEnum.Ingreso;
                }
            }
            set {
                this.TipoComprobanteText = Enum.GetName(typeof(TipoCFDIEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante fiscal, emitido, recibido, nomina, etc
        /// </summary>           
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipoComprobante {
            get {
                if (this.IdSubTipoComprobante == 1)
                    return CFDISubTipoEnum.Emitido;
                else if (this.IdSubTipoComprobante == 2)
                    return CFDISubTipoEnum.Recibido;
                else if (this.IdSubTipoComprobante == 3)
                    return CFDISubTipoEnum.Nomina;
                else
                    return CFDISubTipoEnum.None;
            }
            set {
                this.IdSubTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc
        /// </summary>           
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioTipoComprobanteEnum TipoDocumento {
            get {
                return (MovimientoBancarioTipoComprobanteEnum)Enum.Parse(typeof(MovimientoBancarioTipoComprobanteEnum), this.TipoDocumentoText);
            }
            set {
                this.TipoDocumentoText = Enum.GetName(typeof(MovimientoBancarioTipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }
    }
}
