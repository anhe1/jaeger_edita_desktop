﻿using System;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Beta.Domain.Banco.Entities {
    [SugarTable("_bncmov", "movimientos bancarios")]
    public class MovimientoBancarioModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int statusint;
        private int idCuentaBancaria;
        private string concepto;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string beneficiario;
        private string claveMoneda;
        private string numDocto;
        private string referencia;
        private string numAutorizacion;
        private string notas;
        private string numOperacion;
        private string referenciaNumerica;
        private string autoriza;
        private string cancela;
        private string formaPago;
        private string claveFormaPago;
        private decimal tipoCambio;
        private decimal cargo;
        private decimal abono;
        private string beneficiarioRFC;
        private string numeroCuenta;
        private string cuentaCLABE;
        private string sucursal;
        private string claveBanco;
        private DateTime? fechaVence;
        private DateTime? fechaDocto;
        private DateTime fechaEmision;
        private DateTime? fechaPago;
        private DateTime? fechaBoveda;
        private DateTime? fechaCancela;
        private string numeroDeCuenta;
        private int idCuenta2;
        private string claveBancoP;
        private string identificador;
        private int idConcepto;
        private int tipoint;
        private int tipoOperacionInt;
        private string json;
        private string bancoT;
        private int idDirectorio;
        private bool porJustificar;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public MovimientoBancarioModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
            this.fechaEmision = DateTime.Now;
            this.tipoCambio = 1;
            this.claveMoneda = "MXN";
            this.porJustificar = false;
        }

        /// <summary>
        /// obtener o establecer el indice del movimiento
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_id")]
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "_bncmov_id", ColumnDescription = "indice del movimiento", IsNullable = false)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_a")]
        [SugarColumn(ColumnName = "_bncmov_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del estado del movimiento
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_idstts")]
        [SugarColumn(ColumnName = "_bncmov_idstts", ColumnDescription = "status del movimiento", DefaultValue = "1")]
        public int IdStatus {
            get {
                return this.statusint;
            }
            set {
                this.statusint = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de movimiento o efecto del movimiento 1 = ingreso o 2 = Egreso
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_idtp")]
        [SugarColumn(ColumnName = "_bncmov_idtp", ColumnDescription = "tipo de movimiento", DefaultValue = "1")]
        public int IdTipo {
            get {
                return this.tipoint;
            }
            set {
                this.tipoint = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de operacion
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_idop")]
        [SugarColumn(ColumnName = "_bncmov_idop", ColumnDescription = "tipo de operacion", DefaultValue = "1")]
        public int IdTipoOperacion {
            get {
                return this.tipoOperacionInt;
            }
            set {
                this.tipoOperacionInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del concepto de movimiento
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_idcncp")]
        [SugarColumn(ColumnName = "_bncmov_idcncp", ColumnDescription = "indice del concepto de movimiento")]
        public int IdConcepto {
            get {
                return this.idConcepto;
            }
            set {
                this.idConcepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_drctr_id")]
        [SugarColumn(ColumnName = "_bncmov_drctr_id", ColumnDescription = "indice del directorio", IsNullable = true)]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la cuenta de banco a utilizar
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_idcnta1")]
        [SugarColumn(ColumnName = "_bncmov_idcnta1", ColumnDescription = "indice de la cuenta de banco a utilizar")]
        public int IdCuentaP {
            get {
                return this.idCuentaBancaria;
            }
            set {
                this.idCuentaBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la cuenta del receptor o beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_idcnta2")]
        [SugarColumn(ColumnName = "_bncmov_idcnta2", ColumnDescription = "indice de la cuenta del banco receptor")]
        public int IdCuentaT {
            get {
                return this.idCuenta2;
            }
            set {
                this.idCuenta2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:bandera solo para indicar si el gasto es por justificar
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_por")]
        [SugarColumn(ColumnName = "_bncmov_por", ColumnDescription = "bandera solo para indicar si el gasto es por justificar", IsNullable = true, Length = 1)]
        public bool PorComprobar {
            get {
                return this.porJustificar;
            }
            set {
                this.porJustificar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        [JsonProperty("noIdent", Order = -1)]
        [DataNames("_cntbl3_noiden")]
        [SugarColumn(ColumnName = "_bncmov_noiden", ColumnDescription = "identificador de la prepoliza", IsNullable = true, Length = 11)]
        public string Identificador {
            get {
                return this.identificador;
            }
            set {
                this.identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_numctap")]
        [SugarColumn(ColumnName = "_bncmov_numctap", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumeroCuentaP {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_clvbp")]
        [SugarColumn(ColumnName = "_bncmov_clvbp", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string ClaveBancoP {
            get {
                return this.claveBancoP;
            }
            set {
                this.claveBancoP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fecems")]
        [SugarColumn(ColumnName = "_bncmov_fecems", ColumnDescription = "fecha de la prepoliza", IsNullable = true)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fecdoc")]
        [SugarColumn(ColumnName = "_bncmov_fecdoc", ColumnDescription = "fecha del documento", IsNullable = true)]
        public DateTime? FechaDocto {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaDocto >= firstGoodDate)
                    return this.fechaDocto;
                else
                    return null;
            }
            set {
                this.fechaDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento 
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fcvnc")]
        [SugarColumn(ColumnName = "_bncmov_fcvnc", ColumnDescription = "fecha de vencimiento", IsNullable = true)]
        public DateTime? FechaVence {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaVence >= firstGooDate)
                    return this.fechaVence;
                else
                    return null;
            }
            set {
                this.fechaVence = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cobro o pago
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fccbr")]
        [SugarColumn(ColumnName = "_bncmov_fccbr", ColumnDescription = "fecha de cobro o pago", IsNullable = true)]
        public DateTime? FechaAplicacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPago >= firstGoodDate)
                    return this.fechaPago;
                else
                    return null;
            }
            set {
                this.fechaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de liberación (transmición al banco, por default es null) 
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fclib")]
        [SugarColumn(ColumnName = "_bncmov_fclib", ColumnDescription = "fecha de liberación (transmición al banco)", IsNullable = true)]
        public DateTime? FechaBoveda {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaBoveda >= firstGooDate)
                    return this.fechaBoveda;
                return null;
            }
            set {
                this.fechaBoveda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion (default null)
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fccncl")]
        [SugarColumn(ColumnName = "_bncmov_fccncl", ColumnDescription = "fecha de cancelacion", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGooDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer nombre del beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_benf")]
        [SugarColumn(ColumnName = "_bncmov_benf", ColumnDescription = "nombre deel beneficiario", IsNullable = true, Length = 256)]
        public string BeneficiarioT {
            get {
                return this.beneficiario;
            }
            set {
                this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del receptor del documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_rfcr")]
        [SugarColumn(ColumnName = "_bncmov_rfcr", ColumnDescription = "registro federal de causante del receptor", IsNullable = true, Length = 15)]
        public string BeneficiarioRFCT {
            get {
                return this.beneficiarioRFC;
            }
            set {
                this.beneficiarioRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_clvbt")]
        [SugarColumn(ColumnName = "_bncmov_clvbt", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string ClaveBancoT {
            get {
                return this.claveBanco;
            }
            set {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_bncmov_bancot")]
        [SugarColumn(ColumnName = "_bncmov_bancot", ColumnDescription = "numero de sucursal", Length = 255, IsNullable = true)]
        public string BancoT {
            get {
                return this.bancoT;
            }
            set {
                this.bancoT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el numero de cuenta del beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_numctat")]
        [SugarColumn(ColumnName = "_bncmov_numctat", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumeroCuentaT {
            get {
                return this.numeroCuenta;
            }
            set {
                this.numeroCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE del beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_clabet")]
        [SugarColumn(ColumnName = "_bncmov_clabet", ColumnDescription = "Clave Bancaria Estandarizada (CLABE)", Length = 18, IsNullable = true)]
        public string CuentaCLABET {
            get {
                return this.cuentaCLABE;
            }
            set {
                this.cuentaCLABE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sucursal del banco receptor
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_scrslt")]
        [SugarColumn(ColumnName = "_bncmov_scrslt", ColumnDescription = "numero de sucursal", Length = 20, IsNullable = true)]
        public string SucursalT {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de forma de pago SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_clvfrm")]
        [SugarColumn(ColumnName = "_bncmov_clvfrm", ColumnDescription = "clave forma de pago", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la forma de pago o cobro
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_frmpg")]
        [SugarColumn(ColumnName = "_bncmov_frmpg", ColumnDescription = "descripcion de la forma de pago o cobro", IsNullable = true, Length = 64)]
        public string FormaPagoDescripcion {
            get {
                return this.formaPago;
            }
            set {
                this.formaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero del documento
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_nodocto")]
        [SugarColumn(ColumnName = "_bncmov_nodocto", ColumnDescription = "numero de documento", IsNullable = true, Length = 20)]
        public string NumDocto {
            get {
                return this.numDocto;
            }
            set {
                this.numDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CVE_CONCEP
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_desc")]
        [SugarColumn(ColumnName = "_bncmov_desc", ColumnDescription = "descripcion del movimiento", IsNullable = false, Length = 256)]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia alfanumerica
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_ref")]
        [SugarColumn(ColumnName = "_bncmov_ref", ColumnDescription = "referencia alfanumerica", IsNullable = true, Length = 24)]
        public string Referencia {
            get {
                return this.referencia;
            }
            set {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de autorizacion bancaria
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_numauto")]
        [SugarColumn(ColumnName = "_bncmov_numauto", ColumnDescription = "numero de autorizacion", IsNullable = true, Length = 24)]
        public string NumAutorizacion {
            get {
                return this.numAutorizacion;
            }
            set {
                this.numAutorizacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de operacion (json)
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_numope")]
        [SugarColumn(ColumnName = "_bncmov_numope", ColumnDescription = "numero de operacion", Length = 20, IsNullable = true)]
        public string NumOperacion {
            get {
                return this.numOperacion;
            }
            set {
                this.numOperacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia numerica, este dato pertenece al json
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_refnum")]
        [SugarColumn(ColumnName = "_bncmov_refnum", ColumnDescription = "referencia numerica", Length = 20, IsNullable = true)]
        public string ReferenciaNumerica {
            get {
                return this.referenciaNumerica;
            }
            set {
                this.referenciaNumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio utilizado
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_tpcmb")]
        [SugarColumn(ColumnName = "_bncmov_tpcmb", ColumnDescription = "tipo de cambio", Length = 4, DecimalDigits = 2)]
        public decimal TipoCambio {
            get {
                return this.tipoCambio;
            }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el cargo al documento (default 0)
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_cargo")]
        [SugarColumn(ColumnName = "_bncmov_cargo", ColumnDescription = "cargo / retiro", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el abono al documento (default 0)
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_abono")]
        [SugarColumn(ColumnName = "_bncmov_abono", ColumnDescription = "abono / deposito", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de moneda SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_clvmnd")]
        [SugarColumn(ColumnName = "_bncmov_clvmnd", ColumnDescription = "clave SAT de moneda", Length = 3, IsNullable = true)]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota para este documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_nota")]
        [SugarColumn(ColumnName = "_bncmov_nota", ColumnDescription = "observaciones", IsNullable = true, Length = 64)]
        public string Nota {
            get {
                return this.notas;
            }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario cancela el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_usr_c")]
        [SugarColumn(ColumnName = "_bncmov_usr_c", ColumnDescription = "clave del usuario que cancela el comprobante", IsNullable = true, Length = 10)]
        public string Cancela {
            get {
                return this.cancela;
            }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estblecer la clave del usuario que autoriza el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bncmov_usr_a")]
        [SugarColumn(ColumnName = "_bncmov_usr_a", ColumnDescription = "clave del usuario que cancela", IsNullable = true, Length = 10)]
        public string Autoriza {
            get {
                return this.autoriza;
            }
            set {
                this.autoriza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_fn")]
        [SugarColumn(ColumnName = "_bncmov_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_fm")]
        [SugarColumn(ColumnName = "_bncmov_fm", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_usr_n")]
        [SugarColumn(ColumnName = "_bncmov_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_usr_m")]
        [SugarColumn(ColumnName = "_bncmov_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_bncmov_json")]
        [SugarColumn(ColumnName = "_bncmov_json", ColumnDescription = "json", ColumnDataType = "Text", IsNullable = true)]
        public string InfoAuxiliar {
            get {
                return this.json;
            }
            set {
                this.json = value;
                this.OnPropertyChanged();
            }
        }
    }
}
