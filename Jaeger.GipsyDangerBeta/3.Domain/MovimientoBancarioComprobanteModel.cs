﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Newtonsoft.Json;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Beta.Domain.Banco.Entities {
    [SugarTable("_bnccmp", "comprobantes relacionados a movmientos bancarios")]
    public class MovimientoBancarioComprobanteModel : BasePropertyChangeImplementation, IMovimientoBancarioComprobanteModel {
        #region declaraciones
        private int index;
        private bool activo;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string version;
        private string folio;
        private string serie;
        private string emisorRFC;
        private string emisor;
        private string receptor;
        private string idDocumento;
        private string claveMetodoPago;
        private string claveFormaPago;
        private DateTime fecha;
        private int idComprobante;
        private decimal monto;
        private string estado;
        private int numParcialidad;
        private string claveMoneda;
        private decimal cargo;
        private decimal abono;
        private decimal acumulado;
        private string receptorRFC;
        private string identificador;
        private string tipoDocumento;
        private int idmovimiento;
        private string tipoComprobanteText;
        private int subTipoComprobante;
        private string status;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public MovimientoBancarioComprobanteModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        #region propiedades

        /// <summary>
        /// obtener o establecer el indice principal de la tabla
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bnccmp_id")]
        [SugarColumn(ColumnName = "_bnccmp_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [JsonIgnore]
        [DataNames("_bnccmp_a")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_bnccmp_a", ColumnDescription = "registro activo", DefaultValue = "1", Length = 1, IsNullable = true)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el movimiento bancario
        /// </summary>
        [JsonIgnore]
        [DataNames("_bnccmp_subid")]
        [SugarColumn(ColumnName = "_bnccmp_subid", ColumnDescription = "indice de relacion con el movimiento bancario", IsNullable = true)]
        public int IdMovimiento {
            get {
                return this.idmovimiento;
            }
            set {
                this.idmovimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        [JsonProperty("noIdent", Order = -1)]
        [DataNames("_bncmov_noiden")]
        [SugarColumn(ColumnName = "_bncmov_noiden", ColumnDescription = "identificador de la prepoliza", IsNullable = true, Length = 11)]
        public string Identificador {
            get {
                return this.identificador;
            }
            set {
                this.identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [JsonProperty("ver", Order = 1)]
        [DataNames("_bnccmp_ver", "_cfdi_ver")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "_bnccmp_ver", ColumnDescription = "registro activo", DefaultValue = "1", Length = 1, IsNullable = true)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (efecto: ingreso, egreso, traslado, nomina, pagos)
        /// </summary>
        [JsonProperty("tipoComprobante", Order = 8)]
        [DataNames("_bnccmp_efecto", "_cfdi_efecto")]
        [SugarColumn(ColumnName = "_bnccmp_efecto", ColumnDescription = "efecto del comprobante", Length = 10, IsIgnore = false)]
        public string TipoComprobanteText {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        [DataNames("_cfdi_status")]
        [SugarColumn(IsIgnore = true, ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get {
                return this.status;
            }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        [JsonProperty("subTipoComprobante", Order = 9)]
        [DataNames("_bnccmp_sbtp", "_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_bnccmp_sbtp", ColumnDescription = "subtipo del comprobante, emitido, recibido, nomina, etc")]
        public int IdSubTipoComprobante {
            get {
                return  this.subTipoComprobante;
            }
            set {
                this.subTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc en modo texto
        /// </summary>     
        [JsonProperty("tipo", Order = 6)]
        [DataNames("_bnccmp_tipo")]
        [SugarColumn(ColumnName = "_bnccmp_tipo", ColumnDescription = "tipo de comprobante", Length = 10, IsNullable = true)]
        public string TipoDocumentoText {
            get {
                return this.tipoDocumento;
            }
            set {
                this.tipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla del comprobante
        /// </summary>
        [JsonProperty("idcom", Order = 7)]
        [DataNames("_bnccmp_idcom", "_cfdi_id")]
        [SugarColumn(ColumnName = "_bnccmp_idcom", ColumnDescription = "indice del comprobante fiscal")]
        public int IdComprobante {
            get {
                return this.idComprobante;
            }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el folio del comprobante
        /// </summary>
        [JsonProperty("folio", Order = 8)]
        [DataNames("_bnccmp_folio", "_cfdi_folio")]
        [SugarColumn(ColumnName = "_bnccmp_folio", ColumnDescription = "folio del comprobante", Length = 22)]
        public string Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie del comprobante
        /// </summary>
        [JsonProperty("serie", Order = 9)]
        [DataNames("_bnccmp_serie", "_cfdi_serie")]
        [SugarColumn(ColumnName = "_bnccmp_serie", ColumnDescription = "serie del comprobante", Length = 25)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del emisor del comprobante
        /// </summary>
        [JsonProperty("rfce", Order = 10)]
        [DataNames("_bnccmp_rfce", "_cfdi_rfce")]
        [SugarColumn(ColumnName = "_bnccmp_rfce", ColumnDescription = "rfc emisor del comprobante", Length = 15)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del emisor del comprobante
        /// </summary>
        [JsonProperty("emisor", Order = 11)]
        [DataNames("_bnccmp_emisor", "_cfdi_nome")]
        [SugarColumn(ColumnName = "_bnccmp_emisor", ColumnDescription = "nombre del emisor del comprobante", Length = 255)]
        public string EmisorNombre {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del receptor del comprobante
        /// </summary>
        [JsonProperty("rfcr", Order = 12)]
        [DataNames("_bnccmp_rfcr", "_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_bnccmp_rfcr", ColumnDescription = "rfc del receptor del comprobante", Length = 15)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [JsonProperty("receptor", Order = 13)]
        [DataNames("_bnccmp_receptor", "_cfdi_nomr")]
        [SugarColumn(ColumnName = "_bnccmp_receptor", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get {
                return this.receptor;
            }
            set {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emision", Order = 14)]
        [DataNames("_bnccmp_fecems", "_cfdi_fecems")]
        [SugarColumn(ColumnName = "_bnccmp_fecems", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fecha;
            }
            set {
                this.fecha = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("mtdpg", Order = 15)]
        [DataNames("_bnccmp_mtdpg", "_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_bnccmp_mtdpg", ColumnDescription = "clave del metodo de pago", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPago;
            }
            set {
                if (value != null) {
                    if (value.Length > 3) {
                        this.claveMetodoPago = value.Substring(0, 2);
                    } else {
                        this.claveMetodoPago = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("frmpg", Order = 16)]
        [DataNames("_bnccmp_frmpg", "_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_bnccmp_frmpg", ColumnDescription = "clave de la forma de pago", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                if (value != null) {
                    if (value.Length > 2) {
                        this.claveFormaPago = value.Substring(0,1);
                    } else {
                        this.claveFormaPago = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("moneda", Order = 17)]
        [DataNames("_bnccmp_moneda", "_cfdi_moneda")]
        [SugarColumn(ColumnName = "_bnccmp_moneda", ColumnDescription = "clave de moneda", Length = 3, IsNullable = true)]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("uuid", Order = 18)]
        [DataNames("_bnccmp_uuid", "_cfdi_uuid")]
        [SugarColumn(ColumnName = "_bnccmp_uuid", ColumnDescription = "folio fiscal (uuid)", Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("par", Order = 19)]
        [DataNames("_bnccmp_par", "_cfdi_par")]
        [SugarColumn(ColumnName = "_bnccmp_par", ColumnDescription = "numero de partida")]
        public int NumParcialidad {
            get {
                return this.numParcialidad;
            }
            set {

                this.numParcialidad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("total", Order = 20)]
        [DataNames("_bnccmp_total", "_cfdi_total")]
        [SugarColumn(ColumnName = "_bnccmp_total", ColumnDescription = "total del comprobante", Length = 11, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.monto;
            }
            set {
                this.monto = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("cargo", Order = 21)]
        [DataNames("_bnccmp_cargo")]
        [SugarColumn(ColumnName = "_bnccmp_cargo", ColumnDescription = "cargo", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("abono", Order = 22)]
        [DataNames("_bnccmp_abono")]
        [SugarColumn(ColumnName = "_bnccmp_abono", ColumnDescription = "abono", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("acumulado", Order = 23)]
        [DataNames("_bnccmp_cbrd", "_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_bnccmp_acumulado", ColumnDescription = "acumulado del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_bnccmp_estado", "_cfdi_estado")]
        [SugarColumn(ColumnName = "_bnccmp_estado", ColumnDescription = "ultimo estado del comprobante", Length = 10, IsNullable = true)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_usr_n")]
        [SugarColumn(ColumnName = "_bnccmp_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bnccmp_fn")]
        [SugarColumn(ColumnName = "_bnccmp_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_fm")]
        [SugarColumn(ColumnName = "_bnccmp_fm", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("_bncmov_usr_m")]
        [SugarColumn(ColumnName = "_bnccmp_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public object Tag { get; set; }
    }
}
