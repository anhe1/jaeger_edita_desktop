﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// repositorio de ordenes de cliente (orden de compra)
    /// </summary>
    public class SqlSugarOrdenClienteRepository : SqlSugarContext<OrdenClienteModel>, ISqlOrdenClienteRepository {
        public SqlSugarOrdenClienteRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// almacenar orden de cliente
        /// </summary>
        public OrdenClienteDetailModel Save(OrdenClienteDetailModel model) {
            var usuario = "";
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Id = this.Insert(model);
                usuario = model.Creo;
            }
            else {
                model.FechaModifica = DateTime.Now;
                usuario = model.Modifica;
                this.Update(model);
            }

            if (model.Conceptos != null) {
                if (model.Conceptos.Count > 0) {
                    for (int i = 0; i < model.Conceptos.Count; i++) {
                        if (model.Conceptos[i].IdConcepto == 0) {
                            model.Conceptos[i].IdOrdenCliente = model.Id;
                            model.Conceptos[i].Creo = usuario;
                            model.Conceptos[i].FechaNuevo = DateTime.Now;
                            model.Conceptos[i].IdConcepto = this.Db.Insertable<OrdenClienteConceptoModel>(model.Conceptos[i]).ExecuteReturnIdentity();
                        }
                        else {
                            this.Db.Updateable<OrdenClienteConceptoModel>(model.Conceptos[i]).ExecuteCommand();
                        }
                    }
                }
            }

            if (model.Documentos != null) {
                if (model.Documentos.Count > 0) {
                    for (int i = 0; i < model.Documentos.Count; i++) {
                        if (model.Documentos[i].Id == 0) {
                            model.Documentos[i].IdOrdenCliente = model.Id;
                            model.Documentos[i].IsActive = true;
                            model.Documentos[i].Creo = usuario;
                            model.Documentos[i].FechaNuevo = DateTime.Now;
                            model.Documentos[i].Id = this.Db.Insertable<OrdenClienteDoctoModel>(model.Documentos[i]).ExecuteReturnIdentity();
                        }
                    }
                }
            }

            return model;
        }

        public OrdenClienteDetailModel GetOrden(int index) {
            //Manual mode
            var result = this.Db.Queryable<OrdenClienteDetailModel>().Mapper((itemModel, cache) => {
                var allItems = cache.Get(orderList => {
                    var allIds = orderList.Select(it => it.Id).ToList();
                    return this.Db.Queryable<OrdenClienteConceptoDetailModel>().Where(it => allIds.Contains(it.IdOrdenCliente)).OrderBy(it => it.Secuencia).ToList();//Execute only once
                });

                var otrositems = cache.Get(ord => {
                    var allids = ord.Select(it => it.Id).ToList();
                    return this.Db.Queryable<OrdenClienteDoctoModel>().Where(it => allids.Contains(it.IdOrdenCliente)).ToList();
                });
                itemModel.Conceptos = new BindingList<OrdenClienteConceptoDetailModel>(allItems.Where(it => it.IdOrdenCliente == itemModel.Id).ToList());
                itemModel.Documentos = new BindingList<OrdenClienteDoctoModel>(otrositems.Where(it => it.IdOrdenCliente == itemModel.Id).ToList());
            }).Where(it => it.Id == index).Single();
            return result;
        }

        

        /// <summary>
        /// 
        /// </summary>
        public bool Create() {
            try {
                this.Db.CodeFirst.InitTables<OrdenClienteModel, OrdenClienteConceptoModel, OrdenClienteDoctoModel, OrdenClienteOrdenProduccionModel>();
                return true;
            }
            catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
