﻿using System;
using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Ventas.Repositories {
    public class SqlSugarComisionDescuentoRepository : MySqlSugarContext<ComisionDescuentoModel>, ISqlComisionDescuentoRepository {
        public SqlSugarComisionDescuentoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        //public override bool Delete(int index) {
        //    var sqlCommand = @"DELETE FROM CMSND WHERE CMSND_ID = @CMSND_ID;";
        //    sqlCommand.Parameters.AddWithValue("@CMSND_ID", index);
        //    return this.ExecuteTransaction(sqlCommand) > 0;
        //}

        //public ComisionDescuentoModel GetById(int index) {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"SELECT * FROM CMSND WHERE CMSND_ID = @CMSND_ID"
        //    };
        //    sqlCommand.Parameters.AddWithValue("@CMSND_ID", index);
        //    return this.GetMapper(sqlCommand).FirstOrDefault();
        //}

        //public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"SELECT * FROM CMSND @condiciones ORDER BY CMSND_RNG1 ASC, CMSND_RNG2 ASC"
        //    };

        //    if (conditionals == null) {
        //        conditionals = new List<Conditional>();
        //    }

        //    if (conditionals.Count > 0) {
        //        var d = FireBird.Services.ExpressionTool.ConditionalModelToSql(conditionals);
        //        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "WHERE" + d.Key);
        //        sqlCommand.Parameters.AddRange(d.Value);
        //    } else {
        //        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
        //    }
        //    return this.GetMapper<ComisionDescuentoModel>(sqlCommand).ToList();
        //}

        //public IEnumerable<ComisionDescuentoModel> GetList() {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"SELECT * FROM CMSND ORDER BY CMSND_RNG1 ASC, CMSND_RNG2 ASC"
        //    };
        //    return this.GetMapper(sqlCommand);
        //}

        //public int Insert(ComisionDescuentoModel item) {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"INSERT INTO CMSND ( CMSND_ID, CMSND_A, CMSND_CMSNT_ID, CMSND_RNG1, CMSND_RNG2, CMSND_FACTOR, CMSND_NOM, CMSND_FN, CMSND_USU_N) 
        //                                   VALUES (@CMSND_ID,@CMSND_A,@CMSND_CMSNT_ID,@CMSND_RNG1,@CMSND_RNG2,@CMSND_FACTOR,@CMSND_NOM,@CMSND_FN,@CMSND_USU_N) RETURNING CMSND_ID"
        //    };
        //    sqlCommand.Parameters.AddWithValue("@CMSND_ID", item.IdDescuento);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_A", item.Activo);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_CMSNT_ID", item.IdComision);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_RNG1", item.Inicio);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_RNG2", item.Final);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_FACTOR", item.Factor);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_NOM", item.Descripcion);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_FN", item.FechaNuevo);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_USU_N", item.Creo);

        //    throw new NotImplementedException();
        //}

        public ComisionDescuentoModel Save(ComisionDescuentoModel model) {
            if (model.IdDescuento == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdDescuento = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }

        //public int Update(ComisionDescuentoModel item) {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"UPDATE CMSND SET CMSND_A = @CMSND_A, CMSND_CMSNT_ID = @CMSND_CMSNT_ID, CMSND_RNG1 = @CMSND_RNG1, CMSND_RNG2 = @CMSND_RNG2, CMSND_FACTOR = @CMSND_FACTOR, CMSND_NOM = @CMSND_NOM, CMSND_FM = @CMSND_FM, CMSND_USU_M = @CMSND_USU_M WHERE CMSND_ID = @CMSND_ID;"
        //    };
        //    sqlCommand.Parameters.AddWithValue("@CMSND_ID", item.IdDescuento);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_A", item.Activo);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_CMSNT_ID", item.IdComision);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_RNG1", item.Inicio);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_RNG2", item.Final);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_FACTOR", item.Factor);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_NOM", item.Descripcion);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_FN", item.FechaNuevo);
        //    sqlCommand.Parameters.AddWithValue("@CMSND_USU_N", item.Creo);
        //    throw new NotImplementedException();
        //}
    }
}
