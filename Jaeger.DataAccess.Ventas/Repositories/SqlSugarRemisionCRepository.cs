﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Ventas.Repositories {
    /// <summary>
    /// comisiones a vendedor
    /// </summary>
    public class SqlSugarRemisionCRepository : MySqlSugarContext<RemisionComisionModel>, ISqlRemisionCRepository {
        public SqlSugarRemisionCRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public override int Insert(RemisionComisionModel item) {
            var sqlCommand = @"INSERT INTO RMSNC (RMSNC_RMSN_ID, RMSNC_A, RMSNC_CTCMS_ID, RMSNC_DRCTR_ID, RMSNC_VNDR_ID, RMSNC_CTDIS_ID, RMSNC_DESC, RMSNC_FCPAC, RMSNC_FCACT, RMSNC_IMPR, RMSNC_ACUML, RMSNC_VNDR, RMSNC_FN, RMSNC_USR_N) VALUES (@RMSNC_RMSN_ID, @RMSNC_A, @RMSNC_CTCMS_ID, @RMSNC_DRCTR_ID, @RMSNC_VNDR_ID, @RMSNC_CTDIS_ID, @RMSNC_DESC, @RMSNC_FCPAC, @RMSNC_FCACT, @RMSNC_IMPR, @RMSNC_ACUML, @RMSNC_VNDR, @RMSNC_FN, @RMSNC_USR_N) RETURNING RMSNC_ID";
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@RMSNC_RMSN_ID", item.IdRemision),
                new SugarParameter("@RMSNC_A", item.Activo),
                new SugarParameter("@RMSNC_CTCMS_ID", item.IdComision),
                new SugarParameter("@RMSNC_DRCTR_ID", item.IdCliente),
                new SugarParameter("@RMSNC_VNDR_ID", item.IdVendedor),
                new SugarParameter("@RMSNC_CTDIS_ID", item.IsDisponible),
                new SugarParameter("@RMSNC_DESC", item.Comision),
                new SugarParameter("@RMSNC_FCPAC", item.ComisionFactor),
                new SugarParameter("@RMSNC_FCACT", item.ComisionFactorActualizado),
                new SugarParameter("@RMSNC_IMPR", item.ComisionPorPagar),
                new SugarParameter("@RMSNC_ACUML", item.ComisionPagada),
                new SugarParameter("@RMSNC_VNDR", item.Vendedor),
                new SugarParameter("@RMSNC_FN", item.FechaModifica),
                new SugarParameter("@RMSNC_USR_N", item.Modifica),
            };
            item.IdRemision = this.ExecuteScalar(sqlCommand, parameters);
            if (item.IdRemision > 0)
                return item.IdRemision;
            return 0;
        }

        public override int Update(RemisionComisionModel item) {
            var sqlCommand = @"UPDATE RMSNC SET RMSNC_RMSN_ID = @RMSNC_RMSN_ID, RMSNC_A = @RMSNC_A, RMSNC_CTCMS_ID = @RMSNC_CTCMS_ID, RMSNC_DRCTR_ID = @RMSNC_DRCTR_ID, RMSNC_VNDR_ID = @RMSNC_VNDR_ID, RMSNC_CTDIS_ID = @RMSNC_CTDIS_ID, RMSNC_DESC = @RMSNC_DESC, RMSNC_FCPAC = @RMSNC_FCPAC, RMSNC_FCACT = @RMSNC_FCACT, RMSNC_IMPR = @RMSNC_IMPR, RMSNC_ACUML = @RMSNC_ACUML, RMSNC_VNDR = @RMSNC_VNDR, RMSNC_FN = @RMSNC_FN, RMSNC_USR_N = @RMSNC_USR_N WHERE (RMSNC_ID = @RMSNC_ID)";
            var parameters = new List<SugarParameter>() {
                new SugarParameter("@RMSNC_RMSN_ID", item.IdRemision),
                new SugarParameter("@RMSNC_A", item.Activo),
                new SugarParameter("@RMSNC_CTCMS_ID", item.IdComision),
                new SugarParameter("@RMSNC_DRCTR_ID", item.IdCliente),
                new SugarParameter("@RMSNC_VNDR_ID", item.IdVendedor),
                new SugarParameter("@RMSNC_CTDIS_ID", item.IsDisponible),
                new SugarParameter("@RMSNC_DESC", item.Comision),
                new SugarParameter("@RMSNC_FCPAC", item.ComisionFactor),
                new SugarParameter("@RMSNC_FCACT", item.ComisionFactorActualizado),
                new SugarParameter("@RMSNC_IMPR", item.ComisionPorPagar),
                new SugarParameter("@RMSNC_ACUML", item.ComisionPagada),
                new SugarParameter("@RMSNC_VNDR", item.Vendedor),
                new SugarParameter("@RMSNC_FN", item.FechaModifica),
                new SugarParameter("@RMSNC_USR_N", item.Modifica),
            };
            return this.ExecuteTransaction(sqlCommand, parameters);
        }
        #endregion

        public override IEnumerable<T1> GetList<T1>(List<IConditional> condidional) {
            var sqlCommand = @"SELECT RMSN.*, RMSNC.* FROM RMSN LEFT JOIN RMSNC ON RMSNC.RMSNC_RMSN_ID = RMSN.RMSN_ID @wcondiciones ORDER BY RMSN_FOLIO DESC";

            return this.GetMapper<T1>(sqlCommand, condidional);
        }

        public IEnumerable<T1> GetToList<T1>(List<IConditional> condidional) where T1 : class, new() {
            var concepto = condidional.Where(it => it.FieldName.ToUpper() == "BNCMOV_CNCP_ID").FirstOrDefault();
            var lista = condidional.Where(it => it.FieldName.ToUpper() != "BNCMOV_CNCP_ID").ToList();
            var sqlCommand = @"SELECT RMSNC.*, RMSN.*
                                FROM RMSNC
                                LEFT JOIN RMSN ON RMSN.RMSN_ID = RMSNC.RMSNC_RMSN_ID
                                LEFT JOIN BNCCMP ON BNCCMP.BNCCMP_IDCOM = RMSNC.RMSNC_RMSN_ID 
                                LEFT JOIN BNCMOV ON BNCMOV.BNCMOV_ID = BNCCMP.BNCCMP_SUBID AND BNCMOV.BNCMOV_CNCP_ID IN (@CONCEPTO)
                                WHERE BNCCMP.BNCCMP_IDCOM IS NULL @condiciones
                                ORDER BY RMSNC.RMSNC_RMSN_ID DESC";
            var parameters = new List<SugarParameter>() { new SugarParameter("@CONCEPTO", concepto.FieldValue) };
            return this.GetMapper<T1>(sqlCommand, parameters);
        }

        public int Saveable(RemisionComisionModel item) {
            var sqlCommand = @"UPDATE OR INSERT INTO RMSNC ( RMSNC_RMSN_ID, RMSNC_A, RMSNC_CTCMS_ID, RMSNC_DRCTR_ID, RMSNC_VNDR_ID, RMSNC_CTDIS_ID, RMSNC_DESC, RMSNC_FCPAC, RMSNC_FCACT, RMSNC_IMPR, RMSNC_ACUML, RMSNC_VNDR, RMSNC_FN, RMSNC_USR_N)
                                                     VALUES (@RMSNC_RMSN_ID,@RMSNC_A,@RMSNC_CTCMS_ID,@RMSNC_DRCTR_ID,@RMSNC_VNDR_ID,@RMSNC_CTDIS_ID,@RMSNC_DESC,@RMSNC_FCPAC,@RMSNC_FCACT,@RMSNC_IMPR,@RMSNC_ACUML,@RMSNC_VNDR,@RMSNC_FN,@RMSNC_USR_N) MATCHING (RMSNC_RMSN_ID);";
            item.Modifica = User;
            item.FechaModifica = DateTime.Now;
            var parameters = new List<SugarParameter> {
                new SugarParameter("@RMSNC_RMSN_ID", item.IdRemision),
                new SugarParameter("@RMSNC_A", item.Activo),
                new SugarParameter("@RMSNC_CTCMS_ID", item.IdComision),
                new SugarParameter("@RMSNC_DRCTR_ID", item.IdCliente),
                new SugarParameter("@RMSNC_VNDR_ID", item.IdVendedor),
                new SugarParameter("@RMSNC_CTDIS_ID", item.IsDisponible),
                new SugarParameter("@RMSNC_DESC", item.Comision),
                new SugarParameter("@RMSNC_FCPAC", item.ComisionFactor),
                new SugarParameter("@RMSNC_FCACT", item.ComisionFactorActualizado),
                new SugarParameter("@RMSNC_IMPR", item.ComisionPorPagar),
                new SugarParameter("@RMSNC_ACUML", item.ComisionPagada),
                new SugarParameter("@RMSNC_VNDR", item.Vendedor),
                new SugarParameter("@RMSNC_FN", item.FechaModifica),
                new SugarParameter("@RMSNC_USR_N", item.Modifica),
            };
            return ExecuteTransaction(sqlCommand, parameters);
        }

        /// <summary>
        /// actualizar bandera de comisiones
        /// </summary>
        /// <param name="idRemision"></param>
        /// <param name="value">0 <-no esta agregada a ningun recibo, 1 <- comsion autorizada y esta agregada a un recibo, 2 <-comision pagada y ya esta autorizada en un recibo</param>
        public bool Update(int[] idRemision, int value) {
            // ctlrms_catcms_id = 0 < --no esta agregada a ningun recibo
            // ctlrms_catcms_id = 1 < --la comsion autorizada y esta agregada a un recibo
            // ctlrms_catcms_id = 2 < --comision pagada y ya esta autorizada en un recibo
            var sqlCommand = "UPDATE RMSNC SET RMSNC_CTDIS_ID=@RMSNC_CTDIS_ID WHERE RMSNC_RMSN_ID IN (@INDEXS)";

            if (idRemision.Length > 0) {
                var parameters = new List<SugarParameter> { new SugarParameter("@RMSNC_CTDIS_ID", value) };
                sqlCommand = sqlCommand.Replace("@INDEXS", string.Join(",", idRemision));

                return this.ExecuteTransaction(sqlCommand, parameters) > 0;
            }
            return false;
        }

        public RemisionComisionModel Save(RemisionComisionModel item) {
            item.FechaModifica = DateTime.Now;
            item.Modifica = User;
            if (item.IdRemision == 0) {
                item.IdRemision = Insert(item);
            } else {
                Update(item);
            }
            return item;
        }
    }
}
