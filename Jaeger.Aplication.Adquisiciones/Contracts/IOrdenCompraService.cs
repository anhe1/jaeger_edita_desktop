﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.Aplication.Adquisiciones.Contracts {
    /// <summary>
    /// ordenes de compra
    /// </summary>
    public interface IOrdenCompraService {

        OrdenCompraConfig Configuracion {
            get; set;
        }

        OrdenCompraDetailModel GetNew();

        BindingList<OrdenCompraSingleModel> GetList(int year, int month);

        OrdenCompraDetailModel GetComprobante(int id);

        OrdenCompraDetailModel Save(OrdenCompraDetailModel item);

        OrdenCompraSingleModel Save(OrdenCompraSingleModel item);

        OrdenCompraConfig Save(OrdenCompraConfig item);

        //List<OrdenCompraStatusModel> GetStatus();

        /// <summary>
        /// listado de tipos de relacion de orden de compra
        /// </summary>
        //List<OrdenCompraRelacionTipo> GetTipoRelacion();

        List<OrdenCompraSingleModel> GetRelacionado(string rfc, int year);

        bool Cancelar(OrdenCompraModel item);

        void CrearTablas();
    }
}
