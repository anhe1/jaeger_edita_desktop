﻿using System.ComponentModel;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.Aplication.Adquisiciones.Contracts {
    /// <summary>
    /// servicio de directorio
    /// </summary>
    public interface IContribuyenteService {
        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        BindingList<OrdenCompraProveedorDetailModel> GetContribuyentes();
    }
}
