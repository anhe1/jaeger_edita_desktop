﻿using System.ComponentModel;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.Aplication.Adquisiciones.Contracts {
    public interface ISolicitudCotizacionService {
        OrdenCompraConfig Configuracion {
            get; set;
        }

        SolicitudCotizacionDetailModel GetNew();

        SolicitudCotizacionDetailModel GetById(int index);

        SolicitudCotizacionDetailModel Save(SolicitudCotizacionDetailModel model);

        BindingList<SolicitudCotizacionSingleModel> GetList(int year, int month = 0);

        SolicitudCotizacionPrinter GetPrinter(int index);

        void CrearTablas();
    }
}
