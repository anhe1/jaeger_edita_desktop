﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.Empresa.Repositories;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Adquisiciones.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Aplication.Adquisiciones.Services {
    /// <summary>
    /// servicio de orden de compra
    /// </summary>
    public class OrdenCompraService : IOrdenCompraService {
        protected ISqlOrdenCompraRepository ordenCompraRepository;
        protected ISqlConfiguracionRepository configuracionRepository;

        public OrdenCompraService() {
            this.ordenCompraRepository = new SqlSugarOrdenCompraRepository(ConfigService.Synapsis.RDS.Edita);
            this.configuracionRepository = new SqlSugarConfiguracionRepository(ConfigService.Synapsis.RDS.Edita);

            // obtener la configuracion
            this.Configuracion = new OrdenCompraConfig();
            var d = this.configuracionRepository.GetByKey(ConfiguracionKeyEnum.ocompra11.ToString());
            if (d != null) {
                var d1 = OrdenCompraConfig.Json(d.Data);
                if (d1 != null) {
                    this.Configuracion = d1;
                }
            }
        }

        public OrdenCompraConfig Configuracion {
            get; set;
        }

        public OrdenCompraDetailModel GetNew() {
            return new OrdenCompraDetailModel();
        }

        /// <summary>
        /// obtener orden de compra
        /// </summary>
        public OrdenCompraDetailModel GetComprobante(int id) {
            return this.ordenCompraRepository.GetById(id);
        }

        /// <summary>
        /// almacenar orden de compra
        /// </summary>
        public OrdenCompraDetailModel Save(OrdenCompraDetailModel item) {
            if (item.Id == 0) {
                item.Creo = ConfigService.Piloto.Clave;
                item.FechaEmision = DateTime.Now;
                for (int i = 0; i < item.Conceptos.Count; i++) {
                    item.Conceptos[i].Creo = ConfigService.Piloto.Clave;
                    item.Conceptos[i].FechaNuevo = DateTime.Now;
                }
            }
            return this.ordenCompraRepository.Save(item);
        }

        public OrdenCompraSingleModel Save(OrdenCompraSingleModel item) {
            if (this.ordenCompraRepository.Update(item) == 0)
                Console.WriteLine("Error al autorizar");
            return item;
        }

        public OrdenCompraConfig Save(OrdenCompraConfig item) {
            var d1 = new Domain.Empresa.Entities.EmpresaConfiguracionModel() { Key = ConfiguracionKeyEnum.ocompra11.ToString(), Data = item.Json() };
            this.configuracionRepository.Save(d1);
            return item;
        }

        /// <summary>
        /// Listado de status para ordenes de compra
        /// </summary>
        public static List<OrdenCompraStatusModel> GetStatus() {
            return ((OrdenCompraStatusEnum[])Enum.GetValues(typeof(OrdenCompraStatusEnum))).Select(c => new OrdenCompraStatusModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// listado de tipos de relacion de orden de compra
        /// </summary>
        public static List<OrdenCompraRelacionTipo> GetTipoRelacion() {
            return new List<OrdenCompraRelacionTipo>() { new OrdenCompraRelacionTipo("01", "Orden de Compra cancelada"),
                new OrdenCompraRelacionTipo("02", "Cambio de Precios") };
        }

        public List<OrdenCompraSingleModel> GetRelacionado(string rfc, int year) {
            var _date = new DateTime(year, 1, 1);
            var _condiciones = new List<Conditional>() { new Conditional("_ordcmp_rfc", rfc, ConditionalTypeEnum.Equal), new Conditional("_ordcmp_fn", _date.ToShortDateString(), ConditionalTypeEnum.GreaterThanOrEqual) };
            return this.ordenCompraRepository.GetList(_condiciones).ToList();
        }

        /// <summary>
        /// cancelar orden de compra
        /// </summary>
        public bool Cancelar(OrdenCompraModel item) {
            item.FechaCancela = DateTime.Now;
            item.Cancela = ConfigService.Piloto.Clave;
            return this.ordenCompraRepository.Cancelar(item, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener listado de ordenes de compra por periodos
        /// </summary>
        public BindingList<OrdenCompraSingleModel> GetList(int year, int month) {
            return new BindingList<OrdenCompraSingleModel>(this.ordenCompraRepository.GetList(month, year).ToList());
        }

        /// <summary>
        /// crear sistema de tablas para ordenes de compra
        /// </summary>
        public void CrearTablas() {
            this.ordenCompraRepository.CrearTablas();
        }
    }
}
