﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.Empresa.Repositories;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.Domain.Adquisiciones.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Aplication.Adquisiciones.Services {
    public class SolicitudCotizacionService : ISolicitudCotizacionService {
        protected ISqlSolicitudCotizacionRepository solicitudRepository;
        protected ISqlSolicitudCotizacionConceptoRepository solicitudConceptoRepository;
        protected ISqlConfiguracionRepository configuracionRepository;

        public SolicitudCotizacionService() {
            this.solicitudRepository = new SqlSugarSolicitudCotizacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.solicitudConceptoRepository = new SqlSolicitudCotizacionConceptoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.configuracionRepository = new SqlSugarConfiguracionRepository(ConfigService.Synapsis.RDS.Edita);

            // obtener la configuracion
            this.Configuracion = new OrdenCompraConfig();
            var d = this.configuracionRepository.GetByKey(ConfiguracionKeyEnum.ocompra11.ToString());
            if (d != null) {
                var d1 = OrdenCompraConfig.Json(d.Data);
                if (d1 != null) {
                    this.Configuracion = d1;
                }
            }
        }

        public OrdenCompraConfig Configuracion {
            get; set;
        }

        public void CrearTablas() {
            this.solicitudRepository.Crear();
            this.solicitudConceptoRepository.CreateTable();
        }

        public BindingList<SolicitudCotizacionSingleModel> GetList(int year, int month = 0) {
            var condiciones = new List<IConditional> {
                new Conditional("extract(year from _ordcms_fn)", year.ToString())
            };

            if (month > 0) {
                condiciones.Add(new Conditional("extract(month from _ordcms_fn)", month.ToString()));
            }

            return new BindingList<SolicitudCotizacionSingleModel>(this.solicitudRepository.GetList<SolicitudCotizacionSingleModel>(condiciones).ToList());
        }

        public SolicitudCotizacionDetailModel GetById(int index) {
            var response = this.solicitudRepository.GetById(index);
            return response;
        }

        public SolicitudCotizacionDetailModel GetNew() {
            return new SolicitudCotizacionDetailModel() { FechaNuevo = DateTime.Now };
        }

        public SolicitudCotizacionDetailModel Save(SolicitudCotizacionDetailModel model) {
            model = this.solicitudRepository.Save(model);
            if (model.Conceptos != null) {
                for (int i = 0; i < model.Conceptos.Count; i++) {
                    model.Conceptos[i].IdSolicitud = model.IdSolicitud;
                    model.Conceptos[i] = this.solicitudConceptoRepository.Save(model.Conceptos[i]);
                }
            }
            return model;
        }

        public SolicitudCotizacionPrinter GetPrinter(int index) {
            var solicitud = (SolicitudCotizacionDetailModel)this.solicitudRepository.GetById(index);
            var printer = new SolicitudCotizacionPrinter(solicitud);
            return printer;
        }

        public static List<MetodoEnvioModel> GetMetodoEnvio() {
            return new List<MetodoEnvioModel> {
                new MetodoEnvioModel(0, "No aplica"),
                new MetodoEnvioModel(1, "Transporte de proveedor"),
                new MetodoEnvioModel(2, "Recoger en sucursal"),
                new MetodoEnvioModel(3, "Paquetería Interna"),
            };
        }

        public static List<CondicionesModel> GetCondiciones() {
            return new List<CondicionesModel> {
                new CondicionesModel(0, "No aplica"),
                new CondicionesModel(1, "Pago al contado"),
                new CondicionesModel(2, "Pago anticipado"),
                new CondicionesModel(3, "Pago aplazado"),
            };
        }
    }
}
