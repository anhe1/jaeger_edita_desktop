﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Adquisiciones.Contracts;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Adquisiciones.Contracts;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Adquisiciones.Entities;

namespace Jaeger.Aplication.Adquisiciones.Services {
    public class ContribuyenteService : IContribuyenteService {
        protected CFDISubTipoEnum subTipo;
        protected ISqlContribuyenteRepository comprobanteContribuyenteRepository;

        public ContribuyenteService(CFDISubTipoEnum subTipo) {
            this.subTipo = subTipo;
            this.comprobanteContribuyenteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita);
        }

        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        public BindingList<OrdenCompraProveedorDetailModel> GetContribuyentes() {
            if (this.subTipo == CFDISubTipoEnum.Emitido)
                return new BindingList<OrdenCompraProveedorDetailModel>(this.comprobanteContribuyenteRepository.GetList("Cliente", true).ToList());
            else if (this.subTipo == CFDISubTipoEnum.Recibido)
                return new BindingList<OrdenCompraProveedorDetailModel>(this.comprobanteContribuyenteRepository.GetList("Proveedor", true).ToList());
            return null;
        }
    }
}
