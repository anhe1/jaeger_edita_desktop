﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientosReportePrinter : BasePropertyChangeImplementation {
        private DateTime fechaInicial;
        private DateTime? fechaFinal;
        private List<MovimientoBancarioDetailModel> movimientos;

        public MovimientosReportePrinter() {
            this.fechaInicial = DateTime.Now;
            this.fechaFinal = null;
        }

        public DateTime FechaInicial {
            get {
                return this.fechaInicial;
            }
            set {
                this.fechaInicial = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime? FechaFinal {
            get {
                return this.fechaFinal;
            }
            set {
                this.fechaFinal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la informacion de los movimientos de la cuenta
        /// </summary>
        public System.Collections.Generic.List<MovimientoBancarioDetailModel> Movimientos {
            get {
                return this.movimientos;
            }
            set {
                this.movimientos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
