﻿using System;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// movimiento bancario
    /// </summary>
    [SugarTable("BNCMOV", "movimientos bancarios")]
    public class MovimientoBancarioModel : BasePropertyChangeImplementation, IMovimientoBancarioModel {
        #region declaraciones
        private int index;
        private bool activo;
        private string version;
        private int statusint;
        private int idCuentaBancaria;
        private string concepto;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string beneficiario;
        private string claveMoneda;
        private string numDocto;
        private string referencia;
        private string numAutorizacion;
        private string notas;
        private string numOperacion;
        private string referenciaNumerica;
        private string autoriza;
        private string cancela;
        private string formaPago;
        private string claveFormaPago;
        private decimal tipoCambio;
        private decimal cargo;
        private decimal abono;
        private string beneficiarioRFC;
        private string numeroCuenta;
        private string cuentaCLABE;
        private string sucursal;
        private string claveBanco;
        private DateTime? fechaVence;
        private DateTime? fechaDocto;
        private DateTime fechaEmision;
        private DateTime? fechaPago;
        private DateTime? fechaBoveda;
        private DateTime? fechaCancela;
        private string numeroDeCuenta;
        private int idCuenta2;
        private string claveBancoP;
        private string identificador;
        private int idConcepto;
        private int tipoint;
        private int tipoOperacionInt;
        private string json;
        private string bancoT;
        private int idDirectorio;
        private bool porJustificar;
        private int idFormato;
        private bool afectarSaldoComprobantes;
        private bool afectarSaldoCuenta;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public MovimientoBancarioModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
            this.fechaEmision = DateTime.Now;
            this.tipoCambio = 1;
            this.claveMoneda = "MXN";
            this.porJustificar = false;
            this.version = "3.0";
        }

        /// <summary>
        /// obtener o establecer el indice del movimiento
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_ID")]
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "BNCMOV_ID", ColumnDescription = "indice del movimiento", IsNullable = false)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_A")]
        [SugarColumn(ColumnName = "BNCMOV_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer version de aplicacion del banco
        /// </summary>
        [JsonProperty("version")]
        [DataNames("BNCMOV_VER")]
        [SugarColumn(ColumnName = "BNCMOV_VER", ColumnDescription = "version del documento", DefaultValue = "3.0", Length = 3)]
        public string Version {
            get { return this.version; }
            set { this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del estado del movimiento
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_STTS_ID")]
        [SugarColumn(ColumnName = "BNCMOV_STTS_ID", ColumnDescription = "status del movimiento", DefaultValue = "1")]
        public int IdStatus {
            get {
                return this.statusint;
            }
            set {
                this.statusint = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de movimiento o efecto del movimiento 1 = ingreso o 2 = Egreso
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_TIPO_ID")]
        [SugarColumn(ColumnName = "BNCMOV_TIPO_ID", ColumnDescription = "tipo de movimiento", DefaultValue = "1")]
        public int IdTipo {
            get {
                return this.tipoint;
            }
            set {
                this.tipoint = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de operacion
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_OPER_ID")]
        [SugarColumn(ColumnName = "BNCMOV_OPER_ID", ColumnDescription = "tipo de operacion", DefaultValue = "1")]
        public int IdTipoOperacion {
            get {
                return this.tipoOperacionInt;
            }
            set {
                this.tipoOperacionInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del concepto de movimiento
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CNCP_ID")]
        [SugarColumn(ColumnName = "BNCMOV_CNCP_ID", ColumnDescription = "indice del concepto de movimiento")]
        public int IdConcepto {
            get {
                return this.idConcepto;
            }
            set {
                this.idConcepto = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("BNCMOV_FRMT_ID")]
        [SugarColumn(ColumnName = "BNCMOV_FRMT_ID", ColumnDescription = "indice del formato de impresion")]
        public int IdFormato {
            get { return this.idFormato; }
            set {
                this.idFormato = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si debe afectar saldo de la cuenta
        /// </summary>
        [DataNames("BNCMOV_ASCTA")]
        [SugarColumn(ColumnName = "BNCMOV_ASCTA", ColumnDescription = "si debe afectar saldo de la cuenta")]
        public bool AfectaSaldoCuenta {
            get { return this.afectarSaldoCuenta; }
            set {
                this.afectarSaldoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la afectactacion de saldos de comprobantes
        /// </summary>
        [DataNames("BNCMOV_ASCOM")]
        [SugarColumn(ColumnName = "BNCMOV_ASCOM", ColumnDescription = "afectactacion de saldos de comprobantes")]
        public bool AfectaSaldoComprobantes {
            get {
                return this.afectarSaldoComprobantes;
            }
            set {
                this.afectarSaldoComprobantes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_DRCTR_ID")]
        [SugarColumn(ColumnName = "BNCMOV_DRCTR_ID", ColumnDescription = "indice del directorio", IsNullable = false)]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la cuenta de banco a utilizar
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CTAE_ID")]
        [SugarColumn(ColumnName = "BNCMOV_CTAE_ID", ColumnDescription = "indice de la cuenta de banco a utilizar")]
        public int IdCuentaP {
            get {
                return this.idCuentaBancaria;
            }
            set {
                this.idCuentaBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la cuenta del receptor o beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CTAR_ID")]
        [SugarColumn(ColumnName = "BNCMOV_CTAR_ID", ColumnDescription = "indice de la cuenta del banco receptor")]
        public int IdCuentaT {
            get {
                return this.idCuenta2;
            }
            set {
                this.idCuenta2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:bandera solo para indicar si el gasto es por justificar
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_POR")]
        [SugarColumn(ColumnName = "BNCMOV_POR", ColumnDescription = "bandera solo para indicar si el gasto es por justificar", IsNullable = false, Length = 1)]
        public bool PorComprobar {
            get {
                return this.porJustificar;
            }
            set {
                this.porJustificar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        [JsonProperty("noIdent", Order = -1)]
        [DataNames("BNCMOV_NOIDEN")]
        [SugarColumn(ColumnName = "BNCMOV_NOIDEN", ColumnDescription = "identificador de la prepoliza", IsNullable = true, Length = 11)]
        public string Identificador {
            get {
                return this.identificador;
            }
            set {
                this.identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_NUMCTAP")]
        [SugarColumn(ColumnName = "BNCMOV_NUMCTAP", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumeroCuentaP {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CLVBP")]
        [SugarColumn(ColumnName = "BNCMOV_CLVBP", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string ClaveBancoP {
            get {
                return this.claveBancoP;
            }
            set {
                this.claveBancoP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_FECEMS")]
        [SugarColumn(ColumnName = "BNCMOV_FECEMS", ColumnDescription = "fecha de la prepoliza", IsNullable = true)]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_FECDOC")]
        [SugarColumn(ColumnName = "BNCMOV_FECDOC", ColumnDescription = "fecha del documento", IsNullable = true)]
        public DateTime? FechaDocto {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaDocto >= firstGoodDate)
                    return this.fechaDocto;
                else
                    return null;
            }
            set {
                this.fechaDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento 
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_FCVNC")]
        [SugarColumn(ColumnName = "BNCMOV_FCVNC", ColumnDescription = "fecha de vencimiento", IsNullable = true)]
        public DateTime? FechaVence {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaVence >= firstGooDate)
                    return this.fechaVence;
                else
                    return null;
            }
            set {
                this.fechaVence = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cobro o pago
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fccbr", "BNCMOV_FCCBR")]
        [SugarColumn(ColumnName = "BNCMOV_FCCBR", ColumnDescription = "fecha de cobro o pago", IsNullable = true)]
        public DateTime? FechaAplicacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPago >= firstGoodDate)
                    return this.fechaPago;
                else
                    return null;
            }
            set {
                this.fechaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de liberación (transmición al banco, por default es null) 
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_FCLIB")]
        [SugarColumn(ColumnName = "BNCMOV_FCLIB", ColumnDescription = "fecha de liberación (transmición al banco)", IsNullable = true)]
        public DateTime? FechaBoveda {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaBoveda >= firstGooDate)
                    return this.fechaBoveda;
                return null;
            }
            set {
                this.fechaBoveda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion (default null)
        /// </summary>           
        [JsonIgnore]
        [DataNames("_cntbl3_fccncl", "BNCMOV_FCCNCL")]
        [SugarColumn(ColumnName = "BNCMOV_FCCNCL", ColumnDescription = "fecha de cancelacion", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGooDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer nombre del beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_BENF")]
        [SugarColumn(ColumnName = "BNCMOV_BENF", ColumnDescription = "nombre deel beneficiario", IsNullable = true, Length = 256)]
        public string BeneficiarioT {
            get {
                return this.beneficiario;
            }
            set {
                this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del receptor del documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_RFCR")]
        [SugarColumn(ColumnName = "BNCMOV_RFCR", ColumnDescription = "registro federal de causante del receptor", IsNullable = true, Length = 15)]
        public string BeneficiarioRFCT {
            get {
                return this.beneficiarioRFC;
            }
            set {
                this.beneficiarioRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CLVBT")]
        [SugarColumn(ColumnName = "BNCMOV_CLVBT", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string ClaveBancoT {
            get {
                return this.claveBanco;
            }
            set {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("BNCMOV_BANCOT")]
        [SugarColumn(ColumnName = "BNCMOV_BANCOT", ColumnDescription = "numero de sucursal", Length = 255, IsNullable = true)]
        public string BancoT {
            get {
                return this.bancoT;
            }
            set {
                this.bancoT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el numero de cuenta del beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_NUMCTAT")]
        [SugarColumn(ColumnName = "BNCMOV_NUMCTAT", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumeroCuentaT {
            get {
                return this.numeroCuenta;
            }
            set {
                this.numeroCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE del beneficiario
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CLABET")]
        [SugarColumn(ColumnName = "BNCMOV_CLABET", ColumnDescription = "Clave Bancaria Estandarizada (CLABE)", Length = 18, IsNullable = true)]
        public string CuentaCLABET {
            get {
                return this.cuentaCLABE;
            }
            set {
                this.cuentaCLABE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sucursal del banco receptor
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_SCRSLT")]
        [SugarColumn(ColumnName = "BNCMOV_SCRSLT", ColumnDescription = "numero de sucursal", Length = 20, IsNullable = true)]
        public string SucursalT {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de forma de pago SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CLVFRM")]
        [SugarColumn(ColumnName = "BNCMOV_CLVFRM", ColumnDescription = "clave forma de pago", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la forma de pago o cobro
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_FRMPG")]
        [SugarColumn(ColumnName = "BNCMOV_FRMPG", ColumnDescription = "descripcion de la forma de pago o cobro", IsNullable = true, Length = 64)]
        public string FormaPagoDescripcion {
            get {
                return this.formaPago;
            }
            set {
                this.formaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero del documento
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_NODOCTO")]
        [SugarColumn(ColumnName = "BNCMOV_NODOCTO", ColumnDescription = "numero de documento", IsNullable = true, Length = 20)]
        public string NumDocto {
            get {
                return this.numDocto;
            }
            set {
                this.numDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CVE_CONCEP
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_DESC")]
        [SugarColumn(ColumnName = "BNCMOV_DESC", ColumnDescription = "descripcion del movimiento", IsNullable = false, Length = 256)]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia alfanumerica
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_REF")]
        [SugarColumn(ColumnName = "BNCMOV_REF", ColumnDescription = "referencia alfanumerica", IsNullable = true, Length = 24)]
        public string Referencia {
            get {
                return this.referencia;
            }
            set {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de autorizacion bancaria
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_NUMAUTO")]
        [SugarColumn(ColumnName = "BNCMOV_NUMAUTO", ColumnDescription = "numero de autorizacion", IsNullable = true, Length = 24)]
        public string NumAutorizacion {
            get {
                return this.numAutorizacion;
            }
            set {
                this.numAutorizacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de operacion (json)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_NUMOPE")]
        [SugarColumn(ColumnName = "BNCMOV_NUMOPE", ColumnDescription = "numero de operacion", Length = 20, IsNullable = true)]
        public string NumOperacion {
            get {
                return this.numOperacion;
            }
            set {
                this.numOperacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia numerica, este dato pertenece al json
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_REFNUM")]
        [SugarColumn(ColumnName = "BNCMOV_REFNUM", ColumnDescription = "referencia numerica", Length = 20, IsNullable = true)]
        public string ReferenciaNumerica {
            get {
                return this.referenciaNumerica;
            }
            set {
                this.referenciaNumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio utilizado
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_TPCMB")]
        [SugarColumn(ColumnName = "BNCMOV_TPCMB", ColumnDescription = "tipo de cambio", Length = 4, DecimalDigits = 2)]
        public decimal TipoCambio {
            get {
                return this.tipoCambio;
            }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el cargo al documento (default 0)
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_CARGO")]
        [SugarColumn(ColumnName = "BNCMOV_CARGO", ColumnDescription = "cargo / retiro", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el abono al documento (default 0)
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_ABONO")]
        [SugarColumn(ColumnName = "BNCMOV_ABONO", ColumnDescription = "abono / deposito", IsNullable = true, Length = 11, DecimalDigits = 4)]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de moneda SAT
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_CLVMND")]
        [SugarColumn(ColumnName = "BNCMOV_CLVMND", ColumnDescription = "clave SAT de moneda", Length = 3, IsNullable = true)]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota para este documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_NOTA")]
        [SugarColumn(ColumnName = "BNCMOV_NOTA", ColumnDescription = "observaciones", IsNullable = true, Length = 64)]
        public string Nota {
            get {
                return this.notas;
            }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario cancela el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_USR_C")]
        [SugarColumn(ColumnName = "BNCMOV_USR_C", ColumnDescription = "clave del usuario que cancela el comprobante", IsNullable = true, IsOnlyIgnoreInsert = true, Length = 10)]
        public string Cancela {
            get {
                return this.cancela;
            }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estblecer la clave del usuario que autoriza el documento
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCMOV_USR_A")]
        [SugarColumn(ColumnName = "BNCMOV_USR_A", ColumnDescription = "clave del usuario que cancela", IsNullable = true, Length = 10, IsOnlyIgnoreInsert = true)]
        public string Autoriza {
            get {
                return this.autoriza;
            }
            set {
                this.autoriza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_FN")]
        [SugarColumn(ColumnName = "BNCMOV_FN", ColumnDescription = "fecha de creacion del registro", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_FM")]
        [SugarColumn(ColumnName = "BNCMOV_FM", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_USR_N")]
        [SugarColumn(ColumnName = "BNCMOV_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCMOV_USR_M")]
        [SugarColumn(ColumnName = "BNCMOV_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("BNCMOV_JSON")]
        [SugarColumn(ColumnName = "BNCMOV_JSON", ColumnDescription = "json", ColumnDataType = "Text", IsNullable = true)]
        public string InfoAuxiliar {
            get {
                return this.json;
            }
            set {
                this.json = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
