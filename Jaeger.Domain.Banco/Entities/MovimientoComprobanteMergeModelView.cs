﻿using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// clase para vista de movimiento e informacion del comprobante relacionado
    /// </summary>
    public class MovimientoComprobanteMergeModelView : MovimientoBancarioModel, IMovimientoBancarioModel, IMovimientoComprobanteMergeModelView {
        #region declaraciones
        private string idDocumento;
        private string version;
        private string tipoComprobanteText;
        private int subTipoComprobante;
        private string tipoDocumento;
        private int idComprobante;
        private string folio;
        private string emisorRFC;
        private string serie;
        private string receptorRFC;
        private string emisor;
        private string receptor;
        private string claveMetodoPago;
        private int numParcialidad;
        private decimal monto;
        private decimal porCobrar;
        private decimal acumulado;
        private string estado;
        #endregion

        public MovimientoComprobanteMergeModelView() : base() {
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [JsonProperty("ver", Order = 1)]
        [DataNames("BNCCMP_VER")]
        [SugarColumn(ColumnName = "BNCCMP_VER", ColumnDescription = "registro activo", Length = 3, IsNullable = true)]
        public new string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (efecto: ingreso, egreso, traslado, nomina, pagos)
        /// </summary>
        [JsonProperty("tipoComprobante", Order = 8)]
        [DataNames("BNCCMP_EFECTO")]
        [SugarColumn(ColumnName = "BNCCMP_EFECTO", ColumnDescription = "efecto del comprobante", Length = 10, IsIgnore = false)]
        public string TipoComprobanteText {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        [JsonProperty("subTipoComprobante", Order = 9)]
        [DataNames("BNCCMP_SBTP")]
        [SugarColumn(ColumnName = "BNCCMP_SBTP", ColumnDescription = "subtipo del comprobante, emitido, recibido, nomina, etc")]
        public int IdSubTipoComprobante {
            get {
                return this.subTipoComprobante;
            }
            set {
                this.subTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc en modo texto
        /// </summary>     
        [JsonProperty("tipo", Order = 6)]
        [DataNames("BNCCMP_TIPO")]
        [SugarColumn(ColumnName = "BNCCMP_TIPO", ColumnDescription = "tipo de comprobante", Length = 10, IsNullable = false)]
        public string TipoDocumentoText {
            get {
                return this.tipoDocumento;
            }
            set {
                this.tipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla del comprobante
        /// </summary>
        [JsonProperty("idcom", Order = 7)]
        [DataNames("BNCCMP_IDCOM")]
        [SugarColumn(ColumnName = "BNCCMP_IDCOM", ColumnDescription = "indice del comprobante fiscal")]
        public int IdComprobante {
            get {
                return this.idComprobante;
            }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el folio del comprobante
        /// </summary>
        [JsonProperty("folio", Order = 8)]
        [DataNames("BNCCMP_FOLIO")]
        [SugarColumn(ColumnName = "BNCCMP_FOLIO", ColumnDescription = "folio del comprobante", Length = 22)]
        public string Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie del comprobante
        /// </summary>
        [JsonProperty("serie", Order = 9)]
        [DataNames("BNCCMP_SERIE")]
        [SugarColumn(ColumnName = "BNCCMP_SERIE", ColumnDescription = "serie del comprobante", Length = 25)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del emisor del comprobante
        /// </summary>
        [JsonProperty("rfce", Order = 10)]
        [DataNames("BNCCMP_RFCE")]
        [SugarColumn(ColumnName = "BNCCMP_RFCE", ColumnDescription = "rfc emisor del comprobante", Length = 15)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del emisor del comprobante
        /// </summary>
        [JsonProperty("emisor", Order = 11)]
        [DataNames("BNCCMP_EMISOR")]
        [SugarColumn(ColumnName = "BNCCMP_EMISOR", ColumnDescription = "nombre del emisor del comprobante", Length = 255)]
        public string EmisorNombre {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del receptor del comprobante
        /// </summary>
        [JsonProperty("rfcr", Order = 12)]
        [DataNames("BNCCMP_RFCR")]
        [SugarColumn(ColumnName = "BNCCMP_RFCR", ColumnDescription = "rfc del receptor del comprobante", Length = 15)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [JsonProperty("receptor", Order = 13)]
        [DataNames("BNCCMP_RECEPTOR")]
        [SugarColumn(ColumnName = "BNCCMP_RECEPTOR", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get {
                return this.receptor;
            }
            set {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("mtdpg", Order = 15)]
        [DataNames("BNCCMP_MTDPG")]
        [SugarColumn(ColumnName = "BNCCMP_MTDPG", ColumnDescription = "clave del metodo de pago", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPago;
            }
            set {
                if (value != null) {
                    if (value.Length > 3) {
                        this.claveMetodoPago = value.Substring(0, 2);
                    } else {
                        this.claveMetodoPago = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("uuid", Order = 18)]
        [DataNames("BNCCMP_UUID")]
        [SugarColumn(ColumnName = "BNCCMP_UUID", ColumnDescription = "folio fiscal (uuid)", Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("par", Order = 19)]
        [DataNames("BNCCMP_PAR")]
        [SugarColumn(ColumnName = "BNCCMP_PAR", ColumnDescription = "numero de partida")]
        public int NumParcialidad {
            get {
                return this.numParcialidad;
            }
            set {

                this.numParcialidad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("total", Order = 20)]
        [DataNames("BNCCMP_TOTAL")]
        [SugarColumn(ColumnName = "BNCCMP_TOTAL", ColumnDescription = "total del comprobante", Length = 11, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.monto;
            }
            set {
                this.monto = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("porCobrar", Order = 22)]
        [DataNames("BNCCMP_XCBR")]
        [SugarColumn(ColumnName = "BNCCMP_XCBR", ColumnDescription = "importe por cobrar una vez aplicado el descuento", Length = 11, DecimalDigits = 4)]
        public decimal PorCobrar {
            get { return this.porCobrar; }
            set {
                this.porCobrar = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("acumulado", Order = 23)]
        [DataNames("BNCCMP_CBRD")]
        [SugarColumn(ColumnName = "BNCCMP_CBRD", ColumnDescription = "acumulado del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("BNCCMP_ESTADO")]
        [SugarColumn(ColumnName = "BNCCMP_ESTADO", ColumnDescription = "ultimo estado del comprobante", Length = 10, IsNullable = true)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCMP_CARGO")]
        public decimal Cargo1 { get; set; }

        [JsonProperty("abono", Order = 24)]
        [DataNames("BNCCMP_ABONO")]
        public decimal Abono1 { get; set; }

        /// <summary>
        /// status del movimiento
        /// </summary>
        public MovimientoBancarioStatusEnum Status {
            get {
                return (MovimientoBancarioStatusEnum)this.IdStatus;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }
    }
}
