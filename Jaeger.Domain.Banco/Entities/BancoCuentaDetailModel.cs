﻿using SqlSugar;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// Cuenta Bancaria Detalle
    /// </summary>
    [SugarTable("BNCCTA", "Bancos: registro de cuentas bancarias")]
    public class BancoCuentaDetailModel : BancoCuentaModel, IBancoCuentaDetailModel, ICuentaBancariaModel {
        public BancoCuentaDetailModel() : base() {

        }

        [SugarColumn(IsIgnore = true)]
        public bool IsIncluded {
            get { return !string.IsNullOrEmpty(this.ClaveBanco); }
        }
    }
}
