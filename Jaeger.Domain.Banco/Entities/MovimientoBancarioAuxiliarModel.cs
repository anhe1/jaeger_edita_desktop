﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// documento relacionado a movimiento bancario
    /// </summary>
    [SugarTable("BNCAUX", "documentos relacionados a movmientos bancarios")]
    public class MovimientoBancarioAuxiliarModel : BasePropertyChangeImplementation, IMovimientoBancarioAuxiliarModel {
        #region declaraciones
        private int index;
        private int subIndex;
        private bool activo;
        private string descripcion;
        private string tipo;
        private string url;
        private DateTime fechaNuevo;
        private string creo;
        private string filename;
        private string _Identificador;
        #endregion

        public MovimientoBancarioAuxiliarModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        #region propiedades
        /// <summary>
        /// indice del auxiliar
        /// </summary>
        [DataNames("BNCAUX_ID")]
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "BNCAUX_ID", ColumnDescription = "indice del movimiento", IsNullable = false)]
        public int IdAuxiliar {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("BNCAUX_A")]
        [SugarColumn(ColumnName = "BNCAUX_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el movimiento bancario
        /// </summary>
        [DataNames("BNCAUX_SBID")]
        [SugarColumn(ColumnName = "BNCAUX_SBID", ColumnDescription = "indice de relacion con la tabla de movimientos", DefaultValue = "0")]
        public int IdMovimiento {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>  
        [DataNames("BNCCMP_NOIDEN")]
        [SugarColumn(ColumnName = "BNCCMP_NOIDEN", ColumnDescription = "identificador de la prepoliza", IsNullable = true, Length = 11, IsIgnore = true)]
        public string Identificador {
            get {
                return this._Identificador;
            }
            set {
                this._Identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descrpcion del auxiliar
        /// </summary>
        [DataNames("BNCAUX_DESC")]
        [SugarColumn(ColumnName = "BNCAUX_DESC", ColumnDescription = "descripcion del auxiliar", IsNullable = false, Length = 256)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del archivo, de forma temporal
        /// </summary>
        [DataNames("BNCAUX_NOM")]
        [SugarColumn(ColumnName = "BNCAUX_NOM", ColumnDescription = "nombre del archivo", IsNullable = false, Length = 64)]
        public string FileName {
            get {
                return this.filename;
            }
            set {
                this.filename = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de archivo
        /// </summary>
        [DataNames("BNCAUX_TP")]
        [SugarColumn(ColumnName = "BNCAUX_TP", ColumnDescription = "descripcion del movimiento", IsNullable = true, Length = 32)]
        public string Tipo {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer URL del archivo 
        /// </summary>
        [DataNames("BNCAUX_URL")]
        [SugarColumn(ColumnName = "BNCAUX_URL", ColumnDescription = "url de descarga para el archivo", IsNullable = true, ColumnDataType = "Text")]
        public string URL {
            get {
                return this.url;
            }
            set {
                this.url = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("BNCAUX_FN")]
        [SugarColumn(ColumnName = "BNCAUX_FN", ColumnDescription = "fecha de creacion del registro", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("BNCAUX_USR_N")]
        [SugarColumn(ColumnName = "BNCAUX_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
