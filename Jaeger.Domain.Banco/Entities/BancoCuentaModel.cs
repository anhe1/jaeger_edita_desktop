﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// Cuenta Bancaria (_bnccta)
    /// </summary>
    [SugarTable("BNCCTA", "Bancos: registro de cuentas bancarias")]
    public class BancoCuentaModel : BasePropertyChangeImplementation, ICuentaBancariaModel {
        #region declaraciones
        private int index;
        private bool activo;
        private bool bancoExtranjero;
        private int diaCorte;
        private decimal saldoInicial;
        private DateTime? fechaApertura;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string rfcBeneficiario;
        private string rfcBanco;
        private string numeroDeCuenta;
        private string numCliente;
        private string claveBanco;
        private string banco;
        private string sucursal;
        private string clabe;
        private string moneda;
        private string funcionario;
        private string telefono;
        private string beneficiario;
        private string alias;
        private string plaza;
        private string creo;
        private string modifica;
        private string cuentaContable;
        private int noCheque;
        private bool readOnly;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public BancoCuentaModel() {
            this.fechaNuevo = DateTime.Now;
            this.activo = true;
            this.readOnly = false;
        }

        #region propiedades
        [DataNames("BNCCTA_ID")]
        [SugarColumn(ColumnName = "BNCCTA_ID", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCuenta {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("BNCCTA_A")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "BNCCTA_A", ColumnDescription = "registro activo", DefaultValue = "1", Length = 1, IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro es solo lectura
        /// </summary>
        [DataNames("BNCCTA_R")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "BNCCTA_R", ColumnDescription = "registro de solo lectura", DefaultValue = "0", IsNullable = false)]
        public bool ReadOnly {
            get { return this.readOnly; }
            set {
                this.readOnly = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCTA_E")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "BNCCTA_E", ColumnDescription = "indicar si el banco es extranjero", DefaultValue = "0", Length = 1, IsNullable = false)]
        public bool Extranjero{
            get { return this.bancoExtranjero; }
            set { this.bancoExtranjero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el dia de corte de la cuenta
        /// </summary>
        [DataNames("BNCCTA_DCRT")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "BNCCTA_DCRT", ColumnDescription = "dia del corte", DefaultValue = "1", Length = 2, IsNullable = false)]
        public int DiaCorte {
            get { return this.diaCorte; }
            set {
                this.diaCorte = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ultimo numero de cheque
        /// </summary>
        [DataNames("BNCCTA_NOCHQ")]
        [SugarColumn(ColumnName = "BNCCTA_NOCHQ", ColumnDescription = "numero de cheque", DefaultValue = "0", IsNullable = true)]
        public int NoCheque {
            get { return this.noCheque; }
            set { this.noCheque = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCTA_SLDINI")]
        [SugarColumn(ColumnName = "BNCCTA_SLDINI", ColumnDescription = "saldo inicial", Length = 11, DecimalDigits = 4, DefaultValue = "0")]
        public decimal SaldoInicial {
            get { return this.saldoInicial; }
            set {
                this.saldoInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [DataNames("BNCCTA_CLVB")]
        [SugarColumn(ColumnName = "BNCCTA_CLVB", ColumnDescription = "clave del banco segun catalogo del SAT", Length = 3, IsNullable = true)]
        public string ClaveBanco {
            get { return this.claveBanco; }
            set {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda a utilizar segun catalgo SAT
        /// </summary>
        [DataNames("BNCCTA_CLV")]
        [SugarColumn(ColumnName = "BNCCTA_CLV", ColumnDescription = "clave de la moneda a utilizar segun catalgo SAT", Length = 3, IsNullable = true)]
        public string Moneda {
            get { return this.moneda; }
            set {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes del beneficiario
        /// </summary>
        [DataNames("BNCCTA_RFCB")]
        [SugarColumn(ColumnName = "BNCCTA_RFCB", ColumnDescription = "rfc del beneficiario de la cuenta de banco", Length = 14, IsNullable = true)]
        public string RFCBenficiario {
            get { return this.rfcBeneficiario; }
            set {
                this.rfcBeneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes del banco
        /// </summary>
        [DataNames("BNCCTA_RFC")]
        [SugarColumn(ColumnName = "BNCCTA_RFC", ColumnDescription = "rfc del banco", Length = 14, IsNullable = true)]
        public string RFC {
            get {
                return this.rfcBanco;
            }
            set {
                this.rfcBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de identificación del cliente, numero de contrato
        /// </summary>
        [DataNames("BNCCTA_NUMCLI")]
        [SugarColumn(ColumnName = "BNCCTA_NUMCLI", ColumnDescription = "numero de identificación del cliente, numero de contrato", Length = 14, IsNullable = true)]
        public string NumCliente {
            get { return this.numCliente; }
            set {
                this.numCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [DataNames("BNCCTA_CLABE")]
        [SugarColumn(ColumnName = "BNCCTA_CLABE", ColumnDescription = "cuenta clabe", Length = 18, IsNullable = true)]
        public string CLABE {
            get { return this.clabe; }
            set {
                this.clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [DataNames("BNCCTA_NUMCTA")]
        [SugarColumn(ColumnName = "BNCCTA_NUMCTA", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = true)]
        public string NumCuenta {
            get { return this.numeroDeCuenta; }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [DataNames("BNCCTA_SCRSL")]
        [SugarColumn(ColumnName = "BNCCTA_SCRSL", ColumnDescription = "sucursal bancaria", Length = 20, IsNullable = true)]
        public string Sucursal {
            get { return this.sucursal; }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCTA_FUNC")]
        [SugarColumn(ColumnName = "BNCCTA_FUNC", ColumnDescription = "nombre del asesor o funcionario", Length = 20, IsNullable = true)]
        public string Funcionario {
            get { return this.funcionario; }
            set {
                this.funcionario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCTA_PLAZA")]
        [SugarColumn(ColumnName = "BNCCTA_PLAZA", ColumnDescription = "numero de plaza", Length = 20, IsNullable = true)]
        public string Plaza {
            get { return this.plaza; }
            set { this.plaza = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCTA_TEL")]
        [SugarColumn(ColumnName = "BNCCTA_TEL", ColumnDescription = "telefono de contacto", Length = 20, IsNullable = true)]
        public string Telefono {
            get { return this.telefono; }
            set { this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// alias de referencia para la cuenta bancaria
        /// </summary>
        [DataNames("BNCCTA_ALIAS")]
        [SugarColumn(ColumnName = "BNCCTA_ALIAS", ColumnDescription = "alias de la cuenta", Length = 20, IsNullable = true)]
        public string Alias {
            get { return this.alias; }
            set {
                this.alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta contable
        /// </summary>
        [DataNames("BNCCTA_CNTA")]
        [SugarColumn(ColumnName = "BNCCTA_CNTA", ColumnDescription = "numero de cuenta contable", Length = 40, IsNullable = true)]
        public string CuentaContable {
            get { return this.cuentaContable; }
            set {
                this.cuentaContable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [DataNames("BNCCTA_BANCO")]
        [SugarColumn(ColumnName = "BNCCTA_BANCO", ColumnDescription = "nombre del banco", Length = 64, IsNullable = true)]
        public string Banco {
            get { return this.banco; }
            set {
                this.banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [DataNames("BNCCTA_BENEF")]
        [SugarColumn(ColumnName = "BNCCTA_BENEF", ColumnDescription = "nombre del beneficiario de la cuenta de banco", Length = 256, IsNullable = true)]
        public string Beneficiario {
            get { return this.beneficiario; }
            set {
                this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCCTA_FECAPE")]
        [SugarColumn(ColumnName = "BNCCTA_FECAPE", ColumnDescription = "fecha de apertura", IsNullable = true)]
        public DateTime? FechaApertura {
            get { return this.fechaApertura; }
            set { this.fechaApertura = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("BNCCTA_FN")]
        [SugarColumn(ColumnName = "BNCCTA_FN", ColumnDescription = "fecha de creacion del registro", DefaultValue = "current_timestamp()", IsNullable = false)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [DataNames("BNCCTA_FM")]
        [SugarColumn(ColumnName = "BNCCTA_FM", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("BNCCTA_USR_N")]
        [SugarColumn(ColumnName = "BNCCTA_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("BNCCTA_USR_M")]
        [SugarColumn(ColumnName = "BNCCTA_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
