﻿using Jaeger.Domain.Banco.Contracts;
using SqlSugar;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// Bancos: formas de pago
    /// </summary>
    [SugarTable("BNCFRM", "Bancos: formas de pago")]
    public class BancoFormaPagoDetailModel : BancoFormaPagoModel, IBancoFormaPagoDetailModel, IBancoFormaPagoModel {
        public BancoFormaPagoDetailModel() : base() {

        }
    }
}