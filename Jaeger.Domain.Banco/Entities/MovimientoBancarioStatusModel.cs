﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientoBancarioStatusModel : BaseSingleModel {
        public MovimientoBancarioStatusModel() {
        }

        public MovimientoBancarioStatusModel(int id, string descripcion) {
            this.Id = id;
            this.Descripcion = descripcion;
        }
    }
}