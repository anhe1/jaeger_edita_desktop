﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using SqlSugar;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Banco.Entities {
    [SugarTable("BNCCNP", "Bancos: conceptos bancarios")]
    public class MovimientoConceptoDetailModel : MovimientoConceptoModel, IMovimientoConceptoDetailModel, IMovimientoConceptoModel {
        #region declaraciones
        private BindingList<ItemSelectedModel> _relacionDirectorio;
        private BindingList<ItemSelectedModel> _relacionComprobantes;
        private BindingList<ItemSelectedModel> _subTipoComprobante;
        private bool _ready = false;
        private bool _readyC = false;
        private bool _readyRC = false;
        #endregion

        public MovimientoConceptoDetailModel() : base() {
            this.IVA = new decimal(.16);
            this.Efecto = MovimientoBancarioEfectoEnum.Ingreso;

            this._relacionDirectorio = new BindingList<ItemSelectedModel>(((TipoRelacionComericalEnum[])Enum.GetValues(typeof(TipoRelacionComericalEnum))).Where(it => it.ToString() != "None")
                .Select(c => new ItemSelectedModel((int)c, false, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description))
                .ToList());

            this._relacionComprobantes = new BindingList<ItemSelectedModel>(((MovimientoBancarioRelacionComprobante[])Enum.GetValues(typeof(MovimientoBancarioRelacionComprobante)))
                .Select(c => new ItemSelectedModel((int)c, false, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description))
                .ToList());

            this._subTipoComprobante = new BindingList<ItemSelectedModel>(((MovimientoBancarioTipoComprobanteEnum[])Enum.GetValues(typeof(MovimientoBancarioTipoComprobanteEnum)))
                .Select(c => new ItemSelectedModel((int)c, false, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description))
                .ToList());
        }

        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioEfectoEnum Efecto {
            get {
                return (MovimientoBancarioEfectoEnum)this.IdTipoEfecto;
            }
            set {
                this.IdTipoEfecto = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BancoTipoOperacionEnum TipoOperacion {
            get {
                return (BancoTipoOperacionEnum)this.IdTipoOperacion;
            }
            set {
                this.IdTipoOperacion = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipoComprobante {
            get {
                return (CFDISubTipoEnum)this.IdSubTipoComprobante;
            }
            set {
                this.IdSubTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioFormatoImpreso FormatoImpreso {
            get { return (MovimientoBancarioFormatoImpreso)base.IdFormatoImpreso; }
            set {
                this.IdFormatoImpreso = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objetos del directorio
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ItemSelectedModel> ObjetosDirectorio {
            get {
                if (this._ready == false) {
                    if (this.Directorio != null) {
                        if (this.Directorio.Length > 0) {
                            var d = this.Directorio.Split(',');
                            this._relacionDirectorio.ToList().ForEach(it => { it.Selected = d.Contains(it.Id.ToString()); });
                        }
                    }
                    this._ready = true;
                }
                return this._relacionDirectorio;
            }
            set {
                this._relacionDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ItemSelectedModel> ObjetosComprobante {
            get {
                if (this._readyC == false) {
                    if (this.TipoComprobante != null) {
                        var d = this.TipoComprobante.Split(',');
                        this._subTipoComprobante.ToList().ForEach(it => { it.Selected = d.Contains(it.Id.ToString()); });
                    }
                    this._readyC = true;
                }
                return this._subTipoComprobante;
            }
            set {
                this._subTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ItemSelectedModel> ObjetosRelacion {
            get {
                if (this._readyRC == false) {
                    if (this.RelacionComprobantes != null) {
                        var d = this.RelacionComprobantes.Split(',');
                        this._relacionComprobantes.ToList().ForEach(it => { it.Selected = d.Contains(it.Id.ToString()); });
                    }
                    this._readyRC = true;
                }
                return this._relacionComprobantes;
            }
            set {
                this._relacionComprobantes = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ItemSelectedModel> ObjetosRelacionSelected {
            get {
                return new BindingList<ItemSelectedModel>(this._relacionComprobantes.Where(it => it.Selected == true).ToList());
            }
        }

        /// <summary>
        /// obenter listado de objetos del directorio seleccionados
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ItemSelectedModel> DirectorioSelected {
            get {
                return new BindingList<ItemSelectedModel>(this._relacionDirectorio.Where(it => it.Selected == true).ToList());
            }
        }
    }
}