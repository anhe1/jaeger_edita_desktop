﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientoBancarioTipoModel : BaseSingleModel {
        public MovimientoBancarioTipoModel() {
        }

        public MovimientoBancarioTipoModel(int id, string descripcion) {
            this.Id = id;
            this.Descripcion = descripcion;
        }
    }
}