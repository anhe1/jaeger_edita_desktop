﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// clase para tipos de comprobante
    /// </summary>
    public class BancoTipoComprobanteModel : BaseSingleModel {
        public BancoTipoComprobanteModel() {
        }

        public BancoTipoComprobanteModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
