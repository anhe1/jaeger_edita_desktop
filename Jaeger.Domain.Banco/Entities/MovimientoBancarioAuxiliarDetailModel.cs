﻿using SqlSugar;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// documentos relacionados a movmientos bancarios
    /// </summary>
    [SugarTable("BNCAUX", "documentos relacionados a movmientos bancarios")]
    public class MovimientoBancarioAuxiliarDetailModel : MovimientoBancarioAuxiliarModel, IMovimientoBancarioAuxiliarDetailModel, IMovimientoBancarioAuxiliarModel {
        private string auxiliarB64;

        public MovimientoBancarioAuxiliarDetailModel() : base() {
            this.auxiliarB64 = null;    
        }

        #region propiedades 
        [SugarColumn(IsIgnore = true)]
        public string Base64 {
            get {
                return this.auxiliarB64;
            }
            set {
                this.auxiliarB64 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// solo para la vista del formulario
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Button {
            get {
                if (ValidacionService.URL(this.URL))
                    return "Descargar";
                return "No disponible";
            }
        }
        #endregion
    }
}
