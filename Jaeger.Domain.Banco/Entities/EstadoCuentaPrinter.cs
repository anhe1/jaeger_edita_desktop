﻿namespace Jaeger.Domain.Banco.Entities {
    public class EstadoCuentaPrinter : EstadoCuentaModel {

        public EstadoCuentaPrinter(EstadoCuentaModel model) {
            this.Cuenta = model.Cuenta;
            this.Movimientos = model.Movimientos;
            this.Saldo = model.Saldo;
        }

        public string[] QrText {
            get {
                return new string[] { this.Cuenta.NumCuenta, "|", this.Cuenta.RFC, "|", this.Saldo.Ejercicio.ToString(), this.Saldo.Periodo.ToString(), this.Saldo.SaldoInicial.ToString() };
            }
        }
    }
}
