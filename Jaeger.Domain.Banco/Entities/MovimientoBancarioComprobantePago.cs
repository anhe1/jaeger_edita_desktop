﻿using SqlSugar;
using System.ComponentModel;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// comprobantes relacionados a movmientos bancarios, con este objeto obtenemos la informacion del comprobante pago y su comprobante relacionado
    /// </summary>
    [SugarTable("BNCCMP", "comprobantes relacionados a movmientos bancarios")]
    public class MovimientoBancarioComprobantePago : MovimientoBancarioComprobanteDetailModel, IMovimientoBancarioComprobanteModel, IMovimientoBancarioComprobanteDetailModel, IDataErrorInfo {
        public MovimientoBancarioComprobantePago() : base() { }

        #region informacion del documento relacionado
        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        [Services.Mapping.DataNames("_cmppgd_imppgd")]
        public decimal ImpPago {
            get; set;
        }

        /// <summary>
        /// uuid del documento relacionado
        /// </summary>
        [Services.Mapping.DataNames("_cmppgd_uuid")]
        public string IdDocumentoR { get; set; }
        #endregion
    }
}
