﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// Tipo de operacion
    /// </summary>
    public class BancoTipoOperacionModel : BaseSingleModel {
        public BancoTipoOperacionModel() {
        }

        public BancoTipoOperacionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}