﻿using System;
using SqlSugar;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// comprobantes relacionados a movmientos bancarios
    /// </summary>
    [SugarTable("BNCCMP", "comprobantes relacionados a movmientos bancarios")]
    public class MovimientoBancarioComprobanteDetailModel : MovimientoBancarioComprobanteModel, IMovimientoBancarioComprobanteModel, IMovimientoBancarioComprobanteDetailModel, IDataErrorInfo {
        public MovimientoBancarioComprobanteDetailModel() : base() {
            this.TipoDocumento = MovimientoBancarioTipoComprobanteEnum.CFDI;
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante fiscal, si es ingreso, egreso, etc
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public TipoCFDIEnum TipoComprobante {
            get {
                switch (this.TipoComprobanteText.ToLower()) {
                    case "ingreso":
                    case "i":
                        return TipoCFDIEnum.Ingreso;
                    case "egreso":
                    case "e":
                        return TipoCFDIEnum.Egreso;
                    case "traslado":
                    case "t":
                        return TipoCFDIEnum.Traslado;
                    case "pagos":
                    case "p":
                        return TipoCFDIEnum.Pagos;
                    case "nomina":
                    case "n":
                        return TipoCFDIEnum.Nomina;
                    default:
                        return TipoCFDIEnum.Ingreso;
                }
            }
            set {
                this.TipoComprobanteText = Enum.GetName(typeof(TipoCFDIEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante fiscal, emitido, recibido, nomina, etc
        /// </summary>           
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipoComprobante {
            get {
                if (this.IdSubTipoComprobante == 1)
                    return CFDISubTipoEnum.Emitido;
                else if (this.IdSubTipoComprobante == 2)
                    return CFDISubTipoEnum.Recibido;
                else if (this.IdSubTipoComprobante == 3)
                    return CFDISubTipoEnum.Nomina;
                else
                    return CFDISubTipoEnum.None;
            }
            set {
                this.IdSubTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc
        /// </summary>           
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioTipoComprobanteEnum TipoDocumento {
            get {
                return (MovimientoBancarioTipoComprobanteEnum)Enum.Parse(typeof(MovimientoBancarioTipoComprobanteEnum), this.TipoDocumentoText);
            }
            set {
                this.TipoDocumentoText = Enum.GetName(typeof(MovimientoBancarioTipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get { return string.Join("|", new string[] { this.Identificador, this.TipoComprobanteText, this.EmisorRFC, this.ReceptorRFC, this.ReceptorNombre, this.IdDocumento, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Total.ToString("#0.00") }); }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if ((this.Acumulado + this.Cargo + this.Abono) > this.Total) {
                    return "El importe del cargo ó abono es mayor al total del comprobante";
                }
                return string.Empty;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "Cargo" | columnName == "Abono") {
                    if ((this.Acumulado + this.Cargo + this.Abono) > this.Total) {
                        return "El importe del cargo ó abono es mayor al total del comprobante";
                    }
                }
                return string.Empty;
            }
        }
    }
}
