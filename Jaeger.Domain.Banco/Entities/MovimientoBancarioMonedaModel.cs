﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// moneda
    /// </summary>
    public class MovimientoBancarioMonedaModel : BasePropertyChangeImplementation {
        private string clave;
        private string descripcion;

        public MovimientoBancarioMonedaModel() {
        }

        public MovimientoBancarioMonedaModel(string clave, string descripcion) {
            this.clave = clave;
            this.descripcion = descripcion;
        }

        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
