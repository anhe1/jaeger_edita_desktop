﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientoBancarioTipoComprobante : BaseSingleModel {
        public MovimientoBancarioTipoComprobante() {
        }

        public MovimientoBancarioTipoComprobante(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
