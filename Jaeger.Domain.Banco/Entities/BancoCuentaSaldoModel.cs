﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    [SugarTable("BNCSLD", "bancos: saldos del control de bancos")]
    public class BancoCuentaSaldoModel : BasePropertyChangeImplementation, IBancoCuentaSaldoModel {
        #region declaraciones
        private int index;
        private int subId;
        private int year;
        private int month;
        private decimal saldoInicial;
        private decimal saldoFinal;
        private DateTime fechaInicial;
        private DateTime fechaNuevo;
        private string creo;
        private string nota;
        #endregion

        public BancoCuentaSaldoModel() {
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice 
        /// </summary>
        [DataNames("BNCSLD_ID")]
        [SugarColumn(ColumnName = "BNCSLD_ID", ColumnDescription = "indice", IsIdentity = true, IsPrimaryKey = true)]
        public int IdSaldo {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// indice de relacion de la cuenta bancaria
        /// </summary>
        [DataNames("BNCSLD_SBID")]
        [SugarColumn(ColumnName = "BNCSLD_SBID", ColumnDescription = "indice de relacion de la cuenta de bancos.")]
        public int IdCuenta {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer año
        /// </summary>
        [DataNames("BNCSLD_ANIO")]
        [SugarColumn(ColumnName = "BNCSLD_ANIO", ColumnDescription = "ejecicio", Length = 2, IsNullable = false)]
        public int Ejercicio {
            get {
                return this.year;
            }
            set {
                this.year = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el mes
        /// </summary>
        [DataNames("BNCSLD_MES")]
        [SugarColumn(ColumnName = "BNCSLD_MES", ColumnDescription = "periodo", Length = 2, IsNullable = false)]
        public int Periodo {
            get {
                return this.month;
            }
            set {
                this.month = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el saldo inicial
        /// </summary>
        [DataNames("BNCSLD_INICIAL")]
        [SugarColumn(ColumnName = "BNCSLD_INICIAL", ColumnDescription = "saldo inicial de la cuenta", Length = 14, DecimalDigits = 4, IsNullable = false)]
        public decimal SaldoInicial {
            get {
                return this.saldoInicial;
            }
            set {
                this.saldoInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el saldo final
        /// </summary>
        [DataNames("BNCSLD_FINAL")]
        [SugarColumn(ColumnName = "BNCSLD_FINAL", ColumnDescription = "saldo final de la cuenta", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal SaldoFinal {
            get {
                return this.saldoFinal;
            }
            set {
                this.saldoFinal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha inicial del saldo
        /// </summary>
        [DataNames("BNCSLD_FS")]
        [SugarColumn(ColumnName = "BNCSLD_FS", ColumnDescription = "fecha inicial del saldo")]
        public DateTime FechaInicial {
            get {
                return this.fechaInicial;
            }
            set {
                this.fechaInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del nuevo registro
        /// </summary>
        [DataNames("BNCSLD_FN")]
        [SugarColumn(ColumnName = "BNCSLD_FN", ColumnDescription = "fecha de creacion del nuevo registro", ColumnDataType = "TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("BNCSLD_USR_N")]
        [SugarColumn(ColumnName = "BNCSLD_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("BNCSLD_NOTA")]
        [SugarColumn(ColumnName = "BNCSLD_NOTA", ColumnDescription = "nota", Length = 255, IsNullable = true)]
        public string Nota {
            get {
                return this.nota;
            }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }
    }
}
