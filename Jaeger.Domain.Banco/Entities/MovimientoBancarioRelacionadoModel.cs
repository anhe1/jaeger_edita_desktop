﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Banco.Entities {
    [SugarTable("BNCMOVR")]
    public class MovimientoBancarioRelacionadoModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int index2;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string notas;
        private int idTipoRelacion;
        #endregion

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("BNCMOVR_ID")]
        [SugarColumn(ColumnName = "BNCMOVR_ID", ColumnDescription = "", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("BNCMOVR_A")]
        [SugarColumn(ColumnName = "BNCMOVR_A", ColumnDescription = "registro activo")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del movimiento
        /// </summary>
        [DataNames("BNCMOVR_MOV_ID")]
        [SugarColumn(ColumnName = "BNCMOVR_MOV_ID", ColumnDescription = "indice del movimiento original")]
        public int IdMovimiento {
            get {
                return this.index2;
            }
            set {
                this.index2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de relacion del movimiento bancario
        /// </summary>
        [DataNames("BNCMOVR_MOV_ID")]
        [SugarColumn(ColumnName = "BNCMOVR_MOV_ID", ColumnDescription = "indice del movimiento original")]
        public int IdTipoRelacion {
            get { return this.idTipoRelacion; }
            set {
                this.idTipoRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota para este documento
        /// </summary>           
        [DataNames("BNCMOV_NOTA")]
        [SugarColumn(ColumnName = "BNCMOV_NOTA", ColumnDescription = "nota para este documento", Length = 100)]
        public string Nota {
            get {
                return this.notas;
            }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("BNCMOVR_USR_N")]
        [SugarColumn(ColumnName = "BNCMOVR_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("BNCMOVR_FN")]
        [SugarColumn(ColumnName = "BNCMOVR_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [DataNames("BNCMOVR_FM")]
        [SugarColumn(ColumnName = "BNCMOVR_FM", ColumnDescription = "fecha de la ultima modificacion del registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("BNCMOVR_USR_M")]
        [SugarColumn(ColumnName = "BNCMOVR_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
