﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// Bancos: Formas de pago
    /// </summary>
    [SugarTable("BNCFRM", "Bancos: formas de pago")]
    public class BancoFormaPagoModel : BasePropertyChangeImplementation, IBancoFormaPagoModel {
        #region declaraciones
        private int index;
        private bool activo;
        private bool afectarSaldo;
        private string clave;
        private string claveSAT;
        private string descripcion;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public BancoFormaPagoModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        public BancoFormaPagoModel(string clave, string claveSAT, string descripcion) {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
            this.clave = clave;
            this.claveSAT = claveSAT;
            this.descripcion = descripcion;
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("BNCFRM_ID")]
        [SugarColumn(ColumnName = "BNCFRM_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames( "BNCFRM_A")]
        [SugarColumn(ColumnName = "BNCFRM_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si debe afectar el saldo d ela cuenta
        /// </summary>
        [DataNames("BNCFRM_AFS")]
        [SugarColumn(ColumnName = "BNCFRM_AFS", ColumnDescription = "afectar saldo de la cuenta", DefaultValue = "1")]
        public bool AfectarSaldo {
            get { return this.afectarSaldo; }
            set { this.afectarSaldo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno
        /// </summary>
        [DataNames("BNCFRM_CLV")]
        [SugarColumn(ColumnName = "BNCFRM_CLV", ColumnDescription = "clave de forma de pago", Length = 3)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave SAT de la forma de pago
        /// </summary>
        [DataNames("BNCFRM_CLVS")]
        [SugarColumn(ColumnName = "BNCFRM_CLVS", ColumnDescription = "clave SAT", Length = 3, IsNullable = true)]
        public string ClaveSAT {
            get {
                return this.claveSAT;
            }
            set {
                this.claveSAT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion de la forma de pago
        /// </summary>
        [DataNames("BNCFRM_NOM")]
        [SugarColumn(ColumnName = "BNCFRM_NOM", ColumnDescription = "descripcion de la forma de pago", Length = 64)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("BNCFRM_FN")]
        [SugarColumn(ColumnName = "BNCFRM_FN", ColumnDescription = "fecha de creacion del registro", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("BNCFRM_USR_N")]
        [SugarColumn(ColumnName = "BNCFRM_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [DataNames("BNCFRM_FM")]
        [SugarColumn(ColumnName = "BNCFRM_FM", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("BNCFRM_USR_M")]
        [SugarColumn(ColumnName = "BNCFRM_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}