﻿using System;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientoBancarioFactory : IMovimientoBancarioFactory {
        #region declaraciones
        private DateTime? fechaAplicacion;
        private decimal _ValorRedondeo;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public MovimientoBancarioFactory() {
            this._ValorRedondeo = new decimal(0.10);
        }

        [DataNames("documento")]
        public string TipoDocumentoText { get; set; }

        /// <summary>
        /// obtener o establecer tipos de comprobantes relacionados al movimiento bancario
        /// </summary>
        public MovimientoBancarioTipoComprobanteEnum TipoDocumento {
            get {
                return (MovimientoBancarioTipoComprobanteEnum)Enum.Parse(typeof(MovimientoBancarioTipoComprobanteEnum), this.TipoDocumentoText);
            }
            set {
                this.TipoDocumentoText = Enum.GetName(typeof(MovimientoBancarioTipoComprobanteEnum), value);
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        [DataNames("BNCCMP_SBTP", "_cfdi_doc_id", "RMSN_CTDOC_ID")]
        public int SubTipoComprobanteInt { get; set; }

        /// <summary>
        /// afectar saldo
        /// </summary>
        [DataNames("afecta")]
        public bool AfectaSaldo { get; set; }

        /// <summary>
        /// obtener o establecer subtipo del comprobante fiscal, emitido, recibido, nomina, etc
        /// </summary>           
        public CFDISubTipoEnum SubTipoComprobante {
            get {
                if (this.SubTipoComprobanteInt == 1)
                    return CFDISubTipoEnum.Emitido;
                else if (this.SubTipoComprobanteInt == 2)
                    return CFDISubTipoEnum.Recibido;
                else if (this.SubTipoComprobanteInt == 3)
                    return CFDISubTipoEnum.Nomina;
                else
                    return CFDISubTipoEnum.None;
            }
            set {
                this.SubTipoComprobanteInt = (int)value;
            }
        }

        /// <summary>
        /// obtener o establecer el efecto del comprobante
        /// </summary>
        [DataNames("efecto")]
        public string EfectoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer indice del comprobante (remision o comprobante fiscal
        /// </summary>
        [DataNames("idcomprobante")]
        public int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el folio fiscal del comprobante (uuid)
        /// </summary>
        [DataNames("uuid")]
        public string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("total")]
        public decimal Total { get; set; }

        [DataNames("xcobrar")]
        public decimal PorCobrar { get; set; }

        /// <summary>
        /// obtener o establecer el importe del cargo
        /// </summary>
        [DataNames("cargo")]
        public decimal Cargo { get; set; }

        /// <summary>
        /// obtener o establecer el importe del abono
        /// </summary>
        [DataNames("abono")]
        public decimal Abono { get; set; }

        /// <summary>
        /// obtener o establecer el monto cobrado o pagado del comprobante
        /// </summary>
        public decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de aplicacion
        /// </summary>
        [DataNames("aplicacion")]
        public DateTime? FechaAplicacion {
            get {
                if (this.fechaAplicacion >= new DateTime(1900, 1, 1))
                    return this.fechaAplicacion;
                return null;
            }
            set {
                this.fechaAplicacion = value;
            }
        }

        public decimal ValorRedondeo {
            get { return this._ValorRedondeo; }
            set { this._ValorRedondeo = value; }
        }

        /// <summary>
        /// obtener status del comprobante
        /// </summary>
        public string Status {
            get {
                // en el caso de un comprobante fiscal
                if (this.TipoDocumento == MovimientoBancarioTipoComprobanteEnum.CFDI) {
                    if (this.SubTipoComprobante == CFDISubTipoEnum.Emitido | this.SubTipoComprobante == CFDISubTipoEnum.Nomina) {
                        this.Acumulado = this.Cargo;
                        if (this.Acumulado + this._ValorRedondeo >= this.Total) {
                            return CFDIStatusEmitidoEnum.Cobrado.ToString();
                        } else {
                            return CFDIStatusEmitidoEnum.PorCobrar.ToString();
                        }
                    } else if (this.SubTipoComprobante == CFDISubTipoEnum.Recibido) {
                        this.Acumulado = this.Abono;
                        if (this.Acumulado >= this.Total) {
                            return CFDIStatusRecibidoEnum.Pagado.ToString();
                        } else {
                            return CFDIStatusRecibidoEnum.PorPagar.ToString();
                        }
                    }
                } else if (this.TipoDocumento == MovimientoBancarioTipoComprobanteEnum.Remision) {
                    // en el caso de remision emitida
                    this.Acumulado = this.Cargo;
                    if (this.Acumulado + this._ValorRedondeo >= this.PorCobrar) {
                        return ((int)CFDIStatusEmitidoEnum.Cobrado).ToString();
                    } else {
                        return ((int)CFDIStatusEmitidoEnum.PorCobrar).ToString();
                    }
                }
                return null;
            }
        }
    }
}
