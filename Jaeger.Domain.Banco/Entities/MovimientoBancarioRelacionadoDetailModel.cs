﻿using SqlSugar;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// clase derelacion de movimientos bancarios
    /// </summary>
    [SugarTable("BNCMOVR")]
    public class MovimientoBancarioRelacionadoDetailModel : MovimientoBancarioRelacionadoModel {
        public MovimientoBancarioRelacionadoDetailModel() : base() {
        }

        /// <summary>
        /// obtener o establecer el tipo de relacion del movimiento bancario
        /// </summary>
        public MovimientoBancarioTipoRelacionEnum TipoRelacion {
            get { return (MovimientoBancarioTipoRelacionEnum)base.IdTipoRelacion; }
            set {
                base.IdTipoRelacion = (int)value;
                this.OnPropertyChanged();
            }
        }
    }
}
