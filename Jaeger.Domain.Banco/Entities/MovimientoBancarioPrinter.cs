﻿using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientoBancarioPrinter : MovimientoBancarioDetailModel, IMovimientoBancarioDetailModel {
        public MovimientoBancarioPrinter(MovimientoBancarioDetailModel model) {
            Id = model.Id;
            Abono = model.Abono;
            Activo = model.Activo;
            AliasP = model.AliasP;
            Autoriza = model.Autoriza;
            Auxiliar = model.Auxiliar;
            BancoP = model.BancoP;
            BancoT = model.BancoT;
            BeneficiarioP = model.BeneficiarioP;
            BeneficiarioRFCP = model.BeneficiarioRFCP;
            BeneficiarioRFCT = model.BeneficiarioRFCT;
            BeneficiarioT = model.BeneficiarioT;
            Cancela = model.Cancela;
            Cargo = model.Cargo;
            ClaveBancoP = model.ClaveBancoP;
            ClaveBancoT = model.ClaveBancoT;
            ClaveFormaPago = model.ClaveFormaPago;
            ClaveMoneda = model.ClaveMoneda;
            Concepto = model.Concepto;
            Creo = model.Creo;
            CuentaCLABEP = model.CuentaCLABEP;
            CuentaCLABET = model.CuentaCLABET;
            CuentaPropia = model.CuentaPropia;
            FechaAplicacion = model.FechaAplicacion;
            FechaBoveda = model.FechaBoveda;
            FechaCancela = model.FechaCancela;
            FechaDocto = model.FechaDocto;
            FechaEmision = model.FechaEmision;
            FechaModifica = model.FechaModifica;
            FechaNuevo = model.FechaNuevo;
            FechaVence = model.FechaVence;
            FormaPagoDescripcion = model.FormaPagoDescripcion;
            IdConcepto = model.IdConcepto;
            IdFormato = model.IdFormato;
            IdCuentaP = model.IdCuentaP;
            IdCuentaT = model.IdCuentaT;
            IdDirectorio = model.IdDirectorio;
            Identificador = model.Identificador;
            InfoAuxiliar = model.InfoAuxiliar;
            Modifica = model.Modifica;
            Nota = model.Nota;
            NumAutorizacion = model.NumAutorizacion;
            NumDocto = model.NumDocto;
            NumeroCuentaP = model.NumeroCuentaP;
            NumeroCuentaT = model.NumeroCuentaT;
            NumOperacion = model.NumOperacion;
            PorComprobar = model.PorComprobar;
            Referencia = model.Referencia;
            ReferenciaNumerica = model.ReferenciaNumerica;
            Saldo = model.Saldo;
            Status = model.Status;
            IdStatus = model.IdStatus;
            SucursalP = model.SucursalP;
            SucursalT = model.SucursalT;
            Tipo = model.Tipo;
            TipoCambio = model.TipoCambio;
            IdTipo = model.IdTipo;
            TipoOperacion = model.TipoOperacion;
            IdTipoOperacion = model.IdTipoOperacion; 
            //var j = MovimientoBancarioDetailModel.Json(model.InfoAuxiliar);
            //CuentaPropia = j.CuentaPropia;
            Comprobantes = model.Comprobantes;
        }

        public MovimientoBancarioPrinter(IMovimientoBancarioDetailModel model) {
            Id = model.Id;
            Abono = model.Abono;
            Activo = model.Activo;
            AliasP = model.AliasP;
            Autoriza = model.Autoriza;
            Auxiliar = model.Auxiliar;
            BancoP = model.BancoP;
            BancoT = model.BancoT;
            BeneficiarioP = model.BeneficiarioP;
            BeneficiarioRFCP = model.BeneficiarioRFCP;
            BeneficiarioRFCT = model.BeneficiarioRFCT;
            BeneficiarioT = model.BeneficiarioT;
            Cancela = model.Cancela;
            Cargo = model.Cargo;
            ClaveBancoP = model.ClaveBancoP;
            ClaveBancoT = model.ClaveBancoT;
            ClaveFormaPago = model.ClaveFormaPago;
            ClaveMoneda = model.ClaveMoneda;
            Concepto = model.Concepto;
            Creo = model.Creo;
            CuentaCLABEP = model.CuentaCLABEP;
            CuentaCLABET = model.CuentaCLABET;
            CuentaPropia = model.CuentaPropia;
            FechaAplicacion = model.FechaAplicacion;
            FechaBoveda = model.FechaBoveda;
            FechaCancela = model.FechaCancela;
            FechaDocto = model.FechaDocto;
            FechaEmision = model.FechaEmision;
            FechaModifica = model.FechaModifica;
            FechaNuevo = model.FechaNuevo;
            FechaVence = model.FechaVence;
            FormaPagoDescripcion = model.FormaPagoDescripcion;
            IdConcepto = model.IdConcepto;
            IdFormato = model.IdFormato;
            IdCuentaP = model.IdCuentaP;
            IdCuentaT = model.IdCuentaT;
            IdDirectorio = model.IdDirectorio;
            Identificador = model.Identificador;
            InfoAuxiliar = model.InfoAuxiliar;
            Modifica = model.Modifica;
            Nota = model.Nota;
            NumAutorizacion = model.NumAutorizacion;
            NumDocto = model.NumDocto;
            NumeroCuentaP = model.NumeroCuentaP;
            NumeroCuentaT = model.NumeroCuentaT;
            NumOperacion = model.NumOperacion;
            PorComprobar = model.PorComprobar;
            Referencia = model.Referencia;
            ReferenciaNumerica = model.ReferenciaNumerica;
            Saldo = model.Saldo;
            Status = model.Status;
            IdStatus = model.IdStatus;
            SucursalP = model.SucursalP;
            SucursalT = model.SucursalT;
            Tipo = model.Tipo;
            TipoCambio = model.TipoCambio;
            IdTipo = model.IdTipo;
            TipoOperacion = model.TipoOperacion;
            IdTipoOperacion = model.IdTipoOperacion;
            //var j = MovimientoBancarioDetailModel.Json(model.InfoAuxiliar);
            //CuentaPropia = j.CuentaPropia;
            Comprobantes = model.Comprobantes;
        }

        public string Titulo { get; set; }

        public byte[] LogoTipo {
            get; set;
        }

        /// <summary>
        /// obtener la cantidad en letra
        /// </summary>
        public string TotalEnLetra {
            get {
                if (this.Tipo == MovimientoBancarioEfectoEnum.Egreso)
                    return NumeroALetras.Convertir((double)this.Cargo, 1);
                else if (this.Tipo == MovimientoBancarioEfectoEnum.Ingreso)
                    return NumeroALetras.Convertir((double)this.Abono, 1);
                else
                    return "";
            }
        }

        /// <summary>
        /// informacion para QR
        /// </summary>
        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.Identificador,
                    (this.NumDocto == null ? "" : "&folio=" + this.NumDocto.ToString()),
                    "&rfce=", this.BeneficiarioRFCP, "&rfcr=", this.BeneficiarioRFCT, "&fecha=", this.FechaEmision.ToShortDateString(), "&total=", this.Cargo.ToString() };
            }
        }
    }
}
