﻿using Newtonsoft.Json;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    public class MovimientoBancarioCuentaModel : BasePropertyChangeImplementation, IMovimientoBancarioCuentaModel {
        #region declaraciones
        private string rfcBanco;
        private string numeroDeCuenta;
        private string claveBanco;
        private string banco;
        private string sucursal;
        private string clabe;
        private string beneficiario;
        private string alias;
        #endregion

        public MovimientoBancarioCuentaModel() {
        }

        #region propiedades
        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [JsonProperty("clvBanco")]
        public string ClaveBanco {
            get {
                return this.claveBanco;
            }
            set {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes del banco
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rfcBanco;
            }
            set {
                this.rfcBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        [JsonProperty("clabe")]
        public string CLABE {
            get {
                return this.clabe;
            }
            set {
                this.clabe = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("alias")]
        public string Alias {
            get {
                return this.alias;
            }
            set {
                this.alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [JsonProperty("numCuenta")]
        public string NumCuenta {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        [JsonProperty("sucursal")]
        public string Sucursal {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [JsonProperty("banco")]
        public string Banco {
            get {
                return this.banco;
            }
            set {
                this.banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        [JsonProperty("beneficiario")]
        public string Beneficiario {
            get {
                return this.beneficiario;
            }
            set {
                this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
