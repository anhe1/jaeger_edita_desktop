﻿using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    public class Configuration : Base.Abstractions.BasePropertyChangeImplementation, IConfiguration {
        #region declaraciones
        private string _Folder;
        private int _Decimales;
        private decimal _Redondeo;
        private string _cveMoneda;
        private int _IdReciboCobro;
        private int _IdReciboPago;
        private int _IdReciboComision;
        #endregion

        public Configuration() {
            this.Folder = "Bancos";
            this.Decimales = 2;
            this.Redondeo = new decimal(.40);
            this._IdReciboCobro = 2;
            this._IdReciboPago = 3;
            this._IdReciboComision = 7;
        }

        public string Folder {
            get { return this._Folder; }
            set { this._Folder = value; }
        }

        public int Decimales {
            get { return this._Decimales; }
            set { this._Decimales = value; }
        }

        public decimal Redondeo {
            get { return this._Redondeo; }
            set { this._Redondeo = value; }
        }

        public string CveMoneda {
            get { return this._cveMoneda; }
            set { this._cveMoneda = value; }
        }

        /// <summary>
        /// obtener o establcer el indice del movimiento para los recibos de cobro
        /// </summary>
        public int IdReciboCobro {
            get { return this._IdReciboCobro; }
            set {
                this._IdReciboCobro = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el indice del movimiento para los recibos de pago
        /// </summary>
        public int IdReciboPago {
            get { return this._IdReciboPago; }
            set {
                this._IdReciboPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el indice del movimiento para los recibos de comisiones
        /// </summary>
        public int IdReciboComision {
            get { return this._IdReciboComision; }
            set {
                this._IdReciboComision = value;
                this.OnPropertyChanged();
            }
        }
    }
}
