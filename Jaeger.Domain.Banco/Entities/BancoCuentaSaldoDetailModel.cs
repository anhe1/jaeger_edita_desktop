﻿using SqlSugar;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// bancos: saldos del control de bancos
    /// </summary>
    [SugarTable("BNCSLD", "bancos: saldos del control de bancos")]
    public class BancoCuentaSaldoDetailModel : BancoCuentaSaldoModel, IBancoCuentaSaldoDetailModel {
        public BancoCuentaSaldoDetailModel() : base() {
        }
    }
}
