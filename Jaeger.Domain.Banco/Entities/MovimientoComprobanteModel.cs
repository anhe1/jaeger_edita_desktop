﻿using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// vista de comprobantes-movimientos
    /// </summary>
    [SugarTable("BNCMOV", "movimientos bancarios")]
    public class MovimientoComprobanteModel : MovimientoBancarioModel {
        public MovimientoComprobanteModel() : base() {
        }

        [DataNames("BNCCMP_IDCOM")]
        public int IdComprobante { get; set; }

        [DataNames("BNCCMP_CARGO")]
        public decimal Cargo1 { get; set; }

        [JsonProperty("abono", Order = 24)]
        [DataNames("BNCCMP_ABONO")]
        public decimal Abono1 { get; set; }

        [DataNames("BNCCMP_CBRD")]
        public decimal Acumulado { get; set; }

        /// <summary>
        /// status del movimiento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioStatusEnum Status {
            get {
                return (MovimientoBancarioStatusEnum)this.IdStatus;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }
    }
}
