﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// comprobante
    /// </summary>
    [SugarTable("_cfdi", "Comprobantes fiscales")]
    public class ComprobanteFiscalModel : BasePropertyChangeImplementation, IComprobanteFiscalModel {
        #region variables locales
        private int index;
        private string version;
        private string tipoComprobanteText;
        private string status;
        private string serie;
        private string folio;
        private string estado;
        private string emisorRFC;
        private string emisorNombre;
        private string receptorRFC;
        private string receptorNombre;
        private DateTime fechaEmision;
        private int subTipoInt;
        private decimal total;
        private int parcialidad;
        private string idDocumento;
        private string claveMoneda;
        private string claveFormaPago;
        private string claveMetodoPago;
        private decimal acumulado;
        private DateTime? fechaUltimoPago;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteFiscalModel() {
            this.Acumulado = 0;
        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdi_id", "CFDI_ID", "_rmsn_id", "RMSN_ID")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int IdComprobante {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// version del comprobante
        /// </summary>
        [DataNames("_cfdi_ver", "CFDI_VER", "_rmsn_ver", "RMSN_VER")]
        [SugarColumn(ColumnName = "_cfdi_ver", ColumnDescription = "version del comprobante fiscal", IsNullable = true, Length = 3)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto", "CFDI_EFECTO")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobanteText {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get {
                return this.status;
            }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_cfdi_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", DefaultValue = "0")]
        public int IdTipoDocumento {
            get {
                return this.subTipoInt;
            }
            set {
                this.subTipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombre {
            get {
                return this.emisorNombre;
            }
            set {
                this.emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get {
                return this.receptorNombre;
            }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cfdi_fecupc", ColumnDescription = "fecha del ultimo pago o cobro de los recibos de pagoo cobro", IsNullable = true)]
        public DateTime? FechaUltimoPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltimoPago >= firstGoodDate) {
                    return this.fechaUltimoPago;
                }
                return null;
            }
            set {
                this.fechaUltimoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_cfdi_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPago;
            }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_cfdi_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de parcialidad en el caso de ser un comprobante de pago
        /// </summary>
        [DataNames("_cfdi_par")]
        [SugarColumn(ColumnName = "_cfdi_par", ColumnDescription = "numero de parcialidad en el caso de ser un comprobante de pago", DefaultValue = "0")]
        public int NumParcialidad {
            get {
                return this.parcialidad;
            }
            set {
                this.parcialidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.total;
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto cobrado o pagado del comprobante
        /// </summary>
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd", ColumnDescription = "monto cobrado o bien lo que se a pagado", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }
    }
}
