﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// movimiento bancario
    /// </summary>
    [SugarTable("BNCMOV", "movimientos bancarios")]
    public class MovimientoBancarioDetailModel : MovimientoBancarioModel, ICloneable, IMovimientoBancarioDetailModel, IMovimientoBancarioModel {
        #region declaraciones
        private IMovimientoBancarioCuentaModel _CuentaPropia;
        private BindingList<MovimientoBancarioComprobanteDetailModel> _Comprobantes;
        private BindingList<MovimientoBancarioAuxiliarDetailModel> _Auxiliares;
        private decimal _TotalComprobantes;
        private decimal _Saldo;
        #endregion

        public MovimientoBancarioDetailModel() : base() {
            this.Status = MovimientoBancarioStatusEnum.Transito;
            this._CuentaPropia = new MovimientoBancarioCuentaModel();

            this._Comprobantes = new BindingList<MovimientoBancarioComprobanteDetailModel>() { RaiseListChangedEvents = true };
            this._Comprobantes.AddingNew += new AddingNewEventHandler(ComprobantesField_AddingNew);
            this._Comprobantes.ListChanged += new ListChangedEventHandler(this.Comprobantes_ListChanged);
            this._Auxiliares = new BindingList<MovimientoBancarioAuxiliarDetailModel>() { RaiseListChangedEvents = true };
        }

        #region propiedades
        /// <summary>
        /// Ingreso = Deposito, Egreso = Retiro
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioEfectoEnum Tipo {
            get {
                return (MovimientoBancarioEfectoEnum)this.IdTipo;
            }
            set {
                this.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de operacion Transferencia, Cuentas etc
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BancoTipoOperacionEnum TipoOperacion {
            get {
                return (BancoTipoOperacionEnum)this.IdTipoOperacion;
            }
            set {
                this.IdTipoOperacion = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// status del movimiento
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioStatusEnum Status {
            get {
                return (MovimientoBancarioStatusEnum)this.IdStatus;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public MovimientoBancarioFormatoImpreso FormatoImpreso {
            get { return (MovimientoBancarioFormatoImpreso)this.IdFormato; }
            set {
                this.IdFormato = (int)value;
                this.OnPropertyChanged();
            }
        }

        #region propiedades de la cuenta propia
        /// <summary>
        /// cuentaPropia.Alias
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_ALIAS")]
        [SugarColumn(IsIgnore = true)]
        public string AliasP {
            get {
                return this._CuentaPropia.Alias;
            }
            set {
                this._CuentaPropia.Alias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT (base.ClaveBancoP)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_CLVB")]
        [SugarColumn(IsIgnore = true)]
        public new string ClaveBancoP {
            get {
                return base.ClaveBancoP;
            }
            set {
                base.ClaveBancoP = value;
                this._CuentaPropia.ClaveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// registro federal de causantes del banco (cuentaPropia.RFC)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_RFCB")]
        [SugarColumn(IsIgnore = true)]
        public string BeneficiarioRFCP {
            get {
                return this._CuentaPropia.RFC;
            }
            set {
                this._CuentaPropia.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del beneficiario de la cuenta (cuentaPropia.Beneficiario)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_BENEF")]
        [SugarColumn(ColumnName = "BNCCTA_BENEF")]
        public string BeneficiarioP {
            get {
                return this._CuentaPropia.Beneficiario;
            }
            set {
                this._CuentaPropia.Beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria (base.NumeroCuentaP)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_NUMCTA")]
        [SugarColumn(ColumnName = "BNCMOV_NUMCTAP", ColumnDescription = "numero de cuenta", Length = 20, IsNullable = false)]
        public new string NumeroCuentaP {
            get {
                return base.NumeroCuentaP;
            }
            set {
                base.NumeroCuentaP = value;
                this._CuentaPropia.NumCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de la sucursal (cuentaPropia.Sucursal)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_SCRSL")]
        [SugarColumn(IsIgnore = true)]
        public string SucursalP {
            get {
                return this._CuentaPropia.Sucursal;
            }
            set {
                this._CuentaPropia.Sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco (cuentaPropia.Banco)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_BANCO")]
        [SugarColumn(IsIgnore = true)]
        public string BancoP {
            get {
                return this._CuentaPropia.Banco;
            }
            set {
                this._CuentaPropia.Banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cuenta CLABE (cuentaPropia.CLABE)
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCTA_CLABE")]
        [SugarColumn(IsIgnore = true)]
        public string CuentaCLABEP {
            get {
                return this._CuentaPropia.CLABE;
            }
            set {
                this._CuentaPropia.CLABE = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        [JsonProperty("cuenta1", Order = 99)]
        [SugarColumn(IsIgnore = true)]
        public IMovimientoBancarioCuentaModel CuentaPropia {
            get {
                return this._CuentaPropia;
            }
            set {
                this._CuentaPropia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal Importe {
            get {
                if (this.Tipo == MovimientoBancarioEfectoEnum.Egreso)
                    return base.Cargo;
                else if (this.Tipo == MovimientoBancarioEfectoEnum.Ingreso)
                    return base.Abono;
                return 0;
            }
            set {
                if (this.Tipo == MovimientoBancarioEfectoEnum.Egreso)
                    base.Cargo = value;
                else if (this.Tipo == MovimientoBancarioEfectoEnum.Ingreso)
                    base.Abono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer comprobantes relacionados
        /// </summary>
        [JsonProperty("comp", Order = 100)]
        [SugarColumn(IsIgnore = true)]
        public BindingList<MovimientoBancarioComprobanteDetailModel> Comprobantes {
            get {
                return this._Comprobantes;
            }
            set {
                if (this._Comprobantes != null) {
                    this._Comprobantes.ListChanged -= Comprobantes_ListChanged;
                }
                this._Comprobantes = value;
                if (this._Comprobantes != null) {
                    this._Comprobantes.ListChanged += Comprobantes_ListChanged;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer listado de documentos auxiliares
        /// </summary>
        [JsonProperty("aux", Order = 200)]
        [SugarColumn(IsIgnore = true)]
        public BindingList<MovimientoBancarioAuxiliarDetailModel> Auxiliar {
            get {
                return this._Auxiliares;
            }
            set {
                this._Auxiliares = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public decimal Saldo {
            get {
                return this._Saldo;
            }
            set {
                this._Saldo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// devuelve verdadero si los importes coninciden
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public bool Comprobado {
            get {
                decimal _importe = 0;
                if (this.Tipo == MovimientoBancarioEfectoEnum.Egreso) {
                    _importe = this.GetTotalAbono();// this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Abono);
                } else {
                    _importe = this.GetTotalCargo();// this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Cargo);
                }
                return _importe == this.Importe;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get {
                if (this.Id == 0 || this.IdStatus > 3) {
                    return false;
                }
                return true;
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get {
                var d1 = string.Join("", this.Comprobantes.Select(it => it.GetOriginalString));
                var d2 = string.Join("|", new string[] { this.Version, this.Identificador.ToString(), this.Tipo.ToString(), this.BeneficiarioRFCP, this.BeneficiarioRFCT, this.BeneficiarioT, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Abono.ToString("0.00"), this.Cargo.ToString("0.00") });
                return "||" + string.Join("|", d2 + d1) + "||";
            }
        }
        #endregion

        #region metodos
        public static MovimientoBancarioDetailModel Json(string json) {
            try {
                var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                if (json != null)
                    return JsonConvert.DeserializeObject<MovimientoBancarioDetailModel>(json, conf);
            } catch (JsonException ex) {
                Console.WriteLine(ex.Message);
            }
            return new MovimientoBancarioDetailModel();
        }

        public string Json() {
            return JsonConvert.SerializeObject(this);
        }

        public void Clonar() {
            this.Identificador = null;
            this.Id = 0;
            this.FechaEmision = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.Status = MovimientoBancarioStatusEnum.Transito;
            this.FechaAplicacion = null;
            this.FechaCancela = null;
            this.FechaBoveda = null;
            this.FechaModifica = null;
            this.FechaVence = null;
            foreach (var item in this._Comprobantes) {
                item.Id = 0;
                item.IdMovimiento = 0;
                item.FechaNuevo = DateTime.Now;
                item.FechaModifica = null;
                item.Modifica = null;
                item.Creo = null;
                if (item.Activo == false)
                    this._Comprobantes.Remove(item);
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }

        public bool Agregar(MovimientoBancarioComprobanteDetailModel model) {
            bool existe = false;
            try {
                var buscar = this._Comprobantes.Where(it => it.IdDocumento == model.IdDocumento).ToList();
                existe = buscar.Count > 0;
            } catch (Exception) {
                existe = false;
            }
            if (!existe) {
                if (this.Tipo == MovimientoBancarioEfectoEnum.Egreso) {
                    model.Abono = model.PorCobrar - model.Acumulado;
                } else {
                    model.Cargo = model.PorCobrar - model.Acumulado;
                }
                this._Comprobantes.Add(model);
                this.Comprobantes_ListChanged(new object(), new ListChangedEventArgs(ListChangedType.ItemAdded, -1));
            }
            return existe;
        }

        public bool Remover(MovimientoBancarioComprobanteDetailModel model) {
            if (model.Id == 0) {
                this.Comprobantes.Remove(model);
                return true;
            } else {
                model.Activo = false;
            }
            return false;
        }

        private void ComprobantesField_AddingNew(object sender, AddingNewEventArgs e) {
        }

        private void Comprobantes_ListChanged(object sender, ListChangedEventArgs e) {
            this._TotalComprobantes = this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Abono);
            if (this.PorComprobar == false)
                this.Cargo = this._TotalComprobantes;

            this._TotalComprobantes = this.Comprobantes.Where(
                (MovimientoBancarioComprobanteDetailModel p) => p.Activo == true && p.TipoComprobante == TipoCFDIEnum.Ingreso).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Cargo);
            if (this.PorComprobar == false)
                this.Abono = this._TotalComprobantes;
            this.Importe = this.Importe;
        }

        public decimal GetTotalAbono() {
            return this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Abono);
        }

        public decimal GetTotalCargo() {
            return this.Comprobantes.Where((MovimientoBancarioComprobanteDetailModel p) => p.Activo == true).Sum((MovimientoBancarioComprobanteDetailModel p) => p.Cargo);
        }

        public bool Check() {
            foreach (var item in this.Comprobantes) {

            }
            return false;
        }
        #endregion
    }
}
