﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Domain.Banco.Entities {
    ///<summary>
    /// Bancos: conceptos bancarios
    ///</summary>
    [SugarTable("BNCCNP", "Bancos: conceptos bancarios")]
    public class MovimientoConceptoModel : BasePropertyChangeImplementation, IMovimientoConceptoModel {
        #region declaraciones
        private int index;
        private int _idTipoEfecto;
        private int tipoOperacionEnum;
        private int subTipoComprobante;
        private int _idStatusComprobante;
        private bool activo;
        private bool requiereComprobantes;
        private bool afectarSaldoComprobantes;
        private bool readOnly;
        private decimal porcentajeIVA;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string concepto;
        private string cuentaContable;
        private string clasificacion;
        private string creo;
        private string modifica;
        private string descripcion;
        private string statusComprobante;
        private int formatoImpreso;
        private string _objetosDirectorio;
        private string _tipoComprobante;
        private string _relacionComprobantes;
        private int _idtipoComprobante;
        #endregion

        public MovimientoConceptoModel() {
            this.activo = true;
            this.requiereComprobantes = false;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer CVE_CONCEP
        /// </summary>
        [DataNames("BNCCNP_ID")]
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnName = "BNCCNP_ID", ColumnDescription = "indice del concepto", IsNullable = false)]
        public int IdConcepto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ESTADO
        /// </summary>
        [DataNames("BNCCNP_A")]
        [SugarColumn(ColumnName = "BNCCNP_A", ColumnDescription = "registro activo", DefaultValue = "1", Length = 1, IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el concepto es solo lectura
        /// </summary>
        [DataNames("BNCCNP_R")]
        [SugarColumn(ColumnName = "BNCCNP_R", ColumnDataType = "SMALLINT", ColumnDescription = "registro de solo lectura", DefaultValue = "0", IsNullable = false)]
        public bool ReadOnly {
            get { return this.readOnly; }
            set {
                this.readOnly = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el requerimiento de comprobanyee
        /// </summary>
        [DataNames("BNCCNP_REQ")]
        [SugarColumn(ColumnName = "BNCCNP_REQ", ColumnDescription = "requiere comprobante", DefaultValue = "0", Length = 1, IsNullable = false)]
        public bool RequiereComprobantes {
            get {
                return this.requiereComprobantes;
            }
            set {
                this.requiereComprobantes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la afectactacion de saldos de comprobantes
        /// </summary>
        [DataNames("BNCCNP_AFS")]
        [SugarColumn(ColumnName = "BNCCNP_AFS", ColumnDescription = "afectactacion de saldos de comprobantes", DefaultValue = "0", Length = 1, IsNullable = false)]
        public bool AfectaSaldoComprobantes {
            get {
                return this.afectarSaldoComprobantes;
            }
            set {
                this.afectarSaldoComprobantes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub tipo de comprobante
        /// </summary>
        [DataNames("BNCCNP_IDSBT")]
        [SugarColumn(ColumnName = "BNCCNP_IDSBT", ColumnDescription = "sub tipo de comprobante", DefaultValue = "0", IsNullable = false)]
        public int IdSubTipoComprobante {
            get {
                return this.subTipoComprobante;
            }
            set {
                this.subTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status del comprobante //BNCCNP_IDSTTS
        /// </summary>
        [DataNames("BNCCNP_STTS_ID", "BNCCNP_STATUS")]
        [SugarColumn(ColumnName = "BNCCNP_STTS_ID", ColumnDescription = "indice del status del comprobante", DefaultValue = "0")]
        public int IdStatusComprobante {
            get { return this._idStatusComprobante; }
            set {
                this._idStatusComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de operacion
        /// </summary>
        [DataNames("BNCCNP_IDOP")]
        [SugarColumn(ColumnName = "BNCCNP_IDOP", ColumnDescription = "indice de tipo de operacion", DefaultValue = "0")]
        public int IdTipoOperacion {
            get {
                return this.tipoOperacionEnum;
            }
            set {
                this.tipoOperacionEnum = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TIPO / efecto
        /// </summary>
        [DataNames("BNCCNP_IDTP")]
        [SugarColumn(ColumnName = "BNCCNP_IDTP", ColumnDescription = "indice de tipo o efecto", DefaultValue = "1")]
        public int IdTipoEfecto {
            get {
                return this._idTipoEfecto;
            }
            set {
                this._idTipoEfecto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de tipo de comprobante
        /// </summary>
        [DataNames("BNCCNP_IDTPCMB")]
        [SugarColumn(ColumnName = "BNCCNP_IDTPCMB", ColumnDescription = "indice de tipo de comprobante", DefaultValue = "0")]
        public int IdTipoComprobante {
            get { return this._idtipoComprobante; }
            set {
                this._idtipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del formato impreso
        /// </summary>
        [DataNames("BNCCNP_FRMT_ID")]
        [SugarColumn(ColumnName = "BNCCNP_FRMT_ID", ColumnDescription = "nombre del formato impreso", DefaultValue = "0")]
        public int IdFormatoImpreso {
            get {
                return this.formatoImpreso;
            }
            set {
                this.formatoImpreso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer IVA
        /// </summary>
        [DataNames("BNCCNP_IVA")]
        [SugarColumn(ColumnName = "BNCCNP_IVA", ColumnDescription = "porcentaje del iva aplicable a la operacion", DefaultValue = "0")]
        public decimal IVA {
            get {
                return this.porcentajeIVA;
            }
            set {
                this.porcentajeIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecr los tipos de comprobante aceptado por el movimiento
        /// </summary>
        [DataNames("BNCCNP_TPCMP")]
        [SugarColumn(ColumnName = "BNCCNP_TPCMP", ColumnDescription = "tipos de comprobante aceptado por el movimiento", Length = 40, IsNullable = true)]
        public string TipoComprobante {
            get { return this._tipoComprobante; }
            set {
                this._tipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el status del comprobante
        /// </summary>
        [DataNames("BNCCNP_STATUS")]
        [SugarColumn(ColumnName = "BNCCNP_STATUS", ColumnDescription = "status de los comprobantes", Length = 40, IsNullable = true)]
        public string StatusComprobante {
            get {
                return this.statusComprobante;
            }
            set {
                this.statusComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de indice de relacion entre comprobantes
        /// </summary>
        [DataNames("BNCCNP_RLCCMP")]
        [SugarColumn(ColumnName = "BNCCNP_RLCCMP", ColumnDescription = "lista de indice de relacion entre comprobantes", Length = 40, IsNullable = true)]
        public string RelacionComprobantes {
            get { return this._relacionComprobantes; }
            set {
                this._relacionComprobantes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CLASIFICACION
        /// </summary>
        [DataNames("BNCCNP_CLASS")]
        [SugarColumn(ColumnName = "BNCCNP_CLASS", ColumnDescription = "clasificacion", Length = 3, IsNullable = true)]
        public string Clasificacion {
            get {
                return this.clasificacion;
            }
            set {
                this.clasificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del concepto
        /// </summary>
        [DataNames("BNCCNP_DESC")]
        [SugarColumn(ColumnName = "BNCCNP_DESC", ColumnDescription = "descipcion del concepto", Length = 40, IsNullable = false)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CONCEP
        /// </summary>
        [DataNames("BNCCNP_NOM")]
        [SugarColumn(ColumnName = "BNCCNP_NOM", ColumnDescription = "nombre, concepto o descripcion", Length = 40, IsNullable = false)]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta contable
        /// </summary>
        [DataNames("BNCCNP_CNTA")]
        [SugarColumn(ColumnName = "BNCCNP_CNTA", ColumnDescription = "cuenta contable", Length = 40, IsNullable = true)]
        public string CuentaContable {
            get {
                return this.cuentaContable;
            }
            set {
                this.cuentaContable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer los objetos del directorio
        /// </summary>
        [DataNames("BNCCNP_RLCDIR")]
        [SugarColumn(ColumnName = "BNCCNP_RLCDIR", ColumnDescription = "objetos de relacion del directorio", Length = 20, IsNullable = true)]
        public string Directorio {
            get {
                return this._objetosDirectorio;
            }
            set {
                this._objetosDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("BNCCNP_FN")]
        [SugarColumn(ColumnName = "BNCCNP_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("BNCCNP_USR_N")]
        [SugarColumn(ColumnName = "BNCCNP_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [DataNames("BNCCNP_FM")]
        [SugarColumn(ColumnName = "BNCCNP_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("BNCCNP_USR_M")]
        [SugarColumn(ColumnName = "BNCCNP_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}