﻿using System.ComponentModel;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    public class EstadoCuentaModel : BasePropertyChangeImplementation {
        private BancoCuentaDetailModel cuenta;
        private BindingList<IMovimientoBancarioDetailModel> movimientos;
        private BancoCuentaSaldoDetailModel saldoCuenta;

        public EstadoCuentaModel() {
            this.cuenta = new BancoCuentaDetailModel();
            this.saldoCuenta = new BancoCuentaSaldoDetailModel();
            this.movimientos = new BindingList<IMovimientoBancarioDetailModel>();
        }

        /// <summary>
        /// obtener o establecer informacion de la cuenta bancaria
        /// </summary>
        public BancoCuentaDetailModel Cuenta {
            get {
                return this.cuenta;
            }
            set {
                this.cuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer informacion del saldo de la cuenta
        /// </summary>
        public BancoCuentaSaldoDetailModel Saldo {
            get {
                return this.saldoCuenta;
            }
            set {
                this.saldoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la informacion de los movimientos de la cuenta
        /// </summary>
        public BindingList<IMovimientoBancarioDetailModel> Movimientos {
            get {
                return this.movimientos;
            }
            set {
                this.movimientos = value;
                this.OnPropertyChanged();
            }
        }

        public void Procesar() {
            decimal saldo1 = 0;
            if (this.Saldo != null) {
                saldo1 = this.Saldo.SaldoInicial;
            }

            for (int i = 0; i < this.movimientos.Count; i++) {
                if (this.movimientos[i].AfectaSaldoCuenta == true) {
                    if (this.movimientos[i].Status != MovimientoBancarioStatusEnum.Cancelado) {
                        if (this.movimientos[i].Tipo == MovimientoBancarioEfectoEnum.Ingreso) {
                            saldo1 = saldo1 + this.movimientos[i].Abono;
                        } else if (this.movimientos[i].Tipo == MovimientoBancarioEfectoEnum.Egreso) {
                            saldo1 = saldo1 - this.movimientos[i].Cargo;
                        }
                        movimientos[i].Saldo = saldo1;
                    }
                } else {
                    movimientos[i].Saldo = saldo1;
                }
            }
        }
    }
}
