﻿using System;
using SqlSugar;
using Newtonsoft.Json;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Banco.Entities {
    /// <summary>
    /// comprobantes relacionados a movmientos bancarios
    /// </summary>
    [SugarTable("BNCCMP", "comprobantes relacionados a movmientos bancarios")]
    public class MovimientoBancarioComprobanteModel : BasePropertyChangeImplementation, IMovimientoBancarioComprobanteModel {
        #region declaraciones
        private int index;
        private bool activo;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string version;
        private string folio;
        private string serie;
        private string emisorRFC;
        private string emisor;
        private string receptor;
        private string idDocumento;
        private string claveMetodoPago;
        private string claveFormaPago;
        private DateTime fecha;
        private int idComprobante;
        private decimal monto;
        private string estado;
        private int numParcialidad;
        private string claveMoneda;
        private decimal cargo;
        private decimal abono;
        private decimal acumulado;
        private string receptorRFC;
        private string identificador;
        private string tipoDocumento;
        private int idmovimiento;
        private string tipoComprobanteText;
        private int subTipoComprobante;
        private string status;
        private decimal porCobrar;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public MovimientoBancarioComprobanteModel() {
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        #region propiedades

        /// <summary>
        /// obtener o establecer el indice principal de la tabla
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCCMP_ID")]
        [SugarColumn(ColumnName = "BNCCMP_ID", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [JsonIgnore]
        [DataNames("BNCCMP_A")]
        [SugarColumn(ColumnDataType = "SMALLINT", ColumnName = "BNCCMP_A", ColumnDescription = "registro activo", DefaultValue = "1", Length = 1, IsNullable = true)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el movimiento bancario
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCMP_SUBID")]
        [SugarColumn(ColumnName = "BNCCMP_SUBID", ColumnDescription = "indice de relacion con el movimiento bancario", IsNullable = false)]
        public int IdMovimiento {
            get {
                return this.idmovimiento;
            }
            set {
                this.idmovimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        [JsonProperty("noIdent", Order = -1)]
        [DataNames("BNCCMP_NOIDEN")]
        [SugarColumn(ColumnName = "BNCCMP_NOIDEN", ColumnDescription = "identificador de la prepoliza", IsNullable = true, Length = 11)]
        public string Identificador {
            get {
                return this.identificador;
            }
            set {
                this.identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [JsonProperty("ver", Order = 1)]
        [DataNames("BNCCMP_VER")]
        [SugarColumn(ColumnName = "BNCCMP_VER", ColumnDescription = "registro activo", Length = 3, IsNullable = true)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (efecto: ingreso, egreso, traslado, nomina, pagos)
        /// </summary>
        [JsonProperty("tipoComprobante", Order = 8)]
        [DataNames("BNCCMP_EFECTO")]
        [SugarColumn(ColumnName = "BNCCMP_EFECTO", ColumnDescription = "efecto del comprobante", Length = 10, IsIgnore = false)]
        public string TipoComprobanteText {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        [DataNames("status", "BNCCMP_STATUS")]
        [SugarColumn(IsIgnore = true, ColumnName = "BNCCMP_STATUS", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get {
                return this.status;
            }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        [JsonProperty("subTipoComprobante", Order = 9)]
        [DataNames("BNCCMP_SBTP")]
        [SugarColumn(ColumnName = "BNCCMP_SBTP", ColumnDescription = "subtipo del comprobante, emitido, recibido, nomina, etc")]
        public int IdSubTipoComprobante {
            get {
                return  this.subTipoComprobante;
            }
            set {
                this.subTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc en modo texto
        /// </summary>     
        [JsonProperty("tipo", Order = 6)]
        [DataNames("BNCCMP_TIPO")]
        [SugarColumn(ColumnName = "BNCCMP_TIPO", ColumnDescription = "tipo de comprobante", Length = 10, IsNullable = false)]
        public string TipoDocumentoText {
            get {
                return this.tipoDocumento;
            }
            set {
                this.tipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla del comprobante
        /// </summary>
        [JsonProperty("idcom", Order = 7)]
        [DataNames("BNCCMP_IDCOM")]
        [SugarColumn(ColumnName = "BNCCMP_IDCOM", ColumnDescription = "indice del comprobante fiscal")]
        public int IdComprobante {
            get {
                return this.idComprobante;
            }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el folio del comprobante
        /// </summary>
        [JsonProperty("folio", Order = 8)]
        [DataNames("BNCCMP_FOLIO")]
        [SugarColumn(ColumnName = "BNCCMP_FOLIO", ColumnDescription = "folio del comprobante", Length = 22)]
        public string Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie del comprobante
        /// </summary>
        [JsonProperty("serie", Order = 9)]
        [DataNames("BNCCMP_SERIE")]
        [SugarColumn(ColumnName = "BNCCMP_SERIE", ColumnDescription = "serie del comprobante", Length = 25)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del emisor del comprobante
        /// </summary>
        [JsonProperty("rfce", Order = 10)]
        [DataNames("BNCCMP_RFCE")]
        [SugarColumn(ColumnName = "BNCCMP_RFCE", ColumnDescription = "rfc emisor del comprobante", Length = 15)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del emisor del comprobante
        /// </summary>
        [JsonProperty("emisor", Order = 11)]
        [DataNames("BNCCMP_EMISOR")]
        [SugarColumn(ColumnName = "BNCCMP_EMISOR", ColumnDescription = "nombre del emisor del comprobante", Length = 255)]
        public string EmisorNombre {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del receptor del comprobante
        /// </summary>
        [JsonProperty("rfcr", Order = 12)]
        [DataNames("BNCCMP_RFCR")]
        [SugarColumn(ColumnName = "BNCCMP_RFCR", ColumnDescription = "rfc del receptor del comprobante", Length = 15)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [JsonProperty("receptor", Order = 13)]
        [DataNames("BNCCMP_RECEPTOR")]
        [SugarColumn(ColumnName = "BNCCMP_RECEPTOR", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get {
                return this.receptor;
            }
            set {
                this.receptor = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("emision", Order = 14)]
        [DataNames("BNCCMP_FECEMS")]
        [SugarColumn(ColumnName = "BNCCMP_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fecha;
            }
            set {
                this.fecha = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("mtdpg", Order = 15)]
        [DataNames("BNCCMP_MTDPG")]
        [SugarColumn(ColumnName = "BNCCMP_MTDPG", ColumnDescription = "clave del metodo de pago", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPago;
            }
            set {
                if (value != null) {
                    if (value.Length > 3) {
                        this.claveMetodoPago = value.Substring(0, 2);
                    } else {
                        this.claveMetodoPago = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("frmpg", Order = 16)]
        [DataNames("BNCCMP_FRMPG")]
        [SugarColumn(ColumnName = "BNCCMP_FRMPG", ColumnDescription = "clave de la forma de pago", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                if (value != null) {
                    if (value.Length > 2) {
                        this.claveFormaPago = value.Substring(0,1);
                    } else {
                        this.claveFormaPago = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("moneda", Order = 17)]
        [DataNames("BNCCMP_MONEDA")]
        [SugarColumn(ColumnName = "BNCCMP_MONEDA", ColumnDescription = "clave de moneda", Length = 3, IsNullable = true)]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("uuid", Order = 18)]
        [DataNames("BNCCMP_UUID")]
        [SugarColumn(ColumnName = "BNCCMP_UUID", ColumnDescription = "folio fiscal (uuid)", Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("par", Order = 19)]
        [DataNames("BNCCMP_PAR")]
        [SugarColumn(ColumnName = "BNCCMP_PAR", ColumnDescription = "numero de partida")]
        public int NumParcialidad {
            get {
                return this.numParcialidad;
            }
            set {

                this.numParcialidad = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("total", Order = 20)]
        [DataNames("BNCCMP_TOTAL")]
        [SugarColumn(ColumnName = "BNCCMP_TOTAL", ColumnDescription = "total del comprobante", Length = 11, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.monto;
            }
            set {
                this.monto = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("porCobrar", Order = 22)]
        [DataNames("BNCCMP_XCBR")]
        [SugarColumn(ColumnName = "BNCCMP_XCBR", ColumnDescription = "importe por cobrar una vez aplicado el descuento", Length = 11, DecimalDigits = 4)]
        public decimal PorCobrar {
            get { return this.porCobrar; }
            set {
                this.porCobrar = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("cargo", Order = 23)]
        [DataNames("BNCCMP_CARGO")]
        [SugarColumn(ColumnName = "BNCCMP_CARGO", ColumnDescription = "cargo", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("abono", Order = 22)]
        [DataNames("BNCCMP_ABONO")]
        [SugarColumn(ColumnName = "BNCCMP_ABONO", ColumnDescription = "abono", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("acumulado", Order = 23)]
        [DataNames("BNCCMP_CBRD")]
        [SugarColumn(ColumnName = "BNCCMP_CBRD", ColumnDescription = "acumulado del comprobante", Length = 11, DecimalDigits = 4, IsNullable = true)]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("BNCCMP_ESTADO")]
        [SugarColumn(ColumnName = "BNCCMP_ESTADO", ColumnDescription = "ultimo estado del comprobante", Length = 10, IsNullable = true)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCMP_USR_N")]
        [SugarColumn(ColumnName = "BNCCMP_USR_N", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsNullable = false)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCMP_FN")]
        [SugarColumn(ColumnName = "BNCCMP_FN", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCMP_FM")]
        [SugarColumn(ColumnName = "BNCCMP_FM", ColumnDescription = "fecha de la ultima modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [JsonIgnore]
        [DataNames("BNCCMP_USR_M")]
        [SugarColumn(ColumnName = "BNCCMP_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
