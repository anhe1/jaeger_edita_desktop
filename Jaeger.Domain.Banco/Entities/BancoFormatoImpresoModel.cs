﻿namespace Jaeger.Domain.Banco.Entities {
    public class BancoFormatoImpresoModel : Base.Abstractions.BasePropertyChangeImplementation, Contracts.IBancoFormatoImpresoModel {
        #region declaraciones
        private int _Index;
        private string _Nombre;
        private string _Reporte;
        #endregion

        public BancoFormatoImpresoModel() { }

        public int IdFormato {
            get { return this._Index; }
            set { this._Index = value;
                this.OnPropertyChanged();
            }
        }

        public string Nombre {
            get { return this._Nombre;}
            set { this._Nombre = value;
                this.OnPropertyChanged();
            }
        }

        public string Reporte {
            get { return this._Reporte;}
            set { this._Reporte = value;
                this.OnPropertyChanged();
            }
        }
    }
}
