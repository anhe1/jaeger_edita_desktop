﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Banco.Builder {
    public class CuentaBancariaQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, ICuentaBancariaQueryBuilder, ICuentaBancariaOnlyActiveQueryBuilder {
        public CuentaBancariaQueryBuilder() : base() { }

        public ICuentaBancariaOnlyActiveQueryBuilder Id(int indice) {
            this._Conditionals.Add(new Conditional("BNCCTA_ID", indice.ToString()));
            return this;
        }

        public ICuentaBancariaOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("BNCCTA_A", "1"));
            return this;
        }

        public ICuentaBancariaOnlyActiveQueryBuilder OnlyRead(bool onlyRead = true) {
            if (onlyRead) this._Conditionals.Add(new Conditional("BNCCTA_R", "1"));
            return this;
        }
    }
}
