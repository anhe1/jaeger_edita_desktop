﻿using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Builder {
    /// <summary>
    /// clase builder para consulta para conceptos bancarios
    /// </summary>
    public interface IConceptoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// Efecto del movimiento bancario
        /// </summary>
        IConceptoEfectoQueryBuilder Efecto(MovimientoBancarioEfectoEnum efecto);

        /// <summary>
        /// tipo de operacion
        /// </summary>
        IConceptoEfectoQueryBuilder TipoOperacion(BancoTipoOperacionEnum tipoOperacion);

        /// <summary>
        /// solo registros activos
        /// </summary>
        /// <param name="onlyActive">default verdadero</param>
        IConceptoQueryBuild OnlyActive(bool onlyActive = true);
        
        IConceptoQueryBuild Id(int indice);

        /// <summary>
        /// registros de solo lectura
        /// </summary>
        IConceptoQueryBuild OnlyRead(bool onlyRead);
    }

    /// <summary>
    /// Efecto del movimiento bancario
    /// </summary>
    public interface IConceptoEfectoQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        /// <param name="onlyActive">default verdadero</param>
        IConceptoQueryBuild OnlyActive(bool onlyActive = true);
    }

    public interface IConceptoQueryBuild : IConditionalBuilder {
        
    }
}
