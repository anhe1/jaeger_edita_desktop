﻿using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Banco.Builder {
    /// <summary>
    /// consulta para busqueda de comprobantes fiscales
    /// </summary>
    public interface IComprobanteSearchQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IComprobanteSearchComprobanteQueryBuilder ForComprobante();
        IComprobanteSearchRemisionQueryBuilder ForRemision();
    }

    public interface IComprobanteSearchComprobanteQueryBuilder {
        IComprobanteSearchEmitidoQueryBuilder Emitido();
        IComprobanteSearchEmitidoQueryBuilder Recibido();
        IComprobanteSearchConceptoQueryBuilder With(IMovimientoConceptoDetailModel concepto);
    }

    public interface IComprobanteSearchRemisionQueryBuilder {
        IComprobanteSearchEmitidoQueryBuilder Emitido();
        IComprobanteSearchConceptoQueryBuilder With(IMovimientoConceptoDetailModel concepto);
    }

    public interface IComprobanteSearchEmitidoQueryBuilder {
        IComprobanteSearchStatusQueryBuilder Status(CFDIStatusEmitidoEnum status);
        IComprobanteSearchStatusQueryBuilder Status(int idStatus);
    }

    public interface IComprobanteSearchRecibidoQueryBuilder {
        IComprobanteSearchStatusQueryBuilder Status(CFDIStatusRecibidoEnum status);
    }

    public interface IComprobanteSearchIdDirectorioQueryBuilder : IConditionalBuilder {
        IComprobanteSearchVendedorQueryBuilder IdVendedor(int idVendedor);
    }

    public interface IComprobanteSearchVendedorQueryBuilder : IConditionalBuilder {

    }

    public interface IComprobanteSearchStatusQueryBuilder : IConditionalBuilder {
        IComprobanteSearchIdDirectorioQueryBuilder IdDirectorio(int idDirectorio);
        IComprobanteSearchVendedorQueryBuilder IdVendedor(int idVendedor);
    }

    public interface IComprobanteSearchConceptoQueryBuilder {
        IComprobanteSearchIdDirectorioQueryBuilder IdDirectorio(int idDirectorio);
        IComprobanteSearchIdDirectorioQueryBuilder WithFolio(int folio);
    }
}
