﻿using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Banco.Builder {
    /// <summary>
    /// clase builder para consulta para conceptos bancarios
    /// </summary>
    public class ConceptoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IConceptoQueryBuilder, IConceptoEfectoQueryBuilder, IConceptoQueryBuild {
        public ConceptoQueryBuilder() : base() { }

        /// <summary>
        /// Efecto del movimiento bancario
        /// </summary>
        public IConceptoEfectoQueryBuilder Efecto(MovimientoBancarioEfectoEnum efecto) {
            var d0 = (int)efecto;
            this._Conditionals.Add(new Conditional("BNCCNP_IDTP", d0.ToString()));
            return this;
        }

        public IConceptoQueryBuild Id(int indice) {
            this._Conditionals.Add(new Conditional("BNCCNP_ID", indice.ToString()));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        /// <param name="onlyActive">default verdadero</param>
        public IConceptoQueryBuild OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("BNCCNP_A", "1"));
            return this;
        }

        /// <summary>
        /// registros de solo lectura
        /// </summary>
        public IConceptoQueryBuild OnlyRead(bool onlyRead) {
            if(onlyRead == true) {
                this._Conditionals.Add(new Conditional("BNCCNP_R", "1"));
            }
            return this;
        }

        /// <summary>
        /// tipo de operacion
        /// </summary>
        public IConceptoEfectoQueryBuilder TipoOperacion(BancoTipoOperacionEnum tipoOperacion) {
            var value = (int)tipoOperacion;
            this._Conditionals.Add(new Conditional("BNCCNP_IDOP", value.ToString()));
            return this;
        }
    }
}
