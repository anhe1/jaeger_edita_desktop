﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Banco.Builder {
    public class BeneficiarioQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IBeneficiarioQueryBuilder, IBeneficiarioRelacionQueryBuilder, IBeneficiarioActivoQueryBuilder {
        public IBeneficiarioRelacionQueryBuilder Relacion(List<string> roles) {
            return this;
        }
        public IBeneficiarioRelacionQueryBuilder Relacion(List<int> roles) {
            this._Conditionals.Add(new Conditional("DRCTRR_CTREL_ID", string.Join(",", roles), Base.ValueObjects.ConditionalTypeEnum.In));
            return this;
        }

        public IBeneficiarioActivoQueryBuilder IsActive(bool active = true) {
            if (active) this._Conditionals.Add(new Conditional("DRCTRR_A", "1"));
            return this;
        }
    }
}
