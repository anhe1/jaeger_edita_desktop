﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Builder {
    public interface ICuentaBancariaQueryBuilder : IConditionalBuilder, IConditionalBuild {
        ICuentaBancariaOnlyActiveQueryBuilder Id(int indice);
        ICuentaBancariaOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
        ICuentaBancariaOnlyActiveQueryBuilder OnlyRead(bool onlyRead = true);
    }

    public interface ICuentaBancariaOnlyActiveQueryBuilder : IConditionalBuilder {

    }
}
