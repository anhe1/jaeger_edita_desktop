﻿using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Banco.Builder {
    public class MovimientoBancarioQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IMovimientoBancarioQueryBuilder, IMovimientoBancarioIdCuentaQueryBuilder, IMovimientoBancarioMonthQueryBuilder,
        IMovimientoBancarioStatusQueryBuilder, IMovimientoBancarioEfectoQueryBuilder, IMovimientoBancarioQueryBuild {

        public MovimientoBancarioQueryBuilder() : base() { }

        public IMovimientoBancarioIdCuentaQueryBuilder IdCuenta(int idCuenta) {
            this._Conditionals.Add(new Conditional("BNCMOV_CTAE_ID", idCuenta.ToString()));
            return this;
        }

        public IMovimientoBancarioIdCuentaQueryBuilder IdDirectorio(int idDirectorio) {
            this._Conditionals.Add(new Conditional("BNCMOV_DRCTR_ID", idDirectorio.ToString()));
            return this;
        }

        public IMovimientoBancarioQueryBuild WithId(int indice) {
            this._Conditionals.Add(new Conditional("BNCMOV_ID", indice.ToString()));
            return this;
        }

        public IMovimientoBancarioQueryBuild Identificador(string identificador) {
            this._Conditionals.Add(new Conditional("BNCMOV_NOIDEN", identificador));
            return this;
        }

        public IMovimientoBancarioQueryBuild Identificador(System.Collections.Generic.List<string> identificador) {
            this.Conditionals.Clear();
            this._Conditionals.Add(new Conditional("BNCMOV_NOIDEN", string.Join(",", identificador), ConditionalTypeEnum.In));
            return this;
        }

        public IMovimientoBancarioMonthQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("EXTRACT(YEAR FROM BNCMOV_FECEMS)", year.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        public IMovimientoBancarioStatusQueryBuilder Month(int month) {
            if (month > 0)
                this._Conditionals.Add(new Conditional("EXTRACT(MONTH FROM BNCMOV_FECEMS)", month.ToString()));
            return this;
        }

        public IMovimientoBancarioEfectoQueryBuilder TipoOperacion(BancoTipoOperacionEnum idOperacion) {
            this._Conditionals.Add(new Conditional("BNCMOV_OPER_ID", idOperacion.ToString()));
            return this;
        }

        public IMovimientoBancarioEfectoQueryBuilder WithEfecto(MovimientoBancarioEfectoEnum efecto) {
            var d0 = (int)efecto;
            this._Conditionals.Add(new Conditional("BNCMOV_TIPO_ID", d0.ToString()));
            return this;
        }

        public IMovimientoBancarioEfectoQueryBuilder WithStatus(MovimientoBancarioStatusEnum status) {
            var d0 = (int)status;
            this._Conditionals.Add(new Conditional("BNCMOV_STTS_ID", d0.ToString()));
            return this;
        }

        public IMovimientoBancarioEfectoQueryBuilder IdConcepto(int idConcepto) {
            this._Conditionals.Add(new Conditional("BNCMOV_CNCP_ID", idConcepto.ToString()));
            return this;
        }

        public IMovimientoBancarioEfectoQueryBuilder IdConcepto(int[] idConcepto) {
            var s0 = string.Join(string.Empty, idConcepto);
            this._Conditionals.Add(new Conditional("BNCMOV_CNCP_ID", s0, ConditionalTypeEnum.In));
            return this;
        }
    }
}
