﻿using System;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Banco.Builder {
    /// <summary>
    /// consulta para busqueda de comprobantes fiscales
    /// </summary>
    public class ComprobanteSearchQueryBuilder : ConditionalBuilder, IComprobanteSearchQueryBuilder, IConditionalBuilder, IConditionalBuild, IComprobanteSearchComprobanteQueryBuilder, IComprobanteSearchRemisionQueryBuilder,
        IComprobanteSearchEmitidoQueryBuilder, IComprobanteSearchRecibidoQueryBuilder, IComprobanteSearchIdDirectorioQueryBuilder, IComprobanteSearchVendedorQueryBuilder, IComprobanteSearchStatusQueryBuilder,
        IComprobanteSearchConceptoQueryBuilder {

        protected internal bool _IsComprobante = true;
        protected internal bool _IsEmitido = true;

        public ComprobanteSearchQueryBuilder() : base() { }

        public IComprobanteSearchComprobanteQueryBuilder ForComprobante() {
            this._IsComprobante = true;

            return this;
        }

        public IComprobanteSearchRemisionQueryBuilder ForRemision() {
            this._IsComprobante = false;
            return this;
        }

        public IComprobanteSearchEmitidoQueryBuilder Emitido() {
            this._IsEmitido = true;
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_doc_id", "1"));
            } else {
                this._Conditionals.Add(new Conditional("RMSN_CTDOC_ID", "26"));
            }
            return this;
        }

        public IComprobanteSearchEmitidoQueryBuilder Recibido() {
            this._IsEmitido = false;
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_doc_id", "2"));
            }
            return this;
        }

        public IComprobanteSearchConceptoQueryBuilder With(IMovimientoConceptoDetailModel concepto) {
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_doc_id", concepto.IdSubTipoComprobante.ToString()));
                if (concepto.IdSubTipoComprobante == 2) {
                    this._Conditionals.Add(new Conditional("_cfdi_status", Enum.Parse(typeof(CFDIStatusRecibidoEnum), concepto.IdStatusComprobante.ToString()).ToString()));
                } else {
                    this._Conditionals.Add(new Conditional("_cfdi_status", Enum.Parse(typeof(CFDIStatusEmitidoEnum), concepto.IdStatusComprobante.ToString()).ToString()));
                }
            } else {
                this._Conditionals.Add(new Conditional("RMSN_CTDOC_ID", "26"));
                this._Conditionals.Add(new Conditional("RMSN_STTS_ID", concepto.IdStatusComprobante.ToString()));
            }
            return this;
        }

        public IComprobanteSearchStatusQueryBuilder Status(CFDIStatusEmitidoEnum status) {
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_status", status.ToString()));
            }
            return this;
        }

        public IComprobanteSearchStatusQueryBuilder Status(CFDIStatusRecibidoEnum status) {
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_status", status.ToString()));
            }
            return this;
        }

        public IComprobanteSearchStatusQueryBuilder Status(int idStatus) {
            if (this._IsComprobante == false) {
                this._Conditionals.Add(new Conditional("RMSN_STTS_ID", idStatus.ToString()));
            }
            return this;
        }

        public IComprobanteSearchVendedorQueryBuilder IdVendedor(int idVendedor) {
            return this;
        }

        public IComprobanteSearchIdDirectorioQueryBuilder IdDirectorio(int idDirectorio) {
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_drctr_id", idDirectorio.ToString()));
            } else {
                this._Conditionals.Add(new Conditional("RMSN_DRCTR_ID", idDirectorio.ToString()));
            }
            return this;
        }

        public IComprobanteSearchIdDirectorioQueryBuilder WithFolio(int folio) {
            if (this._IsComprobante) {
                this._Conditionals.Add(new Conditional("_cfdi_folio", folio.ToString()));
            } else {
                this._Conditionals.Add(new Conditional("RMSN_FOLIO", folio.ToString()));
            }
            return this;
        }
    }
}
