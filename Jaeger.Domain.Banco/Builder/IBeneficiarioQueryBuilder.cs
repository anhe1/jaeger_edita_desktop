﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Builder {
    public interface IBeneficiarioQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IBeneficiarioRelacionQueryBuilder Relacion(List<string> roles);
        IBeneficiarioRelacionQueryBuilder Relacion(List<int> roles);
    }

    public interface IBeneficiarioActivoQueryBuilder : IConditionalBuilder {
    }

    public interface IBeneficiarioRelacionQueryBuilder : IConditionalBuilder {
        IBeneficiarioActivoQueryBuilder IsActive(bool active = true);
    }
}
