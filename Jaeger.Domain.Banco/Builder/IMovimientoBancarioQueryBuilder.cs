﻿using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Builder {
    public interface IMovimientoBancarioQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IMovimientoBancarioIdCuentaQueryBuilder IdCuenta(int idCuenta);
        IMovimientoBancarioMonthQueryBuilder Year(int year);
        IMovimientoBancarioQueryBuild Identificador(string identificador);
        IMovimientoBancarioQueryBuild Identificador(System.Collections.Generic.List<string> identificador);
        IMovimientoBancarioQueryBuild WithId(int indice);
        IMovimientoBancarioIdCuentaQueryBuilder IdDirectorio(int idDirectorio);
    }

    public interface IMovimientoBancarioIdCuentaQueryBuilder : IConditionalBuilder {
        IMovimientoBancarioMonthQueryBuilder Year(int year);
    }

    public interface IMovimientoBancarioMonthQueryBuilder : IConditionalBuilder {
        IMovimientoBancarioStatusQueryBuilder Month(int month);
        IMovimientoBancarioEfectoQueryBuilder WithStatus(MovimientoBancarioStatusEnum status);
        IMovimientoBancarioEfectoQueryBuilder TipoOperacion(BancoTipoOperacionEnum tipoOperacion);
        IMovimientoBancarioEfectoQueryBuilder IdConcepto(int idConcepto);
        IMovimientoBancarioEfectoQueryBuilder IdConcepto(int[] idConcepto);
    }

    public interface IMovimientoBancarioStatusQueryBuilder : IConditionalBuilder {
        IMovimientoBancarioEfectoQueryBuilder WithEfecto(MovimientoBancarioEfectoEnum efecto);
        IMovimientoBancarioEfectoQueryBuilder TipoOperacion(BancoTipoOperacionEnum tipoOperacion);
        IMovimientoBancarioEfectoQueryBuilder IdConcepto(int idConcepto);
        IMovimientoBancarioEfectoQueryBuilder IdConcepto(int[] idConcepto);
    }

    public interface IMovimientoBancarioEfectoQueryBuilder : IConditionalBuilder {
        IMovimientoBancarioEfectoQueryBuilder WithStatus(MovimientoBancarioStatusEnum status);
        IMovimientoBancarioEfectoQueryBuilder WithEfecto(MovimientoBancarioEfectoEnum efecto);
    }

    public interface IMovimientoBancarioQueryBuild : IConditionalBuilder {

    }
}
