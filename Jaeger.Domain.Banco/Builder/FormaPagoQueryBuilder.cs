﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Banco.Builder {
    public class FormaPagoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IFormaPagoQueryBuilder, IFormaPagoOnlyActiveQueryBuilder {
        public FormaPagoQueryBuilder() : base() { }

        public IFormaPagoOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("BNCFRM_A", "1"));
            return this;
        }
    }
}
