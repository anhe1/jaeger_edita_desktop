﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Builder {
    public interface IFormaPagoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IFormaPagoOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IFormaPagoOnlyActiveQueryBuilder : IConditionalBuilder {

    }
}
