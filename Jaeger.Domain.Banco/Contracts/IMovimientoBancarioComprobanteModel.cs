﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioComprobanteModel {
        #region propiedades
        /// <summary>
        /// obtener o establecer el indice principal de la tabla
        /// </summary>           
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con el movimiento bancario
        /// </summary>
        int IdMovimiento { get; set; }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        string Identificador { get; set; }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (efecto: ingreso, egreso, traslado, nomina, pagos)
        /// </summary>
        string TipoComprobanteText { get; set; }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        string Status { get; set; }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        int IdSubTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc en modo texto
        /// </summary>     
        string TipoDocumentoText { get; set; }

        /// <summary>
        /// obtener o establecer el indice de la tabla del comprobante
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el folio del comprobante
        /// </summary>
        string Folio { get; set; }

        /// <summary>
        /// obtener o establecer la serie del comprobante
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del emisor del comprobante
        /// </summary>
        string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del receptor del comprobante
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        string ClaveMetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        string ClaveFormaPago { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        string ClaveMoneda { get; set; }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// numero de parcialidad
        /// </summary>
        int NumParcialidad { get; set; }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer el importe de cargo al comprobante
        /// </summary>
        decimal Cargo { get; set; }

        /// <summary>
        /// obtener o establecer el importe abonado al comprobante
        /// </summary>
        decimal Abono { get; set; }

        /// <summary>
        /// obtener o establecer el importe acumulado de cargos y abonos del comprobante
        /// </summary>
        decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        string Estado { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
        #endregion

        object Tag { get; set; }
    }
}