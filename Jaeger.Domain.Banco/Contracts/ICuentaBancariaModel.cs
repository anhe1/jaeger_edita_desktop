﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// modelo de cuenta bancaria
    /// </summary>
    public interface ICuentaBancariaModel {
        int IdCuenta { get; set; }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer si el registro es solo lectura
        /// </summary>
        bool ReadOnly { get; set; }

        /// <summary>
        /// obtener o establecer el dia de corte de la cuenta
        /// </summary>
        int DiaCorte { get; set; }

        /// <summary>
        /// registro federal de causantes
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        string NumCuenta { get; set; }

        /// <summary>
        /// obtener o establecer numero de identificación del cliente, numero de contrato
        /// </summary>
        string NumCliente { get; set; }

        /// <summary>
        /// obtener o establecer codigo de banco según catalogo del SAT
        /// </summary>
        string ClaveBanco { get; set; }

        /// <summary>
        /// nombre del banco
        /// </summary>
        string Banco { get; set; }

        /// <summary>
        /// obtener o establecer numero de la sucursal
        /// </summary>
        string Sucursal { get; set; }

        /// <summary>
        /// obtener o establecer cuenta CLABE
        /// </summary>
        string CLABE { get; set; }
        
        /// <summary>
        /// obtener o establecer clave de la moneda a utilizar segun catalgo SAT
        /// </summary>
        string Moneda { get; set; }

        string RFCBenficiario { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del funcionario o agente del banco
        /// </summary>
        string Funcionario { get; set; }

        /// <summary>
        /// obtener o establecer el numero de contacto del funcionario o agente del banco
        /// </summary>
        string Telefono { get; set; }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        string Beneficiario { get; set; }

        /// <summary>
        /// obtener o establecer alias de referencia para la cuenta bancaria
        /// </summary>
        string Alias { get; set; }

        string CuentaContable { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        DateTime? FechaApertura { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer el saldo inicial de la cuenta
        /// </summary>
        decimal SaldoInicial { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        object Tag { get; set; }
    }
}
