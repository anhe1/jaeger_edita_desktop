﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IBancoFormaPagoModel {
        /// <summary>
        /// obtener o establecer
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer si debe afectar el saldo d ela cuenta
        /// </summary>
        bool AfectarSaldo { get; set; }

        /// <summary>
        /// obtener o establecer clave de control interno
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer clave SAT de la forma de pago
        /// </summary>
        string ClaveSAT { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la forma de pago
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
    }
}
