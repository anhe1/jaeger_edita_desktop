﻿namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioCuentaModel {
        string ClaveBanco { get; set; }

        /// <summary>
        /// registro federal de causantes del banco
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// cuenta CLABE
        /// </summary>
        string CLABE { get; set; }

        string Alias { get; set; }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        string NumCuenta { get; set; }

        /// <summary>
        /// numero de la sucursal
        /// </summary>
        string Sucursal { get; set; }

        /// <summary>
        /// nombre del banco
        /// </summary>
        string Banco { get; set; }

        /// <summary>
        /// nombre del beneficiario de la cuenta
        /// </summary>
        string Beneficiario { get; set; }
    }
}
