﻿namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioAuxiliarDetailModel : IMovimientoBancarioAuxiliarModel {
        string Base64 { get; set; }
        string Button { get; }
    }
}