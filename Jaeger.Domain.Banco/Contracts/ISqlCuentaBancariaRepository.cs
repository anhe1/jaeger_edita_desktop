﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// repositorio de cuentas bancarias
    /// </summary>
    public interface ISqlCuentaBancariaRepository : IGenericRepository<BancoCuentaModel> {
        /// <summary>
        /// obtener cuenta bancaria por su indice
        /// </summary>
        new BancoCuentaDetailModel GetById(int index);

        /// <summary>
        /// listado de cuentas bancarias
        /// </summary>
        /// <param name="onlyActive">registros activos</param>
        IEnumerable<IBancoCuentaDetailModel> GetList(bool onlyActive);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// almacenar cuenta bancaria
        /// </summary>
        /// <param name="model">modelo</param>
        IBancoCuentaDetailModel Save(IBancoCuentaDetailModel model);

        /// <summary>
        /// crear tablas
        /// </summary>
        void Create();
    }
}
