﻿namespace Jaeger.Domain.Banco.Contracts {
    public interface IBancoCuentaDetailModel : ICuentaBancariaModel {
        bool IsIncluded { get; }
    }
}
