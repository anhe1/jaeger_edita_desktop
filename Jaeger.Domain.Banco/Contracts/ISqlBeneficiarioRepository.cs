﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// catalogo de beneficiarios
    /// </summary>
    public interface ISqlBeneficiarioRepository : IGenericRepository<BeneficiarioModel> {
        #region beneficiarios
        /// <summary>
        /// obtener beneficiario
        /// </summary>
        /// <param name="index">indice</param>
        BeneficiarioDetailModel GetBeneficiario(int index);

        /// <summary>
        /// almacenar beneficiario
        /// </summary>
        /// <param name="model">BeneficiarioDetailModel</param>
        BeneficiarioDetailModel Save(BeneficiarioDetailModel model);

        /// <summary>
        /// obtener listado de beneficiarios
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(bool onlyActive);

        /// <summary>
        /// obtener listado de beneficiarios del directorio, generalmente son todos los registros
        /// </summary>
        IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<Conditional> keyValues);

        /// <summary>
        /// listado del directorio por relaciones 
        /// </summary>
        /// <param name="relacion">lista de relaciones</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="search">palabra de busqueda</param>
        IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<string> relacion, bool onlyActive, string search = "");
        #endregion

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
