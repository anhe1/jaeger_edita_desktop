﻿using System.ComponentModel;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioDetailModel : IMovimientoBancarioModel {
        /// <summary>
        /// Ingreso = Deposito, Egreso = Retiro
        /// </summary>
        MovimientoBancarioEfectoEnum Tipo { get; set; }

        /// <summary>
        /// tipo de operacion Transferencia, Cuentas etc
        /// </summary>
        BancoTipoOperacionEnum TipoOperacion { get; set; }

        /// <summary>
        /// status del movimiento
        /// </summary>
        MovimientoBancarioStatusEnum Status { get; set; }

        MovimientoBancarioFormatoImpreso FormatoImpreso { get; set; }

        #region propiedades de la cuenta propia
        /// <summary>
        /// cuentaPropia.Alias
        /// </summary>
        string AliasP { get; set; }

        /// <summary>
        /// codigo de banco según catalogo del SAT (base.ClaveBancoP)
        /// </summary>
        new string ClaveBancoP { get; set; }

        /// <summary>
        /// registro federal de causantes del banco (cuentaPropia.RFC)
        /// </summary>
        string BeneficiarioRFCP { get; set; }

        /// <summary>
        /// nombre del beneficiario de la cuenta (cuentaPropia.Beneficiario)
        /// </summary>
        string BeneficiarioP { get; set; }

        /// <summary>
        /// numero de cuenta bancaria (base.NumeroCuentaP)
        /// </summary>
        new string NumeroCuentaP { get; set; }

        /// <summary>
        /// numero de la sucursal (cuentaPropia.Sucursal)
        /// </summary>
        string SucursalP { get; set; }

        /// <summary>
        /// nombre del banco (cuentaPropia.Banco)
        /// </summary>
        string BancoP { get; set; }

        /// <summary>
        /// cuenta CLABE (cuentaPropia.CLABE)
        /// </summary>
        string CuentaCLABEP { get; set; }

        #endregion

        IMovimientoBancarioCuentaModel CuentaPropia { get; set; }

        /// <summary>
        /// obtener o establecer importe
        /// </summary>
        decimal Importe { get; set; }

        /// <summary>
        /// obtener o establecer comprobantes relacionados
        /// </summary>
        BindingList<MovimientoBancarioComprobanteDetailModel> Comprobantes { get; set; }

        /// <summary>
        /// obtener o establecer listado de documentos auxiliares
        /// </summary>
        BindingList<MovimientoBancarioAuxiliarDetailModel> Auxiliar { get; set; }

        decimal Saldo { get; set; }

        /// <summary>
        /// devuelve verdadero si los importes coninciden
        /// </summary>
        bool Comprobado { get; }

        bool IsEditable { get; }

        bool Agregar(MovimientoBancarioComprobanteDetailModel model);

        bool Remover(MovimientoBancarioComprobanteDetailModel model);

        decimal GetTotalAbono();

        decimal GetTotalCargo();

        string Json();
    }
}
