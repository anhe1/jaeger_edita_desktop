﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// repositorio de tipos o conceptos de movimientos
    /// </summary>
    public interface ISqlMovimientoConceptoRepository : IGenericRepository<MovimientoConceptoModel> {
        /// <summary>
        /// Almacenar concepto o tipo de movimiento
        /// </summary>
        /// <param name="model">MovimientoConceptoDetailModel</param>
        MovimientoConceptoDetailModel Save(MovimientoConceptoDetailModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// crear tabla de conceptos
        /// </summary>
        void Create();
    }
}
