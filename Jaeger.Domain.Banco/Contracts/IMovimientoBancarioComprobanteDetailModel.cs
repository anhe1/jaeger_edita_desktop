﻿using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioComprobanteDetailModel : IMovimientoBancarioComprobanteModel {
        /// <summary>
        /// obtener o establecer el tipo de comprobante fiscal, si es ingreso, egreso, etc
        /// </summary>
        TipoCFDIEnum TipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer subtipo del comprobante fiscal, emitido, recibido, nomina, etc
        /// </summary>           
        CFDISubTipoEnum SubTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc
        /// </summary>           
        MovimientoBancarioTipoComprobanteEnum TipoDocumento { get; set; }

        string GetOriginalString { get; }
    }
}