﻿using System;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioFactory {
        string TipoDocumentoText { get; set; }

        /// <summary>
        /// obtener o establecer tipos de comprobantes relacionados al movimiento bancario
        /// </summary>
        MovimientoBancarioTipoComprobanteEnum TipoDocumento { get; set; }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        int SubTipoComprobanteInt { get; set; }

        bool AfectaSaldo { get; set; }

        /// <summary>
        /// obtener o establecer subtipo del comprobante fiscal, emitido, recibido, nomina, etc
        /// </summary>           
        CFDISubTipoEnum SubTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el efecto del comprobante
        /// </summary>
        string EfectoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer indice del comprobante (remision o comprobante fiscal
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el folio fiscal del comprobante (uuid)
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        decimal Total { get; set; }

        decimal PorCobrar { get; set; }

        /// <summary>
        /// obtener o establecer el importe del cargo
        /// </summary>
        decimal Cargo { get; set; }

        /// <summary>
        /// obtener o establecer el importe del abono
        /// </summary>
        decimal Abono { get; set; }

        /// <summary>
        /// obtener o establecer el monto cobrado o pagado del comprobante
        /// </summary>
        decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de aplicacion
        /// </summary>
        DateTime? FechaAplicacion { get; set; }

        decimal ValorRedondeo { get; set; }

        /// <summary>
        /// obtener status del comprobante
        /// </summary>
        string Status { get; }
    }
}
