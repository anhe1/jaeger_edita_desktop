﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioModel {
        /// <summary>
        /// obtener o establecer el indice del movimiento
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer version de aplicacion del banco
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtener o establecer el indice del estado del movimiento
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de movimiento o efecto del movimiento 1 = ingreso o 2 = Egreso
        /// </summary>
        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de operacion
        /// </summary>
        int IdTipoOperacion { get; set; }

        /// <summary>
        /// obtener o establecer indice del concepto de movimiento
        /// </summary>
        int IdConcepto { get; set; }

        /// <summary>
        /// obteneer o establecer la clave del formato de impresion
        /// </summary>
        int IdFormato { get; set; }

        /// <summary>
        /// obtener o establecer si debe afectar saldo de la cunta
        /// </summary>
        bool AfectaSaldoCuenta { get; set; }

        /// <summary>
        /// obtener o establecer la afectactacion de saldos de comprobantes
        /// </summary>
        bool AfectaSaldoComprobantes { get; set; }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer el indice de la cuenta de banco a utilizar
        /// </summary>
        int IdCuentaP { get; set; }

        /// <summary>
        /// obtener o establecer indice de la cuenta del receptor o beneficiario
        /// </summary>
        int IdCuentaT { get; set; }

        /// <summary>
        /// Desc:bandera solo para indicar si el gasto es por justificar
        /// Default:0
        /// Nullable:False
        /// </summary>           
        bool PorComprobar { get; set; }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        string Identificador { get; set; }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        string NumeroCuentaP { get; set; }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        string ClaveBancoP { get; set; }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>           
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer fecha del documento
        /// </summary>           
        DateTime? FechaDocto { get; set; }

        /// <summary>
        /// obtener o establecer fecha de vencimiento 
        /// </summary>           
        DateTime? FechaVence { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cobro o pago
        /// </summary>           
        DateTime? FechaAplicacion { get; set; }

        /// <summary>
        /// obtener o establecer fecha de liberación (transmición al banco, por default es null) 
        /// </summary>           
        DateTime? FechaBoveda { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion (default null)
        /// </summary>           
        DateTime? FechaCancela { get; set; }

        /// <summary>
        /// obtener o establecer nombre del beneficiario
        /// </summary>
        string BeneficiarioT { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del receptor del documento
        /// </summary>           
        string BeneficiarioRFCT { get; set; }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        string ClaveBancoT { get; set; }

        /// <summary>
        /// obtener o establece el nombre del banco
        /// </summary>
        string BancoT { get; set; }

        /// <summary>
        /// obtener o establcer el numero de cuenta del beneficiario
        /// </summary>
        string NumeroCuentaT { get; set; }

        /// <summary>
        /// obtener o establecer cuenta CLABE del beneficiario
        /// </summary>
        string CuentaCLABET { get; set; }

        /// <summary>
        /// obtener o establecer sucursal del banco receptor
        /// </summary>
        string SucursalT { get; set; }

        /// <summary>
        /// obtener o establecer clave de forma de pago SAT
        /// </summary>
        string ClaveFormaPago { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion de la forma de pago o cobro
        /// </summary>           
        string FormaPagoDescripcion { get; set; }

        /// <summary>
        /// obtener o establecer el numero del documento
        /// </summary>
        string NumDocto { get; set; }

        /// <summary>
        /// obtener o establecer CVE_CONCEP
        /// </summary>
        string Concepto { get; set; }

        /// <summary>
        /// obtener o establecer referencia alfanumerica
        /// </summary>           
        string Referencia { get; set; }

        /// <summary>
        /// obtener o establecer el numero de autorizacion bancaria
        /// </summary>           
        string NumAutorizacion { get; set; }

        /// <summary>
        /// numero de operacion (json)
        /// </summary>
        string NumOperacion { get; set; }

        /// <summary>
        /// referencia numerica, este dato pertenece al json
        /// </summary>
        string ReferenciaNumerica { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cambio utilizado
        /// </summary>
        decimal TipoCambio { get; set; }

        /// <summary>
        /// obtener o establecer el cargo al documento (default 0)
        /// </summary>           
        decimal Cargo { get; set; }

        /// <summary>
        /// obtener o establecer el abono al documento (default 0)
        /// </summary>           
        decimal Abono { get; set; }

        /// <summary>
        /// obtener o establecer la clave de moneda SAT
        /// </summary>
        string ClaveMoneda { get; set; }

        /// <summary>
        /// obtener o establecer nota para este documento
        /// </summary>           
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario cancela el documento
        /// </summary>           
        string Cancela { get; set; }

        /// <summary>
        /// obtener o estblecer la clave del usuario que autoriza el documento
        /// </summary>           
        string Autoriza { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        string InfoAuxiliar { get; set; }

        object Tag { get; set; }
    }
}
