﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoBancarioAuxiliarModel {
        /// <summary>
        /// indice del auxiliar
        /// </summary>
        int IdAuxiliar { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con el movimiento bancario
        /// </summary>
        int IdMovimiento { get; set; }

        string Identificador { get; set; }

        /// <summary>
        /// obtener o establecer la descrpcion del auxiliar
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del archivo, de forma temporal
        /// </summary>
        string FileName { get; set; }

        /// <summary>
        /// obtener o establecer tipo de archivo
        /// </summary>
        string Tipo { get; set; }

        /// <summary>
        /// obtener o establecer URL del archivo 
        /// </summary>
        string URL { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        object Tag { get; set; }
    }
}