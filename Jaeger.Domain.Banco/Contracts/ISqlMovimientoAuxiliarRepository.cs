﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// repositorio de auxiliares a los movimientos bancarios
    /// </summary>
    public interface ISqlMovimientoAuxiliarRepository : IGenericRepository<MovimientoBancarioAuxiliarModel> {
        IEnumerable<T1> GetList<T1> (List<IConditional> conditionals) where T1 : class, new();
        /// <summary>
        /// listado de auxiliares por condiciones
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        IEnumerable<MovimientoBancarioAuxiliarDetailModel> GetList(List<Conditional> conditionals);

        /// <summary>
        /// almacenar movimiento bancario
        /// </summary>
        MovimientoBancarioAuxiliarDetailModel Save(MovimientoBancarioAuxiliarDetailModel item);

        int Update(MovimientoBancarioAuxiliarDetailModel item);

        /// <summary>
        /// crear tablas de movimientos y comprobantes
        /// </summary>
        bool CreateTables();
    }
}
