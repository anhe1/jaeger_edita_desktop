﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IComprobanteFiscalModel {
        int IdComprobante { get; set; }

        /// <summary>
        /// version del comprobante
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        string TipoComprobanteText { get; set; }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        string Status { get; set; }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        int IdTipoDocumento { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        string Folio { get; set; }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        DateTime? FechaUltimoPago { get; set; }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        string ClaveMetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        string ClaveFormaPago { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        string ClaveMoneda { get; set; }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer el numero de parcialidad en el caso de ser un comprobante de pago
        /// </summary>
        int NumParcialidad { get; set; }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer el monto cobrado o pagado del comprobante
        /// </summary>
        decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        string Estado { get; set; }
    }
}
