﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// catalogo de formas de pago 
    /// </summary>
    public interface ISqlBancoFormaPagoRepository : IGenericRepository<BancoFormaPagoModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// almacenar forma de pago
        /// </summary>
        /// <param name="item">BancoFormaPagoDetailModel</param>
        IBancoFormaPagoDetailModel Save(IBancoFormaPagoDetailModel item);

        bool CrearTabla();
    }
}
