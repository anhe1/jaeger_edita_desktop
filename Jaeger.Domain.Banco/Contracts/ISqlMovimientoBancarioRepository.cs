﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// repositorio de movimientos bancarios
    /// </summary>
    public interface ISqlMovimientoBancarioRepository : IGenericRepository<MovimientoBancarioModel> {
        /// <summary>
        /// actualizar status del moviminto
        /// </summary>
        /// <param name="idStatus">indice del nuevo status</param>
        /// <param name="index">indice del movimiento</param>
        /// <returns>verdadero si transaccion</returns>
        bool Update(int idStatus, int index);

        /// <summary>
        /// obtener listado de movimientos por cuenta
        /// </summary>
        /// <param name="idCuenta">indice de la cuenta</param>
        /// <param name="month">periodo</param>
        /// <param name="year">ejercicio</param>
        /// <param name="idTipo">tipo de movimiento ingreso o egreso</param>
        //IEnumerable<IMovimientoBancarioDetailModel> GetList(int idCuenta, int month, int year, int idTipo);

        /// <summary>
        /// almacenar movimiento bancario
        /// </summary>
        /// <param name="model">MovimientoBancarioDetailModel</param>
        IMovimientoBancarioDetailModel Save(IMovimientoBancarioDetailModel model);

        /// <summary>
        /// aplicar movimiento bancario
        /// </summary>
        /// <param name="model">MovimientoBancarioDetailModel</param>
        /// <returns>verdadero si ejecuta correctamente</returns>
        bool Aplicar(IMovimientoBancarioDetailModel model);

        /// <summary>
        /// obtener listado de comprobantes por condicionales
        /// </summary>
        /// <param name="conditionals">lista de condiciones</param>
        //IEnumerable<MovimientoBancarioComprobanteDetailModel> GetComprobantes(List<Conditional> conditionals);

        /// <summary>
        /// Vista de comprobantes con movimientos
        /// </summary>
        //IEnumerable<MovimientoComprobanteModel> GetComprobanteView(List<Conditional> conditionals);

        //IEnumerable<MovimientoComprobanteMergeModelView> GetComprobanteMerge(List<Conditional> conditionals);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        bool CreateTable();

        /// <summary>
        /// crear tablas de movimientos y comprobantes
        /// </summary>
        bool CreateTables();
    }
}
