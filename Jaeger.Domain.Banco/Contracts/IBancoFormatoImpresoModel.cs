﻿namespace Jaeger.Domain.Banco.Contracts {
    public interface IBancoFormatoImpresoModel {
        int IdFormato { get; set; }
        string Nombre { get; set; }
        string Reporte { get; set; }
    }
}
