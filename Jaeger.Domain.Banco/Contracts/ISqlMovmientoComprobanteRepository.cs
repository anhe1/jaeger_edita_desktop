﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// repositorio de comprobantes para movimientos bancarios
    /// </summary>
    public interface ISqlMovmientoComprobanteRepository : IGenericRepository<MovimientoBancarioComprobanteModel> {
        /// <summary>
        /// almacenar comprobante delacionado con movmientos bancarios
        /// </summary>
        /// <param name="model">MovimientoBancarioComprobanteDetailModel</param>
        MovimientoBancarioComprobanteDetailModel Save(MovimientoBancarioComprobanteDetailModel model);

        /// <summary>
        /// almacenar lista de comprobantes delacionados con movmientos bancarios
        /// </summary>
        /// <param name="model">MovimientoBancarioComprobanteDetailModel</param>
        IEnumerable<MovimientoBancarioComprobanteDetailModel> Save(List<MovimientoBancarioComprobanteDetailModel> models);

        /// <summary>
        /// obtener listado de comprobantes por condicionales
        /// </summary>
        /// <param name="conditionals">lista de condicionales</param>
        IEnumerable<MovimientoBancarioComprobanteDetailModel> GetList(List<IConditional> conditionals);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// crear tabla de comprobantes
        /// </summary>
        void Create();
    }
}
