﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IBancoCuentaSaldoModel {
        /// <summary>
        /// obtener o establecer indice 
        /// </summary>
        int IdSaldo { get; set; }

        /// <summary>
        /// indice de relacion de la cuenta bancaria
        /// </summary>
        int IdCuenta { get; set; }

        /// <summary>
        /// obtener o establecer año
        /// </summary>
        int Ejercicio { get; set; }

        /// <summary>
        /// obtener o establecer el mes
        /// </summary>
        int Periodo { get; set; }

        /// <summary>
        /// obtener o establecer el saldo inicial
        /// </summary>
        decimal SaldoInicial { get; set; }

        /// <summary>
        /// obtener o establecer el saldo final
        /// </summary>
        decimal SaldoFinal { get; set; }

        /// <summary>
        /// obtener o establecer la fecha inicial del saldo
        /// </summary>
        DateTime FechaInicial { get; set; }

        /// <summary>
        /// obtener o establecer la fecha del nuevo registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        string Nota { get; set; }
    }
}
