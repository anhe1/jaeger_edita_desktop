﻿namespace Jaeger.Domain.Banco.Contracts {
    public interface IConfiguration {
        string Folder { get; set; }
        int Decimales { get; set; }
        decimal Redondeo { get; set; }
        string CveMoneda { get; set; }
        int IdReciboCobro { get; set; }
        int IdReciboPago { get; set; }
        int IdReciboComision { get; set; }
    }
}
