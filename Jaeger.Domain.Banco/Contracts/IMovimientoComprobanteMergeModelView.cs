﻿using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.Domain.Banco.Contracts {
    /// <summary>
    /// clase para vista de movimiento e informacion del comprobante relacionado
    /// </summary>
    public interface IMovimientoComprobanteMergeModelView : IMovimientoBancarioModel {
        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        new string Version { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de comprobante (efecto: ingreso, egreso, traslado, nomina, pagos)
        /// </summary>
        string TipoComprobanteText { get; set; }

        /// <summary>
        /// obtener o establecer subtipo del comprobante, emitido, recibido, nomina, etc (modo int para la base) en modo texto
        /// </summary>           
        int IdSubTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de comprobante CFDI, Remision, etc en modo texto
        /// </summary>     
        string TipoDocumentoText { get; set; }

        /// <summary>
        /// obtener o establecer el indice de la tabla del comprobante
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el folio del comprobante
        /// </summary>
        string Folio { get; set; }

        /// <summary>
        /// obtener o establecer la serie del comprobante
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del emisor del comprobante
        /// </summary>
        string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del receptor del comprobante
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombre { get; set; }

        string ClaveMetodoPago { get; set; }
        
        string IdDocumento { get; set; }
        
        int NumParcialidad { get; set; }

        decimal Total { get; set; }

        decimal PorCobrar { get; set; }

        decimal Acumulado { get; set; }

        string Estado { get; set; }

        decimal Cargo1 { get; set; }

        decimal Abono1 { get; set; }

        /// <summary>
        /// status del movimiento
        /// </summary>
        MovimientoBancarioStatusEnum Status { get; set; }
    }
}
