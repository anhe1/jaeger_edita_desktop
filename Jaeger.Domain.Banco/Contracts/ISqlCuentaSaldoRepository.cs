﻿using System;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.Domain.Banco.Contracts {
    public interface ISqlCuentaSaldoRepository : IGenericRepository<BancoCuentaSaldoModel> {
        /// <summary>
        /// obtener saldo de una cuenta bancaria
        /// </summary>
        BancoCuentaSaldoDetailModel GetSaldo(int idCuenta, DateTime fecha);

        BancoCuentaSaldoDetailModel GetSaldo(BancoCuentaModel objeto, DateTime fecha);

        BancoCuentaSaldoDetailModel Save(BancoCuentaSaldoDetailModel model);

        void Create();
    }
}
