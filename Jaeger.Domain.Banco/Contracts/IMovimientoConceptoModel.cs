﻿using System;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoConceptoModel {
        /// <summary>
        /// obtener o establecer indice del concepto (bnccnp_id)
        /// </summary>
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer ESTADO
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer si el concepto es solo lectura
        /// </summary>
        bool ReadOnly { get; set; }

        /// <summary>
        /// obtener o establecer el requerimiento de comprobanyee
        /// </summary>
        bool RequiereComprobantes { get; set; }

        /// <summary>
        /// obtener o establecer la afectactacion de saldos de comprobantes
        /// </summary>
        bool AfectaSaldoComprobantes { get; set; }

        /// <summary>
        /// obtener o establecer sub tipo de comprobante
        /// </summary>
        int IdSubTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del comprobante
        /// </summary>
        int IdStatusComprobante { get; set; }

        /// <summary>
        /// obtener o establecer tipo de operacion
        /// </summary>
        int IdTipoOperacion { get; set; }

        /// <summary>
        /// obtener o establecer TIPO / efecto
        /// </summary>
        int IdTipoEfecto { get; set; }

        /// <summary>
        /// obtener o establecer indice de tipo de comprobante
        /// </summary>
        int IdTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer IVA
        /// </summary>
        decimal IVA { get; set; }

        /// <summary>
        /// obtener o establecr los tipos de comprobante aceptado por el movimiento
        /// </summary>
        string TipoComprobante { get; set; }

        /// <summary>
        /// obtener o establcer el status del comprobante
        /// </summary>
        string StatusComprobante { get; set; }

        /// <summary>
        /// obtener o establecer lista de indice de relacion entre comprobantes
        /// </summary>
        string RelacionComprobantes { get; set; }

        /// <summary>
        /// obtener o establecer CLASIFICACION
        /// </summary>
        string Clasificacion { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion del concepto
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer CONCEP
        /// </summary>
        string Concepto { get; set; }

        /// <summary>
        /// obtener o establecer el numero de cuenta contable
        /// </summary>
        string CuentaContable { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del formato impreso
        /// </summary>
        int IdFormatoImpreso { get; set; }

        /// <summary>
        /// obtener o establcer los objetos del directorio
        /// </summary>
        string Directorio { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        object Tag { get; set; }
    }
}
