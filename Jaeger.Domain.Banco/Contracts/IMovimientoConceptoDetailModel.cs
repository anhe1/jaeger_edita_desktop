﻿using System.ComponentModel;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Banco.Contracts {
    public interface IMovimientoConceptoDetailModel : IMovimientoConceptoModel {
        MovimientoBancarioEfectoEnum Efecto { get; set; }

        BancoTipoOperacionEnum TipoOperacion { get; set; }

        CFDISubTipoEnum SubTipoComprobante { get; set; }

        MovimientoBancarioFormatoImpreso FormatoImpreso { get; set; }

        /// <summary>
        /// obtener o establecer objetos del directorio
        /// </summary>
        BindingList<ItemSelectedModel> ObjetosDirectorio { get; set; }

        BindingList<ItemSelectedModel> ObjetosComprobante { get; set; }

        BindingList<ItemSelectedModel> ObjetosRelacion { get; set; }

        BindingList<ItemSelectedModel> ObjetosRelacionSelected { get; }

        /// <summary>
        /// obenter listado de objetos del directorio seleccionados
        /// </summary>
        BindingList<ItemSelectedModel> DirectorioSelected { get; }
    }
}
