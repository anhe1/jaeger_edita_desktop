﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    public enum MovimientoBancarioFormatoImpreso {
        [Description("Movimiento")]
        Movimiento = 0,
        [Description("Cheque")]
        Cheque = 1,
        [Description("Recibo de Cobro")]
        ReciboCobro = 2,
        [Description("Recibo de Pago")]
        ReciboPago = 3,
        [Description("Recibo de Comisión")]
        ReciboComision = 4
    }
}
