﻿using System.Xml.Serialization;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// efecto del comprobante fiscal
    /// </summary>
    public enum TipoCFDIEnum {
        [XmlEnum("I")]
        Ingreso,
        [XmlEnum("E")]
        Egreso,
        [XmlEnum("T")]
        Traslado,
        [XmlEnum("N")]
        Nomina,
        [XmlEnum("P")]
        Pagos
    }
}
