﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// enumeracion de relacion de comprobantes
    /// </summary>
    public enum MovimientoBancarioRelacionComprobante {
        [Description("Emisor")]
        Emisor = 1,
        [Description("Receptor")]
        Receptor = 2,
        [Description("Vendedor")]
        Vemdedor = 3
    }
}
