﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// No Definido-Transferencia entre cuentas-Transferencia-Emision Cheque-Pago a Proveedor
    /// </summary>
    public enum BancoTipoOperacionEnum {
        /// <summary>
        /// No definido
        /// </summary>
        [Description("No definido")]
        NoDefinido = 0,
        /// <summary>
        /// Transferencia entre cuentas
        /// </summary>
        [Description("Transferencia entre cuentas")]
        TransferenciaEntreCuentas = 1,
        /// <summary>
        /// Transferencia
        /// </summary>
        [Description("Transferencia")]
        Transferencia = 2,
        /// <summary>
        /// Emision de Cheque
        /// </summary>
        [Description("Emisión de cheque")]
        Cheque = 3,
        /// <summary>
        /// Cobro de cliente
        /// </summary>
        [Description("Cobro Cliente")]
        ReciboCobro = 4,
        /// <summary>
        /// Pago a proveedor
        /// </summary>
        [Description("Pago Proveedor")]
        ReciboPago = 5,
        /// <summary>
        /// Otro
        /// </summary>
        [Description("Otro")]
        Otro = 99
    }
}
