﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// Ingreso = Deposito, Egreso = Retiro
    /// </summary>
    public enum MovimientoBancarioEfectoEnum {
        [Description("Depósito")]
        Ingreso = 1,
        [Description("Retiro")]
        Egreso = 2
    }
}
