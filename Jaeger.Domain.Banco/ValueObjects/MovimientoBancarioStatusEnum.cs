﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// 
    /// </summary>
    public enum MovimientoBancarioStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Tránsito")]
        Transito = 1,
        [Description("Aplicado")]
        Aplicado = 2,
        [Description("Auditado")]
        Auditado = 3
    }
}
