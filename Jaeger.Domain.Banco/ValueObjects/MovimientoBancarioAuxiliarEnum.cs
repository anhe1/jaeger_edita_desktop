﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// enumeracion de tipos de auxiliares
    /// </summary>
    public enum MovimientoBancarioAuxiliarEnum {
        /// <summary>
        /// comprobante fiscal digital por internet
        /// </summary>
        [Description("CFDI")]
        CFDI,
        /// <summary>
        /// comprobante remision digital
        /// </summary>
        [Description("CRD")]
        CRD
    }
}
