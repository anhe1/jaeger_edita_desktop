﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    public enum MovimientoBancarioTipoRelacionEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Sustitución de movimiento previo")]
        Sustitucion = 1
    }
}
