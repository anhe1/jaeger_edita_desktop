﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    /// <summary>
    /// tipos de comprobantes relacionados al movimiento bancario
    /// </summary>
    public enum MovimientoBancarioTipoComprobanteEnum {
        [Description("No Aplica")]
        NA = 0,
        [Description("CFD")]
        CFD = 1,
        [Description("CFDI")]
        CFDI = 2,
        [Description("Remisión")]
        Remision = 3
    }
}
