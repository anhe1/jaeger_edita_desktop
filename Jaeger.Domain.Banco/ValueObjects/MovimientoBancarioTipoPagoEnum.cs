﻿using System.ComponentModel;

namespace Jaeger.Domain.Banco.ValueObjects {
    public enum MovimientoBancarioTipoPagoEnum {
        [Description("Efectivo")]
        Efectivo = 01,
        [Description("Cheque")]
        Cheque = 02,
        [Description("Transferencia")]
        Transferencia = 03,
        [Description("Depósito")]
        Deposito = 99
    }
}
