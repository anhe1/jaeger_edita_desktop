﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Certificado.Entities;

namespace Jaeger.DataAccess.Certificado.Repositories {
    /// <summary>
    /// repositorio de certificados
    /// </summary>
    public class SqlSugarCertificadoRepository : MySqlSugarContext<CertificadoModel>, Domain.Certificado.Contracts.ISqlCertificadoRepository {
        public SqlSugarCertificadoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public int Save(Domain.Certificado.Contracts.ICertificado model) {
            var sqlCommand = @"INSERT INTO _ctlcer (_ctlcer_id, _ctlcer_a, _ctlcer_usr_n, _ctlcer_rfc, _ctlcer_serie, _ctlcer_subjectname, _ctlcer_issuerName, _ctlcer_cerb64, _ctlcer_keyb64, _ctlcer_psw64, _ctlcer_notbefore, _ctlcer_notafter, _ctlcer_fn, _ctlcer_filecer, _ctlcer_filekey)
                                            VALUES (@CTLCER_ID, @CTLCER_A, @CTLCER_USR_N, @CTLCER_RFC, @CTLCER_SERIE, @CTLCER_SUBJECTNAME, @CTLCER_ISSUERNAME, @CTLCER_CERB64, @CTLCER_KEYB64, @CTLCER_PSW64, @CTLCER_NOTBEFORE, @CTLCER_NOTAFTER, @CTLCER_FN, @CTLCER_FILECER, @CTLCER_FILEKEY) ON DUPLICATE KEY UPDATE
                _ctlcer_id = @CTLCER_ID, 
                _ctlcer_a = @CTLCER_A, 
                _ctlcer_usr_n = @CTLCER_USR_N, 
                _ctlcer_rfc = @CTLCER_RFC, 
                _ctlcer_serie = @CTLCER_SERIE, 
                _ctlcer_subjectname = @CTLCER_SUBJECTNAME, 
                _ctlcer_issuername = @CTLCER_ISSUERNAME, 
                _ctlcer_cerb64 = @CTLCER_CERB64, 
                _ctlcer_keyb64 = @CTLCER_KEYB64, 
                _ctlcer_psw64 = @CTLCER_PSW64, 
                _ctlcer_notbefore = @CTLCER_NOTBEFORE, 
                _ctlcer_notafter = @CTLCER_NOTAFTER, 
                _ctlcer_fn = @CTLCER_FN, 
                _ctlcer_filecer = @CTLCER_FILECER, 
                _ctlcer_filekey = @CTLCER_FILEKEY";

            var Parameters = new List<SugarParameter>() {
                new SugarParameter("@CTLCER_ID", model.Id),
                new SugarParameter("@CTLCER_A", model.Activo),
                new SugarParameter("@CTLCER_USR_N", model.Creo),
                new SugarParameter("@CTLCER_RFC", model.RFC),
                new SugarParameter("@CTLCER_SERIE", model.Serie),
                new SugarParameter("@CTLCER_SUBJECTNAME", model.RazonSocial),
                new SugarParameter("@CTLCER_ISSUERNAME", model.Emisor),
                new SugarParameter("@CTLCER_CERB64", model.CerB64),
                new SugarParameter("@CTLCER_KEYB64", model.KeyB64),
                new SugarParameter("@CTLCER_PSW64", model.KeyPassB64),
                new SugarParameter("@CTLCER_NOTBEFORE", model.NotBefore),
                new SugarParameter("@CTLCER_NOTAFTER", model.NotAfter),
                new SugarParameter("@CTLCER_FN", model.FechaNuevo),
                new SugarParameter("@CTLCER_FILECER", System.Convert.ToBase64String(model.CerFile)),
                new SugarParameter("@CTLCER_FILEKEY", System.Convert.ToBase64String(model.KeyFile)),
            };
            return this.ExecuteTransaction(sqlCommand, Parameters);
        }

        public CertificadoModel Salveable(CertificadoModel model) {
            model.Creo = this.User;
            model.FechaNuevo = System.DateTime.Now;
            this.Save(model);
            return model;
        }

        /// <summary>
        /// obtener certificado activo en el sistema
        /// </summary>
        public CertificadoModel GetCertificado() {
            var response = this.Db.Queryable<CertificadoModel>().Where(it => it.Activo == true).OrderBy(it => it.Id, OrderByType.Desc).Single();
            int startIndex = response.CerB64.IndexOf("-----BEGIN CERTIFICATE-----");
            int endIndex = response.CerB64.IndexOf("-----END CERTIFICATE-----");
            string cerB64 = response.CerB64.Substring(startIndex + 27, endIndex - startIndex - 27).Trim();
            // quitamos etiquetas
            string keyB64 = response.KeyB64;
            keyB64 = keyB64.Replace("-----BEGIN PRIVATE KEY-----", "");
            keyB64 = keyB64.Replace("-----END PRIVATE KEY-----", "");
            keyB64 = keyB64.Replace("-----BEGIN RSA PRIVATE KEY-----", "");
            keyB64 = keyB64.Replace("-----END RSA PRIVATE KEY-----", "");

            response.CerB64 = cerB64;
            response.KeyB64 = keyB64;
            return response;
        }
    }
}
