﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.PT {
    public class RemisionadoForm : Jaeger.UI.Almacen.PT.Forms.RemisionadoForm {
        protected internal RadMenuItem PorPartida = new RadMenuItem { Text = "Por Partidas", Name = "tadm_gcli_remxpartida" };

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = false;
            this.Load += RemisionesCatalogoForm_Load;
        }

        private void RemisionesCatalogoForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.Almacen.PT.Services.RemisionadoService();
            this.TRemision.Herramientas.Items.Add(this.PorPartida);
            this.PorPartida.Click += this.PorPartida_Click;
        }

        private void PorPartida_Click(object sender, EventArgs e) {
            //var porPartida = new Forms.CP.RemisionadoPartidaForm(new UIMenuElement()) { MdiParent = this.ParentForm, Text = this.PorPartida.Text };
            //porPartida.Show();
        }
    }
}
