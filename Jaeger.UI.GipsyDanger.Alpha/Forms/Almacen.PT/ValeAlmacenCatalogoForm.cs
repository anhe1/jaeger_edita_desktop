﻿using System;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.PT {
    public class ValeAlmacenCatalogoForm : UI.Almacen.PT.Forms.ValeAlmacenCatalogoForm {
        public ValeAlmacenCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ValeAlmacenCatalogoForm_Load;
        }

        private void ValeAlmacenCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new ValeAlmacenService();
            this.TVale.Nuevo.Enabled = true;
        }

        public override void Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new ValeAlmacenForm(this.Service) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        //public override void Consultar() {
        //    var d0 = 
        //    this.vales = this.Service.GetList<ValeAlmacenDetailModel>(this.TVale.GetEjercicio(), this.TVale.GetPeriodo());
        //}
    }
}
