﻿namespace Jaeger.UI.Forms.Almacen.MP {
    public class ValesSalidaForm : UI.Almacen.MP.Forms.ValesAlmacenCatalogoForm {
        public ValesSalidaForm(): base(Domain.Almacen.MP.ValueObjects.MovimientoTipoEnum.Salida) {
            this.Text = "Almacén MP: Salidas";
        }
    }
}
