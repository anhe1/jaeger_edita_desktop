﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class ValesEntradaForm : UI.Almacen.MP.Forms.ValesAlmacenCatalogoForm {
        public ValesEntradaForm(UIMenuElement menuElement) : base(Domain.Almacen.MP.ValueObjects.MovimientoTipoEnum.Entrada) {
            this.Text = "Almacén MP: Entradas";
        }
    }
}
