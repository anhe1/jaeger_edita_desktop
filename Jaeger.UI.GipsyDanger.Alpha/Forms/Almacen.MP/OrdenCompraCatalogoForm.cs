﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Almacen.MP {
    /// <summary>
    ///  catalogo de ordenes de compra sin costos
    /// </summary>
    public class OrdenCompraCatalogoForm : UI.Adquisiciones.Forms.OrdenCompraBaseForm {
        public OrdenCompraCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += OrdenCompraCatalogoSForm_Load;
        }

        private void OrdenCompraCatalogoSForm_Load(object sender, EventArgs e) {
            this.GOrdenCompra.ShowHerramientas = false;
            this.GOrdenCompra.ShowNuevo = false;
            this.GOrdenCompra.Editar.Text = "Ver";
            this.GOrdenCompra.GridData.AllowEditRow = false;

            this.GOrdenCompra.menuContextual.Items.Remove(this.GOrdenCompra.ContextDuplicar);
        }
    }
}
