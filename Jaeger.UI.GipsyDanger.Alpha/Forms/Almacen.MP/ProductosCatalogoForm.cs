﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Almacen.MP {
    public class ProductosCatalogoForm : UI.Almacen.MP.Forms.ProductoCatalogoForm {
        protected internal UIMenuElement menuElement;
        
        public ProductosCatalogoForm(UIMenuElement menuElement) : base() {
            this.menuElement = new UIMenuElement();
            this.Load += this.ProductosCatalogoForm_Load;
        }

        private void ProductosCatalogoForm_Load(object sender, System.EventArgs e) {
            var action = new UIAction(this.menuElement.Permisos);
            this.TProducto.Nuevo.Enabled = action.Agregar;
            this.TProducto.Editar.Enabled = action.Editar;
            this.gProductos.AllowEditRow = action.Editar;
            this.gModelos.AllowEditRow = action.Editar;
            this.TModelo.Autorizar.Enabled = action.Autorizar;
        }
    }
}
