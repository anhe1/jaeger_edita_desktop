﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.UI.Localization;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Kaiju.Services;
using Jaeger.Aplication.Empresa;
using Jaeger.Aplication.Kaiju.Contracts;

namespace Jaeger.UI.Forms {
    public partial class MainRibbonForm : RadRibbonForm {
        #region declaraciones
        private BackgroundWorker _DescargarLogo;
        #endregion

        public MainRibbonForm() {
            InitializeComponent();
        }

        private void MainRibbonForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.Change_Theme(this.office2010BlueTheme1.ThemeName);
            RadGridLocalizationProvider.CurrentProvider = new LocalizationProviderCustomGrid();
            RadMessageLocalizationProvider.CurrentProvider = new LocalizationProviderCustomerMessageBox();
            this._DescargarLogo = new BackgroundWorker();
            this._DescargarLogo.DoWork += DescargarLogo_DoWork;
            this._DescargarLogo.RunWorkerAsync();

            using (var espera = new Common.Forms.Waiting1Form(this.GetMenus)) {
                espera.Text = ConfigService.EsperarServidor;
                espera.ShowDialog(this);
            }

            if (ConfigService.Synapsis.Empresa.RFC == "XAXX010101000") {
                this.Testing();
            }

            this.MenuRibbonBar.SetDefaultTab();
            this.Text = ConfigService.Titulo();
            //this.BEstado.Items.Add(UIMenuRibbon.BEmpresa(ConfigService.Synapsis.Empresa.RFC, ConfigService.Piloto.Clave, Application.ProductVersion.ToString()));
            this.RibbonBar.Visible = true;
        }

        #region metodos privados
        private void Menu_UI_AplicarTema_Click(object sender, EventArgs e) {
            this.Change_Theme(((RadButtonElement)sender).Tag.ToString());
        }

        private void Activar_Form(Type type, UIMenuElement sender) {
            try {
                Form form = (Form)Activator.CreateInstance(type, sender);
                if (form.WindowState == FormWindowState.Maximized) {
                    form.MdiParent = this;
                    form.WindowState = FormWindowState.Maximized;
                    form.Show();
                    this.RadDock.ActivateMdiChild(form);
                } else {
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog(this);
                }
            } catch (Exception ex) {
                RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        /// <summary>
        /// cambiar tema
        /// </summary>
        private void Change_Theme(string nameTheme) {
            ThemeResolutionService.ApplicationThemeName = nameTheme;
        }

        private void Menu_Main_Click(object sender, EventArgs e) {
            var item = (RadItem)sender;
            var menuElement = ConfigService.GeMenuElement(item.Name);

            if (menuElement != null) {
                var localAssembly = new object() as Assembly;
                if (menuElement.Assembly == "Jaeger.UI")
                    localAssembly = this.GetAssembly("Jaeger.UI.GipsyDanger");
                else
                    localAssembly = this.GetAssembly(menuElement.Assembly);

                if (localAssembly != null) {
                    try {
                        Type type = localAssembly.GetType(string.Format("{0}.{1}", menuElement.Assembly, menuElement.Form), true);
                        this.Activar_Form(type, menuElement);
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                } else {
                    RadMessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private Assembly GetAssembly(string assembly) {
            try {
                return Assembly.Load(assembly);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void GetMenus() {
            IProfileToService service = new ProfileToService();
            service.CreateProfile();
            service.CreateMenus();

            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            MenuPermissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);
            
            this.MenuRibbonBar.Refresh();
        }

        private void DescargarLogo_DoWork(object sender, DoWorkEventArgs e) {
            var d0 = new ConfiguracionService();
            IEmpresaService d1 = new EmpresaService();
            var d2 = d1.GetConfiguracion(ConfiguracionKeyEnum.cfdi);

            if (d2 != null) {
                Console.WriteLine("Si");
                var d3 = Domain.Empresa.Entities.ComprobanteConfig.Json(d2.Data);
                if (d3 != null) {
                    ConfigService.Synapsis.Empresa.RegimenFiscal = d3.General.RegimenFiscal;
                    ConfigService.Synapsis.Empresa.RegistroPatronal = d3.General.RegistroPatronal;
                    ConfigService.Synapsis.Empresa.DomicilioFiscal = d3.DomicilioFiscal;
                    Console.WriteLine(ConfigService.Synapsis.Empresa.RegimenFiscal);
                }
            }
            string logo = string.Format("https://s3.amazonaws.com/{0}/Conf/logo-{1}.png", ConfigService.Synapsis.Empresa.RFC.ToLower(), ConfigService.Synapsis.Empresa.Clave);
            string archivo = ManagerPathService.JaegerPath(PathsEnum.Media, string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            Jaeger.Util.Services.FileService.DownloadFile(logo, archivo);
        }
        #endregion

        private void Testing() {
            ConfigService.Synapsis.Empresa.RegimenFiscal = "601";
            ConfigService.Synapsis.Empresa.RazonSocial = "Impresores Profesionales SA de CV";
            ConfigService.Synapsis.Empresa.RFC = "IPR981125PN9";
            ConfigService.Synapsis.Empresa.DomicilioFiscal.CodigoPostal = "08100";
        }
    }
}
