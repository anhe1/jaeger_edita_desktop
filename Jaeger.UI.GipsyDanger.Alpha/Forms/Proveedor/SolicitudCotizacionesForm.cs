﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Proveedor {
    public class SolicitudCotizacionesForm : Adquisiciones.Forms.SolicitudCotizacionesBaseForm {
        public SolicitudCotizacionesForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.SolicitudCotizacionesForm_Load;
        }

        private void SolicitudCotizacionesForm_Load(object sender, EventArgs e) {

        }

        public override void Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new SolicitudCotizacionForm() { MdiParent = this.ParentForm };
            nuevo.Show();
        }
    }
}
