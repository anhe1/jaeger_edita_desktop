﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Proveedor {
    public class OrdenProveedorCatalogoForm : UI.Adquisiciones.Forms.OrdenCompraCatalogoForm {
        protected internal RadMenuItem SolicitudCotizacion = new RadMenuItem { Text = "Solicitud de cotización", Name = "adm_gprv_solcotiza" };

        public OrdenProveedorCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Orden de Compra";
            this.Load += this.OrdenProveedorCatalogo_Load;
        }

        private void OrdenProveedorCatalogo_Load(object sender, EventArgs e) {
            this.GOrdenCompra.Herramientas.Items.Add(this.SolicitudCotizacion);
            this.SolicitudCotizacion.Click += this.SolicitudCotizacion_Click;

            this.SolicitudCotizacion.Tag = ConfigService.GeMenuElement(this.SolicitudCotizacion.Name);
            this.SolicitudCotizacion.Enabled = ((UIMenuElement)this.SolicitudCotizacion.Tag).IsEnable;
            this.Solicitud.Enabled = this.SolicitudCotizacion.Enabled;

        }

        public override void Nuevo_Click(object sender, EventArgs e) {
            var d0 = new OrdenCompraForm() { MdiParent = this.ParentForm };
            d0.Show();
        }

        /// <summary>
        /// formulario de solicitudes de cotizaciones
        /// </summary>
        public virtual void SolicitudCotizacion_Click(object sender, EventArgs e) {
            var element = (UIMenuElement)this.SolicitudCotizacion.Tag;
            var catalogo = new SolicitudCotizacionesForm(element) { MdiParent = this.ParentForm };
            catalogo.Show();
        }

        public override void TSolicitud_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new SolicitudCotizacionForm() { MdiParent = this.ParentForm };
            nuevo.Show();
        }
    }
}
