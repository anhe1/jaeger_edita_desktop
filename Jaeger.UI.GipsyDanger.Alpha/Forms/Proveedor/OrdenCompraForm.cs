﻿namespace Jaeger.UI.Forms.Proveedor {
    public class OrdenCompraForm : Adquisiciones.Forms.OrdenCompraForm {
        public OrdenCompraForm() : base() {
            this.TOrden.general.Receptor.Service = new Aplication.Contribuyentes.DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor);
        }
    }
}
