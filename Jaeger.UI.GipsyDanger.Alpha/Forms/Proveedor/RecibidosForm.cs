﻿using System;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Comprobante.Forms;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Proveedor {
    public class RecibidosForm : ComprobantesFiscalesRecibidoForm {
        protected internal GridViewTemplate gridPagos = new GridViewTemplate();
        protected internal RadMenuItem MenuContextual_CrearPago = new RadMenuItem { Text = "Registrar Pago", ToolTipText = "Crear movimiento Bancario" };
        protected internal RadMenuItem PorPagar = new RadMenuItem { Text = "Por pagar", ToolTipText = "Comprobantes por pagar" };
        protected Aplication.Banco.IBancoMovientosSearchService bancoService;
        protected internal UIMenuElement menuElement;
        public RecibidosForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Load += this.RecibidosCatalogoForm_Load;
        }

        private void RecibidosCatalogoForm_Load(object sender, EventArgs e) {
            this.TComprobante.Editar.Text = "Ver";
            this.TComprobante.Cancelar.Text = "Rechazado";
            this.TComprobante.ShowHerramientas = true;
            this.TComprobante.Herramientas.Items.Add(this.PorPagar);
            this.menuContextual.Items.Add(this.MenuContextual_CrearPago);
            this.MenuContextual_CrearPago.Click += this.MenuContextual_CrearPago_Click;
            this.bancoService = new Aplication.Banco.BancoMovientosSearchService();
            this.TempletePagos();
            gridPagos.HierarchyDataProvider = new GridViewEventDataProvider(gridPagos);
            this.gridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
            this.PorPagar.Click += TComprobante_PorPagar_Click;
        }

        private void TComprobante_PorPagar_Click(object sender, EventArgs e) {
            var d0 = new RecibidosPorPagarForm(this.menuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }

        #region barra de herramientsa
        public override void TComprobante_Cancelar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, "¿Esta seguro de rechazar el comprobante seleccionado?", "Atención", System.Windows.Forms.MessageBoxButtons.YesNo, RadMessageIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes) {

            }
        }

        public override void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                var _visualizar = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                _visualizar.Show();
            }
        }
        #endregion

        #region opciones del menu contextual
        public virtual void MenuContextual_CrearPago_Click(object sender, EventArgs e) {
            var _seleccion = this.GetSelection();
            if (_seleccion != null) {
                if (_seleccion.Count > 0) {
                    var _si = _seleccion.Where(it => it.Status == "Cancelado" | it.Status == "Importado" | it.Status == "Pagado" | it.Estado == "Cancelado");
                    if (_si.Count() > 0) {
                        RadMessageBox.Show(this, "No es posible crear el registro de pago. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }

            var _movimiento = new MovimientoBancarioDetailModel();
            foreach (var item in _seleccion) {
                if (item.Estado != "Cancelado") {
                    _movimiento.Comprobantes.Add(new MovimientoBancarioComprobanteDetailModel {
                        Activo = true,
                        ClaveFormaPago = item.ClaveFormaPago,
                        ClaveMetodoPago = item.ClaveMetodoPago,
                        ClaveMoneda = item.ClaveMoneda,
                        EmisorNombre = item.EmisorNombre,
                        EmisorRFC = item.EmisorRFC,
                        Estado = item.Estado,
                        FechaEmision = item.FechaEmision,
                        FechaNuevo = item.FechaNuevo,
                        IdComprobante = item.Id,
                        IdDocumento = item.IdDocumento,
                        NumParcialidad = item.NumParcialidad,
                        ReceptorNombre = item.ReceptorNombre,
                        ReceptorRFC = item.ReceptorRFC,
                        Serie = item.Serie,
                        Version = item.Version,
                        Total = item.Total,
                        SubTipoComprobante = (CFDISubTipoEnum)item.IdSubTipo,
                        TipoComprobanteText = item.TipoComprobanteText,
                        Folio = item.Folio
                    });
                }
            }
            if (_movimiento.Comprobantes.Count == 0) {
                RadMessageBox.Show(this, "No es posible crear el registro de pago. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
            } else {
                var _nuevoMovimiento = new UI.Banco.Forms.MovimientoBancarioForm(_movimiento, new Aplication.Banco.BancoService());
                _nuevoMovimiento.ShowDialog(this);
            }
        }
        #endregion

        #region acciones del grid
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.gridPagos.Caption) {
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    var tabla = this.bancoService.GetSearchMovimientos(rowView.Id);
                    foreach (var item in tabla) {
                        var newRow = e.Template.Rows.NewRow();
                        newRow.Cells["Identificador"].Value = item.Identificador;
                        newRow.Cells["Status"].Value = item.Status;
                        newRow.Cells["FechaEmision"].Value = item.FechaEmision;
                        newRow.Cells["BeneficiarioT"].Value = item.BeneficiarioT;
                        newRow.Cells["Concepto"].Value = item.Concepto;
                        newRow.Cells["IdDocumento"].Value = item.IdDocumento;
                        newRow.Cells["NumAutorizacion"].Value = item.NumAutorizacion;
                        newRow.Cells["FechaAplicacion"].Value = item.FechaAplicacion;
                        newRow.Cells["Cargo"].Value = item.Cargo1;
                        newRow.Cells["Abono"].Value = item.Abono1;
                        newRow.Cells["Creo"].Value = item.Creo;
                        e.SourceCollection.Add(newRow);
                    }
                }
            }
        }
        private void TempletePagos() {
            var tableViewDefinition1 = new TableViewDefinition();
            gridPagos.Caption = "Pagos";
            var _identificador = new GridViewTextBoxColumn { DataType = typeof(string), FieldName = "Identificador", HeaderText = "No. Ident.", Name = "Identificador", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 70 };
            var _status = new GridViewTextBoxColumn { FieldName = "Estado", HeaderText = "Status", Name = "Status", Width = 65 };
            var _fechaEmision = new GridViewTextBoxColumn { DataType = typeof(DateTime), FieldName = "FechaEmision", FormatString = "{0:dd-MMM-yy}", HeaderText = "Fec. Emisión", Name = "FechaEmision", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 75 };
            var _concepto = new GridViewTextBoxColumn { FieldName = "Concepto", HeaderText = "Concepto", Name = "Concepto", Width = 165 };
            var _beneficiario = new GridViewTextBoxColumn { FieldName = "BeneficiarioT", HeaderText = "Beneficiario", Name = "BeneficiarioT", Width = 175 };
            var _idDocumento = new GridViewTextBoxColumn { FieldName = "IdDocumento", HeaderText = "Id Documento (UUID)", Name = "IdDocumento", Width = 195 };
            var _numAutorizacion = new GridViewTextBoxColumn { FieldName = "NumAutorizacion", HeaderText = "Núm. Autorización", Name = "NumAutorizacion", Width = 85 };
            var _fechaAplicacion = new GridViewTextBoxColumn { DataType = typeof(DateTime), FieldName = "FechaAplicacion", FormatString = "{0:dd-MMM-yy}", HeaderText = "Fec. Aplicación", Name = "FechaAplicacion", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 75 };
            var _cargo = new GridViewTextBoxColumn { DataType = typeof(decimal), FieldName = "Cargo", FormatString = "{0:n}", HeaderText = "Cargo", Name = "Cargo", TextAlignment = System.Drawing.ContentAlignment.MiddleRight, Width = 75 };
            var _abono = new GridViewTextBoxColumn { DataType = typeof(decimal), FieldName = "Abono", FormatString = "{0:n}", HeaderText = "Abono", Name = "Abono", TextAlignment = System.Drawing.ContentAlignment.MiddleRight, Width = 75 };
            var _creo = new GridViewTextBoxColumn { FieldName = "Creo", HeaderText = "Creó", Name = "Creo" };

            gridPagos.Columns.AddRange(new GridViewDataColumn[] { _identificador, _status, _fechaEmision, _beneficiario, _concepto, _idDocumento, _numAutorizacion, _fechaAplicacion, _cargo, _abono, _creo });

            gridPagos.ViewDefinition = tableViewDefinition1;
            this.gridData.MasterTemplate.Templates.Add(gridPagos);
            this.gridPagos.TelerikTemplateCommon();
        }
        #endregion
    }
}
