﻿namespace Jaeger.UI.Forms.Proveedor {
    public class SolicitudCotizacionForm : Adquisiciones.Forms.SolicitudCotizacionForm {
        public SolicitudCotizacionForm() : base() {
            this.TSolicitud.Receptor.Service = new Aplication.Contribuyentes.DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Proveedor);
        }
    }
}
