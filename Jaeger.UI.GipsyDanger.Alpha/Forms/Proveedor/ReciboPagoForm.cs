﻿using System;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.UI.Forms.Proveedor {
    public class ReciboPagoForm : UI.Banco.Forms.MovimientoBancarioForm {

        public ReciboPagoForm(IBancoService service) : base(service) {
            this.Load += this.ReciboPagoForm_Load;
        }

        public ReciboPagoForm(MovimientoBancarioDetailModel movimiento, IBancoService service) : base(movimiento, service) {
            this.Load += this.ReciboPagoForm_Load;
        }

        private void ReciboPagoForm_Load(object sender, EventArgs e) {

        }
    }
}
