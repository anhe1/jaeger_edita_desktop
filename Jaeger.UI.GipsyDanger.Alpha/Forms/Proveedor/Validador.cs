﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Proveedor {
    public class Validador : Comprobante.Forms.Validar.ComprobanteFiscalForm {
        public Validador(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Proveedor: Validar Comprobantes";
        }
    }
}
