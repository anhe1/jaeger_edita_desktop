﻿using System;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Proveedor {
    public class ReciboPagoCatalogoForm : UI.Banco.Forms.MovimientosForm {
        private UIAction _permisos;

        public ReciboPagoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Egreso) {
            this.Text = "Proveedores: Pagos";
            _permisos = new UIAction(menuElement.Permisos);
            this.Load += ReciboPagoCatalogoForm_Load;
        }

        private void ReciboPagoCatalogoForm_Load(object sender, EventArgs e) {
            this.TMovimiento.Nuevo.Click += this.Nuevo_Click;
            this.TMovimiento.Nuevo.Enabled = this._permisos.Agregar;
            this.TMovimiento.Cancelar.Enabled = this._permisos.Cancelar;
            this.TMovimiento.Editar.Enabled = this._permisos.Editar;
            this.Movimientos.Cancelar = this._permisos.Cancelar;
            this.Movimientos.Auditar = this._permisos.Autorizar;
            this.Movimientos.GridData.AllowEditRow = this._permisos.Status;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            var recibo = new ReciboPagoForm(this.service);
            recibo.ShowDialog(this);
        }
    }
}
