﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Proveedor {
    public class RecibidosPorPagarForm : RecibidosForm {
        public RecibidosPorPagarForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Proveedores facturación: Por Pagar";
            this.Load += RecibidosPorPagarForm_Load;
        }

        private void RecibidosPorPagarForm_Load(object sender, System.EventArgs e) {
            this.TComprobante.ShowPeriodo = false;
        }

        public override void Consultar() {
            this.comprobanteFiscalSingles = this.service.GetListByStatus(this.TComprobante.GetEjercicio(), -1, "PorPagar");
        }
    }
}
