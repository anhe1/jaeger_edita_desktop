﻿
namespace Jaeger.UI.Forms.CP {
    partial class OrdenProduccionBuscarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.NumOrden = new Telerik.WinControls.UI.RadTextBox();
            this.lblNumOrden = new Telerik.WinControls.UI.RadLabel();
            this.ordenProduccion = new Jaeger.UI.CP.Forms.Produccion.OrdenProduccionSearchControl();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.lblPedido = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.Descripcion);
            this.groupBox1.Controls.Add(this.lblPedido);
            this.groupBox1.Controls.Add(this.Buscar);
            this.groupBox1.Controls.Add(this.NumOrden);
            this.groupBox1.Controls.Add(this.lblNumOrden);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(697, 40);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // NumOrden
            // 
            this.NumOrden.Location = new System.Drawing.Point(96, 10);
            this.NumOrden.Name = "NumOrden";
            this.NumOrden.Size = new System.Drawing.Size(72, 20);
            this.NumOrden.TabIndex = 2;
            // 
            // lblNumOrden
            // 
            this.lblNumOrden.Location = new System.Drawing.Point(3, 11);
            this.lblNumOrden.Name = "lblNumOrden";
            this.lblNumOrden.Size = new System.Drawing.Size(86, 18);
            this.lblNumOrden.TabIndex = 1;
            this.lblNumOrden.Text = "Núm. de Orden:";
            // 
            // ordenProduccion
            // 
            this.ordenProduccion.Descripcion = null;
            this.ordenProduccion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ordenProduccion.Location = new System.Drawing.Point(0, 40);
            this.ordenProduccion.Name = "ordenProduccion";
            this.ordenProduccion.NumeroOrden = null;
            this.ordenProduccion.Size = new System.Drawing.Size(697, 220);
            this.ordenProduccion.TabIndex = 1;
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.Location = new System.Drawing.Point(577, 10);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(110, 20);
            this.Buscar.TabIndex = 9;
            this.Buscar.Text = "Buscar";
            // 
            // Descripcion
            // 
            this.Descripcion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Descripcion.Location = new System.Drawing.Point(227, 10);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(344, 20);
            this.Descripcion.TabIndex = 11;
            // 
            // lblPedido
            // 
            this.lblPedido.Location = new System.Drawing.Point(177, 11);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(44, 18);
            this.lblPedido.TabIndex = 10;
            this.lblPedido.Text = "Pedido:";
            // 
            // OrdenProduccionBuscarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ordenProduccion);
            this.Controls.Add(this.groupBox1);
            this.Name = "OrdenProduccionBuscarControl";
            this.Size = new System.Drawing.Size(697, 260);
            this.Load += new System.EventHandler(this.OrdenProduccionBuscarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        private Telerik.WinControls.UI.RadLabel lblNumOrden;
        public Telerik.WinControls.UI.RadButton Buscar;
        public UI.CP.Forms.Produccion.OrdenProduccionSearchControl ordenProduccion;
        public Telerik.WinControls.UI.RadTextBox NumOrden;
        public Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadLabel lblPedido;
    }
}
