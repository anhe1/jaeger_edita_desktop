﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.UI.Forms.CP {
    public class RemisionadoForm : UI.CP.Forms.Almacen.PT.RemisionadoForm {
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService comprobanteFiscalService;
        protected internal RadMenuItem contextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
        protected internal RadMenuItem contextMenuReciboCobro = new RadMenuItem { Text = "Crear Recibo de Cobro" };
        protected internal RadMenuItem PorCobrar = new RadMenuItem { Text = "Por Cobrar", Name = "tadm_gcli_remxcobrar" };
        protected internal RadMenuItem PorPartida = new RadMenuItem { Text = "Por Partidas", Name = "tadm_gcli_remxpartida" };

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Load += RemisionesCatalogoForm_Load;
        }

        private void RemisionesCatalogoForm_Load(object sender, EventArgs e) {
            this.TRemision.menuContextual.Items.Add(contextMenuFacturar);
            this.TRemision.menuContextual.Items.Add(contextMenuReciboCobro);
            this.TRemision.Herramientas.Items.Add(this.PorCobrar);
            this.TRemision.Herramientas.Items.Add(this.PorPartida);

            this.comprobanteFiscalService = new Aplication.Comprobante.Services.ComprobantesFiscalesService();
            contextMenuFacturar.Click += this.contextMenuFacturar_Click;

            this.PorCobrar.Click += this.PorCobrar_Click;
            this.PorPartida.Click += this.PorPartida_Click;
        }

        private void PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new Forms.CP.RemisionadoPartidaForm(new UIMenuElement()) { MdiParent = this.ParentForm, Text = this.PorPartida.Text };
            porPartida.Show();
        }

        private void PorCobrar_Click(object sender, EventArgs e) {
            var porPartida = new Forms.CP.RemisionadoPorCobrarForm(new UIMenuElement()) { MdiParent = this.ParentForm, Text = this.PorPartida.Text };
            porPartida.Show();
        }

        private void contextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV33 = this.Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV33 != null) {
                var nuevo = new Clientes.Comprobante1FiscalForm(cfdiV33, CFDISubTipoEnum.Emitido, -1) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            this.Tag = null;
        }

        private void CrearFactura() {
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = this.comprobanteFiscalService.GetNew(CFDITipoComprobanteEnum.Ingreso);
                    foreach (var item in seleccion) {
                        var remision = this._Service.GetById(item.IdRemision);
                        foreach (var item1 in remision.Conceptos) {
                            var concepto = new Domain.Comprobante.Entities.ComprobanteConceptoDetailModel { Cantidad = item1.Cantidad, ValorUnitario = item1.Unitario, Descripcion = item1.Descripcion, NumPedido = item1.IdPedido, ClaveUnidad = "H87", ClaveProdServ = "82121511" };
                            concepto.Impuestos.Add(new Domain.Comprobante.Entities.ComprobanteConceptoImpuesto { Impuesto = ImpuestoEnum.IVA, TasaOCuota = new decimal(0.16), Tipo = TipoImpuestoEnum.Traslado, Base = concepto.Importe });
                            cfdiV33.Conceptos.Add(concepto);
                        }
                    }
                    this.Tag = cfdiV33;
                }
            }
        }
    }
}
