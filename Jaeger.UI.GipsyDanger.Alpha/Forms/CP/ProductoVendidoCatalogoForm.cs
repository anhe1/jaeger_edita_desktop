﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.CP {
    public class ProductoVendidoCatalogoForm : UI.CP.Forms.Ventas.ProductoVendidoCatalogoForm {
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesService comprobanteFiscalService;

        public ProductoVendidoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += ProductoVendidoCatalogoForm_Load;
        }

        private void ProductoVendidoCatalogoForm_Load(object sender, EventArgs e) {
            var contextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
            this.menuContextual.Items.Add(contextMenuFacturar);
            contextMenuFacturar.Click += new EventHandler(this.contextMenuFacturar_Click);
            this.comprobanteFiscalService = new Aplication.Comprobante.Services.ComprobantesFiscalesService();
            this.gridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            //this.radGridFacturacion.HierarchyDataProvider = new GridViewEventDataProvider(this.radGridFacturacion);
        }

        private void contextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV33 = this.Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV33 != null) {
                var nuevo = new UI.Comprobante.Forms.ComprobanteFiscalGeneralForm(cfdiV33, CFDISubTipoEnum.Emitido, -1) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            this.Tag = null;
        }

        #region acciones del grid de datos
        //private void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            //var rowview = e.ParentRow.DataBoundItem as PedPrdPedidoModelView;
            //if (e.Template.Caption == this.radGridFacturacion.Caption) {
            //    if (rowview != null) {
            //        var tabla = this.comprobanteFiscalService.GetList(rowview.IdPedPrd);
            //        foreach (var item in tabla) {
            //            var newRow = e.Template.Rows.NewRow();
            //            newRow.Cells["Status"].Value = item.Status;
            //            newRow.Cells["Folio"].Value = item.Folio;
            //            newRow.Cells["Serie"].Value = item.Serie;
            //            newRow.Cells["IdDocumento"].Value = item.IdDocumento;
            //            newRow.Cells["FechaEmision"].Value = item.FechaEmision;
            //            newRow.Cells["Cantidad"].Value = item.Cantidad;
            //            newRow.Cells["ClaveUnidad"].Value = item.ClaveUnidad;
            //            newRow.Cells["Unidad"].Value = item.Unidad;
            //            newRow.Cells["ClaveProducto"].Value = item.ClaveProdServ;
            //            newRow.Cells["Concepto"].Value = item.Descripcion;
            //            newRow.Cells["Unitario"].Value = item.ValorUnitario;
            //            newRow.Cells["SubTotal"].Value = item.Importe;
            //            e.SourceCollection.Add(newRow);
            //        }
            //    }
            //}
        //}
        #endregion

        private void CrearFactura() {
            if (this.gridData.CurrentRow != null) {
                var seleccion = new List<PedPrdPedidoModelView>();
                seleccion.AddRange(this.gridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as PedPrdPedidoModelView));
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = this.comprobanteFiscalService.GetNew(CFDITipoComprobanteEnum.Ingreso);
                    foreach (var item in seleccion) {
                        var concepto = new Domain.Comprobante.Entities.ComprobanteConceptoDetailModel { Cantidad = item.Cantidad, ValorUnitario = item.Unitario, Descripcion = item.Descripcion, NumPedido = item.IdPedPrd, ClaveUnidad = "H87", ClaveProdServ = "82121511" };
                        concepto.Impuestos.Add(new Domain.Comprobante.Entities.ComprobanteConceptoImpuesto { Impuesto = ImpuestoEnum.IVA, TasaOCuota = new decimal(0.16), Tipo = TipoImpuestoEnum.Traslado, Base = concepto.Importe });
                        cfdiV33.Conceptos.Add(concepto);
                    }
                    this.Tag = cfdiV33;
                }
            }
        }
    }
}
