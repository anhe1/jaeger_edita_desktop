﻿using System;
using System.Windows.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.CP {
    public partial class OrdenProduccionBuscarControl : UserControl {
        public OrdenProduccionBuscarControl() {
            InitializeComponent();
        }

        private void OrdenProduccionBuscarControl_Load(object sender, EventArgs e) {
            this.NumOrden.KeyPress += HelperTelerikExtension.TxbFolio_KeyPress;
        }
    }
}
