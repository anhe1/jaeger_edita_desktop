﻿using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Repositorio {
    public class RepositorioForm : UI.Repositorio.Forms.RepositorioForm {
        protected internal RadMenuItem TEnviar = new RadMenuItem { Text = "Enviar", Name = "TEnviar" };

        public RepositorioForm() : base() {
            this.Load += this.RepositorioForm_Load;
        }

        public RepositorioForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.RepositorioForm_Load;
        }

        private void RepositorioForm_Load(object sender, System.EventArgs e) {
        
        }
    }
}
