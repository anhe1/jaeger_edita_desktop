﻿using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    partial class MainRibbonForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRibbonForm));
            this.MenuRibbonBar = new Telerik.WinControls.UI.RadRibbonBar();
            this.TAdmon = new Telerik.WinControls.UI.RibbonTab();
            this.adm_grp_cliente = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gcli_expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gcli_ordcliente = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_remisionado = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_facturacion = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_cobranza = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcli_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gcli_bedocuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcli_bcredito = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcli_bresumen = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_tesoreria = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gtes_bancoctas = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gtes_banco = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gtes_bformapago = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gtes_bconceptos = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gtes_bbeneficiario = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gtes_bmovimiento = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_contable = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gcontable_catcuentas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gcontable_catrubros = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcontable_ctacontable = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcontable_polizas = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcontable_activos = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gcontable_configurar = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gcontable_cattipo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gcontable_creartablas = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_almacen = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_galmacen_mprima = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_galmmp_catalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_modelo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_reqcompra = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_ordcompra = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_vales = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_devolucion = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_existencia = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmmp_cunidad = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmacen_pterminado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_galmpt_catalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_entrada = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_salida = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_galmpt_existencia = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_grp_proveedor = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gprv_expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gprv_ordcompra = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_validador = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_facturacion = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_pagos = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gprv_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gprv_edocuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gprv_credito = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gprv_resumen = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_grp_nomina = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.nom_btn_empleado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.nom_bemp_expediente = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_bemp_contratos = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_sgrp_grupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.nom_gmov_periodo = new Telerik.WinControls.UI.RadButtonElement();
            this.nom_gmov_subgrupo = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.nom_gmov_vacacion = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gmov_faltas = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gmov_acumula = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gmov_aguinaldo = new Telerik.WinControls.UI.RadButtonElement();
            this.nom_gmovimiento_recibos = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tnom_gmovimiento_consulta = new Telerik.WinControls.UI.RadMenuItem();
            this.tnom_gmovimiento_resumen = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_grp_parametros = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.nom_grp_concepto = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.nom_gconcepto_catalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gparametros_tablas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.nom_gparametros_tisr = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gParametros_tsbe = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gparametros_tsm = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gParametros_tuma = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gparametros_config = new Telerik.WinControls.UI.RadButtonElement();
            this.TProd = new Telerik.WinControls.UI.RibbonTab();
            this.cpemp_grp_directorio = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpemp_gdir_directorio = new Telerik.WinControls.UI.RadButtonElement();
            this.cpemp_grp_producto = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cpemp_gprd_clasificacion = new Telerik.WinControls.UI.RadButtonElement();
            this.cpemp_gprd_catalogo = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_grp_cotizacion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpcot_gcot_productos = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpcot_bprd_producto = new Telerik.WinControls.UI.RadMenuItem();
            this.cpcot_bprd_subproducto = new Telerik.WinControls.UI.RadMenuItem();
            this.cpcot_gcot_presupuesto = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_cotizaciones = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_cliente = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_conf = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_grp_produccion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cppro_gpro_orden = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_bord_historial = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_bord_proceso = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_reqcompra = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_gpro_calendario = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_gpro_sgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cppro_sgrp_depto = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_sgrp_avance = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_sgrp_evaluacion = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcppro_beva_EProduccion = new Telerik.WinControls.UI.RadMenuItem();
            this.tcppro_beva_EPeriodo = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_remision = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_brem_Emitido = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_brem_Recibido = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_grp_ventas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpvnt_gven_vendedor = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_prodvend = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_group1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cpvnt_gven_pedidos = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_comision = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_ventcosto = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_remisionado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpvnt_brem_historial = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_brem_porcobrar = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_brem_rempart = new Telerik.WinControls.UI.RadMenuItem();
            this.TVentas = new Telerik.WinControls.UI.RibbonTab();
            this.cmr_grp_ventas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cmr_gventas_vededores = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gventas_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cmr_gventas_pedidos = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gventas_comision = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gventas_ccomision = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_grp_cp = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cmr_gcp_directorio = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gcp_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cmr_gcp_ordproduccion = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gcp_prodvendido = new Telerik.WinControls.UI.RadButtonElement();
            this.cmr_gcp_remisionado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cmr_gcp_bremisionado = new Telerik.WinControls.UI.RadMenuItem();
            this.cmr_gcp_rempartida = new Telerik.WinControls.UI.RadMenuItem();
            this.cmr_gcp_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cmr_gcp_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.cmr_gcp_resumen = new Telerik.WinControls.UI.RadButtonElement();
            this.TTools = new Telerik.WinControls.UI.RibbonTab();
            this.dsk_grp_config = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gconfig_empresa = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gconfig_avanzado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_usuario = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_menu = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_perfil = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_series = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_tools = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtools_repositorio = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_asistido = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_repositorio1 = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_backup = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_backcfdi = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_backdir = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validador = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_validar = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validarfc = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validacedula = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validaret = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_cancelado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_presuntos = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_certificado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_mantto = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_grp_retencion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tdks_gretencion_recep = new Telerik.WinControls.UI.RadButtonElement();
            this.tdks_gretencion_comprobante = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_theme = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtheme_2010Black = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Blue = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Silver = new Telerik.WinControls.UI.RadButtonElement();
            this.BEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.RadDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.RadContainer = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.windows8Theme1 = new Telerik.WinControls.Themes.Windows8Theme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.dsk_gconfig_emisor = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).BeginInit();
            this.RadDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuRibbonBar
            // 
            this.MenuRibbonBar.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.TAdmon,
            this.TProd,
            this.TVentas,
            this.TTools});
            // 
            // 
            // 
            this.MenuRibbonBar.ExitButton.Text = "Exit";
            this.MenuRibbonBar.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.MenuRibbonBar.Location = new System.Drawing.Point(0, 0);
            this.MenuRibbonBar.Name = "MenuRibbonBar";
            // 
            // 
            // 
            this.MenuRibbonBar.OptionsButton.Text = "Options";
            this.MenuRibbonBar.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.MenuRibbonBar.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.MenuRibbonBar.Size = new System.Drawing.Size(1551, 162);
            this.MenuRibbonBar.StartButtonImage = global::Jaeger.UI.Properties.Resources.logo_editaCP;
            this.MenuRibbonBar.TabIndex = 0;
            this.MenuRibbonBar.Text = "MainRibbonForm";
            this.MenuRibbonBar.Visible = false;
            // 
            // TAdmon
            // 
            this.TAdmon.IsSelected = false;
            this.TAdmon.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_grp_cliente,
            this.adm_grp_tesoreria,
            this.adm_grp_contable,
            this.adm_grp_almacen,
            this.adm_grp_proveedor,
            this.nom_grp_nomina});
            this.TAdmon.Name = "TAdmon";
            this.TAdmon.Text = "TAdministracion";
            this.TAdmon.UseMnemonic = false;
            // 
            // adm_grp_cliente
            // 
            this.adm_grp_cliente.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcli_expediente,
            this.adm_gcli_subgrupo,
            this.adm_gcli_cobranza,
            this.adm_gcli_reportes});
            this.adm_grp_cliente.Name = "adm_grp_cliente";
            this.adm_grp_cliente.Text = "adm_grp_cliente";
            // 
            // adm_gcli_expediente
            // 
            this.adm_gcli_expediente.Image = global::Jaeger.UI.Properties.Resources.people_30px;
            this.adm_gcli_expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcli_expediente.Name = "adm_gcli_expediente";
            this.adm_gcli_expediente.Text = "b.Expediente";
            this.adm_gcli_expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gcli_expediente.TextWrap = true;
            this.adm_gcli_expediente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_subgrupo
            // 
            this.adm_gcli_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcli_ordcliente,
            this.adm_gcli_remisionado,
            this.adm_gcli_facturacion});
            this.adm_gcli_subgrupo.MinSize = new System.Drawing.Size(22, 22);
            this.adm_gcli_subgrupo.Name = "adm_gcli_subgrupo";
            this.adm_gcli_subgrupo.Opacity = 1D;
            this.adm_gcli_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gcli_subgrupo.Text = "g.SubGrupo";
            this.adm_gcli_subgrupo.UseCompatibleTextRendering = false;
            // 
            // adm_gcli_ordcliente
            // 
            this.adm_gcli_ordcliente.Image = global::Jaeger.UI.Properties.Resources.purchase_order_16px;
            this.adm_gcli_ordcliente.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gcli_ordcliente.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gcli_ordcliente.Name = "adm_gcli_ordcliente";
            this.adm_gcli_ordcliente.ShowBorder = false;
            this.adm_gcli_ordcliente.Text = "b.OrdenCliente";
            this.adm_gcli_ordcliente.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gcli_ordcliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gcli_ordcliente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_remisionado
            // 
            this.adm_gcli_remisionado.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gcli_remisionado.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gcli_remisionado.Name = "adm_gcli_remisionado";
            this.adm_gcli_remisionado.ShowBorder = false;
            this.adm_gcli_remisionado.Text = "b.Remision";
            this.adm_gcli_remisionado.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gcli_remisionado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gcli_remisionado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_facturacion
            // 
            this.adm_gcli_facturacion.Image = global::Jaeger.UI.Properties.Resources.invoice_16px;
            this.adm_gcli_facturacion.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gcli_facturacion.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gcli_facturacion.Name = "adm_gcli_facturacion";
            this.adm_gcli_facturacion.ShowBorder = false;
            this.adm_gcli_facturacion.Text = "b.Comprobante";
            this.adm_gcli_facturacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gcli_facturacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gcli_facturacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_cobranza
            // 
            this.adm_gcli_cobranza.Image = global::Jaeger.UI.Properties.Resources.cash_register_32px;
            this.adm_gcli_cobranza.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcli_cobranza.Name = "adm_gcli_cobranza";
            this.adm_gcli_cobranza.Text = "b.Cobro";
            this.adm_gcli_cobranza.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gcli_cobranza.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_reportes
            // 
            this.adm_gcli_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gcli_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gcli_reportes.ExpandArrowButton = false;
            this.adm_gcli_reportes.Image = global::Jaeger.UI.Properties.Resources.document_30px;
            this.adm_gcli_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcli_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcli_bedocuenta,
            this.adm_gcli_bcredito,
            this.adm_gcli_bresumen});
            this.adm_gcli_reportes.Name = "adm_gcli_reportes";
            this.adm_gcli_reportes.Text = "b.Reporte";
            this.adm_gcli_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcli_bedocuenta
            // 
            this.adm_gcli_bedocuenta.Name = "adm_gcli_bedocuenta";
            this.adm_gcli_bedocuenta.Text = "b.EstadoCuenta";
            this.adm_gcli_bedocuenta.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_bcredito
            // 
            this.adm_gcli_bcredito.Name = "adm_gcli_bcredito";
            this.adm_gcli_bcredito.Text = "b.Credito";
            this.adm_gcli_bcredito.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gcli_bresumen
            // 
            this.adm_gcli_bresumen.Name = "adm_gcli_bresumen";
            this.adm_gcli_bresumen.Text = "b.Resumen";
            this.adm_gcli_bresumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_tesoreria
            // 
            this.adm_grp_tesoreria.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gtes_bancoctas,
            this.adm_gtes_banco});
            this.adm_grp_tesoreria.Name = "adm_grp_tesoreria";
            this.adm_grp_tesoreria.Text = "adm_grp_tesoreria";
            // 
            // adm_gtes_bancoctas
            // 
            this.adm_gtes_bancoctas.AutoSize = false;
            this.adm_gtes_bancoctas.Bounds = new System.Drawing.Rectangle(1, 2, 83, 62);
            this.adm_gtes_bancoctas.Image = global::Jaeger.UI.Properties.Resources.bank_cards_30px;
            this.adm_gtes_bancoctas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gtes_bancoctas.Name = "adm_gtes_bancoctas";
            this.adm_gtes_bancoctas.Text = "b.Cuentas";
            this.adm_gtes_bancoctas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gtes_bancoctas.TextWrap = true;
            this.adm_gtes_bancoctas.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_banco
            // 
            this.adm_gtes_banco.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gtes_banco.AutoSize = false;
            this.adm_gtes_banco.Bounds = new System.Drawing.Rectangle(0, 0, 76, 62);
            this.adm_gtes_banco.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gtes_banco.ExpandArrowButton = false;
            this.adm_gtes_banco.Image = global::Jaeger.UI.Properties.Resources.bank_30px;
            this.adm_gtes_banco.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gtes_banco.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gtes_bformapago,
            this.adm_gtes_bconceptos,
            this.adm_gtes_bbeneficiario,
            this.adm_gtes_bmovimiento});
            this.adm_gtes_banco.Name = "adm_gtes_banco";
            this.adm_gtes_banco.StretchHorizontally = true;
            this.adm_gtes_banco.Text = "b.Banco";
            this.adm_gtes_banco.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gtes_bformapago
            // 
            this.adm_gtes_bformapago.Name = "adm_gtes_bformapago";
            this.adm_gtes_bformapago.Text = "b.FormaPago";
            this.adm_gtes_bformapago.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_bconceptos
            // 
            this.adm_gtes_bconceptos.Name = "adm_gtes_bconceptos";
            this.adm_gtes_bconceptos.Text = "b.Conceptos";
            this.adm_gtes_bconceptos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_bbeneficiario
            // 
            this.adm_gtes_bbeneficiario.Name = "adm_gtes_bbeneficiario";
            this.adm_gtes_bbeneficiario.Text = "b.Beneficiario";
            this.adm_gtes_bbeneficiario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gtes_bmovimiento
            // 
            this.adm_gtes_bmovimiento.Name = "adm_gtes_bmovimiento";
            this.adm_gtes_bmovimiento.Text = "b.Movimientos";
            this.adm_gtes_bmovimiento.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_contable
            // 
            this.adm_grp_contable.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcontable_catcuentas,
            this.adm_gcontable_polizas,
            this.adm_gcontable_activos,
            this.adm_gcontable_configurar});
            this.adm_grp_contable.Name = "adm_grp_contable";
            this.adm_grp_contable.Text = "adm_grp_contable";
            // 
            // adm_gcontable_catcuentas
            // 
            this.adm_gcontable_catcuentas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gcontable_catcuentas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gcontable_catcuentas.ExpandArrowButton = false;
            this.adm_gcontable_catcuentas.Image = global::Jaeger.UI.Properties.Resources.cashbook_32;
            this.adm_gcontable_catcuentas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_catcuentas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcontable_catrubros,
            this.adm_gcontable_ctacontable});
            this.adm_gcontable_catcuentas.Name = "adm_gcontable_catcuentas";
            this.adm_gcontable_catcuentas.Text = "b.CatCtas";
            this.adm_gcontable_catcuentas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_catrubros
            // 
            this.adm_gcontable_catrubros.Name = "adm_gcontable_catrubros";
            this.adm_gcontable_catrubros.Text = "b.Rubros";
            // 
            // adm_gcontable_ctacontable
            // 
            this.adm_gcontable_ctacontable.Name = "adm_gcontable_ctacontable";
            this.adm_gcontable_ctacontable.Text = "b.CatCuentas";
            // 
            // adm_gcontable_polizas
            // 
            this.adm_gcontable_polizas.Image = global::Jaeger.UI.Properties.Resources.general_ledger_30px;
            this.adm_gcontable_polizas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_polizas.Name = "adm_gcontable_polizas";
            this.adm_gcontable_polizas.Text = "b.Polizas";
            this.adm_gcontable_polizas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_activos
            // 
            this.adm_gcontable_activos.Image = global::Jaeger.UI.Properties.Resources.book_shelf_30px;
            this.adm_gcontable_activos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_activos.Name = "adm_gcontable_activos";
            this.adm_gcontable_activos.Text = "b.Activos";
            this.adm_gcontable_activos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_configurar
            // 
            this.adm_gcontable_configurar.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gcontable_configurar.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gcontable_configurar.ExpandArrowButton = false;
            this.adm_gcontable_configurar.Image = global::Jaeger.UI.Properties.Resources.settings_30px;
            this.adm_gcontable_configurar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gcontable_configurar.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gcontable_cattipo,
            this.adm_gcontable_creartablas});
            this.adm_gcontable_configurar.Name = "adm_gcontable_configurar";
            this.adm_gcontable_configurar.Text = "b.Configura";
            this.adm_gcontable_configurar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gcontable_cattipo
            // 
            this.adm_gcontable_cattipo.Name = "adm_gcontable_cattipo";
            this.adm_gcontable_cattipo.Text = "b.CatTipo";
            // 
            // adm_gcontable_creartablas
            // 
            this.adm_gcontable_creartablas.Name = "adm_gcontable_creartablas";
            this.adm_gcontable_creartablas.Text = "b.CrearTablas";
            // 
            // adm_grp_almacen
            // 
            this.adm_grp_almacen.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_galmacen_mprima,
            this.adm_galmacen_pterminado});
            this.adm_grp_almacen.Name = "adm_grp_almacen";
            this.adm_grp_almacen.Text = "adm_grp_almacen";
            // 
            // adm_galmacen_mprima
            // 
            this.adm_galmacen_mprima.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_galmacen_mprima.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_galmacen_mprima.ExpandArrowButton = false;
            this.adm_galmacen_mprima.Image = global::Jaeger.UI.Properties.Resources.product_30px;
            this.adm_galmacen_mprima.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_galmacen_mprima.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_galmmp_catalogo,
            this.adm_galmmp_modelo,
            this.adm_galmmp_reqcompra,
            this.adm_galmmp_ordcompra,
            this.adm_galmmp_vales,
            this.adm_galmmp_devolucion,
            this.adm_galmmp_existencia,
            this.adm_galmmp_cunidad});
            this.adm_galmacen_mprima.Name = "adm_galmacen_mprima";
            this.adm_galmacen_mprima.Text = "b.MatPrima";
            this.adm_galmacen_mprima.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_galmmp_catalogo
            // 
            this.adm_galmmp_catalogo.Name = "adm_galmmp_catalogo";
            this.adm_galmmp_catalogo.Text = "b.Catalogo";
            this.adm_galmmp_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_modelo
            // 
            this.adm_galmmp_modelo.Name = "adm_galmmp_modelo";
            this.adm_galmmp_modelo.Text = "b.ProdModelo";
            // 
            // adm_galmmp_reqcompra
            // 
            this.adm_galmmp_reqcompra.Image = global::Jaeger.UI.Properties.Resources.purchase_order_16px;
            this.adm_galmmp_reqcompra.MinSize = new System.Drawing.Size(0, 20);
            this.adm_galmmp_reqcompra.Name = "adm_galmmp_reqcompra";
            this.adm_galmmp_reqcompra.Text = "bReqCompra";
            this.adm_galmmp_reqcompra.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_galmmp_reqcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_galmmp_reqcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_ordcompra
            // 
            this.adm_galmmp_ordcompra.Name = "adm_galmmp_ordcompra";
            this.adm_galmmp_ordcompra.Text = "b.OrdCompra";
            this.adm_galmmp_ordcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_vales
            // 
            this.adm_galmmp_vales.Name = "adm_galmmp_vales";
            this.adm_galmmp_vales.Text = "b.Vales";
            this.adm_galmmp_vales.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_devolucion
            // 
            this.adm_galmmp_devolucion.Name = "adm_galmmp_devolucion";
            this.adm_galmmp_devolucion.Text = "b.Devolucion";
            this.adm_galmmp_devolucion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_existencia
            // 
            this.adm_galmmp_existencia.Name = "adm_galmmp_existencia";
            this.adm_galmmp_existencia.Text = "b.Existencia";
            this.adm_galmmp_existencia.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmmp_cunidad
            // 
            this.adm_galmmp_cunidad.Name = "adm_galmmp_cunidad";
            this.adm_galmmp_cunidad.Text = "b.Cat. Unidad";
            // 
            // adm_galmacen_pterminado
            // 
            this.adm_galmacen_pterminado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_galmacen_pterminado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_galmacen_pterminado.ExpandArrowButton = false;
            this.adm_galmacen_pterminado.Image = global::Jaeger.UI.Properties.Resources.new_product_30px;
            this.adm_galmacen_pterminado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_galmacen_pterminado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_galmpt_catalogo,
            this.adm_galmpt_entrada,
            this.adm_galmpt_salida,
            this.adm_galmpt_existencia});
            this.adm_galmacen_pterminado.Name = "adm_galmacen_pterminado";
            this.adm_galmacen_pterminado.Text = "b.ProdTerminado";
            this.adm_galmacen_pterminado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_galmpt_catalogo
            // 
            this.adm_galmpt_catalogo.Name = "adm_galmpt_catalogo";
            this.adm_galmpt_catalogo.Text = "b.Catalogo";
            this.adm_galmpt_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_entrada
            // 
            this.adm_galmpt_entrada.Name = "adm_galmpt_entrada";
            this.adm_galmpt_entrada.Text = "";
            this.adm_galmpt_entrada.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_salida
            // 
            this.adm_galmpt_salida.Name = "adm_galmpt_salida";
            this.adm_galmpt_salida.Text = "b.Salida";
            this.adm_galmpt_salida.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_galmpt_existencia
            // 
            this.adm_galmpt_existencia.Name = "adm_galmpt_existencia";
            this.adm_galmpt_existencia.Text = "b.Existencia";
            this.adm_galmpt_existencia.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_grp_proveedor
            // 
            this.adm_grp_proveedor.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gprv_expediente,
            this.adm_gprv_subgrupo,
            this.adm_gprv_pagos,
            this.adm_gprv_reportes});
            this.adm_grp_proveedor.Name = "adm_grp_proveedor";
            this.adm_grp_proveedor.Text = "adm_grp_proveedor";
            // 
            // adm_gprv_expediente
            // 
            this.adm_gprv_expediente.Image = global::Jaeger.UI.Properties.Resources.supplier_32;
            this.adm_gprv_expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gprv_expediente.Name = "adm_gprv_expediente";
            this.adm_gprv_expediente.Text = "b.Expediente";
            this.adm_gprv_expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gprv_expediente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_subgrupo
            // 
            this.adm_gprv_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gprv_ordcompra,
            this.adm_gprv_validador,
            this.adm_gprv_facturacion});
            this.adm_gprv_subgrupo.Name = "adm_gprv_subgrupo";
            this.adm_gprv_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gprv_subgrupo.Text = "g.SubGrupo";
            // 
            // adm_gprv_ordcompra
            // 
            this.adm_gprv_ordcompra.Image = global::Jaeger.UI.Properties.Resources.purchase_order_16px;
            this.adm_gprv_ordcompra.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gprv_ordcompra.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gprv_ordcompra.Name = "adm_gprv_ordcompra";
            this.adm_gprv_ordcompra.ShowBorder = false;
            this.adm_gprv_ordcompra.Text = "b.OrdenCompra";
            this.adm_gprv_ordcompra.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gprv_ordcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gprv_ordcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_validador
            // 
            this.adm_gprv_validador.Image = global::Jaeger.UI.Properties.Resources.protect_16px;
            this.adm_gprv_validador.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gprv_validador.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gprv_validador.Name = "adm_gprv_validador";
            this.adm_gprv_validador.ShowBorder = false;
            this.adm_gprv_validador.Text = "b.Validar";
            this.adm_gprv_validador.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gprv_validador.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gprv_validador.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_facturacion
            // 
            this.adm_gprv_facturacion.Image = global::Jaeger.UI.Properties.Resources.invoice_16px;
            this.adm_gprv_facturacion.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.adm_gprv_facturacion.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gprv_facturacion.Name = "adm_gprv_facturacion";
            this.adm_gprv_facturacion.ShowBorder = false;
            this.adm_gprv_facturacion.Text = "b.Comprobante";
            this.adm_gprv_facturacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gprv_facturacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gprv_facturacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_pagos
            // 
            this.adm_gprv_pagos.Image = global::Jaeger.UI.Properties.Resources.check_book_30px;
            this.adm_gprv_pagos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gprv_pagos.Name = "adm_gprv_pagos";
            this.adm_gprv_pagos.Text = "b.Pago";
            this.adm_gprv_pagos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gprv_pagos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_reportes
            // 
            this.adm_gprv_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gprv_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gprv_reportes.ExpandArrowButton = false;
            this.adm_gprv_reportes.Image = global::Jaeger.UI.Properties.Resources.edit_graph_report_30px;
            this.adm_gprv_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gprv_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gprv_edocuenta,
            this.adm_gprv_credito,
            this.adm_gprv_resumen});
            this.adm_gprv_reportes.Name = "adm_gprv_reportes";
            this.adm_gprv_reportes.Text = "b.Reporte";
            this.adm_gprv_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gprv_edocuenta
            // 
            this.adm_gprv_edocuenta.Name = "adm_gprv_edocuenta";
            this.adm_gprv_edocuenta.Text = "b.EdoCuenta";
            this.adm_gprv_edocuenta.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_credito
            // 
            this.adm_gprv_credito.Name = "adm_gprv_credito";
            this.adm_gprv_credito.Text = "b.Reporte1";
            this.adm_gprv_credito.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gprv_resumen
            // 
            this.adm_gprv_resumen.Name = "adm_gprv_resumen";
            this.adm_gprv_resumen.Text = "b.Resumen";
            this.adm_gprv_resumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_grp_nomina
            // 
            this.nom_grp_nomina.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_btn_empleado,
            this.nom_sgrp_grupo,
            this.nom_gmovimiento_recibos,
            this.nom_grp_parametros});
            this.nom_grp_nomina.Name = "nom_grp_nomina";
            this.nom_grp_nomina.Text = "nom_grp_nomina";
            // 
            // nom_btn_empleado
            // 
            this.nom_btn_empleado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.nom_btn_empleado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.nom_btn_empleado.ExpandArrowButton = false;
            this.nom_btn_empleado.Image = global::Jaeger.UI.Properties.Resources.workers_30px;
            this.nom_btn_empleado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.nom_btn_empleado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_bemp_expediente,
            this.nom_bemp_contratos});
            this.nom_btn_empleado.Name = "nom_btn_empleado";
            this.nom_btn_empleado.Text = "b.Empleados";
            this.nom_btn_empleado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // nom_bemp_expediente
            // 
            this.nom_bemp_expediente.Name = "nom_bemp_expediente";
            this.nom_bemp_expediente.Text = "b.Expediente";
            this.nom_bemp_expediente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_bemp_contratos
            // 
            this.nom_bemp_contratos.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.nom_bemp_contratos.Name = "nom_bemp_contratos";
            this.nom_bemp_contratos.Text = "b.Contrato";
            this.nom_bemp_contratos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_sgrp_grupo
            // 
            this.nom_sgrp_grupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_gmov_periodo,
            this.nom_gmov_subgrupo,
            this.nom_gmov_aguinaldo});
            this.nom_sgrp_grupo.Name = "nom_sgrp_grupo";
            this.nom_sgrp_grupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.nom_sgrp_grupo.Text = "g.Grupo";
            // 
            // nom_gmov_periodo
            // 
            this.nom_gmov_periodo.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.nom_gmov_periodo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.nom_gmov_periodo.MinSize = new System.Drawing.Size(0, 20);
            this.nom_gmov_periodo.Name = "nom_gmov_periodo";
            this.nom_gmov_periodo.ShowBorder = false;
            this.nom_gmov_periodo.Text = "b.Periodo";
            this.nom_gmov_periodo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.nom_gmov_periodo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gmov_subgrupo
            // 
            this.nom_gmov_subgrupo.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.nom_gmov_subgrupo.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.nom_gmov_subgrupo.ExpandArrowButton = false;
            this.nom_gmov_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_gmov_vacacion,
            this.nom_gmov_faltas,
            this.nom_gmov_acumula});
            this.nom_gmov_subgrupo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.nom_gmov_subgrupo.MinSize = new System.Drawing.Size(0, 20);
            this.nom_gmov_subgrupo.Name = "nom_gmov_subgrupo";
            this.nom_gmov_subgrupo.Text = "b.Movimientos";
            this.nom_gmov_subgrupo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nom_gmov_vacacion
            // 
            this.nom_gmov_vacacion.Image = global::Jaeger.UI.Properties.Resources.beach_16;
            this.nom_gmov_vacacion.Name = "nom_gmov_vacacion";
            this.nom_gmov_vacacion.Text = "b.Vacaciones";
            this.nom_gmov_vacacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gmov_faltas
            // 
            this.nom_gmov_faltas.Name = "nom_gmov_faltas";
            this.nom_gmov_faltas.Text = "b.Faltas";
            this.nom_gmov_faltas.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gmov_acumula
            // 
            this.nom_gmov_acumula.Name = "nom_gmov_acumula";
            this.nom_gmov_acumula.Text = "b.Acumulados";
            this.nom_gmov_acumula.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gmov_aguinaldo
            // 
            this.nom_gmov_aguinaldo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.nom_gmov_aguinaldo.MinSize = new System.Drawing.Size(0, 20);
            this.nom_gmov_aguinaldo.Name = "nom_gmov_aguinaldo";
            this.nom_gmov_aguinaldo.ShowBorder = false;
            this.nom_gmov_aguinaldo.Text = "b.Aguinaldo";
            this.nom_gmov_aguinaldo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nom_gmovimiento_recibos
            // 
            this.nom_gmovimiento_recibos.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.nom_gmovimiento_recibos.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.nom_gmovimiento_recibos.ExpandArrowButton = false;
            this.nom_gmovimiento_recibos.Image = global::Jaeger.UI.Properties.Resources.money_transfer_30px;
            this.nom_gmovimiento_recibos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.nom_gmovimiento_recibos.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tnom_gmovimiento_consulta,
            this.tnom_gmovimiento_resumen});
            this.nom_gmovimiento_recibos.Name = "nom_gmovimiento_recibos";
            this.nom_gmovimiento_recibos.Text = "b.Recibos";
            this.nom_gmovimiento_recibos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tnom_gmovimiento_consulta
            // 
            this.tnom_gmovimiento_consulta.Name = "tnom_gmovimiento_consulta";
            this.tnom_gmovimiento_consulta.Text = "b.Consulta";
            this.tnom_gmovimiento_consulta.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tnom_gmovimiento_resumen
            // 
            this.tnom_gmovimiento_resumen.Name = "tnom_gmovimiento_resumen";
            this.tnom_gmovimiento_resumen.Text = "b.Resumen";
            this.tnom_gmovimiento_resumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_grp_parametros
            // 
            this.nom_grp_parametros.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_grp_concepto,
            this.nom_gparametros_tablas,
            this.nom_gparametros_config});
            this.nom_grp_parametros.Name = "nom_grp_parametros";
            this.nom_grp_parametros.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.nom_grp_parametros.Text = "g.Parametros";
            // 
            // nom_grp_concepto
            // 
            this.nom_grp_concepto.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.nom_grp_concepto.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.nom_grp_concepto.ExpandArrowButton = false;
            this.nom_grp_concepto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_gconcepto_catalogo});
            this.nom_grp_concepto.MinSize = new System.Drawing.Size(0, 20);
            this.nom_grp_concepto.Name = "nom_grp_concepto";
            this.nom_grp_concepto.Text = "b.Conceptos";
            this.nom_grp_concepto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nom_gconcepto_catalogo
            // 
            this.nom_gconcepto_catalogo.Name = "nom_gconcepto_catalogo";
            this.nom_gconcepto_catalogo.Text = "b.Catalogo";
            this.nom_gconcepto_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gparametros_tablas
            // 
            this.nom_gparametros_tablas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.nom_gparametros_tablas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.nom_gparametros_tablas.ExpandArrowButton = false;
            this.nom_gparametros_tablas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_gparametros_tisr,
            this.nom_gParametros_tsbe,
            this.nom_gparametros_tsm,
            this.nom_gParametros_tuma});
            this.nom_gparametros_tablas.MinSize = new System.Drawing.Size(0, 20);
            this.nom_gparametros_tablas.Name = "nom_gparametros_tablas";
            this.nom_gparametros_tablas.Text = "b.Parametros";
            this.nom_gparametros_tablas.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nom_gparametros_tisr
            // 
            this.nom_gparametros_tisr.Name = "nom_gparametros_tisr";
            this.nom_gparametros_tisr.Text = "b.TISR";
            this.nom_gparametros_tisr.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gParametros_tsbe
            // 
            this.nom_gParametros_tsbe.Name = "nom_gParametros_tsbe";
            this.nom_gParametros_tsbe.Text = "b.TSBE";
            this.nom_gParametros_tsbe.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gparametros_tsm
            // 
            this.nom_gparametros_tsm.Name = "nom_gparametros_tsm";
            this.nom_gparametros_tsm.Text = "b.TSM";
            this.nom_gparametros_tsm.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gParametros_tuma
            // 
            this.nom_gParametros_tuma.Name = "nom_gParametros_tuma";
            this.nom_gParametros_tuma.Text = "b.TUMA";
            this.nom_gParametros_tuma.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gparametros_config
            // 
            this.nom_gparametros_config.Image = global::Jaeger.UI.Properties.Resources.settings_16;
            this.nom_gparametros_config.MinSize = new System.Drawing.Size(0, 20);
            this.nom_gparametros_config.Name = "nom_gparametros_config";
            this.nom_gparametros_config.ShowBorder = false;
            this.nom_gparametros_config.Text = "b.Configuración";
            this.nom_gparametros_config.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.nom_gparametros_config.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.nom_gparametros_config.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TProd
            // 
            this.TProd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TProd.IsSelected = false;
            this.TProd.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_grp_directorio,
            this.cpcot_grp_cotizacion,
            this.cppro_grp_produccion,
            this.cpvnt_grp_ventas});
            this.TProd.Name = "TProd";
            this.TProd.Text = "TProducción";
            this.TProd.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TProd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.TProd.UseMnemonic = false;
            // 
            // cpemp_grp_directorio
            // 
            this.cpemp_grp_directorio.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_gdir_directorio,
            this.cpemp_grp_producto});
            this.cpemp_grp_directorio.Name = "cpemp_grp_directorio";
            this.cpemp_grp_directorio.Text = "g.Directorio";
            // 
            // cpemp_gdir_directorio
            // 
            this.cpemp_gdir_directorio.Image = global::Jaeger.UI.Properties.Resources.male_user_32px;
            this.cpemp_gdir_directorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpemp_gdir_directorio.Name = "cpemp_gdir_directorio";
            this.cpemp_gdir_directorio.Text = "b.Directorio";
            this.cpemp_gdir_directorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpemp_gdir_directorio.UseCompatibleTextRendering = false;
            this.cpemp_gdir_directorio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpemp_grp_producto
            // 
            this.cpemp_grp_producto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_gprd_clasificacion,
            this.cpemp_gprd_catalogo});
            this.cpemp_grp_producto.Name = "cpemp_grp_producto";
            this.cpemp_grp_producto.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cpemp_grp_producto.Text = "cpemp_grp_producto";
            // 
            // cpemp_gprd_clasificacion
            // 
            this.cpemp_gprd_clasificacion.Name = "cpemp_gprd_clasificacion";
            this.cpemp_gprd_clasificacion.ShowBorder = false;
            this.cpemp_gprd_clasificacion.Text = "b.Clasificacion";
            this.cpemp_gprd_clasificacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpemp_gprd_clasificacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpemp_gprd_catalogo
            // 
            this.cpemp_gprd_catalogo.Name = "cpemp_gprd_catalogo";
            this.cpemp_gprd_catalogo.ShowBorder = false;
            this.cpemp_gprd_catalogo.Text = "b.Catalogo";
            this.cpemp_gprd_catalogo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpemp_gprd_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_grp_cotizacion
            // 
            this.cpcot_grp_cotizacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_gcot_productos,
            this.cpcot_gcot_presupuesto,
            this.cpcot_gcot_cotizaciones,
            this.cpcot_gcot_cliente,
            this.cpcot_gcot_conf});
            this.cpcot_grp_cotizacion.Name = "cpcot_grp_cotizacion";
            this.cpcot_grp_cotizacion.Text = "g.Cotizacion";
            // 
            // cpcot_gcot_productos
            // 
            this.cpcot_gcot_productos.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpcot_gcot_productos.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpcot_gcot_productos.ExpandArrowButton = false;
            this.cpcot_gcot_productos.Image = global::Jaeger.UI.Properties.Resources.product_30px;
            this.cpcot_gcot_productos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_productos.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_bprd_producto,
            this.cpcot_bprd_subproducto});
            this.cpcot_gcot_productos.Name = "cpcot_gcot_productos";
            this.cpcot_gcot_productos.Text = "b.Productos";
            this.cpcot_gcot_productos.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_productos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpcot_bprd_producto
            // 
            this.cpcot_bprd_producto.Name = "cpcot_bprd_producto";
            this.cpcot_bprd_producto.Text = "b.Productos";
            // 
            // cpcot_bprd_subproducto
            // 
            this.cpcot_bprd_subproducto.Name = "cpcot_bprd_subproducto";
            this.cpcot_bprd_subproducto.Text = "b.SubProductos";
            // 
            // cpcot_gcot_presupuesto
            // 
            this.cpcot_gcot_presupuesto.Image = global::Jaeger.UI.Properties.Resources.estimate_30px;
            this.cpcot_gcot_presupuesto.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_presupuesto.Name = "cpcot_gcot_presupuesto";
            this.cpcot_gcot_presupuesto.StretchHorizontally = true;
            this.cpcot_gcot_presupuesto.StretchVertically = true;
            this.cpcot_gcot_presupuesto.Text = "b.Presupuestos";
            this.cpcot_gcot_presupuesto.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_presupuesto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpcot_gcot_cotizaciones
            // 
            this.cpcot_gcot_cotizaciones.Image = global::Jaeger.UI.Properties.Resources.accounting_30px;
            this.cpcot_gcot_cotizaciones.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_cotizaciones.Name = "cpcot_gcot_cotizaciones";
            this.cpcot_gcot_cotizaciones.StretchHorizontally = true;
            this.cpcot_gcot_cotizaciones.StretchVertically = true;
            this.cpcot_gcot_cotizaciones.Text = "b.Cotizaciones";
            this.cpcot_gcot_cotizaciones.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_cotizaciones.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpcot_gcot_cotizaciones.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_cliente
            // 
            this.cpcot_gcot_cliente.Image = global::Jaeger.UI.Properties.Resources.male_user_32px;
            this.cpcot_gcot_cliente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_cliente.Name = "cpcot_gcot_cliente";
            this.cpcot_gcot_cliente.Text = "b.Cliente";
            this.cpcot_gcot_cliente.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_cliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpcot_gcot_cliente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_conf
            // 
            this.cpcot_gcot_conf.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpcot_gcot_conf.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpcot_gcot_conf.ExpandArrowButton = false;
            this.cpcot_gcot_conf.Image = global::Jaeger.UI.Properties.Resources.administrative_tools_32px;
            this.cpcot_gcot_conf.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_conf.Name = "cpcot_gcot_conf";
            this.cpcot_gcot_conf.Text = "b.Configuracion";
            this.cpcot_gcot_conf.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_conf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cppro_grp_produccion
            // 
            this.cppro_grp_produccion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_gpro_orden,
            this.cppro_gpro_reqcompra,
            this.cppro_gpro_calendario,
            this.cppro_gpro_sgrupo,
            this.cppro_gpro_remision});
            this.cppro_grp_produccion.Name = "cppro_grp_produccion";
            this.cppro_grp_produccion.Text = "g.Produccion";
            this.cppro_grp_produccion.UseCompatibleTextRendering = false;
            // 
            // cppro_gpro_orden
            // 
            this.cppro_gpro_orden.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_orden.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_orden.ExpandArrowButton = false;
            this.cppro_gpro_orden.Image = global::Jaeger.UI.Properties.Resources.production_order_history_30px;
            this.cppro_gpro_orden.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_orden.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_bord_historial,
            this.cppro_bord_proceso});
            this.cppro_gpro_orden.Name = "cppro_gpro_orden";
            this.cppro_gpro_orden.Text = "b.Ord.Produccion";
            this.cppro_gpro_orden.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_orden.UseCompatibleTextRendering = false;
            // 
            // cppro_bord_historial
            // 
            this.cppro_bord_historial.Name = "cppro_bord_historial";
            this.cppro_bord_historial.Text = "b.Historial";
            this.cppro_bord_historial.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_bord_proceso
            // 
            this.cppro_bord_proceso.Name = "cppro_bord_proceso";
            this.cppro_bord_proceso.Text = "b.EnProceso";
            this.cppro_bord_proceso.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_reqcompra
            // 
            this.cppro_gpro_reqcompra.Image = global::Jaeger.UI.Properties.Resources.archive_list_of_parts_30px;
            this.cppro_gpro_reqcompra.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_reqcompra.Name = "cppro_gpro_reqcompra";
            this.cppro_gpro_reqcompra.Text = "b.Req.Compra";
            this.cppro_gpro_reqcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_reqcompra.TextWrap = true;
            this.cppro_gpro_reqcompra.UseCompatibleTextRendering = false;
            this.cppro_gpro_reqcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_calendario
            // 
            this.cppro_gpro_calendario.Image = global::Jaeger.UI.Properties.Resources.calendar_30px;
            this.cppro_gpro_calendario.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_calendario.Name = "cppro_gpro_calendario";
            this.cppro_gpro_calendario.Text = "b.Calendario";
            this.cppro_gpro_calendario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_calendario.TextWrap = true;
            this.cppro_gpro_calendario.UseCompatibleTextRendering = false;
            this.cppro_gpro_calendario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_sgrupo
            // 
            this.cppro_gpro_sgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_sgrp_depto,
            this.cppro_sgrp_avance,
            this.cppro_sgrp_evaluacion});
            this.cppro_gpro_sgrupo.Name = "cppro_gpro_sgrupo";
            this.cppro_gpro_sgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cppro_gpro_sgrupo.Text = "g.SubGrupo";
            this.cppro_gpro_sgrupo.UseCompatibleTextRendering = false;
            // 
            // cppro_sgrp_depto
            // 
            this.cppro_sgrp_depto.Image = global::Jaeger.UI.Properties.Resources.calendar_16;
            this.cppro_sgrp_depto.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_depto.Name = "cppro_sgrp_depto";
            this.cppro_sgrp_depto.ShowBorder = false;
            this.cppro_sgrp_depto.Text = "g.Departamento";
            this.cppro_sgrp_depto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_depto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cppro_sgrp_depto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_sgrp_avance
            // 
            this.cppro_sgrp_avance.Image = global::Jaeger.UI.Properties.Resources.production_in_progress_16px;
            this.cppro_sgrp_avance.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_avance.Name = "cppro_sgrp_avance";
            this.cppro_sgrp_avance.ShowBorder = false;
            this.cppro_sgrp_avance.Text = "b.Avance";
            this.cppro_sgrp_avance.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_avance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cppro_sgrp_avance.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_sgrp_evaluacion
            // 
            this.cppro_sgrp_evaluacion.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_sgrp_evaluacion.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_sgrp_evaluacion.ExpandArrowButton = false;
            this.cppro_sgrp_evaluacion.Image = global::Jaeger.UI.Properties.Resources.edit_graph_report_16px;
            this.cppro_sgrp_evaluacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcppro_beva_EProduccion,
            this.tcppro_beva_EPeriodo});
            this.cppro_sgrp_evaluacion.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_evaluacion.Name = "cppro_sgrp_evaluacion";
            this.cppro_sgrp_evaluacion.Text = "b.Evaluacion";
            this.cppro_sgrp_evaluacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_evaluacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // tcppro_beva_EProduccion
            // 
            this.tcppro_beva_EProduccion.Name = "tcppro_beva_EProduccion";
            this.tcppro_beva_EProduccion.Text = "b.E.Produccion";
            this.tcppro_beva_EProduccion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcppro_beva_EPeriodo
            // 
            this.tcppro_beva_EPeriodo.Name = "tcppro_beva_EPeriodo";
            this.tcppro_beva_EPeriodo.Text = "b.E.Periodo";
            this.tcppro_beva_EPeriodo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_remision
            // 
            this.cppro_gpro_remision.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_remision.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_remision.ExpandArrowButton = false;
            this.cppro_gpro_remision.Image = global::Jaeger.UI.Properties.Resources.production_finished_30px;
            this.cppro_gpro_remision.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_remision.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_brem_Emitido,
            this.cppro_brem_Recibido});
            this.cppro_gpro_remision.Name = "cppro_gpro_remision";
            this.cppro_gpro_remision.Text = "b.Remisionado";
            this.cppro_gpro_remision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cppro_brem_Emitido
            // 
            this.cppro_brem_Emitido.Name = "cppro_brem_Emitido";
            this.cppro_brem_Emitido.Text = "b.Emitido";
            this.cppro_brem_Emitido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_brem_Recibido
            // 
            this.cppro_brem_Recibido.Name = "cppro_brem_Recibido";
            this.cppro_brem_Recibido.Text = "b.Recibido";
            this.cppro_brem_Recibido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_grp_ventas
            // 
            this.cpvnt_grp_ventas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_vendedor,
            this.cpvnt_gven_prodvend,
            this.cpvnt_gven_group1,
            this.cpvnt_gven_remisionado});
            this.cpvnt_grp_ventas.Name = "cpvnt_grp_ventas";
            this.cpvnt_grp_ventas.Text = "g.Ventas";
            // 
            // cpvnt_gven_vendedor
            // 
            this.cpvnt_gven_vendedor.Image = global::Jaeger.UI.Properties.Resources.businessman_30px;
            this.cpvnt_gven_vendedor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_vendedor.Name = "cpvnt_gven_vendedor";
            this.cpvnt_gven_vendedor.Text = "b.Vendedor";
            this.cpvnt_gven_vendedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpvnt_gven_vendedor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_prodvend
            // 
            this.cpvnt_gven_prodvend.Image = global::Jaeger.UI.Properties.Resources.new_product_30px;
            this.cpvnt_gven_prodvend.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_prodvend.Name = "cpvnt_gven_prodvend";
            this.cpvnt_gven_prodvend.Text = "b.Productos";
            this.cpvnt_gven_prodvend.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpvnt_gven_prodvend.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_group1
            // 
            this.cpvnt_gven_group1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_pedidos,
            this.cpvnt_gven_comision,
            this.cpvnt_gven_ventcosto});
            this.cpvnt_gven_group1.Name = "cpvnt_gven_group1";
            this.cpvnt_gven_group1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cpvnt_gven_group1.Text = "g.Pedidos";
            // 
            // cpvnt_gven_pedidos
            // 
            this.cpvnt_gven_pedidos.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_pedidos.Name = "cpvnt_gven_pedidos";
            this.cpvnt_gven_pedidos.ShowBorder = false;
            this.cpvnt_gven_pedidos.Text = "b.Pedido";
            this.cpvnt_gven_pedidos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_pedidos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_pedidos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_comision
            // 
            this.cpvnt_gven_comision.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_comision.Name = "cpvnt_gven_comision";
            this.cpvnt_gven_comision.ShowBorder = false;
            this.cpvnt_gven_comision.Text = "b.Comision";
            this.cpvnt_gven_comision.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_comision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_comision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_ventcosto
            // 
            this.cpvnt_gven_ventcosto.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_ventcosto.Name = "cpvnt_gven_ventcosto";
            this.cpvnt_gven_ventcosto.ShowBorder = false;
            this.cpvnt_gven_ventcosto.Text = "b.VentaVsCosto";
            this.cpvnt_gven_ventcosto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_ventcosto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_ventcosto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_remisionado
            // 
            this.cpvnt_gven_remisionado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpvnt_gven_remisionado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpvnt_gven_remisionado.ExpandArrowButton = false;
            this.cpvnt_gven_remisionado.Image = global::Jaeger.UI.Properties.Resources.new_product_30px;
            this.cpvnt_gven_remisionado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_remisionado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_brem_historial,
            this.cpvnt_brem_porcobrar,
            this.cpvnt_brem_rempart});
            this.cpvnt_gven_remisionado.Name = "cpvnt_gven_remisionado";
            this.cpvnt_gven_remisionado.Text = "b.Remisionado";
            this.cpvnt_gven_remisionado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpvnt_brem_historial
            // 
            this.cpvnt_brem_historial.Name = "cpvnt_brem_historial";
            this.cpvnt_brem_historial.Text = "b.Remision";
            this.cpvnt_brem_historial.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_brem_porcobrar
            // 
            this.cpvnt_brem_porcobrar.Name = "cpvnt_brem_porcobrar";
            this.cpvnt_brem_porcobrar.Text = "b.PorCobrar";
            this.cpvnt_brem_porcobrar.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_brem_rempart
            // 
            this.cpvnt_brem_rempart.Name = "cpvnt_brem_rempart";
            this.cpvnt_brem_rempart.Text = "b.Partida";
            this.cpvnt_brem_rempart.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TVentas
            // 
            this.TVentas.IsSelected = false;
            this.TVentas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_grp_ventas,
            this.cmr_grp_cp});
            this.TVentas.Name = "TVentas";
            this.TVentas.Text = "TComercial";
            this.TVentas.UseMnemonic = false;
            // 
            // cmr_grp_ventas
            // 
            this.cmr_grp_ventas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gventas_vededores,
            this.cmr_gventas_subgrupo});
            this.cmr_grp_ventas.Name = "cmr_grp_ventas";
            this.cmr_grp_ventas.Text = "cmr_grp_ventas";
            // 
            // cmr_gventas_vededores
            // 
            this.cmr_gventas_vededores.Image = global::Jaeger.UI.Properties.Resources.businessman_30px;
            this.cmr_gventas_vededores.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gventas_vededores.Name = "cmr_gventas_vededores";
            this.cmr_gventas_vededores.Text = "b.Vendedor";
            this.cmr_gventas_vededores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmr_gventas_vededores.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gventas_subgrupo
            // 
            this.cmr_gventas_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gventas_pedidos,
            this.cmr_gventas_comision,
            this.cmr_gventas_ccomision});
            this.cmr_gventas_subgrupo.Name = "cmr_gventas_subgrupo";
            this.cmr_gventas_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cmr_gventas_subgrupo.Text = "cmr_gventas_subgrupo";
            // 
            // cmr_gventas_pedidos
            // 
            this.cmr_gventas_pedidos.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_pedidos.Name = "cmr_gventas_pedidos";
            this.cmr_gventas_pedidos.ShowBorder = false;
            this.cmr_gventas_pedidos.Text = "b.Pedido";
            this.cmr_gventas_pedidos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_pedidos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gventas_comision
            // 
            this.cmr_gventas_comision.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_comision.Name = "cmr_gventas_comision";
            this.cmr_gventas_comision.ShowBorder = false;
            this.cmr_gventas_comision.Text = "b.Comision";
            this.cmr_gventas_comision.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gventas_comision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gventas_ccomision
            // 
            this.cmr_gventas_ccomision.Name = "cmr_gventas_ccomision";
            this.cmr_gventas_ccomision.ShowBorder = false;
            this.cmr_gventas_ccomision.Text = "b.CComision";
            this.cmr_gventas_ccomision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_grp_cp
            // 
            this.cmr_grp_cp.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_directorio,
            this.cmr_gcp_subgrupo,
            this.cmr_gcp_reportes,
            this.cmr_gcp_resumen});
            this.cmr_grp_cp.Name = "cmr_grp_cp";
            this.cmr_grp_cp.Text = "cmr_grp_cp";
            // 
            // cmr_gcp_directorio
            // 
            this.cmr_gcp_directorio.Image = global::Jaeger.UI.Properties.Resources.address_book_30px;
            this.cmr_gcp_directorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gcp_directorio.Name = "cmr_gcp_directorio";
            this.cmr_gcp_directorio.Text = "b.Directorio";
            this.cmr_gcp_directorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmr_gcp_directorio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_subgrupo
            // 
            this.cmr_gcp_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_ordproduccion,
            this.cmr_gcp_prodvendido,
            this.cmr_gcp_remisionado});
            this.cmr_gcp_subgrupo.Name = "cmr_gcp_subgrupo";
            this.cmr_gcp_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cmr_gcp_subgrupo.Text = "g.subgrupo";
            // 
            // cmr_gcp_ordproduccion
            // 
            this.cmr_gcp_ordproduccion.Name = "cmr_gcp_ordproduccion";
            this.cmr_gcp_ordproduccion.ShowBorder = false;
            this.cmr_gcp_ordproduccion.Text = "b.OrdProduccion";
            this.cmr_gcp_ordproduccion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gcp_ordproduccion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_prodvendido
            // 
            this.cmr_gcp_prodvendido.Name = "cmr_gcp_prodvendido";
            this.cmr_gcp_prodvendido.ShowBorder = false;
            this.cmr_gcp_prodvendido.Text = "b.ProdVendidos";
            this.cmr_gcp_prodvendido.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmr_gcp_prodvendido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_remisionado
            // 
            this.cmr_gcp_remisionado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cmr_gcp_remisionado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cmr_gcp_remisionado.ExpandArrowButton = false;
            this.cmr_gcp_remisionado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_bremisionado,
            this.cmr_gcp_rempartida});
            this.cmr_gcp_remisionado.Name = "cmr_gcp_remisionado";
            this.cmr_gcp_remisionado.Text = "b.Remisionado";
            this.cmr_gcp_remisionado.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmr_gcp_bremisionado
            // 
            this.cmr_gcp_bremisionado.Name = "cmr_gcp_bremisionado";
            this.cmr_gcp_bremisionado.Text = "b.Remisionado";
            this.cmr_gcp_bremisionado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_rempartida
            // 
            this.cmr_gcp_rempartida.Name = "cmr_gcp_rempartida";
            this.cmr_gcp_rempartida.Text = "b.RemPartida";
            this.cmr_gcp_rempartida.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_reportes
            // 
            this.cmr_gcp_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cmr_gcp_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cmr_gcp_reportes.ExpandArrowButton = false;
            this.cmr_gcp_reportes.Image = global::Jaeger.UI.Properties.Resources.ratings_32px;
            this.cmr_gcp_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gcp_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cmr_gcp_reporte1});
            this.cmr_gcp_reportes.Name = "cmr_gcp_reportes";
            this.cmr_gcp_reportes.Text = "b.Reportes";
            this.cmr_gcp_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cmr_gcp_reporte1
            // 
            this.cmr_gcp_reporte1.Name = "cmr_gcp_reporte1";
            this.cmr_gcp_reporte1.Text = "b.Reporte1";
            this.cmr_gcp_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cmr_gcp_resumen
            // 
            this.cmr_gcp_resumen.Image = global::Jaeger.UI.Properties.Resources.play_graph_report_32px;
            this.cmr_gcp_resumen.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmr_gcp_resumen.Name = "cmr_gcp_resumen";
            this.cmr_gcp_resumen.Text = "b.Resumen";
            this.cmr_gcp_resumen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmr_gcp_resumen.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TTools
            // 
            this.TTools.AutoEllipsis = false;
            this.TTools.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TTools.IsSelected = false;
            this.TTools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_grp_config,
            this.dsk_grp_tools,
            this.dsk_grp_retencion,
            this.dsk_grp_theme});
            this.TTools.Name = "TTools";
            this.TTools.Text = "THerramientas";
            this.TTools.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TTools.UseCompatibleTextRendering = false;
            this.TTools.UseMnemonic = false;
            // 
            // dsk_grp_config
            // 
            this.dsk_grp_config.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_empresa,
            this.dsk_gconfig_series});
            this.dsk_grp_config.Name = "dsk_grp_config";
            this.dsk_grp_config.Text = "g.Configuracion";
            // 
            // dsk_gconfig_empresa
            // 
            this.dsk_gconfig_empresa.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gconfig_empresa.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gconfig_empresa.ExpandArrowButton = false;
            this.dsk_gconfig_empresa.Image = global::Jaeger.UI.Properties.Resources.control_panel_30px;
            this.dsk_gconfig_empresa.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_empresa.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_emisor,
            this.dsk_gconfig_avanzado,
            this.dsk_gconfig_usuario,
            this.dsk_gconfig_menu,
            this.dsk_gconfig_perfil});
            this.dsk_gconfig_empresa.Name = "dsk_gconfig_empresa";
            this.dsk_gconfig_empresa.Text = "b.Empresa";
            this.dsk_gconfig_empresa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gconfig_avanzado
            // 
            this.dsk_gconfig_avanzado.Name = "dsk_gconfig_avanzado";
            this.dsk_gconfig_avanzado.Text = "b.Avanzado";
            this.dsk_gconfig_avanzado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_usuario
            // 
            this.dsk_gconfig_usuario.Name = "dsk_gconfig_usuario";
            this.dsk_gconfig_usuario.Text = "b.Usuarios";
            // 
            // dsk_gconfig_menu
            // 
            this.dsk_gconfig_menu.Name = "dsk_gconfig_menu";
            this.dsk_gconfig_menu.Text = "b.Menu";
            this.dsk_gconfig_menu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_perfil
            // 
            this.dsk_gconfig_perfil.Name = "dsk_gconfig_perfil";
            this.dsk_gconfig_perfil.Text = "b.Perfil";
            this.dsk_gconfig_perfil.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_series
            // 
            this.dsk_gconfig_series.Image = global::Jaeger.UI.Properties.Resources.counter_32px;
            this.dsk_gconfig_series.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_series.Name = "dsk_gconfig_series";
            this.dsk_gconfig_series.Text = "b.Series";
            this.dsk_gconfig_series.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_grp_tools
            // 
            this.dsk_grp_tools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_repositorio,
            this.dsk_gtools_backup,
            this.dsk_gtools_validador,
            this.dsk_gtools_mantto});
            this.dsk_grp_tools.Name = "dsk_grp_tools";
            this.dsk_grp_tools.Text = "gHerramientas";
            // 
            // dsk_gtools_repositorio
            // 
            this.dsk_gtools_repositorio.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_repositorio.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_repositorio.ExpandArrowButton = false;
            this.dsk_gtools_repositorio.Image = global::Jaeger.UI.Properties.Resources.download_from_cloud_32;
            this.dsk_gtools_repositorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_repositorio.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_asistido,
            this.dsk_gtools_repositorio1});
            this.dsk_gtools_repositorio.Name = "dsk_gtools_repositorio";
            this.dsk_gtools_repositorio.Text = "bRepositorio";
            this.dsk_gtools_repositorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_asistido
            // 
            this.dsk_gtools_asistido.Name = "dsk_gtools_asistido";
            this.dsk_gtools_asistido.Text = "b.Asistido";
            // 
            // dsk_gtools_repositorio1
            // 
            this.dsk_gtools_repositorio1.Name = "dsk_gtools_repositorio1";
            this.dsk_gtools_repositorio1.Text = "b.Repositorio";
            this.dsk_gtools_repositorio1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_backup
            // 
            this.dsk_gtools_backup.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_backup.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_backup.ExpandArrowButton = false;
            this.dsk_gtools_backup.Image = global::Jaeger.UI.Properties.Resources.cloud_storage_32px;
            this.dsk_gtools_backup.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_backup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_backcfdi,
            this.dsk_gtools_backdir});
            this.dsk_gtools_backup.Name = "dsk_gtools_backup";
            this.dsk_gtools_backup.Text = "b.Backup";
            this.dsk_gtools_backup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_backcfdi
            // 
            this.dsk_gtools_backcfdi.Name = "dsk_gtools_backcfdi";
            this.dsk_gtools_backcfdi.Text = "b.BackCFDI";
            this.dsk_gtools_backcfdi.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_backdir
            // 
            this.dsk_gtools_backdir.Name = "dsk_gtools_backdir";
            this.dsk_gtools_backdir.Text = "b.BackupContribuyentes";
            this.dsk_gtools_backdir.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validador
            // 
            this.dsk_gtools_validador.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_validador.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_validador.ExpandArrowButton = false;
            this.dsk_gtools_validador.Image = global::Jaeger.UI.Properties.Resources.protect_32px;
            this.dsk_gtools_validador.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_validador.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_validar,
            this.dsk_gtools_validarfc,
            this.dsk_gtools_validacedula,
            this.dsk_gtools_validaret,
            this.dsk_gtools_cancelado,
            this.dsk_gtools_presuntos,
            this.dsk_gtools_certificado});
            this.dsk_gtools_validador.Name = "dsk_gtools_validador";
            this.dsk_gtools_validador.Text = "b.Validador";
            this.dsk_gtools_validador.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_validar
            // 
            this.dsk_gtools_validar.Name = "dsk_gtools_validar";
            this.dsk_gtools_validar.Text = "b.Comprobante";
            this.dsk_gtools_validar.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validarfc
            // 
            this.dsk_gtools_validarfc.Name = "dsk_gtools_validarfc";
            this.dsk_gtools_validarfc.Text = "Valida RFC";
            this.dsk_gtools_validarfc.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validacedula
            // 
            this.dsk_gtools_validacedula.Image = global::Jaeger.UI.Properties.Resources.smart_card_16;
            this.dsk_gtools_validacedula.Name = "dsk_gtools_validacedula";
            this.dsk_gtools_validacedula.Text = "b.Cedula";
            this.dsk_gtools_validacedula.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validaret
            // 
            this.dsk_gtools_validaret.Name = "dsk_gtools_validaret";
            this.dsk_gtools_validaret.Text = "Valida Retención";
            this.dsk_gtools_validaret.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_cancelado
            // 
            this.dsk_gtools_cancelado.Name = "dsk_gtools_cancelado";
            this.dsk_gtools_cancelado.Text = "b.Cancelados";
            // 
            // dsk_gtools_presuntos
            // 
            this.dsk_gtools_presuntos.Name = "dsk_gtools_presuntos";
            this.dsk_gtools_presuntos.Text = "b.Presuntos";
            // 
            // dsk_gtools_certificado
            // 
            this.dsk_gtools_certificado.Name = "dsk_gtools_certificado";
            this.dsk_gtools_certificado.Text = "b.Certificado";
            this.dsk_gtools_certificado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_mantto
            // 
            this.dsk_gtools_mantto.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_mantto.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_mantto.ExpandArrowButton = false;
            this.dsk_gtools_mantto.Image = global::Jaeger.UI.Properties.Resources.administrative_tools_32px;
            this.dsk_gtools_mantto.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_mantto.Name = "dsk_gtools_mantto";
            this.dsk_gtools_mantto.Text = "b.Mantto";
            this.dsk_gtools_mantto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_grp_retencion
            // 
            this.dsk_grp_retencion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tdks_gretencion_recep,
            this.tdks_gretencion_comprobante});
            this.dsk_grp_retencion.Name = "dsk_grp_retencion";
            this.dsk_grp_retencion.Text = "g.Retencion";
            // 
            // tdks_gretencion_recep
            // 
            this.tdks_gretencion_recep.Image = global::Jaeger.UI.Properties.Resources.customer_30px;
            this.tdks_gretencion_recep.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tdks_gretencion_recep.Name = "tdks_gretencion_recep";
            this.tdks_gretencion_recep.Text = "b.Receptores";
            this.tdks_gretencion_recep.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tdks_gretencion_comprobante
            // 
            this.tdks_gretencion_comprobante.Image = global::Jaeger.UI.Properties.Resources.invoice_30px;
            this.tdks_gretencion_comprobante.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tdks_gretencion_comprobante.Name = "tdks_gretencion_comprobante";
            this.tdks_gretencion_comprobante.Text = "b.Comprobante";
            this.tdks_gretencion_comprobante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_grp_theme
            // 
            this.dsk_grp_theme.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtheme_2010Black,
            this.dsk_gtheme_2010Blue,
            this.dsk_gtheme_2010Silver});
            this.dsk_grp_theme.Name = "dsk_grp_theme";
            this.dsk_grp_theme.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dsk_grp_theme.Text = "gTheme";
            // 
            // dsk_gtheme_2010Black
            // 
            this.dsk_gtheme_2010Black.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Black.Image")));
            this.dsk_gtheme_2010Black.Name = "dsk_gtheme_2010Black";
            this.dsk_gtheme_2010Black.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Black.Tag = "Office2010Black";
            this.dsk_gtheme_2010Black.Text = "Office 2010 Black";
            this.dsk_gtheme_2010Black.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Black.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Black.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Blue
            // 
            this.dsk_gtheme_2010Blue.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Blue.Image")));
            this.dsk_gtheme_2010Blue.Name = "dsk_gtheme_2010Blue";
            this.dsk_gtheme_2010Blue.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Blue.Tag = "Office2010Blue";
            this.dsk_gtheme_2010Blue.Text = "Office 2010 Blue";
            this.dsk_gtheme_2010Blue.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Blue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Blue.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Silver
            // 
            this.dsk_gtheme_2010Silver.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Silver.Image")));
            this.dsk_gtheme_2010Silver.Name = "dsk_gtheme_2010Silver";
            this.dsk_gtheme_2010Silver.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Silver.Tag = "Office2010Silver";
            this.dsk_gtheme_2010Silver.Text = "Office 2010 Silver";
            this.dsk_gtheme_2010Silver.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Silver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Silver.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // BEstado
            // 
            this.BEstado.Location = new System.Drawing.Point(0, 643);
            this.BEstado.Name = "BEstado";
            this.BEstado.Size = new System.Drawing.Size(1551, 26);
            this.BEstado.SizingGrip = false;
            this.BEstado.TabIndex = 1;
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1";
            // 
            // RadDock
            // 
            this.RadDock.AutoDetectMdiChildren = true;
            this.RadDock.Controls.Add(this.RadContainer);
            this.RadDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadDock.IsCleanUpTarget = true;
            this.RadDock.Location = new System.Drawing.Point(0, 162);
            this.RadDock.MainDocumentContainer = this.RadContainer;
            this.RadDock.Name = "RadDock";
            // 
            // 
            // 
            this.RadDock.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadDock.Size = new System.Drawing.Size(1551, 481);
            this.RadDock.TabIndex = 5;
            this.RadDock.TabStop = false;
            // 
            // RadContainer
            // 
            this.RadContainer.Name = "RadContainer";
            // 
            // 
            // 
            this.RadContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadContainer.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // dsk_gconfig_emisor
            // 
            this.dsk_gconfig_emisor.Name = "dsk_gconfig_emisor";
            this.dsk_gconfig_emisor.Text = "b.Empresa";
            this.dsk_gconfig_emisor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // MainRibbonForm
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1551, 669);
            this.Controls.Add(this.RadDock);
            this.Controls.Add(this.BEstado);
            this.Controls.Add(this.MenuRibbonBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainRibbonForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainRibbonForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainRibbonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).EndInit();
            this.RadDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar MenuRibbonBar;
        private Telerik.WinControls.UI.RadStatusStrip BEstado;
        private Telerik.WinControls.UI.RibbonTab TAdmon;
        private Telerik.WinControls.UI.RibbonTab TProd;
        private Telerik.WinControls.UI.RibbonTab TVentas;
        private Telerik.WinControls.UI.RibbonTab TTools;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_cliente;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_expediente;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gcli_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_ordcliente;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_facturacion;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.Docking.RadDock RadDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer RadContainer;
        private Telerik.WinControls.Themes.Windows8Theme windows8Theme1;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_cobranza;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gcli_reportes;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_bedocuenta;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_bcredito;
        private Telerik.WinControls.UI.RadMenuItem adm_gcli_bresumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_tesoreria;
        private Telerik.WinControls.UI.RadButtonElement adm_gtes_bancoctas;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gtes_banco;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bbeneficiario;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bconceptos;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bformapago;
        private Telerik.WinControls.UI.RadMenuItem adm_gtes_bmovimiento;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_contable;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_almacen;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_grp_proveedor;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gcontable_catcuentas;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_catrubros;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_ctacontable;
        private Telerik.WinControls.UI.RadButtonElement adm_gcontable_polizas;
        private Telerik.WinControls.UI.RadButtonElement adm_gcontable_activos;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gcontable_configurar;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_cattipo;
        private Telerik.WinControls.UI.RadMenuItem adm_gcontable_creartablas;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_galmacen_mprima;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_catalogo;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_vales;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_devolucion;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_existencia;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_galmacen_pterminado;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_catalogo;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_entrada;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_salida;
        private Telerik.WinControls.UI.RadMenuItem adm_galmpt_existencia;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_expediente;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gprv_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_ordcompra;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_validador;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_facturacion;
        private Telerik.WinControls.UI.RadButtonElement adm_gprv_pagos;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gprv_reportes;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_edocuenta;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_resumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup cppro_grp_produccion;
        private Telerik.WinControls.UI.RadRibbonBarGroup cmr_grp_cp;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_directorio;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cmr_gcp_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_ordproduccion;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_prodvendido;
        private Telerik.WinControls.UI.RadDropDownButtonElement cmr_gcp_remisionado;
        private Telerik.WinControls.UI.RadMenuItem cmr_gcp_bremisionado;
        private Telerik.WinControls.UI.RadMenuItem cmr_gcp_rempartida;
        private Telerik.WinControls.UI.RadButtonElement cmr_gcp_resumen;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_config;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gconfig_empresa;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_avanzado;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_tools;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_repositorio;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_asistido;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_repositorio1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_theme;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Black;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Blue;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Silver;
        private Telerik.WinControls.UI.RadRibbonBarGroup cmr_grp_ventas;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_vededores;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cmr_gventas_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_pedidos;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_comision;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_usuario;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_ordcompra;
        private Telerik.WinControls.UI.RadMenuItem adm_gprv_credito;
        private Telerik.WinControls.UI.RadDropDownButtonElement cmr_gcp_reportes;
        private Telerik.WinControls.UI.RadMenuItem cmr_gcp_reporte1;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_menu;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_perfil;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_backup;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_validador;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_cancelado;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_presuntos;
        private Telerik.WinControls.UI.RadButtonElement dsk_gconfig_series;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_mantto;
        private Telerik.WinControls.UI.RadButtonElement cmr_gventas_ccomision;
        private Telerik.WinControls.UI.RadButtonElement adm_gcli_remisionado;
        private Telerik.WinControls.UI.RadRibbonBarGroup nom_grp_nomina;
        private Telerik.WinControls.UI.RadDropDownButtonElement nom_btn_empleado;
        private Telerik.WinControls.UI.RadMenuItem nom_bemp_expediente;
        private Telerik.WinControls.UI.RadMenuItem nom_bemp_contratos;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup nom_sgrp_grupo;
        private Telerik.WinControls.UI.RadButtonElement nom_gmov_periodo;
        private Telerik.WinControls.UI.RadDropDownButtonElement nom_gmov_subgrupo;
        private Telerik.WinControls.UI.RadMenuItem nom_gmov_vacacion;
        private Telerik.WinControls.UI.RadMenuItem nom_gmov_faltas;
        private Telerik.WinControls.UI.RadMenuItem nom_gmov_acumula;
        private Telerik.WinControls.UI.RadButtonElement nom_gmov_aguinaldo;
        private Telerik.WinControls.UI.RadDropDownButtonElement nom_gmovimiento_recibos;
        private Telerik.WinControls.UI.RadMenuItem tnom_gmovimiento_consulta;
        private Telerik.WinControls.UI.RadMenuItem tnom_gmovimiento_resumen;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup nom_grp_parametros;
        private Telerik.WinControls.UI.RadDropDownButtonElement nom_grp_concepto;
        private Telerik.WinControls.UI.RadMenuItem nom_gconcepto_catalogo;
        private Telerik.WinControls.UI.RadDropDownButtonElement nom_gparametros_tablas;
        private Telerik.WinControls.UI.RadMenuItem nom_gparametros_tisr;
        private Telerik.WinControls.UI.RadMenuItem nom_gParametros_tsbe;
        private Telerik.WinControls.UI.RadMenuItem nom_gparametros_tsm;
        private Telerik.WinControls.UI.RadMenuItem nom_gParametros_tuma;
        private Telerik.WinControls.UI.RadButtonElement nom_gparametros_config;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validar;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_retencion;
        private Telerik.WinControls.UI.RadButtonElement tdks_gretencion_recep;
        private Telerik.WinControls.UI.RadButtonElement tdks_gretencion_comprobante;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_certificado;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validarfc;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_reqcompra;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_orden;
        private Telerik.WinControls.UI.RadMenuItem cppro_bord_historial;
        private Telerik.WinControls.UI.RadMenuItem cppro_bord_proceso;
        private Telerik.WinControls.UI.RadButtonElement cppro_gpro_reqcompra;
        private Telerik.WinControls.UI.RadButtonElement cppro_gpro_calendario;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cppro_gpro_sgrupo;
        private Telerik.WinControls.UI.RadButtonElement cppro_sgrp_depto;
        private Telerik.WinControls.UI.RadButtonElement cppro_sgrp_avance;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_remision;
        private Telerik.WinControls.UI.RadMenuItem cppro_brem_Emitido;
        private Telerik.WinControls.UI.RadMenuItem cppro_brem_Recibido;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_sgrp_evaluacion;
        private Telerik.WinControls.UI.RadMenuItem tcppro_beva_EProduccion;
        private Telerik.WinControls.UI.RadMenuItem tcppro_beva_EPeriodo;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_modelo;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpvnt_grp_ventas;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_vendedor;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_prodvend;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cpvnt_gven_group1;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_pedidos;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_comision;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_ventcosto;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpvnt_gven_remisionado;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_historial;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_porcobrar;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_rempart;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpemp_grp_directorio;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gdir_directorio;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpcot_grp_cotizacion;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpcot_gcot_productos;
        private Telerik.WinControls.UI.RadMenuItem cpcot_bprd_producto;
        private Telerik.WinControls.UI.RadMenuItem cpcot_bprd_subproducto;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_presupuesto;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_cotizaciones;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_cliente;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpcot_gcot_conf;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validaret;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cpemp_grp_producto;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gprd_clasificacion;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gprd_catalogo;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_backcfdi;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_backdir;
        private Telerik.WinControls.UI.RadMenuItem adm_galmmp_cunidad;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validacedula;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_emisor;
    }
}
