﻿using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Banco;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Forms.Clientes {
    public class BuscarComprobanteForm : UI.Banco.Forms.BuscarComprobanteForm {
        public BuscarComprobanteForm() : base() {
            this.Load += this.BuscarComprobanteForm_Load;
        }

        public BuscarComprobanteForm(MovimientoConceptoDetailModel conceptoDetailModel, int idDirectorio, IBancoService service) : base(conceptoDetailModel, idDirectorio, service) {
            this.Load += this.BuscarComprobanteForm_Load;
        }

        private void BuscarComprobanteForm_Load(object sender, System.EventArgs e) {
            this.Text = "Remisionado al Cliente (CP)";
            this.TBuscar.lblCaption.Text = "Folio: ";
            this.TBuscar.lblCaption.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.TBuscar.Descripcion.MaxSize = new System.Drawing.Size(50, 20);
            this.TBuscar.ShowTextBox = true;
            this.TBuscar.Descripcion.KeyPress += Extensions.TextBoxOnlyNumbers_KeyPress;
            this.TBuscar.Descripcion.TextChanged += this.Descripcion_TextChanged;
        }

        private void Descripcion_TextChanged(object sender, System.EventArgs e) {
            if (!string.IsNullOrEmpty(this.TBuscar.Descripcion.Text))
                this._IdDirectorio = int.Parse(this.TBuscar.Descripcion.Text);
        }
    }
}
