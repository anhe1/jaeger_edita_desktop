﻿using System;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Comprobante.Services;
using Jaeger.Aplication.Comprobante;

namespace Jaeger.UI.Forms.Clientes {
    public class ReciboCobroCatalogoForm : UI.Banco.Forms.MovimientosForm {
        protected internal RadMenuItem context_CrearComprobantePago = new RadMenuItem { Text = "Comprobante de Pago" };
        protected internal RadMenuItem TReciboCobro_Nuevo = new RadMenuItem { Text = "Nuevo" };
        protected Aplication.Comprobante.Contracts.IComprobantesFiscalesSearchService searchService;

        public ReciboCobroCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(Domain.Banco.ValueObjects.MovimientoBancarioEfectoEnum.Ingreso) {
            this.Load += ReciboCobroCatalogoForm_Load;
        }

        private void ReciboCobroCatalogoForm_Load(object sender, EventArgs e) {
            this.Icon = base.Icon;
            this.Text = "Clientes: Cobranza";

            this.searchService = new ComprobanteFiscalSearchService();

            TReciboCobro_Nuevo.Click += TReciboCobro_Nuevo_Click;
            context_CrearComprobantePago.Click += CrearComprobantePago_Click;

            this.TMovimiento.Nuevo.Items.Add(TReciboCobro_Nuevo);
            this.Movimientos.MenuContextual.Items.Add(context_CrearComprobantePago);
        }

        private void TReciboCobro_Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ReciboCobroForm(this.service)) {
                nuevo.ShowDialog(this);
            }
        }

        private void CrearComprobantePago_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                using (var espera = new UI.Common.Forms.Waiting1Form(this.CrearComprobantePago)) {
                    espera.Text = Properties.Resources.Msg_CrearComprobante;
                    espera.ShowDialog(this);

                    if (seleccionado.Tag != null) {
                        // mostrar la vista
                        var _form = new UI.Comprobante.Forms.ComprobanteFiscalRecepcionPagoForm((Domain.Comprobante.Entities.ComprobanteFiscalDetailModel)seleccionado.Tag, Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
                        _form.Show();
                    } else {
                        RadMessageBox.Show(this, Properties.Resources.Msg_CrearComprobante_Error, "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        private void CrearComprobantePago() {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                if (seleccionado.Comprobantes != null) {
                    // lista de comprobantes relacionados
                    var lista = seleccionado.Comprobantes.Select(it => it.IdDocumento).ToList();
                    var _doctos = this.searchService.SearchUUID(lista);
                    if (_doctos != null) {
                        if (_doctos.Count > 0) {
                            IComprobantesFiscalesService c1 = new ComprobantesFiscalesService();
                            // crear el comprobante fiscal
                            var _d1 = c1.CrearReciboElectronicoPago(_doctos.ToList());
                            if (_d1 != null) {
                                _d1.RecepcionPago[0].FechaPago = seleccionado.FechaAplicacion.Value;
                                _d1.RecepcionPago[0].FormaDePagoP = seleccionado.ClaveFormaPago;
                                seleccionado.Tag = _d1;
                            }
                        }
                    }
                }
            }
        }
    }
}
