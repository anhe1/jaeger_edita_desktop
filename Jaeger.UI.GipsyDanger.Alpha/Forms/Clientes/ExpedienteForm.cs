﻿using Jaeger.Aplication.Contribuyentes;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Clientes {
    public class ExpedienteForm : UI.Contribuyentes.Forms.ClientesCatalogoForm {
        public ExpedienteForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ExpedienteForm_Load;
        }

        private void ExpedienteForm_Load(object sender, System.EventArgs e) {
            this.service = new DirectorioService(this.relacionComericalEnum);
        }
    }
}
