﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Forms.Clientes {
    public class EmitidosPorCobrarForm : EmitidosForm {
        public EmitidosPorCobrarForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Clientes facturación: Por Cobrar";
            this.Load += EmitidosPorCobrarForm_Load;
        }

        private void EmitidosPorCobrarForm_Load(object sender, System.EventArgs e) {
            this.TComprobante.ShowPeriodo = false;
        }

        public override void Consultar() {
            this.comprobanteFiscalSingles = this.service.GetListByStatus(this.TComprobante.GetEjercicio(), -1, "PorCobrar");
        }
    }
}
