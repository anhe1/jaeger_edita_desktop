﻿using System;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Comprobante.Forms;

namespace Jaeger.UI.Forms.Clientes {
    /// <summary>
    /// comprobantes emitidos
    /// </summary>
    public class EmitidosForm : Comprobante.Forms.ComprobantesFiscalesEmitidoForm {
        protected internal RadMenuItem MenuContextual_ReciboCobro = new RadMenuItem { Text = "Registrar Cobro" };
        protected internal GridViewTemplate gridCobranza = new GridViewTemplate();
        protected Aplication.Banco.IBancoMovientosSearchService bancoService;
        protected internal RadMenuItem PorCobrar = new RadMenuItem { Text = "Por Cobrar", ToolTipText = "Comprobantes por cobrar" };
        protected internal UIMenuElement menuElement;

        public EmitidosForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Icon = Properties.Resources.bill;
            this.Load += EmitidosForm_Load;
        }

        private void EmitidosForm_Load(object sender, EventArgs e) {
            this.bancoService = new Aplication.Banco.BancoMovientosSearchService();
            this.menuContextual.Items.Add(MenuContextual_ReciboCobro);
            this.MenuContextual_ReciboCobro.Click += new EventHandler(this.TMenuContextual_ReciboCobro_Click);
            this.TComprobante.Herramientas.Items.Add(this.PorCobrar);
            this.TComprobante.ShowHerramientas = true;
            this.TempleteCobranza();
            gridCobranza.HierarchyDataProvider = new GridViewEventDataProvider(gridCobranza);
            this.gridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
            this.PorCobrar.Click += TComprobante_PorCobrar_Click;
        }

        #region barra de herramientas
        private void TComprobante_PorCobrar_Click(object sender, EventArgs e) {
            var d0 = new EmitidosPorCobrarForm(this.menuElement) { MdiParent = this.ParentForm };
            d0.Show();
        }
        public override void TComprobante_Nuevo_Click(object sender, EventArgs e) {
            var _facturaNuevo = new Comprobante1FiscalForm(CFDISubTipoEnum.Emitido, 0) { MdiParent = this.ParentForm };
            _facturaNuevo.Show();
        }

        public override void TComprobante_Editar_Click(object sender, EventArgs e) {
            if (this.gridData.CurrentRow != null) {
                var _seleccionado = this.gridData.CurrentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (_seleccionado != null) {
                    if (_seleccionado.IdDocumento != null && _seleccionado.IdDocumento != "") {
                        var _visualizar = new ComprobanteFiscalGeneralForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                        _visualizar.Show();
                    } else {
                        if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                            var _editar = new ComprobanteFiscalRecepcionPagoForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if ((_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Ingreso | _seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Traslado) && _seleccionado.Documento == "porte") {
                            var _editar = new ComprobanteFiscalCartaPorteForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        } else if (_seleccionado.TipoComprobante == CFDITipoComprobanteEnum.Nomina) {
                            RadMessageBox.Show(this, "Tipo de comprobante no soportado.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                        } else {
                            var _editar = new Comprobante1FiscalForm(CFDISubTipoEnum.Emitido, _seleccionado.Id) { MdiParent = this.ParentForm };
                            _editar.Show();
                        }
                    }
                }
            }
        }
        #endregion

        #region menu contextual
        private void TMenuContextual_ReciboCobro_Click(object sender, EventArgs e) {
            var _seleccion = this.GetSelection();
            if (_seleccion != null) {
                if (_seleccion.Count > 0) {
                    var _si = _seleccion.Where(it => it.Status == "Cancelado" | it.Status == "Importado" | it.Status == "Cobrado" | it.Estado == "Cancelado");
                    if (_si.Count() > 0) {
                        RadMessageBox.Show(this, "No es posible crear el registro de cobranza. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }

            var _movimiento = new MovimientoBancarioDetailModel();
            foreach (var item in _seleccion) {
                _movimiento.Comprobantes.Add(new MovimientoBancarioComprobanteDetailModel {
                    Activo = true,
                    ClaveFormaPago = item.ClaveFormaPago,
                    ClaveMetodoPago = item.ClaveMetodoPago,
                    ClaveMoneda = item.ClaveMoneda,
                    EmisorNombre = item.EmisorNombre,
                    EmisorRFC = item.EmisorRFC,
                    Estado = item.Estado,
                    FechaEmision = item.FechaEmision,
                    FechaNuevo = item.FechaNuevo,
                    IdComprobante = item.Id,
                    IdDocumento = item.IdDocumento,
                    NumParcialidad = item.NumParcialidad,
                    ReceptorNombre = item.ReceptorNombre,
                    ReceptorRFC = item.ReceptorRFC,
                    Serie = item.Serie,
                    Version = item.Version,
                    Total = item.Total,
                    SubTipoComprobante = (CFDISubTipoEnum)item.IdSubTipo,
                    TipoComprobanteText = item.TipoComprobanteText,
                    Folio = item.Folio
                });
            }
            var _nuevoMovimiento = new UI.Banco.Forms.MovimientoBancarioForm(_movimiento, new Aplication.Banco.BancoService());
            _nuevoMovimiento.ShowDialog(this);
        }
        #endregion

        #region acciones del grid
        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.gridCobranza.Caption) {
                var rowView = e.ParentRow.DataBoundItem as ComprobanteFiscalDetailSingleModel;
                if (rowView != null) {
                    var tabla = this.bancoService.GetSearchMovimientos(rowView.Id);
                    foreach (var item in tabla) {
                        var newRow = e.Template.Rows.NewRow();
                        newRow.Cells["Identificador"].Value = item.Identificador;
                        newRow.Cells["Status"].Value = item.Status;
                        newRow.Cells["FechaEmision"].Value = item.FechaEmision;
                        newRow.Cells["BeneficiarioT"].Value = item.BeneficiarioT;
                        newRow.Cells["Concepto"].Value = item.Concepto;
                        newRow.Cells["IdDocumento"].Value = item.IdDocumento;
                        newRow.Cells["NumAutorizacion"].Value = item.NumAutorizacion;
                        newRow.Cells["FechaAplicacion"].Value = item.FechaAplicacion;
                        newRow.Cells["Cargo"].Value = item.Cargo1;
                        newRow.Cells["Abono"].Value = item.Abono1;
                        newRow.Cells["Creo"].Value = item.Creo;
                        e.SourceCollection.Add(newRow);
                    }
                }
            }
        }
        private void TempleteCobranza() {
            var tableViewDefinition1 = new TableViewDefinition();
            gridCobranza.Caption = "Cobranza";
            var _identificador = new GridViewTextBoxColumn { DataType = typeof(string), FieldName = "Identificador", HeaderText = "No. Ident.", Name = "Identificador", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 70 };
            var _status = new GridViewTextBoxColumn { FieldName = "Estado", HeaderText = "Status", Name = "Status", Width = 65 };
            var _fechaEmision = new GridViewTextBoxColumn { DataType = typeof(DateTime), FieldName = "FechaEmision", FormatString = "{0:dd-MMM-yy}", HeaderText = "Fec. Emisión", Name = "FechaEmision", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 75 };
            var _concepto = new GridViewTextBoxColumn { FieldName = "Concepto", HeaderText = "Concepto", Name = "Concepto", Width = 165 };
            var _beneficiario = new GridViewTextBoxColumn { FieldName = "BeneficiarioT", HeaderText = "Beneficiario", Name = "BeneficiarioT", Width = 175 };
            var _idDocumento = new GridViewTextBoxColumn { FieldName = "IdDocumento", HeaderText = "Id Documento (UUID)", Name = "IdDocumento", Width = 195 };
            var _numAutorizacion = new GridViewTextBoxColumn { FieldName = "NumAutorizacion", HeaderText = "Núm. Autorización", Name = "NumAutorizacion", Width = 105 };
            var _fechaAplicacion = new GridViewTextBoxColumn { DataType = typeof(DateTime), FieldName = "FechaAplicacion", FormatString = "{0:dd-MMM-yy}", HeaderText = "Fec. Aplicación", Name = "FechaAplicacion", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 75 };
            var _cargo = new GridViewTextBoxColumn { DataType = typeof(decimal), FieldName = "Cargo", FormatString = "{0:n}", HeaderText = "Cargo", Name = "Cargo", TextAlignment = System.Drawing.ContentAlignment.MiddleRight, Width = 85 };
            var _abono = new GridViewTextBoxColumn { DataType = typeof(decimal), FieldName = "Abono", FormatString = "{0:n}", HeaderText = "Abono", Name = "Abono", TextAlignment = System.Drawing.ContentAlignment.MiddleRight, Width = 85 };
            var _creo = new GridViewTextBoxColumn { FieldName = "Creo", HeaderText = "Creó", Name = "Creo" };

            gridCobranza.Columns.AddRange(new GridViewDataColumn[] { _identificador, _status, _fechaEmision, _beneficiario, _concepto, _idDocumento, _numAutorizacion, _fechaAplicacion, _cargo, _abono, _creo });

            gridCobranza.ViewDefinition = tableViewDefinition1;
            this.gridData.MasterTemplate.Templates.Add(gridCobranza);
            this.gridCobranza.TelerikTemplateCommon();
        }
        #endregion
    }
}
