﻿using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using System.ComponentModel;

namespace Jaeger.UI.Forms.Clientes {
    /// <summary>
    /// Comprobante Fiscal Emitido
    /// </summary>
    public class Comprobante1FiscalForm : Comprobante.Forms.ComprobanteFiscalForm {

        public Comprobante1FiscalForm(CFDISubTipoEnum subtTipo, int indice) : base(subtTipo, indice) {
            this.Load += this.Comprobante1FiscalForm_Load;   
        }

        private void Comprobante1FiscalForm_Load(object sender, System.EventArgs e) {
            
        }

        public Comprobante1FiscalForm(ComprobanteFiscalDetailModel comprobante, CFDISubTipoEnum subtTipo, int indice) : base(comprobante, subtTipo, indice) {
            this.Load += this.Comprobante1FiscalForm_Load;
        }

        public override void TConcepto_Productos_Click(object sender, System.EventArgs e) {
            var _conceptos = new ProdServCatalogoBuscarForm() { StartPosition = System.Windows.Forms.FormStartPosition.CenterParent };
            _conceptos.Agregar += _conceptos_Agregar;
            _conceptos.ShowDialog(this);
        }

        private void _conceptos_Agregar(object sender, ComprobanteConceptoDetailModel e) {
            if (this.ComprobanteControl.Comprobante.Conceptos.Count == 0) {
                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<ComprobanteConceptoDetailModel>();
                this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoInformacionAduanera.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                this.GridConceptoParte.DataMember = "Parte";
                this.GridConceptoImpuestos.DataMember = "Impuestos";
                this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";
            }
            if (e != null) {
                this.ComprobanteControl.Comprobante.Conceptos.Add(e);
            }
        }
    }
}
