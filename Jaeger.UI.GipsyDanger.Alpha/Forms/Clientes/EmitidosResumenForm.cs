﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Clientes {
    public class EmitidosResumenForm : Comprobante.Forms.ComprobantesFiscalesResumenForm {
        public EmitidosResumenForm(UIMenuElement menuElement) : base(CFDISubTipoEnum.Emitido, menuElement) {
        }
    }
}
