﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Forms.Clientes {
    public class EmitidosResumen2 : Comprobante.Forms.ComprobantesFiscalesResumen2 {
        public EmitidosResumen2(UIMenuElement menuElement) : base(CFDISubTipoEnum.Emitido, menuElement) {
            this.Load += ComprobantesEmitidosResumen2_Load;
        }

        private void ComprobantesEmitidosResumen2_Load(object sender, System.EventArgs e) {
            this.Text = "Facuración vs Cobranza";
        }
    }
}
