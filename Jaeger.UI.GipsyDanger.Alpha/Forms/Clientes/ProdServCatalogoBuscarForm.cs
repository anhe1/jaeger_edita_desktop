﻿using System;

namespace Jaeger.UI.Forms.Clientes {
    public class ProdServCatalogoBuscarForm : Comprobante.Forms.ProdServCatalogoBuscarForm {
        protected Telerik.WinControls.UI.RadPageViewPage pageCP = new Telerik.WinControls.UI.RadPageViewPage() { Name = "Uno", Text = "Orden de Producción" };
        

        public ProdServCatalogoBuscarForm() : base() {
            this.Load += this.ProdServCatalogoBuscarForm_Load;
        }

        private void ProdServCatalogoBuscarForm_Load(object sender, EventArgs e) {
            var _pages = this.radPageView.Pages;
            _pages.Add(this.pageCP);
        }
    }
}
