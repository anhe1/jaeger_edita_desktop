﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.UI.Common.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Jaeger.UI.Forms.Clientes {
    public class RemisionadoPartidaForm : UI.Almacen.PT.Forms.RemisionadoPartidaForm {
        //protected Aplication.Comprobante.IComprobantesFiscalesService comprobanteFiscalService;

        public RemisionadoPartidaForm(UIMenuElement menuElement) : base() {
            this.Load += RemisionesFiscalesPartidasForm_Load;
        }

        private void RemisionesFiscalesPartidasForm_Load(object sender, EventArgs e) {
            var contextMenuFacturar = new RadMenuItem { Text = "Crear Factura" };
            contextMenuFacturar.Click += this.contextMenuFacturar_Click;
            this.TRemision.menuContextual.Items.Add(contextMenuFacturar);
            //this.comprobanteFiscalService = new Aplication.Comprobante.Services.ComprobantesFiscalesService();
        }

        private void contextMenuFacturar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.CrearFactura)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var cfdiV33 = this.Tag as Domain.Comprobante.Entities.ComprobanteFiscalDetailModel;
            if (cfdiV33 != null) {
                var nuevo = new UI.Comprobante.Forms.ComprobanteFiscalGeneralForm(cfdiV33, CFDISubTipoEnum.Emitido, -1) { MdiParent = this.ParentForm, Text = "Nuevo Comprobante" };
                nuevo.Show();
            }
            this.Tag = null;
        }

        private void CrearFactura() {
            //if (this.TRemision.GridData.CurrentRow != null) {
            //    var seleccion = new List<RemisionPartidaModel>();
            //    seleccion.AddRange(this.TRemision.GridData.SelectedRows.Where(x => x.IsSelected == true).Select(x => x.DataBoundItem as RemisionPartidaModel));
            //    if (seleccion.Count > 0) {

            //        // crear el comprobante y luego la vista
            //        var cfdiV33 = this.comprobanteFiscalService.GetNew(CFDITipoComprobanteEnum.Ingreso);
            //        foreach (var item in seleccion) {

            //            var concepto = new Domain.Comprobante.Entities.ComprobanteConceptoDetailModel {
            //                Cantidad = item.Cantidad,
            //                ValorUnitario = item.Unitario,
            //                Descripcion = item.Producto,
            //                NumPedido = item.IdPedido,
            //                ClaveUnidad = "H87",
            //                ClaveProdServ = "82121511"
            //            };
            //            concepto.Impuestos.Add(new Domain.Comprobante.Entities.ComprobanteConceptoImpuesto {
            //                Impuesto = ImpuestoEnum.IVA,
            //                TasaOCuota = new decimal(0.16),
            //                Tipo = TipoImpuestoEnum.Traslado,
            //                Base = concepto.Importe
            //            });
            //            cfdiV33.Conceptos.Add(concepto);

            //        }
            //        this.Tag = cfdiV33;
            //    }
            //}
        }
    }
}
