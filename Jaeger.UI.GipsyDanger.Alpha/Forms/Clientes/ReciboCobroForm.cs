﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.Forms.Clientes {
    public class ReciboCobroForm : UI.Banco.Forms.MovimientoBancarioForm {
        public ReciboCobroForm() : base() {

        }

        public ReciboCobroForm(Aplication.Banco.IBancoService service) : base(service) {
            this.Load += ReciboCobroForm_Load;
        }

        public ReciboCobroForm(MovimientoBancarioDetailModel movimiento, Aplication.Banco.IBancoService service) : base(movimiento, service) {
            this.Load += ReciboCobroForm_Load;
        }

        private void ReciboCobroForm_Load(object sender, EventArgs e) {
            this.Text = "Banco: Recibo de Cobro";
        }

        public override void TComprobante_Buscar_Click(object sender, EventArgs e) {
            if (this.TComprobante.Documento.SelectedItem != null) {
                var seleccionado = this.TComprobante.Documento.SelectedItem.DataBoundItem as ItemSelectedModel;
                if (seleccionado != null) {
                    if (seleccionado.Id == (int)MovimientoBancarioTipoComprobanteEnum.Remision) {
                        var _buscar = new BuscarComprobanteForm(this.currentTipoMovimiento, this.TMovimiento.Movimiento.IdDirectorio, this.TMovimiento.Service);
                        _buscar.AgregarComprobante += BTComprobante_Buscar_AgregarComprobante;
                        _buscar.ShowDialog(this);
                    } else {
                        var _buscar = new UI.Banco.Forms.BuscarComprobanteForm(this.currentTipoMovimiento, this.TMovimiento.Movimiento.IdDirectorio, this.TMovimiento.Service);
                        _buscar.AgregarComprobante += BTComprobante_Buscar_AgregarComprobante;
                        _buscar.ShowDialog(this);
                    }
                }
            } else {
                RadMessageBox.Show("Properties.Resources.String7", "Atención", MessageBoxButtons.OK);
            }
        }

        private void BTComprobante_Buscar_AgregarComprobante(object sender, MovimientoBancarioComprobanteDetailModel e) {
            if (e != null) {
                if (e != null) {
                    if (this.TMovimiento.Movimiento.Agregar(e) == true)
                        RadMessageBox.Show("Properties.Resources.String7", "Atención", MessageBoxButtons.OK);
                }
            }
        }
    }
}
