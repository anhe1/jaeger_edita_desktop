﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.DataAccess.FB.Almacen.Repositories
{
    public class SqlFbAlmacenRepository : Abstractions.RepositoryMaster<AlmacenModel>, ISqlAlmacenRepository {
        public SqlFbAlmacenRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public AlmacenModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<AlmacenModel> GetList() {
            throw new NotImplementedException();
        }

        public int Insert(AlmacenModel item) {
            throw new NotImplementedException();
        }

        public int Update(AlmacenModel item) {
            throw new NotImplementedException();
        }
        #endregion

        public IEnumerable<IAlmacenModel> GetList(List<IConditional> conditionals) {
            return new List<IAlmacenModel> {
                new AlmacenModel(0, "AlmacenGR", "Almacén General", Domain.Base.ValueObjects.AlmacenEnum.MP),
                new AlmacenModel(1, "AlmacenMP", "Almacén Materia Prima", Domain.Base.ValueObjects.AlmacenEnum.MP),
                new AlmacenModel(2, "AlmacenPT", "Almacén Producto Terminado", Domain.Base.ValueObjects.AlmacenEnum.PT),
            };
        }

        public AlmacenModel Save(AlmacenModel model) {
            throw new NotImplementedException();
        }
    }
}
