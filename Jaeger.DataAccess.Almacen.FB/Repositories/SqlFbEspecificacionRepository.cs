﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    public class SqlFbEspecificacionRepository : Abstractions.RepositoryMaster<EspecificacionModel>, ISqlEspecificacionRepository {
        public SqlFbEspecificacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(EspecificacionModel item) {
            var sqlCommand = new FbCommand { CommandText = @"INSERT INTO CTESPC (CTESPC_ID, CTESPC_A, CTESPC_NOM, CTESPC_NOTA, CTESPC_FN, CTESPC_USR_N) VALUES (@CTESPC_ID, @CTESPC_A, @CTESPC_NOM, @CTESPC_NOTA, @CTESPC_FN, @CTESPC_USR_N) RETURNING CTESPC_ID" };
            sqlCommand.Parameters.AddWithValue("@CTESPC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTESPC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTESPC_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTESPC_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CTESPC_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTESPC_USR_N", item.Creo);

            item.IdEspecificacion = this.ExecuteScalar(sqlCommand);
            if (item.IdEspecificacion > 0)
                return item.IdEspecificacion;
            return 0;
        }

        public int Update(EspecificacionModel item) {
            var sqlCommand = new FbCommand { CommandText = @"UPDATE CTESPC SET CTESPC_A = @CTESPC_A, CTESPC_NOM = @CTESPC_NOM, CTESPC_NOTA = @CTESPC_NOTA, CTESPC_FM = @CTESPC_FM, CTESPC_USR_M = @CTESPC_USR_M WHERE ((CTESPC_ID = @CTESPC_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@CTESPC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTESPC_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTESPC_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CTESPC_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTESPC_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM CTESPC WHERE ((CTESPC_ID = @CTESPC_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTESPC_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public EspecificacionModel GetById(int index) {
            var sqlCommand = new FbCommand { CommandText = @"SELECT FIRST 1 CTESPC.* FROM CTESPC WHERE ((CTESPC_ID = @CTESPC_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTESPC_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<EspecificacionModel> GetList() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT CTESPC.* FROM CTESPC" };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTESPC.* FROM CTESPC @wcondiciones"
            };
            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public EspecificacionModel Save(EspecificacionModel model) {
            if (model.IdEspecificacion == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdEspecificacion = this.Insert(model);
                model.SetModified = false;
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                if (this.Update(model) > 0)
                    model.SetModified = false;
            }
            return model;
        }
    }
}
