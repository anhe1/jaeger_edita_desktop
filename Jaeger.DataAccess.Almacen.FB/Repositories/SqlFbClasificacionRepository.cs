﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    /// <summary>
    /// clase para el manejo del catalogo de clasificaciones de Bienes, Productos y Servicios (BPS) del almacen
    /// </summary>
    public class SqlFbClasificacionRepository : Abstractions.RepositoryMaster<ClasificacionModel>, Domain.Almacen.Contracts.ISqlClasificacionRepository {
        public SqlFbClasificacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE"
            };
            throw new NotImplementedException();
        }

        public ClasificacionModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<ClasificacionModel> GetList() {
            return this.GetList<ClasificacionModel>(new List<IConditional>());
        }

        public int Insert(ClasificacionModel item) {
            throw new NotImplementedException();
        }

        public int Update(ClasificacionModel item) {
            throw new NotImplementedException();
        }

        public int Salveable(ClasificacionModel model) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE OR INSERT INTO CTCAT (CTCAT_ID, CTCAT_CTCAT_ID, CTCAT_SEC, CTCAT_ALM_ID, CTCAT_NOM) VALUES (@CTCAT_ID, @CTCAT_CTCAT_ID, @CTCAT_SEC, @CTCAT_ALM_ID, @CTCAT_NOM) MATCHING(CTCAT_ID, CTCAT_CTCAT_ID);"
            };
            sqlCommand.Parameters.Add("@CTCAT_ID", model.IdCategoria);
            sqlCommand.Parameters.Add("@CTCAT_CTCAT_ID", model.IdSubCategoria);
            sqlCommand.Parameters.Add("@CTCAT_SEC", model.Secuencia);
            sqlCommand.Parameters.Add("@CTCAT_ALM_ID", model.IdAlmacen);
            sqlCommand.Parameters.Add("@CTCAT_NOM", model.Descripcion);

            return this.ExecuteNoQuery(sqlCommand);
        }
        #endregion

        /// <summary>
        /// Lista generica condicional
        /// </summary>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand { CommandText = "SELECT CTCAT.* FROM CTCAT @condiciones" };
            if (typeof(T1) == typeof(ClasificacionModel)) {
                return GetMapper<T1>(sqlCommand, conditionals);
            } else if (typeof(T1) == typeof(ClasificacionSingle)) {
                // clasificacion simple, vista
                sqlCommand = new FbCommand {
                    CommandText =
                                @"WITH RECURSIVE CATEGORIA (IDCATEGORIA, SUBID, ACTIVO, SECUENCIA, CLASE, DESCRIPCION, NIVEL, CHILDS) AS
                                (
                                  SELECT CTCAT_ID AS IDCATEGORIA, CTCAT_CTCAT_ID AS SUBID, CTCAT_A AS ACTIVO, CTCAT_SEC AS SECUENCIA, CTCAT_NOM AS CLASE, CTCAT_NOM || '/' AS DESCRIPCION, 1, 
  	                                (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = CTCAT_ID) AS CHILDS
                                    FROM CTCAT
                                    WHERE CTCAT_ID = 1  -- THE TREE NODE
                                  UNION ALL
                                  SELECT T.CTCAT_ID, T.CTCAT_CTCAT_ID, (T.CTCAT_A * TP.ACTIVO), T.CTCAT_SEC, T.CTCAT_NOM, 
                                    IIF(CHARACTER_LENGTH(TP.DESCRIPCION) >= 75, LEFT(TP.DESCRIPCION, 75) || '... |', TP.DESCRIPCION) || T.CTCAT_NOM || ' |',  
                                    TP.NIVEL +1,
	                                (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = T.CTCAT_ID) AS CHILDS
                                    FROM CATEGORIA AS TP 
	                                 JOIN CTCAT AS T ON TP.IDCATEGORIA = T.CTCAT_CTCAT_ID 
                                )
                                SELECT  * FROM CATEGORIA 
                                ORDER BY SUBID, SECUENCIA"
                };
                return this.GetMapper<T1>(sqlCommand, conditionals);
            } else if (typeof(T1) == typeof(ClasificacionProductoModel)) {
                sqlCommand = new FbCommand {
                    // consulta para la vista de clasificacion y producto
                    CommandText =
                            @"WITH RECURSIVE Clasificacion (IDCATEGORIA, SUBID,ACTIVO, CLASE, DESCRIPCION, NIVEL, CHILDS) AS
                                (
                                  SELECT CTCAT_ID AS IDCATEGORIA, CTCAT_CTCAT_ID AS SUBID, CTCAT_A AS ACTIVO, CTCAT_NOM AS CLASE, CTCAT_NOM || '/' AS DESCRIPCION, 1,
                                  (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = CTCAT_ID) AS CHILDS
                                    FROM CTCAT
                                    WHERE CTCAT_ID = 1 -- THE TREE NODE
                                  UNION ALL
                                  SELECT T.CTCAT_ID AS IDCATEGORIA, T.CTCAT_CTCAT_ID AS SUBID, (T.CTCAT_A * TP.ACTIVO) AS ACTIVO, T.CTCAT_NOM AS  CLASE, TP.DESCRIPCION || T.CTCAT_NOM || ' | ' AS DESCRIPCION, TP.NIVEL + 1,
                                  (SELECT COUNT(CT.CTCAT_ID) FROM CTCAT CT WHERE CT.CTCAT_CTCAT_ID = T.CTCAT_ID) AS CHILDS
                                    FROM Clasificacion AS TP 
                                     JOIN CTCAT AS T ON TP.IDCATEGORIA = T.CTCAT_CTCAT_ID
                                )
                                SELECT * FROM Clasificacion
                                RIGHT JOIN CTPRD ON Clasificacion.IdCategoria = CTPRD.CTPRD_CTCLS_ID 
                                WHERE CTPRD.CTPRD_A = 1
                                ORDER BY IDCATEGORIA"
                };

                return GetMapper<T1>(sqlCommand, conditionals);
            } 
            throw new NotImplementedException();
        }
    }
}
