﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    /// <summary>
    /// repositorio de medios (imagenes) en los productos
    /// </summary>
    public class SqlFbMediosRepository : Abstractions.RepositoryMaster<MedioModel>, ISqlMediosRepository {
        public SqlFbMediosRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTMDLM SET CTMDLM_A = 0 WHERE CTMDLM_ID = @CTMDLM_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTMDLM_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(MedioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTMDLM ( CTMDLM_ID, CTMDLM_A, CTMDLM_CTMDL_ID, CTMDLM_URL)
                                              VALUES (@CTMDLM_ID,@CTMDLM_A,@CTMDLM_CTMDL_ID,@CTMDLM_URL) RETURNING CTMDLM_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTMDLM_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDLM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTMDLM_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDLM_URL", item.URL);

            item.Id = ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(MedioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTMDLM SET CTMDLM_A = @CTMDLM_A, CTMDLM_CTMDL_ID = @CTMDLM_CTMDL_ID, CTMDLM_URL = @CTMDLM_URL WHERE CTMDLM_ID = @CTMDLM_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTMDLM_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@CTMDLM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTMDLM_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDLM_URL", item.URL);
            return ExecuteTransaction(sqlCommand);
        }

        public MedioModel GetById(int index) {
            return GetList<MedioModel>(new List<IConditional>() { new Conditional("CTMDLM_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<MedioModel> GetList() {
            return GetList<MedioModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CTMDLM @condiciones"
            };
            return GetMapper<T1>(sqlCommand, conditionals);
        }

        public MedioModel Save(MedioModel medio) {
            if (medio.Id == 0) {
                medio.Id = Insert(medio);
            } else {
                Update(medio);
            }
            return medio;
        }
    }
}
