﻿/// develop: anhe1 30062019
/// purpose: Context para catalogos SAT
/// rev.:opcion para recuperar desde los recursos incrustados
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Reflection;
using Newtonsoft.Json;
using Jaeger.Catalogos.ValueObjects;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Abstractions {
    /// <summary>
    /// clase contexto para el manejo de catalogos diversos
    /// </summary>
    /// <typeparam name="T">The type of the T.</typeparam>
    public abstract class CatalogoContext<T> : IGenericCatalogo<T> where T : class, new() {
        #region declaraciones
        private bool recuperarField = true;
        public CatalogoBase<T> catalogo;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public CatalogoContext() {
            this.FileName = "miCatalogo.json";
            this.StartPath = PathsEnum.Catalogos;
            this.catalogo = new CatalogoBase<T>();
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer el nombre del archivo
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// obtener o establecer la version del catalogo
        /// </summary>
        public string Version {
            get {
                return this.catalogo.Version;
            }
            set {
                this.catalogo.Version = value;
            }
        }

        /// <summary>
        /// obtener o establecer titulo del catalogo
        /// </summary>
        public string Title {
            get {
                return this.catalogo.Titulo;
            }
            set {
                this.catalogo.Titulo = value;
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de revision
        /// </summary>
        public string Revision {
            get {
                return this.catalogo.Revision;
            }
            set {
                this.catalogo.Revision = value;
            }
        }

        /// <summary>
        /// obtener o establecer ruta de inicial donde se encuentra el catalogo
        /// </summary>
        public PathsEnum StartPath { get; set; }

        /// <summary>
        /// obtener o establecer si el catalogo debe ser recuperado desde los recursos de la libreria
        /// </summary>
        public bool Recuperar {
            get {
                return this.recuperarField;
            }
            set {
                this.recuperarField = value;
            }
        }

        /// <summary>
        /// obtener o establecer la lista de objetos
        /// </summary>
        public List<T> Items {
            get {
                return this.catalogo.Items;
            }
            set {
                this.catalogo.Items = value;
            }
        }
        #endregion

        #region metodos publicos
        public void Add(T newItem) {
            try {
                if (Items == null) {
                    this.Items = new List<T>();
                }
                T e = this.Items.FirstOrDefault<T>((t) => t == newItem);
                if (e == null)
                    this.Items.Add(newItem);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// eliminar un objeto de la coleccion por la referencia de un objeto
        /// </summary>
        public bool Delete(T deleteItem) {
            try {
                this.Items.Remove(deleteItem);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// eliminar un objeto de la coleccion por referencia del indice
        /// </summary>
        public bool Delete(int index) {
            try {
                this.Items.RemoveAt(index);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// cargar la informacion de un catalogo
        /// </summary>
        public virtual void Load() {
            string localName = this.ResolverName(this.FileName, this.FileName, true);
            if (File.Exists(localName)) {
                StreamReader oStreamReader = new StreamReader(localName);
                string valor = oStreamReader.ReadToEnd();
                oStreamReader.Close();
                if (valor.Length > 0) {
                    var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                    this.catalogo = JsonConvert.DeserializeObject<CatalogoBase<T>>(valor, conf);
                    this.Items = this.catalogo.Items;
                }
                if (this.Items == null)
                    this.Items = new List<T>();
            } else {
                this.Items = new List<T>();
            }
        }

        public virtual void Load(string fileName) {
            var fileInfo = new FileInfo(fileName);
            if (fileInfo.Exists) {
                if (fileInfo.Extension.ToLower() == ".zip") {
                    var valor = this.Unzip(File.ReadAllBytes(fileName));
                    if (!string.IsNullOrEmpty(valor)) {
                        var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                        catalogo = JsonConvert.DeserializeObject<CatalogoBase<T>>(valor, conf);
                        Items = catalogo.Items;
                    }
                } else {

                }
            }
        }

        /// <summary>
        /// cargar la informacion de un catalogo
        /// </summary>
        public void LoadZIP() {
            string localName = this.ResolverName(this.FileName, this.FileName, true);
            if (File.Exists(localName)) {
                StreamReader oStreamReader = new StreamReader(localName);
                string valor = oStreamReader.ReadToEnd();
                oStreamReader.Close();
                if (valor.Length > 0) {
                    var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, DateFormatString = "dd/MM/yyyy" };
                    this.catalogo = JsonConvert.DeserializeObject<CatalogoBase<T>>(valor, conf);
                    this.Items = this.catalogo.Items;
                }
                if (this.Items == null)
                    this.Items = new List<T>();
            } else {
                this.Items = new List<T>();
            }
        }

        /// <summary>
        /// guardar los cambios del catalogo
        /// </summary>
        public bool Save() {
            Encoding utf8WithoutBom = new UTF8Encoding(false);
            File.WriteAllText(this.ResolverName(this.FileName), this.catalogo.ToJson(), utf8WithoutBom);
            return false;
        }

        public bool SaveZIP() {
            var contenido = this.Zip(this.catalogo.ToJson());
            var nombre = Path.ChangeExtension(this.ResolverName(this.FileName), "zip");
            File.WriteAllBytes(nombre, contenido);
            return false;
        }

        /// <summary>
        /// restaurar el catalogo desde el proyecto
        /// </summary>
        public bool Restore() {
            return false;
        }

        public int Import(List<T> items) {
            this.Items = items;
            return this.Items.Count;
        }
        #endregion

        #region metodos privados

        public bool GetResource(string nameResource, string fileName) {
            // sino existe la carpeta la creamos
            if (!(Directory.Exists(Path.GetDirectoryName(fileName)))) {
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            }

            using (Stream oStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("Jaeger.", "Catalogos.Resources.", nameResource + ".zip"))) {
                var d = this.Unzip(this.ReadFully(oStream));
                Encoding utf8WithoutBom = new UTF8Encoding(false);
                File.WriteAllText(fileName, d, utf8WithoutBom);
            }

            return File.Exists(fileName);
        }

        public string ResolverName(string fileName) {
            return JaegerManagerPaths.JaegerPath(this.StartPath, fileName);
        }

        private string ResolverName(string fileName, string fileDefault, bool resource = true) {
            string localName = fileName;
            if (File.Exists(fileName) == false) {
                if (resource) {
                    if (File.Exists(this.ResolverName(fileDefault)) == false) {
                        if (this.Recuperar == true) {
                            if (this.GetResource(fileDefault, this.ResolverName(fileDefault))) {
                                localName = this.ResolverName(fileDefault);
                            }
                        } else {
                            localName = this.ResolverName(fileDefault);
                            this.Save();
                        }
                    } else {
                        localName = this.ResolverName(fileDefault);
                    }
                }
            }
            return localName;
        }

        #region archivo zip
        public byte[] ReadFully(Stream input) {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream()) {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Zips a string into a zipped byte array.
        /// </summary>
        /// <param name="textToZip">The text to be zipped.</param>
        /// <returns>byte[] representing a zipped stream</returns>
        public byte[] Zip(string textToZip) {
            using (var memoryStream = new MemoryStream()) {
                using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true)) {
                    var demoFile = zipArchive.CreateEntry(this.FileName);

                    using (var entryStream = demoFile.Open()) {
                        using (var streamWriter = new StreamWriter(entryStream)) {
                            streamWriter.Write(textToZip);
                        }
                    }
                }

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Unzip a zipped byte array into a string.
        /// </summary>
        /// <param name="zippedBuffer">The byte array to be unzipped</param>
        /// <returns>string representing the original stream</returns>
        public string Unzip(byte[] zippedBuffer) {
            using (var zippedStream = new MemoryStream(zippedBuffer)) {
                using (var archive = new ZipArchive(zippedStream)) {
                    var entry = archive.Entries.FirstOrDefault();

                    if (entry != null) {
                        using (var unzippedEntryStream = entry.Open()) {
                            using (var ms = new MemoryStream()) {
                                unzippedEntryStream.CopyTo(ms);
                                var unzippedArray = ms.ToArray();

                                return Encoding.UTF8.GetString(unzippedArray);
                            }
                        }
                    }

                    return null;
                }
            }
        }
        #endregion
        #endregion
    }
}
