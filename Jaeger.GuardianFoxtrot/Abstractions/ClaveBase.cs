/// purpose: propiedades basicas para un item de un catalogo
using System.ComponentModel;
using Newtonsoft.Json;
using System.Xml.Serialization;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Abstractions {
    /// <summary>
    /// item de catalogo (Clave y Descripcion)
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public abstract class ClaveBase : BasePropertyChangeImplementation, IClaveBase {
        private string claveField;
        private string descripcionField;

        [DisplayName("Clave")]
        [JsonProperty("clv", Order = 0)]
        [XmlAttribute("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Descripción")]
        [JsonProperty("desc", Order = 5)]
        [XmlAttribute("descrip")]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el descriptor de elemento
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        public virtual string Descriptor {
            get { return string.Format("{0}: {1}", this.Clave, this.Descripcion); }
        }
    }
}