﻿/// purpose: propiedades basicas para un item de un catalogo
using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Clave de catalogo, solo contiene fechas de vigencia
    /// </summary>
    public abstract class ClaveBaseVigenciaSingle : BasePropertyChangeImplementation, IClaveBaseVigencia {
        private DateTime? fechaInicioVigenciaField;
        private bool fechaInicioVigenciaFieldSpecified;
        private DateTime? fechaFinVigenciaField;
        private bool fechaFinVigenciaFieldSpecified;

        public ClaveBaseVigenciaSingle() {
            this.VigenciaIni = null;
            this.VigenciaFin = null;
        }

        [Description("Fecha inicio de vigencia")]
        [DisplayName("Fecha inicio de vigencia")]
        [JsonIgnore]
        [XmlIgnore]
        public DateTime? VigenciaIni {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioVigenciaField >= firstGoodDate) {
                    return this.fechaInicioVigenciaField;
                }
                else {
                    return null;
                }
            }
            set {
                this.fechaInicioVigenciaField = value;
                this.fechaInicioVigenciaFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [Description("Fecha fin de vigencia")]
        [DisplayName("Fecha fin de vigencia")]
        [JsonIgnore]
        [XmlIgnore]
        public DateTime? VigenciaFin {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinVigenciaField >= firstGoodDate) {
                    return this.fechaFinVigenciaField;
                }
                else {
                    return null;
                }
            }
            set {
                this.fechaFinVigenciaField = value;
                this.fechaFinVigenciaFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool InicioVigenciaSpecified {
            get {
                return this.fechaInicioVigenciaFieldSpecified;
            }
            set {
                this.fechaInicioVigenciaFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool FinVigenciaSpecified {
            get {
                return this.fechaFinVigenciaFieldSpecified;
            }
            set {
                this.fechaFinVigenciaFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonProperty("vigi", Order = 98)]
        [XmlAttribute("iniVigencia")]
        public string InicioVigenciaX {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioVigenciaField >= firstGoodDate) {
                    return this.fechaInicioVigenciaField.Value.ToString("yyyy-MM-dd");
                }
                else {
                    return null;
                }
            }
            set {
                this.fechaInicioVigenciaField = Convert.ToDateTime(value);
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonProperty("vigf", Order = 99)]
        [XmlAttribute("finVigencia")]
        public string FinVigenciaX {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinVigenciaField >= firstGoodDate) {
                    return this.fechaFinVigenciaField.Value.ToString("yyyy-MM-dd");
                }
                else {
                    return null;
                }
            }
            set {
                this.fechaFinVigenciaField = Convert.ToDateTime(value);
                this.OnPropertyChanged();
            }
        }
    }
}