using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo de tipos de regimen de contracion (nomina)
    /// </summary>
    public class CatalogoTipoRegimen : CatalogoContext<ClaveTipoRegimen>, ITipoRegimenCatalogo {
        public CatalogoTipoRegimen() {
            this.Title = "Catálogo de tipos de régimen de contratación.";
            this.FileName = "CatalogoNominaTipoRegimen.json";
            this.Version = "2.0";
            this.Revision = "0";
        }
    }
}