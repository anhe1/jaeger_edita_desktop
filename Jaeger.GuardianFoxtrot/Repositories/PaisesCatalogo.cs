/// develop: ahe 020720192123
/// purpose: catalogo de paises SAT
using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    public class PaisesCatalogo : CatalogoContext<ClavePais>, IPaisesCatalogo {
        public PaisesCatalogo() {
            this.Title = "Catálogo de Paises";
            this.FileName = "CatalogoPaises.json";
            this.Version = "1.0";
        }

        public ClavePais Search(string findId) {
            ClavePais objeto = new ClavePais();
            objeto = this.Items.SingleOrDefault((ClavePais p) => p.Clave == findId);
            return objeto;
        }
    }
}