﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catálogo tipo de factor para impuestos en CFDI 3.3
    /// </summary>
    public class TipoFactorCatalogo : CatalogoContext<ClaveTipoFactor> {
        public TipoFactorCatalogo() {
            this.Title = "Catálogo tipo factor";
            this.FileName = "CatalogoTipoFactor.json";
            this.Version = "1.0";
            this.Revision = "0";
        }
    }
}
