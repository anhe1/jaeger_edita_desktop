﻿using System.Linq;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Repositories {
    public class BancosCatalogo : CatalogoContext<ClaveBanco>, IBancosCatalogo {
        public BancosCatalogo() {
            this.Title = "Catálogo de Bancos SAT";
            this.FileName = "CatalogoBancos.json";
        }

        public ClaveBanco Search(string findId) {
            var objeto = new ClaveBanco();
            objeto = this.Items.SingleOrDefault((ClaveBanco p) => p.Clave == findId);
            return objeto;
        }
    }
}
