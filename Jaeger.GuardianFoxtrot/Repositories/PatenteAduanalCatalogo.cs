﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catálogo de patentes aduanales
    /// </summary>
    public class PatenteAduanalCatalogo : CatalogoContext<ClavePatenteAduanal>, IPatenteAduanalCatalogo {
        public PatenteAduanalCatalogo() {
            this.Title = "Catálogo de patentes aduanales";
            this.FileName = "CatalogoPatentesAduanales.json";
            this.Revision = "0";
            this.Version = "25.0";
        }
    }
}
