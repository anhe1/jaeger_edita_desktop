/// develop: anhe 020720192115
/// purpose: catalogo de tipos de relación CFDI
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    public class RelacionCFDICatalogo : CatalogoContext<ClaveTipoRelacionCFDI>, IRelacionCFDICatalogo {
        public RelacionCFDICatalogo() {
            this.Title = "Catálogo de tipos de relación entre CFDI.";
            this.FileName = "CatalogoTipoRelacionCFDI.json";
        }

        public ClaveTipoRelacionCFDI Search(string findId) {
            var _search = Regex.Replace(findId, "[^\\d]", "");
            try {
                var _response = new ClaveTipoRelacionCFDI();
                _response = this.Items.SingleOrDefault((ClaveTipoRelacionCFDI p) => p.Clave == _search);
                return _response;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new ClaveTipoRelacionCFDI { Clave = findId };
        }
    }
}