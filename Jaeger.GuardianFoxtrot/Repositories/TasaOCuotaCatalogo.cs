﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Repositories {
    public class TasaOCuotaCatalogo : CatalogoContext<ClaveTasaOCuota>, ITasaOCuotaCatalogo {
        public TasaOCuotaCatalogo() {
            this.Title = "Catálogo de tasas o cuotas de impuestos.";
            this.FileName = "CatalogoTasaOCuota.json";
            this.Version = "2.0";
            this.Revision = "0";
        }
    }
}
