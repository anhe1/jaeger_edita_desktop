﻿using System;
using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    public class UsoCFDICatalogo : CatalogoContext<ClaveUsoCFDI>, IUsoCFDICatalogo {
        public UsoCFDICatalogo() {
            this.Title = "Catálogo de Uso de CFDI";
            this.FileName = "CatalogoUsoCFDI.json";
        }

        public ClaveUsoCFDI Search(string findId) {
            
            try {
                var search = new ClaveUsoCFDI();
                search = this.Items.SingleOrDefault((ClaveUsoCFDI p) => p.Clave == findId.Trim());
                if (search == null)
                    return new ClaveUsoCFDI { Clave = findId };
                return search;

            } catch (System.Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new ClaveUsoCFDI { Clave = findId };
        }
    }
}
