﻿using System.Linq;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catálogo de aduanas (tomado del anexo 22, apéndice I de la RGCE 2017).
    /// </summary>
    public class AduanasCatalogo : CatalogoContext<ClaveAduana>, IAduanasCatalogo {
        public AduanasCatalogo() {
            this.FileName = "CatalogoAduanas.json";
            this.Title = "Catálogo de aduanas (tomado del anexo 22, apéndice I de la RGCE 2017).";
            this.Version = "1.0";
            this.Revision = "1";
        }

        public ClaveAduana Search(string clave) {
            return this.Items.SingleOrDefault((ClaveAduana b) => b.Clave == clave);
        }
    }
}
