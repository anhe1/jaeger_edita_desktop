﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catálogo de tipo de tráfico ferroviario.
    /// </summary>
    public class TipoDeTraficoCatalogo : CatalogoContext<CveTipoDeTrafico>, IClaveTipoDeTraficoCatalogo {
        public TipoDeTraficoCatalogo() {
            this.Title = "Catálogo de tipo de tráfico ferroviario";
            this.FileName = "CatalogoTipoDeTrafico.json";
            this.Version = "";
        }

        public CveTipoDeTrafico Search(string findId) {
            try {
                var search = new CveTipoDeTrafico();
                search = this.Items.SingleOrDefault((CveTipoDeTrafico p) => p.Clave == findId.Trim());
                if (search == null)
                    return new CveTipoDeTrafico { Clave = findId };
                return search;

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new CveTipoDeTrafico { Clave = findId };
        }

        public override void Load() {
            this.Items = new List<CveTipoDeTrafico>() { 
                new CveTipoDeTrafico { Clave = "TT01", Descripcion = "Tráfico local", VigenciaIni = new DateTime(2021, 12, 1) },
                new CveTipoDeTrafico { Clave = "TT02", Descripcion = "Tráfico interlineal remitido", VigenciaIni = new DateTime(2021, 12, 1) },
                new CveTipoDeTrafico { Clave = "TT03", Descripcion = "Tráfico interlineal recibido", VigenciaIni = new DateTime(2021, 12, 1) },
                new CveTipoDeTrafico { Clave = "TT04", Descripcion = "Tráfico interlineal en tránsito", VigenciaIni = new DateTime(2021, 12, 1) }
            };
        }
    }
}
