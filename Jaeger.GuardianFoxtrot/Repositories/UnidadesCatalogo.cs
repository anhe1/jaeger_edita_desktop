﻿using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    public class UnidadesCatalogo : CatalogoContext<ClaveUnidad>, IUnidadesCatalogo {
        public UnidadesCatalogo() {
            this.Title = "Catálogo de unidades de medida para los conceptos en el CFDI.";
            this.FileName = "CatalogoUnidades.json";
        }
        public ClaveUnidad Search(string findId) {
            ClaveUnidad objeto = new ClaveUnidad();
            objeto = this.Items.Find((ClaveUnidad p) => p.Clave == findId);
            return objeto;
        }

        public System.Collections.Generic.IEnumerable<ClaveUnidad> GetSearch(string findId) {
            return this.Items.Where(it => it.Descripcion.Contains(findId)).ToList();
        }
    }
}
