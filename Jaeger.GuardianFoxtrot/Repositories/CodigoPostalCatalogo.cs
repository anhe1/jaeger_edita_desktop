using System.Linq;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo SAT de codigos postales
    /// </summary>
    public class CodigoPostalCatalogo : CatalogoContext<ClaveCodigoPostal>, ICodigosPostalesCatalogo {
        public CodigoPostalCatalogo() {
            this.Title = "Cat�logo de c�digos postales.";
            this.FileName = "CatalogoCodigoPostal.json";
            this.Version = "2.0";
        }

        public ClaveCodigoPostal Search(string find) {
            ClaveCodigoPostal objeto = new ClaveCodigoPostal();
            objeto = this.Items.SingleOrDefault((ClaveCodigoPostal p) => p.Clave == find);
            return objeto;
        }
    }
}