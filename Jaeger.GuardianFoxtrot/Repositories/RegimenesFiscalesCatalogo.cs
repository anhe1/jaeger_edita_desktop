using System;
using System.Linq;
using System.Text.RegularExpressions;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos {
    /// <summary>
    /// Catálogo de régimen fiscal.
    /// </summary>
    public class RegimenesFiscalesCatalogo : CatalogoContext<ClaveRegimenFiscal>, IRegimenesFiscalesCatalogo {
        public RegimenesFiscalesCatalogo() {
            this.Title = "Catálogo Remimenes Fiscales";
            this.FileName = "CatalogoRegimenesFiscales.json";
        }

        public ClaveRegimenFiscal Search(string findId) {
            if (findId != null) {
                string str = Regex.Replace(findId, "[^\\d]", "");
                try {
                    ClaveRegimenFiscal objeto = new ClaveRegimenFiscal();
                    objeto = this.Items.SingleOrDefault((ClaveRegimenFiscal p) => p.Clave == str);
                    return objeto;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return new ClaveRegimenFiscal { Clave = findId };
        }
    }
}