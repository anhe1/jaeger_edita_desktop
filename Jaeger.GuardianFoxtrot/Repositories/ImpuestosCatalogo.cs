﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo de impuestos
    /// </summary>
    public class CatalogoImpuestos : CatalogoContext<ClaveImpuesto>, IImpuestosCatalogo {
        public CatalogoImpuestos() {
            this.Title = "Catálogo de impuestos";
            this.FileName = "CatalogoImpuestos.json";
            this.Version = "1.0";
            this.Revision = "0";
        }
    }
}
