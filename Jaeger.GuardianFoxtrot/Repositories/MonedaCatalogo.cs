using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    public class MonedaCatalogo : CatalogoContext<ClaveMoneda>, IMonedaCatalogo {
        public MonedaCatalogo() {
            this.Title = "Cat�logo de Monedas";
            this.FileName = "CatalogoMoneda.json";
        }

        public ClaveMoneda Search(string findId) {
            ClaveMoneda objeto = new ClaveMoneda();
            objeto = this.Items.SingleOrDefault((ClaveMoneda p) => p.Clave == findId);
            return objeto;
        }
    }
}