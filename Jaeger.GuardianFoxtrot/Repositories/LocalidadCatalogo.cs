﻿using System;
using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catálogo de localidades. 
    /// </summary>
    public class LocalidadCatalogo : CatalogoContext<CveLocalidad>, IClaveLocalidadCatalogo {
        public LocalidadCatalogo() {
            this.Title = "Catálogo de localidades.";
            this.FileName = "CatalogoLocalidad.json";
            this.Version = "1.0";
        }

        public CveLocalidad Search(string findId) {
            try {
                var search = new CveLocalidad();
                search = this.Items.SingleOrDefault((CveLocalidad p) => p.Clave == findId.Trim());
                if (search == null)
                    return new CveLocalidad { Clave = findId };
                return search;

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new CveLocalidad { Clave = findId };
        }
    }
}
