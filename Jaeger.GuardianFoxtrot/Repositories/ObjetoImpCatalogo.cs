﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catalogo de objeto de impuestos para comprobante fiscal 4.0
    /// </summary>
    public class ObjetoImpCatalogo : CatalogoContext<ClaveObjetoImp>, IObjetoImpCatalogo {
        public ObjetoImpCatalogo() {
            this.Title = "Catalogo Exportación";
            this.FileName = "ObjetoImpCatalogo.json";
            this.Version = "1.0";
        }

        public override void Load() {
            this.Items = new System.Collections.Generic.List<ClaveObjetoImp> {
                new ClaveObjetoImp { Clave = "01", Descripcion = "No objeto de impuesto.", VigenciaIni = new System.DateTime(2022, 1, 1) },
                new ClaveObjetoImp { Clave = "02", Descripcion = "Sí objeto de impuesto.", VigenciaIni = new System.DateTime(2022, 1, 1) },
                new ClaveObjetoImp { Clave = "03", Descripcion = "Sí objeto del impuesto y no obligado al desglose.", VigenciaIni = new System.DateTime(2022, 1, 1) }
            };
        }
    }
}
