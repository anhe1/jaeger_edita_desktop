﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Repositories {
    public class NumPedimentoAduanaCatalogo : CatalogoContext<ClaveNumPedimentoAduana>, INumPedimentoAduanaCatalogo {
        public NumPedimentoAduanaCatalogo() {
            this.Title = "Catálogo de números de pedimento operados por aduana y ejercicio.";
            this.FileName = "CatalogoNumPedimentoAduana.json";
            this.Version = "31.0";
            this.Revision = "0";
        }
    }
}
