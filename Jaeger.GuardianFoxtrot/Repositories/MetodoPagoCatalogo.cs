using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo de metodo de pago
    /// </summary>
    public class MetodoPagoCatalogo : CatalogoContext<ClaveMetodoPago>, IMetodoPagoCatalogo {
        public MetodoPagoCatalogo() {
            this.Title = "Cat�logo Metodo de Pago cfdi 3.3";
            this.FileName = "CatalogoMetodoPago33.json";
        }

        public ClaveMetodoPago Search(string findId) {
            ClaveMetodoPago objeto = new ClaveMetodoPago();
            objeto = this.Items.SingleOrDefault((ClaveMetodoPago p) => p.Clave == findId);
            return objeto;
        }
    }
}