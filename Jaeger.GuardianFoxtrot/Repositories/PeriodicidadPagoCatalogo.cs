/// develop: anhe1 020720192133
/// purpose: Catálogo Periodicidad de Pago para nómina

using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo periodicidad de pago
    /// </summary>
    public class PeriodicidadPagoCatalogo : CatalogoContext<ClavePeriodicidadPago>, IPeriodicidadPagoCatalogo {
        public PeriodicidadPagoCatalogo() {
            this.Title = "Catálogo Periodicidad de Pago";
            this.FileName = "CatalogoNominaPeriodicidadPago.json";
        }
    }
}