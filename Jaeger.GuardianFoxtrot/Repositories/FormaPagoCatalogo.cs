﻿using System.Linq;
using System.Text.RegularExpressions;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo de formas de pago
    /// </summary>
    public class FormaPagoCatalogo : CatalogoContext<ClaveFormaPago>, IFormaPagoCatalogo {
        public FormaPagoCatalogo() {
            this.Title = "Catálogo Forma de Pago";
            this.FileName = "CatalogoFormaPago33.json";
        }

        public ClaveFormaPago Search(string findId) {
            if (findId != null) {
                string str = Regex.Replace(findId, @"/[^\d]/g", "");
                ClaveFormaPago objeto = new ClaveFormaPago();
                objeto = this.Items.SingleOrDefault((ClaveFormaPago p) => p.Clave == str);
                return objeto;
            }
            else {
                return new ClaveFormaPago { Clave = findId };
            }
        }
    }
}
