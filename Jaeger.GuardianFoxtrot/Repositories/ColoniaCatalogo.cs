﻿using System;
using System.Linq;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Catálogo de colonias.
    /// </summary>
    public class ColoniaCatalogo : CatalogoContext<ClaveColonia>, IClaveColoniaCatalogo {
        public ColoniaCatalogo() {
            this.Title = "Catálogo de colonias.";
            this.FileName = "CatalogoColonia.json";
            this.Version = "2.0";
        }

        public ClaveColonia Search(string findId) {
            try {
                var search = new ClaveColonia();
                search = this.Items.SingleOrDefault((ClaveColonia p) => p.Clave == findId.Trim());
                if (search == null)
                    return new ClaveColonia { Clave = findId };
                return search;

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new ClaveColonia { Clave = findId };
        }
    }
}
