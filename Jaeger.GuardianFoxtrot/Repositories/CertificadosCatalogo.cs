﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Repositories {
    public class CertificadosCatalogo : CatalogoContext<Certificate>, ICertificadosCatalogo {
        public CertificadosCatalogo() {
            this.Title = "Catálogo de Certificados";
            this.FileName = "CatalogoCertificados.json";
        }

        /// <summary>
        /// recuperar certificado por el numero de serie
        /// </summary>
        /// <param name="serial">numero de serie del certificado</param>
        /// <returns>objeto Certificate</returns>
        public Certificate Search(string serial) {
            Certificate objeto = new Certificate();
            objeto = this.Items.Find((Certificate p) => p.Serial == serial);
            return (objeto == null ? null : objeto);
        }
    }
}
