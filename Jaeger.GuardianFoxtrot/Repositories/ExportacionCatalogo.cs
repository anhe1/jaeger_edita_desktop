﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Repositories {
    public class ExportacionCatalogo : CatalogoContext<ClaveExportacion>, IExportacionCatalogo {
        public ExportacionCatalogo() {
            this.Title = "Catálogo Exportación";
            this.FileName = "ExportacionCatalogo.json";
            this.Version = "2.0";
            this.Revision = "1";
        }

        public override void Load() {
            this.Items = new System.Collections.Generic.List<ClaveExportacion> {
                new ClaveExportacion{ Clave = "01", Descripcion = "No Aplica", VigenciaIni = new System.DateTime(2022, 1, 1) },
                new ClaveExportacion{ Clave = "02", Descripcion = "Definitiva con clave A1", VigenciaIni = new System.DateTime(2022, 1, 1) },
                new ClaveExportacion{ Clave = "03", Descripcion = "Temporal", VigenciaIni = new System.DateTime(2022, 1, 1)},
                new ClaveExportacion{ Clave = "04", Descripcion = "Definitiva con clave distinta a A1 o cuando no existe enajenación en términos del CFF", VigenciaIni = new System.DateTime(2022, 1, 1)}
            };
        }
    }
}
