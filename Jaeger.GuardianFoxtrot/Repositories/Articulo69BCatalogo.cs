﻿/// develop: anhe 09012020 2130
/// purpose:
//Artículo 69-B, primer y segundo párrafo del CFF
//Cuando la autoridad fiscal detecte que un contribuyente ha estado emitiendo comprobantes sin contar
//con los activos, personal, infraestructura o capacidad material, directa o indirectamente, para
//prestar los servicios o producir, comercializar o entregar los bienes que amparan tales comprobantes, 
//o bien, que dichos contribuyentes se encuentren no localizados, se presumirá la inexistencia de las
//operaciones amparadas en tales comprobantes.

//En este supuesto, procederá a notificar a los contribuyentes que se encuentren en dicha situación a
//través de su buzón tributario, de la página de internet del Servicio de Administración Tributaria,
//así como mediante publicación en el Diario Oficial de la Federación, con el objeto de que aquellos
//contribuyentes puedan manifestar ante la autoridad fiscal lo que a su derecho convenga y aportar la
//documentación e información que consideren pertinentes para desvirtuar los hechos que llevaron a la
//autoridad a notificarlos.Para ello, los contribuyentes interesados contarán con un plazo de quince
//días contados a partir de la última de las notificaciones que se hayan efectuado.

using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// Artículo 69-B, primer y segundo párrafo del CFF
    /// </summary>
    public class Articulo69BCatalogo : CatalogoContext<Articulo69B>, IArticulo69BCatalogo {
        public Articulo69BCatalogo() {
            this.Title = "Artículo 69-B, primer y segundo párrafo del CFF";
            this.FileName = "CatalogoListaNegraArticulo69B31032023.json";
        }

        /// <summary>
        /// retorna un objeto codigo agrupador del catalogo del sat
        /// </summary>
        public Articulo69B Search(string findId) {
            Articulo69B objeto = new Articulo69B();
            objeto = this.Items.Find((Articulo69B p) => p.RFC == findId);
            return objeto;
        }
    }
}
