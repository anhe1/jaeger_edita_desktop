﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Repositories {
    /// <summary>
    /// catalogo de estados
    /// </summary>
    public class EstadoCatalogo : CatalogoContext<ClaveEstado>, IEstadoCatalogo {
        public EstadoCatalogo() {
            this.Title = "Catálogo de Estados";
            this.FileName = "CatalogoNominaEstados.json";
        }
    }
}
