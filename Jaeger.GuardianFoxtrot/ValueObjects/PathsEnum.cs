﻿namespace Jaeger.Catalogos.ValueObjects {
    public enum PathsEnum {
        Accuse,
        Catalogos,
        Comprobantes,
        Downloads,
        Google,
        Log,
        Media,
        Reportes,
        Repositorio,
        Resources,
        SAT,
        Templates,
        Temporal
    }
}