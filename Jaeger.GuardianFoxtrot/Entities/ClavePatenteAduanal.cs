﻿// develop: 210120180123
// purpose: Clave de patente aduanal catalogo SAT
using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClavePatenteAduanal : ClaveBaseVigenciaSingle, IClaveBaseVigencia {
        private string claveField;

        public ClavePatenteAduanal() {
        }

        [Description("Clave")]
        [DisplayName("Clave")]
        [JsonProperty("clv")]
        [XmlAttribute("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
