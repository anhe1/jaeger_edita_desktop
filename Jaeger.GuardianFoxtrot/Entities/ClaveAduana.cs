﻿/// develop: 210120180132
/// purpose: Clave de Aduana, Catalogo SAT
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveAduana : ClaveBaseVigencia, IClaveBaseItem {
    }
}