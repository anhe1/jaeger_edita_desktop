﻿// develop: 210120180129
// purpose: clave de metodo de pago, Catalogo SAT
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    public class ClaveMetodoPago : ClaveBaseVigencia, IClaveBaseItem {

    }
}
