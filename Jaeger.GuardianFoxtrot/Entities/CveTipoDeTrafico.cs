﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de tipo de tráfico ferroviario.
    /// </summary>
    public class CveTipoDeTrafico : ClaveBaseVigencia, IClaveBaseItem {

    }
}
