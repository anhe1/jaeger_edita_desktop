﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de tipo de remolque.
    /// </summary>
    public class CveSubTipoRem : ClaveBaseVigencia, IClaveBaseItem {

    }
}
