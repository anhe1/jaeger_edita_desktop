﻿using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Nomina: Catalogo de otro tipo de pago.
    /// </summary>
    public class ClaveTipoOtroPago : ClaveBaseVigencia, IClaveBaseItem {
        public ClaveTipoOtroPago() {

        }
    }
}
