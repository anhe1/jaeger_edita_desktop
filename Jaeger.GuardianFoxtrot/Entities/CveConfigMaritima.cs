﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de configuración marítima.
    /// </summary>
    public class CveConfigMaritima : ClaveBaseVigencia, IClaveBaseItem {

    }
}
