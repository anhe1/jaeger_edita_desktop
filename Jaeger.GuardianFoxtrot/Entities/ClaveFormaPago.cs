﻿// develop: 210120180130
// purpose: Clave Forma de Pago v. 3.3, Catalogo SAT
using System.ComponentModel;
using Jaeger.Catalogos.Contracts;
using Newtonsoft.Json;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    public class ClaveFormaPago : ClaveBaseVigencia, IClaveBaseItem {
        #region declaraciones
        private string bancarizadoField;
        private string numOperacionField;
        private string rfcEmisorCtaOrdenanteField;
        private string ctaOrdenanteField;
        private string patronCtaOrdenanteField;
        private string rfcEmisorCtaBeneficiarioField;
        private string ctaDelBeneficiarioField;
        private string patronCtaBeneficiariaField;
        private string tipoCadPagoField;
        private string nombreBancoEmisorCtaOrdenanteField;
        #endregion

        [Description("Bancarizado")]
        [DisplayName("Bancarizado")]
        [JsonProperty("bancarizado")]
        public string Bancarizado {
            get {
                return this.bancarizadoField;
            }
            set {
                this.bancarizadoField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Número de operación")]
        [DisplayName("Número de operación")]
        [JsonProperty("numOperacion")]
        public string NumOperacion {
            get {
                return this.numOperacionField;
            }
            set {
                this.numOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("RFC del Emisor de la cuenta ordenante")]
        [DisplayName("RFC del Emisor de la cuenta ordenante")]
        [JsonProperty("rfcEmisorCtaOrd")]
        public string RfcEmisorCtaOrdenante {
            get {
                return this.rfcEmisorCtaOrdenanteField;
            }
            set {
                this.rfcEmisorCtaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Cuenta Ordenante")]
        [DisplayName("Cuenta Ordenante")]
        [JsonProperty("ctaOrdenante")]
        public string CtaOrdenante {
            get {
                return this.ctaOrdenanteField;
            }
            set {
                this.ctaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Patrón para cuenta ordenante")]
        [DisplayName("Patrón para cuenta ordenante")]
        [JsonProperty("patronCtaOrd")]
        public string PatronCtaOrdenante {
            get {
                return this.patronCtaOrdenanteField;
            }
            set {
                this.patronCtaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("RFC del Emisor Cuenta de Beneficiario")]
        [DisplayName("RFC del Emisor Cuenta de Beneficiario")]
        [JsonProperty("rfcEmisotBenef")]
        public string RfcEmisorCtaBeneficiario {
            get {
                return this.rfcEmisorCtaBeneficiarioField;
            }
            set {
                this.rfcEmisorCtaBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Cuenta de Benenficiario")]
        [DisplayName("Cuenta de Benenficiario")]
        [JsonProperty("ctaDelBenef")]
        public string CtaDelBeneficiario {
            get {
                return this.ctaDelBeneficiarioField;
            }
            set {
                this.ctaDelBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Patrón para cuenta Beneficiaria")]
        [DisplayName("Patrón para cuenta Beneficiaria")]
        [JsonProperty("patronCtaBenef")]
        public string PatronCtaBeneficiaria {
            get {
                return this.patronCtaBeneficiariaField;
            }
            set {
                this.patronCtaBeneficiariaField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Tipo Cadena Pago")]
        [DisplayName("Tipo Cadena Pago")]
        [JsonProperty("tipoCadPago")]
        public string TipoCadenaPago {
            get {
                return this.tipoCadPagoField;
            }
            set {
                this.tipoCadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombre del Banco emisor de la cuenta ordenante en caso de extranjero
        /// </summary>
        [Description("Nombre del Banco emisor de la cuenta ordenante en caso de extranjero")]
        [DisplayName("Nombre del Banco Emisor Extranjero")]
        [JsonProperty("nombreBancoEmisorCtaOrdenante")]
        public string NombreBancoEmisorCtaOrdenante {
            get {
                return this.nombreBancoEmisorCtaOrdenanteField;
            }
            set {
                this.nombreBancoEmisorCtaOrdenanteField = value;
                this.OnPropertyChanged("NombreBancoEmisorCtaOrdenante");
            }
        }

    }
}
