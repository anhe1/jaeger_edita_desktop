﻿using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// catalogo de periodicidad de retenciones
    /// </summary>
    public class ClaveRetencionPeriodicidad : ClaveBaseVigencia, IClaveBaseItem {
    }
}
