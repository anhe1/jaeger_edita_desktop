﻿// develop: 210120180112
// purpose: definir tasa o cuota del impuesto
using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.ValueObjects;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catálogo de tasas o cuotas de impuestos.
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveTasaOCuota : ClaveBaseVigenciaSingle, IClaveBaseVigencia {
        private EnumRangoOFijo rangoOFijoField;
        private EnumFactor factorField;
        private bool valorMinimoFieldSpecified;
        private bool trasladoField;
        private bool retencionField;
        private decimal valorMinimoField;
        private decimal valorMaximoField;
        private string impuestoField;

        [DisplayName("Rango ó Fijo")]
        [JsonProperty("rng")]
        [XmlAttribute("rangoOFijo")]
        public EnumRangoOFijo RangoOFijo {
            get {
                return this.rangoOFijoField;
            }
            set {
                this.rangoOFijoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Valor mínimo")]
        [JsonProperty("min")]
        [XmlAttribute("valMinimo")]
        public decimal ValorMinimo {
            get {
                return this.valorMinimoField;
            }
            set {
                this.valorMinimoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Valor máximo")]
        [JsonProperty("max")]
        [XmlAttribute("valMaximo")]
        public decimal ValorMaximo {
            get {
                return this.valorMaximoField;
            }
            set {
                this.valorMaximoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Impuesto")]
        [JsonProperty("imp")]
        [XmlAttribute("impuesto")]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Factor")]
        [JsonProperty("fac")]
        [XmlAttribute("factor")]
        public EnumFactor Factor {
            get {
                return this.factorField;
            }
            set {
                this.factorField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Traslado")]
        [JsonProperty("tra")]
        [XmlAttribute("traslado")]
        public bool Traslado {
            get {
                return this.trasladoField;
            }
            set {
                this.trasladoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Retención")]
        [JsonProperty("ret")]
        [XmlAttribute("retencion")]
        public bool Retencion {
            get {
                return this.retencionField;
            }
            set {
                this.retencionField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool ValorMinimoSpecified {
            get {
                return this.valorMinimoFieldSpecified;
            }
            set {
                this.valorMinimoFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

    }
}
