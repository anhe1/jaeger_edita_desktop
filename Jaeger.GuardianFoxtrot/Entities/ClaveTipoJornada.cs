﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Nomina: Catalogo de tipos de jornada laboral.
    /// </summary>
    public class ClaveTipoJornada : ClaveBase, IClaveBase {
        public ClaveTipoJornada() {
        }
    }
}
