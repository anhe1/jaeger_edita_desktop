﻿using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catalogo de uso de comprobantes
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveUsoCFDI : ClaveBaseVigencia, IClaveBaseItem {
        private string fisicaField;
        private string moralField;
        private string regimenFiscalReceptorField;

        public ClaveUsoCFDI() : base() {
        }

        [DisplayName("Fisica")]
        [JsonProperty("fisica")]
        [XmlAttribute("fisica")]
        public string Fisica {
            get {
                return this.fisicaField;
            }
            set {
                this.fisicaField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Moral")]
        [JsonProperty("moral")]
        [XmlAttribute("moral")]
        public string Moral {
            get {
                return this.moralField;
            }
            set {
                this.moralField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Regimen Fiscal Receptor")]
        [JsonProperty("regimenFiscal")]
        [XmlAttribute("regFis")]
        public string RegimenFiscalReceptor {
            get { return this.regimenFiscalReceptorField; }
            set {
                this.regimenFiscalReceptorField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool FisicaX {
            get {
                return this.fisicaField.ToLower().Contains("s");
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool MotalX {
            get {
                return this.moralField.ToLower().Contains("n");
            }
        }
    }
}
