﻿using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catalogo de Nomina: Estados
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveEstado : BasePropertyChangeImplementation {
        private string claveField;
        private string descripcionField;
        private string nombreField;

        public ClaveEstado() {
        }

        /// <summary>
        /// codigo del banco
        /// </summary>
        [DisplayName("Estado")]
        [JsonProperty("clv")]
        [XmlAttribute("estado")]
        public string Estado {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged("Estado");
            }
        }

        [DisplayName("País")]
        [JsonProperty("pais")]
        [XmlAttribute("pais")]

        public string Pais {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged("Pais");
            }
        }

        [DisplayName("Nombre del Estado")]
        [JsonProperty("nom")]
        [XmlAttribute("nombre")]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }
    }
}