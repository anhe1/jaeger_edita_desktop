﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Nomina: Catalogo Tipos de Nomina
    /// </summary>
    public class ClaveTipoNomina : ClaveBase, IClaveBase {
        public ClaveTipoNomina() {
        }
    }
}
