﻿// develop: 250120181816
// purpose: clave para el catalogo de codigos postales SAT
using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Jaeger.Catalogos.Entities {
    [JsonObject]
    [XmlRoot("item")]
    public class ClaveCodigoPostal : ClaveBaseVigenciaSingle {
        private string claveField;
        private string estadoField;
        private string municipioField;
        private string localidadField;
        private int estimuloField;

        public ClaveCodigoPostal() {
            this.claveField = string.Empty;
            this.estadoField = string.Empty;
            this.municipioField = string.Empty;
            this.localidadField = string.Empty;
        }

        [DisplayName("CodigoPostal")]
        [JsonProperty("cod")]
        [XmlAttribute("codigo")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Estado")]
        [JsonProperty("est")]
        [XmlAttribute("estado")]
        public string Estado {
            get {
                return this.estadoField;
            }
            set {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Municipio")]
        [JsonProperty("mun")]
        [XmlAttribute("municipio")]
        public string Municipio {
            get {
                return this.municipioField;
            }
            set {
                this.municipioField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Localidad")]
        [JsonProperty("loc")]
        [XmlAttribute("localidad")]
        public string Localidad {
            get {
                return this.localidadField;
            }
            set {
                this.localidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Estímulo Franja Fronteriza
        /// </summary>
        [DisplayName("Estímulo Franja Fronteriza")]
        [JsonProperty("esti")]
        [XmlAttribute("esti")]
        public int Estimulo {
            get {
                return this.estimuloField;
            }
            set {
                this.estimuloField = value;
                this.OnPropertyChanged();
            }
        }

    }
}