﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de localidades. 
    /// </summary>
    public class CveLocalidad : ClaveBase, IClaveBase {
        private string _Estado;

        public CveLocalidad() : base() {

        }

        public string Estado {
            get { return this._Estado; }
            set { this._Estado = value;
                this.OnPropertyChanged();
            }
        }
    }
}
