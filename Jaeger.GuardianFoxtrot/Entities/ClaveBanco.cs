﻿// develop: 161120162142
// purpose: clase para contener elemento del catalogo de bancos SAT
// rev.: 180120182344: actulizamos la clase
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    public class ClaveBanco : ClaveBaseVigencia, IClaveBaseItem {
        private string razonSocialField;

        [DisplayName("Razon Social")]
        [JsonProperty("rso", Order = 15)]
        public string RazonSocial {
            get {
                return this.razonSocialField;
            }
            set {
                this.razonSocialField = value;
                this.OnPropertyChanged();
            }
        }

    }
}