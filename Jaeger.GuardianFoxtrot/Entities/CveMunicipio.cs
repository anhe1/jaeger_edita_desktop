﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de municipios.
    /// </summary>
    public class CveMunicipio : ClaveBase, IClaveBase {
        public string Estado { get; set; }
    }
}
