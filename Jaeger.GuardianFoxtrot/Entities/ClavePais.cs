﻿// develop: 210120180127
// purpose: Clave de pais, catalogo SAT
using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClavePais : ClaveBase, IClaveBase {
        private string formatoCodigoField;
        private bool formatoCodigoFieldSpecified;
        private string formatoRegistroIdenField;
        private bool formatoRegistroIdenFieldSpecified;
        private string validacionRegistroIdenField;
        private bool validacionRegistroIdenFieldSpecified;
        private string agrupacionesField;
        private bool agrupacionesFieldSpecified;

        [DisplayName("Formato de Código Postal")]
        [JsonProperty("frmc")]
        [XmlAttribute("formatoCodigoPostal")]
        public string FormatoCodigoPostal {
            get {
                return this.formatoCodigoField;
            }
            set {
                this.formatoCodigoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Formato de Registro de Identidad Tributaria")]
        [JsonProperty("frmr")]
        [XmlAttribute("formatoRegIden")]
        public string FormatoRegistroIden {
            get {
                return this.formatoRegistroIdenField;
            }
            set {
                this.formatoRegistroIdenField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Validación del Registro de Identidad Tributaria")]
        [JsonProperty("valr")]
        [XmlAttribute("valRegIden")]
        public string ValidacionRegistroIden {
            get {
                return this.validacionRegistroIdenField;
            }
            set {
                this.validacionRegistroIdenField = value;
                this.OnPropertyChanged();
            }
        }

        [Description("Agrupaciones")]
        [DisplayName("Agrupaciones")]
        [JsonProperty("agrp")]
        [XmlAttribute("agrupaciones")]
        public string Agrupaciones {
            get {
                return this.agrupacionesField;
            }
            set {
                this.agrupacionesField = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool FormatoCodigoSpecified {
            get {
                return this.formatoCodigoFieldSpecified;
            }
            set {
                this.formatoCodigoFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool FormatoRegistroIdenSpecified {
            get {
                return this.formatoRegistroIdenFieldSpecified;
            }
            set {
                this.formatoRegistroIdenFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [XmlIgnore]
        [JsonIgnore]
        public bool ValidacionRegistroIdenSpecified {
            get {
                return this.validacionRegistroIdenFieldSpecified;
            }
            set {
                this.validacionRegistroIdenFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool AgrupacionesSpecified {
            get {
                return this.agrupacionesFieldSpecified;
            }
            set {
                this.agrupacionesFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
    }
}
