﻿// develop: 200120180150
// purpose: clase del catalogo de impuestos SAT para CFDI 3.3
using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    public class ClaveImpuesto : ClaveBase, IClaveBase {
        private string retencionField;
        private string trasladoField;
        private string localFederalField;
        private string entidadField;

        [JsonProperty("ret")]
        [XmlAttribute("retencion")]
        public string RetencionX {
            get {
                return this.retencionField;
            }
            set {
                this.retencionField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Traslado")]
        [JsonProperty("tra")]
        [XmlAttribute("traslado")]
        public string TrasladoX {
            get {
                return this.trasladoField;
            }
            set {
                this.trasladoField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Local ó Federal")]
        [JsonProperty("loc")]
        [XmlAttribute("LocalFederal")]
        public string LocalFederal {
            get {
                return this.localFederalField;
            }
            set {
                this.localFederalField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Entidad en la que aplica")]
        [JsonProperty("ent")]
        [XmlAttribute("entidad")]
        public string Entidad {
            get {
                return this.entidadField;
            }
            set {
                this.entidadField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
