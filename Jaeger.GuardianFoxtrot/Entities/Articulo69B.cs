﻿//Artículo 69-B, primer y segundo párrafo del CFF
//Cuando la autoridad fiscal detecte que un contribuyente ha estado emitiendo comprobantes sin contar
//con los activos, personal, infraestructura o capacidad material, directa o indirectamente, para
//prestar los servicios o producir, comercializar o entregar los bienes que amparan tales comprobantes, 
//o bien, que dichos contribuyentes se encuentren no localizados, se presumirá la inexistencia de las
//operaciones amparadas en tales comprobantes.

//En este supuesto, procederá a notificar a los contribuyentes que se encuentren en dicha situación a
//través de su buzón tributario, de la página de internet del Servicio de Administración Tributaria,
//así como mediante publicación en el Diario Oficial de la Federación, con el objeto de que aquellos
//contribuyentes puedan manifestar ante la autoridad fiscal lo que a su derecho convenga y aportar la
//documentación e información que consideren pertinentes para desvirtuar los hechos que llevaron a la
//autoridad a notificarlos.Para ello, los contribuyentes interesados contarán con un plazo de quince
//días contados a partir de la última de las notificaciones que se hayan efectuado.
using System;
using Newtonsoft.Json;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Artículo 69-B, primer y segundo párrafo del CFF
    /// </summary>
    public class Articulo69B {
        public Articulo69B() {
            this.Presunto = new Presunto();
            this.Desvirtuado = new Desvirtuado();
            this.Definitivo = new Definitivo();
            this.Sentencia = new Sentencia();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// obtener o establecer el Registro Federal de Contribuyentes
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del contribuyente
        /// </summary>
        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer la situación del contribuyente
        /// </summary>
        [JsonProperty("situacion")]
        public string Situacion { get; set; }

        [JsonProperty("presunto")]
        public Presunto Presunto { get; set; }

        [JsonProperty("desvirtuado")]
        public Desvirtuado Desvirtuado { get; set; }

        [JsonProperty("definitivo")]
        public Definitivo Definitivo { get; set; }

        private Sentencia sentencia;
        [JsonProperty("sentencia")]
        public Sentencia Sentencia {
            get {
                if (sentencia != null)
                    return sentencia;
                return null;
            }
            set { sentencia = value; }
        }
    }

    public partial class Presunto {
        /// <summary>
        /// obtener o establecer número y fecha de oficio global de presunción
        /// </summary>
        [JsonProperty("glo")]
        public string NoOficioGlobal { get; set; }

        /// <summary>
        /// obtener o establecer fecha de publicación página SAT presuntos
        /// </summary>
        [JsonProperty("sat")]
        public DateTime? FechaPublicacionSAT { get; set; }

        /// <summary>
        /// Número y fecha de oficio global de presunción
        /// </summary>
        [JsonProperty("of")]
        public string NoOficio { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de publicación DOF presuntos
        /// </summary>
        [JsonProperty("dof")]
        public DateTime? PublicacionDOF { get; set; }
    }

    public partial class Desvirtuado {
        private string noOficio;

        /// <summary>
        /// obtener o establecer Publicación página SAT desvirtuados
        /// </summary>
        [JsonProperty("sat")]
        public DateTime? FechaPublicacionSAT { get; set; }

        /// <summary>
        /// obtener o establecer número y fecha de oficio global de contribuyentes que desvirtuaron
        /// </summary>
        [JsonProperty("of")]
        public string NoOficio {
            get {
                if (!string.IsNullOrEmpty(noOficio))
                    return noOficio;
                return null;
            }
            set { noOficio = value; }
        }

        /// <summary>
        /// obtener o establecer la fecha de publicación DOF desvirtuados
        /// </summary>
        [JsonProperty("dof")]
        public DateTime? PublicacionDOF { get; set; }
    }

    public partial class Definitivo {
        private string noOficio;
        private string fechaPublicacionSAT;
        private string publicacionDOF;

        /// <summary>
        /// obtener o establecer el número y fecha de oficio global de definitivos
        /// </summary>
        [JsonProperty("of")]
        public string NoOficio {
            get {
                if (!string.IsNullOrEmpty(noOficio))
                    return noOficio;
                return null;
            }
            set { noOficio = value; }
        }

        /// <summary>
        /// obtener o establecer la fecha de publicación página SAT definitivos
        /// </summary>
        [JsonProperty("sat")]
        public string FechaPublicacionSAT {
            get {
                if (!string.IsNullOrEmpty(fechaPublicacionSAT))
                    return fechaPublicacionSAT;
                return null;
            }
            set { fechaPublicacionSAT = value; }
        }

        /// <summary>
        /// obtener o establecer la fecha de publicación DOF definitivos
        /// </summary>
        [JsonProperty("dof")]
        public string PublicacionDOF {
            get {
                if (!string.IsNullOrEmpty(publicacionDOF))
                    return publicacionDOF;
                return null;
            }
            set { publicacionDOF = value; }
        }
    }

    public partial class Sentencia {
        private string noOficio;
        private string fechaPublicacionSAT;
        private string noOficio2;
        private string publicacionDOF;

        /// <summary>
        /// obtener o establecer el número y fecha de oficio global de sentencia favorable
        /// </summary>
        [JsonProperty("of")]
        public string NoOficio {
            get {
                if (!string.IsNullOrEmpty(noOficio))
                    return noOficio;
                return null;
            }
            set { noOficio = value; }
        }

        /// <summary>
        /// obtener o establecer la fecha de publicación página SAT sentencia favorable
        /// </summary>
        [JsonProperty("sat")]
        public string FechaPublicacionSAT {
            get {
                if (!string.IsNullOrEmpty(fechaPublicacionSAT))
                    return fechaPublicacionSAT;
                return null;
            }
            set { fechaPublicacionSAT = value; }
        }

        /// <summary>
        /// obtener o establecer número y fecha de oficio global de sentencia favorable
        /// </summary>
        [JsonProperty("of2")]
        public string NoOficio2 {
            get {
                if (!string.IsNullOrEmpty(noOficio2))
                    return noOficio2;
                return null;
            }
            set { noOficio2 = value; }
        }

        /// <summary>
        /// obtener o establecer la fecha de publicación DOF sentencia favorable
        /// </summary>
        [JsonProperty("dof")]
        public string PublicacionDOF {
            get {
                if (!string.IsNullOrEmpty(publicacionDOF))
                    return publicacionDOF;
                return null;
            }
            set { publicacionDOF = value; }
        }
    }
}
