﻿// develop: 210120180059
// purpose: tipo de factor aplicable al impuesto
using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Abstractions;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catalogo tipo de factor
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveTipoFactor : BasePropertyChangeImplementation {
        private string claveField;

        public ClaveTipoFactor() {
        }

        [Description("Clave")]
        [DisplayName("Clave")]
        [JsonProperty("clv")]
        [XmlAttribute("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
