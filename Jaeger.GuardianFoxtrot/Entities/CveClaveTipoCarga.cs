﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo del tipo de carga.
    /// </summary>
    public class CveClaveTipoCarga : ClaveBaseVigencia, IClaveBaseItem {

    }
}
