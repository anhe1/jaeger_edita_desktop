﻿// develop: 190120180051
// purpose: tipo de relacion CFDI ver. 3.3
using System.Xml.Serialization;
using Jaeger.Catalogos.Contracts;
using Newtonsoft.Json;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catalogo de tipos de relacion entre CFDI
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveTipoRelacionCFDI : ClaveBaseVigencia, IClaveBaseItem {
        public ClaveTipoRelacionCFDI() {

        }
    }
}
