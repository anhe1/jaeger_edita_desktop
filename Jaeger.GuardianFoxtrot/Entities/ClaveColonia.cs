﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de colonias.
    /// </summary>
    public class ClaveColonia : ClaveBase, IClaveBase {
        public string CodigoPostal { get; set; }
    }
}
