﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo código transporte aéreo.
    /// </summary>
    public class CveCodigoTransporteAereo : ClaveBaseVigencia, IClaveBaseItem {

    }
}
