﻿// develop: 070720171730
// purpose: Catálogo de unidades de medida para los conceptos en el CFDI. Catalogo SAT

using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catálogo de unidades de medida para los conceptos en el CFDI.
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveUnidad : ClaveBaseVigencia, IClaveBaseItem {
        private string nombreField;
        private string notasField;
        private string simboloField;
        private bool simboloFieldSpecified;

        public ClaveUnidad() {
        }

        [DisplayName("Nombre")]
        [JsonProperty("nom")]
        [XmlAttribute("nombre")]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Notas")]
        [JsonProperty("nota")]
        [XmlAttribute("notas")]
        public string Notas {
            get {
                return this.notasField;
            }
            set {
                this.notasField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Símbolo")]
        [JsonProperty("sim")]
        [XmlAttribute("sim")]
        public string Simbolo {
            get {
                return this.simboloField;
            }
            set {
                this.simboloField = value;
                this.simboloFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool SimboloSpecified {
            get {
                return this.simboloFieldSpecified;
            }
            set {
                this.simboloFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [XmlIgnore]
        public override string Descriptor {
            get { return string.Format("{0}: {1}", this.Clave, this.Nombre); }
        }
    }
}