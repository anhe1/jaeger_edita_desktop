﻿// develop: 240220222107
// purpose: catalogo SAT
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Catálogo de contenedores marítimos.
    /// </summary>
    public class CveContenedorMaritimo : ClaveBaseVigencia, IClaveBaseItem {

    }
}
