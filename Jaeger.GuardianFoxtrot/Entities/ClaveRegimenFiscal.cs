﻿// develop: 210120180116
// purpose: Clave de Regimen Fiscal
using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    [XmlRoot("item")]

    public class ClaveRegimenFiscal : ClaveBaseVigencia, IClaveBaseItem {
        private string fisicaField;
        private string moralField;

        public ClaveRegimenFiscal() {
        }

        [JsonProperty("fisica")]
        [XmlAttribute("fisica")]
        public string FisicaX {
            get {
                return this.fisicaField;
            }
            set {
                this.fisicaField = value;
                this.OnPropertyChanged("FisicaX");
            }
        }

        [JsonProperty("moral")]
        [XmlAttribute("moral")]
        public string MoralX {
            get {
                return this.moralField;
            }
            set {
                this.moralField = value;
                this.OnPropertyChanged("MoralX");
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool Fisica {
            get {
                return !this.fisicaField.ToLower().Contains("no");
            }
            set {
                if (!value) {
                    this.fisicaField = "No";
                }
                else {
                    this.fisicaField = "Si";
                }
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [JsonIgnore]
        [XmlIgnore]
        public bool Moral {
            get {
                return !this.moralField.ToLower().Contains("no");
            }
            set {
                if (!value) {
                    this.moralField = "No";
                }
                else {
                    this.moralField = "Si";
                }
                this.OnPropertyChanged("Moral");
            }
        }
    }
}
