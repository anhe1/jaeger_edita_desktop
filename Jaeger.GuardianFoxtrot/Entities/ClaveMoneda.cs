﻿// develop: 210120180128
// purpose: Clave de Moneda, catalogo SAT
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catalogo de monedas
    /// </summary>
    [JsonObject("item")]
    public class ClaveMoneda : ClaveBaseVigencia, IClaveBaseItem {
        private int decimalesField;
        private int porcentajeVariacionField;

        public ClaveMoneda() {
        }

        [DisplayName("Decimales")]
        [JsonProperty("dec")]
        public int Decimales {
            get {
                return this.decimalesField;
            }
            set {
                this.decimalesField = value;
                this.OnPropertyChanged();
            }
        }

        [DisplayName("Porcentaje variación")]
        [JsonProperty("var")]
        public int PorcentajeVariacion {
            get {
                return this.porcentajeVariacionField;
            }
            set {
                this.porcentajeVariacionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
