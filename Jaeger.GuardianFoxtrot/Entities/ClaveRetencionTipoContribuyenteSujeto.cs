﻿using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Tipo de contribuyente sujeto a retención.
    /// </summary>
    public class ClaveRetencionTipoContribuyenteSujeto : ClaveBaseVigencia, IClaveBaseItem {
    }
}
