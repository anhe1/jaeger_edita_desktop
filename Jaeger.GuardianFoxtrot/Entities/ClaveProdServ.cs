﻿// develop: 070720171127
// purpose: Producto o Servicio (Catalogo SAT)
using System.ComponentModel;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catalogo de productos y servicios.
    /// </summary>
    [XmlRoot("item")]
    [JsonObject("item")]
    public class ClaveProdServ : ClaveBaseVigencia, IClaveBaseItem {
        private string incluirIvaTrasladadoField;
        private string incluirIepsTrasladadoField;
        private string complementoField;
        private bool complementoFieldSpecified;
        private string palabrasSimilaresField;
        private bool palabrasSimilaresFieldSpecified;
        private int estimuloField;

        public ClaveProdServ() {
        }

        [XmlAttribute("iva")]
        [JsonProperty("iva")]
        public string IncluirIvaTrasladado {
            get {
                return this.incluirIvaTrasladadoField;
            }
            set {
                this.incluirIvaTrasladadoField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("ieps")]
        [JsonProperty("ieps")]
        public string IncluirIepsTrasladado {
            get {
                return this.incluirIepsTrasladadoField;
            }
            set {
                this.incluirIepsTrasladadoField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("comp")]
        [JsonProperty("comp")]
        public string Complemento {
            get {
                return this.complementoField;
            }
            set {
                this.complementoField = value;
                this.complementoFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("est")]
        public int Estimulo {
            get {
                return this.estimuloField;
            }
            set {
                this.estimuloField = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute("palSim")]
        [JsonProperty("sim")]
        public string PalabrasSimilares {
            get {
                return this.palabrasSimilaresField;
            }
            set {
                this.palabrasSimilaresField = value;
                this.palabrasSimilaresFieldSpecified = true;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        [JsonIgnore]
        public bool ComplementoSpecified {
            get {
                return this.complementoFieldSpecified;
            }
            set {
                this.complementoFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [XmlIgnore]
        [JsonIgnore]
        public bool PalabrasSimilaresSpecified {
            get {
                return this.palabrasSimilaresFieldSpecified;
            }
            set {
                this.palabrasSimilaresFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }
    }
}