﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Nomina: Catalogo de tipos de hora extra.
    /// </summary>
    public class ClaveTipoHoras : ClaveBase, IClaveBase {
    }
}
