﻿using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    [JsonObject("item")]
    public class ClaveExportacion : ClaveBaseVigencia, IClaveBaseItem {

    }
}
