﻿using Jaeger.Catalogos.Abstractions;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// Nomina: Catalogo de tipo de incapacidad
    /// </summary>
    public class ClaveTipoIncapacidad : ClaveBase, IClaveBase {
        public ClaveTipoIncapacidad() {
        }
    }
}
