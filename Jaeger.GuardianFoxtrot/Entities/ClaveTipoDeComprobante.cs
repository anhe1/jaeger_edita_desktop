﻿// develop: 210120180100
// purpose: clave tipo de comprobantes fiscales v. 3.3
using System.Xml.Serialization;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Tipos de Comprobantes
    /// </summary>
    [JsonObject("item")]
    [XmlRoot("item")]
    public class ClaveTipoDeComprobante : ClaveBaseVigencia, IClaveBaseItem {
        private decimal valorMaximoField;

        /// <summary>
        /// constructor
        /// </summary>
        public ClaveTipoDeComprobante() {
        }

        [Description("Valor Máximo")]
        [DisplayName("Valor Máximo")]
        [JsonProperty("max")]
        [XmlAttribute("valorMax")]
        public decimal ValorMaximo {
            get {
                return this.valorMaximoField;
            }
            set {
                this.valorMaximoField = value;
                this.OnPropertyChanged("ValorMaximo");
            }
        }
    }
}
