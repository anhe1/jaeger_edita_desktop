﻿using Newtonsoft.Json;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Entities {
    /// <summary>
    /// CFDI: Catálogo de números de pedimento operados por aduana y ejercicio.
    /// </summary>
    [JsonObject("item")]
    public class ClaveNumPedimentoAduana : ClaveBaseVigenciaSingle, IClaveBaseVigencia {
        private string claveField;
        private string patenteField;
        private int ejercicioField;
        private int cantidadField;

        [JsonProperty("clv")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("pat")]
        public string Patente {
            get {
                return this.patenteField;
            }
            set {
                this.patenteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("eje")]
        public int Ejercicio {
            get {
                return this.ejercicioField;
            }
            set {
                this.ejercicioField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("can")]
        public int Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
                this.OnPropertyChanged();
            }
        }

    }
}