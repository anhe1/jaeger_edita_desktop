﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Catálogo de colonias.
    /// </summary>
    public interface IClaveColoniaCatalogo : IGenericCatalogo<ClaveColonia> {
        ClaveColonia Search(string findId);
    }
}
