﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoIncapacidadCatalogo : IGenericCatalogo<ClaveTipoIncapacidad> {
    }
}
