﻿/// develop: anhe 190720191510
/// purpose: interface para claves de catalogo SAT
using System;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Interface para clave SAT simple con vigencia
    /// </summary>
    public interface IClaveBaseVigencia {
        DateTime? VigenciaIni { get; set; }
        DateTime? VigenciaFin { get; set; }
    }
}
