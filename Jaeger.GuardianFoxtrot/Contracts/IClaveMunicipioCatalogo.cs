﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Catálogo de municipios.
    /// </summary>
    public interface IClaveMunicipioCatalogo : IGenericCatalogo<CveMunicipio> {
        CveMunicipio Search(string findId);
    }
}
