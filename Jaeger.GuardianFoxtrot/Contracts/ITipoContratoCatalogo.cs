﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoContratoCatalogo : IGenericCatalogo<ClaveTipoContrato> {
    }
}
