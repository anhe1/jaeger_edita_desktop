﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface INumPedimentoAduanaCatalogo : IGenericCatalogo<ClaveNumPedimentoAduana> {
    }
}
