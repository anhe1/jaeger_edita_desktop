﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoComprobanteCatalogo : IGenericCatalogo<ClaveTipoDeComprobante> {
    }
}
