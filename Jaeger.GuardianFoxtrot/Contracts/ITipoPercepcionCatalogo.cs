﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoPercepcionCatalogo : IGenericCatalogo<ClaveTipoPercepcion>{
    }
}
