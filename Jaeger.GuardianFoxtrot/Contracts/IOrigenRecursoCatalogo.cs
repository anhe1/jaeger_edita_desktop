﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IOrigenRecursoCatalogo : IGenericCatalogo<ClaveOrigenRecurso> {
    }
}
