﻿using System.Collections.Generic;

namespace Jaeger.Catalogos.Contracts {
    public interface IGenericRepository<T> where T : class, new() {
        int Insert(T item);

        int Update(T item);

        bool Delete(int index);

        T GetById(int index);

        List<T> GetList();
    }
}
