﻿using Jaeger.Catalogos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Catalogos.Contracts {
    public interface IRetencionTipoPagoCatalogo : IGenericCatalogo<ClaveRetencionTipoPago> {
    }
}
