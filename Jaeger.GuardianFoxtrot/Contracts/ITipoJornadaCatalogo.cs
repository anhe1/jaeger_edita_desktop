﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoJornadaCatalogo :IGenericCatalogo<ClaveTipoJornada> {
    }
}
