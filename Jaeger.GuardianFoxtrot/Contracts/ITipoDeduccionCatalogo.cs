﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoDeduccionCatalogo : IGenericCatalogo<ClaveTipoDeduccion>{
    }
}
