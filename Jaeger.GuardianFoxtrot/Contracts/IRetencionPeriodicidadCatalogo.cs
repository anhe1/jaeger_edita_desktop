﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IRetencionPeriodicidadCatalogo : IGenericCatalogo<ClaveRetencionPeriodicidad> {
    }
}
