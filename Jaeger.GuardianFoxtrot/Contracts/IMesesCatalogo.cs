﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IMesesCatalogo : IGenericCatalogo<ClaveMeses> {
        ClaveMeses Search(string findId);
    }
}
