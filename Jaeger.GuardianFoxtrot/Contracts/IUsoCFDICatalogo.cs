﻿using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Catálogo de uso de comprobantes.
    /// </summary>
    public interface IUsoCFDICatalogo : IGenericCatalogo<ClaveUsoCFDI> {
        ClaveUsoCFDI Search(string findId);
    }
}
