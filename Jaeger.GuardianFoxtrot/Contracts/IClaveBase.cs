﻿/// develop: anhe 190720191510
/// purpose: interface para claves de catalogo SAT
namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Interface para clave de SAT simple
    /// </summary>
    public interface IClaveBase {
        string Clave { get; set; }
        string Descripcion { get; set; }
    }
}
