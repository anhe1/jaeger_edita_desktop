﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// catalogo de exportacion para comprobante fiscal 4.0
    /// </summary>
    public interface IExportacionCatalogo : IGenericCatalogo<ClaveExportacion> {

    }
}
