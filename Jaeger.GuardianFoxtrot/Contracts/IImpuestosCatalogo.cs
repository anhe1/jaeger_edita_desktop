﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IImpuestosCatalogo : IGenericCatalogo<ClaveImpuesto> {
    }
}
