﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoRegimenCatalogo : IGenericCatalogo<ClaveTipoRegimen> {
    }
}
