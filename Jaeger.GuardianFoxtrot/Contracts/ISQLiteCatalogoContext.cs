﻿/// <summary>
/// develop: anhe 180720192216
/// purpose: interface para la implementacion de catalogos
/// </summary>

namespace Jaeger.Catalogos.Contracts {
    public interface ISQLiteCatalogoContext<T> {
        T Search(string clave);
    }
}
