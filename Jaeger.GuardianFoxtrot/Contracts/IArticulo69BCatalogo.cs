﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IArticulo69BCatalogo : IGenericCatalogo<Articulo69B> {
        Articulo69B Search(string findId);
    }
}
