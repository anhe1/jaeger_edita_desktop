﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Catálogo de tipos de relación entre CFDI.
    /// </summary>
    public interface IRelacionCFDICatalogo : IGenericCatalogo<ClaveTipoRelacionCFDI> {
        ClaveTipoRelacionCFDI Search(string findId);
    }
}
