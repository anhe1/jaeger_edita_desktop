﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoCadenaPagoCatalogo : IGenericCatalogo<ClaveTipoCadenaPago> {
    }
}
