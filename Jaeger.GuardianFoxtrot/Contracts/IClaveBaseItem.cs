﻿/// develop: anhe 190720191510
/// purpose: interface para claves de catalogo SAT

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Interface para clave SAT con vigencia
    /// </summary>
    public interface IClaveBaseItem : IClaveBase, IClaveBaseVigencia {

    }
}
