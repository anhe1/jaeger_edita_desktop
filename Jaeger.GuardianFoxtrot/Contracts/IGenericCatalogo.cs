﻿using System.Collections.Generic;

namespace Jaeger.Catalogos.Contracts {
    public interface IGenericCatalogo<T> where T : class, new() {
        string Version { get; set; }
        string Title { get; set; }
        string Revision { get; set; }

        void Add(T item);

        bool Delete(T deleteItem);

        bool Delete(int index);

        void Load();

        void Load(string fileName);

        void LoadZIP();

        bool Save();

        bool SaveZIP();

        bool Restore();

        int Import(List<T> items);

        List<T> Items { get; set; }
    }
}
