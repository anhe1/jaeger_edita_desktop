﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Catálogo de localidades. 
    /// </summary>
    public interface IClaveLocalidadCatalogo : IGenericCatalogo<CveLocalidad> {
        CveLocalidad Search(string findId);
    }
}
