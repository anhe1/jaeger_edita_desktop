﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoOtroPagoCatalogo : IGenericCatalogo<ClaveTipoOtroPago> {
    }
}
