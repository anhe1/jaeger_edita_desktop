﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoNominaCatalogo :IGenericCatalogo<ClaveTipoNomina> {
    }
}
