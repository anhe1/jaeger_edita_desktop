﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IRetencionesCatalogo : IGenericCatalogo<ClaveRetencion> {
    }
}
