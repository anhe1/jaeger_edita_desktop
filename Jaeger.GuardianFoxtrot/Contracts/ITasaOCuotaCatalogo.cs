﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    /// <summary>
    /// Catálogo de tasas o cuotas de impuestos.
    /// </summary>
    public interface ITasaOCuotaCatalogo : IGenericCatalogo<ClaveTasaOCuota> {
    }
}
