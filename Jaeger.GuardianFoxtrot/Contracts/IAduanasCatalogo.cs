﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IAduanasCatalogo : IGenericCatalogo<ClaveAduana>{
    }
}
