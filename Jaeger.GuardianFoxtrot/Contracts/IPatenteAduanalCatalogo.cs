﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IPatenteAduanalCatalogo : IGenericCatalogo<ClavePatenteAduanal> {
    }
}
