﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IFormaPagoCatalogo : IGenericCatalogo<ClaveFormaPago> {
        ClaveFormaPago Search(string findId);
    }
}
