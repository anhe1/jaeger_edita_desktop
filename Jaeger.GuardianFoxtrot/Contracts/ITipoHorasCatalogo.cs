﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ITipoHorasCatalogo : IGenericCatalogo<ClaveTipoHoras> {
    }
}
