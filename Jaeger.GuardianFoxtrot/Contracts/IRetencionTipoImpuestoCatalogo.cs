﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IRetencionTipoImpuestoCatalogo : IGenericCatalogo<ClaveRetencionTipoImpuesto> {
    }
}
