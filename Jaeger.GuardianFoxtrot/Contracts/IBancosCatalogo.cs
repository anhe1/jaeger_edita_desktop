﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IBancosCatalogo : IGenericCatalogo<ClaveBanco> {
        ClaveBanco Search(string findId);
    }
}
