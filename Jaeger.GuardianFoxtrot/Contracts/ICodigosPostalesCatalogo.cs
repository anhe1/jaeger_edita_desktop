﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface ICodigosPostalesCatalogo : IGenericCatalogo<ClaveCodigoPostal> {
        ClaveCodigoPostal Search(string find);
    }
}
