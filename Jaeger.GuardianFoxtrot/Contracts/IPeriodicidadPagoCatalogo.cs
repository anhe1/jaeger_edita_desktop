﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IPeriodicidadPagoCatalogo : IGenericCatalogo<ClavePeriodicidadPago> {
    }
}
