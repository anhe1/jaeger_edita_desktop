﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IRetencionTipoContribuyenteSujetoCatalogo : IGenericCatalogo<ClaveRetencionTipoContribuyenteSujeto> {
    }
}
