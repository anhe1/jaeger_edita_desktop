﻿using Jaeger.Catalogos.Entities;

namespace Jaeger.Catalogos.Contracts {
    public interface IRiesgoPuestoCatalogo : IGenericCatalogo<ClaveRiesgoPuesto> {
    }
}
