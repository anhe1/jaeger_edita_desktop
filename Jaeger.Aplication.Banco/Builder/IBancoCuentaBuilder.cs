﻿using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.Aplication.Banco.Builder {
    public interface IBancoCuentaBuilder {

    }

    public interface IBancoCuentaBuild {
        IBancoCuentaDetailModel Build();
    }
}
