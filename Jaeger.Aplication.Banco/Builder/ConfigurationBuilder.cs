﻿using System.Collections.Generic;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.Aplication.Banco.Builder {
    /// <summary>
    /// configuracion de parametros de la seccion bancos
    /// </summary>
    public class ConfigurationBuilder : IConfigurationBuilder, IConfiguracionBuild {
        protected internal IConfiguration _IConfiguration;

        public ConfigurationBuilder() {
            this._IConfiguration = new Configuration();
        }

        public IConfiguration Build(List<IParametroModel> parametros) {
            if (parametros == null) { return this._IConfiguration; }
            foreach (var item in parametros) {
                switch (item.Key) {
                    case Domain.Empresa.Enums.ConfigKeyEnum.Folder:
                        this._IConfiguration.Folder = item.Data;
                        break;
                    case Domain.Empresa.Enums.ConfigKeyEnum.Decimales:
                        this._IConfiguration.Decimales = DbConvert.ConvertInt32(item.Data);
                        break;
                    case Domain.Empresa.Enums.ConfigKeyEnum.Redondeo:
                        if (item.Data != null) {
                            this._IConfiguration.Redondeo = DbConvert.ConvertDecimal(item.Data);
                        } else {
                            this._IConfiguration.Redondeo = 0;
                        }
                        break;
                    case Domain.Empresa.Enums.ConfigKeyEnum.Moneda:
                        if (item.Data != null) {
                            this._IConfiguration.CveMoneda = DbConvert.ConvertString(item.Data);
                        } else {
                            this._IConfiguration.CveMoneda = "";
                        }
                        break;
                    case Domain.Empresa.Enums.ConfigKeyEnum.ReciboCobroID:
                        this._IConfiguration.IdReciboCobro = DbConvert.ConvertInt32(item.Data);
                        break;
                    case Domain.Empresa.Enums.ConfigKeyEnum.ReciboComisionID:
                        this._IConfiguration.IdReciboComision = DbConvert.ConvertInt32(item.Data);
                        break;
                    case Domain.Empresa.Enums.ConfigKeyEnum.ReciboPagoID:
                        this._IConfiguration.IdReciboPago = DbConvert.ConvertInt32(item.Data);
                        break;
                    default:
                        break;
                }
            }
            return this._IConfiguration;
        }

        public List<IParametroModel> Build(IConfiguration configuration) {
            var d0 = new List<IParametroModel> {
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.Folder, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.Folder),
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.Decimales, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.Decimales.ToString()),
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.Redondeo, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.Redondeo.ToString()),
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.Moneda, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.CveMoneda),
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.ReciboCobroID, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.IdReciboCobro.ToString()),
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.ReciboComisionID, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.IdReciboComision.ToString()),
                new ParametroModel(Domain.Empresa.Enums.ConfigKeyEnum.ReciboPagoID, Domain.Empresa.Enums.ConfigGroupEnum.Bancos, configuration.IdReciboPago.ToString())
            };
            return d0;
        }
    }
}
