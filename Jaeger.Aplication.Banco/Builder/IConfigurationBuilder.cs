﻿using System.Collections.Generic;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Aplication.Banco.Builder {
    /// <summary>
    /// configuracion de parametros de la seccion bancos
    /// </summary>
    public interface IConfigurationBuilder {
        IConfiguration Build(List<IParametroModel> parametros);
    }

    public interface IConfiguracionBuild {
        List<IParametroModel> Build(IConfiguration configuration);
    }
}
