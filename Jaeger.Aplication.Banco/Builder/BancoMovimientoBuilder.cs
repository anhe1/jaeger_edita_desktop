﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Banco.Builder {
    /// <summary>
    /// builder para la creacion de movimiento bancario
    /// </summary>
    public class BancoMovimientoBuilder : IBancoMovimientoBuilder, IBancoMovimientoStatusBuilder, IBancoMovimientoComprobanteBuilder, IBancoMovimientoIdConceptoBuilder, IBancoMovimientoIdBeneficiarioBuilder {
        private readonly IMovimientoBancarioDetailModel _Movimiento;

        public BancoMovimientoBuilder() {
            this._Movimiento = new MovimientoBancarioDetailModel {
                Status = MovimientoBancarioStatusEnum.Transito,
                FechaEmision = DateTime.Now,
                FechaNuevo = DateTime.Now,
                CuentaPropia = new MovimientoBancarioCuentaModel(),
                Comprobantes = new BindingList<MovimientoBancarioComprobanteDetailModel>(),
                Auxiliar = new BindingList<MovimientoBancarioAuxiliarDetailModel>()
            };
        }

        public IBancoMovimientoStatusBuilder New(MovimientoBancarioEfectoEnum efecto = MovimientoBancarioEfectoEnum.Ingreso) {
            this._Movimiento.Tipo = efecto;
            return this;
        }

        public IBancoMovimientoIdBeneficiarioBuilder IdBeneficiario(int idDirectorio) {
            this._Movimiento.IdDirectorio = idDirectorio;
            return this;
        }

        public IBancoMovimientoIdConceptoBuilder Status(MovimientoBancarioStatusEnum status = MovimientoBancarioStatusEnum.Transito) {
            this._Movimiento.Status = status;
            return this;
        }

        public IBancoMovimientoIdBeneficiarioBuilder IdConcepto(int idConcepto) {
            this._Movimiento.IdConcepto = idConcepto;
            return this;
        }

        public IBancoMovimientoComprobanteBuilder Add(MovimientoBancarioComprobanteDetailModel docto) {
            this._Movimiento.Agregar(docto);
            return this;
        }

        public IBancoMovimientoComprobanteBuilder Add(IMovimientoBancarioComprobanteDetailModel docto) {
            this._Movimiento.Agregar(docto as MovimientoBancarioComprobanteDetailModel);
            return this;
        }

        public IBancoMovimientoComprobanteBuilder Clone(IMovimientoBancarioDetailModel model) {
            MapperClassExtensions.MatchAndMap<MovimientoBancarioDetailModel, MovimientoBancarioDetailModel>(model as MovimientoBancarioDetailModel, this._Movimiento as MovimientoBancarioDetailModel);
            model.Id = 0;
            model.FechaEmision = DateTime.Now;
            model.Creo = string.Empty;
            model.FechaModifica = null;
            model.Modifica = string.Empty;
            for (int i = 0; i < model.Comprobantes.Count; i++) {
                model.Comprobantes[i].Id = 0;
                model.Comprobantes[i].IdMovimiento = 0;
                model.Comprobantes[i].Identificador = string.Empty;
                model.Comprobantes[i].FechaNuevo = DateTime.Now;
                model.Comprobantes[i].Creo = string.Empty;
                model.Comprobantes[i].FechaModifica = null;
                model.Comprobantes[i].Modifica = string.Empty;
            }
            return this;
        }

        /// <summary>
        /// Crear movimiento entre cuentas
        /// </summary>
        public static IMovimientoBancarioDetailModel TransferenciaEntreCuentas(IMovimientoBancarioDetailModel model1) {
            var model2 = new MovimientoBancarioDetailModel {
                IdConcepto = model1.IdConcepto,
                Abono = model1.Cargo,
                Cargo = model1.Abono,
                Activo = true,
                FechaNuevo = model1.FechaNuevo,
                Creo = model1.Creo,
                Concepto = model1.Concepto,
                IdCuentaP = model1.IdCuentaT,
                IdCuentaT = model1.IdCuentaP,
                BancoP = model1.BancoT,
                BancoT = model1.BancoP,
                BeneficiarioP = model1.BeneficiarioT,
                BeneficiarioT = model1.BeneficiarioP,
                BeneficiarioRFCP = model1.BeneficiarioRFCT,
                BeneficiarioRFCT = model1.BeneficiarioRFCP,
                ClaveBancoP = model1.ClaveBancoT,
                ClaveBancoT = model1.ClaveBancoP,
                ClaveFormaPago = model1.ClaveFormaPago,
                ClaveMoneda = model1.ClaveMoneda,
                CuentaCLABEP = model1.CuentaCLABET,
                CuentaCLABET = model1.CuentaCLABEP,
                FechaAplicacion = model1.FechaAplicacion,
                FechaBoveda = model1.FechaBoveda,
                FechaCancela = model1.FechaCancela,
                FechaDocto = model1.FechaDocto,
                FechaEmision = model1.FechaEmision,
                FechaModifica = model1.FechaModifica,
                FechaVence = model1.FechaVence,
                FormaPagoDescripcion = model1.FormaPagoDescripcion,
                Modifica = model1.Modifica,
                Nota = model1.Nota,
                Autoriza = model1.Autoriza,
                NumAutorizacion = model1.NumAutorizacion,
                NumDocto = model1.NumDocto,
                NumeroCuentaP = model1.NumeroCuentaT,
                NumeroCuentaT = model1.NumeroCuentaP,
                NumOperacion = model1.NumOperacion,
                Referencia = model1.Referencia,
                ReferenciaNumerica = model1.ReferenciaNumerica,
                IdStatus = model1.IdStatus,
                SucursalP = model1.SucursalT,
                SucursalT = model1.SucursalP,
                Tipo = MovimientoBancarioEfectoEnum.Ingreso,
                TipoCambio = model1.TipoCambio,
                IdTipoOperacion = model1.IdTipoOperacion,
                Cancela = model1.Cancela
            };

            return model2;
        }

        public IMovimientoBancarioDetailModel Build() {
            return this._Movimiento;
        }
    }
}
