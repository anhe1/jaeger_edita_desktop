﻿using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.Aplication.Banco.Builder {
    public class BancoCuentaBuilder : IBancoCuentaBuilder, IBancoCuentaBuild {
        protected internal IBancoCuentaDetailModel _CuentaBancaria;

        public BancoCuentaBuilder() { 
            this._CuentaBancaria = new BancoCuentaDetailModel();
        }

        public IBancoCuentaDetailModel Build() {
            return this._CuentaBancaria;
        }
    }
}
