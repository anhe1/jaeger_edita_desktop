﻿using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.Aplication.Banco.Builder {
    /// <summary>
    /// builder para la creacion de movimiento bancario
    /// </summary>
    public interface IBancoMovimientoBuilder {
        IBancoMovimientoStatusBuilder New(MovimientoBancarioEfectoEnum efecto = MovimientoBancarioEfectoEnum.Ingreso);
        IBancoMovimientoComprobanteBuilder Clone(IMovimientoBancarioDetailModel model);
    }

    public interface IBancoMovimientoStatusBuilder {
        IBancoMovimientoIdConceptoBuilder Status(MovimientoBancarioStatusEnum status = MovimientoBancarioStatusEnum.Transito);
        
    }

    public interface IBancoMovimientoIdBeneficiarioBuilder {
        IBancoMovimientoIdBeneficiarioBuilder IdBeneficiario(int idDirectorio);
        IBancoMovimientoComprobanteBuilder Add(IMovimientoBancarioComprobanteDetailModel docto);
        IBancoMovimientoComprobanteBuilder Add(MovimientoBancarioComprobanteDetailModel docto);
        IMovimientoBancarioDetailModel Build();
    }

    public interface IBancoMovimientoIdConceptoBuilder {
        IBancoMovimientoIdBeneficiarioBuilder IdConcepto(int idConcepto);
        IBancoMovimientoIdBeneficiarioBuilder IdBeneficiario(int idDirectorio);
        IBancoMovimientoComprobanteBuilder Add(IMovimientoBancarioComprobanteDetailModel docto);
        IBancoMovimientoComprobanteBuilder Add(MovimientoBancarioComprobanteDetailModel docto);
        IMovimientoBancarioDetailModel Build();
    }

    public interface IBancoMovimientoComprobanteBuilder {
        IMovimientoBancarioDetailModel Build();
    }
}
