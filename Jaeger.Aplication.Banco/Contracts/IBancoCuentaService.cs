﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// Cuentas Bancarias
    /// </summary>
    public interface IBancoCuentaService {
        /// <summary>
        /// obtener catalogo de monedas 
        /// </summary>
        List<MovimientoBancarioMonedaModel> GetMonedas();

        /// <summary>
        /// obtener cuenta bancaria y detalle
        /// </summary>
        /// <param name="index">indice de la cuenta</param>
        BancoCuentaDetailModel GetCuenta(int index);

        /// <summary>
        /// lista de cuentas bancarias disponibles
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        //BindingList<IBancoCuentaDetailModel> GetList(bool onlyActive);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener saldo de la cuenta
        /// </summary>
        /// <param name="idCuenta">indice de la cuenta</param>
        /// <param name="fecha">fecha</param>
        BancoCuentaSaldoDetailModel GetSaldo(int idCuenta, DateTime fecha);

        /// <summary>
        /// almacenar modelo
        /// </summary>
        IBancoCuentaDetailModel Save(IBancoCuentaDetailModel model);

        /// <summary>
        /// almacenar 
        /// </summary>
        /// <param name="model"></param>
        BancoCuentaSaldoDetailModel Save(BancoCuentaSaldoDetailModel model);

        /// <summary>
        /// crear tablas de sistema
        /// </summary>
        void CreateTables();

        //IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
