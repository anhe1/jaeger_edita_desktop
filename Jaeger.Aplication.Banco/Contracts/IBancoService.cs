﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// servicio para movimientos bancarios
    /// </summary>
    public interface IBancoService : IBancoCuentaService {
        IConfiguration Configuration { get; set; }

        /// <summary>
        /// Almacenar movimiento bancario
        /// </summary>
        IMovimientoBancarioDetailModel Save(IMovimientoBancarioDetailModel model);

        IMovimientoBancarioAuxiliarDetailModel Save(IMovimientoBancarioAuxiliarDetailModel auxiliar);

        List<MovimientoBancarioAuxiliarDetailModel> Save(List<MovimientoBancarioAuxiliarDetailModel> auxiliar, string Identificador);

        new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener listado de formas de pago
        /// </summary>
        List<IBancoFormaPagoModel> GetBancoFormaPago(bool onlyActive = true);

        /// <summary>
        /// obtener listado de contribuyentes con la lista de cuentas bancarias asociadas
        /// </summary>
        /// <param name="relationType">tipo de relacion con el directorio</param>
        /// <returns>lista de objetos ContribuyenteCtaBancoDetailModel</returns>
        BindingList<BeneficiarioDetailModel> GetBeneficiarios(BindingList<ItemSelectedModel> relationType, string search = "");

        /// <summary>
        /// aplicar movimiento bancario
        /// </summary>
        /// <param name="seleccionado"></param>
        /// <returns></returns>
        bool Aplicar(IMovimientoBancarioDetailModel seleccionado);

        //string LayoutBanamex(MovimientoBancarioDetailModel movimiento, string descripcion, bool clabe);
        string Test(BindingList<IMovimientoBancarioDetailModel> dataSource);
    }
}
