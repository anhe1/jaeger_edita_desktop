﻿using System.ComponentModel;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.Aplication.Banco {
    public interface IBancoMovientosSearchService {
        /// <summary>
        /// metodo para la busqueda de movimientos bancarios relacionados a un comprobante fiscal para las vistas relacionadas
        /// </summary>
        BindingList<MovimientoComprobanteMergeModelView> GetSearchMovimientos(int index, MovimientoBancarioTipoComprobanteEnum tipo);
    }
}
