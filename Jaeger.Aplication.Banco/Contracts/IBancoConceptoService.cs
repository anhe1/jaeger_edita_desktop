﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// tipos de operaciones bancarias
    /// </summary>
    public interface IBancoConceptoService {
        /// <summary>
        /// obtener listado de conceptos
        /// </summary>
        //BindingList<MovimientoConceptoDetailModel> GetList();

        /// <summary>
        /// almacenar un concepto
        /// </summary>
        MovimientoConceptoDetailModel Save(MovimientoConceptoDetailModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// crear tablas del sistema
        /// </summary>
       void CreateTables();
    }
}
