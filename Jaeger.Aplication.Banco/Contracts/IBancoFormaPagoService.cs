﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// catalogo de formas de pago
    /// </summary>
    public interface IBancoFormaPagoService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// listado de detalle de formas de pago
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        //BindingList<IBancoFormaPagoDetailModel> GetList(bool onlyActive = true);

        /// <summary>
        /// almacenar forma de pago
        /// </summary>
        IBancoFormaPagoDetailModel Save(IBancoFormaPagoDetailModel model);

        /// <summary>
        /// crear tabla de formas de pago
        /// </summary>
        bool CrearTabla();
    }
}
