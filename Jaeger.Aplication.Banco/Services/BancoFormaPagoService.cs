﻿using System.Collections.Generic;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Builder;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// Formas de Pago
    /// </summary>
    public class BancoFormaPagoService : IBancoFormaPagoService {
        protected ISqlBancoFormaPagoRepository formaPagoRepository;

        /// <summary>
        /// constructor
        /// </summary>
        public BancoFormaPagoService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.formaPagoRepository = new SqlSugarBancoFormaPagoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.formaPagoRepository.GetList<T1>(conditionals);
        }

        public IBancoFormaPagoDetailModel Save(IBancoFormaPagoDetailModel model) {
            try {
                var _return = this.formaPagoRepository.Save(model);
                return _return;
            } catch (Domain.Base.Entities.JaegerException ex) {
                System.Console.WriteLine(ex.Message);
            }
            return model;
        }

        public bool CrearTabla() {
            return this.formaPagoRepository.CrearTabla();
        }

        public static IFormaPagoQueryBuilder Query() {
            return new FormaPagoQueryBuilder();
        }
    }
}
