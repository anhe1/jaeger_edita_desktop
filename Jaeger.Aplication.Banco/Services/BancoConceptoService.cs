﻿using System;
using System.Linq;
using System.Collections.Generic;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Banco.Builder;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Banco {
    public class BancoConceptoService : IBancoConceptoService {
        protected ISqlMovimientoConceptoRepository conceptoRepository;

        public BancoConceptoService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.conceptoRepository = new SqlSugarMovimientoConceptoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.conceptoRepository.GetList<T1>(conditionals).ToList();
        }

        /// <summary>
        /// almacenar un concepto
        /// </summary>
        public MovimientoConceptoDetailModel Save(MovimientoConceptoDetailModel model) {
            if (model.IdConcepto == 0) {
                model.Creo = ConfigService.Piloto.Clave;
            } else {
                model.Modifica = ConfigService.Piloto.Clave;
            }
            return this.conceptoRepository.Save(model);
        }

        /// <summary>
        /// obtener lista de tipos de movimiento
        /// </summary>
        public static List<MovimientoBancarioTipoModel> GetMovimientoTipo() {
            List<MovimientoBancarioTipoModel> enums = ((MovimientoBancarioEfectoEnum[])Enum.GetValues(typeof(MovimientoBancarioEfectoEnum))).Select(c => new MovimientoBancarioTipoModel() { Id = (int)c, Descripcion = c.ToString() }).ToList();
            return enums;
        }

        /// <summary>
        /// obtener listado de tipos de operacion
        /// </summary>
        public static List<BancoTipoOperacionModel> GetTipoOperacion() {
            return EnumerationExtension.GetEnumToClass<BancoTipoOperacionModel, BancoTipoOperacionEnum>().ToList();
        }

        public void CreateTables() {
            this.conceptoRepository.Create();
        }

        public static IConceptoQueryBuilder Query() {
            return new ConceptoQueryBuilder();
        }
    }
}
