﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Banco {
    public class BancoMovientosSearchService : IBancoMovientosSearchService {
        protected ISqlMovimientoBancarioRepository movimientoBancarioRepository;

        public BancoMovientosSearchService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.movimientoBancarioRepository = new SqlSugarMovimientoBancarioRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// metodo para la busqueda de movimientos bancarios relacionados a un comprobante fiscal para las vistas relacionadas
        /// </summary>
        public BindingList<MovimientoComprobanteMergeModelView> GetSearchMovimientos(int index, MovimientoBancarioTipoComprobanteEnum tipo) {
            var d0 = this.movimientoBancarioRepository.GetList<MovimientoComprobanteMergeModelView>(new List<Domain.Base.Builder.IConditional> {
                new Conditional("BNCCMP_IDCOM", index.ToString()) ,
                new Conditional("BNCCMP_TIPO", tipo.ToString())
            }).ToList();
            return new BindingList<MovimientoComprobanteMergeModelView>(d0);
        }
    }
}
